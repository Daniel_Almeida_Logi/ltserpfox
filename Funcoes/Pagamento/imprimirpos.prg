
**
** Fun��o que valida a impress�o de talao - Chamada Reimprimir POS
FUNCTION uf_imprimirPOS_ValImpTalao
   LPARAMETERS uv_prev
   
   

	&& Cursor existe
		IF !(RECCOUNT("uCrsPesqVendas")>0)
			uf_perguntalt_chama("N�O EXISTEM DADOS PARA IMPRIMIR.","OK","",64)
			RETURN .F.
		ENDIF
		
	&& Valida tipo de documento
		IF (ucrsPesqVendas.u_tipodoc == 3)
			uf_perguntalt_chama("N�O � POSS�VEL IMPRIMIR TAL�O PARA ESTE TIPO DE DOCUMENTO.","OK","",64)
			RETURN .F.
		ENDIF

        **IF uf_gerais_getParameter_site('ADM0000000136', 'BOOL', mySite)
        IF uf_gerais_getUmValor("B_terminal", "usaDriverWin", "terminal = '" + ALLTRIM(myTerm) + "'")

            IF (ucrsPesqVendas.u_tipodoc == 0)

                uf_PAGAMENTO_impTalaoRecPos(901, ucrsPesqVendas.ftstamp)

            ELSE

                uv_idImp = uf_gerais_getUmValor("b_impressoes", "id", "tabela = 'FT' and nomeImpressao = 'Tal�o Atendimento'")
                IF !uf_imprimirgerais_createCursTalao(uv_idImp, ucrsPesqVendas.ftstamp, ucrsPesqVendas.ftstamp, UCRSPESQVENDAS.u_nratend, 'RESUMOS', 0, ucrspesqvendas.cobrado)
                    uf_perguntalt_chama("ERRO A GERAR CURSORES DO TAL�O." + chr(13) + "POR FAVOR CONTACTE O SUPORTE.", "OK", "", 48)
                    RETURN .F.
                ENDIF

                SELECT FIprint
                GO TOP
                LOCATE FOR !EMPTY(FIprint.obsInt)

                IF FOUND() AND uf_gerais_getParameter_site('ADM0000000151', 'BOOL', mySite)

                  IF !uv_prev
                  
                     uf_imprimirgerais_sendprinter_talao(uv_idImp, .T., .F.)

                     SELECT FIprint
                     GO TOP
                    
                     REPLACE fiprint.obsInt WITH "" FOR 1=1

                     uf_imprimirgerais_sendprinter_talao(uv_idImp, .T., .F.)
                  ELSE

                     uf_imprimirgerais_sendprinter_talao(uv_idImp, .T., .T.)

                  ENDIF

                ELSE

                  REPLACE fiprint.obsInt WITH "" FOR 1=1

                  uf_imprimirgerais_sendprinter_talao(uv_idImp, .T., uv_prev)
                ENDIF

            ENDIF

            SET CENTURY ON

			TEXT TO msel TEXTMERGE NOSHOW
				exec up_getTotAtendimento '<<ALLTRIM(ucrspesqvendas.u_nrAtend)>>'
			ENDTEXT

			uf_gerais_actGrelha("","uc_totsAtendimento", msel)

			uv_totAtendimento = uc_totsAtendimento.totAtendimento

			FECHA("uc_totsAtendimento")

            IF uf_gerais_getParameter_site('ADM0000000154', 'BOOL', mySite) AND CTOD(ucrspesqvendas.fdata) = DATE() AND uv_totAtendimento >= uf_gerais_getParameter_site('ADM0000000154', 'NUM', mySite) AND !uv_prev
                uf_talao_parque()
            ENDIF

        ELSE

            IF uv_prev
                uf_imprimirPOS_ValPrevTalao()
            ELSE
		
                IF (ucrsPesqVendas.u_tipodoc == 0)
                    uf_regVendas_impTalaoRec(.t.)
                ELSE
                    uf_imprimirPOS_ImpTalao(ucrsPesqVendas.ftstamp,'REIMP',.t., 0)
                ENDIF

            ENDIF

        ENDIF
ENDFUNC	

** Fun��o Impress�o Tal�o Venda - Chamada Reimprimir POS (funcao que corre quando s�o impressos tal�es em separado no atendimento [scan na func anterior no painel de pagamento])
FUNCTION uf_imprimirPOS_ImpTalaoOF
	LPARAMETERS lcStamp, lcPainel, lcReImp, lcPrintType, lcAdiReserva  
		
	

	LOCAL lcValidaImp, lcIsentoIva, lcSQL, lcMoeda, lcMotIsencaoIva
	STORE '' TO lcSQL, lcMotIsencaoIva
	STORE  .f. TO lcValidaImp, lcIsencaoIva
	STORE 'EURO' TO lcMoeda
	&& verifica se deve imprimir com que simbolo - Euro // USD 
	lcMoeda = uf_gerais_getParameter("ADM0000000260","text")	
	IF EMPTY(lcMoeda)
		lcMoeda = 'EURO'
	ENDIF
	LOCAL lcMascaraDinheiro
	lcMascaraDinheiro = uf_imprimirpos_devolveMascaraPvpTalao()

	&& Verifica se deve imprimir motivo de isencao de Iva
	lcMotIsencaoIva = uf_gerais_getParameter("ADM0000000261","Bool")	
	
	CREATE CURSOR uCrsStampsQRCode (ftstamp c(50))

	** N� de vendas no talao
	STORE 1 TO myNrVendasTalao
	** CRIAR CURSORES COM DADOS DAS VENDAS (no caso de re-impress�o)**
	**IF (UPPER(ALLTRIM(lcPainel))!="ATENDIMENTO" AND UPPER(ALLTRIM(lcPainel))!="ARQDIGITAL") OR !USED("uCrsFi") OR !USED("uCrsFt") OR !USED("uCrsFt2") &&OR lcPrintType == 76 OR lcPrintType == 85
        IF USED("uCrsFt")
            fecha("uCrsFt")
        ENDIF 
        IF USED("uCrsFt2")
            fecha("uCrsFt2")
        ENDIF 
        IF USED("uCrsFi")
            fecha("uCrsFi")
        ENDIF 
        IF USED("uCrsFi2")
            fecha("uCrsFi2")
        ENDIF 

		uf_gerais_actGrelha("",[uCrsFt],[exec up_touch_ft ']+lcStamp+['])
		select uCrsFt
		uf_gerais_actGrelha("",[uCrsFt2],[exec up_touch_ft2 ']+lcStamp+['])
		select uCrsFt2
		uf_gerais_actGrelha("",[uCrsFi],[exec up_touch_fi ']+alltrim(lcStamp)+[', '', 0])
		select uCrsFi
		uf_gerais_actGrelha("",[uCrsFi2],[exec up_touch_fi2 ']+alltrim(lcStamp)+[', ''])
		select uCrsFi2
		
			
			
	IF !uf_gerais_getParameter("ADM0000000126","BOOL")	
		&& PREPARAR DEFINI��ES GERAIS DA IMPRESSORA
		lcValidaImp = uf_gerais_setImpressoraPOS(.t.)
	
		IF lcValidaImp
			Store .f. to lcIsentoIva

			IF UPPER(ALLTRIM(lcPainel)) == "ATENDIMENTO"
				&& guardar vales de desconto aplicados
				IF USED("uCrsFiPromo")
					fecha("uCrsFiPromo")
				ENDIF
				SELECT uCrsFi
				GO TOP
				select ref, u_refvale, u_epvp, design from uCrsFi where uCrsFi.epromo=.t. AND ALLTRIM(uCrsFi.ftstamp)=ALLTRIM(lcStamp) INTO CURSOR uCrsFiPromo
				SELECT uCrsFiPromo
				**
			ENDIF
		
			*** DEFINIR VARI�VEIS INICIAIS ***
			Local psico, descUtente, docTotalSemDesc
			Store 0 To psico, descUtente, docTotalSemDesc

			*** CALCULAR DADOS PARA O CABE�ALHO E PARA OS TOTAIS ***
			SELECT uCrsFi
			CALCULATE SUM(uCrsFi.epv*uCrsFi.qtt) FOR ALLTRIM(uCrsFi.ftstamp)==ALLTRIM(lcStamp) TO docTotalSemDesc
			SELECT uCrsFt
			LOCATE FOR ALLTRIM(uCrsFt.ftstamp) = ALLTRIM(lcStamp)
			descUtente = docTotalSemDesc - uCrsFt.ETOTAL
			**
			
			** Cria cabe�alho 
			uf_gerais_criarCabecalhoPOS()
			
		
			** Coloca texto vermelho - existem impressoras que n�o suportam esta funcionalidade nesses cen�rios � necess�rio comentar isto.
			uf_gerais_textoVermPOS() 
			
			SELECT uCrsFT
			LOCATE FOR ALLTRIM(uCrsFt.ftstamp) == ALLTRIM(lcStamp)
	
			IF UPPER(ALLTRIM(ucrse1.pais)) == 'PORTUGAL'
				** Define tipo de Documento
				IF (uCrsFt.u_tipodoc == 8 OR uCrsFt.ndoc == 84)
					IF YEAR(uCrsFt.fdata) > 2012
						?"Fatura-Recibo Nr."
					ELSE
						?"Vnd. a Dinheiro Nr."
					ENDIF 
					??TRANSFORM(uCrsFt.FNO,"999999999")+" "
				ENDIF 

				IF (uCrsFt.u_tipodoc == 9) OR (uCrsFt.ndoc == 11) OR (uCrsFt.ndoc == 12)
					?"Fatura Nr."
					??TRANSFORM(uCrsFt.FNO,"999999999")+" "
				ENDIF 

				IF (uCrsFt.u_tipodoc == 2 OR uCrsFt.u_tipodoc == 7)
					?"Nt. Credito Nr."
					??TRANSFORM(uCrsFt.FNO,"999999999")+" "
				ENDIF 
				
				IF (uCrsFt.u_tipodoc == 4)
					?"Fatura Acordo Nr."
					??TRANSFORM(uCrsFt.FNO,"999999999")+" "
				ENDIF

				** IMPRIMIR TAG IDENTIFICADORA DO DOCUMENTO DE VENDA **
				If (uCrsFt.u_tipodoc=8 OR uCrsFt.ndoc=84) AND uCrsFt.u_lote=0 AND uCrsFt.cobrado=.f.
					??"(VD)"
				EndIf
				If (uCrsFt.u_tipodoc=8 OR uCrsFt.ndoc=84) AND uCrsFt.u_lote=0 AND uCrsFt.cobrado=.t.
					??"(VD)(S)"
				EndIf
				If (uCrsFt.u_tipodoc=8 OR uCrsFt.ndoc=84) AND uCrsFt.u_lote > 0
					??"(VD-CR)"
				EndIf
				If (uCrsFt.u_tipodoc=9) AND uCrsFt.cobrado=.f. AND uCrsFt.u_lote=0
					??"(FT)"
				EndIf
				If (uCrsFt.u_tipodoc=9) AND uCrsFt.cobrado=.T. AND uCrsFt.u_lote=0
					??"(FT)(S)"
				EndIf
				If (uCrsFt.u_tipodoc=9) AND uCrsFt.u_lote > 0
					??"(FT-CR)"
				EndIf
				If (uCrsFt.u_tipodoc=2 OR uCrsFt.u_tipodoc=7)
					??"(RC)"
				ENDIF
				IF (uCrsFt.u_tipodoc=4) And uCrsFt.cobrado=.f. And uCrsFt.u_lote=0
					??"(FA)"
				ENDIF
				IF (uCrsFt.u_tipodoc=4) AND uCrsFt.cobrado=.T. AND uCrsFt.u_lote=0
					??"(FA)(S)"
				ENDIF
				IF (uCrsFt.u_tipodoc=4) AND uCrsFt.u_lote > 0
					??"(FA-CR)"
				ENDIF

				** Imprimir serie de factura��o **
				??"/" + astr(uCrsFt.ndoc)
			ELSE
				SELECT uCrsFt
				IF uCrsFt.ftano>2020
					**? ALLTRIM(uCrsFt.nmdoc) + ' Nr. ' + alltrim(lctiposaft) + ' ' + alltrim(str(uCrsFt.ndoc)) + '/' + alltrim(str(uCrsFt.fno))
					? ALLTRIM(uCrsFt.nmdoc) + ' Nr. ' + alltrim(lctiposaft) + ' ' + alltrim(str(uCrsFt.ndoc))+ alltrim(str(uCrsFt.ftano)) + '/' + alltrim(str(uCrsFt.fno))
				ELSE
					? ALLTRIM(uCrsFt.nmdoc) + ' Nr. ' + alltrim(lctiposaft) + ' ' + alltrim(str(uCrsFt.ndoc)) + '/' + alltrim(str(uCrsFt.fno))
				ENDIF 
			ENDIF 
	
			** Data e Hora do documento
			?"Data: "
			??uf_gerais_getDate(uCrsFt.FDATA,"DATA")
			??"      Hora: "
			**??uCrsFt.SAIDA
			?? Substr(uCrsFt.ousrhora,1,5)

			&& existem impressoras que n�o suportam esta funcionalidade nesses cen�rios � necess�rio comentar isto.
			uf_gerais_resetCorPOS() 
			
		
			&& existem impressoras que n�o suportam esta funcionalidade nesses cen�rios � necess�rio comentar isto.		
			uf_gerais_resetCorPOS() 
			
			uf_gerais_separadorPOS()

			** Dados do Cliente 
			?""
			??"Tal�o de Oferta"
			?""
			IF uCrsFt.no != 200 
				?"Oferecido Por: "
				??Alltrim(uCrsFt.NOME)  
			ENDIF
			?ALLTRIM(uf_gerais_getParameter_site('ADM0000000224', 'TEXT', mySite))

			SELECT uCrsFt2
			LOCATE FOR ALLTRIM(uCrsFt2.ft2stamp)=ALLTRIM(lcStamp)

			
			
			LOCAL lcTamanhoDesign
				
			
				
			IF EMPTY(uCrsFt2.u_codigo)
				** Impress�o das Colunas
				uf_gerais_separadorPOS()
				&& Valida impress�o IVA Logica tem de ser melhorada - Lu�s Leal
				
				**
				SELECT uCrsFi
				SCAN FOR ALLTRIM(uCrsFi.ftstamp) = ALLTRIM(lcStamp) AND uCrsFi.epromo = .f. AND !INLIST(ALLTRIM(uCrsFi.ref),'V000001')
					lcTamanhoDesign = uf_gerais_getParameter("ADM0000000194","NUM") - IIF(UPPER(ALLTRIM(uCrsFi.lobs2))=='REF.VD.ORIG.',2,0) - IIF(uCrsFi.iva==0 and !(UPPER(ALLTRIM(uCrsFi.lobs2))=='REF.VD.ORIG.'),5,0)
					**?IIF(UPPER(ALLTRIM(uCrsFi.lobs2))=='REF.VD.ORIG.','  ','') + LEFT(ALLTRIM(uCrsFi.DESIGN),lcTamanhoDesign) + IIF(uCrsFi.iva==0 and !(UPPER(ALLTRIM(uCrsFi.lobs2))=='REF.VD.ORIG.') and ALLTRIM(uCrsFi.ref)!='V000001',' (*1)','')
					?IIF(UPPER(ALLTRIM(uCrsFi.lobs2))=='REF.VD.ORIG.','  ','') + LEFT(ALLTRIM(uCrsFi.DESIGN),lcTamanhoDesign)
					IF uf_gerais_getParameter("ADM0000000194","NUM") >= 45
						IF !(UPPER(ALLTRIM(uCrsFi.lobs2))=='REF.VD.ORIG.')
							?"QTD: "
							??TRANSFORM(uCrsFi.QTT,"99999.9") +"     "
						ENDIF
					ELSE
						IF !(UPPER(ALLTRIM(uCrsFi.lobs2))=='REF.VD.ORIG.')
							?"QTD: "
							??PADC(astr(round(uCrsFi.QTT,2),8,2),10," ") + "|"
							
						ENDIF
					ENDIF
				ENDSCAN 
			ENDIF 
			uf_gerais_separadorPOS()
			
			&& existem impressoras que n�o suportam esta funcionalidade nesses cen�rios � necess�rio comentar isto.
			uf_gerais_textoVermPOS()
			
			
			&& existem impressoras que n�o suportam esta funcionalidade nesses cen�rios � necess�rio comentar isto.	
			uf_gerais_resetCorPOS() 

		
		
			SELECT uCrsFt
			GO TOP
			IF !EMPTY(uCrsFt.u_nratend)
				?
				??	chr(27)+chr(97)+chr(1)
				?'Atendimento:'
				?
				???	 chr(29)+chr(104)+chr(50) + chr(29)+chr(72)+chr(2) + chr(29)+chr(119)+chr(2) + chr(29)+chr(107)+chr(4) + UPPER(ALLTRIM(uCrsFt.u_nratend)) + chr(0)
				uf_gerais_textoLJPOS()
			ENDIF	

			uf_gerais_textoCJPOS()

			

			&& IMPRESS�O DO RODAP�  
			??? chr(27)+chr(97)+chr(1)
		
			? 'Os bens/servicos adquiridos foram colocados a'
			? 'disposicao do adquirente na data do documento'
			? "Logitools, Lda"
			uf_gerais_feedCutPOS()
		ENDIF
		
		
		uf_gerais_setimpressorapos(.F.)         
		SET PRINTER TO DEFAULT
		
		
		
		
	ENDIF



	
ENDFUNC 



FUNCTION uf_imprimirPOS_reImpTalaoOf
   LPARAMETERS uv_prev
   
   

	&& Cursor existe
		IF !(RECCOUNT("uCrsPesqVendas")>0)
			uf_perguntalt_chama("N�O EXISTEM DADOS PARA IMPRIMIR.","OK","",64)
			RETURN .F.
		ENDIF
		
	&& Valida tipo de documento
		IF (ucrsPesqVendas.u_tipodoc == 3)
			uf_perguntalt_chama("N�O � POSS�VEL IMPRIMIR TAL�O PARA ESTE TIPO DE DOCUMENTO.","OK","",64)
			RETURN .F.
		ENDIF
		
		IF uf_gerais_getUmValor("td", "tipodoc", "ndoc = '" + ALLTRIM(STR(uCrsPesqVendas.ndoc)) + "'")!=1
			uf_perguntalt_chama("N�O � POSS�VEL IMPRIMIR TAL�O PARA ESTE TIPO DE DOCUMENTO.","OK","",64)
			RETURN .F.
		ENDIF
		
	
		


        **IF uf_gerais_getParameter_site('ADM0000000136', 'BOOL', mySite)
        IF uf_gerais_getUmValor("B_terminal", "usaDriverWin", "terminal = '" + ALLTRIM(myTerm) + "'")

            IF (ucrsPesqVendas.u_tipodoc == 0)

                uf_PAGAMENTO_impTalaoRecPos(901, ucrsPesqVendas.ftstamp)

            ELSE

                uv_idImp = uf_gerais_getUmValor("b_impressoes", "id", "tabela = 'FT' and nomeImpressao = 'Talao Oferta'")
                IF !uf_imprimirgerais_createCursTalao(uv_idImp, ucrsPesqVendas.ftstamp, ucrsPesqVendas.ftstamp, UCRSPESQVENDAS.u_nratend, 'RESUMOS', 0, ucrspesqvendas.cobrado)
                    uf_perguntalt_chama("ERRO A GERAR CURSORES DO TAL�O." + chr(13) + "POR FAVOR CONTACTE O SUPORTE.", "OK", "", 48)
                    RETURN .F.
                ENDIF

                SELECT FIprint
                GO TOP
                LOCATE FOR !EMPTY(FIprint.obsInt)

                IF FOUND() AND uf_gerais_getParameter_site('ADM0000000151', 'BOOL', mySite)

                  IF !uv_prev
                  
                     uf_imprimirgerais_sendprinter_talao(uv_idImp, .T., .F.)

                     SELECT FIprint
                     GO TOP
                    
                     REPLACE fiprint.obsInt WITH "" FOR 1=1

                     uf_imprimirgerais_sendprinter_talao(uv_idImp, .T., .F.)
                  ELSE

                     uf_imprimirgerais_sendprinter_talao(uv_idImp, .T., .T.)

                  ENDIF

                ELSE

                  REPLACE fiprint.obsInt WITH "" FOR 1=1

                  uf_imprimirgerais_sendprinter_talao(uv_idImp, .T., uv_prev)
                ENDIF

            ENDIF

            SET CENTURY ON

			TEXT TO msel TEXTMERGE NOSHOW
				exec up_getTotAtendimento '<<ALLTRIM(ucrspesqvendas.u_nrAtend)>>'
			ENDTEXT

			uf_gerais_actGrelha("","uc_totsAtendimento", msel)

			uv_totAtendimento = uc_totsAtendimento.totAtendimento

			FECHA("uc_totsAtendimento")

            IF uf_gerais_getParameter_site('ADM0000000154', 'BOOL', mySite) AND CTOD(ucrspesqvendas.fdata) = DATE() AND uv_totAtendimento >= uf_gerais_getParameter_site('ADM0000000154', 'NUM', mySite) AND !uv_prev
                uf_talao_parque()
            ENDIF

        ELSE
			uf_imprimirPOS_ImpTalaoOF(ucrsPesqVendas.ftstamp,'REIMP',.t., 0)
        ENDIF
ENDFUNC	












** Fun��o que previsualiza o talao 
FUNCTION uf_imprimirPOS_ValPrevTalao
	&& Valida��es
	
		IF (ucrsPesqVendas.u_tipodoc == 0)
			uf_perguntalt_chama("N�O � POSS�VEL PPREVER ESTE TIPO DE DOCUMENTOS.","OK","",64)
			RETURN .F.
		ENDIF
		
		&& Cursor existe
		IF !(RECCOUNT("ucrsPesqVendas")>0)
			uf_perguntalt_chama("N�O EXISTEM DADOS PARA IMPRIMIR.","OK","",64)
			RETURN .F.
		ENDIF
	
		&& Valida tipo de documento
		IF (ucrsPesqVendas.u_tipodoc == 3)
			uf_perguntalt_chama("N�O � POSS�VEL PR�-VISUALIZAR O TAL�O DESTE TIPO DE DOCUMENTO.","OK","",64)
			RETURN .F.
		ENDIF	
		
	uf_imprimirpos_prevTalao(ucrsPesqVendas.ftstamp, "REIMP", .f.)
ENDFUNC


FUNCTION uf_imprimirpos_validaSeDeveImprimeTalao
	LPARAMETERS lcToken

	
	IF(LEN(lcToken)>1)
		RETURN .t.
	ENDIF
	
	
	RETURN .f.
ENDFUNC



** Fun��o que valida impress�o Receita - Chamada Reimprimir POS
FUNCTION uf_imprimirpos_ValImpRec
	LPARAMETERS lcVerso, uv_prev

	&& Cursor existe
	IF !(RECCOUNT("uCrsPesqVendas")>0)
		uf_perguntalt_chama("N�O EXISTEM DADOS PARA IMPRIMIR.","OK","",64)
		RETURN .F.
	ENDIF

	&& Troca ordem impress�o receitas
	** Ordem normal do softwaref
	IF uCrsE1.OrdemImpressaoReceita == .f.	
		SELECT uCrsPesqVendas
		DO CASE
			CASE lcVerso == 1
				IF ucrsPesqVendas.u_lote > 0

					**IF uf_gerais_getParameter_site('ADM0000000136', 'BOOL', mySite)
                    IF uf_gerais_getUmValor("B_terminal", "usaDriverWin", "terminal = '" + ALLTRIM(myTerm) + "'")
					
						uf_imprimirpos_impRecTalaoComum_talaoVerso(uCrsPesqVendas.ftstamp, lcVerso, .F., .F. ,uv_prev, .T.)
					
					ELSE

                        IF !uv_prev

						    uf_imprimirpos_impRec(uCrsPesqVendas.ftstamp, lcVerso, 'REIMP')

                        ELSE

                            uf_imprimirpos_ValPrevRec(lcVerso)

                        ENDIF
						
					ENDIF
				ELSE
					uf_perguntalt_chama("N�O EXISTEM DADOS PARA IMPRIMIR.","OK","",64)
				ENDIF 
			CASE lcVerso == 2
				IF ucrsPesqVendas.u_lote2 > 0
				
					**IF uf_gerais_getParameter_site('ADM0000000136', 'BOOL', mySite)
               IF uf_gerais_getUmValor("B_terminal", "usaDriverWin", "terminal = '" + ALLTRIM(myTerm) + "'")
					
						uf_imprimirpos_impRecTalaoComum_talaoVerso(uCrsPesqVendas.ftstamp, lcVerso, .F., .F. ,uv_prev, .T.)
					
					ELSE

                        IF !uv_prev

						    uf_imprimirpos_impRec(uCrsPesqVendas.ftstamp, lcVerso, 'REIMP')

                        ELSE

                            uf_imprimirpos_ValPrevRec(lcVerso)

                        ENDIF
						
					ENDIF
				ELSE
					uf_perguntalt_chama("N�O EXISTEM DADOS PARA IMPRIMIR.","OK","",64)
				ENDIF 
			OTHERWISE
				uf_perguntalt_chama("O PAR�METRO VERSO EST� INCORRECTO.","OK","",48)
		ENDCASE 
	ELSE
		SELECT uCrsPesqVendas
		** Caso opcao esteja ativa na ficha completa da empresa
		** A troca s� � feita caso seja aplicado um plano com complementariadade
		** independetemente de ambas as entidades comparticiparem  ou n�o.
		IF uCrsPesqVendas.complement > 0
			DO CASE
				CASE lcVerso == 1
					IF ucrsPesqVendas.u_lote2 > 0
					
						**IF uf_gerais_getParameter_site('ADM0000000136', 'BOOL', mySite)
                        IF uf_gerais_getUmValor("B_terminal", "usaDriverWin", "terminal = '" + ALLTRIM(myTerm) + "'")
						
							uf_imprimirpos_impRecTalaoComum_talaoVerso(uCrsPesqVendas.ftstamp, 2, .F., .F. ,uv_prev, .T.)
						
						ELSE

                            IF !uv_prev

							    uf_imprimirpos_impRec(uCrsPesqVendas.ftstamp, 2, 'REIMP')
                            
                            ELSE

                                uf_imprimirpos_ValPrevRec(2)

                            ENDIF
							
						ENDIF
					ELSE
						uf_perguntalt_chama("N�O EXISTEM DADOS PARA IMPRIMIR.","OK","",64)
					ENDIF 
				CASE lcVerso == 2
					IF ucrsPesqVendas.u_lote > 0
					
						**IF uf_gerais_getParameter_site('ADM0000000136', 'BOOL', mySite)
                        IF uf_gerais_getUmValor("B_terminal", "usaDriverWin", "terminal = '" + ALLTRIM(myTerm) + "'")
						
							uf_imprimirpos_impRecTalaoComum_talaoVerso(uCrsPesqVendas.ftstamp, 1, .F., .F. ,uv_prev, .T.)
						
						ELSE

                            IF !uv_prev

    							uf_imprimirpos_impRec(uCrsPesqVendas.ftstamp, 1, 'REIMP')

                            ELSE

                                uf_imprimirpos_ValPrevRec(1)

                            ENDIF

						ENDIF
					ELSE
						uf_perguntalt_chama("N�O EXISTEM DADOS PARA IMPRIMIR.","OK","",64)
					ENDIF 
				OTHERWISE
					uf_perguntalt_chama("O PAR�METRO VERSO EST� INCORRECTO.","OK","",48)
			ENDCASE 
		ELSE
			DO CASE
				CASE lcVerso == 1
					IF ucrsPesqVendas.u_lote > 0
					
						**IF uf_gerais_getParameter_site('ADM0000000136', 'BOOL', mySite)
                  IF uf_gerais_getUmValor("B_terminal", "usaDriverWin", "terminal = '" + ALLTRIM(myTerm) + "'")
						
							uf_imprimirpos_impRecTalaoComum_talaoVerso(uCrsPesqVendas.ftstamp, lcVerso, .F., .F. ,uv_prev, .T.)
						
						ELSE

                            IF !uv_prev
					
    							uf_imprimirpos_impRec(uCrsPesqVendas.ftstamp, lcVerso, 'REIMP')

                            ELSE

                                uf_imprimirpos_ValPrevRec(lcVerso)

                            ENDIF
							
						ENDIF
					ELSE
						uf_perguntalt_chama("N�O EXISTEM DADOS PARA IMPRIMIR.","OK","",64)
					ENDIF 
				CASE lcVerso == 2
					IF ucrsPesqVendas.u_lote2 > 0
					
						**IF uf_gerais_getParameter_site('ADM0000000136', 'BOOL', mySite)
                  IF uf_gerais_getUmValor("B_terminal", "usaDriverWin", "terminal = '" + ALLTRIM(myTerm) + "'")
						
							uf_imprimirpos_impRecTalaoComum_talaoVerso(uCrsPesqVendas.ftstamp, lcVerso, .F., .F. ,uv_prev, .T.)
						
						ELSE

                            IF !uv_prev

    							uf_imprimirpos_impRec(uCrsPesqVendas.ftstamp, lcVerso, 'REIMP')

                            ELSE

                                uf_imprimirpos_ValPrevRec(lcVerso)

                            ENDIF

						ENDIF
					ELSE
						uf_perguntalt_chama("N�O EXISTEM DADOS PARA IMPRIMIR.","OK","",64)
					ENDIF 
				OTHERWISE
					uf_perguntalt_chama("O PAR�METRO VERSO EST� INCORRECTO.","OK","",48)
			ENDCASE 
		ENDIF
	ENDIF
ENDFUNC 


** Fun��o que valida previsualiza��o Receita - Chamada Reimprimir POS
FUNCTION uf_imprimirpos_ValPrevRec
	LPARAMETERS lcVerso

	&& Cursor existe
	IF !(RECCOUNT("uCrsPesqVendas")>0)
		uf_perguntalt_chama("N�O EXISTEM DADOS PARA IMPRIMIR.","OK","",64)
		RETURN .F.
	ENDIF
	
	&& Trocar ordem de impress�o das receitas Lu�s Leal 23-06-2015
	IF uCrsE1.OrdemImpressaoReceita == .f.
		DO CASE
			CASE lcVerso == 1
				IF uCrsPesqVendas.u_lote > 0
					uf_imprimirpos_prevRec(uCrsPesqVendas.ftstamp, lcVerso)
				ELSE
					uf_perguntalt_chama("N�O EXISTEM DADOS PARA PREVIS�O.","OK","",64)
				ENDIF 
			CASE lcVerso == 2
				IF ucrsPesqVendas.u_lote2 > 0
					uf_imprimirpos_prevRec(uCrsPesqVendas.ftstamp, lcVerso)
				ELSE
					uf_perguntalt_chama("N�O EXISTEM DADOS PARA PREVIS�O.","OK","",64)
				ENDIF 
			OTHERWISE
				uf_perguntalt_chama("O PAR�METRO VERSO EST� INCORRECTO.","OK","",48)
		ENDCASE 
	ELSE
		SELECT uCrsPesqVendas
		IF uCrsPesqVendas.u_lote2 > 0
			DO CASE
				CASE lcVerso == 1
					IF ucrsPesqVendas.u_lote > 0
						uf_imprimirpos_prevRec(uCrsPesqVendas.ftstamp, 2)
					ELSE
						uf_perguntalt_chama("N�O EXISTEM DADOS PARA PREVIS�O.","OK","",64)
					ENDIF 
				CASE lcVerso == 2
					IF ucrsPesqVendas.u_lote2 > 0
						uf_imprimirpos_prevRec(uCrsPesqVendas.ftstamp, 1)
					ELSE
						uf_perguntalt_chama("N�O EXISTEM DADOS PARA PREVIS�O.","OK","",64)
					ENDIF 
				OTHERWISE
					uf_perguntalt_chama("O PAR�METRO VERSO EST� INCORRECTO.","OK","",48)
			ENDCASE 
		ELSE
			DO CASE
				CASE lcVerso == 1
					IF ucrsPesqVendas.u_lote > 0
						uf_imprimirpos_prevRec(uCrsPesqVendas.ftstamp, lcVerso)
					ELSE
						uf_perguntalt_chama("N�O EXISTEM DADOS PARA PREVIS�O.","OK","",64)
					ENDIF 
				CASE lcVerso == 2
					IF ucrsPesqVendas.u_lote2 > 0
						uf_imprimirpos_prevRec(uCrsPesqVendas.ftstamp, lcVerso)
					ELSE
						uf_perguntalt_chama("N�O EXISTEM DADOS PARA PREVIS�O.","OK","",64)
					ENDIF 
				OTHERWISE
					uf_perguntalt_chama("O PAR�METRO VERSO EST� INCORRECTO.","OK","",48)
			ENDCASE 
		ENDIF
	ENDIF
ENDFUNC 

** Fun��o que valida impress�o do ultimo tal�o do Tpa - Chamada painel Reimprimir POS
*!*	FUNCTION uf_imprimirpos_VAlTpaUltimo
*!*		LPARAMETERS lcSatus
*!*		
*!*		
*!*		RELEASE lcResponseArray
*!*		DIMENSION lcResponseArray(2)
*!*			

*!*		uf_tpa_operation(@lcResponseArray,"Aguarde...","","app.msg.print.ecr_pos",0)	

*!*		IF(lcResponseArray(1)!=0) && not ok
*!*			uf_perguntalt_chama(uf_tpa_messagemResposta(ALLTRIM(lcResponseArray(2))),"OK","",16)			
*!*		ENDIF

*!*		RELEASE lcResponseArray
*!*	ENDFUNC

 
 FUNCTION uf_imprimirpos_devolveMascaraPvpTalao
 	LOCAL lcMoeda 
 	
 	lcMoeda = uf_gerais_getParameter("ADM0000000260","text")	
 		
	IF EMPTY(lcMoeda)
		lcMoeda = 'EURO'
	ENDIF

	uv_mascara = uf_gerais_getParameter("ADM0000000361","TEXT")	

	DO CASE
		CASE !EMPTY(uv_mascara)
			RETURN ALLTRIM(uv_mascara)
		CASE ALLTRIM(lcMoeda) == 'AOA'
			RETURN  "9999999.99"
		OTHERWISE
			RETURN  "99999.99"	
	ENDCASE

 ENDFUNC
 


** Fun��o que valida impress�o Dados Psicotr�picos - Chamada painel Reimprimir POS
FUNCTION uf_imprimirpos_ValImpPrevPsico
	LPARAMETERS lcPrev
	
	&& Cursor existe
	IF !(RECCOUNT("ucrsPesqVendas")>0)
		uf_perguntalt_chama("N�O EXISTEM DADOS PARA IMPRIMIR.","OK","",64)
		RETURN .F.
	ENDIF
	
	IF lcPrev
		uf_imprimirpos_ImpPrevPsico(ucrsPesqVendas.FTSTAMP, .t., 'REIMP')
	ELSE 
		uf_imprimirpos_ImpPrevPsico(ucrsPesqVendas.FTSTAMP, .f., 'REIMP')
	ENDIF 
ENDFUNC


** Fun��o Impress�o Tal�o Venda - Chamada Reimprimir POS (funcao que corre quando s�o impressos tal�es em separado no atendimento [scan na func anterior no painel de pagamento])
FUNCTION uf_imprimirPOS_ImpTalao
	LPARAMETERS lcStamp, lcPainel, lcReImp, lcPrintType, lcAdiReserva  
		
	LOCAL lcValidaImp, lcIsentoIva, lcSQL, lcMoeda, lcMotIsencaoIva
	STORE  .f. TO lcValidaImp, lcIsencaoIva
	STORE 'EURO' TO lcMoeda
	
	&& verifica se deve imprimir com que simbolo - Euro // USD 
	lcMoeda = uf_gerais_getParameter("ADM0000000260","text")	
	IF EMPTY(lcMoeda)
		lcMoeda = 'EURO'
	ENDIF

	LOCAL lcMascaraDinheiro
	lcMascaraDinheiro = uf_imprimirpos_devolveMascaraPvpTalao()

	&& Verifica se deve imprimir motivo de isencao de Iva
	lcMotIsencaoIva = uf_gerais_getParameter("ADM0000000261","Bool")	
	
	CREATE CURSOR uCrsStampsQRCode (ftstamp c(50))

	** N� de vendas no talao
	STORE 1 TO myNrVendasTalao
	
	** CRIAR CURSORES COM DADOS DAS VENDAS (no caso de re-impress�o)**
	**IF (UPPER(ALLTRIM(lcPainel))!="ATENDIMENTO" AND UPPER(ALLTRIM(lcPainel))!="ARQDIGITAL") OR !USED("uCrsFi") OR !USED("uCrsFt") OR !USED("uCrsFt2") &&OR lcPrintType == 76 OR lcPrintType == 85
        IF USED("uCrsFt")
            fecha("uCrsFt")
        ENDIF 
        IF USED("uCrsFt2")
            fecha("uCrsFt2")
        ENDIF 
        IF USED("uCrsFi")
            fecha("uCrsFi")
        ENDIF 
        IF USED("uCrsFi2")
            fecha("uCrsFi2")
        ENDIF 

		uf_gerais_actGrelha("",[uCrsFt],[exec up_touch_ft ']+lcStamp+['])
		select uCrsFt
		uf_gerais_actGrelha("",[uCrsFt2],[exec up_touch_ft2 ']+lcStamp+['])
		select uCrsFt2
		uf_gerais_actGrelha("",[uCrsFi],[exec up_touch_fi ']+alltrim(lcStamp)+[', '', 0])
		select uCrsFi
		uf_gerais_actGrelha("",[uCrsFi2],[exec up_touch_fi2 ']+alltrim(lcStamp)+[', ''])
		select uCrsFi2
		
		
	
	
	   	lcSQL = ''
		TEXT TO lcSQL NOSHOW TEXTMERGE
			exec up_print_docs_atendimento '<<uCrsFt.ftstamp>>', '', ''
		ENDTEXT 
		IF !uf_gerais_actGrelha("", "uCrsProdComp", lcSql)
			uf_perguntalt_chama("N�o foi poss�vel encontrar a informa��o das vendas [FI]. Por favor contacte o Suporte.", "", "OK", 32)
			RETURN .f.
		ENDIF	
			
	**ENDIF
	
			lcSQL = ''
			TEXT TO lcSQL NOSHOW TEXTMERGE
				select * from B_pagcentral (nolock) WHERE nrAtend = '<<FT.u_nratend>>' AND NO = '<<FT.NO>>'
			ENDTEXT 
			IF !uf_gerais_actGrelha("", "uCrsInfoAtendPagCentral", lcSql)
				uf_perguntalt_chama("N�o foi poss�vel encontrar a informa��o do atendimento. Por favor contacte o Suporte.", "", "OK", 32)
				RETURN .f.
			ENDIF
		
			&& Atribui valores pagamento � no atendimento 
			SELECT uCrsInfoAtendPagCentral
			lcTroco = uCrsInfoAtendPagCentral.etroco
			lcTotal = uCrsInfoAtendPagCentral.total_bruto
			lcModoPag1  = uCrsInfoAtendPagCentral.evdinheiro_semTroco
			lcModoPag2  = uCrsInfoAtendPagCentral.epaga1
			lcModoPag3  = uCrsInfoAtendPagCentral.epaga2
			lcModoPag4  = uCrsInfoAtendPagCentral.echtotal
			lcModoPag5  = uCrsInfoAtendPagCentral.epaga3
			lcModoPag6  = uCrsInfoAtendPagCentral.epaga4
			lcModoPag7  = uCrsInfoAtendPagCentral.epaga5
			lcModoPag8  = uCrsInfoAtendPagCentral.epaga6
			
			FECHA("uCrsInfoAtendPagCentral")
			
			&& valor do atendimento por tipo de pagamento
			LOCAL lcMpag1, lcMpag2, lcMpag3, lcMpag4, lcMpag5, lcMpag6, lcMpag7, lcMpag8
			STORE '' TO lcMpag1, lcMpag2, lcMpag3, lcMpag4, lcMpag5, lcMpag6, lcMpag7, lcMpag8
			
			IF !USED("uCrsConfigModPag")
				TEXT TO lcSQL NOSHOW TEXTMERGE
					select * from B_modoPag
				ENDTEXT 
				If !uf_gerais_actGrelha("", "UcrsConfigModPag", lcSql)
					uf_perguntalt_chama("N�o foi poss�vel verificar as configura��es de Modo de Pagamento. Contacte o suporte.", "", "OK", 32)
					RETURN .f.
				ENDIF
			ENDIF
			
			SELECT uCrsConfigModPag
			GO TOP
			SCAN 
				DO CASE 
					CASE ALLTRIM(UcrsConfigModPag.ref) == "01"
						lcMpag1 = ALLTRIM(UcrsConfigModPag.design)
					CASE ALLTRIM(UcrsConfigModPag.ref) == "02"
						lcMpag2 = ALLTRIM(UcrsConfigModPag.design)			
					CASE ALLTRIM(UcrsConfigModPag.ref) == "03"
						lcMpag3 = ALLTRIM(UcrsConfigModPag.design)
					CASE ALLTRIM(UcrsConfigModPag.ref) == "04"
						lcMpag4 = ALLTRIM(UcrsConfigModPag.design)
					CASE ALLTRIM(UcrsConfigModPag.ref) == "05"
						lcMpag5 = ALLTRIM(UcrsConfigModPag.design)
					CASE ALLTRIM(UcrsConfigModPag.ref) == "06"
						lcMpag6 = ALLTRIM(UcrsConfigModPag.design)
					CASE ALLTRIM(UcrsConfigModPag.ref) == "07"
						lcMpag7 = ALLTRIM(UcrsConfigModPag.design)
					CASE ALLTRIM(UcrsConfigModPag.ref) == "08"
						lcMpag8 = ALLTRIM(UcrsConfigModPag.design)																		
				ENDCASE
			ENDSCAN	
			
			IF myTipoCartao == 'Valor'
				LOCAL lcTotalHistCartao, lcValDispCartao, uv_nrCartao
				STORE 0 TO lcTotalHistCartao, lcValDispCartao
                STORE '' to uv_nrCartao
				SELECT ucrsft			
				lcSQL = ''

				IF ucrsFT.tiposaft == 'NC'

					TEXT TO msel TEXTMERGE NOSHOW

						SELECT DISTINCT
							u_hclstamp
						FROM
							ft(nolock)
						WHERE
							ftstamp in (select fi.ftstamp from fi(nolock) where fi.fistamp in (select ffi.ofistamp from fi(nolock) as ffi where ffi.ftstamp = '<<ucrsFT.ftstamp>>'))
							and u_hclstamp <> ''

					ENDTEXT

					IF uf_gerais_actGrelha("","uv_utentes",msel)

						IF RECCOUNT("uv_utentes") = 1

							SELECT uv_utentes

							TEXT TO lcSQL TEXTMERGE NOSHOW

								SELECT 
									b_fidel.valcartaohist, 
									b_fidel.valcartao,
									b_fidel.nrcartao
								FROM 
									b_fidel(NOLOCK) 
									JOIN b_utentes(NOLOCK) ON b_fidel.clno = b_utentes.no AND b_fidel.clestab = b_utentes.estab
								WHERE 
									b_utentes.utstamp = '<<ALLTRIM(uv_utentes.u_hclstamp)>>'

							ENDTEXT

						ENDIF

						FECHA("uv_utentes")

					ENDIF

				ENDIF

				IF EMPTY(lcSql)

	                IF !EMPTY(ucrsft.u_hclstamp)

    	                TEXT To lcSQL TEXTMERGE NOSHOW
	    					SELECT 
								b_fidel.valcartaohist, 
								b_fidel.valcartao,
								b_fidel.nrcartao
							FROM 
								b_fidel(NOLOCK) 
								JOIN b_utentes(NOLOCK) ON b_fidel.clno = b_utentes.no AND b_fidel.clestab = b_utentes.estab
							WHERE 
								b_utentes.utstamp = '<<ALLTRIM(ucrsft.u_hclstamp)>>'
						ENDTEXT 

					ELSE

						TEXT To lcSQL TEXTMERGE NOSHOW
							SELECT valcartaohist, valcartao, nrcartao FROM b_fidel WHERE clno=<<ucrsft.no>> and clestab=<<ucrsft.estab>>
						ENDTEXT 

					ENDIF

				ENDIF

				uf_gerais_actGrelha("","ucrsverifvalcartao",lcSQL)

				IF RECCOUNT("ucrsverifvalcartao")>0 then
					lcTotalHistCartao = ucrsverifvalcartao.valcartaohist
					lcValDispCartao = ucrsverifvalcartao.valcartao
                    uv_nrCartao = ucrsverifvalcartao.nrcartao
				ENDIF 
			ENDIF 
			
			LOCAL lctiposaft
			STORE 0 TO lctiposaft
			Text To lcSql Noshow Textmerge
				select tiposaft from td where ndoc=<<uCrsFt.ndoc>>
			ENDTEXT
			uf_gerais_actGrelha("","uCrsSaftFile",lcSql)
			lctiposaft= uCrsSaftFile.tiposaft
			fecha("uCrsSaftFile")

	IF !uf_gerais_getParameter("ADM0000000126","BOOL")	
		&& PREPARAR DEFINI��ES GERAIS DA IMPRESSORA
		lcValidaImp = uf_gerais_setImpressoraPOS(.t.)
	
		IF lcValidaImp
			Store .f. to lcIsentoIva

			IF UPPER(ALLTRIM(lcPainel)) == "ATENDIMENTO"
				&& guardar vales de desconto aplicados
				IF USED("uCrsFiPromo")
					fecha("uCrsFiPromo")
				ENDIF
				SELECT uCrsFi
				GO TOP
				select ref, u_refvale, u_epvp, design from uCrsFi where uCrsFi.epromo=.t. AND ALLTRIM(uCrsFi.ftstamp)=ALLTRIM(lcStamp) INTO CURSOR uCrsFiPromo
				SELECT uCrsFiPromo
				**
			ENDIF
		
			*** DEFINIR VARI�VEIS INICIAIS ***
			Local psico, descUtente, docTotalSemDesc
			Store 0 To psico, descUtente, docTotalSemDesc

			*** CALCULAR DADOS PARA O CABE�ALHO E PARA OS TOTAIS ***
			SELECT uCrsFi
			CALCULATE SUM(uCrsFi.epv*uCrsFi.qtt) FOR ALLTRIM(uCrsFi.ftstamp)==ALLTRIM(lcStamp) TO docTotalSemDesc
			SELECT uCrsFt
			LOCATE FOR ALLTRIM(uCrsFt.ftstamp) = ALLTRIM(lcStamp)
			descUtente = docTotalSemDesc - uCrsFt.ETOTAL
			**
			
			** Cria cabe�alho 
			uf_gerais_criarCabecalhoPOS()
			
			** Controla Se � impress�o Original ou 2� via
			IF lcReImp
				IF uf_gerais_getParameter("ADM0000000209","BOOL")
					? PADL("2a VIA",uf_gerais_getParameter("ADM0000000194","NUM")," ")
					**uf_gerais_separadorPOS()
				ENDIF
			ELSE
				? PADL("ORIGINAL",uf_gerais_getParameter("ADM0000000194","NUM")," ")
			ENDIF

			** Coloca texto vermelho - existem impressoras que n�o suportam esta funcionalidade nesses cen�rios � necess�rio comentar isto.
			uf_gerais_textoVermPOS() 
			
			SELECT uCrsFT
			LOCATE FOR ALLTRIM(uCrsFt.ftstamp) == ALLTRIM(lcStamp)
	
			IF UPPER(ALLTRIM(ucrse1.pais)) == 'PORTUGAL'
				** Define tipo de Documento
				IF (uCrsFt.u_tipodoc == 8 OR uCrsFt.ndoc == 84)
					IF YEAR(uCrsFt.fdata) > 2012
						?"Fatura-Recibo Nr."
					ELSE
						?"Vnd. a Dinheiro Nr."
					ENDIF 
					??TRANSFORM(uCrsFt.FNO,"999999999")+" "
				ENDIF 

				IF (uCrsFt.u_tipodoc == 9) OR (uCrsFt.ndoc == 11) OR (uCrsFt.ndoc == 12)
					?"Fatura Nr."
					??TRANSFORM(uCrsFt.FNO,"999999999")+" "
				ENDIF 

				IF (uCrsFt.u_tipodoc == 2 OR uCrsFt.u_tipodoc == 7)
					?"Nt. Credito Nr."
					??TRANSFORM(uCrsFt.FNO,"999999999")+" "
				ENDIF 
				
				IF (uCrsFt.u_tipodoc == 4)
					?"Fatura Acordo Nr."
					??TRANSFORM(uCrsFt.FNO,"999999999")+" "
				ENDIF

				** IMPRIMIR TAG IDENTIFICADORA DO DOCUMENTO DE VENDA **
				If (uCrsFt.u_tipodoc=8 OR uCrsFt.ndoc=84) AND uCrsFt.u_lote=0 AND uCrsFt.cobrado=.f.
					??"(VD)"
				EndIf
				If (uCrsFt.u_tipodoc=8 OR uCrsFt.ndoc=84) AND uCrsFt.u_lote=0 AND uCrsFt.cobrado=.t.
					??"(VD)(S)"
				EndIf
				If (uCrsFt.u_tipodoc=8 OR uCrsFt.ndoc=84) AND uCrsFt.u_lote > 0
					??"(VD-CR)"
				EndIf
				If (uCrsFt.u_tipodoc=9) AND uCrsFt.cobrado=.f. AND uCrsFt.u_lote=0
					??"(FT)"
				EndIf
				If (uCrsFt.u_tipodoc=9) AND uCrsFt.cobrado=.T. AND uCrsFt.u_lote=0
					??"(FT)(S)"
				EndIf
				If (uCrsFt.u_tipodoc=9) AND uCrsFt.u_lote > 0
					??"(FT-CR)"
				EndIf
				If (uCrsFt.u_tipodoc=2 OR uCrsFt.u_tipodoc=7)
					??"(RC)"
				ENDIF
				IF (uCrsFt.u_tipodoc=4) And uCrsFt.cobrado=.f. And uCrsFt.u_lote=0
					??"(FA)"
				ENDIF
				IF (uCrsFt.u_tipodoc=4) AND uCrsFt.cobrado=.T. AND uCrsFt.u_lote=0
					??"(FA)(S)"
				ENDIF
				IF (uCrsFt.u_tipodoc=4) AND uCrsFt.u_lote > 0
					??"(FA-CR)"
				ENDIF

				** Imprimir serie de factura��o **
				??"/" + astr(uCrsFt.ndoc)
			ELSE
				SELECT uCrsFt
				IF uCrsFt.ftano>2020
					**? ALLTRIM(uCrsFt.nmdoc) + ' Nr. ' + alltrim(lctiposaft) + ' ' + alltrim(str(uCrsFt.ndoc)) + '/' + alltrim(str(uCrsFt.fno))
					? ALLTRIM(uCrsFt.nmdoc) + ' Nr. ' + alltrim(lctiposaft) + ' ' + alltrim(str(uCrsFt.ndoc))+ alltrim(str(uCrsFt.ftano)) + '/' + alltrim(str(uCrsFt.fno))
				ELSE
					? ALLTRIM(uCrsFt.nmdoc) + ' Nr. ' + alltrim(lctiposaft) + ' ' + alltrim(str(uCrsFt.ndoc)) + '/' + alltrim(str(uCrsFt.fno))
				ENDIF 
			ENDIF 
	
			** Data e Hora do documento
			?"Data: "
			??uf_gerais_getDate(uCrsFt.FDATA,"DATA")
			??"      Hora: "
			**??uCrsFt.SAIDA
			?? Substr(uCrsFt.ousrhora,1,5)

			&& existem impressoras que n�o suportam esta funcionalidade nesses cen�rios � necess�rio comentar isto.
			uf_gerais_resetCorPOS() 
			
			** Informa��o de prazo de reg. de vendas suspensas
			IF  uCrsFt.cobrado
				IF uf_gerais_getParameter("ADM0000000014","NUM") > 0
					uf_gerais_resetCorPOS()
					uf_gerais_separadorPOS()
					uf_gerais_textoVermPOS()
					?"Prazo de Regularizacao: " + alltrim(Str(uf_gerais_getParameter("ADM0000000014","NUM"))) + " Dias"
				ENDIF 	 
			ENDIF 
			
			** Controla de tem validade fiscal
			SELECT uCrsFt
			IF uCrsFt.u_tipodoc == 3 OR uCrsFt.u_tipodoc == 4
				? "*** Talao sem Validade Fiscal ***"
			ENDIF
		
			&& existem impressoras que n�o suportam esta funcionalidade nesses cen�rios � necess�rio comentar isto.		
			uf_gerais_resetCorPOS() 
			
			uf_gerais_separadorPOS()

			** Dados do Cliente 
			?"Cliente: "
			??Alltrim(uCrsFt.NOME) + Iif(uCrsFt.no==200, '', ' (' + AStr(uCrsFt.no) + ')(' + AStr(uCrsFt.estab) + ')' ) + IIF(!EMPTY(uCrsFt.no_ext), ' - '+ALLTRIM(uCrsFt.no_ext),'' )
			?"Morada: "
			??ALLTRIM(uCrsFt.MORADA)
			?"Cod.Post.: "
			??LEFT(ALLTRIM(uCrsFt.CODPOST),uf_gerais_getParameter("ADM0000000194","NUM")-11)
			?"Contrib. Nr.: "
			if (ALLTRIM(uCrsFt.ncont)!="999999990") AND !EMPTY(ALLTRIM(uCrsFt.ncont)) AND (ALLTRIM(uCrsFt.ncont)!="9999999999")
				??TRANSFORM(uCrsFt.NCONT,"99999999999999999999")
			ELSE
				??ALLTRIM(uf_gerais_getParameter_site('ADM0000000075', 'text', mySite))
				IF UPPER(ALLTRIM(ucrse1.pais)) == 'PORTUGAL'
					?"Nao sujeito passivo de IVA "
				ELSE 
					?" "
				ENDIF 
			ENDIF
			?"Tlf.: "
			??substr(uCrsFt.telefone,1,9)

			** N� de Benefici�rio e N� de Utente s� imprime caso seja farm�cia - Lu�s Leal 20150615
			SELECT uCrsE1
			GO TOP 
			IF ALLTRIM(uCrsE1.tipoempresa) == 'FARMACIA'
				?"N.Utente: "
				??IIF(EMPTY(ALLTRIM(uCrsFt2.u_nbenef)),"         ",ALLTRIM(uCrsFt2.u_nbenef))
				??" N.Benef.: "
				??ALLTRIM(uCrsFt2.u_nbenef2)
			ENDIF
			**


            IF !EMPTY(uCrsFt.u_hclstamp)

                FECHA("uc_dep")

                uf_gerais_actGrelha("","uc_dep","select * from b_utentes(nolock) where utstamp = '" + ALLTRIM(uCrsFt.u_hclstamp) + "'")

                IF RECCOUNT("uc_dep") <> 0

                    IF !uf_gerais_getparameter_site('ADM0000000123', 'BOOL', mysite)
                        uf_gerais_separadorPOS()
                    
                        && Dados Utente a quem � associado os hist�rico de Produtos
                        SELECT uc_dep
                        GO TOP
                    
                        ?"Dependente: "
                        ??Alltrim(uc_dep.NOME) + Iif(uc_dep.no==200, '', ' (' + AStr(uc_dep.no) + ')(' + AStr(uc_dep.estab) + ')' )
                    
                        ?"Morada: "
                        ??ALLTRIM(uc_dep.MORADA)
                        
                        ?"Cod.Post.: "
                        ??LEFT(ALLTRIM(uc_dep.CODPOST),uf_gerais_getParameter("ADM0000000194","NUM")-11)
                        ?"  Tlf.: "
                        ??substr(uc_dep.telefone,1,9)
                        IF ALLTRIM(uCrsE1.tipoempresa) == 'FARMACIA'
                            ??"N.Benef.: "
                            ??IIF(EMPTY(ALLTRIM(uc_dep.nbenef)),"         ",ALLTRIM(uc_dep.nbenef))
                        ENDIF	
                    ENDIF 

                ENDIF

            ENDIF
			
			SELECT uCrsFt2
			LOCATE FOR ALLTRIM(uCrsFt2.ft2stamp)=ALLTRIM(lcStamp)
			
			** informa se foi sujeito a cativa��o de IVA
			IF uCrsFt2.cativa AND uf_gerais_validaIvaDireitoDeducao() == 0
				?"Sujeito a cativa��o de IVA."
			ENDIF 
			
			** SE A VENDA FOR AO DOMICILIO IMPRIME OS DADOS **
			IF uCrsFt2.u_vddom
				uf_gerais_separadorPOS()
				?"Entrega ao Domicilio: "
				?ALLTRIM(uf_gerais_getMacrosReports(5))
				?"Transportador: " + LEFT(ALLTRIM(uCrsFt2.CONTACTO),uf_gerais_getParameter("ADM0000000194","NUM")-15)
				?"Morada: " + ALLTRIM(uCrsFt2.MORADA)
				?"Cod.Post: " + TRANSFORM(uCrsFt2.CODPOST,"########")
				??"  Local.: " + TRANSFORM(uCrsFt2.local,"#################")
				?"Tel.: " + TRANSFORM(uCrsFt2.TELEFONE,"##############")
				??"Email: " + Substr(uCrsFt2.email,1,25)
				IF !Empty(uCrsFt2.u_viatura)
					?"Viatura: " + TRANSFORM(uCrsFt2.u_viatura,"#########################")
					??"Hora: " + Substr(uCrsFt2.horaentrega,1,5)
				ENDIF
			ENDIF 
			**
			
			LOCAL lcTamanhoDesign
				
			** VERIFICAR SE EXISTE COMPARTICIPA��O E IMPRIMIR COLUNAS E LINHAS **
			IF !Empty(uCrsFt2.u_codigo)
				
				** Impress�o colunas 
				uf_gerais_separadorPOS()
				&& Valida impress�o IVA Logica tem de ser melhorada - Lu�s Leal
				IF lcMotIsencaoIva == .t. AND uf_gerais_validaIvaDireitoDeducao() == 0
					?"Produto"
					IF uf_gerais_getParameter("ADM0000000194","NUM") >= 50
						?"   PVP.     Pref.     Qtd.    Comp.   Total    IVA"  &&50
					ELSE
						?"PVP.  | Pref. | Qt. | Comp. | Total |IVA"  &&40
					ENDIF
				ELSE
					?"Produto"
					IF uf_gerais_getParameter("ADM0000000194","NUM") >= 50 
						?"   PVP.     Pref.     Qtd.    Comp.   Total"  &&50
					ELSE
						?"PVP.  | Pref. | Qt. | Comp. | Total"  &&40
					ENDIF
				ENDIF
				
				** Impress�o Linhas
				SELECT uCrsFi
				SCAN FOR ALLTRIM(uCrsFi.ftstamp) = ALLTRIM(lcStamp) AND uCrsFi.epromo = .f. AND !INLIST(ALLTRIM(uCrsFi.ref),'V000001')
					lcTamanhoDesign = uf_gerais_getParameter("ADM0000000194","NUM") - IIF(UPPER(ALLTRIM(uCrsFi.lobs2))=='REF.VD.ORIG.',2,0) - IIF(uCrsFi.iva==0 and !(UPPER(ALLTRIM(uCrsFi.lobs2))=='REF.VD.ORIG.'),5,0)
					**?IIF(UPPER(ALLTRIM(uCrsFi.lobs2))=='REF.VD.ORIG.','  ','') + LEFT(ALLTRIM(uCrsFi.DESIGN),lcTamanhoDesign) + IIF(uCrsFi.iva==0 and !(UPPER(ALLTRIM(uCrsFi.lobs2))=='REF.VD.ORIG.') and ALLTRIM(uCrsFi.ref)!='V000001',' (*1)','')
					?IIF(UPPER(ALLTRIM(uCrsFi.lobs2))=='REF.VD.ORIG.','  ','') + LEFT(ALLTRIM(uCrsFi.DESIGN),lcTamanhoDesign) 
					IF uf_gerais_getParameter("ADM0000000194","NUM") >= 50

						IF !(UPPER(ALLTRIM(uCrsFi.lobs2))=='REF.VD.ORIG.')
							?TRANSFORM(uCrsFi.u_epvp,lcMascaraDinheiro)+" "
							??TRANSFORM(uCrsFi.u_EPREF,lcMascaraDinheiro)+"   "
							??TRANSFORM(uCrsFi.QTT,"9999.9") +"     "
							??TRANSFORM(uCrsFi.U_ETTENT1+uCrsFi.U_ETTENT2,lcMascaraDinheiro)
							??TRANSFORM(uCrsFi.ETILIQUIDO,lcMascaraDinheiro)+" "
							&& Valida impress�o IVA Logica tem de ser melhorada - Lu�s Leal
							IF lcMotIsencaoIva == .t. AND uf_gerais_validaIvaDireitoDeducao() == 0
								??TRANSFORM(uCrsFi.IVA,"9999")+"%"
							ENDIF
							IF uCrsFi.IVA = 0 AND uf_gerais_validaIvaDireitoDeducao() == 0
								IF TYPE("uCrsFi.motisencao") != 'U' AND ALLTRIM(uCrsFi.ref) <> 'V000001'
									?ALLTRIM(uCrsFi.motisencao)
								ENDIF 
							ENDIF
							
								LOCAL uv_Lote
							STORE '' TO uv_Lote
							
							uv_Lote = uf_gerais_usalote(uCrsFi.fistamp, "Vet")

							IF !EMPTY(uv_lote)
                                    ?uv_lote
							ENDIF
							
						
							SELECT uCrsFi2
							GO TOP
                            IF TYPE("uCrsFi2.bonusDescr") <> "U"

								uv_curLine = uCrsFi.fistamp

								SELECT uCrsFi2
								GO TOP

								LOCATE FOR ALLTRIM(uCrsFi2.fistamp) == ALLTRIM(uv_curLine)

								IF FOUND()
									IF !EMPTY(uCrsFi2.bonusDescr)
										?'*'+ALLTRIM(uCrsFi2.bonusDescr)
									ENDIF
								ENDIF

                                SELECT uCrsFi
                            ENDIF
						ENDIF
					ELSE
						IF !(UPPER(ALLTRIM(uCrsFi.lobs2))=='REF.VD.ORIG.')
							?PADC(IIF(OCCURS(".",astr(round(uCrsFi.u_epvp,2),8,2))>0,astr(round(uCrsFi.u_epvp,2),8,2),astr(round(uCrsFi.u_epvp,2),8,2)+".00"),6," ") + "|"
							??PADC(IIF(OCCURS(".",astr(round(uCrsFi.u_EPREF,2),8,2))>0,astr(round(uCrsFi.u_EPREF,2),8,2),astr(round(uCrsFi.u_EPREF,2),8,2)+".00"),7," ") + "|"
							??PADC(astr(round(uCrsFi.QTT,0),8,2),5," ") + "|"
							??PADC(IIF(OCCURS(".",astr(round(uCrsFi.U_ETTENT1+uCrsFi.U_ETTENT2,2),8,2))>0,astr(round(uCrsFi.U_ETTENT1+uCrsFi.U_ETTENT2,2),8,2),astr(round(uCrsFi.U_ETTENT1+uCrsFi.U_ETTENT2,2),8,2)+".00"),7," ") + "|"
							??PADC(IIF(OCCURS(".",astr(round(uCrsFi.ETILIQUIDO,2),8,2))>0,astr(round(uCrsFi.ETILIQUIDO,2),8,2),astr(round(uCrsFi.ETILIQUIDO,2),8,2)+".00"),7," ") + "|"
							&& Valida impress�o IVA Logica tem de ser melhorada - Lu�s Leal
							IF lcMotIsencaoIva == .t. AND uf_gerais_validaIvaDireitoDeducao() == 0
								??PADC(astr(uCrsFi.IVA)+"%",3," ")
							ENDIF
							IF uCrsFi.IVA = 0 AND uf_gerais_validaIvaDireitoDeducao() == 0
								IF TYPE("uCrsFi.motisencao") != 'U' AND ALLTRIM(uCrsFi.ref) <> 'V000001'
									?ALLTRIM(uCrsFi.motisencao)
								ENDIF 
							ENDIF
							
								LOCAL uv_Lote
							STORE '' TO uv_Lote
							
							uv_Lote = uf_gerais_usalote(uCrsFi.fistamp, "Vet")

							IF !EMPTY(uv_lote)
                                    ?uv_lote
							ENDIF
							
							
                            IF TYPE("uCrsFi2.bonusDescr") <> "U"

								uv_curLine = uCrsFi.fistamp

								SELECT uCrsFi2
								GO TOP

								LOCATE FOR ALLTRIM(uCrsFi2.fistamp) == ALLTRIM(uv_curLine)

								IF FOUND()
									IF !EMPTY(uCrsFi2.bonusDescr)
										?'*'+ALLTRIM(uCrsFi2.bonusDescr)
									ENDIF
								ENDIF

                                SELECT uCrsFi
                            ENDIF
						ENDIF
					ENDIF
					
					IF uCrsFi.iva == 0 AND !(UPPER(ALLTRIM(uCrsFi.lobs2))=='REF.VD.ORIG.')
						lcIsentoIva = .t.
					ENDIF
				ENDSCAN 
			ENDIF 
				
			IF EMPTY(uCrsFt2.u_codigo)
				** Impress�o das Colunas
				uf_gerais_separadorPOS()
				&& Valida impress�o IVA Logica tem de ser melhorada - Lu�s Leal
				IF lcMotIsencaoIva == .t.  AND uf_gerais_validaIvaDireitoDeducao() == 0
					?"Produto"
					IF uf_gerais_getParameter("ADM0000000194","NUM") >= 45 AND uf_gerais_validaIvaDireitoDeducao() == 0
						?"     PVP.      Qtd.          Total        IVA" &&45
					ELSE
						?" PVP.   |   Qtd.   |   Total   |   IVA  " &&40
					ENDIF 
				ELSE
					?"Produto"
					IF uf_gerais_getParameter("ADM0000000194","NUM") >= 45 
						?"     PVP.      Qtd.          Total" &&45
					ELSE
						?" PVP.   |   Qtd.   |   Total" &&40
					ENDIF 
				ENDIF

				**
				SELECT uCrsFi
				SCAN FOR ALLTRIM(uCrsFi.ftstamp) = ALLTRIM(lcStamp) AND uCrsFi.epromo = .f. AND !INLIST(ALLTRIM(uCrsFi.ref),'V000001')
					lcTamanhoDesign = uf_gerais_getParameter("ADM0000000194","NUM") - IIF(UPPER(ALLTRIM(uCrsFi.lobs2))=='REF.VD.ORIG.',2,0) - IIF(uCrsFi.iva==0 and !(UPPER(ALLTRIM(uCrsFi.lobs2))=='REF.VD.ORIG.'),5,0)
					**?IIF(UPPER(ALLTRIM(uCrsFi.lobs2))=='REF.VD.ORIG.','  ','') + LEFT(ALLTRIM(uCrsFi.DESIGN),lcTamanhoDesign) + IIF(uCrsFi.iva==0 and !(UPPER(ALLTRIM(uCrsFi.lobs2))=='REF.VD.ORIG.') and ALLTRIM(uCrsFi.ref)!='V000001',' (*1)','')
					?IIF(UPPER(ALLTRIM(uCrsFi.lobs2))=='REF.VD.ORIG.','  ','') + LEFT(ALLTRIM(uCrsFi.DESIGN),lcTamanhoDesign)
					IF uf_gerais_getParameter("ADM0000000194","NUM") >= 45
						IF !(UPPER(ALLTRIM(uCrsFi.lobs2))=='REF.VD.ORIG.')
							?TRANSFORM(uCrsFi.EPV,lcMascaraDinheiro)+"   "
							??TRANSFORM(uCrsFi.QTT,"99999.9") +"     "
							??TRANSFORM(uCrsFi.ETILIQUIDO,lcMascaraDinheiro)+"    "
							&& Valida impress�o IVA Logica tem de ser melhorada - Lu�s Leal
							IF lcMotIsencaoIva == .t. AND uf_gerais_validaIvaDireitoDeducao() == 0
								??TRANSFORM(uCrsFi.IVA,"9999")+"%"
							ENDIF
							IF uCrsFi.IVA = 0 AND uf_gerais_validaIvaDireitoDeducao() == 0
								IF TYPE("uCrsFi.motisencao") != 'U' AND ALLTRIM(uCrsFi.ref) <> 'V000001'
									?ALLTRIM(uCrsFi.motisencao)
								ENDIF 
							ENDIF
								LOCAL uv_Lote
							STORE '' TO uv_Lote
							
							uv_Lote = uf_gerais_usalote(uCrsFi.fistamp, "Vet")

							IF !EMPTY(uv_lote)
                                    ?uv_lote
							ENDIF
							
                            IF TYPE("uCrsFi2.bonusDescr") <> "U"

								uv_curLine = uCrsFi.fistamp

								SELECT uCrsFi2
								GO TOP

								LOCATE FOR ALLTRIM(uCrsFi2.fistamp) == ALLTRIM(uv_curLine)

								IF FOUND()
									IF !EMPTY(uCrsFi2.bonusDescr)
										?'*'+ALLTRIM(uCrsFi2.bonusDescr)
									ENDIF
								ENDIF

                                SELECT uCrsFi
                            ENDIF
						ENDIF
					ELSE
						IF !(UPPER(ALLTRIM(uCrsFi.lobs2))=='REF.VD.ORIG.')
							?PADC(IIF(OCCURS(".",astr(round(uCrsFi.EPV,2),8,2))>0,astr(round(uCrsFi.EPV,2),8,2),astr(round(uCrsFi.EPV,2),8,2)+".00"),8," ") + "|"
							??PADC(astr(round(uCrsFi.QTT,2),8,2),10," ") + "|"
							??PADC(IIF(OCCURS(".",astr(round(uCrsFi.ETILIQUIDO,2),8,2))>0,astr(round(uCrsFi.ETILIQUIDO,2),8,2),astr(round(uCrsFi.ETILIQUIDO,2),8,2)+".00"),11," ") + "|"
							&& Valida impress�o IVA Logica tem de ser melhorada - Lu�s Leal
							IF lcMotIsencaoIva == .t. AND uf_gerais_validaIvaDireitoDeducao() == 0
								??PADC(astr(uCrsFi.IVA)+"%",8," ")
							ENDIF
							IF uCrsFi.IVA = 0 AND uf_gerais_validaIvaDireitoDeducao() == 0
								IF TYPE("uCrsFi.motisencao") != 'U' AND ALLTRIM(uCrsFi.ref) <> 'V000001'
									?ALLTRIM(uCrsFi.motisencao)
								ENDIF 
							ENDIF
								LOCAL uv_Lote
							STORE '' TO uv_Lote
							
							uv_Lote = uf_gerais_usalote(uCrsFi.fistamp, "Vet")

							IF !EMPTY(uv_lote)
                                    ?uv_lote
							ENDIF
							
                            IF TYPE("uCrsFi2.bonusDescr") <> "U"

								uv_curLine = uCrsFi.fistamp

								SELECT uCrsFi2
								GO TOP

								LOCATE FOR ALLTRIM(uCrsFi2.fistamp) == ALLTRIM(uv_curLine)

								IF FOUND()
									IF !EMPTY(uCrsFi2.bonusDescr)
										?'*'+ALLTRIM(uCrsFi2.bonusDescr)
									ENDIF
								ENDIF

                                SELECT uCrsFi
                            ENDIF
						ENDIF
					ENDIF
					
					IF uCrsFi.iva==0 AND !(UPPER(ALLTRIM(uCrsFi.lobs2))=='REF.VD.ORIG.')
						lcIsentoIva = .t.
					ENDIF
				ENDSCAN 
			ENDIF 

			uf_gerais_separadorPOS()
			
			&& existem impressoras que n�o suportam esta funcionalidade nesses cen�rios � necess�rio comentar isto.
			uf_gerais_textoVermPOS()
			
			**  Valor Descontos e Totais do Documento
			&& simbolo de moeda a imprimir
			IF UPPER(ALLTRIM(lcMoeda)) == 'EURO'
				SELECT uCrsFt
				LOCATE FOR ALLTRIM(uCrsFt.ftstamp)=ALLTRIM(lcStamp)
				IF uCrsFt.EDESCC > 0
					?"TOTAL SEM DESCONTOS:"
					??TRANSFORM(docTotalSemDesc,"999999999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(213)
		
					?"Descontos:          "
					??TRANSFORM(descUtente,"999999999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(213)
				ENDIF
			ELSE
				SELECT uCrsFt
				LOCATE FOR ALLTRIM(uCrsFt.ftstamp)=ALLTRIM(lcStamp)
				IF uCrsFt.EDESCC > 0
					IF uf_gerais_getParameter("ADM0000000260","text")='USD'
						?"TOTAL SEM DESCONTOS:"
						??TRANSFORM(docTotalSemDesc,"999999999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(36)
		
						?"Descontos:          "
						??TRANSFORM(descUtente,"999999999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(36)
					ELSE
						?"TOTAL SEM DESCONTOS:"
						??TRANSFORM(docTotalSemDesc,"999999999999.99 ") + " " + ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))
		
						?"Descontos:          "
						??TRANSFORM(descUtente,"999999999999.99 ") + " " + ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))
					ENDIF 
				ENDIF
			ENDIF

			&& Valor do Documento - Deve ser diferente a informa��o a apresentar mediante o tipo de documento 
			&& simbolo de moeda a imprimir
			IF UPPER(ALLTRIM(lcMoeda)) == 'EURO'
				IF lcPrintType != myRegCli AND lcPrintType != 85	
					?"TOTAL DOCUMENTO:    "
					 ??TRANSFORM(uCrsFt.ETOTAL,"999999999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(213)
				ELSE 
					?"VALOR A PAGAR:      "
					??TRANSFORM(uCrsFt.ETOTAL,"999999999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(213)
				ENDIF
			ELSE
				IF uf_gerais_getParameter("ADM0000000260","text")='USD'
					IF lcPrintType != myRegCli AND lcPrintType != 85	
						?"TOTAL DOCUMENTO:    "
						 ??TRANSFORM(uCrsFt.ETOTAL,"999999999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(36)
					ELSE 
						?"VALOR A PAGAR:"
						??TRANSFORM(uCrsFt.ETOTAL,"999999999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(36)
					ENDIF
				ELSE
					IF lcPrintType != myRegCli AND lcPrintType != 85	
						?"TOTAL DOCUMENTO:    "
						 ??TRANSFORM(uCrsFt.ETOTAL,"999999999999.99 ") + " " + ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))
					ELSE 
						?"VALOR A PAGAR:"
						??TRANSFORM(uCrsFt.ETOTAL,"999999999999.99 ") + " " + ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))
					ENDIF				
				ENDIF 
			ENDIF
			
&& Impress�o Valor Descontos e modo de pagamento
		IF UPPER(ALLTRIM(lcMoeda)) == 'EURO'
			
			&& Valores Pagamento
			IF lcModopag1 > 0
				? SUBSTR(lcMpag1,1,20) +":		  	     "
				??TRANSFORM(lcModopag1,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(213)		
			ENDIF
			IF lcModopag2 > 0
				? SUBSTR(lcMpag2,1,20) +":			     "
				??TRANSFORM(lcModopag2,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(213)		
			ENDIF
			IF lcModopag3 > 0		
				? SUBSTR(lcMpag3,1,20) +":			  	     "
				??TRANSFORM(lcModopag3,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(213)			
			ENDIF
			IF lcModopag4 > 0
				? SUBSTR(lcMpag4,1,20) +":		  	     "
				??TRANSFORM(lcModopag4,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(213)	
			ENDIF
			IF lcModopag5 > 0	
				? SUBSTR(lcMpag5,1,20) +":			  	     "
				??TRANSFORM(lcModopag5,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(213)			
			ENDIF
			IF lcModopag6 > 0
				? SUBSTR(lcMpag6,1,20) +":			     "
				??TRANSFORM(lcModopag6,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(213)		
			ENDIF
			IF lcModopag7 > 0
				? SUBSTR(lcMpag7,1,20) +":			     "
				??TRANSFORM(lcModopag7,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(213)			
			ENDIF
			IF lcModopag8 > 0
				? SUBSTR(lcMpag8,1,20) +":			     "
				??TRANSFORM(lcModopag8,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(213)			
			ENDIF
			IF lcTroco > 0
				?"TROCO:			  "
				??TRANSFORM(lcTroco,"999999999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(213)		
			ENDIF
		ELSE
			IF uf_gerais_getParameter("ADM0000000260","text")='USD'
				
				&& Valores Pagamento
				IF lcModopag1 > 0
					? SUBSTR(lcMpag1,1,20) +":	"
					??TRANSFORM(lcModopag1,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(36)		
				ENDIF
				IF lcModopag2 > 0
					? SUBSTR(lcMpag2,1,20) +":	"
					??TRANSFORM(lcModopag2,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(36)		
				ENDIF
				IF lcModopag3 > 0		
					? SUBSTR(lcMpag3,1,20) +":	"
					??TRANSFORM(lcModopag3,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(36)			
				ENDIF
				IF lcModopag4 > 0
					? SUBSTR(lcMpag4,1,20) +":	"
					??TRANSFORM(lcModopag4,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(36)	
				ENDIF
				IF lcModopag5 > 0
					? SUBSTR(lcMpag5,1,20) +":	"
					??TRANSFORM(lcModopag5,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(36)			
				ENDIF
				IF lcModopag6 > 0
					? SUBSTR(lcMpag6,1,20) +":	"
					??TRANSFORM(lcModopag6,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(36)		
				ENDIF
				IF lcModopag7 > 0
					? SUBSTR(lcMpag7,1,20) +":	"
					??TRANSFORM(lcModopag7,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(36)			
				ENDIF
				IF lcModopag8 > 0
					? SUBSTR(lcMpag8,1,20) +":	"
					??TRANSFORM(lcModopag8,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(36)			
				ENDIF
				IF lcTroco > 0
					?"TROCO:	"
					??TRANSFORM(lcTroco,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(36)		
				ENDIF
			ELSE
				&& Valores Pagamento
				IF lcModopag1 > 0
					? SUBSTR(lcMpag1,1,20) +":	"
					??TRANSFORM(lcModopag1,"99999999.99 ") + " " + ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))		
				ENDIF
				IF lcModopag2 > 0
					? SUBSTR(lcMpag2,1,20) +":	"
					??TRANSFORM(lcModopag2,"99999999.99 ") + " " + ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))		
				ENDIF
				IF lcModopag3 > 0		
					? SUBSTR(lcMpag3,1,20) +":	"
					??TRANSFORM(lcModopag3,"99999999.99 ") + " " + ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))			
				ENDIF
				IF lcModopag4 > 0
					? SUBSTR(lcMpag4,1,20) +":	"
					??TRANSFORM(lcModopag4,"99999999.99 ") + " " + ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))	
				ENDIF
				IF lcModopag5 > 0
					? SUBSTR(lcMpag5,1,20) +":	"
					??TRANSFORM(lcModopag5,"99999999.99 ") + " " + ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))			
				ENDIF
				IF lcModopag6 > 0
					? SUBSTR(lcMpag6,1,20) +":	"
					??TRANSFORM(lcModopag6,"99999999.99 ") + " " + ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))		
				ENDIF
				IF lcModopag7 > 0
					? SUBSTR(lcMpag7,1,20) +":	"
					??TRANSFORM(lcModopag7,"99999999.99 ") + " " + ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))		
				ENDIF
				IF lcModopag8 > 0
					? SUBSTR(lcMpag8,1,20) +":	"
					??TRANSFORM(lcModopag8,"99999999.99 ") + " " + ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))			
				ENDIF
				IF lcTroco > 0
					?"TROCO:	"
					??TRANSFORM(lcTroco,"99999999.99 ") + " " + ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))		
				ENDIF
			ENDIF 
		ENDIF		
				
			&& existem impressoras que n�o suportam esta funcionalidade nesses cen�rios � necess�rio comentar isto.	
			uf_gerais_resetCorPOS() 

			&& Resumo IVA - alterado para ficar com apresenta��o diferente 2015-07-29
			&& Valida impress�o IVA Logica tem de ser melhorada - Lu�s Leal
			IF lcMotIsencaoIva == .t. AND uf_gerais_validaIvaDireitoDeducao() == 0
				SELECT uCrsFT
				**GO TOP 
				?""
				?"Resumo do IVA:"
				? "Taxa	     " + "Base     " + "IVA     " + "Total"
				&& Taxa 0%
				IF uCrsFt.EIVAV4 != 0
					?TRANSFORM(uCrsFt.IVATX4,"99")+"%      "  && Taxa
					??TRANSFORM(uCrsFt.EIVAIN4,lcMascaraDinheiro )+" "  && Base
					??TRANSFORM(uCrsFt.EIVAV4,lcMascaraDinheiro) +" "   && Iva
					??TRANSFORM(uCrsFt.EIVAIN4 + uCrsFt.EIVAV4,lcMascaraDinheiro + " ")+" " && Total
				ENDIF
						
				&& Taxa 6%
				IF uCrsFt.EIVAV1 != 0
					?TRANSFORM(uCrsFt.IVATX1,"99")+"%      "
					??TRANSFORM(uCrsFt.EIVAIN1,lcMascaraDinheiro )+" "
					??TRANSFORM(uCrsFt.EIVAV1,lcMascaraDinheiro )+" "
					??TRANSFORM(uCrsFt.EIVAIN1 + uCrsFt.EIVAV1,lcMascaraDinheiro + " " )+" "
				ENDIF
				
				&& Taxa 13%
				IF 	uCrsFt.EIVAV3 != 0	
					?TRANSFORM(uCrsFt.IVATX3,"99")+"%      "
					??TRANSFORM(uCrsFt.EIVAIN3,lcMascaraDinheiro )+" "
					??TRANSFORM(uCrsFt.EIVAV3,lcMascaraDinheiro )+" "
					??TRANSFORM(uCrsFt.EIVAIN3 + uCrsFt.EIVAV3,lcMascaraDinheiro + " ")+" "
				ENDIF
				
				&& Taxa 23%
				IF uCrsFt.EIVAV2 != 0
					?TRANSFORM(uCrsFt.IVATX2,"99")+"%      "
					??TRANSFORM(uCrsFt.EIVAIN2,lcMascaraDinheiro )+" "
					??TRANSFORM(uCrsFt.EIVAV2,lcMascaraDinheiro )+" "
					??TRANSFORM(uCrsFt.EIVAIN2 + uCrsFt.EIVAV2,lcMascaraDinheiro + " ")+" "
				ENDIF
				
				&& Taxa 5
				IF uCrsFt.EIVAV5 != 0
					?TRANSFORM(uCrsFt.IVATX5,"99")+"%      "
					??TRANSFORM(uCrsFt.EIVAIN5,lcMascaraDinheiro )+" "
					??TRANSFORM(uCrsFt.EIVAV5,lcMascaraDinheiro )+" "
					??TRANSFORM(uCrsFt.EIVAIN5 + uCrsFt.EIVAV5,lcMascaraDinheiro + " ")+" "
				ENDIF
				
				&& Taxa 6
				IF uCrsFt.EIVAV6 != 0
					?TRANSFORM(uCrsFt.IVATX6,"99")+"%      "
					??TRANSFORM(uCrsFt.EIVAIN6,lcMascaraDinheiro )+" "
					??TRANSFORM(uCrsFt.EIVAV6,lcMascaraDinheiro )+" "
					??TRANSFORM(uCrsFt.EIVAIN6 + uCrsFt.EIVAV6,lcMascaraDinheiro + " ")+" "
				ENDIF
				
				&& Taxa 7
				IF uCrsFt.EIVAV7 != 0
					?TRANSFORM(uCrsFt.IVATX7,"99")+"%      "
					??TRANSFORM(uCrsFt.EIVAIN7,lcMascaraDinheiro )+" "
					??TRANSFORM(uCrsFt.EIVAV7,lcMascaraDinheiro )+" "
					??TRANSFORM(uCrsFt.EIVAIN7 + uCrsFt.EIVAV7,lcMascaraDinheiro + " ")+" "
				ENDIF
				
				&& Taxa 8
				IF uCrsFt.EIVAV8 != 0
					?TRANSFORM(uCrsFt.IVATX8,"99")+"%      "
					??TRANSFORM(uCrsFt.EIVAIN8,lcMascaraDinheiro )+" "
					??TRANSFORM(uCrsFt.EIVAV8,lcMascaraDinheiro )+" "
					??TRANSFORM(uCrsFt.EIVAIN8 + uCrsFt.EIVAV8,lcMascaraDinheiro + " ")+" "
				ENDIF
				
				&& Taxa 9
				IF uCrsFt.EIVAV9 != 0
					?TRANSFORM(uCrsFt.IVATX9,"99")+"%      "
					??TRANSFORM(uCrsFt.EIVAIN9,lcMascaraDinheiro )+" "
					??TRANSFORM(uCrsFt.EIVAV9,lcMascaraDinheiro )+" "
					??TRANSFORM(uCrsFt.EIVAIN9 + uCrsFt.EIVAV9,lcMascaraDinheiro + " ")+" "
				ENDIF
				
				&& Taxa 10
				IF uCrsFt.EIVAV10 != 0
					?TRANSFORM(uCrsFt.IVATX10,"99")+"%      "
					??TRANSFORM(uCrsFt.EIVAIN10,lcMascaraDinheiro )+" "
					??TRANSFORM(uCrsFt.EIVAV10,lcMascaraDinheiro )+" "
					??TRANSFORM(uCrsFt.EIVAIN10 + uCrsFt.EIVAV10,lcMascaraDinheiro + " ")+" "
				ENDIF
				
				&& Taxa 11
				IF uCrsFt.EIVAV11 != 0
					?TRANSFORM(uCrsFt.IVATX11,"99")+"%      "
					??TRANSFORM(uCrsFt.EIVAIN11,lcMascaraDinheiro )+" "
					??TRANSFORM(uCrsFt.EIVAV11,lcMascaraDinheiro )+" "
					??TRANSFORM(uCrsFt.EIVAIN11 + uCrsFt.EIVAV11,lcMascaraDinheiro + " ")+" "
				ENDIF
				
				&& Taxa 12
				IF uCrsFt.EIVAV12 != 0
					?TRANSFORM(uCrsFt.IVATX12,"99")+"%      "
					??TRANSFORM(uCrsFt.EIVAIN12,lcMascaraDinheiro )+" "
					??TRANSFORM(uCrsFt.EIVAV12,lcMascaraDinheiro )+" "
					??TRANSFORM(uCrsFt.EIVAIN12 + uCrsFt.EIVAV12,lcMascaraDinheiro + " ")+" "
				ENDIF
				
				&& Taxa 13
				IF uCrsFt.EIVAV13 != 0
					?TRANSFORM(uCrsFt.IVATX13,"99")+"%      "
					??TRANSFORM(uCrsFt.EIVAIN13,lcMascaraDinheiro )+" "
					??TRANSFORM(uCrsFt.EIVAV13,lcMascaraDinheiro )+" "
					??TRANSFORM(uCrsFt.EIVAIN13 + uCrsFt.EIVAV13,lcMascaraDinheiro + " ")+" "
				ENDIF
			ENDIF
			
			&& existem impressoras que n�o suportam esta funcionalidade nesses cen�rios � necess�rio comentar isto.
			uf_gerais_textoVermPOS() 

			uf_gerais_resetCorPOS() 
		
		
		
		
	
			
			IF USED("uCrsProdComp") OR  uCrsProdComp.total_compart_estado1 <> 0 OR uCrsProdComp.total_compart_ext1 <> 0 OR uCrsProdComp.total_compart_cli1 <>0
				?""
				?""
				?"Info. Produtos Comparticipados"
				IF uCrsProdComp.total_compart_estado1 <> 0
					??? CHR(29)+CHR(33)+CHR(16.5) && aumentar letra
					?"Custo Suportado p/Estado:" +TRANSFORM(uCrsProdComp.total_compart_estado1,"9999.99" )
					??? CHR(29)+CHR(33)+CHR(0) && diminuir letra
				ENDIF
				IF uCrsProdComp.total_compart_cli1 <>0 OR uCrsProdComp.total_compart_estado1 <> 0
					??? CHR(29)+CHR(33)+CHR(16.5) && aumentar letra
					?"Custo Suportado p/Utente:" +TRANSFORM(uCrsProdComp.total_compart_cli1,"9999.99")
					??? CHR(29)+CHR(33)+CHR(0) && diminuir letra	
				ENDIF	
				IF uCrsProdComp.total_compart_ext1 <> 0
					?"Custo Suportado p/Entidade:		"
					??TRANSFORM(uCrsProdComp.total_compart_ext1,lcMascaraDinheiro )
				ENDIF
				
				
			ENDIF
			
		
		
		
		
			SELECT uCrsFt
			GO TOP
			IF !EMPTY(uCrsFt.u_nratend)
				?
				??	chr(27)+chr(97)+chr(1)
				?'Atendimento:'
				?
				???	 chr(29)+chr(104)+chr(50) + chr(29)+chr(72)+chr(2) + chr(29)+chr(119)+chr(2) + chr(29)+chr(107)+chr(4) + UPPER(ALLTRIM(uCrsFt.u_nratend)) + chr(0)
				uf_gerais_textoLJPOS()
			ENDIF	

			&& impress�o c�digo de barras de reserva
			IF !EMPTY(lcAdiReserva)
				IF USED("uCrsNrReserva")
					SELECT uCrsNrReserva
					GO TOP 
					IF !EMPTY(uCrsNrReserva.nrReserva)
						?
						??	chr(27)+chr(97)+chr(1)
						?'ID Reserva:'
						?
						???	 chr(29)+chr(104)+chr(50) + chr(29)+chr(72)+chr(2) + chr(29)+chr(119)+chr(2) + chr(29)+chr(107)+chr(4) + UPPER(ALLTRIM(uCrsNrReserva.nrReserva)) + chr(0)
						uf_gerais_textoLJPOS()					
					ENDIF
				ENDIF
			ENDIF

			uf_gerais_textoCJPOS()

			** Impress�o pontos cart�o cliente 
			IF UPPER(ALLTRIM(lcPainel)) == "ATENDIMENTO" 
				** verificar se usou vale de desconto **
				&& simbolo de moeda a imprimir
                IF myTipoCartao == 'Pontos'
                    IF UPPER(ALLTRIM(lcMoeda)) == 'EURO'
                        IF USED("uCrsFiPromo")
                            IF RECCOUNT("uCrsFiPromo")>0
                                ??? CHR(29)+CHR(33)+CHR(16) && aumentar letra
                                ? IIF(myTipoCartao == 'Pontos',"Vales Utilizados", "Valor Utilizado")
                                ??? CHR(29)+CHR(33)+CHR(0) && diminuir letra
                                SELECT uCrsFiPromo
                                GO TOP
                                SCAN
                                    ? IIF(myTipoCartao == 'Pontos','Vale Desconto aplicado de ', 'Valor Desconto aplicado: ') + astr(ABS(uCrsFiPromo.u_epvp),9,2) + chr(27)+chr(116)+chr(19)+CHR(213)
                                ENDSCAN
                            ENDIF
                        ENDIF
                    ELSE
                        IF USED("uCrsFiPromo")
                            IF RECCOUNT("uCrsFiPromo")>0
                                ??? CHR(29)+CHR(33)+CHR(16) && aumentar letra
                                ? IIF(myTipoCartao == 'Pontos',"Vales Utilizados", "Valor Utilizado")
                                ??? CHR(29)+CHR(33)+CHR(0) && diminuir letra
                                SELECT uCrsFiPromo
                                GO TOP
                                SCAN
                                    IF uf_gerais_getParameter("ADM0000000260","text")='USD'
                                        ? IIF(myTipoCartao == 'Pontos','Vale Desconto aplicado de ', 'Valor Desconto aplicado: ') + astr(ABS(uCrsFiPromo.u_epvp),9,2) + chr(27)+chr(116)+chr(19)+CHR(36)
                                    ELSE
                                        ? IIF(myTipoCartao == 'Pontos','Vale Desconto aplicado de ', 'Valor Desconto aplicado: ') + astr(ABS(uCrsFiPromo.u_epvp),9,2) + " " + ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))
                                    ENDIF 
                                ENDSCAN
                            ENDIF
                        ENDIF
                    ENDIF
                ENDIF
					**
				IF myTipoCartao == 'Pontos'		
					IF myCartaoClienteLt				
						** levantar dados **
						SELECT uCrsAtendCl
						IF uf_gerais_actGrelha("",[uCrsUltTranCartao],[select nrcartao, pontosAtrib, pontosUsa, inactivo from B_fidel (nolock) where nrcartao=']+ALLTRIM(uCrsAtendCl.nrcartao)+['])
							IF reccount("uCrsUltTranCartao")>0
								IF !EMPTY(uCrsUltTranCartao.nrcartao)
                                    
									? ""
									??? CHR(29)+CHR(33)+CHR(16) && aumentar letra
									? "Cartao Cliente"
									??? CHR(29)+CHR(33)+CHR(0) && diminuir letra
									? "Total Disponivel: " + astr(uCrsUltTranCartao.pontosAtrib-uCrsUltTranCartao.pontosUsa) + " pontos"						
									IF TYPE("myTotalPagCc")=="N"
										? "Ganho neste Atendimento: " + astr(myTotalPagCc) + " pontos"
										? "Total Ganho com Cartao: " + astr(uCrsUltTranCartao.pontosAtrib) + " pontos"
									ELSE
										? "Total Ganho com Cartao: " + astr(uCrsUltTranCartao.pontosAtrib) + " pontos"
									ENDIF
								ENDIF
							ENDIF

							fecha("uCrsUltTranCartao")
						ENDIF
						
						** se usou sistema de promo��es **
						? ""
						**********************************
					ENDIF
					*************************************
				ENDIF 
			ENDIF 
			


			SELECT uCrsFt
			IF myTipoCartao == 'Valor' AND uCrsFt.no <> 200 AND NOT EMPTY(uv_nrCartao)
			
				SELECT uCrsFt
				uf_imprimirPOS_devolveValoresCartaoAtendimento(uCrsFt.u_nratend)
					
				SELECT uCrsFt2
				? ""
				??? CHR(29)+CHR(33)+CHR(16) && aumentar letra
				? "Cartao Cliente"
				??? CHR(29)+CHR(33)+CHR(0) && diminuir letra

				
				if(USED("uCrsFtResultTemp"))
					SELECT uCrsFtResultTemp	
			
					IF uCrsFtResultTemp.valcartao <> 0 OR uCrsFtResultTemp.valcartaoutilizado<>0
						IF UPPER(ALLTRIM(lcMoeda)) == 'EURO'
							IF uCrsFtResultTemp.valcartao > 0
								? 'Ganho neste Atendimento: ' + astr(ABS(uCrsFtResultTemp.valcartao),9,2) + chr(27)+chr(116)+chr(19)+CHR(213)
							ENDIF 
							IF uCrsFtResultTemp.valcartaoutilizado <> 0
								? 'Utilizado neste Atendimento: ' + astr(ABS(uCrsFtResultTemp.valcartaoutilizado),9,2) + ' ' + chr(27)+chr(116)+chr(19)+CHR(213)
							ENDIF 
						ELSE
							IF uf_gerais_getParameter("ADM0000000260","text")='USD'
								IF uCrsFtResultTemp.valcartao > 0
									? 'Ganho neste Atendimento: ' + astr(uCrsFtResultTemp.valcartao) + ' ' + chr(27)+chr(116)+chr(19)+CHR(36)
								ENDIF 
								IF uCrsFtResultTemp.valcartaoutilizado <> 0
									? 'Utilizado neste Atendimento: ' + astr(uCrsFtResultTemp.valcartaoutilizado) + ' ' + chr(27)+chr(116)+chr(19)+CHR(36)
								ENDIF 
							ELSE
								IF uCrsFtResultTemp.valcartao > 0
									? 'Ganho neste Atendimento: ' + astr(uCrsFtResultTemp.valcartao) + ' ' + ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))
								ENDIF 
								IF uCrsFtResultTemp.valcartaoutilizado <> 0
									? 'Utilizado neste Atendimento: ' + astr(uCrsFtResultTemp.valcartaoutilizado) + ' ' + ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))
								ENDIF 
							ENDIF 
						ENDIF 
						? ""
					ENDIF 
				ENDIF 
				
				
				fecha("uCrsFtResultTemp")
				SELECT uCrsFt2
				IF UPPER(ALLTRIM(lcMoeda)) == 'EURO'
					? 'Disponivel: ' + astr(ABS(lcValDispCartao),9,2) + ' ' + chr(27)+chr(116)+chr(19)+CHR(213)
					? 'Total Ganho em Cartao: ' + astr(ABS(lcTotalHistCartao),9,2) + ' ' + chr(27)+chr(116)+chr(19)+CHR(213)
				ELSE
					IF uf_gerais_getParameter("ADM0000000260","text")='USD'
						? 'Dispon�vel: ' + astr(lcValDispCartao) + ' ' + chr(27)+chr(116)+chr(19)+CHR(36)
						? 'Total Ganho em Cart�o: ' + astr(lcTotalHistCartao) + ' ' + chr(27)+chr(116)+chr(19)+CHR(36)
					ELSE
						? 'Dispon�vel: ' + astr(lcValDispCartao) + ' ' + ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))
						? 'Total Ganho em Cart�o: ' + astr(lcTotalHistCartao) + ' ' + ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))
					ENDIF 
					? ""
				ENDIF 
				
			ENDIF 

			&& IMPRESS�O DO RODAP�  
			??? chr(27)+chr(97)+chr(1)

			IF UPPER(ALLTRIM(lcPainel)) == "ATENDIMENTO"
				If lcPrintType==myRegCli OR lcPrintType==85
					?
					?"    ----------------------------------"
					?"       O Cliente"
				ENDIF
			ENDIF
					
*!*				&& Verifica Isen��o Iva							
*!*				IF lcMotIsencaoIva == .t. AND uf_gerais_validaIvaDireitoDeducao() == 0
*!*					IF lcIsentoIva
*!*						DO CASE
*!*							CASE ucrsFt.pais==3
*!*								?"Nota (*1): Isento Artigo 14. do CIVA (ou similar)."
*!*							CASE ucrsFt.pais==2
*!*								?"Nota (*1): Isento Artigo 14. do RITI (ou similar)."
*!*							OTHERWISE 
*!*								? ALLTRIM(ucrse1.motivo_isencao_iva)
*!*								**?"Nota (*1): Isento Artigo 9. do CIVA (ou similar)."
*!*						ENDCASE 
*!*					ENDIF
*!*				ENDIF
			
			? 'Os bens/servicos adquiridos foram colocados a'
			? 'disposicao do adquirente na data do documento'
			&& Informa��o certifica��o	
			IF uf_gerais_actGrelha("","uCrsIduCert","exec up_gerais_iduCert '"+ALLTRIM(lcStamp)+"'")
				IF RECCOUNT("uCrsIduCert")>0	
					IF uCrsIduCert.temCert
						*SET CONSOLE OFF
						&& Valida impress�o IVA Logica tem de ser melhorada - Lu�s Leal
						IF lcMotIsencaoIva == .t. AND uf_gerais_validaIvaDireitoDeducao() == 0 AND uf_gerais_getParameter_site("ADM0000000166", "BOOL", mySite)
							?'IVA INCLUIDO'
						ENDIF
						
						? ALLTRIM(uCrsIduCert.parte1)
						?? '-'+ALLTRIM(uCrsIduCert.parte2)  + ' ' + ALLTRIM(uCrsFt2.u_docorig)	
					ELSE
						IF uf_gerais_actGrelha("","ucrsRodapeCert",[exec up_cert_TextoRodape '] + ALLTRIM(uCrsFt.tiposaft) + ['])
							**Alterado novas regras certifica��o 20120313		
							?ALLTRIM(ucrsRodapeCert.rodape)
						ELSE
							&& Valida impress�o IVA Logica tem de ser melhorada - Lu�s Leal
							IF lcMotIsencaoIva == .t. AND uf_gerais_validaIvaDireitoDeducao() == 0 AND uf_gerais_getParameter_site("ADM0000000166", "BOOL", mySite)
								?"Processado por Computador - IVA INCLUIDO"
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF uf_gerais_actGrelha("","ucrsRodapeCert",[exec up_cert_TextoRodape '] + ALLTRIM(uCrsFt.tiposaft) + ['])
						**Alterado novas regras certifica��o 20120313		
						?ALLTRIM(ucrsRodapeCert.rodape)
					ELSE
						&& Valida impress�o IVA Logica tem de ser melhorada - Lu�s Leal
						IF lcMotIsencaoIva == .t. AND uf_gerais_validaIvaDireitoDeducao() == 0 AND uf_gerais_getParameter_site("ADM0000000166", "BOOL", mySite)
							?"Processado por Computador - IVA INCLUIDO"
						ENDIF
					ENDIF
				ENDIF
				fecha("uCrsIduCert")
				
				SELECT uCrsStampsQRCode
				APPEND BLANK 
				replace uCrsStampsQRCode.ftstamp WITH ALLTRIM(uCrsFt.ftstamp)
				
			ELSE
				IF uf_gerais_actGrelha("","ucrsRodapeCert",[exec up_cert_TextoRodape '] + ALLTRIM(uCrsFt.tiposaft) + ['])
					**Alterado novas regras certifica��o 20120313		
					?ALLTRIM(ucrsRodapeCert.rodape)
				ELSE
					&& Valida impress�o IVA Logica tem de ser melhorada - Lu�s Leal
					IF lcMotIsencaoIva == .t. AND uf_gerais_validaIvaDireitoDeducao() == 0 AND uf_gerais_getParameter_site("ADM0000000166", "BOOL", mySite)
						?"Processado por Computador - IVA INCLUIDO"
					ENDIF
				ENDIF
			ENDIF
			********************************
			
			** Publicidade **
			? "Logitools, Lda"
			
			** VERIFICA SE DEVE IMPRIMIR O C�DIGO ADICIONAL DO OPERADOR EM VEZ DO C�DIGO PRINCIPAL **
			LOCAL lcOpCode
			STORE 0 TO lcOpCode
			
			IF uf_gerais_getParameter("ADM0000000047","BOOL")
				lcOpCode = myOpAltCod
			ELSE
				lcOpCode = uCrsFt.vendedor
			ENDIF 
			
			SELECT uCrsFt	
			GO TOP 
			?"Op: "
			IF lcOpCode==0
				??Substr(uCrsFt.vendnm,1,30) + " (" + Alltrim(Str(uCrsFt.vendedor)) + ")"
				?"" 
			ELSE
				??Substr(uCrsFt.vendnm,1,30) + " (" + Alltrim(Str(lcOpCode)) + ")" 
				?""
			ENDIF
			
			&& Texto rodap� Tal�es
			uf_gerais_criarRodapePOS()
			
			&& Corta Papel e abre gaveta
			IF reccount("uCrsStampsQRCode")=0 OR UPPER(mypaisconfsoftw)!='PORTUGAL'
				uf_gerais_feedCutPOS()
			ENDIF  
			**uf_gerais_openDrawPOS()
			
			&& verificar se imprime tal�o com n� de atendimento separado
*!*				IF uf_gerais_getParameter("ADM0000000207","BOOL") AND !lcReImp
*!*					** IMPRESSAO DO CODIGO DO ATENDIMENTO **
*!*							
*!*					SELECT uCrsFt
*!*					GO TOP
*!*					IF !EMPTY(uCrsFt.u_nratend)
*!*						?
*!*						?
*!*						??	chr(27)+chr(97)+chr(1)
*!*						?'Atendimento:'
*!*						?
*!*						???	 chr(29)+chr(104)+chr(50) + chr(29)+chr(72)+chr(2) + chr(29)+chr(119)+chr(2) + chr(29)+chr(107)+chr(4) + UPPER(ALLTRIM(uCrsFt.u_nratend)) + chr(0)
*!*						uf_gerais_textoLJPOS()
*!*						
*!*						LOCAL lcTotAtendSep
*!*						SELECT uCrsFt
*!*						CALCULATE SUM(uCrsFt.etotal) TO lcTotAtendSep
*!*						?"Total Documento   :"
*!*						IF EMPTY(uf_gerais_getParameter("ADM0000000260","text")) or UPPER(ALLTRIM(uf_gerais_getParameter("ADM0000000260","text"))) == "EURO"
*!*							??TRANSFORM(lcTotAtendSep,"99999999999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(213)
*!*						ELSE
*!*							IF uf_gerais_getParameter("ADM0000000260","text")='USD'
*!*								??TRANSFORM(lcTotAtendSep,"99999999999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(36)
*!*							ELSE
*!*								??TRANSFORM(lcTotAtendSep,"99999999999999.99 ") + " " + ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))
*!*							ENDIF 
*!*						ENDIF 
*!*						
*!*						uf_gerais_feedCutPOS()
*!*					ENDIF
*!*				ENDIF
			
			**LIBERTAR IMPRESSORA, COLOCAR NO DEFAULT
			uf_gerais_setImpressoraPOS(.f.)
			
			
			** QRCODES
			IF reccount("uCrsStampsQRCode")>0 AND UPPER(mypaisconfsoftw)=='PORTUGAL'
				LOCAL lcTokenQrcode, lcordemQRCode
				STORE 1 TO lcordemQRCode
				lcTokenQrcode = uf_gerais_stamp()
				SELECT uCrsStampsQRCode
				GO TOP 
				SCAN
					uf_gerais_gerar_qrcodejpg(ALLTRIM(uCrsStampsQRCode.ftstamp), ALLTRIM(lcTokenQrcode), lcordemQRCode)
					lcordemQRCode = lcordemQRCode + 1
				ENDSCAN 
				** Indicar o corte na �ltima imagem
				IF uf_gerais_getParameter_site("ADM0000000149", "BOOL", mysite)
					TEXT TO lcsql TEXTMERGE NOSHOW
					 	update 
					 		image_printPos 
					 	set 
					 		feedcut=1 
					 	where 
					 		token='<<ALLTRIM(lcTokenQrcode)>>' 
					 		and [order]=(select max([order]) from image_printPos (nolock) where token='<<ALLTRIM(lcTokenQrcode)>>')
				    ENDTEXT
				    uf_gerais_actgrelha("", "", lcsql)
				ENDIF 
				
				STORE '&' TO lcadd
				lcwspath = ALLTRIM(uf_gerais_getparameter("ADM0000000185", "TEXT"))+'\ltsPrinter\ltsPrinter.jar'+ ' ' + '"--idCl='+UPPER(ALLTRIM(sql_db))+'" '+' "--token='+ALLTRIM(lcTokenQrcode)+'"'
				lcwsdir = ALLTRIM(uf_gerais_getparameter("ADM0000000185", "TEXT"))+'\ltsPrinter'
				lcwspath = "cmd /c cd /d "+lcwsdir+" "+lcadd+lcadd+" javaw -jar "+lcwspath

				owsshell = CREATEOBJECT("WScript.Shell")
				owsshell.run(lcwspath, 0, .T.)
				
				&& Funcoes abrir Gaveta e Cortar Papel
				IF !uf_gerais_getParameter_site("ADM0000000149", "BOOL", mysite)
					uf_gerais_setImpressoraPOS(.t.)
					uf_gerais_feedCutPOS()
					uf_gerais_setImpressoraPOS(.f.)
				ENDIF
			ENDIF 
			
			IF USED("uCrsStampsQRCode")
				fecha("uCrsStampsQRCode")
			ENDIF 
			
			IF uf_gerais_getParameter("ADM0000000207","BOOL") AND !lcReImp
				** IMPRESSAO DO CODIGO DO ATENDIMENTO **
						
				SELECT uCrsFt
				GO TOP
				IF !EMPTY(uCrsFt.u_nratend)
					?
					?
					??	chr(27)+chr(97)+chr(1)
					?'Atendimento:'
					?
					???	 chr(29)+chr(104)+chr(50) + chr(29)+chr(72)+chr(2) + chr(29)+chr(119)+chr(2) + chr(29)+chr(107)+chr(4) + UPPER(ALLTRIM(uCrsFt.u_nratend)) + chr(0)
					uf_gerais_textoLJPOS()
					
					LOCAL lcTotAtendSep
					SELECT uCrsFt
					CALCULATE SUM(uCrsFt.etotal) TO lcTotAtendSep
					?"Total Documento   :"
					IF EMPTY(uf_gerais_getParameter("ADM0000000260","text")) or UPPER(ALLTRIM(uf_gerais_getParameter("ADM0000000260","text"))) == "EURO"
						??TRANSFORM(lcTotAtendSep,"99999999999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(213)
					ELSE
						IF uf_gerais_getParameter("ADM0000000260","text")='USD'
							??TRANSFORM(lcTotAtendSep,"99999999999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(36)
						ELSE
							??TRANSFORM(lcTotAtendSep,"99999999999999.99 ") + " " + ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))
						ENDIF 
					ENDIF 
					
					uf_gerais_feedCutPOS()
				ENDIF
			ENDIF
			
			&&verificar psico			
			IF UPPER(ALLTRIM(lcPainel)) == "ATENDIMENTO"
				IF lcPrintType!=myRegCli AND lcPrintType!=85 && N�o imprimir tal�o psico quando o doc a imprimir � uma Nt. Cr�dito
					uf_imprimirpos_ImpPrevPsico(lcStamp, .f., lcPainel)
				ENDIF
			ENDIF 
		ENDIF
	ELSE && Tal�o A6
		PUBLIC myDocIdu1, myDocIdu2, myDocIdu3, myDocIdu4, myDocIdu5, myDocIdu6, myDocIdu7, myDocIdu8, myDocIdu9, myDocIdu10
		PUBLIC myDocIdu11, myDocIdu12, myDocIdu13, myDocIdu14, myDocIdu15, myDocIdu16, myDocIdu17, myDocIdu18, myDocIdu19, myDocIdu20
		PUBLIC myDocIduc1, myDocIduc2, myDocIduc3, myDocIduc4, myDocIduc5, myDocIduc6, myDocIduc7, myDocIduc8, myDocIduc9, myDocIduc10
		PUBLIC myDocIduc11, myDocIduc12, myDocIduc13, myDocIduc14, myDocIduc15, myDocIduc16, myDocIduc17, myDocIduc18, myDocIduc19, myDocIduc20
		PUBLIC myDocIduh1, myDocIduh2, myDocIduh3, myDocIduh4, myDocIduh5, myDocIduh6, myDocIduh7, myDocIduh8, myDocIduh9, myDocIduh10
		PUBLIC myDocIduh11, myDocIduh12, myDocIduh13, myDocIduh14, myDocIduh15, myDocIduh16, myDocIduh17, myDocIduh18, myDocIduh19, myDocIduh20
		PUBLIC myOpCode, myOpCode1, myPrazoReg, myIsentoIva, myTemCertIdu, mySemValFisc
		PUBLIC myEivain1Total, myEivain2Total, myEivain3Total, myEivain4Total,myEivain5Total,myEivain6Total,myEivain7Total,myEivain8Total,myEivain9Total,myEivain10Total,myEivain11Total,myEivain12Total,myEivain13Total
		PUBLIC myDocTotal, myDescTotal
		PUBLIC myEivav1Total, myEivav2Total, myEivav3Total, myEivav4Total, myEivav5Total, myEivav6Total, myEivav7Total, myEivav8Total, myEivav9Total, myEivav10Total, myEivav11Total, myEivav12Total, myEivav13Total
		PUBLIC myDescCcAd1, myDescCcAd2, myDescCcAd3, myDescCcAd4, lcTipoDocImp
		
		LOCAL lcDocIdu, lcDocIduc, lcDocIduh, i, lcTemComp, myRodape1, myRodape2, myRodape3
		LOCAL lcNomeFicheirolt
		
		STORE '' TO myDocIdu1, myDocIdu2, myDocIdu3, myDocIdu4, myDocIdu5, myDocIdu6, myDocIdu7, myDocIdu8, myDocIdu9, myDocIdu10
		STORE '' TO myDocIdu11, myDocIdu12, myDocIdu13, myDocIdu14, myDocIdu15, myDocIdu16, myDocIdu17, myDocIdu18, myDocIdu19, myDocIdu20
		
		STORE '' TO myDocIduc1, myDocIduc2, myDocIduc3, myDocIduc4, myDocIduc5, myDocIduc6, myDocIduc7, myDocIduc8, myDocIduc9, myDocIduc10
		STORE '' TO myDocIduc11, myDocIduc12, myDocIduc13, myDocIduc14, myDocIduc15, myDocIduc16, myDocIduc17, myDocIduc18, myDocIduc19, myDocIduc20
		
		STORE '' TO myDocIduh1, myDocIduh2, myDocIduh3, myDocIduh4, myDocIduh5, myDocIduh6, myDocIduh7, myDocIduh8, myDocIduh9, myDocIduh10
		STORE '' TO myDocIduh11, myDocIduh12, myDocIduh13, myDocIduh14, myDocIduh15, myDocIduh16, myDocIduh17, myDocIduh18, myDocIduh19, myDocIduh20
		
		STORE '' TO myOpCode1, myPrazoReg, myTemCertIdu, mySemValFisc
		
		STORE 0 TO myOpCode, myDocTotal, myDescTotal

		STORE 0 TO myEivain1Total, myEivain2Total, myEivain3Total, myEivain4Total,myEivain5Total,myEivain6Total,myEivain7Total,myEivain8Total,myEivain9Total,myEivain10Total,myEivain11Total,myEivain12Total,myEivain13Total
		STORE 0 TO myEivav1Total, myEivav2Total, myEivav3Total, myEivav4Total, myEivav5Total, myEivav6Total, myEivav7Total, myEivav8Total, myEivav9Total, myEivav10Total, myEivav11Total, myEivav12Total, myEivav13Total
		
		STORE '' TO myDescCcAd1, myDescCcAd2, myDescCcAd3, myDescCcAd4, myPontosDispCcAd, myPontosTotalCcAd, myPontosGanhosCcAd
		
		STORE '' TO lcDocIdu, lcDocIduc, lcDocIduh, lcNomeFicheirolt, myRodape1, myRodape2, myRodape3, lcTipoDocImp
		
		STORE 1 TO i
	
		SELECT uCrsFt
		LOCATE FOR ALLTRIM(uCrsFt.ftstamp) = ALLTRIM(lcStamp)
		lcDocIdu 	= 'myDocIdu' + astr(i)
		lcDocIduc	= 'myDocIduc' + astr(i)
		lcDocIduh	= 'myDocIduh' + astr(i)
		
		IF (uCrsFt.u_tipodoc=8) OR (uCrsFt.ndoc=32)
			IF YEAR(uCrsFt.fdata) > 2012
				lcTipoDocImp = "Fatura-Recibo Nr."
			ELSE
				lcTipoDocImp = "Vnd. a Dinheiro Nr."
			ENDIF
		ENDIF

		IF (uCrsFt.u_tipodoc=9) OR (uCrsFt.ndoc=11) OR (uCrsFt.ndoc=12)
			lcTipoDocImp  = "Fatura Nr."
		ENDIF

		IF (uCrsFt.u_tipodoc=2) or (uCrsFt.u_tipodoc=7)
			lcTipoDocImp  = "Nt. Credito Nr."
		ENDIF
		
		IF (uCrsFt.u_tipodoc=4)
			lcTipoDocImp  = "Fatura Acordo Nr."
		ENDIF
		
		IF (uCrsFt.u_tipodoc=3)
			lcTipoDocImp  = "Insercao Receita Nr."
		ENDIF

		** IMPRIMIR TAG IDENTIFICADORA DO DOCUMENTO DE VENDA **
		DO CASE
			CASE (uCrsFt.u_tipodoc=8) AND uCrsFt.u_lote=0 AND uCrsFt.cobrado=.f.
				lcTagDocImp  = "(VD)"
			CASE (uCrsFt.u_tipodoc=8) AND uCrsFt.u_lote=0 AND uCrsFt.cobrado=.t.
				lcTagDocImp  = "(VD)(S)"		
			CASE (uCrsFt.u_tipodoc=8) AND uCrsFt.u_lote > 0
				lcTagDocImp  = "(VD-CR)"
			CASE (uCrsFt.ndoc=32) AND uCrsFt.u_lote=0
				lcTagDocImp  = "(VS)"
			CASE (uCrsFt.ndoc=32) AND uCrsFt.u_lote > 0
				lcTagDocImp  = "(VS-CR)"
			CASE (uCrsFt.u_tipodoc=9) AND uCrsFt.cobrado=.f. AND uCrsFt.u_lote=0
				lcTagDocImp  = "(FT)"
			CASE (uCrsFt.u_tipodoc=9) AND uCrsFt.cobrado=.T. AND uCrsFt.u_lote=0
				lcTagDocImp  = "(FT)(S)"
			CASE (uCrsFt.u_tipodoc=9) AND uCrsFt.u_lote > 0
				lcTagDocImp  = "(FT-CR)"
			CASE (uCrsFt.u_tipodoc=2) OR (uCrsFt.u_tipodoc=7)
				lcTagDocImp  = "(RC)"
			CASE (uCrsFt.u_tipodoc=4) And uCrsFt.cobrado=.f. And uCrsFt.u_lote=0
				lcTagDocImp  = "(FA)"
			CASE (uCrsFt.u_tipodoc=4) AND uCrsFt.cobrado=.T. AND uCrsFt.u_lote=0
				lcTagDocImp  = "(FA)(S)"
			CASE (uCrsFt.u_tipodoc=4) AND uCrsFt.u_lote > 0
				lcTagDocImp  = "(FA-CR)"
			CASE (uCrsFt.u_tipodoc=3)
				lcTagDocImp  = "(IR)"
			OTHERWISE
				lcTagDocImp  = "(__)"
		ENDCASE
 
		&lcDocIdu = ALLTRIM(lcTipoDocImp) + transform(uCrsFt.fno,"999999999") + " " + ALLTRIM(lcTagDocImp) + "/" + astr(uCrsFt.ndoc)
		
		IF uf_gerais_actGrelha("",[uCrsIduCert],[exec up_gerais_iduCert ']+ALLTRIM(uCrsFt.ftstamp)+['])
			IF RECCOUNT("uCrsIduCert")>0
				IF uCrsIduCert.temCert
					&lcDocIduc		= 'Referente a: ' + ALLTRIM(lcTipoDocImp) + astr(uCrsFt.fno) + " " + ALLTRIM(lcTagDocImp) + "/" + astr(uCrsFt.ndoc)
					&lcDocIduh		= ALLTRIM(uCrsIduCert.parte1)
					myTemCertIdu 	= ALLTRIM(uCrsIduCert.parte2)
					
					&& se o campo documento original estiver preenchido adiciona o valor ao texto da certifica��o
					SELECT uCrsFt2
					LOCATE FOR ALLTRIM(uCrsFt2.ft2stamp) = ALLTRIM(lcStamp)
					IF !(ALLTRIM(UCrsFt2.u_docorig) == "")
						myTemCertIdu = myTemCertIdu + ' ' + ALLTRIM(uCrsFt2.u_DocOrig)
					ENDIF
				ENDIF
			ENDIF
			fecha("uCrsIduCert")
		ENDIF
		
		i = i+1
		
		SELECT uCrsFt
		LOCATE FOR ALLTRIM(uCrsFt.ftstamp) = ALLTRIM(lcStamp)
		
		select uCrsE1
		********************************************
	
		&& Verificar se imprime capital social
		IF uf_gerais_getParameter("ADM0000000124","BOOL") == .t.
			myImpCapSocial = .t.
		ENDIF
		*****************************************
		
		** Verificar se o documento tem prazo de regulariza��o e se � um documento fiscal **
		SELECT uCrsFt
		LOCATE FOR ALLTRIM(uCrsFt.ftstamp) = ALLTRIM(lcStamp)
		IF uCrsFt.cobrado
			IF uf_gerais_getParameter("ADM0000000014","NUM") > 0
				myPrazoReg = "Prazo de Regularizacao: " + aStr(uf_gerais_getParameter("ADM0000000014","NUM")) + " Dias"
			ENDIF
		ENDIF
		
		SELECT uCrsFt	
		IF uCrsFt.u_tipodoc == 3 OR uCrsFt.u_tipodoc == 4
			*** Talao sem Validade Fiscal ***
			mySemValFisc = "*** Talao sem Validade Fiscal ***"
		ENDIF
		*************************************************************************************

		** guardar dados do operador **
		LOCAL lcOpCode
		STORE 0 TO lcOpCode
		
		IF uf_gerais_getParameter("ADM0000000047","BOOL")
			lcOpCode = myOpAltCod
		ELSE
			lcOpCode = uCrsFt.vendedor
		ENDIF 
		
		IF lcOpCode==0
			myOpCode1 = "Op: " + Substr(uCrsFt.vendnm,1,30) + " (" + Alltrim(Str(uCrsFt.vendedor)) + ")"
		ELSE
			myOpCode1 = "Op: " + Substr(uCrsFt.vendnm,1,30) + " (" + Alltrim(Str(lcOpCode)) + ")"
		ENDIF
		*******************************

		** dados adicionais do cliente **
		SELECT uCrsFt
		LOCATE FOR ALLTRIM(uCrsFt.ftstamp) = ALLTRIM(lcStamp)
		IF uf_gerais_actGrelha("",[uCrsBenef],[exec up_touch_dadosCliente ]+astr(uCrsFt.no)+[, ]+astr(uCrsFt.estab))
			If !Reccount()>0
				uf_perguntalt_chama("OCORREU UM PROBLEMA A LER DADOS ADICIONAIS DO CLIENTE.","OK","",16)
				RETURN
			ENDIF
		ENDIF
		*********************************

		** Verificar se existe documentos com comparticipa��o **
		SELECT uCrsFt2
		LOCATE FOR ALLTRIM(uCrsFt2.ft2stamp) = ALLTRIM(lcStamp)
		SCAN
			IF !Empty(uCrsFt2.u_codigo)
				lcTemComp = .t.
			EndIf
		ENDSCAN
		*********************************************************

		** Verificar se Existem produtos isentos de Iva **
		SELECT uCrsFi
		SCAN FOR ALLTRIM(uCrsFi.ftstamp) = ALLTRIM(lcStamp)
			IF uCrsFi.iva==0 AND !(UPPER(ALLTRIM(uCrsFi.lobs2))=='REF.VD.ORIG.')
				MyIsentoIva = .t.
			ENDIF
		ENDSCAN
		**************************************************

		*** CALCULAR DADOS PARA O CABE�ALHO E PARA OS TOTAIS ***
		
		SELECT uCrsFt
		LOCATE FOR ALLTRIM(uCrsFt.ftstamp) = ALLTRIM(lcStamp)
		myDocTotal = uCrsFt.etotal
		myDescTotal = uCrsFt.edescc
		myEivain1Total = uCrsFt.EIVAIN1
		myEivain2Total = uCrsFt.EIVAIN2
		myEivain3Total = uCrsFt.EIVAIN3
		myEivain4Total = uCrsFt.EIVAIN4
		myEivain5Total = uCrsFt.EIVAIN5
		myEivain6Total = uCrsFt.EIVAIN6
		myEivain7Total = uCrsFt.EIVAIN7
		myEivain8Total = uCrsFt.EIVAIN8
		myEivain9Total = uCrsFt.EIVAIN9
		myEivain10Total = uCrsFt.EIVAIN10
		myEivain11Total = uCrsFt.EIVAIN11
		myEivain12Total = uCrsFt.EIVAIN12
		myEivain13Total = uCrsFt.EIVAIN13
		myEivav1Total = uCrsFt.EIVAV1
		myEivav2Total = uCrsFt.EIVAV2
		myEivav3Total = uCrsFt.EIVAV3
		myEivav4Total = uCrsFt.EIVAV4
		myEivav5Total = uCrsFt.EIVAV5
		myEivav6Total = uCrsFt.EIVAV6
		myEivav7Total = uCrsFt.EIVAV7
		myEivav8Total = uCrsFt.EIVAV8
		myEivav9Total = uCrsFt.EIVAV9
		myEivav10Total = uCrsFt.EIVAV10
		myEivav11Total = uCrsFt.EIVAV11
		myEivav12Total = uCrsFt.EIVAV12
		myEivav13Total = uCrsFt.EIVAV13
		*********************************************************

		&& textos parametrizaveis - valida por empresa
		IF !EMPTY(ALLTRIM(uf_gerais_getParameter_site('ADM0000000080', "TEXT", mySite)))
			? ALLTRIM(uf_gerais_getParameter_site('ADM0000000080', "TEXT", mySite))
		ENDIF
			
		&& textos parametrizaveis - valida por empresa
		IF !EMPTY(ALLTRIM(uf_gerais_getParameter_site('ADM0000000081', "TEXT", mySite)))
			? ALLTRIM(uf_gerais_getParameter_site('ADM0000000081', "TEXT", mySite))
		ENDIF
		
		&& textos parametrizaveis - valida por empresa
		IF !EMPTY(ALLTRIM(uf_gerais_getParameter_site('ADM0000000082', "TEXT", mySite)))
			? ALLTRIM(uf_gerais_getParameter_site('ADM0000000082', "TEXT", mySite))
		ENDIF
		******************************************

		********************************
		LOCAL DefaultPrinter
		STORE '' TO DefaultPrinter
			
		lcOldPrinter = SET("printer",2) && Impressora por defeito
		
		Set Printer To Name ''+alltrim(myPosPrinter)+''
		Set Device To print
		Set Console off
		
		SELECT uCrsFt
		LOCATE FOR ALLTRIM(uCrsFt.ftstamp) = ALLTRIM(lcStamp)
	
		** **Alterado novas regras certifica��o 20120313
		IF uCrsFt.ncont == "000000000" OR EMPTY(ALLTRIM(uCrsFt.ncont))
			replace uCrsFt.ncont WITH ALLTRIM(uf_gerais_getParameter_site('ADM0000000075', 'text', mySite))
		ENDIF 
		
		SELECT uCrsFt2
		LOCATE FOR ALLTRIM(uCrsFt2.ft2stamp) = ALLTRIM(lcStamp)
		
		&&efecutar c�pia de uCrsFi para poder apagar linhas que n�o interessam para o report
			SELECT * FROM uCrsFi INTO CURSOR uCrsTempA6Fi READWRITE
			
		SELECT uCrsFi
		GO TOP
		SCAN FOR ALLTRIM(uCrsFi.ftstamp) != ALLTRIM(lcStamp)
			DELETE
		ENDSCAN		
		
		SELECT uCrsE1
		GO TOP 
		SELECT uCrsFt
		LOCATE FOR ALLTRIM(uCrsFt.ftstamp) == ALLTRIM(lcStamp)
		SELECT uCrsFi
		GO TOP
		SET REPORTBEHAVIOR 80
		SELECT uCrsFi
		REPORT FORM ALLTRIM(myPath)+"\analises\talaoA6.frx" TO PRINTER NOCONSOLE
		
		&& se tiver dados de venda ao domicilio imprime esses dados
		IF uCrsFt2.ft2_u_vddom == .t.
			SET REPORTBEHAVIOR 80
			REPORT FORM ALLTRIM(myPath)+"\analises\talaoA6_domicilio.frx" TO PRINTER NOCONSOLE
		ENDIF
		
		SET PRINTER TO DEFAULT
		
		** limpar cursores e vari�veis publicas **
		IF USED("uCrsBenef")
			Fecha([uCrsBenef])
		ENDIF
		IF USED("uCrsUltTranCartao")
			fecha("uCrsUltTranCartao")
		ENDIF
		
		RELEASE myDocIdu1, myDocIdu2, myDocIdu3, myDocIdu4, myDocIdu5, myDocIdu6, myDocIdu7, myDocIdu8, myDocIdu9, myDocIdu10
		RELEASE myDocIdu11, myDocIdu12, myDocIdu13, myDocIdu14, myDocIdu15, myDocIdu16, myDocIdu17, myDocIdu18, myDocIdu19, myDocIdu20
		RELEASE myDocIduc1, myDocIduc2, myDocIduc3, myDocIduc4, myDocIduc5, myDocIduc6, myDocIduc7, myDocIduc8, myDocIduc9, myDocIduc10
		RELEASE myDocIduc11, myDocIduc12, myDocIduc13, myDocIduc14, myDocIduc15, myDocIduc16, myDocIduc17, myDocIduc18, myDocIduc19, myDocIduc20
		RELEASE myDocIduh1, myDocIduh2, myDocIduh3, myDocIduh4, myDocIduh5, myDocIduh6, myDocIduh7, myDocIduh8, myDocIduh9, myDocIduh10
		RELEASE myDocIduh11, myDocIduh12, myDocIduh13, myDocIduh14, myDocIduh15, myDocIduh16, myDocIduh17, myDocIduh18, myDocIduh19, myDocIduh20
		**RELEASE myEivain1Total, myEivain2Total, myEivain3Total, myEivain4Total, myDocTotal, myDescTotal, myEivav1Total, myEivav2Total, myEivav3Total, myEivav4Total
		RELEASE myEivain1Total, myEivain2Total, myEivain3Total, myEivain4Total,myEivain5Total,myEivain6Total,myEivain7Total,myEivain8Total,myEivain9Total,myEivain10Total,myEivain11Total,myEivain12Total,myEivain13Total
		RELEASE myDocTotal, myDescTotal
		RELEASE myEivav1Total, myEivav2Total, myEivav3Total, myEivav4Total, myEivav5Total, myEivav6Total, myEivav7Total, myEivav8Total, myEivav9Total, myEivav10Total, myEivav11Total, myEivav12Total, myEivav13Total
		RELEASE myOpCode1, myPrazoReg, myIsentoIva, myTemCertIdu, myReImpressao, mySemValFisc, myImpCapSocial
		RELEASE myDescCcAd1, myDescCcAd2, myDescCcAd3, myDescCcAd4, myPontosDispCcAd, myPontosTotalCcAd, myPontosGanhosCcAd
		RELEASE myRodape1, myRodape2, myRodape3
		
		&&verificar psico			
		IF UPPER(ALLTRIM(lcPainel)) == "ATENDIMENTO"
			IF lcPrintType!=myRegCli AND lcPrintType!=85 && N�o imprimir tal�o psico quando o doc a imprimir � uma Nt. Cr�dito

				uf_imprimirpos_ImpPrevPsico(lcStamp, .f., lcPainel)
			ENDIF
		ENDIF
			 
		&&repor cursor uCrsFi
			SELECT * FROM uCrsTempA6Fi INTO CURSOR uCrsFi READWRITE
			IF USED("uCrsTempA6Fi")
				fecha("uCrsTempA6Fi")
			ENDIF
	ENDIF 
ENDFUNC 


** Fun��o Previs�o Tal�o Venda **
FUNCTION uf_imprimirpos_prevTalao
	
	LPARAMETERS lcStamp, lcPainel, lcExporta
	PUBLIC myPaisNacional
	
	PUBLIC myReImpressao, myImpCapSocial

	IF UPPER(ALLTRIM(lcPainel)) != 'ARQDIGITAL' OR !USED("uCrsFt") OR !USED("uCrsFt2") OR !USED("uCrsFi") OR !USED("uCrsFi2")
		uf_gerais_actGrelha("",[uCrsFt],[exec up_touch_ft ']+lcStamp+['])
		Select uCrsFt
		uf_gerais_actGrelha("",[uCrsFt2],[exec up_touch_ft2 ']+lcStamp+['])
		Select uCrsFt2
		uf_gerais_actGrelha("",[uCrsFi],[exec up_touch_fi ']+alltrim(lcStamp)+[', '', 0])
		Select uCrsFi
		uf_gerais_actGrelha("",[uCrsFi2],[exec up_touch_fi2 ']+alltrim(lcStamp)+[', ''])
		select uCrsFi2
		IF UPPER(ALLTRIM(lcPainel)) != 'ARQDIGITAL'
			myReImpressao = .t.
		ENDIF
	ENDIF
	
	** Texto de Isen��o no IDU (calculado com base na nacionalidade do utente)
	lcSQL = ""
	Select uCrsFt
	TEXT TO lcSQL TEXTMERGE NOSHOW
		Select codigoP FROM b_utentes (nolock) WHERE b_utentes.no = <<uCrsFt.no>> and b_utentes.estab = <<uCrsFt.estab>>
 	ENDTEXT
 	IF !uf_gerais_actGrelha("","uCrsDadosNacionalidadeUtente",lcSQL)
		uf_perguntalt_chama("Ocorreu um erro a verificar Nacionalidade do Utente. POR FAVOR CONTACTE O SUPORTE.1","OK","",16)
 		RETURN .f.
 	Endif	
 	Select uCrsDadosNacionalidadeUtente
 	IF UPPER(ALLTRIM(uCrsDadosNacionalidadeUtente.codigoP)) != 'PT'
		myPaisNacional = .f.
	ELSE
		myPaisNacional = .t.		
	ENDIF 
	
	IF !uf_gerais_getParameter("ADM0000000209","BOOL")
		myReImpressao = .f.
	ENDIF 
	
	IF !USED("uCrsFt")
		RETURN .f.
	ELSE
		SELECT uCrsFt
		IF !RECCOUNT()>0
			RETURN .f.
		ENDIF
	ENDIF

	** Atribui variveis publicas para uso no Report (10)
	PUBLIC myDocIdu1, myDocIdu2, myDocIdu3, myDocIdu4, myDocIdu5, myDocIdu6, myDocIdu7, myDocIdu8, myDocIdu9, myDocIdu10
	PUBLIC myDocIdu11, myDocIdu12, myDocIdu13, myDocIdu14, myDocIdu15, myDocIdu16, myDocIdu17, myDocIdu18, myDocIdu19, myDocIdu20
	PUBLIC myDocIduc1, myDocIduc2, myDocIduc3, myDocIduc4, myDocIduc5, myDocIduc6, myDocIduc7, myDocIduc8, myDocIduc9, myDocIduc10
	PUBLIC myDocIduc11, myDocIduc12, myDocIduc13, myDocIduc14, myDocIduc15, myDocIduc16, myDocIduc17, myDocIduc18, myDocIduc19, myDocIduc20
	PUBLIC myDocIduh1, myDocIduh2, myDocIduh3, myDocIduh4, myDocIduh5, myDocIduh6, myDocIduh7, myDocIduh8, myDocIduh9, myDocIduh10
	PUBLIC myDocIduh11, myDocIduh12, myDocIduh13, myDocIduh14, myDocIduh15, myDocIduh16, myDocIduh17, myDocIduh18, myDocIduh19, myDocIduh20
	PUBLIC myOpCode1, myPrazoReg, myIsentoIva, myTemCertIdu, mySemValFisc
	**PUBLIC myEivain1Total, myEivain2Total, myEivain3Total, myEivain4Total, myDocTotal, myDescTotal, myEivav1Total, myEivav2Total, myEivav3Total, myEivav4Total
	PUBLIC myEivain1Total, myEivain2Total, myEivain3Total, myEivain4Total,myEivain5Total,myEivain6Total,myEivain7Total,myEivain8Total,myEivain9Total,myEivain10Total,myEivain11Total,myEivain12Total,myEivain13Total
	PUBLIC myDocTotal, myDescTotal
	PUBLIC myEivav1Total, myEivav2Total, myEivav3Total, myEivav4Total, myEivav5Total, myEivav6Total, myEivav7Total, myEivav8Total, myEivav9Total, myEivav10Total, myEivav11Total, myEivav12Total, myEivav13Total
	PUBLIC myDescCcAd1, myDescCcAd2, myDescCcAd3, myDescCcAd4
	LOCAL lcDocIdu, lcDocIduc, lcDocIduh, i, lcTemComp, myRodape1, myRodape2, myRodape3
	
	STORE '' TO myDocIdu1, myDocIdu2, myDocIdu3, myDocIdu4, myDocIdu5, myDocIdu6, myDocIdu7, myDocIdu8, myDocIdu9, myDocIdu10
	STORE '' TO myDocIdu11, myDocIdu12, myDocIdu13, myDocIdu14, myDocIdu15, myDocIdu16, myDocIdu17, myDocIdu18, myDocIdu19, myDocIdu20
	
	STORE '' TO myDocIduc1, myDocIduc2, myDocIduc3, myDocIduc4, myDocIduc5, myDocIduc6, myDocIduc7, myDocIduc8, myDocIduc9, myDocIduc10
	STORE '' TO myDocIduc11, myDocIduc12, myDocIduc13, myDocIduc14, myDocIduc15, myDocIduc16, myDocIduc17, myDocIduc18, myDocIduc19, myDocIduc20
	
	STORE '' TO myDocIduh1, myDocIduh2, myDocIduh3, myDocIduh4, myDocIduh5, myDocIduh6, myDocIduh7, myDocIduh8, myDocIduh9, myDocIduh10
	STORE '' TO myDocIduh11, myDocIduh12, myDocIduh13, myDocIduh14, myDocIduh15, myDocIduh16, myDocIduh17, myDocIduh18, myDocIduh19, myDocIduh20
	
	STORE '' TO myOpCode1, myPrazoReg, myTemCertIdu, mySemValFisc
	
	**STORE 0 TO myEivain1Total, myEivain2Total, myEivain3Total, myEivain4Total, myDocTotal, myDescTotal, myEivav1Total, myEivav2Total, myEivav3Total, myEivav4Total
	STORE 0 TO myOpCode, myDocTotal, myDescTotal
	STORE 0 TO myEivain1Total, myEivain2Total, myEivain3Total, myEivain4Total,myEivain5Total,myEivain6Total,myEivain7Total,myEivain8Total,myEivain9Total,myEivain10Total,myEivain11Total,myEivain12Total,myEivain13Total
	STORE 0 TO myEivav1Total, myEivav2Total, myEivav3Total, myEivav4Total, myEivav5Total, myEivav6Total, myEivav7Total, myEivav8Total, myEivav9Total, myEivav10Total, myEivav11Total, myEivav12Total, myEivav13Total
	
	STORE '' TO myDescCcAd1, myDescCcAd2, myDescCcAd3, myDescCcAd4, myPontosDispCcAd, myPontosTotalCcAd, myPontosGanhosCcAd
	
	STORE '' TO lcDocIdu, lcDocIduc, lcDocIduh, myRodape1, myRodape2, myRodape3
	STORE 1 TO i

	*****
	SELECT uCrsFt
	GO TOP
	SCAN
		lcDocIdu 	= 'myDocIdu' + astr(i)
		lcDocIduc	= 'myDocIduc' + astr(i)
		lcDocIduh	= 'myDocIduh' + astr(i)
		
		SELECT uCrsFt
		DO case
			CASE (uCrsFt.u_tipodoc=8) OR (uCrsFt.ndoc=32)
				IF YEAR(uCrsFt.fdata) > 2012
					lcTipoDocImp = "Fatura-Recibo Nr."
				ELSE
					lcTipoDocImp = "Vnd. a Dinheiro Nr."
				ENDIF
			
			CASE (uCrsFt.u_tipodoc=9) OR (uCrsFt.ndoc=11) OR (uCrsFt.ndoc=12)
				lcTipoDocImp  = "Fatura Nr." &&Arq. Digital
			
			CASE (uCrsFt.u_tipodoc=2) or (uCrsFt.u_tipodoc=7)
				lcTipoDocImp  = "Nt. Credito Nr." &&Arq. Digital
			
			CASE (uCrsFt.u_tipodoc=4)
				lcTipoDocImp  = "Fatura Acordo Nr." &&Arq. Digital
			
			CASE (uCrsFt.u_tipodoc=3)
				lcTipoDocImp  = "Insercao Receita Nr." &&Arq. Digital
			
			CASE (uCrsFt.u_tipodoc=11)
				lcTipoDocImp  = "Vnd. Manual Nr." && Venda Manual
			
			OTHERWISE
				lcTipoDocImp  = "" &&
		endcase	

		** IMPRIMIR TAG IDENTIFICADORA DO DOCUMENTO DE VENDA **
		DO CASE
			CASE (uCrsFt.u_tipodoc=8) AND uCrsFt.u_lote=0 AND uCrsFt.cobrado=.f.
				lcTagDocImp  = "(VD)"
			CASE (uCrsFt.u_tipodoc=8) AND uCrsFt.u_lote=0 AND uCrsFt.cobrado=.t.
				lcTagDocImp  = "(VD)(S)"		
			CASE (uCrsFt.u_tipodoc=8) AND uCrsFt.u_lote > 0
				lcTagDocImp  = "(VD-CR)"
			CASE (uCrsFt.ndoc=32) AND uCrsFt.u_lote=0
				lcTagDocImp  = "(VS)"
			CASE (uCrsFt.ndoc=32) AND uCrsFt.u_lote > 0
				lcTagDocImp  = "(VS-CR)"
			CASE (uCrsFt.u_tipodoc=9) AND uCrsFt.cobrado=.f. AND uCrsFt.u_lote=0
				lcTagDocImp  = "(FT)"
			CASE (uCrsFt.u_tipodoc=9) AND uCrsFt.cobrado=.T. AND uCrsFt.u_lote=0
				lcTagDocImp  = "(FT)(S)"
			CASE (uCrsFt.u_tipodoc=9) AND uCrsFt.u_lote > 0
				lcTagDocImp  = "(FT-CR)"
			CASE (uCrsFt.u_tipodoc=2) OR (uCrsFt.u_tipodoc=7)
				lcTagDocImp  = "(RC)"
			CASE (uCrsFt.u_tipodoc=4) And uCrsFt.cobrado=.f. And uCrsFt.u_lote=0
				lcTagDocImp  = "(FA)"
			CASE (uCrsFt.u_tipodoc=4) AND uCrsFt.cobrado=.T. AND uCrsFt.u_lote=0
				lcTagDocImp  = "(FA)(S)"
			CASE (uCrsFt.u_tipodoc=4) AND uCrsFt.u_lote > 0
				lcTagDocImp  = "(FA-CR)"
			CASE (uCrsFt.u_tipodoc=3)
				lcTagDocImp  = "(IR)"
			OTHERWISE
				lcTagDocImp  = "(__)"
		ENDCASE
 
		&lcDocIdu 	= ALLTRIM(lcTipoDocImp) + transform(uCrsFt.fno,"999999999") + " " + ALLTRIM(lcTagDocImp) + "/" + astr(uCrsFt.ndoc)
		
		IF uf_gerais_actGrelha("",[uCrsIduCert],[exec up_gerais_iduCert ']+ALLTRIM(uCrsFt.ftstamp)+['])
			IF RECCOUNT("uCrsIduCert")>0
				IF uCrsIduCert.temCert
					&lcDocIduc		= 'Referente a: ' + ALLTRIM(lcTipoDocImp) + astr(uCrsFt.fno) + " " + ALLTRIM(lcTagDocImp) + "/" + astr(uCrsFt.ndoc)
					&lcDocIduh		= ALLTRIM(uCrsIduCert.parte1)
					myTemCertIdu 	= ALLTRIM(uCrsIduCert.parte2)
					
					&& se o campo documento original estiver preenchido adiciona o valor ao texto da certifica��o
					SELECT uCrsFt2
					GO TOP
					IF !(ALLTRIM(UCrsFt2.u_docorig) == "")
						myTemCertIdu = myTemCertIdu + ' ' + ALLTRIM(uCrsFt2.u_DocOrig)
					ENDIF
					
				ENDIF
			ENDIF
			fecha("uCrsIduCert")
		ENDIF
		
		i = i+1
		
		SELECT uCrsFt
	ENDSCAN
	
	select uCRSe1

	** verificar se imprimir capital social **
	IF uf_gerais_getParameter("ADM0000000124","BOOL")
		myImpCapSocial = .t.
	ENDIF
	**

	** Verificar se o documento tem prazo de regulariza��o e se � um documento fiscal **
	SELECT uCrsFt
	GO TOP
	IF uCrsFt.cobrado
		IF uf_gerais_getParameter("ADM0000000014","NUM") > 0
			myPrazoReg = "Prazo de Regularizacao: " + aStr(uf_gerais_getParameter("ADM0000000014","NUM")) + " Dias"
		ENDIF
	ENDIF
		
	IF uCrsFt.u_tipodoc==3 OR uCrsFt.u_tipodoc==4
		*** Talao sem Validade Fiscal ***
		mySemValFisc = "*** Talao sem Validade Fiscal ***"
	ENDIF
	**
	
	** VERIFICA SE DEVE IMPRIMIR O C�DIGO ADICIONAL DO OPERADOR EM VEZ DO C�DIGO PRINCIPAL **
	LOCAL lcOpCode
	STORE 0 TO lcOpCode
	
	IF uf_gerais_getParameter("ADM0000000047","BOOL")
		lcOpCode = myOpAltCod
	ELSE
		lcOpCode = uCrsFt.vendedor
	ENDIF 
	
	IF lcOpCode==0
		myOpCode1 = "Op: " + Substr(uCrsFt.vendnm,1,30) + " (" + Alltrim(Str(uCrsFt.vendedor)) + ")"
	ELSE
		myOpCode1 = "Op: " + Substr(uCrsFt.vendnm,1,30) + " (" + Alltrim(Str(lcOpCode)) + ")"
	ENDIF
	**

	** dados adicionais do cliente **
	SELECT uCrsFt
	GO TOP
	IF uf_gerais_actGrelha("",[uCrsBenef],[exec up_touch_dadosCliente ]+astr(uCrsFt.no)+[, ]+astr(uCrsFt.estab))
		If !Reccount()>0
			uf_perguntalt_chama("OCORREU UM PROBLEMA A LER DADOS ADICIONAIS DO CLIENTE.","OK","",16)
			RETURN .f.
		ENDIF
	ENDIF
	**

	** Verificar se existe documentos com comparticipa��o **
	SELECT uCrsFt2
	GO TOP
	SCAN
		IF !Empty(uCrsFt2.u_codigo)
			lcTemComp = .t.
		EndIf
	ENDSCAN
	*********************************************************

	** Verificar se Existem produtos isentos de Iva **
	SELECT uCrsFi
	GO TOP
	SCAN
		IF uCrsFi.iva==0 AND !(UPPER(ALLTRIM(uCrsFi.lobs2))=='REF.VD.ORIG.')
			MyIsentoIva = .t.
		ENDIF
	ENDSCAN
	**************************************************

	*** CALCULAR DADOS PARA O CABE�ALHO E PARA OS TOTAIS ***
	
	SELECT uCrsFt
	Calculate Sum(uCrsFt.etotal)  TO myDocTotal
	Calculate Sum(uCrsFt.edescc)  TO myDescTotal
	Calculate Sum(uCrsFt.EIVAIN1) TO myEivain1Total
	Calculate Sum(uCrsFt.EIVAIN2) TO myEivain2Total
	Calculate Sum(uCrsFt.EIVAIN3) TO myEivain3Total
	Calculate Sum(uCrsFt.EIVAIN4) TO myEivain4Total
	Calculate Sum(uCrsFt.EIVAIN5) TO myEivain5Total
	Calculate Sum(uCrsFt.EIVAIN6) TO myEivain6Total
	Calculate Sum(uCrsFt.EIVAIN7) TO myEivain7Total
	Calculate Sum(uCrsFt.EIVAIN8) TO myEivain8Total
	Calculate Sum(uCrsFt.EIVAIN9) TO myEivain9Total
	Calculate Sum(uCrsFt.EIVAIN10) TO myEivain10Total
	Calculate Sum(uCrsFt.EIVAIN11) TO myEivain11Total
	Calculate Sum(uCrsFt.EIVAIN12) TO myEivain12Total
	Calculate Sum(uCrsFt.EIVAIN13) TO myEivain13Total
	Calculate Sum(uCrsFt.EIVAV1)  TO myEivav1Total
	Calculate Sum(uCrsFt.EIVAV2)  TO myEivav2Total
	Calculate Sum(uCrsFt.EIVAV3)  TO myEivav3Total
	Calculate Sum(uCrsFt.EIVAV4)  TO myEivav4Total
	Calculate Sum(uCrsFt.EIVAV5)  TO myEivav5Total
	Calculate Sum(uCrsFt.EIVAV6)  TO myEivav6Total
	Calculate Sum(uCrsFt.EIVAV7)  TO myEivav7Total
	Calculate Sum(uCrsFt.EIVAV8)  TO myEivav8Total
	Calculate Sum(uCrsFt.EIVAV9)  TO myEivav9Total
	Calculate Sum(uCrsFt.EIVAV10)  TO myEivav10Total
	Calculate Sum(uCrsFt.EIVAV11)  TO myEivav11Total
	Calculate Sum(uCrsFt.EIVAV12)  TO myEivav12Total
	Calculate Sum(uCrsFt.EIVAV13)  TO myEivav13Total
	*********************************************************

	** textos parametrizaveis nos parametros **
	myRodape1 = ALLTRIM(uf_gerais_getParameter("ADM0000000131","TEXT"))
	myRodape2 = ALLTRIM(uf_gerais_getParameter("ADM0000000132","TEXT"))
	myRodape3 = ALLTRIM(uf_gerais_getParameter("ADM0000000133","TEXT"))
	******************************************
	
	SELECT uCrsFt
	GO TOP
	uf_gerais_actGrelha("","ucrsTextoRodape",[exec up_cert_TextoRodape '] + ALLTRIM(uCRSFT.tiposaft) + ['])

	IF !uf_gerais_getParameter("ADM0000000126","BOOL") && A6
		SELECT uCrsFt
		GO TOP 	
		SELECT uCrsFt2
		GO TOP
		SELECT uCrsFi
		GO TOP
		IF lcTemComp
			IF FILE(ALLTRIM(myPath)+"\analises\talaocc.frx")
				IF lcExporta
					*Imprimir para PDF
					uf_arquivodigital_preparaPDFCreatorToSave()
					
					SELECT uCrsFi
					GO TOP
					SELECT uCrsFi
					SET REPORTBEHAVIOR 80
					REPORT FORM ALLTRIM(myPath)+"\analises\talaocc.frx" TO PRINTER NOCONSOLE

					*Fecha tramento de impress�o
					INKEY(1)
				ELSE
					SET REPORTBEHAVIOR 90
					REPORT FORM ALLTRIM(myPath)+"\analises\talaocc.frx" TO PRINTER PREVIEW
					SET REPORTBEHAVIOR 80
				ENDIF
			ENDIF
		ELSE
			IF FILE(ALLTRIM(myPath)+"\analises\talao.frx")
				IF lcExporta
					*Imprimir para PDF
					uf_arquivodigital_preparaPDFCreatorToSave()
					
					SET REPORTBEHAVIOR 80
					SELECT uCrsFi
					GO TOP
					SELECT uCrsFi
					REPORT FORM ALLTRIM(myPath)+"\analises\talao.frx" TO PRINTER NOCONSOLE

					*Fecha tramento de impress�o
					INKEY(1)	
				ELSE
					
					SET REPORTBEHAVIOR 90
					REPORT FORM ALLTRIM(myPath)+"\analises\talao.frx" TO PRINTER PREVIEW
					SET REPORTBEHAVIOR 80
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF FILE(ALLTRIM(myPath)+"\analises\talaoA6.frx")
			SELECT uCrsFt
			GO TOP 	
			SELECT uCrsFt2
			GO TOP
			SELECT uCrsFi
			GO TOP
			
			
			IF lcExporta
				*Imprimir para PDF
				uf_arquivodigital_preparaPDFCreatorToSave()
				
				SELECT uCrsE1
				GO TOP 
				SELECT uCrsFT
				GO TOP
				SELECT uCrsFi
				GO TOP
				SET REPORTBEHAVIOR 80
				SELECT uCrsFi
				REPORT FORM ALLTRIM(myPath)+"\analises\talaoA6.frx" TO PRINTER NOCONSOLE

				*Fecha tramento de impress�o
				INKEY(1)	
			ELSE
				SET REPORTBEHAVIOR 90
				REPORT FORM ALLTRIM(myPath)+"\analises\talaoA6.frx" TO PRINTER PREVIEW
			ENDIF
		ENDIF
	ENDIF
	
	IF !lcExporta
		Set Device To screen
		SET PRINTER TO DEFAULT
	ENDIF
	
	** limpar cursores e vari�veis publicas **
	IF USED("uCrsBenef")
		Fecha([uCrsBenef])
	ENDIF
	
	IF UPPER(ALLTRIM(lcPainel)) != 'ARQDIGITAL'
		IF USED("uCrsFt")
			fecha("uCrsFt")
		ENDIF
		
		IF USED("uCrsFt2")
			fecha("uCrsFt2")
		ENDIF
		
		IF USED("uCrsFi")
			fecha("uCrsFi")
		ENDIF

		IF USED("uCrsFi2")
			fecha("uCrsFi2")
		ENDIF
	ENDIF
	
	RELEASE myDocIdu1, myDocIdu2, myDocIdu3, myDocIdu4, myDocIdu5, myDocIdu6, myDocIdu7, myDocIdu8, myDocIdu9, myDocIdu10
	RELEASE myDocIdu11, myDocIdu12, myDocIdu13, myDocIdu14, myDocIdu15, myDocIdu16, myDocIdu17, myDocIdu18, myDocIdu19, myDocIdu20
	RELEASE myDocIduc1, myDocIduc2, myDocIduc3, myDocIduc4, myDocIduc5, myDocIduc6, myDocIduc7, myDocIduc8, myDocIduc9, myDocIduc10
	RELEASE myDocIduc11, myDocIduc12, myDocIduc13, myDocIduc14, myDocIduc15, myDocIduc16, myDocIduc17, myDocIduc18, myDocIduc19, myDocIduc20
	RELEASE myDocIduh1, myDocIduh2, myDocIduh3, myDocIduh4, myDocIduh5, myDocIduh6, myDocIduh7, myDocIduh8, myDocIduh9, myDocIduh10
	RELEASE myDocIduh11, myDocIduh12, myDocIduh13, myDocIduh14, myDocIduh15, myDocIduh16, myDocIduh17, myDocIduh18, myDocIduh19, myDocIduh20
	**RELEASE myEivain1Total, myEivain2Total, myEivain3Total, myEivain4Total, myDocTotal, myDescTotal, myEivav1Total, myEivav2Total, myEivav3Total, myEivav4Total
	RELEASE myEivain1Total, myEivain2Total, myEivain3Total, myEivain4Total,myEivain5Total,myEivain6Total,myEivain7Total,myEivain8Total,myEivain9Total,myEivain10Total,myEivain11Total,myEivain12Total,myEivain13Total
	RELEASE myDocTotal, myDescTotal
	RELEASE myEivav1Total, myEivav2Total, myEivav3Total, myEivav4Total, myEivav5Total, myEivav6Total, myEivav7Total, myEivav8Total, myEivav9Total, myEivav10Total, myEivav11Total, myEivav12Total, myEivav13Total
	RELEASE myOpCode1, myPrazoReg, myIsentoIva, myTemCertIdu, myReImpressao, mySemValFisc, myImpCapSocial
	RELEASE myDescCcAd1, myDescCcAd2, myDescCcAd3, myDescCcAd4, myPontosDispCcAd, myPontosTotalCcAd, myPontosGanhosCcAd
	RELEASE myRodape1, myRodape2, myRodape3
	******************************************
ENDFUNC

FUNCTION uf_imprimirpos_validaSeDeveImprimeTalao
	LPARAMETERS lcToken

	
	IF(LEN(lcToken)>1)
		RETURN .t.
	ENDIF
	
	
	RETURN .f.
ENDFUNC


**Exepcoes para imprimir em talao
**Se existir complementariade e for RSP(LOTE 96 e 97)
**Se o plano for Astellas - AS Astellas, Lundbeck, PSOPortugal - QP

FUNCTION uf_imprimirpos_exepcoesImprimirTalao
	LPARAMETERS lcCod2,lcLote, lcPlanoActual, lcLote2 

	**Se existir complementariade e for RSP(LOTE 96 e 97)
	IF !EMPTY(lcCod2) AND (lcLote=="96" OR lcLote =="97" OR (lcLote2=="96" OR lcLote2 =="97"))
		RETURN .t.
	ENDIF
	
	**Se o plano for Astellas - AS Astellas, Lundbeck, PSOPortugal - QP
	**IF(ALLTRIM(lcPlanoActual)=="AS" OR ALLTRIM(lcPlanoActual)=="WG" OR ALLTRIM(lcPlanoActual)=="GK" OR  ALLTRIM(lcPlanoActual)=="QP" OR  ALLTRIM(lcPlanoActual)=="OG" OR  ALLTRIM(lcPlanoActual)=="CST";
	**	OR  ALLTRIM(lcPlanoActual)=="77" OR  ALLTRIM(lcPlanoActual)=="RS" OR  ALLTRIM(lcPlanoActual)=="OM" OR  ALLTRIM(lcPlanoActual)=="AS")
	**	RETURN .t.
	**ENDIF
	
	IF uf_gerais_getUmValor("cptpla", "imprimeTalao", "codigo = '" + ALLTRIM(lcPlanoActual) + "'")	
		RETURN .T.
	ENDIF
	
	RETURN .f.
	
ENDFUNC



** Funcao Impressao Verso Receita **
FUNCTION uf_imprimirpos_impRec
		
	LPARAMETERS lcStamp, lcVerso, lcPainel

	LOCAL lcMsg, lcPathLogitools
	lcMsg = ""

*!*		IF UPPER(ALLTRIM(lcPainel)) != "ATENDIMENTO" OR !USED("uCrsFi") OR !USED("uCrsFt") OR !USED("uCrsFt2")
*!*			uf_gerais_actGrelha("",[uCrsFt],[exec up_touch_ft ']+alltrim(lcStamp)+['])
*!*			Select uCrsFt
*!*			uf_gerais_actGrelha("",[uCrsFt2],[exec up_touch_ft2 ']+alltrim(lcStamp)+['])
*!*			Select uCrsFt2
*!*			uf_gerais_actGrelha("",[uCrsFi],[exec up_touch_fi ']+alltrim(lcStamp)+[', '', 0])
*!*			Select uCrsFi
*!*			
*!*		ELSE
*!*			Select uCrsFt
*!*			LOCATE FOR ALLTRIM(uCrsFt.ftstamp) = ALLTRIM(lcStamp)
*!*			Select uCrsFt2
*!*			LOCATE FOR ALLTRIM(uCrsFt2.ft2stamp) = ALLTRIM(lcStamp)
*!*		ENDIF

    IF USED("uCrsFt")
       fecha("uCrsFt")
	ENDIF 
    IF USED("uCrsFt2")
       fecha("uCrsFt2")
    ENDIF 
    IF USED("uCrsFi")
        fecha("uCrsFi")
    ENDIF 
    IF USED("uCrsFi2")
        fecha("uCrsFi2")
    ENDIF 
    	
	uf_gerais_actGrelha("",[uCrsFt],[exec up_touch_ft ']+alltrim(lcStamp)+['])
	Select uCrsFt
	uf_gerais_actGrelha("",[uCrsFt2],[exec up_touch_ft2 ']+alltrim(lcStamp)+['])
	Select uCrsFt2
	uf_gerais_actGrelha("",[uCrsFi],[exec up_touch_fi ']+alltrim(lcStamp)+[', '', 0])
	Select uCrsFi
	uf_gerais_actGrelha("",[uCrsFi2],[exec up_touch_fi2 ']+alltrim(lcStamp)+[', ''])
	select uCrsFi2

	** DEFINIR VARI�VEIS INICIAIS 
	LOCAL lcCod, lcCod2, imprimeVerso1, imprimeVerso2, lcPsico
	lcCod	= uCrsFt2.u_codigo
	lcCod2	= uCrsFt2.u_codigo2
	STORE 0 TO imprimeVerso1, imprimeVerso2, lcPsico
	
	SELECT uCrsFi
	SCAN FOR ALLTRIM(uCrsFi.ftstamp) = ALLTRIM(lcStamp)
		IF u_ettent1>0
			imprimeVerso1 = imprimeVerso1 + uCrsFi.qtt
		ENDIF
		IF u_ettent2>0
			imprimeVerso2 = imprimeVerso2 + uCrsFi.qtt
		ENDIF
		IF u_psico == .t.
			lcPsico = 1
		ENDIF

		
	ENDSCAN
	** se tiver dois codigos u_codigo, u_codigo2 � complementar e o sns ta no u_codigo2 
	** verificar se usa impressora A6 **
	IF uf_gerais_getParameter("ADM0000000126","BOOL")
		DO CASE
			CASE lcVerso == 1 && verso 1
				lcVerso = 3
			CASE lcVerso == 2 && verso 2
				lcVerso = 4
		ENDCASE
	ENDIF
	**
	**MESSAGEBOX(lcVerso)
*!*		LOCAL lcimprime
*!*		STORE .t. TO lcimprime
*!*		IF ALLTRIM(uCrsFt.nmdoc)!='Inser��o de Receita'
*!*			DO CASE
*!*				CASE lcVerso = 1
*!*					SELECT uCrsImpTalaoPos
*!*					GO TOP 
*!*					SCAN 
*!*						IF ALLTRIM(uCrsFt2.u_receita)==ALLTRIM(uCrsImpTalaoPos.nrreceita) AND uCrsImpTalaoPos.impcomp1=.t.
*!*							lcimprime = .f.
*!*						ENDIF 
*!*					ENDSCAN 
*!*				CASE lcVerso = 2
*!*					SELECT uCrsImpTalaoPos
*!*					GO TOP 
*!*					SCAN 
*!*						IF ALLTRIM(uCrsFt2.u_receita)==ALLTRIM(uCrsImpTalaoPos.nrreceita) AND uCrsImpTalaoPos.impcomp2=.t.
*!*							lcimprime = .f.
*!*						ENDIF 
*!*					ENDSCAN 
*!*			ENDCASE
*!*		ENDIF 
*!*		
*!*		IF lcimprime = .f.		
*!*			RETURN .f. 
*!*		ENDIF 

	
	IF UPPER(ALLTRIM(ucrse1.pais)) == 'PORTUGAL'
	DO CASE 
		
		CASE lcVerso == 1 && verso 1 
			IF !EMPTY(lcCod)
				IF imprimeVerso1 > 0
					
					&&:TODO
					** PREPARAR DEFINI��ES GERAIS DA IMPRESSORA
					*lcValidaImp = uf_gerais_setImpressoraPOS(.t.)
					*IF lcValidaImp
						Select uCrsFt
						LOCATE FOR ALLTRIM(uCrsFt.ftstamp) = ALLTRIM(lcStamp)
						***PREPARAR DADOS PARA O VERSO DE RECEITA 1
						uf_gerais_actGrelha("","T","SELECT convert(varchar(3),tlote.loteId) as tlote FROM ctltrct (nolock) inner join tlote(nolock) on tlote.codigo=ctltrct.tlote WHERE ctltrctstamp=(select u_ltstamp from ft (nolock) where ftstamp='"+ALLTRIM(uCrsFt.ftstamp)+"')")
		   		    	uf_gerais_actGrelha("","T2","SELECT convert(varchar(3),tlote.loteId) as tlote FROM ctltrct (nolock) inner join tlote(nolock) on tlote.codigo=ctltrct.tlote WHERE ctltrctstamp=(select u_ltstamp2 from ft (nolock) where ftstamp='"+ALLTRIM(uCrsFt.ftstamp)+"')")

						uf_gerais_actGrelha("","L","SELECT convert(varchar(3),lote) as lote FROM ctltrct (nolock) WHERE ctltrctstamp=(select u_ltstamp from ft (nolock) where ftstamp='"+ALLTRIM(uCrsFt.ftstamp)+"')")
						uf_gerais_actGrelha("","R","SELECT convert(varchar(5),nreceita) as nreceita FROM ctltrct (nolock) WHERE ctltrctstamp=(select u_ltstamp from ft (nolock) where ftstamp='"+ALLTRIM(uCrsFt.ftstamp)+"')")
						uf_gerais_actGrelha("","S","SELECT convert(varchar(3),slote) as slote FROM ctltrct (nolock) WHERE ctltrctstamp=(select u_ltstamp from ft (nolock) where ftstamp='"+ALLTRIM(uCrsFt.ftstamp)+"')")
						
						Select uCrsFt
						LOCATE FOR ALLTRIM(uCrsFt.ftstamp) = ALLTRIM(lcStamp)
						Select uCrsFt2
						LOCATE FOR ALLTRIM(uCrsFt2.ft2stamp) = ALLTRIM(lcStamp)


						lcMsg = uf_imprimirpos_impRecTalaoComum(lcStamp, lcVerso)
						
						** VERIFICAR SE � NECESS�RIO IMPRIMIR OS DADOS DO ADQUIRENTE, PSICOTR�PICOS **
						IF lcPsico > 0
							LOCAL lcValidaPsico
							STORE .f. TO lcValidaPsico
							
							** Verificar se Existem Dados Psico **
							SELECT uCrsFt
							LOCATE FOR ALLTRIM(uCrsFt.ftstamp) == ALLTRIM(lcStamp)
							
							IF UPPER(ALLTRIM(lcPainel))!="ATENDIMENTO"
								IF uf_gerais_actGrelha("",[uCrsReImpAdqPsico],[select * from B_dadosPsico (nolock) where ftstamp=']+alltrim(uCrsFt.ftstamp)+['])
									IF RECCOUNT("uCrsReImpAdqPsico")>0
										lcValidaPsico = .t.
									ENDIF
								ENDIF
							ELSE
								SELECT * FROM dadosPsico WHERE ALLTRIM(dadosPsico.ftstamp)=ALLTRIM(uCrsFt.ftstamp) INTO CURSOR uCrsReImpAdqPsico READWRITE
								IF RECCOUNT("uCrsReImpAdqPsico")>0
									lcValidaPsico = .t.
								ENDIF
							ENDIF

							IF lcValidaPsico
								SELECT uCrsReImpAdqPsico
								GO TOP
								**lcMsg = lcMsg + "??" + "?10" + "?10"
								lcMsg = lcMsg + "??" + "Adquirente:"
								**lcMsg = lcMsg + "??" + "?10" + "?10"
								lcMsg = lcMsg + "??" + "Nome: " + SUBSTR(ALLTRIM(uCrsReImpAdqPsico.u_nmutavi),1,40)
								lcMsg = lcMsg + "??" + "?10"
								lcMsg = lcMsg + "??" + "B.I.: " + ALLTRIM(uCrsReImpAdqPsico.u_ndutavi)
								lcMsg = lcMsg + "??" + "?10"
								lcMsg = lcMsg + "??" + "Data: " + IIF(YEAR(uCrsReImpAdqPsico.u_ddutavi) = 3000, "CC Vitalicio", uf_gerais_getDate(uCrsReImpAdqPsico.u_ddutavi,"DATA"))
							
								fecha("uCrsReImpAdqPsico")
							ENDIF
						ENDIF
						
					
						LOCAL lcLote, lcLote2		
						LOCAL lcSlip 
						
						STORE "0" TO lcLote
						STORE "0" TO lcLote2
						
						SELECT T
						lcLote = ALLTRIM(T.tlote)
						SELECT T2
						lcLote2 = ALLTRIM(T2.tlote)
						
						**Valida exep��es � impressao por talao
						IF uf_imprimirpos_exepcoesImprimirTalao(lcCod2,lcLote,lcCod, lcLote2)
							lcSlip = "1"
						ELSE
							IF(imprimeVerso1<=4)
								lcSlip = "4"
							ELSE
								lcSlip = "1"

							ENDIF
								
						ENDIF
						
						IF ALLTRIM(uCrsFt.nmdoc)='Inser��o de Receita' AND !EMPTY(uCrsFt2.u_abrev2) AND EMPTY(uCrsFt2.codacesso)
							uf_perguntalt_chama("4 - VAI SER IMPRESSA A RECEITA DA ENTIDADE "+ALLTRIM(uCrsFt2.u_abrev)+" N� " + ALLTRIM(uCrsFt2.u_receita),"OK","",64)
						ENDIF
						** Enviar para a impressora via java 
						lcPathLogitools = uf_gerais_getParameter('ADM0000000185', 'TEXT')
						
						lcRpath	 = 'javaw -jar ' + '"' + ALLTRIM(lcPathLogitools) + '\ltsPrint\LtsPrint.jar" printpos ' + lcSlip + ' "' + myPosPrinter + '" "' + lcMsg + '"'
													
						oWSShell = CREATEOBJECT("WScript.Shell")
						oWSShell.Run(lcRpath, 0, .t.)
						
					*ENDIF
					*uf_gerais_setImpressoraPOS(.f.)
				ENDIF
			ENDIF 
			
		CASE lcVerso == 2 && verso 2
			IF !EMPTY(lcCod2)
				IF imprimeVerso2 > 0
					&& PREPARAR DEFINI��ES GERAIS DA IMPRESSORA
					*lcValidaImp = uf_gerais_setImpressoraPOS(.t.)
					*IF lcValidaImp
						Select uCrsFt
						LOCATE FOR ALLTRIM(uCrsFt.ftstamp) = ALLTRIM(lcStamp)
						***PREPARAR DADOS PARA O VERSO DE RECEITA 2
						uf_gerais_actGrelha("","T","SELECT convert(varchar(3),tlote.loteId) as tlote FROM ctltrct (nolock) inner join tlote(nolock) on tlote.codigo=ctltrct.tlote WHERE ctltrctstamp=(select u_ltstamp2 from ft (nolock) where ftstamp='"+ALLTRIM(uCrsFt.ftstamp)+"')")
		   		    	uf_gerais_actGrelha("","T2","SELECT convert(varchar(3),tlote.loteId) as tlote FROM ctltrct (nolock) inner join tlote(nolock) on tlote.codigo=ctltrct.tlote WHERE ctltrctstamp=(select u_ltstamp2 from ft (nolock) where ftstamp='"+ALLTRIM(uCrsFt.ftstamp)+"')")

						uf_gerais_actGrelha("","L","SELECT convert(varchar(3),lote) as lote FROM ctltrct (nolock) WHERE ctltrctstamp=(select u_ltstamp2 from ft (nolock) where ftstamp='"+ALLTRIM(uCrsFt.ftstamp)+"')")
						uf_gerais_actGrelha("","R","SELECT convert(varchar(5),nreceita) as nreceita FROM ctltrct (nolock) WHERE ctltrctstamp=(select u_ltstamp2 from ft (nolock) where ftstamp='"+ALLTRIM(uCrsFt.ftstamp)+"')")
						uf_gerais_actGrelha("","S","SELECT convert(varchar(3),slote) as slote FROM ctltrct (nolock) WHERE ctltrctstamp=(select u_ltstamp2 from ft (nolock) where ftstamp='"+ALLTRIM(uCrsFt.ftstamp)+"')")
						
						Select uCrsFt
						LOCATE FOR ALLTRIM(uCrsFt.ftstamp) = ALLTRIM(lcStamp)
						Select uCrsFt2
						LOCATE FOR ALLTRIM(uCrsFt2.ft2stamp) = ALLTRIM(lcStamp)

						lcMsg = uf_imprimirpos_impRecTalaoComum(lcStamp, lcVerso)
						
						** VERIFICAR SE � NECESS�RIO IMPRIMIR OS DADOS DO ADQUIRENTE, PSICOTR�PICOS **
						IF lcPsico > 0
							LOCAL lcValidaPsico
							STORE .f. TO lcValidaPsico
							
							** Verificar se Existem Dados Psico **
							SELECT uCrsFt
							LOCATE FOR ALLTRIM(uCrsFt.ftstamp) = ALLTRIM(lcStamp)
							
							IF UPPER(ALLTRIM(lcPainel))!="ATENDIMENTO"
								IF uf_gerais_actGrelha("",[uCrsReImpAdqPsico],[select * from B_dadosPsico (nolock) where ftstamp=']+alltrim(uCrsFt.ftstamp)+['])
									IF RECCOUNT("uCrsReImpAdqPsico")>0
										lcValidaPsico = .t.
									ENDIF
								ENDIF
							ELSE
								SELECT * FROM dadosPsico WHERE ALLTRIM(dadosPsico.ftstamp)=ALLTRIM(uCrsFt.ftstamp) INTO CURSOR uCrsReImpAdqPsico READWRITE
								IF RECCOUNT("uCrsReImpAdqPsico")>0
									lcValidaPsico = .t.
								ENDIF
							ENDIF 
							
							IF lcValidaPsico
								SELECT uCrsReImpAdqPsico
								GO TOP
								**lcMsg = lcMsg + "??" + "?10" + "?10"
								lcMsg = lcMsg + "??" + "Adquirente:"
								**lcMsg = lcMsg + "??" + "?10" + "?10"
								lcMsg = lcMsg + "??" + "Nome: " + SUBSTR(ALLTRIM(uCrsReImpAdqPsico.u_nmutavi),1,40)
								lcMsg = lcMsg + "??" + "?10"
								lcMsg = lcMsg + "??" + "B.I.: " + ALLTRIM(uCrsReImpAdqPsico.u_ndutavi)
								lcMsg = lcMsg + "??" + "?10"
								lcMsg = lcMsg + "??" + "Data: " + IIF(YEAR(uCrsReImpAdqPsico.u_ddutavi) = 3000, "CC Vitalicio", uf_gerais_getDate(uCrsReImpAdqPsico.u_ddutavi,"DATA"))
								
								fecha("uCrsReImpAdqPsico")
							ENDIF
						ENDIF
						**
		
						LOCAL lcSlip
						IF imprimeVerso2<=4
							lcSlip = "4"
						ELSE
							lcSlip = "1"
						ENDIF
						
						
						&&:TODO
						IF ALLTRIM(uCrsFt.nmdoc)='Inser��o de Receita' AND !EMPTY(uCrsFt2.u_abrev2) AND EMPTY(uCrsFt2.codacesso)
							uf_perguntalt_chama("3 - VAI SER IMPRESSA A RECEITA DA ENTIDADE "+ALLTRIM(uCrsFt2.u_abrev2)+" N� " + ALLTRIM(uCrsFt2.u_receita),"OK","",64)
						ENDIF 
						** Enviar para a impressora via java
						lcPathLogitools = uf_gerais_getParameter('ADM0000000185', 'TEXT')

						lcRpath	 = 'javaw -jar ' + '"' + ALLTRIM(lcPathLogitools) + '\ltsPrint\LtsPrint.jar" printpos ' + lcSlip + ' "' + myPosPrinter + '" "' + lcMsg + '"'

									
						oWSShell = CREATEOBJECT("WScript.Shell")
						oWSShell.Run(lcRpath, 0, .t.)
						
					*ENDIF
					*uf_gerais_setImpressoraPOS(.f.)
				ENDIF
			ENDIF 
		
		CASE lcVerso == 3 && verso 1 - impressora A6
			uf_imprimirpos_prevRec(lcStamp,1,.t.)
		
		CASE lcVerso == 4 && verso 2 - impressora A6
			uf_imprimirpos_prevRec(lcStamp,2,.t.)
		
		OTHERWISE
			uf_perguntalt_chama("PAR�METRO DE VERSO DA RECEITA INV�LIDO","OK","",48)
			
	ENDCASE
	ENDIF 
	
	IF ALLTRIM(uCrsFt.nmdoc)!='Inser��o de Receita'
		IF USED("uCrsImpTalaoPos")
			DO CASE
				CASE lcVerso = 1
					SELECT uCrsImpTalaoPos
					GO TOP 
					SCAN 
						IF ALLTRIM(uCrsFt2.u_receita)==ALLTRIM(uCrsImpTalaoPos.nrreceita) &&AND uCrsImpTalaoPos.impcomp1=.t.
							replace uCrsImpTalaoPos.impcomp1 WITH .t.
						ENDIF 
					ENDSCAN 
				CASE lcVerso = 2
					SELECT uCrsImpTalaoPos
					GO TOP 
					SCAN 
						IF ALLTRIM(uCrsFt2.u_receita)==ALLTRIM(uCrsImpTalaoPos.nrreceita) &&AND uCrsImpTalaoPos.impcomp2=.t.
							replace uCrsImpTalaoPos.impcomp2 WITH .t.
						ENDIF 
					ENDSCAN 
			ENDCASE
		ENDIF 
	ENDIF 
	
ENDFUNC 


FUNCTION uf_imprimirpos_impRec_V2
		
	LPARAMETERS lcStamp, lcVerso, lcPainel, lcPedeAviso

	LOCAL lcMsg, lcPathLogitools, lcAvisaPrint
	lcMsg = ""
	lcAvisaPrint = lcPedeAviso

    IF USED("uCrsFt")
       fecha("uCrsFt")
	ENDIF 
    IF USED("uCrsFt2")
       fecha("uCrsFt2")
    ENDIF 
    IF USED("uCrsFi")
        fecha("uCrsFi")
    ENDIF 
    IF USED("uCrsFi2")
        fecha("uCrsFi2")
    ENDIF 
      	
	uf_gerais_actGrelha("",[uCrsFt],[exec up_touch_ft ']+alltrim(lcStamp)+['])
	Select uCrsFt
	uf_gerais_actGrelha("",[uCrsFt2],[exec up_touch_ft2 ']+alltrim(lcStamp)+['])
	Select uCrsFt2
	uf_gerais_actGrelha("",[uCrsFi],[exec up_touch_fi ']+alltrim(lcStamp)+[', '', 0])
	Select uCrsFi
	uf_gerais_actGrelha("",[uCrsFi2],[exec up_touch_fi2 ']+alltrim(lcStamp)+[', ''])
	select uCrsFi2

	** DEFINIR VARI�VEIS INICIAIS 
	LOCAL lcCod, lcCod2, imprimeVerso1, imprimeVerso2, lcPsico
	lcCod	= uCrsFt2.u_codigo
	lcCod2	= uCrsFt2.u_codigo2
	IF lcAvisaPrint = .f. AND !EMPTY(uCrsFt2.u_codigo2) AND ALLTRIM(uCrsFt.u_tlote)!='96' AND ALLTRIM(uCrsFt.u_tlote)!='97'
		lcAvisaPrint  = .t.
	ENDIF 
	STORE 0 TO imprimeVerso1, imprimeVerso2, lcPsico
	
	SELECT uCrsFi
	SCAN FOR ALLTRIM(uCrsFi.ftstamp) = ALLTRIM(lcStamp)
		IF u_ettent1>0
			imprimeVerso1 = imprimeVerso1 + uCrsFi.qtt
		ENDIF
		IF u_ettent2>0
			imprimeVerso2 = imprimeVerso2 + uCrsFi.qtt
		ENDIF
		IF u_psico == .t.
			lcPsico = 1
		ENDIF

		
	ENDSCAN
	** se tiver dois codigos u_codigo, u_codigo2 � complementar e o sns ta no u_codigo2 
	** verificar se usa impressora A6 **
	IF uf_gerais_getParameter("ADM0000000126","BOOL")
		DO CASE
			CASE lcVerso == 1 && verso 1
				lcVerso = 3
			CASE lcVerso == 2 && verso 2
				lcVerso = 4
		ENDCASE
	ENDIF
	**

	IF UPPER(ALLTRIM(ucrse1.pais)) == 'PORTUGAL'
	DO CASE 
		
		CASE lcVerso == 1 && verso 1 
			IF !EMPTY(lcCod)
				IF imprimeVerso1 > 0
					
					&&:TODO
					** PREPARAR DEFINI��ES GERAIS DA IMPRESSORA
					*lcValidaImp = uf_gerais_setImpressoraPOS(.t.)
					*IF lcValidaImp
						Select uCrsFt
						LOCATE FOR ALLTRIM(uCrsFt.ftstamp) = ALLTRIM(lcStamp)
						***PREPARAR DADOS PARA O VERSO DE RECEITA 1
						uf_gerais_actGrelha("","T","SELECT convert(varchar(3),tlote.loteId) as tlote FROM ctltrct (nolock) inner join tlote(nolock) on tlote.codigo=ctltrct.tlote WHERE ctltrctstamp=(select u_ltstamp from ft (nolock) where ftstamp='"+ALLTRIM(uCrsFt.ftstamp)+"')")
		   		    	uf_gerais_actGrelha("","T2","SELECT convert(varchar(3),tlote.loteId) as tlote FROM ctltrct (nolock) inner join tlote(nolock) on tlote.codigo=ctltrct.tlote WHERE ctltrctstamp=(select u_ltstamp2 from ft (nolock) where ftstamp='"+ALLTRIM(uCrsFt.ftstamp)+"')")

						uf_gerais_actGrelha("","L","SELECT convert(varchar(3),lote) as lote FROM ctltrct (nolock) WHERE ctltrctstamp=(select u_ltstamp from ft (nolock) where ftstamp='"+ALLTRIM(uCrsFt.ftstamp)+"')")
						uf_gerais_actGrelha("","R","SELECT convert(varchar(5),nreceita) as nreceita FROM ctltrct (nolock) WHERE ctltrctstamp=(select u_ltstamp from ft (nolock) where ftstamp='"+ALLTRIM(uCrsFt.ftstamp)+"')")
						uf_gerais_actGrelha("","S","SELECT convert(varchar(3),slote) as slote FROM ctltrct (nolock) WHERE ctltrctstamp=(select u_ltstamp from ft (nolock) where ftstamp='"+ALLTRIM(uCrsFt.ftstamp)+"')")
						
						Select uCrsFt
						LOCATE FOR ALLTRIM(uCrsFt.ftstamp) = ALLTRIM(lcStamp)
						Select uCrsFt2
						LOCATE FOR ALLTRIM(uCrsFt2.ft2stamp) = ALLTRIM(lcStamp)

						lcMsg = uf_imprimirpos_impRecTalaoComum_V2(lcStamp, lcVerso, lcAvisaPrint)
						
						** VERIFICAR SE � NECESS�RIO IMPRIMIR OS DADOS DO ADQUIRENTE, PSICOTR�PICOS **
						IF lcPsico > 0
							LOCAL lcValidaPsico
							STORE .f. TO lcValidaPsico
							
							** Verificar se Existem Dados Psico **
							SELECT uCrsFt
							LOCATE FOR ALLTRIM(uCrsFt.ftstamp) == ALLTRIM(lcStamp)
							
							IF UPPER(ALLTRIM(lcPainel))!="ATENDIMENTO"
								IF uf_gerais_actGrelha("",[uCrsReImpAdqPsico],[select * from B_dadosPsico (nolock) where ftstamp=']+alltrim(uCrsFt.ftstamp)+['])
									IF RECCOUNT("uCrsReImpAdqPsico")>0
										lcValidaPsico = .t.
									ENDIF
								ENDIF
							ELSE
								IF type("atendimento") == "U"
									SELECT * FROM dadosPsico WHERE ALLTRIM(dadosPsico.ftstamp)=ALLTRIM(uCrsFt.ftstamp) INTO CURSOR uCrsReImpAdqPsico READWRITE
									IF RECCOUNT("uCrsReImpAdqPsico")>0
										lcValidaPsico = .t.
									ENDIF
								ELSE
									IF uf_gerais_actGrelha("",[uCrsReImpAdqPsico],[select * from B_dadosPsico (nolock) where ftstamp=']+alltrim(uCrsFt.ftstamp)+['])
										IF RECCOUNT("uCrsReImpAdqPsico")>0
											lcValidaPsico = .t.
										ENDIF
									ENDIF
								ENDIF 
							ENDIF

							IF lcValidaPsico
								SELECT uCrsReImpAdqPsico
								GO TOP
								**lcMsg = lcMsg + "??" + "?10" + "?10"
								lcMsg = lcMsg + "??" + "Adquirente:"
								**lcMsg = lcMsg + "??" + "?10" + "?10"
								lcMsg = lcMsg + "??" + "Nome: " + SUBSTR(ALLTRIM(uCrsReImpAdqPsico.u_nmutavi),1,40)
								lcMsg = lcMsg + "??" + "?10"
								lcMsg = lcMsg + "??" + "B.I.: " + ALLTRIM(uCrsReImpAdqPsico.u_ndutavi)
								lcMsg = lcMsg + "??" + "?10"
								lcMsg = lcMsg + "??" + "Data: " + IIF(YEAR(uCrsReImpAdqPsico.u_ddutavi) = 3000, "CC Vitalicio", uf_gerais_getDate(uCrsReImpAdqPsico.u_ddutavi,"DATA"))
							
								fecha("uCrsReImpAdqPsico")
							ENDIF
						ENDIF
						
					
						LOCAL lcLote, lcLote2		
						LOCAL lcSlip 
						
						STORE "0" TO lcLote
						STORE "0" TO lcLote2
						
						SELECT T
						lcLote = ALLTRIM(T.tlote)
						SELECT T2
						lcLote2 = ALLTRIM(T2.tlote)
						
						**Valida exep��es � impressao por talao
						IF uf_imprimirpos_exepcoesImprimirTalao(lcCod2,lcLote,lcCod, lcLote2)
							lcSlip = "1"
						ELSE
							IF(imprimeVerso1<=4)
								lcSlip = "4"
							ELSE
								lcSlip = "1"

							ENDIF
								
						ENDIF
						
						IF ALLTRIM(uCrsFt.nmdoc)='Inser��o de Receita' AND !EMPTY(uCrsFt2.u_abrev2) AND EMPTY(uCrsFt2.codacesso)
							uf_perguntalt_chama("4 - VAI SER IMPRESSA A RECEITA DA ENTIDADE "+ALLTRIM(uCrsFt2.u_abrev)+" N� " + ALLTRIM(uCrsFt2.u_receita),"OK","",64)
						ENDIF
						** Enviar para a impressora via java 
						lcPathLogitools = uf_gerais_getParameter('ADM0000000185', 'TEXT')
						
						
						lcRpath	 = 'javaw -jar ' + '"' + ALLTRIM(lcPathLogitools) + '\ltsPrint\LtsPrint.jar" printpos ' + lcSlip + ' "' + myPosPrinter + '" "' + lcMsg + '"'

													
						oWSShell = CREATEOBJECT("WScript.Shell")
						oWSShell.Run(lcRpath, 0, .t.)
						
					*ENDIF
					*uf_gerais_setImpressoraPOS(.f.)
				ENDIF
			ENDIF 
			
		CASE lcVerso == 2 && verso 2
			IF !EMPTY(lcCod2)
				IF imprimeVerso2 > 0
					&& PREPARAR DEFINI��ES GERAIS DA IMPRESSORA
					*lcValidaImp = uf_gerais_setImpressoraPOS(.t.)
					*IF lcValidaImp
						Select uCrsFt
						LOCATE FOR ALLTRIM(uCrsFt.ftstamp) = ALLTRIM(lcStamp)
						***PREPARAR DADOS PARA O VERSO DE RECEITA 2
						uf_gerais_actGrelha("","T","SELECT convert(varchar(3),loteId) as tlote FROM ctltrct (nolock) inner join tlote(nolock) on tlote.codigo=ctltrct.tlote WHERE ctltrctstamp=(select u_ltstamp2 from ft (nolock) where ftstamp='"+ALLTRIM(uCrsFt.ftstamp)+"')")
		   		    	uf_gerais_actGrelha("","T2","SELECT convert(varchar(3),loteId) as tlote FROM ctltrct (nolock) inner join tlote(nolock) on tlote.codigo=ctltrct.tlote WHERE ctltrctstamp=(select u_ltstamp2 from ft (nolock) where ftstamp='"+ALLTRIM(uCrsFt.ftstamp)+"')")

						uf_gerais_actGrelha("","L","SELECT convert(varchar(3),lote) as lote FROM ctltrct (nolock) WHERE ctltrctstamp=(select u_ltstamp2 from ft (nolock) where ftstamp='"+ALLTRIM(uCrsFt.ftstamp)+"')")
						uf_gerais_actGrelha("","R","SELECT convert(varchar(5),nreceita) as nreceita FROM ctltrct (nolock) WHERE ctltrctstamp=(select u_ltstamp2 from ft (nolock) where ftstamp='"+ALLTRIM(uCrsFt.ftstamp)+"')")
						uf_gerais_actGrelha("","S","SELECT convert(varchar(3),slote) as slote FROM ctltrct (nolock) WHERE ctltrctstamp=(select u_ltstamp2 from ft (nolock) where ftstamp='"+ALLTRIM(uCrsFt.ftstamp)+"')")
						
						Select uCrsFt
						LOCATE FOR ALLTRIM(uCrsFt.ftstamp) = ALLTRIM(lcStamp)
						Select uCrsFt2
						LOCATE FOR ALLTRIM(uCrsFt2.ft2stamp) = ALLTRIM(lcStamp)

						lcMsg = uf_imprimirpos_impRecTalaoComum_V2(lcStamp, lcVerso, lcAvisaPrint)
					
						** VERIFICAR SE � NECESS�RIO IMPRIMIR OS DADOS DO ADQUIRENTE, PSICOTR�PICOS **
						IF lcPsico > 0
							LOCAL lcValidaPsico
							STORE .f. TO lcValidaPsico
							
							** Verificar se Existem Dados Psico **
							SELECT uCrsFt
							LOCATE FOR ALLTRIM(uCrsFt.ftstamp) = ALLTRIM(lcStamp)
							
							IF UPPER(ALLTRIM(lcPainel))!="ATENDIMENTO"
								IF uf_gerais_actGrelha("",[uCrsReImpAdqPsico],[select * from B_dadosPsico (nolock) where ftstamp=']+alltrim(uCrsFt.ftstamp)+['])
									IF RECCOUNT("uCrsReImpAdqPsico")>0
										lcValidaPsico = .t.
									ENDIF
								ENDIF
							ELSE
								SELECT * FROM dadosPsico WHERE ALLTRIM(dadosPsico.ftstamp)=ALLTRIM(uCrsFt.ftstamp) INTO CURSOR uCrsReImpAdqPsico READWRITE
								IF RECCOUNT("uCrsReImpAdqPsico")>0
									lcValidaPsico = .t.
								ENDIF
							ENDIF 
							
							IF lcValidaPsico
								SELECT uCrsReImpAdqPsico
								GO TOP
								**lcMsg = lcMsg + "??" + "?10" + "?10"
								lcMsg = lcMsg + "??" + "Adquirente:"
								**lcMsg = lcMsg + "??" + "?10" + "?10"
								lcMsg = lcMsg + "??" + "Nome: " + SUBSTR(ALLTRIM(uCrsReImpAdqPsico.u_nmutavi),1,40)
								lcMsg = lcMsg + "??" + "?10"
								lcMsg = lcMsg + "??" + "B.I.: " + ALLTRIM(uCrsReImpAdqPsico.u_ndutavi)
								lcMsg = lcMsg + "??" + "?10"
								lcMsg = lcMsg + "??" + "Data: " + IIF(YEAR(uCrsReImpAdqPsico.u_ddutavi) = 3000, "CC Vitalicio", uf_gerais_getDate(uCrsReImpAdqPsico.u_ddutavi,"DATA"))
								
								fecha("uCrsReImpAdqPsico")
							ENDIF
						ENDIF
						**
		
						LOCAL lcSlip
						IF imprimeVerso2<=4
							lcSlip = "4"
						ELSE
							lcSlip = "1"
						ENDIF
						
						
						&&:TODO
						IF ALLTRIM(uCrsFt.nmdoc)='Inser��o de Receita' AND !EMPTY(uCrsFt2.u_abrev2) AND EMPTY(uCrsFt2.codacesso)
							uf_perguntalt_chama("3 - VAI SER IMPRESSA A RECEITA DA ENTIDADE "+ALLTRIM(uCrsFt2.u_abrev2)+" N� " + ALLTRIM(uCrsFt2.u_receita),"OK","",64)
						ENDIF 
						** Enviar para a impressora via java
						lcPathLogitools = uf_gerais_getParameter('ADM0000000185', 'TEXT')

						lcRpath	 = 'javaw -jar ' + '"' + ALLTRIM(lcPathLogitools) + '\ltsPrint\LtsPrint.jar" printpos ' + lcSlip + ' "' + myPosPrinter + '" "' + lcMsg + '"'
						
								
						oWSShell = CREATEOBJECT("WScript.Shell")
						oWSShell.Run(lcRpath, 0, .t.)
						
					*ENDIF
					*uf_gerais_setImpressoraPOS(.f.)
				ENDIF
			ENDIF 
		
		CASE lcVerso == 3 && verso 1 - impressora A6
			uf_imprimirpos_prevRec(lcStamp,1,.t.)
		
		CASE lcVerso == 4 && verso 2 - impressora A6
			uf_imprimirpos_prevRec(lcStamp,2,.t.)
		
		OTHERWISE
			uf_perguntalt_chama("PAR�METRO DE VERSO DA RECEITA INV�LIDO","OK","",48)
			
	ENDCASE
	ENDIF 
	
ENDFUNC 


** Funcao Impressao Verso Receita Normal C�DIGO IMPRESSAO**
FUNCTION uf_imprimirpos_impRecTalaoComum

	LPARAMETERS lcStamp, lcVerso
	LOCAL lcMsg, lcOrdemImpressaoReceita, lcCompartV1, lcCompartV2
	lcMsg = ""
	
	    IF USED("uCrsFt")
	       fecha("uCrsFt")
		ENDIF 
	    IF USED("uCrsFt2")
	       fecha("uCrsFt2")
	    ENDIF 
	    IF USED("uCrsFi")
	        fecha("uCrsFi")
	    ENDIF 
		IF USED("uCrsFi2")
			fecha("uCrsFi2")
		ENDIF 

	    uf_gerais_actGrelha("",[uCrsFt],[exec up_touch_ft ']+lcStamp+['])
	    select uCrsFt
	    uf_gerais_actGrelha("",[uCrsFt2],[exec up_touch_ft2 ']+lcStamp+['])
	    select uCrsFt2
	    uf_gerais_actGrelha("",[uCrsFi],[exec up_touch_fi ']+alltrim(lcStamp)+[', '', 0])
	    select uCrsFi
		uf_gerais_actGrelha("",[uCrsFi2],[exec up_touch_fi2 ']+alltrim(lcStamp)+[', ''])
		select uCrsFi2
	    
	    IF USED("uCrsCabVendasprint")
	    	LOCAL lcPos 
			Select uCrsCabVendasprint
			lcPos = Recno()
			
	    	LOCAL lcnrrecprint
	    	lcnrrecprint = ALLTRIM(uCrsFt2.u_receita)
	    	SELECT uCrsFt
	    	IF ALLTRIM(uCrsFt.nmdoc) == 'Inser��o de Receita'
		    	SELECT uCrsCabVendasprint
		    	GO TOP 
		    	SCAN 
		    		IF AT(ALLTRIM(lcnrrecprint),ALLTRIM(uCrsCabVendasprint.design))>0
		    			SELECT uCrsCabVendasprint
		    			DELETE 
		    		ENDIF 
		    	ENDSCAN 
		    	
		    	SELECT uCrsCabVendasprint
				TRY 
					GO lcPos 
				CATCH
					GO TOP  
				ENDTRY
			ENDIF 
		
	    ENDIF 
		
		IF USED("uCrsImpTalaoPos")
			SELECT * FROM uCrsImpTalaoPos WHERE !EMPTY(uCrsImpTalaoPos.nrreceita) INTO CURSOR uCrsImpTalaoPosAux READWRITE 
		ENDIF 
		
*!*			IF ALLTRIM(uCrsFt.nmdoc)!='Inser��o de Receita' AND (RECCOUNT("uCrsImpTalaoPosAux")>1 OR !EMPTY(uCrsFt2.u_abrev2)) AND uCrsImpTalaoPosAux.rm = .t.
*!*				DO CASE
*!*					CASE lcVerso = 1
*!*						uf_perguntalt_chama("1 - VAI SER IMPRESSA A RECEITA DA ENTIDADE "+ALLTRIM(uCrsFt2.u_abrev)+" N� " + ALLTRIM(uCrsFt2.u_receita),"OK","",64)
*!*					CASE lcVerso = 2
*!*						uf_perguntalt_chama("2 - VAI SER IMPRESSA A RECEITA DA ENTIDADE "+ALLTRIM(uCrsFt2.u_abrev2)+" N� " + ALLTRIM(uCrsFt2.u_receita),"OK","",64)
*!*				ENDCASE
*!*			ENDIF  
		
		IF USED("uCrsImpTalaoPos ")
			fecha("uCrsImpTalaoPosAux")
		ENDIF 
				
		**uf_perguntalt_chama("VAI SER IMPRESSA A RECEITA N� " + ALLTRIM(ft2.u_receita),"OK","",64)
		
		** cursores e variaveis para o data-matrix (cabe�alho e linhas)
		LOCAL dm_codigofarmacia, dm_codigoentidade, dm_data, dm_operador, dm_serie, dm_lote, dm_posicaolote, dm_nrvenda, dm_nrreceita, dm_campo2, dm_campo3, dm_campo4, dm_totalutenteliquido, lcFee, lcTotalFee
		STORE "" TO dm_codigofarmacia, dm_codigoentidade, dm_data, dm_operador, dm_serie, dm_lote, dm_posicaolote, dm_nrvenda, dm_nrreceita, dm_campo2, dm_campo3, dm_campo4, dm_totalutenteliquido
		STORE 0.00 TO lcFee, lcTotalFee
		
		IF YEAR(uCrsFt.fdata) >= 2017
			CREATE CURSOR uCrsDMl(cnp c(7), portaria c(3), pvp c(6), pref c(6), comparticipacao c(6), valorutente c(6), p4mb c(6),fee c(6), direitoopcao c(1))
		ELSE
			CREATE CURSOR uCrsDMl(cnp c(7), portaria c(3), pvp c(6), pref c(6), comparticipacao c(6), valorutente c(6), direitoopcao c(1))
		ENDIF
		
		** IMPRESS�O DO CABE�ALHO
		** NOME DA EMPRESA ***
		lcMsg = lcMsg + "??" + "?10?10?10"
		lcMsg = lcMsg + "??" + SUBSTR(uCrsE1.NOMECOMP,1,53)
		
		** OUTROS DADOS DA EMPRESA
		lcMsg = lcMsg + "??" + "?10"
		lcMsg = lcMsg + "??" + SUBSTR(uCrsE1.local,1,10)
		lcMsg = lcMsg + "??" + " Tel:"
		lcMsg = lcMsg + "??" + TRANSFORM(uCrsE1.TELEFONE,"############")
		lcMsg = lcMsg + "??" + "NIF:"
		lcMsg = lcMsg + "??" + TRANSFORM(uCrsE1.NCONT,"99999999999999")
		lcMsg = lcMsg + "??" + "?10"
		lcMsg = lcMsg + "??" + ALLTRIM(uf_gerais_getMacrosReports(2))
		lcMsg = lcMsg + "??" + "?10"
		lcMsg = lcMsg + "??" + "Dir. Tec. "
		lcMsg = lcMsg + "??" + SUBSTR(uCrsE1.U_DIRTEC,1,43)
		lcMsg = lcMsg + "??" + "?10"
		
		** ??##< && Indica que o que vem a seguir � impresso ao lado do datamatrix

		lcMsg = lcMsg + "??##<" + "Cap.Social: " + TRANSFORM(uCrsE1.ecapsocial,"###########") && capital social
		lcMsg = lcMsg + "??##<" + "DOCUMENTO PARA FATURACAO"

		&& impress�o da data da dispensa no verso de receita - se data da dispensa (bidata) != vazio imprime data dispensa sen�o imprime data fatura
		&& no caso das re inser��es imprime data da receita
		IF uCrsFt.u_tipodoc == 3 && se for uma inser��o de receita imprime cdata
			**IF uf_gerais_getDate(uCrsFt.CDATA,"DATA") == '01.01.1900'
			IF uf_gerais_getDate(uCrsFt.BIDATA,"DATA") == '01.01.1900'		
				lcMsg = lcMsg + "??" + '                        '
						
				dm_data = "00000000" && datamatrix data
			ELSE
				&& lcMsg = lcMsg + "??" + uf_gerais_getDate(uCrsFt.CDATA,"DATA") + ' ' + Alltrim(uCrsFt.usrhora)
				lcMsg = lcMsg + "??" + uf_gerais_getDate(uCrsFt.BIDATA,"DATA") + ' ' + Alltrim(uCrsFt.usrhora)			

				&& dm_data = REPLICATE('0',8-LEN(uf_gerais_getDate(uCrsFt.CDATA,"SQL"))) + uf_gerais_getDate(uCrsFt.CDATA,"SQL") && datamatrix data			
				dm_data = REPLICATE('0',8-LEN(uf_gerais_getDate(uCrsFt.BIDATA,"SQL"))) + uf_gerais_getDate(uCrsFt.CDATA,"SQL") && datamatrix data						
			ENDIF
		ELSE
			&& IF uf_gerais_getDate(uCrsFt.CDATA,"DATA") == '01.01.1900'
			IF uf_gerais_getDate(uCrsFt.BIDATA,"DATA") == '01.01.1900'		
				lcMsg = lcMsg + "??" + uf_gerais_getDate(uCrsFt.FDATA,"DATA") + ' ' + ALLTRIM(uCrsFt.usrhora)
				
				dm_data = REPLICATE('0',8-LEN(uf_gerais_getDate(uCrsFt.FDATA,"SQL"))) + uf_gerais_getDate(uCrsFt.FDATA,"SQL") && datamatrix data			
			ELSE
				&&lcMsg = lcMsg + "??" + uf_gerais_getDate(uCrsFt.CDATA,"DATA") + ' ' + Alltrim(uCrsFt.usrhora)
				lcMsg = lcMsg + "??" + uf_gerais_getDate(uCrsFt.BIDATA,"DATA") + ' ' + ALLTRIM(uCrsFt.usrhora)

				&&dm_data = REPLICATE('0',8-LEN(uf_gerais_getDate(uCrsFt.CDATA,"SQL"))) + uf_gerais_getDate(uCrsFt.CDATA,"SQL") && data			
				dm_data = REPLICATE('0',8-LEN(uf_gerais_getDate(uCrsFt.BIDATA,"SQL"))) + uf_gerais_getDate(uCrsFt.BIDATA,"SQL") && data
			ENDIF
		ENDIF

		** Tipo e Nr Documento
		lcMsg = lcMsg + " Vnd - " + ALLTRIM(Str(uCrsFt.NDOC)) + "/" + astr(uCrsFt.FNO) + "(" + astr(uCrsFt.VENDEDOR) + ")"
		lcMsg = lcMsg + "??" + "?10"
		
		dm_operador  = REPLICATE('0',10-LEN(ALLTRIM(LEFT(uCrsFt.vendnm,10)))) + ALLTRIM(LEFT(uCrsFt.vendnm,10)) && datamatrix vendedor
		dm_nrvenda   = REPLICATE('0',7-LEN(astr(uCrsFt.FNO))) + astr(uCrsFt.FNO) && datamatrix n�mero venda
		dm_nrreceita = REPLICATE('0',20-LEN(ALLTRIM(uCrsFt2.u_receita))) + ALLTRIM(uCrsFt2.u_receita) && datamatrix receita

		** Ativar tinteiro a vermelho
		IF uf_gerais_getParameter('ADM0000000196', 'BOOL')
			lcMsg = lcMsg + "??##<"	+ "?29" + "?40" + "?78" + "?2" + "?0" + "?48" + "?50"
		ELSE
			lcMsg = lcMsg + "??##<"	&&+ "?29" + "?40" + "?78" + "?2" + "?0" + "?48" + "?50"
		ENDIF 
		
		DO CASE
			CASE lcVerso = 1
				lcMsg = lcMsg + "?<" + uCrsFt2.U_CODIGO + " " + SUBSTR(uCrsFt2.U_DESIGN,1,30)

				** datamatrix
				dm_codigoentidade = REPLICATE('0',3-LEN(ALLTRIM(uCrsFt2.U_CODIGO))) + ALLTRIM(uCrsFt2.U_CODIGO) && C�digo entidade (??? c�digo plano)
			
			CASE lcVerso = 2
				lcMsg = lcMsg + "?<" + uCrsFt2.U_CODIGO2 + " " + SUBSTR(uCrsFt2.U_DESIGN2,1,30)

				** datamatrix
				dm_codigoentidade = REPLICATE('0',3-LEN(ALLTRIM(uCrsFt2.U_CODIGO2))) + ALLTRIM(uCrsFt2.U_CODIGO2) && C�digo entidade 2 (??? c�digo plano)
		ENDCASE

		** Ativar tinteiro a preto
		IF uf_gerais_getParameter('ADM0000000196', 'BOOL')
			lcMsg = lcMsg + "?29" + "?40" + "?78" + "?2" + "?0" + "?48" + "?49"
		ENDIF 
		
		** Ativar tinteiro a vermelho
		IF uf_gerais_getParameter('ADM0000000196', 'BOOL')
			lcMsg = lcMsg + "??##<"	+ "?29" + "?40" + "?78" + "?2" + "?0" + "?48" + "?50"
		ELSE
			lcMsg = lcMsg + "??##<"	&&+ "?29" + "?40" + "?78" + "?2" + "?0" + "?48" + "?50"
		ENDIF 
		** tlote, lote, nreceita, slote
		lcMsg = lcMsg + "?<" + "T: " + T.TLOTE + " " + "L: " + L.LOTE + " " + "R: " + R.NRECEITA + " " + "S: " + S.SLOTE
		** Ativar tinteiro a preto
		IF uf_gerais_getParameter('ADM0000000196', 'BOOL')
			lcMsg = lcMsg + "?29" + "?40" + "?78" + "?2" + "?0" + "?48" + "?49"
		ENDIF 
		
		dm_lote    	   = REPLICATE('0',4-LEN(ASTR(L.LOTE))) + ASTR(L.LOTE) && datamatrix lote
		dm_posicaolote = REPLICATE('0',5-LEN(ASTR(R.NRECEITA))) + ASTR(R.NRECEITA) && damatrix posicao lote
		dm_serie       = REPLICATE('0',3-LEN(ASTR(S.SLOTE))) + ASTR(S.SLOTE) && datamatrix serie
		
		** Impress�o Nr de receita
		lcMsg = lcMsg + "??##<" + "Nr.Rec.: " + uCrsFt2.u_receita
						
		** Impress�o Nr de benefici�rio - s� imprime o n� de benefici�rio para entidades complementares
		SELECT uCrsFt2
		LOCATE FOR ALLTRIM(uCrsFt2.ft2stamp) == ALLTRIM(lcStamp)
		DO CASE
			CASE lcVerso = 1
				IF !EMPTY(ALLTRIM(uCrsFt2.U_NBENEF2)) AND ALLTRIM(uCrsFt2.U_NBENEF2) <> ALLTRIM(uCrsFt2.c2codpost)
					lcMsg = lcMsg + "??##<" + "Nr.Benef.: " + uCrsFt2.U_NBENEF2
				ENDIF
				IF EMPTY(ALLTRIM(uCrsFt2.U_NBENEF2)) AND !EMPTY(ALLTRIM(uCrsFt2.U_NBENEF)) AND ALLTRIM(uCrsFt2.U_NBENEF) <> ALLTRIM(uCrsFt2.c2codpost)
					lcMsg = lcMsg + "??##<" + "Nr.Benef.: " + uCrsFt2.U_NBENEF
				ENDIF
            IF !EMPTY(uCrsFt2.c2codpost)
               lcMsg = lcMsg + "??##<" + "Nr.Cart�o.: " + ALLTRIM(uCrsFt2.c2codpost)
            ENDIF

			**	IF !EMPTY(ALLTRIM(uCrsFt2.U_NBENEF))
			**		lcMsg = lcMsg + "??##<" + "Nr.Benef.: " + uCrsFt2.U_NBENEF
			**	ENDIF
			OTHERWISE
		ENDCASE

		** tag para no fim ser substitu�da pelo datamatrix
		** ter de ser inserida depois do texto a imprimir ao lado do mesmo
		lcMsg = lcMsg + "%%1"
		
		lcMsg = lcMsg + "??" + "?10"
		lcMsg = lcMsg + "??" + "?27" + "?51" + "?15" && diminuir line spacing
		lcMsg = lcMsg + "??" + "?27" + "?45" + "?1" && ativar sublinhado
		IF YEAR(uCrsFt.fdata) >= 2017
			lcMsg = lcMsg + "??" + "Prod  Pvp   Pref  Qtt  CompUni  CompTot  Utente  P4MB Fee"
		ELSE
			lcMsg = lcMsg + "??" + "Prod  Pvp   Pref  Qtt  CompUni  CompTot  Utente"
		ENDIF
		lcMsg = lcMsg + "??" + "?10"
		lcMsg = lcMsg + "??" + "?27" + "?45" + "?0" && desativar sublinhado
		
		** guardar tabela diplomas para mapear com id para o c�digo matrix
		local lcDiploma_id
		IF !USED("uCrsDplms")
			uf_gerais_actGrelha("","uCrsDplms","select u_design, diploma_id from dplms (nolock) where u_design!=''")
		ENDIF
		
		
		&&:TODO
		**guarda a ordem de impress�o de receita
		ordemImpressaoReceita=uCrsE1.ordemimpressaoreceita
		
		
		
		IF(lcOrdemImpressaoReceita==.f.)
			lcCompartV1 = uCrsFt2.u_abrev
			lcCompartV2 = uCrsFt2.u_abrev2
		ELSE
			lcCompartV1 = uCrsFt2.u_abrev2
			lcCompartV2 = uCrsFt2.u_abrev
		ENDIF
		
		
		
		
			
		** IMPRESS�O DAS LINHAS **
		LOCAL lcContadorLinhas
		lcContadorLinhas = 1
		SELECT uCrsFi
		DO CASE 
			CASE lcVerso = 1
				SCAN FOR uCrsFi.U_ETTENT1 > 0 AND ALLTRIM(uCrsFi.ftstamp) == ALLTRIM(uCrsFt.ftstamp)
					*FOR i=1 to qtt
						lcMsg = lcMsg + "??" + "?10"
						lcMsg = lcMsg + "??" + astr(lcContadorLinhas) + ") " + ALLTRIM(substr(uCrsFi.DESIGN,1,50)) + IIF(!EMPTY(uCrsFi.u_codemb)," -Cod. Emb.:" + uCrsFi.u_codemb,"")
						lcMsg = lcMsg + "??" + "?27" + "?51" + "?20"					
						lcMsg = lcMsg + "??" + "?10"
						lcMsg = lcMsg + "??" + TRANSFORM(uCrsFi.U_EPVP,"9999999.99")
						lcMsg = lcMsg + "??" + TRANSFORM(uCrsFi.U_EPREF,"999999.99")
						lcMsg = lcMsg + "??" + "  " + astr(uCrsFi.qtt) 
						lcMsg = lcMsg + "??" + "    " + ALLTRIM(TRANSFORM(ROUND((uCrsFi.U_ETTENT1/uCrsFi.qtt),2), "99999999.99 "))
						lcMsg = lcMsg + "??" + "    " + ALLTRIM(TRANSFORM(ROUND(uCrsFi.U_ETTENT1,2), "99999999.99 "))		
						lcMsg = lcMsg + "??" + "    " + ALLTRIM(TRANSFORM(ROUND((uCrsFi.U_EPVP-uCrsFi.U_ETTENT1/uCrsFi.qtt)*uCrsFi.qtt,2), "9999999.99"))
						IF YEAR(uCrsFt.fdata) >= 2017 AND ALLTRIM(lcCompartV1) =="SNS" && uCrsFi.u_generico
							lcMsg = lcMsg + "??" + "   " + ALLTRIM(TRANSFORM(ROUND(uCrsFi.PvpTop4,2), "9999999.99"))
							
							IF(MONTH(uCrsFt.fdata) == 1  and year(uCrsFt.fdata) <= 2017)
								IF uCrsFi.u_generico
									lcMSg = lcMsg + "??" + "  " + IIF(uCrsFi.pvptop4 > 0 AND uCrsFi.u_epvp <= uCrsFi.pvptop4, ALLTRIM(TRANSFORM(ROUND(0.35*uCrsFi.qtt,2), "9.99")), "0.00")
								ELSE
									lcMSg = lcMsg + "??" + "  " + IIF(uCrsFi.pvptop4 > 0 AND uCrsFi.pic <= uCrsFi.pvptop4, ALLTRIM(TRANSFORM(ROUND(0.35*uCrsFi.qtt,2), "9.99")), "0.00")
								ENDIF
								
							ELSE
								lcMSg = lcMsg + "??" + "  " + ALLTRIM(TRANSFORM(uCrsFi.pvp4_fee,"9.99"))
								
							ENDIF
									
							&&lcMSg = lcMsg + "??" + "  " + IIF(uCrsFi.pvptop4 > 0 AND uCrsFi.u_epvp <= uCrsFi.pvptop4, ALLTRIM(TRANSFORM(ROUND(0.35*uCrsFi.qtt,2), "9.99")), "0.00")
						ENDIF
						lcMsg = lcMsg + "??" + "?10"
						FOR i = 1 TO qtt
							IF i == uCrsFi.qtt
								lcMsg = lcMsg + "??" + "?29" + "?102" + "?1" + "?29" + "?72" + "?2" + "?29" + "?119" + "?3" + "?29" + "?104" + "?40" + "?29" + "?107" + "?4" + "??" + SUBSTR(uCrsFi.REF,1,7) + "??" + "?0"
							ELSE 
								lcMsg = lcMsg + "??" + "?29" + "?102" + "?1" + "?29" + "?72" + "?0" + "?29" + "?119" + "?3" + "?29" + "?104" + "?40" + "?29" + "?107" + "?4" + "??" + SUBSTR(uCrsFi.REF,1,7) + "??" + "?0"
								lcMsg = lcMsg + "??" + "?10"
							ENDIF
							
							&& s� adiciona caso seja inferior a 10 linhas sen�o d� erro na impress�o do verso em tal�o 
							&& O codigo dm nem sequer � necess�rio nestes casos (impress�o de verso em tal�o com receitas de mais de 10 medicamentos) -- LL 28/07/2016
							IF lcContadorLinhas < 10
								** datamatrix
								SELECT uCrsDMl
								APPEND BLANK
								REPLACE uCrsDMl.cnp WITH REPLICATE('0', 7 - LEN(alltrim(LEFT(uCrsFi.ref,7)))) + ALLTRIM(LEFT(uCrsFi.ref,7)) && datamatrix cnp
								
								** datamatrix portaria
								lcDiploma_id = 0
								SELECT uCrsDplms
								GO TOP
								SCAN
									If ALLTRIM(uCrsDplms.u_design) == ALLTRIM(uCrsFi.u_diploma)
										lcDiploma_id = uCrsDplms.diploma_id
									ENDIF
								ENDSCAN
								
								SELECT uCrsDMl
								replace uCrsDMl.portaria		 WITH REPLICATE('0', 3 - LEN(astr(lcDiploma_id))) + astr(lcDiploma_id) && datamatrix portaria
								replace uCrsDMl.pvp				 WITH REPLICATE('0', 6 - LEN(LEFT(astr(ROUND(uCrsFi.u_epvp * 100, 0)),6))) + LEFT(astr(ROUND(uCrsFi.u_epvp * 100,0)),6) && datamatrix pvp
								replace uCrsDMl.pref 			 WITH REPLICATE('0', 6 - LEN(LEFT(astr(ROUND(uCrsFi.u_epref * 100,0)),6))) + LEFT(astr(ROUND(uCrsFi.u_epref * 100,0)),6) && datamatrix pref
								replace uCrsDMl.comparticipacao  WITH REPLICATE('0', 6 - LEN(LEFT(astr(ROUND(round(uCrsFi.u_ettent1/uCrsFi.qtt,2) * 100,0)),6))) + LEFT(astr(ROUND(round(uCrsFi.u_ettent1/uCrsFi.qtt,2) * 100,0)),6) && datamatrix comparticipacao
								replace uCrsDMl.valorutente      WITH REPLICATE('0', 6 - LEN(LEFT(astr(ROUND(ROUND(uCrsFi.u_epvp-uCrsFi.u_ettent1/uCrsFi.qtt,2) * 100,0)),6))) + LEFT(astr(ROUND(ROUND(uCrsFi.u_epvp-uCrsFi.u_ettent1/uCrsFi.qtt,2) * 100,0)),6) && datamatrix valorutente
								
						
								
								IF YEAR(uCrsFt.fdata) >= 2017  AND ALLTRIM(lcCompartV1) =="SNS"
									
									
									replace uCrsDMl.p4mb WITH REPLICATE('0', 6 - LEN(LEFT(astr(ROUND(uCrsFi.pvptop4 * 100, 0)),6))) + LEFT(astr(ROUND(uCrsFi.pvptop4 * 100,0)),6) && datamatrix pvptop4
									&& datamatrix free
									
									IF(MONTH(uCrsFt.fdata) == 1 and YEAR(uCrsFt.fdata) == 2017 )
										IF uCrsFi.u_generico
											replace uCrsDMl.fee WITH IIF(uCrsFi.pvptop4 > 0 AND uCrsFi.u_epvp <= uCrsFi.pvptop4, '000035', '000000')
										ELSE
											replace uCrsDMl.fee WITH IIF(uCrsFi.pvptop4 > 0 AND uCrsFi.pic <= uCrsFi.pvptop4, '000035', '000000')
										ENDIF
			
										
									ELSE
										
										replace uCrsDMl.fee WITH REPLICATE('0', 6 -  LEN(LEFT(astr(ROUND(uCrsFi.pvp4_fee* 100,0)),6)))  + LEFT(astr(ROUND(uCrsFi.pvp4_fee* 100,0)),6)&& pvptop4_fee

									ENDIF
									
									
									
								ENDIF
								
								
								
								
								IF EMPTY(ALLTRIM(uCrsFi.lobs3)) && se n�o for utilizada excep��o
									replace uCrsDMl.direitoopcao WITH uCrsFi.opcao && datamatrix direito opcao	
								ELSE
									IF ALLTRIM(uCrsFi.lobs3) == 'C1'
										replace uCrsDMl.direitoopcao WITH "S" && datamatrix direito opcao Excecao C1
									ELSE
										replace uCrsDMl.direitoopcao WITH "N" && datamatrix direito opcao Restantes Excecoes
									ENDIF
								ENDIF
							ENDIF
						ENDFOR
						lcContadorLinhas = lcContadorLinhas + 1
						
					*ENDFOR
					
					SELECT uCrsFi
				ENDSCAN

			CASE lcVerso = 2
				SCAN FOR uCrsFi.U_ETTENT2 > 0 AND ALLTRIM(uCrsFi.ftstamp) == ALLTRIM(uCrsFt.ftstamp)
					*FOR i=1 to qtt
						lcMsg = lcMsg + "??" + "?10"
						lcMsg = lcMsg + "??" + astr(lcContadorLinhas) + ") " + ALLTRIM(substr(uCrsFi.DESIGN,1,50)) + IIF(!EMPTY(uCrsFi.u_codemb)," -Cod. Emb.:" + uCrsFi.u_codemb,"")
						lcMsg = lcMsg + "??" + "?27" + "?51" + "?20"
						lcMsg = lcMsg + "??" + "?10"
						lcMsg = lcMsg + "??" + TRANSFORM(uCrsFi.U_EPVP,"9999999.99")
						lcMsg = lcMsg + "??" + TRANSFORM(uCrsFi.U_EPREF,"99999999.99 ")
						lcMsg = lcMsg + "??" + "  " + astr(uCrsFi.qtt)
						lcMsg = lcMsg + "??" + "    " + ALLTRIM(TRANSFORM(ROUND((uCrsFi.U_ETTENT2/uCrsFi.qtt),2), "99999999.99 "))
						lcMsg = lcMsg + "??" + "    " + ALLTRIM(TRANSFORM(ROUND(uCrsFi.U_ETTENT2,2), "9999999.99"))
						lcMsg = lcMsg + "??" + "    " + ALLTRIM(TRANSFORM(ROUND((uCrsFi.U_EPVP-uCrsFi.U_ETTENT2/uCrsFi.qtt)*uCrsFi.qtt,2), "9999999.99"))
						
						
						
						IF YEAR(uCrsFt.fdata) >= 2017 AND ALLTRIM(lcCompartV2) =="SNS"
							lcMsg = lcMsg + "??" + "   " + ALLTRIM(TRANSFORM(ROUND(uCrsFi.PvpTop4,2), "9999999.99"))		
							&&lcMSg = lcMsg + "??" + "  " + IIF(uCrsFi.pvptop4 > 0 AND uCrsFi.u_epvp <= uCrsFi.pvptop4, ALLTRIM(TRANSFORM(ROUND(0.35*uCrsFi.qtt,2), "9.99")), "0.00")
							
							IF(MONTH(uCrsFt.fdata) == 1 and year(uCrsFt.fdata) == 2017  )
								IF uCrsFi.u_generico
									lcMSg = lcMsg + "??" + "  " + IIF(uCrsFi.pvptop4 > 0 AND uCrsFi.u_epvp <= uCrsFi.pvptop4, ALLTRIM(TRANSFORM(ROUND(0.35*uCrsFi.qtt,2), "9.99")), "0.00")
								ELSE
									lcMSg = lcMsg + "??" + "  " + IIF(uCrsFi.pvptop4 > 0 AND uCrsFi.pic <= uCrsFi.pvptop4, ALLTRIM(TRANSFORM(ROUND(0.35*uCrsFi.qtt,2), "9.99")), "0.00")
								ENDIF
								
							ELSE
								lcMSg = lcMsg + "??" + "  " + ALLTRIM(TRANSFORM(uCrsFi.pvp4_fee,"9.99"))
								
							ENDIF
							
						ENDIF
						lcMsg = lcMsg + "??" + "?10"
						FOR i = 1 to qtt	
							IF i == uCrsFi.qtt
								lcMsg = lcMsg + "??" + "?29" + "?102" + "?1" + "?29" + "?72" + "?2" + "?29" + "?119" + "?3" + "?29" + "?104" + "?40" + "?29" + "?107" + "?4" + "??" + SUBSTR(uCrsFi.REF,1,7) + "??" + "?0"
							ELSE
								lcMsg = lcMsg + "??" + "?29" + "?102" + "?1" + "?29" + "?72" + "?0" + "?29" + "?119" + "?3" + "?29" + "?104" + "?40" + "?29" + "?107" + "?4" + "??" + SUBSTR(uCrsFi.REF,1,7) + "??" + "?0"
								lcMsg = lcMsg + "??" + "?10"
							ENDIF
		
							&& s� adiciona caso seja inferior a 10 linhas sen�o d� erro na impress�o do verso em tal�o - o codigo dm nem sequer � necess�rio nestes casos (impress�o de verso em tal�o com receitas de mais de 10 medicamentos) -- LL 28/07/2016
							IF lcContadorLinhas < 10
								** datamatrix
								SELECT uCrsDMl
								APPEND BLANK
								replace uCrsDMl.cnp WITH REPLICATE('0',7-LEN(alltrim(uCrsFi.ref))) + ALLTRIM(uCrsFi.ref) && datamatrix cnp
								
								** datamatrix portaria
								lcDiploma_id = 0
								SELECT uCrsDplms
								GO TOP
								SCAN
									IF ALLTRIM(uCrsDplms.u_design) == ALLTRIM(uCrsFi.u_diploma)
										lcDiploma_id = uCrsDplms.diploma_id
									ENDIF
								ENDSCAN					
								
								SELECT uCrsDMl
								replace uCrsDMl.portaria WITH REPLICATE('0', 3 - LEN(astr(lcDiploma_id))) + astr(lcDiploma_id) && datamatrix portaria
								replace uCrsDMl.pvp WITH REPLICATE('0', 6 - LEN(LEFT(astr(ROUND(uCrsFi.u_epvp * 100, 0)),6))) + LEFT(astr(ROUND(uCrsFi.u_epvp * 100,0)),6) && datamatrix pvp
								replace uCrsDMl.pref WITH REPLICATE('0', 6 - LEN(LEFT(astr(ROUND(uCrsFi.u_epref * 100,0)),6))) + LEFT(astr(ROUND(uCrsFi.u_epref * 100,0)),6) && datamatrix pref
								replace uCrsDMl.comparticipacao WITH REPLICATE('0', 6 - LEN(LEFT(astr(ROUND(round(uCrsFi.u_ettent2/uCrsFi.qtt,2) * 100,0)),6))) + LEFT(astr(ROUND(round(uCrsFi.u_ettent2/uCrsFi.qtt,2) * 100,0)),6) && datamatrix comparticipacao
								replace uCrsDMl.valorutente WITH REPLICATE('0', 6 - LEN(LEFT(astr(ROUND(ROUND(uCrsFi.u_epvp-uCrsFi.u_ettent2/uCrsFi.qtt,2) * 100,0)),6))) + LEFT(astr(ROUND(ROUND(uCrsFi.u_epvp-uCrsFi.u_ettent2/uCrsFi.qtt,2) * 100,0)),6) && datamatrix valorutente
								
								IF YEAR(uCrsFt.fdata) >= 2017  AND ALLTRIM(lcCompartV2) =="SNS"
									
									replace uCrsDMl.p4mb WITH REPLICATE('0', 6 - LEN(LEFT(astr(ROUND(uCrsFi.pvptop4 * 100, 0)),6))) + LEFT(astr(ROUND(uCrsFi.pvptop4 * 100,0)),6) && datamatrix pvp
									&& datamatrix free
									IF(MONTH(uCrsFt.fdata) == 1 and year(uCrsFt.fdata) == 2017  )
										IF uCrsFi.u_generico
											replace uCrsDMl.fee WITH IIF(uCrsFi.pvptop4 > 0 AND uCrsFi.u_epvp <= uCrsFi.pvptop4, '000035', '000000')
										ELSE
											replace uCrsDMl.fee WITH IIF(uCrsFi.pvptop4 > 0 AND uCrsFi.pic <= uCrsFi.pvptop4, '000035', '000000')
										ENDIF
										
									ELSE
									
									   	replace uCrsDMl.fee WITH REPLICATE('0', 6 -  LEN(LEFT(astr(ROUND(uCrsFi.pvp4_fee* 100,0)),6)))  + LEFT(astr(ROUND(uCrsFi.pvp4_fee* 100,0)),6)&& pvptop4_fee
		
									ENDIF
									
									
								ENDIF
								
								
								
								


								IF EMPTY(ALLTRIM(uCrsFi.lobs3)) && se n�o for utilizada excep��o
									replace uCrsDMl.direitoopcao WITH uCrsFi.opcao && datamatrix direito opcao	
								ELSE
									IF ALLTRIM(uCrsFi.lobs3) == 'C1'
										replace uCrsDMl.direitoopcao WITH "S" && datamatrix direito opcao Excecao C1
									ELSE 
										replace uCrsDMl.direitoopcao WITH "N" && datamatrix direito opcao Restantes Excecoes
									ENDIF
								ENDIF
							ENDIF
						ENDFOR
						
						lcContadorLinhas = lcContadorLinhas + 1
						
					*ENDFOR

					SELECT uCrsFi
				ENDSCAN
				
		ENDCASE
		
		
		&& Imprime linhas que n�o est�o preenchidas
		SELECT uCrsDMl
		COUNT TO lcPosicoesDML 
		IF lcPosicoesDML < 4
			FOR i=1 TO 4-lcPosicoesDML 
				
				** datamatrix
				SELECT uCrsDMl
				APPEND BLANK
				replace cnp		 			WITH REPLICATE('0',7) && datamatrix cnp
				replace portaria 			WITH REPLICATE('0',3) && datamatrix portaria
				replace pvp 				WITH REPLICATE('0', 6) && datamatrix pvp
				replace pref				WITH REPLICATE('0', 6) && datamatrix pref
				replace comparticipacao 	WITH REPLICATE('0', 6) && datamatrix comparticipacao
				replace valorutente 		WITH REPLICATE('0', 6) && datamatrix valorutente	
				IF YEAR(uCrsFt.fdata) >= 2017
					replace uCrsDMl.p4mb	WITH REPLICATE('0', 6) && datamatrix pvptop4
					replace uCrsDMl.fee		WITH REPLICATE('0', 6) && datamatrix fee
				ENDIF
				replace direitoopcao 		WITH '0'
			ENDFOR  
		ENDIF 
		
		SELECT uCrsFi
		
		lcMsg = lcMsg + "??" + "?10"
		lcMsg = lcMsg + "??" + "-------------------------------------------"

		** PREPARAR OS TOTAIS
		DO CASE
			CASE lcVerso = 1
				calculate sum(uCrsFi.u_epvp*uCrsFi.qtt) for uCrsFi.u_ettent1>0 AND ALLTRIM(uCrsFi.ftstamp) = ALLTRIM(uCrsFt.ftstamp) to totalPvp
				calculate sum(uCrsFi.qtt) for uCrsFi.u_ettent1>0 AND ALLTRIM(uCrsFi.ftstamp) = ALLTRIM(uCrsFt.ftstamp) to totalQtt
				calculate sum(uCrsFi.u_ettent1) for uCrsFi.u_ettent1>0 AND ALLTRIM(uCrsFi.ftstamp) = ALLTRIM(uCrsFt.ftstamp) to totalComp
				calculate sum(uCrsFi.u_epvp*uCrsFi.qtt-uCrsFi.u_ettent1) FOR uCrsFi.u_ettent1>0 AND ALLTRIM(uCrsFi.ftstamp) = ALLTRIM(uCrsFt.ftstamp) to totalUtente
				
				IF YEAR(uCrsFt.fdata) >= 2017  AND ALLTRIM(lcCompartV1)=="SNS"
					IF MONTH(uCrsFt.fdata)==1 AND year(uCrsFt.fdata) == 2017
						calculate sum(0.35*uCrsfi.qtt) FOR uCrsFi.pvpTop4 > 0 AND ((uCrsFi.u_generico=.t. AND uCrsFi.u_epvp <= uCrsFi.pvptop4) OR (uCrsFi.u_generico=.f. AND uCrsFi.pic <= uCrsFi.pvptop4)) to lcTotalFee
					ELSE
						calculate sum(uCrsFi.pvp4_fee) FOR uCrsFi.u_ettent1>0 AND ALLTRIM(uCrsFi.ftstamp) = ALLTRIM(uCrsFt.ftstamp) to lcTotalFee
					ENDIF
				ENDIF
				
				
			CASE lcVerso = 2
				calculate sum(uCrsFi.u_epvp*uCrsFi.qtt) for uCrsFi.u_ettent2>0 AND ALLTRIM(uCrsFi.ftstamp) = ALLTRIM(uCrsFt.ftstamp) to totalPvp
				calculate sum(uCrsFi.qtt) for uCrsFi.u_ettent2>0 AND ALLTRIM(uCrsFi.ftstamp) = ALLTRIM(uCrsFt.ftstamp) to totalQtt
				calculate sum(uCrsFi.u_ettent2) for uCrsFi.u_ettent2>0 AND ALLTRIM(uCrsFi.ftstamp) = ALLTRIM(uCrsFt.ftstamp) to totalComp
				calculate sum(uCrsFi.u_epvp*uCrsFi.qtt-uCrsFi.u_ettent2) FOR uCrsFi.u_ettent2>0 AND ALLTRIM(uCrsFi.ftstamp) = ALLTRIM(uCrsFt.ftstamp) to totalUtente
				IF YEAR(uCrsFt.fdata) >= 2017  AND ALLTRIM(lcCompartV2)=="SNS"
					IF MONTH(uCrsFt.fdata)==1 AND year(uCrsFt.fdata) == 2017 
						calculate sum(0.35*uCrsfi.qtt) FOR uCrsFi.pvpTop4 > 0 AND ((uCrsFi.u_generico=.t. AND uCrsFi.u_epvp <= uCrsFi.pvptop4) OR (uCrsFi.u_generico=.f. AND uCrsFi.pic <= uCrsFi.pvptop4)) to lcTotalFee
					ELSE
				
						calculate sum(uCrsFi.pvp4_fee) FOR uCrsFi.u_ettent2>0 AND ALLTRIM(uCrsFi.ftstamp) = ALLTRIM(uCrsFt.ftstamp) to lcTotalFee	
					ENDIF
				ENDIF

		ENDCASE


		&& datamatrix free

		
		***** IMPRESS�O DO RODAP� *******
		lcMsg = lcMsg + "??" + "?27" + "?51" + "?25"
		lcMsg = lcMsg + "??" + "?10"
		lcMsg = lcMsg + "??" + "Total Euros"
		lcMsg = lcMsg + "??" + "?10"
		lcMsg = lcMsg + "??" + TRANSFORM(ROUND(totalPvp,2),"######.##") + "      "
		lcMsg = lcMsg + "??" + TRANSFORM(ROUND(totalQtt,2),"####")
		lcMsg = lcMsg + "??" + TRANSFORM(ROUND(totalComp,2),"######.##")
		lcMsg = lcMsg + "??" + TRANSFORM(ROUND(totalUtente,2),"########.##") + "       "
		IF YEAR(uCrsFt.fdata) >= 2017
			lcMsg = lcMsg + "??" + TRANSFORM(ROUND(lcTotalFee,2),"########.##")	
		ENDIF
		

		IF VARTYPE(UsaManCompart)!="U"
			IF !UsaManCompart 
				dm_totalutenteliquido = REPLICATE('0',6-LEN(astr(round(totalUtente,2) * 100))) + astr(round(totalUtente,2) * 100) && datamatrix total utente iliquido
			ELSE
				dm_totalutenteliquido = ''
			ENDIF 
		ELSE
			dm_totalutenteliquido = REPLICATE('0',6-LEN(astr(round(totalUtente,2) * 100))) + astr(round(totalUtente,2) * 100) && datamatrix total utente iliquido
		ENDIF 

		lcMsg = lcMsg + "??" + "?10"
		lcMsg = lcMsg + "??" + "Declaro que me foram dispensadas as " + alltrim(TRANSFORM(totalQtt,"###")) + " embalagens"
		lcMsg = lcMsg + "??" + "?10"
		lcMsg = lcMsg + "??" + "de medicamentos constantes da receita e prestados"
		lcMsg = lcMsg + "??" + "?10"
		lcMsg = lcMsg + "??" + "os conselhos e informacoes sobre a sua utilizacao."

		if uf_gerais_getdate(uCrsFt.fdata,"SQL") > "20130331"
		
			** DIREITO DE OP��O **
			LOCAL exerciDireitoOpcao, lcLinhaOpcao, lcContLinha
			STORE .f. TO exerciDireitoOpcao
			STORE '' TO lcLinhaOpcao
			*** Excep��o ***
			LOCAL nLinhaDirOpcaoC1, nLinhaDirOpcaoC2
			STORE '' TO nLinhaDirOpcaoC1, nLinhaDirOpcaoC2
			
			STORE 1 TO lcContLinha

			&& verificar se tem PVP maior que o PVP5 (pvpmaxre)
			SELECT uCrsFi
			GO TOP
			DO CASE

				CASE lcVerso == 1
					SELECT uCrsFi
					SCAN FOR uCrsFi.U_ETTENT1 > 0 AND ALLTRIM(uCrsFi.ftstamp) == ALLTRIM(uCrsFt.ftstamp)
						**FOR i=1 TO uCrsFi.qtt Alterado a 24/02/2015 devido aos agrupamentos de c�digos de barras nos versos de receita
						
							** Op��o
							** Exerci direito de opcao
							IF uCrsFi.opcao == 'S' AND EMPTY(ALLTRIM(uCrsFi.lobs3))
								exerciDireitoOpcao = .t.
								IF EMPTY(lcLinhaOpcao)
									lcLinhaOpcao = astr(lcContLinha)
								ELSE
									lcLinhaOpcao = lcLinhaOpcao + ", " + astr(lcContLinha)
								ENDIF
							ENDIF
							
							** N�o exerci direito de op��o
							IF uCrsFi.opcao == 'N' AND EMPTY(ALLTRIM(uCrsFi.lobs3))
								IF EMPTY(nLinhaDirOpcaoC2)
									nLinhaDirOpcaoC2 = astr(lcContLinha)
								ELSE
									nLinhaDirOpcaoC2 = nLinhaDirOpcaoC2 + "," + astr(lcCOntLinha)
								ENDIF
							ENDIF
												
							** Excep��o
							IF !EMPTY(ALLTRIM(uCrsFi.lobs3))
								DO CASE
								
									** exerci direito de opcao com excep��o especial
									CASE ALLTRIM(ucrsFi.lobs3) == "C1"
										IF EMPTY(nLinhaDirOpcaoC1)
											nLinhaDirOpcaoC1 = astr(lcContLinha)
										ELSE
											nLinhaDirOpcaoC1 = nLinhaDirOpcaoC1 + "," + astr(lcCOntLinha)
										ENDIF
									
									** n�o exerci direito de opcao
									CASE ALLTRIM(ucrsFi.lobs3) == "C2" OR ALLTRIM(ucrsFi.lobs3) == "A" OR ALLTRIM(ucrsFi.lobs3) == "B"
										IF EMPTY(nLinhaDirOpcaoC2)
											nLinhaDirOpcaoC2 = astr(lcContLinha)
										ELSE
											nLinhaDirOpcaoC2 = nLinhaDirOpcaoC2 + "," + astr(lcCOntLinha)
										ENDIF
								ENDCASE
							ENDIF 
						
								lcContLinha = lcContLinha + 1
						**ENDFOR
					ENDSCAN

				CASE lcVerso == 2

					SELECT uCrsFi
					SCAN FOR uCrsFi.U_ETTENT2 > 0 AND ALLTRIM(uCrsFi.ftstamp) == ALLTRIM(uCrsFt.ftstamp)
						**FOR i=1 TO uCrsFi.qtt
							** Op��o
							** Exerci direito de opcao
							IF uCrsFi.opcao == 'S' AND EMPTY(ALLTRIM(uCrsFi.lobs3))
								exerciDireitoOpcao = .t.
								IF EMPTY(lcLinhaOpcao)
									lcLinhaOpcao = astr(lcContLinha)
								ELSE
									lcLinhaOpcao = lcLinhaOpcao + ", " + astr(lcContLinha)
								ENDIF
							ENDIF
							
							** N�o exerci direito de op��o
							IF uCrsFi.opcao == 'N' AND EMPTY(ALLTRIM(uCrsFi.lobs3))
								IF EMPTY(nLinhaDirOpcaoC2)
									nLinhaDirOpcaoC2 = astr(lcContLinha)
								ELSE
									nLinhaDirOpcaoC2 = nLinhaDirOpcaoC2 + "," + astr(lcCOntLinha)
								ENDIF
							ENDIF
													
							** Excep��o
							IF !EMPTY(ALLTRIM(uCrsFi.lobs3))
								DO CASE
								
									** exerci direito de opcao com excep��o especial
									CASE ALLTRIM(ucrsFi.lobs3) == "C1"
										IF EMPTY(nLinhaDirOpcaoC1)
											nLinhaDirOpcaoC1 = astr(lcContLinha)
										ELSE
											nLinhaDirOpcaoC1 = nLinhaDirOpcaoC1 + "," + astr(lcCOntLinha)
										ENDIF
									
									** n�o exerci direito de opcao
									CASE ALLTRIM(ucrsFi.lobs3) == "C2" OR ALLTRIM(ucrsFi.lobs3) == "A" OR ALLTRIM(ucrsFi.lobs3) == "B"
										IF EMPTY(nLinhaDirOpcaoC2)
											nLinhaDirOpcaoC2 = astr(lcContLinha)
										ELSE
											nLinhaDirOpcaoC2 = nLinhaDirOpcaoC2 + "," + astr(lcCOntLinha)
										ENDIF
								ENDCASE
							ENDIF 
							
							lcContLinha = lcContLinha + 1
						**ENDFOR
					ENDSCAN
			ENDCASE

			IF !EMPTY(lcLinhaOpcao)
				lcLinhaOpcao = lcLinhaOpcao + ')'
			ENDIF
			IF !EMPTY(nLinhaDirOpcaoC1)
				nLinhaDirOpcaoC1  = nLinhaDirOpcaoC1 + ')'
			ENDIF
			IF !EMPTY(nLinhaDirOpcaoC2)
				nLinhaDirOpcaoC2  = nLinhaDirOpcaoC2 + ')'
			ENDIF
			
			IF exerciDireitoOpcao
				lcMsg = lcMsg + "??" + "?10" + "?10"
				lcMsg = lcMsg + "??" + lcLinhaOpcao + " - Direito de Opcao: exerci o direito de opcao "
				lcMsg = lcMsg + "??" + "?10"
				lcMsg = lcMsg + "??" + "para medicamento com preco superior ao 5 mais barato."
			ENDIF
			
			IF !EMPTY(ALLTRIM(nLinhaDirOpcaoC1))
				lcMsg = lcMsg + "??" + "?10" + "?10"
				lcMsg = lcMsg + "??" + nLinhaDirOpcaoC1 + " - Direito de Opcao: exerci o direito de opcao "
				lcMsg = lcMsg + "??" + "?10"
				lcMsg = lcMsg + "??" + "por medicamento mais barato que o prescrito para "
				lcMsg = lcMsg + "??" + "?10"
				lcMsg = lcMsg + "??" + "continuidade terapeutica de tratamento superior a "
				lcMsg = lcMsg + "??" + "?10"
				lcMsg = lcMsg + "??" + "28 dias."
			ENDIF

			IF !EMPTY(ALLTRIM(nLinhaDirOpcaoC2))
				lcMsg = lcMsg + "??" + "?10" + "?10"
				lcMsg = lcMsg + "??" + nLinhaDirOpcaoC2 + " - Direito de Opcao: nao exerci direito de opcao."
			ENDIF
			
			***************************
		ENDIF
		lcMsg = lcMsg + "??" + "?10" + "?10"
		lcMsg = lcMsg + "??" + "O Utente ________________________________________"
		lcMsg = lcMsg + "??" + "?10" + "?10"
		**lcMsg = lcMsg + "??" + "?10" + "?10"

		IF uf_gerais_getdate(uCrsFt.fdata,"SQL") <= "20130331"
			lcMsg = lcMsg + "??" + "?10" + "?10"
			lcMsg = lcMsg + "??" + "Direito de opcao ________________________________"
			lcMsg = lcMsg + "??" + "?10" + "?10"
			**lcMsg = lcMsg + "??" + "?10" + "?10"
		ENDIF
		
        IF lcVerso == 2
		    lcMsg = lcMsg + "??" + "?10"
		    lcMsg = lcMsg + "??" + "?27" + "?100" + "?7"
		    lcMsg = lcMsg + "??" + "?29" + "?86" + "?48"
        ENDIF

		** DataMatrix **
		LOCAL lcDM
		lcDM = ""

		SELECT uCrsE1
		GO TOP

		** vers�o
		lcDM = lcDM + '104'
		
		** c�digo farm�cia
		lcDM = lcDM + REPLICATE('0',6-LEN(ASTR(uCrsE1.u_infarmed))) + ASTR(uCrsE1.u_infarmed)
		
		** codigo entidade + data + operador + serie + lote + posicao lote + numero venda + numero receita 
		lcDM = lcDM + dm_codigoentidade + dm_data + dm_operador + dm_serie + dm_lote + dm_posicaolote + dm_nrvenda + dm_nrreceita 
		
		** campo2 + campo3 + campo4
		lcDM = lcDM	+ "000000000000" + "000000000000" + "00000000000000000000"

		** linhas
		SELECT uCrsDMl
		GO TOP
		SCAN
		

			** cnp + portaria + pvp + pref + comparticipacao + valor utente + direitoopcao
	*		lcDM = lcDM + uCrsDMl.cnp + uCrsDMl.portaria + uCrsDMl.pvp + uCrsDMl.pref + uCrsDMl.comparticipacao + uCrsDMl.valorutente + uCrsDMl.direitoopcao
		lcDM = lcDM + uCrsDMl.cnp + uCrsDMl.portaria + uCrsDMl.pvp + uCrsDMl.pref + uCrsDMl.comparticipacao + uCrsDMl.valorutente + uCrsDMl.p4mb + uCrsDMl.fee + uCrsDMl.direitoopcao 
		ENDSCAN
		fecha("uCrsDMl")

		** total utente liquido
		lcDM = lcDM + dm_totalutenteliquido
		****************

		** adicionar o datamatrix ao conte�do da receita
		** caracteres "##DM" identificam datamatrix
		lcDM = "??##DM" + lcDM
		lcMsg = STRTRAN(lcMsg, "%%1", lcDM)
			
		RETURN lcMsg

ENDFUNC 


FUNCTION uf_imprimirpos_impRecTalaoComum_V2

	LPARAMETERS lcStamp, lcVerso, LcAvisaPrint
	LOCAL lcMsg, lcOrdemImpressaoReceita, lcCompartV1, lcCompartV2, lcAvisaPrintV2
	lcAvisaPrintV2 = LcAvisaPrint
	lcMsg = ""

	    IF USED("uCrsFt")
	       fecha("uCrsFt")
		ENDIF 
	    IF USED("uCrsFt2")
	       fecha("uCrsFt2")
	    ENDIF 
	    IF USED("uCrsFi")
	        fecha("uCrsFi")
	    ENDIF 
		IF USED("uCrsFi2")
			fecha("uCrsFi2")
		ENDIF 

	    uf_gerais_actGrelha("",[uCrsFt],[exec up_touch_ft ']+lcStamp+['])
	    select uCrsFt
	    uf_gerais_actGrelha("",[uCrsFt2],[exec up_touch_ft2 ']+lcStamp+['])
	    select uCrsFt2
	    uf_gerais_actGrelha("",[uCrsFi],[exec up_touch_fi ']+alltrim(lcStamp)+[', '', 0])
	    select uCrsFi
		uf_gerais_actGrelha("",[uCrsFi2],[exec up_touch_fi2 ']+alltrim(lcStamp)+[', ''])
		select uCrsFi2
	    
	    IF USED("uCrsCabVendasprint")
	    	LOCAL lcPos 
			Select uCrsCabVendasprint
			lcPos = Recno()
			
	    	LOCAL lcnrrecprint
	    	lcnrrecprint = ALLTRIM(uCrsFt2.u_receita)
	    	SELECT uCrsFt
	    	IF ALLTRIM(uCrsFt.nmdoc) == 'Inser��o de Receita'
		    	SELECT uCrsCabVendasprint
		    	GO TOP 
		    	SCAN 
		    		IF AT(ALLTRIM(lcnrrecprint),ALLTRIM(uCrsCabVendasprint.design))>0
		    			SELECT uCrsCabVendasprint
		    			DELETE 
		    		ENDIF 
		    	ENDSCAN 
		    	
		    	SELECT uCrsCabVendasprint
				TRY 
					GO lcPos 
				CATCH
					GO TOP  
				ENDTRY
			ENDIF 
		
	    ENDIF 
		
		IF USED("uCrsImpTalaoPos")
			SELECT * FROM uCrsImpTalaoPos WHERE !EMPTY(uCrsImpTalaoPos.nrreceita) INTO CURSOR uCrsImpTalaoPosAux READWRITE 
		ENDIF 

*!*			IF ALLTRIM(uCrsFt.nmdoc)!='Inser��o de Receita' AND (RECCOUNT("uCrsImpTalaoPosAux")>1 OR !EMPTY(uCrsFt2.u_abrev2)) AND uCrsImpTalaoPosAux.rm = .t.
		IF lcAvisaPrintV2 
			DO CASE
				CASE lcVerso = 1
					uf_perguntalt_chama("1 - VAI SER IMPRESSA A RECEITA DA ENTIDADE "+ALLTRIM(uCrsFt2.u_abrev)+" N� " + ALLTRIM(uCrsFt2.u_receita),"OK","",64)
				CASE lcVerso = 2
					uf_perguntalt_chama("2 - VAI SER IMPRESSA A RECEITA DA ENTIDADE "+ALLTRIM(uCrsFt2.u_abrev2)+" N� " + ALLTRIM(uCrsFt2.u_receita),"OK","",64)
			ENDCASE
		ENDIF 
*!*			ENDIF  
		
		IF USED("uCrsImpTalaoPos ")
			fecha("uCrsImpTalaoPosAux")
		ENDIF 
				
		**uf_perguntalt_chama("VAI SER IMPRESSA A RECEITA N� " + ALLTRIM(ft2.u_receita),"OK","",64)
		
		** cursores e variaveis para o data-matrix (cabe�alho e linhas)
		LOCAL dm_codigofarmacia, dm_codigoentidade, dm_data, dm_operador, dm_serie, dm_lote, dm_posicaolote, dm_nrvenda, dm_nrreceita, dm_campo2, dm_campo3, dm_campo4, dm_totalutenteliquido, lcFee, lcTotalFee
		STORE "" TO dm_codigofarmacia, dm_codigoentidade, dm_data, dm_operador, dm_serie, dm_lote, dm_posicaolote, dm_nrvenda, dm_nrreceita, dm_campo2, dm_campo3, dm_campo4, dm_totalutenteliquido
		STORE 0.00 TO lcFee, lcTotalFee
		
		IF YEAR(uCrsFt.fdata) >= 2017
			CREATE CURSOR uCrsDMl(cnp c(7), portaria c(3), pvp c(6), pref c(6), comparticipacao c(6), valorutente c(6), p4mb c(6),fee c(6), direitoopcao c(1))
		ELSE
			CREATE CURSOR uCrsDMl(cnp c(7), portaria c(3), pvp c(6), pref c(6), comparticipacao c(6), valorutente c(6), direitoopcao c(1))
		ENDIF
		
		** IMPRESS�O DO CABE�ALHO
		** NOME DA EMPRESA ***
		lcMsg = lcMsg + "??" + "?10?10?10"
		lcMsg = lcMsg + "??" + SUBSTR(uCrsE1.NOMECOMP,1,53)
		
		** OUTROS DADOS DA EMPRESA
		lcMsg = lcMsg + "??" + "?10"
		lcMsg = lcMsg + "??" + SUBSTR(uCrsE1.local,1,10)
		lcMsg = lcMsg + "??" + " Tel:"
		lcMsg = lcMsg + "??" + TRANSFORM(uCrsE1.TELEFONE,"############")
		lcMsg = lcMsg + "??" + "NIF:"
		lcMsg = lcMsg + "??" + TRANSFORM(uCrsE1.NCONT,"99999999999999")
		lcMsg = lcMsg + "??" + "?10"
		lcMsg = lcMsg + "??" + ALLTRIM(uf_gerais_getMacrosReports(2))
		lcMsg = lcMsg + "??" + "?10"
		lcMsg = lcMsg + "??" + "Dir. Tec. "
		lcMsg = lcMsg + "??" + SUBSTR(uCrsE1.U_DIRTEC,1,43)
		lcMsg = lcMsg + "??" + "?10"
		
		** ??##< && Indica que o que vem a seguir � impresso ao lado do datamatrix

		lcMsg = lcMsg + "??##<" + "Cap.Social: " + TRANSFORM(uCrsE1.ecapsocial,"###########") && capital social
		lcMsg = lcMsg + "??##<" + "DOCUMENTO PARA FATURACAO"

		&& impress�o da data da dispensa no verso de receita - se data da dispensa (bidata) != vazio imprime data dispensa sen�o imprime data fatura
		&& no caso das re inser��es imprime data da receita
		IF uCrsFt.u_tipodoc == 3 && se for uma inser��o de receita imprime cdata
			**IF uf_gerais_getDate(uCrsFt.CDATA,"DATA") == '01.01.1900'
			IF uf_gerais_getDate(uCrsFt.BIDATA,"DATA") == '01.01.1900'		
				lcMsg = lcMsg + "??" + '                        '
						
				dm_data = "00000000" && datamatrix data
			ELSE
				&& lcMsg = lcMsg + "??" + uf_gerais_getDate(uCrsFt.CDATA,"DATA") + ' ' + Alltrim(uCrsFt.usrhora)
				lcMsg = lcMsg + "??" + uf_gerais_getDate(uCrsFt.BIDATA,"DATA") + ' ' + Alltrim(uCrsFt.usrhora)			

				&& dm_data = REPLICATE('0',8-LEN(uf_gerais_getDate(uCrsFt.CDATA,"SQL"))) + uf_gerais_getDate(uCrsFt.CDATA,"SQL") && datamatrix data			
				dm_data = REPLICATE('0',8-LEN(uf_gerais_getDate(uCrsFt.BIDATA,"SQL"))) + uf_gerais_getDate(uCrsFt.CDATA,"SQL") && datamatrix data						
			ENDIF
		ELSE
			&& IF uf_gerais_getDate(uCrsFt.CDATA,"DATA") == '01.01.1900'
			IF uf_gerais_getDate(uCrsFt.BIDATA,"DATA") == '01.01.1900'		
				lcMsg = lcMsg + "??" + uf_gerais_getDate(uCrsFt.FDATA,"DATA") + ' ' + ALLTRIM(uCrsFt.usrhora)
				
				dm_data = REPLICATE('0',8-LEN(uf_gerais_getDate(uCrsFt.FDATA,"SQL"))) + uf_gerais_getDate(uCrsFt.FDATA,"SQL") && datamatrix data			
			ELSE
				&&lcMsg = lcMsg + "??" + uf_gerais_getDate(uCrsFt.CDATA,"DATA") + ' ' + Alltrim(uCrsFt.usrhora)
				lcMsg = lcMsg + "??" + uf_gerais_getDate(uCrsFt.BIDATA,"DATA") + ' ' + ALLTRIM(uCrsFt.usrhora)

				&&dm_data = REPLICATE('0',8-LEN(uf_gerais_getDate(uCrsFt.CDATA,"SQL"))) + uf_gerais_getDate(uCrsFt.CDATA,"SQL") && data			
				dm_data = REPLICATE('0',8-LEN(uf_gerais_getDate(uCrsFt.BIDATA,"SQL"))) + uf_gerais_getDate(uCrsFt.BIDATA,"SQL") && data
			ENDIF
		ENDIF

		** Tipo e Nr Documento
		lcMsg = lcMsg + " Vnd - " + ALLTRIM(Str(uCrsFt.NDOC)) + "/" + astr(uCrsFt.FNO) + "(" + astr(uCrsFt.VENDEDOR) + ")"
		lcMsg = lcMsg + "??" + "?10"
		
		dm_operador  = REPLICATE('0',10-LEN(ALLTRIM(LEFT(uCrsFt.vendnm,10)))) + ALLTRIM(LEFT(uCrsFt.vendnm,10)) && datamatrix vendedor
		dm_nrvenda   = REPLICATE('0',7-LEN(astr(uCrsFt.FNO))) + astr(uCrsFt.FNO) && datamatrix n�mero venda
		dm_nrreceita = REPLICATE('0',20-LEN(ALLTRIM(uCrsFt2.u_receita))) + ALLTRIM(uCrsFt2.u_receita) && datamatrix receita

		** Ativar tinteiro a vermelho
		IF uf_gerais_getParameter('ADM0000000196', 'BOOL')
			lcMsg = lcMsg + "??##<"	+ "?29" + "?40" + "?78" + "?2" + "?0" + "?48" + "?50"
		ELSE
			lcMsg = lcMsg + "??##<"	&&+ "?29" + "?40" + "?78" + "?2" + "?0" + "?48" + "?50"
		ENDIF 
		
		DO CASE
			CASE lcVerso = 1
				lcMsg = lcMsg + "?<" + uCrsFt2.U_CODIGO + " " + SUBSTR(uCrsFt2.U_DESIGN,1,30)

				** datamatrix
				dm_codigoentidade = REPLICATE('0',3-LEN(ALLTRIM(uCrsFt2.U_CODIGO))) + ALLTRIM(uCrsFt2.U_CODIGO) && C�digo entidade (??? c�digo plano)
			
			CASE lcVerso = 2
				lcMsg = lcMsg + "?<" + uCrsFt2.U_CODIGO2 + " " + SUBSTR(uCrsFt2.U_DESIGN2,1,30)

				** datamatrix
				dm_codigoentidade = REPLICATE('0',3-LEN(ALLTRIM(uCrsFt2.U_CODIGO2))) + ALLTRIM(uCrsFt2.U_CODIGO2) && C�digo entidade 2 (??? c�digo plano)
		ENDCASE

		** Ativar tinteiro a preto
		IF uf_gerais_getParameter('ADM0000000196', 'BOOL')
			lcMsg = lcMsg + "?29" + "?40" + "?78" + "?2" + "?0" + "?48" + "?49"
		ENDIF 
		
		** Ativar tinteiro a vermelho
		IF uf_gerais_getParameter('ADM0000000196', 'BOOL')
			lcMsg = lcMsg + "??##<"	+ "?29" + "?40" + "?78" + "?2" + "?0" + "?48" + "?50"
		ELSE
			lcMsg = lcMsg + "??##<"	&&+ "?29" + "?40" + "?78" + "?2" + "?0" + "?48" + "?50"
		ENDIF 
		** tlote, lote, nreceita, slote
		lcMsg = lcMsg + "?<" + "T: " + T.TLOTE + " " + "L: " + L.LOTE + " " + "R: " + R.NRECEITA + " " + "S: " + S.SLOTE
		** Ativar tinteiro a preto
		IF uf_gerais_getParameter('ADM0000000196', 'BOOL')
			lcMsg = lcMsg + "?29" + "?40" + "?78" + "?2" + "?0" + "?48" + "?49"
		ENDIF 
		
		dm_lote    	   = REPLICATE('0',4-LEN(ASTR(L.LOTE))) + ASTR(L.LOTE) && datamatrix lote
		dm_posicaolote = REPLICATE('0',5-LEN(ASTR(R.NRECEITA))) + ASTR(R.NRECEITA) && damatrix posicao lote
		dm_serie       = REPLICATE('0',3-LEN(ASTR(S.SLOTE))) + ASTR(S.SLOTE) && datamatrix serie
		
		** Impress�o Nr de receita
		lcMsg = lcMsg + "??##<" + "Nr.Rec.: " + uCrsFt2.u_receita
						
		** Impress�o Nr de benefici�rio - s� imprime o n� de benefici�rio para entidades complementares
		SELECT uCrsFt2
		LOCATE FOR ALLTRIM(uCrsFt2.ft2stamp) == ALLTRIM(lcStamp)
		DO CASE
			CASE lcVerso = 1
				IF !EMPTY(ALLTRIM(uCrsFt2.U_NBENEF2)) AND ALLTRIM(uCrsFt2.U_NBENEF2) <> ALLTRIM(uCrsFt2.c2codpost)
					lcMsg = lcMsg + "??##<" + "Nr.Benef.: " + uCrsFt2.U_NBENEF2
				ENDIF
				IF EMPTY(ALLTRIM(uCrsFt2.U_NBENEF2)) AND !EMPTY(ALLTRIM(uCrsFt2.U_NBENEF)) AND ALLTRIM(uCrsFt2.U_NBENEF) <> ALLTRIM(uCrsFt2.c2codpost)
					lcMsg = lcMsg + "??##<" + "Nr.Benef.: " + uCrsFt2.U_NBENEF
				ENDIF
            IF !EMPTY(uCrsFt2.c2codpost)
               lcMsg = lcMsg + "??##<" + "Nr.Cart�o.: " + ALLTRIM(uCrsFt2.c2codpost)
            ENDIF

			**	IF !EMPTY(ALLTRIM(uCrsFt2.U_NBENEF))
			**		lcMsg = lcMsg + "??##<" + "Nr.Benef.: " + uCrsFt2.U_NBENEF
			**	ENDIF
			OTHERWISE
		ENDCASE

		** tag para no fim ser substitu�da pelo datamatrix
		** ter de ser inserida depois do texto a imprimir ao lado do mesmo
		lcMsg = lcMsg + "%%1"
		
		lcMsg = lcMsg + "??" + "?10"
		lcMsg = lcMsg + "??" + "?27" + "?51" + "?15" && diminuir line spacing
		lcMsg = lcMsg + "??" + "?27" + "?45" + "?1" && ativar sublinhado
		IF YEAR(uCrsFt.fdata) >= 2017
			lcMsg = lcMsg + "??" + "Prod  Pvp   Pref  Qtt  CompUni  CompTot  Utente  P4MB Fee"
		ELSE
			lcMsg = lcMsg + "??" + "Prod  Pvp   Pref  Qtt  CompUni  CompTot  Utente"
		ENDIF
		lcMsg = lcMsg + "??" + "?10"
		lcMsg = lcMsg + "??" + "?27" + "?45" + "?0" && desativar sublinhado
		
		** guardar tabela diplomas para mapear com id para o c�digo matrix
		local lcDiploma_id
		IF !USED("uCrsDplms")
			uf_gerais_actGrelha("","uCrsDplms","select u_design, diploma_id from dplms (nolock) where u_design!=''")
		ENDIF
		
		
		&&:TODO
		**guarda a ordem de impress�o de receita
		ordemImpressaoReceita=uCrsE1.ordemimpressaoreceita
		
		
		
		IF(lcOrdemImpressaoReceita==.f.)
			lcCompartV1 = uCrsFt2.u_abrev
			lcCompartV2 = uCrsFt2.u_abrev2
		ELSE
			lcCompartV1 = uCrsFt2.u_abrev2
			lcCompartV2 = uCrsFt2.u_abrev
		ENDIF
		
		
		
		
			
		** IMPRESS�O DAS LINHAS **
		LOCAL lcContadorLinhas
		lcContadorLinhas = 1
		SELECT uCrsFi
		DO CASE 
			CASE lcVerso = 1
				SCAN FOR uCrsFi.U_ETTENT1 > 0 AND ALLTRIM(uCrsFi.ftstamp) == ALLTRIM(uCrsFt.ftstamp)
					*FOR i=1 to qtt
						lcMsg = lcMsg + "??" + "?10"
						lcMsg = lcMsg + "??" + astr(lcContadorLinhas) + ") " + ALLTRIM(substr(uCrsFi.DESIGN,1,50)) + IIF(!EMPTY(uCrsFi.u_codemb)," -Cod. Emb.:" + uCrsFi.u_codemb,"")
						lcMsg = lcMsg + "??" + "?27" + "?51" + "?20"					
						lcMsg = lcMsg + "??" + "?10"
						lcMsg = lcMsg + "??" + TRANSFORM(uCrsFi.U_EPVP,"9999999.99")
						lcMsg = lcMsg + "??" + TRANSFORM(uCrsFi.U_EPREF,"999999.99")
						lcMsg = lcMsg + "??" + "  " + astr(uCrsFi.qtt) 
						lcMsg = lcMsg + "??" + "    " + ALLTRIM(TRANSFORM(ROUND((uCrsFi.U_ETTENT1/uCrsFi.qtt),2), "99999999.99 "))
						lcMsg = lcMsg + "??" + "    " + ALLTRIM(TRANSFORM(ROUND(uCrsFi.U_ETTENT1,2), "99999999.99 "))		
						lcMsg = lcMsg + "??" + "    " + ALLTRIM(TRANSFORM(ROUND((uCrsFi.U_EPVP-uCrsFi.U_ETTENT1/uCrsFi.qtt)*uCrsFi.qtt,2), "9999999.99"))
						IF YEAR(uCrsFt.fdata) >= 2017 AND ALLTRIM(lcCompartV1) =="SNS" && uCrsFi.u_generico
							lcMsg = lcMsg + "??" + "   " + ALLTRIM(TRANSFORM(ROUND(uCrsFi.PvpTop4,2), "9999999.99"))
							
							IF(MONTH(uCrsFt.fdata) == 1  and year(uCrsFt.fdata) <= 2017)
								IF uCrsFi.u_generico
									lcMSg = lcMsg + "??" + "  " + IIF(uCrsFi.pvptop4 > 0 AND uCrsFi.u_epvp <= uCrsFi.pvptop4, ALLTRIM(TRANSFORM(ROUND(0.35*uCrsFi.qtt,2), "9.99")), "0.00")
								ELSE
									lcMSg = lcMsg + "??" + "  " + IIF(uCrsFi.pvptop4 > 0 AND uCrsFi.pic <= uCrsFi.pvptop4, ALLTRIM(TRANSFORM(ROUND(0.35*uCrsFi.qtt,2), "9.99")), "0.00")
								ENDIF
								
							ELSE
								lcMSg = lcMsg + "??" + "  " + ALLTRIM(TRANSFORM(uCrsFi.pvp4_fee,"9.99"))
								
							ENDIF
									
							&&lcMSg = lcMsg + "??" + "  " + IIF(uCrsFi.pvptop4 > 0 AND uCrsFi.u_epvp <= uCrsFi.pvptop4, ALLTRIM(TRANSFORM(ROUND(0.35*uCrsFi.qtt,2), "9.99")), "0.00")
						ENDIF
						lcMsg = lcMsg + "??" + "?10"
						FOR i = 1 TO qtt
							IF i == uCrsFi.qtt
								lcMsg = lcMsg + "??" + "?29" + "?102" + "?1" + "?29" + "?72" + "?2" + "?29" + "?119" + "?3" + "?29" + "?104" + "?40" + "?29" + "?107" + "?4" + "??" + SUBSTR(uCrsFi.REF,1,7) + "??" + "?0"
							ELSE 
								lcMsg = lcMsg + "??" + "?29" + "?102" + "?1" + "?29" + "?72" + "?0" + "?29" + "?119" + "?3" + "?29" + "?104" + "?40" + "?29" + "?107" + "?4" + "??" + SUBSTR(uCrsFi.REF,1,7) + "??" + "?0"
								lcMsg = lcMsg + "??" + "?10"
							ENDIF
							
							&& s� adiciona caso seja inferior a 10 linhas sen�o d� erro na impress�o do verso em tal�o 
							&& O codigo dm nem sequer � necess�rio nestes casos (impress�o de verso em tal�o com receitas de mais de 10 medicamentos) -- LL 28/07/2016
							IF lcContadorLinhas < 10
								** datamatrix
								SELECT uCrsDMl
								APPEND BLANK
								REPLACE uCrsDMl.cnp WITH REPLICATE('0', 7 - LEN(alltrim(LEFT(uCrsFi.ref,7)))) + ALLTRIM(LEFT(uCrsFi.ref,7)) && datamatrix cnp
								
								** datamatrix portaria
								lcDiploma_id = 0
								SELECT uCrsDplms
								GO TOP
								SCAN
									If ALLTRIM(uCrsDplms.u_design) == ALLTRIM(uCrsFi.u_diploma)
										lcDiploma_id = uCrsDplms.diploma_id
									ENDIF
								ENDSCAN
								
								SELECT uCrsDMl
								replace uCrsDMl.portaria		 WITH REPLICATE('0', 3 - LEN(astr(lcDiploma_id))) + astr(lcDiploma_id) && datamatrix portaria
								replace uCrsDMl.pvp				 WITH REPLICATE('0', 6 - LEN(LEFT(astr(ROUND(uCrsFi.u_epvp * 100, 0)),6))) + LEFT(astr(ROUND(uCrsFi.u_epvp * 100,0)),6) && datamatrix pvp
								replace uCrsDMl.pref 			 WITH REPLICATE('0', 6 - LEN(LEFT(astr(ROUND(uCrsFi.u_epref * 100,0)),6))) + LEFT(astr(ROUND(uCrsFi.u_epref * 100,0)),6) && datamatrix pref
								replace uCrsDMl.comparticipacao  WITH REPLICATE('0', 6 - LEN(LEFT(astr(ROUND(round(uCrsFi.u_ettent1/uCrsFi.qtt,2) * 100,0)),6))) + LEFT(astr(ROUND(round(uCrsFi.u_ettent1/uCrsFi.qtt,2) * 100,0)),6) && datamatrix comparticipacao
								replace uCrsDMl.valorutente      WITH REPLICATE('0', 6 - LEN(LEFT(astr(ROUND(ROUND(uCrsFi.u_epvp-uCrsFi.u_ettent1/uCrsFi.qtt,2) * 100,0)),6))) + LEFT(astr(ROUND(ROUND(uCrsFi.u_epvp-uCrsFi.u_ettent1/uCrsFi.qtt,2) * 100,0)),6) && datamatrix valorutente
								
						
								
								IF YEAR(uCrsFt.fdata) >= 2017  AND ALLTRIM(lcCompartV1) =="SNS"
									
									
									replace uCrsDMl.p4mb WITH REPLICATE('0', 6 - LEN(LEFT(astr(ROUND(uCrsFi.pvptop4 * 100, 0)),6))) + LEFT(astr(ROUND(uCrsFi.pvptop4 * 100,0)),6) && datamatrix pvptop4
									&& datamatrix free
									
									IF(MONTH(uCrsFt.fdata) == 1 and YEAR(uCrsFt.fdata) == 2017 )
										IF uCrsFi.u_generico
											replace uCrsDMl.fee WITH IIF(uCrsFi.pvptop4 > 0 AND uCrsFi.u_epvp <= uCrsFi.pvptop4, '000035', '000000')
										ELSE
											replace uCrsDMl.fee WITH IIF(uCrsFi.pvptop4 > 0 AND uCrsFi.pic <= uCrsFi.pvptop4, '000035', '000000')
										ENDIF
			
										
									ELSE
										
										replace uCrsDMl.fee WITH REPLICATE('0', 6 -  LEN(LEFT(astr(ROUND(uCrsFi.pvp4_fee* 100,0)),6)))  + LEFT(astr(ROUND(uCrsFi.pvp4_fee* 100,0)),6)&& pvptop4_fee

									ENDIF
									
									
									
								ENDIF
								
								
								
								
								IF EMPTY(ALLTRIM(uCrsFi.lobs3)) && se n�o for utilizada excep��o
									replace uCrsDMl.direitoopcao WITH uCrsFi.opcao && datamatrix direito opcao	
								ELSE
									IF ALLTRIM(uCrsFi.lobs3) == 'C1'
										replace uCrsDMl.direitoopcao WITH "S" && datamatrix direito opcao Excecao C1
									ELSE
										replace uCrsDMl.direitoopcao WITH "N" && datamatrix direito opcao Restantes Excecoes
									ENDIF
								ENDIF
							ENDIF
						ENDFOR
						lcContadorLinhas = lcContadorLinhas + 1
						
					*ENDFOR
					
					SELECT uCrsFi
				ENDSCAN

			CASE lcVerso = 2
				SCAN FOR uCrsFi.U_ETTENT2 > 0 AND ALLTRIM(uCrsFi.ftstamp) == ALLTRIM(uCrsFt.ftstamp)
					*FOR i=1 to qtt
						lcMsg = lcMsg + "??" + "?10"
						lcMsg = lcMsg + "??" + astr(lcContadorLinhas) + ") " + ALLTRIM(substr(uCrsFi.DESIGN,1,50)) + IIF(!EMPTY(uCrsFi.u_codemb)," -Cod. Emb.:" + uCrsFi.u_codemb,"")
						lcMsg = lcMsg + "??" + "?27" + "?51" + "?20"
						lcMsg = lcMsg + "??" + "?10"
						lcMsg = lcMsg + "??" + TRANSFORM(uCrsFi.U_EPVP,"9999999.99")
						lcMsg = lcMsg + "??" + TRANSFORM(uCrsFi.U_EPREF,"99999999.99 ")
						lcMsg = lcMsg + "??" + "  " + astr(uCrsFi.qtt)
						lcMsg = lcMsg + "??" + "    " + ALLTRIM(TRANSFORM(ROUND((uCrsFi.U_ETTENT2/uCrsFi.qtt),2), "99999999.99 "))
						lcMsg = lcMsg + "??" + "    " + ALLTRIM(TRANSFORM(ROUND(uCrsFi.U_ETTENT2,2), "9999999.99"))
						lcMsg = lcMsg + "??" + "    " + ALLTRIM(TRANSFORM(ROUND((uCrsFi.U_EPVP-uCrsFi.U_ETTENT2/uCrsFi.qtt)*uCrsFi.qtt,2), "9999999.99"))
						
						
						
						IF YEAR(uCrsFt.fdata) >= 2017 AND ALLTRIM(lcCompartV2) =="SNS"
							lcMsg = lcMsg + "??" + "   " + ALLTRIM(TRANSFORM(ROUND(uCrsFi.PvpTop4,2), "9999999.99"))		
							&&lcMSg = lcMsg + "??" + "  " + IIF(uCrsFi.pvptop4 > 0 AND uCrsFi.u_epvp <= uCrsFi.pvptop4, ALLTRIM(TRANSFORM(ROUND(0.35*uCrsFi.qtt,2), "9.99")), "0.00")
							
							IF(MONTH(uCrsFt.fdata) == 1 and year(uCrsFt.fdata) == 2017  )
								IF uCrsFi.u_generico
									lcMSg = lcMsg + "??" + "  " + IIF(uCrsFi.pvptop4 > 0 AND uCrsFi.u_epvp <= uCrsFi.pvptop4, ALLTRIM(TRANSFORM(ROUND(0.35*uCrsFi.qtt,2), "9.99")), "0.00")
								ELSE
									lcMSg = lcMsg + "??" + "  " + IIF(uCrsFi.pvptop4 > 0 AND uCrsFi.pic <= uCrsFi.pvptop4, ALLTRIM(TRANSFORM(ROUND(0.35*uCrsFi.qtt,2), "9.99")), "0.00")
								ENDIF
								
							ELSE
								lcMSg = lcMsg + "??" + "  " + ALLTRIM(TRANSFORM(uCrsFi.pvp4_fee,"9.99"))
								
							ENDIF
							
						ENDIF
						lcMsg = lcMsg + "??" + "?10"
						FOR i = 1 to qtt	
							IF i == uCrsFi.qtt
								lcMsg = lcMsg + "??" + "?29" + "?102" + "?1" + "?29" + "?72" + "?2" + "?29" + "?119" + "?3" + "?29" + "?104" + "?40" + "?29" + "?107" + "?4" + "??" + SUBSTR(uCrsFi.REF,1,7) + "??" + "?0"
							ELSE
								lcMsg = lcMsg + "??" + "?29" + "?102" + "?1" + "?29" + "?72" + "?0" + "?29" + "?119" + "?3" + "?29" + "?104" + "?40" + "?29" + "?107" + "?4" + "??" + SUBSTR(uCrsFi.REF,1,7) + "??" + "?0"
								lcMsg = lcMsg + "??" + "?10"
							ENDIF
		
							&& s� adiciona caso seja inferior a 10 linhas sen�o d� erro na impress�o do verso em tal�o - o codigo dm nem sequer � necess�rio nestes casos (impress�o de verso em tal�o com receitas de mais de 10 medicamentos) -- LL 28/07/2016
							IF lcContadorLinhas < 10
								** datamatrix
								SELECT uCrsDMl
								APPEND BLANK
								replace uCrsDMl.cnp WITH REPLICATE('0',7-LEN(alltrim(uCrsFi.ref))) + ALLTRIM(uCrsFi.ref) && datamatrix cnp
								
								** datamatrix portaria
								lcDiploma_id = 0
								SELECT uCrsDplms
								GO TOP
								SCAN
									IF ALLTRIM(uCrsDplms.u_design) == ALLTRIM(uCrsFi.u_diploma)
										lcDiploma_id = uCrsDplms.diploma_id
									ENDIF
								ENDSCAN					
								
								SELECT uCrsDMl
								replace uCrsDMl.portaria WITH REPLICATE('0', 3 - LEN(astr(lcDiploma_id))) + astr(lcDiploma_id) && datamatrix portaria
								replace uCrsDMl.pvp WITH REPLICATE('0', 6 - LEN(LEFT(astr(ROUND(uCrsFi.u_epvp * 100, 0)),6))) + LEFT(astr(ROUND(uCrsFi.u_epvp * 100,0)),6) && datamatrix pvp
								replace uCrsDMl.pref WITH REPLICATE('0', 6 - LEN(LEFT(astr(ROUND(uCrsFi.u_epref * 100,0)),6))) + LEFT(astr(ROUND(uCrsFi.u_epref * 100,0)),6) && datamatrix pref
								replace uCrsDMl.comparticipacao WITH REPLICATE('0', 6 - LEN(LEFT(astr(ROUND(round(uCrsFi.u_ettent2/uCrsFi.qtt,2) * 100,0)),6))) + LEFT(astr(ROUND(round(uCrsFi.u_ettent2/uCrsFi.qtt,2) * 100,0)),6) && datamatrix comparticipacao
								replace uCrsDMl.valorutente WITH REPLICATE('0', 6 - LEN(LEFT(astr(ROUND(ROUND(uCrsFi.u_epvp-uCrsFi.u_ettent2/uCrsFi.qtt,2) * 100,0)),6))) + LEFT(astr(ROUND(ROUND(uCrsFi.u_epvp-uCrsFi.u_ettent2/uCrsFi.qtt,2) * 100,0)),6) && datamatrix valorutente
								
								IF YEAR(uCrsFt.fdata) >= 2017  AND ALLTRIM(lcCompartV2) =="SNS"
									
									replace uCrsDMl.p4mb WITH REPLICATE('0', 6 - LEN(LEFT(astr(ROUND(uCrsFi.pvptop4 * 100, 0)),6))) + LEFT(astr(ROUND(uCrsFi.pvptop4 * 100,0)),6) && datamatrix pvp
									&& datamatrix free
									IF(MONTH(uCrsFt.fdata) == 1 and year(uCrsFt.fdata) == 2017  )
										IF uCrsFi.u_generico
											replace uCrsDMl.fee WITH IIF(uCrsFi.pvptop4 > 0 AND uCrsFi.u_epvp <= uCrsFi.pvptop4, '000035', '000000')
										ELSE
											replace uCrsDMl.fee WITH IIF(uCrsFi.pvptop4 > 0 AND uCrsFi.pic <= uCrsFi.pvptop4, '000035', '000000')
										ENDIF
										
									ELSE
									
									   	replace uCrsDMl.fee WITH REPLICATE('0', 6 -  LEN(LEFT(astr(ROUND(uCrsFi.pvp4_fee* 100,0)),6)))  + LEFT(astr(ROUND(uCrsFi.pvp4_fee* 100,0)),6)&& pvptop4_fee
		
									ENDIF
									
									
								ENDIF
								
								
								
								


								IF EMPTY(ALLTRIM(uCrsFi.lobs3)) && se n�o for utilizada excep��o
									replace uCrsDMl.direitoopcao WITH uCrsFi.opcao && datamatrix direito opcao	
								ELSE
									IF ALLTRIM(uCrsFi.lobs3) == 'C1'
										replace uCrsDMl.direitoopcao WITH "S" && datamatrix direito opcao Excecao C1
									ELSE 
										replace uCrsDMl.direitoopcao WITH "N" && datamatrix direito opcao Restantes Excecoes
									ENDIF
								ENDIF
							ENDIF
						ENDFOR
						
						lcContadorLinhas = lcContadorLinhas + 1
						
					*ENDFOR

					SELECT uCrsFi
				ENDSCAN
				
		ENDCASE
		
		
		&& Imprime linhas que n�o est�o preenchidas
		SELECT uCrsDMl
		COUNT TO lcPosicoesDML 
		IF lcPosicoesDML < 4
			FOR i=1 TO 4-lcPosicoesDML 
				
				** datamatrix
				SELECT uCrsDMl
				APPEND BLANK
				replace cnp		 			WITH REPLICATE('0',7) && datamatrix cnp
				replace portaria 			WITH REPLICATE('0',3) && datamatrix portaria
				replace pvp 				WITH REPLICATE('0', 6) && datamatrix pvp
				replace pref				WITH REPLICATE('0', 6) && datamatrix pref
				replace comparticipacao 	WITH REPLICATE('0', 6) && datamatrix comparticipacao
				replace valorutente 		WITH REPLICATE('0', 6) && datamatrix valorutente	
				IF YEAR(uCrsFt.fdata) >= 2017
					replace uCrsDMl.p4mb	WITH REPLICATE('0', 6) && datamatrix pvptop4
					replace uCrsDMl.fee		WITH REPLICATE('0', 6) && datamatrix fee
				ENDIF
				replace direitoopcao 		WITH '0'
			ENDFOR  
		ENDIF 
		
		SELECT uCrsFi
		
		lcMsg = lcMsg + "??" + "?10"
		lcMsg = lcMsg + "??" + "-------------------------------------------"

		** PREPARAR OS TOTAIS
		DO CASE
			CASE lcVerso = 1
				calculate sum(uCrsFi.u_epvp*uCrsFi.qtt) for uCrsFi.u_ettent1>0 AND ALLTRIM(uCrsFi.ftstamp) = ALLTRIM(uCrsFt.ftstamp) to totalPvp
				calculate sum(uCrsFi.qtt) for uCrsFi.u_ettent1>0 AND ALLTRIM(uCrsFi.ftstamp) = ALLTRIM(uCrsFt.ftstamp) to totalQtt
				calculate sum(uCrsFi.u_ettent1) for uCrsFi.u_ettent1>0 AND ALLTRIM(uCrsFi.ftstamp) = ALLTRIM(uCrsFt.ftstamp) to totalComp
				calculate sum(uCrsFi.u_epvp*uCrsFi.qtt-uCrsFi.u_ettent1) FOR uCrsFi.u_ettent1>0 AND ALLTRIM(uCrsFi.ftstamp) = ALLTRIM(uCrsFt.ftstamp) to totalUtente
				
				IF YEAR(uCrsFt.fdata) >= 2017  AND ALLTRIM(lcCompartV1)=="SNS"
					IF MONTH(uCrsFt.fdata)==1 AND year(uCrsFt.fdata) == 2017
						calculate sum(0.35*uCrsfi.qtt) FOR uCrsFi.pvpTop4 > 0 AND ((uCrsFi.u_generico=.t. AND uCrsFi.u_epvp <= uCrsFi.pvptop4) OR (uCrsFi.u_generico=.f. AND uCrsFi.pic <= uCrsFi.pvptop4)) to lcTotalFee
					ELSE
						calculate sum(uCrsFi.pvp4_fee) FOR uCrsFi.u_ettent1>0 AND ALLTRIM(uCrsFi.ftstamp) = ALLTRIM(uCrsFt.ftstamp) to lcTotalFee
					ENDIF
				ENDIF
				
				
			CASE lcVerso = 2
				calculate sum(uCrsFi.u_epvp*uCrsFi.qtt) for uCrsFi.u_ettent2>0 AND ALLTRIM(uCrsFi.ftstamp) = ALLTRIM(uCrsFt.ftstamp) to totalPvp
				calculate sum(uCrsFi.qtt) for uCrsFi.u_ettent2>0 AND ALLTRIM(uCrsFi.ftstamp) = ALLTRIM(uCrsFt.ftstamp) to totalQtt
				calculate sum(uCrsFi.u_ettent2) for uCrsFi.u_ettent2>0 AND ALLTRIM(uCrsFi.ftstamp) = ALLTRIM(uCrsFt.ftstamp) to totalComp
				calculate sum(uCrsFi.u_epvp*uCrsFi.qtt-uCrsFi.u_ettent2) FOR uCrsFi.u_ettent2>0 AND ALLTRIM(uCrsFi.ftstamp) = ALLTRIM(uCrsFt.ftstamp) to totalUtente
				IF YEAR(uCrsFt.fdata) >= 2017  AND ALLTRIM(lcCompartV2)=="SNS"
					IF MONTH(uCrsFt.fdata)==1 AND year(uCrsFt.fdata) == 2017 
						calculate sum(0.35*uCrsfi.qtt) FOR uCrsFi.pvpTop4 > 0 AND ((uCrsFi.u_generico=.t. AND uCrsFi.u_epvp <= uCrsFi.pvptop4) OR (uCrsFi.u_generico=.f. AND uCrsFi.pic <= uCrsFi.pvptop4)) to lcTotalFee
					ELSE
				
						calculate sum(uCrsFi.pvp4_fee) FOR uCrsFi.u_ettent2>0 AND ALLTRIM(uCrsFi.ftstamp) = ALLTRIM(uCrsFt.ftstamp) to lcTotalFee	
					ENDIF
				ENDIF

		ENDCASE


		&& datamatrix free

		
		***** IMPRESS�O DO RODAP� *******
		lcMsg = lcMsg + "??" + "?27" + "?51" + "?25"
		lcMsg = lcMsg + "??" + "?10"
		lcMsg = lcMsg + "??" + "Total Euros"
		lcMsg = lcMsg + "??" + "?10"
		lcMsg = lcMsg + "??" + TRANSFORM(ROUND(totalPvp,2),"######.##") + "      "
		lcMsg = lcMsg + "??" + TRANSFORM(ROUND(totalQtt,2),"####")
		lcMsg = lcMsg + "??" + TRANSFORM(ROUND(totalComp,2),"######.##")
		lcMsg = lcMsg + "??" + TRANSFORM(ROUND(totalUtente,2),"########.##") + "       "
		IF YEAR(uCrsFt.fdata) >= 2017
			lcMsg = lcMsg + "??" + TRANSFORM(ROUND(lcTotalFee,2),"########.##")	
		ENDIF
		

		IF VARTYPE(UsaManCompart)!="U"
			IF !UsaManCompart 
				dm_totalutenteliquido = REPLICATE('0',6-LEN(astr(round(totalUtente,2) * 100))) + astr(round(totalUtente,2) * 100) && datamatrix total utente iliquido
			ELSE
				dm_totalutenteliquido = ''
			ENDIF 
		ELSE
			dm_totalutenteliquido = REPLICATE('0',6-LEN(astr(round(totalUtente,2) * 100))) + astr(round(totalUtente,2) * 100) && datamatrix total utente iliquido
		ENDIF 

		lcMsg = lcMsg + "??" + "?10"
		lcMsg = lcMsg + "??" + "Declaro que me foram dispensadas as " + alltrim(TRANSFORM(totalQtt,"###")) + " embalagens"
		lcMsg = lcMsg + "??" + "?10"
		lcMsg = lcMsg + "??" + "de medicamentos constantes da receita e prestados"
		lcMsg = lcMsg + "??" + "?10"
		lcMsg = lcMsg + "??" + "os conselhos e informacoes sobre a sua utilizacao."

		if uf_gerais_getdate(uCrsFt.fdata,"SQL") > "20130331"
		
			** DIREITO DE OP��O **
			LOCAL exerciDireitoOpcao, lcLinhaOpcao, lcContLinha
			STORE .f. TO exerciDireitoOpcao
			STORE '' TO lcLinhaOpcao
			*** Excep��o ***
			LOCAL nLinhaDirOpcaoC1, nLinhaDirOpcaoC2
			STORE '' TO nLinhaDirOpcaoC1, nLinhaDirOpcaoC2
			
			STORE 1 TO lcContLinha

			&& verificar se tem PVP maior que o PVP5 (pvpmaxre)
			SELECT uCrsFi
			GO TOP
			DO CASE

				CASE lcVerso == 1
					SELECT uCrsFi
					SCAN FOR uCrsFi.U_ETTENT1 > 0 AND ALLTRIM(uCrsFi.ftstamp) == ALLTRIM(uCrsFt.ftstamp)
						**FOR i=1 TO uCrsFi.qtt Alterado a 24/02/2015 devido aos agrupamentos de c�digos de barras nos versos de receita
						
							** Op��o
							** Exerci direito de opcao
							IF uCrsFi.opcao == 'S' AND EMPTY(ALLTRIM(uCrsFi.lobs3))
								exerciDireitoOpcao = .t.
								IF EMPTY(lcLinhaOpcao)
									lcLinhaOpcao = astr(lcContLinha)
								ELSE
									lcLinhaOpcao = lcLinhaOpcao + ", " + astr(lcContLinha)
								ENDIF
							ENDIF
							
							** N�o exerci direito de op��o
							IF uCrsFi.opcao == 'N' AND EMPTY(ALLTRIM(uCrsFi.lobs3))
								IF EMPTY(nLinhaDirOpcaoC2)
									nLinhaDirOpcaoC2 = astr(lcContLinha)
								ELSE
									nLinhaDirOpcaoC2 = nLinhaDirOpcaoC2 + "," + astr(lcCOntLinha)
								ENDIF
							ENDIF
												
							** Excep��o
							IF !EMPTY(ALLTRIM(uCrsFi.lobs3))
								DO CASE
								
									** exerci direito de opcao com excep��o especial
									CASE ALLTRIM(ucrsFi.lobs3) == "C1"
										IF EMPTY(nLinhaDirOpcaoC1)
											nLinhaDirOpcaoC1 = astr(lcContLinha)
										ELSE
											nLinhaDirOpcaoC1 = nLinhaDirOpcaoC1 + "," + astr(lcCOntLinha)
										ENDIF
									
									** n�o exerci direito de opcao
									CASE ALLTRIM(ucrsFi.lobs3) == "C2" OR ALLTRIM(ucrsFi.lobs3) == "A" OR ALLTRIM(ucrsFi.lobs3) == "B"
										IF EMPTY(nLinhaDirOpcaoC2)
											nLinhaDirOpcaoC2 = astr(lcContLinha)
										ELSE
											nLinhaDirOpcaoC2 = nLinhaDirOpcaoC2 + "," + astr(lcCOntLinha)
										ENDIF
								ENDCASE
							ENDIF 
						
								lcContLinha = lcContLinha + 1
						**ENDFOR
					ENDSCAN

				CASE lcVerso == 2

					SELECT uCrsFi
					SCAN FOR uCrsFi.U_ETTENT2 > 0 AND ALLTRIM(uCrsFi.ftstamp) == ALLTRIM(uCrsFt.ftstamp)
						**FOR i=1 TO uCrsFi.qtt
							** Op��o
							** Exerci direito de opcao
							IF uCrsFi.opcao == 'S' AND EMPTY(ALLTRIM(uCrsFi.lobs3))
								exerciDireitoOpcao = .t.
								IF EMPTY(lcLinhaOpcao)
									lcLinhaOpcao = astr(lcContLinha)
								ELSE
									lcLinhaOpcao = lcLinhaOpcao + ", " + astr(lcContLinha)
								ENDIF
							ENDIF
							
							** N�o exerci direito de op��o
							IF uCrsFi.opcao == 'N' AND EMPTY(ALLTRIM(uCrsFi.lobs3))
								IF EMPTY(nLinhaDirOpcaoC2)
									nLinhaDirOpcaoC2 = astr(lcContLinha)
								ELSE
									nLinhaDirOpcaoC2 = nLinhaDirOpcaoC2 + "," + astr(lcCOntLinha)
								ENDIF
							ENDIF
													
							** Excep��o
							IF !EMPTY(ALLTRIM(uCrsFi.lobs3))
								DO CASE
								
									** exerci direito de opcao com excep��o especial
									CASE ALLTRIM(ucrsFi.lobs3) == "C1"
										IF EMPTY(nLinhaDirOpcaoC1)
											nLinhaDirOpcaoC1 = astr(lcContLinha)
										ELSE
											nLinhaDirOpcaoC1 = nLinhaDirOpcaoC1 + "," + astr(lcCOntLinha)
										ENDIF
									
									** n�o exerci direito de opcao
									CASE ALLTRIM(ucrsFi.lobs3) == "C2" OR ALLTRIM(ucrsFi.lobs3) == "A" OR ALLTRIM(ucrsFi.lobs3) == "B"
										IF EMPTY(nLinhaDirOpcaoC2)
											nLinhaDirOpcaoC2 = astr(lcContLinha)
										ELSE
											nLinhaDirOpcaoC2 = nLinhaDirOpcaoC2 + "," + astr(lcCOntLinha)
										ENDIF
								ENDCASE
							ENDIF 
							
							lcContLinha = lcContLinha + 1
						**ENDFOR
					ENDSCAN
			ENDCASE

			IF !EMPTY(lcLinhaOpcao)
				lcLinhaOpcao = lcLinhaOpcao + ')'
			ENDIF
			IF !EMPTY(nLinhaDirOpcaoC1)
				nLinhaDirOpcaoC1  = nLinhaDirOpcaoC1 + ')'
			ENDIF
			IF !EMPTY(nLinhaDirOpcaoC2)
				nLinhaDirOpcaoC2  = nLinhaDirOpcaoC2 + ')'
			ENDIF
			
			IF exerciDireitoOpcao
				lcMsg = lcMsg + "??" + "?10" + "?10"
				lcMsg = lcMsg + "??" + lcLinhaOpcao + " - Direito de Opcao: exerci o direito de opcao "
				lcMsg = lcMsg + "??" + "?10"
				lcMsg = lcMsg + "??" + "para medicamento com preco superior ao 5 mais barato."
			ENDIF
			
			IF !EMPTY(ALLTRIM(nLinhaDirOpcaoC1))
				lcMsg = lcMsg + "??" + "?10" + "?10"
				lcMsg = lcMsg + "??" + nLinhaDirOpcaoC1 + " - Direito de Opcao: exerci o direito de opcao "
				lcMsg = lcMsg + "??" + "?10"
				lcMsg = lcMsg + "??" + "por medicamento mais barato que o prescrito para "
				lcMsg = lcMsg + "??" + "?10"
				lcMsg = lcMsg + "??" + "continuidade terapeutica de tratamento superior a "
				lcMsg = lcMsg + "??" + "?10"
				lcMsg = lcMsg + "??" + "28 dias."
			ENDIF

			IF !EMPTY(ALLTRIM(nLinhaDirOpcaoC2))
				lcMsg = lcMsg + "??" + "?10" + "?10"
				lcMsg = lcMsg + "??" + nLinhaDirOpcaoC2 + " - Direito de Opcao: nao exerci direito de opcao."
			ENDIF
			
			***************************
		ENDIF
		lcMsg = lcMsg + "??" + "?10" + "?10"
		lcMsg = lcMsg + "??" + "O Utente ________________________________________"
		lcMsg = lcMsg + "??" + "?10" + "?10"
		**lcMsg = lcMsg + "??" + "?10" + "?10"

		IF uf_gerais_getdate(uCrsFt.fdata,"SQL") <= "20130331"
			lcMsg = lcMsg + "??" + "?10" + "?10"
			lcMsg = lcMsg + "??" + "Direito de opcao ________________________________"
			lcMsg = lcMsg + "??" + "?10" + "?10"
			**lcMsg = lcMsg + "??" + "?10" + "?10"
		ENDIF
		

        IF EMPTY(ucrsFT2.token)
		    lcMsg = lcMsg + "??" + "?10"
		    lcMsg = lcMsg + "??" + "?27" + "?100" + "?7"
		    lcMsg = lcMsg + "??" + "?29" + "?86" + "?48"
        ENDIF

		** DataMatrix **
		LOCAL lcDM
		lcDM = ""

		SELECT uCrsE1
		GO TOP

		** vers�o
		lcDM = lcDM + '104'
		
		** c�digo farm�cia
		lcDM = lcDM + REPLICATE('0',6-LEN(ASTR(uCrsE1.u_infarmed))) + ASTR(uCrsE1.u_infarmed)
		
		** codigo entidade + data + operador + serie + lote + posicao lote + numero venda + numero receita 
		lcDM = lcDM + dm_codigoentidade + dm_data + dm_operador + dm_serie + dm_lote + dm_posicaolote + dm_nrvenda + dm_nrreceita 
		
		** campo2 + campo3 + campo4
		lcDM = lcDM	+ "000000000000" + "000000000000" + "00000000000000000000"

		** linhas
		SELECT uCrsDMl
		GO TOP
		SCAN
		

			** cnp + portaria + pvp + pref + comparticipacao + valor utente + direitoopcao
	*		lcDM = lcDM + uCrsDMl.cnp + uCrsDMl.portaria + uCrsDMl.pvp + uCrsDMl.pref + uCrsDMl.comparticipacao + uCrsDMl.valorutente + uCrsDMl.direitoopcao
		lcDM = lcDM + uCrsDMl.cnp + uCrsDMl.portaria + uCrsDMl.pvp + uCrsDMl.pref + uCrsDMl.comparticipacao + uCrsDMl.valorutente + uCrsDMl.p4mb + uCrsDMl.fee + uCrsDMl.direitoopcao 
		ENDSCAN
		fecha("uCrsDMl")

		** total utente liquido
		lcDM = lcDM + dm_totalutenteliquido
		****************

		** adicionar o datamatrix ao conte�do da receita
		** caracteres "##DM" identificam datamatrix
		lcDM = "??##DM" + lcDM
		lcMsg = STRTRAN(lcMsg, "%%1", lcDM)
			
		RETURN lcMsg

ENDFUNC 



** Verifica o valor do fee para cada medicamento

FUNCTION uf_imprimirpos_calculaFee
	LPARAMETERS  lcAbrevCompart
	LOCAL lcFee
	STORE 0 TO lcFee
	
	IF(ALLTRIM(UPPER(lcAbrevCompart))=="SNS")
		
		IF(month(uCrsFi.rdata)==1 AND year(uCrsFi.rdata)==2017)
						
			IF(uCrsFi.pvpTop4 > 0 AND ((uCrsFi.u_generico=.t. AND uCrsFi.u_epvp <= uCrsFi.pvptop4) OR (uCrsFi.u_generico=.f. AND uCrsFi.pic <= uCrsFi.pvptop4)))	
				lcFee = ROUND(0.35*uCrsFi.qtt,2)
			ELSE
				lcFee=0
			ENDIF
									
		ELSE
			lcFee=uCrsFi.pvp4_fee
		ENDIF
	ENDIF	

	RETURN lcFee	
				
ENDFUNC				


** Funcao Previs�o Verso Receita / Impress�o Verso Impressora A6 **
FUNCTION uf_imprimirpos_prevRec
	LPARAMETERS lcStamp, lcVerso, lcImpA6
	
	PUBLIC lcRef, lcQTT, lcu_Epvp, lcDesign, lcu_ettent1, lcu_Epref, lcu_lordem, myVersoRec, myOpCode, totalPvp, totalqtt, totalComp, totalUtente, lcTotalFee 
	PUBLIC myDirOpcaoText
	LOCAL lcCod, lcCod2, lcAbrev, lcAbrev2, imprimeVerso1, imprimeVerso2, lcPsico
	STORE 0 TO totalPvp, totalqtt, totalComp, totalUtente, lcTotalFee, lcQTT
	STORE '' TO myDirOpcaoText, lcAbrev, lcAbrev2
	
	myVersoRec = lcVerso
	
	**Necess�rio actualizar dados dos cursor para a impressoa estar com dados actualizados, existem updates, lotes e tipo de lote directamente � tabela FT
	uf_gerais_actGrelha("",[uCrsFt],[exec up_touch_ft ']+alltrim(lcStamp)+['])
	Select uCrsFt
	uf_gerais_actGrelha("",[uCrsFt2],[exec up_touch_ft2 ']+alltrim(lcStamp)+['])
	Select uCrsFt2
	uf_gerais_actGrelha("",[uCrsFi],[exec up_touch_fi ']+alltrim(lcStamp)+[', '', 0])

	IF !uf_imprimirpos_validaSeImprimeVersoReceita(lcVerso)
		RETURN .F.
	ENDIF

	Select uCrsFi
	lcCod = uCrsFt2.u_codigo
	lcCod2 = uCrsFt2.u_codigo2
	lcAbrev = uCrsFt2.u_abrev
	lcAbrev2 = uCrsFt2.u_abrev2
	STORE 0 TO imprimeVerso1, imprimeVerso2, lcPsico
	
	SELECT uCrsFi
	GO TOP
	SCAN
		IF u_ettent1>0
			imprimeVerso1 = imprimeVerso1 + uCrsFi.qtt
		ENDIF
		IF u_ettent2>0
			imprimeVerso2 = imprimeVerso2 + uCrsFi.qtt
		ENDIF
		IF u_psico == .t.
			lcPsico = 1
		ENDIF
	ENDSCAN
	

	** VERIFICA SE DEVE IMPRIMIR O C�DIGO ADICIONAL DO OPERADOR EM VEZ DO C�DIGO PRINCIPAL **
	LOCAL lcOpCode
	STORE 0 TO lcOpCode
	
	IF uf_gerais_getParameter("ADM0000000047","BOOL")
		lcOpCode = myOpAltCod
	ELSE
		lcOpCode = uCrsFt.vendedor
	ENDIF 
	
	IF lcOpCode==0
		myOpCode = Alltrim(Str(uCrsFt.vendedor))
	ELSE
		myOpCode = Alltrim(Str(lcOpCode))
	ENDIF

	
	** DIREITO DE OP��O **
		LOCAL exerciDireitoOpcao, lcLinhaOpcao, lcContLinha
		STORE .f. TO exerciDireitoOpcao
		STORE '' TO lcLinhaOpcao
		
		*** Excep��o ***
		LOCAL nLinhaDirOpcaoC1, nLinhaDirOpcaoC2
		STORE '' TO nLinhaDirOpcaoC1, nLinhaDirOpcaoC2
		STORE 1 TO lcContLinha

		&& verificar se tem PVP maior que o PVP5 (pvpmaxre)
		SELECT uCrsFi
		GO TOP
		DO CASE

			CASE lcVerso == 1
				SCAN FOR uCrsFi.U_ETTENT1 > 0 AND ALLTRIM(uCrsFi.ftstamp) == ALLTRIM(uCrsFt.ftstamp)
					FOR i=1 TO uCrsFi.qtt
					
						** Op��o
						** Exerci direito de opcao
						IF uCrsFi.opcao == 'S' AND EMPTY(ALLTRIM(uCrsFi.lobs3))
							exerciDireitoOpcao = .t.
							IF EMPTY(lcLinhaOpcao)
								lcLinhaOpcao = astr(lcContLinha)
							ELSE
								lcLinhaOpcao = lcLinhaOpcao + ", " + astr(lcContLinha)
							ENDIF
						ENDIF
						
						** N�o exerci direito de op��o
						IF uCrsFi.opcao == 'N' AND EMPTY(ALLTRIM(uCrsFi.lobs3))
							IF EMPTY(nLinhaDirOpcaoC2)
								nLinhaDirOpcaoC2 = astr(lcContLinha)
							ELSE
								nLinhaDirOpcaoC2 = nLinhaDirOpcaoC2 + "," + astr(lcCOntLinha)
							ENDIF
						ENDIF
			
						** Excep��o
						IF !EMPTY(ALLTRIM(uCrsFi.lobs3))
							DO CASE
							
								** Exerci direito de opcao com excep��o especial
								CASE ALLTRIM(ucrsFi.lobs3) == "C1"
									IF EMPTY(nLinhaDirOpcaoC1)
										nLinhaDirOpcaoC1 = astr(lcContLinha)
									ELSE
										nLinhaDirOpcaoC1 = nLinhaDirOpcaoC1 + "," + astr(lcCOntLinha)
									ENDIF
								
								** N�o exerci direito de opcao
								CASE ALLTRIM(ucrsFi.lobs3) == "C2" OR ALLTRIM(ucrsFi.lobs3) == "A" OR ALLTRIM(ucrsFi.lobs3) == "B"
									IF EMPTY(nLinhaDirOpcaoC2)
										nLinhaDirOpcaoC2 = astr(lcContLinha)
									ELSE
										nLinhaDirOpcaoC2 = nLinhaDirOpcaoC2 + "," + astr(lcCOntLinha)
									ENDIF
							ENDCASE
						ENDIF 
					
							lcContLinha = lcContLinha + 1
							&&Add Fee
							replace pvp4_fee WITH uf_imprimirpos_calculaFee(lcAbrev) IN uCrsFi
					ENDFOR
					
					
					SELECT uCrsFi
				ENDSCAN

			CASE lcVerso == 2
				SCAN FOR uCrsFi.U_ETTENT2 > 0 AND ALLTRIM(uCrsFi.ftstamp) == ALLTRIM(uCrsFt.ftstamp)
					FOR i=1 TO uCrsFi.qtt
						** Op��o
						** Exerci direito de opcao
						IF uCrsFi.opcao == 'S' AND EMPTY(ALLTRIM(uCrsFi.lobs3))
							exerciDireitoOpcao = .t.
							IF EMPTY(lcLinhaOpcao)
								lcLinhaOpcao = astr(lcContLinha)
							ELSE
								lcLinhaOpcao = lcLinhaOpcao + ", " + astr(lcContLinha)
							ENDIF
						ENDIF
						
						** N�o exerci direito de op��o
						IF uCrsFi.opcao == 'N' AND EMPTY(ALLTRIM(uCrsFi.lobs3))
							IF EMPTY(nLinhaDirOpcaoC2)
								nLinhaDirOpcaoC2 = astr(lcContLinha)
							ELSE
								nLinhaDirOpcaoC2 = nLinhaDirOpcaoC2 + "," + astr(lcCOntLinha)
							ENDIF
						ENDIF
					
						** Excep��o
						IF !EMPTY(ALLTRIM(uCrsFi.lobs3))
							DO CASE
							
								** exerci direito de opcao com excep��o especial
								CASE ALLTRIM(ucrsFi.lobs3) == "C1"
									IF EMPTY(nLinhaDirOpcaoC1)
										nLinhaDirOpcaoC1 = astr(lcContLinha)
									ELSE
										nLinhaDirOpcaoC1 = nLinhaDirOpcaoC1 + "," + astr(lcCOntLinha)
									ENDIF
								
								** n�o exerci direito de opcao
								CASE ALLTRIM(ucrsFi.lobs3) == "C2" OR ALLTRIM(ucrsFi.lobs3) == "A" OR ALLTRIM(ucrsFi.lobs3) == "B"
									IF EMPTY(nLinhaDirOpcaoC2)
										nLinhaDirOpcaoC2 = astr(lcContLinha)
									ELSE
										nLinhaDirOpcaoC2 = nLinhaDirOpcaoC2 + "," + astr(lcCOntLinha)
									ENDIF
							ENDCASE
						ENDIF 
						
						lcContLinha = lcContLinha + 1
						
						&&Add Fee
						replace pvp4_fee WITH uf_imprimirpos_calculaFee(lcAbrev2) IN uCrsFi
						
					
					ENDFOR
					
				
					

					SELECT uCrsFi
				ENDSCAN
		ENDCASE

		IF !EMPTY(lcLinhaOpcao)
			lcLinhaOpcao = lcLinhaOpcao + ')'
		ENDIF
		IF !EMPTY(nLinhaDirOpcaoC1)
			nLinhaDirOpcaoC1  = nLinhaDirOpcaoC1 + ')'
		ENDIF
		IF !EMPTY(nLinhaDirOpcaoC2)
			nLinhaDirOpcaoC2  = nLinhaDirOpcaoC2 + ')'
		ENDIF
		
		IF exerciDireitoOpcao
			myDirOpcaoText = myDirOpcaoText + CHR(13) + lcLinhaOpcao + " - Direito de Opcao: Exerci o direito de opcao "
			myDirOpcaoText = myDirOpcaoText + "para medicamento com preco superior ao 5 mais barato."
		ENDIF
		
		IF !EMPTY(ALLTRIM(nLinhaDirOpcaoC1))
			myDirOpcaoText = myDirOpcaoText + CHR(13) + nLinhaDirOpcaoC1 + " - Direito de Opcao: Exerci o direito de opcao "
			myDirOpcaoText = myDirOpcaoText + "por medicamento mais barato que o prescrito para "
			myDirOpcaoText = myDirOpcaoText + "continuidade terapeutica de tratamento superior a "
			myDirOpcaoText = myDirOpcaoText + "28 dias."
		ENDIF

		IF !EMPTY(ALLTRIM(nLinhaDirOpcaoC2))
			myDirOpcaoText = myDirOpcaoText + CHR(13) + nLinhaDirOpcaoC2 + " - Direito de Opcao: nao exerci direito de opcao."
		ENDIF
	**
	
	DO CASE 
		CASE lcVerso == 1
			IF !EMPTY(lcCod)
				If imprimeVerso1 > 0
				
					***PREPARAR DADOS PARA O VERSO DE RECEITA 1
					Select uCrsFt
					uf_gerais_actGrelha("","T","SELECT convert(varchar(3),loteId) as tlote FROM ctltrct (nolock) inner join tlote(nolock) on tlote.codigo=ctltrct.tlote WHERE ctltrctstamp='" + ALLTRIM(uCrsFt.u_ltstamp) + "'")
	   		    	uf_gerais_actGrelha("","T2","SELECT convert(varchar(3),loteId) as tlote FROM ctltrct (nolock) inner join tlote(nolock) on tlote.codigo=ctltrct.tlote WHERE ctltrctstamp=(select u_ltstamp2 from ft (nolock) where ftstamp='"+ALLTRIM(uCrsFt.ftstamp)+"')")

					uf_gerais_actGrelha("","L","SELECT convert(varchar(3),lote) as lote FROM ctltrct (nolock) WHERE ctltrctstamp='" + ALLTRIM(uCrsFt.u_ltstamp) + "'")
					uf_gerais_actGrelha("","R","SELECT convert(varchar(5),nreceita) as nreceita FROM ctltrct (nolock) WHERE ctltrctstamp='" + ALLTRIM(uCrsFt.u_ltstamp) + "'")
					uf_gerais_actGrelha("","S","SELECT convert(varchar(3),slote) as slote FROM ctltrct (nolock) WHERE ctltrctstamp='" + ALLTRIM(uCrsFt.u_ltstamp) + "'")
					
					**:TODO
					SELECT uCrsFi
					calculate sum(uCrsFi.pvp4_fee) for uCrsFi.u_ettent1>0 to lcTotalFee
					
					IF !uf_gerais_getParameter("ADM0000000126","BOOL")
						uf_imprimirpos_prevImpTalA6()
						SELECT ucrsFt
						SELECT uCrsFt2
						SELECT uCrsFi
						DELETE FOR uCrsFi.u_ettent1 == 0 AND uCrsFi.u_ettent2 == 0
						SELECT uCrsFi
						GO TOP
						SET REPORTBEHAVIOR 90
						Report Form Alltrim(myPath)+'\analises\versorec.frx' To PRINTER PREVIEW
					ELSE				
						uf_imprimirpos_prevImpTalA6()
						IF !lcImpA6
							SELECT uCrsFi
							DELETE FOR uCrsFi.u_ettent1 == 0 AND uCrsFi.u_ettent2 == 0
							GO TOP
							
							**
							uf_imprimirgerais_dataMatrixCriaImagem(0, 0,lcVerso)
							
							SET REPORTBEHAVIOR 80
							SELECT uCrsFi
							GO Top
							Report Form Alltrim(myPath)+'\analises\versorecA6.frx' To PRINTER PREVIEW
						ELSE
							SELECT uCrsE1
							GO TOP 
							SELECT uCrsFT
							GO TOP
							SELECT uCrsFi
							DELETE FOR uCrsFi.u_ettent1 == 0 AND uCrsFi.u_ettent2 == 0
							GO TOP
							
							**
							uf_imprimirgerais_dataMatrixCriaImagem(0, 0,lcVerso)
							
							SET REPORTBEHAVIOR 80
							SELECT uCrsFi
							GO Top
							Report Form Alltrim(myPath)+'\analises\versorecA6.frx' TO PRINTER NOCONSOLE
						ENDIF
					ENDIF

				ELSE
					uf_perguntalt_chama("N�O EXISTEM DADOS A IMPRIMIR.","OK","",64)
				ENDIF
			ELSE 
				uf_perguntalt_chama("N�O EXISTEM DADOS A IMPRIMIR.","OK","",64)
			ENDIF
			
		CASE lcVerso == 2
		
			IF !EMPTY(lcCod2)
				If imprimeVerso2 > 0
					***PREPARAR DADOS PARA O VERSO DE RECEITA 1
					uf_gerais_actGrelha("","T","SELECT convert(varchar(3),loteId) as tlote FROM ctltrct (nolock) inner join tlote(nolock) on tlote.codigo=ctltrct.tlote  WHERE ctltrctstamp='" + ALLTRIM(uCrsFt.u_ltstamp2) + "'")
	   		    	uf_gerais_actGrelha("","T2","SELECT convert(varchar(3),loteId) as tlote FROM ctltrct (nolock) inner join tlote(nolock) on tlote.codigo=ctltrct.tlote   WHERE ctltrctstamp=(select u_ltstamp2 from ft (nolock) where ftstamp='"+ALLTRIM(uCrsFt.ftstamp)+"')")

					uf_gerais_actGrelha("","L","SELECT convert(varchar(3),lote) as lote FROM ctltrct (nolock) WHERE ctltrctstamp='" + ALLTRIM(uCrsFt.u_ltstamp2) + "'")
					uf_gerais_actGrelha("","R","SELECT convert(varchar(5),nreceita) as nreceita FROM ctltrct (nolock) WHERE ctltrctstamp='" + ALLTRIM(uCrsFt.u_ltstamp2) + "'")
					uf_gerais_actGrelha("","S","SELECT convert(varchar(3),slote) as slote FROM ctltrct (nolock) WHERE ctltrctstamp='" + ALLTRIM(uCrsFt.u_ltstamp2) + "'")
					**:TODO
					SELECT uCrsFi
					calculate sum(uCrsFi.pvp4_fee) for uCrsFi.u_ettent1>0 to lcTotalFee
					 
					IF !uf_gerais_getParameter("ADM0000000126","BOOL")
						uf_imprimirpos_prevImpTalA6()
						SELECT uCrsFi
						DELETE FOR uCrsFi.u_ettent1 == 0 AND uCrsFi.u_ettent2 == 0
						SELECT uCrsFi
						GO TOP
						
						SET REPORTBEHAVIOR 90
						SELECT uCrsFi
						GO TOP
						
						Report Form Alltrim(myPath)+'\analises\versorec.frx' To PRINTER PREVIEW
					ELSE
						uf_imprimirpos_prevImpTalA6()
						IF !lcImpA6
							SELECT uCrsFi
							DELETE FOR uCrsFi.u_ettent1 == 0 AND uCrsFi.u_ettent2 == 0
							GO TOP
							
							uf_imprimirgerais_dataMatrixCriaImagem(0, 0,lcVerso)
						
							SET REPORTBEHAVIOR 80
							SELECT uCrsFi
							GO Top
							
							Report Form Alltrim(myPath)+'\analises\versorecA6.frx' To PRINTER PREVIEW
						ELSE
							SELECT uCrsE1
							GO TOP 
							SELECT uCrsFT
							GO TOP
							SELECT uCrsFi
							DELETE FOR uCrsFi.u_ettent1 == 0 AND uCrsFi.u_ettent2 == 0
							GO TOP
							
							uf_imprimirgerais_dataMatrixCriaImagem(0, 0,lcVerso)
							
							SET REPORTBEHAVIOR 80
							
							SELECT uCrsFi
							GO Top
							
							Report Form Alltrim(myPath)+'\analises\versorecA6.frx' TO PRINTER NOCONSOLE
						ENDIF
					ENDIF
				ELSE
					uf_perguntalt_chama("N�O EXISTEM DADOS A IMPRIMIR.","OK","",64)
				ENDIF
			ELSE 
				uf_perguntalt_chama("N�O EXISTEM DADOS A IMPRIMIR.","OK","",64)
			ENDIF
		OTHERWISE
			uf_perguntalt_chama("PAR�METRO DE VERSO DA RECEITA INV�LIDO","OK","",48)
	ENDCASE 
ENDFUNC 


**
FUNCTION uf_imprimirpos_prevImpTalA6
	LOCAL lcFtstamp,lcopcao

	** PREPARAR OS TOTAIS
	SELECT uCrsFi
	calculate sum(uCrsFi.u_epvp*uCrsFi.qtt) for uCrsFi.u_ettent1>0 to totalPvp
	calculate sum(uCrsFi.qtt) for uCrsFi.u_ettent1>0 to totalqtt
	calculate sum(uCrsFi.u_ettent1) for uCrsFi.u_ettent1>0 to totalComp
	calculate sum(uCrsFi.u_epvp*uCrsFi.qtt-uCrsFi.u_ettent1) for uCrsFi.u_ettent1>0 to totalUtente

	SELECT uCrsFi
	GO TOP
	SCAN
		If uCrsFi.qtt>1 AND uCrsFi.u_ettent1>0
			lcRef		= uCrsFi.ref
			lcQTT		= uCrsFi.qtt
			lcu_Epvp	= uCrsFi.u_Epvp
			lcDesign    = uCrsFi.Design    
			lcu_ettent1 = Round(uCrsFi.u_ettent1/lcQTT,2)
			lcu_Epref   = uCrsFi.u_epref
			lcu_lordem	= uCrsFi.lordem + 1
			
			replace uCrsFi.qtt			With 1
			replace uCrsFi.u_Epvp		With lcu_Epvp
			replace uCrsFi.u_ettent1	With lcu_ettent1 
			
			lcFtstamp = uCrsFi.ftstamp
			lcopcao = uCrsFi.opcao
			
			** faltam calculos
			lcPos=RECNO("uCrsFi")
			For I=1 to lcQTT-1
				APPEND Blank
				replace uCrsFi.ftstamp	 With lcFtstamp 
				replace uCrsFi.qtt		 With 1
				replace uCRsFi.Ref 		 With lcRef
				replace uCRsFi.Design	 With lcDesign
				replace uCrsFi.u_Epvp 	 With lcu_Epvp
				replace uCrsFi.u_ettent1 With lcu_ettent1 
				replace uCrsFi.u_epref	 With lcu_epref
				replace uCrsFi.lordem	 WITH lcu_lordem  
				replace uCrsFi.opcao 	 WITH lcopcao
			ENDFOR
			SELECT uCrsFi
			try
				GO lcPos
			CATCH
				***
			ENDTRY
		ENDIF
	ENDSCAN

	** VERIFICAR SE � NECESS�RIO IMPRIMIR OS DADOS DO ADQUIRENTE, PSICOTR�PICOS **
	SELECT uCrsFt
	GO TOP
	IF uf_gerais_actGrelha("",[uCrsReImpAdqPsico],[select top 1 * from B_dadosPsico (nolock) where ftstamp=']+alltrim(uCrsFt.ftstamp)+['])
		***
	ENDIF
	***********************************************
	
	** cursores e variaveis para o data-matrix (cabe�alho e linhas)
	LOCAL dm_codigofarmacia, dm_codigoentidade, dm_data, dm_operador, dm_serie, dm_lote, dm_posicaolote, dm_nrvenda, dm_nrreceita, dm_campo2, dm_campo3, dm_campo4, dm_totalutenteliquido
	STORE "" TO dm_codigofarmacia, dm_codigoentidade, dm_data, dm_operador, dm_serie, dm_lote, dm_posicaolote, dm_nrvenda, dm_nrreceita, dm_campo2, dm_campo3, dm_campo4, dm_totalutenteliquido
	CREATE CURSOR uCrsDMl(cnp c(7), portaria c(3), pvp c(6), pref c(6), comparticipacao c(6), valorutente c(6), direitoopcao c(1))
	
	SELECT uCrsFt2
	GO TOP
	SELECT uCrsFt
	GO TOP
	SELECT uCrsFi
	Index on lordem Tag lordem
	SELECT uCrsFi
	GO TOP
	
ENDFUNC 


** Previs�o / Impress�o Tal�o Psicotr�picos
FUNCTION uf_imprimirpos_ImpPrevPsico
	LPARAMETERS lcStamp, lcPrev, lcPainel

	IF EMPTY(lcStamp)
		RETURN .F.
	ENDIF
		
		IF UPPER(ALLTRIM(lcPainel))!="ATENDIMENTO" OR !USED("uCrsFi") OR !USED("uCrsFt") OR !USED("uCrsFt2") OR !USED("uCrsFi2")
			uf_gerais_actGrelha("",[uCrsFt],[exec up_touch_ft ']+alltrim(lcStamp)+['])
			Select uCrsFt
			uf_gerais_actGrelha("",[uCrsFt2],[exec up_touch_ft2 ']+alltrim(lcStamp)+['])
			Select uCrsFt2
			uf_gerais_actGrelha("",[uCrsFi],[exec up_touch_fi ']+alltrim(lcStamp)+[', '', 0])
			Select uCrsFi
			uf_gerais_actGrelha("",[uCrsFi2],[exec up_touch_fi2 ']+alltrim(lcStamp)+[', ''])
			select uCrsFi2
		ENDIF 
		
		Local lcPsico
		lcPsico=0
		
		SELECT uCrsFi
		GO TOP
		SCAN FOR ALLTRIM(UPPER(uCrsFi.ftstamp)) = ALLTRIM(UPPER(lcStamp)) AND uCrsFi.u_psico=.t.
			lcPsico = lcPsico + 1
		ENDSCAN

		** Valida��o para n�o imprimir tal�es de psicotr�picos repetidos 2015/01/19
		IF UPPER(ALLTRIM(lcPainel)) != "REIMP"
			IF !USED("uCrsValidaImpPsico")
				CREATE CURSOR uCrsValidaImpPsico (stamp c(25))
			ENDIF
			SELECT uCrsValidaImpPsico 
			LOCATE FOR ALLTRIM(UPPER(lcStamp)) == ALLTRIM(UPPER(uCrsValidaImpPsico.stamp))
			IF FOUND ()
				RETURN .f.
			ENDIF
			SELECT uCrsValidaImpPsico
			APPEND BLANK
			REPLACE uCrsValidaImpPsico.stamp 	WITH lcStamp
			SELECT uCrsValidaImpPsico
		ENDIF
		**

		IF lcPsico>0
		
			LOCAL lcOpCode
			STORE 0 TO lcOpCode

			SELECT uCrsFt
			LOCATE FOR ALLTRIM(uCrsFt.ftstamp) = ALLTRIM(lcStamp)
			IF uf_gerais_getParameter("ADM0000000047","BOOL")
				lcOpCode = myOpAltCod
			ELSE
				lcOpCode = uCrsFt.vendedor
			ENDIF 
				
			uf_gerais_actGrelha("",[uCrsReImpPsico],[select * from B_dadosPsico (nolock) where ftstamp=']+alltrim(lcStamp)+['])
			Select uCrsReImpPsico
			
            **IF uf_gerais_getParameter_site('ADM0000000136', 'BOOL', mySite)
            IF uf_gerais_getUmValor("B_terminal", "usaDriverWin", "terminal = '" + ALLTRIM(myTerm) + "'")


                uv_idImp = uf_gerais_getUmValor("B_impressoes", "id", "tabela = 'FT' and nomeImpressao = 'Tal�o Psicotr�picos'")

                IF !uf_imprimirgerais_createCursTalao(uv_idImp, lcstamp)
                    uf_perguntalt_chama("ERRO A GERAR CURSORES DE IMPRESS�ES. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
                    RETURN .f.
                ENDIF

				    lcNrTpsico = IIF(UPPER(ALLTRIM(lcPainel))!="ATENDIMENTO", 1, uf_gerais_getParameter("ADM0000000018","NUM"))

                For i = 1 to lcNrTpsico

                    uf_imprimirgerais_sendprinter_talao(uv_idImp , .f., lcPrev)

                ENDFOR

            ELSE
                IF !uf_gerais_getparameter("ADM0000000126","BOOL") AND lcPrev = .f. AND RECCOUNT("uCrsReImpPsico")>0&&imprime tal�o psico
                    && PREPARAR DEFINI��ES GERAIS DA IMPRESSORA
                    lcValidaImp = uf_gerais_setImpressoraPOS(.t.)
                    
                    IF lcValidaImp
                        *** INICIALIZAR A IMPRESSORA ***
                        ???	chr(027)+chr(064)
                        *** ESCOLHER A IMPRESSORA ***
                        ???	chr(27)+chr(99)+chr(48)+chr(1)
                        *** DEFINIR C�DIGO DE CARACTERES PORTUGU�S ***
                        ???	chr(027)+chr(116)+chr(003)
                        *** ESCOLHER MODOS DE IMPRESS�O ***
                        ???	chr(27)+chr(33)+chr(1)
                        
                        LOCAL lcNrTpsico
                        IF UPPER(ALLTRIM(lcPainel))!="ATENDIMENTO"
                            lcNrTpsico = 1
                        ELSE
                            lcNrTpsico = uf_gerais_getParameter("ADM0000000018","NUM")
                        ENDIF 	

                        For i = 1 to lcNrTpsico
                            ****** IMPRESS�O DO CABE�ALHO ******
                            *** NOME DA EMPRESA ***
                            ??	" "
                            ?	LEFT(ALLTRIM(uCrsE1.NOMECOMP),uf_gerais_getParameter("ADM0000000194","NUM"))

                            *** OUTROS DADOS DA EMPRESA ***
                            ??? chr(27)+chr(33)+chr(1)	&& print mode B
                            ?	SUBSTR(uCrsE1.local,1,10)
                            ??	" Tel:"
                            ??	TRANSFORM(uCrsE1.TELEFONE,"#########")
                            ??	" NIF:"
                            ??	TRANSFORM(uCrsE1.NCONT,"99999999999")
							? 	ALLTRIM(uf_gerais_getMacrosReports(2))
                            ?	"Dir. Tec. "
                            ??	LEFT(ALLTRIM(uCrsE1.U_DIRTEC),uf_gerais_getParameter("ADM0000000194","NUM"))
                            uf_gerais_separadorPOS()
                            ?	PADC("DOCUMENTO DE PSICOTROPICOS",uf_gerais_getParameter("ADM0000000194","NUM")," ")
                            uf_gerais_separadorPOS()
                            SELECT uCrsFt
                            LOCATE FOR ALLTRIM(uCrsFt.ftstamp) = ALLTRIM(lcStamp)
                            ?	uf_gerais_getDate(uCrsFt.FDATA,"DATA")
                            ??	"     N.Doc : "
                            ??	TRANSFORM(uCrsFt.FNO,"#########")
                            ??	"(" + TRANSFORM(uCrsFt.NDOC,"###") + ")"
                            SELECT uCrsFt2
                            LOCATE FOR ALLTRIM(uCrsFt2.ft2stamp) = ALLTRIM(lcStamp)
                            ?	"Nr. Receita: " + Substr(uCrsFt2.u_receita,1,22)
                            SELECT uCrsFt
                            LOCATE FOR ALLTRIM(uCrsFt.ftstamp) = ALLTRIM(lcStamp)
                            ?"Op: "
                            IF lcOpCode==0
                                ??Substr(uCrsFt.vendnm,1,30) + " (" + Alltrim(Str(uCrsFt.vendedor)) + ")" 
                            ELSE
                                ??Substr(uCrsFt.vendnm,1,30) + " (" + Alltrim(Str(lcOpCode)) + ")" 
                            ENDIF
                            
                            ?	" "
                            ?	"Produto            Qtt      N.Saida PSI:"
                            uf_gerais_separadorPOS()

                            ****** IMPRESS�O DAS LINHAS ******
                            SELECT uCrsFi
                            SCAN FOR ALLTRIM(uCrsFi.ftstamp) == ALLTRIM(lcStamp) AND uCrsFi.u_psico == .t.
                                ?	LEFT(ALLTRIM(uCrsFi.design),uf_gerais_getParameter("ADM0000000194","NUM"))
                                ?	SUBSTR(uCrsFi.REF,1,13) + "      " + TRANSFORM(uCrsFi.QTT,"###") + "      " + TRANSFORM(uCrsFi.U_PSICONT,"#######")
                            ENDSCAN

                            ****** IMPRESS�O DO RODAP� ******
                            uf_gerais_separadorPOS()
                            ?	"Medico : " + ALLTRIM(uCrsReImpPsico.U_MEDICO)
                            ?	"Doente : "
                            ?	"  Nome    : " + ALLTRIM(uCrsReImpPsico.U_NMUTDISP)
                            ?	"  Morada  : " + ALLTRIM(uCrsReImpPsico.U_MOUTDISP)
                            ?	"Adquirente : "
                            ?	"  Nome    : " + ALLTRIM(uCrsReImpPsico.U_NMUTAVI)
                            ?	"  Morada  : " + ALLTRIM(uCrsReImpPsico.U_MOUTAVI)
                            ?	"  Doc     : " + uCrsReImpPsico.U_NDUTAVI
                            ?	"  Data Doc: "
                            ??	IIF(YEAR(uCrsReImpPsico.U_DDUTAVI) = 3000,"CC Vitalicio", uf_gerais_getDate(uCrsReImpPsico.U_DDUTAVI,"DATA"))
                            ??	"  Idade : " + TRANSFORM(uCrsReImpPsico.U_IDUTAVI,"###")
                            
                            uf_gerais_feedCutPOS()
                        ENDFOR
                    ENDIF
                    uf_gerais_setImpressoraPOS(.f.)
                ELSE
                    PUBLIC myOpCode
                    myOpCode = lcOpCode
                    IF lcPrev = .f. AND uf_gerais_getparameter("ADM0000000126","BOOL") && imprime psico A6
                        Select uCrsReImpPsico
                        GO TOP
                        Select uCrsFi
                        GO TOP 
                        Report Form Alltrim(myPath)+'\analises\talaopsico.frx' TO PRINTER NOCONSOLE
                    ELSE
                        IF lcPrev = .t.&& preview psico
                            Select uCrsReImpPsico
                            GO TOP
                            Select uCrsFi
                            GO TOP
                            
                            SET REPORTBEHAVIOR 90
                            Report Form Alltrim(myPath)+'\analises\talaopsico.frx' To Printer PREVIEW
                            SET REPORTBEHAVIOR 80
                        ENDIF 
                    ENDIF 
                ENDIF 
            ENDIF
		ELSE
			IF UPPER(ALLTRIM(lcPainel))!="ATENDIMENTO"
				uf_perguntalt_chama("N�O EXISTEM DADOS PARA IMPRIMIR.","OK","",64)
			ENDIF 
		ENDIF
ENDFUNC 


** Funcao Imprimir Tal�o Conjunto - Impress�o final do Pagamento
FUNCTION uf_imprimirpos_impTalaoConjuntoPos

	LPARAMETERS nrTaloes, lcTroco, lcTotal, lcModoPag1, lcModoPag2, lcModoPag3, lcModoPag4, lcModoPag5, lcModoPag6, lcModoPag7, lcModoPag8
	LOCAL lcSQL 

	LOCAL lcStamp, lcValidaImp, lcPsico, lcIsentoIva, lcImpCapSocial, lcCountVendasNSusp, lcMoeda, lcMotIsencaoIva , lcRepete, lcVdNormal
	STORE '' TO lcStamp
	STORE .f. TO lcValidaImp, lcImpCapSocial, lcMotIsencaoIva 
	STORE 0 TO lcPsico, myNrVendasTalao, lcCountVendasNSusp
	STORE 'EURO' TO lcMoeda
	STORE 1 TO lcRepete
	STORE .t. TO lcVdNormal

	&& verifica se deve imprimir com que simbolo - Euro // USD 
	lcMoeda = uf_gerais_getParameter("ADM0000000260","text")	
	IF EMPTY(lcMoeda)
		lcMoeda = 'EURO'
	ENDIF
	
	
		

	LOCAL lcMascaraDinheiro  
	lcMascaraDinheiro = uf_imprimirpos_devolveMascaraPvpTalao()
		
	&& Verifica se deve imprimir motivo de isencao de Iva
	lcMotIsencaoIva = uf_gerais_getParameter("ADM0000000261","Bool")	

	&& CALCULAR DADOS PARA O CABE�ALHO E PARA OS TOTAIS
	Local lcCod, descTotal, descUtente, docTotal, docTotalSemDesc, eivain1Total, eivain2Total, eivain3Total, eivain4Total, eivav1Total, eivav2Total, eivav3Total, eivav4Total, lcnratend
	LOCAL eivain5Total, eivain6Total, eivain7Total, eivain8Total, eivain9Total, eivain10Total, eivain11Total, eivain12Total, eivain13Total, eivav5Total, eivav6Total, eivav7Total, eivav8Total, eivav9Total, eivav10Total, eivav11Total, eivav12Total, eivav13Total
	Store 0 To descTotal, descUtente, docTotal, docTotalSemDesc, eivain1Total, eivain2Total, eivain3Total, eivain4Total, eivav1Total, eivav2Total, eivav3Total, eivav4Total
	STORE 0 TO eivain5Total, eivain6Total, eivain7Total, eivain8Total, eivain9Total, eivain10Total, eivain11Total, eivain12Total, eivain13Total, eivav5Total, eivav6Total, eivav7Total, eivav8Total, eivav9Total, eivav10Total, eivav11Total, eivav12Total, eivav13Total
	STORE '' TO lcnratend
	lcCod = .f.

	&& VERIFICA SE DEVE IMPRIMIR O C�DIGO ADICIONAL DO OPERADOR EM VEZ DO C�DIGO PRINCIPAL **
	LOCAL lcValidaCodAlt		
	STORE 0 TO lcValidaCodAlt
	
	IF uf_gerais_getParameter("ADM0000000047","BOOL")
		lcValidaCodAlt = 1
	ENDIF 

	&& Verifica se existem promo��es aplicadas - lcPromo � o nome da campanha aplicada
	IF USED("uCrsfi")
		LOCAL lcValidaPromo, lcPromo
		STORE .f. TO lcValidaPromo
		STORE '' TO lcPromo

		SELECT uCrsFi
		GO TOP 
		SCAN
			IF !EMPTY(uCrsFi.campanhas)
				lcValidaPromo   = .t.
				lcPromo 		= ALLTRIM(lcPromo) + IIF(EMPTY(ALLTRIM(lcPromo)),"",";") + ALLTRIM(uCrsfi.campanhas)
			ENDIF
		ENDSCAN 
	ENDIF 
	
	&& Verifica os pontos do cliente atribuidos no atendimento - 
	IF myCartaoClienteLT
		lcSQL = ''
		TEXT TO lcSQL NOSHOW TEXTMERGE
			select nrcartao, pontosAtrib, pontosUsa, inactivo from B_fidel (nolock) where nrCartao = '<<ALLTRIM(uCrsAtendCl.nrcartao)>>'
		ENDTEXT 
		IF !uf_gerais_actGrelha("", "uCrsUltTranCartao", lcSql)
			uf_perguntalt_chama("N�o foi poss�vel verificar pontos a atribuir no atendimento. Por favor contacte o suporte.", "", "OK", 32)
			RETURN .f.
		ENDIF	
	ENDIF
	
	&& Variaveis tipo de pagamento
	LOCAL lcMpag1, lcMpag2, lcMpag3, lcMpag4, lcMpag5, lcMpag6, lcMpag7, lcMpag8
	STORE '' TO lcMpag1, lcMpag2, lcMpag3, lcMpag4, lcMpag5, lcMpag6, lcMpag7, lcMpag8
	
	SELECT UcrsConfigModPag
	GO TOP
	SCAN 
		DO CASE 
			CASE ALLTRIM(UcrsConfigModPag.ref) == "01"
				lcMpag1 = ALLTRIM(UcrsConfigModPag.design)
			CASE ALLTRIM(UcrsConfigModPag.ref) == "02"
				lcMpag2 = ALLTRIM(UcrsConfigModPag.design)			
			CASE ALLTRIM(UcrsConfigModPag.ref) == "03"
				lcMpag3 = ALLTRIM(UcrsConfigModPag.design)
			CASE ALLTRIM(UcrsConfigModPag.ref) == "04"
				lcMpag4 = ALLTRIM(UcrsConfigModPag.design)
			CASE ALLTRIM(UcrsConfigModPag.ref) == "05"
				lcMpag5 = ALLTRIM(UcrsConfigModPag.design)
			CASE ALLTRIM(UcrsConfigModPag.ref) == "06"
				lcMpag6 = ALLTRIM(UcrsConfigModPag.design)
			CASE ALLTRIM(UcrsConfigModPag.ref) == "07"
				lcMpag7 = ALLTRIM(UcrsConfigModPag.design)
			CASE ALLTRIM(UcrsConfigModPag.ref) == "08"
				lcMpag8 = ALLTRIM(UcrsConfigModPag.design)																		
		ENDCASE
	ENDSCAN	
	
	CREATE CURSOR uCrsStampsUtilizados (ftstamp c(50))	
	&&	
	SELECT uCrsImpTalaoPos
	GO TOP 
	SELECT * FROM uCrsImpTalaoPos WHERE !(uCrsimpTalaoPos.susp) AND !(uCrsImpTalaoPos.tipo=myRegCli) AND !(uCrsImpTalaoPos.tipo=85) AND !(uCrsImpTalaoPos.tipo=903) AND !(uCrsImpTalaoPos.tipo=901) And !(uCrsImpTalaoPos.tipo=904) And !(uCrsImpTalaoPos.tipo=905) AND !(uCrsImpTalaoPos.tipo=75) AND EMPTY(uCrsimpTalaoPos.AdiReserva) INTO CURSOR uCrsImpTalaoPosCalc READWRITE 
	
	SELECT uCrsImpTalaoPosCalc 
	GO TOP 
	SELECT distinct stamp as ftstamp FROM uCrsImpTalaoPosCalc INTO CURSOR uCrsStampsUtilizados readwrite
	
	CREATE CURSOR uCrsStampsQRCode (ftstamp c(50))	
	
	&&:Solu��o Sousa Reis - linhas n�o impressas
	&&GO TOP
	&&SCAN FOR !(uCrsimpTalaoPos.susp) AND !(uCrsImpTalaoPos.tipo=myRegCli) AND !(uCrsImpTalaoPos.tipo=85) AND !(uCrsImpTalaoPos.tipo=903) AND !(uCrsImpTalaoPos.tipo=901) And !(uCrsImpTalaoPos.tipo=904) And !(uCrsImpTalaoPos.tipo=905)
	**SCAN FOR !(uCrsimpTalaoPos.susp) AND !(uCrsImpTalaoPos.tipo=myRegCli) AND !(uCrsImpTalaoPos.tipo=85) AND !(uCrsImpTalaoPos.tipo=903) AND !(uCrsImpTalaoPos.tipo=901) And !(uCrsImpTalaoPos.tipo=904) And !(uCrsImpTalaoPos.tipo=905) AND !(uCrsImpTalaoPos.tipo=75) AND EMPTY(uCrsimpTalaoPos.AdiReserva)
	LOCAL lcPosCurT, lcFtStampProcessado
	lcFtStampProcessado = .f.
	SELECT uCrsImpTalaoPosCalc 
	**BROWSE 
	GO TOP 
	SCAN 
		lcPosCurT= RECNO("uCrsImpTalaoPosCalc")
		
		lcStamp = uCrsImpTalaoPosCalc.stamp
		
*!*			lcFtStampProcessado = .f.
*!*			SELECT uCrsStampsUtilizados 
*!*			GO TOP 
*!*			SCAN 
*!*				IF ALLTRIM(uCrsStampsUtilizados.ftstamp) == ALLTRIM(lcStamp)
*!*					lcFtStampProcessado = .t.
*!*				ENDIF 
*!*			ENDSCAN 
*!*			lcSQL = ""
*!*			TEXT TO lcSQL TEXTMERGE NOSHOW
*!*				select * from fi (nolock) where ftstamp='<<ALLTRIM(lcStamp)>>' and design like '%Ad. Reserva%'
*!*			ENDTEXT
*!*			If !uf_gerais_actGrelha("", "uCrsTempLojas", lcSql)
*!*				uf_perguntalt_chama("N�o foi possivel encontrar regitos.", "", "OK", 16)
*!*				RETURN .f.
*!*			ENDIF
*!*			SELECT uCrsTempLojas
*!*			IF RECCOUNT("uCrsTempLojas")>0
*!*				lcFtStampProcessado = .t.
*!*			ENDIF 
		
		
		**IF lcFtStampProcessado = .f.
			&& guardar vales de desconto aplicados
			IF USED("uCrsFiPromo")
				fecha("uCrsFiPromo")
			ENDIF
			SELECT fi
			GO TOP
			SELECT ref, u_refvale, u_epvp, design from fi where epromo =.t. INTO CURSOR uCrsFiPromo
			SELECT uCrsFiPromo
			**
			
			myNrVendasTalao = myNrVendasTalao + 1
			
			if(USED("uCrsFt"))
				select uCrsFt
				GO TOP 
				LOCATE FOR ALLTRIM(uCrsFt.ftstamp)==ALLTRIM(lcStamp)
			ENDIF
			
	        IF USED("uCrsFtA")
	            fecha("uCrsFtA")
	        ENDIF 
	        IF USED("uCrsFt2A")
	            fecha("uCrsFt2A")
	        ENDIF 
	        IF USED("uCrsFiA")
	            fecha("uCrsFiA")
	        ENDIF 
	            
	        uf_gerais_actGrelha("",[uCrsFtA],[exec up_touch_ft ']+lcStamp+['])
	        select uCrsFtA
	        uf_gerais_actGrelha("",[uCrsFt2A],[exec up_touch_ft2 ']+lcStamp+['])
	        select uCrsFt2A
	        uf_gerais_actGrelha("",[uCrsFiA],[exec up_touch_fi ']+alltrim(lcStamp)+[', '', 0])
	        select uCrsFiA
	        
   	        SELECT uCrsFtA
   	        IF uCrsFtA.u_tipodoc != 3 
		        **_cliptext=lcStamp
		        **MESSAGEBOX('Total do doc - ' + ALLTRIM(STR(uCrsFtA.etotal,15,2))) 
				docTotal = docTotal + uCrsFtA.etotal
				descTotal = descTotal + uCrsFtA.edescc
				eivain1Total = eivain1Total + uCrsFtA.EIVAIN1
				eivain2Total = eivain2Total + uCrsFtA.EIVAIN2
				eivain3Total = eivain3Total + uCrsFtA.EIVAIN3
				eivain4Total = eivain4Total + uCrsFtA.EIVAIN4
				eivain5Total = eivain5Total + uCrsFtA.EIVAIN5
				eivain6Total = eivain6Total + uCrsFtA.EIVAIN6
				eivain7Total = eivain7Total + uCrsFtA.EIVAIN7
				eivain8Total = eivain8Total + uCrsFtA.EIVAIN8
				eivain9Total = eivain9Total + uCrsFtA.EIVAIN9
				eivain10Total = eivain10Total + uCrsFtA.EIVAIN10
				eivain11Total = eivain11Total + uCrsFtA.EIVAIN11
				eivain12Total = eivain12Total + uCrsFtA.EIVAIN12
				eivain13Total = eivain13Total + uCrsFtA.EIVAIN13
				eivav1Total = eivav1Total + uCrsFtA.EIVAV1
				eivav2Total = eivav2Total + uCrsFtA.EIVAV2
				eivav3Total = eivav3Total + uCrsFtA.EIVAV3
				eivav4Total = eivav4Total + uCrsFtA.EIVAV4
				eivav5Total = eivav5Total + uCrsFtA.EIVAV5
				eivav6Total = eivav6Total + uCrsFtA.EIVAV6
				eivav7Total = eivav7Total + uCrsFtA.EIVAV7
				eivav8Total = eivav8Total + uCrsFtA.EIVAV8
				eivav9Total = eivav9Total + uCrsFtA.EIVAV9
				eivav10Total = eivav10Total + uCrsFtA.EIVAV10
				eivav11Total = eivav11Total + uCrsFtA.EIVAV11
				eivav12Total = eivav12Total + uCrsFtA.EIVAV12
				eivav13Total = eivav13Total + uCrsFtA.EIVAV13
				SELECT uCrsFiA
				SCAN FOR ALLTRIM(uCrsFiA.ftstamp)==ALLTRIM(lcStamp)
					docTotalSemDesc = docTotalSemDesc + (uCrsFiA.epv*uCrsFiA.qtt)
				ENDSCAN 
				
				SELECT uCrsFt2A
				LOCATE FOR ALLTRIM(uCrsFt2A.ft2stamp)==ALLTRIM(lcStamp)
				IF !Empty(uCrsFt2A.u_codigo)
					lcCod = .t.
				ENDIF
				**uf_perguntalt_chama("parou.","OK","",16)
				lcCountVendasNSusp = lcCountVendasNSusp + 1
			ENDIF 
			**SELECT uCrsStampsUtilizados 
			**APPEND BLANK 
			**replace uCrsStampsUtilizados.ftstamp WITH ALLTRIM(lcStamp)
		**ENDIF 
		   
		SELECT uCrsImpTalaoPosCalc
		TRY
			GO lcPosCurT	
		CATCH
		ENDTRY
	ENDSCAN


**	MESSAGEBOX('Total da impress�o - ' + ALLTRIM(STR(doctotal,15,2)))
	
	fecha("uCrsImpTalaoPosCalc")
	
	IF !USED("uCrsFta")
		SELECT uCrsImpTalaoPos
		uf_gerais_actGrelha("",[uCrsFtA],[exec up_touch_ft ']+ALLTRIM(uCrsImpTalaoPos.stamp)+['])
	ENDIF 
	
	SELECT uCrsFta
	GO TOP
	IF !EMPTY(uCrsFta.u_nratend)
		LOCAL lcNrAtAux
		lcNrAtAux = UPPER(ALLTRIM(uCrsFta.u_nratend))
		
		IF USED("uCrsFiVD")
            fecha("uCrsFiVD")
        ENDIF 
            
        uf_gerais_actGrelha("",[uCrsFiVD],[exec up_touch_fi_atendimento ']+ALLTRIM(lcNrAtAux)+['])
        select uCrsFiVD
        GO TOP 
        SCAN 
        	**IF !EMPTY(fi.ref) AND (!EMPTY(fi.ofistamp) OR ALLTRIM(fi.ref)=='R000001')
        	IF !EMPTY(uCrsFiVD.ofistamp) OR ALLTRIM(uCrsFiVD.ref)=='R000001'
        		lcVdNormal = .f.
        	ENDIF 
        ENDSCAN 
	ENDIF 
	
	IF lcCountVendasNSusp == 0
		RETURN .f.
	ENDIF 
	
	descUtente = docTotalSemDesc - docTotal
	
	IF USED("uCrsFt")
		IF !Reccount("uCrsFt") > 0
			RETURN .f.
		ENDIF
	ELSE
		RETURN .f.
	ENDIF

	** pontos do atendimento
	LOCAL LcValcartaoAT
	STORE 0 TO LcValcartaoAT
	uf_gerais_actGrelha("",[uCrsValPontos],[select sum(valcartao) as totcartao from fi (nolock) where ftstamp in (select ftstamp from ft (nolock) where u_nratend=']+UPPER(ALLTRIM(uCrsFta.u_nratend))+[')])
	select uCrsValPontos
	LcValcartaoAT = uCrsValPontos.totcartao 

	&&:Solu��o: As vezes n�o imprime as linhas 	        
	IF lcVdNormal = .t.
		SELECT uCrsFta
		GO TOP
		IF !EMPTY(uCrsFta.u_nratend)
			LOCAL lcNrAtAux
			lcNrAtAux = UPPER(ALLTRIM(uCrsFta.u_nratend))
			
			IF USED("uCrsFt")
	            fecha("uCrsFt")
	        ENDIF 
	        IF USED("uCrsFt2")
	            fecha("uCrsFt2")
	        ENDIF 
	        IF USED("uCrsFi")
	            fecha("uCrsFi")
	        ENDIF 
			IF USED("uCrsFi2")
				fecha("uCrsFi2")
			ENDIF 
	            
	        uf_gerais_actGrelha("",[uCrsFt],[exec up_touch_ft_atendimento ']+ALLTRIM(lcNrAtAux)+['])
	        select uCrsFt
	        uf_gerais_actGrelha("",[uCrsFt2],[exec up_touch_ft2_atendimento ']+ALLTRIM(lcNrAtAux)+['])
	        select uCrsFt2
	        uf_gerais_actGrelha("",[uCrsFi],[exec up_touch_fi_atendimento ']+ALLTRIM(lcNrAtAux)+['])
	        select uCrsFi
			uf_gerais_actGrelha("",[uCrsFi2],[exec up_touch_fi2 ']+alltrim(lcNrAtAux)+[', ''])
			select uCrsFi2
	       
		ENDIF 
	ENDIF 
	
	if used("uCrsFt")
		lcSQL = ''
		TEXT TO lcSQL NOSHOW TEXTMERGE
			exec up_print_docs_atendimento '<<uCrsFt.ftstamp>>', '<<uCrsFt.u_nratend>>', ''
		ENDTEXT 
		IF !uf_gerais_actGrelha("", "uCrsProdComp", lcSql)
			uf_perguntalt_chama("N�o foi poss�vel encontrar a informa��o das vendas [FI]. Por favor contacte o Suporte.", "", "OK", 32)
			RETURN .f.
		ENDIF	
	endif 

	IF myTipoCartao == 'Valor'
		LOCAL lcTotalHistCartao, lcValDispCartao
		STORE 0 TO lcTotalHistCartao, lcValDispCartao
		SELECT ucrsft
		lcSQL = ''

        IF !EMPTY(ucrsft.u_hclstamp)

            TEXT To lcSQL TEXTMERGE NOSHOW
                SELECT 
                    b_fidel.valcartaohist, 
                    b_fidel.valcartao 
                FROM 
                    b_fidel(NOLOCK) 
                    JOIN b_utentes(NOLOCK) ON b_fidel.clno = b_utentes.no AND b_fidel.clestab = b_utentes.estab
                WHERE 
                    b_utentes.utstamp = '<<ALLTRIM(ucrsft.u_hclstamp)>>'
            ENDTEXT 

        ELSE

            TEXT To lcSQL TEXTMERGE NOSHOW
                SELECT valcartaohist, valcartao FROM b_fidel WHERE clno=<<ucrsft.no>> and clestab=<<ucrsft.estab>>
            ENDTEXT 

        ENDIF

		uf_gerais_actGrelha("","ucrsverifvalcartao",lcSQL)
		IF RECCOUNT("ucrsverifvalcartao")>0 then
			lcTotalHistCartao = ucrsverifvalcartao.valcartaohist
			lcValDispCartao = ucrsverifvalcartao.valcartao 
		ENDIF 
	ENDIF 
	
	LOCAL lctiposaft
	STORE 0 TO lctiposaft
	Text To lcSql Noshow Textmerge
		select tiposaft from td where ndoc=<<uCrsFt.ndoc>>
	ENDTEXT
	uf_gerais_actGrelha("","uCrsSaftFile",lcSql)
	lctiposaft= uCrsSaftFile.tiposaft
	fecha("uCrsSaftFile")
	
	SELECT uCrsFt
	IF uCrsFt.FNO = 0 
		LOCAL lcfnotroca
		lcfnotroca = 0
		Text To lcSql Noshow textmerge
			SELECT fno FROM ft (nolock) WHERE ftstamp='<<ALLTRIM(uCrsFt.ftstamp)>>'
		Endtext
		uf_gerais_actGrelha("","uCrsNumDocsPrint1",lcSql)
		SELECT uCrsNumDocsPrint1
		lcfnotroca = uCrsNumDocsPrint1.FNO
		fecha("uCrsNumDocsPrint1")
		SELECT uCrsFt
		replace uCrsFt.FNO WITH lcfnotroca 
	ENDIF 
	
	** Altera��o para imprimir 2 vias se tiver campanhas
	** MESSAGEBOX(usacampanha)
	IF usacampanha=1 AND uf_gerais_getParameter('ADM0000000283','BOOL')
		lcRepete=2
	ENDIF 

	FOR i=1 TO lcRepete

		CREATE CURSOR uCrsStampsQRCode (ftstamp c(50))	
		
		&& PREPARAR DEFINI��ES GERAIS DA IMPRESSORA
		lcValidaImp = uf_gerais_setImpressoraPOS(.t.)
		
		IF lcValidaImp
			&& Cabe�alho da Venda
			uf_gerais_criarCabecalhoPOS()

			&& Controla se Imprime Original ou 2� VIA
			IF nrTaloes > 1
				IF uf_gerais_getParameter("ADM0000000209","BOOL")
					?PADL("2a VIA",uf_gerais_getParameter("ADM0000000194","NUM")," ")
					**uf_gerais_separadorPOS()
				ENDIF
			ELSE 
				IF i=1 
					?PADL("ORIGINAL",uf_gerais_getParameter("ADM0000000194","NUM")," ")
				ELSE
					?PADL("2a VIA",uf_gerais_getParameter("ADM0000000194","NUM")," ")
				ENDIF 
			ENDIF

			&& IMPRIMIR O TIPO DE DOCUMENTO DE VENDA
			&& existem impressoras que n�o suportam esta funcionalidade nesses cen�rios � necess�rio comentar isto.
			uf_gerais_textoVermPOS()
			
			LOCAL lcPosFTPrint, lcImprimeFts, lcFtstampSel
			
			SELECT uCrsFt
			GO TOP
			SCAN
				lcImprimeFts = .f.
				lcFtstampSel = ALLTRIM(uCrsFt.ftstamp)
				SELECT uCrsStampsUtilizados
				GO TOP 
				SCAN 
					IF ALLTRIM(uCrsStampsUtilizados.ftstamp) == ALLTRIM(lcFtstampSel)
						lcImprimeFts = .t.
					ENDIF 
				ENDSCAN 
				
				IF lcImprimeFts =.t.

					SELECT uCrsFt
					lcPosFTPrint= RECNO("uCrsFt")
					
					SELECT uCrsImpTalaoPos
					GO TOP 
					LOCATE FOR ALLTRIM(uCrsImpTalaoPos.stamp)==ALLTRIM(uCrsFt.ftstamp)
					&&IF !(uCrsimpTalaoPos.susp) AND !(uCrsImpTalaoPos.tipo==myRegCli) AND !(uCrsImpTalaoPos.tipo==85) AND !(uCrsImpTalaoPos.tipo==903) AND !(uCrsImpTalaoPos.tipo==901) AND !(uCrsImpTalaoPos.tipo==904) AND !(uCrsImpTalaoPos.tipo==905)
					&&IF !(uCrsimpTalaoPos.susp) AND !(uCrsImpTalaoPos.tipo==myRegCli) AND !(uCrsImpTalaoPos.tipo==85) AND !(uCrsImpTalaoPos.tipo==903) AND !(uCrsImpTalaoPos.tipo==901) AND !(uCrsImpTalaoPos.tipo==904) AND !(uCrsImpTalaoPos.tipo==905) AND EMPTY(uCrsimpTalaoPos.AdiReserva)
					IF !(uCrsimpTalaoPos.susp) AND !(uCrsImpTalaoPos.tipo=myRegCli) AND !(uCrsImpTalaoPos.tipo=85) AND !(uCrsImpTalaoPos.tipo=903) AND !(uCrsImpTalaoPos.tipo=901) And !(uCrsImpTalaoPos.tipo=904) And !(uCrsImpTalaoPos.tipo=905) AND !(uCrsImpTalaoPos.tipo=75) AND EMPTY(uCrsimpTalaoPos.AdiReserva)
						
						IF UPPER(ALLTRIM(ucrse1.pais)) == 'PORTUGAL'
							DO CASE
								CASE (uCrsFt.u_tipodoc==8) OR (uCrsFt.ndoc==32) OR (uCrsFt.ndoc==31) OR (uCrsFt.ndoc==84)
									IF YEAR(uCrsFt.fdata) > 2012
										?"Fatura-Recibo Nr."
									ELSE
										?"Vnd. a Dinheiro Nr."
									ENDIF 
									??TRANSFORM(uCrsFt.FNO,"999999999")+" "
							
								CASE (uCrsFt.u_tipodoc==9) OR (uCrsFt.ndoc==11) OR (uCrsFt.ndoc==12)
									?"Fatura Nr."
									??TRANSFORM(uCrsFt.FNO,"999999999")+" "
							
								CASE (uCrsFt.u_tipodoc==11)
									?"Vnd. Manual Nr."
									??TRANSFORM(uCrsFt.FNO,"999999999")+" "
								
								CASE (uCrsFt.ndoc==4)
									?"Dev. a Cliente Nr."
									??TRANSFORM(uCrsFt.FNO,"999999999")+" "
									
								CASE (uCrsFt.u_tipodoc == 4)
									?"Fatura Acordo Nr."
									??TRANSFORM(uCrsFt.FNO,"999999999")+" "
								
								OTHERWISE
									***
							ENDCASE
								***
					
							&& IMPRIMIR TAG IDENTIFICADORA DO DOCUMENTO DE VENDA
							DO CASE
								CASE (uCrsFt.u_tipodoc==8 OR uCrsFt.ndoc==84) AND uCrsFt.u_lote==0 AND uCrsFt.cobrado==.f.
									??"(VD)"
							
								CASE (uCrsFt.u_tipodoc==8 OR uCrsFt.ndoc==84) AND uCrsFt.u_lote > 0
									??"(VD-CR)"
							
								CASE (uCrsFt.u_tipodoc==9) AND uCrsFt.cobrado==.f. AND uCrsFt.u_lote==0
									??"(FT)"
							
								CASE (uCrsFt.u_tipodoc==9) AND uCrsFt.u_lote > 0
									??"(FT-CR)"
							
								CASE (uCrsFt.u_tipodoc==11)
									??"(VDM)"
								
								CASE (uCrsFt.u_tipodoc=4) And uCrsFt.cobrado=.f. And uCrsFt.u_lote=0
									??"(FA)"

								CASE (uCrsFt.u_tipodoc=4) AND uCrsFt.cobrado=.T. AND uCrsFt.u_lote=0
									??"(FA)(S)"

								CASE (uCrsFt.u_tipodoc=4) AND uCrsFt.u_lote > 0
									??"(FA-CR)"	

								OTHERWISE
									***
									
							ENDCASE
							**
							
							&& Imprimir serie de factura��o
							??"/" + astr(uCrsFT.ndoc)
							
							IF ALLTRIM(uCrsAtendCL.ncont)<>ALLTRIM(uCrsFT.ncont)
								??" - NIF:" + ALLTRIM(uCrsFT.ncont)
							ENDIF 
						ELSE
							SELECT uCrsFt
							IF uCrsFt.ftano>2020
								? ALLTRIM(uCrsFt.nmdoc) + ' Nr. ' + alltrim(lctiposaft) + ' ' + alltrim(str(uCrsFt.ndoc)) + alltrim(str(uCrsFt.ftano)) + '/' + alltrim(str(uCrsFt.fno))
							ELSE
								? ALLTRIM(uCrsFt.nmdoc) + ' Nr. ' + alltrim(lctiposaft) + ' ' + alltrim(str(uCrsFt.ndoc)) + '/' + alltrim(str(uCrsFt.fno))
							ENDIF 
						ENDIF  
					ENDIF 
					
					SELECT uCrsFt
					TRY
						GO lcPosFTPrint	
					CATCH
					ENDTRY
				ENDIF 
				
			ENDSCAN

			SELECT uCrsFT
			GO TOP
			
			&& Imprime Data e Hora
			?"Data: "
			??uf_gerais_getDate(uCrsFt.FDATA,"DATA")
			??"      Hora: "
			**??uCrsFt.SAIDA
			?? Substr(uCrsFtA.ousrhora,1,5)
			
*!*				** Controla de tem validade fiscal
*!*				SELECT uCrsFt
*!*				IF uCrsFt.u_tipodoc == 3 OR uCrsFt.u_tipodoc == 4
*!*					? "*** Talao sem Validade Fiscal ***"
*!*				ENDIF

			&& existem impressoras que n�o suportam esta funcionalidade nesses cen�rios � necess�rio comentar isto.
		   	uf_gerais_resetCorPOS() 
	   	
			uf_gerais_separadorPOS()	

			&& Impress�o dados Utente + Dados Ent. Fat
			SELECT uCrsAtendCL
			GO TOP  
			SELECT uCrsFT
			GO TOP	

			&& Se Utente for diferente da Entidade Fatura��o
			IF uCrsAtendCL.no != uCrsFT.no or uCrsAtendCL.estab != uCrsFT.estab
				&& Dados Entidade Faturacao
				?"Cliente: "
				??Alltrim(uCrsFt.NOME) + Iif(uCrsFt.no==200, '', ' (' + AStr(uCrsFt.no) + ')(' + AStr(uCrsFt.estab) + ')' )  + IIF(!EMPTY(uCrsFt.no_ext), ' - '+ALLTRIM(uCrsFt.no_ext),'' )
				?"Morada: "
				??ALLTRIM(uCrsFt.MORADA)
				?"Cod.Post.: "
				??LEFT(ALLTRIM(uCrsFt.CODPOST),uf_gerais_getParameter("ADM0000000194","NUM")-11)

				SELECT uCrsFt
				GO TOP
			
				?"Contrib. Nr.: "
				if (ALLTRIM(uCrsFt.ncont)!="999999990") AND !EMPTY(ALLTRIM(uCrsFt.ncont)) AND (ALLTRIM(uCrsFt.ncont)!="9999999999")
					??TRANSFORM(uCrsFt.NCONT,"99999999999999999999")
				ELSE
					??ALLTRIM(uf_gerais_getParameter_site('ADM0000000075', 'text', mySite))
					IF UPPER(ALLTRIM(ucrse1.pais)) == 'PORTUGAL'
						?"Nao sujeito passivo de IVA"
					ELSE
						?" "
					ENDIF 
				ENDIF
				
				?"Tlf.: "
				??substr(uCrsFt.telefone,1,9)

				IF ALLTRIM(uCrsE1.tipoempresa) == 'FARMACIA'
					?"N.Utente: "
					??IIF(EMPTY(ALLTRIM(uCrsFt2.u_nbenef)),"         ",ALLTRIM(uCrsFt2.u_nbenef))
					??"  N.Benef.: "
					??ALLTRIM(uCrsFt2.u_nbenef2)
				ENDIF
			
				** imprime dependente consoante parametro empresa ADM0000000123
				
				IF !uf_gerais_getparameter_site('ADM0000000123', 'BOOL', mysite)
					uf_gerais_separadorPOS()
				
					&& Dados Utente a quem � associado os hist�rico de Produtos
					SELECT uCrsAtendCL
					GO TOP
				
					?"Dependente: "
					??Alltrim(uCrsAtendCL.NOME) + Iif(uCrsAtendCL.no==200, '', ' (' + AStr(uCrsAtendCL.no) + ')(' + AStr(uCrsAtendCL.estab) + ')' )
				
					?"Morada: "
					??ALLTRIM(uCrsAtendCL.MORADA)
					
					?"Cod.Post.: "
					??LEFT(ALLTRIM(uCrsAtendCL.CODPOST),uf_gerais_getParameter("ADM0000000194","NUM")-11)
					?"  Tlf.: "
					??substr(uCrsAtendCL.telefone,1,9)
					IF ALLTRIM(uCrsE1.tipoempresa) == 'FARMACIA'
						??"N.Benef.: "
						??IIF(EMPTY(ALLTRIM(uCrsAtendCL.nbenef)),"         ",ALLTRIM(uCrsAtendCL.nbenef))
					ENDIF	
				ENDIF 

			ELSE

				?"Cliente: "
				??Alltrim(uCrsFt.NOME) + Iif(uCrsFt.no==200, '', ' (' + AStr(uCrsFt.no) + ')(' + AStr(uCrsFt.estab) + ')' )  + IIF(!EMPTY(uCrsFt.no_ext), ' - '+ALLTRIM(uCrsFt.no_ext),'' )
				?"Morada: "
				??ALLTRIM(uCrsFt.MORADA)
				
				?"Cod.Post.: "
				??LEFT(ALLTRIM(uCrsFt.CODPOST),uf_gerais_getParameter("ADM0000000194","NUM")-11)

				SELECT uCrsFt
				GO TOP
			
				?"Contrib. Nr.: "
				if (ALLTRIM(uCrsFt.ncont)!="999999990") AND !EMPTY(ALLTRIM(uCrsFt.ncont)) AND (ALLTRIM(uCrsFt.ncont)!="9999999999")
					??TRANSFORM(uCrsFt.NCONT,"99999999999999999999")
				ELSE
					??ALLTRIM(uf_gerais_getParameter_site('ADM0000000075', 'text', mySite))
					IF UPPER(ALLTRIM(ucrse1.pais)) == 'PORTUGAL'	
						? "Nao sujeito passivo de IVA"
					ELSE
						? " "
					ENDIF 
				ENDIF
				
				?"Tlf.: "
				??substr(uCrsFt.telefone,1,9)

				IF ALLTRIM(uCrsE1.tipoempresa) == 'FARMACIA'
					?"N.Utente: "
					??IIF(EMPTY(ALLTRIM(uCrsFt2.u_nbenef)),"         ",ALLTRIM(uCrsFt2.u_nbenef))
					??" N.Benef.: "
					??ALLTRIM(uCrsFt2.u_nbenef2)
				ENDIF		

			ENDIF
			
			
			SELECT uCrsFt2
			&& Informa se est� sujeito a cativa��o de IVA
			IF uCrsFt2.cativa AND uf_gerais_validaIvaDireitoDeducao() == 0
				?"Sujeito a cativa��o de IVA."
			ENDIF 
			
			&& SE A VENDA FOR AO DOMICILIO IMPRIME OS DADOS 
			IF Ft2.u_vddom
				uf_gerais_separadorPOS()
				?"Entrega ao Domicilio: "
				?ALLTRIM(uf_gerais_getMacrosReports(5))
				?"Transportador: " + LEFT(ALLTRIM(uCrsFt2.CONTACTO),uf_gerais_getParameter("ADM0000000194","NUM")-15)
				?"Morada: " + ALLTRIM(uCrsFt2.MORADA)
				?"Cod.Post: " + TRANSFORM(Ft2.CODPOST,"########")
				??"  Local.: " + TRANSFORM(Ft2.local,"#################")
				?"Tel.: " + TRANSFORM(Ft2.TELEFONE,"##############")
				??"Email: " + Substr(Ft2.email,1,25)
				IF !Empty(Ft2.u_viatura)
					?"Viatura: " + TRANSFORM(Ft2.u_viatura,"#########################")
					??"Hora: " + Substr(Ft2.horaentrega,1,5)
				ENDIF
			ENDIF 
			
			LOCAL lcTamanhoDesign, lcexisteadres
			STORE .f. TO lcexisteadres
			
			&& VERIFICAR SE EXISTE COMPARTICIPA��O E IMPRIMIR COLUNAS E LINHAS 
			IF !EMPTY(lcCod) 
				IF lcMotIsencaoIva == .t. AND uf_gerais_validaIvaDireitoDeducao() == 0
					****** IMPRESS�O DAS COLUNAS ******
					
					uf_gerais_separadorPOS()
					?"Produto"
					IF uf_gerais_getParameter("ADM0000000194","NUM") >= 50
						?"   PVP.     Pref.     Qtd.    Comp.   Total    IVA" &&50
					ELSE
						?"PVP.  | Pref. | Qt. | Comp. | Total |IVA" &&40
					ENDIF					
				ELSE
					****** IMPRESS�O DAS COLUNAS ******
					uf_gerais_separadorPOS()
					?"Produto"
					IF uf_gerais_getParameter("ADM0000000194","NUM") >= 50
						?"   PVP.     Pref.     Qtd.    Comp.   Total" &&50
					ELSE
						?"PVP.  | Pref. | Qt. | Comp. | Total" &&40
					ENDIF			
				ENDIF				
				
				LOCAL lcImprimeFis , lcFistampSel 
				&& Impress�o das Linhas
				SELECT uCrsFi
				GO TOP
				SCAN
					lcImprimeFis = .f.
					lcFistampSel = ALLTRIM(uCrsFi.ftstamp)
					SELECT uCrsStampsUtilizados
					GO TOP 
					SCAN 
						IF ALLTRIM(uCrsStampsUtilizados.ftstamp) == ALLTRIM(lcFistampSel)
							lcImprimeFis = .t.
						ENDIF 
					ENDSCAN 
					
					IF lcImprimeFis =.t.
					
						SELECT uCrsFi
						LOCAL lcPosuCrsFi
						lcPosuCrsFi= RECNO("uCrsFi")
					
						SELECT uCrsImpTalaoPos
						GO TOP 
						LOCATE FOR ALLTRIM(uCrsImpTalaoPos.stamp) == ALLTRIM(uCrsFi.ftstamp)
						&&IF !(uCrsimpTalaoPos.susp) AND !(uCrsImpTalaoPos.tipo==myRegCli) AND !(uCrsImpTalaoPos.tipo=85) AND !(uCrsImpTalaoPos.tipo=903) And !(uCrsImpTalaoPos.tipo=901) And !(uCrsImpTalaoPos.tipo=904) And !(uCrsImpTalaoPos.tipo=905)
						&&IF !(uCrsimpTalaoPos.susp) AND !(uCrsImpTalaoPos.tipo==myRegCli) AND !(uCrsImpTalaoPos.tipo=85) AND !(uCrsImpTalaoPos.tipo=903) And !(uCrsImpTalaoPos.tipo=901) And !(uCrsImpTalaoPos.tipo=904) And !(uCrsImpTalaoPos.tipo=905) AND !(uCrsimpTalaoPos.AdiReserva)
						IF !(uCrsimpTalaoPos.susp) AND !(uCrsImpTalaoPos.tipo=myRegCli) AND !(uCrsImpTalaoPos.tipo=85) AND !(uCrsImpTalaoPos.tipo=903) AND !(uCrsImpTalaoPos.tipo=901) And !(uCrsImpTalaoPos.tipo=904) And !(uCrsImpTalaoPos.tipo=905) AND !(uCrsImpTalaoPos.tipo=75) AND EMPTY(uCrsimpTalaoPos.AdiReserva) AND !INLIST(ALLTRIM(uCrsFi.ref),'V000001')
							lcTamanhoDesign = uf_gerais_getParameter("ADM0000000194","NUM") - IIF(UPPER(ALLTRIM(uCrsFi.lobs2))=='REF.VD.ORIG.',2,0) - IIF(uCrsFi.iva==0 and !(UPPER(ALLTRIM(uCrsFi.lobs2))=='REF.VD.ORIG.'),5,0)
							**?IIF(UPPER(ALLTRIM(uCrsFi.lobs2))=='REF.VD.ORIG.','  ','') + substr(ALLTRIM(uCrsFi.DESIGN),1,45) + IIF(uCrsFi.iva==0 and !(UPPER(ALLTRIM(uCrsFi.lobs2))=='REF.VD.ORIG.') and ALLTRIM(uCrsFi.ref)!='V000001',' (*1)','')
							?IIF(UPPER(ALLTRIM(uCrsFi.lobs2))=='REF.VD.ORIG.','  ','') + substr(ALLTRIM(uCrsFi.DESIGN),1,45) 
							IF uf_gerais_getParameter("ADM0000000194","NUM") >= 50
								IF !(UPPER(ALLTRIM(uCrsFi.lobs2))=='REF.VD.ORIG.')
									?TRANSFORM(uCrsFi.u_epvp,lcMascaraDinheiro )+" "
									??TRANSFORM(uCrsFi.u_EPREF,lcMascaraDinheiro )+"   "
									??TRANSFORM(uCrsFi.QTT,"9999.9") +"   "
									??TRANSFORM(uCrsFi.U_ETTENT1+uCrsFi.U_ETTENT2, lcMascaraDinheiro)
									??TRANSFORM(uCrsFi.ETILIQUIDO,lcMascaraDinheiro)+"  "
									IF lcMotIsencaoIva == .t. AND uf_gerais_validaIvaDireitoDeducao() == 0
										??TRANSFORM(uCrsFi.IVA,"9999")+"%"
									ENDIF
									IF uCrsFi.IVA = 0 AND uf_gerais_validaIvaDireitoDeducao() == 0
										IF TYPE("uCrsFi.motisencao") != 'U' AND ALLTRIM(uCrsFi.ref) <> 'V000001'
											?ALLTRIM(uCrsFi.motisencao)
										ENDIF 
									ENDIF
										LOCAL uv_Lote
									STORE '' TO uv_Lote
									
									uv_Lote = uf_gerais_usalote(uCrsFi.fistamp, "Vet")

									IF !EMPTY(uv_lote)
		                                    ?uv_lote
									ENDIF
									
									
									uf_gerais_actGrelha("",[uCrsFi2],[exec up_touch_fi2 '', ']+alltrim(uCrsFi.fistamp)+['])
									SELECT	uCrsFi2

									IF TYPE("uCrsFi2.bonusDescr") <> "U"

										uv_curLine = uCrsFi.fistamp

										SELECT uCrsFi2
										GO TOP

										LOCATE FOR ALLTRIM(uCrsFi2.fistamp) == ALLTRIM(uv_curLine)

										IF FOUND()
											IF !EMPTY(uCrsFi2.bonusDescr)
												?'*'+ALLTRIM(strtran(uCrsFi2.bonusDescr,'�','c'))

											ENDIF
										ENDIF

										SELECT uCrsFi
									ENDIF
								ENDIF
							ELSE
								IF !(UPPER(ALLTRIM(uCrsFi.lobs2))=='REF.VD.ORIG.')
									?PADC(IIF(OCCURS(".",astr(round(uCrsFi.u_epvp,2),8,2))>0,astr(round(uCrsFi.u_epvp,2),8,2),astr(round(uCrsFi.u_epvp,2),8,2)+".00"),6," ") + "|"
									??PADC(IIF(OCCURS(".",astr(round(uCrsFi.u_EPREF,2),8,2))>0,astr(round(uCrsFi.u_EPREF,2),8,2),astr(round(uCrsFi.u_EPREF,2),8,2)+".00"),7," ") + "|"
									??PADC(astr(round(uCrsFi.QTT,0),8,2),5," ") + "|"
									??PADC(IIF(OCCURS(".",astr(round(uCrsFi.U_ETTENT1+uCrsFi.U_ETTENT2,2),8,2))>0,astr(round(uCrsFi.U_ETTENT1+uCrsFi.U_ETTENT2,2),8,2),astr(round(uCrsFi.U_ETTENT1+uCrsFi.U_ETTENT2,2),8,2)+".00"),7," ") + "|"
									??PADC(IIF(OCCURS(".",astr(round(uCrsFi.ETILIQUIDO,2),8,2))>0,astr(round(uCrsFi.ETILIQUIDO,2),8,2),astr(round(uCrsFi.ETILIQUIDO,2),8,2)+".00"),7," ") + "|"
									IF lcMotIsencaoIva == .t. AND uf_gerais_validaIvaDireitoDeducao() == 0
										??PADC(astr(uCrsFi.IVA)+"%",3," ")
									ENDIF
									IF uCrsFi.IVA = 0 AND uf_gerais_validaIvaDireitoDeducao() == 0
										IF TYPE("uCrsFi.motisencao") != 'U' AND ALLTRIM(uCrsFi.ref) <> 'V000001'
											?ALLTRIM(uCrsFi.motisencao)
										ENDIF 
									ENDIF
									
									LOCAL uv_Lote
									STORE '' TO uv_Lote
									
									uv_Lote = uf_gerais_usalote(uCrsFi.fistamp, "Vet")

									IF !EMPTY(uv_lote)
		                                    ?uv_Lote
									ENDIF
									
									uf_gerais_actGrelha("",[uCrsFi2],[exec up_touch_fi2 '', ']+alltrim(uCrsFi.fistamp)+['])
								
									
									IF TYPE("uCrsFi2.bonusDescr") <> "U"

										uv_curLine = uCrsFi.fistamp

										SELECT uCrsFi2
										GO TOP

										LOCATE FOR ALLTRIM(uCrsFi2.fistamp) == ALLTRIM(uv_curLine)

										IF FOUND()
											IF !EMPTY(uCrsFi2.bonusDescr)
												?'*'+ALLTRIM(strtran(uCrsFi2.bonusDescr,'�','c'))
											ENDIF
										ENDIF

										SELECT uCrsFi
									ENDIF
								ENDIF
							ENDIF
							
							IF uCrsFi.iva==0 AND !(UPPER(ALLTRIM(uCrsFi.lobs2))=='REF.VD.ORIG.') 
								lcIsentoIva = .t.
							ENDIF
						ENDIF 
						SELECT uCrsFi
						TRY
							GO lcPosuCrsFi
						CATCH
						ENDTRY
					ENDIF 
				ENDSCAN
			ENDIF
			
			IF EMPTY(lcCod)
				IF lcMotIsencaoIva == .t.AND uf_gerais_validaIvaDireitoDeducao() == 0
					&& Impress�o das Colunas
					uf_gerais_separadorPOS()
					?"Produto"
					IF uf_gerais_getParameter("ADM0000000194","NUM") >= 45
						?"     PVP.      Qtd.          Total        IVA" &&45
					ELSE
						?" PVP.   |   Qtd.   |   Total   |   IVA  " &&40
					ENDIF
				ELSE
					&& Impress�o das Colunas
					uf_gerais_separadorPOS()
					?"Produto"
					IF uf_gerais_getParameter("ADM0000000194","NUM") >= 45
						?"     PVP.      Qtd.          Total" &&45
					ELSE
						?" PVP.   |   Qtd.   |   Total" &&40
					ENDIF
				ENDIF
				
				LOCAL lcImprimeFis , lcFistampSel 
				&& Impress�o das Linhas
				SELECT uCrsFi
				GO TOP
				SCAN
					lcImprimeFis = .f.
					lcFistampSel = ALLTRIM(uCrsFi.ftstamp)
					SELECT uCrsStampsUtilizados
					GO TOP 
					SCAN 
						IF ALLTRIM(uCrsStampsUtilizados.ftstamp) == ALLTRIM(lcFistampSel)
							lcImprimeFis = .t.
						ENDIF 
					ENDSCAN 
					
					IF lcImprimeFis =.t.
						LOCAL lcPosuCrsFi
						lcPosuCrsFt= RECNO("uCrsFi")
					
						SELECT uCrsImpTalaoPos
						GO TOP 
						LOCATE FOR ALLTRIM(uCrsImpTalaoPos.stamp)==ALLTRIM(uCrsFi.ftstamp)
						&&IF !(uCrsimpTalaoPos.susp) And !(uCrsImpTalaoPos.tipo==myRegCli) AND !(uCrsImpTalaoPos.tipo==85) And !(uCrsImpTalaoPos.tipo==903) And !(uCrsImpTalaoPos.tipo==901) And !(uCrsImpTalaoPos.tipo==904) And !(uCrsImpTalaoPos.tipo==905)
						&& IF !(uCrsimpTalaoPos.susp) And !(uCrsImpTalaoPos.tipo==myRegCli) AND !(uCrsImpTalaoPos.tipo==85) And !(uCrsImpTalaoPos.tipo==903) And !(uCrsImpTalaoPos.tipo==901) And !(uCrsImpTalaoPos.tipo==904) And !(uCrsImpTalaoPos.tipo==905) AND !(uCrsimpTalaoPos.AdiReserva)
						IF !(uCrsimpTalaoPos.susp) AND !(uCrsImpTalaoPos.tipo=myRegCli) AND !(uCrsImpTalaoPos.tipo=85) AND !(uCrsImpTalaoPos.tipo=903) AND !(uCrsImpTalaoPos.tipo=901) And !(uCrsImpTalaoPos.tipo=904) And !(uCrsImpTalaoPos.tipo=905) AND !(uCrsImpTalaoPos.tipo=75) AND EMPTY(uCrsimpTalaoPos.AdiReserva) AND !INLIST(ALLTRIM(uCrsFi.ref),'V000001')
							lcTamanhoDesign = uf_gerais_getParameter("ADM0000000194","NUM") - IIF(UPPER(ALLTRIM(uCrsFi.lobs2))=='REF.VD.ORIG.',2,0) - IIF(uCrsFi.iva==0 and !(UPPER(ALLTRIM(uCrsFi.lobs2))=='REF.VD.ORIG.'),5,0)
							**?IIF(UPPER(ALLTRIM(uCrsFi.lobs2))=='REF.VD.ORIG.','  ','') + substr(ALLTRIM(uCrsFi.DESIGN),1,45) + IIF(uCrsFi.iva==0 and !(UPPER(ALLTRIM(uCrsFi.lobs2))=='REF.VD.ORIG.') and ALLTRIM(uCrsFi.ref)!='V000001',' (*1)','')
							?IIF(UPPER(ALLTRIM(uCrsFi.lobs2))=='REF.VD.ORIG.','  ','') + substr(ALLTRIM(uCrsFi.DESIGN),1,45) 
							IF uf_gerais_getParameter("ADM0000000194","NUM") >= 45
								IF !(UPPER(ALLTRIM(uCrsFi.lobs2))=='REF.VD.ORIG.')
									?TRANSFORM(uCrsFi.EPV,lcMascaraDinheiro )+"   "
									??TRANSFORM(uCrsFi.QTT,"99999.9") +"     "
									??TRANSFORM(uCrsFi.ETILIQUIDO,lcMascaraDinheiro)+"    "
									IF lcMotIsencaoIva == .t. AND uf_gerais_validaIvaDireitoDeducao() == 0
										??TRANSFORM(uCrsFi.IVA,"9999")+"%"
									ENDIF
									IF uCrsFi.IVA = 0 AND uf_gerais_validaIvaDireitoDeducao() == 0
                                        IF TYPE("uCrsFi.motisencao") != 'U' AND ALLTRIM(uCrsFi.ref) <> 'V000001'
                                            ?ALLTRIM(uCrsFi.motisencao)
                                        ENDIF 
									ENDIF
										LOCAL uv_Lote
							STORE '' TO uv_Lote
							
							uv_Lote = uf_gerais_usalote(uCrsFi.fistamp, "Vet")

							IF !EMPTY(uv_lote)
                                    ?uv_lote
							ENDIF
							
									IF TYPE("uCrsFi2.bonusDescr") <> "U"

										uv_curLine = uCrsFi.fistamp

										SELECT uCrsFi2
										GO TOP

										LOCATE FOR ALLTRIM(uCrsFi2.fistamp) == ALLTRIM(uv_curLine)

										IF FOUND()
											IF !EMPTY(uCrsFi2.bonusDescr)
												?'*'+ALLTRIM(uCrsFi2.bonusDescr)
											ENDIF
										ENDIF

										SELECT uCrsFi
									ENDIF
								ENDIF
							ELSE
								IF !(UPPER(ALLTRIM(uCrsFi.lobs2))=='REF.VD.ORIG.')
									?PADC(IIF(OCCURS(".",astr(round(uCrsFi.EPV,2),8,2))>0,astr(round(uCrsFi.EPV,2),8,2),astr(round(uCrsFi.EPV,2),8,2)+".00"),8," ") + "|"
									??PADC(astr(round(uCrsFi.QTT,2),8,2),10," ") + "|"
									??PADC(IIF(OCCURS(".",astr(round(uCrsFi.ETILIQUIDO,2),8,2))>0,astr(round(uCrsFi.ETILIQUIDO,2),8,2),astr(round(uCrsFi.ETILIQUIDO,2),8,2)+".00"),11," ") + "|"
									IF lcMotIsencaoIva == .t. AND uf_gerais_validaIvaDireitoDeducao() == 0	 
										??PADC(astr(uCrsFi.IVA)+"%",8," ")
									ENDIF
									IF uCrsFi.IVA = 0 AND uf_gerais_validaIvaDireitoDeducao() == 0
                                        IF TYPE("uCrsFi.motisencao") != 'U' AND ALLTRIM(uCrsFi.ref) <> 'V000001'
                                            ?ALLTRIM(uCrsFi.motisencao)
                                        ENDIF 
									ENDIF
									
										LOCAL uv_Lote
							STORE '' TO uv_Lote
							
							uv_Lote = uf_gerais_usalote(uCrsFi.fistamp, "Vet")

							IF !EMPTY(uv_lote)
                                    ?uv_lote
							ENDIF
							
									IF TYPE("uCrsFi2.bonusDescr") <> "U"

										uv_curLine = uCrsFi.fistamp

										SELECT uCrsFi2
										GO TOP

										LOCATE FOR ALLTRIM(uCrsFi2.fistamp) == ALLTRIM(uv_curLine)

										IF FOUND()
											IF !EMPTY(uCrsFi2.bonusDescr)
												?'*'+ALLTRIM(uCrsFi2.bonusDescr)
											ENDIF
										ENDIF

										SELECT uCrsFi
									ENDIF
								ENDIF
							ENDIF
							
							IF uCrsFi.iva==0 AND !(UPPER(ALLTRIM(uCrsFi.lobs2))=='REF.VD.ORIG.')
								lcIsentoIva = .t.
							ENDIF
						ENDIF
						SELECT uCrsFi
						TRY
							GO lcPosuCrsFi
						CATCH
						ENDTRY
					ENDIF 
				ENDSCAN 
			ENDIF

			uf_gerais_separadorPOS()

			&& existem impressoras que n�o suportam esta funcionalidade nesses cen�rios � necess�rio comentar isto.
			uf_gerais_textoVermPOS() 
			
			&& Impress�o Valor Descontos e Totais de Documento 
			IF UPPER(ALLTRIM(lcMoeda)) == 'EURO'
				IF descTotal > 0
					?"TOTAL SEM DESCONTO	 "
					??TRANSFORM(docTotalSemDesc,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(213)
								
					?"Descontos:		 "
					??TRANSFORM(descUtente,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(213)
				ENDIF
	
				?"VALOR A PAGAR:      "
				??TRANSFORM(doctotal,"999999999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(213)	

				&& existem impressoras que n�o suportam esta funcionalidade nesses cen�rios � necess�rio comentar isto.
				uf_gerais_resetCorPOS() 
				
				&& Valores Pagamento
				IF lcModopag1 > 0
					? SUBSTR(lcMpag1,1,20) +":		  	     "
					??TRANSFORM(lcModopag1,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(213)		
				ENDIF
				IF lcModopag2 > 0
					? SUBSTR(lcMpag2,1,20) +":			     "
					??TRANSFORM(lcModopag2,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(213)		
				ENDIF
				IF lcModopag3 > 0		
					? SUBSTR(lcMpag3,1,20) +":			  	     "
					??TRANSFORM(lcModopag3,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(213)			
				ENDIF
				IF lcModopag4 > 0
					? SUBSTR(lcMpag4,1,20) +":		  	     "
					??TRANSFORM(lcModopag4,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(213)	
				ENDIF
				IF lcModopag5 > 0
					&& ? SUBSTR(lcMpag5,1,20) +":		  	     	 " asdfg
					? SUBSTR(lcMpag5,1,20) +":			  	     "
					??TRANSFORM(lcModopag5,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(213)			
				ENDIF
				IF lcModopag6 > 0
					? SUBSTR(lcMpag6,1,20) +":			     "
					??TRANSFORM(lcModopag6,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(213)		
				ENDIF
				IF lcModopag7 > 0
					? SUBSTR(lcMpag7,1,20) +":			     "
					??TRANSFORM(lcModopag7,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(213)			
				ENDIF
				IF lcModopag8 > 0
					? SUBSTR(lcMpag8,1,20) +":			     "
					??TRANSFORM(lcModopag8,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(213)			
				ENDIF
				IF lcTroco > 0
					?"TROCO:			  "
					??TRANSFORM(lcTroco,"999999999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(213)		
				ENDIF
			ELSE
				IF descTotal > 0
					?"TOTAL SEM DESCONTO	 "
					IF uf_gerais_getParameter("ADM0000000260","text")='USD'
						??TRANSFORM(docTotalSemDesc,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(36)
					ELSE
						??TRANSFORM(docTotalSemDesc,"99999999.99 ") + " " + ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))
					ENDIF 
								
					?"Descontos:		 "
					IF uf_gerais_getParameter("ADM0000000260","text")='USD'
						??TRANSFORM(descUtente,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(36)
					ELSE
						??TRANSFORM(descUtente,"99999999.99 ") + " " + ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))
					ENDIF 
				ENDIF
		
				?"VALOR A PAGAR:	"
				IF uf_gerais_getParameter("ADM0000000260","text")='USD'
					??TRANSFORM(docTotal,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(36)
				ELSE
					??TRANSFORM(docTotal,"99999999.99 ") + " " + ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))
				ENDIF 

				&& existem impressoras que n�o suportam esta funcionalidade nesses cen�rios � necess�rio comentar isto.
				uf_gerais_resetCorPOS() 
				
				&& Valores Pagamento
				IF uf_gerais_getParameter("ADM0000000260","text")='USD'
					IF lcModopag1 > 0
						? SUBSTR(lcMpag1,1,20) +":		  	     "
						??TRANSFORM(lcModopag1,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(36)		
					ENDIF
					IF lcModopag2 > 0
						? SUBSTR(lcMpag2,1,20) +":			     "
						??TRANSFORM(lcModopag2,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(36)		
					ENDIF
					IF lcModopag3 > 0		
						? SUBSTR(lcMpag3,1,20) +":			  	     "
						??TRANSFORM(lcModopag3,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(36)			
					ENDIF
					IF lcModopag4 > 0
						? SUBSTR(lcMpag4,1,20) +":		  	     "
						??TRANSFORM(lcModopag4,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(36)	
					ENDIF
					IF lcModopag5 > 0
						? SUBSTR(lcMpag5,1,20) +":			  	     "
						??TRANSFORM(lcModopag5,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(36)			
					ENDIF
					IF lcModopag6 > 0
						? SUBSTR(lcMpag6,1,20) +":			     "
						??TRANSFORM(lcModopag6,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(36)		
					ENDIF
					IF lcModopag7 > 0
						? SUBSTR(lcMpag7,1,20) +":			     "
						??TRANSFORM(lcModopag7,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(36)			
					ENDIF
					IF lcModopag8 > 0
						? SUBSTR(lcMpag8,1,20) +":			     "
						??TRANSFORM(lcModopag8,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(36)			
					ENDIF
					IF lcTroco > 0
						?"TROCO:				     "
						??TRANSFORM(lcTroco,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(36)		
					ENDIF
				ELSE
					IF lcModopag1 > 0
						? SUBSTR(lcMpag1,1,20) +":		  	     "
						??TRANSFORM(lcModopag1,"99999999.99 ") + " " + ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))		
					ENDIF
					IF lcModopag2 > 0
						? SUBSTR(lcMpag2,1,20) +":			     "
						??TRANSFORM(lcModopag2,"99999999.99 ") + " " + ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))		
					ENDIF
					IF lcModopag3 > 0		
						? SUBSTR(lcMpag3,1,20) +":			  	     "
						??TRANSFORM(lcModopag3,"99999999.99 ") + " " + ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))			
					ENDIF
					IF lcModopag4 > 0
						? SUBSTR(lcMpag4,1,20) +":		  	     "
						??TRANSFORM(lcModopag4,"99999999.99 ") + " " + ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))	
					ENDIF
					IF lcModopag5 > 0
						? SUBSTR(lcMpag5,1,20) +":			  	     "
						??TRANSFORM(lcModopag5,"99999999.99 ") + " " + ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))			
					ENDIF
					IF lcModopag6 > 0
						? SUBSTR(lcMpag6,1,20) +":			     "
						??TRANSFORM(lcModopag6,"99999999.99 ") + " " + ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))		
					ENDIF
					IF lcModopag7 > 0
						? SUBSTR(lcMpag7,1,20) +":			     "
						??TRANSFORM(lcModopag7,"99999999.99 ") + " " + ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))			
					ENDIF
					IF lcModopag8 > 0
						? SUBSTR(lcMpag8,1,20) +":			     "
						??TRANSFORM(lcModopag8,"99999999.99 ") + " " + ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))			
					ENDIF
					IF lcTroco > 0
						?"TROCO:				     "
						??TRANSFORM(lcTroco,"99999999.99 ") + " " + ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))		
					ENDIF				
				ENDIF 
			ENDIF
			
			&& Resumo IVA - alterado para ficar com apresenta��o diferente 20150729 Lu�s Leal
			IF lcMotIsencaoIva == .t. AND uf_gerais_validaIvaDireitoDeducao() == 0	 
				SELECT uCrsFT
				?""
				?"Resumo do IVA:"
				? "Taxa	     " + "Base     " + "IVA     " + "Total"
				&& Taxa 0%
				IF EIVAIN4Total > 0
					?TRANSFORM(uCrsFt.IVATX4,"99")+"%      "
					??TRANSFORM(EIVAIN4Total,lcMascaraDinheiro )+" "
					??TRANSFORM(EIVAV4Total,lcMascaraDinheiro )+" "
					??TRANSFORM(EIVAIN4Total + EIVAV4Total,lcMascaraDinheiro)+" "
				ENDIF
						
				&& Taxa 6%
				IF EIVAV1Total > 0
					?TRANSFORM(uCrsFt.IVATX1,"99")+"%      "
					??TRANSFORM(EIVAIN1Total,lcMascaraDinheiro )+" "
					??TRANSFORM(EIVAV1Total,lcMascaraDinheiro )+" "
					??TRANSFORM(EIVAIN1Total + EIVAV1Total,lcMascaraDinheiro)+" "
				ENDIF
				
				&& Taxa 13%
				IF 	EIVAV3Total > 0	
					?TRANSFORM(uCrsFt.IVATX3,"99")+"%      "
					??TRANSFORM(EIVAIN3Total,lcMascaraDinheiro )+" "
					??TRANSFORM(EIVAV3Total,lcMascaraDinheiro )+" "
					??TRANSFORM(EIVAIN3Total + EIVAV3Total,lcMascaraDinheiro)+" "
				ENDIF
				
				&& Taxa 23%
				IF EIVAV2Total > 0
					?TRANSFORM(uCrsFt.IVATX2,"99")+"%      "
					??TRANSFORM(EIVAIN2Total,lcMascaraDinheiro )+" "
					??TRANSFORM(EIVAV2Total,lcMascaraDinheiro )+" "
					??TRANSFORM(EIVAIN2Total + EIVAV2Total,lcMascaraDinheiro)+" "
				ENDIF
				
				&& Taxa 5
				IF uCrsFt.EIVAV5 != 0
					?TRANSFORM(uCrsFt.IVATX5,"99")+"%      "
					??TRANSFORM(uCrsFt.EIVAIN5,lcMascaraDinheiro )+" "
					??TRANSFORM(uCrsFt.EIVAV5,lcMascaraDinheiro )+" "
					??TRANSFORM(uCrsFt.EIVAIN5 + uCrsFt.EIVAV5,lcMascaraDinheiro + " ")+" "
				ENDIF
				
				&& Taxa 6
				IF uCrsFt.EIVAV6 != 0
					?TRANSFORM(uCrsFt.IVATX6,"99")+"%      "
					??TRANSFORM(uCrsFt.EIVAIN6,lcMascaraDinheiro )+" "
					??TRANSFORM(uCrsFt.EIVAV6,lcMascaraDinheiro )+" "
					??TRANSFORM(uCrsFt.EIVAIN6 + uCrsFt.EIVAV6,lcMascaraDinheiro + " ")+" "
				ENDIF
				
				&& Taxa 7
				IF uCrsFt.EIVAV7 != 0
					?TRANSFORM(uCrsFt.IVATX7,"99")+"%      "
					??TRANSFORM(uCrsFt.EIVAIN7,lcMascaraDinheiro )+" "
					??TRANSFORM(uCrsFt.EIVAV7,lcMascaraDinheiro )+" "
					??TRANSFORM(uCrsFt.EIVAIN7 + uCrsFt.EIVAV7,lcMascaraDinheiro + " ")+" "
				ENDIF
				
				&& Taxa 8
				IF uCrsFt.EIVAV8 != 0
					?TRANSFORM(uCrsFt.IVATX8,"99")+"%      "
					??TRANSFORM(uCrsFt.EIVAIN8,lcMascaraDinheiro )+" "
					??TRANSFORM(uCrsFt.EIVAV8,lcMascaraDinheiro )+" "
					??TRANSFORM(uCrsFt.EIVAIN8 + uCrsFt.EIVAV8,lcMascaraDinheiro + " ")+" "
				ENDIF
				
				&& Taxa 9
				IF uCrsFt.EIVAV9 != 0
					?TRANSFORM(uCrsFt.IVATX9,"99")+"%      "
					??TRANSFORM(uCrsFt.EIVAIN9,lcMascaraDinheiro )+" "
					??TRANSFORM(uCrsFt.EIVAV9,lcMascaraDinheiro )+" "
					??TRANSFORM(uCrsFt.EIVAIN9 + uCrsFt.EIVAV9,lcMascaraDinheiro + " ")+" "
				ENDIF
				
				&& Taxa 10
				IF uCrsFt.EIVAV10 != 0
					?TRANSFORM(uCrsFt.IVATX10,"99")+"%      "
					??TRANSFORM(uCrsFt.EIVAIN10,lcMascaraDinheiro )+" "
					??TRANSFORM(uCrsFt.EIVAV10,lcMascaraDinheiro )+" "
					??TRANSFORM(uCrsFt.EIVAIN10 + uCrsFt.EIVAV10,lcMascaraDinheiro + " ")+" "
				ENDIF
				
				&& Taxa 11
				IF uCrsFt.EIVAV11 != 0
					?TRANSFORM(uCrsFt.IVATX11,"99")+"%      "
					??TRANSFORM(uCrsFt.EIVAIN11,lcMascaraDinheiro )+" "
					??TRANSFORM(uCrsFt.EIVAV11,lcMascaraDinheiro )+" "
					??TRANSFORM(uCrsFt.EIVAIN11 + uCrsFt.EIVAV11,lcMascaraDinheiro + " ")+" "
				ENDIF
				
				&& Taxa 12
				IF uCrsFt.EIVAV12 != 0
					?TRANSFORM(uCrsFt.IVATX12,"99")+"%      "
					??TRANSFORM(uCrsFt.EIVAIN12,lcMascaraDinheiro )+" "
					??TRANSFORM(uCrsFt.EIVAV12,lcMascaraDinheiro )+" "
					??TRANSFORM(uCrsFt.EIVAIN12 + uCrsFt.EIVAV12,lcMascaraDinheiro + " ")+" "
				ENDIF
				
				&& Taxa 13
				IF uCrsFt.EIVAV13 != 0
					?TRANSFORM(uCrsFt.IVATX13,"99")+"%      "
					??TRANSFORM(uCrsFt.EIVAIN13,lcMascaraDinheiro )+" "
					??TRANSFORM(uCrsFt.EIVAV13,lcMascaraDinheiro )+" "
					??TRANSFORM(uCrsFt.EIVAIN13 + uCrsFt.EIVAV13,lcMascaraDinheiro + " ")+" "
				ENDIF
			ENDIF
			
			
			
			IF used("uCrsProdComp") or uCrsProdComp.total_compart_estado1 <> 0 OR uCrsProdComp.total_compart_ext1 <> 0 OR uCrsProdComp.total_compart_cli1 <>0
				?""
				?""
				?"Info. Produtos Comparticipados"
				IF uCrsProdComp.total_compart_estado1 <> 0
					??? CHR(29)+CHR(33)+CHR(16.5) && aumentar letra
					?"Custo Suportado p/Estado:" + TRANSFORM(uCrsProdComp.total_compart_estado1,"9999.99")
					??? CHR(29)+CHR(33)+CHR(0) && diminuir letra
				ENDIF
			
				IF uCrsProdComp.total_compart_cli1 <>0 OR uCrsProdComp.total_compart_estado1 <> 0
					??? CHR(29)+CHR(33)+CHR(16.5) && aumentar letra
					?"Custo Suportado p/Utente:"+TRANSFORM(uCrsProdComp.total_compart_cli1,"9999.99")
					??? CHR(29)+CHR(33)+CHR(0) && diminuir letra
				ENDIF
			
				
				IF uCrsProdComp.total_compart_ext1 <> 0
					?"Custo Suportado p/Entidade:	"+TRANSFORM(uCrsProdComp.total_compart_ext1,lcMascaraDinheiro )
				ENDIF
				
					
			ENDIF
			
			
		

			&& Impressao codigo do atendimento
			SELECT uCrsFta
			GO TOP
			IF !EMPTY(uCrsFta.u_nratend)
				?
				??	chr(27)+chr(97)+chr(1)
				?'Atendimento:'
				?
				??? chr(29) + chr(104) +chr(50) + chr(29)+ chr(72) + chr(2) + chr(29)+ chr(119) +chr(2) + chr(29)+chr(107)+chr(4) + UPPER(ALLTRIM(uCrsFta.u_nratend)) + chr(0)
				uf_gerais_textoLJPOS()
			ENDIF	
			
			SELECT uCrsFi
			GO TOP 
			SCAN 
				IF LEFT(ALLTRIM(uCrsFi.design),11)=='Ad. Reserva'
					lcexisteadres = .t.
				ENDIF 
			ENDSCAN 
			
			IF lcexisteadres = .t. AND USED("uCrsReservaBO")
				SELECT uCrsReservaBO
				IF len(alltrim(str(uCrsReservaBO.no)))<7
					lcIdReserva = 'R' + ALLTRIM(STR(uCrsReservaBO.obrano)) + 'C' + ALLTRIM(STR(uCrsReservaBO.no)) + 'D' + ALLTRIM(STR(uCrsReservaBO.estab))
				ELSE
					lcIdReserva = 'NRES' + ALLTRIM(STR(uCrsReservaBO.obrano)) + 'E'
				ENDIF 
				?
				??	chr(27)+chr(97)+chr(1)
				?'ID Reserva:'
				?
				???	 chr(29)+chr(104)+chr(50) + chr(29)+chr(72)+chr(2) + chr(29)+chr(119)+chr(2) + chr(29)+chr(107)+chr(4) + UPPER(ALLTRIM(lcIdReserva)) + chr(0)
				uf_gerais_textoLJPOS()					
			ENDIF 

			&& Centrar Informa��o
			uf_gerais_textoCJPOS()	

			&& Impress�o da informa��o relativa �s Campanhas	
			&& Verifica se tem campanhas aplicadas
			IF lcValidaPromo == .t.
				? ""
				??? CHR(29)+CHR(33)+CHR(16) && aumentar letra
				? "Campanhas"
				??? CHR(29)+CHR(33)+CHR(0) && diminuir letra
				? ALLTRIM(lcPromo)
			ENDIF
			

            IF myTipoCartao == 'Pontos'
                IF UPPER(ALLTRIM(lcMoeda)) == 'EURO'
                    && Valor do Vale de Desconto Aplicado
                    IF RECCOUNT("uCrsFiPromo")>0
                        ??? CHR(29)+CHR(33)+CHR(16) && aumentar letra
                        ? IIF(myTipoCartao == 'Pontos',"Vales Utilizados", "Valor Utilizado")
                        ??? CHR(29)+CHR(33)+CHR(0) && diminuir letra
                        SELECT uCrsFiPromo
                        GO TOP
                        SCAN
                            ? IIF(myTipoCartao == 'Pontos','Vale Desconto aplicado de ', 'Valor Desconto aplicado: ') + astr(ABS(uCrsFiPromo.u_epvp),9,2) + chr(27)+chr(116)+chr(19)+CHR(213)
                        ENDSCAN
                    ENDIF
                ELSE
                    && Valor do Vale de Desconto Aplicado
                    IF RECCOUNT("uCrsFiPromo")>0
                        ??? CHR(29)+CHR(33)+CHR(16) && aumentar letra
                        ? IIF(myTipoCartao == 'Pontos',"Vales Utilizados", "Valor Utilizado")
                        ??? CHR(29)+CHR(33)+CHR(0) && diminuir letra
                        SELECT uCrsFiPromo
                        GO TOP
                        SCAN
                            IF uf_gerais_getParameter("ADM0000000260","text")='USD'
                                ? IIF(myTipoCartao == 'Pontos','Vale Desconto aplicado de ', 'Valor Desconto aplicado: ') + astr(ABS(uCrsFiPromo.u_epvp),9,2) + chr(27)+chr(116)+chr(19)+CHR(36)
                            ELSE
                                ? IIF(myTipoCartao == 'Pontos','Vale Desconto aplicado de ', 'Valor Desconto aplicado: ') + astr(ABS(uCrsFiPromo.u_epvp),9,2) + " " + ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))
                            ENDIF 
                        ENDSCAN
                    ENDIF
                ENDIF 
            ENDIF
			
			IF myTipoCartao == 'Pontos'			
				&& Valor Pontos Ganhos com Cartao
				IF USED("uCrsUltTranCartao")
					IF !EMPTY(uCrsUltTranCartao.nrcartao)
                        
						? ""
						??? CHR(29)+CHR(33)+CHR(16) && aumentar letra
						? "Cartao Cliente"
						??? CHR(29)+CHR(33)+CHR(0) && diminuir letra

						? "Total Disponivel: " + astr(uCrsUltTranCartao.pontosAtrib-uCrsUltTranCartao.pontosUsa) + " pontos"						
						IF TYPE("myTotalPagCc")=="N"
							? "Ganho neste Atendimento: " + astr(myTotalPagCc) + " pontos"
							? "Total Ganho com Cartao: " + astr(uCrsUltTranCartao.pontosAtrib) + " pontos"
							? ""
						ELSE  
							? "Total Ganho com Cartao: " + astr(uCrsUltTranCartao.pontosAtrib) + " pontos"
							? ""
						ENDIF
					ENDIF
				ENDIF
			ENDIF 

            SELECT uCrsAtendCL
		
			SELECT uCrsFt
			IF myTipoCartao == 'Valor' AND uCrsFt.no <> 200 AND !EMPTY(lcVdNormal) AND !EMPTY(uCrsAtendCL.nrCartao)
				
		
				SELECT uCrsFt
				uf_imprimirPOS_devolveValoresCartaoAtendimento(uCrsFt.u_nratend)
			
				if(USED("uCrsFtResultTemp"))
					SELECT 	uCrsFtResultTemp
                    
					SELECT uCrsFt2
					? ""
					??? CHR(29)+CHR(33)+CHR(16) && aumentar letra
					? "Cartao Cliente"
					??? CHR(29)+CHR(33)+CHR(0) && diminuir letra	

					IF uCrsFtResultTemp.valcartao <> 0 OR uCrsFtResultTemp.valcartaoutilizado<>0 OR LcValcartaoAT <> 0
						IF UPPER(ALLTRIM(lcMoeda)) == 'EURO'
							IF uCrsFtResultTemp.valcartao > 0
								? 'Ganho neste Atendimento: ' + astr(ABS(uCrsFtResultTemp.valcartao),9,2) + chr(27)+chr(116)+chr(19)+CHR(213)
							ENDIF 
							IF LcValcartaoAT > 0 AND uCrsFtResultTemp.valcartao = 0
								? 'Ganho neste Atendimento: ' + astr(ABS(LcValcartaoAT),9,2) + chr(27)+chr(116)+chr(19)+CHR(213)
							ENDIF 
							IF uCrsFtResultTemp.valcartaoutilizado <> 0
								? 'Utilizado neste Atendimento: ' + astr(ABS(uCrsFtResultTemp.valcartaoutilizado),9,2) + ' ' + chr(27)+chr(116)+chr(19)+CHR(213)
							ENDIF 
						ELSE
							IF uf_gerais_getParameter("ADM0000000260","text")='USD'
								IF uCrsFtResultTemp.valcartao > 0 
									? 'Ganho neste Atendimento: ' + astr(uCrsFtResultTemp.valcartao) + ' ' + chr(27)+chr(116)+chr(19)+CHR(36)
								ENDIF 
								IF LcValcartaoAT > 0 AND uCrsFtResultTemp.valcartao = 0
									? 'Ganho neste Atendimento: ' + astr(ABS(LcValcartaoAT),9,2) + ' ' + chr(27)+chr(116)+chr(19)+CHR(36)
								ENDIF 
								IF uCrsFt2.valcartaoutilizado <> 0
									? 'Utilizado neste Atendimento: ' + astr(uCrsFtResultTemp.valcartaoutilizado) + ' ' + chr(27)+chr(116)+chr(19)+CHR(36)
								ENDIF 
							ELSE
								IF uCrsFt2.valcartao > 0
									? 'Ganho neste Atendimento: ' + astr(uCrsFtResultTemp.valcartao) + ' ' + ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))
								ENDIF 
								IF LcValcartaoAT > 0 AND uCrsFtResultTemp.valcartao = 0
									? 'Ganho neste Atendimento: ' + astr(ABS(LcValcartaoAT),9,2) + ' ' + ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))
								ENDIF 
								IF uCrsFtResultTemp.valcartaoutilizado <> 0
									? 'Utilizado neste Atendimento: ' + astr(uCrsFtResultTemp.valcartaoutilizado) + ' ' + ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))
								ENDIF 
							ENDIF 
						ENDIF 
						? ""
					ENDIF
				ENDIF  
				fecha("uCrsFtResultTemp")

                SELECT uCrsFt2
                IF UPPER(ALLTRIM(lcMoeda)) == 'EURO'
                    ? 'Disponivel: ' + astr(ABS(lcValDispCartao),9,2) + ' ' + chr(27)+chr(116)+chr(19)+CHR(213)
                    ? 'Total Ganho em Cartao: ' + astr(ABS(lcTotalHistCartao),9,2) + ' ' + chr(27)+chr(116)+chr(19)+CHR(213)
                ELSE
                    IF uf_gerais_getParameter("ADM0000000260","text")='USD'
                        ? 'Dispon�vel: ' + astr(lcValDispCartao) + ' ' + chr(27)+chr(116)+chr(19)+CHR(36)
                        ? 'Total Ganho em Cart�o: ' + astr(lcTotalHistCartao) + ' ' + chr(27)+chr(116)+chr(19)+CHR(36)
                    ELSE
                        ? 'Dispon�vel: ' + astr(lcValDispCartao) + ' ' + ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))
                        ? 'Total Ganho em Cart�o: ' + astr(lcTotalHistCartao) + ' ' + ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))
                    ENDIF 
                    ? ""
                ENDIF 

			ENDIF
			 
					
			&& Impress�o rodap� apenas dispon�vel para Reg. a Cliente
			??? chr(27)+chr(97)+chr(1)
			IF uCrsFt.u_tipodoc == 2 OR uCrsFt.u_tipodoc == 7
				?
				?"      ----------------------------------"
				?"         O Cliente"
			ENDIF
		
*!*				&& Texto Relativo � isen��o de IVa
*!*				IF lcMotIsencaoIva == .t. AND uf_gerais_validaIvaDireitoDeducao() == 0
*!*			 		IF lcIsentoIva
*!*						DO CASE
*!*							CASE ucrsFt.pais==3
*!*								?"Nota (*1): Isencao prevista no n. 3 do art. 14. do CIVA"
*!*							CASE ucrsFt.pais==2
*!*								?"Nota (*1): Isencao ao abrigo da alinea a) do art. 14. do RITI"
*!*							OTHERWISE 
*!*								? ALLTRIM(ucrse1.motivo_isencao_iva)
*!*								**?"Nota (*1): Isencao Prevista Art. 9. do CIVA (ou similar)"
*!*						ENDCASE 
*!*					ENDIF
*!*				ENDIF
			
			&& 
			
			LOCAL lcValidaCert, lcNrCert, lcCharCert
			STORE '' TO lcNrCert, lcCharCert
			
			SELECT uCrsFT
			GO TOP
			SCAN
				SELECT uCrsImpTalaoPos
				LOCATE FOR ALLTRIM(uCrsImpTalaoPos.stamp) == ALLTRIM(uCrsFt.ftstamp)
				&&IF !(uCrsimpTalaoPos.susp) AND !(uCrsImpTalaoPos.tipo==myRegCli) AND !(uCrsImpTalaoPos.tipo=85) AND !(uCrsImpTalaoPos.tipo=903) AND !(uCrsImpTalaoPos.tipo=901) AND !(uCrsImpTalaoPos.tipo=904) AND !(uCrsImpTalaoPos.tipo=905)
				IF !(uCrsimpTalaoPos.susp) AND !(uCrsImpTalaoPos.tipo==myRegCli) AND !(uCrsImpTalaoPos.tipo=85) AND !(uCrsImpTalaoPos.tipo=903) AND !(uCrsImpTalaoPos.tipo=901) AND !(uCrsImpTalaoPos.tipo=904) AND !(uCrsImpTalaoPos.tipo=905) AND EMPTY(uCrsimpTalaoPos.AdiReserva)
					IF uf_gerais_actGrelha("",[uCrsIduCert],[exec up_gerais_iduCert ']+ALLTRIM(uCrsFt.ftstamp)+['])
						IF RECCOUNT()>0
							IF uCrsIduCert.temCert
								IF !lcValidaCert
									IF lcMotIsencaoIva == .t. AND uf_gerais_validaIvaDireitoDeducao() == 0 AND uf_gerais_getParameter_site("ADM0000000166", "BOOL", mySite)
										?'IVA INCLUIDO'
									ENDIF
								ENDIF
								
								IF myNrVendasTalao>1
									?"Referente a: "
									IF (uCrsFt.u_tipodoc=8) OR (uCrsFt.ndoc=32) OR (uCrsFt.ndoc=31) OR (uCrsFt.ndoc=84)
										IF YEAR(uCrsFt.fdata) > 2012
											??"Fatura-Recibo Nr."
										ELSE
											??"Vnd. a Dinheiro Nr."
										ENDIF 
										??astr(uCrsFt.FNO)+" "
									ENDIF
									IF (uCrsFt.u_tipodoc=9) OR (uCrsFt.ndoc=11) OR (uCrsFt.ndoc=12)
										??"Fatura Nr."
										??astr(uCrsFt.FNO)+" "
									ENDIF
									IF (uCrsFt.ndoc=4)
										??"Dev. a Cliente Nr."
										??astr(uCrsFt.FNO)+" "
									EndIf
									** IMPRIMIR TAG IDENTIFICADORA DO DOCUMENTO DE VENDA **
										DO case
									CASE (uCrsFt.u_tipodoc=8 OR uCrsFt.ndoc=84) AND uCrsFt.u_lote=0 AND uCrsFt.cobrado=.f.
											??"(VD)"
										
										CASE (uCrsFt.u_tipodoc=8 OR uCrsFt.ndoc=84) AND uCrsFt.u_lote > 0
											??"(VD-CR)"
										
										CASE (uCrsFt.u_tipodoc=9) AND uCrsFt.cobrado=.f. AND uCrsFt.u_lote=0
											??"(FT)"
										
										CASE (uCrsFt.u_tipodoc=9) AND uCrsFt.u_lote > 0
											??"(FT-CR)"
											
										CASE (uCrsFt.u_tipodoc=11)
		 									??"(VDM)"
		 									
		 								OTHERWISE
		 									??"(   )"
		 							ENDCASE
						
									** Imprimir serie de factura��o **
									??"/" + astr(uCrsFt.ndoc)
									** imprimir hash
									?? ' - '+ALLTRIM(uCrsIduCert.parte1)

                                 **   ?""

*!*	                                    IF uf_gerais_actgrelha("", "FTQRCODE", "exec up_print_qrcode_a4 '"+ALLTRIM(ucrsFT.ftstamp)+"'")
*!*	                                        IF RECCOUNT("FTQRCODE") <> 0
*!*	                                            SELECT FTQRCODE
*!*	                                            GO TOP 

*!*	                                            uv_qr = uf_imprimirgerais_gerarQRCode(ALLTRIM(FTQRCODE.qrcode))

*!*	                                            ?""
*!*	                                            ???uv_qr
*!*	                                            ?""

*!*	                                        ENDIF
*!*	                                    ENDIF

								ENDIF
								
								lcValidaCert 	= .t.
								
								lcCharCert		= ALLTRIM(uCrsIduCert.parte1)
								lcNrCert 		= ALLTRIM(uCrsIduCert.parte2) + ' ' + ALLTRIM(uCrsFt2.u_docorig)
							ENDIF
							
							SELECT uCrsStampsQRCode
							APPEND BLANK 
							replace uCrsStampsQRCode.ftstamp WITH ALLTRIM(uCrsFt.ftstamp)
							
						ENDIF
						fecha("uCrsIduCert")
						
					ENDIF
				ENDIF 	
				SELECT uCrsFt
			ENDSCAN
			
						
			? 'Os bens/servicos adquiridos foram colocados a'
			? 'disposicao do adquirente na data do documento'
			&& Certifica��o
			IF !lcValidaCert
				IF uf_gerais_actGrelha("","ucrsRodapeCert",[exec up_cert_TextoRodape '] + ALLTRIM(uCrsFt.tiposaft) + ['])
					**Alterado novas regras certifica��o 20120313		
					?ALLTRIM(ucrsRodapeCert.rodape)
				ELSE
					IF lcMotIsencaoIva == .t. AND uf_gerais_validaIvaDireitoDeducao() == 0 AND uf_gerais_getParameter_site("ADM0000000166", "BOOL", mySite)
						?"Processado por Computador - IVA INCLUIDO"
					ENDIF
				ENDIF
			ELSE
				IF myNrVendasTalao<=1
					? ALLTRIM(lcCharCert)+'-'+ALLTRIM(lcNrCert)
				ELSE
					? ALLTRIM(lcNrCert)
				ENDIF
			ENDIF	

			&& Publicidade 
			? "Logitools, Lda"
			
			&& Identificar Operador
			SELECT uCrsFt
			GO TOP 
			?"Atendido Por: "
			IF lcValidaCodAlt == 0
				??Substr(uCrsFT.vendnm,1,30) + " (" + Alltrim(Str(uCrsFt.vendedor)) + ")"
				?""
			ELSE
				??Substr(uCrsFT.vendnm,1,30) + " (" + Alltrim(Str(myOpAltCod)) + ")"
				?""
			ENDIF

			&& Texto rodap� Tal�es
			uf_gerais_criarRodapePOS()
			
*!*				&& Funcoes abrir Gaveta e Cortar Papel
*!*				IF !uf_gerais_getParameter_site("ADM0000000149", "BOOL", mysite)
*!*					uf_gerais_feedCutPOS()
*!*				ENDIF 
			IF reccount("uCrsStampsQRCode")=0 OR UPPER(mypaisconfsoftw)!='PORTUGAL'
				uf_gerais_feedCutPOS()
			ENDIF 
			uf_gerais_openDrawPOS()
		
*!*				&&verificar se imprime tal�o com n� de atendimento separado
*!*				IF uf_gerais_getParameter("ADM0000000207","BOOL")
*!*					** IMPRESSAO DO CODIGO DO ATENDIMENTO **
*!*					SELECT uCrsFta
*!*					GO TOP
*!*					IF !EMPTY(uCrsFta.u_nratend)
*!*						?""
*!*						?""
*!*						??	chr(27)+chr(97)+chr(1)
*!*						?'Atendimento:'
*!*						?
*!*						???	  chr(29) + chr(104) +chr(50) + chr(29)+chr(72)+chr(2) + chr(29)+chr(119)+chr(2) + chr(29)+chr(107)+chr(4) + UPPER(ALLTRIM(uCrsFta.u_nratend)) + chr(0)
*!*						uf_gerais_textoLJPOS()
*!*					
*!*						?"Total Documento   :"
*!*						**??TRANSFORM(docTotal,"99999999999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(213)
*!*						
*!*						IF EMPTY(uf_gerais_getParameter("ADM0000000260","text")) or UPPER(ALLTRIM(uf_gerais_getParameter("ADM0000000260","text"))) == "EURO"
*!*							??TRANSFORM(docTotal,"99999999999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(213)
*!*						ELSE
*!*							IF uf_gerais_getParameter("ADM0000000260","text")='USD'
*!*								??TRANSFORM(docTotal,"99999999999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(36)
*!*							ELSE
*!*								??TRANSFORM(docTotal,"99999999999999.99 ") + " " + ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))
*!*							ENDIF 
*!*						ENDIF 
*!*						
*!*						uf_gerais_feedCutPOS()
*!*					ENDIF
*!*				ENDIF
			
			SELECT uCrsFt
			GO TOP 
			**PSICO**
			** VERIFICAR SE � NECESS�RIO IMPRIMIR O TALAO DE PSICOTROPICOS **
			SELECT uCrsFi
			GO TOP
			SCAN
				IF uCrsFi.u_psico=.t.
					lcPsico = lcPsico + 1
				ENDIF
			ENDSCAN
			
*!*				** CASO SEJA NECESS�RIO, IMPRIMIR O TAL�O DE PSICOTROPICOS
*!*				IF lcPsico > 0
*!*					LOCAL lcStampDuplicado, lcPosCrsFiPsico
*!*					STORE '' TO lcStampDuplicado
*!*					STORE 0 TO lcPosCrsFiPsico
*!*					
*!*					SELECT uCrsFi
*!*					GO TOP
*!*					SCAN FOR uCrsFi.u_psico = .t.
*!*						IF ALLTRIM(lcStampDuplicado) != ALLTRIM(uCrsFi.ftstamp)
*!*							lcPosCrsFiPsico = RECNO("uCrsFi")
*!*							lcStampDuplicado = ALLTRIM(uCrsFi.ftstamp)
*!*							uf_imprimirpos_ImpPrevPsico(ALLTRIM(uCrsFi.ftstamp), .f., "ATENDIMENTO")
*!*							TRY 
*!*								SELECT uCrsFi
*!*								GO lcPosCrsFiPsico 
*!*							CATCH
*!*								***
*!*							ENDTRY 
*!*						ENDIF 
*!*					ENDSCAN
*!*				ENDIF 
		ENDIF

		uf_gerais_setImpressoraPOS(.f.)

		IF reccount("uCrsStampsQRCode")>0 AND UPPER(mypaisconfsoftw)=='PORTUGAL'
			** QRCODES
			LOCAL lcTokenQrcode, lcordemQRCode
			STORE 1 TO lcordemQRCode
			lcTokenQrcode = uf_gerais_stamp()
			SELECT uCrsStampsQRCode
			GO TOP 
			SCAN
				uf_gerais_gerar_qrcodejpg(ALLTRIM(uCrsStampsQRCode.ftstamp), ALLTRIM(lcTokenQrcode), lcordemQRCode)
				lcordemQRCode = lcordemQRCode + 1
			ENDSCAN 
			** Indicar o corte na �ltima imagem
			IF uf_gerais_getParameter_site("ADM0000000149", "BOOL", mysite)
				TEXT TO lcsql TEXTMERGE NOSHOW
				 	update 
				 		image_printPos 
				 	set 
				 		feedcut=1 
				 	where 
				 		token='<<ALLTRIM(lcTokenQrcode)>>' 
				 		and [order]=(select max([order]) from image_printPos (nolock) where token='<<ALLTRIM(lcTokenQrcode)>>')
			    ENDTEXT
			    uf_gerais_actgrelha("", "", lcsql)
			ENDIF 
			
			STORE '&' TO lcadd
			lcwspath = ALLTRIM(uf_gerais_getparameter("ADM0000000185", "TEXT"))+'\ltsPrinter\ltsPrinter.jar'+ ' ' + '"--idCl='+UPPER(ALLTRIM(sql_db))+'" '+' "--token='+ALLTRIM(lcTokenQrcode)+'"'
			lcwsdir = ALLTRIM(uf_gerais_getparameter("ADM0000000185", "TEXT"))+'\ltsPrinter'
			lcwspath = "cmd /c cd /d "+lcwsdir+" "+lcadd+lcadd+" javaw -jar "+lcwspath


			owsshell = CREATEOBJECT("WScript.Shell")
			owsshell.run(lcwspath, 0, .T.)
			
			&& Funcoes abrir Gaveta e Cortar Papel
			IF !uf_gerais_getParameter_site("ADM0000000149", "BOOL", mysite)
				uf_gerais_setImpressoraPOS(.t.)
				uf_gerais_feedCutPOS()
				uf_gerais_setImpressoraPOS(.f.)
			ENDIF
		ENDIF 
		
		

		
		IF USED("uCrsStampsQRCode")
			fecha("uCrsStampsQRCode")
		ENDIF 
		
		** CASO SEJA NECESS�RIO, IMPRIMIR O TAL�O DE PSICOTROPICOS
		IF lcPsico > 0
			LOCAL lcStampDuplicado, lcPosCrsFiPsico
			STORE '' TO lcStampDuplicado
			STORE 0 TO lcPosCrsFiPsico
			
			SELECT uCrsFi
			GO TOP
			SCAN FOR uCrsFi.u_psico = .t.
				IF ALLTRIM(lcStampDuplicado) != ALLTRIM(uCrsFi.ftstamp)
					lcPosCrsFiPsico = RECNO("uCrsFi")
					lcStampDuplicado = ALLTRIM(uCrsFi.ftstamp)
					uf_gerais_setImpressoraPOS(.t.)
					uf_imprimirpos_ImpPrevPsico(ALLTRIM(uCrsFi.ftstamp), .f., "ATENDIMENTO")
					uf_gerais_feedCutPOS()
					uf_gerais_setImpressoraPOS(.f.)
					TRY 
						SELECT uCrsFi
						GO lcPosCrsFiPsico 
					CATCH
						***
					ENDTRY 
				ENDIF 
			ENDSCAN
		ENDIF
			
		
		&&verificar se imprime tal�o com n� de atendimento separado
		IF uf_gerais_getParameter("ADM0000000207","BOOL")
			** IMPRESSAO DO CODIGO DO ATENDIMENTO **
			SELECT uCrsFta
			GO TOP
			IF !EMPTY(uCrsFta.u_nratend)
				if(uf_gerais_setImpressoraPOS(.t.))
					?""
					?""
					??	chr(27)+chr(97)+chr(1)
					?'Atendimento:'
					?
					???	  chr(29) + chr(104) +chr(50) + chr(29)+chr(72)+chr(2) + chr(29)+chr(119)+chr(2) + chr(29)+chr(107)+chr(4) + UPPER(ALLTRIM(uCrsFta.u_nratend)) + chr(0)
					uf_gerais_textoLJPOS()
				
					?"Total Documento   :"
					**??TRANSFORM(docTotal,"99999999999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(213)
					
					IF EMPTY(uf_gerais_getParameter("ADM0000000260","text")) or UPPER(ALLTRIM(uf_gerais_getParameter("ADM0000000260","text"))) == "EURO"
						??TRANSFORM(docTotal,"99999999999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(213)
					ELSE
						IF uf_gerais_getParameter("ADM0000000260","text")='USD'
							??TRANSFORM(docTotal,"99999999999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(36)
						ELSE
							??TRANSFORM(docTotal,"99999999999999.99 ") + " " + ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))
						ENDIF 
					ENDIF 
					
					uf_gerais_feedCutPOS()
				ENDIF
				
				uf_gerais_setImpressoraPOS(.f.)
			ENDIF
		ENDIF 

	ENDFOR 
	
	IF USED("uCrsFtA")
        fecha("uCrsFtA")
    ENDIF 
    IF USED("uCrsFt2A")
        fecha("uCrsFt2A")
    ENDIF 
    IF USED("uCrsFiA")
        fecha("uCrsFiA")
    ENDIF 
     
    IF USED("uCrsStampsUtilizados")
	    fecha("uCrsStampsUtilizados")   
	ENDIF 
        
	usacampanha=0
	lcRepete=1
ENDFUNC


** Funcao impress�o talao movimento de Caixa
FUNCTION uf_imprimirpos_impTalaoMovimentoCaixa
	LPARAMETERS pTipo, pValor, pMotivo, pData, pHora, pNMovimento
	

    **IF uf_gerais_getParameter_site('ADM0000000136', 'BOOL', mySite)
    IF uf_gerais_getUmValor("B_terminal", "usaDriverWin", "terminal = '" + ALLTRIM(myTerm) + "'")

        CREATE CURSOR uc_PrintMovCX (tipo c(25), valor c(50), motivo c(254), data c(10), hora c(8), nMovimento c(30), operador c(50))

        SELECT uc_PrintMovCX
        APPEND BLANK

        REPLACE uc_PrintMovCX.tipo WITH ALLTRIM(pTipo)
        REPLACE uc_PrintMovCX.valor WITH ALLTRIM(pValor)
        REPLACE uc_PrintMovCX.motivo WITH ALLTRIM(pMotivo)
        REPLACE uc_PrintMovCX.data WITH ALLTRIM(uf_gerais_getDate(pData,"DATA"))
        REPLACE uc_PrintMovCX.hora WITH ALLTRIM(pHora)
        REPLACE uc_PrintMovCX.nMovimento WITH ALLTRIM(pNMovimento)
        REPLACE uc_PrintMovCX.operador WITH ALLTRIM(ch_vendnm)

        uv_idImp = uf_gerais_getUmValor("B_impressoes", "id", "tabela = 'CX' and nomeImpressao = 'Tal�o Movimento Caixa'")

        uf_imprimirgerais_sendprinter_talao(uv_idImp , .f., .F.)

    ELSE

        lcValidaImp = uf_gerais_setImpressoraPOS(.t.)
            
        uf_gerais_criarCabecalhoPOS()
        
        uf_gerais_textoVermPOS()
        ?PADC(pTipo,uf_gerais_getParameter("ADM0000000194","NUM")-12," ") && -12 porque o tipo de letra � maior
        uf_gerais_resetCorPOS()
        
        uf_gerais_separadorPOS()
        
        ?""
        ?"Data: "
        ??uf_gerais_getDate(pData,"DATA")
        ?"Hora: "
        ??pHora
        ?"Operador: "
        ??ch_vendnm
        ?"N. Movimento: "
        ??pNMovimento
        
        ?""
        ?"Valor: "
        ??TRANSFORM(pValor,"99999999.99")
        
        ?""
        ?"Motivo: "
        ??pMotivo 
        
        uf_gerais_feedCutPOS()
        **uf_gerais_openDrawPOS()
        
        uf_gerais_setImpressoraPOS(.f.)

    ENDIF

ENDFUNC

FUNCTION uf_imprimirpos_limpaCursoresReimprimirAtend()
		
	&&limpa cursores
	
	IF USED("uCrsInfoAtendPagCentral")
		fecha("uCrsInfoAtendPagCentral")
	ENDIF
	
	
	IF USED("uCrsFtAux")
		fecha("uCrsFtAux")
	ENDIF
	
	IF USED("uCrsFiAux")
		fecha("uCrsFiAux")
	ENDIF
	
		
	IF USED("uCrsFt2Aux")		
		fecha("uCrsFt2Aux")
	ENDIF
	
	IF USED("uCrsFtHelper")
		fecha("uCrsFtHelper")
	ENDIF
	
	IF USED("uCrsFtHelperAux")
		fecha("uCrsFtHelperAux")
	ENDIF
	
	IF USED("uCrsFiHelper")
		fecha("uCrsFiHelper")
	ENDIF
	
		
	IF USED("uCrsFt2Helper")		
		fecha("uCrsFt2Helper")
	ENDIF
	

ENDFUNC



** Funcao para validar reimpress�o do tal�o Lu�s Leal
FUNCTION uf_imprimirPOS_ValImpAtendimento

	LOCAL lcNrAtend, lcData, lcNoCliente, lcSQL

	SELECT uCrsPesqVendas
	
	lcNrAtend = uCrsPesqVendas.u_nratend
	lcData = LEFT(uCrsPesqVendas.fdata,4)
	lcNoCliente = uCrsPesqVendas.no
	 
	&& verifica se o Cursor tem dados
	IF !(RECCOUNT("uCrsPesqVendas")>0)
		uf_perguntalt_chama("N�O EXISTEM DADOS PARA IMPRIMIR.","OK","",64)
		RETURN .F.
	ENDIF
	
	&& Valida tipo de documento
	IF (ucrsPesqVendas.u_tipodoc == 3)
		uf_perguntalt_chama("N�O � POSS�VEL IMPRIMIR TAL�O PARA ESTE TIPO DE DOCUMENTO.","OK","",64)
		RETURN .F.
	ENDIF
	
	&&limpa cursores
	uf_imprimirpos_limpaCursoresReimprimirAtend()
			
	lcSQL = ''
	TEXT TO lcSQL NOSHOW TEXTMERGE
		select * from B_pagcentral (nolock) WHERE nrAtend = '<<lcNrAtend>>' AND ano = '<<lcData>>' AND NO = '<<lcNoCliente>>'
	ENDTEXT 
	IF !uf_gerais_actGrelha("", "uCrsInfoAtendPagCentral", lcSql)
		uf_perguntalt_chama("N�o foi poss�vel encontrar a informa��o do atendimento. Por favor contacte o Suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF
	
	lcSQL = ''	
	TEXT TO lcSQL NOSHOW TEXTMERGE
		select td.u_tipodoc,b_utentes.nrcartao, td.tiposaft, b_utentes.no_ext,ft.* FROM ft (nolock) INNER JOIN td (nolock) ON td.ndoc = ft.ndoc INNER JOIN b_utentes (nolock) ON b_utentes.no = ft.no AND b_utentes.estab = ft.estab WHERE u_nratend = '<<lcNrAtend>>' AND ftano = '<<lcData>>' AND ft.site = '<<mysite>>' AND FT.NO = '<<lcNoCliente>>' AND td.u_tipodoc != 3 
	ENDTEXT 
	IF !uf_gerais_actGrelha("", "uCrsFtHelper", lcSql)
		uf_perguntalt_chama("N�o foi poss�vel encontrar a informa��o das vendas [FT]. Por favor contacte o Suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF	
	
	
	

	lcSQL = ''
	TEXT TO lcSQL NOSHOW TEXTMERGE
		SELECT ft.ftstamp,ft.tipodoc,ft.cobrado,isnull(fi2.codmotiseimp,'') as codmotiseimp, isnull(fi2.motiseimp,'') as motiseimp, isnull(fi2.motiseimp,'') as motisencao,fi.* FROM fi (nolock) INNER JOIN ft (nolock) ON ft.ftstamp = fi.ftstamp INNER JOIN td (nolock) ON td.ndoc = ft.ndoc left join fi2 (nolock) on fi2.fistamp=fi.fistamp WHERE ft.u_nratend = '<<lcNrAtend>>' AND ft.ftano = '<<lcData>>' AND ft.site = '<<mysite>>' AND FT.NO = '<<lcNoCliente>>'  AND td.u_tipodoc != 3 
	ENDTEXT 
	IF !uf_gerais_actGrelha("", "uCrsFiHelper", lcSql)
		uf_perguntalt_chama("N�o foi poss�vel encontrar a informa��o das vendas [FI]. Por favor contacte o Suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF	
	
	lcSQL = ''
	TEXT TO lcSQL NOSHOW TEXTMERGE
		select ft2.*,ft.cobrado,ft.ftstamp FROM ft2 (nolock) INNER JOIN ft (nolock) ON ft.ftstamp = ft2.ft2stamp WHERE ft.u_nratend = '<<lcNrAtend>>' AND ft.ftano = '<<lcData>>' AND  ft.site = '<<mysite>>' AND FT.NO = '<<lcNoCliente>>' 
	ENDTEXT 
	
	IF !uf_gerais_actGrelha("", "uCrsFt2Helper", lcSql)
		uf_perguntalt_chama("N�o foi poss�vel encontrar a informa��o das vendas [FT2]. Por favor contacte o Suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF	

	
	&& imprime atendimento se nao suspensas
	SELECT * FROM  uCrsFtHelper  WHERE cobrado=.f. INTO CURSOR uCrsFtAux READWRITE 
	SELECT * FROM  uCrsFt2Helper WHERE cobrado=.f. INTO CURSOR uCrsFt2Aux READWRITE 
	SELECT * FROM  uCrsFiHelper  WHERE cobrado=.f. INTO CURSOR uCrsFiAux READWRITE 
	
	
	
	&& se nao suspensas
	IF(RECCOUNT("uCrsFtAux")>0)
		uf_imprimirpos_ReimprimirAtend()
	ENDIF
	
	SELECT * FROM  uCrsFtHelper  WHERE cobrado=.t. INTO CURSOR uCrsFtHelperAux  READWRITE 

	&&Imprimir suspensas em taloes diferentes
	SELECT uCrsFtHelperAux  
	GO TOP
	SCAN 
			
			SELECT * FROM  uCrsFtHelper  WHERE ALLTRIM(uCrsFtHelper.ftstamp)=ALLTRIM(uCrsFtHelperAux.ftstamp) INTO CURSOR uCrsFtAux READWRITE 
			SELECT * FROM  uCrsFt2Helper WHERE ALLTRIM(uCrsFt2Helper.ftstamp)=ALLTRIM(uCrsFtHelperAux.ftstamp) INTO CURSOR uCrsFt2Aux READWRITE 
			SELECT * FROM  uCrsFiHelper  WHERE ALLTRIM(uCrsFiHelper.ftstamp)=ALLTRIM(uCrsFtHelperAux.ftstamp) INTO CURSOR uCrsFiAux READWRITE 
			

			&& se suspensas
			IF(RECCOUNT("uCrsFtAux")>0)
				uf_imprimirpos_ReimprimirAtend()
			ENDIF
	
	ENDSCAN
	

	&&limpa cursores
	uf_imprimirpos_limpaCursoresReimprimirAtend()
	
ENDFUNC	

FUNCTION uf_imprimirPOS_devolveValoresCartaoAtendimento
		LPARAM	lcNrAtend


		IF uf_gerais_actGrelha("",[uCrsFtResultTemp],"exec up_gerais_at_devolve_total_cartao '" + ALLTRIM(mySite)  + "','" + ALLTRIM(lcNrAtend) + "'")
			IF RECCOUNT("uCrsFtResultTemp")>0
				RETURN .t.
			ENDIF
		ENDIF
	
		
				
		RETURN .f.
		
endfunc		


** Funcao Reimprimir Atendimento (Imprime todos os documentos de um mesmo atendimento) - Lu�s Leal

FUNCTION uf_imprimirpos_ReimprimirAtend
	
	
	LOCAL lcSQL,lcMoeda, lcMotIsencaoIva, uc_tempNO, uc_tempEstab
	STORE '' TO lcSQL, lcMoeda 
	STORE  .t. TO lcMotIsencaoIva 
    STORE  0 TO uc_tempNO, uc_tempEstab
	
	&& verifica se deve imprimir com que simbolo - Euro // USD 
	lcMoeda = uf_gerais_getParameter("ADM0000000260","text")	

	IF EMPTY(lcMoeda)
		lcMoeda = 'EURO'
	ENDIF
	
	lcSQL = ''
	TEXT TO lcSQL NOSHOW TEXTMERGE
		exec up_print_docs_atendimento '<<uCrsFtAux.ftstamp>>', '<<uCrsFtAux.u_nratend>>', ''
	ENDTEXT 
	IF !uf_gerais_actGrelha("", "uCrsProdComp", lcSql)
		uf_perguntalt_chama("N�o foi poss�vel encontrar a informa��o das vendas [FI]. Por favor contacte o Suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF
							
	LOCAL lcMascaraDinheiro
	lcMascaraDinheiro = uf_imprimirpos_devolveMascaraPvpTalao()
	
	&& Verifica utiliza a informa��o do IVA no Tal�o
	lcMotIsencaoIva = uf_gerais_getParameter("ADM0000000261","Bool")	
	

	&& CALCULAR DADOS PARA O CABE�ALHO E PARA OS TOTAIS
	LOCAL lcCod, descTotal, descUtente, docTotal, docTotalSemDesc, eivain1Total, eivain2Total, eivain3Total, eivain4Total, eivav1Total, eivav2Total, eivav3Total, eivav4Total
	LOCAL eivain5Total, eivain6Total, eivain7Total, eivain8Total, eivain9Total, eivain10Total, eivain11Total, eivain12Total, eivain13Total, eivav5Total, eivav6Total, eivav7Total, eivav8Total, eivav9Total, eivav10Total, eivav11Total, eivav12Total, eivav13Total
	LOCAL nrTaloes, lcTroco, lcTotal, lcModoPag1, lcModoPag2, lcModoPag3, lcModoPag4, lcModoPag5, lcModoPag6, lcModoPag7, lcModoPag8, lcIsentoIva, lcTamanhoDesign, lcNrCartao, lcCountVendasSusp, lcTotalAtend 
	STORE 0 To descTotal, descUtente, docTotal, docTotalSemDesc, eivain1Total, eivain2Total, eivain3Total, eivain4Total, eivav1Total, eivav2Total, eivav3Total, eivav4Total, lcCountVendasSusp, lcTotalAtend 
	STORE 0 TO eivain5Total, eivain6Total, eivain7Total, eivain8Total, eivain9Total, eivain10Total, eivain11Total, eivain12Total, eivain13Total, eivav5Total, eivav6Total, eivav7Total, eivav8Total, eivav9Total, eivav10Total, eivav11Total, eivav12Total, eivav13Total
	STORE 0 TO nrTaloes,lcTroco, lcTotal, lcModoPag1, lcModoPag2, lcModoPag3, lcModoPag4, lcModoPag5, lcModoPag6, lcModoPag7, lcModoPag8, lcTamanhoDesign, docTotal
	STORE .f. TO lcCod, lcIsentoIva
	STORE '' TO  lcNrCartao
	
	
	CREATE CURSOR uCrsStampsQRCode (ftstamp c(50))
	
	
		
	
	&& Verifica cart�o cliente
	SELECT uCrsFtAux
	GO TOP 
    
	SCAN
	LOCATE FOR uCrsFtAux.nrcartao != ''
	IF FOUND()
		lcNrCartao = ALLTRIM(uCrsFtAux.nrcartao)
	ENDIF
	ENDSCAN
	&& Atribui valores pagamento � no atendimento 
	SELECT uCrsInfoAtendPagCentral
	lcTroco = uCrsInfoAtendPagCentral.etroco
	lcTotal = uCrsInfoAtendPagCentral.total_bruto
	lcModoPag1  = uCrsInfoAtendPagCentral.evdinheiro_semTroco
	lcModoPag2  = uCrsInfoAtendPagCentral.epaga1
	lcModoPag3  = uCrsInfoAtendPagCentral.epaga2
	lcModoPag4  = uCrsInfoAtendPagCentral.echtotal
	lcModoPag5  = uCrsInfoAtendPagCentral.epaga3
	lcModoPag6  = uCrsInfoAtendPagCentral.epaga4
	lcModoPag7  = uCrsInfoAtendPagCentral.epaga5
	lcModoPag8  = uCrsInfoAtendPagCentral.epaga6
	
	&& Atribui Valores IVA e valor Total Atendimento
	SELECT uCrsFtAux
	GO TOP
	SCAN
		IF uCrsFTAux.u_tipodoc != 2 
			docTotal 		= docTotal + uCrsFtAux.etotal
			descTotal		= descTotal + uCrsFtAux.edescc
			eivain1Total 	= eivain1Total + uCrsFtAux.EIVAIN1
			eivain2Total 	= eivain2Total + uCrsFtAux.EIVAIN2
			eivain3Total 	= eivain3Total + uCrsFtAux.EIVAIN3
			eivain4Total 	= eivain4Total + uCrsFtAux.EIVAIN4
			eivain5Total 	= eivain5Total + uCrsFtAux.EIVAIN5
			eivain6Total 	= eivain6Total + uCrsFtAux.EIVAIN6
			eivain7Total 	= eivain7Total + uCrsFtAux.EIVAIN7
			eivain8Total 	= eivain8Total + uCrsFtAux.EIVAIN8
			eivain9Total 	= eivain9Total + uCrsFtAux.EIVAIN9
			eivain10Total 	= eivain10Total + uCrsFtAux.EIVAIN10
			eivain11Total 	= eivain11Total + uCrsFtAux.EIVAIN11
			eivain12Total 	= eivain12Total + uCrsFtAux.EIVAIN12
			eivain13Total 	= eivain13Total + uCrsFtAux.EIVAIN13
			eivav1Total 	= eivav1Total + uCrsFtAux.EIVAV1
			eivav2Total 	= eivav2Total + uCrsFtAux.EIVAV2
			eivav3Total 	= eivav3Total + uCrsFtAux.EIVAV3
			eivav4Total 	= eivav4Total + uCrsFtAux.EIVAV4
			eivav5Total 	= eivav5Total + uCrsFtAux.EIVAV5
			eivav6Total 	= eivav6Total + uCrsFtAux.EIVAV6
			eivav7Total 	= eivav7Total + uCrsFtAux.EIVAV7
			eivav8Total 	= eivav8Total + uCrsFtAux.EIVAV8
			eivav9Total 	= eivav9Total + uCrsFtAux.EIVAV9
			eivav10Total 	= eivav10Total + uCrsFtAux.EIVAV10
			eivav11Total 	= eivav11Total + uCrsFtAux.EIVAV11
			eivav12Total 	= eivav12Total + uCrsFtAux.EIVAV12
			eivav13Total 	= eivav13Total + uCrsFtAux.EIVAV13
		ENDIF
		select uCrsFtAux		
	ENDSCAN
	
	
	
	&& Verificar Total Documento e Desconto
	SELECT uCrsFiAux
	SCAN 
		IF uCrsFiAux.tipodoc != 3
			docTotalSemDesc = docTotalSemDesc + (uCrsFiAux.epv*uCrsFiAux.qtt)
		ENDIF
	ENDSCAN

	SELECT uCrsFtAux
	SCAN
		IF uCrsFTAux.u_tipodoc != 2 
			lcTotalAtend = lcTotalAtend + uCrsFtAux.ETOTAL
		ENDIF
	ENDSCAN

	descUtente = docTotalSemDesc - lcTotalAtend 
				
	&& Verificar se o atendimento tem comparticipa��o
	select uCrsFiAux
	GO TOP 
	SCAN
		IF uCrsFiAux.u_txcomp != 100 
			lcCod = .t.
		ENDIF
	ENDSCAN
	
	&& VERIFICA SE DEVE IMPRIMIR O C�DIGO ADICIONAL DO OPERADOR EM VEZ DO C�DIGO PRINCIPAL **
	LOCAL lcValidaCodAlt		
	STORE 0 TO lcValidaCodAlt
	
	IF uf_gerais_getParameter("ADM0000000047","BOOL")
		lcValidaCodAlt = 1
	ENDIF 


	&& Verifica se existem promo��es aplicadas
	IF USED("uCrsFiHelper")		
		LOCAL lcValidaPromo, lcPromo
		STORE .f. TO lcValidaPromo
		STORE '' TO lcPromo
		SELECT uCrsFiHelper
		GO TOP 
		SCAN
			IF !EMPTY(uCrsFiHelper.campanhas) AND uCrsFiHelper.pontos == 0
				lcValidaPromo = .t.
				lcPromo = ALLTRIM(lcPromo) + IIF(EMPTY(ALLTRIM(lcPromo)),"",";") + ALLTRIM(uCrsFiHelper.campanhas)
			ENDIF
		ENDSCAN 
	ENDIF 
	

	&& Verifica pontos ganho no atendimento - 20151109
	LOCAL lcPontosAtendimento
	STORE 0 TO lcPontosAtendimento
	SELECT uCrsFiHelper
	GO TOP
	CALCULATE SUM(uCrsFiHelper.pontos) TO lcPontosAtendimento

	&& guardar vales de desconto aplicados
	IF USED("uCrsFiPromo")
		fecha("uCrsFiPromo")
	ENDIF
	SELECT uCrsFiAux
	GO TOP
	SELECT ref, u_refvale, u_epvp, design from uCrsFiHelper where epromo = .t. INTO CURSOR uCrsFiPromo
	SELECT uCrsFiPromo

	&& valor do atendimento por tipo de pagamento
	LOCAL lcMpag1, lcMpag2, lcMpag3, lcMpag4, lcMpag5, lcMpag6, lcMpag7, lcMpag8
	STORE '' TO lcMpag1, lcMpag2, lcMpag3, lcMpag4, lcMpag5, lcMpag6, lcMpag7, lcMpag8
	
	IF !USED("uCrsConfigModPag")
		TEXT TO lcSQL NOSHOW TEXTMERGE
			select * from B_modoPag
		ENDTEXT 
		If !uf_gerais_actGrelha("", "UcrsConfigModPag", lcSql)
			uf_perguntalt_chama("N�o foi poss�vel verificar as configura��es de Modo de Pagamento. Contacte o suporte.", "", "OK", 32)
			RETURN .f.
		ENDIF
	ENDIF

	
	IF !USED("uCrsFi2")
		TEXT TO lcSQL NOSHOW TEXTMERGE
			select * from fi2 WHERE fistamp = '<<uCrsFiAux.fistamp>>'
		ENDTEXT 
		If !uf_gerais_actGrelha("", "uCrsFi2", lcSql)
			uf_perguntalt_chama("N�o foi poss�vel verificar as configura��es de Modo de Pagamento. Contacte o suporte.", "", "OK", 32)
			RETURN .f.
		ENDIF
	ENDIF



	SELECT uCrsConfigModPag
	GO TOP
	SCAN 
		DO CASE 
			CASE ALLTRIM(UcrsConfigModPag.ref) == "01"
				lcMpag1 = ALLTRIM(UcrsConfigModPag.design)
			CASE ALLTRIM(UcrsConfigModPag.ref) == "02"
				lcMpag2 = ALLTRIM(UcrsConfigModPag.design)			
			CASE ALLTRIM(UcrsConfigModPag.ref) == "03"
				lcMpag3 = ALLTRIM(UcrsConfigModPag.design)
			CASE ALLTRIM(UcrsConfigModPag.ref) == "04"
				lcMpag4 = ALLTRIM(UcrsConfigModPag.design)
			CASE ALLTRIM(UcrsConfigModPag.ref) == "05"
				lcMpag5 = ALLTRIM(UcrsConfigModPag.design)
			CASE ALLTRIM(UcrsConfigModPag.ref) == "06"
				lcMpag6 = ALLTRIM(UcrsConfigModPag.design)
			CASE ALLTRIM(UcrsConfigModPag.ref) == "07"
				lcMpag7 = ALLTRIM(UcrsConfigModPag.design)
			CASE ALLTRIM(UcrsConfigModPag.ref) == "08"
				lcMpag8 = ALLTRIM(UcrsConfigModPag.design)																		
		ENDCASE
	ENDSCAN	

	IF myTipoCartao == 'Valor'
		LOCAL lcTotalHistCartao, lcValDispCartao, uv_nrCartao
		STORE 0 TO lcTotalHistCartao, lcValDispCartao
        STORE '' TO uv_nrCartao
		SELECT ucrsftAux
        GO TOP

        uc_tempNO = ucrsftAux.no
        uc_tempEstab = ucrsftAux.estab
		lcSQL = ''

        IF !EMPTY(ucrsftAux.u_hclstamp)

            TEXT TO lcSQL TEXTMERGE NOSHOW
                SELECT 
                    b_fidel.valcartaohist, 
                    b_fidel.valcartao,
                    b_fidel.nrcartao
                FROM 
                    b_fidel(NOLOCK) 
                    JOIN b_utentes(NOLOCK) ON b_fidel.clno = b_utentes.no AND b_fidel.clestab = b_utentes.estab
                WHERE 
                    b_utentes.utstamp = '<<ALLTRIM(ucrsftAux.u_hclstamp)>>'
            ENDTEXT

        ELSE
        
		    TEXT To lcSQL TEXTMERGE NOSHOW
			    SELECT valcartaohist, valcartao, nrcartao FROM b_fidel(NOLOCK) WHERE clno=<<ASTR(uc_tempNO)>> and clestab=<<ASTR(uc_tempEstab)>>
		    ENDTEXT 

        ENDIF

		uf_gerais_actGrelha("","ucrsverifvalcartao",lcSQL)
        
		IF RECCOUNT("ucrsverifvalcartao")>0 then
			lcTotalHistCartao = ucrsverifvalcartao.valcartaohist
			lcValDispCartao = ucrsverifvalcartao.valcartao 
            uv_nrCartao = ucrsverifvalcartao.nrcartao
		ENDIF 
	ENDIF
	
	LOCAL lctiposaft
	STORE 0 TO lctiposaft
	Text To lcSql Noshow Textmerge
		select tiposaft from td where ndoc=<<uCrsFtAux.ndoc>>
	ENDTEXT
	uf_gerais_actGrelha("","uCrsSaftFile",lcSql)
	lctiposaft= uCrsSaftFile.tiposaft
	fecha("uCrsSaftFile") 

	&& PREPARAR DEFINI��ES GERAIS DA IMPRESSORA
	lcValidaImp = uf_gerais_setImpressoraPOS(.t.)

	IF lcValidaImp
		&& Cabe�alho da Venda
		uf_gerais_criarCabecalhoPOS()

		&& Sendo reimpress�o � sempre segunda via
		?PADL("2a VIA",uf_gerais_getParameter("ADM0000000194","NUM")," ")

		&& IMPRIMIR O TIPO DE DOCUMENTO DE VENDA
		&& existem impressoras que n�o suportam esta funcionalidade nesses cen�rios � necess�rio comentar isto.
		uf_gerais_textoVermPOS()
		
		SELECT uCrsFtAux
		GO TOP
		SCAN
			IF uCrsFTAux.u_tipodoc != 2 
				IF UPPER(ALLTRIM(ucrse1.pais)) == 'PORTUGAL'
					DO CASE
						CASE (uCrsFtAux.u_tipodoc==8) OR (uCrsFtAux.ndoc==32) OR (uCrsFtAux.ndoc==31) OR (uCrsFtAux.ndoc==84)
							?"Fatura-Recibo Nr."
							??TRANSFORM(uCrsFtAux.FNO,"999999999")+" "
						
						CASE (uCrsFtAux.u_tipodoc==9) OR (uCrsFtAux.ndoc==11) OR (uCrsFtAux.ndoc==12)
							?"Fatura Nr."
							??TRANSFORM(uCrsFtAux.FNO,"999999999")+" "
						
						CASE (uCrsFtAux.u_tipodoc==11)
							?"Vnd. Manual Nr."
							??TRANSFORM(uCrsFtAux.FNO,"999999999")+" "
							
						CASE (uCrsFtAux.ndoc==4)
							?"Dev. a Cliente Nr."
							??TRANSFORM(uCrsFtAux.FNO,"999999999")+" "
								
						CASE (uCrsFtAux.u_tipodoc == 4)
							?"Fatura Acordo Nr."
							??TRANSFORM(uCrsFtAux.FNO,"999999999")+" "
							
						OTHERWISE
							***
					ENDCASE
					
					&& IMPRIMIR TAG IDENTIFICADORA DO DOCUMENTO DE VENDA
					DO CASE
						CASE (uCrsFtAux.u_tipodoc==8 OR uCrsFtAux.ndoc==84) AND uCrsFtAux.u_lote==0 AND uCrsFtAux.cobrado==.f.
							??"(VD)"
						
						CASE (uCrsFtAux.u_tipodoc==8 OR uCrsFtAux.ndoc==84) AND uCrsFtAux.u_lote==0 AND uCrsFtAux.cobrado==.t.
							??"(VD)(S)"	
						
						CASE (uCrsFtAux.u_tipodoc==8 OR uCrsFtAux.ndoc==84) AND uCrsFtAux.u_lote > 0
							??"(VD-CR)"
						
						CASE (uCrsFtAux.u_tipodoc==9) AND uCrsFtAux.cobrado==.f. AND uCrsFtAux.u_lote==0
							??"(FT)"
							
						CASE (uCrsFtAux.u_tipodoc==9) AND uCrsFtAux.cobrado==.t. AND uCrsFtAux.u_lote==0
							??"(FT)(S)"			
						
						CASE (uCrsFtAux.u_tipodoc==9) AND uCrsFtAux.u_lote > 0
							??"(FT-CR)"

						CASE (uCrsFtAux.u_tipodoc==11)
							??"(VDM)"
							
						CASE (uCrsFtAux.u_tipodoc=4) And uCrsFtAux.cobrado=.f. And uCrsFtAux.u_lote=0
							??"(FA)"

						CASE (uCrsFtAux.u_tipodoc=4) AND uCrsFtAux.cobrado=.T. AND uCrsFtAux.u_lote=0
							??"(FA)(S)"

						CASE (uCrsFtAux.u_tipodoc=4) AND uCrsFtAux.u_lote > 0
							??"(FA-CR)"

						OTHERWISE
							***
								
					ENDCASE
					*******************************
					
					&& Imprimir serie de factura��o
					??"/" + astr(uCrsFtAux.ndoc)
				ELSE
					SELECT uCrsFtAux
					IF uCrsFtAux.ftano>2020
						? ALLTRIM(uCrsFtAux.nmdoc) + ' Nr. ' + alltrim(lctiposaft) + ' ' + alltrim(str(uCrsFtAux.ndoc)) + alltrim(str(uCrsFtAux.ftano)) + '/' + alltrim(str(uCrsFtAux.fno))
					ELSE
						? ALLTRIM(uCrsFtAux.nmdoc) + ' Nr. ' + alltrim(lctiposaft) + ' ' + alltrim(str(uCrsFtAux.ndoc)) + '/' + alltrim(str(uCrsFtAux.fno))
					ENDIF 
				ENDIF 
			ENDIF 
		ENDSCAN

		SELECT uCrsFTAux
		GO TOP
			
		&& Imprime Data e Hora
		?"Data: "
		??uf_gerais_getDate(uCrsFtAux.FDATA,"DATA")
		??"      Hora: "
		**??uCrsFtAux.SAIDA
		?? Substr(uCrsFtAux.ousrhora,1,5)
		
		** Controla de tem validade fiscal
		SELECT uCrsFtAux
		GO TOP 
		IF uCrsFtAux.u_tipodoc == 3 OR uCrsFtAux.u_tipodoc == 4
			? "*** Talao sem Validade Fiscal ***"
		ENDIF

		&& existem impressoras que n�o suportam esta funcionalidade nesses cen�rios � necess�rio comentar isto.
	   	uf_gerais_resetCorPOS() 
	   	
		uf_gerais_separadorPOS()

		&& Impress�o dados Utente 
		SELECT uCrsFtAux
		GO TOP

		&& Imprime sempre os dados guardados na FT 
		?"Cliente: "
		??Alltrim(uCrsFtAux.NOME) + Iif(uCrsFtAux.no==200, '', ' (' + AStr(uCrsFtAux.no) + ')(' + AStr(uCrsFtAux.estab) + ')' ) + IIF(!EMPTY(uCrsFtAux.no_ext), ' - '+ALLTRIM(uCrsFtAux.no_ext),'' )
		?"Morada: "
		??ALLTRIM(uCrsFtAux.MORADA)
		?"Cod.Post.: "
		??LEFT(ALLTRIM(uCrsFtAux.CODPOST),uf_gerais_getParameter("ADM0000000194","NUM")-11)	
		
		?"Contrib. Nr.: "
		IF (ALLTRIM(uCrsFtAux.ncont)!="999999990" AND ALLTRIM(uCrsFtAux.ncont)!="9999999999") AND !EMPTY(ALLTRIM(uCrsFtAux.ncont))
			??TRANSFORM(uCrsFtAux.NCONT,"99999999999999999999")
		ELSE
			??ALLTRIM(uf_gerais_getParameter_site('ADM0000000075', 'text', mySite))
			IF UPPER(ALLTRIM(ucrse1.pais)) == 'PORTUGAL'	
				?"Nao sujeito passivo de IVA"
			ELSE
				?" "
			ENDIF 
		ENDIF
				
		?"Tlf.: "
		??substr(uCrsFtAux.telefone,1,9)

		IF ALLTRIM(uCrsE1.tipoempresa) == 'FARMACIA'
			?"N.Utente: "
			??IIF(EMPTY(ALLTRIM(uCrsFt2Aux.u_nbenef)),"         ",ALLTRIM(uCrsFt2Aux.u_nbenef))
			??"  N.Benef.: "
			??ALLTRIM(uCrsFt2Aux.u_nbenef2)
		ENDIF

        IF !EMPTY(uCrsFtAux.u_hclstamp)

            FECHA("uc_dep")

            uf_gerais_actGrelha("","uc_dep","select * from b_utentes(nolock) where utstamp = '" + ALLTRIM(uCrsFtAux.u_hclstamp) + "'")

            IF RECCOUNT("uc_dep") <> 0

                IF !uf_gerais_getparameter_site('ADM0000000123', 'BOOL', mysite)
                    uf_gerais_separadorPOS()
                
                    && Dados Utente a quem � associado os hist�rico de Produtos
                    SELECT uc_dep
                    GO TOP
                
                    ?"Dependente: "
                    ??Alltrim(uc_dep.NOME) + Iif(uc_dep.no==200, '', ' (' + AStr(uc_dep.no) + ')(' + AStr(uc_dep.estab) + ')' )
                
                    ?"Morada: "
                    ??ALLTRIM(uc_dep.MORADA)
                    
                    ?"Cod.Post.: "
                    ??LEFT(ALLTRIM(uc_dep.CODPOST),uf_gerais_getParameter("ADM0000000194","NUM")-11)
                    ?"  Tlf.: "
                    ??substr(uc_dep.telefone,1,9)
                    IF ALLTRIM(uCrsE1.tipoempresa) == 'FARMACIA'
                        ??"N.Benef.: "
                        ??IIF(EMPTY(ALLTRIM(uc_dep.nbenef)),"         ",ALLTRIM(uc_dep.nbenef))
                    ENDIF	
                ENDIF 

            ENDIF

		ENDIF

		&& Informa se est� sujeito a cativa��o de IVA
		IF uCrsFt2Aux.cativa
			?"Sujeito a cativa��o de IVA."
		ENDIF 
		
		** SE A VENDA FOR AO DOMICILIO IMPRIME OS DADOS **
		IF uCrsFt2Aux.u_vddom
			uf_gerais_separadorPOS()
			?"Entrega ao Domicilio: "
			?ALLTRIM(uf_gerais_getMacrosReports(5))
			?"Transportador: " + LEFT(ALLTRIM(uCrsFt2Aux.CONTACTO),uf_gerais_getParameter("ADM0000000194","NUM")-15)
			?"Morada: " + ALLTRIM(uCrsFt2Aux.MORADA)
			?"Cod.Post: " + TRANSFORM(uCrsFt2Aux.CODPOST,"########")
			??"  Local.: " + TRANSFORM(uCrsFt2Aux.local,"#################")
			?"Tel.: " + TRANSFORM(uCrsFt2Aux.TELEFONE,"##############")
			??"Email: " + Substr(uCrsFt2Aux.email,1,25)
			IF !Empty(uCrsFt2Aux.u_viatura)
				?"Viatura: " + TRANSFORM(uCrsFt2Aux.u_viatura,"#########################")
				??"Hora: " + Substr(uCrsFt2Aux.horaentrega,1,5)
			ENDIF
		ENDIF 
		**
		
		&& VERIFICAR SE EXISTE COMPARTICIPA��O E IMPRIMIR COLUNAS E LINHAS 
		IF !EMPTY(lcCod) 
			IF lcMotIsencaoIva == .t. AND uf_gerais_validaIvaDireitoDeducao() == 0
				****** IMPRESS�O DAS COLUNAS ******
				uf_gerais_separadorPOS()
				?"Produto"
				IF uf_gerais_getParameter("ADM0000000194","NUM") >= 50
					?"   PVP.     Pref.     Qtd.    Comp.   Total    IVA" &&50
				ELSE
					?"PVP.  | Pref. | Qt. | Comp. | Total |IVA" &&40
				ENDIF					
			ELSE
				****** IMPRESS�O DAS COLUNAS ******
				uf_gerais_separadorPOS()
				?"Produto"
				IF uf_gerais_getParameter("ADM0000000194","NUM") >= 50
					?"   PVP.     Pref.     Qtd.    Comp.   Total" &&50
				ELSE
					?"PVP.  | Pref. | Qt. | Comp. | Total" &&40
				ENDIF		
			ENDIF			

			&& Impress�o das Linhas
			SELECT uCrsFiAux
			GO TOP
			SCAN
				SELECT uCrsFTAux
				LOCATE FOR ALLTRIM(uCrsFTAux.ftstamp) == ALLTRIM(uCrsFiAux.ftstamp)
				IF uCrsFTAux.u_tipodoc != 2 AND !INLIST(ALLTRIM(uCrsFiAux.ref),'V000001')
					lcTamanhoDesign = uf_gerais_getParameter("ADM0000000194","NUM") - IIF(UPPER(ALLTRIM(uCrsFiAux.lobs2))=='REF.VD.ORIG.',2,0) - IIF(uCrsFiAux.iva==0 and !(UPPER(ALLTRIM(uCrsFiAux.lobs2))=='REF.VD.ORIG.'),5,0)
					**?IIF(UPPER(ALLTRIM(uCrsFiAux.lobs2))=='REF.VD.ORIG.','  ','') + substr(ALLTRIM(uCrsFiAux.DESIGN),1,45) + IIF(uCrsFiAux.iva==0 and !(UPPER(ALLTRIM(uCrsFiAux.lobs2))=='REF.VD.ORIG.') and ALLTRIM(uCrsFiAux.ref)!='V000001',' (*1)','')
					?IIF(UPPER(ALLTRIM(uCrsFiAux.lobs2))=='REF.VD.ORIG.','  ','') + substr(ALLTRIM(uCrsFiAux.DESIGN),1,45) 
					IF uf_gerais_getParameter("ADM0000000194","NUM") >= 50
						IF !(UPPER(ALLTRIM(uCrsFiAux.lobs2))=='REF.VD.ORIG.')
							?TRANSFORM(uCrsFiAux.u_epvp,lcMascaraDinheiro )+" "
							??TRANSFORM(uCrsFiAux.u_EPREF,lcMascaraDinheiro )+"   "
							??TRANSFORM(uCrsFiAux.QTT,"9999.9")
							??TRANSFORM(uCrsFiAux.U_ETTENT1+uCrsFiAux.U_ETTENT2,lcMascaraDinheiro )
							??TRANSFORM(uCrsFiAux.ETILIQUIDO,"9999999.99")+"  "
							IF lcMotIsencaoIva == .t. AND uf_gerais_validaIvaDireitoDeducao() == 0
								??TRANSFORM(uCrsFiAux.IVA,"9999")+"%"
							ENDIF
							IF uCrsFiAux.IVA = 0 AND uf_gerais_validaIvaDireitoDeducao() == 0
                                IF TYPE("uCrsFiAux.motisencao") != 'U' AND ALLTRIM(uCrsFiAux.ref) <> 'V000001'
                                    ?ALLTRIM(uCrsFiAux.motisencao)
                                ENDIF 
							ENDIF
							
							LOCAL uv_Lote
							STORE '' TO uv_Lote
							
							uv_Lote = uf_gerais_usalote(uCrsFiAux.fistamp, "Vet")

							IF !EMPTY(uv_lote)
                                    ?uv_lote
							ENDIF
							

							uf_gerais_actGrelha("",[uCrsFi2],[exec up_touch_fi2 '', ']+alltrim(uCrsFiAux.fistamp)+['])
							SELECT uCrsFi2
							
							
							IF TYPE("uCrsFi2.bonusDescr") <> "U"

								uv_curLine = uCrsFiAux.fistamp

								SELECT uCrsFi2
								GO TOP

								LOCATE FOR ALLTRIM(uCrsFi2.fistamp) == ALLTRIM(uv_curLine)

								IF FOUND()
									IF !EMPTY(uCrsFi2.bonusDescr)
										?'*'+ALLTRIM(uCrsFi2.bonusDescr)
									ENDIF
								ENDIF

								SELECT uCrsFiAux
								 
							ENDIF
						ENDIF
					ELSE
						IF !(UPPER(ALLTRIM(uCrsFiAux.lobs2))=='REF.VD.ORIG.')
							?PADC(IIF(OCCURS(".",astr(round(uCrsFiAux.u_epvp,2),8,2))>0,astr(round(uCrsFiAux.u_epvp,2),8,2),astr(round(uCrsFiAux.u_epvp,2),8,2)+".00"),6," ") + "|"
							??PADC(IIF(OCCURS(".",astr(round(uCrsFiAux.u_EPREF,2),8,2))>0,astr(round(uCrsFiAux.u_EPREF,2),8,2),astr(round(uCrsFiAux.u_EPREF,2),8,2)+".00"),7," ") + "|"
							??PADC(astr(round(uCrsFiAux.QTT,0),8,2),5," ") + "|"
							??PADC(IIF(OCCURS(".",astr(round(uCrsFiAux.U_ETTENT1+uCrsFiAux.U_ETTENT2,2),8,2))>0,astr(round(uCrsFiAux.U_ETTENT1+uCrsFiAux.U_ETTENT2,2),8,2),astr(round(uCrsFiAux.U_ETTENT1+uCrsFiAux.U_ETTENT2,2),8,2)+".00"),7," ") + "|"
							??PADC(IIF(OCCURS(".",astr(round(uCrsFiAux.ETILIQUIDO,2),8,2))>0,astr(round(uCrsFiAux.ETILIQUIDO,2),8,2),astr(round(uCrsFiAux.ETILIQUIDO,2),8,2)+".00"),7," ") + "|"
							IF lcMotIsencaoIva == .t. AND uf_gerais_validaIvaDireitoDeducao() == 0
								??PADC(astr(uCrsFiAux.IVA)+"%",3," ")
							ENDIF
							IF uCrsFiAux.IVA = 0 AND uf_gerais_validaIvaDireitoDeducao() == 0
                                IF TYPE("uCrsFiAux.motisencao") != 'U' AND ALLTRIM(uCrsFiAux.ref) <> 'V000001'
                                    ?ALLTRIM(uCrsFiAux.motisencao)
                                ENDIF 
							ENDIF
							
								LOCAL uv_Lote
							STORE '' TO uv_Lote
							
							uv_Lote = uf_gerais_usalote(uCrsFiAux.fistamp, "Vet")

							IF !EMPTY(uv_lote)
                                    ?uv_lote
							ENDIF
							
							
							SELECT uCrsFi2
							 
					
							IF TYPE("uCrsFi2.bonusDescr") <> "U"

								uv_curLine = uCrsFiAux.fistamp

								SELECT uCrsFi2
								GO TOP

								LOCATE FOR ALLTRIM(uCrsFi2.fistamp) == ALLTRIM(uv_curLine)

								IF FOUND()
									IF !EMPTY(uCrsFi2.bonusDescr)
										?'*'+ALLTRIM(uCrsFi2.bonusDescr)
									ENDIF
								ENDIF

								SELECT uCrsFiAux
								 
							ENDIF
						ENDIF
					ENDIF
					

					
					
					
					IF uCrsFiAux.iva == 0 AND !(UPPER(ALLTRIM(uCrsFiAux.lobs2))=='REF.VD.ORIG.')
						lcIsentoIva = .t.
					ENDIF
				ENDIF 
			ENDSCAN
		ENDIF
			
		IF EMPTY(lcCod)
			IF lcMotIsencaoIva == .t.	
				&& Impress�o das Colunas
				uf_gerais_separadorPOS()
				?"Produto"
				IF uf_gerais_getParameter("ADM0000000194","NUM") >= 45 AND uf_gerais_validaIvaDireitoDeducao() == 0
					?"     PVP.      Qtd.          Total        IVA" &&45
				ELSE
					?" PVP.   |   Qtd.   |   Total   |   IVA  " &&40
				ENDIF
			ELSE
				&& Impress�o das Colunas
				uf_gerais_separadorPOS()
				?"Produto"
				IF uf_gerais_getParameter("ADM0000000194","NUM") >= 45
					?"     PVP.      Qtd.          Total" &&45
				ELSE
					?" PVP.   |   Qtd.   |   Total" &&40
				ENDIF
			ENDIF
			
			&& IMPRESS�O DAS LINHAS
			SELECT uCrsFiAux
			GO TOP
			 
			SCAN
				SELECT uCrsFTAux
				LOCATE FOR ALLTRIM(uCrsFTAux.ftstamp)==ALLTRIM(uCrsFiAux.ftstamp)
				IF uCrsFTAux.u_tipodoc != 2 AND !INLIST(ALLTRIM(uCrsFiAux.ref),'V000001')
					lcTamanhoDesign = uf_gerais_getParameter("ADM0000000194","NUM") - IIF(UPPER(ALLTRIM(uCrsFiAux.lobs2))=='REF.VD.ORIG.',2,0) - IIF(uCrsFiAux.iva==0 and !(UPPER(ALLTRIM(uCrsFiAux.lobs2))=='REF.VD.ORIG.'),5,0)
					**?IIF(UPPER(ALLTRIM(uCrsFiAux.lobs2))=='REF.VD.ORIG.','  ','') + substr(ALLTRIM(uCrsFiAux.DESIGN),1,45) + IIF(uCrsFiAux.iva==0 and !(UPPER(ALLTRIM(uCrsFiAux.lobs2))=='REF.VD.ORIG.') and ALLTRIM(uCrsFiAux.ref)!='V000001',' (*1)','')
					?IIF(UPPER(ALLTRIM(uCrsFiAux.lobs2))=='REF.VD.ORIG.','  ','') + substr(ALLTRIM(uCrsFiAux.DESIGN),1,45) 
					IF uf_gerais_getParameter("ADM0000000194","NUM") >= 45
						IF !(UPPER(ALLTRIM(uCrsFiAux.lobs2))=='REF.VD.ORIG.')
							?TRANSFORM(uCrsFiAux.EPV,lcMascaraDinheiro )+"   "
							??TRANSFORM(uCrsFiAux.QTT,"99999.9")  +"     "
							??TRANSFORM(uCrsFiAux.ETILIQUIDO,lcMascaraDinheiro )+"    "
							IF lcMotIsencaoIva == .t.	AND uf_gerais_validaIvaDireitoDeducao() == 0
								??TRANSFORM(uCrsFiAux.IVA,"9999")+"%"
							ENDIF
							IF uCrsFiAux.IVA = 0 AND uf_gerais_validaIvaDireitoDeducao() == 0
								IF TYPE("uCrsFiAux.motisencao") != 'U' AND ALLTRIM(uCrsFiAux.ref) <> 'V000001' 
									?ALLTRIM(uCrsFiAux.motisencao)
								ENDIF 
							ENDIF
								
								LOCAL uv_Lote
							STORE '' TO uv_Lote
							
							uv_Lote = uf_gerais_usalote(uCrsFiAux.fistamp, "Vet")

							IF !EMPTY(uv_lote)
                                    ?uv_lote
							ENDIF
							
							
							IF TYPE("uCrsFi2.bonusDescr") <> "U"

								uv_curLine = uCrsFiAux.fistamp

								SELECT uCrsFi2
								GO TOP
								 

								LOCATE FOR ALLTRIM(uCrsFi2.fistamp) == ALLTRIM(uv_curLine)

								IF FOUND()
									IF !EMPTY(uCrsFi2.bonusDescr)
										?'*'+ALLTRIM(uCrsFi2.bonusDescr)
									ENDIF
								ENDIF

                                SELECT uCrsFiAux
                            ENDIF
						ENDIF
					ELSE
						IF !(UPPER(ALLTRIM(uCrsFiAux.lobs2))=='REF.VD.ORIG.')
							?PADC(IIF(OCCURS(".",astr(round(uCrsFiAux.EPV,2),8,2))>0,astr(round(uCrsFiAux.EPV,2),8,2),astr(round(uCrsFiAux.EPV,2),8,2)+".00"),8," ") + "|"
							??PADC(astr(round(uCrsFiAux.QTT,2),8,2),10," ") + "|"
							??PADC(IIF(OCCURS(".",astr(round(uCrsFiAux.ETILIQUIDO,2),8,2))>0,astr(round(uCrsFiAux.ETILIQUIDO,2),8,2),astr(round(uCrsFiAux.ETILIQUIDO,2),8,2)+".00"),11," ") + "|"
							IF lcMotIsencaoIva == .t. AND uf_gerais_validaIvaDireitoDeducao() == 0	 
								??PADC(astr(uCrsFiAux.IVA)+"%",8," ")
							ENDIF
							IF uCrsFiAux.IVA = 0 AND uf_gerais_validaIvaDireitoDeducao() == 0
								IF TYPE("uCrsFiAux.motisencao") != 'U' AND ALLTRIM(uCrsFiAux.ref) <> 'V000001'
									?ALLTRIM(uCrsFiAux.motisencao)
								ENDIF 
							ENDIF
							
								LOCAL uv_Lote
							STORE '' TO uv_Lote
							
							uv_Lote = uf_gerais_usalote(uCrsFiAux.fistamp, "Vet")

							IF !EMPTY(uv_lote)
                                    ?uv_lote
							ENDIF
							
							
							IF TYPE("uCrsFi2.bonusDescr") <> "U"

								uv_curLine = uCrsFiAux.fistamp

								SELECT uCrsFi2
								GO TOP
								 

								LOCATE FOR ALLTRIM(uCrsFi2.fistamp) == ALLTRIM(uv_curLine)

								IF FOUND()
									IF !EMPTY(uCrsFi2.bonusDescr)
										?'*'+ALLTRIM(uCrsFi2.bonusDescr)
									ENDIF
								ENDIF

                                SELECT uCrsFiAux
                                 
                            ENDIF
						ENDIF
					ENDIF
					
					IF uCrsFiAux.iva == 0 AND !(UPPER(ALLTRIM(uCrsFiAux.lobs2))=='REF.VD.ORIG.')
						lcIsentoIva = .t.
					ENDIF
				

					SELECT uCrsFi2
					IF TYPE("uCrsFi2.bonusDescr") <> "U"

						uv_curLine = uCrsFiAux.fistamp

						SELECT uCrsFi2
						GO TOP
						 

						LOCATE FOR ALLTRIM(uCrsFi2.fistamp) == ALLTRIM(uv_curLine)

						IF FOUND()
							IF !EMPTY(uCrsFi2.bonusDescr)
								?'*'+ALLTRIM(uCrsFi2.bonusDescr)
							ENDIF
						ENDIF

						SELECT uCrsFiAux
						 
					ENDIF
					
				ENDIF
			ENDSCAN 
		ENDIF

		uf_gerais_separadorPOS()

		&& existem impressoras que n�o suportam esta funcionalidade nesses cen�rios � necess�rio comentar isto.
		uf_gerais_textoVermPOS() 
		
		&& Impress�o Valor Descontos e Totais de Documento 
		IF UPPER(ALLTRIM(lcMoeda)) == 'EURO'
			IF descTotal > 0
				?"TOTAL SEM DESCONTO	 "
				??TRANSFORM(docTotalSemDesc,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(213)
							
				?"Descontos:		 "
				??TRANSFORM(descUtente,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(213)
			ENDIF

			?"VALOR A PAGAR:      "
			??TRANSFORM(doctotal,"999999999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(213)

			&& existem impressoras que n�o suportam esta funcionalidade nesses cen�rios � necess�rio comentar isto.
			uf_gerais_resetCorPOS() 
			
			&& Valores Pagamento
			IF lcModopag1 > 0
				? SUBSTR(lcMpag1,1,20) +":		  	     "
				??TRANSFORM(lcModopag1,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(213)		
			ENDIF
			IF lcModopag2 > 0
				? SUBSTR(lcMpag2,1,20) +":			     "
				??TRANSFORM(lcModopag2,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(213)		
			ENDIF
			IF lcModopag3 > 0		
				? SUBSTR(lcMpag3,1,20) +":			  	     "
				??TRANSFORM(lcModopag3,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(213)			
			ENDIF
			IF lcModopag4 > 0
				? SUBSTR(lcMpag4,1,20) +":		  	     "
				??TRANSFORM(lcModopag4,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(213)	
			ENDIF
			IF lcModopag5 > 0	
				? SUBSTR(lcMpag5,1,20) +":			  	     "
				??TRANSFORM(lcModopag5,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(213)			
			ENDIF
			IF lcModopag6 > 0
				? SUBSTR(lcMpag6,1,20) +":			     "
				??TRANSFORM(lcModopag6,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(213)		
			ENDIF
			IF lcModopag7 > 0
				? SUBSTR(lcMpag7,1,20) +":			     "
				??TRANSFORM(lcModopag7,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(213)			
			ENDIF
			IF lcModopag8 > 0
				? SUBSTR(lcMpag8,1,20) +":			     "
				??TRANSFORM(lcModopag8,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(213)			
			ENDIF
			IF lcTroco > 0
				?"TROCO:			  "
				??TRANSFORM(lcTroco,"999999999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(213)		
			ENDIF
		ELSE
			IF uf_gerais_getParameter("ADM0000000260","text")='USD'
				IF descTotal > 0
					?"TOTAL SEM DESCONTO	 "
					??TRANSFORM(docTotalSemDesc,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(36)
								
					?"Descontos:		 "
					??TRANSFORM(descUtente,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(36)
				ENDIF
	
				?"VALOR A PAGAR:	"
				??TRANSFORM(docTotal,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(36)

				&& existem impressoras que n�o suportam esta funcionalidade nesses cen�rios � necess�rio comentar isto.
				uf_gerais_resetCorPOS() 
				
				&& Valores Pagamento
				IF lcModopag1 > 0
					? SUBSTR(lcMpag1,1,20) +":		  	     "
					??TRANSFORM(lcModopag1,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(36)		
				ENDIF
				IF lcModopag2 > 0
					? SUBSTR(lcMpag2,1,20) +":			     "
					??TRANSFORM(lcModopag2,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(36)		
				ENDIF
				IF lcModopag3 > 0		
					? SUBSTR(lcMpag3,1,20) +":			  	     "
					??TRANSFORM(lcModopag3,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(36)			
				ENDIF
				IF lcModopag4 > 0
					? SUBSTR(lcMpag4,1,20) +":		  	     "
					??TRANSFORM(lcModopag4,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(36)	
				ENDIF
				IF lcModopag5 > 0
					? SUBSTR(lcMpag5,1,20) +":		  	     	 "
					??TRANSFORM(lcModopag5,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(36)			
				ENDIF
				IF lcModopag6 > 0
					? SUBSTR(lcMpag6,1,20) +":			     "
					??TRANSFORM(lcModopag6,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(36)		
				ENDIF
				IF lcModopag7 > 0
					? SUBSTR(lcMpag7,1,20) +":			     "
					??TRANSFORM(lcModopag7,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(36)			
				ENDIF
				IF lcModopag8 > 0
					? SUBSTR(lcMpag8,1,20) +":			     "
					??TRANSFORM(lcModopag8,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(36)			
				ENDIF
				IF lcTroco > 0
					?"TROCO:				     "
					??TRANSFORM(lcTroco,"99999999.99 ") + chr(27)+chr(116)+chr(19)+CHR(36)		
				ENDIF
			ELSE
				IF descTotal > 0
					?"TOTAL SEM DESCONTO	 "
					??TRANSFORM(docTotalSemDesc,"99999999.99 ") + " " + ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))
								
					?"Descontos:		 "
					??TRANSFORM(descUtente,"99999999.99 ") + " " + ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))
				ENDIF
	
				?"VALOR A PAGAR:	"
				??TRANSFORM(docTotal,"99999999.99 ") + " " + ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))

				&& existem impressoras que n�o suportam esta funcionalidade nesses cen�rios � necess�rio comentar isto.
				uf_gerais_resetCorPOS() 
				
				&& Valores Pagamento
				IF lcModopag1 > 0
					? SUBSTR(lcMpag1,1,20) +":		  	     "
					??TRANSFORM(lcModopag1,"99999999.99 ") + " " + ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))		
				ENDIF
				IF lcModopag2 > 0
					? SUBSTR(lcMpag2,1,20) +":			     "
					??TRANSFORM(lcModopag2,"99999999.99 ") + " " + ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))		
				ENDIF
				IF lcModopag3 > 0		
					? SUBSTR(lcMpag3,1,20) +":			  	     "
					??TRANSFORM(lcModopag3,"99999999.99 ") + " " + ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))			
				ENDIF
				IF lcModopag4 > 0
					? SUBSTR(lcMpag4,1,20) +":		  	     "
					??TRANSFORM(lcModopag4,"99999999.99 ") + " " + ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))	
				ENDIF
				IF lcModopag5 > 0
					? SUBSTR(lcMpag5,1,20) +":		  	     	 "
					??TRANSFORM(lcModopag5,"99999999.99 ") + " " + ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))			
				ENDIF
				IF lcModopag6 > 0
					? SUBSTR(lcMpag6,1,20) +":			     "
					??TRANSFORM(lcModopag6,"99999999.99 ") + " " + ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))		
				ENDIF
				IF lcModopag7 > 0
					? SUBSTR(lcMpag7,1,20) +":			     "
					??TRANSFORM(lcModopag7,"99999999.99 ") + " " + ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))		
				ENDIF
				IF lcModopag8 > 0
					? SUBSTR(lcMpag8,1,20) +":			     "
					??TRANSFORM(lcModopag8,"99999999.99 ") + " " + ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))			
				ENDIF
				IF lcTroco > 0
					?"TROCO:				     "
					??TRANSFORM(lcTroco,"99999999.99 ") + " " + ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))		
				ENDIF
			ENDIF 
		ENDIF
		
		&& Resumo IVA - alterado para ficar com apresenta��o diferente 2015-07-29
		IF lcMotIsencaoIva == .t.	AND uf_gerais_validaIvaDireitoDeducao() == 0
			SELECT uCrsFTAux
			?""
			?"Resumo do IVA:"
			? "Taxa	     " + "Base     " + "IVA     " + "Total"
			&& Taxa 0%
			IF EIVAIN4Total > 0
				?TRANSFORM(uCrsFTAux.IVATX4,"99")+"%      "
				??TRANSFORM(EIVAIN4Total,lcMascaraDinheiro )+" "
				??TRANSFORM(EIVAV4Total,lcMascaraDinheiro )+" "
				??TRANSFORM(EIVAIN4Total + EIVAV4Total,lcMascaraDinheiro)+" "
			ENDIF
					
			&& Taxa 6%
			IF EIVAV1Total > 0
				?TRANSFORM(uCrsFTAux.IVATX1,"99")+"%      "
				??TRANSFORM(EIVAIN1Total,lcMascaraDinheiro )+" "
				??TRANSFORM(EIVAV1Total,lcMascaraDinheiro )+" "
				??TRANSFORM(EIVAIN1Total + EIVAV1Total,lcMascaraDinheiro )+" "
			ENDIF
			
			&& Taxa 13%
			IF 	EIVAV3Total > 0	
				?TRANSFORM(uCrsFTAux.IVATX3,"99")+"%      "
				??TRANSFORM(EIVAIN3Total,lcMascaraDinheiro )+" "
				??TRANSFORM(EIVAV3Total,lcMascaraDinheiro )+" "
				??TRANSFORM(EIVAIN3Total + EIVAV3Total,lcMascaraDinheiro )+" "
			ENDIF
			
			&& Taxa 23%
			IF EIVAV2Total > 0
				?TRANSFORM(uCrsFTAux.IVATX2,"99")+"%      "
				??TRANSFORM(EIVAIN2Total,lcMascaraDinheiro )+" "
				??TRANSFORM(EIVAV2Total,lcMascaraDinheiro )+" "
				??TRANSFORM(EIVAIN2Total + EIVAV2Total,lcMascaraDinheiro )+" "
			ENDIF
			
			&& Taxa 5
				IF EIVAV5Total > 0
					?TRANSFORM(uCrsFTAux.IVATX5,"99")+"%      "
					??TRANSFORM(EIVAIN5Total,lcMascaraDinheiro )+" "
					??TRANSFORM(EIVAV5Total,lcMascaraDinheiro )+" "
					??TRANSFORM(EIVAIN5Total + EIVAV5Total,lcMascaraDinheiro )+" "
				ENDIF
				
				&& Taxa 6
				IF EIVAV6Total > 0
				?TRANSFORM(uCrsFTAux.IVATX6,"99")+"%      "
					??TRANSFORM(EIVAIN6Total,lcMascaraDinheiro )+" "
					??TRANSFORM(EIVAV6Total,lcMascaraDinheiro )+" "
					??TRANSFORM(EIVAIN6Total + EIVAV6Total,lcMascaraDinheiro )+" "
				ENDIF
				
				&& Taxa 7
				IF EIVAV7Total > 0
					?TRANSFORM(uCrsFTAux.IVATX7,"99")+"%      "
					??TRANSFORM(EIVAIN7Total,lcMascaraDinheiro )+" "
					??TRANSFORM(EIVAV7Total,lcMascaraDinheiro )+" "
					??TRANSFORM(EIVAIN7Total + EIVAV7Total,lcMascaraDinheiro )+" "
				ENDIF
				
				&& Taxa 8
				IF EIVAV8Total > 0
					?TRANSFORM(uCrsFTAux.IVATX8,"99")+"%      "
					??TRANSFORM(EIVAIN8Total,lcMascaraDinheiro )+" "
					??TRANSFORM(EIVAV8Total,lcMascaraDinheiro )+" "
					??TRANSFORM(EIVAIN8Total + EIVAV8Total,lcMascaraDinheiro )+" "
				ENDIF
				
				&& Taxa 9
				IF EIVAV9Total > 0
					?TRANSFORM(uCrsFTAux.IVATX9,"99")+"%      "
					??TRANSFORM(EIVAIN9Total,lcMascaraDinheiro )+" "
					??TRANSFORM(EIVAV9Total,lcMascaraDinheiro )+" "
					??TRANSFORM(EIVAIN9Total + EIVAV9Total,lcMascaraDinheiro )+" "
				ENDIF
				
				&& Taxa 10
				IF EIVAV10Total > 0
					?TRANSFORM(uCrsFTAux.IVATX10,"99")+"%      "
					??TRANSFORM(EIVAIN10Total,lcMascaraDinheiro )+" "
					??TRANSFORM(EIVAV10Total,lcMascaraDinheiro )+" "
					??TRANSFORM(EIVAIN10Total + EIVAV10Total,lcMascaraDinheiro )+" "
				ENDIF
				
				&& Taxa 11
				IF EIVAV11Total > 0
					?TRANSFORM(uCrsFTAux.IVATX11,"99")+"%      "
					??TRANSFORM(EIVAIN11Total,lcMascaraDinheiro )+" "
					??TRANSFORM(EIVAV11Total,lcMascaraDinheiro )+" "
					??TRANSFORM(EIVAIN11Total + EIVAV11Total,lcMascaraDinheiro )+" "
				ENDIF
				
				&& Taxa 12
				IF EIVAV12Total > 0
					?TRANSFORM(uCrsFTAux.IVATX12,"99")+"%      "
					??TRANSFORM(EIVAIN12Total,lcMascaraDinheiro )+" "
					??TRANSFORM(EIVAV12Total,lcMascaraDinheiro )+" "
					??TRANSFORM(EIVAIN12Total + EIVAV12Total,lcMascaraDinheiro )+" "
				ENDIF
				
				&& Taxa 13
				IF EIVAV13Total > 0
					?TRANSFORM(uCrsFTAux.IVATX13,"99")+"%      "
					??TRANSFORM(EIVAIN13Total,lcMascaraDinheiro )+" "
					??TRANSFORM(EIVAV13Total,lcMascaraDinheiro )+" "
					??TRANSFORM(EIVAIN13Total + EIVAV13Total,lcMascaraDinheiro )+" "
				ENDIF
		ENDIF
	
		
		IF USED("uCrsProdComp") OR uCrsProdComp.total_compart_estado1 <> 0 OR uCrsProdComp.total_compart_ext1 <> 0 OR uCrsProdComp.total_compart_cli1 <>0
			?""
			?""
			?"Info. Produtos Comparticipados"
			IF uCrsProdComp.total_compart_estado1 <> 0
				??? CHR(29)+CHR(33)+CHR(16.5) && aumentar letra
				?"Custo Suportado p/Estado:" + TRANSFORM(uCrsProdComp.total_compart_estado1,"9999.99")
				??? CHR(29)+CHR(33)+CHR(0) && diminuir letra
			ENDIF
			IF uCrsProdComp.total_compart_cli1 <>0 OR uCrsProdComp.total_compart_estado1 <> 0
				??? CHR(29)+CHR(33)+CHR(16.5) && aumentar letra
				?"Custo Suportado p/Utente:" + TRANSFORM(uCrsProdComp.total_compart_cli1,"9999.99")
				??? CHR(29)+CHR(33)+CHR(0) && diminuir letra
			ENDIF	
			IF uCrsProdComp.total_compart_ext1 <> 0
				?"Custo Suportado p/Entidade:		"
				??TRANSFORM(uCrsProdComp.total_compart_ext1,lcMascaraDinheiro )
			ENDIF
			
			
		ENDIF
		
	
		
	
		&& Impressao codigo do atendimento
		SELECT uCrsFTAux
		GO TOP
		IF !EMPTY(uCrsFTAux.u_nratend)
			?
			??	chr(27)+chr(97)+chr(1)
			?'Atendimento:'
			?
			??? chr(29) + chr(104) +chr(50) + chr(29)+ chr(72) + chr(2) + chr(29)+ chr(119) +chr(2) + chr(29)+chr(107)+chr(4) + UPPER(ALLTRIM(uCrsFTAux.u_nratend)) + chr(0)
			uf_gerais_textoLJPOS()
		ENDIF

		&& Centrar Informa��o
		uf_gerais_textoCJPOS()

		&& Informa��o Promo��es - Lu�s Leal 2015-07-28
		IF lcValidaPromo == .t.
			? ""
			??? CHR(29)+CHR(33)+CHR(16) && aumentar letra
			? "Campanhas"
			??? CHR(29)+CHR(33)+CHR(0) && diminuir letra
			? ALLTRIM(lcPromo)
		ENDIF

		&& Impress�o pontos cart�o cliente 
		&& Verificar se usou vale de desconto 
        IF myTipoCartao == 'Pontos'
            IF USED("uCrsFiPromo")
                IF UPPER(ALLTRIM(lcMoeda)) == 'EURO'
                    IF RECCOUNT("uCrsFiPromo")>0
                        ??? CHR(29)+CHR(33)+CHR(16) && aumentar letra
                        ? IIF(myTipoCartao == 'Pontos',"Vales Utilizados", "Valor Utilizado")
                        ??? CHR(29)+CHR(33)+CHR(0) && diminuir letra
                        SELECT uCrsFiPromo
                        GO TOP
                        SCAN
                            ? IIF(myTipoCartao == 'Pontos','Vale Desconto aplicado de ', 'Valor Desconto aplicado: ') + astr(ABS(uCrsFiPromo.u_epvp),9,2) + chr(27)+chr(116)+chr(19)+CHR(213)
                        ENDSCAN
                    ENDIF
                ELSE
                    IF RECCOUNT("uCrsFiPromo")>0
                        ??? CHR(29)+CHR(33)+CHR(16) && aumentar letra
                        ? IIF(myTipoCartao == 'Pontos',"Vales Utilizados", "Valor Utilizado")
                        ??? CHR(29)+CHR(33)+CHR(0) && diminuir letra
                        SELECT uCrsFiPromo
                        GO TOP
                        SCAN
                            IF uf_gerais_getParameter("ADM0000000260","text")='USD'
                                ? IIF(myTipoCartao == 'Pontos','Vale Desconto aplicado de ', 'Valor Desconto aplicado: ') + astr(ABS(uCrsFiPromo.u_epvp),9,2) + chr(27)+chr(116)+chr(19)+CHR(36)
                            ELSE
                                ? IIF(myTipoCartao == 'Pontos','Vale Desconto aplicado de ', 'Valor Desconto aplicado: ') + astr(ABS(uCrsFiPromo.u_epvp),9,2) + " " + ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))
                            ENDIF 
                        ENDSCAN
                    ENDIF
                ENDIF
            ENDIF
        ENDIF
		
		
			
		IF myTipoCartao == 'Pontos'
			IF myCartaoClienteLT
				** levantar dados **
				IF uf_gerais_actGrelha("",[uCrsUltTranCartao],[select nrcartao, pontosAtrib, pontosUsa, inactivo from B_fidel (nolock) where nrcartao=']+ALLTRIM(uCrsFTAux.nrcartao)+['])
					IF reccount() > 0
						IF !EMPTY(uCrsUltTranCartao.nrcartao)
							
							? ""
							??? CHR(29)+CHR(33)+CHR(16) && aumentar letra
							? "Cartao Cliente"
							??? CHR(29)+CHR(33)+CHR(0) && diminuir letra

							? "Total Disponivel: " + astr(uCrsUltTranCartao.pontosAtrib-uCrsUltTranCartao.pontosUsa) + " pontos"						
							? "Ganho neste Atendimento: " + astr(lcPontosAtendimento) + " pontos"
							? "Total Ganho com Cartao: " + astr(uCrsUltTranCartao.pontosAtrib) + " pontos"
							? ""
						ENDIF
					ENDIF
					fecha("uCrsUltTranCartao")
				ENDIF
			ENDIF
		ENDIF 
		

		SELECT uCrsFtAux
		IF myTipoCartao == 'Valor' AND uCrsFtAux.no <> 200 AND NOT EMPTY(uv_nrCartao)
		
			
			SELECT uCrsFtAux
			uf_imprimirPOS_devolveValoresCartaoAtendimento(uCrsFtAux.u_nratend)
					
			if(USED("uCrsFtResultTemp")) 
                SELECT uCrsFtResultTemp
                
				SELECT uCrsFt2Aux
				? ""
				??? CHR(29)+CHR(33)+CHR(16) && aumentar letra
				? "Cartao Cliente"
				??? CHR(29)+CHR(33)+CHR(0) && diminuir letra

				IF uCrsFtResultTemp.valcartao <> 0 OR uCrsFtResultTemp.valcartaoutilizado<>0
					IF UPPER(ALLTRIM(lcMoeda)) == 'EURO'
						IF uCrsFtResultTemp.valcartao > 0
							? 'Ganho neste Atendimento: ' + astr(ABS(uCrsFtResultTemp.valcartao),9,2) + chr(27)+chr(116)+chr(19)+CHR(213)
						ENDIF 
						IF uCrsFtResultTemp.valcartaoutilizado <> 0
							? 'Utilizado neste Atendimento: ' + astr(ABS(uCrsFtResultTemp.valcartaoutilizado),9,2) + ' ' + chr(27)+chr(116)+chr(19)+CHR(213)
						ENDIF 
					ELSE
						IF uf_gerais_getParameter("ADM0000000260","text")='USD'
							IF uCrsFtResultTemp.valcartao > 0
								? 'Ganho neste Atendimento: ' + astr(uCrsFtResultTemp.valcartao) + ' ' + chr(27)+chr(116)+chr(19)+CHR(36)
							ENDIF 
							IF uCrsFtResultTemp.valcartaoutilizado <> 0
								? 'Utilizado neste Atendimento: ' + astr(uCrsFtResultTemp.valcartaoutilizado) + ' ' + chr(27)+chr(116)+chr(19)+CHR(36)
							ENDIF 
						ELSE
							IF uCrsFtResultTemp.valcartao > 0
								? 'Ganho neste Atendimento: ' + astr(uCrsFtResultTemp.valcartao) + ' ' + ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))
							ENDIF 
							IF uCrsFtResultTemp.valcartaoutilizado <> 0
								? 'Utilizado neste Atendimento: ' + astr(uCrsFtResultTemp.valcartaoutilizado) + ' ' + ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))
							ENDIF 
						ENDIF 
					ENDIF 
					? ""
				ENDIF
			ENDIF
			 
			
			fecha("uCrsFtResultTemp")
			
            SELECT uCrsFt2Aux
            IF UPPER(ALLTRIM(lcMoeda)) == 'EURO'
                ? 'Disponivel: ' + astr(ABS(lcValDispCartao),9,2) + ' ' + chr(27)+chr(116)+chr(19)+CHR(213)
                ? 'Total Ganho em Cartao: ' + astr(ABS(lcTotalHistCartao),9,2) + ' ' + chr(27)+chr(116)+chr(19)+CHR(213)
            ELSE
                IF uf_gerais_getParameter("ADM0000000260","text")='USD'
                    ? 'Dispon�vel: ' + astr(lcValDispCartao) + ' ' + chr(27)+chr(116)+chr(19)+CHR(36)
                    ? 'Total Ganho em Cart�o: ' + astr(lcTotalHistCartao) + ' ' + chr(27)+chr(116)+chr(19)+CHR(36)
                ELSE
                    ? 'Dispon�vel: ' + astr(lcValDispCartao) + ' ' + ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))
                    ? 'Total Ganho em Cart�o: ' + astr(lcTotalHistCartao) + ' ' + ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))
                ENDIF 
                ? ""
            ENDIF 

		ENDIF 		
			
		&& Impress�o rodap� apenas dispon�vel para Reg. a Cliente
		??? chr(27)+chr(97)+chr(1)
		IF uCrsFTAux.u_tipodoc == 2 OR uCrsFTAux.u_tipodoc == 7
			?
			?"      ----------------------------------"
			?"         O Cliente"
		ENDIF
	
*!*			&& Texto Relativo � isen��o de IVa
*!*			IF lcMotIsencaoIva == .t. AND uf_gerais_validaIvaDireitoDeducao() == 0
*!*		 		IF lcIsentoIva
*!*					DO CASE
*!*						CASE uCrsFTAux.pais == 3
*!*							?"Nota (*1): Isencao prevista no n. 3 do art. 14. do CIVA"
*!*						CASE uCrsFTAux.pais == 2 
*!*							?"Nota (*1): Isencao ao abrigo da alinea a) do art. 14. do RITI"
*!*						OTHERWISE 
*!*							? ALLTRIM(ucrse1.motivo_isencao_iva)
*!*							**?"Nota (*1): Isencao prevista no Art. 9. do CIVA (ou similar)"
*!*					ENDCASE 
*!*				ENDIF
*!*			ENDIF
		
		&& 
		LOCAL lcValidaCert, lcNrCert, lcCharCert
		STORE '' TO lcNrCert, lcCharCert
		
		SELECT uCrsFTAux
		GO TOP
		SCAN
			IF uCrsFTAux.u_tipodoc != 2 
				IF uf_gerais_actGrelha("",[uCrsIduCert],[exec up_gerais_iduCert ']+ALLTRIM(uCrsFTAux.ftstamp)+['])
					IF RECCOUNT()>0
						IF uCrsIduCert.temCert
							IF !lcValidaCert
								IF lcMotIsencaoIva == .t. AND uf_gerais_validaIvaDireitoDeducao() == 0	 AND uf_gerais_getParameter_site("ADM0000000166", "BOOL", mySite)
									?'IVA INCLUIDO'
								ENDIF
							ENDIF
							
							**IF myNrVendasTalao > 1
								?"Referente a: "
								IF (uCrsFTAux.u_tipodoc == 8) OR (uCrsFTAux.ndoc == 32) OR (uCrsFTAux.ndoc == 31) OR (uCrsFTAux.ndoc == 84)
									??"Fatura-Recibo Nr."
									??astr(uCrsFTAux.FNO)+" "
								ENDIF
								IF (uCrsFTAux.u_tipodoc == 9) OR (uCrsFTAux.ndoc == 11) OR (uCrsFTAux.ndoc == 12)
									??"Fatura Nr."
									??astr(uCrsFTAux.FNO)+" "
								ENDIF
								IF (uCrsFTAux.ndoc == 4)
									??"Dev. a Cliente Nr."
									??astr(uCrsFTAux.FNO)+" "
								ENDIF 
								
								** IMPRIMIR TAG IDENTIFICADORA DO DOCUMENTO DE VENDA **
								DO CASE 
									CASE (uCrsFTAux.u_tipodoc == 8 OR uCrsFTAux.ndoc == 84) AND uCrsFTAux.u_lote=0 AND uCrsFTAux.cobrado=.f.
										??"(VD)"
									
									CASE (uCrsFTAux.u_tipodoc == 8 OR uCrsFTAux.ndoc == 84) AND uCrsFTAux.u_lote = 0 AND uCrsFTAux.cobrado=.t.
										??"(VD)(S)"
									
									CASE (uCrsFTAux.u_tipodoc == 8 OR uCrsFTAux.ndoc == 84) AND uCrsFTAux.u_lote > 0
										??"(VD-CR)"
									
									CASE (uCrsFTAux.u_tipodoc == 9) AND uCrsFTAux.cobrado == .f. AND uCrsFTAux.u_lote == 0
										??"(FT)"
										
									CASE (uCrsFTAux.u_tipodoc == 9) AND uCrsFTAux.cobrado == .t. AND uCrsFTAux.u_lote == 0
										??"(FT)(S)"
									
									CASE (uCrsFTAux.u_tipodoc == 9) AND uCrsFTAux.u_lote > 0
										??"(FT-CR)"
										
									CASE (uCrsFTAux.u_tipodoc == 11)
	 									??"(VDM)"
	 									
	 								OTHERWISE
	 									??"(   )"
	 							ENDCASE
					
								** Imprimir serie de factura��o **
								??"/" + astr(uCrsFTAux.ndoc)
								** imprimir hash
								?? ' - '+ALLTRIM(uCrsIduCert.parte1)
							**ENDIF

*!*	                                IF uf_gerais_actgrelha("", "FTQRCODE", "exec up_print_qrcode_a4 '"+ALLTRIM(uCrsFTAux.ftstamp)+"'")
*!*	                                    IF RECCOUNT("FTQRCODE") <> 0
*!*	                                        SELECT FTQRCODE
*!*	                                        GO TOP 

*!*	                                        uv_qr = uf_imprimirgerais_gerarQRCode(ALLTRIM(FTQRCODE.qrcode))

*!*	                                        ?""
*!*	                                        ???uv_qr
*!*	                                        ?""

*!*	                                    ENDIF
*!*									ENDIF
							
							lcValidaCert 	= .t.
							
							lcCharCert		= ALLTRIM(uCrsIduCert.parte1)
							lcNrCert 		= ALLTRIM(uCrsIduCert.parte2) + ' ' + ALLTRIM(uCrsFt2Aux.u_docorig)
							
							SELECT uCrsStampsQRCode
							APPEND BLANK 
							replace uCrsStampsQRCode.ftstamp WITH ALLTRIM(uCrsFTAux.ftstamp)
							
						ENDIF
					ENDIF
					fecha("uCrsIduCert")
				ENDIF
			ENDIF 	
			SELECT uCrsFTAux
		ENDSCAN
		
		? 'Os bens/servicos adquiridos foram colocados a'
		? 'disposicao do adquirente na data do documento'
		&& Certifica��o
		IF !lcValidaCert
			IF uf_gerais_actGrelha("","ucrsRodapeCert",[exec up_cert_TextoRodape '] + ALLTRIM(uCrsFTAux.tiposaft) + ['])
				**Alterado novas regras certifica��o 20120313		
				?ALLTRIM(ucrsRodapeCert.rodape)
			ELSE
				IF lcMotIsencaoIva == .t.	
					?"Processado por Computador - IVA INCLUIDO"
				ENDIF
			ENDIF
		ELSE
			IF myNrVendasTalao<=1
				? ALLTRIM(lcCharCert)+'-'+ALLTRIM(lcNrCert)
			ELSE
				? ALLTRIM(lcNrCert)
			ENDIF
		ENDIF

		&& Publicidade 
		? "Logitools, Lda"
		
		&& Identificar Operador
		SELECT uCrsFTAux
		GO TOP 
		?"Atendido Por: "
		IF lcValidaCodAlt == 0
			??Substr(uCrsFTAux.vendnm,1,30) + " (" + Alltrim(Str(uCrsFTAux.vendedor)) + ")"
			?""
		ELSE
			??Substr(uCrsFTAux.vendnm,1,30) + " (" + Alltrim(Str(myOpAltCod)) + ")"
			?""
		ENDIF

		&& Texto rodap� Tal�es
		uf_gerais_criarRodapePOS()
		
		&& Funcoes abrir Gaveta e Cortar Papel
		IF reccount("uCrsStampsQRCode")=0 OR UPPER(mypaisconfsoftw)!='PORTUGAL'
				uf_gerais_feedCutPOS()
			ENDIF 
		**uf_gerais_openDrawPOS()
		
	
		**PSICO**
		** VERIFICAR SE � NECESS�RIO IMPRIMIR O TALAO DE PSICOTROPICOS **
		LOCAL lcStampDuplicado, lcPosCrsFiPsico, lcPsico
		STORE '' TO lcStampDuplicado
		STORE 0 TO lcPosCrsFiPsico, lcPsico
		
		
		SELECT uCrsFiAux
		GO TOP
		SCAN
			IF uCrsFiAux.u_psico=.t.
				lcPsico = lcPsico + 1
			ENDIF
		ENDSCAN
		
*!*			** CASO SEJA NECESS�RIO, IMPRIMIR O TAL�O DE PSICOTROPICOS
*!*			IF lcPsico > 0
*!*				SELECT uCrsFiAux
*!*				GO TOP
*!*				SCAN FOR uCrsFiAux.u_psico = .t.
*!*					IF ALLTRIM(lcStampDuplicado) != ALLTRIM(uCrsFiAux.ftstamp)
*!*						lcPosCrsFiPsico = RECNO("uCrsFiAux")
*!*						lcStampDuplicado = ALLTRIM(uCrsFiAux.ftstamp)
*!*						uf_imprimirpos_ImpPrevPsico(ALLTRIM(uCrsFiAux.ftstamp), .f., "ATENDIMENTO")
*!*						TRY 
*!*							SELECT uCrsFiAux
*!*							GO lcPosCrsFiPsico 
*!*						CATCH
*!*							***
*!*						ENDTRY 
*!*					ENDIF 
*!*				ENDSCAN
*!*			ENDIF 
	ENDIF

	uf_gerais_setImpressoraPOS(.f.)
	
	&& fecha cursores

	IF reccount("uCrsStampsQRCode")>0 AND UPPER(mypaisconfsoftw)=='PORTUGAL'
		** QRCODES
		LOCAL lcTokenQrcode, lcordemQRCode
		STORE 1 TO lcordemQRCode
		lcTokenQrcode = uf_gerais_stamp()
		SELECT uCrsStampsQRCode
		GO TOP 
		SCAN
			uf_gerais_gerar_qrcodejpg(ALLTRIM(uCrsStampsQRCode.ftstamp), ALLTRIM(lcTokenQrcode), lcordemQRCode)
			lcordemQRCode = lcordemQRCode + 1
		ENDSCAN 
		** Indicar o corte na �ltima imagem
		IF uf_gerais_getParameter_site("ADM0000000149", "BOOL", mysite)
			TEXT TO lcsql TEXTMERGE NOSHOW
			 	update 
			 		image_printPos 
			 	set 
			 		feedcut=1 
			 	where 
			 		token='<<ALLTRIM(lcTokenQrcode)>>' 
			 		and [order]=(select max([order]) from image_printPos (nolock) where token='<<ALLTRIM(lcTokenQrcode)>>')
		    ENDTEXT
		    uf_gerais_actgrelha("", "", lcsql)
		ENDIF 
		
		STORE '&' TO lcadd
		lcwspath = ALLTRIM(uf_gerais_getparameter("ADM0000000185", "TEXT"))+'\ltsPrinter\ltsPrinter.jar'+ ' ' + '"--idCl='+UPPER(ALLTRIM(sql_db))+'" '+' "--token='+ALLTRIM(lcTokenQrcode)+'"'
		lcwsdir = ALLTRIM(uf_gerais_getparameter("ADM0000000185", "TEXT"))+'\ltsPrinter'
		lcwspath = "cmd /c cd /d "+lcwsdir+" "+lcadd+lcadd+" javaw -jar "+lcwspath


		owsshell = CREATEOBJECT("WScript.Shell")
		owsshell.run(lcwspath, 0, .T.)
		
		&& Funcoes abrir Gaveta e Cortar Papel
		IF !uf_gerais_getParameter_site("ADM0000000149", "BOOL", mysite)
			uf_gerais_setImpressoraPOS(.t.)
			uf_gerais_feedCutPOS()
			uf_gerais_setImpressoraPOS(.f.)
		ENDIF 
	ENDIF 
	
	IF USED("uCrsStampsQRCode")
		fecha("uCrsStampsQRCode")
	ENDIF 
	
	
	** CASO SEJA NECESS�RIO, IMPRIMIR O TAL�O DE PSICOTROPICOS
	IF TYPE("lcPisoc") == "N"
		IF lcPsico > 0
			SELECT uCrsFiAux
			GO TOP
			SCAN FOR uCrsFiAux.u_psico = .t.
				IF ALLTRIM(lcStampDuplicado) != ALLTRIM(uCrsFiAux.ftstamp)
					lcPosCrsFiPsico = RECNO("uCrsFiAux")
					lcStampDuplicado = ALLTRIM(uCrsFiAux.ftstamp)
					uf_gerais_setImpressoraPOS(.t.)
					uf_imprimirpos_ImpPrevPsico(ALLTRIM(uCrsFiAux.ftstamp), .f., "ATENDIMENTO")
					uf_gerais_feedCutPOS()
					uf_gerais_setImpressoraPOS(.f.)
					TRY 
						SELECT uCrsFiAux
						GO lcPosCrsFiPsico 
					CATCH
						***
					ENDTRY 
				ENDIF 
			ENDSCAN
		ENDIF 
	ENDIF
		
	IF USED("uCrsFiPromo")
		fecha("uCrsFiPromo")
	ENDIF
	
	IF USED("uCrsUltTranCartao")
		fecha("uCrsUltTranCartao")
	ENDIF
	
ENDFUNC


FUNCTION uf_imprimirPOS_ImpTalao_reservas
	LPARAMETERS LcbostampRes
	LOCAL lcValorAdiantado 
	STORE 0 TO lcValorAdiantado 

    IF uf_gerais_getUmValor("B_terminal", "usaDriverWin", "terminal = '" + ALLTRIM(myTerm) + "'")
        RETURN .F.
    ENDIF

	lcSQL = ''
	TEXT TO lcSQL TEXTMERGE NOSHOW
		select sum(etiliquido) as totad from fi (nolock) where fistamp in (select fistamp from bi2 (nolock) where bostamp='<<ALLTRIM(LcbostampRes)>>')
	ENDTEXT
	uf_gerais_actGrelha("","uCrsResAd",lcSQL)
	IF RECCOUNT("uCrsResAd")>0
		lcValorAdiantado = uCrsResAd.totad
	ENDIF 
	fecha("uCrsResAd")
	
	IF lcValorAdiantado = 0 
		&& Verificar se a impressora existe
		LOCAL lcValidaImp, lcIdReserva
		STORE '' TO lcIdReserva
		lcValidaImp = uf_gerais_setImpressoraPOS(.t.)

		IF lcValidaImp=.f.
			RETURN .f.
		ENDIF 
		SELECT uCrsReservaBO
		IF len(alltrim(str(uCrsReservaBO.no)))<7
			lcIdReserva = 'R' + ALLTRIM(STR(uCrsReservaBO.obrano)) + 'C' + ALLTRIM(STR(uCrsReservaBO.no)) + 'D' + ALLTRIM(STR(uCrsReservaBO.estab))
		ELSE
			lcIdReserva = 'NRES' + ALLTRIM(STR(uCrsReservaBO.obrano)) + 'E'
		ENDIF 
		
		IF !uf_gerais_getParameter("ADM0000000126","BOOL")

			uf_gerais_criarCabecalhoPOS()	
			
			uf_gerais_textoVermPOS()
			
			?"Reserva de Cliente Nr."
			??TRANSFORM(uCrsReservaBO.obrano,"999999999")+" "
			
			uf_gerais_resetCorPOS()
			
			** guardar dados do operador **
			STORE 0 TO lcOpCode
			SELECT uCrsReservaBO
			GO TOP
			lcOpCode = uCrsReservaBO.vendedor
			
			IF uf_gerais_getParameter("ADM0000000047","BOOL")
				lcSQL="select top 1 ISNULL(u_no,0) as no from cm3 (nolock) where cm=" + astr(uCrsReservaBO.vendedor)
			ELSE
				lcSQL="select top 1 ISNULL(cm,0) as no from cm3 (nolock) where cm=" + astr(uCrsReservaBO.vendedor)
			ENDIF 
			
			IF uf_gerais_actGrelha("","uCrsCodOp",lcSQL)
				IF RECCOUNT()>0
					lcOpCode = uCrsCodOp.no
				ENDIF
				fecha("uCrsCodOp")
			ENDIF
			
			SELECT uCrsReservaBO
			** Vendedor
			?"Op: "
			IF lcOpCode==0
				??Substr(uCrsReservaBO.vendnm,1,30) + " (" + Alltrim(Str(uCrsReservaBO.vendedor)) + ")"
			ELSE
				??Substr(uCrsReservaBO.vendnm,1,30) + " (" + Alltrim(Str(lcOpCode)) + ")"
			ENDIF
			
			** Data
			?"Data: "
			??uf_gerais_getDate(uCrsReservaBO.dataobra,"DATA")
			??"      Hora: "
			??uCrsReservaBO.ousrhora
					
			** Validade
			?"Data Validade: "
			??uf_gerais_getDate(uCrsReservaBO.datafinal,"DATA")
					
			uf_gerais_resetCorPOS()

			uf_gerais_separadorPOS()
		
			** Dados Do Cliente **
			?"Cliente: "
			??Alltrim(uCrsCliente.NOME) + Iif(uCrsCliente.no=200, '', ' (' + Alltrim(Str(uCrsCliente.no)) + ')(' + Alltrim(Str(uCrsCliente.estab)) + ')' )
			?"Morada: "
			??TRANSFORM(uCrsCliente.MORADA,"########################################")
		
			?"Cod.Post.: "
			??TRANSFORM(uCrsCliente.CODPOST,"###################################")
			
			?"Telf.: "
			??substr(uCrsCliente.telefone,1,12)
			??" Tlmvl.: "
			??substr(uCrsCliente.tlmvl,1,13)
			
			?"Email: "
			??substr(uCrsCliente.email,1,20)
			**
			
			****** IMPRESS�O DAS COLUNAS *******
			uf_gerais_separadorPOS()
			?"Produto"
			?"     Ref.         Qtd.   "
			uf_gerais_separadorPOS()
			**
					
			**
			** A quantidade � sempre 1 se for impressas linhas individuais, tcStamBi vazio
			SELECT uCrsReservaBI
			GO TOP
			SCAN
				IF uCrsReservaBi.sel == .T.
					?SUBSTR(ALLTRIM(uCrsReservaBi.DESIGN),1,45)
					?"     " + ALLTRIM(uCrsReservaBi.ref)+"   "
					??TRANSFORM(uCrsReservaBi.QTT,"9999.9")
				ENDIF 	
			ENDSCAN
			
			
			&& Adiantamento de Reserva
			IF lcValorAdiantado > 0
				uf_gerais_separadorPOS()
				uf_gerais_textoVermPOS()
				SELECT uCrsValorAdiantado
				GO TOP
				?"Adiantamento: " + astr(lcValorAdiantado,8,2)
				uf_gerais_resetCorPOS()
			ENDIF 
			
			**
					
			&& Impressao codigo barras com identifica��o reservas
			?
			??	chr(27)+chr(97)+chr(1)
			?'Reserva'
			?
			??? chr(29) + chr(104) +chr(50) + chr(29)+ chr(72) + chr(2) + chr(29)+ chr(119)+chr(2) + chr(29)+chr(107)+chr(4) + UPPER(ALLTRIM(lcIdReserva)) + chr(0)
		
			
			uf_gerais_textoLJPOS()
				
			****** IMPRESS�O DO RODAP� *****
			uf_gerais_textoCJPOS()
			uf_gerais_separadorPOS()

			
			&& textos parametrizaveis - valida por empresa
			IF !EMPTY(ALLTRIM(uf_gerais_getParameter_site('ADM0000000083', "TEXT", mySite)))
				? ALLTRIM(uf_gerais_getParameter_site('ADM0000000083', "TEXT", mySite))
			ENDIF

				
			IF uf_gerais_actGrelha("","ucrsRodapeCert",[exec up_cert_TextoRodape ''])
				**Alterado novas regras certifica��o 20120313		
				?ALLTRIM(ucrsRodapeCert.rodape)
			ELSE
				?"Processado por Computador"
			ENDIF
			**
			
			uf_gerais_feedCutPOS()
			
		
		ENDIF
		
		** libertar impressora
		uf_gerais_setImpressoraPOS(.f.)
	ENDIF 
	
ENDFUNC 


FUNCTION uf_imprimirPOS_ImpTalao_reservas_comAD
	LPARAMETERS LcbostampRes
	LOCAL lcValorAdiantado 
	STORE 0 TO lcValorAdiantado 

	lcSQL = ''
	TEXT TO lcSQL TEXTMERGE NOSHOW
		select sum(etiliquido) as totad from fi (nolock) where fistamp in (select fistamp from bi2 (nolock) where bostamp='<<ALLTRIM(LcbostampRes)>>')
	ENDTEXT
	uf_gerais_actGrelha("","uCrsResAd",lcSQL)
	IF RECCOUNT("uCrsResAd")>0
		lcValorAdiantado = uCrsResAd.totad
	ENDIF 
	fecha("uCrsResAd")
	
	
	** Cursor BO 
	IF USED("uCrsReservaBO")
		Fecha("uCrsReservaBO")
	ENDIF
	
	IF !uf_gerais_actGrelha("","uCrsReservaBO",[SELECT TOP 1 * FROM bo (nolock) WHERE bostamp = '] + ALLTRIM(LcbostampRes) + ['])
		uf_perguntalt_chama("Ocorreu uma anomalia a obter a informa��o da reserva. Por favor contacte o Suporte.","OK","",16)
		RETURN .f.
	ENDIF
	
	IF USED("uCrsCliente")
		Fecha("uCrsCliente")
	ENDIF

	IF !uf_gerais_actGrelha("","uCrsCliente","SELECT * FROM b_utentes (nolock) where no = (select no from bo (nolock) where bostamp='" + ALLTRIM(LcbostampRes) + "') and estab = (select estab from bo (nolock) where bostamp='" + ALLTRIM(LcbostampRes) + "')")
		uf_perguntalt_chama("Ocorreu uma anomalia ao obter a informa��o do cliente. Por favor contacte o Suporte.","OK","",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF
	
	IF USED("uCrsReservaBI")
		Fecha("uCrsReservaBI")
	ENDIF
	IF !uf_gerais_actGrelha("","uCrsReservaBI",[select convert(bit,1) as sel,* from bi (nolock) WHERE bostamp = '] + ALLTRIM(LcbostampRes) + ['])
		uf_perguntalt_chama("Ocorreu uma anomalia ao obter o detalhe da reserva. Por favor contacte o Suporte.","OK","",16)
		RETURN .f.
	ENDIF
			
	&& Verificar se a impressora existe
	LOCAL lcValidaImp, lcIdReserva
	STORE '' TO lcIdReserva
	lcValidaImp = uf_gerais_setImpressoraPOS(.t.)

	IF lcValidaImp=.f.
		RETURN .f.
	ENDIF 
	SELECT uCrsReservaBO
	IF len(alltrim(str(uCrsReservaBO.no)))<7
		lcIdReserva = 'R' + ALLTRIM(STR(uCrsReservaBO.obrano)) + 'C' + ALLTRIM(STR(uCrsReservaBO.no)) + 'D' + ALLTRIM(STR(uCrsReservaBO.estab))
	ELSE
		lcIdReserva = 'NRES' + ALLTRIM(STR(uCrsReservaBO.obrano)) + 'E'
	ENDIF 
	
	IF !uf_gerais_getParameter("ADM0000000126","BOOL")

		uf_gerais_criarCabecalhoPOS()	
		
		uf_gerais_textoVermPOS()
		
		?"Reserva de Cliente Nr."
		??TRANSFORM(uCrsReservaBO.obrano,"999999999")+" "
		
		uf_gerais_resetCorPOS()
		
		** guardar dados do operador **
		STORE 0 TO lcOpCode
		SELECT uCrsReservaBO
		GO TOP
		lcOpCode = uCrsReservaBO.vendedor
		
		IF uf_gerais_getParameter("ADM0000000047","BOOL")
			lcSQL="select top 1 ISNULL(u_no,0) as no from cm3 (nolock) where cm=" + astr(uCrsReservaBO.vendedor)
		ELSE
			lcSQL="select top 1 ISNULL(cm,0) as no from cm3 (nolock) where cm=" + astr(uCrsReservaBO.vendedor)
		ENDIF 
		
		IF uf_gerais_actGrelha("","uCrsCodOp",lcSQL)
			IF RECCOUNT()>0
				lcOpCode = uCrsCodOp.no
			ENDIF
			fecha("uCrsCodOp")
		ENDIF
		
		SELECT uCrsReservaBO
		** Vendedor
		?"Op: "
		IF lcOpCode==0
			??Substr(uCrsReservaBO.vendnm,1,30) + " (" + Alltrim(Str(uCrsReservaBO.vendedor)) + ")"
		ELSE
			??Substr(uCrsReservaBO.vendnm,1,30) + " (" + Alltrim(Str(lcOpCode)) + ")"
		ENDIF
		
		** Data
		?"Data: "
		??uf_gerais_getDate(uCrsReservaBO.dataobra,"DATA")
		??"      Hora: "
		??uCrsReservaBO.ousrhora
				
		** Validade
		?"Data Validade: "
		??uf_gerais_getDate(uCrsReservaBO.datafinal,"DATA")
				
		uf_gerais_resetCorPOS()

		uf_gerais_separadorPOS()
	
		** Dados Do Cliente **
		?"Cliente: "
		??Alltrim(uCrsCliente.NOME) + Iif(uCrsCliente.no=200, '', ' (' + Alltrim(Str(uCrsCliente.no)) + ')(' + Alltrim(Str(uCrsCliente.estab)) + ')' )
		?"Morada: "
		??TRANSFORM(uCrsCliente.MORADA,"########################################")
	
		?"Cod.Post.: "
		??TRANSFORM(uCrsCliente.CODPOST,"###################################")
		
		?"Telf.: "
		??substr(uCrsCliente.telefone,1,12)
		??" Tlmvl.: "
		??substr(uCrsCliente.tlmvl,1,13)
		
		?"Email: "
		??substr(uCrsCliente.email,1,20)
		**
		
		****** IMPRESS�O DAS COLUNAS *******
		uf_gerais_separadorPOS()
		?"Produto"
		?"     Ref.         Qtd.   "
		uf_gerais_separadorPOS()
		**
				
		**
		** A quantidade � sempre 1 se for impressas linhas individuais, tcStamBi vazio
		SELECT uCrsReservaBI
		GO TOP
		SCAN
			IF uCrsReservaBi.sel == .T.
				?SUBSTR(ALLTRIM(uCrsReservaBi.DESIGN),1,45)
				?"     " + ALLTRIM(uCrsReservaBi.ref)+"   "
				??TRANSFORM(uCrsReservaBi.QTT,"9999.9")
			ENDIF 	
		ENDSCAN
		
		
		&& Adiantamento de Reserva
		IF lcValorAdiantado > 0
			uf_gerais_separadorPOS()
			uf_gerais_textoVermPOS()
			?"Adiantamento: " + astr(lcValorAdiantado,8,2)
			uf_gerais_resetCorPOS()
		ENDIF 
		
		**
				
		&& Impressao codigo barras com identifica��o reservas
		?
		??	chr(27)+chr(97)+chr(1)
		?'Reserva'
		?
		??? chr(29) + chr(104) +chr(50) + chr(29)+ chr(72) + chr(2) + chr(29)+ chr(119)+chr(2) + chr(29)+chr(107)+chr(4) + UPPER(ALLTRIM(lcIdReserva)) + chr(0)
	
		
		uf_gerais_textoLJPOS()
			
		****** IMPRESS�O DO RODAP� *****
		uf_gerais_textoCJPOS()
		uf_gerais_separadorPOS()

		
		&& textos parametrizaveis - valida por empresa
		IF !EMPTY(ALLTRIM(uf_gerais_getParameter_site('ADM0000000083', "TEXT", mySite)))
			? ALLTRIM(uf_gerais_getParameter_site('ADM0000000083', "TEXT", mySite))
		ENDIF

			
		IF uf_gerais_actGrelha("","ucrsRodapeCert",[exec up_cert_TextoRodape ''])
			**Alterado novas regras certifica��o 20120313		
			?ALLTRIM(ucrsRodapeCert.rodape)
		ELSE
			?"Processado por Computador"
		ENDIF
		**
		
		uf_gerais_feedCutPOS()
		
	
	ENDIF
	
	** libertar impressora
	uf_gerais_setImpressoraPOS(.f.)

	
ENDFUNC 


FUNCTION uf_imprimirPOS_ValImpAtendimento_AT
	LPARAMETERS lcNrAtend, lcData, lcNoCliente
	LOCAL lcSQL
*!*		SELECT uCrsPesqVendas
	
*!*		lcNrAtend = uCrsPesqVendas.u_nratend
*!*		lcData = LEFT(uCrsPesqVendas.fdata,4)
*!*		lcNoCliente = uCrsPesqVendas.no
	 
	&& verifica se o Cursor tem dados
*!*		IF !(RECCOUNT("uCrsPesqVendas")>0)
*!*			uf_perguntalt_chama("N�O EXISTEM DADOS PARA IMPRIMIR.","OK","",64)
*!*			RETURN .F.
*!*		ENDIF
	
	&& Valida tipo de documento
*!*		IF (ucrsPesqVendas.u_tipodoc == 3)
*!*			uf_perguntalt_chama("N�O � POSS�VEL IMPRIMIR TAL�O PARA ESTE TIPO DE DOCUMENTO.","OK","",64)
*!*			RETURN .F.
*!*		ENDIF
	
	&&limpa cursores
	uf_imprimirpos_limpaCursoresReimprimirAtend()
			
	lcSQL = ''
	TEXT TO lcSQL NOSHOW TEXTMERGE
		select * from B_pagcentral (nolock) WHERE nrAtend = '<<lcNrAtend>>' AND ano = '<<lcData>>' AND NO = '<<lcNoCliente>>'
	ENDTEXT 
	IF !uf_gerais_actGrelha("", "uCrsInfoAtendPagCentral", lcSql)
		uf_perguntalt_chama("N�o foi poss�vel encontrar a informa��o do atendimento. Por favor contacte o Suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF
	
	lcSQL = ''	
	TEXT TO lcSQL NOSHOW TEXTMERGE
		select td.u_tipodoc,b_utentes.nrcartao, td.tiposaft, b_utentes.no_ext,ft.* FROM ft (nolock) INNER JOIN td (nolock) ON td.ndoc = ft.ndoc INNER JOIN b_utentes (nolock) ON b_utentes.no = ft.no AND b_utentes.estab = ft.estab WHERE u_nratend = '<<lcNrAtend>>' AND ftano = '<<lcData>>' AND ft.site = '<<mysite>>' AND FT.NO = '<<lcNoCliente>>' AND td.u_tipodoc != 3 
	ENDTEXT 
	IF !uf_gerais_actGrelha("", "uCrsFtHelper", lcSql)
		uf_perguntalt_chama("N�o foi poss�vel encontrar a informa��o das vendas [FT]. Por favor contacte o Suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF	
	
	lcSQL = ''
	TEXT TO lcSQL NOSHOW TEXTMERGE
		exec up_print_docs_atendimento '<<uCrsFtHelper.ftstamp>>', '<<uCrsFtHelper.u_nratend>>', ''
	ENDTEXT 
	IF !uf_gerais_actGrelha("", "uCrsProdComp", lcSql)
		uf_perguntalt_chama("N�o foi poss�vel encontrar a informa��o das vendas [FI]. Por favor contacte o Suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF

	lcSQL = ''
	TEXT TO lcSQL NOSHOW TEXTMERGE
		SELECT ft.ftstamp,ft.tipodoc,ft.cobrado,isnull(fi2.codmotiseimp,'') as codmotiseimp, isnull(fi2.motiseimp,'') as motiseimp, isnull(fi2.motiseimp,'') as motisencao, fi.* FROM fi (nolock) INNER JOIN ft (nolock) ON ft.ftstamp = fi.ftstamp INNER JOIN td (nolock) ON td.ndoc = ft.ndoc left join fi2 (nolock) on fi2.fistamp=fi.fistamp WHERE ft.u_nratend = '<<lcNrAtend>>' AND ft.ftano = '<<lcData>>' AND ft.site = '<<mysite>>' AND FT.NO = '<<lcNoCliente>>'  AND td.u_tipodoc != 3 
	ENDTEXT 
	IF !uf_gerais_actGrelha("", "uCrsFiHelper", lcSql)
		uf_perguntalt_chama("N�o foi poss�vel encontrar a informa��o das vendas [FI]. Por favor contacte o Suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF	
	
	lcSQL = ''
	TEXT TO lcSQL NOSHOW TEXTMERGE
		select ft2.*,ft.cobrado,ft.ftstamp FROM ft2 (nolock) INNER JOIN ft (nolock) ON ft.ftstamp = ft2.ft2stamp WHERE ft.u_nratend = '<<lcNrAtend>>' AND ft.ftano = '<<lcData>>' AND  ft.site = '<<mysite>>' AND FT.NO = '<<lcNoCliente>>' 
	ENDTEXT 
	
	IF !uf_gerais_actGrelha("", "uCrsFt2Helper", lcSql)
		uf_perguntalt_chama("N�o foi poss�vel encontrar a informa��o das vendas [FT2]. Por favor contacte o Suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF	

	
	&& imprime atendimento se nao suspensas
	SELECT * FROM  uCrsFtHelper  WHERE cobrado=.f. INTO CURSOR uCrsFtAux READWRITE 
	SELECT * FROM  uCrsFt2Helper WHERE cobrado=.f. INTO CURSOR uCrsFt2Aux READWRITE 
	SELECT * FROM  uCrsFiHelper  WHERE cobrado=.f. INTO CURSOR uCrsFiAux READWRITE 
	
	&& se nao suspensas
	IF(RECCOUNT("uCrsFtAux")>0)
		uf_imprimirpos_ReimprimirAtend()
	ENDIF
	
	SELECT * FROM  uCrsFtHelper  WHERE cobrado=.t. INTO CURSOR uCrsFtHelperAux  READWRITE 

	&&Imprimir suspensas em taloes diferentes
	SELECT uCrsFtHelperAux  
	GO TOP
	SCAN 
			
			SELECT * FROM  uCrsFtHelper  WHERE ALLTRIM(uCrsFtHelper.ftstamp)=ALLTRIM(uCrsFtHelperAux.ftstamp) INTO CURSOR uCrsFtAux READWRITE 
			SELECT * FROM  uCrsFt2Helper WHERE ALLTRIM(uCrsFt2Helper.ftstamp)=ALLTRIM(uCrsFtHelperAux.ftstamp) INTO CURSOR uCrsFt2Aux READWRITE 
			SELECT * FROM  uCrsFiHelper  WHERE ALLTRIM(uCrsFiHelper.ftstamp)=ALLTRIM(uCrsFtHelperAux.ftstamp) INTO CURSOR uCrsFiAux READWRITE 
			
			lcSQL = ''
			TEXT TO lcSQL NOSHOW TEXTMERGE
				exec up_print_docs_atendimento '<<uCrsFtHelper.ftstamp>>', '<<uCrsFtHelper.u_nratend>>', ''
			ENDTEXT 
			IF !uf_gerais_actGrelha("", "uCrsProdComp", lcSql)
				uf_perguntalt_chama("N�o foi poss�vel encontrar a informa��o das vendas [FI]. Por favor contacte o Suporte.", "", "OK", 32)
				RETURN .f.
			ENDIF
			&& se suspensas
			IF(RECCOUNT("uCrsFtAux")>0)
				uf_imprimirpos_ReimprimirAtend()
			ENDIF
	
	ENDSCAN
	

	&&limpa cursores
	uf_imprimirpos_limpaCursoresReimprimirAtend()
	
ENDFUNC	



FUNCTION uf_imprimirpos_impRecTalaoComum_talao

	LPARAMETERS lcStamp, lcVerso
	LOCAL lcMsg, lcOrdemImpressaoReceita, lcCompartV1, lcCompartV2
	lcMsg = ""

	    IF USED("uCrsFt")
	       fecha("uCrsFt")
		ENDIF 
	    IF USED("uCrsFt2")
	       fecha("uCrsFt2")
	    ENDIF 
	    IF USED("uCrsFi")
	        fecha("uCrsFi")
	    ENDIF 
		IF USED("uCrsFi2")
			fecha("uCrsFi2")
		ENDIF 

	    uf_gerais_actGrelha("",[uCrsFt],[exec up_touch_ft ']+lcStamp+['])
	    select uCrsFt
	    uf_gerais_actGrelha("",[uCrsFt2],[exec up_touch_ft2 ']+lcStamp+['])
	    select uCrsFt2
	    uf_gerais_actGrelha("",[uCrsFi],[exec up_touch_fi ']+alltrim(lcStamp)+[', '', 0])
	    select uCrsFi
		uf_gerais_actGrelha("",[uCrsFi2],[exec up_touch_fi2 ']+alltrim(lcStamp)+[', ''])
		select uCrsFi2
	    
	    IF USED("uCrsCabVendasprint")
	    	LOCAL lcPos 
			Select uCrsCabVendasprint
			lcPos = Recno()
			
	    	LOCAL lcnrrecprint
	    	lcnrrecprint = ALLTRIM(uCrsFt2.u_receita)
	    	SELECT uCrsFt
	    	IF ALLTRIM(uCrsFt.nmdoc) == 'Inser��o de Receita'
		    	SELECT uCrsCabVendasprint
		    	GO TOP 
		    	SCAN 
		    		IF AT(ALLTRIM(lcnrrecprint),ALLTRIM(uCrsCabVendasprint.design))>0
		    			SELECT uCrsCabVendasprint
		    			DELETE 
		    		ENDIF 
		    	ENDSCAN 
		    	
		    	SELECT uCrsCabVendasprint
				TRY 
					GO lcPos 
				CATCH
					GO TOP  
				ENDTRY
			ENDIF 
		
	    ENDIF 
		
		IF USED("uCrsImpTalaoPos")
			SELECT * FROM uCrsImpTalaoPos WHERE !EMPTY(uCrsImpTalaoPos.nrreceita) INTO CURSOR uCrsImpTalaoPosAux READWRITE 
		ENDIF 
		
		IF USED("uCrsImpTalaoPos ")
			fecha("uCrsImpTalaoPosAux")
		ENDIF 
		
		** cursores e variaveis para o data-matrix (cabe�alho e linhas)
		LOCAL dm_codigofarmacia, dm_codigoentidade, dm_data, dm_operador, dm_serie, dm_lote, dm_posicaolote, dm_nrvenda, dm_nrreceita, dm_campo2, dm_campo3, dm_campo4, dm_totalutenteliquido, lcFee, lcTotalFee
		STORE "" TO dm_codigofarmacia, dm_codigoentidade, dm_data, dm_operador, dm_serie, dm_lote, dm_posicaolote, dm_nrvenda, dm_nrreceita, dm_campo2, dm_campo3, dm_campo4, dm_totalutenteliquido
		STORE 0.00 TO lcFee, lcTotalFee
		
		IF YEAR(uCrsFt.fdata) >= 2017
			CREATE CURSOR uCrsDMl(cnp c(7), portaria c(3), pvp c(6), pref c(6), comparticipacao c(6), valorutente c(6), p4mb c(6),fee c(6), direitoopcao c(1))
		ELSE
			CREATE CURSOR uCrsDMl(cnp c(7), portaria c(3), pvp c(6), pref c(6), comparticipacao c(6), valorutente c(6), direitoopcao c(1))
		ENDIF
		
		** IMPRESS�O DO CABE�ALHO
		** NOME DA EMPRESA ***
		lcMsg = lcMsg + "??" + "?10?10?10"
		lcMsg = lcMsg + "??" + SUBSTR(uCrsE1.NOMECOMP,1,53)
		
		** OUTROS DADOS DA EMPRESA
		lcMsg = lcMsg + "??" + "?10"
		lcMsg = lcMsg + "??" + SUBSTR(uCrsE1.local,1,10)
		lcMsg = lcMsg + "??" + " Tel:"
		lcMsg = lcMsg + "??" + TRANSFORM(uCrsE1.TELEFONE,"############")
		lcMsg = lcMsg + "??" + ALLTRIM(uf_gerais_getMacrosReports(2))
		lcMsg = lcMsg + "??" + "NIF:"
		lcMsg = lcMsg + "??" + TRANSFORM(uCrsE1.NCONT,"99999999999999")
		lcMsg = lcMsg + "??" + "?10"
		lcMsg = lcMsg + "??" + "Dir. Tec. "
		lcMsg = lcMsg + "??" + SUBSTR(uCrsE1.U_DIRTEC,1,43)
		lcMsg = lcMsg + "??" + "?10"
		
		** ??##< && Indica que o que vem a seguir � impresso ao lado do datamatrix

		lcMsg = lcMsg + "??##<" + "Cap.Social: " + TRANSFORM(uCrsE1.ecapsocial,"###########") && capital social
		lcMsg = lcMsg + "??##<" + "DOCUMENTO PARA FATURACAO"

		&& impress�o da data da dispensa no verso de receita - se data da dispensa (bidata) != vazio imprime data dispensa sen�o imprime data fatura
		&& no caso das re inser��es imprime data da receita
		IF uCrsFt.u_tipodoc == 3 && se for uma inser��o de receita imprime cdata
			**IF uf_gerais_getDate(uCrsFt.CDATA,"DATA") == '01.01.1900'
			IF uf_gerais_getDate(uCrsFt.BIDATA,"DATA") == '01.01.1900'		
				lcMsg = lcMsg + "??" + '                        '
						
				dm_data = "00000000" && datamatrix data
			ELSE
				&& lcMsg = lcMsg + "??" + uf_gerais_getDate(uCrsFt.CDATA,"DATA") + ' ' + Alltrim(uCrsFt.usrhora)
				lcMsg = lcMsg + "??" + uf_gerais_getDate(uCrsFt.BIDATA,"DATA") + ' ' + Alltrim(uCrsFt.usrhora)			

				&& dm_data = REPLICATE('0',8-LEN(uf_gerais_getDate(uCrsFt.CDATA,"SQL"))) + uf_gerais_getDate(uCrsFt.CDATA,"SQL") && datamatrix data			
				dm_data = REPLICATE('0',8-LEN(uf_gerais_getDate(uCrsFt.BIDATA,"SQL"))) + uf_gerais_getDate(uCrsFt.CDATA,"SQL") && datamatrix data						
			ENDIF
		ELSE
			&& IF uf_gerais_getDate(uCrsFt.CDATA,"DATA") == '01.01.1900'
			IF uf_gerais_getDate(uCrsFt.BIDATA,"DATA") == '01.01.1900'		
				lcMsg = lcMsg + "??" + uf_gerais_getDate(uCrsFt.FDATA,"DATA") + ' ' + ALLTRIM(uCrsFt.usrhora)
				
				dm_data = REPLICATE('0',8-LEN(uf_gerais_getDate(uCrsFt.FDATA,"SQL"))) + uf_gerais_getDate(uCrsFt.FDATA,"SQL") && datamatrix data			
			ELSE
				&&lcMsg = lcMsg + "??" + uf_gerais_getDate(uCrsFt.CDATA,"DATA") + ' ' + Alltrim(uCrsFt.usrhora)
				lcMsg = lcMsg + "??" + uf_gerais_getDate(uCrsFt.BIDATA,"DATA") + ' ' + ALLTRIM(uCrsFt.usrhora)

				&&dm_data = REPLICATE('0',8-LEN(uf_gerais_getDate(uCrsFt.CDATA,"SQL"))) + uf_gerais_getDate(uCrsFt.CDATA,"SQL") && data			
				dm_data = REPLICATE('0',8-LEN(uf_gerais_getDate(uCrsFt.BIDATA,"SQL"))) + uf_gerais_getDate(uCrsFt.BIDATA,"SQL") && data
			ENDIF
		ENDIF

		** Tipo e Nr Documento
		lcMsg = lcMsg + " Vnd - " + ALLTRIM(Str(uCrsFt.NDOC)) + "/" + astr(uCrsFt.FNO) + "(" + astr(uCrsFt.VENDEDOR) + ")"
		lcMsg = lcMsg + "??" + "?10"
		
		dm_operador  = REPLICATE('0',10-LEN(ALLTRIM(LEFT(uCrsFt.vendnm,10)))) + ALLTRIM(LEFT(uCrsFt.vendnm,10)) && datamatrix vendedor
		dm_nrvenda   = REPLICATE('0',7-LEN(astr(uCrsFt.FNO))) + astr(uCrsFt.FNO) && datamatrix n�mero venda
		dm_nrreceita = REPLICATE('0',20-LEN(ALLTRIM(uCrsFt2.u_receita))) + ALLTRIM(uCrsFt2.u_receita) && datamatrix receita

		** Ativar tinteiro a vermelho
		IF uf_gerais_getParameter('ADM0000000196', 'BOOL')
			lcMsg = lcMsg + "??##<"	+ "?29" + "?40" + "?78" + "?2" + "?0" + "?48" + "?50"
		ELSE
			lcMsg = lcMsg + "??##<"	&&+ "?29" + "?40" + "?78" + "?2" + "?0" + "?48" + "?50"
		ENDIF 
		
		DO CASE
			CASE lcVerso = 1
				lcMsg = lcMsg + "?<" + uCrsFt2.U_CODIGO + " " + SUBSTR(uCrsFt2.U_DESIGN,1,30)

				** datamatrix
				dm_codigoentidade = REPLICATE('0',3-LEN(ALLTRIM(uCrsFt2.U_CODIGO))) + ALLTRIM(uCrsFt2.U_CODIGO) && C�digo entidade (??? c�digo plano)
			
			CASE lcVerso = 2
				lcMsg = lcMsg + "?<" + uCrsFt2.U_CODIGO2 + " " + SUBSTR(uCrsFt2.U_DESIGN2,1,30)

				** datamatrix
				dm_codigoentidade = REPLICATE('0',3-LEN(ALLTRIM(uCrsFt2.U_CODIGO2))) + ALLTRIM(uCrsFt2.U_CODIGO2) && C�digo entidade 2 (??? c�digo plano)
		ENDCASE

		** Ativar tinteiro a preto
		IF uf_gerais_getParameter('ADM0000000196', 'BOOL')
			lcMsg = lcMsg + "?29" + "?40" + "?78" + "?2" + "?0" + "?48" + "?49"
		ENDIF 
		
		** Ativar tinteiro a vermelho
		IF uf_gerais_getParameter('ADM0000000196', 'BOOL')
			lcMsg = lcMsg + "??##<"	+ "?29" + "?40" + "?78" + "?2" + "?0" + "?48" + "?50"
		ELSE
			lcMsg = lcMsg + "??##<"	&&+ "?29" + "?40" + "?78" + "?2" + "?0" + "?48" + "?50"
		ENDIF 
		** tlote, lote, nreceita, slote
		lcMsg = lcMsg + "?<" + "T: " + T.TLOTE + " " + "L: " + L.LOTE + " " + "R: " + R.NRECEITA + " " + "S: " + S.SLOTE
		** Ativar tinteiro a preto
		IF uf_gerais_getParameter('ADM0000000196', 'BOOL')
			lcMsg = lcMsg + "?29" + "?40" + "?78" + "?2" + "?0" + "?48" + "?49"
		ENDIF 
		
		dm_lote    	   = REPLICATE('0',4-LEN(ASTR(L.LOTE))) + ASTR(L.LOTE) && datamatrix lote
		dm_posicaolote = REPLICATE('0',5-LEN(ASTR(R.NRECEITA))) + ASTR(R.NRECEITA) && damatrix posicao lote
		dm_serie       = REPLICATE('0',3-LEN(ASTR(S.SLOTE))) + ASTR(S.SLOTE) && datamatrix serie
		
		** Impress�o Nr de receita
		lcMsg = lcMsg + "??##<" + "Nr.Rec.: " + uCrsFt2.u_receita
						
		** Impress�o Nr de benefici�rio - s� imprime o n� de benefici�rio para entidades complementares
		SELECT uCrsFt2
		LOCATE FOR ALLTRIM(uCrsFt2.ft2stamp) == ALLTRIM(lcStamp)
		DO CASE
			CASE lcVerso = 1
				IF !EMPTY(ALLTRIM(uCrsFt2.U_NBENEF2)) AND ALLTRIM(uCrsFt2.U_NBENEF2) <> ALLTRIM(uCrsFt2.c2codpost)
					lcMsg = lcMsg + "??##<" + "Nr.Benef.: " + uCrsFt2.U_NBENEF2
				ENDIF
				IF EMPTY(ALLTRIM(uCrsFt2.U_NBENEF2)) AND !EMPTY(ALLTRIM(uCrsFt2.U_NBENEF)) AND ALLTRIM(uCrsFt2.U_NBENEF) <> ALLTRIM(uCrsFt2.c2codpost)
					lcMsg = lcMsg + "??##<" + "Nr.Benef.: " + uCrsFt2.U_NBENEF
				ENDIF
            IF !EMPTY(uCrsFt2.c2codpost)
               lcMsg = lcMsg + "??##<" + "Nr.Cart�o.: " + ALLTRIM(uCrsFt2.c2codpost)
            ENDIF

			**	IF !EMPTY(ALLTRIM(uCrsFt2.U_NBENEF))
			**		lcMsg = lcMsg + "??##<" + "Nr.Benef.: " + uCrsFt2.U_NBENEF
			**	ENDIF
			OTHERWISE
		ENDCASE

		** tag para no fim ser substitu�da pelo datamatrix
		** ter de ser inserida depois do texto a imprimir ao lado do mesmo
		lcMsg = lcMsg + "%%1"
		
		lcMsg = lcMsg + "??" + "?10"
		lcMsg = lcMsg + "??" + "?27" + "?51" + "?15" && diminuir line spacing
		lcMsg = lcMsg + "??" + "?27" + "?45" + "?1" && ativar sublinhado
		IF YEAR(uCrsFt.fdata) >= 2017
			lcMsg = lcMsg + "??" + "Prod  Pvp   Pref  Qtt  CompUni  CompTot  Utente  P4MB Fee"
		ELSE
			lcMsg = lcMsg + "??" + "Prod  Pvp   Pref  Qtt  CompUni  CompTot  Utente"
		ENDIF
		lcMsg = lcMsg + "??" + "?10"
		lcMsg = lcMsg + "??" + "?27" + "?45" + "?0" && desativar sublinhado
		
		** guardar tabela diplomas para mapear com id para o c�digo matrix
		local lcDiploma_id
		IF !USED("uCrsDplms")
			uf_gerais_actGrelha("","uCrsDplms","select u_design, diploma_id from dplms (nolock) where u_design!=''")
		ENDIF
		
		
		&&:TODO
		**guarda a ordem de impress�o de receita
		ordemImpressaoReceita=uCrsE1.ordemimpressaoreceita
		
		
		
		IF(lcOrdemImpressaoReceita==.f.)
			lcCompartV1 = uCrsFt2.u_abrev
			lcCompartV2 = uCrsFt2.u_abrev2
		ELSE
			lcCompartV1 = uCrsFt2.u_abrev2
			lcCompartV2 = uCrsFt2.u_abrev
		ENDIF
		
		
		
		
			
		** IMPRESS�O DAS LINHAS **
		LOCAL lcContadorLinhas
		lcContadorLinhas = 1
		SELECT uCrsFi
		DO CASE 
			CASE lcVerso = 1
				SCAN FOR uCrsFi.U_ETTENT1 > 0 AND ALLTRIM(uCrsFi.ftstamp) == ALLTRIM(uCrsFt.ftstamp)
					*FOR i=1 to qtt
						lcMsg = lcMsg + "??" + "?10"
						lcMsg = lcMsg + "??" + astr(lcContadorLinhas) + ") " + ALLTRIM(substr(uCrsFi.DESIGN,1,50)) + IIF(!EMPTY(uCrsFi.u_codemb)," -Cod. Emb.:" + uCrsFi.u_codemb,"")
						lcMsg = lcMsg + "??" + "?27" + "?51" + "?20"					
						lcMsg = lcMsg + "??" + "?10"
						lcMsg = lcMsg + "??" + TRANSFORM(uCrsFi.U_EPVP,"9999999.99")
						lcMsg = lcMsg + "??" + TRANSFORM(uCrsFi.U_EPREF,"999999.99")
						lcMsg = lcMsg + "??" + "  " + astr(uCrsFi.qtt) 
						lcMsg = lcMsg + "??" + "    " + ALLTRIM(TRANSFORM(ROUND((uCrsFi.U_ETTENT1/uCrsFi.qtt),2), "99999999.99 "))
						lcMsg = lcMsg + "??" + "    " + ALLTRIM(TRANSFORM(ROUND(uCrsFi.U_ETTENT1,2), "99999999.99 "))		
						lcMsg = lcMsg + "??" + "    " + ALLTRIM(TRANSFORM(ROUND((uCrsFi.U_EPVP-uCrsFi.U_ETTENT1/uCrsFi.qtt)*uCrsFi.qtt,2), "9999999.99"))
						IF YEAR(uCrsFt.fdata) >= 2017 AND ALLTRIM(lcCompartV1) =="SNS" && uCrsFi.u_generico
							lcMsg = lcMsg + "??" + "   " + ALLTRIM(TRANSFORM(ROUND(uCrsFi.PvpTop4,2), "9999999.99"))
							
							IF(MONTH(uCrsFt.fdata) == 1  and year(uCrsFt.fdata) <= 2017)
								IF uCrsFi.u_generico
									lcMSg = lcMsg + "??" + "  " + IIF(uCrsFi.pvptop4 > 0 AND uCrsFi.u_epvp <= uCrsFi.pvptop4, ALLTRIM(TRANSFORM(ROUND(0.35*uCrsFi.qtt,2), "9.99")), "0.00")
								ELSE
									lcMSg = lcMsg + "??" + "  " + IIF(uCrsFi.pvptop4 > 0 AND uCrsFi.pic <= uCrsFi.pvptop4, ALLTRIM(TRANSFORM(ROUND(0.35*uCrsFi.qtt,2), "9.99")), "0.00")
								ENDIF
								
							ELSE
								lcMSg = lcMsg + "??" + "  " + ALLTRIM(TRANSFORM(uCrsFi.pvp4_fee,"9.99"))
								
							ENDIF
									
							&&lcMSg = lcMsg + "??" + "  " + IIF(uCrsFi.pvptop4 > 0 AND uCrsFi.u_epvp <= uCrsFi.pvptop4, ALLTRIM(TRANSFORM(ROUND(0.35*uCrsFi.qtt,2), "9.99")), "0.00")
						ENDIF
						lcMsg = lcMsg + "??" + "?10"
						FOR i = 1 TO qtt
							IF i == uCrsFi.qtt
								lcMsg = lcMsg + "??" + "?29" + "?102" + "?1" + "?29" + "?72" + "?2" + "?29" + "?119" + "?3" + "?29" + "?104" + "?40" + "?29" + "?107" + "?4" + "??" + SUBSTR(uCrsFi.REF,1,7) + "??" + "?0"
							ELSE 
								lcMsg = lcMsg + "??" + "?29" + "?102" + "?1" + "?29" + "?72" + "?0" + "?29" + "?119" + "?3" + "?29" + "?104" + "?40" + "?29" + "?107" + "?4" + "??" + SUBSTR(uCrsFi.REF,1,7) + "??" + "?0"
								lcMsg = lcMsg + "??" + "?10"
							ENDIF
							
							&& s� adiciona caso seja inferior a 10 linhas sen�o d� erro na impress�o do verso em tal�o 
							&& O codigo dm nem sequer � necess�rio nestes casos (impress�o de verso em tal�o com receitas de mais de 10 medicamentos) -- LL 28/07/2016
							IF lcContadorLinhas < 10
								** datamatrix
								SELECT uCrsDMl
								APPEND BLANK
								REPLACE uCrsDMl.cnp WITH REPLICATE('0', 7 - LEN(alltrim(LEFT(uCrsFi.ref,7)))) + ALLTRIM(LEFT(uCrsFi.ref,7)) && datamatrix cnp
								
								** datamatrix portaria
								lcDiploma_id = 0
								SELECT uCrsDplms
								GO TOP
								SCAN
									If ALLTRIM(uCrsDplms.u_design) == ALLTRIM(uCrsFi.u_diploma)
										lcDiploma_id = uCrsDplms.diploma_id
									ENDIF
								ENDSCAN
								
								SELECT uCrsDMl
								replace uCrsDMl.portaria		 WITH REPLICATE('0', 3 - LEN(astr(lcDiploma_id))) + astr(lcDiploma_id) && datamatrix portaria
								replace uCrsDMl.pvp				 WITH REPLICATE('0', 6 - LEN(LEFT(astr(ROUND(uCrsFi.u_epvp * 100, 0)),6))) + LEFT(astr(ROUND(uCrsFi.u_epvp * 100,0)),6) && datamatrix pvp
								replace uCrsDMl.pref 			 WITH REPLICATE('0', 6 - LEN(LEFT(astr(ROUND(uCrsFi.u_epref * 100,0)),6))) + LEFT(astr(ROUND(uCrsFi.u_epref * 100,0)),6) && datamatrix pref
								replace uCrsDMl.comparticipacao  WITH REPLICATE('0', 6 - LEN(LEFT(astr(ROUND(round(uCrsFi.u_ettent1/uCrsFi.qtt,2) * 100,0)),6))) + LEFT(astr(ROUND(round(uCrsFi.u_ettent1/uCrsFi.qtt,2) * 100,0)),6) && datamatrix comparticipacao
								replace uCrsDMl.valorutente      WITH REPLICATE('0', 6 - LEN(LEFT(astr(ROUND(ROUND(uCrsFi.u_epvp-uCrsFi.u_ettent1/uCrsFi.qtt,2) * 100,0)),6))) + LEFT(astr(ROUND(ROUND(uCrsFi.u_epvp-uCrsFi.u_ettent1/uCrsFi.qtt,2) * 100,0)),6) && datamatrix valorutente
								
						
								
								IF YEAR(uCrsFt.fdata) >= 2017  AND ALLTRIM(lcCompartV1) =="SNS"
									
									
									replace uCrsDMl.p4mb WITH REPLICATE('0', 6 - LEN(LEFT(astr(ROUND(uCrsFi.pvptop4 * 100, 0)),6))) + LEFT(astr(ROUND(uCrsFi.pvptop4 * 100,0)),6) && datamatrix pvptop4
									&& datamatrix free
									
									IF(MONTH(uCrsFt.fdata) == 1 and YEAR(uCrsFt.fdata) == 2017 )
										IF uCrsFi.u_generico
											replace uCrsDMl.fee WITH IIF(uCrsFi.pvptop4 > 0 AND uCrsFi.u_epvp <= uCrsFi.pvptop4, '000035', '000000')
										ELSE
											replace uCrsDMl.fee WITH IIF(uCrsFi.pvptop4 > 0 AND uCrsFi.pic <= uCrsFi.pvptop4, '000035', '000000')
										ENDIF
			
										
									ELSE
										
										replace uCrsDMl.fee WITH REPLICATE('0', 6 -  LEN(LEFT(astr(ROUND(uCrsFi.pvp4_fee* 100,0)),6)))  + LEFT(astr(ROUND(uCrsFi.pvp4_fee* 100,0)),6)&& pvptop4_fee

									ENDIF
									
									
									
								ENDIF
								
								
								
								
								IF EMPTY(ALLTRIM(uCrsFi.lobs3)) && se n�o for utilizada excep��o
									replace uCrsDMl.direitoopcao WITH uCrsFi.opcao && datamatrix direito opcao	
								ELSE
									IF ALLTRIM(uCrsFi.lobs3) == 'C1'
										replace uCrsDMl.direitoopcao WITH "S" && datamatrix direito opcao Excecao C1
									ELSE
										replace uCrsDMl.direitoopcao WITH "N" && datamatrix direito opcao Restantes Excecoes
									ENDIF
								ENDIF
							ENDIF
						ENDFOR
						lcContadorLinhas = lcContadorLinhas + 1
						
					*ENDFOR
					
					SELECT uCrsFi
				ENDSCAN

			CASE lcVerso = 2
				SCAN FOR uCrsFi.U_ETTENT2 > 0 AND ALLTRIM(uCrsFi.ftstamp) == ALLTRIM(uCrsFt.ftstamp)
					*FOR i=1 to qtt
						lcMsg = lcMsg + "??" + "?10"
						lcMsg = lcMsg + "??" + astr(lcContadorLinhas) + ") " + ALLTRIM(substr(uCrsFi.DESIGN,1,50)) + IIF(!EMPTY(uCrsFi.u_codemb)," -Cod. Emb.:" + uCrsFi.u_codemb,"")
						lcMsg = lcMsg + "??" + "?27" + "?51" + "?20"
						lcMsg = lcMsg + "??" + "?10"
						lcMsg = lcMsg + "??" + TRANSFORM(uCrsFi.U_EPVP,"9999999.99")
						lcMsg = lcMsg + "??" + TRANSFORM(uCrsFi.U_EPREF,"99999999.99 ")
						lcMsg = lcMsg + "??" + "  " + astr(uCrsFi.qtt)
						lcMsg = lcMsg + "??" + "    " + ALLTRIM(TRANSFORM(ROUND((uCrsFi.U_ETTENT2/uCrsFi.qtt),2), "99999999.99 "))
						lcMsg = lcMsg + "??" + "    " + ALLTRIM(TRANSFORM(ROUND(uCrsFi.U_ETTENT2,2), "9999999.99"))
						lcMsg = lcMsg + "??" + "    " + ALLTRIM(TRANSFORM(ROUND((uCrsFi.U_EPVP-uCrsFi.U_ETTENT2/uCrsFi.qtt)*uCrsFi.qtt,2), "9999999.99"))
						
						
						
						IF YEAR(uCrsFt.fdata) >= 2017 AND ALLTRIM(lcCompartV2) =="SNS"
							lcMsg = lcMsg + "??" + "   " + ALLTRIM(TRANSFORM(ROUND(uCrsFi.PvpTop4,2), "9999999.99"))		
							&&lcMSg = lcMsg + "??" + "  " + IIF(uCrsFi.pvptop4 > 0 AND uCrsFi.u_epvp <= uCrsFi.pvptop4, ALLTRIM(TRANSFORM(ROUND(0.35*uCrsFi.qtt,2), "9.99")), "0.00")
							
							IF(MONTH(uCrsFt.fdata) == 1 and year(uCrsFt.fdata) == 2017  )
								IF uCrsFi.u_generico
									lcMSg = lcMsg + "??" + "  " + IIF(uCrsFi.pvptop4 > 0 AND uCrsFi.u_epvp <= uCrsFi.pvptop4, ALLTRIM(TRANSFORM(ROUND(0.35*uCrsFi.qtt,2), "9.99")), "0.00")
								ELSE
									lcMSg = lcMsg + "??" + "  " + IIF(uCrsFi.pvptop4 > 0 AND uCrsFi.pic <= uCrsFi.pvptop4, ALLTRIM(TRANSFORM(ROUND(0.35*uCrsFi.qtt,2), "9.99")), "0.00")
								ENDIF
								
							ELSE
								lcMSg = lcMsg + "??" + "  " + ALLTRIM(TRANSFORM(uCrsFi.pvp4_fee,"9.99"))
								
							ENDIF
							
						ENDIF
						lcMsg = lcMsg + "??" + "?10"
						FOR i = 1 to qtt	
							IF i == uCrsFi.qtt
								lcMsg = lcMsg + "??" + "?29" + "?102" + "?1" + "?29" + "?72" + "?2" + "?29" + "?119" + "?3" + "?29" + "?104" + "?40" + "?29" + "?107" + "?4" + "??" + SUBSTR(uCrsFi.REF,1,7) + "??" + "?0"
							ELSE
								lcMsg = lcMsg + "??" + "?29" + "?102" + "?1" + "?29" + "?72" + "?0" + "?29" + "?119" + "?3" + "?29" + "?104" + "?40" + "?29" + "?107" + "?4" + "??" + SUBSTR(uCrsFi.REF,1,7) + "??" + "?0"
								lcMsg = lcMsg + "??" + "?10"
							ENDIF
		
							&& s� adiciona caso seja inferior a 10 linhas sen�o d� erro na impress�o do verso em tal�o - o codigo dm nem sequer � necess�rio nestes casos (impress�o de verso em tal�o com receitas de mais de 10 medicamentos) -- LL 28/07/2016
							IF lcContadorLinhas < 10
								** datamatrix
								SELECT uCrsDMl
								APPEND BLANK
								replace uCrsDMl.cnp WITH REPLICATE('0',7-LEN(alltrim(uCrsFi.ref))) + ALLTRIM(uCrsFi.ref) && datamatrix cnp
								
								** datamatrix portaria
								lcDiploma_id = 0
								SELECT uCrsDplms
								GO TOP
								SCAN
									IF ALLTRIM(uCrsDplms.u_design) == ALLTRIM(uCrsFi.u_diploma)
										lcDiploma_id = uCrsDplms.diploma_id
									ENDIF
								ENDSCAN					
								
								SELECT uCrsDMl
								replace uCrsDMl.portaria WITH REPLICATE('0', 3 - LEN(astr(lcDiploma_id))) + astr(lcDiploma_id) && datamatrix portaria
								replace uCrsDMl.pvp WITH REPLICATE('0', 6 - LEN(LEFT(astr(ROUND(uCrsFi.u_epvp * 100, 0)),6))) + LEFT(astr(ROUND(uCrsFi.u_epvp * 100,0)),6) && datamatrix pvp
								replace uCrsDMl.pref WITH REPLICATE('0', 6 - LEN(LEFT(astr(ROUND(uCrsFi.u_epref * 100,0)),6))) + LEFT(astr(ROUND(uCrsFi.u_epref * 100,0)),6) && datamatrix pref
								replace uCrsDMl.comparticipacao WITH REPLICATE('0', 6 - LEN(LEFT(astr(ROUND(round(uCrsFi.u_ettent2/uCrsFi.qtt,2) * 100,0)),6))) + LEFT(astr(ROUND(round(uCrsFi.u_ettent2/uCrsFi.qtt,2) * 100,0)),6) && datamatrix comparticipacao
								replace uCrsDMl.valorutente WITH REPLICATE('0', 6 - LEN(LEFT(astr(ROUND(ROUND(uCrsFi.u_epvp-uCrsFi.u_ettent2/uCrsFi.qtt,2) * 100,0)),6))) + LEFT(astr(ROUND(ROUND(uCrsFi.u_epvp-uCrsFi.u_ettent2/uCrsFi.qtt,2) * 100,0)),6) && datamatrix valorutente
								
								IF YEAR(uCrsFt.fdata) >= 2017  AND ALLTRIM(lcCompartV2) =="SNS"
									
									replace uCrsDMl.p4mb WITH REPLICATE('0', 6 - LEN(LEFT(astr(ROUND(uCrsFi.pvptop4 * 100, 0)),6))) + LEFT(astr(ROUND(uCrsFi.pvptop4 * 100,0)),6) && datamatrix pvp
									&& datamatrix free
									IF(MONTH(uCrsFt.fdata) == 1 and year(uCrsFt.fdata) == 2017  )
										IF uCrsFi.u_generico
											replace uCrsDMl.fee WITH IIF(uCrsFi.pvptop4 > 0 AND uCrsFi.u_epvp <= uCrsFi.pvptop4, '000035', '000000')
										ELSE
											replace uCrsDMl.fee WITH IIF(uCrsFi.pvptop4 > 0 AND uCrsFi.pic <= uCrsFi.pvptop4, '000035', '000000')
										ENDIF
										
									ELSE
									
									   	replace uCrsDMl.fee WITH REPLICATE('0', 6 -  LEN(LEFT(astr(ROUND(uCrsFi.pvp4_fee* 100,0)),6)))  + LEFT(astr(ROUND(uCrsFi.pvp4_fee* 100,0)),6)&& pvptop4_fee
		
									ENDIF
									
									
								ENDIF
								
								
								
								


								IF EMPTY(ALLTRIM(uCrsFi.lobs3)) && se n�o for utilizada excep��o
									replace uCrsDMl.direitoopcao WITH uCrsFi.opcao && datamatrix direito opcao	
								ELSE
									IF ALLTRIM(uCrsFi.lobs3) == 'C1'
										replace uCrsDMl.direitoopcao WITH "S" && datamatrix direito opcao Excecao C1
									ELSE 
										replace uCrsDMl.direitoopcao WITH "N" && datamatrix direito opcao Restantes Excecoes
									ENDIF
								ENDIF
							ENDIF
						ENDFOR
						
						lcContadorLinhas = lcContadorLinhas + 1
						
					*ENDFOR

					SELECT uCrsFi
				ENDSCAN
				
		ENDCASE
		
		
		&& Imprime linhas que n�o est�o preenchidas
		SELECT uCrsDMl
		COUNT TO lcPosicoesDML 
		IF lcPosicoesDML < 4
			FOR i=1 TO 4-lcPosicoesDML 
				
				** datamatrix
				SELECT uCrsDMl
				APPEND BLANK
				replace cnp		 			WITH REPLICATE('0',7) && datamatrix cnp
				replace portaria 			WITH REPLICATE('0',3) && datamatrix portaria
				replace pvp 				WITH REPLICATE('0', 6) && datamatrix pvp
				replace pref				WITH REPLICATE('0', 6) && datamatrix pref
				replace comparticipacao 	WITH REPLICATE('0', 6) && datamatrix comparticipacao
				replace valorutente 		WITH REPLICATE('0', 6) && datamatrix valorutente	
				IF YEAR(uCrsFt.fdata) >= 2017
					replace uCrsDMl.p4mb	WITH REPLICATE('0', 6) && datamatrix pvptop4
					replace uCrsDMl.fee		WITH REPLICATE('0', 6) && datamatrix fee
				ENDIF
				replace direitoopcao 		WITH '0'
			ENDFOR  
		ENDIF 
		
		SELECT uCrsFi
		
		lcMsg = lcMsg + "??" + "?10"
		lcMsg = lcMsg + "??" + "-------------------------------------------"

		** PREPARAR OS TOTAIS
		DO CASE
			CASE lcVerso = 1
				calculate sum(uCrsFi.u_epvp*uCrsFi.qtt) for uCrsFi.u_ettent1>0 AND ALLTRIM(uCrsFi.ftstamp) = ALLTRIM(uCrsFt.ftstamp) to totalPvp
				calculate sum(uCrsFi.qtt) for uCrsFi.u_ettent1>0 AND ALLTRIM(uCrsFi.ftstamp) = ALLTRIM(uCrsFt.ftstamp) to totalQtt
				calculate sum(uCrsFi.u_ettent1) for uCrsFi.u_ettent1>0 AND ALLTRIM(uCrsFi.ftstamp) = ALLTRIM(uCrsFt.ftstamp) to totalComp
				calculate sum(uCrsFi.u_epvp*uCrsFi.qtt-uCrsFi.u_ettent1) FOR uCrsFi.u_ettent1>0 AND ALLTRIM(uCrsFi.ftstamp) = ALLTRIM(uCrsFt.ftstamp) to totalUtente
				
				IF YEAR(uCrsFt.fdata) >= 2017  AND ALLTRIM(lcCompartV1)=="SNS"
					IF MONTH(uCrsFt.fdata)==1 AND year(uCrsFt.fdata) == 2017
						calculate sum(0.35*uCrsfi.qtt) FOR uCrsFi.pvpTop4 > 0 AND ((uCrsFi.u_generico=.t. AND uCrsFi.u_epvp <= uCrsFi.pvptop4) OR (uCrsFi.u_generico=.f. AND uCrsFi.pic <= uCrsFi.pvptop4)) to lcTotalFee
					ELSE
						calculate sum(uCrsFi.pvp4_fee) FOR uCrsFi.u_ettent1>0 AND ALLTRIM(uCrsFi.ftstamp) = ALLTRIM(uCrsFt.ftstamp) to lcTotalFee
					ENDIF
				ENDIF
				
				
			CASE lcVerso = 2
				calculate sum(uCrsFi.u_epvp*uCrsFi.qtt) for uCrsFi.u_ettent2>0 AND ALLTRIM(uCrsFi.ftstamp) = ALLTRIM(uCrsFt.ftstamp) to totalPvp
				calculate sum(uCrsFi.qtt) for uCrsFi.u_ettent2>0 AND ALLTRIM(uCrsFi.ftstamp) = ALLTRIM(uCrsFt.ftstamp) to totalQtt
				calculate sum(uCrsFi.u_ettent2) for uCrsFi.u_ettent2>0 AND ALLTRIM(uCrsFi.ftstamp) = ALLTRIM(uCrsFt.ftstamp) to totalComp
				calculate sum(uCrsFi.u_epvp*uCrsFi.qtt-uCrsFi.u_ettent2) FOR uCrsFi.u_ettent2>0 AND ALLTRIM(uCrsFi.ftstamp) = ALLTRIM(uCrsFt.ftstamp) to totalUtente
				IF YEAR(uCrsFt.fdata) >= 2017  AND ALLTRIM(lcCompartV2)=="SNS"
					IF MONTH(uCrsFt.fdata)==1 AND year(uCrsFt.fdata) == 2017 
						calculate sum(0.35*uCrsfi.qtt) FOR uCrsFi.pvpTop4 > 0 AND ((uCrsFi.u_generico=.t. AND uCrsFi.u_epvp <= uCrsFi.pvptop4) OR (uCrsFi.u_generico=.f. AND uCrsFi.pic <= uCrsFi.pvptop4)) to lcTotalFee
					ELSE
				
						calculate sum(uCrsFi.pvp4_fee) FOR uCrsFi.u_ettent2>0 AND ALLTRIM(uCrsFi.ftstamp) = ALLTRIM(uCrsFt.ftstamp) to lcTotalFee	
					ENDIF
				ENDIF

		ENDCASE


		&& datamatrix free

		
		***** IMPRESS�O DO RODAP� *******
		lcMsg = lcMsg + "??" + "?27" + "?51" + "?25"
		lcMsg = lcMsg + "??" + "?10"
		lcMsg = lcMsg + "??" + "Total Euros"
		lcMsg = lcMsg + "??" + "?10"
		lcMsg = lcMsg + "??" + TRANSFORM(ROUND(totalPvp,2),"######.##") + "      "
		lcMsg = lcMsg + "??" + TRANSFORM(ROUND(totalQtt,2),"####")
		lcMsg = lcMsg + "??" + TRANSFORM(ROUND(totalComp,2),"######.##")
		lcMsg = lcMsg + "??" + TRANSFORM(ROUND(totalUtente,2),"########.##") + "       "
		IF YEAR(uCrsFt.fdata) >= 2017
			lcMsg = lcMsg + "??" + TRANSFORM(ROUND(lcTotalFee,2),"########.##")	
		ENDIF
		

		IF VARTYPE(UsaManCompart)!="U"
			IF !UsaManCompart 
				dm_totalutenteliquido = REPLICATE('0',6-LEN(astr(round(totalUtente,2) * 100))) + astr(round(totalUtente,2) * 100) && datamatrix total utente iliquido
			ELSE
				dm_totalutenteliquido = ''
			ENDIF 
		ELSE
			dm_totalutenteliquido = REPLICATE('0',6-LEN(astr(round(totalUtente,2) * 100))) + astr(round(totalUtente,2) * 100) && datamatrix total utente iliquido
		ENDIF 

		lcMsg = lcMsg + "??" + "?10"
		lcMsg = lcMsg + "??" + "Declaro que me foram dispensadas as " + alltrim(TRANSFORM(totalQtt,"###")) + " embalagens"
		lcMsg = lcMsg + "??" + "?10"
		lcMsg = lcMsg + "??" + "de medicamentos constantes da receita e prestados"
		lcMsg = lcMsg + "??" + "?10"
		lcMsg = lcMsg + "??" + "os conselhos e informacoes sobre a sua utilizacao."

		if uf_gerais_getdate(uCrsFt.fdata,"SQL") > "20130331"
		
			** DIREITO DE OP��O **
			LOCAL exerciDireitoOpcao, lcLinhaOpcao, lcContLinha
			STORE .f. TO exerciDireitoOpcao
			STORE '' TO lcLinhaOpcao
			*** Excep��o ***
			LOCAL nLinhaDirOpcaoC1, nLinhaDirOpcaoC2
			STORE '' TO nLinhaDirOpcaoC1, nLinhaDirOpcaoC2
			
			STORE 1 TO lcContLinha

			&& verificar se tem PVP maior que o PVP5 (pvpmaxre)
			SELECT uCrsFi
			GO TOP
			DO CASE

				CASE lcVerso == 1
					SELECT uCrsFi
					SCAN FOR uCrsFi.U_ETTENT1 > 0 AND ALLTRIM(uCrsFi.ftstamp) == ALLTRIM(uCrsFt.ftstamp)
						**FOR i=1 TO uCrsFi.qtt Alterado a 24/02/2015 devido aos agrupamentos de c�digos de barras nos versos de receita
						
							** Op��o
							** Exerci direito de opcao
							IF uCrsFi.opcao == 'S' AND EMPTY(ALLTRIM(uCrsFi.lobs3))
								exerciDireitoOpcao = .t.
								IF EMPTY(lcLinhaOpcao)
									lcLinhaOpcao = astr(lcContLinha)
								ELSE
									lcLinhaOpcao = lcLinhaOpcao + ", " + astr(lcContLinha)
								ENDIF
							ENDIF
							
							** N�o exerci direito de op��o
							IF uCrsFi.opcao == 'N' AND EMPTY(ALLTRIM(uCrsFi.lobs3))
								IF EMPTY(nLinhaDirOpcaoC2)
									nLinhaDirOpcaoC2 = astr(lcContLinha)
								ELSE
									nLinhaDirOpcaoC2 = nLinhaDirOpcaoC2 + "," + astr(lcCOntLinha)
								ENDIF
							ENDIF
												
							** Excep��o
							IF !EMPTY(ALLTRIM(uCrsFi.lobs3))
								DO CASE
								
									** exerci direito de opcao com excep��o especial
									CASE ALLTRIM(ucrsFi.lobs3) == "C1"
										IF EMPTY(nLinhaDirOpcaoC1)
											nLinhaDirOpcaoC1 = astr(lcContLinha)
										ELSE
											nLinhaDirOpcaoC1 = nLinhaDirOpcaoC1 + "," + astr(lcCOntLinha)
										ENDIF
									
									** n�o exerci direito de opcao
									CASE ALLTRIM(ucrsFi.lobs3) == "C2" OR ALLTRIM(ucrsFi.lobs3) == "A" OR ALLTRIM(ucrsFi.lobs3) == "B"
										IF EMPTY(nLinhaDirOpcaoC2)
											nLinhaDirOpcaoC2 = astr(lcContLinha)
										ELSE
											nLinhaDirOpcaoC2 = nLinhaDirOpcaoC2 + "," + astr(lcCOntLinha)
										ENDIF
								ENDCASE
							ENDIF 
						
								lcContLinha = lcContLinha + 1
						**ENDFOR
					ENDSCAN

				CASE lcVerso == 2

					SELECT uCrsFi
					SCAN FOR uCrsFi.U_ETTENT2 > 0 AND ALLTRIM(uCrsFi.ftstamp) == ALLTRIM(uCrsFt.ftstamp)
						**FOR i=1 TO uCrsFi.qtt
							** Op��o
							** Exerci direito de opcao
							IF uCrsFi.opcao == 'S' AND EMPTY(ALLTRIM(uCrsFi.lobs3))
								exerciDireitoOpcao = .t.
								IF EMPTY(lcLinhaOpcao)
									lcLinhaOpcao = astr(lcContLinha)
								ELSE
									lcLinhaOpcao = lcLinhaOpcao + ", " + astr(lcContLinha)
								ENDIF
							ENDIF
							
							** N�o exerci direito de op��o
							IF uCrsFi.opcao == 'N' AND EMPTY(ALLTRIM(uCrsFi.lobs3))
								IF EMPTY(nLinhaDirOpcaoC2)
									nLinhaDirOpcaoC2 = astr(lcContLinha)
								ELSE
									nLinhaDirOpcaoC2 = nLinhaDirOpcaoC2 + "," + astr(lcCOntLinha)
								ENDIF
							ENDIF
													
							** Excep��o
							IF !EMPTY(ALLTRIM(uCrsFi.lobs3))
								DO CASE
								
									** exerci direito de opcao com excep��o especial
									CASE ALLTRIM(ucrsFi.lobs3) == "C1"
										IF EMPTY(nLinhaDirOpcaoC1)
											nLinhaDirOpcaoC1 = astr(lcContLinha)
										ELSE
											nLinhaDirOpcaoC1 = nLinhaDirOpcaoC1 + "," + astr(lcCOntLinha)
										ENDIF
									
									** n�o exerci direito de opcao
									CASE ALLTRIM(ucrsFi.lobs3) == "C2" OR ALLTRIM(ucrsFi.lobs3) == "A" OR ALLTRIM(ucrsFi.lobs3) == "B"
										IF EMPTY(nLinhaDirOpcaoC2)
											nLinhaDirOpcaoC2 = astr(lcContLinha)
										ELSE
											nLinhaDirOpcaoC2 = nLinhaDirOpcaoC2 + "," + astr(lcCOntLinha)
										ENDIF
								ENDCASE
							ENDIF 
							
							lcContLinha = lcContLinha + 1
						**ENDFOR
					ENDSCAN
			ENDCASE

			IF !EMPTY(lcLinhaOpcao)
				lcLinhaOpcao = lcLinhaOpcao + ')'
			ENDIF
			IF !EMPTY(nLinhaDirOpcaoC1)
				nLinhaDirOpcaoC1  = nLinhaDirOpcaoC1 + ')'
			ENDIF
			IF !EMPTY(nLinhaDirOpcaoC2)
				nLinhaDirOpcaoC2  = nLinhaDirOpcaoC2 + ')'
			ENDIF
			
			IF exerciDireitoOpcao
				lcMsg = lcMsg + "??" + "?10" + "?10"
				lcMsg = lcMsg + "??" + lcLinhaOpcao + " - Direito de Opcao: exerci o direito de opcao "
				lcMsg = lcMsg + "??" + "?10"
				lcMsg = lcMsg + "??" + "para medicamento com preco superior ao 5 mais barato."
			ENDIF
			
			IF !EMPTY(ALLTRIM(nLinhaDirOpcaoC1))
				lcMsg = lcMsg + "??" + "?10" + "?10"
				lcMsg = lcMsg + "??" + nLinhaDirOpcaoC1 + " - Direito de Opcao: exerci o direito de opcao "
				lcMsg = lcMsg + "??" + "?10"
				lcMsg = lcMsg + "??" + "por medicamento mais barato que o prescrito para "
				lcMsg = lcMsg + "??" + "?10"
				lcMsg = lcMsg + "??" + "continuidade terapeutica de tratamento superior a "
				lcMsg = lcMsg + "??" + "?10"
				lcMsg = lcMsg + "??" + "28 dias."
			ENDIF

			IF !EMPTY(ALLTRIM(nLinhaDirOpcaoC2))
				lcMsg = lcMsg + "??" + "?10" + "?10"
				lcMsg = lcMsg + "??" + nLinhaDirOpcaoC2 + " - Direito de Opcao: nao exerci direito de opcao."
			ENDIF
			
			***************************
		ENDIF
		lcMsg = lcMsg + "??" + "?10" + "?10"
		lcMsg = lcMsg + "??" + "O Utente ________________________________________"
		lcMsg = lcMsg + "??" + "?10" + "?10"
		**lcMsg = lcMsg + "??" + "?10" + "?10"

		IF uf_gerais_getdate(uCrsFt.fdata,"SQL") <= "20130331"
			lcMsg = lcMsg + "??" + "?10" + "?10"
			lcMsg = lcMsg + "??" + "Direito de opcao ________________________________"
			lcMsg = lcMsg + "??" + "?10" + "?10"
			**lcMsg = lcMsg + "??" + "?10" + "?10"
		ENDIF
		
        IF lcVerso == 2
		    lcMsg = lcMsg + "??" + "?10"
		    lcMsg = lcMsg + "??" + "?27" + "?100" + "?7"
		    lcMsg = lcMsg + "??" + "?29" + "?86" + "?48"
        ENDIF

		** DataMatrix **
		LOCAL lcDM
		lcDM = ""

		SELECT uCrsE1
		GO TOP

		** vers�o
		lcDM = lcDM + '104'
		
		** c�digo farm�cia
		lcDM = lcDM + REPLICATE('0',6-LEN(ASTR(uCrsE1.u_infarmed))) + ASTR(uCrsE1.u_infarmed)
		
		** codigo entidade + data + operador + serie + lote + posicao lote + numero venda + numero receita 
		lcDM = lcDM + dm_codigoentidade + dm_data + dm_operador + dm_serie + dm_lote + dm_posicaolote + dm_nrvenda + dm_nrreceita 
		
		** campo2 + campo3 + campo4
		lcDM = lcDM	+ "000000000000" + "000000000000" + "00000000000000000000"

		** linhas
		SELECT uCrsDMl
		GO TOP
		SCAN
		

			** cnp + portaria + pvp + pref + comparticipacao + valor utente + direitoopcao
	*		lcDM = lcDM + uCrsDMl.cnp + uCrsDMl.portaria + uCrsDMl.pvp + uCrsDMl.pref + uCrsDMl.comparticipacao + uCrsDMl.valorutente + uCrsDMl.direitoopcao
		lcDM = lcDM + uCrsDMl.cnp + uCrsDMl.portaria + uCrsDMl.pvp + uCrsDMl.pref + uCrsDMl.comparticipacao + uCrsDMl.valorutente + uCrsDMl.p4mb + uCrsDMl.fee + uCrsDMl.direitoopcao 
		ENDSCAN
		fecha("uCrsDMl")

		** total utente liquido
		lcDM = lcDM + dm_totalutenteliquido
		****************

		** adicionar o datamatrix ao conte�do da receita
		** caracteres "##DM" identificam datamatrix
		lcDM = "??##DM" + lcDM
		lcMsg = STRTRAN(lcMsg, "%%1", lcDM)
			
		RETURN lcMsg

ENDFUNC 


FUNCTION uf_imprimirPOS_reImpAtend
   LPARAMETERS uv_prev

	 	

    **IF uf_gerais_getParameter_site('ADM0000000136', 'BOOL', mySite)
    IF uf_gerais_getUmValor("B_terminal", "usaDriverWin", "terminal = '" + ALLTRIM(myTerm) + "'")

            uv_idImp = uf_gerais_getUmValor("b_impressoes", "id", "tabela = 'FT' and nomeImpressao = 'Tal�o Atendimento'")
            IF !uf_imprimirgerais_createCursTalao(uv_idImp, UCRSPESQVENDAS.ftstamp, UCRSPESQVENDAS.ftstamp, UCRSPESQVENDAS.u_nratend, 'REIMPPOS', 1, .F.)
            	uf_perguntalt_chama("ERRO A GERAR CURSORES DO TAL�O." + chr(13) + "POR FAVOR CONTACTE O SUPORTE.", "OK", "", 48)
            	RETURN .F.
            ENDIF
            IF USED("FTPrint") AND RECCOUNT("FTPrint") <> 0

				SELECT DISTINCT utstamp FROM FTprint WITH (BUFFERING = .T.) ORDER BY utstamp INTO CURSOR uc_utentesTalao			

				SELECT uc_utentesTalao
				GO TOP

				SCAN

					SELECT FIprint
					SET FILTER TO uf_gerais_compStr(FIprint.utstamp, uc_utentesTalao.utstamp)

					SELECT FIprint
					GO BOTTOM 

					IF !uf_gerais_compStr(FIprint.ftstamp, FIprint.lastStamp)
						UPDATE FIprint SET FIprint.lastStamp = ALLTRIM(FIprint.ftStamp) WHERE uf_gerais_compStr(FIprint.utstamp, uc_utentesTalao.utstamp)
					ENDIF

					SELECT FIprint
					GO TOP

					SELECT FTprint
					SET FILTER TO uf_gerais_compStr(FTprint.utstamp, uc_utentesTalao.utstamp)

					SELECT FIprint
					GO TOP
					LOCATE FOR !EMPTY(FIprint.obsInt)

					IF FOUND() AND uf_gerais_getParameter_site('ADM0000000151', 'BOOL', mySite)

					IF !uv_prev

						uf_imprimirgerais_sendprinter_talao(uv_idImp, .T., .f.)

						SELECT FIprint
						GO TOP
						
						REPLACE fiprint.obsInt WITH "" FOR 1=1

						uf_imprimirgerais_sendprinter_talao(uv_idImp, .T., .f.)
					ELSE
						uf_imprimirgerais_sendprinter_talao(uv_idImp, .T., uv_prev)
					ENDIF

					ELSE

						REPLACE fiprint.obsInt WITH "" FOR 1=1

						uf_imprimirgerais_sendprinter_talao(uv_idImp, .T., uv_prev)
					ENDIF

					SELECT FTprint
					SET FILTER TO 

					SELECT FIprint
					SET FILTER TO

					SELECT uc_utentesTalao
				ENDSCAN
                
            ENDIF


            uf_gerais_actGrelha("","uc_vdRes", "SELECT ftstamp, u_nratend FROM ft(nolock) where u_nratend = '" + ALLTRIM(UCRSPESQVENDAS.u_nratend) + "' and EXISTS(select 1 from fi(nolock) where fi.ftstamp = ft.ftstamp and fi.design like '%Ad. Reserva%')")
            
            SELECT uc_vdRes
            GO TOP
            
            SCAN

	            uv_idImp = uf_gerais_getUmValor("b_impressoes", "id", "tabela = 'FT' and nomeImpressao = 'Tal�o Atendimento'")
    	        IF !uf_imprimirgerais_createCursTalao(uv_idImp, uc_vdRes.ftstamp, uc_vdRes.ftstamp, uc_vdRes.u_nratend, 'REIMPPOSRES', 1, .F.)
        	    	uf_perguntalt_chama("ERRO A GERAR CURSORES DO TAL�O." + chr(13) + "POR FAVOR CONTACTE O SUPORTE.", "OK", "", 48)
            		RETURN .F.
	            ENDIF
    	        IF USED("FTPrint") AND RECCOUNT("FTPrint") <> 0

                    SELECT FIprint
                    GO TOP
                    LOCATE FOR !EMPTY(FIprint.obsInt)

                    IF FOUND() AND uf_gerais_getParameter_site('ADM0000000151', 'BOOL', mySite)

                     IF !uv_prev

                        uf_imprimirgerais_sendprinter_talao(uv_idImp, .T., .f.)

                        SELECT FIprint
                        GO TOP
                        
                        REPLACE fiprint.obsInt WITH "" FOR 1=1

                        uf_imprimirgerais_sendprinter_talao(uv_idImp, .T., .f.)
                     ELSE
                        uf_imprimirgerais_sendprinter_talao(uv_idImp, .T., .T.)
                     ENDIF

                    ELSE

                        REPLACE fiprint.obsInt WITH "" FOR 1=1

                        uf_imprimirgerais_sendprinter_talao(uv_idImp, .T., uv_prev)
                    ENDIF

	            ENDIF
	            
	            SELECT uc_vdRes
			ENDSCAN
            
            uf_gerais_actGrelha("","uc_vdSusp", "SELECT ftstamp, u_nratend FROM ft(nolock) where u_nratend = '" + ALLTRIM(UCRSPESQVENDAS.u_nratend) + "' and cobrado = 1")
            
            SELECT uc_vdSusp
            GO TOP
            
            SCAN

	            uv_idImp = uf_gerais_getUmValor("b_impressoes", "id", "tabela = 'FT' and nomeImpressao = 'Tal�o Atendimento'")
    	        IF !uf_imprimirgerais_createCursTalao(uv_idImp, uc_vdSusp.ftstamp, uc_vdSusp.ftstamp, uc_vdSusp.u_nratend, 'REIMPPOSSUSP', 1, .T.)
        	    	uf_perguntalt_chama("ERRO A GERAR CURSORES DO TAL�O." + chr(13) + "POR FAVOR CONTACTE O SUPORTE.", "OK", "", 48)
            		RETURN .F.
	            ENDIF
    	        IF USED("FTPrint") AND RECCOUNT("FTPrint") <> 0

                    SELECT FIprint
                    GO TOP
                    LOCATE FOR !EMPTY(FIprint.obsInt)

                    IF FOUND() AND uf_gerais_getParameter_site('ADM0000000151', 'BOOL', mySite)

                     IF !uv_prev

                        uf_imprimirgerais_sendprinter_talao(uv_idImp, .T., .f.)

                        SELECT FIprint
                        GO TOP
                        
                        REPLACE fiprint.obsInt WITH "" FOR 1=1

                        uf_imprimirgerais_sendprinter_talao(uv_idImp, .T., .f.)
                     ELSE
                        uf_imprimirgerais_sendprinter_talao(uv_idImp, .T., .T.)
                     ENDIF

                    ELSE

                        REPLACE fiprint.obsInt WITH "" FOR 1=1

                        uf_imprimirgerais_sendprinter_talao(uv_idImp, .T., uv_prev)
                    ENDIF

	            ENDIF
	            
	            SELECT uc_vdSusp
			ENDSCAN

            SELECT ucrspesqvendas
            uv_atend = ucrspesqvendas.u_nratend

            SELECT * FROM ucrspesqvendas WITH (BUFFERING = .T.) WHERE ucrspesqvendas.u_nratend = uv_atend and ucrspesqvendas.u_tipodoc == 0 INTO CURSOR uc_tmpRec

            SELECT uc_tmpRec
            GO TOP
            SCAN

                uf_PAGAMENTO_impTalaoRecPos(901, uc_tmpRec.ftstamp, uv_prev)

            ENDSCAN

            FECHA("uc_tmpRec")

			TEXT TO msel TEXTMERGE NOSHOW
				exec up_getTotAtendimento '<<ALLTRIM(ucrspesqvendas.u_nrAtend)>>'
			ENDTEXT

			uf_gerais_actGrelha("","uc_totsAtendimento", msel)

			uv_totAtendimento = uc_totsAtendimento.totAtendimento

			FECHA("uc_totsAtendimento")

            IF uf_gerais_getParameter_site('ADM0000000154', 'BOOL', mySite) AND CTOD(ucrspesqvendas.fdata) = DATE() AND uv_totAtendimento >= uf_gerais_getParameter_site('ADM0000000154', 'NUM', mySite) 
                uf_talao_parque()
            ENDIF
    ELSE

        uf_imprimirPOS_ValImpAtendimento()

    ENDIF

ENDFUNC

FUNCTION uf_imprimirpos_impRecTalaoComum_talaoVerso
	LPARAMETERS lcStamp, lcVerso, uv_reImp, uv_aviso, uv_prev, lcReimprimir
	
	LOCAL lcMsg, lcOrdemImpressaoReceita, lcCompartV1, lcCompartV2
	lcMsg = ""

	PUBLIC upv_curVerso
	upv_curVerso = lcVerso

	IF USED("uCrsFt")
        fecha("uCrsFt")
    ENDIF 
    IF USED("uCrsFt2")
        fecha("uCrsFt2")
    ENDIF 
    IF USED("uCrsFi")
        fecha("uCrsFi")
    ENDIF 
    IF USED("uCrsFi2")
        fecha("uCrsFi2")
    ENDIF 

    uf_gerais_actGrelha("",[uCrsFt],[exec up_touch_ft ']+lcStamp+['])
    select uCrsFt
    uf_gerais_actGrelha("",[uCrsFt2],[exec up_touch_ft2 ']+lcStamp+['])
    select uCrsFt2
    uf_gerais_actGrelha("",[uCrsFi],[exec up_touch_fi ']+alltrim(lcStamp)+[', '', 0])
    select uCrsFi
	uf_gerais_actGrelha("",[uCrsFi2],[exec up_touch_fi2 ']+alltrim(lcStamp)+[', ''])
	select uCrsFi2

    SELECT uCrsFt2
    
    IF !lcReimprimir
		IF !uf_imprimirpos_validaSeImprimeVersoReceita(lcVerso)
			RETURN .F.
		ENDIF
	ENDIF
	
    IF lcVerso = 1

      select uCrsFt
      uf_gerais_actGrelha("","T","SELECT convert(varchar(3),loteId) as tlote FROM ctltrct (nolock) inner join tlote(nolock) on tlote.codigo=ctltrct.tlote  WHERE ctltrctstamp='" + ALLTRIM(uCrsFt.u_ltstamp) + "'")
      uf_gerais_actGrelha("","T2","SELECT convert(varchar(3),loteId) as tlote FROM ctltrct (nolock) inner join tlote(nolock) on tlote.codigo=ctltrct.tlote   WHERE ctltrctstamp='"+ALLTRIM(uCrsFt.u_ltstamp)+"'")
      uf_gerais_actGrelha("","L","SELECT convert(varchar(3),lote) as lote FROM ctltrct (nolock) WHERE ctltrctstamp='" + ALLTRIM(uCrsFt.u_ltstamp) + "'")
      uf_gerais_actGrelha("","R","SELECT convert(varchar(5),nreceita) as nreceita FROM ctltrct (nolock) WHERE ctltrctstamp='" + ALLTRIM(uCrsFt.u_ltstamp) + "'")
      uf_gerais_actGrelha("","S","SELECT convert(varchar(3),slote) as slote FROM ctltrct (nolock) WHERE ctltrctstamp='" + ALLTRIM(uCrsFt.u_ltstamp) + "'")

    ELSE

      select uCrsFt
      uf_gerais_actGrelha("","T","SELECT convert(varchar(3),loteId) as tlote FROM ctltrct (nolock) inner join tlote(nolock) on tlote.codigo=ctltrct.tlote  WHERE ctltrctstamp='" + ALLTRIM(uCrsFt.u_ltstamp2) + "'")
      uf_gerais_actGrelha("","T2","SELECT convert(varchar(3),loteId) as tlote FROM ctltrct (nolock) inner join tlote(nolock) on tlote.codigo=ctltrct.tlote   WHERE ctltrctstamp='"+ALLTRIM(uCrsFt.u_ltstamp2)+"'")
      uf_gerais_actGrelha("","L","SELECT convert(varchar(3),lote) as lote FROM ctltrct (nolock) WHERE ctltrctstamp='" + ALLTRIM(uCrsFt.u_ltstamp2) + "'")
      uf_gerais_actGrelha("","R","SELECT convert(varchar(5),nreceita) as nreceita FROM ctltrct (nolock) WHERE ctltrctstamp='" + ALLTRIM(uCrsFt.u_ltstamp2) + "'")
      uf_gerais_actGrelha("","S","SELECT convert(varchar(3),slote) as slote FROM ctltrct (nolock) WHERE ctltrctstamp='" + ALLTRIM(uCrsFt.u_ltstamp2) + "'")

    ENDIF
    
    IF USED("uCrsCabVendasprint")
        LOCAL lcPos 
        Select uCrsCabVendasprint
        lcPos = Recno()
        
        LOCAL lcnrrecprint
        lcnrrecprint = ALLTRIM(uCrsFt2.u_receita)
        SELECT uCrsFt
        IF ALLTRIM(uCrsFt.nmdoc) == 'Inser��o de Receita'
            SELECT uCrsCabVendasprint
            GO TOP 
            SCAN 
                IF AT(ALLTRIM(lcnrrecprint),ALLTRIM(uCrsCabVendasprint.design))>0
                    SELECT uCrsCabVendasprint
                    DELETE 
                ENDIF 
            ENDSCAN 
            
            SELECT uCrsCabVendasprint
            TRY 
                GO lcPos 
            CATCH
                GO TOP  
            ENDTRY
        ENDIF 
    
    ENDIF 
    
    IF USED("uCrsImpTalaoPos")
        SELECT * FROM uCrsImpTalaoPos WHERE !EMPTY(uCrsImpTalaoPos.nrreceita) INTO CURSOR uCrsImpTalaoPosAux READWRITE 
    ENDIF 
    
    IF USED("uCrsImpTalaoPos ")
        fecha("uCrsImpTalaoPosAux")
    ENDIF 
    
    ** cursores e variaveis para o data-matrix (cabe�alho e linhas)
    LOCAL dm_codigofarmacia, dm_codigoentidade, dm_data, dm_operador, dm_serie, dm_lote, dm_posicaolote, dm_nrvenda, dm_nrreceita, dm_campo2, dm_campo3, dm_campo4, dm_totalutenteliquido, lcFee
    STORE "" TO dm_codigofarmacia, dm_codigoentidade, dm_data, dm_operador, dm_serie, dm_lote, dm_posicaolote, dm_nrvenda, dm_nrreceita, dm_campo2, dm_campo3, dm_campo4, dm_totalutenteliquido
    STORE 0.00 TO lcFee
    
    IF YEAR(uCrsFt.fdata) >= 2017
        CREATE CURSOR uCrsDMl(cnp c(7), portaria c(3), pvp c(6), pref c(6), comparticipacao c(6), valorutente c(6), p4mb c(6),fee c(6), direitoopcao c(1))
    ELSE
        CREATE CURSOR uCrsDMl(cnp c(7), portaria c(3), pvp c(6), pref c(6), comparticipacao c(6), valorutente c(6), direitoopcao c(1))
    ENDIF
    
    public lcdocdescr
    && impress�o da data da dispensa no verso de receita - se data da dispensa (bidata) != vazio imprime data dispensa sen�o imprime data fatura
    && no caso das re inser��es imprime data da receita
    IF uCrsFt.u_tipodoc == 3 && se for uma inser��o de receita imprime cdata
        IF uf_gerais_getDate(uCrsFt.BIDATA,"DATA") == '01.01.1900'		
            dm_data = "00000000" && datamatrix data
        ELSE		
            dm_data = REPLICATE('0',8-LEN(uf_gerais_getDate(uCrsFt.BIDATA,"SQL"))) + uf_gerais_getDate(uCrsFt.CDATA,"SQL") && datamatrix data						
        ENDIF
    ELSE
        && IF uf_gerais_getDate(uCrsFt.CDATA,"DATA") == '01.01.1900'
        IF uf_gerais_getDate(uCrsFt.BIDATA,"DATA") == '01.01.1900'		
            dm_data = REPLICATE('0',8-LEN(uf_gerais_getDate(uCrsFt.FDATA,"SQL"))) + uf_gerais_getDate(uCrsFt.FDATA,"SQL") && datamatrix data			
        ELSE		
            dm_data = REPLICATE('0',8-LEN(uf_gerais_getDate(uCrsFt.BIDATA,"SQL"))) + uf_gerais_getDate(uCrsFt.BIDATA,"SQL") && data
        ENDIF
    ENDIF
    
    lcdocdescr = uf_gerais_getDate(uCrsFt.BIDATA,"DATA") + ' ' + ALLTRIM(uCrsFt.usrhora) + " Vnd - " + ALLTRIM(Str(uCrsFt.NDOC)) + "/" + astr(uCrsFt.FNO) + "(" + astr(uCrsFt.VENDEDOR) + ")"
    
    dm_operador  = REPLICATE('0',10-LEN(ALLTRIM(LEFT(uCrsFt.vendnm,10)))) + ALLTRIM(LEFT(uCrsFt.vendnm,10)) && datamatrix vendedor
    dm_nrvenda   = REPLICATE('0',7-LEN(astr(uCrsFt.FNO))) + astr(uCrsFt.FNO) && datamatrix n�mero venda
    dm_nrreceita = REPLICATE('0',20-LEN(ALLTRIM(uCrsFt2.u_receita))) + ALLTRIM(uCrsFt2.u_receita) && datamatrix receita
    
    public lcCodentPrint
    DO CASE
        CASE lcVerso = 1
            lcCodentPrint = uCrsFt2.U_CODIGO + " " + SUBSTR(uCrsFt2.U_DESIGN,1,30)
            ** datamatrix
            dm_codigoentidade = REPLICATE('0',3-LEN(ALLTRIM(uCrsFt2.U_CODIGO))) + ALLTRIM(uCrsFt2.U_CODIGO) && C�digo entidade (??? c�digo plano)
        CASE lcVerso = 2
            lcCodentPrint = uCrsFt2.U_CODIGO2 + " " + SUBSTR(uCrsFt2.U_DESIGN2,1,30)
            ** datamatrix
            dm_codigoentidade = REPLICATE('0',3-LEN(ALLTRIM(uCrsFt2.U_CODIGO2))) + ALLTRIM(uCrsFt2.U_CODIGO2) && C�digo entidade 2 (??? c�digo plano)
    ENDCASE
    
    dm_lote    	   = REPLICATE('0',4-LEN(ASTR(L.LOTE))) + ASTR(L.LOTE) && datamatrix lote
    dm_posicaolote = REPLICATE('0',5-LEN(ASTR(R.NRECEITA))) + ASTR(R.NRECEITA) && damatrix posicao lote
    dm_serie       = REPLICATE('0',3-LEN(ASTR(S.SLOTE))) + ASTR(S.SLOTE) && datamatrix serie
                    
    ** Impress�o Nr de benefici�rio - s� imprime o n� de benefici�rio para entidades complementares
    public lcnbenef
    store '' to lcnbenef
    SELECT uCrsFt2
    LOCATE FOR ALLTRIM(uCrsFt2.ft2stamp) == ALLTRIM(lcStamp)
    DO CASE
        CASE lcVerso = 1
            IF !EMPTY(ALLTRIM(uCrsFt2.U_NBENEF2))
                lcnbenef = "Nr.Benef.: " + uCrsFt2.U_NBENEF2
            ENDIF
            IF EMPTY(ALLTRIM(uCrsFt2.U_NBENEF2)) AND EMPTY(ALLTRIM(uCrsFt2.U_NBENEF))
                lcnbenef = "Nr.Benef.: " + uCrsFt2.U_NBENEF
            ENDIF
        OTHERWISE
    ENDCASE
    
    ** guardar tabela diplomas para mapear com id para o c�digo matrix
    local lcDiploma_id
    IF !USED("uCrsDplms")
        uf_gerais_actGrelha("","uCrsDplms","select u_design, diploma_id from dplms (nolock) where u_design!=''")
    ENDIF
    
    
    &&:TODO
    **guarda a ordem de impress�o de receita
    ordemImpressaoReceita=uCrsE1.ordemimpressaoreceita
    
    IF(lcOrdemImpressaoReceita==.f.)
        lcCompartV1 = uCrsFt2.u_abrev
        lcCompartV2 = uCrsFt2.u_abrev2
    ELSE
        lcCompartV1 = uCrsFt2.u_abrev2
        lcCompartV2 = uCrsFt2.u_abrev
    ENDIF
        
    ** IMPRESS�O DAS LINHAS **
    LOCAL lcContadorLinhas
    lcContadorLinhas = 1
    
    SELECT uCrsFi 
    DO CASE 
        CASE lcVerso = 1
            select recno() as lineno, * from uCrsFi where uCrsFi.U_ETTENT1 > 0 AND ALLTRIM(uCrsFi.ftstamp) == ALLTRIM(uCrsFt.ftstamp) into cursor uCrsl1 readwrite
            SCAN FOR uCrsFi.U_ETTENT1 > 0 AND ALLTRIM(uCrsFi.ftstamp) == ALLTRIM(uCrsFt.ftstamp)
                    FOR i = 1 TO qtt
                        
                        && s� adiciona caso seja inferior a 10 linhas sen�o d� erro na impress�o do verso em tal�o 
                        && O codigo dm nem sequer � necess�rio nestes casos (impress�o de verso em tal�o com receitas de mais de 10 medicamentos) -- LL 28/07/2016
                        IF lcContadorLinhas < 10
                            ** datamatrix
                            SELECT uCrsDMl
                            APPEND BLANK
                            REPLACE uCrsDMl.cnp WITH REPLICATE('0', 7 - LEN(alltrim(LEFT(uCrsFi.ref,7)))) + ALLTRIM(LEFT(uCrsFi.ref,7)) && datamatrix cnp
                            
                            ** datamatrix portaria
                            lcDiploma_id = 0
                            SELECT uCrsDplms
                            GO TOP
                            SCAN
                                If ALLTRIM(uCrsDplms.u_design) == ALLTRIM(uCrsFi.u_diploma)
                                    lcDiploma_id = uCrsDplms.diploma_id
                                ENDIF
                            ENDSCAN
                            
                            SELECT uCrsDMl
                            replace uCrsDMl.portaria		 WITH REPLICATE('0', 3 - LEN(astr(lcDiploma_id))) + astr(lcDiploma_id) && datamatrix portaria
                            replace uCrsDMl.pvp				 WITH REPLICATE('0', 6 - LEN(LEFT(astr(ROUND(uCrsFi.u_epvp * 100, 0)),6))) + LEFT(astr(ROUND(uCrsFi.u_epvp * 100,0)),6) && datamatrix pvp
                            replace uCrsDMl.pref 			 WITH REPLICATE('0', 6 - LEN(LEFT(astr(ROUND(uCrsFi.u_epref * 100,0)),6))) + LEFT(astr(ROUND(uCrsFi.u_epref * 100,0)),6) && datamatrix pref
                            replace uCrsDMl.comparticipacao  WITH REPLICATE('0', 6 - LEN(LEFT(astr(ROUND(round(uCrsFi.u_ettent1/uCrsFi.qtt,2) * 100,0)),6))) + LEFT(astr(ROUND(round(uCrsFi.u_ettent1/uCrsFi.qtt,2) * 100,0)),6) && datamatrix comparticipacao
                            replace uCrsDMl.valorutente      WITH REPLICATE('0', 6 - LEN(LEFT(astr(ROUND(ROUND(uCrsFi.u_epvp-uCrsFi.u_ettent1/uCrsFi.qtt,2) * 100,0)),6))) + LEFT(astr(ROUND(ROUND(uCrsFi.u_epvp-uCrsFi.u_ettent1/uCrsFi.qtt,2) * 100,0)),6) && datamatrix valorutente

                            IF YEAR(uCrsFt.fdata) >= 2017  AND ALLTRIM(lcCompartV1) =="SNS"									
                                replace uCrsDMl.p4mb WITH REPLICATE('0', 6 - LEN(LEFT(astr(ROUND(uCrsFi.pvptop4 * 100, 0)),6))) + LEFT(astr(ROUND(uCrsFi.pvptop4 * 100,0)),6) && datamatrix pvptop4
                                && datamatrix free								
                                IF(MONTH(uCrsFt.fdata) == 1 and YEAR(uCrsFt.fdata) == 2017 )
                                    IF uCrsFi.u_generico
                                        replace uCrsDMl.fee WITH IIF(uCrsFi.pvptop4 > 0 AND uCrsFi.u_epvp <= uCrsFi.pvptop4, '000035', '000000')
                                    ELSE
                                        replace uCrsDMl.fee WITH IIF(uCrsFi.pvptop4 > 0 AND uCrsFi.pic <= uCrsFi.pvptop4, '000035', '000000')
                                    ENDIF
                                ELSE
                                    replace uCrsDMl.fee WITH REPLICATE('0', 6 -  LEN(LEFT(astr(ROUND(uCrsFi.pvp4_fee* 100,0)),6)))  + LEFT(astr(ROUND(uCrsFi.pvp4_fee* 100,0)),6)&& pvptop4_fee
                                ENDIF
                                
                            ENDIF
                            
                            IF EMPTY(ALLTRIM(uCrsFi.lobs3)) && se n�o for utilizada excep��o
                                replace uCrsDMl.direitoopcao WITH uCrsFi.opcao && datamatrix direito opcao	
                            ELSE
                                IF ALLTRIM(uCrsFi.lobs3) == 'C1'
                                    replace uCrsDMl.direitoopcao WITH "S" && datamatrix direito opcao Excecao C1
                                ELSE
                                    replace uCrsDMl.direitoopcao WITH "N" && datamatrix direito opcao Restantes Excecoes
                                ENDIF
                            ENDIF
                        ENDIF
                    ENDFOR
                    lcContadorLinhas = lcContadorLinhas + 1
                
                SELECT uCrsFi
            ENDSCAN

        CASE lcVerso = 2
            select recno() as lineno,* from uCrsFi where uCrsFi.U_ETTENT2 > 0 AND ALLTRIM(uCrsFi.ftstamp) == ALLTRIM(uCrsFt.ftstamp) into cursor uCrsl1 readwrite
            SCAN FOR uCrsFi.U_ETTENT2 > 0 AND ALLTRIM(uCrsFi.ftstamp) == ALLTRIM(uCrsFt.ftstamp)
                
                    FOR i = 1 to qtt	
    
                        && s� adiciona caso seja inferior a 10 linhas sen�o d� erro na impress�o do verso em tal�o - o codigo dm nem sequer � necess�rio nestes casos (impress�o de verso em tal�o com receitas de mais de 10 medicamentos) -- LL 28/07/2016
                        IF lcContadorLinhas < 10
                            ** datamatrix
                            SELECT uCrsDMl
                            APPEND BLANK
                            replace uCrsDMl.cnp WITH REPLICATE('0',7-LEN(alltrim(uCrsFi.ref))) + ALLTRIM(uCrsFi.ref) && datamatrix cnp
                            
                            ** datamatrix portaria
                            lcDiploma_id = 0
                            SELECT uCrsDplms
                            GO TOP
                            SCAN
                                IF ALLTRIM(uCrsDplms.u_design) == ALLTRIM(uCrsFi.u_diploma)
                                    lcDiploma_id = uCrsDplms.diploma_id
                                ENDIF
                            ENDSCAN					
                            
                            SELECT uCrsDMl
                            replace uCrsDMl.portaria WITH REPLICATE('0', 3 - LEN(astr(lcDiploma_id))) + astr(lcDiploma_id) && datamatrix portaria
                            replace uCrsDMl.pvp WITH REPLICATE('0', 6 - LEN(LEFT(astr(ROUND(uCrsFi.u_epvp * 100, 0)),6))) + LEFT(astr(ROUND(uCrsFi.u_epvp * 100,0)),6) && datamatrix pvp
                            replace uCrsDMl.pref WITH REPLICATE('0', 6 - LEN(LEFT(astr(ROUND(uCrsFi.u_epref * 100,0)),6))) + LEFT(astr(ROUND(uCrsFi.u_epref * 100,0)),6) && datamatrix pref
                            replace uCrsDMl.comparticipacao WITH REPLICATE('0', 6 - LEN(LEFT(astr(ROUND(round(uCrsFi.u_ettent2/uCrsFi.qtt,2) * 100,0)),6))) + LEFT(astr(ROUND(round(uCrsFi.u_ettent2/uCrsFi.qtt,2) * 100,0)),6) && datamatrix comparticipacao
                            replace uCrsDMl.valorutente WITH REPLICATE('0', 6 - LEN(LEFT(astr(ROUND(ROUND(uCrsFi.u_epvp-uCrsFi.u_ettent2/uCrsFi.qtt,2) * 100,0)),6))) + LEFT(astr(ROUND(ROUND(uCrsFi.u_epvp-uCrsFi.u_ettent2/uCrsFi.qtt,2) * 100,0)),6) && datamatrix valorutente
                            
                            IF YEAR(uCrsFt.fdata) >= 2017  AND ALLTRIM(lcCompartV2) =="SNS"
                                
                                replace uCrsDMl.p4mb WITH REPLICATE('0', 6 - LEN(LEFT(astr(ROUND(uCrsFi.pvptop4 * 100, 0)),6))) + LEFT(astr(ROUND(uCrsFi.pvptop4 * 100,0)),6) && datamatrix pvp
                                && datamatrix free
                                IF(MONTH(uCrsFt.fdata) == 1 and year(uCrsFt.fdata) == 2017  )
                                    IF uCrsFi.u_generico
                                        replace uCrsDMl.fee WITH IIF(uCrsFi.pvptop4 > 0 AND uCrsFi.u_epvp <= uCrsFi.pvptop4, '000035', '000000')
                                    ELSE
                                        replace uCrsDMl.fee WITH IIF(uCrsFi.pvptop4 > 0 AND uCrsFi.pic <= uCrsFi.pvptop4, '000035', '000000')
                                    ENDIF
                                    
                                ELSE
                                
                                    replace uCrsDMl.fee WITH REPLICATE('0', 6 -  LEN(LEFT(astr(ROUND(uCrsFi.pvp4_fee* 100,0)),6)))  + LEFT(astr(ROUND(uCrsFi.pvp4_fee* 100,0)),6)&& pvptop4_fee
    
                                ENDIF
                                
                                
                            ENDIF
                            IF EMPTY(ALLTRIM(uCrsFi.lobs3)) && se n�o for utilizada excep��o
                                replace uCrsDMl.direitoopcao WITH uCrsFi.opcao && datamatrix direito opcao	
                            ELSE
                                IF ALLTRIM(uCrsFi.lobs3) == 'C1'
                                    replace uCrsDMl.direitoopcao WITH "S" && datamatrix direito opcao Excecao C1
                                ELSE 
                                    replace uCrsDMl.direitoopcao WITH "N" && datamatrix direito opcao Restantes Excecoes
                                ENDIF
                            ENDIF
                        ENDIF
                    ENDFOR
                    
                    lcContadorLinhas = lcContadorLinhas + 1

                SELECT uCrsFi
            ENDSCAN
            
    ENDCASE
		
		&& Imprime linhas que n�o est�o preenchidas
    SELECT uCrsDMl
    COUNT TO lcPosicoesDML 
    IF lcPosicoesDML < 4
        FOR i=1 TO 4-lcPosicoesDML 
            
            ** datamatrix
            SELECT uCrsDMl
            APPEND BLANK
            replace cnp		 			WITH REPLICATE('0',7) && datamatrix cnp
            replace portaria 			WITH REPLICATE('0',3) && datamatrix portaria
            replace pvp 				WITH REPLICATE('0', 6) && datamatrix pvp
            replace pref				WITH REPLICATE('0', 6) && datamatrix pref
            replace comparticipacao 	WITH REPLICATE('0', 6) && datamatrix comparticipacao
            replace valorutente 		WITH REPLICATE('0', 6) && datamatrix valorutente	
            IF YEAR(uCrsFt.fdata) >= 2017
                replace uCrsDMl.p4mb	WITH REPLICATE('0', 6) && datamatrix pvptop4
                replace uCrsDMl.fee		WITH REPLICATE('0', 6) && datamatrix fee
            ENDIF
            replace direitoopcao 		WITH '0'
        ENDFOR  
    ENDIF 
    
    SELECT uCrsFi
    
    public totalPvp, totalQtt, totalComp, totalUtente, lcTotalFee
    store 0 to totalPvp, totalQtt, totalComp, totalUtente, lcTotalFee
    
    ** PREPARAR OS TOTAIS
    DO CASE
        CASE lcVerso = 1
            calculate sum(uCrsFi.u_epvp*uCrsFi.qtt) for uCrsFi.u_ettent1>0 AND ALLTRIM(uCrsFi.ftstamp) = ALLTRIM(uCrsFt.ftstamp) to totalPvp
            calculate sum(uCrsFi.qtt) for uCrsFi.u_ettent1>0 AND ALLTRIM(uCrsFi.ftstamp) = ALLTRIM(uCrsFt.ftstamp) to totalQtt
            calculate sum(uCrsFi.u_ettent1) for uCrsFi.u_ettent1>0 AND ALLTRIM(uCrsFi.ftstamp) = ALLTRIM(uCrsFt.ftstamp) to totalComp
            calculate sum(uCrsFi.u_epvp*uCrsFi.qtt-uCrsFi.u_ettent1) FOR uCrsFi.u_ettent1>0 AND ALLTRIM(uCrsFi.ftstamp) = ALLTRIM(uCrsFt.ftstamp) to totalUtente
            
            IF YEAR(uCrsFt.fdata) >= 2017  AND ALLTRIM(lcCompartV1)=="SNS"
                IF MONTH(uCrsFt.fdata)==1 AND year(uCrsFt.fdata) == 2017
                    calculate sum(0.35*uCrsfi.qtt) FOR uCrsFi.pvpTop4 > 0 AND ((uCrsFi.u_generico=.t. AND uCrsFi.u_epvp <= uCrsFi.pvptop4) OR (uCrsFi.u_generico=.f. AND uCrsFi.pic <= uCrsFi.pvptop4)) to lcTotalFee
                ELSE
                    calculate sum(uCrsFi.pvp4_fee) FOR uCrsFi.u_ettent1>0 AND ALLTRIM(uCrsFi.ftstamp) = ALLTRIM(uCrsFt.ftstamp) to lcTotalFee
                ENDIF
            ENDIF
            
            
        CASE lcVerso = 2
            calculate sum(uCrsFi.u_epvp*uCrsFi.qtt) for uCrsFi.u_ettent2>0 AND ALLTRIM(uCrsFi.ftstamp) = ALLTRIM(uCrsFt.ftstamp) to totalPvp
            calculate sum(uCrsFi.qtt) for uCrsFi.u_ettent2>0 AND ALLTRIM(uCrsFi.ftstamp) = ALLTRIM(uCrsFt.ftstamp) to totalQtt
            calculate sum(uCrsFi.u_ettent2) for uCrsFi.u_ettent2>0 AND ALLTRIM(uCrsFi.ftstamp) = ALLTRIM(uCrsFt.ftstamp) to totalComp
            calculate sum(uCrsFi.u_epvp*uCrsFi.qtt-uCrsFi.u_ettent2) FOR uCrsFi.u_ettent2>0 AND ALLTRIM(uCrsFi.ftstamp) = ALLTRIM(uCrsFt.ftstamp) to totalUtente
            IF YEAR(uCrsFt.fdata) >= 2017  AND ALLTRIM(lcCompartV2)=="SNS"
                IF MONTH(uCrsFt.fdata)==1 AND year(uCrsFt.fdata) == 2017 
                    calculate sum(0.35*uCrsfi.qtt) FOR uCrsFi.pvpTop4 > 0 AND ((uCrsFi.u_generico=.t. AND uCrsFi.u_epvp <= uCrsFi.pvptop4) OR (uCrsFi.u_generico=.f. AND uCrsFi.pic <= uCrsFi.pvptop4)) to lcTotalFee
                ELSE
            
                    calculate sum(uCrsFi.pvp4_fee) FOR uCrsFi.u_ettent2>0 AND ALLTRIM(uCrsFi.ftstamp) = ALLTRIM(uCrsFt.ftstamp) to lcTotalFee	
                ENDIF
            ENDIF

    ENDCASE

    IF VARTYPE(UsaManCompart)!="U"
        IF !UsaManCompart 
            dm_totalutenteliquido = REPLICATE('0',6-LEN(astr(round(totalUtente,2) * 100))) + astr(round(totalUtente,2) * 100) && datamatrix total utente iliquido
        ELSE
            dm_totalutenteliquido = ''
        ENDIF 
    ELSE
        dm_totalutenteliquido = REPLICATE('0',6-LEN(astr(round(totalUtente,2) * 100))) + astr(round(totalUtente,2) * 100) && datamatrix total utente iliquido
    ENDIF 

    if uf_gerais_getdate(uCrsFt.fdata,"SQL") > "20130331"
    
        ** DIREITO DE OP��O **
        public exerciDireitoOpcao, lcLinhaOpcao, lcContLinha
        STORE .f. TO exerciDireitoOpcao
        STORE '' TO lcLinhaOpcao
        *** Excep��o ***
        public nLinhaDirOpcaoC1, nLinhaDirOpcaoC2
        STORE '' TO nLinhaDirOpcaoC1, nLinhaDirOpcaoC2
        
        STORE 1 TO lcContLinha

        && verificar se tem PVP maior que o PVP5 (pvpmaxre)
        SELECT uCrsFi
        GO TOP
        DO CASE

            CASE lcVerso == 1
                SELECT uCrsFi
                SCAN FOR uCrsFi.U_ETTENT1 > 0 AND ALLTRIM(uCrsFi.ftstamp) == ALLTRIM(uCrsFt.ftstamp)
                    **FOR i=1 TO uCrsFi.qtt Alterado a 24/02/2015 devido aos agrupamentos de c�digos de barras nos versos de receita
                    
                        ** Op��o
                        ** Exerci direito de opcao
                        IF uCrsFi.opcao == 'S' AND EMPTY(ALLTRIM(uCrsFi.lobs3))
                            exerciDireitoOpcao = .t.
                            IF EMPTY(lcLinhaOpcao)
                                lcLinhaOpcao = astr(lcContLinha)
                            ELSE
                                lcLinhaOpcao = lcLinhaOpcao + ", " + astr(lcContLinha)
                            ENDIF
                        ENDIF
                        
                        ** N�o exerci direito de op��o
                        IF uCrsFi.opcao == 'N' AND EMPTY(ALLTRIM(uCrsFi.lobs3))
                            IF EMPTY(nLinhaDirOpcaoC2)
                                nLinhaDirOpcaoC2 = astr(lcContLinha)
                            ELSE
                                nLinhaDirOpcaoC2 = nLinhaDirOpcaoC2 + "," + astr(lcCOntLinha)
                            ENDIF
                        ENDIF
                                            
                        ** Excep��o
                        IF !EMPTY(ALLTRIM(uCrsFi.lobs3))
                            DO CASE
                            
                                ** exerci direito de opcao com excep��o especial
                                CASE ALLTRIM(ucrsFi.lobs3) == "C1"
                                    IF EMPTY(nLinhaDirOpcaoC1)
                                        nLinhaDirOpcaoC1 = astr(lcContLinha)
                                    ELSE
                                        nLinhaDirOpcaoC1 = nLinhaDirOpcaoC1 + "," + astr(lcCOntLinha)
                                    ENDIF
                                
                                ** n�o exerci direito de opcao
                                CASE ALLTRIM(ucrsFi.lobs3) == "C2" OR ALLTRIM(ucrsFi.lobs3) == "A" OR ALLTRIM(ucrsFi.lobs3) == "B"
                                    IF EMPTY(nLinhaDirOpcaoC2)
                                        nLinhaDirOpcaoC2 = astr(lcContLinha)
                                    ELSE
                                        nLinhaDirOpcaoC2 = nLinhaDirOpcaoC2 + "," + astr(lcCOntLinha)
                                    ENDIF
                            ENDCASE
                        ENDIF 
                    
                            lcContLinha = lcContLinha + 1
                    **ENDFOR
                ENDSCAN

            CASE lcVerso == 2

                SELECT uCrsFi
                SCAN FOR uCrsFi.U_ETTENT2 > 0 AND ALLTRIM(uCrsFi.ftstamp) == ALLTRIM(uCrsFt.ftstamp)
                    **FOR i=1 TO uCrsFi.qtt
                        ** Op��o
                        ** Exerci direito de opcao
                        IF uCrsFi.opcao == 'S' AND EMPTY(ALLTRIM(uCrsFi.lobs3))
                            exerciDireitoOpcao = .t.
                            IF EMPTY(lcLinhaOpcao)
                                lcLinhaOpcao = astr(lcContLinha)
                            ELSE
                                lcLinhaOpcao = lcLinhaOpcao + ", " + astr(lcContLinha)
                            ENDIF
                        ENDIF
                        
                        ** N�o exerci direito de op��o
                        IF uCrsFi.opcao == 'N' AND EMPTY(ALLTRIM(uCrsFi.lobs3))
                            IF EMPTY(nLinhaDirOpcaoC2)
                                nLinhaDirOpcaoC2 = astr(lcContLinha)
                            ELSE
                                nLinhaDirOpcaoC2 = nLinhaDirOpcaoC2 + "," + astr(lcCOntLinha)
                            ENDIF
                        ENDIF
                                                
                        ** Excep��o
                        IF !EMPTY(ALLTRIM(uCrsFi.lobs3))
                            DO CASE
                            
                                ** exerci direito de opcao com excep��o especial
                                CASE ALLTRIM(ucrsFi.lobs3) == "C1"
                                    IF EMPTY(nLinhaDirOpcaoC1)
                                        nLinhaDirOpcaoC1 = astr(lcContLinha)
                                    ELSE
                                        nLinhaDirOpcaoC1 = nLinhaDirOpcaoC1 + "," + astr(lcCOntLinha)
                                    ENDIF
                                
                                ** n�o exerci direito de opcao
                                CASE ALLTRIM(ucrsFi.lobs3) == "C2" OR ALLTRIM(ucrsFi.lobs3) == "A" OR ALLTRIM(ucrsFi.lobs3) == "B"
                                    IF EMPTY(nLinhaDirOpcaoC2)
                                        nLinhaDirOpcaoC2 = astr(lcContLinha)
                                    ELSE
                                        nLinhaDirOpcaoC2 = nLinhaDirOpcaoC2 + "," + astr(lcCOntLinha)
                                    ENDIF
                            ENDCASE
                        ENDIF 
                        
                        lcContLinha = lcContLinha + 1
                    **ENDFOR
                ENDSCAN
        ENDCASE

        IF !EMPTY(lcLinhaOpcao)
            lcLinhaOpcao = lcLinhaOpcao + ')'
        ENDIF
        IF !EMPTY(nLinhaDirOpcaoC1)
            nLinhaDirOpcaoC1  = nLinhaDirOpcaoC1 + ')'
        ENDIF
        IF !EMPTY(nLinhaDirOpcaoC2)
            nLinhaDirOpcaoC2  = nLinhaDirOpcaoC2 + ')'
        ENDIF
        
        ***************************
    ENDIF

		** DataMatrix **
    LOCAL lcDM
    lcDM = ""

    SELECT uCrsE1
    GO TOP

    ** vers�o
    lcDM = lcDM + '104'
    
    ** c�digo farm�cia
    lcDM = lcDM + REPLICATE('0',6-LEN(ASTR(uCrsE1.u_infarmed))) + ASTR(uCrsE1.u_infarmed)
    
    ** codigo entidade + data + operador + serie + lote + posicao lote + numero venda + numero receita 
    lcDM = lcDM + dm_codigoentidade + dm_data + dm_operador + dm_serie + dm_lote + dm_posicaolote + dm_nrvenda + dm_nrreceita 
    
    ** campo2 + campo3 + campo4
    lcDM = lcDM	+ "000000000000" + "000000000000" + "00000000000000000000"

    ** linhas
    SELECT uCrsDMl
    GO TOP
    SCAN
		

        ** cnp + portaria + pvp + pref + comparticipacao + valor utente + direitoopcao
*		lcDM = lcDM + uCrsDMl.cnp + uCrsDMl.portaria + uCrsDMl.pvp + uCrsDMl.pref + uCrsDMl.comparticipacao + uCrsDMl.valorutente + uCrsDMl.direitoopcao
    lcDM = lcDM + uCrsDMl.cnp + uCrsDMl.portaria + uCrsDMl.pvp + uCrsDMl.pref + uCrsDMl.comparticipacao + uCrsDMl.valorutente + uCrsDMl.p4mb + uCrsDMl.fee + uCrsDMl.direitoopcao 
    ENDSCAN
    fecha("uCrsDMl")

    ** total utente liquido
    lcDM = lcDM + dm_totalutenteliquido
    ****************

    ** adicionar o datamatrix ao conte�do da receita
    public lcDmText, lcDmFile
    lcDmText=lcDM
    store '' to lcDmFile

    ** caracteres "##DM" identificam datamatrix
    lcDM = "??##DM" + lcDM
    lcMsg = STRTRAN(lcMsg, "%%1", lcDM)
    
    
    IF DIRECTORY(ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\ltsPrint')
        lcFilePath = ALLTRIM(ALLTRIM(uf_gerais_getParameter("ADM0000000015","TEXT")) + "\Cache\" + ALLTRIM(lcstamp) + ".png")

        lcDmFile = lcFilePath && variavel publica usada no IDU
        
    
                    
        IF !FILE(ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\ltsPrint\ltsPrint.jar')
            uf_perguntalt_chama("Ficheiro configura��o de Datamatrix n�o encontrado. Contacte o Suporte","OK","",64)
            RETURN .f.
        ENDIF 
        
        lcWsPath = ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\ltsPrint\ltsPrint.jar "datamatrix" "' + ALLTRIM(lcFilePath) + '" "' + ALLTRIM(lcDmText) + '"'
        lcWsPath = "javaw -jar " + lcWsPath 


        
        oWSShell = CREATEOBJECT("WScript.Shell")
        oWSShell.Run(lcWsPath, 0, .f.)	
    ENDIF
    
    WAIT WINDOW 'A criar codigo datamatrix...' TIMEOUT 2 && Tempo para permitir a gera��o da imagem na cache
        
	uv_idImp = uf_gerais_getUmValor("B_impressoes", "id", "tabela = 'FT' and nomeImpressao = 'Tal�o Verso Receita'")

    SELECT uCrsFt2

	LOCAL lcCod, lcCod2, imprimeVerso1, imprimeVerso2, lcPsico
	lcCod	= uCrsFt2.u_codigo
	lcCod2	= uCrsFt2.u_codigo2

	STORE 0 TO imprimeVerso1, imprimeVerso2
	
	SELECT uCrsFi
    GO TOP
	SCAN FOR ALLTRIM(uCrsFi.ftstamp) = ALLTRIM(lcStamp)
		IF uCrsFi.u_ettent1>0
			imprimeVerso1 = imprimeVerso1 + uCrsFi.qtt
		ENDIF
		IF uCrsFi.u_ettent2>0
			imprimeVerso2 = imprimeVerso2 + uCrsFi.qtt
		ENDIF
	ENDSCAN

    LOCAL lcLote, lcLote2		
    LOCAL lcSlip 
    
    STORE "0" TO lcLote
    STORE "0" TO lcLote2
    
    SELECT T
    lcLote = ALLTRIM(T.tlote)
    SELECT T2
    lcLote2 = ALLTRIM(T2.tlote)
    
    **Valida exep��es � impressao por talao
    IF uf_imprimirpos_exepcoesImprimirTalao(lcCod2,lcLote,lcCod, lcLote2)
        lcSlip = "1"
    ELSE
        IF(imprimeVerso1<=4)
            lcSlip = "4"
        ELSE
            lcSlip = "1"

        ENDIF
            
    ENDIF

    IF (lcVerso = 1 AND imprimeVerso1 <> 0) OR (lcVerso = 2 AND imprimeVerso2 <> 0)
    

	    IF uv_aviso 
	
		    uf_perguntalt_chama(ASTR(lcVerso) + " - VAI SER IMPRESSA A RECEITA DA ENTIDADE " + IIF(lcVerso = 1, ALLTRIM(uCrsFt2.u_abrev), ALLTRIM(uCrsFt2.u_abrev2))+" N� " + ALLTRIM(uCrsFt2.u_receita),"OK","",64)		
	
	    ENDIF

	    uf_imprimirgerais_sendprinter_talao(uv_idImp, uv_reImp, uv_prev, lcSlip)

    ENDIF
			
ENDFUNC 

FUNCTION uf_imprimirpos_validaSeImprimeVersoReceita
	LPARAMETERS lcVerso
    
    IF !USED("uCrsFt2")
    	RETURN .F.
    ENDIF
    
	**u_codigo � prenechido se for entidade ou sns sozinhos
	**u_codigo2 tem o valor do sns caso tenha plano complementar que esta rpenechido no u_codigo    
    IF lcVerso == 1 AND !uf_gerais_getUmValor("cptpla", "imprime", "codigo = '" + ALLTRIM(uCrsFt2.u_codigo) + "'")
    	RETURN .F.
    ENDIF
	
	IF lcVerso == 2 AND !uf_gerais_getUmValor("cptpla", "imprime", "codigo = '" + ALLTRIM(uCrsFt2.u_codigo2) + "'")
		RETURN .F.
	ENDIF
	
	RETURN .T.
ENDFUNC