FUNCTION uf_pagExterno_chama
	LPARAMETERS uv_refPag, uv_no, uv_estab, uv_email, uv_tlmvl, uv_desc, uv_duracao
	
	IF EMPTY(uv_refPag)
		RETURN .F.
	ENDIF

	IF uf_gerais_getUmValor("regrasPagExterno", "count(*)", "refPag = '" + uv_refPag + "'") = 0
		uf_perguntalt_chama("Este M�todo de Pagamento n�o est� devidamente configurado. Por favor contacte o suporte.","OK","",64)
		RETURN .F.
	ENDIF

	IF TYPE("PAGEXTERNO")=="U"
		DO FORM PAGEXTERNO WITH uv_refPag, uv_no, uv_estab, uv_email, uv_tlmvl, uv_desc
	ELSE
		PAGEXTERNO.release
		DO FORM PAGEXTERNO WITH uv_refPag, uv_no, uv_estab, uv_email, uv_tlmvl, uv_desc
	ENDIF
	
ENDFUNC

FUNCTION uf_pagExterno_sair

    IF WEXIST("PAGEXTERNO")
		PAGEXTERNO.hide
        PAGEXTERNO.release
    ENDIF

ENDFUNC

FUNCTION uf_pagExterno_gravar

	IF !USED("uc_regrasPagExterno")
		RETURN .F.
	ENDIF
	
	uv_tlmvl = ALLTRIM(PAGEXTERNO.txtNumTlmvl.value)
	uv_email = ALLTRIM(PAGEXTERNO.txtMail.value)
	uv_duracao = ALLTRIM(PAGEXTERNO.txtDuracao.value)
	uv_descricao = ALLTRIM(PAGEXTERNO.edtDescricao.value)
	
	SELECT uc_regrasPagExterno
	
	IF uc_regrasPagExterno.tlmvl AND EMPTY(uv_tlmvl)
		uf_perguntalt_chama("O campo N�mero de Telem�vel � de preenchimento obrigat�rio.","OK","",64)
		PAGEXTERNO.txtNumTlmvl.setFocus()
		RETURN .F.
	ENDIF
	
	IF !EMPTY(uv_tlmvl) AND !uf_gerais_checkRegex("^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$", ALLTRIM(uv_tlmvl))
		uf_perguntalt_chama("Tem de introduzir um N�mero de Telem�vel v�lido.","OK","",64)
		PAGEXTERNO.txtNumTlmvl.setFocus()
		RETURN .F.
	ENDIF
	
	IF uc_regrasPagExterno.eMail AND EMPTY(uv_email)
		uf_perguntalt_chama("O campo E-mail � de preenchimento obrigat�rio.","OK","",64)
		PAGEXTERNO.txtMail.setFocus()
		RETURN .F.
	ENDIF

	IF !EMPTY(uv_email) AND !uf_gerais_checkRegex("^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$",ALLTRIM(uv_email))
		uf_perguntalt_chama("Tem de introduzir um E-mail v�lido.","OK","",64)
		PAGEXTERNO.txtMail.setFocus()
		RETURN .F.
	ENDIF
	
	IF uc_regrasPagExterno.duracao AND (EMPTY(uv_duracao) OR uv_duracao = "0")
		uf_perguntalt_chama("O campo Dura��o � de preenchimento obrigat�rio.","OK","",64)
		PAGEXTERNO.txtDuracao.setFocus()
		RETURN .F.
	ENDIF
	
	IF !EMPTY(uv_duracao) AND !uf_gerais_checkRegex("^\d+$", ALLTRIM(uv_duracao))
		uf_perguntalt_chama("O campo Dura��o � de preenchimento obrigat�rio.","OK","",64)
		PAGEXTERNO.txtDuracao.setFocus()
		RETURN .F.
	ENDIF
	
	IF uc_regrasPagExterno.descricao AND EMPTY(uv_descricao)
		uf_perguntalt_chama("O campo Descri��o � de preenchimento obrigat�rio.","OK","",64)
		PAGEXTERNO.edtDescricao.setFocus()
		RETURN .F.
	ENDIF
	
	uv_cur = "uc_dadosPagExterno" + ALLTRIM(PAGEXTERNO.modoPag)
	
	IF USED(uv_cur)
		FECHA(uv_cur)
	ENDIF
	
	CREATE CURSOR &uv_cur. (tlmvl C(50), email C(254), duracao C(50), descricao M)
	
	SELECT &uv_cur.
	APPEND BLANK 
	REPLACE &uv_cur..tlmvl WITH uv_tlmvl,;
			&uv_cur..email WITH uv_eMail,;
			&uv_cur..duracao WITH IIF(PAGEXTERNO.txtDuracao.visible, ALLTRIM(uv_duracao), ''),;
			&uv_cur..descricao WITH ALLTRIM(uv_descricao)
			
	uf_pagExterno_sair()

ENDFUNC

PROCEDURE uf_pagExterno_resize


	SELECT uc_regrasPagExterno
	GO TOP

	IF !uc_regrasPagExterno.tlmvlVisible
		PAGEXTERNO.txtnumTlmvl.Visible = .F.
		PAGEXTERNO.lblTlmvl.Visible = .F.
		
		PAGEXTERNO.txtMail.Top = 	PAGEXTERNO.txtMail.Top - 70
		PAGEXTERNO.lblMail.Top = 	PAGEXTERNO.lblMail.Top - 70
		
		PAGEXTERNO.txtDuracao.Top = 	PAGEXTERNO.txtDuracao.Top - 70
		PAGEXTERNO.lblDuracao.Top = 	PAGEXTERNO.lblDuracao.Top - 70
		
		PAGEXTERNO.edtDescricao.Top = PAGEXTERNO.edtDescricao.Top - 70
		PAGEXTERNO.lblDescricao.Top = PAGEXTERNO.lblDescricao.Top - 70
	ENDIF

	IF !uc_regrasPagExterno.emailVisible
		PAGEXTERNO.txtMail.Visible = .F.
		PAGEXTERNO.lblMail.Visible = .F.
			
		PAGEXTERNO.txtDuracao.Top = 	PAGEXTERNO.txtDuracao.Top - 70
		PAGEXTERNO.lblDuracao.Top = 	PAGEXTERNO.lblDuracao.Top - 70
		
		PAGEXTERNO.edtDescricao.Top = PAGEXTERNO.edtDescricao.Top - 70
		PAGEXTERNO.lblDescricao.Top = PAGEXTERNO.lblDescricao.Top - 70
	ENDIF

	IF !uc_regrasPagExterno.duracaoVisible
		PAGEXTERNO.txtDuracao.Visible = .F.
		PAGEXTERNO.lblDuracao.Visible = .F.		
		
		PAGEXTERNO.EdtDescricao.Top = PAGEXTERNO.EdtDescricao.Top - 70
		PAGEXTERNO.lblDescricao.Top = PAGEXTERNO.lblDescricao.Top - 70
	ENDIF

	IF !uc_regrasPagExterno.descricaoVisible
		PAGEXTERNO.edtDescricao.Visible = .F.
		PAGEXTERNO.lblDescricao.Visible = .F.		
	ENDIF


ENDPROC