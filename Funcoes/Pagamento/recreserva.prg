** chama o painel 
FUNCTION uf_RECRESERVA_chama	
	LOCAL lnTotal, lcDataReceita 
	lnTotal = 0
	lcDataReceita = DATE()
	

	IF USED("crsAtendStCopia")	
		fecha("crsAtendStCopia")			
	ENDIF
		

	IF USED("crsFICopia")
		fecha("crsFICopia")
	ENDIF 
	
		
	IF USED("crsFI2Copia")
		fecha("crsFI2Copia")
	ENDIF 
	
	IF USED("crsFTCopia")
		fecha("crsFTCopia")
	ENDIF 
	
		
	IF USED("crsFT2Copia")
		fecha("crsFT2Copia")
	ENDIF 

	

	CREATE CURSOR uCrsRec(sel L, venda C(254), total N(13,2), stamp c(25), dataReceita d)
	SELECT FIinsercaoReceita
	GO TOP 
	SCAN 
		IF LEFT(ALLTRIM(FIinsercaoReceita.design),1) == "."
		
			&& Atribui data de Receita, preechida no atendimento	
			IF USED("uCrsAtendComp")
				Select uCrsAtendComp
				LOCATE FOR ALLTRIM(uCrsAtendComp.fistamp) == ALLTRIM(FIinsercaoReceita.fistamp)
				IF FOUND()
					** lcDataReceita = uCrsAtendComp.datareceita
					lcDataReceita = uCrsAtendComp.dataDispensa
				ENDIF 
			ENDIF 
		
			INSERT INTO uCrsRec (sel, venda, total, stamp, dataReceita) ;
			VALUES(.f., ALLTRIM(FIinsercaoReceita.design), lnTotal, ALLTRIM(FIinsercaoReceita.fistamp), lcDataReceita)
			lnTotal = 0
		ELSE
			lnTotal = lnTotal + FIinsercaoReceita.etiliquido
			IF RECCOUNT("uCrsRec")>0
				SELECT uCrsRec
				replace uCrsRec.total WITH lnTotal
			ENDIF 
		ENDIF 
		
		SELECT FIinsercaoReceita
	ENDSCAN 
	
	**SELECT uCrsRec
	**DELETE FOR uCrsRec.total=0
	
	SELECT uCrsRec
	GO TOP 
	
	&& controla abertura do painel	
	IF TYPE("RECRESERVA") == "U"
		DO FORM RECRESERVA
	ELSE
		RECRESERVA.show()
	ENDIF
ENDFUNC


**
FUNCTION uf_RECRESERVA_sel
	LOCAL lcCodAcesso, lcCodDirOpcao, lcToken, lcTipoReceita, lcEntVal2, lcNrencontra 
	STORE '' TO lcCodAcesso, lcCodDirOpcao, lcToken, lcTipoReceita, lcEntVal2
	STORE 0 TO lcNrencontra
	recreserva.Timer2.Enabled=.f.	

	**
	SELECT uCrsRec
	IF !uCrsRec.sel
		RETURN .f.
	ENDIF 
	IF USED("uCrsVerificaRespostaDEM")
		IF Empty(FIinsercaoReceita.u_nbenef2) 

		 	DO CASE 
		 		CASE at('.[FN]',ucrsrec.venda)>0 
		 			lcNrencontra=lcNrencontra+1
				CASE at('.[707]',ucrsrec.venda)>0 
					lcNrencontra=lcNrencontra+1
				CASE at('.[FL]',ucrsrec.venda)>0 
					lcNrencontra=lcNrencontra+1
				CASE at('.[WG][ASTELLAS]',ucrsrec.venda)>0 
					lcNrencontra=lcNrencontra+1
				CASE at('.[AS][ASTELLAS]',ucrsrec.venda)>0 
					lcNrencontra=lcNrencontra+1
				CASE at('.[QS][Ferring]',ucrsrec.venda)>0 
					lcNrencontra=lcNrencontra+1
				CASE at('.[GK][LUNDBECK]',ucrsrec.venda)>0 
					lcNrencontra=lcNrencontra+1
				CASE at('.[N03][NOVARTISAPOIO]',ucrsrec.venda)>0 
					lcNrencontra=lcNrencontra+1
				CASE at('.[P01][NOVARTISAPOIO]',ucrsrec.venda)>0 
					lcNrencontra=lcNrencontra+1
				CASE at('.[PE][NOVARTISAPOIO]',ucrsrec.venda)>0 
					lcNrencontra=lcNrencontra+1
				CASE at('.[PF][NOVARTISAPOIO]',ucrsrec.venda)>0 
					lcNrencontra=lcNrencontra+1
				CASE at('.[P02][NOVARTISAPOIO]',ucrsrec.venda)>0 
 					lcNrencontra=lcNrencontra+1
				CASE at('.[N04][NOVARTISAPOIO]',ucrsrec.venda)>0 
					lcNrencontra=lcNrencontra+1
				CASE at('.[Y01][NOVARTISAPOIO]',ucrsrec.venda)>0 
					lcNrencontra=lcNrencontra+1
				CASE at('.[QU1][SERVIER]',ucrsrec.venda)>0 
	 				lcNrencontra=lcNrencontra+1
				CASE at('.[QU2][SERVIER]',ucrsrec.venda)>0 
					lcNrencontra=lcNrencontra+1
				CASE at('.[QP][PSOPortugal]',ucrsrec.venda)>0
					lcNrencontra=lcNrencontra+1
				CASE at('.[01][SNS]',ucrsrec.venda)>0
					lcNrencontra=lcNrencontra+1
				CASE at('.[41][SNS]',ucrsrec.venda)>0
					lcNrencontra=lcNrencontra+1
				CASE at('.[45][SNS]',ucrsrec.venda)>0
					lcNrencontra=lcNrencontra+1					
				CASE at('.[45A][SNS]',ucrsrec.venda)>0
					lcNrencontra=lcNrencontra+1
				CASE at('.[45B][SNS]',ucrsrec.venda)>0
					lcNrencontra=lcNrencontra+1					
				CASE at('.[46][SNS]',ucrsrec.venda)>0
					lcNrencontra=lcNrencontra+1					
				CASE at('.[46A][SNS]',ucrsrec.venda)>0
					lcNrencontra=lcNrencontra+1
				CASE at('.[47][SNS]',ucrsrec.venda)>0
					lcNrencontra=lcNrencontra+1					
				CASE at('.[48][SNS]',ucrsrec.venda)>0
					lcNrencontra=lcNrencontra+1		
				CASE at('.[48A][SNS]',ucrsrec.venda)>0
					lcNrencontra=lcNrencontra+1								
				CASE at('.[CE][SNS]',ucrsrec.venda)>0
					lcNrencontra=lcNrencontra+1					
				CASE at('.[DO][SNS]',ucrsrec.venda)>0
					lcNrencontra=lcNrencontra+1					
				CASE at('.[DS][SNS]',ucrsrec.venda)>0
					lcNrencontra=lcNrencontra+1					
				CASE at('.[LA][SNS]',ucrsrec.venda)>0
					lcNrencontra=lcNrencontra+1					
				OTHERWISE 

					**
			ENDCASE 

		 	** 	AND (ALLTRIM(uCrsAtendComp.codigo) != 'FN' ;
		 	**	AND ALLTRIM(uCrsAtendComp.codigo) != '707' ;
		 	**	AND UPPER(ALLTRIM(uCrsAtendComp.codigo)) != 'FL' ;
		 	**	AND ALLTRIM(Atendimento.pgfDados.page3.txtAbrev.value)!='ASTELLAS' ;
		 	**	AND ALLTRIM(Atendimento.pgfDados.page3.txtAbrev.value)!='Ferring' ;
		 	**	AND ALLTRIM(Atendimento.pgfDados.page3.txtAbrev.value)!='LUNDBECK' ;
		 	**	AND ALLTRIM(Atendimento.pgfDados.page3.txtAbrev.value)!='NOVARTISAPOIO' ;
		 	**	AND ALLTRIM(Atendimento.pgfDados.page3.txtAbrev.value)!='SERVIER' ;
		 	**	AND ALLTRIM(Atendimento.pgfDados.page3.txtAbrev.value)!='PSOPortugal')

			 	IF lcNrencontra=0
			 	LOCAL lcNb
				STORE '' TO lcNb
				lcNb=INPUTBOX("QUAL O N� DE BENEFICI�RIO?","Falta N� de Benefici�rio.","")
				replace FIinsercaoReceita.u_nbenef2 WITH lcNb

				**uf_perguntalt_chama("TEM QUE INTRODUZIR O N� DE BENEFICI�RIO2.","OK","",64)
				**Return .F.
			ENDIF 

		ENDIF 
	
	ELSE 
		
		**SELECT ucrsrec
		**lcEntVal2 = substr(ucrsrec.venda,at('[',substr(ucrsrec.venda,3,20))+3,at(']',substr(ucrsrec.venda,9,20)))
		
		IF EMPTY(FIinsercaoReceita.u_nbenef2) ;
 			AND at('.[01][SNS]',ucrsrec.venda)=0 ;
 			AND at('.[AS][ASTELLAS]',ucrsrec.venda)=0 ;
 			AND at('.[WG][ASTELLAS]',ucrsrec.venda)=0 ;
 			AND at('.[QS][Ferring]',ucrsrec.venda)=0 ;
 			AND at('.[GK][LUNDBECK]',ucrsrec.venda)=0 ;
 			AND at('.[N03][NOVARTISAPOIO]',ucrsrec.venda)=0 ;
 			AND at('.[P01][NOVARTISAPOIO]',ucrsrec.venda)=0 ;
 			AND at('.[PE][NOVARTISAPOIO]',ucrsrec.venda)=0 ;
 			AND at('.[PF][NOVARTISAPOIO]',ucrsrec.venda)=0 ;
 			AND at('.[P02][NOVARTISAPOIO]',ucrsrec.venda)=0 ;
 			AND at('.[N04][NOVARTISAPOIO]',ucrsrec.venda)=0 ;
 			AND at('.[Y01][NOVARTISAPOIO]',ucrsrec.venda)=0 ;
 			AND at('.[QU1][SERVIER]',ucrsrec.venda)=0 ;
 			AND at('.[QU2][SERVIER]',ucrsrec.venda)=0 ;
 			AND at('.[QP][PSOPortugal]',ucrsrec.venda)=0 ;
 			AND at('[SNS]',ucrsrec.venda)=0 
 			**AND Empty(ft2.U_NBENEF2)
			**uf_perguntalt_chama("TEM QUE INTRODUZIR O N� DE BENEFICI�RIO2.","OK","",64)
			**Return .F.
			
			LOCAL lcNb
			STORE '' TO lcNb
			lcNb=INPUTBOX("QUAL O N� DE BENEFICI�RIO?","Falta N� de Benefici�rio.","")
			replace FIinsercaoReceita.u_nbenef2 WITH lcNb
		ENDIF 
	ENDIF 

**	IF Empty(ft2.U_NBENEF) AND !EMPTY(FIinsercaoReceita.u_nbenef)
**		replace ft2.U_NBENEF WITH ALLTRIM(FIinsercaoReceita.u_nbenef)
**	endif
	
**	IF Empty(ft2.U_NBENEF2) AND !EMPTY(FIinsercaoReceita.u_nbenef2)
**		replace ft2.U_NBENEF2 WITH ALLTRIM(FIinsercaoReceita.u_nbenef2)
**	endif
	
	atendimento.lockscreen = .t.
	
	LOCAL lcFIStampCopy, llExit, lcCod, lcReceita, lcSQL, lcCod2, lcDesign, lcDesign2, lcAbrev, lcAbrev2
	

	
	** get codigo plano e numero receita
	SELECT FIinsercaoReceita
	LOCATE FOR ALLTRIM(FIinsercaoReceita.fistamp) == ALLTRIM(uCrsRec.stamp)
	IF FOUND()
		SELECT FIinsercaoReceita
		&& variavel com o tipo de receita para controlar se imprime verso de receita ou n�o quando a receita � criada
		lcTipoReceita = ALLTRIM(FIinsercaoReceita.tipoR)
		
		** get codigo plano
		lcCod = STREXTRACT(FIinsercaoReceita.design, '[', ']', 1, 0)
		
		lcSQL = ''
		TEXT TO lcSql NOSHOW TEXTMERGE
			exec up_receituario_dadosDoPlano '<<lcCod>>'
		ENDTEXT
		If !uf_gerais_actGrelha("",[uCrsCodPla],lcSql) OR Reccount([uCrsCodPla])=0
			uf_perguntalt_chama("O C�DIGO '"+lcCod+"' N�O EXISTE. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			RETURN .f.
		ENDIF
		
		
		** nr receita
		SELECT FIinsercaoReceita
		lcReceita	= STREXTRACT(FIinsercaoReceita.design, ':', ' -', 1, 0)
		
		** c�digos DEM p grava��o correta da informa��o
		IF USED("uCrsCabVendas")
			SELECT uCrsCabVendas
			LOCATE FOR ALLTRIM(ASTR(uCrsCabVendas.lordem)) == ALLTRIM(ASTR(FIinsercaoReceita.lordem))
			IF FOUND()
				lcCodAcesso = ALLTRIM(uCrsCabVendas.CodAcesso)
				lcCodDirOpcao = ALLTRIM(uCrsCabVendas.CodDirOpcao)
				lcToken = ALLTRIM(uCrsCabVendas.Token)
			ENDIF
		ENDIF

		SELECT uCrsCodPla
		lcCod2		= uCrsCodPla.codigo2
		lcDesign	= uCrsCodPla.design
		lcDesign2	= uCrsCodPla.design2
		lcAbrev		= uCrsCodPla.cptorgabrev
		lcAbrev2	= uCrsCodPla.cptorgabrev2
			
		IF uCrsCodPla.obriga_nrReceita == 2 AND EMPTY(lcReceita)
			uf_perguntalt_chama("O n�mero de receita � obrigat�rio. Por favor verifique.", "OK", "", 16)
			SELECT uCrsRec
			replace uCrsRec.sel WITH .f.
			fecha("uCrsCodPla")
			RETURN .f.
		ENDIF
		fecha("uCrsCodPla")
	ENDIF
	
	*guardar copia dos cursores FI, FT e FT2, ucrsAtendST
	SELECT FI
	GO TOP
	SELECT * FROM fi INTO CURSOR crsFICopia READWRITE 
	SELECT FI2
	GO TOP
	SELECT * FROM fi2 INTO CURSOR crsFI2Copia READWRITE 
	SELECT FT
	GO TOP
	SELECT * FROM FT INTO CURSOR crsFTCopia READWRITE 
	SELECT FT2
	GO TOP
	SELECT * FROM ft2 INTO CURSOR crsFT2Copia READWRITE 	

	
	&&Guarda a copia do UCRSATENDST  	
	SELECT UCRSATENDST  
	GO TOP
	SELECT * FROM UCRSATENDST  INTO CURSOR  crsAtendStCopia READWRITE 
	
	
	*gerar cursor FI com dados desta receita
	SELECT FI
	DELETE ALL 
	
	IF !USED("crsFItmp")
		SELECT '' as CodAcesso, '' as CorDirOpcao, * FROM FI WHERE 1=0 INTO CURSOR crsFItmp READWRITE
	ELSE
		SELECT crsFItmp
		DELETE ALL
	ENDIF 
	
	SELECT FIinsercaoReceita
	LOCATE FOR ALLTRIM(FIinsercaoReceita.fistamp) == ALLTRIM(uCrsRec.stamp)
	IF FOUND()

		
		
		&& guardar codigos local prescri��o e m�dico
		SELECT FIinsercaoReceita
		SELECT ft2
		replace ft2.u_entpresc	WITH ALLTRIM(FIinsercaoReceita.rvpstamp) IN ft2
		replace ft2.u_nopresc	WITH ALLTRIM(FIinsercaoReceita.szzstamp) IN ft2
		replace ft2.u_nbenef	WITH ALLTRIM(FIinsercaoReceita.u_nbenef) IN ft2
		replace ft2.u_nbenef2	WITH ALLTRIM(FIinsercaoReceita.u_nbenef2) IN ft2
		
		
			
		LOCAL lcIdCartao 
		
		SELECT FIinsercaoReceita
		lcIdCartao  =  ALLTRIM(FIinsercaoReceita.u_nbenef2)
			
		SELECT FIinsercaoReceita		
		if(USED("ucrsatendcomp"))
			SELECT ucrsatendcomp
			GO TOP
			SCAN FOR ALLTRIM(ucrsatendcomp.fistamp) == ALLTRIM(FIinsercaoReceita.fistamp)
				lcIdCartao = ALLTRIM(ucrsatendcomp.nCartao)
			SELECT ucrsatendcomp
			ENDSCAN	
		ENDIF
		

	
		SELECT FIinsercaoReceita
	
		**
		
		SELECT FT2
		replace ft2.token		 WITH lcToken
		replace ft2.CodAcesso  	 WITH lcCodAcesso 
		replace ft2.CodDirOpcao  WITH lcCodDirOpcao 
		replace ft2.u_receita 	 WITH lcReceita
		replace ft2.u_codigo  	 WITH lcCod
		replace ft2.u_codigo2 	 WITH lcCod2
		replace ft2.u_design  	 WITH lcDesign
		replace ft2.u_design2 	 WITH lcDesign2
		replace ft2.u_abrev   	 WITH lcAbrev
		replace ft2.u_abrev2   	 WITH lcAbrev2
		replace ft2.c2codpost	 WITH lcIdCartao	
		
		
		
		replace ft2.u_acertovs 	WITH 0
		replace ft2.modop1 		WITH ''
		replace ft2.modop2 		WITH ''
		replace ft2.epaga1 		WITH 0
		replace ft2.epaga2 		WITH 0
		replace ft2.eacerto 	WITH 0
		replace ft2.etroco 		WITH 0
		replace ft2.evdinheiro 	WITH 0
		**
		
		SELECT FIinsercaoReceita
		SKIP 1
		DO WHILE !(Left(FIinsercaoReceita.design,1) == ".") AND llExit == .f.
			SELECT FIinsercaoReceita
			IF EOF("FIinsercaoReceita")
				llExit = .t.
			ELSE
				lcFIStampCopy = FIinsercaoReceita.fistamp

				SELECT * FROM crsFItmp ;
				UNION ALL ;
				SELECT * FROM FIinsercaoReceita WHERE ALLTRIM(FIinsercaoReceita.fistamp) == ALLTRIM(lcFIStampCopy) ;
				INTO CURSOR crsFItmp READWRITE 
				
				SELECT FIinsercaoReceita
				LOCATE FOR ALLTRIM(FIinsercaoReceita.fistamp) == ALLTRIM(lcFIStampCopy)
				
				SKIP 1
			ENDIF 
		ENDDO 
	ENDIF 
	LOCAL lcpsicoreceita
	STORE .f. TO lcpsicoreceita
	SELECT fi
	APPEND FROM DBF("crsFItmp")
	replace ALL fi.partes WITH 0 IN "FI"
	SELECT fi
	GO TOP 
	SCAN
		LOCAL lcFistamp 
		lcFistamp = ALLTRIM(fi.fistamp)
		lnNovoStamp = uf_gerais_stamp()
		replace fi.fistamp WITH lnNovoStamp IN "FI"
		IF fi.u_psico=.t.
			lcpsicoreceita = .t.
		ENDIF 
		if(USED("fi2"))
			SELECT fi2
			GO TOP 
			SCAN FOR fi2.fistamp==ALLTRIM(lcFistamp )
				replace fi2.fistamp WITH ALLTRIM(lnNovoStamp) IN "FI2"
			SELECT fi2
			ENDSCAN 
		endif

	SELECT fi	
	ENDSCAN 
	

	**
	
	** FT
	uf_atendimento_actTotaisFt()
	
	SELECT * FROM ucrsGeralTD WHERE u_tipodoc == 3 AND site == ALLTRIM(mySite) INTO CURSOR "TD"
	
	replace ft.nmdoc WITH td.nmdoc IN "FT"
	replace ALL fi.nmdoc WITH td.nmdoc IN "FI"
	replace ft.ndoc WITH td.ndoc IN "FT"
	replace ALL fi.ndoc WITH td.ndoc IN "FI"
	replace ft.tipodoc WITH td.tipodoc IN "FT"
	replace ALL fi.tipodoc WITH td.tipodoc IN "FI"
	replace ALL fi.valcartao WITH 0 IN "FI"
	
	replace ft.cobrado WITH .f. IN "FT"
	
	TEXT TO lcSQL NOSHOW TEXTMERGE
		select dateadd(HOUR, <<difhoraria>>, getdate()) data
	ENDTEXT 	
	uf_gerais_actGrelha("","uCrsMyInvoiceData",lcSQL)
	select uCrsMyInvoiceData
	myInvoiceData = uCrsMyInvoiceData.data
	replace ft.fdata WITH myInvoiceData IN "FT"
	
	

	&& Caso a data de receita seja alterada no Atendimento	
	IF !EMPTY(ucrsRec.datareceita) AND YEAR(ucrsRec.datareceita) != 1900
		Select ucrsRec
		lcDataReceita = ucrsRec.dataReceita
	ELSE 
		lcDataReceita = myInvoiceData 
	ENDIF

	replace ft.bidata WITH lcDataReceita IN "FT"
	
	lcFtStamp = uf_gerais_stamp()
	lcFtStamprec = ALLTRIM(lcFtStamp)
	
	SELECT ft
	replace ft.ftstamp WITH lcFtStamp 
	replace ft.u_nratend WITH nratendimento
	SELECT fi
	replace ALL fi.ftstamp WITH lcFtStamp 
	SELECT fi2
	replace ALL fi2.ftstamp WITH lcFtStamp 
	SELECT ft2
	replace ft2.ft2stamp with lcFtStamp 
	
	IF USED("docs_reserva")
		SELECT docs_reserva
		GO BOTTOM 
		APPEND BLANK 
		replace stampdoc WITH ALLTRIM(lcFtStamp)
		replace tabledoc WITH 'FT'
		replace tipodoc WITH 'Receita'
	ENDIF 
	
	SELECT FT
	replace ft.fno WITH 1
	**

	&& Cursor de Dados do Produto (semelhante ao atendimento)
	IF !USED("UCRSATENDST")
		IF !(uf_gerais_actGrelha("","UCRSATENDST",[set fmtonly on exec up_touch_ActRef '',0,<<mysite_nr>> set fmtonly off]))
			uf_perguntalt_chama("OCORREU UM ERRO A CRIAR O CURSOR [UCRSATENDST]-fact.","OK","",16)
			atendimento.lockscreen = .f.
			uf_recreserva_restauraCursores()
			RETURN .F.
		ENDIF
	ELSE
		SELECT ucrsAtendST
		DELETE ALL
		
		SELECT fi
		GO top
		SCAN 
		
			lcSql = ""
			TEXT TO lcSQL NOSHOW TEXTMERGE
				exec up_touch_ActRef '<<alltrim(fi.ref)>>',<<myArmazem>>,<<mysite_nr>>
			ENDTEXT 
			uf_gerais_actGrelha("",[uCrsActRef],lcSQL)
			
			INSERT INTO UCRSATENDST SELECT * FROM uCrsActRef
			SELECT fi
			SELECT UCRSATENDST
			GO BOTTOM 
			replace UCRSATENDST.ststamp with fi.fistamp	
		ENDSCAN 
		
	ENDIF
	**
	** gravar
	PUBLIC myFtIntroducao
	myFtIntroducao = .t.
	

	IF !uf_facturacao_gravar(.t.)
		myFtIntroducao = .f.
		uf_recreserva_restauraCursores()
		atendimento.lockscreen = .f.
		uf_perguntalt_chama("OCORREU UM ERRO A CRIAR A RECEITA","OK","",16)
		RETURN .f.
	ENDIF
	
	** Verificar se tem de fazer comparticipa��o eletr�nica da receita
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		select codigo from cptpla (nolock)
		where (codigo = (select u_codigo from ft2 (nolock) where ft2stamp = '<<ALLTRIM(lcFtStamprec)>>') or codigo = (select u_codigo2 from ft2 (nolock) where ft2stamp = '<<ALLTRIM(lcFtStamprec)>>'))
		and e_compart=1
	ENDTEXT 
	uf_gerais_actGrelha("","ucrscompartext",lcSQL) 
	
	LOCAL lcJaUtilizado 
	lcJaUtilizado  = .f.
	
	LOCAL lctokenpedefetiva
	lctokenpedefetiva = 'XXXXXXXXXXXXXXXXXXXXX'
	
	IF reccount("ucrscompartext") > 0

	
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			select top 1 utilizado, token from ft_compart (nolock) where nrreceita = (select u_receita from ft2 (nolock) where ft2stamp = '<<ALLTRIM(lcFtStamprec)>>' AND type=1) order by ousrdata desc
		ENDTEXT 
		uf_gerais_actGrelha("","ucrscomparttoken",lcSQL) 
		lctokenpedefetiva = ALLTRIM(ucrscomparttoken.token)	
		lcJaUtilizado = ucrscomparttoken.utilizado			
		lcNewTokenCompart = uf_gerais_stamp()
		
		
		IF EMPTY(lcJaUtilizado) AND EMPTY(uf_compart_efetiva(lctokenpedefetiva , lcNewTokenCompart))
			RETURN .f.
		ENDIF
	ENDIF 
	
	fecha("ucrscomparttoken")
	fecha("ucrscompartext")
	
	
	
	
	** gravar ftstamp na ft_compart
	IF TYPE("stampcompart")<>"U" AND LEN(ALLTRIM(stampcompart))>0
		TEXT TO lcSQL NOSHOW TEXTMERGE 

			
			UPDATE fi_compart  SET fi_compart.ftstamp='<<ALLTRIM(lcFtStamprec)>>' from fi_compart(nolock)  
			inner join ft_compart(nolock) on ft_compart.ftstamp = fi_compart.ftstamp and ft_compart.token = fi_compart.token
			WHERE ft_compart.nrreceita = (select u_receita from ft2 (nolock) where ft2stamp='<<ALLTRIM(lcFtStamprec)>>')
			and ft_compart.codacesso = (select codacesso from ft2 (nolock) where ft2stamp='<<ALLTRIM(lcFtStamprec)>>')
			and ft_compart.ousrdata>DATEADD(ss,-90,getdate())
			AND ft_compart.ftstamp like '%<<ALLTRIM(stampcompart)>>%' AND LEN(ft_compart.ftstamp)<21
			
			
			UPDATE ft_compart SET ft_compart.ftstamp='<<ALLTRIM(lcFtStamprec)>>' 
			WHERE ft_compart.nrreceita = (select u_receita from ft2 (nolock) where ft2stamp='<<ALLTRIM(lcFtStamprec)>>')
			and ft_compart.codacesso = (select codacesso from ft2 (nolock) where ft2stamp='<<ALLTRIM(lcFtStamprec)>>')
			and ft_compart.ousrdata>DATEADD(ss,-90,getdate())
			AND ft_compart.ftstamp like '%<<ALLTRIM(stampcompart)>>%' AND LEN(ft_compart.ftstamp)<21
			
			UPDATE ft_compart  SET utilizado = 1 WHERE token = '<<ALLTRIM(lctokenpedefetiva)>>' AND type = 1
			
		ENDTEXT 
	ELSE
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			
			UPDATE fi_compart  SET fi_compart.ftstamp='<<ALLTRIM(lcFtStamprec)>>' from fi_compart(nolock)  
			inner join ft_compart(nolock) on ft_compart.ftstamp = fi_compart.ftstamp and ft_compart.token = fi_compart.token
			WHERE ft_compart.nrreceita = (select u_receita from ft2 (nolock) where ft2stamp='<<ALLTRIM(lcFtStamprec)>>')
			and ft_compart.codacesso = (select codacesso from ft2 (nolock) where ft2stamp='<<ALLTRIM(lcFtStamprec)>>')
			and ft_compart.ousrdata>DATEADD(ss,-90,getdate())
					
			UPDATE ft_compart SET ft_compart.ftstamp='<<ALLTRIM(lcFtStamprec)>>' 
			WHERE ft_compart.nrreceita = (select u_receita from ft2 (nolock) where ft2stamp='<<ALLTRIM(lcFtStamprec)>>')
			and ft_compart.codacesso = (select codacesso from ft2 (nolock) where ft2stamp='<<ALLTRIM(lcFtStamprec)>>')
			and ft_compart.ousrdata>DATEADD(ss,-90,getdate())
			
			
			UPDATE ft_compart  SET utilizado = 1 WHERE token = '<<ALLTRIM(lctokenpedefetiva)>>' AND type = 1
			
		ENDTEXT 

	ENDIF 
	

	
	
	uf_gerais_actGrelha("","",lcSQL) 
	
	** gravar token da Ft_compart na FT2
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		update ft2 set token_efectivacao_compl= ISNULL((select top 1 token from ft_compart (nolock) where ftstamp='<<ALLTRIM(lcFtStamprec)>>' AND type=2),'')
		where ft2stamp='<<ALLTRIM(lcFtStamprec)>>'
	ENDTEXT 
	uf_gerais_actGrelha("","",lcSQL) 
	
	IF lcpsicoreceita = .t.
		** valida se os dadospsico est�o preenchidos
		SELECT dadospsicoaux	
		if reccount("dadospsicoaux")=0
			select dadospsicoaux
			append blank	
		endif 
		
		SELECT dadospsicoaux
		GO TOP 
		IF YEAR(dadospsicoaux.u_recdata)=1900 OR YEAR(dadospsicoaux.u_recdata)=0
			IF USED("uCrsatendCl")
				IF uf_gerais_getDate(uCrsatendCl.nascimento,"SQL") != '19000101'
					lcIdade = STR(uf_gerais_getIdade(uCrsatendCl.nascimento))
				else
					**lcIdade = str(lcIdade)
					lcIdade = str(0)
				ENDIF
				replace dadospsicoaux.u_nmutavi WITH uCrsatendCl.u_nmutavi
				replace dadospsicoaux.u_moutavi WITH uCrsatendCl.u_moutavi
				replace dadospsicoaux.u_cputavi WITH uCrsatendCl.u_cputavi
				replace dadospsicoaux.u_ndutavi WITH uCrsatendCl.u_ndutavi
				replace dadospsicoaux.u_ddutavi WITH uCrsatendCl.u_ddutavi
				replace dadospsicoaux.u_idutavi WITH uCrsatendCl.u_idutavi
				replace dadospsicoaux.u_nmutdisp WITH uCrsatendCl.nome
				replace dadospsicoaux.u_moutdisp WITH uCrsatendCl.morada
				replace dadospsicoaux.u_cputdisp WITH uCrsatendCl.codpost
			
				IF USED("uCrsatendCl")
						SELECT uCrsatendCl
						uv_nascimentoAD = uCrsatendCl.u_dcutavi
				ELSE
					IF USED("dadospsico")
						uv_nascimentoAD = dadospsico.nascimento
					ELSE 
						uv_nascimentoAD = ''
					ENDIF 
				ENDIF
				REPLACE dadospsicoaux.nascimento WITH uv_nascimentoAD 
			
				SELECT uCrsCabVendas
				replace dadospsicoaux.u_recdata WITH uCrsCabVendas.datareceita
				replace dadospsicoaux.u_medico  WITH ALLTRIM(uCrsCabVendas.nopresc)
			ENDIF 
		ENDIF 
		
		SELECT dadospsicoaux
		GO TOP 
		
		**uf_perguntalt_chama("PSICOTR�PICOS NAS RECEITAS","OK","",16)
		lcSQL = ''
		IF !empty(dadospsicoaux.u_nmutdisp)
							
			TEXT TO lcSQL NOSHOW TEXTMERGE 
				delete from B_dadosPsico where ftstamp='<<ALLTRIM(ft.ftstamp)>>'
				
				insert into B_dadosPsico (
					ftstamp,
					u_recdata, u_medico,
					u_nmutdisp, u_moutdisp, u_cputdisp,
					u_nmutavi, u_moutavi, u_cputavi,
					u_ndutavi, u_ddutavi, u_idutavi,
					u_dirtec, nascimento
				)
				Values (
					'<<ALLTRIM(ft.ftstamp)>>',
					'<<IIF(empty(dadospsicoaux.u_recdata),'19000101',uf_gerais_getDate(dadospsicoaux.u_recdata,"SQL"))>>', '<<ALLTRIM(dadospsicoaux.u_medico)>>',
					'<<ALLTRIM(dadospsicoaux.u_nmutdisp)>>', '<<ALLTRIM(dadospsicoaux.u_moutdisp)>>', '<<ALLTRIM(dadospsicoaux.u_cputdisp)>>',
					'<<ALLTRIM(dadospsicoaux.u_nmutavi)>>', '<<ALLTRIM(dadospsicoaux.u_moutavi)>>', '<<ALLTRIM(dadospsicoaux.u_cputavi)>>',
					'<<ALLTRIM(dadospsicoaux.u_ndutavi)>>', '<<IIF(empty(dadospsicoaux.u_ddutavi),'19000101',uf_gerais_getDate(dadospsicoaux.u_ddutavi,"SQL"))>>', <<dadospsicoaux.u_idutavi>>,
					'<<ALLTRIM(uCrsE1.u_dirtec)>>', '<<IIF(empty(dadospsicoaux.nascimento),'19000101',uf_gerais_getDate(dadospsicoaux.nascimento,"SQL"))>>'
					)
			ENDTEXT
		ELSE
			LOCAL LcStampFtPsico
			SELECT ft
			LcStampFtPsico = ALLTRIM(ft.ftstamp)
			
			SELECT uCrsatendCl
			SELECT uCrsCabVendas
			TEXT TO lcSQL noshow textmerge
			
				delete from B_dadosPsico where ftstamp='<<ALLTRIM(LcStampFtPsico)>>'
				insert into B_dadosPsico (
					ftstamp,
					u_recdata, u_medico,
					u_nmutdisp, u_moutdisp, u_cputdisp,
					u_nmutavi, u_moutavi, u_cputavi,
					u_ndutavi, u_ddutavi, u_idutavi,
					u_dirtec, nascimento
				)
				Values (
					'<<LcStampFtPsico>>',
					'<<IIF(empty(uCrsCabVendas.datareceita),'19000101',uf_gerais_getDate(uCrsCabVendas.datareceita,"SQL"))>>', '<<ALLTRIM(uCrsCabVendas.nopresc)>>',
					'<<ALLTRIM(uCrsatendCl.nome)>>', '<<ALLTRIM(uCrsatendCl.morada)>>', '<<ALLTRIM(uCrsatendCl.codpost)>>',
					'<<ALLTRIM(uCrsatendCl.u_nmutavi)>>', '<<ALLTRIM(uCrsatendCl.u_moutavi)>>', '<<ALLTRIM(uCrsatendCl.u_cputavi)>>',
					'<<ALLTRIM(uCrsatendCl.u_ndutavi)>>', '<<IIF(empty(uCrsatendCl.u_ddutavi),'19000101',uf_gerais_getDate(uCrsatendCl.u_ddutavi,"SQL"))>>', <<uCrsatendCl.u_idutavi>>,
					'<<ALLTRIM(uCrsE1.u_dirtec)>>', '<<IIF(empty(dadospsicoaux.nascimento),'19000101',uf_gerais_getDate(dadospsicoaux.nascimento,"SQL"))>>'
					)
			ENDTEXT
		ENDIF 
		IF !uf_gerais_actGrelha("", "", lcSQL)
			uf_perguntalt_chama("ERRO A INSERIR OS DADOS DOS PSICOTR�PICOS NAS RECEITAS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		ENDIF
	ENDIF 
	
	SELECT dadospsicoaux
	DELETE ALL 
	
	myFtIntroducao = .f.
	
	
	**imprimir
	**se existir necessidade comentar este bloco todo e descomentar o seguinte


	&& Cria cursor para posteriormente permitir a impress�o dos tal�es
	IF !USED("uCrsImpTalaoPos")
		create cursor uCrsImpTalaoPos (stamp c(26), tipo i(4), lordem i(8), susp l, AdiReserva l, nrreceita c(26) ,rm l, planocomp l, impcomp1 l, impcomp2 l, lote c(5), impresso L , plano c(5))
	ENDIF 

	Select uCrsImpTalaoPos
	Append Blank
	replace uCrsImpTalaoPos.stamp			WITH Alltrim(ft.ftstamp)
	replace uCrsImpTalaoPos.tipo			WITH ft.ndoc
	replace uCrsImpTalaoPos.lordem			WITH uCrsCabVendas.lordem
	IF Right(alltrim(uCrsCabVendas.design),10) == "*SUSPENSA*" Or Right(Alltrim(uCrsCabVendas.design),18)=="*SUSPENSA RESERVA*"
		replace uCrsImpTalaoPos.susp		WITH .T.
	ELSE 
		replace uCrsImpTalaoPos.susp		WITH .F.
	ENDIF 
	IF !EMPTY(uCrsCabVendas.TemAdiantamento)
		replace uCrsImpTalaoPos.AdiReserva	WITH .T. 
	ELSE
		replace uCrsImpTalaoPos.AdiReserva	WITH .F. 		
	ENDIF
	replace uCrsImpTalaoPos.planocomp 		WITH uCrsCabVendas.planocomplem
	IF ALLTRIM(uCrsCabVendas.receita_tipo)=='RM' 
		replace uCrsImpTalaoPos.rm WITH .t.
	ELSE
		replace uCrsImpTalaoPos.rm WITH .f.
	ENDIF 
	replace uCrsImpTalaoPos.impcomp1 WITH .f.
	replace uCrsImpTalaoPos.impcomp2 WITH .f.
	replace uCrsImpTalaoPos.nrreceita WITH substr(uCrsCabVendas.design, at('Receita',uCrsCabVendas.design )+8,at(' - ',uCrsCabVendas.design )- (at('Receita',uCrsCabVendas.design )+8))
					
	
*!*	    IF !EMPTY(ALLTRIM(lcCod))
*!*	    	IF(ALLTRIM(lcTipoReceita) != 'RSP')
*!*	        	uf_imprimirpos_imprec(Alltrim(Ft.Ftstamp),1,"REIMP")
*!*	        ELSE
*!*	            		
*!*	        	IF(lcCod!="01") and (lcCod!="41") and (lcCod!="45") and (lcCod!="45A") ;
*!*	        			and (lcCod!="45B") and (lcCod!="46") and (lcCod!="46A");
*!*						and (lcCod!="47") and (lcCod!="48") and (lcCod!="48A") ;
*!*						and (lcCod!="CE") and (lcCod!="DO") and (lcCod!="DS") and (lcCod!="LA") &&se for SNS e RSP nao imprime
*!*	        		uf_imprimirpos_imprec(Alltrim(Ft.Ftstamp),1,"REIMP")
*!*	        	ENDIF
*!*	 	
*!*	        ENDIF
*!*	        
*!*	    ENDIF
*!*	    
*!*	    
*!*	    IF !EMPTY(ALLTRIM(lcCod2))
*!*	    	IF(lcTipoReceita != 'RSP')
*!*		        uf_imprimirpos_imprec(Alltrim(Ft.Ftstamp),2,"REIMP")
*!*		       ** SELECT FT
*!*		       ** uf_imprimirpos_impRec(Alltrim(Ft.Ftstamp), 1, "REIMP") 
*!*		    ELSE
*!*		    	IF(ALLTRIM(lcCod2)!="01" ) and (lcCod2!="41") and (lcCod2!="45") and (lcCod2!="45A") ;
*!*	        			and (lcCod2!="45B") and (lcCod2!="46") and (lcCod2!="46A");
*!*						and (lcCod2!="47") and (lcCod2!="48") and (lcCod2!="48A") ;
*!*						and (lcCod2!="CE") and (lcCod2!="DO") and (lcCod2!="DS") and (lcCod2!="LA") &&se for SNS e RSP nao imprime
*!*	   		    	uf_gerais_actGrelha("","T","SELECT convert(varchar(3),tlote) as tlote FROM ctltrct (nolock) WHERE ctltrctstamp=(select u_ltstamp from ft (nolock) where ftstamp='"+ALLTRIM(uCrsFt.ftstamp)+"')")
*!*	   		    	uf_gerais_actGrelha("","T2","SELECT convert(varchar(3),tlote) as tlote FROM ctltrct (nolock) WHERE ctltrctstamp=(select u_ltstamp2 from ft (nolock) where ftstamp='"+ALLTRIM(uCrsFt.ftstamp)+"')")

*!*			    **	IF ALLTRIM(T.tlote)<>'96' AND ALLTRIM(T.tlote)<>'97'

*!*	    	    		uf_imprimirpos_imprec(Alltrim(Ft.Ftstamp),2,"REIMP")
*!*		    	    	SELECT FT
*!*		        		uf_imprimirpos_impRec(Alltrim(Ft.Ftstamp), 1, "REIMP") 
*!*		        	
*!*		        **	ENDIF 
*!*	        	ENDIF  
*!*	        ENDIF
*!*	                                     
*!*	    ENDIF
    ***
    

    
    
    **imprimir - antigo
*!*	    IF !EMPTY(ALLTRIM(lcCod)) AND lcTipoReceita != 'RSP'
*!*	        uf_imprimirpos_imprec(Alltrim(Ft.Ftstamp),1,"REIMP")
*!*	    ENDIF
*!*	    IF !EMPTY(ALLTRIM(lcCod2)) AND lcTipoReceita != 'RSP'
*!*	        uf_imprimirpos_imprec(Alltrim(Ft.Ftstamp),2,"REIMP")
*!*	        SELECT FT
*!*	        uf_imprimirpos_impRec(Alltrim(Ft.Ftstamp), 1, "REIMP")                                
*!*	    ENDIF


		

	

	
	fecha("ucrsFI")
	fecha("ucrsFT")
	fecha("ucrsFT2")
	
	
	fecha("uCrsCodPla")
	
	IF USED("FIinsercaoReceita")
		SELECT crsFItmp
		SCAN
			DELETE FOR ALLTRIM(FIinsercaoReceita.fistamp) == ALLTRIM(crsFItmp.fistamp) IN FIinsercaoReceita
		ENDSCAN
		
		SELECT uCrsRec
		DELETE FOR ALLTRIM(FIinsercaoReceita.fistamp) == ALLTRIM(uCrsRec.stamp) IN FIinsercaoReceita
	ENDIF 		
	IF USED("uCrsRec")
		SELECT uCrsRec
		DELETE
	ENDIF 
	
	** Validar receita nas reservas
**	IF !EMPTY(TokenReserva)
	SELECT uCrsTokenReserva 
	
	GO TOP 
	SCAN 
		IF !EMPTY(uCrsTokenReserva.TokenReserva)
			lcSQL = ''
			TEXT TO lcSQL NOSHOW TEXTMERGE 
				exec up_dem_tipoLote '<<ALLTRIM(uCrsTokenReserva.TokenReserva)>>'
			ENDTEXT
			IF !uf_gerais_actGrelha("", "uCrsValidacaoReceita", lcSQL)
				uf_perguntalt_chama("N�o foi poss�vel verificar o lote a que ficou atribuida a receita. Por favor contacte o suporte.", "", "OK", 32)
				RETURN .f.
			ELSE
			**	_cliptext=ALLTRIM(uCrsTokenReserva.TokenReserva)
			**	MESSAGEBOX(uCrsTokenReserva.TokenReserva)
			**	MESSAGEBOX(uCrsValidacaoReceita.tipoLote)
				IF ALLTRIM(uCrsValidacaoReceita.tipoLote) = '96' OR ALLTRIM(uCrsValidacaoReceita.tipoLote) = '98'
					ValReservaErro = .t.
				ENDIF
			ENDIF
		ENDIF 
		SELECT uCrsTokenReserva 
	ENDSCAN 
**	ENDIF 
**	TokenReserva=''
**	MESSAGEBOX(lcReservasErro)
**	IF lcReservasErro = .t.
**		uf_perguntalt_chama("EXISTEM RECEITAS QUE FICARAM COLOCADAS EM LOTES COM ERRO. POR FAVOR VERIFIQUE AS RECEITAS DISPENSADAS.","OK","",48)
**	ENDIF 
**	lcReservasErro = .f.
	SELECT uCrsTokenReserva 
	DELETE ALL

	recreserva.Timer2.Enabled=.t.
	recreserva.refresh()
	
	uf_recreserva_restauraCursores()
	
	atendimento.lockscreen = .f.

*!*		SELECT uCrsRec
*!*		GO TOP
*!*		COUNT TO nActive

*!*		IF nActive == 0
*!*			uf_RECRESERVA_sair()
*!*		ENDIF 
	
	
ENDFUNC


**
FUNCTION uf_recreserva_restauraCursores
	SELECT FI
	DELETE ALL
	APPEND FROM DBF("crsFICopia")
	
	
	SELECT FI2
	DELETE ALL
	APPEND FROM DBF("crsFI2Copia")
	
	SELECT FT
	DELETE ALL
	APPEND FROM DBF("crsFTCopia")
	
	SELECT FT2
	DELETE ALL
	APPEND FROM DBF("crsFT2Copia")
	
	
	SELECT ucrsAtendST
	DELETE ALL
	APPEND FROM DBF("crsAtendStCopia")
	
	
ENDFUNC 


************************************************
** leitura de c�digos de barras com o scanner **
************************************************
FUNCTION uf_recreserva_scanner
	
	recreserva.txtscanner.Value = strtran(recreserva.txtscanner.Value, chr(39), '')
	
	&& valida preenchimento
	If Empty(alltrim(recreserva.txtscanner.value))
		RETURN .F.
	ENDIF
	
	Local lcRef
	Local lcValida
	Store .f. To lcValida
	
	lcRef = UPPER(alltrim(recreserva.txtscanner.value))

	DO CASE
*************************
** Local de Prescri��o **
*************************
		CASE Left(lcRef,1) == 'U' Or Left(lcRef,1) == 'M'
			If Upper(Left(lcRef,1))=='U'
				recreserva.txtLocalPresc.value = LEFT(Alltrim(lcRef),25)
			Else
				recreserva.txtMedicoPresc.value = LEFT(Alltrim(lcRef),25)
			ENDIF
			
			lcValida = .t.
			
******************
** Nr de benef. **			
******************
		CASE (Len(lcRef) > 8) And (Len(lcRef) < 13)
			IF EMPTY(ALLTRIM(recreserva.txtBenef.value))
				recreserva.txtBenef.value = LEFT(Alltrim(lcRef),15)
			ELSE 
				recreserva.txtBenef2.value = LEFT(Alltrim(lcRef),15)
			ENDIF
			
			lcValida = .t.

*************************
** Produto e n� receita**
*************************
		CASE (Len(lcRef) >= 13) And (Len(lcRef) <= 19)
			SELECT FIinsercaoReceita
			REPLACE FIinsercaoReceita.design WITH StrExtract(FIinsercaoReceita.design, '.', ':', 1, 4) + LEFT(Alltrim(lcRef),19) + " -" + StrExtract(FIinsercaoReceita.design, ' -', '', 1, 2)

			SELECT uCrsRec
			REPLACE uCrsRec.venda WITH StrExtract(uCrsRec.venda, '.', ':', 1, 4) + LEFT(Alltrim(lcRef),19) + " -" + StrExtract(uCrsRec.venda, ' -', '', 1, 2)
			recreserva.txtNumReceita.value = LEFT(Alltrim(lcRef),19)
			
			lcValida = .t.
	ENDCASE
	
	&& limpa valor
	recreserva.txtscanner.value = ""
	
	IF !lcValida
		uf_perguntalt_chama("Leitura de Scanner Inv�lida.","OK","",64)
	ENDIF
ENDFUNC

****************************************
** fecha o painel e liberta variaveis **
****************************************
FUNCTION uf_RECRESERVA_sair


	IF USED("uCrsRec")
		SELECT uCrsRec
		COUNT TO nActive
		IF nActive > 0 &&AND !(TYPE("PAGAMENTO") == "U")
			uf_perguntalt_chama("DEVE IMPRIMIR TODAS AS RECEITAS ANTES DE CONTINUAR.","","OK")
			RETURN .f.
		ENDIF
		IF USED("uCrsRec")
			fecha("uCrsRec")
		ENDIF 
		IF USED("FIinsercaoReceita")
			fecha("FIinsercaoReceita")
		ENDIF 
	ENDIF 
	
	fecha("crsAtendStCopia")
	
	IF USED("crsFICopia")
		fecha("crsFICopia")
	ENDIF 
	
	IF USED("crsFI2Copia")
		fecha("crsFI2Copia")
	ENDIF 
	
	
	IF USED("crsFTCopia")
		fecha("crsFTCopia")
	ENDIF 
	
		
	IF USED("crsFT2Copia")
		fecha("crsFT2Copia")
	ENDIF 

	
	IF ValReservaErro = .t.
		uf_perguntalt_chama("EXISTEM RECEITAS QUE FICARAM COLOCADAS EM LOTES COM ERRO. POR FAVOR VERIFIQUE AS RECEITAS DISPENSADAS.","OK","",48)
	ENDIF 
	ValReservaErro = .f.
	
	** IMPPRESS�O DE N�O DISPENSADOS
	
	select * from uCrsDEMndisp with (buffering=.t.) into cursor uCrsDEMndispAux readwrite 
	
	SELECT uCrsDEMndisp
	GO TOP 
	DELETE FOR alltrim(uCrsDEMndisp.receita) != ALLTRIM(recreserva.txtNumReceita.value)
	
	SELECT uCrsDEMndisp
	GO TOP 
	COUNT FOR !EMPTY(uCrsDEMndisp.receita) TO lcNoNDisp
	
	**IF uf_gerais_getParameter('ADM0000000285','BOOL') AND lcNoNDisp>0
	**	IF uf_perguntalt_chama("DESEJA IMPRIMIR OS MEDICAMENTOS POR DISPENSAR DA(S) GUIA(S) DE TRATAMENTO?","Sim","N�o")
	**		uf_imprimir_nao_dispensados()
	**	ENDIF 
	**ENDIF 
	
	
	PUBLIC myEscolha
	** IMPRESS�O DE N�O DISPENSADOS
	IF uf_gerais_getParameter('ADM0000000285','BOOL') AND lcNoNDisp>0
		IF uf_perguntalt_chama("DESEJA IMPRIMIR/ENVIAR OS MEDICAMENTOS POR DISPENSAR DA(S) GUIA(S) DE TRATAMENTO?","Sim","N�o")
			uf_valorescombo_chama("myEscolha", 0, "select sel = convert(bit,0),'IMPRIMIR' as tipo union all select sel = convert(bit,0),'EMAIL' as tipo union all select sel = convert(bit,0),'SMS' as tipo", 1, "tipo", "tipo")
			IF !EMPTY(myEscolha)
					DO CASE
						CASE ALLTRIM(myEscolha) = "IMPRIMIR"
							uf_imprimir_nao_dispensados()
						CASE ALLTRIM(myEscolha) = "EMAIL"
							IF ucrse1.usacoms=.t. 
								uf_EMAIL_nao_dispensados()
							ELSE
								uf_perguntalt_chama("PARA UTILIZAR ESTA FUNCIONALIDADE DEVER� ADQUIRIR O M�DULO DE COMUNICA��ES." + CHR(13) + "PARA MAIS INFORMA��ES POR FAVOR CONTACTE A LOGITOOLS (OP��O - COMERCIAL).", "OK","",16)
							ENDIF 
						CASE ALLTRIM(myEscolha) = "SMS"
							IF ucrse1.usacoms=.t. 
								uf_SMS_nao_dispensados()
							ELSE
								uf_perguntalt_chama("PARA UTILIZAR ESTA FUNCIONALIDADE DEVER� ADQUIRIR O M�DULO DE COMUNICA��ES." + CHR(13) + "PARA MAIS INFORMA��ES POR FAVOR CONTACTE A LOGITOOLS (OP��O - COMERCIAL).", "OK","",16)
							ENDIF 
						OTHERWISE
					ENDCASE 
	*!*			IF uf_perguntalt_chama("DESEJA IMPRIMIR OS MEDICAMENTOS POR DISPENSAR DA(S) GUIA(S) DE TRATAMENTO?","Sim","N�o")
	*!*				uf_imprimir_nao_dispensados()
	*!*			ENDIF
			ENDIF 
		ENDIF 
	ENDIF 
	

	** Limpa o atendimento se n�o houverem mais linhas
	SELECT fi
	LOCATE FOR !EMPTY(fi.ref)
	IF !FOUND()
		uf_atendimento_limpar()	
	ENDIF 

	
	&& fecha painel
	RECRESERVA.hide()	
	RECRESERVA.release()
	
ENDFUNC