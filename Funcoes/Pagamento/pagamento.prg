Do imprimirpos
Do recreserva
Do contagem
Do consentimento
Do maqdinheiro
Do pagexterno

** Abrir painel de pagamento
Function uf_PAGAMENTO_chama
	&& vari�veis publicas do painel de pagamento
	Public pTalSep, pFimAtendimento, myInvoiceData, lcNrVenda, lcNrRecibo, pPagPost, lcProdJaPagos, valpagmoeda, moedapag, tmrclock1, StampProcNC, lcNratendPrint, lcAnoPrint, lcNoClPrint, myImprimeA4
	Store '19000101' To myInvoiceData
	Store .F. To pFimAtendimento
	Store .F. To pPagPost, lcProdJaPagos
	Store 0.00 To valpagmoeda
	Store '' To moedapag, StampProcNC, lcNratendPrint
	Store 0 To tmrclock1
	Store Alltrim(Str(Year(Date()))) To lcAnoPrint
	**



	uf_PAGAMENTO_prepararAbertura()

	&& Controla Abertura do Painel
	If Type("PAGAMENTO")=="U"
		Do Form PAGAMENTO
	Else
		PAGAMENTO.Show
	Endif

Endfunc


** Fechar o ecr� de pagamento
Function uf_PAGAMENTO_sair
	Lparameters lcModoS

	If !uf_gerais_getParameter("ADM0000000118","BOOL")
		Local lcValidaRec
		Store .F. To lcValidaRec
		Select uCrsCabVendas
		Go Top
		Locate For uCrsCabVendas.sel == .F.
		If Found()
			lcValidaRec = .T.
		Endif
		If lcValidaRec
			uf_perguntalt_chama("N�o pode sair do Pagamento sem imprimir todas as receitas.","OK","",64)
			Return .F.
		Endif

		If Used("uCrsCabVendas")
			fecha("uCrsCabVendas")
		Endif
	Endif

	If pFimAtendimento
		uf_PAGAMENTO_limparVendasFimVd()
	Endif

	Release pFimAtendimento
	Release MyValidaEsperaImp
	Release MyImprimeTalao
	PrecisaRecibo = .F.
	avisos=.F.
	If Type("infovenda") != "U"
		uf_infovenda_sair()
		uf_infoVenda_exit()
	Endif
	**Wait Window "A fechar" TIMEOUT 1

	** Regista o cancelamento do pagamento
	If uf_gerais_getParameter_site('ADM0000000021','BOOL') And Empty(lcModoS)
		Local lcStampFtAtend
		Select ft
		lcStampFtAtend = Alltrim(ft.ftstamp)

		lcSQL = ""
		TEXT TO lcSQL NOSHOW TEXTMERGE
			insert into cancel_pagamento (utilizador, userno ,data,valor,nome,no,estab, loja, terminal, stamp)
			values ('<<ALLTRIM(M.CH_VENDNM)>>', <<ch_userno>> ,GETDATE(),<<pagamento.txtValAtend.value>>,'<<ALLTRIM(uCrsAtendCL.nome)>>',<<uCrsAtendCL.no>>,<<uCrsAtendCL.estab>>, '<<ALLTRIM(mysite)>>', '<<ALLTRIM(myTerm)>>', '<<ALLTRIM(lcStampFtAtend)>>')
		ENDTEXT

		Select fi
		Go Top
		Select * From fi Where !Empty(fi.ref) Into Cursor cur_filinapag
		Select cur_filinapag
		Scan
			lcSQL1 = ""
			TEXT TO lcSQL1 NOSHOW TEXTMERGE
				insert into cancel_pagamento_prod(stamp, ref, design, qtt)
				values
				('<<ALLTRIM(lcStampFtAtend)>>', '<<ALLTRIM(cur_filinapag.ref)>>', '<<ALLTRIM(cur_filinapag.design)>>', <<cur_filinapag.qtt>>)
			ENDTEXT
			lcSQL = lcSQL + lcSQL1
			Select cur_filinapag
		Endscan
		fecha("cur_filinapag")
		uf_gerais_actGrelha("","",lcSQL)

	Endif

	PAGAMENTO.Release
	Release PAGAMENTO

	If Type("ATENDIMENTO")!="U"
		Do Case
			Case FTSeguradora = 'IMPRIME'
				uf_atendimento_sair()
				uf_facturacao_chama(Alltrim(StampFTSeguradora))
				uf_imprimirgerais_Chama('FACTURACAO')
			Case FTSeguradora = 'GUARDA'
				uf_atendimento_sair()
				uf_facturacao_chama(Alltrim(StampFTSeguradora))
				uf_imprimirgerais_Chama('FACTURACAO')
				IMPRIMIRGERAIS.pageframe1.page1.chkParaPDF.Value = 1
				uf_imprimirGerais_gravar()
				uf_imprimirGerais_sair()
				uf_facturacao_sair()
			Case FTSeguradora = 'NCSEGIMPRIME'
				uf_atendimento_sair()
				uf_facturacao_chama(Alltrim(StampFTSeguradora))
				uf_imprimirgerais_Chama('FACTURACAO')
			Otherwise
				ATENDIMENTO.Show
		Endcase
	Endif
Endfunc


** valida se o dcumento foi criado com sucesso
Function  uf_pagamento_validaDocumentoEnviaEmail
	Lparameters lcFtstamp, lcFtNo, lcFtSerie, lcSqlFinal, nrAtendimento

	Local lcSQL, lcNrAtendFinal
	lcNrAtendFinal = ""

	If(Empty(Alltrim(lcFtstamp)))
		Return .F.
	Endif

	**testar envio sql erro doc em falta
	&&lcFtstamp = lcFtstamp + "__"


	If Type("nrAtendimento") != 'U'
		lcNrAtendFinal=Alltrim(nrAtendimento)
	Endif



	lcSQL = ''
	TEXT TO lcSQL NOSHOW TEXTMERGE
		SELECT count(ftstamp) as conta FROM FT(nolock) where ftstamp='<<ALLTRIM(lcFtstamp)>>'
	ENDTEXT
	If !uf_gerais_actGrelha("","uCrsTempValidaFt",lcSQL)
		uf_perguntalt_chama("O software n�o conseguiu criar o documento. Por favor contacte o suporte.","OK","",16)
		lcValidaDoc = .T.
	Else

		Select uCrsTempValidaFt
		If uCrsTempValidaFt.Conta == 0
			Local lcMensagem
			lcMensagem = 'Desaparecimento efectivo de documentos'
			lcMensagem = lcMensagem  + Chr(13) + 'Cliente: '                              + Alltrim(ucrse1.nomecomp)
			lcMensagem = lcMensagem  + Chr(13) + 'Loja: '                                 + Alltrim(ucrse1.siteloja)
			lcMensagem = lcMensagem  + Chr(13) + 'Stamp do documento: '                   + lcFtstamp
			lcMensagem = lcMensagem  + Chr(13) + 'N�mero do documento: '                  + Str(lcFtNo)
			lcMensagem = lcMensagem  + Chr(13) + 'Serie do documento: '                   + Str(lcFtSerie)
			lcMensagem = lcMensagem  + Chr(13) + 'nrAtend: '                              + lcNrAtendFinal
			lcMensagem = lcMensagem  + Chr(13) + 'Verificar de imediato o que se passa!'
			If(!Empty(lcSqlFinal))
				lcMensagem = lcMensagem + Chr(13) + Chr(13) + Chr(13) + 'SQL; ' + Alltrim(lcSqlFinal)
			Endif
			If(!Empty(lcSqlFinal))
				uf_gerais_registaerrolog(Alltrim(lcSqlFinal),"DOCFALTA: " +  Str(lcFtSerie)  + "-" + Str(lcFtNo))
			Endif
			uf_gerais_registaerrolog('DESAPARECIMENTO DOCUMENTOS - Email',"DOCFALTA: " +  Str(lcFtSerie)  + "-" + Str(lcFtNo),"","","","","","",Alltrim(lcMensagem ))

			uf_startup_sendmail_errors('DESAPARECIMENTO DOCUMENTOS', lcMensagem )

			uf_perguntalt_chama("O software n�o conseguiu criar o documento. Por favor verifique.","OK","",16)
		Endif
	Endif

	fecha("uCrsTempValidaFt")

Endfunc





**
Function uf_pagamento_InactivaObjectosNegativo
	If PAGAMENTO.txtValAtend.Value = 0
		PAGAMENTO.txtDinheiro.Enabled = .F.
		PAGAMENTO.txtMB.Enabled = .F.
		PAGAMENTO.txtVisa.Enabled = .F.
		PAGAMENTO.txtCheque.Enabled = .F.
		PAGAMENTO.txtModPag3.Enabled = .F.
		PAGAMENTO.txtModPag4.Enabled = .F.
		PAGAMENTO.txtModPag5.Enabled = .F.
		PAGAMENTO.txtModPag6.Enabled = .F.
		PAGAMENTO.btnDinheiro.Enable(.F.)
		PAGAMENTO.btnMB.Enable(.F.)
		PAGAMENTO.btnVisa.Enable(.F.)
		PAGAMENTO.btnCheque.Enable(.F.)
		PAGAMENTO.btnModPag3.Enable(.F.)
		PAGAMENTO.btnModPag4.Enable(.F.)
		PAGAMENTO.btnModPag5.Enable(.F.)
		PAGAMENTO.btnModPag6.Enable(.F.)

		If !myPagCentral Or  uf_gerais_validaPagCentralDinheiro()
			If(uf_pagamento_validaPagCentralTerminal())
				PAGAMENTO.txtDinheiro.Enabled = .T.
				PAGAMENTO.txtModPag5.Enabled = .T.
				PAGAMENTO.btnModPag5.Enable(.T.)
				PAGAMENTO.btnDinheiro.Enable(.T.)
				PAGAMENTO.txtModPag5.Value = astr(PAGAMENTO.txtValAtend.Value,15,2)
			Else
				PAGAMENTO.txtDinheiro.Value = astr(PAGAMENTO.txtValAtend.Value,15,2)
			Endif
		Endif
	Else
		PAGAMENTO.txtDinheiro.Enabled = .T.
		PAGAMENTO.txtDinheiro.Init()

		PAGAMENTO.txtMB.Enabled = .T.
		PAGAMENTO.txtMB.Init()

		PAGAMENTO.txtVisa.Enabled = .T.
		PAGAMENTO.txtVisa.Init()

		PAGAMENTO.txtCheque.Enabled = .T.
		PAGAMENTO.txtCheque.Init()

		PAGAMENTO.txtModPag3.Enabled = .T.
		PAGAMENTO.txtModPag3.Init()

		PAGAMENTO.txtModPag4.Enabled = .T.
		PAGAMENTO.txtModPag4.Init()

		PAGAMENTO.txtModPag5.Enabled = .T.
		PAGAMENTO.txtModPag5.Init()

		PAGAMENTO.txtModPag6.Enabled = .T.
		PAGAMENTO.txtModPag6.Init()

		PAGAMENTO.btnDinheiro.Enable(.T.)
		PAGAMENTO.btnMB.Enable(.T.)
		PAGAMENTO.btnVisa.Enable(.T.)
		PAGAMENTO.btnCheque.Enable(.T.)
		PAGAMENTO.btnModPag3.Enable(.T.)
		PAGAMENTO.btnModPag4.Enable(.T.)
		PAGAMENTO.btnModPag5.Enable(.T.)
		PAGAMENTO.btnModPag6.Enable(.T.)

	Endif
Endfunc


****************
* Abrir Gaveta *
****************
Function uf_PAGAMENTO_abreGaveta
	Lparameters lcAtendimento

	If !uf_gerais_getUmValor("B_terminal", "gaveta", "terminal = '" + Alltrim(myTerm) + "'")
		Return .F.
	Endif

	uf_gerais_setImpressoraPOS(.T.)
	uf_gerais_openDrawPOS()
	uf_gerais_setImpressoraPOS(.F.)
	If lcAtendimento == .F.
		PAGAMENTO.txtDinheiro.SetFocus()
	Endif
Endfunc


**************************************
* mostrar dados de venda a dom�cilio *
**************************************
Function uf_PAGAMENTO_vdDom
	PAGAMENTO.lblTitulo.Caption = "Dados para Venda ao Domic�lio"
	PAGAMENTO.pgFrVDP.ActivePage = 2
Endfunc


*****************************************
* mostrar dados de atendimento/receitas *
*****************************************
Function uf_PAGAMENTO_atendimento
	PAGAMENTO.lblTitulo.Caption = "Vendas do Utente / Entidade de Factura��o"
	PAGAMENTO.pgFrVDP.ActivePage = 1
Endfunc


****************************************************
* Importar dados do cliente para o cursor de psico *
****************************************************
Function uf_pagamento_importarDadosPsicoCL
	Local lcValida, lcValida2, lcIdade
	Store .F. To lcValida, lcValida2

	Select dadosPsico
	If Reccount("dadosPsico") > 1
		lcValida = .T.
	Endif
	If lcValida
		If uf_perguntalt_chama("PRETENDE IMPORTAR OS DADOS PARA TODO O ATENDIMENTO?"+Chr(13)+Chr(13)+"Se [n�o], s� ser�o importados para esta venda.","Sim","N�o")
			lcValida2 = .T.
		Endif
	Endif

	lcIdade = 0
	If uf_gerais_getDate(uCrsatendCl.nascimento,"SQL") != '19000101'
		lcIdade = Str(uf_gerais_getIdade(uCrsatendCl.nascimento))
	Else
		lcIdade = Str(lcIdade)
	Endif

	If lcValida2
		Select dadosPsico
		Go Top
		Scan
			Replace dadosPsico.u_cputavi With uCrsatendCl.codpost
			Replace dadosPsico.u_cputdisp With uCrsatendCl.codpost
			Replace dadosPsico.u_idutavi With Val(lcIdade)
			Replace dadosPsico.u_moutavi With uCrsatendCl.morada
			Replace dadosPsico.u_moutdisp With uCrsatendCl.morada
			Replace dadosPsico.u_ndutavi With uCrsatendCl.bino
			Replace dadosPsico.u_nmutavi With uCrsatendCl.Nome
			Replace dadosPsico.u_nmutdisp With uCrsatendCl.Nome
		Endscan
	Else
		Select dadosPsico
		Replace dadosPsico.u_cputavi With uCrsatendCl.codpost
		Replace dadosPsico.u_cputdisp With uCrsatendCl.codpost
		Replace dadosPsico.u_idutavi With Val(lcIdade)
		Replace dadosPsico.u_moutavi With uCrsatendCl.morada
		Replace dadosPsico.u_moutdisp With uCrsatendCl.morada
		Replace dadosPsico.u_ndutavi With uCrsatendCl.bino
		Replace dadosPsico.u_nmutavi With uCrsatendCl.Nome
		Replace dadosPsico.u_nmutdisp With uCrsatendCl.Nome
	Endif

	Select uCrsCabVendas
	Select dadosPsico
	Locate For dadosPsico.cabVendasOrdem == uCrsCabVendas.Lordem

	PAGAMENTO.pgFrVDP.page3.Refresh
Endfunc


**********************************
* mostrar dados de psicotr�picos *
**********************************
Function uf_PAGAMENTO_psico
	Local ordem, validaOrdem, validaVenda, validaPsico
	Store 0 To ordem
	Store .F. To validaOrdem, validaVenda, validaPsico

	Select fi
	Scan For fi.Design = uCrsCabVendas.Design And fi.Lordem = uCrsCabVendas.Lordem
		ordem = Recno("fi")
	Endscan

	Select fi
	Scan
		If !validaOrdem
			Try
				Go ordem
			Catch
			Endtry
			validaOrdem = .T.
		Endif
		If Left(fi.Design,1) = "." And Empty(Alltrim(fi.ref))
			If !validaVenda
				validaVenda = .T.
			Else
				Exit
			Endif
		Else
			If fi.u_psico
				validaPsico = .T.
				Exit
			Endif
		Endif
	Endscan

	If validaPsico
		&& criar linha no cursor DadosPsico
		Select  dadosPsico
		Locate For dadosPsico.cabVendasOrdem == uCrsCabVendas.Lordem

		PAGAMENTO.lblTitulo.Caption = "Dados para Psicotr�picos"
		PAGAMENTO.pgFrVDP.ActivePage = 3
		PAGAMENTO.pgFrVDP.page3.Refresh

		&& validar se existe receita
		Select uCrsCabVendas
		If Left(Alltrim(uCrsCabVendas.Design),2)==".S" Or Right(Alltrim(uCrsCabVendas.Design),10)=="*SUSPENSA*" Or Right(Alltrim(uCrsCabVendas.Design),18)=="*SUSPENSA RESERVA*"
			PAGAMENTO.pgFrVDP.page3.txtReceita.Enabled=.F.
			PAGAMENTO.pgFrVDP.page3.txtDataRec.Enabled=.F.
		Else
			PAGAMENTO.pgFrVDP.page3.txtReceita.Value = Strextract(uCrsCabVendas.Design, ':', ' -', 1, 0)
			PAGAMENTO.pgFrVDP.page3.txtReceita.Enabled = .T.
			PAGAMENTO.pgFrVDP.page3.txtDataRec.Enabled = .T.
		Endif
	Else
		uf_perguntalt_chama("A RECEITA SELECCIONADA N�O CONT�M PSICOTR�PICOS.","OK","",64)
	Endif
Endfunc


*************************************
* Gravar Dados no Cursor DadosPsico *
*************************************
Function uf_PAGAMENTO_gravarPsicoCursor()
	Select uCrsCabVendas
	If !(Left(Alltrim(uCrsCabVendas.Design),2)==".S" Or Right(Alltrim(uCrsCabVendas.Design),10)=="*SUSPENSA*") Or Right(Alltrim(uCrsCabVendas.Design),18)=="*SUSPENSA RESERVA*"
		Replace uCrsCabVendas.Design With Strextract(uCrsCabVendas.Design, '.', ':', 1, 4) + Alltrim(PAGAMENTO.pgFrVDP.page3.txtReceita.Value) + " -" + Strextract(uCrsCabVendas.Design, ' -', '', 1, 2)
		Select fi
		Locate For fi.Lordem = uCrsCabVendas.Lordem
		Replace fi.Design With Strextract(uCrsCabVendas.Design, '.', ':', 1, 4) + Alltrim(PAGAMENTO.pgFrVDP.page3.txtReceita.Value) + " -" + Strextract(uCrsCabVendas.Design, ' -', '', 1, 2)
		PAGAMENTO.pgFrVDP.page1.grdVendas.Refresh
	Endif
Endfunc


********************************************
* inactivar objectos caso use Pag. Central *
********************************************
Function uf_PAGAMENTO_verificarPagCentral
	If myPagCentral And !uf_gerais_validaPagCentralDinheiro()
		PAGAMENTO.txtDinheiro.ReadOnly	= .T.
		PAGAMENTO.txtMB.ReadOnly		= .T.
		PAGAMENTO.txtVisa.ReadOnly		= .T.
		PAGAMENTO.txtCheque.ReadOnly	= .T.
		PAGAMENTO.txtModPag3.ReadOnly	= .T.
		PAGAMENTO.txtModPag4.ReadOnly	= .T.
		PAGAMENTO.txtModPag5.ReadOnly	= .T.
		PAGAMENTO.txtModPag6.ReadOnly	= .T.

		PAGAMENTO.btnDinheiro.Enable(.F.)
		PAGAMENTO.btnMB.Enable(.F.)
		PAGAMENTO.btnVisa.Enable(.F.)
		PAGAMENTO.btnCheque.Enable(.F.)
		PAGAMENTO.btnModPag3.Enable(.F.)
		PAGAMENTO.btnModPag4.Enable(.F.)
		PAGAMENTO.btnModPag5.Enable(.F.)
		PAGAMENTO.btnModPag6.Enable(.F.)
	Endif
Endfunc


*****************************************************************************************
* verifica se o p�rametro de tal�es separados est� activo e controla o objecto do menu *
*****************************************************************************************
Function uf_PAGAMENTO_verificarTalaoSep
	&& Valida se permitir tal�es conjuntos
	If myImpTalaoConjunto
		&&imagem com visto
		PAGAMENTO.menu1.estado("talSep","DISABLE")
		PAGAMENTO.menu1.talSep.config("Tal�es Sep.", myPath + "\imagens\icons\checked_w.png", "uf_PAGAMENTO_talSep","F2")
		pTalSep = .T.
	Else
		&&imagem sem visto
		PAGAMENTO.menu1.estado("talSep","ENABLE")
		PAGAMENTO.menu1.talSep.config("Tal�es Sep.", myPath + "\imagens\icons\unchecked_w.png", "uf_PAGAMENTO_talSep","F2")
		pTalSep = .F.
	Endif
Endfunc


*********************************************************************************
* caso o par�metro de tal�es separados esteja activo, controlar objecto do menu *
*********************************************************************************
Function uf_PAGAMENTO_talSep
	If pTalSep == .F.
		PAGAMENTO.menu1.talSep.config("Tal�es Sep.", myPath + "\imagens\icons\checked_w.png", "uf_PAGAMENTO_talSep","F2")
		pTalSep = .T.
		PAGAMENTO.txtDinheiro.SetFocus()
	Else
		PAGAMENTO.menu1.talSep.config("Tal�es Sep.", myPath + "\imagens\icons\unchecked_w.png", "uf_PAGAMENTO_talSep","F2")
		pTalSep = .F.
		PAGAMENTO.txtDinheiro.SetFocus()
	Endif
Endfunc


*********************************************************************************
* funcao para guardar a opcao de imprimir receitas pela ordem do atendimento *
*********************************************************************************
Function uf_PAGAMENTO_imprimirOrdem

	If ATENDIMENTO.imprecordem == .F.
		PAGAMENTO.menu1.impOrdem.config("Imp. Ordenada", myPath + "\imagens\icons\checked_w.png", "uf_PAGAMENTO_imprimirOrdem","F4")
		ATENDIMENTO.imprecordem = .T.
	Else
		PAGAMENTO.menu1.impOrdem.config("Imp. Ordenada", myPath + "\imagens\icons\unchecked_w.png", "uf_PAGAMENTO_imprimirOrdem","F4")
		ATENDIMENTO.imprecordem = .F.
	Endif

	PAGAMENTO.txtDinheiro.SetFocus()

Endfunc


***********************************
* carregar o cursor com as vendas *
***********************************
Function uf_PAGAMENTO_prepararAbertura
	Local lcValida, lcComp, lcCompFact, lcValidaDev, lcValidaFact, lcValidaFactDev, olcCod, lcValorDoc, lcValorReg, lcValidaFactPago, lcValidaDiploma
	Store .F. To lcValida, lcCompFact, lcComp, lcValidaDev, lcValidaFact, lcValidaFactDev, lcValidaFactPago, lcValidaDiploma
	Store "xxx" To olcCoda
	Store 0 To lcValorReg, lcValorDoc

	**
	**	IF !USED("UcrsConfigModPag")
	lcSQL = ''
	If Alltrim(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))='�'
		TEXT TO lcSQL NOSHOW TEXTMERGE
				select * from B_modoPag (nolock)
		ENDTEXT
	Else
		TEXT TO lcSQL NOSHOW TEXTMERGE
				select * from B_modoPag_moeda(nolock)
		ENDTEXT
	Endif
	If !uf_gerais_actGrelha("", "UcrsConfigModPag", lcSQL)
		uf_perguntalt_chama("N�o foi poss�vel verificar as configura��es de Modo de Pagamento. Contacte o suporte.", "", "OK", 32)
		Return .F.
	Endif
	**	ENDIF

	**
	uf_pagamento_preparaCabVendas()

	Local lcValePos, lcUcrsValeEtiliquido
	lcValePos=1
	lcUcrsValeEtiliquido = 0

	Select uCrsCabVendas
	Go Top
	Scan
		Replace uCrsCabVendas.valorPagar With uCrsCabVendas.etiliquido
		Replace uCrsCabVendas.nifgenerico With .F.
	Endscan

	If Used("uCrsVale")
		Select uCrsCabVendas
		Go Top
		Scan
			Select fi
			Go Top
			Scan
				If lcValida && se j� encontrou venda
					If Left(fi.Design,1) == "."
						Exit && j� percorreu todas as linhas desta venda -> sair do scan
					Else
						** Guardar valor total (etiliquido) do ucrsvale
						Select ucrsvale
						Go Top
						Scan For Alltrim(ucrsvale.fistamp) == Alltrim(fi.ofistamp)
							lcUcrsValeEtiliquido = ucrsvale.etiliquido
							Exit
						Endscan

						If (fi.marcada == .T.) And (uCrsCabVendas.sel == .T.) And (uCrsCabVendas.Fact == .T.) And (Right(Alltrim(uCrsCabVendas.Design),10) != "*SUSPENSA*") And !(Right(Alltrim(uCrsCabVendas.Design),18)=="*SUSPENSA RESERVA*")

							Select ucrsvale
							Replace ucrsvale.Fact 		With .T. For Alltrim(ucrsvale.fistamp) == Alltrim(fi.ofistamp)
							&& caso o novo pre�o da linha seja inferior ao pre�o a regularizador, assumir a diferen�a como desconto financeiro no recibo
							&& caso seja superior n�o assume desconto nenhum
							If (fi.etiliquido <= lcUcrsValeEtiliquido)
								Replace ucrsvale.totalCdesc	With fi.etiliquido For Alltrim(ucrsvale.fistamp) == Alltrim(fi.ofistamp)
							Else
								Replace ucrsvale.totalCdesc With lcUcrsValeEtiliquido For Alltrim(ucrsvale.fistamp) == Alltrim(fi.ofistamp)
							Endif
							Go Top
						Else
							Select ucrsvale
							Replace ucrsvale.Fact With .F. For Alltrim(ucrsvale.fistamp) == Alltrim(fi.ofistamp)
							If (fi.etiliquido <= lcUcrsValeEtiliquido)
								Replace ucrsvale.totalCdesc	With fi.etiliquido For Alltrim(ucrsvale.fistamp) == Alltrim(fi.ofistamp)
							Else
								Replace ucrsvale.totalCdesc With lcUcrsValeEtiliquido For Alltrim(ucrsvale.fistamp) == Alltrim(fi.ofistamp)
							Endif
							Go Top
						Endif

						If (fi.partes != 1 And fi.marcada != .T.) Or (fi.partes == 0 And fi.marcada == .T.)
							lcValorDoc = lcValorDoc + fi.etiliquido
						Endif

						Select ucrsvale
						lcValePos = Recno()
						Go Top
						Scan For Alltrim(ucrsvale.fistamp) == Alltrim(fi.ofistamp)
							If ucrsvale.lancacc != .T.
								lcValorReg = lcValorReg + ucrsvale.etiliquido
							Else
								If ucrsvale.lancacc == .T. And ucrsvale.fmarcada
									lcValorReg = lcValorReg + (ucrsvale.eaquisicao * ucrsvale.etiliquido / ucrsvale.qtt)
								Endif
							Endif

							Replace ucrsvale.factPago	With .F.
						Endscan
						Try
							Go lcValePos
						Catch
						Endtry
					Endif
				Endif

				If (Alltrim(fi.Design) == Alltrim(uCrsCabVendas.Design)) And (fi.Lordem == uCrsCabVendas.Lordem)
					lcValida=.T. && encontrou venda
				Endif

				Select fi
			Endscan

			Select uCrsCabVendas
			Replace uCrsCabVendas.valorPagar With Round(lcValorDoc - lcValorReg,2)

			Store 0 To lcValorDoc, lcValorReg
			Store .F. To lcValida, lcTemComp
			Select uCrsCabVendas
		Endscan
	Endif

	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE
		exec up_cartao_aplicarValesPendentes <<ft.no>>,<<ft.estab>>
	ENDTEXT

	If !uf_gerais_actGrelha("","uCrsVales",lcSQL)
		uf_perguntalt_chama("OCORREU UM ERRO A PROCURAR OS VALES DO CLIENTE.","OK","",16)
		Return .F.
	Endif

	** Se o parametro de aplica��o n�o estiver ativo limpar todos os vales
	If uf_gerais_getParameter("ADM0000000266","BOOL") == .F.
		Select uCrsVales
		Go Top
		Scan
			Delete
		Endscan
	Endif

	** cursor com as linhas dos documentos a criar para as seguradoras com as comparticipa��es
	If !Used("uCrsCabFactSeg")
		lcSQL = ""
		TEXT TO lcSQL NOSHOW TEXTMERGE
			select cast('' as varchar(35)) as ftstamp  where 0=1
		ENDTEXT

		If !uf_gerais_actGrelha("","uCrsCabFactSeg",lcSQL)
			uf_perguntalt_chama("OCORREU UM ERRO A PROCURAR OS VALES DO CLIENTE.","OK","",16)
			Return .F.
		Endif
	Else
		Select uCrsCabFactSeg
		Delete All
	Endif

	** Elimina vales j� aplicados no atendimento
	Select u_refVale From fi Where !Empty(u_refVale) Into Cursor ucrsFIRefsValesTemp Readwrite
	Select ucrsFIRefsValesTemp
	Go Top
	Scan
		Select uCrsVales
		Go Top
		Scan For Upper(Alltrim(uCrsVales.ref)) == Upper(Alltrim(ucrsFIRefsValesTemp.u_refVale))
			Select uCrsVales
			Delete
		Endscan
	Endscan
	If Used("ucrsFIRefsValesTemp")
		fecha("ucrsFIRefsValesTemp")
	Endif

	uf_atendimento_reserva_cursores()

	If Reccount("uCrsCabVendas")>1
		Select uCrsCabVendas
		Go Top
		Scan
			Local lcTemResFI
			Store .F. To lcTemResFI
			Select * From fi Where Left(Alltrim(Str(fi.Lordem)),1)==Left(Alltrim(Str(uCrsCabVendas.Lordem)),1)  And !Empty(fi.ref) And !Empty(fi.tipor) Order By Lordem Into Cursor ucrsapagacanvendas Readwrite
			If Reccount("ucrsapagacanvendas")>0
				lcTemResFI=.T.
			Endif
			If uCrsCabVendas.etiliquido=0 And At('RESERVA',Upper(uCrsCabVendas.Design))=0 And uCrsCabVendas.valorPagar=0 And lcTemResFI=.T. And Upper(myPaisConfSoftw) == 'PORTUGAL'
				Delete
			Endif
			fecha("ucrsapagacanvendas")
		Endscan
	Endif

	Select uCrsCabVendas
	Go Top

	Select uCrsVales
	Go Top

	Select uCrsCabVendas
	Go Top
Endfunc


**
Function uf_regvendas_setposVirtualKeyboard
	Try
		Do Case
			Case PAGAMENTO.ActiveControl.Name == 'txtDinheiro'
				PAGAMENTO.txtDinheiro.SelStart = Len(Alltrim(PAGAMENTO.txtDinheiro.Value))

			Case PAGAMENTO.ActiveControl.Name == 'txtMB'
				PAGAMENTO.txtMB.SelStart = Len(Alltrim(PAGAMENTO.txtMB.Value))

			Case PAGAMENTO.ActiveControl.Name == 'txtVisa'
				PAGAMENTO.txtVisa.SelStart = Len(Alltrim(PAGAMENTO.txtVisa.Value))

			Case PAGAMENTO.ActiveControl.Name == 'txtCheque'
				PAGAMENTO.txtCheque.SelStart = Len(Alltrim(PAGAMENTO.txtCheque.Value))

			Case PAGAMENTO.ActiveControl.Name == 'txtModPag3'
				PAGAMENTO.txtModPag3.SelStart = Len(Alltrim(PAGAMENTO.txtModPag3.Value))

			Case PAGAMENTO.ActiveControl.Name == 'txtModPag4'
				PAGAMENTO.txtModPag4.SelStart = Len(Alltrim(PAGAMENTO.txtModPag4.Value))

			Case PAGAMENTO.ActiveControl.Name == 'txtModPag5'
				PAGAMENTO.txtModPag5.SelStart = Len(Alltrim(PAGAMENTO.txtModPag5.Value))

			Case PAGAMENTO.ActiveControl.Name == 'txtModPag6'
				PAGAMENTO.txtModPag6.SelStart = Len(Alltrim(PAGAMENTO.txtModPag6.Value))

			Otherwise
				***
		Endcase
	Catch
		****
	Endtry
Endfunc

******************************
* teclado num�rico do painel *
******************************
Function uf_PAGAMENTO_tecladonumerico
	Lparameters lcTexto

	If PAGAMENTO.txtValAtend.Value <= 0 Or (myPagCentral And !uf_gerais_validaPagCentralDinheiro())
		Return .F.
	Endif

	Local lcLen, lcObj
	Store "" To lcObj
	lcLen=0

	Do Case
		Case PAGAMENTO.txtDinheiro.BackColor = Rgb(230,242,255)
			lcObj = 'PAGAMENTO.txtDinheiro'
			If (Occurs(".", PAGAMENTO.txtDinheiro.Value)>0) And (lcTexto=".")
				Return
			Endif
		Case PAGAMENTO.txtMB.BackColor = Rgb(230,242,255)
			lcObj ='PAGAMENTO.txtMB'
			If (Occurs(".", PAGAMENTO.txtMB.Value)>0) And (lcTexto=".")
				Return
			Endif
		Case PAGAMENTO.txtVisa.BackColor = Rgb(230,242,255)
			lcObj ='PAGAMENTO.txtVisa'
			If (Occurs(".", PAGAMENTO.txtVisa.Value)>0) And (lcTexto=".")
				Return
			Endif
		Case PAGAMENTO.txtCheque.BackColor = Rgb(230,242,255)
			lcObj ='PAGAMENTO.txtCheque'
			If (Occurs(".", PAGAMENTO.txtCheque.Value)>0) And (lcTexto=".")
				Return
			Endif
		Case PAGAMENTO.txtModPag3.BackColor = Rgb(230,242,255)
			lcObj ='PAGAMENTO.txtModPag3'
			If (Occurs(".", PAGAMENTO.txtModPag3.Value)>0) And (lcTexto=".")
				Return
			Endif
		Case PAGAMENTO.txtModPag4.BackColor = Rgb(230,242,255)
			lcObj ='PAGAMENTO.txtModPag4'
			If (Occurs(".", PAGAMENTO.txtModPag4.Value)>0) And (lcTexto=".")
				Return
			Endif
		Case PAGAMENTO.txtModPag5.BackColor = Rgb(230,242,255)
			lcObj ='PAGAMENTO.txtModPag5'
			If (Occurs(".", PAGAMENTO.txtModPag5.Value)>0) And (lcTexto=".")
				Return
			Endif
		Case PAGAMENTO.txtModPag6.BackColor = Rgb(230,242,255)
			lcObj ='PAGAMENTO.txtModPag6'
			If (Occurs(".", PAGAMENTO.txtModPag6.Value)>0) And (lcTexto=".")
				Return
			Endif
		Otherwise
			Return
	Endcase

	If lcTexto == "backdel"
		lcLen = Len(Alltrim(&lcObj..Value))
		&lcObj..Value = Left(Alltrim(&lcObj..Value),lcLen-1)
	Else
		If lcTexto == "del"
			&lcObj..Value = ""
		Else
			If Len(Alltrim(&lcObj..Value))>=10
				Return .F.
			Endif
			&lcObj..Value = Alltrim(&lcObj..Value) + lcTexto
		Endif
	Endif
Endfunc

*************************************************
* preencher textbox: dinheiro, mb, visa, cheque *
*************************************************
Function uf_PAGAMENTO_modoPagamento
	Lparameters lcModoP


	If Round(PAGAMENTO.txtValAtend.Value,2) < 0 And Inlist(lcModoP, 'modpag3', 'modpag4', 'modpag5', 'modpag6') And Upper(Alltrim(PAGAMENTO.btn&lcModoP..Label7.Caption)) == "VALES"
		Select uCrsCLEF
		If uCrsCLEF.no = 200
			uf_perguntalt_chama("N�O PODE GERAR VALES PARA CLIENTES SEM FICHA!","OK","",64)
			Return .F.
		Endif
	Endif

	If Round(Val(astr(PAGAMENTO.txtValRec.Value,15,2)),2) >= Abs(Round(PAGAMENTO.txtValAtend.Value,2)) Or (myPagCentral And !uf_gerais_validaPagCentralDinheiro())
		Return .F.
	Endif


	If Empty(PAGAMENTO.txtValRec.Value) And !Empty(PAGAMENTO.txtValAtend.Value)

		Do Case
			Case lcModoP=="dinheiro"
				If Type("UtilPlafond") <> 'U'
					UtilPlafond = .F.
				Endif
				If Round(PAGAMENTO.txtValAtend.Value,2) > 0
					PAGAMENTO.txtDinheiro.Value = astr(PAGAMENTO.txtValAtend.Value,15,2)
				Else
					If uf_pagamento_permsDev("dinheiro")
						PAGAMENTO.txtDinheiro.Value = astr(PAGAMENTO.txtValAtend.Value,15,2)
					Endif
				Endif
			Case lcModoP=="mb"
				If Type("UtilPlafond") <> 'U'
					UtilPlafond = .F.
				Endif
				If Round(PAGAMENTO.txtValAtend.Value,2) > 0
					PAGAMENTO.txtMB.Value = astr(PAGAMENTO.txtValAtend.Value,15,2)
				Else
					If uf_pagamento_permsDev("mb")
						PAGAMENTO.txtMB.Value = astr(PAGAMENTO.txtValAtend.Value,15,2)
					Endif
				Endif
			Case lcModoP=="visa"
				If Type("UtilPlafond") <> 'U'
					UtilPlafond = .F.
				Endif
				If Round(PAGAMENTO.txtValAtend.Value,2) > 0
					PAGAMENTO.txtVisa.Value = astr(PAGAMENTO.txtValAtend.Value,15,2)
				Else
					If uf_pagamento_permsDev("visa")
						PAGAMENTO.txtVisa.Value = astr(PAGAMENTO.txtValAtend.Value,15,2)
					Endif
				Endif
			Case lcModoP=="cheque"
				If Type("UtilPlafond") <> 'U'
					UtilPlafond = .F.
				Endif
				If Round(PAGAMENTO.txtValAtend.Value,2) > 0
					PAGAMENTO.txtCheque.Value = astr(PAGAMENTO.txtValAtend.Value,15,2)
				Else
					If uf_pagamento_permsDev("cheque")
						PAGAMENTO.txtCheque.Value = astr(PAGAMENTO.txtValAtend.Value,15,2)
					Endif
				Endif
			Case lcModoP=="modpag3"

				uv_comandoFox = uf_gerais_getUmValor("b_modoPag", "comandoFox", "ref = '05'")

				If !Empty(uv_comandoFox)

					If &uv_comandoFox.
						PAGAMENTO.txtModPag3.Value = astr(PAGAMENTO.txtValAtend.Value,15,2)
						uf_PAGAMENTO_actValoresPg()
						Return .T.
					Else
						Return .F.
					Endif

				Else

					If Type("UtilPlafond") <> 'U'
						UtilPlafond = .F.
					Endif
					If Round(PAGAMENTO.txtValAtend.Value,2) > 0
						PAGAMENTO.txtModPag3.Value = astr(PAGAMENTO.txtValAtend.Value,15,2)
					Else
						If uf_pagamento_permsDev(Alltrim(PAGAMENTO.btnModPag3.Label7.Caption))
							PAGAMENTO.txtModPag3.Value = astr(PAGAMENTO.txtValAtend.Value,15,2)
						Endif
					Endif

				Endif
			Case lcModoP=="modpag4"

				uv_comandoFox = uf_gerais_getUmValor("b_modoPag", "comandoFox", "ref = '06'")

				If !Empty(uv_comandoFox)

					If &uv_comandoFox.
						PAGAMENTO.txtModPag4.Value = astr(PAGAMENTO.txtValAtend.Value,15,2)
						uf_PAGAMENTO_actValoresPg()
						Return .T.
					Else
						Return .F.
					Endif

				Else

					If PAGAMENTO.txtValAtend.Value > 0
						If Type("UtilPlafond") <> 'U'
							UtilPlafond = .F.
						Endif
						If (Alltrim(PAGAMENTO.btnModPag4.Label7.Caption) = 'USD' Or Alltrim(PAGAMENTO.btnModPag4.Label7.Caption) = 'EUR')
							Local lcRetValMoeda
							lcRetValMoeda = uf_pagamento_moeda(Alltrim(PAGAMENTO.btnModPag4.Label7.Caption))
							PAGAMENTO.txtModPag4.Value = astr(lcRetValMoeda,15,2)
						Else
							PAGAMENTO.txtModPag4.Value = astr(PAGAMENTO.txtValAtend.Value,15,2)
						Endif
					Else
						If uf_pagamento_permsDev(Alltrim(PAGAMENTO.btnModPag4.Label7.Caption))
							If (Alltrim(PAGAMENTO.btnModPag4.Label7.Caption) = 'USD' Or Alltrim(PAGAMENTO.btnModPag4.Label7.Caption) = 'EUR')
								Local lcRetValMoeda
								lcRetValMoeda = uf_pagamento_moeda(Alltrim(PAGAMENTO.btnModPag4.Label7.Caption))
								PAGAMENTO.txtModPag4.Value = astr(lcRetValMoeda,15,2)
							Else
								PAGAMENTO.txtModPag4.Value = astr(PAGAMENTO.txtValAtend.Value,15,2)
							Endif
						Endif
					Endif

				Endif
			Case lcModoP=="modpag5"

				uv_comandoFox = uf_gerais_getUmValor("b_modoPag", "comandoFox", "ref = '07'")

				If !Empty(uv_comandoFox)

					If &uv_comandoFox.
						PAGAMENTO.txtModPag5.Value = astr(PAGAMENTO.txtValAtend.Value,15,2)
						uf_PAGAMENTO_actValoresPg()
						Return .T.
					Else
						Return .F.
					Endif

				Else

					If Type("UtilPlafond") <> 'U'
						UtilPlafond = .F.
					Endif
					If PAGAMENTO.txtValAtend.Value > 0
						If (Alltrim(PAGAMENTO.btnModPag5.Label7.Caption) = 'USD' Or Alltrim(PAGAMENTO.btnModPag5.Label7.Caption) = 'EUR')
							Local lcRetValMoeda
							lcRetValMoeda = uf_pagamento_moeda(Alltrim(PAGAMENTO.btnModPag5.Label7.Caption))
							PAGAMENTO.txtModPag5.Value = astr(lcRetValMoeda,15,2)
						Else
							PAGAMENTO.txtModPag5.Value = astr(PAGAMENTO.txtValAtend.Value,15,2)
						Endif
					Else
						If uf_pagamento_permsDev(Alltrim(PAGAMENTO.btnModPag5.Label7.Caption))
							If (Alltrim(PAGAMENTO.btnModPag5.Label7.Caption) = 'USD' Or Alltrim(PAGAMENTO.btnModPag5.Label7.Caption) = 'EUR')
								Local lcRetValMoeda
								lcRetValMoeda = uf_pagamento_moeda(Alltrim(PAGAMENTO.btnModPag5.Label7.Caption))
								PAGAMENTO.txtModPag5.Value = astr(lcRetValMoeda,15,2)
							Else
								PAGAMENTO.txtModPag5.Value = astr(PAGAMENTO.txtValAtend.Value,15,2)
							Endif
						Endif
					Endif
				Endif
			Case lcModoP=="modpag6"

				uv_comandoFox = uf_gerais_getUmValor("b_modoPag", "comandoFox", "ref = '08'")

				If !Empty(uv_comandoFox)

					If &uv_comandoFox.
						PAGAMENTO.txtModPag6.Value = astr(PAGAMENTO.txtValAtend.Value,15,2)
						uf_PAGAMENTO_actValoresPg()
						Return .T.
					Else
						Return .F.
					Endif

				Else

					If uf_gerais_getParameter('ADM0000000294', 'BOOL') = .T.
						If Type("UtilPlafond") <> 'U'
							UtilPlafond = .F.
						Endif
						PAGAMENTO.txtModPag6.Value = astr(PAGAMENTO.txtValAtend.Value,15,2)
					Else
						Public ValUtPLF
						Store 0 To ValUtPLF
						PLafondDispUt = 0
						Select uCrsatendCl
						PLafondDispUt= uCrsatendCl.eplafond - uCrsatendCl.esaldo
						If PLafondDispUt>0 Then

							uf_PAGAMENTO_resetValoresModoPag()
							uf_PAGAMENTO_actValoresPg()

							If PLafondDispUt > PAGAMENTO.txtValAtend.Value
								PAGAMENTO.txtModPag6.Value = astr(PAGAMENTO.txtValAtend.Value,15,2)
							Else
								PAGAMENTO.txtModPag6.Value = astr(PLafondDispUt,15,2)
								PrecisaRecibo = .T.
							Endif
							**uf_tecladonumerico_chama("ValUtPLF", "Plf. Dispon�vel: "+ ALLTRIM(STR(PLafondDispUt)) +" "+ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))+" " + CHR(13) + "QUAL O VALOR A UTILIZAR?", 0, .t., .f., 6, 2)
							If Val(PAGAMENTO.txtModPag6.Value) > 0
								PLafondDispUt = Val(PAGAMENTO.txtModPag6.Value)
								UtilPlafond = .T.
							Else
								UtilPlafond = .F.
							Endif

							uf_PAGAMENTO_actValoresPg()
							Return .F.
						Else
							uf_perguntalt_chama("N�O DISP�E DE PLAFOND DISPON�VEL PARA UTILIZAR.","OK","",64)
							Return .F.
						Endif
					Endif
				Endif
			Otherwise
				uf_perguntalt_chama("PAR�METRO DE PAGAMENTO INV�LIDO","OK","",64)
				Return .F.
		Endcase
	Endif



	If (Round(PAGAMENTO.txtValAtend.Value,2) - Round(Val(astr(PAGAMENTO.txtValRec.Value,15,2)),2)) > 0

		Do Case
			Case lcModoP=="dinheiro"


				PAGAMENTO.txtDinheiro.Value = astr(Round(PAGAMENTO.txtValAtend.Value,2) - ;
					(;
					Val(PAGAMENTO.txtMB.Value);
					+ Val(PAGAMENTO.txtVisa.Value);
					+ Val(PAGAMENTO.txtCheque.Value);
					+ Val(PAGAMENTO.txtModPag3.Value);
					+ Val(PAGAMENTO.txtModPag4.Value);
					+ Val(PAGAMENTO.txtModPag5.Value);
					+ Val(PAGAMENTO.txtModPag6.Value);
					),15,2)

				PAGAMENTO.txtDinheiro.SetFocus

			Case lcModoP=="mb"
				PAGAMENTO.txtMB.Value = astr(Round(PAGAMENTO.txtValAtend.Value,2) - ;
					(;
					Val(PAGAMENTO.txtDinheiro.Value);
					+ Val(PAGAMENTO.txtVisa.Value);
					+ Val(PAGAMENTO.txtCheque.Value);
					+ Val(PAGAMENTO.txtModPag3.Value);
					+ Val(PAGAMENTO.txtModPag4.Value);
					+ Val(PAGAMENTO.txtModPag5.Value);
					+ Val(PAGAMENTO.txtModPag6.Value);
					),15,2)

				PAGAMENTO.txtMB.SetFocus

			Case lcModoP=="visa"
				PAGAMENTO.txtVisa.Value = astr(Round(PAGAMENTO.txtValAtend.Value,2) - ;
					(;
					Val(PAGAMENTO.txtDinheiro.Value);
					+ Val(PAGAMENTO.txtMB.Value);
					+ Val(PAGAMENTO.txtCheque.Value);
					+ Val(PAGAMENTO.txtModPag3.Value);
					+ Val(PAGAMENTO.txtModPag4.Value);
					+ Val(PAGAMENTO.txtModPag5.Value);
					+ Val(PAGAMENTO.txtModPag6.Value);
					),15,2)

				PAGAMENTO.txtVisa.SetFocus

			Case lcModoP=="cheque"
				PAGAMENTO.txtCheque.Value = astr(Round(PAGAMENTO.txtValAtend.Value,2) - ;
					(;
					Val(PAGAMENTO.txtDinheiro.Value);
					+ Val(PAGAMENTO.txtMB.Value);
					+ Val(PAGAMENTO.txtVisa.Value);
					+ Val(PAGAMENTO.txtModPag3.Value);
					+ Val(PAGAMENTO.txtModPag4.Value);
					+ Val(PAGAMENTO.txtModPag5.Value);
					+ Val(PAGAMENTO.txtModPag6.Value);
					),15,2)

				PAGAMENTO.txtCheque.SetFocus

			Case lcModoP=="modpag3"


				uv_comandoFox = uf_gerais_getUmValor("b_modoPag", "comandoFox", "ref = '05'")

				If !Empty(uv_comandoFox)

					If &uv_comandoFox.
						PAGAMENTO.txtModPag3.Value = astr(Round(PAGAMENTO.txtValAtend.Value,2) - ;
							(;
							Val(PAGAMENTO.txtDinheiro.Value);
							+ Val(PAGAMENTO.txtMB.Value);
							+ Val(PAGAMENTO.txtVisa.Value);
							+ Val(PAGAMENTO.txtCheque.Value);
							+ Val(PAGAMENTO.txtModPag4.Value);
							+ Val(PAGAMENTO.txtModPag5.Value);
							+ Val(PAGAMENTO.txtModPag6.Value);
							),15,2)
						uf_PAGAMENTO_actValoresPg()
						Return .T.
					Else
						Return .F.
					Endif

				Else

					PAGAMENTO.txtModPag3.Value = astr(Round(PAGAMENTO.txtValAtend.Value,2) - ;
						(;
						Val(PAGAMENTO.txtDinheiro.Value);
						+ Val(PAGAMENTO.txtMB.Value);
						+ Val(PAGAMENTO.txtVisa.Value);
						+ Val(PAGAMENTO.txtCheque.Value);
						+ Val(PAGAMENTO.txtModPag4.Value);
						+ Val(PAGAMENTO.txtModPag5.Value);
						+ Val(PAGAMENTO.txtModPag6.Value);
						),15,2)
					PAGAMENTO.txtModPag3.SetFocus

				Endif

			Case lcModoP=="modpag4"

				uv_comandoFox = uf_gerais_getUmValor("b_modoPag", "comandoFox", "ref = '06'")

				If !Empty(uv_comandoFox)

					If &uv_comandoFox.
						PAGAMENTO.txtModPag4.Value = astr(Round(PAGAMENTO.txtValAtend.Value,2) - ;
							(;
							Val(PAGAMENTO.txtDinheiro.Value);
							+ Val(PAGAMENTO.txtMB.Value);
							+ Val(PAGAMENTO.txtVisa.Value);
							+ Val(PAGAMENTO.txtCheque.Value);
							+ Val(PAGAMENTO.txtModPag3.Value);
							+ Val(PAGAMENTO.txtModPag5.Value);
							+ Val(PAGAMENTO.txtModPag6.Value);
							),15,2)
						uf_PAGAMENTO_actValoresPg()
						Return .T.
					Else
						Return .F.
					Endif

				Else

					If PAGAMENTO.txtValAtend.Value > 0
						If Alltrim(PAGAMENTO.btnModPag4.Label7.Caption) = 'USD' Or Alltrim(PAGAMENTO.btnModPag4.Label7.Caption) = 'EUR'
							If Val(PAGAMENTO.txtModPag4.Value) = 0
								Local lcRetValMoeda
								lcRetValMoeda = uf_pagamento_moeda(Alltrim(PAGAMENTO.btnModPag4.Label7.Caption))
								PAGAMENTO.txtModPag4.Value = astr(lcRetValMoeda,15,2)
							Else
								PAGAMENTO.txtModPag4.Value=PAGAMENTO.txtModPag4.Value
							Endif
						Else
							PAGAMENTO.txtModPag4.Value = astr(Round(PAGAMENTO.txtValAtend.Value,2) - ;
								(;
								Val(PAGAMENTO.txtDinheiro.Value);
								+ Val(PAGAMENTO.txtMB.Value);
								+ Val(PAGAMENTO.txtVisa.Value);
								+ Val(PAGAMENTO.txtCheque.Value);
								+ Val(PAGAMENTO.txtModPag3.Value);
								+ Val(PAGAMENTO.txtModPag5.Value);
								+ Val(PAGAMENTO.txtModPag6.Value);
								),15,2)

						Endif
						PAGAMENTO.txtModPag4.SetFocus
					Endif

				Endif


			Case lcModoP=="modpag5"

				uv_comandoFox = uf_gerais_getUmValor("b_modoPag", "comandoFox", "ref = '07'")

				If !Empty(uv_comandoFox)

					If &uv_comandoFox.
						PAGAMENTO.txtModPag5.Value = astr(Round(PAGAMENTO.txtValAtend.Value,2) - ;
							(;
							Val(PAGAMENTO.txtDinheiro.Value);
							+ Val(PAGAMENTO.txtMB.Value);
							+ Val(PAGAMENTO.txtVisa.Value);
							+ Val(PAGAMENTO.txtCheque.Value);
							+ Val(PAGAMENTO.txtModPag3.Value);
							+ Val(PAGAMENTO.txtModPag4.Value);
							+ Val(PAGAMENTO.txtModPag6.Value);
							),15,2)
						uf_PAGAMENTO_actValoresPg()
						Return .T.
					Else
						Return .F.
					Endif

				Else

					If PAGAMENTO.txtValAtend.Value > 0
						If Alltrim(PAGAMENTO.btnModPag5.Label7.Caption) = 'USD' Or Alltrim(PAGAMENTO.btnModPag5.Label7.Caption) = 'EUR'
							If Val(PAGAMENTO.txtModPag5.Value) = 0
								Local lcRetValMoeda
								lcRetValMoeda = uf_pagamento_moeda(Alltrim(PAGAMENTO.btnModPag5.Label7.Caption))
								PAGAMENTO.txtModPag5.Value = astr(lcRetValMoeda,15,2)
							Else
								PAGAMENTO.txtModPag5.Value=PAGAMENTO.txtModPag5.Value
							Endif
						Else
							PAGAMENTO.txtModPag5.Value = astr(Round(PAGAMENTO.txtValAtend.Value,2) - ;
								(;
								Val(PAGAMENTO.txtDinheiro.Value);
								+ Val(PAGAMENTO.txtMB.Value);
								+ Val(PAGAMENTO.txtVisa.Value);
								+ Val(PAGAMENTO.txtCheque.Value);
								+ Val(PAGAMENTO.txtModPag3.Value);
								+ Val(PAGAMENTO.txtModPag4.Value);
								+ Val(PAGAMENTO.txtModPag6.Value);
								),15,2)
						Endif
						PAGAMENTO.txtModPag5.SetFocus
					Endif

				Endif

			Case lcModoP=="modpag6"

				uv_comandoFox = uf_gerais_getUmValor("b_modoPag", "comandoFox", "ref = '08'")

				If !Empty(uv_comandoFox)

					If &uv_comandoFox.
						PAGAMENTO.txtModPag6.Value = astr(Round(PAGAMENTO.txtValAtend.Value,2) - ;
							(;
							Val(PAGAMENTO.txtDinheiro.Value);
							+ Val(PAGAMENTO.txtMB.Value);
							+ Val(PAGAMENTO.txtVisa.Value);
							+ Val(PAGAMENTO.txtCheque.Value);
							+ Val(PAGAMENTO.txtModPag3.Value);
							+ Val(PAGAMENTO.txtModPag4.Value);
							+ Val(PAGAMENTO.txtModPag5.Value);
							),15,2)
						uf_PAGAMENTO_actValoresPg()
						Return .T.
					Else
						Return .F.
					Endif

				Else

					If uf_gerais_getParameter('ADM0000000294', 'BOOL') = .T.
						PAGAMENTO.txtModPag6.Value = astr(Round(PAGAMENTO.txtValAtend.Value,2) - ;
							(;
							Val(PAGAMENTO.txtDinheiro.Value);
							+ Val(PAGAMENTO.txtMB.Value);
							+ Val(PAGAMENTO.txtVisa.Value);
							+ Val(PAGAMENTO.txtCheque.Value);
							+ Val(PAGAMENTO.txtModPag3.Value);
							+ Val(PAGAMENTO.txtModPag4.Value);
							+ Val(PAGAMENTO.txtModPag5.Value);
							),15,2)
						PAGAMENTO.txtModPag6.SetFocus
					Else
						Public ValUtPLF
						Store 0 To ValUtPLF
						PLafondDispUt = 0
						Select uCrsatendCl
						PLafondDispUt= uCrsatendCl.eplafond - uCrsatendCl.esaldo
						If PLafondDispUt>0 Then

							uf_PAGAMENTO_resetValoresModoPag()
							uf_PAGAMENTO_actValoresPg()

							If PLafondDispUt > PAGAMENTO.txtValAtend.Value
								PAGAMENTO.txtModPag6.Value = astr(PAGAMENTO.txtValAtend.Value,15,2)
							Else
								PAGAMENTO.txtModPag6.Value = astr(PLafondDispUt,15,2)
								PrecisaRecibo = .T.
							Endif
							**uf_tecladonumerico_chama("ValUtPLF", "Plf. Dispon�vel: "+ ALLTRIM(STR(PLafondDispUt)) +" "+ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))+" " + CHR(13) + "QUAL O VALOR A UTILIZAR?", 0, .t., .f., 6, 2)
							If Val(PAGAMENTO.txtModPag6.Value) > 0
								PLafondDispUt = Val(PAGAMENTO.txtModPag6.Value)
								UtilPlafond = .T.
							Else
								UtilPlafond = .F.
							Endif
							uf_PAGAMENTO_actValoresPg()
							Return .F.
						Else
							uf_perguntalt_chama("N�O DISP�E DE PLAFOND DISPON�VEL PARA UTILIZAR.","OK","",64)
							Return .F.
						Endif
					Endif

				Endif



			Otherwise
				uf_perguntalt_chama("PAR�METRO DE PAGAMENTO INV�LIDO","OK","",64)
				Return .F.
		Endcase
	Endif


	uf_PAGAMENTO_actValoresPg()

	PAGAMENTO.Refresh

Endfunc


*******************************************
* Actualizar valores de pagamento e troco *
*******************************************
Function uf_PAGAMENTO_actValoresPg
	If Occurs(".", PAGAMENTO.txtValRec.Value) > 0
		Local lcDec
		lcDec = Strextract(PAGAMENTO.txtValRec.Value,".")

		If !Empty(lcDec)
			If Len(lcDec)=1
				PAGAMENTO.txtValRec.Value = astr((;
					Val(PAGAMENTO.txtDinheiro.Value);
					+Val(PAGAMENTO.txtMB.Value);
					+Val(PAGAMENTO.txtVisa.Value);
					+Val(PAGAMENTO.txtCheque.Value);
					+Val(PAGAMENTO.txtModPag3.Value);
					+Val(PAGAMENTO.txtModPag4.Value);
					+Val(PAGAMENTO.txtModPag5.Value);
					+Val(PAGAMENTO.txtModPag6.Value);
					),15,2)
			Else
				If Len(lcDec)=2
					PAGAMENTO.txtValRec.Value =astr((;
						Val(PAGAMENTO.txtDinheiro.Value);
						+Val(PAGAMENTO.txtMB.Value);
						+Val(PAGAMENTO.txtVisa.Value);
						+Val(PAGAMENTO.txtCheque.Value);
						+Val(PAGAMENTO.txtModPag3.Value);
						+Val(PAGAMENTO.txtModPag4.Value);
						+Val(PAGAMENTO.txtModPag5.Value);
						+Val(PAGAMENTO.txtModPag6.Value);
						),15,2)
				Else
					PAGAMENTO.txtValRec.Value = astr((;
						Val(PAGAMENTO.txtDinheiro.Value);
						+Val(PAGAMENTO.txtMB.Value);
						+Val(PAGAMENTO.txtVisa.Value);
						+Val(PAGAMENTO.txtCheque.Value);
						+Val(PAGAMENTO.txtModPag3.Value);
						+Val(PAGAMENTO.txtModPag4.Value);
						+Val(PAGAMENTO.txtModPag5.Value);
						+Val(PAGAMENTO.txtModPag6.Value);
						),15,2)
				Endif
			Endif
		Endif
	Else
		PAGAMENTO.txtValRec.Value = astr((;
			Val(PAGAMENTO.txtDinheiro.Value);
			+Val(PAGAMENTO.txtMB.Value);
			+Val(PAGAMENTO.txtVisa.Value);
			+Val(PAGAMENTO.txtCheque.Value);
			+Val(PAGAMENTO.txtModPag3.Value);
			+Val(PAGAMENTO.txtModPag4.Value);
			+Val(PAGAMENTO.txtModPag5.Value);
			+Val(PAGAMENTO.txtModPag6.Value);
			),15,2)
	Endif

	If Val(PAGAMENTO.txtValRec.Value)=0
		PAGAMENTO.txtValRec.Value = ""
	Endif

	uf_PAGAMENTO_corrigirCasasDecPg()
	uf_PAGAMENTO_calcularTrocoPg()
Endfunc


***************************
* Corrigir Casas Decimais *
***************************
Function uf_PAGAMENTO_corrigirCasasDecPg
	Local lcDec
	lcDec = Strextract(PAGAMENTO.txtValRec.Value,".")
	If Len(lcDec)>2
		PAGAMENTO.txtValRec.Value =astr(Val(PAGAMENTO.txtValRec.Value),15,2)
	Endif
	lcDec = Strextract(PAGAMENTO.txtDinheiro.Value,".")
	If Len(Alltrim(lcDec))>2
		PAGAMENTO.txtDinheiro.Value = astr(Val(PAGAMENTO.txtDinheiro.Value),15,2)
	Endif
	lcDec = Strextract(PAGAMENTO.txtMB.Value,".")
	If Len(Alltrim(lcDec))>2
		PAGAMENTO.txtMB.Value =astr(Val(PAGAMENTO.txtMB.Value),15,2)
	Endif
	lcDec = Strextract(PAGAMENTO.txtVisa.Value,".")
	If Len(Alltrim(lcDec))>2
		PAGAMENTO.txtVisa.Value =astr(Val(PAGAMENTO.txtVisa.Value),15,2)
	Endif
	lcDec = Strextract(PAGAMENTO.txtCheque.Value,".")
	If Len(Alltrim(lcDec))>2
		PAGAMENTO.txtCheque.Value =astr(Val(PAGAMENTO.txtCheque.Value),15,2)
	Endif
	lcDec = Strextract(PAGAMENTO.txtModPag3.Value,".")
	If Len(Alltrim(lcDec))>2
		PAGAMENTO.txtModPag3.Value =astr(Val(PAGAMENTO.txtModPag3.Value),15,2)
	Endif
	lcDec = Strextract(PAGAMENTO.txtModPag4.Value,".")
	If Len(Alltrim(lcDec))>2
		PAGAMENTO.txtModPag4.Value =astr(Val(PAGAMENTO.txtModPag4.Value),15,2)
	Endif
	lcDec = Strextract(PAGAMENTO.txtModPag5.Value,".")
	If Len(Alltrim(lcDec))>2
		PAGAMENTO.txtModPag5.Value =astr(Val(PAGAMENTO.txtModPag5.Value),15,2)
	Endif
	lcDec = Strextract(PAGAMENTO.txtModPag6.Value,".")
	If Len(Alltrim(lcDec))>2
		PAGAMENTO.txtModPag6.Value =astr(Val(PAGAMENTO.txtModPag6.Value),15,2)
	Endif

	uf_regvendas_setposVirtualKeyboard()
Endfunc


******************
* Calcular Troco *
******************
Function uf_PAGAMENTO_calcularTrocoPg
	If PAGAMENTO.txtValAtend.Value > 0
		lcTroco = Round(Val(astr(PAGAMENTO.txtValRec.Value,15,2)),2) - Round(PAGAMENTO.txtValAtend.Value,2)
		If lcTroco > 0
			PAGAMENTO.txtTroco.Value = Round(lcTroco,2)
		Else
			PAGAMENTO.txtTroco.Value = 0
		Endif
	Else
		PAGAMENTO.txtTroco.Value = 0
		If PAGAMENTO.txtValAtend.Value!=0
			If(!uf_pagamento_validaPagCentralTerminal())

				uv_hasDev = .F.

				Select ucrsvale
				Go Top

				Locate For ucrsvale.dev = 1

				If Found()
					uv_hasDev = .T.
				Endif

				If !uf_gerais_getParameter('ADM0000000341', 'BOOL') And !uf_gerais_getParameter_site('ADM0000000162', 'BOOL', mySite)
					PAGAMENTO.txtDinheiro.Value = astr(PAGAMENTO.txtValAtend.Value,15,2)
				Endif
			Endif
		Endif
		Return .T.
	Endif
Endfunc


***************************************************
* Evento click da checkbox da coluna Imp. Receita *
***************************************************
Function uf_PAGAMENTO_imprimeReceita
	Select uCrsCabVendas
	If Left(uCrsCabVendas.Design,12) == ".SEM RECEITA" Or Right(uCrsCabVendas.Design,10) == "*SUSPENSA*" Or PAGAMENTO.pgFrVDP.page1.grdVendas.Columns(2).check1.Value == .F. Or Right(Alltrim(uCrsCabVendas.Design),18)=="*SUSPENSA RESERVA*"
		PAGAMENTO.pgFrVDP.page1.grdVendas.Columns(2).check1.Value = .T.
		uf_perguntalt_chama("ESTA VENDA N�O CONT�M RECEITA OU J� FOI IMPRESSA.","OK","",64)
	Else
		If pFimAtendimento
			If !uf_gerais_getParameter("ADM0000000119","BOOL")
				&& Troca da Ordem da Receita
				Select ucrse1
				If ucrse1.OrdemImpressaoReceita == .F.
					Select uCrsCabVendas
					Messagebox(1)
					**IF uf_gerais_getParameter_site('ADM0000000136', 'BOOL', mySite)
					If uf_gerais_getUmValor("B_terminal", "usaDriverWin", "terminal = '" + Alltrim(myTerm) + "'")

						uf_imprimirpos_impRecTalaoComum_talaoVerso(uCrsCabVendas.stamp, 1, .F.)

					Else

						uf_imprimirpos_impRec(uCrsCabVendas.stamp, 1, "ATENDIMENTO")

					Endif
					Select uCrsCabVendas
					If Upper(Alltrim(uCrsCabVendas.receita_tipo)) != "RSP"
						Messagebox(2)
						**IF uf_gerais_getParameter_site('ADM0000000136', 'BOOL', mySite)
						If uf_gerais_getUmValor("B_terminal", "usaDriverWin", "terminal = '" + Alltrim(myTerm) + "'")

							Select uCrsCabVendas
							uf_imprimirpos_impRecTalaoComum_talaoVerso(uCrsCabVendas.stamp, 2, .F.)

						Else

							Select uCrsCabVendas
							uf_imprimirpos_impRec(uCrsCabVendas.stamp, 2, "ATENDIMENTO")

						Endif
					Else
						Return .F.
					Endif
				Else
					If Upper(Alltrim(uCrsCabVendas.receita_tipo)) != "RSP"
						Messagebox(3)
						**IF uf_gerais_getParameter_site('ADM0000000136', 'BOOL', mySite)
						If uf_gerais_getUmValor("B_terminal", "usaDriverWin", "terminal = '" + Alltrim(myTerm) + "'")

							Select uCrsCabVendas
							uf_imprimirpos_impRecTalaoComum_talaoVerso(uCrsCabVendas.stamp, 2, .F.)

						Else

							Select uCrsCabVendas
							uf_imprimirpos_impRec(uCrsCabVendas.stamp, 2, "ATENDIMENTO")
						Endif
					Endif
					Messagebox(4)
					**IF uf_gerais_getParameter_site('ADM0000000136', 'BOOL', mySite)
					If uf_gerais_getUmValor("B_terminal", "usaDriverWin", "terminal = '" + Alltrim(myTerm) + "'")

						Select uCrsCabVendas
						uf_imprimirpos_impRecTalaoComum_talaoVerso(uCrsCabVendas.stamp, 1, .F.)

					Else

						Select uCrsCabVendas
						uf_imprimirpos_impRec(uCrsCabVendas.stamp, 1, "ATENDIMENTO")

					Endif
				Endif

				Local lcPos, lcValida
				Store .F. To lcValida

				lcPos = Recno("uCrsCabVendas")
				Select uCrsCabVendas
				Go Top
				Scan
					If uCrsCabVendas.sel == .F.
						lcValida = .T.
						Exit
					Endif
				Endscan
				Select uCrsCabVendas
				Try
					Go lcPos
				Catch
				Endtry
				If lcValida == .F.
					&& Impress�o Tal�o
					If MyValidaEsperaImp == .T. And MyImprimeTalao = .T.
						uf_PAGAMENTO_eventoImpressaoPos(.F.)
					Endif

					uf_PAGAMENTO_sair( .T. )
				Else
					PAGAMENTO.txtDinheiro.SetFocus
				Endif
			Else
				PAGAMENTO.pgFrVDP.page1.grdVendas.Columns(2).check1.Value = .F.
				uf_perguntalt_chama("O sistema est� configurado para n�o permitir imprimir receitas no atendimento.","OK","",64)
				Return .F.
			Endif
		Else
			PAGAMENTO.pgFrVDP.page1.grdVendas.Columns(2).check1.Value = .F.
			uf_perguntalt_chama("DEVE FINALIZAR O ATENDIMENTO ANTES DE IMPRIMIR AS RECEITAS.","OK","",64)
			Return .F.
		Endif
	Endif
Endfunc


***********************************
* Alterar n� de tal�es a imprimir *
***********************************
Function uf_PAGAMENTO_nTaloes

	If Type("upv_altNumTaloes") <> "U"
		upv_altNumTaloes = .T.
	Else
		Public upv_altNumTaloes
		upv_altNumTaloes = .T.
	Endif

	Do Case
		Case PAGAMENTO.nTaloes == 1
			PAGAMENTO.menu1.nTaloes.config("N� Tal�es", myPath + "\imagens\icons\2_w.png", "uf_PAGAMENTO_nTaloes","F3")
			PAGAMENTO.nTaloes = 2

		Case PAGAMENTO.nTaloes == 2
			PAGAMENTO.menu1.nTaloes.config("N� Tal�es", myPath + "\imagens\icons\3_w.png", "uf_PAGAMENTO_nTaloes","F3")
			PAGAMENTO.nTaloes = 3

		Case PAGAMENTO.nTaloes == 3
			PAGAMENTO.menu1.nTaloes.config("N� Tal�es", myPath + "\imagens\icons\0_w.png", "uf_PAGAMENTO_nTaloes","F3")
			PAGAMENTO.nTaloes = 0

		Case PAGAMENTO.nTaloes == 0
			PAGAMENTO.menu1.nTaloes.config("N� Tal�es", myPath + "\imagens\icons\1_w.png", "uf_PAGAMENTO_nTaloes","F3")
			PAGAMENTO.nTaloes = 1

		Otherwise
			PAGAMENTO.menu1.nTaloes.config("N� Tal�es", myPath + "\imagens\icons\1_w.png", "uf_PAGAMENTO_nTaloes","F3")
			PAGAMENTO.nTaloes = 1
	Endcase

	PAGAMENTO.txtDinheiro.SetFocus()

Endfunc


********************************************************************
* Validar e Gravar dados no Cursor referentes � Venda ao Domic�lio *
********************************************************************
Function uf_PAGAMENTO_saveDadosDomicilio
	If Empty(Alltrim(PAGAMENTO.pgFrVDP.page2.txtTransp.Value))
		uf_perguntalt_chama("O CAMPO: TRANSPORTADOR, � DE PREENCHIMENTO OBRIGAT�RIO.","OK","",48)
		Return .F.
	Endif

	If Empty(Alltrim(PAGAMENTO.pgFrVDP.page2.txtCodPost.Value))
		uf_perguntalt_chama("O CAMPO: C�DIGO POSTAL, � DE PREENCHIMENTO OBRIGAT�RIO.","OK","",48)
		Return .F.
	Endif

	If Empty(Alltrim(PAGAMENTO.pgFrVDP.page2.txtLocal.Value))
		uf_perguntalt_chama("O CAMPO: LOCALIDADE, � DE PREENCHIMENTO OBRIGAT�RIO.","OK","",48)
		Return .F.
	Endif

	Return .T.
Endfunc



** Cria contador de psicotr�picos ou Benzo no Pagamento (chamada tamb�m da factura��o)
Function uf_PAGAMENTO_ContadorPsicoBenzo
	Local lcCntPsi, lcCntBen
	Store 0 To lcCntPsi, lcCntBen

	Select fi
	Go Top
	Scan
		If !(Left(fi.Design,1)==".") And fi.partes==0
			If fi.u_psico
				&& Foi Detectado Psicotropico => Ler contador e actualizar Var. Local
				If lcCntPsi=0
					TEXT TO lcSql NOSHOW TEXTMERGE
						exec up_gerais_psico 1, '<<mySite>>'
					ENDTEXT
					If uf_gerais_actGrelha("",[uCrsContPsi],lcSQL)
						lcCntPsi = uCrsContPsi.ct
						fecha([uCrsContPsi])
					Else
						uf_perguntalt_chama("OCORREU UMA ANOMALIA A CALCULAR O CONTADOR PSICOTR�PICO."+ Chr(13)+ "O SOFTWARE VAI SER ENCERRADO COMO MEDIDA DE PREVEN��O.","OK","",16)
						Quit
					Endif
				Endif

				&& Se contador da Linha estiver Vazio � Actualizado
				If (fi.u_psicont == 0)
					lcCntPsi = lcCntPsi + 1
					Select fi
					Replace fi.u_psicont With lcCntPsi In fi
				Endif
			Else
				If fi.u_benzo
					&& Contador gerado caso o campo esteja Vazio
					If lcCntBen=0
						&& Vamos ler o Valor no Server Caso o Contador esteja a Zero ***
						TEXT TO lcSql NOSHOW TEXTMERGE PRETEXT 7
							exec up_gerais_benzo 1, '<<mySite>>'
						ENDTEXT
						If uf_gerais_actGrelha("",[uCrsContBen],lcSQL)
							lcCntBen=uCrsContBen.ct
							fecha([uCrsContBen])
						Else
							uf_perguntalt_chama("OCORREU UMA ANOMALIA A CALCULAR CONTADOR BENZODIAZEPINA. POR FAVOR CONTACTE O SUPORTE."+ Chr(13)+" O SOFTWARE VAI SER ENCERRADO COMO MEDIDA DE PREVEN��O.","OK","",16)
							Quit
						Endif
					Endif

					If fi.u_bencont==0
						lcCntBen=lcCntBen+1
						Select fi
						Replace fi.u_bencont With lcCntBen In fi
					Endif
				Endif
			Endif
		Endif
	Endscan
Endfunc


***************************************************
* Validar data de vencimento do cr�dito a cliente *
***************************************************
Function uf_PAGAMENTO_dataVencimentoPos
	Lparameters lnClNo, lnClEstab

	Local lcPdata
	lcPdata=Datetime()+(difhoraria*3600)
	**lcPdata=datetime()

	If uCrsCLEF.TP_vencimento=2
		lcPdata=Date()+30
	Else
		If uCrsCLEF.TP_vencimento=3
			lcPdata = Gomonth(Ctod(Alltrim(Str(uCrsCLEF.TP_diames))+Right(astr(Date()),8)),1)
		Endif
	Endif

	Return lcPdata
Endfunc

Function uf_pagamento_validaFinalizaValidaMultivenda

	fecha("uCrsCabVendasTemp")

	If(Used("uCrsCabVendas"))

		Select * From uCrsCabVendas Into Cursor uCrsCabVendasTemp


		If(Used("uCrsCabVendasTemp")  And Empty(mymultivenda))
			Local lnRegistos
			lnRegistos  = 0
			Select uCrsCabVendasTemp
			Go Top
			Count To lnRegisto For !Deleted()
			If(lnRegisto>1)
				fecha("uCrsCabVendasTemp")
				Return .F.
			Endif
		Endif
	Endif

	fecha("uCrsCabVendasTemp")
	Return .T.
Endfunc



** Valida��es antes de finalizar o atendimento
Function uf_PAGAMENTO_gravar

	uf_gerais_regista_logs_passos("PAGAMENTO",nrAtendimento,"PAG0A","VALIDAR PAGAMENTO")

	If(!uf_pagamento_validaFinalizaValidaMultivenda())
		uf_perguntalt_chama("N�o pode efetuar atendimentos multivenda. Por favor valide.", "OK", "", 16)
		Return .F.
	Endif


	If Type("nrAtendimentoRec") != 'U'

		&&valida se pagamento em plafond em simultaneo
		If Type("PAGAMENTO.btnModPag6")!= 'U'  And  Type("PAGAMENTO.btnModPag6.Label7")!= 'U'
			If Alltrim(Upper(PAGAMENTO.btnModPag6.Label7.Caption)) = "PLF. DISP."
				If(uf_pagamento_validaMultiTipoPagComPlafond())
					uf_perguntalt_chama("N�o pode utilizar plafond em simult�neo com outros m�todos de pagamento.", "OK", "", 64)
					Return .F.
				Endif
			Endif
		Endif

		If Val(PAGAMENTO.txtModPag6.Value) <> 0  And Val(PAGAMENTO.txtValRec.Value) < PAGAMENTO.txtValAtend.Value
			uf_perguntalt_chama("AO UTILIZAR O PLAFOND TEM QUE PAGAR A TOTALIDADE DO ATENDMENTO","OK","",16)
			Return(.F.)
		Endif

		**


		nrAtendimentoRec = nrAtendimento
		Select uCrsValsPagam
		Delete All
		Append Blank
		Replace uCrsValsPagam.pagam1 With Val(PAGAMENTO.txtDinheiro.Value)
		Replace uCrsValsPagam.pagam2 With Val(PAGAMENTO.txtMB.Value)
		Replace uCrsValsPagam.pagam3 With Val(PAGAMENTO.txtVisa.Value)
		Replace uCrsValsPagam.pagam4 With Val(PAGAMENTO.txtCheque.Value)
		Replace uCrsValsPagam.pagam5 With Val(PAGAMENTO.txtModPag3.Value)
		Replace uCrsValsPagam.pagam6 With Val(PAGAMENTO.txtModPag4.Value)
		Replace uCrsValsPagam.pagam7 With Val(PAGAMENTO.txtModPag5.Value)
		Replace uCrsValsPagam.pagam8 With Val(PAGAMENTO.txtModPag6.Value)
		If Vartype(PAGAMENTO.txtTroco.Value)='N'
			Replace uCrsValsPagam.troco With PAGAMENTO.txtTroco.Value
		Else
			Replace uCrsValsPagam.troco With Val(PAGAMENTO.txtTroco.Value)
		Endif
	Endif





	If uf_gerais_getParameter_site("ADM0000000133","BOOL", mySite)

		Select fi
		Go Top

		Locate For fi.ref = 'V000001'

		If Found()
			If !uf_gerais_chkconexao()
				uf_perguntalt_chama("N�O FOI POSS�VEL COMUNICAR COM A CENTRAL. POR FAVOR REMOVA O DESCONTO DE VALOR EM CART�O PARA PODER TERMINAR.","OK","",16)
				Return(.F.)
			Endif
		Endif

	Endif




	If UtilPlafond = .T.
		Select uCrsCabVendas
		Replace uCrsCabVendas.modoPag With .T.
		If !uf_PAGAMENTO_clickXpag()
			Replace uCrsCabVendas.modoPag With .F.
			Return .F.
		Else
			UtilPlafond = .T.    && mantem o valor da variavel
		Endif
	Endif

	** pgamento por maquina dinehiro com o atendimento em espera
	If Type("REGVENDAS")=="U"
		Public lcContinuaMaqPag
		lcContinuaMaqPag = .F.
		Local lcDinheiroPc
		lcDinheiroPc = Val(PAGAMENTO.txtDinheiro.Value)
		If lcDinheiroPc!=0 And uf_gerais_getParameter_site('ADM0000000041', 'BOOL', mySite) = .T.
			uf_maqdinheiro_chama()
			If lcContinuaMaqPag = .F.
				Return(.F.)
			Endif
		Endif
	Endif

	&& Verificar Caixa
	Select ucrse1
	Go Top
	lcSQL = ''
	If ucrse1.gestao_cx_operador = .T.
		lcSQL = ''
		TEXT TO lcSQL TEXTMERGE NOSHOW
				exec up_caixa_verificaCaixaAberta '<<ALLTRIM(mysite)>>', '<<ALLTRIM(ch_vendnm)>>', 1
		ENDTEXT
		If !uf_gerais_actGrelha("", "uCrsCxAberta", lcSQL)
			uf_perguntalt_chama("N�o foi poss�vel verificar as defini��es de Caixa","OK","",16)
			Return .F.
		Else
			If !Reccount("uCrsCxAberta")>0
				uf_perguntalt_chama("A SUA SESS�O DE CAIXA FOI FECHADA. N�O PODE CONTINUAR SEM PRIMEIRO ABRIR NOVA SESS�O.","OK","",48)
				fecha("uCrsCxAberta")
				Return .F.
			Endif
			fecha("uCrsCxAberta")
		Endif
	Else
		lcSQL = ''
		TEXT TO lcSQL TEXTMERGE NOSHOW
				exec up_caixa_verificaCaixaAberta '<<ALLTRIM(mysite)>>', '<<ALLTRIM(myTerm)>>', 0
		ENDTEXT
		If !uf_gerais_actGrelha("", "uCrsCxAberta", lcSQL)
			uf_perguntalt_chama("N�o foi poss�vel verificar as defini��es de Caixa","OK","",16)
			Return .F.
		Else
			If !Reccount("uCrsCxAberta")>0
				uf_perguntalt_chama("A SUA SESS�O DE CAIXA FOI FECHADA. N�O PODE CONTINUAR SEM PRIMEIRO ABRIR NOVA SESS�O.","OK","",48)
				fecha("uCrsCxAberta")
				Return .F.
			Endif
			fecha("uCrsCxAberta")
		Endif
	Endif

	&& Valida troco
	If myValidaTroco

		lcTotalNaoDinheiro = Val(PAGAMENTO.txtMB.Value)	+ Val(PAGAMENTO.txtVisa.Value) +Val(PAGAMENTO.txtCheque.Value)

		If !Inlist(Alltrim(PAGAMENTO.btnModPag3.Label7.Caption), 'USD', 'EUR')
			lcTotalNaoDinheiro = lcTotalNaoDinheiro + Val(PAGAMENTO.txtModPag3.Value)
		Endif
		If !Inlist(Alltrim(PAGAMENTO.btnModPag4.Label7.Caption), 'USD', 'EUR')
			lcTotalNaoDinheiro = lcTotalNaoDinheiro + Val(PAGAMENTO.txtModPag4.Value)
		Endif
		If !Inlist(Alltrim(PAGAMENTO.btnModPag5.Label7.Caption), 'USD', 'EUR')
			lcTotalNaoDinheiro = lcTotalNaoDinheiro + Val(PAGAMENTO.txtModPag5.Value)
		Endif
		If !Inlist(Alltrim(PAGAMENTO.btnModPag6.Label7.Caption), 'USD', 'EUR')
			lcTotalNaoDinheiro = lcTotalNaoDinheiro + Val(PAGAMENTO.txtModPag6.Value)
		Endif

		If lcTotalNaoDinheiro > Round(PAGAMENTO.txtValAtend.Value,2) And Round(PAGAMENTO.txtValAtend.Value,2) > 0
			uf_perguntalt_chama("APENAS PODER� INCLUIR TROCO NO MEIO DE PAGAMENTO A DINHEIRO. POR FAVOR VERIFIQUE.","OK","",64)
			Return .F.
		Endif

	Endif



	&& valida total recebido e conta numero de vendas a cr�dito
	Local lcCntC, lcValCred, lcColocaCredito, lcValPergCred
	Store 0 To lcCntC
	Store .T. To lcValCred
	Store .F. To lcColocaCredito, lcValPergCred
	Select uCrsCabVendas
	Go Top
	Scan
		If Round(PAGAMENTO.txtValAtend.Value,2) < 0 And Round(PAGAMENTO.txtValAtend.Value,2) <> Round(Val(astr(PAGAMENTO.txtValRec.Value,15,2)),2)
			uf_PAGAMENTO_modoPagamento("dinheiro")
		Endif
		If (!uCrsCabVendas.modoPag) And ( Round(Val(astr(PAGAMENTO.txtValRec.Value,15,2)),2) < Round(PAGAMENTO.txtValAtend.Value,2) ) And (!myPagCentral Or uf_gerais_validaPagCentralDinheiro())
			Local lcValCred
			Store .T. To lcValCred

			&& Valida Se Permite Vendas a Cr�dito no Atendimento
			Select ft
			If !(myVendasCredito)
				lcValCred = .F.
			Endif

			&& Valida Posto Offline
			If myOffline
				lcValCred = .F.
			Endif

			&& valida cliente
			Select ft
			If !Empty(ft.no)
				If ft.no == 200
					lcValCred = .F.
				Endif
			Endif
			**

			&& Valida se valor recebido < valor atendimento
			If !lcValCred Or Round(Val(astr(PAGAMENTO.txtValRec.Value,15,2)),2) > 0
				uf_perguntalt_chama("N�o pode finalizar uma venda a dinheiro com o valor recebido inferior ao valor total do documento.","OK","",48)
				Return .F.
			Endif

			&& valida se coloca o atendimento a cr�dito caso n�o utilizador n�o coloque valor nos meios de pagamento
			If !lcValPergCred
				&& Caso tenha o parametro ativo
				If uf_gerais_getParameter('ADM0000000265', 'BOOL') == .T.
					If !uf_perguntalt_chama("N�o seleccionou nenhum meio de pagamento!" + Chr(13) + "Pretende colocar o atendimento a cr�dito?","Sim","N�o")
						lcValPergCred = .T.
						Return .F.
					Else
						lcValPergCred = .T.
						lcColocaCredito = .T.
					Endif
				Else
					&& caso esteja desativo avisa o operador
					uf_perguntalt_chama("N�o pode finalizar uma venda a dinheiro com o valor recebido inferior ao valor total do documento. Favor verifique.","OK","",48)
					lcValPergCred = .T.
					Return .F.
				Endif
			Else
				lcValPergCred = .T.
				lcColocaCredito = .T.
			Endif
		Endif

		&& aproveitar para guardar n�mero de vendas a cr�dito
		If uCrsCabVendas.modoPag
			lcCntC = lcCntC + 1
		Endif
	Endscan


	If lcColocaCredito
		Select uCrsCabVendas
		Go Top
		Scan
			PAGAMENTO.pgFrVDP.page1.grdVendas.Columns(4).check1.Value = 1
			Select uCrsCabVendas
			Replace uCrsCabVendas.modoPag With .T.
			If !uf_PAGAMENTO_clickXpag()
				PAGAMENTO.pgFrVDP.page1.grdVendas.Columns(4).check1.Value = 0
				Select uCrsCabVendas
				Replace uCrsCabVendas.modoPag With .F.

				Return .F.
			Endif

			&& aproveitar para guardar n�mero de vendas a cr�dito
			If uCrsCabVendas.modoPag
				lcCntC = lcCntC + 1
			Endif
		Endscan
	Endif

	&& verificar se o atendimento est� todo a cr�dito e existe valor a receber
	If lcCntC = Reccount("uCrsCabVendas")
		If !Empty(PAGAMENTO.txtValRec.Value)
			If !uf_perguntalt_chama("ATEN��O: VAI GRAVAR OS DOCUMENTOS A CR�DITO, NO ENTANTO REGISTOU UM VALOR A RECEBER. TEM A CERTEZA QUE PRETENDE CONTINUAR?"+Chr(13)+Chr(13)+"Se 'Sim' o valor recebido ser� ignorado.","Sim","N�o")
				Return .F.
			Endif
		Endif
	Endif

	&& validar nrAtendimento
	If Empty(nrAtendimento)
		uf_perguntalt_chama("O CAMPO N� DE ATENDIMENTO N�O EST� PREENCHIDO. REINICIE O PAINEL DE ATENDIMENTO.","OK","",48)
		Return .F.
	Endif

	&& Entidade Factura��o
	Select uCrsatendCl
	Select uCrsCLEF
	If !(Alltrim(uCrsatendCl.utstamp) == Alltrim(uCrsCLEF.utstamp))
		If !uf_perguntalt_chama("A Entidade de Factura��o � diferente do Utente seleccionado no atendimento." + Chr(13) + "Tem a certeza que pretende continuar?", "Sim", "N�o")
			Return .F.
		Else
			If Reccount("uCrsCLEF") > 0
				Select ft
				Replace ;
					ft.Nome			With Alltrim(uCrsCLEF.Nome),;
					ft.nome2		With Alltrim(uCrsCLEF.nome2),;
					ft.no			With uCrsCLEF.no,;
					ft.morada		With Alltrim(uCrsCLEF.morada),;
					ft.Local		With Alltrim(uCrsCLEF.Local),;
					ft.codpost		With Alltrim(uCrsCLEF.codpost),;
					ft.ncont		With Alltrim(uCrsCLEF.ncont),;
					ft.telefone		With Alltrim(uCrsCLEF.telefone),;
					ft.zona			With Alltrim(uCrsCLEF.zona),;
					ft.estab		With uCrsCLEF.estab,;
					ft.tipo			With Alltrim(uCrsCLEF.tipo),;
					ft.bino			With uCrsCLEF.bino,;
					ft.moeda		With uCrsCLEF.moeda,;
					ft.pais			With Iif(uCrsCLEF.codigoP=="PT",1,2),;
					ft.ccusto		With uCrsCLEF.ccusto,;
					ft.u_hclstamp	With uCrsatendCl.utstamp
				Select ft2
				Replace ;
					ft2.morada		With Alltrim(uCrsCLEF.morada),;
					ft2.Local		With Alltrim(uCrsCLEF.Local),;
					ft2.codpost		With Alltrim(uCrsCLEF.codpost),;
					ft2.telefone	With Alltrim(uCrsCLEF.telefone),;
					ft2.email		With Alltrim(uCrsCLEF.email);
					ft2.nomeUtPag	With Alltrim(uCrsatendCl.Nome)
				Select fi
				&& Centro analitico - linhas
				Replace All ficcusto	With uCrsCLEF.ccusto	In fi
				Go Top
			Endif
		Endif
	Endif




	&& Novo Controle Plafond Entidade de Fatura��o - 20150515 - Lu�s Leal
	If ucrse1.controla_plafond == .T.
		If lcCntC > 0 && tem cr�ditos
			Select uCrsCLEF
			If uCrsCLEF.no != 200 And ucrse1.plafond_cliente > 0
				Local lcValorTotal
				Store 0 To lcValorTotal

				If !uf_gerais_varificaCreditoEPalavrapasse(uCrsCLEF.no, uCrsCLEF.estab)
					Return .F.
				Endif

				Select uCrsCabVendas
				Calculate Sum(uCrsCabVendas.valorPagar) For uCrsCabVendas.modoPag == .T. To lcValorTotal
				If Upper(Alltrim(ucrse1.pais)) == 'PORTUGAL'
					If uCrsCLEF.eplafond == 0
						*!*							IF (uCrsCLEF.esaldo + lcValorTotal) > uCrse1.plafond_cliente
						*!*								IF uf_perguntalt_chama("O plafond do cliente foi ultrapassado." + CHR(13) + "Pretende continuar com o atendimento?","Sim","N�o",64)
						*!*									PUBLIC cval
						*!*									STORE '' TO cval
						*!*
						*!*									uf_tecladoalpha_chama("cval", "INTRODUZA PASSWORD PARA PLAFOND:", .t., .f., 0)
						*!*
						*!*									IF EMPTY(cval)
						*!*										uf_perguntalt_chama("Deve introduzir a password de Supervisor para permitir ultrapassar o plafond.","OK","",64)
						*!*										RETURN .f.
						*!*									ELSE
						*!*										IF !(UPPER(ALLTRIM(cval))==UPPER(ALLTRIM(uf_gerais_getParameter('ADM0000000187', 'TEXT'))))
						*!*											uf_perguntalt_chama("A password de Supervisor n�o est� correcta.","OK","",64)
						*!*										RETURN .F.
						*!*										ENDIF
						*!*									ENDIF
						*!*								ELSE
						*!*									RETURN .f.
						*!*								ENDIF
						*!*							ENDIF
					Else
						If (uCrsCLEF.esaldo+lcValorTotal) > uCrsCLEF.eplafond
							If uf_perguntalt_chama("O plafond do cliente foi ultrapassado." + Chr(13) + "Pretende continuar com o atendimento?","Sim","N�o",64)
								Public cval
								Store '' To cval
								If uf_gerais_getParameter('ADM0000000187', 'BOOL')
									uf_tecladoalpha_chama("cval", "INTRODUZA PASSWORD PARA PLAFOND:", .T., .F., 0)

									If Empty(cval)
										uf_perguntalt_chama("Deve introduzir a password de Supervisor para permitir ultrapassar o plafond.","OK","",64)
										Return .F.
									Else
										If !(Upper(Alltrim(cval))==Upper(Alltrim(uf_gerais_getParameter('ADM0000000187', 'TEXT'))))
											uf_perguntalt_chama("A password de Supervisor n�o est� correcta.","OK","",64)
											Return .F.
										Endif
									Endif
								Endif
							Else
								Return .F.
							Endif
						Endif
					Endif
				Else
					If (lcValorTotal - (uCrsCLEF.eplafond - uCrsCLEF.esaldo)) > 0
						uf_perguntalt_chama("O plafond do cliente foi ultrapassado." + Chr(13) + "N�o pode concluir o atendimento.","OK","",64)
						Return .F.
					Endif
				Endif
			Else
				Select uCrsCLEF
				If uCrsCLEF.no != 200 &&AND uCrsCLEF.eplafond > 0
					Local lcValorTotal
					Store 0 To lcValorTotal

					If !uf_gerais_varificaCreditoEPalavrapasse(uCrsCLEF.no, uCrsCLEF.estab)
						Return .F.
					Endif

					Select uCrsCabVendas
					Calculate Sum(uCrsCabVendas.valorPagar) For uCrsCabVendas.modoPag==.T. To lcValorTotal
					If Upper(Alltrim(ucrse1.pais)) == 'PORTUGAL'
						If (uCrsCLEF.esaldo+lcValorTotal) > uCrsCLEF.eplafond And pPagPost=.F.
							If uf_perguntalt_chama("O plafond do cliente foi ultrapassado." + Chr(13) + "Pretende continuar com o atendimento?","Sim","N�o",64)
								Public cval
								Store '' To cval
								If uf_gerais_getParameter('ADM0000000187', 'BOOL')
									uf_tecladoalpha_chama("cval", "INTRODUZA PASSWORD PARA PLAFOND:", .T., .F., 0)

									If Empty(cval)
										uf_perguntalt_chama("Deve introduzir a password de Supervisor para permitir ultrapassar o plafond.","OK","",64)
										Return .F.
									Else
										If !(Upper(Alltrim(cval))==Upper(Alltrim(uf_gerais_getParameter('ADM0000000187', 'TEXT'))))
											uf_perguntalt_chama("A password de Supervisor n�o est� correcta.","OK","",64)
											Return .F.
										Endif
									Endif
								Endif
							Else
								Return .F.
							Endif
						Endif
					Else
						If PrecisaRecibo = .F.
							If (lcValorTotal - (uCrsCLEF.eplafond - uCrsCLEF.esaldo)) > 0
								uf_perguntalt_chama("O plafond do cliente foi ultrapassado." + Chr(13) + "N�o pode concluir o atendimento.","OK","",64)
								Return .F.
							Endif
						Endif
					Endif
				Endif
			Endif
		Endif
	Endif



	&& verificar valores negativos em vales de adiantamento de reserva
	Select fi
	Scan For Upper(Alltrim(fi.ref)) == "V999999"
		If fi.etiliquido != 0
			Select fi
			Replace fi.Design With Alltrim(fi.Design) + ' (' + astr(fi.etiliquido,15,2) + ')'
			Replace fi.etiliquido With 0
		Endif
	Endscan

	&& remover bistamp das linhas de adiantamento de reservas regularizadas
	Select fi
	Scan For Alltrim(fi.ref) = "R000001" And !Empty(Alltrim(fi.ofistamp))
		Replace fi.bistamp With ''
	Endscan

	** Verificar se existem linhas sem refer�ncias e com qtt
	Local lcRefVazia, lcQttSRef
	Store '' To lcRefVazia
	Store .F. To lcQttSRef
	Select fi
	Go Top
	Scan
		If Empty(fi.ref) And fi.qtt<>0
			TEXT TO lcSQL NOSHOW TEXTMERGE
				select ref from st where design='<<alltrim(fi.design)>>' and site_nr=<<mysite_nr>>
			ENDTEXT
			If !uf_gerais_actGrelha("","uCrsPesqRefDesign", lcSQL)
				uf_perguntalt_chama("Ocorreu uma anomalia pesquisar a refer�ncia de uma linha sem a mesma. Contacte o suporte.","OK","",16)
				Return .F.
			Endif
			If Reccount("uCrsPesqRefDesign")=0
				lcQttSRef = .T.
			Else
				If !Empty(uCrsPesqRefDesign.ref)
					lcRefVazia = Alltrim(uCrsPesqRefDesign.ref)
					Select fi
					Replace fi.ref With lcRefVazia
				Else
					lcQttSRef = .T.
				Endif
			Endif
			fecha("uCrsPesqRefDesign")
		Endif
		Select fi
	Endscan
	If lcQttSRef == .T.
		uf_perguntalt_chama("EXISTE UMA OU MAIS LINHAS SEM REFER�NCIA PREENCHIDA. POR FAVOR VERIFIQUE.","OK","",48)
		lcQttSRef = .F.
		Select fi
		Go Top
		Return .F.
	Endif





	&& Calcular valor a ser colocado no cart�o
	Select uCrsatendCl
	If myTipoCartao == 'Valor' And Len(Alltrim(uCrsatendCl.nrcartao))>0
		uf_calcular_valor_cartao()
	Endif

	uf_gerais_regista_logs_passos("PAGAMENTO",nrAtendimento,"PAG0B","VALIDAR PAGAMENTO")
	**SELECT ft
	**brow


	&& Movido para depois do pagamento confirmado"
	&& Efectiva��o Receitas DEM
	*!*		IF EMPTY(uf_atendimento_receitasEfetivar())
	*!*			uf_pagamento_receitasAnular()
	*!*			RETURN .f.
	*!*		ENDIF

	&& passou valida��es - vai gravar o atendimento
	uf_PAGAMENTO_finalizarAtendimento()

Endfunc


** Calcular o n�mero de documentos que v�o mexer com caixa
Function uf_PAGAMENTO_calcularDCaixa

	Local lcCndD
	lcCntD = 0

	Select uCrsCabVendas
	Go Top
	Scan
		If uCrsCabVendas.modoPag=.F.
			lcCntD = lcCntD + 1
		Endif

		Select uCrsCabVendas
	Endscan

	Return lcCntD
Endfunc



** Comunica com o TPa  e valida opera��o de pagamento no Tpa
Function uf_pagamento_pagaTpa
	Lparameters lcAmount

	uf_gerais_regista_logs_passos("PAGAMENTO",nrAtendimento,"PAG00","PAGA TPA PAGAMENTO")

	If(lcAmount >0)
		Release lcResponseArray
		Dimension lcResponseArray(2)


		uf_tpa_operation(@lcResponseArray,"Aguarde...","","app.msg.purchase.operation",lcAmount )
		If(lcResponseArray(1)!=0) && not ok
			If(!uf_perguntalt_chama(Alltrim(lcResponseArray(2)) + Chr(13)  + "Pretende continuar com o atendimento?" ,"Sim","N�o",64))
				Return .F.
			Else
				Return .T.
			Endif
		Endif
		Release lcResponseArray
	Endif


	uf_gerais_regista_logs_passos("PAGAMENTO",nrAtendimento,"PAG01","PAGA TPA PAGAMENTO")

	Return .T.
Endfunc


Function uf_pagamento_pagaTpa_somaat
	Lparameters lcAmount



	If(lcAmount >0)
		Release lcResponseArray
		Dimension lcResponseArray(2)


		uf_tpa_operation(@lcResponseArray,"Aguarde...","","app.msg.purchase.operation",lcAmount )
		If(lcResponseArray(1)!=0) && not ok
			**IF(!uf_perguntalt_chama(ALLTRIM(lcResponseArray(2)) + CHR(13)  + "Pretende continuar com o atendimento?" ,"Sim","N�o",64))
			**	RETURN .f.
			**ELSE
			**	RETURN .t.
			**ENDIF
			uf_perguntalt_chama(Alltrim(lcResponseArray(2)) + Chr(13)  + "N�O � POSS�VEL CONTINUAR COM A OPERA��O", "OK" ,48)
			Return .F.
		Endif
		Release lcResponseArray
	Endif
	Return .T.
Endfunc


Function uf_pagamento_validaSePagaCentralConfiguradoMetodoPag
	Local lcMetodoCentralizado,lcPosModPag

	lcPosModPag= Recno("UcrsConfigModPag")

	Select UcrsConfigModPag
	Go Top
	Scan For Lower(Alltrim(UcrsConfigModPag.Design)) == "cashdro"
		If Found()
			lcMetodoCentralizado =.T.
		Endif

	Endscan

	Select UcrsConfigModPag
	Try
		Go lcPosModPag
	Catch
	Endtry

	If(lcMetodoCentralizado==.F.)
		Return .T.
	Endif


	If(uf_pagamento_validaPagCentralTerminal())
		Return .T.
	Endif

	Return .F.

Endfunc




** Valida se o terminal do Pag. centralizado est� configurado
Function uf_pagamento_validaPagCentralTerminal


	Local lcMetodoCashDro, lcTemStamp,lcPosModPag

	lcMetodoCentralizado=.F.
	lcTemStampCashdro = Vartype(myCashDroStamp)
	lcPosModPag= Recno("UcrsConfigModPag")


	Select UcrsConfigModPag
	Go Top
	Scan For Lower(Alltrim(UcrsConfigModPag.Design)) == "cashdro"
		If Found()
			lcMetodoCentralizado =.T.
		Endif

	Endscan


	Select UcrsConfigModPag
	Try
		Go lcPosModPag
	Catch
	Endtry


	If(lcTemStampCashdro !="U" And lcMetodoCentralizado==.T.)
		Return .T.
	Endif
	Return .F.
Endfunc



** Comunica com o cashDro e valida opera��o de devolucao no CashDro
Function uf_pagamento_devolveCashDro
	Lparameters lcAmount

	If(lcAmount >0)

		*!*	    lcResponseArray(1) - codigo da resposta do cashdro
		*!*	  	lcResponseArray(2) - mensagem enviada pelo cashdro
		*!*	  	lcResponseArray(3) - Estado no caso da mensagem de status
		*!*	  	lcResponseArray(4) - Ultimo request cashDro
		*!*	  	lcResponseArray(5) - MSG_ID
		*!*	  	lcResponseArray(6) - Stauts Code Request

		Release lcResponseArray
		Dimension lcResponseArray(6)

		uf_cashDro_operation(@lcResponseArray,"A comunicar com o CashDro...","app.msg.menu.operation_payment_request",lcAmount,"","0")


		&&Manualy Canceled
		If(Lower(Alltrim(lcResponseArray(4)))=="app.msg.menu.operation_cancel_request")
			uf_perguntalt_chama("A Opera��o foi cancelada.","OK","",64)
			Return "-1"
		Endif


		&&STATUS NOT OK
		If((Alltrim(lcResponseArray(1))!="1" And Alltrim(lcResponseArray(1))!="2")  Or Empty(Alltrim(lcResponseArray(1))) Or Isnull(lcResponseArray(1))) && not ok
			uf_perguntalt_chama(" Cashdro: " + Alltrim(lcResponseArray(2)) + Chr(13) + Chr(13) + "O Cashdro n�o conseguiu realizar a opera��o." + Chr(13),"Voltar Pagam.")
			Return "-1"
		Else

			&&REST REQUEST NOT OK
			If(lcResponseArray(6)!=200)
				Return "-1"
			Endif

			&&STATUS SALE
			If(!Empty(Alltrim(lcResponseArray(3))))
				Do Case
					Case Alltrim(lcResponseArray(3))== "2"
						uf_perguntalt_chama("A Opera��o foi fechada.","OK","",64)
						Return "-1"
					Case Alltrim(lcResponseArray(3))== "3"
						uf_perguntalt_chama("A Opera��o foi cancelada.","OK","",64)
						Return "-1"
					Case Alltrim(lcResponseArray(3))== "4"
						uf_perguntalt_chama("A Opera��o foi cancelada.","OK","",64)
						Return "-1"
					Case Alltrim(lcResponseArray(3))== "99"
						uf_perguntalt_chama("O valor da devolu��o n�o est� correcto, por favor, dirija-se ao cashdro.","OK","",64)
				Endcase
			Endif

		Endif
		&&devolve msg_id se tudo correu bem
		Return  Alltrim(lcResponseArray(5))
		Release lcResponseArray
	Endif

	Return "-1"
Endfunc


** Comunica com o cashDro e valida opera��o de pagamento no CashDro
Function uf_pagamento_pagaCashDro
	Lparameters lcAmount

	If(lcAmount >0)

		*!*	    lcResponseArray(1) - codigo da resposta do cashdro
		*!*	  	lcResponseArray(2) - mensagem enviada pelo cashdro
		*!*	  	lcResponseArray(3) - Estado no caso da mensagem de status
		*!*	  	lcResponseArray(4) - Ultimo request cashDro
		*!*	  	lcResponseArray(5) - MSG_ID
		*!*	  	lcResponseArray(6) - Stauts Code Request

		Release lcResponseArray
		Dimension lcResponseArray(6)

		uf_cashDro_operation(@lcResponseArray,"A comunicar com o CashDro...","app.msg.menu.operation_sale_request",lcAmount,"","0")


		&&Manualy Canceled
		If(Lower(Alltrim(lcResponseArray(4)))=="app.msg.menu.operation_cancel_request")
			uf_perguntalt_chama("A Opera��o foi cancelada.","OK","",64)
			Return "-1"
		Endif


		&&STATUS NOT OK
		If((Alltrim(lcResponseArray(1))!="1" And Alltrim(lcResponseArray(1))!="2")  Or Empty(Alltrim(lcResponseArray(1))) Or Isnull(lcResponseArray(1))) && not ok
			uf_perguntalt_chama(" Cashdro: " + Alltrim(lcResponseArray(2)) + Chr(13) + Chr(13) + "O Cashdro n�o conseguiu realizar a opera��o." + Chr(13),"Voltar Pagam.")
			Return "-1"
		Else

			&&REST REQUEST NOT OK
			If(lcResponseArray(6)!=200)
				Return "-1"
			Endif



			&&STATUS SALE
			If(!Empty(Alltrim(lcResponseArray(3))))
				Do Case
					Case Alltrim(lcResponseArray(3))== "2"
						uf_perguntalt_chama("A Opera��o foi fechada.","OK","",64)
						Return "-1"
					Case Alltrim(lcResponseArray(3))== "3"
						uf_perguntalt_chama("A Opera��o foi cancelada.","OK","",64)
						Return "-1"
					Case Alltrim(lcResponseArray(3))== "4"
						uf_perguntalt_chama("A Opera��o foi cancelada.","OK","",64)
						Return "-1"
					Case Alltrim(lcResponseArray(3))== "99"
						uf_perguntalt_chama("O troco n�o est� correcto, por favor, dirija-se ao cashdro.","OK","",64)
				Endcase
			Endif

		Endif
		&&devolve msg_id se tudo correu bem
		Return  Alltrim(lcResponseArray(5))
		Release lcResponseArray
	Endif

	Return "-1"
Endfunc






** Finalizar Atendimento - Grvar Vendas // Grvar Regs // Gravar Pag Central // Imp Talao // Imp Receita
Function uf_PAGAMENTO_finalizarAtendimento

	uf_gerais_regista_logs_passos("PAGAMENTO",nrAtendimento,"PAG02","FINALIZAR PAGAMENTO")

	Local lcValidaInsercaoDocumento, lcSQLInsertDoc, lcSQLInsertDocAux, lcNrVenda, lcSQLDadosPsico, lcSqlCert, lcVendaCount, lcSQLPagCentral, lcSQLFtDeclare , lcOldClNo, lcOldClEstab
	Store '' To lcSQLInsertDoc, lcSQLInsertDocAux, lcSQLDadosPsico, lcSqlCert, lcSQLPagCentral, lcSQLFtDeclare
	Store .F. To lcValidaInsercaoDocumento
	Store 0 To lcNrVenda, lcVendaCount, lcOldClNo

	Public InfoVendaResposta
	Store .T. To InfoVendaResposta

	Select * From uCrsCabVendas Where Alltrim(receita_tipo)=='RM' Into Cursor uCrsCabVendasprint Readwrite

	Select * From dadosPsico Into Cursor dadospsicoaux Readwrite

	Create Cursor uCrsFiIns (fistamp c(30))

	&& construir cursor com  linhas copiadas da BI para fechar documentos
	Select ref, Design, qtt, epv ,etiliquido, iva, tabiva, ivaincl, armazem,ecusto, desconto, epcp ,bistamp, bostamp, .F. As act From fi Where !Empty(fi.bostamp) And !Empty(fi.ref) And Alltrim(fi.nmdos)=='Encomenda de Cliente' Order By bostamp, bistamp Into Cursor ucrsLinFiBi Readwrite
	Select Distinct bostamp, .F. As act, tipor From fi Where !Empty(fi.bistamp) And Alltrim(fi.nmdos)=='Encomenda de Cliente' Into Cursor ucrsLinFiBo Readwrite


	Local  lcMbPc
	Store 0 To  lcMbPc

	If Empty(PAGAMENTO.txtMB.Value)
		lcMbPc = 0
	Else
		lcMbPc = Val(PAGAMENTO.txtMB.Value)
	Endif

	Local lcAmount
	lcAmount  = lcMbPc

	&&Pagamento Multibanco e Visa
	If(Vartype(myTpaStamp)!="U" And lcAmount>0)
		If(!uf_pagamento_pagaTpa(lcAmount))
			Return .F.
		Endif
	Endif

	If !uf_gerais_actGrelha("","uc_modosPagExterno", "exec up_vendas_checkModoPagExterno")

		uf_perguntalt_chama("Erro a verificar modos de pagamento externo. Por favor contacte o suporte.","OK","",16)
		Return .F.

	Endif

	If Reccount("uc_modosPagExterno") > 0

		Create Cursor uc_pagsExterno(modoPag c(10), tlmvl c(50), email c(254), duracao c(50), descricao M, valor N(14,2), tipoPagExt N(3), tipoPagExtDesc c(100), receiverID c(100), receiverName c(100), no N(9), estab N(5), nrAtend c(20))
		Local uv_valor, uv_curCursor

		Select uc_modosPagExterno
		Go Top

		Scan

			uv_valor = 0

			Do Case
				Case Alltrim(uc_modosPagExterno.ref) = "05"

					uv_valor = Val(PAGAMENTO.txtModPag3.Value)

					If uv_valor <> 0 And !Used("uc_dadosPagExterno05")
						uf_perguntalt_chama("N�o preencheu os dados para Pagamento Externo:" + Chr(13) + Alltrim(PAGAMENTO.btnModPag3.Label7.Caption),"OK","",16)
						Return .F.
					Endif

				Case Alltrim(uc_modosPagExterno.ref) = "06"

					uv_valor = Val(PAGAMENTO.txtModPag4.Value)

					If uv_valor <> 0 And !Used("uc_dadosPagExterno06")
						uf_perguntalt_chama("N�o preencheu os dados para Pagamento Externo:" + Chr(13) + Alltrim(PAGAMENTO.btnModPag4.Label7.Caption),"OK","",16)
						Return .F.
					Endif

				Case Alltrim(uc_modosPagExterno.ref) = "07"

					uv_valor = Val(PAGAMENTO.txtModPag5.Value)

					If uv_valor <> 0 And !Used("uc_dadosPagExterno07")
						uf_perguntalt_chama("N�o preencheu os dados para Pagamento Externo:" + Chr(13) + Alltrim(PAGAMENTO.btnModPag5.Label7.Caption),"OK","",16)
						Return .F.
					Endif

				Case Alltrim(uc_modosPagExterno.ref) = "08"

					uv_valor = Val(PAGAMENTO.txtModPag6.Value)

					If uv_valor <> 0 And !Used("uc_dadosPagExterno08")
						uf_perguntalt_chama("N�o preencheu os dados para Pagamento Externo:" + Chr(13) + Alltrim(PAGAMENTO.btnModPag6.Label7.Caption),"OK","",16)
						Return .F.
					Endif

				Otherwise
					Loop
			Endcase

			uv_curCursor = "uc_dadosPagExterno" + Alltrim(uc_modosPagExterno.ref)

			If uv_valor <> 0 And Used(uv_curCursor)

				Select uCrsCLEF

				Select uc_pagsExterno
				Append Blank

				Replace uc_pagsExterno.modoPag With Alltrim(uc_modosPagExterno.ref)
				Replace uc_pagsExterno.tlmvl With Alltrim(&uv_curCursor..tlmvl)
				Replace uc_pagsExterno.email With Alltrim(&uv_curCursor..email)
				Replace uc_pagsExterno.duracao With Alltrim(&uv_curCursor..duracao)
				Replace uc_pagsExterno.descricao With Alltrim(&uv_curCursor..descricao)
				Replace uc_pagsExterno.valor With uv_valor
				Replace uc_pagsExterno.tipoPagExt With uc_modosPagExterno.tipoPagExt
				Replace uc_pagsExterno.tipoPagExtDesc With uc_modosPagExterno.tipoPagExtDesc
				Replace uc_pagsExterno.receiverID With uc_modosPagExterno.receiverID
				Replace uc_pagsExterno.receiverName With uc_modosPagExterno.receiverName
				Replace uc_pagsExterno.no With uCrsCLEF.no
				Replace uc_pagsExterno.estab With uCrsCLEF.estab
				Replace uc_pagsExterno.nrAtend With Alltrim(nrAtendimento)

			Endif


			Select uc_modosPagExterno
		Endscan

		Select uc_pagsExterno

		If Reccount("uc_pagsExterno") > 0
			If !uf_pagamento_reqPagExterno()
				uf_perguntalt_chama("N�o foi poss�vel criar os pagamentos externos. Por favor contacte o suporte.","OK","",16)
				Return .F.
			Endif
		Endif

	Endif




	If lcResSAD = .T. Or lcResAD100 = .T. Or lcResAdParc = .T.
		Select * From uCrsCabVendas Where At('Reserva',Design)>0 Into Cursor ucrsCabVendasres Readwrite

		uf_gerais_regista_logs_passos("PAGAMENTO",nrAtendimento,"PAG03","PROCESSA RESERVA PAGAMENTO")

		Select fi

		If !uf_pagamento_procreservas()
			Return .F.
		Endif
		Select fi

		Select * From fi Into Cursor ucrstempfiRes Readwrite
		If Used("ucrsCabVendasres")
			fecha("ucrsCabVendasres")
		Endif

		uf_gerais_regista_logs_passos("PAGAMENTO",nrAtendimento,"PAG04","PROCESSA RESERVA PAGAMENTO")
	Endif

	If InfoVendaResposta = .F.
		uf_PAGAMENTO_sair(.T.)
		Return .F.
	Endif

	** atualizar uCrsCabVendas
	Select fi
	Go Top
	Scan
		If !Empty(fi.tipor)
			Select uCrsCabVendas
			Go Top
			Update uCrsCabVendas Set uCrsCabVendas.Design = Alltrim(fi.Design) Where uCrsCabVendas.Lordem=fi.Lordem
		Endif
		Select fi
	Endscan

	Select fi
	Delete From fi Where Empty(fi.ref) And Empty(fi.Design) And fi.qtt=0 And fi.etiliquido=0


	uf_gerais_regista_logs_passos("PAGAMENTO",nrAtendimento,"PAG05","PRINT PAGAMENTO")


	If Used("uCrstempfiRes") And Reccount("uCrstempfiRes") < 2
		**	MESSAGEBOX('1')
		**uf_perguntalt_chama("1 verifica curtor uCrsImpTalaoPos ?","Sim","N�o")

		*******************
		If Used("uCrsImpTalaoPos")
			TEXT TO lcSQL NOSHOW TEXTMERGE
				select '' as codigo, 0 as Ucrscptpla
				union all
				select codigo, obriga_nrReceita as Ucrscptpla from cptpla (nolock) --where obriga_nrReceita=0
			ENDTEXT
			uf_gerais_actGrelha("", "Ucrscptpla",lcSQL)

			Select * From uCrsImpTalaoPos INNER Join Ucrscptpla On Ucrscptpla.codigo=uCrsImpTalaoPos.plano;
				WHERE (((!Empty(uCrsImpTalaoPos.nrreceita) Or (Empty(uCrsImpTalaoPos.nrreceita) And Ucrscptpla.Ucrscptpla=0)) And (uCrsImpTalaoPos.rm=.T. Or (uCrsImpTalaoPos.rm=.F. And uCrsImpTalaoPos.planocomp=.T.))) Or (Alltrim(uCrsImpTalaoPos.lote)=='98' Or Alltrim(uCrsImpTalaoPos.lote)=='99')) And uCrsImpTalaoPos.impresso=.F.  Into Cursor uCrsImpTalaoPosTMP Readwrite

			Select * From uCrsImpTalaoPosTMP Order By Lordem Into Cursor uCrsImpTalaoPosTMP Readwrite

			fecha("Ucrscptpla")

			If Used("uCrsImpTalaoPosTMP")
				Local lcpedeconf
				Store .F. To lcpedeaviso
				Select uCrsImpTalaoPosTMP
				If Reccount("uCrsImpTalaoPosTMP")>1
					lcpedeaviso = .T.
				Endif

				Select uCrsImpTalaoPosTMP
				Go Top
				Scan
					If ucrse1.OrdemImpressaoReceita == .F.
						If uCrsImpTalaoPosTMP.Lordem <> 99999 Or (Alltrim(uCrsImpTalaoPosTMP.lote)=='98' Or Alltrim(uCrsImpTalaoPosTMP.lote)=='99')
							**IF uf_gerais_getParameter_site('ADM0000000136', 'BOOL', mySite)
							Messagebox(5)
							If uf_gerais_getUmValor("B_terminal", "usaDriverWin", "terminal = '" + Alltrim(myTerm) + "'")
								Select uCrsImpTalaoPosTMP
								uf_imprimirpos_impRecTalaoComum_talaoVerso(uCrsImpTalaoPosTMP.stamp, 1, .F., .T.)
							Else
								Select uCrsImpTalaoPosTMP
								uf_imprimirpos_impRec_V2(uCrsImpTalaoPosTMP.stamp, 1, "ATENDIMENTO", lcpedeaviso)
							Endif

						Endif
						Select uCrsImpTalaoPosTMP
						If uCrsImpTalaoPosTMP.rm=.T. Or (uCrsImpTalaoPosTMP.planocomp = .T. And (Alltrim(uCrsImpTalaoPosTMP.lote)=='98' Or Alltrim(uCrsImpTalaoPosTMP.lote)=='99'))
							**IF uf_gerais_getParameter_site('ADM0000000136', 'BOOL', mySite)
							Messagebox(6)
							If uf_gerais_getUmValor("B_terminal", "usaDriverWin", "terminal = '" + Alltrim(myTerm) + "'")
								Select uCrsImpTalaoPosTMP
								uf_imprimirpos_impRecTalaoComum_talaoVerso(uCrsImpTalaoPosTMP.stamp, 2, .F., .T.)
							Else
								uf_imprimirpos_impRec_V2(uCrsImpTalaoPosTMP.stamp, 2, "ATENDIMENTO", lcpedeaviso)
							Endif
						Endif
						Select uCrsImpTalaoPosTMP
						**IF uCrsImpTalaoPosTMP.rm=.f. AND (uCrsImpTalaoPosTMP.planocomp = .t. AND (ALLTRIM(uCrsImpTalaoPosTMP.lote)=='96' OR ALLTRIM(uCrsImpTalaoPosTMP.lote)=='97'))
						**	uf_imprimirpos_impRec_V2(uCrsImpTalaoPosTMP.stamp, 2, "ATENDIMENTO", lcpedeaviso)
						**ENDIF
					Else
						**uf_perguntalt_chama("ver cursores aqui","Sim","N�o")
						Select uCrsImpTalaoPosTMP
						If uCrsImpTalaoPosTMP.rm=.T. Or (uCrsImpTalaoPosTMP.planocomp = .T. And (Alltrim(uCrsImpTalaoPosTMP.lote)=='98' Or Alltrim(uCrsImpTalaoPosTMP.lote)=='99'))
							**IF uf_gerais_getParameter_site('ADM0000000136', 'BOOL', mySite)
							Messagebox(7)
							If uf_gerais_getUmValor("B_terminal", "usaDriverWin", "terminal = '" + Alltrim(myTerm) + "'")
								uf_imprimirpos_impRecTalaoComum_talaoVerso(uCrsImpTalaoPosTMP.stamp, 2, .F., .T.)
							Else
								uf_imprimirpos_impRec_V2(uCrsImpTalaoPosTMP.stamp, 2, "ATENDIMENTO", lcpedeaviso)
							Endif
						Endif
						Select uCrsImpTalaoPosTMP
						**IF uCrsImpTalaoPosTMP.rm=.f. AND (uCrsImpTalaoPosTMP.planocomp = .t. AND (ALLTRIM(uCrsImpTalaoPosTMP.lote)=='96' OR ALLTRIM(uCrsImpTalaoPosTMP.lote)=='97'))
						**	uf_imprimirpos_impRec_V2(uCrsImpTalaoPosTMP.stamp, 2, "ATENDIMENTO", lcpedeaviso)
						**ENDIF
						If uCrsImpTalaoPosTMP.Lordem <> 99999 Or (Alltrim(uCrsImpTalaoPosTMP.lote)=='98' Or Alltrim(uCrsImpTalaoPosTMP.lote)=='99')
							**IF uf_gerais_getParameter_site('ADM0000000136', 'BOOL', mySite)
							Messagebox(8)
							If uf_gerais_getUmValor("B_terminal", "usaDriverWin", "terminal = '" + Alltrim(myTerm) + "'")
								Select uCrsImpTalaoPosTMP
								uf_imprimirpos_impRecTalaoComum_talaoVerso(uCrsImpTalaoPosTMP.stamp, 1, .F., .T.)
							Else
								Select uCrsImpTalaoPosTMP
								uf_imprimirpos_impRec_V2(uCrsImpTalaoPosTMP.stamp, 1, "ATENDIMENTO", lcpedeaviso)
							Endif

						Endif
					Endif
					Select uCrsImpTalaoPosTMP
					Replace uCrsImpTalaoPosTMP.impresso With .T.
					Update uCrsImpTalaoPos Set impresso=.T. Where Alltrim(uCrsImpTalaoPos.stamp)==Alltrim(uCrsImpTalaoPosTMP.stamp)
				Endscan
			Endif
			fecha("uCrsImpTalaoPosTMP")
		Endif

		*******************

		uf_gerais_regista_logs_passos("PAGAMENTO",nrAtendimento,"PAG06","PRINT PAGAMENTO")

		fecha("uCrstempfiRes")

		&& processamento das linhas dos dossiers afetos � venda
		If Used("ucrsLinFiBi")
			uf_pagamento_proclinhasenc()
		Endif

		&& fecho de documentos com as linhas totalmente satisfeitas
		If Used("ucrsLinFiBo") Or Used("ucrsLinBoFecho")
			uf_pagamento_fechalinhasenc()
		Endif
		Messagebox(9)

		**IF uf_gerais_getParameter_site('ADM0000000136', 'BOOL', mySite)
		If uf_gerais_getUmValor("B_terminal", "usaDriverWin", "terminal = '" + Alltrim(myTerm) + "'")
			uf_PAGAMENTO_eventoImpressaoPos(.F.)
		Endif

		If Used("ucrsLinFiBi")
			fecha("ucrsLinFiBi")
		Endif
		If Used("ucrsLinFiBo")
			fecha("ucrsLinFiBo")
		Endif
		If Used("ucrsLinBoFecho")
			fecha("ucrsLinBoFecho")
		Endif

		uf_gerais_regista_logs_passos("PAGAMENTO",nrAtendimento,"PAG07","FIM PAGAMENTO")

		uf_PAGAMENTO_limparVendasFimVd()


		uf_PAGAMENTO_sair(.T.)
		uf_atendimento_ValoresDefeito()
	Else
		**MESSAGEBOX('2')
		fecha("uCrstempfiRes")
		&& GUARDAR N� DE VENDAS
		If Used("uCrsCabVendas")
			Local lcNrVendas
			lcNrVendas = 0
			Select uCrsCabVendas
			Calculate Cnt() To lcNrVendas

			If lcNrVendas == 0
				uf_perguntalt_chama("N�o foi encontrada informa��o geral da venda. Por favor feche o ecr� de pagamento e volte a abrir." + Chr(13) + "Se o problema persistir por favor contacte o suporte.","OK","",16)

				&& anula receitas DEM registadas anteriormente
				&&uf_pagamento_receitasAnular()

				Return .F.
			Endif
		Else
			uf_perguntalt_chama("N�o foi encontrada informa��o geral da venda. Por favor feche o ecr� de pagamento e volte a abrir." + Chr(13) + "Se o problema persistir por favor contacte o suporte.","OK","",16)

			&& anula receitas DEM registadas anteriormente
			&&uf_pagamento_receitasAnular()

			Return .F.
		Endif
		**

		&& DEFINIR VARI�VEIS
		Local lcFtstamp, lcFt2Stamp, lcFiStamp, lcHora, lcPdata, lcNmDoc, lcCod, lcReceita, lcVia, lcCod2, lcDesign, lcDesign2, lcAbrev, lcAbrev2, lcTran, lcLtStamp2, lcNewTokenCompart,lcSqlFi2
		Store "" To lcFtstamp, lcFt2Stamp, lcFiStamp, lcHora, lcNmDoc, lcCod, lcReceita, lcVia, lcCod2, lcDesign, lcDesign2, lcAbrev, lcAbrev2, lcTran, lcLtStamp2, lcNewTokenCompart, lcSqlFi2

		Local lcClNo, lcClEstab, lcClNome, lcModoFac, lcTipoDoc, lcNdoc, lcNcont
		Store 0 To lcClNo, lcClEstab, lcClNome, lcTipoDoc, lcNdoc
		Store '' To lcModoFac, lcNcont

		Local lcTotQtt, lcQtt1, lcEtotal, lcEttIliq, lcEcusto, lcEttIva, EivaIn1, EivaIn2, EivaIn3, EivaIn4, EivaIn5, EivaIn6, EivaIn7, EivaIn8, EivaIn9, EivaIn10, EivaIn11, EivaIn12, EivaIn13, EivaV1, EivaV2, EivaV3, EivaV4, EivaV5, EivaV6, EivaV7, EivaV8, EivaV9, EivaV10, EivaV11, EivaV12, EivaV13
		Store 0 To lcTotQtt, lcQtt1, lcEtotal, lcEttIliq, lcEcusto, lcEttIva, EivaIn1, EivaIn2, EivaIn3, EivaIn4, EivaIn5, EivaIn6, EivaIn7, EivaIn8, EivaIn9, EivaIn10, EivaIn11, EivaIn12, EivaIn13, EivaV1, EivaV2, EivaV3, EivaV4, EivaV5, EivaV6, EivaV7, EivaV8, EivaV9, EivaV10, EivaV11, EivaV12, EivaV13

		Local lcDesc, lcTotDesc, lcPrecoSemIva, lcCntModPag, lcTroco, lcDinheiro, lcVisa, lcMB, lcCheque, lcModPag3, lcModPag4, lcModPag5, lcModPag6
		Store 0 To lcDesc, lcTotDesc, lcPrecoSemIva, lcCntModPag, lcTroco, lcDinheiro, lcVisa, lcMB, lcCheque, lcModPag3, lcModPag4, lcModPag5, lcModPag6

		Local lcValida, lcQttSRef
		Store .F. To lcValida, lcQttSRef

		Local lcOkFt, lcOkFt2, lcOkFi, lcOkPagCentral

		Public lcSusp
		Store .F. To lcSusp, lcOkPagCentral

		&& DADOS DO CLIENTE - adicionado ncont 12/11/2015 LL
		Select ft
		If !Empty(ft.no)
			lcClNo		= ft.no
			lcClEstab	= ft.estab
			lcClNome	= Alltrim(ft.Nome)
			lcNcont		= Alltrim(ft.ncont)
			lcOldClNo	= ft.no
			lcOldClEstab= ft.estab
		Else
			uf_perguntalt_chama("A INFORMA��O RELATIVA AO CLIENTE FOI PERDIDA. POR QUEST�ES DE SEGURAN�A O SOFTWARE VAI SER ENCERRADO.","OK","",16)

			&& anula receitas DEM registadas anteriormente
			&&uf_pagamento_receitasAnular()

			Quit
		Endif

		Local lcFindNdoc, lcFindDocNr
		Select ft
		lcFindNdoc = ft.ndoc
		lcFindDocNr = ft.fno

		If Used("uCrsCLEF")
			Select uCrsCLEF
			Go Top
			lcModoFac = Upper(Alltrim(uCrsCLEF.modofact))
		Endif

		lcCntD = uf_PAGAMENTO_calcularDCaixa()

		If lcNrVendas != lcCntD
			lcPdata = uf_PAGAMENTO_dataVencimentoPos(lcClNo, lcClEstab)
		Else
			lcPdata=Datetime()+(difhoraria*3600)
			**lcPdata=datetime())
		Endif

		uf_gerais_regista_logs_passos("PAGAMENTO",nrAtendimento,"PAG08","ABRE GAVETA PAGAMENTO")

		** Abre a gaveta caso n�o seja a cr�dito
		If PAGAMENTO.txtValAtend.Value > 0
			uf_PAGAMENTO_abreGaveta(.T.)
		Endif

		&& Guardar dados para Pagamento Central
		Local lcTrocoPc, lcDinheiroPc, lcMbPc, lcVisaPc, lcChequePc
		Store 0 To lcTrocoPc, lcDinheiroPc, lcMbPc, lcVisaPc, lcChequePc

		If Empty(PAGAMENTO.txtTroco.Value)
			lcTrocoPc = 0
		Else
			lcTrocoPc = PAGAMENTO.txtTroco.Value
		Endif
		If Empty(PAGAMENTO.txtDinheiro.Value)
			lcDinheiroPc = 0
		Else
			lcDinheiroPc = Val(PAGAMENTO.txtDinheiro.Value)
		Endif
		If Empty(PAGAMENTO.txtMB.Value)
			lcMbPc = 0
		Else
			lcMbPc = Val(PAGAMENTO.txtMB.Value)
		Endif
		If Empty(PAGAMENTO.txtVisa.Value)
			lcVisaPc = 0
		Else
			lcVisaPc = Val(PAGAMENTO.txtVisa.Value)
		Endif
		If Empty(PAGAMENTO.txtCheque.Value)
			lcChequePc = 0
		Else
			lcChequePc = Val(PAGAMENTO.txtCheque.Value)
		Endif
		If Empty(PAGAMENTO.txtModPag3.Value)
			lcModPag3 = 0
		Else
			lcModPag3 = Val(PAGAMENTO.txtModPag3.Value)
		Endif
		If Empty(PAGAMENTO.txtModPag4.Value)
			lcModPag4 = 0
		Else
			lcModPag4 = Val(PAGAMENTO.txtModPag4.Value)
		Endif
		If Empty(PAGAMENTO.txtModPag5.Value)
			lcModPag5 = 0
		Else
			lcModPag5 = Val(PAGAMENTO.txtModPag5.Value)
		Endif
		If Empty(PAGAMENTO.txtModPag6.Value)
			lcModPag6 = 0
		Else
			lcModPag6 = Val(PAGAMENTO.txtModPag6.Value)
		Endif

		&&LOCAL lcAmount
		&&lcAmount  = lcMbPc

		&&Pagamento Multibanco e Visa
		&&IF(vartype(myTpaStamp)!="U" and lcAmount>0)
		&&	IF(!uf_pagamento_pagaTpa(lcAmount))
		&&		RETURN .f.
		&&	ENDIF
		&&ENDIF

		&&Pagamento / devolucao CashDro
		*******************************************************

		Local lcOpID
		lcOpID = "0"
		Local lcDevCashDro

		&&valida se tem CashDro e se terminal est� configurado
		If(!uf_pagamento_validaSePagaCentralConfiguradoMetodoPag() And lcModPag5>0)
			uf_perguntalt_chama("O terminal n�o est� configurado.","OK","",64)
			Return .F.
		Endif

		&&Pagamento CashDro
		If(Vartype(myCashDroStamp)!="U" And lcModPag5 >0)
			lcOpID = Alltrim(uf_pagamento_pagaCashDro(lcModPag5))
			If(lcOpID=="-1")
				Return .F.
			Endif
		Endif

		&&Devolu��o CashDro
		If(lcModPag5 <0 And Vartype(myCashDroStamp)!="U" )
			lcOpID = Alltrim(uf_pagamento_devolveCashDro(Abs(lcModPag5 )))
			If(lcOpID=="-1")
				Return .F.
			Endif
		Endif



		*************************************************
		&& Efetiva��o da dispensa das embalagens
		&& Sa�das

		uf_gerais_regista_logs_passos("PAGAMENTO",nrAtendimento,"PAG09","NMVO COMUNICA PAGAMENTO")


		Select fi_trans_info
		Go Top
		Select * From fi_trans_info Where fi_trans_info.tipo='S' Into Cursor fi_trans_info_s Readwrite
		If Reccount("fi_trans_info_s")>0
			uf_Atendimento_DispensePackVerify()
		Endif
		fecha("fi_trans_info_s")
		&& Entradas
		Select fi_trans_info
		Go Top
		Select * From fi_trans_info Where fi_trans_info.tipo='E' Into Cursor fi_trans_info_e Readwrite
		If Reccount("fi_trans_info_e")>0
			uf_Atendimento_UndoDispenseSinglePack()
		Endif
		fecha("fi_trans_info_e")

		Select fi_trans_info
		Go Top
		**	DELETE FROM fi_trans_info WHERE EMPTY(fi_trans_info.batchid)

		uf_gerais_regista_logs_passos("PAGAMENTO",nrAtendimento,"PAG10","NMVO FIM COMUNICA PAGAMENTO")

		uf_valida_info_caixas_dispensa()

		&& Efectiva��o SNS Externos
		&&covid comentario inicio
		uf_gerais_regista_logs_passos("PAGAMENTO",nrAtendimento,"PAG10A","uf_valida_info_caixas_dispensa PAGAMENTO")


		If Empty(uf_atendimento_receitasEfetivarSNSExterno())
			uf_atendimento_receitasAnularSNSExterno()
			Return .F.
		Endif
		&&covid comentario fim

		uf_gerais_regista_logs_passos("PAGAMENTO",nrAtendimento,"PAG11","COMPART EXTERNA PAGAMENTO")

		&& Efetiva��o da comparticipa��o
		If !Empty(TokenCompart) And CompSemSimul = .F. And !uf_pagamento_validaSeEfectivado(TokenCompart) &&AND uf_atendimento_validaEfectivacaoCompartExterna()
			lcNewTokenCompart = uf_gerais_stamp()
			If Empty(uf_compart_efetiva(TokenCompart, lcNewTokenCompart))
				Return .F.
			Endif
		Endif

		uf_gerais_regista_logs_passos("PAGAMENTO",nrAtendimento,"PAG12","COMPART SNS PAGAMENTO")


		&& Efectiva��o Receitas DEM
		If Empty(uf_atendimento_receitasEfetivar())
			uf_pagamento_receitasAnular()
			Return .F.
		Endif

		uf_gerais_regista_logs_passos("PAGAMENTO",nrAtendimento,"PAG13","FIM COMPART SNS PAGAMENTO")

		&& Guardar Valor Devolucoes Reais
		Local lcDevRealPc, lcDevRealPc1, lcDevRealPc2, lcNtCreditoPc, lcCreditoPc, lcTotalBruto
		Store 0 To lcDevRealPc, lcDevRealPc1, lcDevRealPc2, lcNtCreditoPc, lcCreditoPc, lcTotalBruto

		If Used("uCrsVale")
			Select ucrsvale

			&& devolu��es de produtos de vendas a dinheiro
			Calculate Sum(ucrsvale.etiliquido) To lcDevRealPc For ucrsvale.dev==1 And ucrsvale.lancacc==.F.
			&& devolu��es de produtos de facturas pagas na totalidade (qtt)
			Calculate Sum(ucrsvale.etiliquido) To lcDevRealPc1 For ucrsvale.dev==1 And ucrsvale.lancacc==.T. And ucrsvale.fmarcada==.T. And (ucrsvale.etiliquido=ucrsvale.eaquisicao Or (ucrsvale.qtt<=ucrsvale.eaquisicao))
			&& devolu��es de produtos de facturas pagas parcialmente
			Calculate Sum(ucrsvale.eaquisicao*ucrsvale.etiliquido/ucrsvale.qtt) To lcDevRealPc2 For ucrsvale.dev==1 And ucrsvale.lancacc==.T. And ucrsvale.fmarcada==.T. And !(ucrsvale.etiliquido=ucrsvale.eaquisicao Or (ucrsvale.qtt<=ucrsvale.eaquisicao))
			&& somar tudo
			lcDevRealPc = lcDevRealPc + lcDevRealPc1 + lcDevRealPc2

			&& total regulariza��es/devolu��es
			Calculate Sum(ucrsvale.etiliquido) To lcNtCreditoPc For !(ucrsvale.factPago==.F. And (ucrsvale.lancacc==.T. And ucrsvale.dev==0 And ucrsvale.Fact==.T.) And ucrsvale.fmarcada==.F.)
		Endif

		&& total cr�ditos
		Select uCrsCabVendas
		Calculate Sum(uCrsCabVendas.etiliquido) To lcCreditoPc For modoPag==.T.
		Calculate Sum(uCrsCabVendas.etiliquido) To lcTotalBruto For modoPag==.F.


		&& criar instru��o Gravar atendimento na dbo.B_pagCentral
		If Empty(mySsStamp)
			mySsStamp = uf_gerais_stamp()
		Endif

		Local lcDeveFechar, lcFechaStamp
		lcDeveFechar = .F.
		lcFechaStamp = ''



		** fecha vendas sem dinheiro
		If uf_gerais_validaPagCentralDinheiro()
			If(Round((lcDinheiroPc-lcTrocoPc),2))!=0
				lcDeveFechar =.F.
			Else
				lcDeveFechar =.T.
				lcFechaStamp = uf_gerais_stamp()
			Endif

		Endif

		lcNratendPrint = Alltrim(nrAtendimento)
		lcNoClPrint = lcClNo

		uv_geraVale = .F.

		If Round(PAGAMENTO.txtValAtend.Value,2) < 0
			If !Empty(Val(PAGAMENTO.txtModPag3.Value)) And Upper(Alltrim(PAGAMENTO.btnModPag3.Label7.Caption)) == "VALES"
				uv_geraVale = .T.
			Endif
			If !Empty(Val(PAGAMENTO.txtModPag4.Value)) And Upper(Alltrim(PAGAMENTO.btnModPag4.Label7.Caption)) == "VALES"
				uv_geraVale = .T.
			Endif
			If !Empty(Val(PAGAMENTO.txtModPag5.Value)) And Upper(Alltrim(PAGAMENTO.btnModPag5.Label7.Caption)) == "VALES"
				uv_geraVale = .T.
			Endif
			If !Empty(Val(PAGAMENTO.txtModPag6.Value)) And Upper(Alltrim(PAGAMENTO.btnModPag6.Label7.Caption)) == "VALES"
				uv_geraVale = .T.
			Endif
		Endif


		uf_gerais_regista_logs_passos("PAGAMENTO",nrAtendimento,"PAG14","CRIA MOVIMENTO CAIXA PAGAMENTO")
		uf_gerais_regista_logs_passos("B_PAGCENTRAL",nrAtendimento,"b_pag6","insert antes uf_PAGAMENTO_finalizarAtendimento")

		lcSQLPagCentral = ''
		If !uv_geraVale
			TEXT TO lcSQLPagCentral TEXTMERGE NOSHOW
				INSERT INTO B_pagCentral
					(stamp, nrAtend, nrVendas, total, ano, no, estab, vendedor
					,nome ,terminal, terminal_nome,	evdinheiro, epaga1, epaga2, echtotal
					,evdinheiro_semTroco, etroco, total_bruto, devolucoes, ntCredito, creditos, cxstamp, ssstamp, site
					,fechado, epaga3, epaga4, epaga5, epaga6
					,odata, udata, valpagmoeda, moedapag, fechastamp)
				VALUES
					('<<astr(YEAR(DATE())) + ALLTRIM(nrAtendimento)>>',	'<<ALLTRIM(nrAtendimento)>>', <<lcNrVendas>>, <<Round(PAGAMENTO.txtValAtend.value,2)>>,	<<YEAR(DATE())>>, <<lcClNo>>, <<lcClEstab>>, <<ch_vendedor>>
					,'<<ALLTRIM(ch_vendnm)>>', <<myTermNo>>, '<<ALLTRIM(myTerm)>>',	<<ROUND((lcDinheiroPc-lcTrocoPc),2)>>, <<ROUND(lcVisaPc,2)>>, <<ROUND(lcMbPc,2)>>, <<ROUND(lcChequePc,2)>>, <<ROUND(lcDinheiroPc,2)>>
					,<<ROUND(lcTrocoPc,2)>>, <<ROUND(lcTotalBruto,2)>>, <<ROUND(lcDevRealPc,2)>>, <<ROUND(lcNtCreditoPc,2)>>, <<ROUND(lcCreditoPc,2)>>,	'<<ALLTRIM(myCxStamp)>>', '<<ALLTRIM(mySsStamp)>>', '<<ALLTRIM(mySite)>>'
					,<<IIF(Round(PAGAMENTO.txtValAtend.value,2) == 0 OR !EMPTY(lcDeveFechar), "1", "0")>>, <<lcModPag3>>, <<lcModPag4>>, <<lcModPag5>>, <<lcModPag6>>
					, dateadd(HOUR, <<difhoraria>>, getdate()), dateadd(HOUR, <<difhoraria>>, getdate()), <<valpagmoeda>>, '<<ALLTRIM(moedapag)>>',	'<<ALLTRIM(lcFechaStamp)>>')
			ENDTEXT

		Endif

		Local lcDinheiroPc
		lcDinheiroPc = Val(PAGAMENTO.txtDinheiro.Value)
		If uf_gerais_getParameter_site('ADM0000000041', 'BOOL', mySite) = .T. And lcDinheiroPc>0
			lcSQLPagCentralfechado = ''
			TEXT TO lcSQLPagCentralfechado TEXTMERGE NOSHOW
				update b_pagcentral
				set fechado=(select top 1 ISNULL(fechado,0) from B_pagCentral_maquinas (nolock) where B_pagCentral_maquinas.nratend=B_pagCentral.nrAtend)
				where nrAtend='<<ALLTRIM(nrAtendimento)>>'
			ENDTEXT

			lcSQLPagCentral = lcSQLPagCentral + lcSQLPagCentralfechado

		Endif

		lcSQLPagCentral = uf_gerais_trataPlicasSQL(lcSQLPagCentral)
		**
		&& n� de modos de pagamento **
		If !myPagCentral Or uf_gerais_validaPagCentralDinheiro()
			If !Empty(PAGAMENTO.txtDinheiro.Value)
				lcCntModPag = lcCntModPag + 1
			Endif
			If !Empty(PAGAMENTO.txtMB.Value)
				lcCntModPag = lcCntModPag + 1
			Endif
			If !Empty(PAGAMENTO.txtVisa.Value)
				lcCntModPag = lcCntModPag + 1
			Endif
			If !Empty(PAGAMENTO.txtCheque.Value)
				lcCntModPag = lcCntModPag + 1
			Endif

			&& DISTRIBUIR VALORES RECEBIDOS
			If lcCntD>0 And lcCntModPag>0
				If !Empty(PAGAMENTO.txtTroco.Value)
					lcTroco =( PAGAMENTO.txtTroco.Value / (lcCntD) )
					Select ft2
					Replace ft2.etroco	With lcTroco
				Endif
				If !Empty(PAGAMENTO.txtDinheiro.Value)
					lcDinheiro = Val(PAGAMENTO.txtDinheiro.Value) / lcCntD
					If lcTroco!=0
						lcDinheiro = lcDinheiro - lcTroco
					Endif
					Select ft2
					Replace ft2.evdinheiro	With lcDinheiro
				Endif
				If !Empty(PAGAMENTO.txtMB.Value)
					lcMB = Val(PAGAMENTO.txtMB.Value) / lcCntD
					Select ft2
					Replace ft2.epaga2	With lcMB
				Endif
				If !Empty(PAGAMENTO.txtVisa.Value)
					lcVisa = Val(PAGAMENTO.txtVisa.Value) / lcCntD
					Select ft2
					Replace ft2.epaga1	With lcVisa
				Endif
				If !Empty(PAGAMENTO.txtCheque.Value)
					lcCheque = Val(PAGAMENTO.txtCheque.Value) / lcCntD
					Select ft
					Replace ft.echtotal	With lcCheque
				Endif
				If !Empty(PAGAMENTO.txtModPag3.Value)
					lcModPag3 = Val(PAGAMENTO.txtModPag3.Value) / lcCntD
					Select ft2
					Replace ft2.epaga3	With lcModPag3
				Endif
				If !Empty(PAGAMENTO.txtModPag4.Value)
					lcModPag4 = Val(PAGAMENTO.txtModPag4.Value) / lcCntD
					Select ft2
					Replace ft2.epaga4	With lcModPag4
				Endif
				If !Empty(PAGAMENTO.txtModPag5.Value)
					lcModPag5 = Val(PAGAMENTO.txtModPag5.Value) / lcCntD
					Select ft2
					Replace ft2.epaga5	With lcModPag5
				Endif
				If !Empty(PAGAMENTO.txtModPag6.Value)
					lcModPag6 = Val(PAGAMENTO.txtModPag6.Value) / lcCntD
					Select ft2
					Replace ft2.epaga6	With lcModPag6
				Endif
			Endif
		Endif

		&& SE A VENDA FOR AO DOMICILIO
		If ft2.u_vddom
			If !uf_PAGAMENTO_saveDadosDomicilio()
				&& anula receitas DEM registadas anteriormente
				uf_pagamento_receitasAnular()

				Return .F.
			Endif
		Endif


		If Used("uCrsInfoAlertas")
			Select uCrsListaValidades
			Go Top
			Scan
				Delete
			Endscan
		Endif
		If Used("uCrsListaValidades")
			Select uCrsListaValidades
			Go Top
			Scan
				Delete
			Endscan
		Endif
		If Used("uCrsInteraccoes")
			Select uCrsInteraccoes
			Go Top
			Scan
				Delete
			Endscan
		Endif
		If Used("uCrsListaPVPDif")
			Select uCrsListaPVPDif
			Go Top
			Scan
				Delete
			Endscan
		Endif
		If Used("ucrsReceitaValidar")
			Select ucrsReceitaValidar
			Go Top
			Scan
				Delete
			Endscan
		Endif
		If Used("ucrsReceitaEfetivarAp")
			Select ucrsReceitaEfetivarAp
			Go Top
			Scan
				Delete
			Endscan
		Endif
		If Used("ucrsProdutosNaoCoferidos")
			Select ucrsProdutosNaoCoferidos
			Go Top
			Scan
				Delete
			Endscan
		Endif

		avisos=.F.

		If Type("infovenda") != "U"
			uf_infovenda_sair()
			uf_infoVenda_exit()
		Endif


		uf_gerais_regista_logs_passos("PAGAMENTO",nrAtendimento,"PAG15","VALIDACOES FINAIS PAGAMENTO")
		uf_gerais_regista_logs_passos("PAGAMENTO",nrAtendimento,"PAG16","GRAVAR ACERTOS PAGAMENTO")

		&& PERCORRER AS VENDAS DO ATENDIMENTO
		Select uCrsCabVendas
		Go Top
		Scan For uCrsCabVendas.saved = .F.

			Select uCrsCabVendas

			Local lcTipoDocOrigem, lcFaturaOrigemAlterada
			Store '' To lcTipoDocOrigem
			Store .F. To lcFaturaOrigemAlterada
			If !Empty(uCrsCabVendas.ofistamp)
				TEXT TO lcSql NOSHOW TEXTMERGE
					select ft.nmdoc, cast(ft.etotal as numeric(15,2)) as etotal, ft.no from fi
					inner join ft on fi.ftstamp=ft.ftstamp
					where fistamp='<<ALLTRIM(uCrsCabVendas.ofistamp)>>'
				ENDTEXT
				uf_gerais_actGrelha("",[uCrsCodDocOrigem],lcSQL)
				If Reccount("uCrsCodDocOrigem") >0
					**IF ALLTRIM(uCrsCodDocOrigem.nmdoc)=='Factura' AND (uCrsCodDocOrigem.no<>ft.no OR uCrsCodDocOrigem.etotal<>uCrsCabVendas.valorpagar)
					If Alltrim(uCrsCodDocOrigem.nmdoc)=='Factura' And (uCrsCodDocOrigem.no<>ft.no Or (uCrsCodDocOrigem.etotal<>uCrsCabVendas.valorPagar And (uCrsCodDocOrigem.etotal*(-1))<>uCrsCabVendas.valorPagar))
						lcFaturaOrigemAlterada=.T.
					Endif
				Endif
			Endif
			fecha("uCrsCodDocOrigem")
			**MESSAGEBOX(lcFaturaOrigemAlterada)
			Select uCrsCabVendas

			lcValidaInsercaoDocumento = .F.
			&& Validar Acerto Reg.Vendas
			Select ft2
			Replace ft2.u_acertovs	With uCrsCabVendas.valorPagar
			Replace ft2.obscl        With uCrsCabVendas.obscl
			Replace ft2.obsint         With uCrsCabVendas.obsint

			If (uCrsCabVendas.partes == 0)  And (uCrsCabVendas.Fact == .F. Or uCrsCabVendas.modoPag == .T. Or Right(Alltrim(uCrsCabVendas.Design),10) == "*SUSPENSA*" Or uCrsCabVendas.sel == .F. Or Right(Alltrim(uCrsCabVendas.Design),18) == "*SUSPENSA RESERVA*")
				&& CRIAR CURSOR COM DADOS DE COMPARTICIPA��O DA VENDA
				If Left(uCrsCabVendas.Design,12) != ".SEM RECEITA"
					lcCod		= Strextract(uCrsCabVendas.Design, '[', ']', 1, 0)
					lcReceita	= Strextract(uCrsCabVendas.Design, ':', ' -', 1, 0)
					If !Empty(lcCod)
						If !Empty(astr(Strextract(uCrsCabVendas.Design, '-', '-', 1,2)))
							lcVia = astr(Strextract(uCrsCabVendas.Design, '-', '-', 1,2))
						Else
							lcVia = "Normal"
						Endif

						lcSQL = ''
						TEXT TO lcSql NOSHOW TEXTMERGE
									exec up_receituario_dadosDoPlano '<<lcCod>>'
						ENDTEXT
						If !uf_gerais_actGrelha("",[uCrsCodPla],lcSQL) Or Reccount([uCrsCodPla])=0
							uf_perguntalt_chama("O C�DIGO '"+lcCod+"' N�O EXISTE. POR FAVOR CONTACTE O SUPORTE.","OK","",16)

							&& anula receitas DEM registadas anteriormente
							uf_pagamento_receitasAnular()

							Return .F.
						Else
							lcCod2		= uCrsCodPla.codigo2
							lcDesign	= uCrsCodPla.Design
							lcDesign2	= uCrsCodPla.design2
							lcAbrev		= uCrsCodPla.cptorgabrev
							lcAbrev2	= uCrsCodPla.cptorgabrev2
						Endif
					Endif
				Else
					lcCod = ''
					lcReceita = ''
					lcVia = ''
					lcCod2 = ''
					lcDesign = ''
					lcDesign2 = ''
					lcAbrev = ''
					lcAbrev2 = ''
				Endif

				&& QUAL O TIPO DO DOCUMENTO A GRAVAR E N� DA VENDA
				If uCrsCabVendas.modoPag == .F.
					If myOffline
						lcNdoc	= 84
						lcNmDoc	= "Venda Offline"
					Else
						lcNdoc	= myVd
						lcNmDoc	= myVdNm
					Endif
				Else
					lcNdoc		= myFact
					lcNmDoc		= myFactNm
				Endif

				&& verficar se � suspensa
				If Right(Alltrim(uCrsCabVendas.Design),10)=="*SUSPENSA*" Or Right(Alltrim(uCrsCabVendas.Design),18)=="*SUSPENSA RESERVA*"
					lcSusp = .T.
				Else
					lcSusp = .F.
				Endif



				lcTipoDoc = 1


				uf_gerais_regista_logs_passos("PAGAMENTO",nrAtendimento,"PAG17","PREPARA CURSOR TALOES PAGAMENTO")

				&& GUARDAR STAMPS DAS VENDAS
				Select ft
				lcFtstamp = uf_gerais_stamp()

				&& Cria cursor para posteriormente permitir a impress�o dos tal�es
				If !Used("uCrsImpTalaoPos")
					Create Cursor uCrsImpTalaoPos (stamp c(26), tipo i(4), Lordem i(8), Susp l, AdiReserva l, nrreceita c(26) ,rm l, planocomp l, impcomp1 l, impcomp2 l, lote c(5), impresso l , plano c(5))
				Endif

				Select uCrsImpTalaoPos
				Append Blank
				Replace uCrsImpTalaoPos.stamp			With Alltrim(lcFtstamp)
				Replace uCrsImpTalaoPos.tipo			With lcNdoc
				Replace uCrsImpTalaoPos.Lordem			With uCrsCabVendas.Lordem
				If Right(Alltrim(uCrsCabVendas.Design),10) == "*SUSPENSA*" Or Right(Alltrim(uCrsCabVendas.Design),18)=="*SUSPENSA RESERVA*"
					Replace uCrsImpTalaoPos.Susp		With .T.
				Else
					Replace uCrsImpTalaoPos.Susp		With .F.
				Endif
				If !Empty(uCrsCabVendas.TemAdiantamento)
					Replace uCrsImpTalaoPos.AdiReserva	With .T.
				Else
					Replace uCrsImpTalaoPos.AdiReserva	With .F.
				Endif
				Replace uCrsImpTalaoPos.planocomp 		With uCrsCabVendas.planocomplem
				If Alltrim(uCrsCabVendas.receita_tipo)=='RM'
					Replace uCrsImpTalaoPos.rm With .T.
				Else
					Replace uCrsImpTalaoPos.rm With .F.
				Endif
				Replace uCrsImpTalaoPos.impcomp1 With .F.
				Replace uCrsImpTalaoPos.impcomp2 With .F.
				Replace uCrsImpTalaoPos.nrreceita With Substr(uCrsCabVendas.Design, At('Receita',uCrsCabVendas.Design )+8,At(' - ',uCrsCabVendas.Design )- (At('Receita',uCrsCabVendas.Design )+8))
				Replace uCrsImpTalaoPos.plano With Substr(uCrsCabVendas.Design, At('.[',uCrsCabVendas.Design )+2,At('][',uCrsCabVendas.Design )- (At('.[',uCrsCabVendas.Design )+2))



				&& Cria Cursores Auxiliares - Usados na Imp Tal�es.Receitas // Arquivo Digital // ++ ??
				If !Used("uCrsFt")
					Select ft
					Go Top
					Select * From ft Where 1=0 Into Cursor uCrsFt Readwrite
				Endif

				&&
				If !Used("uCrsFt2")
					Select *, u_vddom As 'ft2_u_vddom', contacto As 'ft2_contacto', u_viatura As 'ft2_u_viatura', morada As 'ft2_morada', codpost As 'ft2_codpost', Local As 'ft2_local', telefone As 'ft2_telefone', email As 'ft2_email', horaentrega As 'ft2_horaentrega' From ft2 Where 1=0 Into Cursor uCrsFt2 Readwrite
				Endif

				&&
				If !Used("uCrsFi")
					Select * From fi Where 1=0 Into Cursor uCrsFi Readwrite
				Endif

				If !Used("uCrsFi2")
					Select * From fi2 Where 1=0 Into Cursor uCrsFi2 Readwrite
				Endif

				&& Criar Data e HORA DO SISTEMA com base na timezone
				&& :::TODO
				*!*						SET HOURS TO 24
				*!*						Atime=ttoc(datetime()+(difhoraria*3600),2)
				*!*						Astr=left(Atime,8)
				*!*						lcHora = Astr
				uf_gerais_regista_logs_passos("PAGAMENTO",nrAtendimento,"PAG17A","GRAVA DOCUMENTOS")
				lcHora	= Time() && � necess�rio remover os segundos durante a inser��o na tabela
				TEXT TO lcSQL TEXTMERGE NOSHOW
						select dateadd(HOUR, <<difhoraria>>, getdate()) data
				ENDTEXT
				uf_gerais_actGrelha("","uCrsMyInvoiceData",lcSQL)
				Select uCrsMyInvoiceData
				myInvoiceData = uf_gerais_getDate(uCrsMyInvoiceData.Data,"SQL")
				fecha("uCrsMyInvoiceData")



				&& CALCULAR TOTAIS POR VENDA
				Select fi
				Go Top
				Scan For fi.partes = 0
					If lcValida && se j� encontrou venda
						If Left(fi.Design,1)=="."
							Exit && j� percorreu todas as linhas desta venda -> sair do scan
						Else
							&& Calculo da base de incid�ncia (lcEttIliq), calculo total documento (lcEtotal) � feito linha � linha
							lcTotQtt		= lcTotQtt	+ fi.qtt
							lcQtt1			= lcQtt1	+ fi.qtt
							lcEtotal		= lcEtotal	+ fi.etiliquido
							lcEttIliq		= lcEttIliq	+ (fi.etiliquido/(fi.iva/100+1))
							lcEcusto		= lcEcusto	+ fi.ecusto

							&& Calculo do IVA � feito tamb�m � linha
							Do Case
								Case fi.tabiva = 1
									EivaIn1	= EivaIn1 + (fi.etiliquido/(fi.iva/100+1))
									EivaV1	= EivaV1 + fi.etiliquido - (fi.etiliquido/(fi.iva/100+1))
								Case fi.tabiva = 2
									EivaIn2	= EivaIn2 + (fi.etiliquido/(fi.iva/100+1))
									EivaV2	= EivaV2 + fi.etiliquido - (fi.etiliquido/(fi.iva/100+1))
								Case fi.tabiva = 3
									EivaIn3	= EivaIn3 + (fi.etiliquido/(fi.iva/100+1))
									EivaV3	= EivaV3 + fi.etiliquido - (fi.etiliquido/(fi.iva/100+1))
								Case fi.tabiva = 4
									EivaIn4	= EivaIn4 + (fi.etiliquido/(fi.iva/100+1))
									EivaV4	= EivaV4 + fi.etiliquido - (fi.etiliquido/(fi.iva/100+1))
								Case fi.tabiva = 5
									EivaIn5	= EivaIn5 + (fi.etiliquido/(fi.iva/100+1))
									EivaV5	= EivaV5 + fi.etiliquido - (fi.etiliquido/(fi.iva/100+1))
								Case fi.tabiva = 6
									EivaIn6	= EivaIn6 + (fi.etiliquido/(fi.iva/100+1))
									EivaV6	= EivaV6 + fi.etiliquido - (fi.etiliquido/(fi.iva/100+1))
								Case fi.tabiva = 7
									EivaIn7	= EivaIn7 + (fi.etiliquido/(fi.iva/100+1))
									EivaV7	= EivaV7 + fi.etiliquido - (fi.etiliquido/(fi.iva/100+1))
								Case fi.tabiva = 8
									EivaIn8	= EivaIn8 + (fi.etiliquido/(fi.iva/100+1))
									EivaV8	= EivaV8 + fi.etiliquido - (fi.etiliquido/(fi.iva/100+1))
								Case fi.tabiva = 9
									EivaIn9	= EivaIn9 + (fi.etiliquido/(fi.iva/100+1))
									EivaV9	= EivaV9 + fi.etiliquido - (fi.etiliquido/(fi.iva/100+1))
								Case fi.tabiva = 10
									EivaIn10	= EivaIn10 + (fi.etiliquido/(fi.iva/100+1))
									EivaV10	= EivaV10 + fi.etiliquido - (fi.etiliquido/(fi.iva/100+1))
								Case fi.tabiva = 11
									EivaIn11	= EivaIn11 + (fi.etiliquido/(fi.iva/100+1))
									EivaV11	= EivaV11 + fi.etiliquido - (fi.etiliquido/(fi.iva/100+1))
								Case fi.tabiva = 12
									EivaIn12	= EivaIn12 + (fi.etiliquido/(fi.iva/100+1))
									EivaV12	= EivaV12 + fi.etiliquido - (fi.etiliquido/(fi.iva/100+1))
								Case fi.tabiva = 13
									EivaIn13	= EivaIn13 + (fi.etiliquido/(fi.iva/100+1))
									EivaV13	= EivaV13 + fi.etiliquido - (fi.etiliquido/(fi.iva/100+1))
							Endcase

							If fi.desconto > 0  Or fi.desc2>0 Or fi.u_descval>0 && And fi.etiliquido >0
								If fi.ivaincl
									lcValorSemIva = (fi.epv*fi.qtt) / (fi.iva / 100 + 1)
								Else
									lcValorSemIva = fi.epv*fi.qtt
								Endif

								If fi.desconto>0
									lcDesc = lcValorSemIva - (lcValorSemIva * ((100-fi.desconto)/100))
									lcTotDesc = lcTotDesc + lcDesc
									lcValorSemIva = lcValorSemIva - lcTotDesc
								Endif

								If fi.desc2 > 0
									lcDesc = lcValorSemIva - (lcValorSemIva * ((100-fi.desc2)/100))
									lcTotDesc = lcTotDesc + lcDesc
									lcValorSemIva = lcValorSemIva - lcTotDesc
								Endif

								If fi.u_descval>0
									If fi.ivaincl
										lcDesc = fi.u_descval/(fi.iva/100+1)
									Else
										lcDesc = fi.u_descval
									Endif
									lcTotDesc = lcTotDesc + lcDesc
								Endif
							Endif

						Endif
					Endif

					If ((Alltrim(fi.Design)==Alltrim(uCrsCabVendas.Design)) Or (Left(Alltrim(fi.Design),Len(Alltrim(uCrsCabVendas.Design)))==Alltrim(uCrsCabVendas.Design))) And (fi.Lordem==uCrsCabVendas.Lordem)
						lcValida=.T. && encontrou venda
					Endif
				Endscan

				*!*						LOCAL lcObsclRes, lcObsintRes
				*!*						STORE '' TO  lcObsclRes, lcObsintRes

				*!*						SELECT uCrsCabVendas
				*!*						GO TOP
				*!*						SCAN
				*!*							IF uCrsCabVendas.partes > 0
				*!*								lcObsclRes = lcObsclRes  + IIF(!EMPTY(ALLTRIM(lcObsclRes)),",","") + ALLTRIM(uCrsCabVendas.obscl)
				*!*								lcObsintRes = lcObsintRes + IIF(!EMPTY(ALLTRIM(lcObsintRes)),",","") + ALLTRIM(uCrsCabVendas.obsint)
				*!*							ENDIF
				*!*						ENDSCAN

				*!*						SELECT ft2
				*!*						IF myReservasCl
				*!*							REPLACE ft2.obscl		WITH ALLTRIM(lcObsclRes)
				*!*							REPLACE ft2.obsint 		WITH ALLTRIM(lcObsintRes)
				*!*						ENDIF

				&& guardar codigos local prescri��o e m�dico
				Replace ft2.u_entpresc	With Alltrim(uCrsCabVendas.entpresc) In ft2
				Replace ft2.u_nopresc	With Alltrim(uCrsCabVendas.nopresc) In ft2
				Replace ft2.u_nbenef	With Alltrim(uCrsCabVendas.u_nbenef) In ft2
				Replace ft2.u_nbenef2	With Alltrim(uCrsCabVendas.u_nbenef2) In ft2

				&& Motivo de Isen��o de IVA
				Local lcIsencaoIva
				Store '' To lcIsencaoIva

				Do Case
					Case ft.pais == 3
						lcIsencaoIva = "Isento Artigo 14. do CIVA (ou similar)"
					Case ft.pais == 2
						lcIsencaoIva = "Isento Artigo 14. do RITI (ou similar)"
					Otherwise
						If Empty(ucrse1.motivo_isencao_iva)
							lcIsencaoIva = "Isento Artigo 9. do CIVA (ou similar)"
						Else
							lcIsencaoIva = Alltrim(ucrse1.motivo_isencao_iva)
						Endif
				Endcase

				If uCrsCabVendas.isencaoiva == .T.
					Replace ft2.motiseimp With  lcIsencaoIva In ft2
				Else
					Replace ft2.motiseimp With '' In ft2
				Endif

				&& INCREMENTAR CONTADOR PSICO/BENZO
				uf_PAGAMENTO_ContadorPsicoBenzo()

				&& cursor para enviar par�metros - valor IVA
				If !Used("uCrsValIva")
					Create Cursor uCrsValIva ;
						(lcEivaIn1 numeric(19,4), lcEivaIn2 numeric(19,4), lcEivaIn3 numeric(19,4), lcEivaIn4 numeric(19,4), lcEivaIn5 numeric(19,4), lcEivaIn6 numeric(19,4), lcEivaIn7 numeric(19,4), lcEivaIn8 numeric(19,4), lcEivaIn9 numeric(19,4), lcEivaIn10 numeric(19,4),lcEivaIn11 numeric(19,4),lcEivaIn12 numeric(19,4),lcEivaIn13 numeric(19,4),;
						lcEivaV1 numeric(19,4), lcEivaV2 numeric(19,4), lcEivaV3 numeric(19,4), lcEivaV4 numeric(19,4), lcEivaV5 numeric(19,4), lcEivaV6 numeric(19,4), lcEivaV7 numeric(19,4), lcEivaV8 numeric(19,4), lcEivaV9 numeric(19,4), lcEivaV10 numeric(19,4), lcEivaV11 numeric(19,4), lcEivaV12 numeric(19,4), lcEivaV13 numeric(19,4))
				Else
					**SELECT uCrsValIva
					**DELETE ALL
					fecha("uCrsValIva")
					Create Cursor uCrsValIva ;
						(lcEivaIn1 numeric(19,4), lcEivaIn2 numeric(19,4), lcEivaIn3 numeric(19,4), lcEivaIn4 numeric(19,4), lcEivaIn5 numeric(19,4), lcEivaIn6 numeric(19,4), lcEivaIn7 numeric(19,4), lcEivaIn8 numeric(19,4), lcEivaIn9 numeric(19,4), lcEivaIn10 numeric(19,4),lcEivaIn11 numeric(19,4),lcEivaIn12 numeric(19,4),lcEivaIn13 numeric(19,4),;
						lcEivaV1 numeric(19,4), lcEivaV2 numeric(19,4), lcEivaV3 numeric(19,4), lcEivaV4 numeric(19,4), lcEivaV5 numeric(19,4), lcEivaV6 numeric(19,4), lcEivaV7 numeric(19,4), lcEivaV8 numeric(19,4), lcEivaV9 numeric(19,4), lcEivaV10 numeric(19,4), lcEivaV11 numeric(19,4), lcEivaV12 numeric(19,4), lcEivaV13 numeric(19,4))
				Endif
				Select uCrsValIva
				Append Blank
				Replace ;
					uCrsValIva.lcEivaIn1 With Round(EivaIn1,2) ;
					uCrsValIva.lcEivaIn2 With Round(EivaIn2,2) ;
					uCrsValIva.lcEivaIn3 With Round(EivaIn3,2) ;
					uCrsValIva.lcEivaIn4 With Round(EivaIn4,2) ;
					uCrsValIva.lcEivaIn5 With Round(EivaIn5,2) ;
					uCrsValIva.lcEivaIn6 With Round(EivaIn6,2) ;
					uCrsValIva.lcEivaIn7 With Round(EivaIn7,2) ;
					uCrsValIva.lcEivaIn8 With Round(EivaIn8,2) ;
					uCrsValIva.lcEivaIn9 With Round(EivaIn9,2) ;
					uCrsValIva.lcEivaIn10 With Round(EivaIn10,2) ;
					uCrsValIva.lcEivaIn11 With Round(EivaIn11,2) ;
					uCrsValIva.lcEivaIn12 With Round(EivaIn12,2) ;
					uCrsValIva.lcEivaIn13 With Round(EivaIn13,2) ;
					uCrsValIva.lcEivaV1 With Round(EivaV1,2) ;
					uCrsValIva.lcEivaV2 With Round(EivaV2,2) ;
					uCrsValIva.lcEivaV3 With Round(EivaV3,2) ;
					uCrsValIva.lcEivaV4 With Round(EivaV4,2) ;
					uCrsValIva.lcEivaV5 With Round(EivaV5,2) ;
					uCrsValIva.lcEivaV6 With Round(EivaV6,2) ;
					uCrsValIva.lcEivaV7 With Round(EivaV7,2) ;
					uCrsValIva.lcEivaV8 With Round(EivaV8,2) ;
					uCrsValIva.lcEivaV9 With Round(EivaV9,2) ;
					uCrsValIva.lcEivaV10 With Round(EivaV10,2) ;
					uCrsValIva.lcEivaV11 With Round(EivaV11,2) ;
					uCrsValIva.lcEivaV12 With Round(EivaV12,2) ;
					uCrsValIva.lcEivaV13 With Round(EivaV13,2)

				&& N� Cart�o de Comparticipa��o
				Select ft2
				Replace ft2.c2codpost With uCrsCabVendas.cartao

				&& Remover poss�veis linhas em branco
				Select fi
				Delete From fi Where Empty(fi.ref) And Empty(fi.Design) And fi.qtt=0 And fi.etiliquido=0

				If Used("ucrsLinFiBi")
					fecha("ucrsLinFiBi")
				Endif
				If Used("ucrsLinFiBo")
					fecha("ucrsLinFiBo")
				Endif

				&& construir cursor com  linhas copiadas da BI para fechar documentos
				Select ref, Design, qtt, epv ,etiliquido, iva, tabiva, ivaincl, armazem,ecusto, desconto, epcp ,bistamp, bostamp, .F. As act From fi Where !Empty(fi.bostamp) And !Empty(fi.ref) And Alltrim(fi.nmdos)=='Encomenda de Cliente' Order By bostamp, bistamp Into Cursor ucrsLinFiBi Readwrite
				Select Distinct bostamp, .F. As act, tipor From fi Where !Empty(fi.bistamp) And Alltrim(fi.nmdos)=='Encomenda de Cliente' Into Cursor ucrsLinFiBo Readwrite
				Select Distinct bostamp From fi Where !Empty(fi.bistamp) Into Cursor ucrsLinBoFecho Readwrite

				&& INSTRUCAO PARA GRAVAR A VENDA
				Store .F. To lcOkFt, lcOkFt2, lcOkFi
				Select uCrsValIva

				&& n� da venda que est� a ser gravada
				lcVendaCount = lcVendaCount + 1

				Select uCrsCabVendas

				&& Insert na dbo.Ft2
				lcSqlFt2 = uf_PAGAMENTO_GravarVendaFt2(lcFtstamp, lcNdoc, lcReceita, lcCod, lcCod2, lcDesign, lcDesign2, lcAbrev, lcAbrev2, lcHora, uCrsCabVendas.codAcesso, uCrsCabVendas.codDirOpcao, uCrsCabVendas.token)
				lcSqlFt2 = uf_gerais_trataPlicasSQL(lcSqlFt2)

				&& Declare variavel @fno - s� � declarada na 1� vez
				If lcVendaCount < 2
					TEXT TO lcSQLFtDeclare NOSHOW TEXTMERGE
							DECLARE @fno as numeric(9,0)
							DECLARE @tabNewDocNr TABLE (newDocNr NUMERIC(9, 0))
					ENDTEXT
				Else
					lcSQLFtDeclare = ''
				Endif

				Public lcStampFiRepetidoERRO
				Store .F. To lcStampFiRepetidoERRO


				Select uCrsatendCl
				Local lcNifUtente
				*lcNifUtente = ALLTRIM(ucrsatendcl.ncont)

				lcNifUtente = Alltrim(Iif(uCrsCLEF.utstamp <> uCrsatendCl.utstamp, uCrsCLEF.ncont, uCrsatendCl.ncont))

				Select uCrsCabVendas
				If uCrsCabVendas.nifgenerico = .T.
					Select ft
					If Upper(Alltrim(ucrse1.pais)) == 'PORTUGAL'
						Replace ft.ncont With '999999990'
						lcNcont = '999999990'
					Else
						Replace ft.ncont With '9999999999'
						lcNcont = '9999999999'
					Endif
				Else
					Select ft
					Replace ft.ncont With Alltrim(lcNifUtente)
					lcNcont = Alltrim(lcNifUtente)
				Endif



				Select uCrsCabVendas
				&& Insert na dbo.Ft
				lcSQLFt = uf_PAGAMENTO_GravarVendaFt(;
					lcFtstamp, lcNmDoc, lcNdoc, lcTipoDoc, lcClNo, lcClNome, lcClEstab, lcTotQtt, lcQtt1, Round(lcEtotal,2), Round(lcEttIliq,2), lcEcusto,;
					ROUND(uCrsValIva.lcEivaV1+uCrsValIva.lcEivaV2+uCrsValIva.lcEivaV3+uCrsValIva.lcEivaV4+uCrsValIva.lcEivaV5+uCrsValIva.lcEivaV6+uCrsValIva.lcEivaV7+uCrsValIva.lcEivaV8+uCrsValIva.lcEivaV9+uCrsValIva.lcEivaV10+uCrsValIva.lcEivaV11+uCrsValIva.lcEivaV12+uCrsValIva.lcEivaV13,2),;
					ROUND(lcTotDesc,2), lcPdata, lcHora, Iif(Right(Alltrim(uCrsCabVendas.Design),18)=="*SUSPENSA RESERVA*", .F., lcSusp),;
					IIF(uf_gerais_getDate(uCrsCabVendas.datareceita)=="19000101",uf_gerais_getDate(Date(),'SQL'),;
					IIF(uf_gerais_getDate(uCrsCabVendas.datareceita,'SQL')=='19000101',uf_gerais_getDate(Date(),'SQL'),;
					uf_gerais_getDate(uCrsCabVendas.datareceita,'SQL'))),;
					lcNcont,;
					IIF(Empty(uf_gerais_getDate(uCrsCabVendas.datadispensa,'SQL')) Or uf_gerais_compStr(uf_gerais_getDate(uCrsCabVendas.datadispensa,'SQL'),'19000101'),uf_gerais_getDate(Date(),'SQL'),;
					uf_gerais_getDate(uCrsCabVendas.datadispensa,'SQL')))
				lcSQLFt = uf_gerais_trataPlicasSQL(lcSQLFtDeclare + lcSQLFt)


				Public lcDetaLinasRepetidas, lcPsiconaslinhas, lcJaProcPsico
				Store .F. To lcDetaLinasRepetidas, lcPsiconaslinhas, lcJaProcPsico
				&& Insert na dbo.Fi
				lcSqlFi	= uf_PAGAMENTO_gravarVendaFi(lcFtstamp, lcNmDoc, lcNdoc, lcTipoDoc, lcNrVenda, lcHora, uCrsCabVendas.codAcesso)
				lcSqlFi = uf_gerais_trataPlicasSQL(lcSqlFi)

				&& Gravar certifica��o
				lcSqlCert = uf_pagamento_gravarCert(lcFtstamp, myInvoiceData, lcHora, lcNdoc, lcEtotal)
				lcSqlCert = uf_gerais_trataPlicasSQL(lcSqlCert)

				If lcStampFiRepetidoERRO=.T.
					lcSqlFt2 = ''
					lcSQLFt = ''
					lcSqlFi = ''
					lcSqlCert = ''
				Else
					Select uCrsCabVendas
					Local lcCabVendasLordem
					lcCabVendasLordem = uCrsCabVendas.Lordem
					&& Gravar dados de PSICO/BENZO
					lcSQLDadosPsico = ''

					&&DELETE FROM dadosPsico WHERE EMPTY(ALLTRIM(ftstamp))

					Select dadosPsico
					If Reccount("dadosPsico")=1 And Empty(dadosPsico.cabVendasOrdem)
						uf_PAGAMENTO_gravarDadosPsico(lcFtstamp, uCrsCabVendas.Lordem, .F.)
						lcJaProcPsico = .T.
					Else
						Go Top
						Scan &&FOR dadosPsico.CabVendasOrdem == uCrsCabVendas.Lordem

							If lcCabVendasLordem = dadosPsico.cabVendasOrdem


								lcSQLdPsico = uf_PAGAMENTO_gravarDadosPsico(lcFtstamp, uCrsCabVendas.Lordem, .T.)
								lcSQLdPsico = uf_gerais_trataPlicasSQL(lcSQLdPsico)

								lcSQLDadosPsico = lcSQLdPsico
								lcJaProcPsico = .T.
							Endif

							Select dadosPsico
						Endscan
					Endif
					If lcPsiconaslinhas = .T. And lcJaProcPsico = .F.
						uf_PAGAMENTO_gravarDadosPsico(lcFtstamp, uCrsCabVendas.Lordem, .F.)
					Endif


					** Verificar se tem dadospsico na inser��o da receita - reservas
					If Len(lcSQLDadosPsico) = 0 And At('Receita:', Alltrim(uCrsCabVendas.Design))<>0 And At('RESERVA', Alltrim(uCrsCabVendas.Design))<>0
						Local lcnrrecpsico, lcconta1, lcconta2
						Store 0 To lcconta1, lcconta2
						lcnrrecpsico = Substr(uCrsCabVendasprint.Design,At('Receita:',uCrsCabVendasprint.Design )+8,(At(' -',uCrsCabVendasprint.Design))-(At('Receita:',uCrsCabVendasprint.Design )+8))
						If Len(Alltrim(lcnrrecpsico)) = 13 Or Len(Alltrim(lcnrrecpsico)) = 19
							TEXT TO lcSql NOSHOW TEXTMERGE
									select * from B_dadosPsico (nolock) where ftstamp in (select TOP 1 ft2stamp from ft2 (nolock) inner join ft (nolock) on ft.ftstamp=ft2.ft2stamp where ft2.u_receita = '<<ALLTRIM(lcnrrecpsico)>>' and ft.no=<<lcClNo>> order by ft.fdata desc)
							ENDTEXT
							uf_gerais_actGrelha("",[uCrsPsicoReserva],lcSQL)
							Select uCrsPsicoReserva
							lcconta1 = Reccount("uCrsPsicoReserva")

							TEXT TO lcSql NOSHOW TEXTMERGE
									select * from B_dadosPsico (nolock) where ftstamp = '<<ALLTRIM(lcFtStamp)>>'
							ENDTEXT
							uf_gerais_actGrelha("",[uCrsPsicoReserva1],lcSQL)
							Select uCrsPsicoReserva1
							lcconta2 = Reccount("uCrsPsicoReserva1")

							If lcconta1 > 0 And lcconta2 = 0

								uv_codSPMS = ''

								TEXT TO lcSqlpsicores NOSHOW TEXTMERGE
										insert into B_dadosPsico (ftstamp,u_recdata,u_medico,u_nmutdisp,u_moutdisp,u_cputdisp,u_nmutavi,u_moutavi,u_cputavi,u_ndutavi,u_ddutavi,u_idutavi,u_dirtec, codigoDocSPMS, contactoCli)
										select '<<ALLTRIM(lcFtStamp)>>',u_recdata,u_medico,u_nmutdisp,u_moutdisp,u_cputdisp,u_nmutavi,u_moutavi,u_cputavi,u_ndutavi,u_ddutavi,u_idutavi,u_dirtec, '<<ALLTRIM(uv_codSPMS)>>', contactoCli from B_dadosPsico (nolock) where ftstamp in (SELECT TOP 1 ft2stamp from ft2 (nolock) where ft2.u_receita='<<ALLTRIM(lcnrrecpsico)>>' order by ousrdata desc)
								ENDTEXT
								lcSqlpsicores = uf_gerais_trataPlicasSQL(lcSqlpsicores)
								lcSQLDadosPsico = lcSQLDadosPsico + Chr(13) + lcSqlpsicores
							Endif
							fecha("uCrsPsicoReserva")
							fecha("uCrsPsicoReserva1")
						Endif
					Endif

					*!*							&& Gravar certifica��o
					*!*							lcSqlCert = uf_pagamento_gravarCert(lcFtStamp, myInvoiceData, lcHora, lcNdoc, lcEtotal)
					*!*							lcSqlCert = uf_gerais_trataPlicasSQL(lcSqlCert)
				Endif

				&& Informa��o das Vendas + Certifica��o + Psicotr�picos

				If Vartype(receitaencomenda)!='U'
					If lcDetaLinasRepetidas = .F.
						lcSQLInsertDoc = lcSQLInsertDoc + Chr(13)+ lcSqlFt2 + Chr(13) + lcSQLFt + Chr(13) + lcSqlFi + Chr(13)  + lcSqlFi2 + Chr(13) + lcSQLDadosPsico + Chr(13) + lcSqlCert
					Endif
				Else
					lcSQLInsertDoc = lcSQLInsertDoc + Chr(13)+ lcSqlFt2 + Chr(13) + lcSQLFt + Chr(13) + lcSqlFi + Chr(13)  + lcSqlFi2 + Chr(13) + lcSQLDadosPsico + Chr(13) + lcSqlCert
				Endif

				&& Actualizar cursores com Stamp da Venda para posterior utilizacao
				Select uCrsCabVendas
				Replace uCrsCabVendas.stamp With lcFtstamp

				Select dadosPsico
				Locate For dadosPsico.cabVendasOrdem == uCrsCabVendas.Lordem
				Replace ftstamp With lcFtstamp

				Select uCrsCabVendas
				Replace uCrsCabVendas.saved With .T.

				pFimAtendimento = .T.
				**PAGAMENTO.menu1.estado("opcoes,psico,talSep,nTaloes,gaveta,impOrdem", "SHOW", "PAGAMENTO", .f., "SAIR", .t.)
				PAGAMENTO.menu1.estado("opcoes,talSep,nTaloes,gaveta,impA4", "SHOW", "PAGAMENTO", .F., "SAIR", .T.)
				**

				&& reset �s vari�veis
				Store .F. To lcValida
				Store 0 To lcTotQtt, lcQtt1, lcEtotal, lcEttIliq, lcEcusto, EivaIn1, EivaIn2, EivaIn3, EivaIn4, EivaIn5, EivaIn6, EivaIn7, EivaIn8, EivaIn9, EivaV1, EivaV2, EivaV3, EivaV4, EivaV5, EivaV6, EivaV7, EivaV8, EivaV9, lcEttIva
				Store 0 To lcTotDesc, lcDesc, lcValorSemIva
				**
			Endif

			Select uCrsCabVendas
		Endscan

		uf_gerais_regista_logs_passos("PAGAMENTO",nrAtendimento,"PAG18","FIM GRAVAR DOCS PAGAMENTO")

		** FIM PERCORRER VENDAS A GRAVAR PARA CRIAR INSTRUCAO GRAVACAO DE VENDA

		&& Cursor com o resultado das transa��es efetuadas nas valida��es doa dados dos medicamentos
		**IF USED("fi_trans_info")
		**	SELECT * FROM fi_trans_info WHERE !EMPTY(fi_trans_info.fistamp) INTO CURSOR fi_trans_info_aux READWRITE
		**	IF RECCOUNT("fi_trans_info_aux")>0
		**		lcSqlfi_trans_info = uf_PAGAMENTO_GravarReg_fi_trans_info()
		**		lcSqlfi_trans_info = uf_gerais_trataPlicasSQL(lcSqlfi_trans_info)
		**		lcSQLInsertDoc = lcSQLInsertDoc + lcSqlfi_trans_info
		**	ENDIF
		**	fecha("fi_trans_info_aux")
		**ENDIF

		&& Cursor com as strings JSON enviadas e recebidas nas comunica��es anteriores
		If Used("nrserie_com_results")
			If Reccount("nrserie_com_results")>0
				lcSqlnrserie_com_results = uf_PAGAMENTO_GravarReg_nrserie_com_results()
				lcSqlnrserie_com_results = uf_gerais_trataPlicasSQL(lcSqlnrserie_com_results)
				lcSQLInsertDoc = lcSQLInsertDoc + lcSqlnrserie_com_results
			Endif
			fecha("nrserie_com_results")
		Endif

		&& Cursor com os pedidos de mixed bulk recebidos
		If Used("mixed_bulk_pend")
			If Reccount("mixed_bulk_pend")>0
				lcSqlmixed_bulk_pend = uf_PAGAMENTO_GravarReg_mixed_bulk_pend()
				lcSqlmixed_bulk_pend = uf_gerais_trataPlicasSQL(lcSqlmixed_bulk_pend)
				lcSQLInsertDoc = lcSQLInsertDoc + lcSqlmixed_bulk_pend
			Endif
			fecha("mixed_bulk_pend")
		Endif

		&& SQL INSTRUCAO FINAL (adiciona Pag Central) - Insert Valores Vendas + Psicotr�picos + Certifica��o + Valores � Pag Central
		If lcVendaCount > 0
			lcSQLInsertDoc = lcSQLInsertDoc + Chr(13) + lcSQLPagCentral
		Endif
		**_cliptext=lcSQLInsertDoc

		uf_gerais_regista_logs_passos("B_PAGCENTRAL",nrAtendimento,"b_pag6","depois de passar para outra variavel uf_PAGAMENTO_finalizarAtendimento")

		Local lcSqlFinal
		lcSqlFinal = lcSQLInsertDoc

		&& FASE 1 - Instrucao Gravacao Venda - VENDAS + CONTADORES PSICOTR�PICOS + CERTIFICA��O + TABELA PAGAMENTOS CENTRALIZADOS
		If !Empty(lcSQLInsertDoc)
			lcSQL = ''
			**_cliptext = lcSQLInsertDoc
			TEXT TO lcSQL NOSHOW TEXTMERGE
				exec up_gerais_execSql 'Atendimento - Insert', 1,'<<lcSQLInsertDoc>>', '', '', '' , '', ''
			ENDTEXT
			If !uf_gerais_actGrelha("","uCrsFnoAT",lcSQL)
				uf_perguntalt_chama("OCORREU UMA ANOMALIA AO GRAVAR A(S) VENDA(S). POR FAVOR REINICIE O SOFTWARE E REFA�A O ATENDIMENTO. OBRIGADO.","OK","",16)

				lcValidaInsercaoDocumento = .F.

				&& anula receitas DEM registadas anteriormente
				uf_pagamento_receitasAnular()

				Return .F.
			Else
				&& Tudo ok passa para a segunda instrucao de gravacao
				** Atualiza a cativa��o dos cabe�alhos
				If uf_gerais_getParameter_site('ADM0000000071', 'BOOL', mySite)
					TEXT TO lcSQL1 NOSHOW TEXTMERGE
						UPDATE ft2 SET valcativa = (select SUM(fi2.valcativa) from fi2 WHERE fi2.ftstamp='<<ALLTRIM(lcFtStamp)>>') WHERE ft2stamp = '<<ALLTRIM(lcFtStamp)>>'
					ENDTEXT
					If !uf_gerais_actGrelha("","",lcSQL1)
						uf_perguntalt_chama("OCORREU UMA ANOMALIA AO ATUALIZAR O VALOR DA CATIVA��O DO IVA NOS CABE�ALHOS. POR FAVOR CONTACTE O SUPORTE. OBRIGADO.","OK","",16)
					Endif
				Endif
				lcValidaInsercaoDocumento = .T.
			Endif
		Endif

		uf_gerais_regista_logs_passos("B_PAGCENTRAL",nrAtendimento,"b_pag6","depois de inserido uf_PAGAMENTO_finalizarAtendimento")

		** VERIFICAR SE EXISTEM FATURAS SEM LINHAS
		*!*			TEXT TO lcSQL1 NOSHOW TEXTMERGE
		*!*				select u_nratend,nmdoc, fno from ft (nolock) where CONVERT(varchar, fdata, 112)=CONVERT(varchar, getdate(), 112) and ousrhora>CONVERT(VARCHAR(8),dateadd(mi,-2,getdate()),108)  and (select count(fistamp) from fi (nolock) where fi.ftstamp=ft.ftstamp)=0
		*!*			ENDTEXT
		*!*			IF !uf_gerais_actGrelha("","UcrsDocSLin",lcSQL1)
		*!*				uf_perguntalt_chama("OCORREU UMA ANOMALIA AO TENTAR VERIFICAR DOCUMENTOS SEM LINHAS. POR FAVOR CONTACTE O SUPORTE. OBRIGADO.","OK","",16)
		*!*			ENDIF
		*!*			SELECT UcrsDocSLin
		*!*			IF RECCOUNT("UcrsDocSLin")>0
		*!*				LOCAL lcMensagem
		*!*				lcMensagem = ALLTRIM(ucrse1.id_lt) + ' - ' + ALLTRIM(ucrse1.nomecomp) + ' - ' + ALLTRIM(m_chinis) + ' - Documento emitido sem linhas '
		*!*				lcMensagem = lcMensagem + CHR(13) + 'Atendimento: ' + ALLTRIM(UcrsDocSLin.u_nratend) + ' | Documento: ' + ALLTRIM(UcrsDocSLin.nmdoc) + ' N� ' + ALLTRIM(str(UcrsDocSLin.fno))
		*!*				**uf_startup_sendmail_errors('ERRO!!! - Documento sem linhas emitido', lcMensagem )
		*!*			ENDIF
		*!*

		** Atualiza��o de valor em cart�o centralmente
		Select uCrsatendCl
		Local lccartaofidel
		lccartaofidel = Alltrim(uCrsatendCl.nrcartao)
		**IF uf_gerais_getParameter_site('ADM0000000133', 'BOOL', mySite) AND !EMPTY(lccartaofidel) AND uf_gerais_chkconexao()
		**	TEXT TO lcSQL1 NOSHOW TEXTMERGE
		**		select isnull(SUM(fi.valcartao),0) as valor from fi (nolock) WHERE fi.ftstamp in (select ftstamp from ft ftt (nolock) where ftt.u_nratend = (select u_nratend from ft (nolock) where ft.ftstamp='<<ALLTRIM(lcFtStamp)>>'))
		**	ENDTEXT
		**	IF !uf_gerais_actGrelha("","ucsvalcartaocentral",lcSQL1)
		**		uf_perguntalt_chama("OCORREU UMA ANOMALIA AO ATUALIZAR O VALOR DA CATIVA��O DO IVA NOS CABE�ALHOS. POR FAVOR CONTACTE O SUPORTE. OBRIGADO.","OK","",16)
		**	ENDIF
		**	SELECT ucsvalcartaocentral
		**	IF ucsvalcartaocentral.valor <> 0
		**		LOCAL lcvalcartaoimputar
		**		lcvalcartaoimputar = ucsvalcartaocentral.valor
		**		#DEFINE  FLAG_ICC_FORCE_CONNECTION 1
		**	 	DECLARE Long InternetCheckConnection IN Wininet.dll String Url, Long dwFlags, Long Reserved
		**
		**		LOCAL lcValidaLigInternet, lcUrl, lcRemoteSQL, lcRemoteDB, lcRemoteSRV
		**		STORE .t. TO lcvalidaLigInternet
		**		STORE '' TO lcUrl , lcRemoteSQL, lcRemoteDB, lcRemoteSRV
		**
		**		lcUrl = uf_gerais_getParameter_site('ADM0000000089', 'TEXT', mySite)
		**		lcRemoteSQL = uf_gerais_getParameter_site('ADM0000000090', 'TEXT', mySite)
		**		lcRemoteDB = substr(uf_gerais_getParameter_site('ADM0000000090', 'TEXT', mySite),at('].[',uf_gerais_getParameter_site('ADM0000000090', 'TEXT', mySite))+2 ,99)
		**		lcRemoteSRV = substr(uf_gerais_getParameter_site('ADM0000000090', 'TEXT', mySite), 1,at('].[',uf_gerais_getParameter_site('ADM0000000090', 'TEXT', mySite)))
		**
		**		lcSQL =''
		**		TEXT TO lcSQL NOSHOW TEXTMERGE
		**
		**			exec <<ALLTRIM(lcRemoteSQL)>>.[dbo].sp_update_valcartao_loja_dc
		**				'<<ALLTRIM(uCrsAtendCL.nrcartao)>>'
		**			,<<lcvalcartaoimputar>>
		**		ENDTEXT
		**		IF !uf_gerais_actgrelha("", "", lcSQL)
		**			uf_perguntalt_chama("Erro a registar o valor em cart�o na BD central. Por favor verifique.","OK","",64)
		**			**RETURN .f.
		**		ENDIF
		**	ENDIF
		**	fecha("ucsvalcartaocentral")
		**ENDIF


		If uf_gerais_getParameter_site('ADM0000000152', 'BOOL', mySite)
			uf_PAGAMENTO_grava_controlados()
		Endif

		**
		*!*			&& Enviar faturas comparticipadas via WS
		*!*		    IF CompSemSimul = .t.
		*!*		    	TEXT TO lcSQLdescr NOSHOW TEXTMERGE
		*!*					select td.tiposaft+ ' '+ltrim(rtrim(str(ft.ndoc)))+'/'+ltrim(rtrim(str(fno)))+' '+ltrim(rtrim(str(ftano))) as docdescr from ft (nolock)
		*!*					inner join td (nolock) on ft.ndoc=td.ndoc where ftstamp='<<ALLTRIM(lcFtStamp)>>'
		*!*				ENDTEXT
		*!*				IF !uf_gerais_actGrelha("","docdescr",lcSQLdescr)
		*!*					uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VALIDAR O N� DO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE. OBRIGADO.","OK","",16)
		*!*				ENDIF
		*!*				SELECT docdescr
		*!*		    	uf_compart_envia(ALLTRIM(lcFtStamp), ALLTRIM(docdescr.docdescr))
		*!*		    	fecha("docdescr")
		*!*		    ENDIF

		&& processamento das linhas dos dossiers afetos � venda
		If Used("ucrsLinFiBi")
			uf_pagamento_proclinhasenc()
		Endif

		&& fecho de documentos com as linhas totalmente satisfeitas
		If Used("ucrsLinFiBo")
			uf_pagamento_fechalinhasenc()
		Endif

		If Used("ucrsLinFiBi")
			fecha("ucrsLinFiBi")
		Endif
		If Used("ucrsLinFiBo")
			fecha("ucrsLinFiBo")
		Endif

		uf_gerais_regista_logs_passos("PAGAMENTO",nrAtendimento,"PAG19","FECHAR RESERVAS E ENC PAGAMENTO")

		** valida se a venda marcada como guardada foi gravada com sucesso
		** valida se tem stamp e se tem insert � ft e se paramentro est� activo
		If Used("uCrsCabVendas") And  At("insert into ft",Lower(lcSqlFinal))>0 And uf_gerais_getParameter('ADM0000000369', 'BOOL')
			Select uCrsCabVendas
			Go Top
			Scan
				Local lcDadosStamp
				lcDadosStamp= Alltrim(uCrsCabVendas.stamp)
				If(!Empty(uCrsCabVendas.saved))
					uf_pagamento_validaDocumentoEnviaEmail(Alltrim(lcDadosStamp),lcFindDocNr,lcFindNdoc,lcSqlFinal, nrAtendimento)
				Endif
			Endscan
		Endif

		&& VERIFICA��O SE DESAPARECERAM VENDAS
		*!*			IF UPPER(ALLTRIM(ucrse1.pais)) == 'PORTUGAL'
		*!*				lcSQL = ''
		*!*				TEXT TO lcSQL NOSHOW TEXTMERGE
		*!*					select isnull(MAX(fno),0) as noftcl, isnull((select MAX(fno) from docslogitools.dbo.ft (nolock) where ndoc=<<lcFindNdoc>> and ftano=YEAR(getdate()) and id_cl='<<ucrse1.id_lt>>'),0) as noftdocslogitools from ft (nolock) where ndoc=<<lcFindNdoc>> and ftano=YEAR(getdate())
		*!*				ENDTEXT
		*!*				IF !uf_gerais_actGrelha("","uCrsMaxNoS",lcSQL)
		*!*					uf_perguntalt_chama("OCORREU UMA ANOMALIA VERIFICAR A SEQU�NCIA DOS NUMEROS DOS DOCUMENTOS. OBRIGADO.","OK","",16)
		*!*				ELSE
		*!*					SELECT uCrsMaxNoS
		*!*					IF uCrsMaxNoS.noftcl-uCrsMaxNoS.noftdocslogitools > 1 OR uCrsMaxNoS.noftcl-uCrsMaxNoS.noftdocslogitools < -1
		*!*						LOCAL lcMensagem
		*!*						lcMensagem = 'Poss�vel desaparecimento de documentos'
		*!*						lcMensagem = lcMensagem  + CHR(13) + 'Cliente: ' + ALLTRIM(ucrse1.nomecomp)
		*!*						lcMensagem = lcMensagem  + CHR(13) + 'Loja: ' + ALLTRIM(ucrse1.siteloja)
		*!*						lcMensagem = lcMensagem  + CHR(13) + 'Nr. interno do documento: ' + str(lcFindNdoc)
		*!*						lcMensagem = lcMensagem  + CHR(13) + '�ltimo documento da BD auxiliar: ' + STR(uCrsMaxNoS.noftdocslogitools)
		*!*						lcMensagem = lcMensagem  + CHR(13) + '�ltimo documento da BD do cliente: ' + STR(uCrsMaxNoS.noftcl)
		*!*						lcMensagem = lcMensagem  + CHR(13) + 'Verificar de imediato o que se passa!'
		*!*					**	uf_startup_sendmail_errors('Erro na numera��o - DESAPARECIMENTO DOCUMENTOS', lcMensagem ) --esta a devolver spam
		*!*					ENDIF
		*!*				ENDIF
		*!*			ENDIF

		&& FASE 2 - Instrucao Gravacao extra Venda - Informa��o Auxiliar (n�o essencial � correta grava��o da venda) - INSERCAO TABELA RECEITUARIO

		uf_gerais_regista_logs_passos("PAGAMENTO",nrAtendimento,"PAG20","CRIA RECEITUARIO PAGAMENTO")

		If lcValidaInsercaoDocumento = .T.
			Select uCrsCabVendas
			Go Top
			Scan
				&&IF (uCrsCabVendas.partes == 0)  AND (uCrsCabVendas.Fact == .F. OR uCrsCabVendas.modoPag == .T. OR Right(alltrim(uCrsCabVendas.design),10) == "*SUSPENSA*" OR uCrsCabVendas.sel == .f. OR RIGHT(Alltrim(uCrsCabVendas.design),18) == "*SUSPENSA RESERVA*")
				If (uCrsCabVendas.partes == 0)  And (uCrsCabVendas.Fact == .F. Or uCrsCabVendas.modoPag == .T. Or Right(Alltrim(uCrsCabVendas.Design),10) == "*SUSPENSA*" Or uCrsCabVendas.sel == .F. Or Right(Alltrim(uCrsCabVendas.Design),18) == "*SUSPENSA RESERVA*");
						AND uCrsCabVendas.mantemreceita == .F.
					&& CRIAR CURSOR COM DADOS DE COMPARTICIPA��O DA VENDA
					If Left(uCrsCabVendas.Design,12) != ".SEM RECEITA"
						lcCod		= Strextract(uCrsCabVendas.Design, '[', ']', 1, 0)
						lcReceita	= Strextract(uCrsCabVendas.Design, ':', ' -', 1, 0)
						If !Empty(lcCod)
							If !Empty(astr(Strextract(uCrsCabVendas.Design, '-', '-', 1,2)))
								lcVia = astr(Strextract(uCrsCabVendas.Design, '-', '-', 1,2))
							Else
								lcVia = "Normal"
							Endif

							lcSQL = ''
							TEXT TO lcSql NOSHOW TEXTMERGE
								exec up_receituario_dadosDoPlano '<<lcCod>>'
							ENDTEXT
							If !uf_gerais_actGrelha("",[uCrsCodPla],lcSQL) Or Reccount([uCrsCodPla])=0
								uf_perguntalt_chama("O C�DIGO '"+lcCod+"' N�O EXISTE. POR FAVOR CONTACTE O SUPORTE.","OK","",16)

								&& anula receitas DEM registadas anteriormente
								uf_pagamento_receitasAnular()
								Return .F.
							Else
								lcCod2		= uCrsCodPla.codigo2
								lcDesign	= uCrsCodPla.Design
								lcDesign2	= uCrsCodPla.design2
								lcAbrev		= uCrsCodPla.cptorgabrev
								lcAbrev2	= uCrsCodPla.cptorgabrev2
							Endif
						Endif
					Else
						lcCod 		= ''
						lcReceita	= ''
						lcVia 		= ''
						lcCod2 		= ''
						lcDesign 	= ''
						lcDesign2 	= ''
						lcAbrev 	= ''
						lcAbrev2 	= ''
					Endif

					&& Verifica se � Suspensa ou N�o
					If Right(Alltrim(uCrsCabVendas.Design),10)=="*SUSPENSA*" Or Right(Alltrim(uCrsCabVendas.Design),18)=="*SUSPENSA RESERVA*"
						lcSusp = .T.
					Else
						lcSusp = .F.
					Endif

					&& atualiza o fno no cursor uCrsFT para utilizar na impress�o
					lcSQL = ''
					TEXT TO lcSQL TEXTMERGE NOSHOW
						SELECT FNO FROM FT (NOLOCK) WHERE FTSTAMP = '<<ALLTRIM(uCrsCabVendas.STAMP)>>'
					ENDTEXT
					If !uf_gerais_actGrelha("","uCrsAuxFNO", lcSQL)
						uf_pergunta_chama("OCORREU UMA ANOMALIA AO VERIFICAR O N� DO DOCUMENTO PARA IMPRESS�O. POR FAVOR CONTATE O SUPORTE. OBRIGADO", "OK", "", 16)

						&& anula receitas DEM registadas anteriormente
						uf_pagamento_receitasAnular()
					Else
						Select  uCrsFt
						Go Top
						Scan For Alltrim(uCrsFt.ftstamp) == Alltrim(uCrsCabVendas.stamp)
							Replace uCrsFt.fno With uCrsAuxFNO.fno
						Endscan
					Endif

					If Used("uCrsAuxFNO")
						fecha("uCrsAuxFNO")
					Endif


					&& Criar Lotes
					If (!Empty(lcCod)) And (lcSusp=.F.)
						For i = 1 To 3
							If uf_receituario_VerificaLote("Front", lcCod, lcCod2, uCrsCabVendas.stamp, lcVia, lcReceita, uCrsCabVendas.token)
								i = 3
							Else
								If i == 3
									uf_perguntalt_chama("OCORREU UMA ANOMALIA AO REGISTAR A VENDA NOS LOTES. TEM DE INTRODUZIR A RECEITA MANUALMENTE. OBRIGADO.","OK","",16)
									uf_gerais_registaerrolog("Ocorreu uma anomalia ao registar a venda nos Lotes - " + Chr(13) + "Local: Pagamento - FASE 2")

									&& anula receitas DEM registadas anteriormente
									uf_pagamento_receitasAnular()
								Endif
							Endif
						Endfor
					Endif

				Endif
			Endscan
		Endif

		uf_gerais_regista_logs_passos("PAGAMENTO",nrAtendimento,"PAG21","FIM CRIA RECEITUARIO PAGAMENTO")
		uf_gerais_regista_logs_passos("PAGAMENTO",nrAtendimento,"PAG22","CONTROLO RESERVAS E ARQUIVO DIGITAL PAGAMENTO")
		&& FASE 2.1 Controlo de Reservas e Arquivo Digital - Lu�s Leal 20160511
		Select uCrsCabVendas
		Go Top
		Scan
			&& Abate as reservas
			If myReservasCl
				uf_reservas_abater()
			Endif
			**

			&& gravar arquivo digital
			If myArquivoDigital
				uf_arquivoDigital_TaloesLt(uCrsCabVendas.stamp)
			Endif

			If myArquivoDigitalPDF
				If uf_gerais_getParameter("ADM0000000191","BOOL")
					Select * From uCrsFt  Into Cursor uCrsFtBak Readwrite
					Select * From uCrsFt2 Into Cursor uCrsFt2Bak Readwrite
					Select * From uCrsFi  Into Cursor uCrsFiBak Readwrite
					Select * From uCrsFi2  Into Cursor uCrsFi2Bak Readwrite

					Select uCrsCabVendas
					Select uCrsFt
					Delete For Alltrim(uCrsFt.ftstamp) != Alltrim(uCrsCabVendas.stamp)
					Select uCrsFt2
					Delete For Alltrim(uCrsFt2.ft2stamp) != Alltrim(uCrsCabVendas.stamp)
					Select uCrsFi
					Delete For Alltrim(uCrsFi.ftstamp) != Alltrim(uCrsCabVendas.stamp)
					Select uCrsFi2
					Delete For Alltrim(uCrsFi2.ftstamp) != Alltrim(uCrsCabVendas.stamp)

					Select * From uCrsFtBak  Into Cursor uCrsFt  Readwrite
					Select * From uCrsFt2Bak Into Cursor uCrsFt2 Readwrite
					Select * From uCrsFiBak  Into Cursor uCrsFi  Readwrite
					Select * From uCrsFi2Bak  Into Cursor uCrsFi2  Readwrite
				Endif
			Endif

			Select uCrsCabVendas
		Endscan

		uf_gerais_regista_logs_passos("PAGAMENTO",nrAtendimento,"PAG23","REGULARIZAR VENDAS PAGAMENTO")

		&& FASE 3 - CASO NECESS�RIO REGULARIZAR VENDAS CRIA DOCUMENTOS DE REGULARIZA��O
		Local lcNo, lcEstab, lcValePos, lcStamporigproc
		Store 0 To lcNo, lcEstab, lcValePos
		Store '' To lcStamporigproc

		Select ft
		lcNo = ft.no
		lcEstab = ft.estab

		If Used("uCrsVale")

			Select ucrsvale
			Go Top

			Select Distinct no, estab, Alltrim(Upper(Nome)) As Nome, Alltrim(ncont) As ncont From ucrsvale Into Cursor uCrsValidaClReg Readwrite

			Select ucrsvale
			Go Top
			Select Distinct no From ucrsvale Into Cursor uCrsValidaClRegNo Readwrite

			&& Cen�rio 1 - REGULARIZAR PRODUTOS QUE VAO ORIGINAR DOCUMENTOS DE REC. QUE N�O MEXAM EM CAIXA
			Select ucrsvale
			Go Top
			Scan For ucrsvale.factPago=.T.
				Local lcPos
				lcPos=Recno("uCrsVale")

				&& CRIAR CURSORES COM DADOS DAS LINHAS A REGULARIZAR
				&& levanta dados da fatura gerada para poder emitir um recibo sobre a mesma
				lcSQL = ''
				TEXT TO lcSQL NOSHOW TEXTMERGE
					SELECT
						convert(bit,0) as manipulado
						,convert(varchar(2),'') as tipoR
						,fi.id_Dispensa_Eletronica_D as id_validacaoDem
						,convert(varchar(20),'') as receita
						,convert(varchar(40),'') as token
						,convert(bit,0) as dem
						,0 as qtDem
						,convert(varchar(250),design) as design
						,convert(bit,0) as usalote
						,convert(bit,0) as alertalote
						,convert(bit,0) as compSNS
						,convert(varchar(8),'') as CNPEM
						,0.000 as PvpTop4
						,*
					from
						FI (nolock)
						left join fi_comp (nolock) on fi.fistamp = fi_comp.id_fi
					WHERE
						ofistamp = '<<alltrim(uCrsVale.fistamp)>>'
				ENDTEXT
				uf_gerais_actGrelha("",[uCrsFiReg],lcSQL)
				Select uCrsFiReg
				If ucrsvale.eaquisicao != 0 && !!!
					&&uf_gerais_registaErroLog("Tracking buraco", "V1")
					** se o valor a regularizar for inferior ao valor faturado, recibar apenas o valor a regularizar caso contr�rio regularizar o valor faturado
					If uCrsFiReg.etiliquido > ucrsvale.etiliquido
						Replace uCrsFiReg.qtt			With Round(ucrsvale.eaquisicao,2)
						Replace uCrsFiReg.etiliquido	With (ucrsvale.eaquisicao*ucrsvale.etiliquido/ucrsvale.qtt)
						Replace uCrsFiReg.tiliquido		With (ucrsvale.eaquisicao*ucrsvale.etiliquido/ucrsvale.qtt)*200.482
						Replace uCrsFiReg.esltt			With (ucrsvale.eaquisicao*ucrsvale.etiliquido/ucrsvale.qtt)
						Replace uCrsFiReg.sltt			With (ucrsvale.eaquisicao*ucrsvale.etiliquido/ucrsvale.qtt)*200.482
						Replace uCrsFiReg.tliquido		With (ucrsvale.eaquisicao*ucrsvale.etiliquido/ucrsvale.qtt)
					Else
						Replace uCrsFiReg.qtt			With Round(ucrsvale.eaquisicao,2)
						Replace uCrsFiReg.etiliquido	With (ucrsvale.eaquisicao*uCrsFiReg.etiliquido/ucrsvale.qtt)
						Replace uCrsFiReg.tiliquido		With (ucrsvale.eaquisicao*uCrsFiReg.etiliquido/ucrsvale.qtt)*200.482
						Replace uCrsFiReg.esltt			With (ucrsvale.eaquisicao*uCrsFiReg.etiliquido/ucrsvale.qtt)
						Replace uCrsFiReg.sltt			With (ucrsvale.eaquisicao*uCrsFiReg.etiliquido/ucrsvale.qtt)*200.482
						Replace uCrsFiReg.tliquido		With (ucrsvale.eaquisicao*uCrsFiReg.etiliquido/ucrsvale.qtt)
					Endif
				Endif
				Select uCrsFiReg
				Go Top
				If !Used("uCrsFi2Reg")
					* CRIAR CURSORES PARA AGREGAR DADOS DAS LINHAS
					Select fi
					Select * From fi Where 1=0 Into Cursor uCrsFi2Reg Readwrite
					**SELECT * FROM uCrsFiReg INTO CURSOR uCrsFi2Reg WHERE 1=0 Readwrite
				Endif

				Select uCrsFi2Reg && Agregar dados das vendas aos cursores criados anteriormente **
				Append From Dbf("uCrsFiReg")
				Delete For Empty(uCrsFiReg.ref)

				fecha("uCrsFiReg")

				*!*					Select uCrsVale
				*!*	                TRY
				*!*	                    GO lcPos
				*!*	                CATCH
				*!*	                ENDTRY
			Endscan

			Local lcValidRec
			Store .F. To lcValidRec
			Select uCrsCabVendas
			Go Top
			Scan
				If Vartype(lcProdJaPagos)!='U'
					If uCrsCabVendas.modoPag==.T. And lcProdJaPagos = .F.
						lcValidRec = .T.
					Endif
				Else
					If uCrsCabVendas.modoPag==.T.
						lcValidRec = .T.
					Endif
				Endif
			Endscan

			If Used("uCrsFi2Reg") && CRIAR DOCUMENTOS DE REGULARIZA��O PARA ESTE CLIENTE
				If Reccount("uCrsFi2Reg")>0
					* criar rec. que n�o mexe em caixa
					If lcValidRec = .F.
						&&uf_gerais_registaErroLog("Tracking buraco", "V2")
						uf_gerais_actGrelha("",[uCrsClReg],[select nome, nome2, no, tipo, morada, local, pais, codpost, ncont, estab, telefone, ccusto from b_utentes (nolock) where b_utentes.no=]+Alltrim(Str(lcNo))+[ and b_utentes.estab=]+Alltrim(Str(lcEstab)))
						uf_PAGAMENTO_criarDocRec(.T.)

						&& o valor do acerto est� a ser "somado" e n�o calculada � linha por cabe�alho de venda - para n�o estar a mexer no calculo pq ter� outra implica��es, encontro o valor mais alto do acerto
						&& que � o valor correto e crio apenas um acerto com esse valor - LL 18042016
						Local lcAuxAcerto
						Store 0 To lcAuxAcerto

						Select uCrsCabVendas
						Go Top
						Scan
							If uCrsCabVendas.eacerto > 0
								&& guarda o valor de acerto >
								If uCrsCabVendas.eacerto > lcAuxAcerto
									lcAuxAcerto = uCrsCabVendas.eacerto
								Endif
							Endif

							Select uCrsCabVendas
						Endscan

						If lcAuxAcerto > 0
							Select uCrsClReg
							Go Top
							&&uf_gerais_registaErroLog("Tracking buraco", "V3")
							uf_pagamento_criarAcertoCC(lcAuxAcerto, uCrsClReg.no, uCrsClReg.estab, uCrsClReg.Nome, uCrsClReg.tipo, uCrsClReg.pais)
						Endif
						**
					Else
						**uf_perguntalt_chama("N�O PODE EMITIR UM RECIBO NORMAL NUMA VENDA A CR�DITO","OK","",64)
						**return(.f.)
					Endif

				Endif
				fecha("uCrsFi2Reg")
			Endif
			**

			&& cen�rio 2 - REGULARIZAR PRODUTOS QUE VAO ORIGINAR DOCUMENTOS DE REG. E REC. QUE N�O MEXAM EM CAIXA **
			Select uCrsValidaClReg
			Go Top
			Scan && PERCORRER CLIENTE A CLIENTE
				Select ucrsvale
				Go Top
				Scan For (ucrsvale.no=uCrsValidaClReg.no And ucrsvale.estab=uCrsValidaClReg.estab) And ucrsvale.factPago=.T.
					Local lcPos
					lcPos=Recno("uCrsVale")
					&&uf_gerais_registaErroLog("Tracking buraco", "V4")
					lcSQL = ''
					TEXT TO lcSQL NOSHOW TEXTMERGE
						select
							convert(bit,0) as manipulado
							,convert(varchar(2),'') as tipoR
							,fi.id_Dispensa_Eletronica_D as id_validacaoDem
							,convert(varchar(20),'') as receita
							,convert(varchar(40),'') as token
							,convert(bit,0) as dem
							,0 as qtDem
							,convert(varchar(250),design) as design
							,convert(bit,0) as usalote
							,convert(bit,0) as alertalote
							,convert(bit,0) as compSNS
							,convert(varchar(8),'') as CNPEM
							,0.000 as PvpTop4
							,*
						from
							FI (nolock)
							left join fi_comp (nolock) on fi.fistamp = fi_comp.id_fi
						WHERE
							fistamp = '<<alltrim(uCrsVale.fistamp)>>'
					ENDTEXT

					uf_gerais_actGrelha("",[uCrsFiReg],lcSQL) && CRIAR CURSORES COM DADOS DAS LINHAS A REGULARIZAR **

					Select uCrsFiReg
					If uCrsFiReg.qtt!=ucrsvale.qtt
						Replace uCrsFiReg.qtt			With ucrsvale.qtt
						Replace uCrsFiReg.etiliquido	With ucrsvale.etiliquido
						Replace uCrsFiReg.tiliquido		With ucrsvale.etiliquido*200.482
						Replace uCrsFiReg.esltt			With ucrsvale.etiliquido
						Replace uCrsFiReg.sltt			With ucrsvale.etiliquido*200.482
						Replace uCrsFiReg.tliquido		With ucrsvale.etiliquido
					Endif
					Select uCrsFiReg
					Go Top
					If !Used("uCrsFi2Reg")
						**Select * from uCrsFiReg Into Cursor uCrsFi2Reg Where 1=0 Readwrite && CRIAR CURSORES PARA AGREGAR DADOS DAS LINHAS **
						Select fi
						Select * From fi Where 1=0 Into Cursor uCrsFi2Reg Readwrite
					Endif

					Select uCrsFi2Reg && Agregar dados das vendas aos cursores criados anteriormente **
					Append From Dbf("uCrsFiReg")
					Delete For Empty(uCrsFiReg.ref)
					fecha("uCrsFiReg")

					*!*						Select uCrsVale
					*!*		                TRY
					*!*		                    GO lcPos
					*!*		                CATCH
					*!*		                ENDTRY
				Endscan

				If Used("uCrsFi2Reg") && CRIAR DOCUMENTOS DE REGULARIZA��O PARA ESTE CLIENTE
					If Reccount("uCrsFi2Reg")>0
						&&uf_gerais_registaErroLog("Tracking buraco", "V5")
						* gravar reg e rec n�o mexam em caixa
						lcSQL = ""
						TEXT TO lcSql NOSHOW TEXTMERGE
							select
								nome, nome2, no, morada, local, codpost
								, ncont, estab, telefone, ccusto
							from
								b_utentes (nolock)
							where
								b_utentes.no = <<uCrsValidaClReg.no>>
								and b_utentes.estab = <<uCrsValidaClReg.estab>>
						ENDTEXT

						uf_gerais_actGrelha("",[uCrsClReg],lcSQL)

						uf_PAGAMENTO_criarDocReg(.T., .T.)
					Endif
					fecha("uCrsFi2Reg")
				Endif

				Select uCrsValidaClReg
			Endscan
			**
			&& Cen�rio 3 - REGULARIZAR PRODUTOS QUE VAO ORIGINAR DOCUMENTOS DE REG. E REC.
			Select uCrsValidaClReg
			Go Top
			Scan && PERCORRER CLIENTE A CLIENTE
				&&uf_gerais_registaErroLog("Tracking buraco", "V6")
				Select  ucrsvale
				Go Top
				Scan For (ucrsvale.no == uCrsValidaClReg.no And ucrsvale.estab == uCrsValidaClReg.estab And Upper(Alltrim(ucrsvale.Nome)) == Upper(Alltrim(uCrsValidaClReg.Nome)));
						AND ucrsvale.factPago=.F.;
						AND ((ucrsvale.lancacc != .T. And ucrsvale.dev!=2) Or (ucrsvale.lancacc=.T. And (ucrsvale.Fact!=.T. Or (ucrsvale.Fact=.T. And ucrsvale.dev!=0) Or ucrsvale.fmarcada=.T.) And !(ucrsvale.fmarcada=.T. And ucrsvale.dev=2)));
						AND ucrsvale.ncont=uCrsValidaClReg.ncont

					Local lcPos
					lcPos=Recno("uCrsvale")

					&& devolu��es de vendas que n�o tenham sido dispensadas na loja atual
					If ucrsvale.fno == 0
						&& usa o cursor fi do atendimento pq os dados n�o existem na BD
						Select Top 1 * From fi Into Cursor uCrsFiReg Where fi.partes = 1 And Alltrim(fi.fistamp)=Alltrim(ucrsvale.fistamp)  Order By fi.ref Readwrite

					Else
						lcSQL = ""
						TEXT TO lcSQL NOSHOW TEXTMERGE
							Select
								convert(bit,0) as manipulado
								,convert(varchar(2),'') as tipoR
								,fi.id_Dispensa_Eletronica_D as id_validacaoDem
								,convert(varchar(20),'') as receita
								,convert(varchar(40),'') as token
								,convert(bit,0) as dem
								,0 as qtDem
								,convert(varchar(250),design) as design
								,convert(bit,0) as usalote
								,convert(bit,0) as alertalote
								,convert(bit,0) as compSNS
								,convert(varchar(8),'') as CNPEM
								,0.000 as PvpTop4
								,0 as qttrobot
								,*
							from
								fi (nolock)
								left join fi_comp (nolock) on fi.fistamp = fi_comp.id_fi
							where
								fistamp='<<alltrim(uCrsVale.fistamp)>>'
						ENDTEXT

						uf_gerais_actGrelha("",[uCrsFiReg],lcSQL) && CRIAR CURSORES COM DADOS DAS LINHAS A REGULARIZAR **
					Endif


					Select uCrsFiReg
					If uCrsFiReg.qtt!=ucrsvale.qtt
						Replace uCrsFiReg.qtt			With ucrsvale.qtt
						Replace uCrsFiReg.etiliquido	With ucrsvale.etiliquido
						Replace uCrsFiReg.tiliquido		With ucrsvale.etiliquido*200.482
						Replace uCrsFiReg.esltt			With ucrsvale.etiliquido
						Replace uCrsFiReg.sltt			With ucrsvale.etiliquido*200.482
						Replace uCrsFiReg.tliquido		With ucrsvale.etiliquido
						Replace uCrsFiReg.fmarcada		With ucrsvale.fmarcada
						Replace uCrsFiReg.eaquisicao	With ucrsvale.eaquisicao
					Endif
					Select uCrsFiReg
					Go Top
					If !Used("uCrsFi2Reg")
						**SELECT * from uCrsFiReg INTO Cursor uCrsFi2Reg Where 1=0 Readwrite && CRIAR CURSORES PARA AGREGAR DADOS DAS LINHAS **
						Select fi
						Select * From fi Where 1=0 Into Cursor uCrsFi2Reg Readwrite
					Endif

					Select uCrsFi2Reg && Agregar dados das vendas aos cursores criados anteriormente **
					Append From Dbf("uCrsFiReg")
					Delete For Empty(uCrsFiReg.ref)

					fecha("uCrsFiReg")

					*!*						SELECT uCrsVale
					*!*		                TRY
					*!*		                    GO lcPos
					*!*		                CATCH
					*!*		                ENDTRY
				Endscan



				If Used("uCrsFi2Reg") && CRIAR DOCUMENTOS DE REGULARIZA��O PARA ESTE CLIENTE
					If Reccount("uCrsFi2Reg") > 0
						&&uf_gerais_registaErroLog("Tracking buraco", "V7")
						&& vou criar reg e rec
						lcSQL = ""
						TEXT TO lcSQL NOSHOW TEXTMERGE
							select
								nome, nome2, no, morada
								,local, codpost, ncont
								,estab, telefone, ccusto
							from
								b_utentes (nolock)
							where
								b_utentes.no = <<uCrsValidaClReg.no>>
								and b_utentes.estab = <<uCrsValidaClReg.estab>>
						ENDTEXT
						uf_gerais_actGrelha("",[uCrsClReg],lcSQL)


						** Se for Cliente generico e tiver sido alterado o nome, o documentos de Regulariza��o deve conter o nome do documento de origem - alterado para colocar contribuinte  12/11/2015 LL
						** o nome e ncont dos documentos de regulariza��o t�m de ser obrigatoriamente os mesmo dos documentos de origem, alterado para ficar nessa l�gica 21/12/2015 LL
						** IF uCrsValidaClReg.no == 200
						If !Empty(uCrsValidaClReg.Nome)
							Select uCrsClReg
							Replace uCrsClReg.Nome With Alltrim(uCrsValidaClReg.Nome)
							Replace uCrsClReg.ncont With Alltrim(uCrsValidaClReg.ncont)
						Endif
						**ENDIF

						uf_PAGAMENTO_criarDocReg(.T.)
					Endif
					fecha("uCrsFi2Reg")
				Endif

				Select uCrsValidaClReg
			Endscan

			&& REGULARIZAR PRODUTOS QUE VAO ORIGINAR DOCUMENTOS DE REC.
			If lcModoFac == "FACTURA��O ACORDO" Or lcModoFac == "FACTURA��O LAR"
				&&uf_gerais_registaErroLog("Tracking buraco", "V8")
				Select uCrsValidaClRegNo
				Go Top
				Scan && PERCORRER CLIENTE A CLIENTE
					Select ucrsvale
					Go Top
					Scan For ucrsvale.no=uCrsValidaClRegNo.no And ucrsvale.factPago=.F.;
							AND (ucrsvale.lancacc=.T. And ucrsvale.dev=0 And ucrsvale.Fact=.T.) And ucrsvale.fmarcada=.F.

						Local lcPos
						lcPos=Recno("uCrsVale")

						lcSQL =""
						TEXT TO lcSQL NOSHOW TEXTMERGE
							select
								convert(bit,0) as manipulado
								,convert(varchar(2),'') as tipoR
								,fi.id_Dispensa_Eletronica_D as id_validacaoDem
								,convert(varchar(20),'') as receita
								,convert(varchar(40),'') as token
								,convert(bit,0) as dem
								,0 as qtDem
								,convert(varchar(250),design) as design
								,convert(bit,0) as usalote
								,convert(bit,0) as alertalote
								,convert(bit,0) as compSNS
								,convert(varchar(8),'') as CNPEM
								,0.000 as PvpTop4
								,*
							from
								fi (nolock)
								left join fi_comp (nolock) on fi.fistamp = fi_comp.id_fi
							where
								fistamp='<<alltrim(uCrsVale.fistamp)>>'
						ENDTEXT
						uf_gerais_actGrelha("",[uCrsFiReg],lcSQL)
						Select uCrsFiReg
						If uCrsFiReg.qtt!=ucrsvale.qtt
							Replace uCrsFiReg.qtt			With ucrsvale.qtt
							Replace uCrsFiReg.etiliquido	With ucrsvale.etiliquido
							Replace uCrsFiReg.tiliquido		With ucrsvale.etiliquido*200.482
							Replace uCrsFiReg.esltt			With ucrsvale.etiliquido
							Replace uCrsFiReg.sltt			With ucrsvale.etiliquido*200.482
							Replace uCrsFiReg.tliquido		With ucrsvale.etiliquido
							Replace uCrsFiReg.fmarcada		With ucrsvale.fmarcada
						Endif
						Select uCrsFiReg
						Go Top
						If !Used("uCrsFi2Reg")
							**Select * from uCrsFiReg Into Cursor uCrsFi2Reg Where 1=0 Readwrite && CRIAR CURSORES PARA AGREGAR DADOS DAS LINHAS **
							Select fi
							Select * From fi Where 1=0 Into Cursor uCrsFi2Reg Readwrite
						Endif

						Select uCrsFi2Reg && Agregar dados das vendas aos cursores criados anteriormente **
						Append From Dbf("uCrsFiReg")
						Delete For Empty(uCrsFiReg.ref)

						fecha("uCrsFiReg")

						*!*							SELECT uCrsVale
						*!*			                TRY
						*!*			                    GO lcPos
						*!*			                CATCH
						*!*			                ENDTRY
					Endscan

					Local lcValidRec1
					Store .F. To lcValidRec1
					Select uCrsCabVendas
					Go Top
					Scan
						If uCrsCabVendas.modoPag = .T.
							lcValidRec1 = .T.
						Endif
					Endscan

					If Used("uCrsFi2Reg") && CRIAR DOCUMENTOS DE REGULARIZA��O PARA ESTE CLIENTE
						If Reccount("uCrsFi2Reg")>0
							If lcValidRec1 = .F.
								&& criar apenas rec.
								uf_gerais_actGrelha("",[uCrsClReg],[select nome, nome2, no, morada, local, codpost, ncont, estab, telefone, ccusto from b_utentes (nolock) where b_utentes.estab=0 and b_utentes.no=]+Alltrim(Str(uCrsValidaClRegNo.no)))

								uf_PAGAMENTO_criarDocRec()
							Else
								uf_perguntalt_chama("N�O PODE EMITIR UM RECIBO NORMAL NUMA VENDA A CR�DITO","OK","",64)
							Endif
						Endif
						fecha("uCrsFi2Reg")
					Endif

					Select uCrsValidaClRegNo
				Endscan
			Else

				&&uf_gerais_registaErroLog("Tracking buraco", "V9")

				Select uCrsValidaClReg
				Go Top
				Scan && PERCORRER CLIENTE A CLIENTE
					Select  ucrsvale
					Go  Top
					Scan For (ucrsvale.no=uCrsValidaClReg.no And ucrsvale.estab=uCrsValidaClReg.estab);
							AND ucrsvale.factPago=.F.;
							AND (ucrsvale.lancacc=.T. And ucrsvale.dev=0 And ucrsvale.Fact=.T.) And ucrsvale.fmarcada=.F.

						Local lcPos
						lcPos=Recno("uCrsVale")

						TEXT TO lcSQL NOSHOW TEXTMERGE
							select
								convert(bit,0) as manipulado
								,convert(varchar(2),'') as tipoR
								,fi.id_Dispensa_Eletronica_D as id_validacaoDem
								,convert(varchar(20),'') as receita
								,convert(varchar(40),'') as token
								,convert(bit,0) as dem
								,0 as qtDem
								,convert(varchar(250),design) as design
								,convert(bit,0) as usalote
								,convert(bit,0) as alertalote
								,convert(bit,0) as compSNS
								,convert(varchar(8),'') as CNPEM
								,0.000 as PvpTop4
								,*
							from
								FI (nolock)
								left join fi_comp (nolock) on fi.fistamp = fi_comp.id_fi
							WHERE
								fistamp='<<alltrim(uCrsVale.fistamp)>>'
						ENDTEXT

						uf_gerais_actGrelha("",[uCrsFiReg],lcSQL) && CRIAR CURSORES COM DADOS DAS LINHAS A REGULARIZAR **
						Select uCrsFiReg
						If uCrsFiReg.qtt!=ucrsvale.qtt
							Replace uCrsFiReg.qtt			With ucrsvale.qtt
							Replace uCrsFiReg.etiliquido	With ucrsvale.etiliquido
							Replace uCrsFiReg.tiliquido		With ucrsvale.etiliquido*200.482
							Replace uCrsFiReg.esltt			With ucrsvale.etiliquido
							Replace uCrsFiReg.sltt			With ucrsvale.etiliquido*200.482
							Replace uCrsFiReg.tliquido		With ucrsvale.etiliquido
							Replace uCrsFiReg.fmarcada		With ucrsvale.fmarcada
						Endif

						Select uCrsFiReg
						Go Top
						If !Used("uCrsFi2Reg")
							**Select * from uCrsFiReg Into Cursor uCrsFi2Reg Where 1=0 Readwrite && CRIAR CURSORES PARA AGREGAR DADOS DAS LINHAS **
							Select fi
							Select * From fi Where 1=0 Into Cursor uCrsFi2Reg Readwrite
						Endif

						Select uCrsFi2Reg && Agregar dados das vendas aos cursores criados anteriormente **
						Append From Dbf("uCrsFiReg")
						Delete For Empty(uCrsFiReg.ref)

						fecha("uCrsFiReg")

						*!*							Select uCrsVale
						*!*							TRY
						*!*			                    GO lcPos
						*!*			                CATCH
						*!*			                ENDTRY
					Endscan

					If Used("uCrsFi2Reg") && CRIAR DOCUMENTOS DE REGULARIZA��O PARA ESTE CLIENTE

						If Reccount("uCrsFi2Reg")>0
							&& criar apenas rec
							uf_gerais_actGrelha("",[uCrsClReg],[select nome, nome2, no, morada, local, codpost, ncont, estab, telefone, ccusto from b_utentes (nolock) where b_utentes.no=]+Alltrim(Str(uCrsValidaClReg.no))+[ and b_utentes.estab=]+Alltrim(Str(uCrsValidaClReg.estab)))

							uf_PAGAMENTO_criarDocRec()
						Endif
						fecha("uCrsFi2Reg")
					Endif

					Select uCrsValidaClReg
				Endscan
			Endif

			uf_gerais_regista_logs_passos("PAGAMENTO",nrAtendimento,"PAG24","REGULARIZAR PRODUTOS PAGAMENTO")

			&&uf_gerais_registaErroLog("Tracking buraco", "V10")
			&& REGULARIZAR PRODUTOS QUE VAO ORIGINAR DOCUMENTOS DE REG.
			Select uCrsValidaClReg
			Go Top
			Scan && PERCORRER CLIENTE A CLIENTE
				Select ucrsvale
				Go Top
				Scan For(ucrsvale.no=uCrsValidaClReg.no And ucrsvale.estab=uCrsValidaClReg.estab);
						AND ucrsvale.factPago=.F.;
						AND (ucrsvale.lancacc=.T. And ucrsvale.dev=2 And ucrsvale.fmarcada=.T.) Or (ucrsvale.lancacc=.F. And ucrsvale.dev=2)

					Local lcPos
					lcPos=Recno("uCrsVale")

					TEXT TO lcSQL NOSHOW TEXTMERGE
						select
							convert(bit,0) as manipulado
							,convert(varchar(2),'') as tipoR
							,fi.id_Dispensa_Eletronica_D as id_validacaoDem
							,convert(varchar(20),'') as receita
							,convert(varchar(40),'') as token
							,convert(bit,0) as dem
							,0 as qtDem
							,convert(varchar(250),design) as design
							,convert(bit,0) as usalote
							,convert(bit,0) as alertalote
							,convert(bit,0) as compSNS
							,convert(varchar(8),'') as CNPEM
							,0.000 as PvpTop4
							,*
						from
							FI (nolock)
							left join fi_comp (nolock) on fi.fistamp = fi_comp.id_fi
						WHERE
							fistamp='<<alltrim(uCrsVale.fistamp)>>'
					ENDTEXT

					uf_gerais_actGrelha("",[uCrsFiReg],lcSQL) && CRIAR CURSORES COM DADOS DAS LINHAS A REGULARIZAR **
					Select uCrsFiReg
					If uCrsFiReg.qtt!=ucrsvale.qtt
						Replace uCrsFiReg.qtt			With ucrsvale.qtt
						Replace uCrsFiReg.etiliquido	With ucrsvale.etiliquido
						Replace uCrsFiReg.tiliquido		With ucrsvale.etiliquido*200.482
						Replace uCrsFiReg.esltt			With ucrsvale.etiliquido
						Replace uCrsFiReg.sltt			With ucrsvale.etiliquido*200.482
						Replace uCrsFiReg.tliquido		With ucrsvale.etiliquido
						Replace uCrsFiReg.fmarcada		With ucrsvale.fmarcada
					Endif
					Select uCrsFiReg
					Go  Top
					If !Used("uCrsFi2Reg")
						**Select * from uCrsFiReg Into Cursor uCrsFi2Reg Where 1=0 READWRITE && CRIAR CURSORES PARA AGREGAR DADOS DAS LINHAS **
						Select fi
						Select * From fi Where 1=0 Into Cursor uCrsFi2Reg Readwrite
					Endif

					Select uCrsFi2Reg && Agregar dados das vendas aos cursores criados anteriormente **
					Append From Dbf("uCrsFiReg")
					Delete For Empty(uCrsFiReg.ref)

					fecha("uCrsFiReg")
					*!*
					*!*						Select uCrsVale
					*!*		                TRY
					*!*		                    GO lcPos
					*!*		                CATCH
					*!*		                ENDTRY
				Endscan

				If Used("uCrsFi2Reg") && CRIAR DOCUMENTOS DE REGULARIZA��O PARA ESTE CLIENTE
					If Reccount("uCrsFi2Reg")>0
						&& criar apenas reg
						uf_gerais_actGrelha("",[uCrsClReg],[select nome, nome2, no, morada, local, codpost, ncont, estab, telefone, ccusto from b_utentes (nolock) where b_utentes.no=]+Alltrim(Str(uCrsValidaClReg.no))+[ and b_utentes.estab=]+Alltrim(Str(uCrsValidaClReg.estab)))

						uf_PAGAMENTO_criarDocReg(.F.)
					Endif
					fecha("uCrsFi2Reg")
				Endif

				Select uCrsValidaClReg
			Endscan

			If Used("uCrsClReg")
				fecha("uCrsClReg")
			Endif

			Select fi
			lcPosFi = Recno("Fi")

			Select fi
			Go Top
			Scan
				If !Empty(fi.ofistamp)
					lcStamporigproc = Alltrim(fi.ofistamp)
				Endif
			Endscan

			Select fi
			Try
				Go lcPosFi
			Catch
				Go Top
			Endtry

			** Anula��o da fatura��o � seguradora
			If uf_gerais_getParameter_site('ADM0000000074', 'BOOL', mySite) And !Empty(lcStamporigproc)
				Local lcStampCredito
				Store '' To lcStampCredito
				lcSQL = ''
				TEXT TO lcSQL NOSHOW TEXTMERGE
					select ft2stamp from ft2 where stamporigproc=(select ftstamp from fi where fistamp='<<ALLTRIM(lcStamporigproc)>>')
				ENDTEXT

				If !uf_gerais_actGrelha("","uCrsProcSeg",lcSQL)
					uf_perguntalt_chama("OCORREU UMA ANOMALIA VERIFICAR O DOCUMENTO ASSOCIADO AO PROCESSO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
					Return .F.
				Endif
				Select uCrsProcSeg
				If Reccount("uCrsProcSeg") > 0
					uf_pagamento_nc_seguradora(Alltrim(uCrsProcSeg.ft2stamp))
				Endif

				fecha("uCrsProcSeg")
			Endif

		Endif


		uf_gerais_regista_logs_passos("PAGAMENTO",nrAtendimento,"PAG25","INSERIR REGISTO CAIXA PAGAMENTO")

		&& Insert na PagCentral Caso seja apenas uma devolu��o a Cliente, ou recebimento de uma Fatura
		If lcVendaCount = 0

			If !Empty(lcSQLPagCentral)
				lcSQL = ''
				TEXT TO lcSQL NOSHOW TEXTMERGE
					exec up_gerais_execSql 'Atendimento - Insert', 1,'<<lcSQLPagCentral>>', '', '', '' , '', ''
				ENDTEXT

				If !uf_gerais_actGrelha("","uCrsPagCentralAux",lcSQL)
					uf_perguntalt_chama("OCORREU UMA ANOMALIA AO GRAVAR O VALOR DO ATENDIMENTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
					Return .F.
				Endif
			Endif
		Endif

		uf_gerais_regista_logs_passos("PAGAMENTO",nrAtendimento,"PAG26","INICIO ACTUALIZAR MARGENS PAGAMENTO")

		&& Actualizar Margens na ST
		Select fi
		Go Top
		Scan For !Empty(Alltrim(fi.ref))
			uf_documentos_updateMargensFoSt(Alltrim(fi.ref), '')
		Endscan


		uf_gerais_regista_logs_passos("PAGAMENTO",nrAtendimento,"PAG26A","FIM ACTUALIZAR MARGENS PAGAMENTO")

		If Upper(Alltrim(ucrse1.pais)) != 'PORTUGAL'
			** integra��o com as encomendas puxadas do cabe�alho das vendas
			If Vartype(stampenc) != 'U' And !Empty(stampenc)
				Select fi
				Go Top
				Scan
					If !Empty(fi.bistamp)
						lcSQL = ''
						TEXT TO lcSQL NOSHOW TEXTMERGE
							UPDATE bi SET qtt = <<fi.qtt>>, ettdeb = <<fi.etiliquido>>, edebito = <<fi.epv>> WHERE bi.bistamp = '<<ALLTRIM(fi.bistamp)>>'
						ENDTEXT

						If !uf_gerais_actGrelha("","uCrsPagCentralAux",lcSQL)
							uf_perguntalt_chama("OCORREU UMA ANOMALIA AO ATUALIZAR A ENCOMENDA DE ORIGEM. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
							Return .F.
						Endif
					Else
						If !Empty(fi.ref)
							lcSQL = ''
							TEXT TO lcSQL NOSHOW TEXTMERGE
								insert into bi (bistamp, nmdos, obrano, ref, design, qtt, qtt2, iva, tabiva, armazem, no, ndos, rdata, fechada
										, datafinal, dataobra, dataopen, lordem, local, morada, codpost, nome, vendedor, vendnm, edebito
										, ettdeb, bostamp, ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora, fno, nmdoc, ndoc, oftstamp, fdata,
										eslvu)
								select left(newid(),21), nmdos, obrano, '<<ALLTRIM(fi.ref)>>', '<<ALLTRIM(fi.design)>>', <<fi.qtt>>, <<fi.qtt>>, <<fi.iva>>, <<fi.tabiva>>, <<fi.armazem>>, no, ndos, dataobra, fechada
										, dataobra, dataobra, dataobra, isnull((select max(lordem) from bi (nolock) where bostamp='<<ALLTRIM(stampenc)>>'),1)+1, local, morada, codpost, nome, vendedor, vendnm, <<fi.epv>>
										, <<fi.etiliquido>>, bostamp, '<<ALLTRIM(m_chinis)>>', getdate(), '<<ALLTRIM(time())>>', '<<ALLTRIM(m_chinis)>>', getdate(), '<<ALLTRIM(time())>>', 0, '', 0, '', getdate(),
										<<fi.epv>> from bo (nolock) where bostamp='<<ALLTRIM(stampenc)>>'
							ENDTEXT

							If !uf_gerais_actGrelha("","uCrsPagCentralAux",lcSQL)
								uf_perguntalt_chama("OCORREU UMA ANOMALIA AO ATUALIZAR (INSERIR) A ENCOMENDA DE ORIGEM. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
								Return .F.
							Endif
						Endif
					Endif
				Endscan
				lcSQL = ''
				TEXT TO lcSQL NOSHOW TEXTMERGE
					update bo
					set etotal=(select sum(ettdeb) from bi (nolock) where bi.bostamp=bo.bostamp), etotaldeb =(select sum(ettdeb) from bi (nolock) where bi.bostamp=bo.bostamp)
					, fechada=1
					where bostamp='<<ALLTRIM(stampenc)>>'

					update bo2 set status='Faturada' where bo2stamp='<<ALLTRIM(stampenc)>>'

					update bi
					set fechada=1
					where bostamp='<<ALLTRIM(stampenc)>>'
				ENDTEXT

				If !uf_gerais_actGrelha("","uCrsPagCentralAux",lcSQL)
					uf_perguntalt_chama("OCORREU UMA ANOMALIA AO ATUALIZAR (INSERIR) A ENCOMENDA DE ORIGEM. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
					Return .F.
				Endif

			Endif
		Endif

		uf_gerais_regista_logs_passos("PAGAMENTO",nrAtendimento,"PAG26B","INICIO DE ACTUALIZAR PONTOS CART�O")

		&& Tratar pontos de Cart�o - funcao de emiss�o vale de desconto passa por aqui
		&& atualizado a 26/10/2016 para voltar a colocar o vale dispon�vel caso seja feita a devolu��o de uma venda em que foi descontado o vale - Lu�s Leal
		If ATENDIMENTO.pgfDados.page1.chkPontos.Tag == "true" And myTipoCartao == 'Pontos'
			uf_gerais_meiaRegua("A calcular pontos/valor acumulados em cart�o")
			uf_CARTAOCLIENTE_actualizaPontosClPag(.F., nrAtendimento)
			regua(2)
		Else
			&& regista informa��o de vendas em que n�o s�o atribuidos pontos ao cliente intencionalmente pelo operador
			Select uCrsatendCl
			If !Empty(uCrsatendCl.nrcartao)
				lcSQL = ''
				TEXT TO lcSQL NOSHOW textmerge
					INSERT INTO B_ocorrencias(
						stamp, linkStamp, tipo, grau, descr,
						oValor, dValor,
						usr, date, site)
					values
						(LEFT(newID(),25),'<<ALLTRIM(nrAtendimento)>>' ,'Cart�o Cliente', 3, 'Venda sem Pontos para cliente com cart�o <<ALLTRIM(uCrsAtendCL.nrcartao)>>',
						'', '',
						<<ch_userno>>, dateadd(HOUR, <<difhoraria>>, getdate()), '<<ALLTRIM(mysite)>>')
				ENDTEXT
				If !uf_gerais_actGrelha("", "", lcSQL)
					uf_perguntalt_chama("OCORREU UM PROBLEMA A REGISTAR OCORR�NCIA. POR FAVOR CONTACTE O SUPORTE.","OK","",48)
				Endif
			Endif
		Endif

		uf_gerais_regista_logs_passos("PAGAMENTO",nrAtendimento,"PAG26C","FIM DE ACTUALIZAR PONTOS CART�O")

		Select uCrsCabVendas
		Go Top
		Scan
			&& se for devolu��o verifica se a venda original tem vale e volta a colocar o vale novamente dispon�vel
			If uCrsCabVendas.partes > 0
				&& query que define se a venda original tem vale baseada no ofistamp do cursor uCrsCabVendas
				lcSQL = ''
				TEXT TO lcSQL NOSHOW TEXTMERGE
					SELECT
						fi2.u_refvale
					FROM fi (nolock)
					INNER JOIN fi fi2 ON fi.ftstamp = fi2. ftstamp
					WHERE fi.fistamp = '<<ALLTRIM(uCrsCabVendas.ofistamp)>>'
					AND fi2.u_refvale != ''
				ENDTEXT

				If !uf_gerais_actGrelha("","uCrsAuxAtualizaVale",lcSQL)
					uf_perguntalt_chama("OCORREU UMA ANOMALIA AO ATUALIZAR OS VALES DISPON�VEIS NA CONTA DO CLIENTE. POR FAVOR CONTACTE O SUPORTE.OBRIGADO.")
				Else
					&& se identificar vales atualiza tabela de vale e coloca o vale novamente dispon�vel
					If Reccount("uCrsAuxAtualizaVale") > 0
						Select uCrsAuxAtualizaVale
						Go Top
						Scan
							lcSQL = ''
							TEXT TO lcSQL NOSHOW textmerge
								UPDATE b_fidelVale SET abatido = 0, anulado = 0, usrdata =  dateadd(HOUR, <<difhoraria>>, getdate()), usr = <<ch_userno>> WHERE ref = '<<ALLTRIM(uCrsAuxAtualizaVale.u_refvale)>>'
							ENDTEXT
							If !uf_gerais_actGrelha("", "", lcSQL)
								uf_perguntalt_chama("OCORREU UMA ANOMALIA AO ATUALIZAR OS VALES DISPON�VEIS NA CONTA DO CLIENTE. POR FAVOR CONTACTE O SUPORTE.OBRIGADO.","OK","",48)
							Endif

							Select uCrsAuxAtualizaVale
						Endscan
					Endif

					If Used("uCrsAuxAtualizaVale")
						fecha("uCrsAuxAtualizaVale")
					Endif

				Endif
			Endif

			Select uCrsCabVendas
		Endscan
		**


		If (Type("atendimento") != "U")
			ATENDIMENTO.campanhas = .F.
		Endif


		uf_gerais_regista_logs_passos("PAGAMENTO",nrAtendimento,"PAG27","ABATER VALES PAGAMENTO")

		&& Abate Vales Descontados
		uf_gerais_meiaRegua("A validar vales descontados")
		uf_PAGAMENTO_AbateValesSelecionados()
		regua(2)

		&& Actualiza ultimo registo alterado
		uf_gerais_gravaUltRegisto(Str(myTermNo), Left(nrAtendimento,8))

		&& Imprimir Declara��o de Autoriza��o do Cliente (A6)
		If uf_gerais_getParameter("ADM0000000134","BOOL")
			uf_utentes_impDeclaracaoAutorizacao()
		Endif

		&& valida lotes das receitas criadas - LL 20161126
		Local lcValSaxenda
		lcValSaxenda = .F.
		Select fi
		Go Top
		Scan
			If Alltrim(fi.ref)='5681903' Or Alltrim(fi.ref)='5771050'
				lcValSaxenda = .T.
			Endif
		Endscan



		uf_gerais_regista_logs_passos("PAGAMENTO",nrAtendimento,"PAG28","VALIDAR RECEITAS LOTE PAGAMENTO")
		uf_Pagamento_validarReceitas()


		If Reccount("uCrsAuxReceitasComErro") > 0 And lcValSaxenda = .F.
			uf_perguntalt_chama("EXISTEM RECEITAS QUE FICARAM COLOCADAS EM LOTES COM ERRO. POR FAVOR VERIFIQUE AS RECEITAS DISPENSADAS.","OK","",48)
		Endif
		fecha("uCrsAuxReceitasComErro")

		&& FASE 5 - Impress�o verso Receita e Tal�o
		Local lcValidaRec, lcNoReceitas
		Public MyValidaEsperaImp, MyImprimeTalao
		Store .F. To lcValidaRec
		Store 0 To lcNoReceitas
		Store .F. To MyValidaEsperaImp, MyImprimeTalao

		Select uCrsCabVendas
		Go Top
		Scan
			If uCrsCabVendas.sel2 == .F.
				lcValidaRec = .T.
				Exit
			Endif
		Endscan

		&&Confirma Pagamento Cashdro
		If(Vartype(myCashDroStamp)!="U" And Val(lcOpID)>0)
			Release lcResponseArray
			Dimension lcResponseArray(6)

			uf_cashDro_operation(@lcResponseArray,"A fechar a opera��o no cashdro...","app.msg.menu.operation_import_request",0,"",lcOpID)
			Release lcResponseArray

		Endif

		Select uCrsCabVendas
		Go Top
		Count For uCrsCabVendas.sel2 == .F. To lcNoReceitas

		Select uCrsDEMndisp
		Go Top
		Count For !Empty(uCrsDEMndisp.receita) To lcNoNDisp

		*!*		PUBLIC myEscolha
		*!*		** IMPRESS�O DE N�O DISPENSADOS
		*!*		IF uf_gerais_getParameter('ADM0000000285','BOOL') AND lcNoNDisp>0
		*!*			IF uf_perguntalt_chama("DESEJA IMPRIMIR/ENVIAR OS MEDICAMENTOS POR DISPENSAR DA(S) GUIA(S) DE TRATAMENTO?","Sim","N�o")
		*!*				uf_valorescombo_chama("myEscolha", 0, "select sel = convert(bit,0),'IMPRIMIR' as tipo union all select sel = convert(bit,0),'EMAIL' as tipo union all select sel = convert(bit,0),'SMS' as tipo", 1, "tipo", "tipo")
		*!*				IF !EMPTY(myEscolha)
		*!*						DO CASE
		*!*							CASE ALLTRIM(myEscolha) = "IMPRIMIR"
		*!*								uf_imprimir_nao_dispensados()
		*!*							CASE ALLTRIM(myEscolha) = "EMAIL"
		*!*								IF ucrse1.usacoms=.t.
		*!*									uf_EMAIL_nao_dispensados()
		*!*								ELSE
		*!*									uf_perguntalt_chama("PARA UTILIZAR ESTA FUNCIONALIDADE DEVER� ADQUIRIR O M�DULO DE COMUNICA��ES." + CHR(13) + "PARA MAIS INFORMA��ES POR FAVOR CONTACTE A LOGITOOLS (OP��O - COMERCIAL).", "OK","",16)
		*!*								ENDIF
		*!*							CASE ALLTRIM(myEscolha) = "SMS"
		*!*								IF ucrse1.usacoms=.t.
		*!*									uf_SMS_nao_dispensados()
		*!*								ELSE
		*!*									uf_perguntalt_chama("PARA UTILIZAR ESTA FUNCIONALIDADE DEVER� ADQUIRIR O M�DULO DE COMUNICA��ES." + CHR(13) + "PARA MAIS INFORMA��ES POR FAVOR CONTACTE A LOGITOOLS (OP��O - COMERCIAL).", "OK","",16)
		*!*								ENDIF
		*!*							OTHERWISE
		*!*						ENDCASE
		*!*		*!*			IF uf_perguntalt_chama("DESEJA IMPRIMIR OS MEDICAMENTOS POR DISPENSAR DA(S) GUIA(S) DE TRATAMENTO?","Sim","N�o")
		*!*		*!*				uf_imprimir_nao_dispensados()
		*!*		*!*			ENDIF
		*!*				ENDIF
		*!*			ENDIF
		*!*		ENDIF

		** impress�o de tal�o de controle Nova Nordisk
		If NNordisk = .T.
			uf_talao_novonordisk()
		Endif
		NNordisk = .F.

		Select uCrsDEMndisp
		Delete All

		** impress�o de tal�o para parque
		If uf_gerais_getParameter_site('ADM0000000154', 'BOOL', mySite)

			uv_tot = Val(Transform(PAGAMENTO.txtValAtend.Value,'99999999.99'))

			If Used("fimancompart")
				Select fimancompart
				Go Top

				Scan For !Empty(fimancompart.perc) Or !Empty(fimancompart.valor)

					uv_tot = uv_tot + fimancompart.valorTot

				Endscan

			Endif

			If uv_tot >= uf_gerais_getParameter_site('ADM0000000154', 'NUM', mySite)
				uf_talao_parque()
			Endif
		Endif

		uv_geraVale = .F.

		If Round(PAGAMENTO.txtValAtend.Value,2) < 0
			If !Empty(Val(PAGAMENTO.txtModPag3.Value)) And Upper(Alltrim(PAGAMENTO.btnModPag3.Label7.Caption)) == "VALES"
				uv_geraVale = .T.
			Endif
			If !Empty(Val(PAGAMENTO.txtModPag4.Value)) And Upper(Alltrim(PAGAMENTO.btnModPag4.Label7.Caption)) == "VALES"
				uv_geraVale = .T.
			Endif
			If !Empty(Val(PAGAMENTO.txtModPag5.Value)) And Upper(Alltrim(PAGAMENTO.btnModPag5.Label7.Caption)) == "VALES"
				uv_geraVale = .T.
			Endif
			If !Empty(Val(PAGAMENTO.txtModPag6.Value)) And Upper(Alltrim(PAGAMENTO.btnModPag6.Label7.Caption)) == "VALES"
				uv_geraVale = .T.
			Endif
		Endif

		If uv_geraVale
			uv_valor = Abs(Round(PAGAMENTO.txtValAtend.Value,2))
			**uf_pagamento_imprimeValeDescGen(ft.no, ft.estab, uv_valor, uf_gerais_getUmValor("b_utentes", "utstamp", "no = " + ASTR(ft.no) + " and estab = " + ASTR(ft.estab)),ft.nome, .T.)
			uf_CARTAOCLIENTE_emitirValesDirectos(uCrsCLEF.no, uCrsCLEF.estab, '', uv_valor, .T., '')
		Endif

		&& set focus para permitir a leitura do n� de receita com o scanner
		**pagamento.pgfrVDP.page1.grdVendas.venda.text1.setfocus
		&& Definir se Imprime primeiro Tal�es ou Receita 2015-05-01 - LL

		uf_gerais_regista_logs_passos("PAGAMENTO",nrAtendimento,"PAG29","IMPRESS�ES PAGAMENTO")

		If uf_gerais_getParameter("ADM0000000254", "BOOL") && Vai imprimir primeiro a Receita

			If Used("uCrsImpTalaoPos")

				Select uCrsImpTalaoPos
				Go Top
				Scan
					TEXT TO lcSQL NOSHOW TEXTMERGE
						select u_tlote, u_tlote2 from ft (nolock) where ftstamp='<<ALLTRIM(uCrsImpTalaoPos.stamp)>>'
					ENDTEXT
					uf_gerais_actGrelha("", "ucrsCompartLote", lcSQL)
					If Alltrim(ucrsCompartLote.u_tlote)=='98' Or Alltrim(ucrsCompartLote.u_tlote)=='99'
						Select uCrsImpTalaoPos
						Replace uCrsImpTalaoPos.lote With Alltrim(ucrsCompartLote.u_tlote)
					Endif
					If Alltrim(ucrsCompartLote.u_tlote2)=='98' Or Alltrim(ucrsCompartLote.u_tlote2)=='99'
						Select uCrsImpTalaoPos
						Replace uCrsImpTalaoPos.lote With Alltrim(ucrsCompartLote.u_tlote2)
						Replace uCrsImpTalaoPos.planocomp With .T.
					Endif
					fecha("ucrsCompartLote")
				Endscan

				Select * From uCrsImpTalaoPos Where !Empty(uCrsImpTalaoPos.nrreceita) And impresso=.F. Into Cursor uCrsImpTalaoPosTMP Readwrite
			Else
				Create Cursor uCrsImpTalaoPos (stamp c(26), tipo i(4), Lordem i(8), Susp l, AdiReserva l, nrreceita c(26) ,rm l, planocomp l, impcomp1 l, impcomp2 l, lote c(5), impresso l , plano c(5))
				Select uCrsCabVendas
				Go Top
				Scan
					Select uCrsImpTalaoPos
					Append Blank
					Replace uCrsImpTalaoPos.stamp			With Alltrim(uCrsCabVendas.stamp)
					If Right(Alltrim(uCrsCabVendas.Design),10) == "*SUSPENSA*" Or Right(Alltrim(uCrsCabVendas.Design),18)=="*SUSPENSA RESERVA*"
						Replace uCrsImpTalaoPos.Susp		With .T.
					Else
						Replace uCrsImpTalaoPos.Susp		With .F.
					Endif
					If !Empty(uCrsCabVendas.TemAdiantamento)
						Replace uCrsImpTalaoPos.AdiReserva	With .T.
					Else
						Replace uCrsImpTalaoPos.AdiReserva	With .F.
					Endif
					Replace uCrsImpTalaoPos.planocomp 		With uCrsCabVendas.planocomplem
					If Alltrim(uCrsCabVendas.receita_tipo)=='RM'
						Replace uCrsImpTalaoPos.rm With .T.
					Else
						Replace uCrsImpTalaoPos.rm With .F.
					Endif
					Replace uCrsImpTalaoPos.impcomp1 With .F.
					Replace uCrsImpTalaoPos.impcomp2 With .F.
					Replace uCrsImpTalaoPos.nrreceita With Substr(uCrsCabVendas.Design, At('Receita',uCrsCabVendas.Design )+8,At(' - ',uCrsCabVendas.Design )- (At('Receita',uCrsCabVendas.Design )+8))
					Replace uCrsImpTalaoPos.plano With Substr(uCrsCabVendas.Design, At('.[',uCrsCabVendas.Design )+2,At('][',uCrsCabVendas.Design )- (At('.[',uCrsCabVendas.Design )+2))
					Select uCrsCabVendas
				Endscan
			Endif

			Select uCrsImpTalaoPos
			Go Top
			Scan
				If uCrsImpTalaoPos.tipo=75
					Local nrreceitadel
					nrreceitadel = Alltrim(uCrsImpTalaoPos.nrreceita)
					If Used("uCrsImpTalaoPosTMP")
						Select uCrsImpTalaoPosTMP
						Delete From uCrsImpTalaoPosTMP Where Alltrim(uCrsImpTalaoPosTMP.nrreceita) == Alltrim(nrreceitadel) And uCrsImpTalaoPosTMP.tipo<>75
					Endif
				Endif
				If Used("uCrsImpTalaoPosTMP ")
					Select uCrsImpTalaoPosTMP
					Go Top
					Scan
						TEXT TO lcSQL NOSHOW TEXTMERGE
							select top 1 token from Dispensa_Eletronica (nolock) where receita_nr='<<ALLTRIM(uCrsImpTalaoPosTMP.nrreceita)>>' and resultado_efetivacao_descr='Pedido processado com sucesso.'
						ENDTEXT
						uf_gerais_actGrelha("", "ucrsCompartVal", lcSQL)
						If Reccount("ucrsCompartVal")>0
							Select uCrsImpTalaoPosTMP
							Replace uCrsImpTalaoPosTMP.rm With .F.
							Replace uCrsImpTalaoPosTMP.Lordem With 99999
						Endif
						fecha("ucrsCompartVal")
						Select uCrsImpTalaoPosTMP
					Endscan
					Select uCrsImpTalaoPos
				Endif
			Endscan

			Select uCrsImpTalaoPos
			Go Top
			Scan
				TEXT TO lcSQL NOSHOW TEXTMERGE
					select u_tlote, u_tlote2 from ft (nolock) where ftstamp='<<ALLTRIM(uCrsImpTalaoPos.stamp)>>'
				ENDTEXT
				uf_gerais_actGrelha("", "ucrsCompartLote", lcSQL)
				If Alltrim(ucrsCompartLote.u_tlote)=='98' Or Alltrim(ucrsCompartLote.u_tlote)=='99' Or Alltrim(ucrsCompartLote.u_tlote)=='96' Or Alltrim(ucrsCompartLote.u_tlote)=='97'
					Select uCrsImpTalaoPos
					Replace uCrsImpTalaoPos.lote With Alltrim(ucrsCompartLote.u_tlote)
				Endif
				If Alltrim(ucrsCompartLote.u_tlote2)=='98' Or Alltrim(ucrsCompartLote.u_tlote2)=='99' Or Alltrim(ucrsCompartLote.u_tlote2)=='96' Or Alltrim(ucrsCompartLote.u_tlote2)=='97'
					Select uCrsImpTalaoPos
					Replace uCrsImpTalaoPos.lote With Alltrim(ucrsCompartLote.u_tlote2)
					Replace uCrsImpTalaoPos.planocomp With .T.
				Endif
				fecha("ucrsCompartLote")
			Endscan



			Select uCrsImpTalaoPos
			Go Top
			Scan
				TEXT TO lcSQL NOSHOW TEXTMERGE
					select u_ltstamp2,u_ltstamp, u_lote, u_lote2
					,(select COUNT(bistamp) from bi (nolock) where bistamp in (select bistamp from fi where fi.ftstamp=ft.ftstamp) and bi.nmdos='Reserva de Cliente') as nrres
					from ft (nolock) where ftstamp='<<ALLTRIM(uCrsImpTalaoPos.stamp)>>'
				ENDTEXT
				uf_gerais_actGrelha("", "Ucrsltstamp",lcSQL)
				Select Ucrsltstamp
				If (Empty(Alltrim(Ucrsltstamp.u_ltstamp)) And Empty(Alltrim(Ucrsltstamp.u_ltstamp2)) And nrres = 0) Or (Ucrsltstamp.u_lote=0 And Ucrsltstamp.u_lote2=0)
					**IF EMPTY(ALLTRIM(Ucrsltstamp.u_ltstamp)) and EMPTY(ALLTRIM(Ucrsltstamp.u_ltstamp2))
					Select uCrsImpTalaoPos
					Replace uCrsImpTalaoPos.impresso With .T.
				Endif
				fecha("Ucrsltstamp")
				Select uCrsImpTalaoPos
			Endscan

			TEXT TO lcSQL NOSHOW TEXTMERGE
				select '' as codigo, 0 as Ucrscptpla
				union all
				select codigo, obriga_nrReceita as Ucrscptpla from cptpla (nolock) --where obriga_nrReceita=0
			ENDTEXT
			uf_gerais_actGrelha("", "Ucrscptpla",lcSQL)

			Select * From uCrsImpTalaoPos INNER Join Ucrscptpla On Ucrscptpla.codigo=uCrsImpTalaoPos.plano;
				WHERE (((!Empty(uCrsImpTalaoPos.nrreceita) Or (Empty(uCrsImpTalaoPos.nrreceita) And Ucrscptpla.Ucrscptpla=0)) And (uCrsImpTalaoPos.rm=.T. Or (uCrsImpTalaoPos.rm=.F. And uCrsImpTalaoPos.planocomp=.T.))) Or (Alltrim(uCrsImpTalaoPos.lote)=='98' Or Alltrim(uCrsImpTalaoPos.lote)=='99')) And uCrsImpTalaoPos.impresso=.F.  Into Cursor uCrsImpTalaoPosTMP Readwrite

			Select * From uCrsImpTalaoPosTMP Order By Lordem Into Cursor uCrsImpTalaoPosTMP Readwrite

			fecha("Ucrscptpla")
			**MESSAGEBOX('antes1')
			**uf_perguntalt_chama("1 verifica curtor uCrsImpTalaoPos ?","Sim","N�o")
			If Used("uCrsImpTalaoPosTMP")
				Local lcpedeconf
				Store .F. To lcpedeaviso
				Select uCrsImpTalaoPosTMP
				If Reccount("uCrsImpTalaoPosTMP")>1
					lcpedeaviso = .T.
				Endif

				Select uCrsImpTalaoPosTMP
				Go Top
				Scan
					If ucrse1.OrdemImpressaoReceita == .F.
						If uCrsImpTalaoPosTMP.Lordem <> 99999 Or (Alltrim(uCrsImpTalaoPosTMP.lote)=='98' Or Alltrim(uCrsImpTalaoPosTMP.lote)=='99')
							**IF uf_gerais_getParameter_site('ADM0000000136', 'BOOL', mySite)
							Messagebox(10)
							If uf_gerais_getUmValor("B_terminal", "usaDriverWin", "terminal = '" + Alltrim(myTerm) + "'")
								Select uCrsImpTalaoPosTMP
								uf_imprimirpos_impRecTalaoComum_talaoVerso(uCrsImpTalaoPosTMP.stamp, 1, .F., .T.)
							Else
								Select uCrsImpTalaoPosTMP
								uf_imprimirpos_impRec_V2(uCrsImpTalaoPosTMP.stamp, 1, "ATENDIMENTO", lcpedeaviso)
							Endif

						Endif
						Select uCrsImpTalaoPosTMP
						If uCrsImpTalaoPosTMP.rm=.T. Or (uCrsImpTalaoPosTMP.planocomp = .T. And (Alltrim(uCrsImpTalaoPosTMP.lote)=='98' Or Alltrim(uCrsImpTalaoPosTMP.lote)=='99'))
							**IF uf_gerais_getParameter_site('ADM0000000136', 'BOOL', mySite)
							Messagebox(11)
							If uf_gerais_getUmValor("B_terminal", "usaDriverWin", "terminal = '" + Alltrim(myTerm) + "'")
								uf_imprimirpos_impRecTalaoComum_talaoVerso(uCrsImpTalaoPosTMP.stamp, 2, .F., .T.)
							Else
								uf_imprimirpos_impRec_V2(uCrsImpTalaoPosTMP.stamp, 2, "ATENDIMENTO", lcpedeaviso)
							Endif
						Endif
						Select uCrsImpTalaoPosTMP
						**IF uCrsImpTalaoPosTMP.rm=.f. AND (uCrsImpTalaoPosTMP.planocomp = .t. AND (ALLTRIM(uCrsImpTalaoPosTMP.lote)=='96' OR ALLTRIM(uCrsImpTalaoPosTMP.lote)=='97'))
						**	uf_imprimirpos_impRec_V2(uCrsImpTalaoPosTMP.stamp, 2, "ATENDIMENTO", lcpedeaviso)
						**ENDIF
					Else
						**uf_perguntalt_chama("ver cursores aqui","Sim","N�o")
						Select uCrsImpTalaoPosTMP
						If uCrsImpTalaoPosTMP.rm=.T. Or (uCrsImpTalaoPosTMP.planocomp = .T. And (Alltrim(uCrsImpTalaoPosTMP.lote)=='98' Or Alltrim(uCrsImpTalaoPosTMP.lote)=='99'))
							**IF uf_gerais_getParameter_site('ADM0000000136', 'BOOL', mySite)
							Messagebox(12)
							If uf_gerais_getUmValor("B_terminal", "usaDriverWin", "terminal = '" + Alltrim(myTerm) + "'")
								uf_imprimirpos_impRecTalaoComum_talaoVerso(uCrsImpTalaoPosTMP.stamp, 2, .F., .T.)
							Else
								uf_imprimirpos_impRec_V2(uCrsImpTalaoPosTMP.stamp, 2, "ATENDIMENTO", lcpedeaviso)
							Endif
						Endif
						Select uCrsImpTalaoPosTMP
						**IF uCrsImpTalaoPosTMP.rm=.f. AND (uCrsImpTalaoPosTMP.planocomp = .t. AND (ALLTRIM(uCrsImpTalaoPosTMP.lote)=='96' OR ALLTRIM(uCrsImpTalaoPosTMP.lote)=='97'))
						**	uf_imprimirpos_impRec_V2(uCrsImpTalaoPosTMP.stamp, 2, "ATENDIMENTO", lcpedeaviso)
						**ENDIF
						If uCrsImpTalaoPosTMP.Lordem <> 99999 Or (Alltrim(uCrsImpTalaoPosTMP.lote)=='98' Or Alltrim(uCrsImpTalaoPosTMP.lote)=='99')
							**IF uf_gerais_getParameter_site('ADM0000000136', 'BOOL', mySite)
							Messagebox(13)
							If uf_gerais_getUmValor("B_terminal", "usaDriverWin", "terminal = '" + Alltrim(myTerm) + "'")
								Select uCrsImpTalaoPosTMP
								uf_imprimirpos_impRecTalaoComum_talaoVerso(uCrsImpTalaoPosTMP.stamp, 1, .F., .T.)
							Else
								Select uCrsImpTalaoPosTMP
								uf_imprimirpos_impRec_V2(uCrsImpTalaoPosTMP.stamp, 1, "ATENDIMENTO", lcpedeaviso)
							Endif

						Endif
					Endif
					Select uCrsImpTalaoPosTMP
					Replace uCrsImpTalaoPosTMP.impresso With .T.
					Update uCrsImpTalaoPos Set impresso=.T. Where Alltrim(uCrsImpTalaoPos.stamp)==Alltrim(uCrsImpTalaoPosTMP.stamp)
				Endscan
			Endif
			fecha("uCrsImpTalaoPosTMP")



			**uf_perguntalt_chama("1 verifica uCrsImpTalaoPos?","Sim","N�o")
			&& Permite impress�o de receitas no atendimento
			If !uf_gerais_getParameter("ADM0000000119","BOOL")
				&& Imprime receita e atendimento tem mais de uma receita
				If lcValidaRec And lcNoReceitas > 1
					&& Imprime receita por ordem sequencial
					**IF 	atendimento.imprecordem == .t.
					Select uCrsCabVendas
					Go Top
					&& adicionada valida��o do plano complmentar pq tem de imprimir verso neste cen�rio
					Scan For Upper(Alltrim(uCrsCabVendas.receita_tipo)) != "RSP" Or uCrsCabVendas.planocomplem == .T.
						If uCrsCabVendas.sel == .F.
							&& Funcionalidade que altera ordem de impress�o da complement. - Lu�s Leal 2015/06/23
							&& ENTIDADE - SNS
							If ucrse1.OrdemImpressaoReceita == .F.
								Select uCrsCabVendas

								**									uf_imprimirpos_impRec(uCrsCabVendas.stamp, 1, "ATENDIMENTO")
								Select uCrsCabVendas
								If Upper(Alltrim(uCrsCabVendas.receita_tipo)) != "RSP"
									Select uCrsCabVendas

									**										uf_imprimirpos_impRec(uCrsCabVendas.stamp, 2, "ATENDIMENTO")
								Endif
							Else
								&& SNS - ENTIDADE
								Select uCrsCabVendas
								If Upper(Alltrim(uCrsCabVendas.receita_tipo)) != "RSP"

									Select uCrsCabVendas
									**										uf_imprimirpos_impRec(uCrsCabVendas.stamp, 2, "ATENDIMENTO")
								Endif

								Select uCrsCabVendas
								**									uf_imprimirpos_impRec(uCrsCabVendas.stamp, 1, "ATENDIMENTO")
							Endif

							Select uCrsCabVendas
							Replace uCrsCabVendas.sel With .T.
						Endif
						Select uCrsCabVendas
					Endscan
					*!*						ELSE
					*!*							&& Painel de pagamento fica em modo de espera para utente seleccionar receita para impress�o
					*!*							MyValidaEsperaImp = .t.
					*!*							MyImprimeTalao = .t.
					*!*						ENDIF
				Else
					&& Imprime uma unica receita
					If lcValidaRec
						Select uCrsCabVendas
						Go Top
						&& adicionada valida��o do plano complmentar pq tem de imprimir verso neste cen�rio
						Scan For Upper(Alltrim(uCrsCabVendas.receita_tipo)) != "RSP" Or uCrsCabVendas.planocomplem == .T.
							If uCrsCabVendas.sel == .F.
								&& Funcionalidade que altera ordem de impress�o da complement. - LL 20150623
								&& ENTIDADE - SNS
								If ucrse1.OrdemImpressaoReceita == .F.

									Select uCrsCabVendas
									**	uf_imprimirpos_impRec(uCrsCabVendas.stamp, 1, "ATENDIMENTO")
									**									SELECT uCrsCabVendas
									If Upper(Alltrim(uCrsCabVendas.receita_tipo)) != "RSP"

										Select uCrsCabVendas
										**										uf_imprimirpos_impRec(uCrsCabVendas.stamp, 2, "ATENDIMENTO")
									Endif
								Else
									&& SNS - ENTIDADE
									If Upper(Alltrim(uCrsCabVendas.receita_tipo)) != "RSP"

										Select uCrsCabVendas
										**										uf_imprimirpos_impRec(uCrsCabVendas.stamp, 2, "ATENDIMENTO")
									Endif

									Select uCrsCabVendas
									**									uf_imprimirpos_impRec(uCrsCabVendas.stamp, 1, "ATENDIMENTO")
								Endif

								Select uCrsCabVendas
								Replace uCrsCabVendas.sel With .T.
							Endif
							Select uCrsCabVendas
						Endscan
					Endif
				Endif

				&& variavel controlo espera impress�o
				If MyValidaEsperaImp == .T.
					**	RETURN .f.
				Endif

			Endif

			uf_gerais_regista_logs_passos("PAGAMENTO",nrAtendimento,"PAG30","EVENTO IMPRIMIR TALAO PAGAMENTO")

			&& imprimir os tal�es
			uf_PAGAMENTO_eventoImpressaoPos(.F.)

			**guarda registo de atendimento no sistema de senhas
			Select ucrse1
			If (ucrse1.has_ticket_integration)
				uf_ticket_update_nrAtend(Alltrim(nrAtendimento))
			Endif

			** Tratar da fatura��o das comparticipa��es �s seguradoras
			Select uCrsCabFactSeg
			If FactCompartSeg And Reccount("uCrsCabFactSeg") > 0 And Len(Alltrim(uCrsCabFactSeg.ftstamp))>0
				uf_pagamento_fact_seguradora()
			Endif


			&& fecha pagamento e prepara novo atendimento
			uf_PAGAMENTO_limparVendasFimVd()

			uf_PAGAMENTO_sair( .T. )

			** Pergunta se quer fazer o pagamento ou novo atendimento no caso de ter escolhido pagamento posterior
			If pPagPost = .T.
				If uf_gerais_getParameter('ADM0000000294', 'BOOL') = .T.
					If uf_perguntalt_chama("Deseja efetuar o recebimento dos documentos ou fazer novo atendimento?","Recebimento","Atendimento")
						pPagPost = .F.
						uf_atendimento_Pagar_Varios(lcOldClNo, lcOldClEstab, .T.)
					Endif
				Endif
			Endif



		Else && Vai imprimir primeiro o Tal�o
			&& imprimir os tal�es
			uf_gerais_regista_logs_passos("PAGAMENTO",nrAtendimento,"PAG31","EVENTO IMPRIMIR TALAO PAGAMENTO")

			uf_PAGAMENTO_eventoImpressaoPos(.F.)

			&& permite impress�o de receitas no atendimento
			If !uf_gerais_getParameter("ADM0000000119","BOOL")
				&& atendimento tem receitas e mais do que 1 receita
				If lcValidaRec And lcNoReceitas > 1
					&& Impress�o por ordem das receitas
					*IF 	atendimento.imprecordem == .t.
					Select uCrsCabVendas
					Go Top
					Scan For Upper(Alltrim(uCrsCabVendas.receita_tipo)) != "RSP" Or uCrsCabVendas.planocomplem == .T.
						If uCrsCabVendas.sel == .F.
							&& Funcionalidade que altera ordem de impress�o da complement. - Lu�s Leal 2015/06/23
							&& ENTIDADE - SNS
							If ucrse1.OrdemImpressaoReceita == .F.

								Select uCrsCabVendas
								**									uf_imprimirpos_impRec(uCrsCabVendas.stamp, 1, "ATENDIMENTO")
								Select uCrsCabVendas
								If Upper(Alltrim(uCrsCabVendas.receita_tipo)) != "RSP"

									Select uCrsCabVendas
									**										uf_imprimirpos_impRec(uCrsCabVendas.stamp, 2, "ATENDIMENTO")
								Endif
							Else
								&& SNS - ENTIDADE
								If Upper(Alltrim(uCrsCabVendas.receita_tipo)) != "RSP"

									Select uCrsCabVendas
									**										uf_imprimirpos_impRec(uCrsCabVendas.stamp, 2, "ATENDIMENTO")
								Endif

								Select uCrsCabVendas
								**									uf_imprimirpos_impRec(uCrsCabVendas.stamp, 1, "ATENDIMENTO")

							Endif

							Select uCrsCabVendas
							Replace uCrsCabVendas.sel With .T.
						Endif
						Select uCrsCabVendas
					Endscan
					*!*						ELSE
					*!*							&& Faz com o painel de pagamento n�ao feche para o utilizador porder seleccionar as receitas a imprimir
					*!*							MyValidaEsperaImp = .t.
					*!*						ENDIF
				Else
					&& Valida de atendimento tem receita
					If lcValidaRec
						Select uCrsCabVendas
						Go Top
						Scan For Upper(Alltrim(uCrsCabVendas.receita_tipo)) != "RSP" Or uCrsCabVendas.planocomplem == .T.
							If uCrsCabVendas.sel == .F.
								&& Funcionalidade que altera ordem de impress�o da complement. - Lu�s Leal 2015/06/23
								&& ENTIDADE - SNS
								If ucrse1.OrdemImpressaoReceita == .F.

									Select uCrsCabVendas
									**									uf_imprimirpos_impRec(uCrsCabVendas.stamp, 1, "ATENDIMENTO")
									Select uCrsCabVendas
									If Upper(Alltrim(uCrsCabVendas.receita_tipo)) != "RSP"

										Select uCrsCabVendas
										**										uf_imprimirpos_impRec(uCrsCabVendas.stamp, 2, "ATENDIMENTO")
									Endif
								Else
									&& SNS - ENTIDADE
									If Upper(Alltrim(uCrsCabVendas.receita_tipo)) != "RSP"

										Select uCrsCabVendas
										**										uf_imprimirpos_impRec(uCrsCabVendas.stamp, 2, "ATENDIMENTO")
									Endif

									Select uCrsCabVendas
									**									uf_imprimirpos_impRec(uCrsCabVendas.stamp, 1, "ATENDIMENTO")
								Endif

								Select uCrsCabVendas
								Replace uCrsCabVendas.sel With .T.
							Endif
							Select uCrsCabVendas
						Endscan
					Else
						** Altera��o em 25-01-2018 para imprimir verso quando � RSP e a �nica entidade n�o � o SNS
						Select uCrsCabVendas
						Go Top
						If Upper(Alltrim(uCrsCabVendas.receita_tipo)) == 'RSP' And (Substr(Alltrim(uCrsCabVendas.Design),7,3)!='SNS' And Substr(Alltrim(uCrsCabVendas.Design),8,3)!='SNS') And Occurs('[', uCrsCabVendas.Design)<=2
							Select uCrsCabVendas

							**							uf_imprimirpos_impRec(uCrsCabVendas.stamp, 1, "ATENDIMENTO")
						Endif
					Endif
				Endif

				&& variavel controlo espera impress�o
				**IF MyValidaEsperaImp == .t.
				**	RETURN .f.
				**ENDIF

				&& Verifica se faltam imprimir receitas
				*uf_perguntalt_chama("espera aqui sff2.","OK","",16)

				If Used("uCrsCabVendasprint")
					If Reccount("uCrsCabVendasprint")>0
						Select uCrsCabVendasprint
						Go Top
						Scan
							Local lcnrrecimp
							Store '' To lcnrrecimp
							lcnrrecimp = Substr(uCrsCabVendasprint.Design,At('Receita:',uCrsCabVendasprint.Design )+8,(At(' -',uCrsCabVendasprint.Design))-(At('Receita:',uCrsCabVendasprint.Design )+8))
							TEXT TO lcSQL NOSHOW TEXTMERGE
								select ft2stamp from ft2 (nolock) where u_receita='<<ALLTRIM(lcnrrecimp)>>' order BY ft2.ousrdata desc
							ENDTEXT
							If !uf_gerais_actGrelha("", "UcrsFt2StampRI",lcSQL)
								Messagebox("DESCULPE, MAS OCORREU UM ERRO O DOCUMENTO DE FATURA��O DA RECEITA. POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
								Return .F.
							Endif
							If !Empty(lcnrrecimp )

								**uf_perguntalt_chama(" 333 - VAI SER IMPRESSA A RECEITA DA ENTIDADE:  N� " + ALLTRIM(lcnrrecimp),"OK","",64)
								**								uf_imprimirpos_impRec(UcrsFt2StampRI.ft2stamp, 1, "ATENDIMENTO")
							Endif

							fecha("UcrsFt2StampRI")
							Select uCrsCabVendasprint

						Endscan
					Endif
				Endif


				If Used("uCrsImpTalaoPos")
					Select * From uCrsImpTalaoPos Where !Empty(uCrsImpTalaoPos.nrreceita) And impresso=.F. Into Cursor uCrsImpTalaoPosTMP Readwrite
				Else
					Create Cursor uCrsImpTalaoPos (stamp c(26), tipo i(4), Lordem i(8), Susp l, AdiReserva l, nrreceita c(26) ,rm l, planocomp l, impcomp1 l, impcomp2 l, lote c(5), impresso l , plano c(5))
					Select uCrsCabVendas
					Go Top
					Scan
						Local lcvalidains
						Store .T. To lcvalidains

						*!*							IF !EMPTY(uCrsCabVendas.ofistamp)
						*!*								TEXT TO lcSQL NOSHOW TEXTMERGE
						*!*									select design from fi (nolock) where fistamp='<<ALLTRIM(uCrsCabVendas.ofistamp)>>'
						*!*								ENDTEXT
						*!*								IF !uf_gerais_actGrelha("", "Ucrsdesignfiorig",lcSQL)
						*!*									MESSAGEBOX("DESCULPE, MAS OCORREU UM ERRO O DOCUMENTO DE FATURA��O DA RECEITA. POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
						*!*									RETURN .f.
						*!*								ENDIF
						*!*								SELECT Ucrsdesignfiorig
						*!*								IF AT('Ad. Reserva', Ucrsdesignfiorig.design)>0
						*!*									lcvalidains = .f.
						*!*								ENDIF
						*!*								fecha("Ucrsdesignfiorig")
						*!*							ENDIF
						If lcvalidains = .T.
							Select uCrsImpTalaoPos

							Append Blank
							Replace uCrsImpTalaoPos.stamp			With Alltrim(uCrsCabVendas.stamp)
							If Right(Alltrim(uCrsCabVendas.Design),10) == "*SUSPENSA*" Or Right(Alltrim(uCrsCabVendas.Design),18)=="*SUSPENSA RESERVA*"
								Replace uCrsImpTalaoPos.Susp		With .T.
							Else
								Replace uCrsImpTalaoPos.Susp		With .F.
							Endif
							If !Empty(uCrsCabVendas.TemAdiantamento)
								Replace uCrsImpTalaoPos.AdiReserva	With .T.
							Else
								Replace uCrsImpTalaoPos.AdiReserva	With .F.
							Endif
							Replace uCrsImpTalaoPos.planocomp 		With uCrsCabVendas.planocomplem
							If Alltrim(uCrsCabVendas.receita_tipo)=='RM'
								Replace uCrsImpTalaoPos.rm With .T.
							Else
								Replace uCrsImpTalaoPos.rm With .F.
							Endif
							Replace uCrsImpTalaoPos.impcomp1 With .F.
							Replace uCrsImpTalaoPos.impcomp2 With .F.
							Replace uCrsImpTalaoPos.nrreceita With Substr(uCrsCabVendas.Design, At('Receita',uCrsCabVendas.Design )+8,At(' - ',uCrsCabVendas.Design )- (At('Receita',uCrsCabVendas.Design )+8))
							Replace uCrsImpTalaoPos.plano With Substr(uCrsCabVendas.Design, At('.[',uCrsCabVendas.Design )+2,At('][',uCrsCabVendas.Design )- (At('.[',uCrsCabVendas.Design )+2))
							Select uCrsCabVendas
						Endif
					Endscan
				Endif

				Select uCrsImpTalaoPos
				Go Top
				Scan
					TEXT TO lcSQL NOSHOW TEXTMERGE
						select u_tlote, u_tlote2 from ft (nolock) where ftstamp='<<ALLTRIM(uCrsImpTalaoPos.stamp)>>'
					ENDTEXT
					uf_gerais_actGrelha("", "ucrsCompartLote", lcSQL)
					If Alltrim(ucrsCompartLote.u_tlote)=='98' Or Alltrim(ucrsCompartLote.u_tlote)=='99' Or Alltrim(ucrsCompartLote.u_tlote)=='96' Or Alltrim(ucrsCompartLote.u_tlote)=='97'
						Select uCrsImpTalaoPos
						Replace uCrsImpTalaoPos.lote With Alltrim(ucrsCompartLote.u_tlote)
					Endif
					If Alltrim(ucrsCompartLote.u_tlote2)=='98' Or Alltrim(ucrsCompartLote.u_tlote2)=='99' Or Alltrim(ucrsCompartLote.u_tlote2)=='96' Or Alltrim(ucrsCompartLote.u_tlote2)=='97'
						Select uCrsImpTalaoPos
						Replace uCrsImpTalaoPos.lote With Alltrim(ucrsCompartLote.u_tlote2)
						Replace uCrsImpTalaoPos.planocomp With .T.
					Endif
					fecha("ucrsCompartLote")
				Endscan

				Select uCrsImpTalaoPos
				Go Top
				Scan
					If uCrsImpTalaoPos.tipo=75
						Local nrreceitadel
						nrreceitadel = Alltrim(uCrsImpTalaoPos.nrreceita)
						If Used("uCrsImpTalaoPosTMP ")
							Select uCrsImpTalaoPosTMP
							Delete From uCrsImpTalaoPosTMP Where Alltrim(uCrsImpTalaoPosTMP.nrreceita) == Alltrim(nrreceitadel) And uCrsImpTalaoPosTMP.tipo<>75
						Endif
					Endif
					If Used("uCrsImpTalaoPosTMP ")
						Select uCrsImpTalaoPosTMP
						Go Top
						Scan
							TEXT TO lcSQL NOSHOW TEXTMERGE
								select top 1 token from Dispensa_Eletronica (nolock) where receita_nr='<<ALLTRIM(uCrsImpTalaoPosTMP.nrreceita)>>' and resultado_efetivacao_descr='Pedido processado com sucesso.'
							ENDTEXT
							uf_gerais_actGrelha("", "ucrsCompartVal", lcSQL)
							If Reccount("ucrsCompartVal")>0
								Select uCrsImpTalaoPosTMP
								Replace uCrsImpTalaoPosTMP.rm With .F.
								Replace uCrsImpTalaoPosTMP.Lordem With 99999
							Endif
							fecha("ucrsCompartVal")
							Select uCrsImpTalaoPosTMP
						Endscan
					Endif
					Select uCrsImpTalaoPos
				Endscan

				Select uCrsImpTalaoPos
				Go Top
				Scan
					If uCrsImpTalaoPos.tipo=75 And !Empty(uCrsImpTalaoPos.nrreceita)
						Local nrreceitadel
						nrreceitadel = Alltrim(uCrsImpTalaoPos.nrreceita)
						Delete From uCrsImpTalaoPos Where nrreceita = Alltrim(nrreceitadel) And tipo = 3
					Endif
				Endscan

				Select uCrsImpTalaoPos
				Go Top
				Scan
					TEXT TO lcSQL NOSHOW TEXTMERGE
						select u_ltstamp2,u_ltstamp from ft (nolock) where ftstamp='<<ALLTRIM(uCrsImpTalaoPos.stamp)>>'
					ENDTEXT
					uf_gerais_actGrelha("", "Ucrsltstamp",lcSQL)
					Select Ucrsltstamp
					If Empty(Alltrim(Ucrsltstamp.u_ltstamp)) And Empty(Alltrim(Ucrsltstamp.u_ltstamp2))
						Select uCrsImpTalaoPos
						Delete
					Endif
					fecha("Ucrsltstamp")
					Select uCrsImpTalaoPos
				Endscan

				**SELECT * FROM uCrsImpTalaoPos WHERE ((!EMPTY(uCrsImpTalaoPos.nrreceita) AND (uCrsImpTalaoPos.rm=.t. OR (uCrsImpTalaoPos.rm=.f. AND uCrsImpTalaoPos.planocomp=.t.)) ) OR (ALLTRIM(uCrsImpTalaoPos.lote)=='98' OR ALLTRIM(uCrsImpTalaoPos.lote)=='99')) AND impresso=.f.  INTO CURSOR uCrsImpTalaoPosTMP READWRITE

				**	uf_perguntalt_chama("2 verifica uCrsImpTalaoPos?","Sim","N�o")

				**SELECT * FROM uCrsImpTalaoPosTMP order by lordem INTO CURSOR uCrsImpTalaoPosTMP READWRITE
				TEXT TO lcSQL NOSHOW TEXTMERGE
					select '' as codigo, 0 as Ucrscptpla
					union all
					select codigo, obriga_nrReceita from cptpla (nolock) --where obriga_nrReceita=0
				ENDTEXT
				uf_gerais_actGrelha("", "Ucrscptpla",lcSQL)

				Select * From uCrsImpTalaoPos INNER Join Ucrscptpla On Ucrscptpla.codigo=uCrsImpTalaoPos.plano;
					WHERE (((!Empty(uCrsImpTalaoPos.nrreceita) Or (Empty(uCrsImpTalaoPos.nrreceita) And Ucrscptpla.Ucrscptpla=0)) And (uCrsImpTalaoPos.rm=.T. Or (uCrsImpTalaoPos.rm=.F. And uCrsImpTalaoPos.planocomp=.T.)) ) Or (Alltrim(uCrsImpTalaoPos.lote)=='98' Or Alltrim(uCrsImpTalaoPos.lote)=='99')) And impresso=.F.  Into Cursor uCrsImpTalaoPosTMP Readwrite

				Select * From uCrsImpTalaoPosTMP Order By Lordem Into Cursor uCrsImpTalaoPosTMP Readwrite

				fecha("Ucrscptpla")
				If Used("uCrsImpTalaoPosTMP")
					Local lcpedeconf
					Store .F. To lcpedeaviso
					Select uCrsImpTalaoPosTMP
					If Reccount("uCrsImpTalaoPosTMP")>1
						lcpedeaviso = .T.
					Endif

					Select uCrsImpTalaoPosTMP
					Go Top
					Scan
						If ucrse1.OrdemImpressaoReceita == .F.
							If uCrsImpTalaoPosTMP.Lordem <> 99999 Or (Alltrim(uCrsImpTalaoPosTMP.lote)=='98' Or Alltrim(uCrsImpTalaoPosTMP.lote)=='99')
								**IF uf_gerais_getParameter_site('ADM0000000136', 'BOOL', mySite)
								Messagebox(14)
								If uf_gerais_getUmValor("B_terminal", "usaDriverWin", "terminal = '" + Alltrim(myTerm) + "'")
									Select uCrsImpTalaoPosTMP
									uf_imprimirpos_impRecTalaoComum_talaoVerso(uCrsImpTalaoPosTMP.stamp, 1, .F., .T.)
								Else
									Select uCrsImpTalaoPosTMP
									uf_imprimirpos_impRec_V2(uCrsImpTalaoPosTMP.stamp, 1, "ATENDIMENTO", lcpedeaviso)
								Endif
							Endif
							Select uCrsImpTalaoPosTMP
							If uCrsImpTalaoPosTMP.rm=.T. Or (uCrsImpTalaoPosTMP.planocomp = .T. And (Alltrim(uCrsImpTalaoPosTMP.lote)=='98' Or Alltrim(uCrsImpTalaoPosTMP.lote)=='99'))
								**IF uf_gerais_getParameter_site('ADM0000000136', 'BOOL', mySite)
								Messagebox(15)
								If uf_gerais_getUmValor("B_terminal", "usaDriverWin", "terminal = '" + Alltrim(myTerm) + "'")
									Select uCrsImpTalaoPosTMP
									uf_imprimirpos_impRecTalaoComum_talaoVerso(uCrsImpTalaoPosTMP.stamp, 2, .F., .T.)
								Else
									uf_imprimirpos_impRec_V2(uCrsImpTalaoPosTMP.stamp, 2, "ATENDIMENTO", lcpedeaviso)
								Endif
							Endif
							Select uCrsImpTalaoPosTMP
							**IF uCrsImpTalaoPosTMP.rm=.f. AND (uCrsImpTalaoPosTMP.planocomp = .t. AND (ALLTRIM(uCrsImpTalaoPosTMP.lote)=='96' OR ALLTRIM(uCrsImpTalaoPosTMP.lote)=='97'))
							**	uf_imprimirpos_impRec_V2(uCrsImpTalaoPosTMP.stamp, 2, "ATENDIMENTO", lcpedeaviso)
							**ENDIF
						Else
							Select uCrsImpTalaoPosTMP
							If uCrsImpTalaoPosTMP.rm=.T. Or (uCrsImpTalaoPosTMP.planocomp = .T. And (Alltrim(uCrsImpTalaoPosTMP.lote)=='98' Or Alltrim(uCrsImpTalaoPosTMP.lote)=='99'))
								**IF uf_gerais_getParameter_site('ADM0000000136', 'BOOL', mySite)
								Messagebox(16)
								If uf_gerais_getUmValor("B_terminal", "usaDriverWin", "terminal = '" + Alltrim(myTerm) + "'")
									uf_imprimirpos_impRecTalaoComum_talaoVerso(uCrsImpTalaoPosTMP.stamp, 2, .F., .T.)
								Else
									uf_imprimirpos_impRec_V2(uCrsImpTalaoPosTMP.stamp, 2, "ATENDIMENTO", lcpedeaviso)
								Endif
							Endif
							Select uCrsImpTalaoPosTMP
							**IF uCrsImpTalaoPosTMP.rm=.f. AND (uCrsImpTalaoPosTMP.planocomp = .t. AND (ALLTRIM(uCrsImpTalaoPosTMP.lote)=='96' OR ALLTRIM(uCrsImpTalaoPosTMP.lote)=='97'))
							**	uf_imprimirpos_impRec_V2(uCrsImpTalaoPosTMP.stamp, 2, "ATENDIMENTO", lcpedeaviso)
							**ENDIF
							If uCrsImpTalaoPosTMP.Lordem <> 99999 Or (Alltrim(uCrsImpTalaoPosTMP.lote)=='98' Or Alltrim(uCrsImpTalaoPosTMP.lote)=='99')
								**IF uf_gerais_getParameter_site('ADM0000000136', 'BOOL', mySite)
								Messagebox(17)
								If uf_gerais_getUmValor("B_terminal", "usaDriverWin", "terminal = '" + Alltrim(myTerm) + "'")
									Select uCrsImpTalaoPosTMP
									uf_imprimirpos_impRecTalaoComum_talaoVerso(uCrsImpTalaoPosTMP.stamp, 1, .F., .T.)
								Else
									Select uCrsImpTalaoPosTMP
									uf_imprimirpos_impRec_V2(uCrsImpTalaoPosTMP.stamp, 1, "ATENDIMENTO", lcpedeaviso)
								Endif
							Endif
						Endif
						Select uCrsImpTalaoPosTMP
						Replace uCrsImpTalaoPosTMP.impresso With .T.
						Update uCrsImpTalaoPos Set impresso=.T. Where Alltrim(uCrsImpTalaoPos.stamp)==Alltrim(uCrsImpTalaoPosTMP.stamp)
					Endscan
				Endif
				fecha("uCrsImpTalaoPosTMP")
				**	uf_perguntalt_chama("2 verifica uCrsImpTalaoPos?","Sim","N�o")
				&& preparar novo atendimento

				**guarda registo de atendimento no sistema de senhas


				uf_gerais_regista_logs_passos("PAGAMENTO",nrAtendimento,"PAG32","FIM EVENTO IMPRIMIR TALAO PAGAMENTO")

				Select ucrse1
				If (ucrse1.has_ticket_integration)
					uf_ticket_update_nrAtend(Alltrim(nrAtendimento))
				Endif

				** Tratar da fatura��o das comparticipa��es �s seguradoras
				Select uCrsCabFactSeg
				If FactCompartSeg And Reccount("uCrsCabFactSeg") > 0
					uf_pagamento_fact_seguradora()
				Endif

				uf_PAGAMENTO_limparVendasFimVd()

				uf_gerais_regista_logs_passos("PAGAMENTO",nrAtendimento,"PAG33","LIMPAR PAGAMENTO")

				uf_PAGAMENTO_sair( .T. )


				** Pergunta se quer fazer o pagamento ou novo atendimento no caso de ter escolhido pagamento posterior
				If pPagPost = .T.
					** Pergunta se quer fazer o pagamento ou novo atendimento no caso de ter escolhido pagamento posterior
					If uf_gerais_getParameter('ADM0000000294', 'BOOL') = .T.
						If uf_perguntalt_chama("Deseja efetuar o recebimento dos documentos ou fazer novo atendimento?","Recebimento","Atendimento")
							pPagPost = .F.
							uf_atendimento_Pagar_Varios(lcOldClNo, lcOldClEstab, .T.)
						Endif
					Endif
				Else
					**este parametro nao se ativa para faturas a seguradora
					If uf_gerais_getParameter("ADM0000000282","NUM")>5 And ucrse1.has_ticket_integration == .T. And Vartype(my_xopvision_term) != "U" And ucrse1.senhaMotivoSemAtendimento == .T. And uf_gerais_getParameter("ADM0000000282","BOOL")=.T.
						uf_filaEspera_carregaDados_conta()
						If Type("contagem") == "U"
							Do Form contagem
						Else
							contagem.Show()
						Endif
					Endif
					&&IF ucrsE1.has_ticket_integration == .T. AND vartype(my_xopvision_term) != "U" AND ucrsE1.senhaMotivoSemAtendimento == .T.
					&& uf_Atendimento_tmrCallNextTicket()
					&&ENDIF
				Endif


			Endif
		Endif

	Endif

	If Used("ucrsfiencact")
		fecha("ucrsfiencact")
	Endif

	If Used("uCrsFiIns")
		fecha("uCrsFiIns")
	Endif

Endfunc


** Gravar dados na tabela FT
Function uf_PAGAMENTO_GravarVendaFt
	Lparameters lnFtStamp, lnNmDoc, lnNdoc, lnTipoDoc, lnClNo, lnClNome, lnClEstab, lnTotQtt, lnQtt1, lnEtotal, lnEttIliq, lnEcusto, lnEttIva, lnTotDesc, lnPdata, lnHora, lnSusp, lnCData, lnNCont, lnbidata

	If Empty(lnNCont)
		lcNcont = ""
	Else
		lcNcont = lnNCont
	Endif

	Local lcExecuteSQL, lcOk, lcu_TipoDoc
	Store "" To lcExecuteSQL
	Store .F. To lcOk
	Store 0 To lcu_TipoDoc

	Select ucrsGeralTd
	Locate For ucrsGeralTd.ndoc == lnNdoc
	If Found()
		lcu_TipoDoc = ucrsGeralTd.u_tipodoc
	Endif

	Select uCrsValIva
	Go Top

	If Type("ATENDIMENTO") != "U"
		If !Used("uCrsActAcerto") && cursor para depois actualizar acerto na multivenda
			Create Cursor uCrsActAcerto (ftstamp c(28), restamp c(28))
		Endif
	Endif

	Local xExecNewDocNr

	&& NOTAS:
	* Bilhete de identidade: ft.bidata, ft.bino, ft.bilocal
	* o campo ftid � incrementado automaticamente pela BD
	If  Type("myCorriguirReceita") <> "U"
		If myCorriguirReceita
			xExecNewDocNr = "EXEC up_get_newDocNr 'FT'," + Str(lnNdoc) + ", '" + mySite + "', 1,1,'" + Alltrim(myOldStamp)+ "' "

		Else
			xExecNewDocNr = "EXEC up_get_newDocNr 'FT'," + Str(lnNdoc) + ", '" + mySite + "', 1"
		Endif
	Else
		xExecNewDocNr = "EXEC up_get_newDocNr 'FT'," + Str(lnNdoc) + ", '" + mySite + "', 1"
	Endif


	lcSQL = ''
	TEXT TO lcSql NOSHOW TEXTMERGE
		--SET @fno = (select ISNULL(MAX(ft.fno),0) + 1 from ft (nolock) where (ft.ftano between Year('<<myInvoiceData>>')-<<IIF(uf_gerais_getParameter_site('ADM0000000142', 'BOOL', mySite),0,1)>> AND Year('<<myInvoiceData>>')) and ft.ndoc=<<ALLTRIM(Str(lnNdoc))>>)

		SET @fno = 0
		DELETE FROM @tabNewDocNr

		INSERT INTO @tabNewDocNr (newDocNr)
		<<xExecNewDocNr>>

		SET @fno = (SELECT newDocNr FROM @tabNewDocNr)

		INSERT INTO ft (
			ftstamp, pais, nmdoc, fno, [no], nome, morada, [local]
			,codpost, ncont, bino, bidata, bilocal, telefone, zona, vendedor
			,vendnm, fdata, ftano, pdata, carga, descar, saida, ivatx1
			,ivatx2, ivatx3, fin, ndoc, moeda, fref, ccusto, ncusto
			,facturada, fnoft, nmdocft,	estab, cdata, ivatx4, segmento, totqtt
			,qtt1, qtt2, tipo, cobrado, cobranca, tipodoc, chora, ivatx5
			,ivatx6, ivatx7, ivatx8, ivatx9, cambiofixo, memissao, cobrador, rota
			,multi, cheque, clbanco, clcheque, chtotal,	echtotal, custo, eivain1
			,eivain2, eivain3, eivav1, eivav2, eivav3, ettiliq, edescc, ettiva
			,etotal, eivain4, eivav4, efinv, ecusto, eivain5, eivav5, edebreg
			,eivain6, eivav6, eivain7, eivav7, eivain8, eivav8, eivain9, eivav9
			,total, totalmoeda, ivain1, ivain2, ivain3, ivain4, ivain5, ivain6
			,ivain7, ivain8, ivain9, ivav1, ivav2, ivav3, ivav4, ivav5
			,ivav6, ivav7, ivav8, ivav9, ttiliq, ttiva, descc, debreg
			,debregm, intid, nome2, tpstamp, tpdesc, erdtotal, rdtotal
			,rdtotalm, cambio, [site], pnome, pno, cxstamp, cxusername, ssstamp, ssusername
			,anulado, virs, evirs, valorm2, u_lote, u_tlote, u_tlote2, u_ltstamp
			,u_slote, u_slote2, u_nslote, u_nslote2, u_ltstamp2, u_lote2, ousrinis, ousrdata
			,ousrhora, usrinis, usrdata, usrhora, u_nratend, u_hclstamp, datatransporte,localcarga
			,id_tesouraria_conta
			, ivatx10 ,ivatx11, ivatx12, ivatx13
			, eivain10 , eivain11 ,eivain12, eivain13
			, eivav10 , eivav11 ,eivav12, eivav13
		)
		Values (
			'<<lnFtStamp>>', <<ft.pais>>, '<<lnNmDoc>>', @fno, <<lnClNo>>, '<<Alltrim(lnClNome)>>', '<<Alltrim(ft.morada)>>', '<<Alltrim(ft.local)>>'
			,'<<Alltrim(ft.codpost)>>', '<<lcNCont>>', '<<Alltrim(ft.bino)>>', '<<ALLTRIM(lnbidata)>>', '<<Alltrim(ft.bilocal)>>', '<<Alltrim(ft.telefone)>>', '<<Alltrim(ft.zona)>>', <<ft.vendedor>>
			,'<<Alltrim(ft.vendnm)>>', '<<myInvoiceData>>', year('<<myInvoiceData>>'), '<<Iif(lcu_TipoDoc=9,uf_gerais_getDate(lnPdata,"SQL"),uf_gerais_getDate((datetime()+(difhoraria*3600)),"SQL"))>>', '<<ALLTRIM(ft.carga)>>', '<<ALLTRIM(ft.descar)>>', Left('<<lnHora>>',5), <<ft.ivatx1>>
			,<<ft.ivatx2>>, <<ft.ivatx3>>, <<ft.fin>>, <<lnNdoc>>, '<<ALLTRIM(ft.moeda)>>',	'<<ALLTRIM(ft.fref)>>', '<<ALLTRIM(ft.ccusto)>>', '<<ALLTRIM(ft.ncusto)>>'
			,<<IIF(ft.facturada,1,0)>>, <<ft.fnoft>>, '<<ALLTRIM(ft.nmdocft)>>', <<lnClEstab>>, '<<ALLTRIM(lnCData)>>', <<ft.ivatx4>>, '<<ALLTRIM(ft.segmento)>>', <<lnTotQtt>>
			,<<lnQtt1>>, <<ft.qtt2>>, '<<ALLTRIM(ft.tipo)>>', <<IIF(EMPTY(lnSusp),0,1)>>, '<<ALLTRIM(ft.cobranca)>>', <<lnTipoDoc>>, Left('<<Chrtran(lnHora,":","")>>',4), <<ft.ivatx5>>
			,<<ft.ivatx6>>, <<ft.ivatx7>>, <<ft.ivatx8>>, <<ft.ivatx9>>, <<IIF(ft.cambiofixo,1,0)>>, '<<ALLTRIM(ft.memissao)>>', '<<ALLTRIM(ft.cobrador)>>', '<<ALLTRIM(ft.rota)>>'
			,<<IIF(ft.multi,1,0)>>, <<IIF(ft.cheque,1,0)>>, '<<ALLTRIM(ft.clbanco)>>', '<<ALLTRIM(ft.clcheque)>>', <<IIF(lnNdoc!=myFact,ft.chtotal,0)>>,<<IIF(lnNdoc!=myFact,ft.echtotal,0)>>,<<lnEcusto*200.482>>, <<uCrsValIva.lcEivaIn1>>
			,<<uCrsValIva.lcEivaIn2>>, <<uCrsValIva.lcEivaIn3>>, <<uCrsValIva.lcEivaV1>>, <<uCrsValIva.lcEivaV2>>, <<uCrsValIva.lcEivaV3>>, <<lnEttIliq>>, <<lnTotDesc>>, <<lnEttIva>>
			,<<lnEtotal>>, <<uCrsValIva.lcEivaIn4>>, <<uCrsValIva.lcEivaV4>>, <<ft.efinv>>, <<lnEcusto>>, <<uCrsValIva.lcEivaIn5>>, <<uCrsValIva.lcEivaV5>>, <<ft.edebreg>>
			,<<uCrsValIva.lcEivaIn6>>, <<uCrsValIva.lcEivaV6>>, <<uCrsValIva.lcEivaIn7>>, <<uCrsValIva.lcEivaV7>>, <<uCrsValIva.lcEivaIn8>>, <<uCrsValIva.lcEivaV8>>, <<uCrsValIva.lcEivaIn9>>, <<uCrsValIva.lcEivaV9>>
			,<<lnEtotal*200.482>>, <<ft.totalmoeda>>, <<uCrsValIva.lcEivaIn1*200.482>>, <<uCrsValIva.lcEivaIn2*200.482>>, <<uCrsValIva.lcEivaIn3*200.482>>, <<uCrsValIva.lcEivaIn4*200.482>>, <<uCrsValIva.lcEivaIn5*200.482>>, <<ft.ivain6>>
			,<<ft.ivain7>>, <<ft.ivain8>>, <<ft.ivain9>>, <<uCrsValIva.lcEivaV1*200.482>>, <<uCrsValIva.lcEivaV2*200.482>>, <<uCrsValIva.lcEivaV3*200.482>>, <<uCrsValIva.lcEivaV4*200.482>>, <<uCrsValIva.lcEivaV5*200.482>>
			,<<ft.ivav6>>, <<ft.ivav7>>, <<ft.ivav8>>, <<ft.ivav9>>, <<lnEttIliq*200.482>>,	<<lnEttIva*200.482>>, <<ft.descc>>, <<ft.debreg>>
			,<<ft.debregm>>, '<<ALLTRIM(ft.intid)>>', '<<ALLTRIM(ft.nome2)>>', '<<ALLTRIM(ft.tpstamp)>>', '<<ALLTRIM(ft.tpdesc)>>', <<ft.erdtotal>>, <<ft.rdtotal>>, <<ft.rdtotalm>>
			,<<IIF(lnNdoc=myFact,1,0)>>, '<<ALLTRIM(mySite)>>', '<<ALLTRIM(myTerm)>>', <<myTermNo>>, '<<ALLTRIM(ft.cxstamp)>>', '<<ALLTRIM(ft.cxusername)>>', '<<ALLTRIM(ft.ssstamp)>>', '<<ALLTRIM(ft.ssusername)>>'
			,<<IIF(ft.anulado,1,0)>>, <<ft.virs>>, <<ft.evirs>>, <<ft.valorm2>>, '<<ft.u_lote>>', '<<ft.u_tlote>>', '<<ft.u_tlote2>>', '<<ALLTRIM(ft.u_ltstamp)>>'
			,<<ft.u_slote>>, <<ft.u_slote2>>, <<ft.u_nslote>>, <<ft.u_nslote2>>, '<<ALLTRIM(ft.u_ltstamp2)>>', <<ft.u_lote2>>, '<<ALLTRIM(m_chinis)>>', CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
			, '<<ALLTRIM(lnHora)>>', '<<ALLTRIM(m_chinis)>>', CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), '<<ALLTRIM(lnHora)>>', '<<ALLTRIM(nrAtendimento)>>','<<ALLTRIM(ft.u_hclstamp)>>', '<<ALLTRIM(uf_gerais_getDate(ft.datatransporte,"SQL"))>>','<<ALLTRIM(ft.localcarga)>>'
			,<<ft.id_tesouraria_conta>>
			,<<ft.ivatx10>>, <<ft.ivatx11>>,<<ft.ivatx12>>, <<ft.ivatx13>>
			,<<uCrsValIva.lcEivaIn10>>, <<uCrsValIva.lcEivaIn11>>,<<uCrsValIva.lcEivaIn12>>, <<uCrsValIva.lcEivaIn13>>
			,<<uCrsValIva.lcEivaV10>>, <<uCrsValIva.lcEivaV11>>, <<uCrsValIva.lcEivaV12>>,<<uCrsValIva.lcEivaV13>>
		)
	ENDTEXT




	lcExecuteSQL = lcExecuteSQL + Chr(13) + lcSQL




	If Type("ATENDIMENTO") != "U"

		&& acrescentar nova linha ao cursor
		If Used("uCrsFt")
			Select uCrsFt
			Append From Dbf("ft")
			Replace;
				ftstamp With lnFtStamp;
				nmdoc 	With lnNmDoc;
				no 		With lnClNo;
				nome 	With Alltrim(lnClNome);
				fdata	With Date();
				ftano	With Year(Date());
			&&pdata	WITH Iif(lnNdoc=myFact,lnPdata,datetime());
			pdata	WITH Iif(lnNdoc=myFact,lnPdata,datetime()+(difhoraria*3600));
			saida	WITH Left(lnHora,5);
			ndoc	WITH lnNdoc;
			estab	WITH lnClEstab;
			cdata	WITH CTOD(uf_gerais_getDate(lnCData));
			bidata  WITH CTOD(uf_gerais_getDate(lnbidata));
			totqtt	WITH lnTotQtt;
			qtt1	WITH lnQtt1;
			cobrado	WITH lnSusp;
			tipodoc	WITH lnTipoDoc;
			chora	WITH Left(Chrtran(lnHora,":",""),4);
			chtotal	WITH IIF(lnNdoc!=myFact,ft.chtotal,0);
			echtotal WITH IIF(lnNdoc!=myFact,ft.echtotal,0);
			custo	WITH lnEcusto*200.482;
			eivain1	WITH uCrsValIva.lcEivaIn1;
			eivain2	WITH uCrsValIva.lcEivaIn2;
			eivain3	WITH uCrsValIva.lcEivaIn3;
			eivav1	WITH uCrsValIva.lcEivaV1;
			eivav2	WITH uCrsValIva.lcEivaV2;
			eivav3	WITH uCrsValIva.lcEivaV3;
			ettiliq	WITH lnEttIliq;
			edescc	WITH lnTotDesc;
			ettiva	WITH lnEttIva;
			etotal	WITH lnEtotal;
			eivain4	WITH uCrsValIva.lcEivaIn4;
			eivav4	WITH uCrsValIva.lcEivaV4;
			ecusto	WITH lnEcusto;
			eivain5 WITH uCrsValIva.lcEivaIn5;
			eivav5	WITH uCrsValIva.lcEivaV5;
			eivain6 WITH uCrsValIva.lcEivaIn6;
			eivav6	WITH uCrsValIva.lcEivaV6;
			eivain7 WITH uCrsValIva.lcEivaIn7;
			eivav7	WITH uCrsValIva.lcEivaV7;
			eivain8 WITH uCrsValIva.lcEivaIn8;
			eivav8	WITH uCrsValIva.lcEivaV8;
			eivain9 WITH uCrsValIva.lcEivaIn9;
			eivav9	WITH uCrsValIva.lcEivaV9;
			total	WITH lnEtotal*200.482;
			ivain1	WITH uCrsValIva.lcEivaIn1*200.482;
			ivain2	WITH uCrsValIva.lcEivaIn2*200.482;
			ivain3	WITH uCrsValIva.lcEivaIn3*200.482;
			ivain4	WITH uCrsValIva.lcEivaIn4*200.482;
			ivain5	WITH uCrsValIva.lcEivaIn5*200.482;
			ivav1	WITH uCrsValIva.lcEivaV1*200.482;
			ivav2	WITH uCrsValIva.lcEivaV2*200.482;
			ivav3	WITH uCrsValIva.lcEivaV3*200.482;
			ivav4	WITH uCrsValIva.lcEivaV4*200.482;
			ivav5	WITH uCrsValIva.lcEivaV5*200.482;
			ttiliq	WITH lnEttIliq*200.482;
			ttiva	WITH lnEttIva*200.482;
			cambio	WITH IIF(lnNdoc=myFact,1,0);
			pnome	WITH myTerm;
			pno		WITH myTermNo;
			ousrinis	WITH m_chinis;
			ousrdata	WITH date();
			ousrhora	WITH lnHora;
			usrinis		WITH m_chinis;
			usrdata		WITH date();
			usrhora		WITH lnHora;
			u_nratend	WITH nrAtendimento

			Select ucrsGeralTd
			Scan For ndoc=lnNdoc
				Select uCrsFt
				Replace;
					u_tipodoc	With ucrsGeralTd.u_tipodoc;
					tiposaft	With ucrsGeralTd.tiposaft
			Endscan
		Endif

	Endif

	If Type("ATENDIMENTO") != "U"
		Insert Into uCrsActAcerto (ftstamp, restamp) Values (Alltrim(lnFtStamp), '')
	Endif

	Return lcExecuteSQL
Endfunc





** Gravar dados na tabela FT Seguradoras
Function uf_PAGAMENTO_GravarVendaFt_Seg
	Lparameters lnFtStamp, lnNmDoc, lnNdoc, lnTipoDoc, lnClNo, lnClNome, lnClEstab, lnTotQtt, lnQtt1, lnEtotal, lnEttIliq, lnEcusto, lnEttIva, lnTotDesc, lnPdata, lnHora, lnSusp, lnCData, lnNCont, lnbidata, lnNrAtendimento

	Local lcExecuteSQL, lcOk, lcu_TipoDoc
	Store "" To lcExecuteSQL
	Store .F. To lcOk
	Store 0 To lcu_TipoDoc



	lcSQL = ''
	TEXT TO lcSql NOSHOW TEXTMERGE
		--SET @fno = (select ISNULL(MAX(ft.fno),0) + 1 from ft (nolock) where (ft.ftano between Year('<<myInvoiceData>>')-<<IIF(uf_gerais_getParameter_site('ADM0000000142', 'BOOL', mySite),0,1)>> AND Year('<<myInvoiceData>>')) and ft.ndoc=<<ALLTRIM(Str(lnNdoc))>>)

		SET @fno = 0
		DELETE FROM @tabNewDocNr

		INSERT INTO @tabNewDocNr (newDocNr)
		EXEC up_get_newDocNr 'FT', <<lnNdoc>>, '<<mySite>>', 1

		SET @fno = (SELECT newDocNr FROM @tabNewDocNr)

		INSERT INTO ft (
			ftstamp, pais, nmdoc, fno, [no], nome, morada, [local]
			,codpost, ncont, bino, bidata, bilocal, telefone, zona, vendedor
			,vendnm, fdata, ftano, pdata, carga, descar, saida, ivatx1
			,ivatx2, ivatx3, fin, ndoc, moeda, fref, ccusto, ncusto
			,facturada, fnoft, nmdocft,	estab, cdata, ivatx4, segmento, totqtt
			,qtt1, qtt2, tipo, cobrado, cobranca, tipodoc, chora, ivatx5
			,ivatx6, ivatx7, ivatx8, ivatx9, cambiofixo, memissao, cobrador, rota
			,multi, cheque, clbanco, clcheque, chtotal,	echtotal, custo, eivain1
			,eivain2, eivain3, eivav1, eivav2, eivav3, ettiliq, edescc, ettiva
			,etotal, eivain4, eivav4, efinv, ecusto, eivain5, eivav5, edebreg
			,eivain6, eivav6, eivain7, eivav7, eivain8, eivav8, eivain9, eivav9
			,total, totalmoeda, ivain1, ivain2, ivain3, ivain4, ivain5, ivain6
			,ivain7, ivain8, ivain9, ivav1, ivav2, ivav3, ivav4, ivav5
			,ivav6, ivav7, ivav8, ivav9, ttiliq, ttiva, descc, debreg
			,debregm, intid, nome2, tpstamp, tpdesc, erdtotal, rdtotal
			,rdtotalm, cambio, [site], pnome, pno, cxstamp, cxusername, ssstamp, ssusername
			,anulado, virs, evirs, valorm2, u_lote, u_tlote, u_tlote2, u_ltstamp
			,u_slote, u_slote2, u_nslote, u_nslote2, u_ltstamp2, u_lote2, ousrinis, ousrdata
			,ousrhora, usrinis, usrdata, usrhora, u_nratend, u_hclstamp, datatransporte,localcarga
			,id_tesouraria_conta
			, ivatx10 ,ivatx11, ivatx12, ivatx13
			, eivain10 , eivain11 ,eivain12, eivain13
			, eivav10 , eivav11 ,eivav12, eivav13
		)
		Values (
			'<<lnFtStamp>>', <<sft.pais>>, '<<lnNmDoc>>', @fno, <<lnClNo>>, '<<Alltrim(lnClNome)>>', '<<Alltrim(sft.morada)>>', '<<Alltrim(sft.local)>>'
			,'<<Alltrim(sft.codpost)>>', '<<sft.NCont>>', '<<Alltrim(sft.bino)>>', '<<ALLTRIM(lnbidata)>>', '<<Alltrim(sft.bilocal)>>', '<<Alltrim(sft.telefone)>>', '<<Alltrim(sft.zona)>>', <<sft.vendedor>>
			,'<<Alltrim(sft.vendnm)>>', '<<myInvoiceData>>', year('<<myInvoiceData>>'), '<<Iif(lcu_TipoDoc=9,uf_gerais_getDate(lnPdata,"SQL"),uf_gerais_getDate((datetime()+(difhoraria*3600)),"SQL"))>>', '<<ALLTRIM(sft.carga)>>', '<<ALLTRIM(sft.descar)>>', Left('<<lnHora>>',5), <<sft.ivatx1>>
			,<<sft.ivatx2>>, <<sft.ivatx3>>, <<sft.fin>>, <<lnNdoc>>, '<<ALLTRIM(sft.moeda)>>',	'<<ALLTRIM(sft.fref)>>', '<<ALLTRIM(sft.ccusto)>>', '<<ALLTRIM(sft.ncusto)>>'
			,<<IIF(sft.facturada,1,0)>>, <<sft.fnoft>>, '<<ALLTRIM(sft.nmdocft)>>', <<lnClEstab>>, '<<ALLTRIM(lnCData)>>', <<sft.ivatx4>>, '<<ALLTRIM(sft.segmento)>>', <<lnTotQtt>>
			,<<lnQtt1>>, <<sft.qtt2>>, '<<ALLTRIM(sft.tipo)>>', <<IIF(EMPTY(lnSusp),0,1)>>, '<<ALLTRIM(sft.cobranca)>>', <<lnTipoDoc>>, Left('<<Chrtran(lnHora,":","")>>',4), <<sft.ivatx5>>
			,<<sft.ivatx6>>, <<sft.ivatx7>>, <<sft.ivatx8>>, <<sft.ivatx9>>, <<IIF(sft.cambiofixo,1,0)>>, '<<ALLTRIM(sft.memissao)>>', '<<ALLTRIM(sft.cobrador)>>', '<<ALLTRIM(sft.rota)>>'
			,<<IIF(sft.multi,1,0)>>, <<IIF(sft.cheque,1,0)>>, '<<ALLTRIM(sft.clbanco)>>', '<<ALLTRIM(sft.clcheque)>>', <<IIF(lnNdoc!=myFact,sft.chtotal,0)>>,<<IIF(lnNdoc!=myFact,sft.echtotal,0)>>,<<lnEcusto*200.482>>, <<uCrsValIva.lcEivaIn1>>
			,<<uCrsValIva.lcEivaIn2>>, <<uCrsValIva.lcEivaIn3>>, <<uCrsValIva.lcEivaV1>>, <<uCrsValIva.lcEivaV2>>, <<uCrsValIva.lcEivaV3>>, <<lnEttIliq>>, <<lnTotDesc>>, <<lnEttIva>>
			,<<lnEtotal>>, <<uCrsValIva.lcEivaIn4>>, <<uCrsValIva.lcEivaV4>>, <<sft.efinv>>, <<lnEcusto>>, <<uCrsValIva.lcEivaIn5>>, <<uCrsValIva.lcEivaV5>>, <<sft.edebreg>>
			,<<sft.eivain6>>, <<sft.eivav6>>, <<sft.eivain7>>, <<sft.eivav7>>, <<sft.eivain8>>, <<sft.eivav8>>, <<sft.eivain9>>, <<sft.eivav9>>
			,<<lnEtotal*200.482>>, <<sft.totalmoeda>>, <<uCrsValIva.lcEivaIn1*200.482>>, <<uCrsValIva.lcEivaIn2*200.482>>, <<uCrsValIva.lcEivaIn3*200.482>>, <<uCrsValIva.lcEivaIn4*200.482>>, <<uCrsValIva.lcEivaIn5*200.482>>, <<sft.ivain6>>
			,<<sft.ivain7>>, <<sft.ivain8>>, <<sft.ivain9>>, <<uCrsValIva.lcEivaV1*200.482>>, <<uCrsValIva.lcEivaV2*200.482>>, <<uCrsValIva.lcEivaV3*200.482>>, <<uCrsValIva.lcEivaV4*200.482>>, <<uCrsValIva.lcEivaV5*200.482>>
			,<<sft.ivav6>>, <<sft.ivav7>>, <<sft.ivav8>>, <<sft.ivav9>>, <<lnEttIliq*200.482>>,	<<lnEttIva*200.482>>, <<sft.descc>>, <<sft.debreg>>
			,<<sft.debregm>>, '<<ALLTRIM(sft.intid)>>', '<<ALLTRIM(sft.nome2)>>', '<<ALLTRIM(sft.tpstamp)>>', '<<ALLTRIM(sft.tpdesc)>>', <<sft.erdtotal>>, <<sft.rdtotal>>, <<sft.rdtotalm>>
			,<<IIF(lnNdoc=myFact,1,0)>>, '<<ALLTRIM(mySite)>>', '<<ALLTRIM(myTerm)>>', <<myTermNo>>, '<<ALLTRIM(sft.cxstamp)>>', '<<ALLTRIM(sft.cxusername)>>', '<<ALLTRIM(sft.ssstamp)>>', '<<ALLTRIM(sft.ssusername)>>'
			,<<IIF(sft.anulado,1,0)>>, <<sft.virs>>, <<sft.evirs>>, <<sft.valorm2>>, '<<sft.u_lote>>', '<<sft.u_tlote>>', '<<sft.u_tlote2>>', '<<ALLTRIM(sft.u_ltstamp)>>'
			,<<sft.u_slote>>, <<sft.u_slote2>>, <<sft.u_nslote>>, <<sft.u_nslote2>>, '<<ALLTRIM(sft.u_ltstamp2)>>', <<sft.u_lote2>>, '<<ALLTRIM(m_chinis)>>', CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
			, '<<ALLTRIM(lnHora)>>', '<<ALLTRIM(m_chinis)>>', CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), '<<ALLTRIM(lnHora)>>', '<<ALLTRIM(lnNrAtendimento)>>','<<ALLTRIM(sft.u_hclstamp)>>', '<<ALLTRIM(uf_gerais_getDate(sft.datatransporte,"SQL"))>>','<<ALLTRIM(sft.localcarga)>>'
			,<<sft.id_tesouraria_conta>>
			,<<sft.ivatx10>>, <<sft.ivatx11>>,<<sft.ivatx12>>, <<sft.ivatx13>>
			,<<uCrsValIva.lcEivaIn10>>, <<uCrsValIva.lcEivaIn11>>,<<uCrsValIva.lcEivaIn12>>, <<uCrsValIva.lcEivaIn13>>
			,<<uCrsValIva.lcEivaV10>>, <<uCrsValIva.lcEivaV11>>, <<uCrsValIva.lcEivaV12>>,<<uCrsValIva.lcEivaV13>>
		)
	ENDTEXT

	lcExecuteSQL = lcExecuteSQL + Chr(13) + lcSQL

	Return lcExecuteSQL
Endfunc


** Gravar dados FT2
Function uf_PAGAMENTO_GravarVendaFt2
	Lparameters lnFtStamp, lnNdoc, lnReceita, lnCod, lnCod2, lnDesign, lnDesign2, lnAbrev, lnAbrev2, lnHora, lnCodAcesso, lnCodDirOpcao, lnToken, uv_tokenEfectiva

	Local lcSQL, lcOk, lcExecuteSQL
	Store "" To lcExecuteSQL, lcSQL


	&& Notas:
	* vdollocal = "Caixa"
	* vdlocal = "C"

	lcSQL = ''
	TEXT TO lcSQL NOSHOW TEXTMERGE
		Insert Into ft2 (
			contacto, u_viatura, morada, codpost, local, telefone, email, horaentrega
			,ft2stamp, u_vddom, obsdoc, u_receita, u_entpresc, u_nopresc, u_codigo, u_codigo2
			,u_nbenef, u_nbenef2, u_abrev, u_abrev2, c2no, c2estab, subproc, u_vdsusprg
			,u_design, u_design2, c2nome, c2pais, evdinheiro, epaga1, epaga2, etroco
			,u_acertovs, c2codpost, eacerto, vdollocal, vdlocal, modop1, modop2, ousrdata
			,ousrinis, ousrhora, usrdata, usrinis, usrhora, motiseimp, formapag, epaga3
			,epaga4, epaga5, epaga6, codAcesso, codDirOpcao, token, u_docorig
			,token_efectivacao_compl, token_anulacao_compl, valcativa, cativa, perccativa, impresso, stamporigproc, nrelegibilidade,u_hclstamp1, obsCl, obsInt,
            xpdCodPost, xpdLocalidade, nomeUtPag, id_efr_externa
		)
		Values (
			'<<ALLTRIM(ft2.contacto)>>', '<<ALLTRIM(ft2.u_viatura)>>', '<<ALLTRIM(ft2.morada)>>', '<<ALLTRIM(ft2.codpost)>>', '<<ALLTRIM(ft2.local)>>', '<<ALLTRIM(ft2.telefone)>>', '<<ALLTRIM(ft2.email)>>', '<<ALLTRIM(ft2.horaentrega)>>'
			,'<<ALLTRIM(lnFtStamp)>>', <<IIF(ft2.u_vddom,1,0)>>, '<<ALLTRIM(ft2.obsdoc)>>', '<<LEFT(lnReceita,20)>>', '<<ALLTRIM(ft2.u_entpresc)>>', '<<ALLTRIM(ft2.u_nopresc)>>', '<<ALLTRIM(lnCod)>>', '<<ALLTRIM(lnCod2)>>'
			,'<<ALLTRIM(ft2.u_nbenef)>>', '<<ALLTRIM(ft2.u_nbenef2)>>', '<<ALLTRIM(lnAbrev)>>', '<<ALLTRIM(lnAbrev2)>>', <<ft2.c2no>>, <<ft2.c2estab>>, '<<ALLTRIM(ft2.subproc)>>', <<IIF(ft2.u_vdsusprg,1,0)>>
			,'<<ALLTRIM(lnDesign)>>', '<<ALLTRIM(lnDesign2)>>', '<<ALLTRIM(ft2.c2nome)>>', <<ft2.c2pais>>, <<Iif(lnNdoc!=myFact,ft2.evdinheiro,0)>>, <<Iif(lnNdoc!=myFact,ft2.epaga1,0)>>, <<Iif(lnNdoc!=myFact,ft2.epaga2,0)>>, <<IIF(lnNdoc!=myFact,ft2.etroco,0)>>
			,<<ft2.u_acertovs>>, '<<ALLTRIM(ft2.c2codpost)>>', <<ft2.eacerto>>, 'Caixa', 'C', '<<ALLTRIM(ft2.modop1)>>', '<<ALLTRIM(ft2.modop2)>>', CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
			,'<<ALLTRIM(m_chinis)>>', '<<ALLTRIM(lnHora)>>', CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), '<<ALLTRIM(m_chinis)>>', '<<ALLTRIM(lnHora)>>', '<<ALLTRIM(ft2.motiseimp)>>', '1', <<ft2.epaga3>>
			,<<ft2.epaga4>>, <<ft2.epaga5>>, <<ft2.epaga6>>, '<<IIF(EMPTY(lnCodAcesso),"",ALLTRIM(lnCodAcesso))>>', '<<IIF(EMPTY(lnCodDirOpcao),"",ALLTRIM(lnCodDirOpcao))>>','<<IIF(EMPTY(lntoken),"",ALLTRIM(lntoken))>>', '<<ALLTRIM(ft2.u_docorig)>>'
			,'<<IIF(!EMPTY(uv_tokenEfectiva), ALLTRIM(uv_tokenEfectiva), "")>>','', <<ft2.valcativa>>, <<IIF(ft2.cativa,1,0)>>, <<ft2.perccativa>>, 1, '<<ALLTRIM(ft2.stamporigproc)>>', '<<ALLTRIM(ft2.nrelegibilidade)>>', '<<ALLTRIM(ft2.U_HCLSTAMP1)>>', '<<ALLTRIM(ft2.obsCl)>>', '<<ALLTRIM(ft2.obsInt)>>',
            '<<IIF(TYPE("ft2.xpdCodpost") == "U", "", ALLTRIM(ft2.xpdCodpost))>>', '<<IIF(TYPE("ft2.xpdLocalidade") == "U", "", ALLTRIM(ft2.xpdLocalidade))>>','<<IIF(TYPE("ft2.nomeUtPag") == "U", "", ALLTRIM(ft2.nomeUtPag))>>', '<<ft2.id_efr_externa>>'
		)
	ENDTEXT

	lcExecuteSQL  = lcExecuteSQL + Chr(13) + lcSQL

	If Vartype(StampProcNC)!="U"
		If !Empty(StampProcNC)
			lcSQL = ''
			TEXT TO lcSQL NOSHOW TEXTMERGE
				update ft2 SET stamporigproc='<<ALLTRIM(lnFtStamp)>>' WHERE ft2stamp='<<StampProcNC>>'
			ENDTEXT
			StampProcNC=''

			lcExecuteSQL  = lcExecuteSQL + Chr(13) + lcSQL
		Endif
	Endif


	** validar se vai haver fatura��o da comparticipa��o � seguradora. se sim adicionar ao cursor
	If FactCompartSeg And Len(Alltrim(lnCod))>0
		Select uCrsCabFactSeg
		Go Bottom
		Append Blank
		Replace uCrsCabFactSeg.ftstamp With Alltrim(lnFtStamp)
	Endif

	If Type("ATENDIMENTO") != "U"

		If Used("uCrsFt2")

			&& acrescentar nova linha ao cursor
			Select ft2
			Go Top

			Select uCrsFt2
			Append From Dbf("ft2")
			Replace;
				ft2stamp 	With lnFtStamp;
				epaga1		With Iif(lnNdoc!=myFact,ft2.epaga1,0);
				epaga2		With Iif(lnNdoc!=myFact,ft2.epaga2,0);
				etroco		With Iif(lnNdoc!=myFact,ft2.etroco,0);
				evdinheiro	With Iif(lnNdoc!=myFact,ft2.evdinheiro,0);
				vdollocal	With 'Caixa';
				vdlocal		With 'C';
				u_codigo	With lnCod;
				u_receita	With lnReceita;
				u_design	With lnDesign;
				u_codigo2	With lnCod2;
				u_design2	With lnDesign2;
				u_abrev		With lnAbrev;
				u_abrev2	With lnAbrev2;
				ousrinis	With m_chinis;
				ousrdata	With Date();
				ousrhora	With lnHora;
				usrinis		With m_chinis;
				usrdata		With Date();
				usrhora		With lnHora;
				u_nbenef	With ft2.u_nbenef;
				u_nbenef2	With ft2.u_nbenef2;
				nomeUtPag	With ft2.nomeUtPag
		Endif
	Endif

	Return lcExecuteSQL
Endfunc


** Gravar dados FT2 seguradoras
Function uf_PAGAMENTO_GravarVendaFt2_Seg
	Lparameters lnFtStamp, lnNdoc, lnReceita, lnCod, lnCod2, lnDesign, lnDesign2, lnAbrev, lnAbrev2, lnHora, lnCodAcesso, lnCodDirOpcao, lnToken

	Local lcSQL, lcOk, lcExecuteSQL
	Store "" To lcExecuteSQL, lcSQL
	Select sft2

	&& Notas:
	* vdollocal = "Caixa"
	* vdlocal = "C"
	lcSQL = ''
	TEXT TO lcSQL NOSHOW TEXTMERGE
		Insert Into ft2 (
			contacto, u_viatura, morada, codpost, local, telefone, email, horaentrega
			,ft2stamp, u_vddom, obsdoc, u_receita, u_entpresc, u_nopresc, u_codigo, u_codigo2
			,u_nbenef, u_nbenef2, u_abrev, u_abrev2, c2no, c2estab, subproc, u_vdsusprg
			,u_design, u_design2, c2nome, c2pais, evdinheiro, epaga1, epaga2, etroco
			,u_acertovs, c2codpost, eacerto, vdollocal, vdlocal, modop1, modop2, ousrdata
			,ousrinis, ousrhora, usrdata, usrinis, usrhora, motiseimp, formapag, epaga3
			,epaga4, epaga5, epaga6, codAcesso, codDirOpcao, token, u_docorig
			, token_efectivacao_compl, token_anulacao_compl, valcativa, cativa, perccativa, impresso, stamporigproc, nrelegibilidade, id_efr_externa
		)
		Values (
			'<<ALLTRIM(sft2.contacto)>>', '<<ALLTRIM(sft2.u_viatura)>>', '<<ALLTRIM(sft2.morada)>>', '<<ALLTRIM(sft2.codpost)>>', '<<ALLTRIM(sft2.local)>>', '<<ALLTRIM(sft2.telefone)>>', '<<ALLTRIM(sft2.email)>>', '<<ALLTRIM(sft2.horaentrega)>>'
			,'<<ALLTRIM(lnFtStamp)>>', <<IIF(sft2.u_vddom,1,0)>>, '<<ALLTRIM(sft2.obsdoc)>>', '<<LEFT(lnReceita,20)>>', '<<ALLTRIM(sft2.u_entpresc)>>', '<<ALLTRIM(sft2.u_nopresc)>>', '<<ALLTRIM(lnCod)>>', '<<ALLTRIM(lnCod2)>>'
			,'<<ALLTRIM(sft2.u_nbenef)>>', '<<ALLTRIM(sft2.u_nbenef2)>>', '<<ALLTRIM(lnAbrev)>>', '<<ALLTRIM(lnAbrev2)>>', <<sft2.c2no>>, <<sft2.c2estab>>, '<<ALLTRIM(sft2.subproc)>>', <<IIF(sft2.u_vdsusprg,1,0)>>
			,'<<ALLTRIM(lnDesign)>>', '<<ALLTRIM(lnDesign2)>>', '<<ALLTRIM(sft2.c2nome)>>', <<sft2.c2pais>>, <<Iif(lnNdoc!=myFact,sft2.evdinheiro,0)>>, <<Iif(lnNdoc!=myFact,sft2.epaga1,0)>>, <<Iif(lnNdoc!=myFact,sft2.epaga2,0)>>, <<IIF(lnNdoc!=myFact,sft2.etroco,0)>>
			,<<sft2.u_acertovs>>, '<<ALLTRIM(sft2.c2codpost)>>', <<sft2.eacerto>>, 'Caixa', 'C', '<<ALLTRIM(sft2.modop1)>>', '<<ALLTRIM(sft2.modop2)>>', CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
			,'<<ALLTRIM(m_chinis)>>', '<<ALLTRIM(lnHora)>>', CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), '<<ALLTRIM(m_chinis)>>', '<<ALLTRIM(lnHora)>>', '<<ALLTRIM(sft2.motiseimp)>>', '1', <<sft2.epaga3>>
			,<<sft2.epaga4>>, <<sft2.epaga5>>, <<sft2.epaga6>>, '<<IIF(EMPTY(lnCodAcesso),"",ALLTRIM(lnCodAcesso))>>', '<<IIF(EMPTY(lnCodDirOpcao),"",ALLTRIM(lnCodDirOpcao))>>','<<IIF(EMPTY(lntoken),"",ALLTRIM(lntoken))>>', '<<ALLTRIM(sft2.u_docorig)>>'
			,'','', <<sft2.valcativa>>, <<IIF(sft2.cativa,1,0)>>, <<sft2.perccativa>>, 1, '<<ALLTRIM(sft2.stamporigproc)>>', '<<ALLTRIM(sft2.nrelegibilidade)>>', '<<ft2.id_efr_externa>>'
		)
	ENDTEXT

	lcExecuteSQL  = lcExecuteSQL + Chr(13) + lcSQL

	Return lcExecuteSQL
Endfunc


** actualizar reinsercao de receita em comparticipa��es externas
Function uf_PAGAMENTO_ActualizaCompartExterna
	Lparameters lnFtStamp, lnOFtStamp, lcTokenExterno



	Local  lcExecuteSQL, lcSQL
	Store "" To lcExecuteSQL, lcSQL


	lcSQL = ''
	If Type("stampcompart")<>"U" And Len(Alltrim(stampcompart))>0
		TEXT TO lcSQL NOSHOW TEXTMERGE
			update ft_compart set ofstamp=ftstamp , ftstamp = '<<ALLTRIM(lnFtStamp)>>'  where ftstamp = '<<ALLTRIM(lnOFtStamp)>>' and token='<<ALLTRIM(lcTokenExterno)>>' and type = 2 AND ftstamp like '%<<ALLTRIM(stampcompart)>>%' AND LEN(ftstamp)<21 AND ousrdata>DATEADD(ss,-30,getdate())

			update ft2 set token_efectivacao_compl='<<ALLTRIM(lcTokenExterno)>>'  WHERE ft2stamp = '<<ALLTRIM(lnFtStamp)>>' AND ousrdata>DATEADD(ss,-60,getdate())
		ENDTEXT
	Else
		TEXT TO lcSQL NOSHOW TEXTMERGE
			update ft_compart set ofstamp=ftstamp , ftstamp = '<<ALLTRIM(lnFtStamp)>>'  where ftstamp = '<<ALLTRIM(lnOFtStamp)>>' and token='<<ALLTRIM(lcTokenExterno)>>' and type = 2

			update ft2 set token_efectivacao_compl='<<ALLTRIM(lcTokenExterno)>>'  WHERE ft2stamp = '<<ALLTRIM(lnFtStamp)>>'
		ENDTEXT
	Endif

	**_cliptext=lcSql
	**uf_perguntalt_chama("Espera aqui","OK","",16)

	lcExecuteSQL  = lcExecuteSQL + Chr(13) + lcSQL


	Return lcExecuteSQL
Endfunc





&&GRAVA LINHA FI2
Function uf_PAGAMENTO_gravarVendaFi2
	Lparameters lcFtstamp,lcFiStamp, lcCodMotiseImpLin, lcMotiseImpLin

	Local lcSqlTotal

	lcSqlTotal  = ''


	&&valida se curosr existe
	If(!Used("fi2"))
		Return ''
	Endif


	&&valida se o cursor est� preenchido
	If(Reccount("fi2")==0)
		Return ''
	Endif




	Select fi2
	Go Top
	Scan  For Alltrim(fi2.fistamp) == Alltrim(lcFiStamp)
		Local lcDataValidade,lcFiStamp,lcSQL,lcJustTecCod, lcJustTecDescr,lcIndisponivel, lcIsDem, lcIdValidacaoDem, lcBonusTipo, lcBonusId, lcBonusDescr, lcDescontoHabitual, lnReturnContributedValue, lcIdProcesso, idEpisodio

		Store '' To lcJustTecCod,lcJustTecDescr, lcIdProcesso, idEpisodio

		lcSQL                		= ''
		lcFiStamp            		= Alltrim(fi2.fistamp)
		lcCodUnicoEmb        		= Alltrim(fi2.u_comb_emb_matrix)
		lcValCatIva          		= fi2.valcativa
		lcJustTecCod         		= Alltrim(fi2.justificacao_tecnica_cod)
		lcJustTecDescr       		= Alltrim(fi2.justificacao_tecnica_descr)
		lcIsDem              		= Iif(fi2.isDem,1,0)
		lcIdValidacaoDem     		= Alltrim(fi2.iDValidacaoDem)
		lcBonusTipo          		= Alltrim(fi2.bonusTipo)
		lcBonusId            		= Alltrim(fi2.bonusId)
		lcBonusDescr        		= Alltrim(fi2.bonusDescr)
		lcDescontoHabitual   		= fi2.descontoHabitual
		lnReturnContributedValue  	= fi2.returnContributedValue
		lcDataValidade				= fi2.dataValidade
		lcIdProcesso				= Alltrim(fi2.idProcesso)
		lcIdEpisodio				= Alltrim(fi2.idEpisodio)


		If(Empty(fi2.indisponivel))
			lcIndisponivel = 0
		Else
			lcIndisponivel = 1
		Endif



		If(Empty(lcDataValidade))
			lcDataValidade = "19000101"
		Endif



		If(Vartype(lcDataValidade) == "T"  And Year(lcDataValidade)<2001)
			lcDataValidade = "19000101"
		Endif

		**garantir que a dta est� em formato legivel para o sql
		lcDataValidade  = uf_gerais_getDate(lcDataValidade ,"SQL")


		&& valida se existe registo na  fi, para n�o adicionar lixo

		TEXT TO lcSql NOSHOW TEXTMERGE

			INSERT INTO fi2 (fistamp, ftstamp, u_comb_emb_matrix, valcativa, justificacao_tecnica_cod, justificacao_tecnica_descr, indisponivel, codmotiseimp, motiseimp, isDem,iDValidacaoDem, bonusTipo, bonusId, bonusDescr, descontoHabitual, returnContributedValue,dataValidade, idProcesso, idEpisodio)
			VALUES ('<<ALLTRIM(lcFiStamp)>>','<<ALLTRIM(lcFtStamp)>>','<<ALLTRIM(lcCodUnicoEmb)>>', <<lcValCatIva>>,'<<lcJustTecCod>>','<<lcJustTecDescr>>',<<lcIndisponivel>>,'<<lcCodMotiseImpLin>>', '<<lcMotiseImpLin>>',<<lcIsDem>>,'<<ALLTRIM(lcIdValidacaoDem)>>','<<ALLTRIM(lcBonusTipo)>>','<<ALLTRIM(lcBonusId)>>','<<ALLTRIM(lcBonusDescr)>>',<<lcDescontoHabitual>>,<<lnReturnContributedValue>>,'<<lcDataValidade>>','<<lcIdProcesso>>','<<lcIdEpisodio>>')

		ENDTEXT

		lcSqlTotal  = lcSqlTotal  + Chr(13) + lcSQL

		Select fi2
	Endscan


	Return lcSqlTotal


Endfunc

&&GRAVA LINHA FI2
Function uf_PAGAMENTO_gravarVendaFi2_seg
	Lparameters lcFtstamp,lcFiStamp

	Local lcSqlTotal
	lcSqlTotal  = ''

	&&valida se curosr existe
	If(!Used("sfi2"))
		Return ''
	Endif

	&&valida se o cursor est� preenchido
	If(Reccount("sfi2")==0)
		Return ''
	Endif

	Select sfi2
	Go Top
	Scan  For Alltrim(sfi2.fistamp) == Alltrim(lcFiStamp)
		Local lcFiStamp,lcSQL,lcJustTecCod, lcJustTecDescr

		Store '' To lcJustTecCod,lcJustTecDescr

		lcSQL  = ''
		lcFiStamp = Alltrim(sfi2.fistamp)
		lcCodUnicoEmb  = Alltrim(sfi2.u_comb_emb_matrix)
		lcValCatIva = sfi2.valcativa
		lcJustTecCod  = Alltrim(sfi2.justificacao_tecnica_cod)
		lcJustTecDescr  = Alltrim(sfi2.justificacao_tecnica_descr)

		&& valida se existe registo na  fi, para n�o adicionar lixo

		TEXT TO lcSql NOSHOW TEXTMERGE

			INSERT INTO fi2 (fistamp, ftstamp, u_comb_emb_matrix, valcativa, justificacao_tecnica_cod, justificacao_tecnica_descr) VALUES ('<<ALLTRIM(lcFiStamp)>>','<<ALLTRIM(lcFtStamp)>>','<<ALLTRIM(lcCodUnicoEmb)>>', <<lcValCatIva>>,'<<lcJustTecCod>>','<<lcJustTecDescr>>')

		ENDTEXT

		lcSqlTotal  = lcSqlTotal  + Chr(13) + lcSQL

		Select sfi2
	Endscan

	Return lcSqlTotal

Endfunc



** Gravar dados FI
Function uf_PAGAMENTO_gravarVendaFi
	Lparameters lnFtStamp, lnNmDoc, lnNdoc, lnTipoDoc, lnFno, lnHora, lnCodAcesso, lnBackOffice, lnClNoL, lnClEstabL
	Local lnValida, lcOk, lcCampos, lcValores, lcExecuteSQL, lcCabVendastoken
	Store "" To lcExecuteSQL

	If Type("ATENDIMENTO") != "U"
		If Type("RECRESERVA") != "U"
			Store .T. To lnValida
		Else
			Store .F. To lnValida
		Endif
	Else
		Store .T. To lnValida
	Endif
	Store .T. To lcOk
	Store "" To lcCampos, lcValores


	If Type("ATENDIMENTO") != "U"
		If !Used("uCrsNovosStamps") && cursor para depois actualizar ofistamps das novas vendas
			Create Cursor uCrsNovosStamps (ftstamp c(25), fistamp c(25))
		Endif
	Endif

	If Type("ATENDIMENTO") != "U" And Used("ucrsCabVendas")
		lcCabVendastoken=uCrsCabVendas.token
	Else
		lcCabVendastoken=''
	Endif

	** Verifica se o C�lculo do logitools do FEE=c�lculo retornado da Dispensa_Eletronica_DD

	If !Empty(lcCabVendastoken)
		Select fi
		Go Top
		Scan
			TEXT TO lcSql NOSHOW TEXTMERGE
				select top 1 retorno_fee from Dispensa_Eletronica_DD (nolock) where id='<<ALLTRIM(fi.id_validacaoDem)>>' and ref='<<ALLTRIM(fi.ref)>>'
				and token='<<ALLTRIM(lcCabVendastoken)>>' AND retorno_comp_sns>0
			ENDTEXT
			uf_gerais_actGrelha("", "uCrsVerifFEE",lcSQL)
			Select uCrsVerifFEE
			If Reccount("uCrsVerifFEE") > 0
				Local lcretorno_fee
				lcretorno_fee = uCrsVerifFEE.retorno_fee
				Select fi
				If Round(lcretorno_fee ,2)<>Round(fi.pvp4_fee,2)
					Replace fi.pvp4_fee With uCrsVerifFEE.retorno_fee
				Endif
			Endif
			Select fi
		Endscan
	Endif

	** fim verifica��o

	&& Velifica��o dos pontos stribuidos anteriormente para retificar
	If myTipoCartao == 'Valor'
		**MESSAGEBOX('origem')
		Local lcTmpValcartao, lcTmpTipodoc, lcTmpValcartaoVales,lcValorCartaoSoma
		Store 0 To lcTmpValcartao, lcTmpTipodoc, lcTmpValcartaoVales,lcValorCartaoSoma


		Select fi
		Go Top
		Scan
			If !Empty(fi.ofistamp)
				TEXT TO lcSql NOSHOW TEXTMERGE
					SELECT tipodoc, valcartao FROM fi (nolock) WHERE fistamp='<<ALLTRIM(fi.ofistamp)>>'
				ENDTEXT
				uf_gerais_actGrelha("", "uCrsVerifValCartao",lcSQL)
				lcTmpValcartao = uCrsVerifValCartao.valcartao
				lcTmpTipodoc = uCrsVerifValCartao.tipodoc
				Select fi
				If lcTmpTipodoc = 1
					**	replace fi.valcartao WITH lcTmpValcartao
				Else
					Replace fi.valcartao With lcTmpValcartao * (-1)
				Endif
				fecha("uCrsVerifValCartao")
			Endif
			Select fi
		Endscan
	Endif
	Local lcAdiantamento
	lcAdiantamento = .F.

	**sincroniza Fi com Fi2
	uf_pagamento_sincronizaFiComFi2()

	&& Notas:
	* epvori = epv
	* pvori = pv
	* stipo = 1 ???
	Select fi

	Go Top

	Scan For fi.partes==0
		If lnValida && se j� encontrou venda
			If Left(fi.Design,1) == "." And lnBackOffice == .F.
				Exit && j� precorreu todas as linhas desta venda -> sair do scan
			Else
				Local lcFistampRepetido, lcstampOld
				lcFistampRepetido = .F.
				lcstampOld = ''
				&& calcula sempre o stamp da linha para evitar possibilidade remota de stamps iguais
				lnFiStamp = fi.fistamp

				Local lcPos
				lcPos=Recno("fi")

				If Used("UCRSFIINS")
					Select uCrsFiIns
					Go Top
					Scan
						If Alltrim(lnFiStamp) == Alltrim(uCrsFiIns.fistamp) And !Empty(lnFiStamp) And !Empty(uCrsFiIns.fistamp)
							lcFistampRepetido = .T.
						Endif
					Endscan

					Local lcSqlFi2
					lcSqlFi2  = ''

					If lcFistampRepetido = .T.
						**MESSAGEBOX('altera��o de stamp repetido')
						If(Vartype(lcStampFiRepetidoERRO)!="U")
							lcStampFiRepetidoERRO = .T.
						Endif

						Select fi
						Try
							Go lcPos
						Catch
						Endtry

						If Used("fi")
							lcstampOld = Alltrim(fi.fistamp)
							lcStampNew = uf_gerais_stamp()
							Select fi
							Replace fi.fistamp With Alltrim(lcStampNew)
							lnFiStamp = fi.fistamp

							lcMensagem = Alltrim(ucrse1.id_lt) + ' - Old: ' + Alltrim(lcstampOld )+ ' - New:' + Alltrim(lcStampNew)
							uf_startup_sendmail_errors('Erro Fistamp Repetido', lcMensagem )
							lcDetaLinasRepetidas = .T.
						Endif
					Endif
				Endif

				Select fi
				Try
					Go lcPos
				Catch
				Endtry

				Local lcCodMotiseImpLin , lcMotiseImpLin
				Store '' To lcCodMotiseImpLin , lcMotiseImpLin

				If fi.iva=0
					If Len(Alltrim(fi.codmotiseimp))!=0
						lcCodMotiseImpLin = Alltrim(fi.codmotiseimp)
						lcMotiseImpLin = Alltrim(fi.motiseimp)
					Else
						lcCodMotiseImpLin = Alltrim(ucrse1.codmotiseimp)
						lcMotiseImpLin = Alltrim(ucrse1.motivo_isencao_iva)
					Endif
				Endif


				&& Insert na dbo.Fi2
				lcSqlFi2 = uf_PAGAMENTO_gravarVendaFi2(Alltrim(lnFtStamp), Alltrim(lnFiStamp), Alltrim(lcCodMotiseImpLin), Alltrim(lcMotiseImpLin) )


				Select fi
				If At("Ad. Reserva", Alltrim(fi.Design))>0
					lcAdiantamento = .T.
				Endif

				&& campos rvpstamp e szzstamp s�o utilizados no atendimento para guardar o local de prescri��o e o medico, logo n�o devem ser guardados estes valores (campos)
				TEXT TO lcSql NOSHOW TEXTMERGE
					insert into fi (
						fistamp, nmdoc, fno, ref, design, qtt, tiliquido, etiliquido
						,unidade, unidad2, iva, ivaincl, codigo, tabiva, ndoc, armazem
						,fnoft, ndocft, ftanoft, ftregstamp, lobs2, litem2, litem, lobs3
						,rdata,	cpoc, composto, lrecno, lordem, fmarcada, partes, altura
						,largura, oref, lote, usr1, usr2, usr3,	usr4, usr5
						,ftstamp, cor, tam, stipo, fifref, tipodoc, familia, bistamp
						,stns, ficcusto, fincusto, ofistamp, pv, pvmoeda, epv, tmoeda
						,eaquisicao, custo, ecusto, slvu, eslvu, sltt,esltt,slvumoeda
						,slttmoeda, nccod, epromo, fivendedor, fivendnm, desconto, desc2, desc3
						,desc4, desc5, desc6, u_descval, iectin, eiectin, vbase, evbase
						,tliquido, num1, pcp, epcp, pvori,epvori, zona, morada
						,[local], codpost, telefone, email, tkhposlstamp, u_generico, u_cnp, u_psicont
						,u_bencont, u_psico, u_benzo, u_ettent1, u_ettent2, u_epref, pic, pvpmaxre
						,opcao, u_stock, u_epvp, u_ip, u_comp, u_diploma, ousrinis, ousrdata
						,ousrhora, usrinis, usrdata, usrhora, marcada, u_genalt, u_txcomp, amostra
						,u_refvale, EVALDESC, VALDESC, u_codemb, refentidade, campanhas, id_Dispensa_Eletronica_D
						,pvp4_fee, posologia, valcartao
					)
					Values (
						'<<ALLTRIM(lnFiStamp)>>', '<<lnNmDoc>>', @fno, '<<ALLTRIM(fi.ref)>>', '<<ALLTRIM(fi.design)>>', <<fi.qtt>>, <<fi.tiliquido>>, <<fi.etiliquido>>
						,'<<ALLTRIM(fi.unidade)>>', '<<ALLTRIM(fi.unidad2)>>', <<fi.iva>>, <<IIF(fi.ivaincl,1,0)>>,	'<<ALLTRIM(fi.codigo)>>', <<fi.tabiva>>, <<lnNdoc>>, <<fi.armazem>>
						,<<fi.fnoft>>, <<fi.ndocft>>, <<fi.ftanoft>>, '<<ALLTRIM(fi.ftregstamp)>>', '<<ALLTRIM(fi.lobs2)>>', '<<ALLTRIM(fi.litem2)>>', '<<ALLTRIM(fi.litem)>>', '<<ALLTRIM(fi.lobs3)>>'
						,'<<ALLTRIM(myInvoiceData)>>', <<fi.cpoc>>, <<IIF(fi.composto,1,0)>>, '<<ALLTRIM(fi.lrecno)>>', <<fi.lordem>>, <<IIF(fi.fmarcada,1,0)>>, <<fi.partes>>, <<fi.altura>>
						,<<fi.largura>>, '<<ALLTRIM(fi.oref)>>', '<<ALLTRIM(fi.lote)>>', '<<ALLTRIM(fi.usr1)>>', '<<ALLTRIM(fi.usr2)>>', '<<ALLTRIM(fi.usr3)>>', '<<ALLTRIM(fi.usr4)>>', '<<ALLTRIM(fi.usr5)>>'
						,'<<ALLTRIM(lnFtStamp)>>','<<ALLTRIM(fi.cor)>>', '<<ALLTRIM(fi.tam)>>', 1, '<<ALLTRIM(fi.fifref)>>', <<lnTipoDoc>>, '<<ALLTRIM(fi.familia)>>', '<<ALLTRIM(fi.bistamp)>>'
						,<<IIF(fi.stns,1,0)>>, '<<ALLTRIM(fi.ficcusto)>>', '<<ALLTRIM(fi.fincusto)>>', '<<ALLTRIM(fi.ofistamp)>>', <<fi.pv>>, <<fi.pvmoeda>>, <<fi.epv>>, <<fi.tmoeda>>
						,<<fi.eaquisicao>>, <<fi.custo>>, <<fi.ecusto>>, <<round(fi.pv/(fi.iva/100+1),2)>>, <<Round(fi.epv/(fi.iva/100+1),2)>>,	<<fi.tiliquido/(fi.iva/100+1)>>, <<fi.etiliquido/(fi.iva/100+1)>>, <<fi.slvumoeda>>
						,<<fi.slttmoeda>>, '<<ALLTRIM(fi.nccod)>>', <<IIF(fi.epromo,1,0)>>, <<ft.vendedor>>, '<<ALLTRIM(ft.vendnm)>>', <<fi.desconto>>, <<fi.desc2>>, <<fi.desc3>>
						,<<fi.desc4>>, <<fi.desc5>>, <<fi.desc6>>, <<fi.u_descval>>, <<fi.iectin>>, <<fi.eiectin>>,	<<fi.vbase>>, <<fi.evbase>>
						,<<fi.tliquido>>, <<fi.num1>>, <<fi.pcp>>, <<fi.epcp>>, <<fi.pv>>, <<fi.epv>>, '<<ALLTRIM(fi.zona)>>', '<<ALLTRIM(fi.morada)>>'
						,'<<ALLTRIM(fi.local)>>', '<<ALLTRIM(fi.codpost)>>', '<<ALLTRIM(fi.telefone)>>', '<<ALLTRIM(fi.email)>>', '<<ALLTRIM(fi.tkhposlstamp)>>', <<IIF(fi.u_generico,1,0)>>, '<<ALLTRIM(fi.u_cnp)>>', <<fi.u_psicont>>
						, <<fi.u_bencont>>, <<IIF(fi.u_psico,1,0)>>, <<IIF(fi.u_benzo,1,0)>>, <<fi.u_ettent1>>, <<fi.u_ettent2>>, <<fi.u_epref>>, <<fi.pic>>, <<fi.pvpmaxre>>
						,'<<ALLTRIM(fi.opcao)>>', <<fi.u_stock>>, <<fi.u_epvp>>, <<IIF(fi.u_ip,1,0)>>, <<IIF(fi.u_comp,1,0)>>, '<<ALLTRIM(fi.u_diploma)>>', '<<ALLTRIM(m_chinis)>>', CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
						,'<<ALLTRIM(lnHora)>>', '<<ALLTRIM(m_chinis)>>', CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), '<<ALLTRIM(lnHora)>>', 0, <<IIF(fi.u_genalt,1,0)>>, <<fi.u_txcomp>>, <<IIF(fi.amostra, 1, 0)>>
						,'<<ALLTRIM(fi.u_refvale)>>', <<IIF(fi.qtt == 0,0,(fi.u_descval / fi.qtt))>>, <<IIF(fi.qtt == 0,0,(fi.u_descval / fi.qtt) * 200.482)>>, '<<ALLTRIM(fi.u_codemb)>>', '<<ALLTRIM(fi.refentidade)>>', '<<ALLTRIM(fi.campanhas)>>', '<<ALLTRIM(fi.id_validacaoDem)>>'
						,<<fi.pvp4_fee>>, '<<ALLTRIM(fi.posologia)>>', <<fi.valcartao>>
					)
					IF <<fi.comp_tipo>> != 0 OR <<fi.comp_tipo_2>> != 0
					BEGIN
						INSERT INTO fi_comp (id_fi,comp_tipo,comp,comp_diploma,comp_tipo_2,comp_2,comp_diploma_2) VALUES ('<<ALLTRIM(lnFiStamp)>>',<<fi.comp_tipo>>,<<fi.comp>>,<<fi.comp_diploma>>,<<fi.comp_tipo_2>>,<<fi.comp_2>>,<<fi.comp_diploma_2>>)
					END




				ENDTEXT

				lcExecuteSQL  = lcExecuteSQL + Chr(13) + lcSQL +  Chr(13) +  lcSqlFi2

				** verificar novamente se tem psico nas linhas
				If fi.u_psico = .T.
					lcPsiconaslinhas = .T.
				Endif
				*!*					IF myTipoCartao == 'Valor' AND fi.valcartao < 0
				*!*						lcSql = ''
				*!*						TEXT TO lcSql NOSHOW TEXTMERGE
				*!*							UPDATE b_fidel SET valcartaousado = valcartaousado + (<<fi.valcartao>>*(-1)) WHERE b_fidel.clno=<<ft.no>> and b_fidel.clestab=<<ft.estab>>
				*!*						ENDTEXT
				*!*						lcExecuteSQL  = lcExecuteSQL + CHR(13) + lcSQL
				*!*					ENDIF

				&& atualiza��o do ftstamp nas tabelas da comparticipa��o



				If !Empty(TokenCompart) And Vartype(lnCodAcesso)=='C'

					If Type("stampcompart")<>"U" And Len(Alltrim(stampcompart))>0

						TEXT TO lcSql NOSHOW TEXTMERGE
							UPDATE fi_compart SET ftstamp='<<ALLTRIM(lnFtStamp)>>' WHERE fistamp='<<ALLTRIM(lnFiStamp)>>' AND codacesso='<<ALLTRIM(lnCodacesso)>>' AND ftstamp like '%<<ALLTRIM(stampcompart)>>%' AND LEN(ftstamp)<21 AND ousrdata>DATEADD(ss,-60,getdate())

							UPDATE ft_compart SET ft_compart.ftstamp='<<ALLTRIM(lnFtStamp)>>' WHERE ft_compart.token in (select fi_compart.token from fi_compart WHERE fi_compart.fistamp='<<ALLTRIM(lnFiStamp)>>' AND fi_compart.codacesso='<<ALLTRIM(lnCodacesso)>>') and ft_compart.codacesso='<<ALLTRIM(lnCodacesso)>>' AND ftstamp like '%<<ALLTRIM(stampcompart)>>%' AND LEN(ft_compart.ftstamp)<21 AND ousrdata>DATEADD(ss,-60,getdate())

							UPDATE ft_compart_result SET ft_compart_result.ftstamp = '<<ALLTRIM(lnFtStamp)>>' WHERE ft_compart_result.token in (select top 1 fi_compart.token from fi_compart WHERE fi_compart.fistamp='<<ALLTRIM(lnFiStamp)>>' AND fi_compart.codacesso='<<ALLTRIM(lnCodacesso)>>' order by ousrdata desc) AND ftstamp like '%<<ALLTRIM(stampcompart)>>%' AND LEN(ftstamp)<21 AND ousrdata>DATEADD(ss,-60,getdate())

							UPDATE ft2 SET token_efectivacao_compl=ISNULL((select top 1 ft_compart.token from ft_compart(nolock) WHERE ft_compart.ftstamp='<<ALLTRIM(lnFtStamp)>>' AND type=2),'') WHERE ft2stamp='<<ALLTRIM(lnFtStamp)>>'

						ENDTEXT
					Else

						TEXT TO lcSql NOSHOW TEXTMERGE
							UPDATE fi_compart SET ftstamp='<<ALLTRIM(lnFtStamp)>>' WHERE fistamp='<<ALLTRIM(lnFiStamp)>>' AND codacesso='<<ALLTRIM(lnCodacesso)>>' AND LEN(ftstamp)<21 AND ousrdata>DATEADD(ss,-60,getdate())

							UPDATE ft_compart SET ft_compart.ftstamp='<<ALLTRIM(lnFtStamp)>>' WHERE ft_compart.token in (select fi_compart.token from fi_compart WHERE fi_compart.fistamp='<<ALLTRIM(lnFiStamp)>>' AND fi_compart.codacesso='<<ALLTRIM(lnCodacesso)>>') and ft_compart.codacesso='<<ALLTRIM(lnCodacesso)>>' AND LEN(ft_compart.ftstamp)<21 AND ousrdata>DATEADD(ss,-60,getdate())

							UPDATE ft_compart_result SET ft_compart_result.ftstamp = '<<ALLTRIM(lnFtStamp)>>' WHERE ft_compart_result.token in (select top 1 fi_compart.token from fi_compart WHERE fi_compart.fistamp='<<ALLTRIM(lnFiStamp)>>' AND fi_compart.codacesso='<<ALLTRIM(lnCodacesso)>>' order by ousrdata desc) AND LEN(ftstamp)<21 AND ousrdata>DATEADD(ss,-60,getdate())

							UPDATE ft2 SET token_efectivacao_compl=ISNULL((select top 1 ft_compart.token from ft_compart(nolock) WHERE ft_compart.ftstamp='<<ALLTRIM(lnFtStamp)>>' AND type=2),'') WHERE ft2stamp='<<ALLTRIM(lnFtStamp)>>'

						ENDTEXT
					Endif
					**_cliptext=lcSql
					**uf_perguntalt_chama("Espera aqui","OK","",16)
					lcExecuteSQL  = lcExecuteSQL + Chr(13) + lcSQL
				Endif



				If Type("ATENDIMENTO") != "U" And Type("RECRESERVA")=="U"
					Try
						If Used("uCrsFi")

							&& acrescentar nova linha ao cursor
							Select * From uCrsFi Union All Select * From fi Where Alltrim(fi.fistamp) == Alltrim(lnFiStamp) Into Cursor uCrsFi Readwrite
							Go Bottom
							Replace;
								fistamp		With lnFiStamp;
								nmdoc		With lnNmDoc;
								ndoc		With lnNdoc;
								rdata		With Date();
								ftstamp		With lnFtStamp;
								tipodoc		With lnTipoDoc;
								slvu		With Round(fi.Pv/(fi.iva/100+1),2);
								eslvu		With Round(fi.epv/(fi.iva/100+1),2);
								sltt		With fi.tiliquido/(fi.iva/100+1);
								esltt		With fi.etiliquido/(fi.iva/100+1);
								ousrinis	With m_chinis;
								ousrdata	With Date();
								ousrhora	With lnHora;
								usrinis		With m_chinis;
								usrdata		With Date();
								usrhora		With lnHora;
								marcada		With .F.;
								evaldesc 	With Iif(fi.qtt==0, 0, (fi.u_descval / fi.qtt));
								valdesc		With Iif(fi.qtt==0, 0, ((fi.u_descval / fi.qtt) * 200.482))
						Endif

						If Used("uCrsFi2")

							&& acrescentar nova linha ao cursor
							Select * From uCrsFi2 Union All Select * From fi2 Where Alltrim(fi2.fistamp) == Alltrim(lnFiStamp) Into Cursor uCrsFi2 Readwrite
							Go Bottom
							Replace;
								fistamp		With lnFiStamp;
								ftstamp		With lnFtStamp
						Endif

					Catch
						uf_perguntalt_chama("Foi detectada uma anomalia a guardar dados para impress�o. Por favor verifique o tal�o impresso.", "OK","",64)
					Endtry
				Endif

				If Type("ATENDIMENTO") != "U"
					If !Empty(fi.ofistamp)
						Insert Into uCrsNovosStamps (ftstamp, fistamp) Values (Alltrim(lnFtStamp), Alltrim(lnFiStamp))
					Endif
				Endif
			Endif
		Endif

		If Type("ATENDIMENTO") != "U"
			If Type("RECRESERVA") != "U"
				lnValida = .T.
			Else
				If ((Alltrim(fi.Design)==Alltrim(uCrsCabVendas.Design)) Or (Left(Alltrim(fi.Design),Len(Alltrim(uCrsCabVendas.Design)))==Alltrim(uCrsCabVendas.Design))) And (fi.Lordem==uCrsCabVendas.Lordem)
					lnValida = .T. && encontrou venda
				Endif
			Endif

		Else
			lnValida = .T.
		Endif

		** regista linhas no cursor auxiliar para nai dar erro de fistamp duplicado
		If Used("uCrsFiIns")
			Select uCrsFiIns
			Go Bottom
			Append Blank
			Replace uCrsFiIns.fistamp With Alltrim(fi.fistamp )
		Endif

		Select fi

	Endscan

	If lcAdiantamento = .T.
		If Used("docs_reserva")
			Select docs_reserva
			Go Bottom
			Append Blank
			Replace stampdoc With Alltrim(lnFtStamp)
			Replace tabledoc With 'FT'
			Replace tipodoc With 'Adiantemento'
		Endif
	Endif

	If Type("ATENDIMENTO") != "U"
		Select fi_nnordisk
		Go Top
		Scan

			TEXT TO lcSql NOSHOW TEXTMERGE
				insert into fi_nnordisk (
						fistamp, nrembal, codbar
					)
					Values (
						'<<ALLTRIM(fi_nnordisk.fistamp)>>', '<<ALLTRIM(fi_nnordisk.nrembal)>>', '<<ALLTRIM(fi_nnordisk.codbar)>>'
					)

			ENDTEXT

			lcExecuteSQL  = lcExecuteSQL + Chr(13) + lcSQL
		Endscan
	Endif

	If myTipoCartao == 'Valor'

		** retona utilizador a quem deve ser descontado

		uv_insertValCartao = .T.

		uf_PAGAMENTO_retornaClienteADescontarValorCartao(ft.no, ft.estab, ft.ftstamp)

		TEXT TO uv_queryCartao TEXTMERGE NOSHOW

				UPDATE b_fidel
				SET valcartao = valcartao + ISNULL((select SUM(fi.valcartao) from fi(nolock) WHERE fi.ftstamp='<<ALLTRIM(lnFtStamp)>>'),0)
				, valcartaohist = valcartaohist + ISNULL((select SUM(case when fi.valcartao>0 then fi.valcartao else 0 end) from fi(nolock) WHERE fi.ftstamp='<<ALLTRIM(lnFtStamp)>>'),0)
				, atendimento = atendimento + 1
				, usrdata = GETDATE()
				WHERE b_fidel.clno=<<ucrClNumberCardTemp.no>> and b_fidel.clestab=<<ucrClNumberCardTemp.estab>>
		ENDTEXT

		If uf_gerais_getParameter_site('ADM0000000133', 'BOOL', mySite)

			Select fi
			Calculate Sum(valcartao) To uv_totValcartao

			Select fi
			Calculate Sum(valcartao) For  valcartao > 0 To uv_totValcartaoHist

			Select fi
			Calculate Sum(valcartao) For  valcartao < 0 To uv_totValcartaoUsado

			If uv_totValcartao <> 0

				Set Century On
				uv_curDT = Datetime()

				TEXT TO uv_sql TEXTMERGE NOSHOW

					UPDATE b_fidel
					SET valcartao = valcartao + <<uv_totValcartao>>
					, valcartaohist = valcartaohist + <<uv_totValcartaoHist>>
					, atendimento = atendimento + 1
					, valcartaoUsado = ABS(valcartaoUsado) + ABS(<<uv_totValcartaoUsado>>)
					, usrdata = '<<uv_curDT>>'
					WHERE b_fidel.clno=<<ucrClNumberCardTemp.no>> and b_fidel.clestab=<<ucrClNumberCardTemp.estab>>

				ENDTEXT

				If Used("uc_cartaoCentral")
					fecha("uc_cartaoCentral")
				Endif


				uv_returnQuery = "select * from b_fidel(nolock) where b_fidel.clno= " + astr(ucrClNumberCardTemp.no) + " and b_fidel.clestab= " + astr(ucrClNumberCardTemp.estab)

				If !uf_gerais_runSQLCentral(uv_sql, "uc_cartaoCentral", uv_returnQuery)
					uf_perguntalt_chama("OCORREU UM PROBLEMA A ATUALIZAR O CART�O DE CLIENTE NA CENTRAL." + Chr(13) + "N�O SER�O GUARDADOS OS DADOS DO CART�O DE CLIENTE." + Chr(13) + "POR FAVOR ADICIONE OS PONTOS MANUALMENTE.","OK","",16)

					uv_nrCartao = uf_gerais_getUmValor("b_fidel", "nrCartao", "clno = " + astr(ucrClNumberCardTemp.no) + " and clestab = " + astr(ucrClNumberCardTemp.estab) + " and inactivo = 0")
					TEXT TO uv_dadosCliente TEXTMERGE NOSHOW

						No: <<ASTR(ucrClNumberCardTemp.no)>>;
						Estab: <<ASTR(ucrClNumberCardTemp.estab)>>;
						Cart�o: <<ALLTRIM(uv_nrCartao)>>

					ENDTEXT
					uf_gerais_registaOcorrencia("Pontos Cartao", "Erro a atualizar os pontos do cart�o na central.", 1, astr(uv_totValcartao,14,4), Alltrim(uv_dadosCliente), Iif(!Empty(nrAtendimento), Alltrim(nrAtendimento), Alltrim(lnFtStamp)))

					uv_insertValCartao = .F.
				Endif

				If uv_insertValCartao

					Select uc_cartaoCentral
					Set Century On
					TEXT TO uv_queryCartao TEXTMERGE NOSHOW
						UPDATE b_fidel
						SET valcartao = <<uc_cartaoCentral.valcartao>>
						, valcartaohist = <<uc_cartaoCentral.valcartaohist>>
						, atendimento = <<uc_cartaoCentral.atendimento>>
						, valcartaoUsado = <<uc_cartaoCentral.valcartaoUsado>>
						, usrdata = '<<uv_curDT>>'
						WHERE b_fidel.clno=<<uc_cartaoCentral.clno>> and b_fidel.clestab=<<uc_cartaoCentral.clestab>> AND b_fidel.nrcartao = '<<ALLTRIM(uc_cartaoCentral.nrcartao)>>'
					ENDTEXT

				Endif
			Endif

		Endif

		If uv_insertValCartao

			lcSQL = ''
			TEXT TO lcSql NOSHOW TEXTMERGE
				UPDATE ft2 SET valcartao=ISNULL((select SUM(fi.valcartao) from fi(nolock) WHERE fi.ftstamp='<<ALLTRIM(lnFtStamp)>>' AND fi.valcartao>0),0) WHERE ft2stamp='<<ALLTRIM(lnFtStamp)>>'
				UPDATE ft2 SET valcartaoutilizado=ISNULL((select SUM(fi.valcartao) from fi(nolock) WHERE fi.ftstamp='<<ALLTRIM(lnFtStamp)>>' AND fi.valcartao<0),0) WHERE ft2stamp='<<ALLTRIM(lnFtStamp)>>'

				<<ALLTRIM(uv_queryCartao)>>
			ENDTEXT
			lcExecuteSQL  = lcExecuteSQL + Chr(13) + lcSQL

		Endif


	Endif



	If(Used("ucrClNumberCardTemp"))
		fecha("ucrClNumberCardTemp")
	Endif


	Return lcExecuteSQL

Endfunc

**altera a entidade a retirar pontos
Function uf_PAGAMENTO_retornaClienteADescontarValorCartao
	Lparameters lnNo, lnEstab, lnFtStamp
	Local lcSqlCard,  lnHclstamp, lnHclstamp2

	If(Used("ucrClNumberCardTemp"))
		fecha("ucrClNumberCardTemp")
	Endif

	lnHclstamp  = ''
	lnHclstamp2 =''


	fecha("ucrsFtTempAux")
	fecha("ucrsFt2TempAux")



	If(Used("ft"))

		Local lcPos
		lcPos=Recno("ft")

		Select ft
		Go Top

		Select u_hclstamp From ft Where ftstamp = lnFtStamp Into Cursor ucrsFtTempAux Readwrite
		Select ucrsFtTempAux
		Go Top
		lnHclstamp = Alltrim(ucrsFtTempAux.u_hclstamp)

		Select ft
		Try
			Go lcPos
		Catch
		Endtry

	Endif

	If(Used("ft2"))
		Local lcPos2
		lcPos2=Recno("ft2")

		Select ft2
		Go Top

		Select u_hclstamp1 From ft2 Where ft2stamp = lnFtStamp Into Cursor ucrsFt2TempAux Readwrite
		Select ucrsFt2TempAux
		Go Top
		lnHclstamp2 = Alltrim(ucrsFt2TempAux.u_hclstamp1)

		Select ft2
		Try
			Go lcPos2
		Catch
		Endtry
	Endif






	fecha("ucrsFtTempAux")
	fecha("ucrsFt2TempAux")

	If(Empty(Alltrim(lnHclstamp)) And Empty(Alltrim(lnHclstamp2)))

		Create Cursor ucrClNumberCardTemp (no numeric(10,0), estab numeric(3,0))

		Insert Into ucrClNumberCardTemp(no, estab) Values (lnNo, lnEstab)
	Else
		If(!Empty(Alltrim(lnHclstamp)))
			TEXT TO lcSqlCard NOSHOW TEXTMERGE
				select
					top 1
					no,
					estab
				from
					b_utentes (nolock)
				where
					b_utentes.utstamp = '<<ALLTRIM(lnHclstamp)>>'
			ENDTEXT
		Else
			TEXT TO lcSqlCard NOSHOW TEXTMERGE
				select
					top 1
					no,
					estab
				from
					b_utentes (nolock)
				where
					b_utentes.utstamp =  '<<ALLTRIM(lnHclstamp2)>>'
			ENDTEXT
		Endif


		uf_gerais_actGrelha("",[ucrClNumberCardTemp],lcSqlCard)

		If(Reccount("ucrClNumberCardTemp") == 0)
			Insert Into ucrClNumberCardTemp(no, estab) Values (lnNo, lnEstab)
		Endif
	Endif

	If(Used("ucrClNumberCardTemp"))
		Select ucrClNumberCardTemp
		Go Top
	Endif



Endfunc


**


** Gravar dados FI
Function uf_PAGAMENTO_gravarVendaFi_Seg
	Lparameters lnFtStamp, lnNmDoc, lnNdoc, lnTipoDoc, lnFno, lnHora, lnCodAcesso, lnBackOffice, lnClNoL, lnClEstabL
	Local lnValida, lcOk, lcCampos, lcValores, lcExecuteSQL, lcCabVendastoken
	Store "" To lcExecuteSQL

	Select sfi
	Go Top
	Scan
		&& calcula sempre o stamp da linha para evitar possibilidade remota de stamps iguais
		lnFiStamp = sfi.fistamp

		Local lcSqlFi2
		lcSqlFi2  = ''

		&& Insert na dbo.Fi2
		lcSqlFi2 = uf_PAGAMENTO_gravarVendaFi2_seg(Alltrim(lnFtStamp), Alltrim(lnFiStamp) )


		Select fi
		&& campos rvpstamp e szzstamp s�o utilizados no atendimento para guardar o local de prescri��o e o medico, logo n�o devem ser guardados estes valores (campos)
		TEXT TO lcSql NOSHOW TEXTMERGE
					insert into fi (
						fistamp, nmdoc, fno, ref, design, qtt, tiliquido, etiliquido
						,unidade, unidad2, iva, ivaincl, codigo, tabiva, ndoc, armazem
						,fnoft, ndocft, ftanoft, ftregstamp, lobs2, litem2, litem, lobs3
						,rdata,	cpoc, composto, lrecno, lordem, fmarcada, partes, altura
						,largura, oref, lote, usr1, usr2, usr3,	usr4, usr5
						,ftstamp, cor, tam, stipo, fifref, tipodoc, familia, bistamp
						,stns, ficcusto, fincusto, ofistamp, pv, pvmoeda, epv, tmoeda
						,eaquisicao, custo, ecusto, slvu, eslvu, sltt,esltt,slvumoeda
						,slttmoeda, nccod, epromo, fivendedor, fivendnm, desconto, desc2, desc3
						,desc4, desc5, desc6, u_descval, iectin, eiectin, vbase, evbase
						,tliquido, num1, pcp, epcp, pvori,epvori, zona, morada
						,[local], codpost, telefone, email, tkhposlstamp, u_generico, u_cnp, u_psicont
						,u_bencont, u_psico, u_benzo, u_ettent1, u_ettent2, u_epref, pic, pvpmaxre
						,opcao, u_stock, u_epvp, u_ip, u_comp, u_diploma, ousrinis, ousrdata
						,ousrhora, usrinis, usrdata, usrhora, marcada, u_genalt, u_txcomp, amostra
						,u_refvale, EVALDESC, VALDESC, u_codemb, refentidade, campanhas, id_Dispensa_Eletronica_D
						,pvp4_fee, posologia, valcartao
					)
					Values (
						'<<ALLTRIM(lnFiStamp)>>', '<<lnNmDoc>>', @fno, '<<ALLTRIM(sfi.ref)>>', '<<ALLTRIM(sfi.design)>>', <<sfi.qtt>>, <<sfi.tiliquido>>, <<sfi.etiliquido>>
						,'<<ALLTRIM(sfi.unidade)>>', '<<ALLTRIM(sfi.unidad2)>>', <<sfi.iva>>, <<IIF(sfi.ivaincl,1,0)>>,	'<<ALLTRIM(sfi.codigo)>>', <<sfi.tabiva>>, <<lnNdoc>>, <<sfi.armazem>>
						,<<sfi.fnoft>>, <<sfi.ndocft>>, <<sfi.ftanoft>>, '<<ALLTRIM(sfi.ftregstamp)>>', '<<ALLTRIM(sfi.lobs2)>>', '<<ALLTRIM(sfi.litem2)>>', '<<ALLTRIM(sfi.litem)>>', '<<ALLTRIM(sfi.lobs3)>>'
						,'<<ALLTRIM(myInvoiceData)>>', <<sfi.cpoc>>, <<IIF(sfi.composto,1,0)>>, '<<ALLTRIM(sfi.lrecno)>>', <<sfi.lordem>>, <<IIF(sfi.fmarcada,1,0)>>, <<sfi.partes>>, <<sfi.altura>>
						,<<sfi.largura>>, '<<ALLTRIM(sfi.oref)>>', '<<ALLTRIM(sfi.lote)>>', '<<ALLTRIM(sfi.usr1)>>', '<<ALLTRIM(sfi.usr2)>>', '<<ALLTRIM(sfi.usr3)>>', '<<ALLTRIM(sfi.usr4)>>', '<<ALLTRIM(sfi.usr5)>>'
						,'<<ALLTRIM(lnFtStamp)>>','<<ALLTRIM(sfi.cor)>>', '<<ALLTRIM(sfi.tam)>>', 1, '<<ALLTRIM(sfi.fifref)>>', <<lnTipoDoc>>, '<<ALLTRIM(sfi.familia)>>', '<<ALLTRIM(sfi.bistamp)>>'
						,<<IIF(sfi.stns,1,0)>>, '<<ALLTRIM(sfi.ficcusto)>>', '<<ALLTRIM(sfi.fincusto)>>', '<<ALLTRIM(sfi.ofistamp)>>', <<sfi.pv>>, <<sfi.pvmoeda>>, <<sfi.epv>>, <<sfi.tmoeda>>
						,<<sfi.eaquisicao>>, <<sfi.custo>>, <<sfi.ecusto>>, <<round(sfi.pv/(sfi.iva/100+1),2)>>, <<Round(sfi.epv/(sfi.iva/100+1),2)>>,	<<sfi.tiliquido/(sfi.iva/100+1)>>, <<sfi.etiliquido/(sfi.iva/100+1)>>, <<sfi.slvumoeda>>
						,<<sfi.slttmoeda>>, '<<ALLTRIM(sfi.nccod)>>', <<IIF(sfi.epromo,1,0)>>, <<ft.vendedor>>, '<<ALLTRIM(ft.vendnm)>>', <<sfi.desconto>>, <<sfi.desc2>>, <<sfi.desc3>>
						,<<sfi.desc4>>, <<sfi.desc5>>, <<sfi.desc6>>, <<sfi.u_descval>>, <<sfi.iectin>>, <<sfi.eiectin>>,	<<sfi.vbase>>, <<sfi.evbase>>
						,<<sfi.tliquido>>, <<sfi.num1>>, <<sfi.pcp>>, <<sfi.epcp>>, <<sfi.pv>>, <<sfi.epv>>, '<<ALLTRIM(sfi.zona)>>', '<<ALLTRIM(sfi.morada)>>'
						,'<<ALLTRIM(sfi.local)>>', '<<ALLTRIM(sfi.codpost)>>', '<<ALLTRIM(sfi.telefone)>>', '<<ALLTRIM(sfi.email)>>', '<<ALLTRIM(sfi.tkhposlstamp)>>', <<IIF(sfi.u_generico,1,0)>>, '<<ALLTRIM(sfi.u_cnp)>>', <<sfi.u_psicont>>
						, <<sfi.u_bencont>>, <<IIF(sfi.u_psico,1,0)>>, <<IIF(sfi.u_benzo,1,0)>>, <<sfi.u_ettent1>>, <<sfi.u_ettent2>>, <<sfi.u_epref>>, <<sfi.pic>>, <<sfi.pvpmaxre>>
						,'<<ALLTRIM(sfi.opcao)>>', <<sfi.u_stock>>, <<sfi.u_epvp>>, <<IIF(sfi.u_ip,1,0)>>, <<IIF(sfi.u_comp,1,0)>>, '<<ALLTRIM(sfi.u_diploma)>>', '<<ALLTRIM(m_chinis)>>', CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
						,'<<ALLTRIM(lnHora)>>', '<<ALLTRIM(m_chinis)>>', CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), '<<ALLTRIM(lnHora)>>', 0, <<IIF(sfi.u_genalt,1,0)>>, <<sfi.u_txcomp>>, <<IIF(sfi.amostra, 1, 0)>>
						,'<<ALLTRIM(sfi.u_refvale)>>', <<IIF(sfi.qtt == 0,0,(sfi.u_descval / sfi.qtt))>>, <<IIF(sfi.qtt == 0,0,(sfi.u_descval / sfi.qtt) * 200.482)>>, '<<ALLTRIM(sfi.u_codemb)>>', '<<ALLTRIM(sfi.refentidade)>>', '<<ALLTRIM(sfi.campanhas)>>', '<<ALLTRIM(sfi.id_validacaoDem)>>'
						,<<sfi.pvp4_fee>>, '<<ALLTRIM(sfi.posologia)>>', <<sfi.valcartao>>
					)
					IF <<sfi.comp_tipo>> != 0 OR <<sfi.comp_tipo_2>> != 0
					BEGIN
						INSERT INTO fi_comp (id_fi,comp_tipo,comp,comp_diploma,comp_tipo_2,comp_2,comp_diploma_2) VALUES ('<<ALLTRIM(lnFiStamp)>>',<<sfi.comp_tipo>>,<<comp>>,<<sfi.comp_diploma>>,<<sfi.comp_tipo_2>>,<<sfi.comp_2>>,<<sfi.comp_diploma_2>>)
					END




		ENDTEXT

		lcExecuteSQL  = lcExecuteSQL + Chr(13) + lcSQL +  Chr(13) +  lcSqlFi2

	Endscan

	Return lcExecuteSQL

Endfunc



** Gravar Dados de Psicotr�picos
Function uf_PAGAMENTO_gravarDadosPsico
	Lparameters lnFtStamp, lcOrdem, lcRetornaSQL


	Local lcOk, lcSQL
	Store .F. To lcOk


	Select dadosPsico
	If Type("ATENDIMENTO") != "U"
		Locate For dadosPsico.cabVendasOrdem = lcOrdem
	Else
		Go Top
	Endif

	If Used("uCrsatendCl")

		Select uCrsatendCl

		uv_nascimentoAD = uCrsatendCl.u_dcutavi

	Else

		If Used("dadospsico")
			uv_nascimentoAD = dadosPsico.nascimento
		Else
			uv_nascimentoAD = ''
		Endif

	Endif
	If lcRetornaSQL == .T.
		Select dadosPsico
		lcSQL = ''

		TEXT TO lcSQL NOSHOW TEXTMERGE

				   if(select COUNT(*) from B_dadosPsico(nolock) where ftstamp = '<<ALLTRIM(lnFtStamp)>>')=0
				   BEGIN
						insert into B_dadosPsico (
							ftstamp,
							u_recdata, u_medico,
							u_nmutdisp, u_moutdisp, u_cputdisp,
							u_nmutavi, u_moutavi, u_cputavi,
							u_ndutavi, u_ddutavi, u_idutavi,
							u_dirtec, nascimento, codigoDocSPMS, contactoCli
						)
						Values (
							'<<ALLTRIM(lnFtStamp)>>',
							'<<IIF(empty(dadosPsico.u_recdata),'19000101',uf_gerais_getDate(dadosPsico.u_recdata,"SQL"))>>', '<<ALLTRIM(dadosPsico.u_medico)>>',
							'<<ALLTRIM(dadospsico.u_nmutdisp)>>','<<ALLTRIM(dadospsico.u_moutdisp)>>', '<<ALLTRIM(dadospsico.u_cputdisp)>>',
							'<<ALLTRIM(dadospsico.u_nmutavi)>>', '<<ALLTRIM(dadospsico.u_moutavi)>>', '<<ALLTRIM(dadosPsico.u_cputavi)>>',
							'<<ALLTRIM(dadospsico.u_ndutavi)>>', '<<IIF(empty(dadosPsico.u_ddutavi),'19000101',uf_gerais_getDate(dadosPsico.u_ddutavi,"SQL"))>>', <<dadospsico.u_idutavi>>,
							'<<ALLTRIM(uCrsE1.u_dirtec)>>', '<<IIF(empty(uv_nascimentoAD),'19000101',uf_gerais_getDate(uv_nascimentoAD,"SQL"))>>', '<<ALLTRIM(dadosPsico.codigoDocSPMS)>>',
                            '<<ALLTRIM(dadospsico.contactoCli)>>'
							)
					end
		ENDTEXT

		Return lcSQL
	Else
		If uf_pagamento_existedadospsico(Alltrim(lnFtStamp))=.F.
			Select dadosPsico
			lcSQL = ''
			If !Empty(dadosPsico.u_nmutdisp)

				TEXT TO lcSQL noshow textmerge
					if(select COUNT(*) from B_dadosPsico(nolock) where ftstamp = '<<ALLTRIM(lnFtStamp)>>')=0
					begin
						insert into B_dadosPsico (
							ftstamp,
							u_recdata, u_medico,
							u_nmutdisp, u_moutdisp, u_cputdisp,
							u_nmutavi, u_moutavi, u_cputavi,
							u_ndutavi, u_ddutavi, u_idutavi,
							u_dirtec, nascimento, codigoDocSPMS, contactoCli
						)
						Values (
							'<<lnFtStamp>>',
							'<<IIF(empty(dadosPsico.u_recdata),'19000101',uf_gerais_getDate(dadosPsico.u_recdata,"SQL"))>>', '<<ALLTRIM(dadosPsico.u_medico)>>',
							'<<ALLTRIM(dadospsico.u_nmutdisp)>>', '<<ALLTRIM(dadospsico.u_moutdisp)>>', '<<ALLTRIM(dadospsico.u_cputdisp)>>',
							'<<ALLTRIM(dadospsico.u_nmutavi)>>', '<<ALLTRIM(dadospsico.u_moutavi)>>', '<<ALLTRIM(dadosPsico.u_cputavi)>>',
							'<<ALLTRIM(dadospsico.u_ndutavi)>>', '<<IIF(empty(dadosPsico.u_ddutavi),'19000101',uf_gerais_getDate(dadosPsico.u_ddutavi,"SQL"))>>', <<dadospsico.u_idutavi>>,
							'<<ALLTRIM(uCrsE1.u_dirtec)>>', '<<IIF(empty(uv_nascimentoAD),'19000101',uf_gerais_getDate(uv_nascimentoAD,"SQL"))>>', '<<ALLTRIM(dadosPsico.codigoDocSPMS)>>',
                            '<<ALLTRIM(dadosPsico.contactoCli)>>'
							)
					end
				ENDTEXT


			Else

				lcSQL = ''

				If(Used("uCrsCabVendas") And Used("uCrsatendCl") )
					Select uCrsatendCl
					Select uCrsCabVendas

					TEXT TO lcSQL noshow textmerge
						if(select COUNT(*) from B_dadosPsico(nolock) where ftstamp = '<<ALLTRIM(lnFtStamp)>>')=0
						begin
							insert into B_dadosPsico (
								ftstamp,
								u_recdata, u_medico,
								u_nmutdisp, u_moutdisp, u_cputdisp,
								u_nmutavi, u_moutavi, u_cputavi,
								u_ndutavi, u_ddutavi, u_idutavi,
								u_dirtec, nascimento, codigoDocSPMS, contactoCli
							)
							Values (
								'<<lnFtStamp>>',
								'<<IIF(empty(uCrsCabVendas.datareceita),'19000101',uf_gerais_getDate(uCrsCabVendas.datareceita,"SQL"))>>', '<<ALLTRIM(uCrsCabVendas.nopresc)>>',
								'<<ALLTRIM(uCrsatendCl.nome)>>', '<<ALLTRIM(uCrsatendCl.morada)>>', '<<ALLTRIM(uCrsatendCl.codpost)>>',
								'<<ALLTRIM(uCrsatendCl.u_nmutavi)>>', '<<ALLTRIM(uCrsatendCl.u_moutavi)>>', '<<ALLTRIM(uCrsatendCl.u_cputavi)>>',
								'<<ALLTRIM(uCrsatendCl.u_ndutavi)>>', '<<IIF(empty(uCrsatendCl.u_ddutavi),'19000101',uf_gerais_getDate(uCrsatendCl.u_ddutavi,"SQL"))>>', <<uCrsatendCl.u_idutavi>>,
								'<<ALLTRIM(uCrsE1.u_dirtec)>>', '<<IIF(empty(uv_nascimentoAD),'19000101',uf_gerais_getDate(uv_nascimentoAD,"SQL"))>>', '<<IIF(!EMPTY(ucrsAtendCl.codDocID), ucrsAtendCl.codDocID, ucrsAtendCl.codDocIDUT)>>',
                                '<<ALLTRIM(uCrsatendCl.telefone)>>'
								)
						end
					ENDTEXT

				Endif
			Endif



			If !uf_gerais_actGrelha("","",lcSQL)
				uf_perguntalt_chama("OCORREU UMA ANOMALIA AO GRAVAR A INFORMA��O DOS PSICOTR�PICOS. POR FAVOR CONTACTE O SUPORTE. OBRIGADO.","OK","",16)
				lcOk = .F.
			Else
				lcOk = .T.
			Endif
		Else
			lcOk = .T.
		Endif

		Return lcOk
	Endif

Endfunc


**
Function uf_PAGAMENTO_criarDocRec
	Lparameters tcBool
	Local lcGravacomSucesso
	Store .F. To lcGravacomSucesso

	Local lcExecuteSQL, lcSqlRecibado, lcSqlRe, lcSQLRlAux, lcSQLRl
	Store "" To lcExecuteSQL, lcSqlRecibado, lcSqlRe, lcSQLRlAux, lcSQLRl

	Local lcFtstamp, lcReStamp, lcPdata, lcNmDoc, lcRmDoc, lcHora, lcValStamp1, lcRdata
	Store "" To lcFtstamp, lcReStamp, lcNmDoc, lcRmDoc, lcHora, lcValStamp1

	Local lcClNo, lcClEstab, lcClNome, lcTipoDoc, lcNdoc, lcFno, lcRdoc
	Store 0 To lcClNo, lcClEstab, lcClNome, lcTipoDoc, lcNdoc, lcFno, lcRdoc

	Local lcTotQtt, lcQtt1, lcEtotal, lcEttIliq, lcEcusto, lcEttIva, lcEivaIn1, lcEivaIn2, lcEivaIn3, lcEivaIn4, lcEivaIn5, lcEivaIn6, lcEivaIn7, lcEivaIn8, lcEivaIn9, lcEivaIn10, lcEivaIn11, lcEivaIn12, lcEivaIn13, lcEivaV1, lcEivaV2, lcEivaV3, lcEivaV4, lcEivaV5, lcEivaV6, lcEivaV7, lcEivaV8, lcEivaV9, lcEivaV10, lcEivaV11, lcEivaV12, lcEivaV13
	Store 0 To lcTotQtt, lcQtt1, lcEtotal, lcEttIliq, lcEcusto, lcEttIva, lcEivaIn1, lcEivaIn2, lcEivaIn3, lcEivaIn4, lcEivaIn5, lcEivaIn6, lcEivaIn7, lcEivaIn8, lcEivaIn9,lcEivaIn10,lcEivaIn11,lcEivaIn12, lcEivaIn13, lcEivaV1, lcEivaV2, lcEivaV3, lcEivaV4, lcEivaV5, lcEivaV6, lcEivaV7, lcEivaV8, lcEivaV9, lcEivaV10, lcEivaV11, lcEivaV12 ,lcEivaV13

	Local lcEval, lcTotCdesc, lcTotCdescPerc, lcTotCdescValor
	Store 0 To lcEval, lcTotCdesc, lcTotCdescPerc, lcTotCdescValor

	&& DATA DO VENCIMENTO
	lcPdata=Datetime()+(difhoraria*3600)
	**lcPdata=datetime()
	**

	&& TIPO E N� DO DOCUMENTO A GRAVAR
	If !uf_gerais_getParameter("ADM0000000308","BOOL")
		If myOffline
			lcRdoc	= 4
			lcRmDoc	= "Normal Offline"
		Else
			lcRdoc	= 1
			lcRmDoc	= "Normal"
		Endif
	Else
		TEXT TO lcSQL NOSHOW TEXTMERGE
			select nmdoc, ndoc from tsre where site='<<ALLTRIM(mySite)>>' and left(nmdoc,4)='Rec.'
		ENDTEXT
		uf_gerais_actGrelha("","uCrsConfRec",lcSQL)
		Select uCrsConfRec
		lcRdoc	= uCrsConfRec.ndoc
		lcRmDoc	= Alltrim(uCrsConfRec.nmdoc)
		fecha("uCrsConfRec")
	Endif
	**

	&& Criar Stamp para Recibo
	lcReStamp = uf_gerais_stamp()

	&& Cria Cursor para Posteriormente Permitir a Impress�o dos Tal�es
	If !Used("uCrsImpTalaoPos")
		Create Cursor uCrsImpTalaoPos (stamp c(26), tipo i(4), Lordem i(8), Susp l, AdiReserva l, nrreceita c(26) ,rm l, planocomp l, impcomp1 l, impcomp2 l, lote c(5), impresso l , plano c(5))
	Endif

	Select uCrsImpTalaoPos
	Append Blank
	Replace uCrsImpTalaoPos.stamp		With Alltrim(lcReStamp)
	If myOffline
		Replace uCrsImpTalaoPos.tipo	With 904
	Else
		Replace uCrsImpTalaoPos.tipo	With 901
	Endif
	Replace uCrsImpTalaoPos.Lordem		With 99999
	Replace uCrsImpTalaoPos.Susp		With .F.
	**

	&& GUARDAR DATA e HORA DO SISTEMA
	&&::TODO
	*!*		SET HOURS TO 24
	*!*		Atime=ttoc(datetime()+(difhoraria*3600),2)
	*!*		Astr=left(Atime,8)
	*!*		lcHora = Astr
	lcHora			= Time() && � necess�rio remover os segundos durante a inser��o na tabela do campo saida
	TEXT TO lcSQL TEXTMERGE NOSHOW
		select dateadd(HOUR, <<difhoraria>>, getdate()) data
	ENDTEXT
	uf_gerais_actGrelha("","uCrsMyInvoiceData",lcSQL)
	Select uCrsMyInvoiceData
	myInvoiceData = uf_gerais_getDate(uCrsMyInvoiceData.Data,"SQL")
	fecha("uCrsMyInvoiceData")
	**

	&& CALCULAR TOTAIS Cabe�alho
	Select uCrsFi2Reg
	Go Top
	Scan

		Select ucrsvale
		Go Top
		Scan For (ucrsvale.fistamp==uCrsFi2Reg.fistamp) Or (ucrsvale.factPago=.T. And ucrsvale.fistamp==uCrsFi2Reg.ofistamp)
			lcTotCdesc = lcTotCdesc + ucrsvale.totalCdesc
			Exit
		Endscan

		Select uCrsFi2Reg
		lcTotQtt	= lcTotQtt	+ uCrsFi2Reg.qtt
		lcQtt1		= lcQtt1	+ uCrsFi2Reg.qtt
		lcEtotal	= lcEtotal	+ uCrsFi2Reg.etiliquido
		lcEttIliq	= lcEttIliq	+ (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1))
		lcEcusto	= lcEcusto	+ uCrsFi2Reg.ecusto

		If uCrsFi2Reg.tabiva=1
			lcEivaIn1	= lcEivaIn1 + (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1))
			lcEivaV1	= lcEivaV1 + uCrsFi2Reg.etiliquido - (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1))
		Else
			If uCrsFi2Reg.tabiva=2
				lcEivaIn2	= lcEivaIn2	+ (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1))
				lcEivaV2	= lcEivaV2 + uCrsFi2Reg.etiliquido - (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1))
			Else
				If uCrsFi2Reg.tabiva=3
					lcEivaIn3	= lcEivaIn3	+ (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1))
					lcEivaV3	= lcEivaV3 + uCrsFi2Reg.etiliquido - (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1))
				Else
					If uCrsFi2Reg.tabiva=4
						lcEivaIn4	= lcEivaIn4	+ (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1))
						lcEivaV4	= lcEivaV4 + uCrsFi2Reg.etiliquido - (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1))
					Else
						If uCrsFi2Reg.tabiva=5
							lcEivaIn5	= lcEivaIn5	+ (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1))
							lcEivaV5	= lcEivaV5 + uCrsFi2Reg.etiliquido - (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1))
						Else
							If uCrsFi2Reg.tabiva=6
								lcEivaIn6	= lcEivaIn6	+ (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1))
								lcEivaV6	= lcEivaV6 + uCrsFi2Reg.etiliquido - (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1))
							Else
								If uCrsFi2Reg.tabiva=7
									lcEivaIn7	= lcEivaIn7	+ (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1))
									lcEivaV7	= lcEivaV7 + uCrsFi2Reg.etiliquido - (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1))
								Else
									If uCrsFi2Reg.tabiva=8
										lcEivaIn8	= lcEivaIn8	+ (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1))
										lcEivaV8	= lcEivaV8 + uCrsFi2Reg.etiliquido - (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1))
									Else
										If uCrsFi2Reg.tabiva=9
											lcEivaIn9	= lcEivaIn9	+ (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1))
											lcEivaV9	= lcEivaV9 + uCrsFi2Reg.etiliquido - (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1))
										Else
											If uCrsFi2Reg.tabiva=10
												lcEivaIn10	= lcEivaIn10 + (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1))
												lcEivaV10	= lcEivaV10  + uCrsFi2Reg.etiliquido - (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1))
											Else
												If  uCrsFi2Reg.tabiva=11
													lcEivaIn11	= lcEivaIn11 + (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1))
													lcEivaV11	= lcEivaV11  + uCrsFi2Reg.etiliquido - (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1))
												Else
													If  uCrsFi2Reg.tabiva=12
														lcEivaIn12	= lcEivaIn12 + (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1))
														lcEivaV12	= lcEivaV12  + uCrsFi2Reg.etiliquido - (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1))
													Else
														If  uCrsFi2Reg.tabiva=13
															lcEivaIn13	= lcEivaIn13 + (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1))
															lcEivaV13	= lcEivaV13  + uCrsFi2Reg.etiliquido - (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1))
														Endif
													Endif
												Endif
											Endif

										Endif
									Endif
								Endif
							Endif
						Endif
					Endif
				Endif
			Endif
		Endif

		lcFno = uCrsFi2Reg.fno
		Select uCrsFi2Reg
	Endscan
	**

	&& CALCULAR O DESCONTO CASO EXISTA
	If !tcBool
		If Round(lcTotCdesc,2) != Round(lcEtotal,2)
			lcTotCdescPerc = ((lcTotCdesc/lcEtotal * 100) - 100) * (-1)
			lcTotCdescValor = lcEtotal - lcTotCdesc
		Endif
	Endif
	**

	&& GRAVAR CABE�ALHO DO RECIBO
	If !tcBool
		**	uf_perguntalt_chama("WAIT 1","OK","",16)
		If myOffline
			lcSqlRe = uf_PAGAMENTO_GravarRegRe(4, lcReStamp, lcRmDoc, lcRdoc, lcFno, Round(lcTotCdesc,2), lcHora, Round(lcTotCdescPerc,2), Round(lcTotCdescValor,2), .F., .F.)
		Else
			lcSqlRe = uf_PAGAMENTO_GravarRegRe(1, lcReStamp, lcRmDoc, lcRdoc, lcFno, Round(lcTotCdesc,2), lcHora, Round(lcTotCdescPerc,2), Round(lcTotCdescValor,2), .F., .F.)
		Endif
	Else
		**	uf_perguntalt_chama("WAIT 2","OK","",16)
		If myOffline
			lcSqlRe = uf_PAGAMENTO_GravarRegRe(4, lcReStamp, lcRmDoc, lcRdoc, lcFno, Round(lcTotCdesc,2), lcHora, 0, 0, .T., .T.)
		Else
			lcSqlRe = uf_PAGAMENTO_GravarRegRe(1, lcReStamp, lcRmDoc, lcRdoc, lcFno, Round(lcTotCdesc,2), lcHora, 0, 0, .T., .T.)
		Endif
	Endif

	Store 0 To lcTotQtt, lcQtt1, lcEtotal, lcEttIliq, lcEcusto, lcEivaIn1, lcEivaIn2, lcEivaIn3, lcEivaIn4, lcEivaIn5, lcEivaIn6, lcEivaIn7, lcEivaIn8, lcEivaIn9, lcEivaV1, lcEivaV2, lcEivaV3, lcEivaV4, lcEivaV5, lcEivaV6, lcEivaV7, lcEivaV8, lcEivaV9, lcEivaV10, lcEivaV11,lcEivaV12,lcEivaV13, lcEttIva
	Store 0 To lcTotCdesc, lcTotCdescPerc, lcTotCdescValor
	**

	&& CALCULAR TOTAIS Linhas
	Select uCrsFi2Reg
	&& nota:
	* 1 = Factura
	* 88 = Factura Importada
	**SELECT distinct ftstamp FROM uCrsFi2Reg WHERE ndoc=1 OR ndoc=88 or ndoc=myFact INTO CURSOR uCrsFi2RegStamps READWRITE && alterado para contemplarar series de factura��o dinamicas
	Select Distinct ftstamp From uCrsFi2Reg Where Alltrim(nmdoc)='Factura' Or Alltrim(nmdoc)='Factura Importada' Or ndoc=myFact Into Cursor uCrsFi2RegStamps Readwrite && alterado para contemplarar series de factura��o dinamicas

	Local lcEtiliquidoNovo
	lcEtiliquidoNovo = 0.00

	**	uf_perguntalt_chama("Espera aqui","OK","",16)

	Select uCrsFi2RegStamps
	Go Top
	Scan
		Select uCrsFi2Reg
		Go Top
		Scan For Alltrim(uCrsFi2Reg.ftstamp)==Alltrim(uCrsFi2RegStamps.ftstamp)

			Select fi
			Go Top
			Scan For uCrsFi2Reg.fistamp == fi.fistamp && condi��o para determinal o total por regularizar (rl.eval)
				lcEtiliquidoNovo = fi.etiliquido
				Exit
			Endscan

			Select uCrsFi2Reg
			lcTotQtt	= lcTotQtt	+ uCrsFi2Reg.qtt
			lcQtt1		= lcQtt1	+ uCrsFi2Reg.qtt
			lcEtotal	= lcEtotal	+ uCrsFi2Reg.etiliquido
			lcEttIliq	= lcEttIliq	+ (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1))

			If lcEtiliquidoNovo < uCrsFi2Reg.etiliquido
				lcEval = lcEval + uCrsFi2Reg.etiliquido
			Else
				lcEval = lcEval + lcEtiliquidoNovo
			Endif

			lcEcusto	= lcEcusto	+ uCrsFi2Reg.ecusto

			Do Case
				Case uCrsFi2Reg.tabiva == 1
					lcEivaIn1	= Round(lcEivaIn1 + (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1)),2)
					lcEivaV1	= Round(lcEivaV1 + uCrsFi2Reg.etiliquido - (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1)),2)
				Case uCrsFi2Reg.tabiva == 2
					lcEivaIn2	= Round(lcEivaIn2	+ (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1)),2)
					lcEivaV2	= Round(lcEivaV2 + uCrsFi2Reg.etiliquido - (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1)),2)
				Case uCrsFi2Reg.tabiva == 3
					lcEivaIn3	= Round(lcEivaIn3	+ (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1)),2)
					lcEivaV3	= Round(lcEivaV3 + uCrsFi2Reg.etiliquido - (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1)),2)
				Case uCrsFi2Reg.tabiva == 4
					lcEivaIn4	= Round(lcEivaIn4	+ (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1)),2)
					lcEivaV4	= Round(lcEivaV4 + uCrsFi2Reg.etiliquido - (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1)),2)
				Case uCrsFi2Reg.tabiva == 5
					lcEivaIn5	= Round(lcEivaIn5	+ (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1)),2)
					lcEivaV5	= Round(lcEivaV5 + uCrsFi2Reg.etiliquido - (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1)),2)
				Case uCrsFi2Reg.tabiva == 6
					lcEivaIn6	= Round(lcEivaIn6	+ (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1)),2)
					lcEivaV6 	= Round(lcEivaV6 + uCrsFi2Reg.etiliquido - (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1)),2)
				Case uCrsFi2Reg.tabiva == 7
					lcEivaIn7	= Round(lcEivaIn7	+ (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1)),2)
					lcEivaV7 	= Round(lcEivaV7 + uCrsFi2Reg.etiliquido - (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1)),2)
				Case uCrsFi2Reg.tabiva == 8
					lcEivaIn8	= Round(lcEivaIn8	+ (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1)),2)
					lcEivaV8 	= Round(lcEivaV8 + uCrsFi2Reg.etiliquido - (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1)),2)
				Case uCrsFi2Reg.tabiva == 9
					lcEivaIn9	= Round(lcEivaIn9	+ (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1)),2)
					lcEivaV9 	= Round(lcEivaV9 + uCrsFi2Reg.etiliquido - (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1)),2)
				Case uCrsFi2Reg.tabiva == 10
					lcEivaIn10	= Round(lcEivaIn10	+ (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1)),2)
					lcEivaV10 	= Round(lcEivaV10 + uCrsFi2Reg.etiliquido - (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1)),2)
				Case uCrsFi2Reg.tabiva == 11
					lcEivaIn11	= Round(lcEivaIn11	+ (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1)),2)
					lcEivaV11 	= Round(lcEivaV11 + uCrsFi2Reg.etiliquido - (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1)),2)
				Case uCrsFi2Reg.tabiva == 12
					lcEivaIn12	= Round(lcEivaIn12	+ (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1)),2)
					lcEivaV12 	= Round(lcEivaV12 + uCrsFi2Reg.etiliquido - (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1)),2)
				Case uCrsFi2Reg.tabiva == 13
					lcEivaIn13	= Round(lcEivaIn13	+ (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1)),2)
					lcEivaV13 	= Round(lcEivaV13 + uCrsFi2Reg.etiliquido - (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1)),2)

			Endcase

			If Empty(uCrsFi2Reg.ofistamp) Or myRemoveCompart=.F.
				&& marcar produto como recibado
				TEXT to lcSqlRec noshow textmerge
					update fi set fmarcada=1, eaquisicao=eaquisicao+<<uCrsFi2Reg.qtt>>
					where fistamp='<<alltrim(uCrsFi2Reg.fistamp)>>'
				ENDTEXT
				lcSqlRecibado = lcSqlRecibado + Chr(13) + lcSqlRec
				**

				lcValStamp1 = uCrsFi2Reg.ftstamp
				lcFno 		= uCrsFi2Reg.fno
				lcRdata		= uf_gerais_getDate(uCrsFi2Reg.rdata,"SQL")
			Else
				TEXT to lcSqlRec noshow textmerge
					update fi set fmarcada=1, eaquisicao=eaquisicao+<<uCrsFi2Reg.qtt>>
					where fistamp='<<alltrim(uCrsFi2Reg.ofistamp)>>'
				ENDTEXT
				lcSqlRecibado = lcSqlRecibado + Chr(13) + lcSqlRec

				lcSQL = ''
				TEXT TO lcSQL NOSHOW TEXTMERGE
					select ftstamp, fno from fi where fistamp='<<ALLTRIM(uCrsFi2Reg.ofistamp)>>'
				ENDTEXT

				If !uf_gerais_actGrelha("","CurDocOrigem",lcSQL)
					uf_perguntalt_chama("OCORREU UM ERRO A OBTER OD DADOS DO DOCUMENTO ORIGEM. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
					Return .F.
				Endif

				lcValStamp1 = CurDocOrigem.ftstamp
				lcFno 		= CurDocOrigem.fno
				Select uCrsFi2Reg
				lcRdata		= uf_gerais_getDate(uCrsFi2Reg.rdata,"SQL")
			Endif

			Select uCrsFi2Reg
		Endscan


		&& GRAVAR LINHAS DO RECIBO
		If !Empty(lcValStamp1)
			If myOffline
				lcSQLRlAux = uf_PAGAMENTO_GravarRegRl(4, lcReStamp, lcValStamp1, lcRdoc, lcNrRecibo, lcFno, Round(lcEtotal,2), Round(lcEivaV1,2), Round(lcEivaV2,2), Round(lcEivaV3,2), Round(lcEivaV4,2), Round(lcEivaV5,2), Round(lcEivaV6,2), Round(lcEivaV7,2), Round(lcEivaV8,2), Round(lcEivaV9,2), lcHora, lcRdata, Round(lcEval,2), Round(lcEivaV10,2), Round(lcEivaV11,2),Round(lcEivaV12,2),Round(lcEivaV13,2))
			Else
				lcSQLRlAux = uf_PAGAMENTO_GravarRegRl(1, lcReStamp, lcValStamp1, lcRdoc, lcNrRecibo, lcFno, Round(lcEtotal,2), Round(lcEivaV1,2), Round(lcEivaV2,2), Round(lcEivaV3,2), Round(lcEivaV4,2), Round(lcEivaV5,2), Round(lcEivaV6,2), Round(lcEivaV7,2), Round(lcEivaV8,2), Round(lcEivaV9,2), lcHora, lcRdata, Round(lcEval,2), Round(lcEivaV10,2), Round(lcEivaV11,2),Round(lcEivaV12,2),Round(lcEivaV13,2))
			Endif

			lcSQLRl = lcSQLRl + Chr(13) + lcSQLRlAux

			Store 0 To lcTotQtt, lcQtt1, lcEtotal, lcEttIliq, lcEcusto, lcEval, lcEivaIn1, lcEivaIn2, lcEivaIn3, lcEivaIn4, lcEivaIn5, lcEivaIn6, lcEivaIn7, lcEivaIn8, lcEivaIn9, lcEivaIn10, lcEivaIn11, lcEivaIn12, lcEivaIn13
			Store 0 To lcEivaV1, lcEivaV2, lcEivaV3, lcEivaV4, lcEivaV5, lcEivaV6, lcEivaV7, lcEivaV8, lcEivaV9,lcEivaV10,lcEivaV11,lcEivaV12,lcEivaV13, lcEttIva, lcFno
			Store "" To lcValStamp1
		Endif
		**

		Select uCrsFi2RegStamps
	Endscan

	If Used("uCrsFi2RegStamps")
		fecha("uCrsFi2RegStamps")
	Endif
	**

	&& GUARDAR PRODUTOS INCLU�DOS NO RECIBO EM CURSOR
	If !Used("uCrsFi2Lreg")
		Select * From uCrsFi2Reg Into Cursor uCrsFi2Lreg Readwrite
	Else
		Select uCrsFi2Lreg
		Append From Dbf("uCrsFi2Reg")
		Delete For Empty(uCrsFi2Lreg.ref)
	Endif
	**

	** Gravar na BD
	lcSqlRe = uf_gerais_trataPlicasSQL(lcSqlRe)
	lcSqlRecibado = uf_gerais_trataPlicasSQL(lcSqlRecibado)
	lcSQLRl = uf_gerais_trataPlicasSQL(lcSQLRl)

	lcSQLInsertRe = lcSqlRe + lcSqlRecibado + lcSQLRl

	If !Empty(lcSQLInsertRe)

		lcSQL = ''
		TEXT TO lcSQL NOSHOW TEXTMERGE
			exec up_gerais_execSql 'At. - RE', 1,'<<lcSQLInsertRe>>', '', '', '' , '', ''
		ENDTEXT

		If !uf_gerais_actGrelha("","",lcSQL)
			uf_perguntalt_chama("OCORREU UM ERRO A GRAVAR O DOCUMENTO DE REGULARIZA��O. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			Return .F.
		Endif

		uf_gravarCert(lcReStamp, uf_gerais_getUmValor("re", "rno", "restamp = '" + lcReStamp + "'"), lcRdoc, lcRdata, lcRdata, lcHora, uf_gerais_getUmValor("re", "etotal", "restamp = '" + lcReStamp + "'"), 'RE')

		lcSQL = ''
		TEXT TO lcSQL NOSHOW TEXTMERGE
			select * from re (nolock) where reano=year(getdate())
			and etotal=(select etotal from re ree (nolock) where ree.rno=re.rno-1 and ree.nmdoc=re.nmdoc and ree.reano=re.reano)
			and no=(select no from re ree (nolock) where ree.rno=re.rno-1 and ree.nmdoc=re.nmdoc  and ree.reano=re.reano)
			and desc1=(select desc1 from re ree (nolock) where ree.rno=re.rno-1 and ree.nmdoc=re.nmdoc  and ree.reano=re.reano)
			and etotal<>0
			and desc1<>''
			and re.restamp='<<ALLTRIM(lcReStamp)>>'
		ENDTEXT

		If !uf_gerais_actGrelha("","ucrsRecAntRepetido",lcSQL)
			uf_perguntalt_chama("OCORREU UM ERRO A VERIFICAR O RECIBO ANTERIOR. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			Return .F.
		Else
			Select ucrsRecAntRepetido
			If Reccount("ucrsRecAntRepetido")>0
				lcSQL = ''
				TEXT TO lcSQL NOSHOW TEXTMERGE
					DELETE FROM re WHERE restamp='<<ALLTRIM(lcReStamp)>>'
					DELETE FROM rl WHERE restamp='<<ALLTRIM(lcReStamp)>>'
					DELETE FROM b_cert WHERE stamp='<<ALLTRIM(lcReStamp)>>'
				ENDTEXT

				If !uf_gerais_actGrelha("","",lcSQL)
					uf_perguntalt_chama("OCORREU UM ERRO A ANULAR O RECIBO ANTERIOR. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
					Return .F.
				Endif

				Select uCrsImpTalaoPos
				Delete From uCrsImpTalaoPos Where Alltrim(uCrsImpTalaoPos.stamp) = Alltrim(lcReStamp)

				Local lcMensagem
				lcMensagem = Alltrim(ucrse1.id_lt) + ' - ' + Alltrim(ucrse1.nomecomp) + ' - ' + Alltrim(m_chinis) + ' - Recibo duplicado removido '
				uf_startup_sendmail_errors('Erro Recibo Repetido', lcMensagem )
			Endif
		Endif

		fecha("ucrsRecAntRepetido")

	Endif

Endfunc

Function uf_pagamento_sincronizaFiComFi2



	If(Used("fi2"))
		Select fi
		Go Top
		Update  fi2 Set fi2.isDem = fi.dem, fi2.iDValidacaoDem = fi.id_validacaoDem From fi2 INNER Join fi On  Alltrim(fi2.fistamp)= Alltrim(fi.fistamp)
	Endif


Endfunc



******************************
* GRAVAR CABE�ALHO DO RECIBO *
******************************
Function uf_PAGAMENTO_GravarRegRe
	Lparameters lnTipoRec, lnReStamp, lnRmDoc, lnRdoc, lnFno, lnEtotal, lnHora, lnDescPerc, lnDescValor, lcNregCaixa, lcBackoff, lcOlCodigo, lcOlLocal, lcContado

	Local lcExecuteSQL
	Store "" To lcExecuteSQL

	If !Used("uCrsActAcerto") && cursor para depois actualizar acerto na multivenda
		Create Cursor uCrsActAcerto (ftstamp c(28), restamp c(28))
	Endif

	If !(Type("lcOlLocal") == "C")
		lcOlLocal = "Caixa"
	Endif

	Local lnOlcodigo, lnDesc1, lcOk, lcTeLocal, lcTipoTsre
	Store "" To lnOlcodigo, lnDesc1, lcTeLocal
	Store .T. To lcOk

	If Upper(Alltrim(lcOlLocal)) == "CAIXA"
		lcTeLocal = 'C'
	Else
		lcTeLocal = 'B'
	Endif

	lcTipoTsre  = 0
	lcTipoTsre  = uf_pagamento_getTsre(lnRdoc)

	If lnTipoRec=3 Or lnTipoRec=5
		lnOlcodigo	= 'R00002'
		If lcTipoTsre == 2 &&Vale de Reembolso
			lnDesc1		= 'N/Nt. Cr�dito no. ' + astr(lnFno) + '.'
		Endif
	Else
		lnOlcodigo	= 'R00001'
		If lcTipoTsre == 2  &&Vale de Reembolso
			lnDesc1		='N/Factura no. ' + astr(lnFno) + '.'
		Endif
	Endif

	If lcNregCaixa
		lnOlcodigo	= 'R00003'
	Endif

	If Type("lcOlCodigo") == "C"
		lnOlcodigo = lcOlCodigo
	Endif

	&& Preencher a conta de tesouraria que est� na configura��o do recibo - altera��o em 20190117 para poder escolher as conta da tesouraria a integrar
	**IF EMPTY(lcContado) && Default de recebimento
	**	lcContado = 99
	**ENDIF
	uf_gerais_actGrelha("",[uCrsContado],[select contado from tsre (nolock) where ndoc=]+Str(lnRdoc))
	lcContado = uCrsContado.contado
	fecha("uCrsContado")

	&& N� DO DOCUMENTO
	**IF UPPER(ALLTRIM(uCrse1.id_lt)) == 'E01322A' AND REGVENDAS.pgfReg.page2.chkRecEnt.tag = "true"
	**	uf_gerais_actGrelha("",[uCrsMaxRno],[select case when max(re.rno) is null then 0 else max(re.rno) end as maxrno from re (nolock) where re.ndoc=]+Str(lnRdoc))
	**ELSE
	**	**uf_gerais_actGrelha("",[uCrsMaxRno],[select case when max(re.rno) is null then 0 else max(re.rno) end as maxrno from re (nolock) where reano=YEAR(']+myInvoiceData+[') and re.ndoc=]+Str(lnRdoc))
	**	uf_gerais_actGrelha("",[uCrsMaxRno],[select case when max(re.rno) is null then 0 else max(re.rno) end as maxrno from re (nolock) where (reano between Year(']+myInvoiceData+[')-]+IIF(uf_gerais_getParameter_site('ADM0000000142', 'BOOL', mySite),'0','1')+[ AND Year(']+myInvoiceData+[')) and re.ndoc=]+Str(lnRdoc))
	**ENDIF
	lcNrRecibo = uf_gerais_newDocNr("RE", lnRdoc, .F.) &&uCrsMaxRno.maxrno+1
	**fecha("uCrsMaxRno")
	****************************

	TEXT to lcSql noshow textmerge

		DECLARE @rno as numeric(9,0)
		DECLARE @tabNewDocNr TABLE (newDocNr NUMERIC(9,0))

		SET @rno = 0
		DELETE FROM @tabNewDocNr

		INSERT INTO @tabNewDocNr (newDocNr)
		EXEC up_get_newDocNr 'RE', <<lnRdoc>>, '<<mySite>>', 1

		SET @rno = (SELECT newDocNr FROM @tabNewDocNr)

		Insert into re (
			restamp, nmdoc, rno, ndoc,
			rdata, reano,
			nome, [no], morada, [local], codpost, ncont, estab,
			etotal, evdinheiro, moeda2, ccusto,
			cheque, echtotal, modop1, epaga1, modop2, epaga2,
			olcodigo, telocal, moeda,
			desc1,
			process, procdata, vdata, ollocal,
			vendedor, vendnm, tipo, pais, memissao,
			efinv, finv, fin,
			[site], pnome, pno, cxstamp, cxusername, ssstamp, ssusername,
			ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora,
			u_nratend, u_backoff, contado, u_tipodoc
		)
		values (
			'<<lnReStamp>>', '<<lnRmDoc>>', @rno, <<lnRdoc>>,
			'<<myInvoiceData>>', YEAR('<<myInvoiceData>>'),
			'<<uCrsClReg.nome>>', <<uCrsClReg.no>>, '<<alltrim(uCrsClReg.morada)>>', '<<alltrim(uCrsClReg.local)>>', '<<alltrim(uCrsClReg.codpost)>>', '<<alltrim(uCrsClReg.ncont)>>', <<uCrsClReg.estab>>,
			<<lnEtotal>>, <<ft2.evdinheiro>>, '<<ALLTRIM(ft.moeda)>>', '<<uCrsClReg.ccusto>>',
			<<Iif(ft.cheque,1,0)>>, <<ft.echtotal>>, 'VISA', <<ft2.epaga1>>, 'MultiBanco', <<ft2.epaga2>>,
			'<<lnOlcodigo>>', '<<lcTeLocal>>', '<<ALLTRIM(ft.moeda)>>',
			LEFT('<<lnDesc1>>',250),
			1, '<<myInvoiceData>>', CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), '<<lcOlLocal>>',
			'<<ch_vendedor>>','<<alltrim(ch_vendnm)>>', 'Dinheiro', <<ft.pais>>, 'EURO',
			<<lnDescValor>>, <<lnDescValor*200.482>>, <<lnDescPerc>>,
			'<<alltrim(mySite)>>', '<<alltrim(myTerm)>>', <<myTermNo>>, '<<alltrim(myCxStamp)>>', '<<alltrim(myCxUser)>>', '<<alltrim(mySsStamp)>>', '<<alltrim(ch_vendnm)>>',
			'<<m_chinis>>', CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), '<<lnHora>>', '<<m_chinis>>', CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), '<<lnHora>>',
			'<<nrAtendimento>>', <<Iif(lcBackoff,1,0)>>, <<lcContado>>, <<lcTipoTsre>>
		)
	ENDTEXT


	lcExecuteSQL  = lcExecuteSQL + Chr(13) + lcSQL

	Insert Into uCrsActAcerto (ftstamp, restamp) Values ('', Alltrim(lnReStamp))

	**	IF lnTipoRec <> 6
	**		uf_gravarCert(lnReStamp, lcNrRecibo, lnRdoc, myInvoiceData, myInvoiceData, lnHora, lnEtotal, 'RE')
	**	ENDIF

	Return lcExecuteSQL
Endfunc


***************************
* GRAVAR LINHAS DO RECIBO *
***************************
Function uf_PAGAMENTO_GravarRegRl
	Lparameters lnTipoRec, lnReStamp, lnFtStamp, lnRdoc, lnRno, lnFno, lnEtotal, lnEivaV1, lnEivaV2, lnEivaV3, lnEivaV4, lnEivaV5, lnEivaV6, lnEivaV7, lnEivaV8, lnEivaV9, lnHora, lnRdata, lnEVal, lnEivaV10,lnEivaV11,lnEivaV12, lnEivaV13

	Local lcEtotal, lcEivaV1, lcEivaV2, lcEivaV3, lcEivaV4, lcEivaV5, lcEivaV6, lcEivaV7, lcEivaV8, lcEivaV9, lcEivaV10, lcEivaV11, lcEivaV12, lcEivaV13, lcCredF
	Store 0 To lcEtotal, lcEivaV1, lcEivaV2, lcEivaV3, lcEivaV4, lcEivaV5, lcEivaV6, lcEivaV7, lcEivaV8, lcEivaV9, lcEivaV10, lcEivaV11, lcEivaV12, lcEivaV13, lcCredF


	Local lcExecuteSQL
	Store "" To lcExecuteSQL

	Local lnRlStamp, lnCdesc, lnCm, lcLordem, lcOk, i
	lnCdesc= ""
	Store 0 To lnCm, lcLordem, i
	Store .T. To lcOk


	TEXT TO lcSQL TEXTMERGE NOSHOW
		select
			nmdoc
			, ndoc
			, (select cmcc from td (nolock) where ft.ndoc=td.ndoc) as td
			, etotal, eivav1, eivav2, eivav3, eivav4, eivav5, eivav6, eivav7, eivav8, eivav9, eivav10, eivav11, eivav12, eivav13
			, (select edebf from cc (nolock) where cc.ftstamp=ft.ftstamp) as edebf
			, (select ecredf from cc (nolock) where cc.ftstamp=ft.ftstamp) as ecredf
		from ft
		where ftstamp='<<ALLTRIM(lnFtStamp)>>'
	ENDTEXT
	uf_gerais_actGrelha("","uCrsConfFT",lcSQL)
	If Reccount("uCrsConfFT")>0
		lnCdesc		= uCrsConfFT.nmdoc
		lnCm		= uCrsConfFT.td
	Else
		fecha("uCrsConfFT")
		TEXT TO lcSQL TEXTMERGE NOSHOW
			select
				cmdesc as nmdoc
				, nrdoc as ndoc
				, isnull((select cmcc from td (nolock) where cc.nrdoc=td.ndoc),case when cmdesc='N/Nt. Cr�dito' then 57 else 1 end) as td
				, (case when edeb>0 then edeb else ecred end) as etotal, eivav1, eivav2, eivav3, eivav4, eivav5, eivav6, eivav7, eivav8, eivav9, eivav10, eivav11, eivav12, eivav13
				,  edebf
				,  ecredf
			from cc (nolock)
			where ccstamp='<<ALLTRIM(lnFtStamp)>>'
		ENDTEXT
		uf_gerais_actGrelha("","uCrsConfFT",lcSQL)
		If Reccount("uCrsConfFT")>0
			lnCdesc		= uCrsConfFT.nmdoc
			lnCm		= uCrsConfFT.td
		Endif
	Endif

	Local lcTotSup
	Store .F. To lcTotSup




	If uCrsConfFT.td<50
		lcLordem=200

		If lnEtotal > uCrsConfFT.etotal - uCrsConfFT.edebf

			lcEtotal = uCrsConfFT.etotal - uCrsConfFT.edebf
			lcCredF = uCrsConfFT.ecredf
			lcEivaV1 = uCrsConfFT.EivaV1  * ((((uCrsConfFT.etotal - uCrsConfFT.edebf )*100)/uCrsConfFT.etotal)/100)
			lcEivaV2 = uCrsConfFT.EivaV2  * ((((uCrsConfFT.etotal - uCrsConfFT.edebf )*100)/uCrsConfFT.etotal)/100)
			lcEivaV3 = uCrsConfFT.EivaV3  * ((((uCrsConfFT.etotal - uCrsConfFT.edebf )*100)/uCrsConfFT.etotal)/100)
			lcEivaV4 = uCrsConfFT.EivaV4  * ((((uCrsConfFT.etotal - uCrsConfFT.edebf )*100)/uCrsConfFT.etotal)/100)
			lcEivaV5 = uCrsConfFT.EivaV5  * ((((uCrsConfFT.etotal - uCrsConfFT.edebf )*100)/uCrsConfFT.etotal)/100)
			lcEivaV6 = uCrsConfFT.EivaV6  * ((((uCrsConfFT.etotal - uCrsConfFT.edebf )*100)/uCrsConfFT.etotal)/100)
			lcEivaV7 = uCrsConfFT.EivaV7  * ((((uCrsConfFT.etotal - uCrsConfFT.edebf )*100)/uCrsConfFT.etotal)/100)
			lcEivaV8 = uCrsConfFT.EivaV8  * ((((uCrsConfFT.etotal - uCrsConfFT.edebf )*100)/uCrsConfFT.etotal)/100)
			lcEivaV9 = uCrsConfFT.EivaV9  * ((((uCrsConfFT.etotal - uCrsConfFT.edebf )*100)/uCrsConfFT.etotal)/100)
			lcEivaV10 = uCrsConfFT.EivaV10  * ((((uCrsConfFT.etotal - uCrsConfFT.edebf )*100)/uCrsConfFT.etotal)/100)
			lcEivaV11 = uCrsConfFT.EivaV11  * ((((uCrsConfFT.etotal - uCrsConfFT.edebf )*100)/uCrsConfFT.etotal)/100)
			lcEivaV12 = uCrsConfFT.EivaV12  * ((((uCrsConfFT.etotal - uCrsConfFT.edebf )*100)/uCrsConfFT.etotal)/100)
			lcEivaV13 = uCrsConfFT.EivaV13  * ((((uCrsConfFT.etotal - uCrsConfFT.edebf )*100)/uCrsConfFT.etotal)/100)

			lcTotSup = .T.
		Endif
	Else
		lcLordem=100


		If lnEtotal < uCrsConfFT.etotal - uCrsConfFT.edebf

			lcEtotal = uCrsConfFT.etotal - uCrsConfFT.edebf
			lcCredF = uCrsConfFT.ecredf
			lcEivaV1 = uCrsConfFT.EivaV1  * ((((uCrsConfFT.etotal - uCrsConfFT.edebf )*100)/uCrsConfFT.etotal)/100)
			lcEivaV2 = uCrsConfFT.EivaV2  * ((((uCrsConfFT.etotal - uCrsConfFT.edebf )*100)/uCrsConfFT.etotal)/100)
			lcEivaV3 = uCrsConfFT.EivaV3  * ((((uCrsConfFT.etotal - uCrsConfFT.edebf )*100)/uCrsConfFT.etotal)/100)
			lcEivaV4 = uCrsConfFT.EivaV4  * ((((uCrsConfFT.etotal - uCrsConfFT.edebf )*100)/uCrsConfFT.etotal)/100)
			lcEivaV5 = uCrsConfFT.EivaV5  * ((((uCrsConfFT.etotal - uCrsConfFT.edebf )*100)/uCrsConfFT.etotal)/100)
			lcEivaV6 = uCrsConfFT.EivaV6  * ((((uCrsConfFT.etotal - uCrsConfFT.edebf )*100)/uCrsConfFT.etotal)/100)
			lcEivaV7 = uCrsConfFT.EivaV7  * ((((uCrsConfFT.etotal - uCrsConfFT.edebf )*100)/uCrsConfFT.etotal)/100)
			lcEivaV8 = uCrsConfFT.EivaV8  * ((((uCrsConfFT.etotal - uCrsConfFT.edebf )*100)/uCrsConfFT.etotal)/100)
			lcEivaV9 = uCrsConfFT.EivaV9  * ((((uCrsConfFT.etotal - uCrsConfFT.edebf )*100)/uCrsConfFT.etotal)/100)
			lcEivaV10 = uCrsConfFT.EivaV10  * ((((uCrsConfFT.etotal - uCrsConfFT.edebf )*100)/uCrsConfFT.etotal)/100)
			lcEivaV11 = uCrsConfFT.EivaV11  * ((((uCrsConfFT.etotal - uCrsConfFT.edebf )*100)/uCrsConfFT.etotal)/100)
			lcEivaV12 = uCrsConfFT.EivaV12  * ((((uCrsConfFT.etotal - uCrsConfFT.edebf )*100)/uCrsConfFT.etotal)/100)
			lcEivaV13 = uCrsConfFT.EivaV13  * ((((uCrsConfFT.etotal - uCrsConfFT.edebf )*100)/uCrsConfFT.etotal)/100)
			lcTotSup = .T.
		Endif
	Endif







	If Used("uCrsConfFT")
		fecha("uCrsConfFT")
	Endif

	*!*		If lnTipoRec==3 OR lnTipoRec==5
	*!*			lnCdesc		= 'N/Nt. Credito'
	*!*			lnCm		= 57
	*!*		Else
	*!*			lnCdesc		= 'N/Factura'
	*!*			lnCm		= 1
	*!*		EndIf

	*!*		If (lnCdesc=='N/Factura')
	*!*			lcLordem=200
	*!*		Else
	*!*			lcLordem=100
	*!*		EndIf

	If Type("lnRdata") != "C"
		lnRdata = myInvoiceData
	Endif


	** corre��o dos acertos de c/c
	If Alltrim(lnCdesc)='N/Nt. Cr�dito' And lnFno=0 And lnCm=57


		lnRlStamp = uf_gerais_stamp()
		If lcTotSup = .F.
			TEXT to lcSql noshow textmerge
				Insert into rl (
					restamp, rlstamp, ndoc, rno, cdesc, nrdoc,
					datalc, dataven,
					ccstamp,
					cm, eval, erec,
					process, moeda, rdata,
					eivav1, eivav2, eivav3, eivav4, eivav5, eivav6, eivav7, eivav8, eivav9, lordem,
					eivavori1, eivavori2, eivavori3, eivavori4, eivavori5, eivavori6, eivavori7, eivavori8, eivavori9,
					enaval, evori, moedoc,
					ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora
				)
				values (
					'<<lnReStamp>>', '<<lnRlStamp>>', <<lnRdoc>>, @rno, '<<lnCdesc>>', '<<lnFno>>',
					'<<lnRdata>>', '<<myInvoiceData>>',
					'<<lnFtStamp>>',
					<<lnCm>>, -<<lnEVal>>, -<<lnEtotal>>,
					1, '<<ALLTRIM(ft.moeda)>>', '<<myInvoiceData>>',
					<<lnEivaV1>>, <<lnEivaV2>>, <<lnEivaV3>>, <<lnEivaV4>>, <<lnEivaV5>>, <<lnEivaV6>>, <<lnEivaV7>>, <<lnEivaV8>>, <<lnEivaV9>>, <<lcLordem>>,
					<<lnEivaV1>>, <<lnEivaV2>>, <<lnEivaV3>>, <<lnEivaV4>>, <<lnEivaV5>>, <<lnEivaV6>>, <<lnEivaV7>>, <<lnEivaV8>>, <<lnEivaV9>>,
					<<lnEtotal>>, <<lnEtotal>>, 'PTE ou EURO',
					'<<m_chinis>>', CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), '<<lnHora>>', '<<m_chinis>>', CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), '<<lnHora>>'
				)
			ENDTEXT
		Else


			If lnFno=0

				TEXT TO lcSql TEXTMERGE NOSHOW
					Insert into rl (
						restamp, rlstamp, ndoc, rno, cdesc, nrdoc,
						datalc, dataven,
						ccstamp,
						cm, eval, erec,
						process, moeda, rdata,
						eivav1, eivav2, eivav3, eivav4, eivav5, eivav6, eivav7, eivav8, eivav9, eivav10, eivav11, eivav12, eivav13, lordem,
						eivavori1, eivavori2, eivavori3, eivavori4, eivavori5, eivavori6, eivavori7, eivavori8, eivavori9, eivavori10, eivavori11,eivavori12, eivavori13,
						enaval, evori, moedoc,
						ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora
					)
					values (
						'<<lnReStamp>>', '<<lnRlStamp>>', <<lnRdoc>>, @rno, '<<lnCdesc>>', '<<lnFno>>',
						'<<lnRdata>>', '<<myInvoiceData>>',
						'<<lnFtStamp>>',
						<<lnCm>>, <<((ABS(lcEtotal)*-1) - (ABS(lcCredF)*-1)) - lnEVal>>, <<lnEVal>>,
						1, '<<ALLTRIM(ft.moeda)>>', '<<myInvoiceData>>',
						<<lcEivaV1>>, <<lcEivaV2>>, <<lcEivaV3>>, <<lcEivaV4>>, <<lcEivaV5>>, <<lcEivaV6>>, <<lcEivaV7>>, <<lcEivaV8>>, <<lcEivaV9>>, <<lcEivaV10>>, <<lcEivaV11>>, <<lcEivaV12>>, <<lcEivaV13>>, <<lcLordem>>,
						<<lcEivaV1>>, <<lcEivaV2>>, <<lcEivaV3>>, <<lcEivaV4>>, <<lcEivaV5>>, <<lcEivaV6>>, <<lcEivaV7>>, <<lcEivaV8>>, <<lcEivaV9>>, <<lcEivaV10>>, <<lcEivaV11>>, <<lcEivaV12>>, <<lcEivaV13>>,
						<<lnEVal>>, <<((ABS(lcEtotal)*-1) - (ABS(lcCredF)*-1))>>, 'PTE ou EURO',
						'<<m_chinis>>', CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), '<<lnHora>>', '<<m_chinis>>', CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), '<<lnHora>>'
					)
				ENDTEXT

			Else

				TEXT to lcSql noshow textmerge
					Insert into rl (
						restamp, rlstamp, ndoc, rno, cdesc, nrdoc,
						datalc, dataven,
						ccstamp,
						cm, eval, erec,
						process, moeda, rdata,
						eivav1, eivav2, eivav3, eivav4, eivav5, eivav6, eivav7, eivav8, eivav9, eivav10, eivav11, eivav12, eivav13, lordem,
						eivavori1, eivavori2, eivavori3, eivavori4, eivavori5, eivavori6, eivavori7, eivavori8, eivavori9, eivavori10, eivavori11, eivavori12, eivavori13,
						enaval, evori, moedoc,
						ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora
					)
					values (
						'<<lnReStamp>>', '<<lnRlStamp>>', <<lnRdoc>>, @rno, '<<lnCdesc>>', '<<lnFno>>',
						'<<lnRdata>>', '<<myInvoiceData>>',
						'<<lnFtStamp>>',
						<<lnCm>>, -<<lcEtotal>>, -<<lcEtotal>>,
						1, '<<ALLTRIM(ft.moeda)>>', '<<myInvoiceData>>',
						<<lcEivaV1>>, <<lcEivaV2>>, <<lcEivaV3>>, <<lcEivaV4>>, <<lcEivaV5>>, <<lcEivaV6>>, <<lcEivaV7>>, <<lcEivaV8>>, <<lcEivaV9>>, <<lcEivaV10>>, <<lcEivaV11>>, <<lcEivaV12>>, <<lcEivaV13>>, <<lcLordem>>,
						<<lcEivaV1>>, <<lcEivaV2>>, <<lcEivaV3>>, <<lcEivaV4>>, <<lcEivaV5>>, <<lcEivaV6>>, <<lcEivaV7>>, <<lcEivaV8>>, <<lcEivaV9>>, <<lcEivaV10>>, <<lcEivaV11>>, <<lcEivaV12>>, <<lcEivaV13>>,
						<<lcEtotal>>, <<lcEtotal>>, 'PTE ou EURO',
						'<<m_chinis>>', CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), '<<lnHora>>', '<<m_chinis>>', CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), '<<lnHora>>'
					)
				ENDTEXT

			Endif
		Endif
	Else

		lnRlStamp = uf_gerais_stamp()
		If lcTotSup = .F.
			TEXT to lcSql noshow textmerge
				Insert into rl (
					restamp, rlstamp, ndoc, rno, cdesc, nrdoc,
					datalc, dataven,
					ccstamp,
					cm, eval, erec,
					process, moeda, rdata,
					eivav1, eivav2, eivav3, eivav4, eivav5, eivav6, eivav7, eivav8, eivav9, eivav10, eivav11, eivav12, eivav13,  lordem,
					eivavori1, eivavori2, eivavori3, eivavori4, eivavori5, eivavori6, eivavori7, eivavori8, eivavori9, eivavori10, eivavori11, eivavori12, eivavori13,
					enaval, evori, moedoc,
					ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora
				)
				values (
					'<<lnReStamp>>', '<<lnRlStamp>>', <<lnRdoc>>, @rno, '<<lnCdesc>>', '<<lnFno>>',
					'<<lnRdata>>', '<<myInvoiceData>>',
					'<<lnFtStamp>>',
					<<lnCm>>, <<lnEVal>>, <<lnEtotal>>,
					1, '<<ALLTRIM(ft.moeda)>>', '<<myInvoiceData>>',
					<<lnEivaV1>>, <<lnEivaV2>>, <<lnEivaV3>>, <<lnEivaV4>>, <<lnEivaV5>>, <<lnEivaV6>>, <<lnEivaV7>>, <<lnEivaV8>>, <<lnEivaV9>>, <<lnEivaV10>>, <<lnEivaV11>>, <<lnEivaV12>>, <<lnEivaV13>>, <<lcLordem>>,
					<<lnEivaV1>>, <<lnEivaV2>>, <<lnEivaV3>>, <<lnEivaV4>>, <<lnEivaV5>>, <<lnEivaV6>>, <<lnEivaV7>>, <<lnEivaV8>>, <<lnEivaV9>>, <<lnEivaV10>>, <<lnEivaV11>>, <<lnEivaV12>>, <<lnEivaV13>>,
					<<lnEtotal>>, <<lnEtotal>>, 'PTE ou EURO',
					'<<m_chinis>>', CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), '<<lnHora>>', '<<m_chinis>>', CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), '<<lnHora>>'
				)
			ENDTEXT
		Else
			TEXT to lcSql noshow textmerge
				Insert into rl (
					restamp, rlstamp, ndoc, rno, cdesc, nrdoc,
					datalc, dataven,
					ccstamp,
					cm, eval, erec,
					process, moeda, rdata,
					eivav1, eivav2, eivav3, eivav4, eivav5, eivav6, eivav7, eivav8, eivav9, eivav10, eivav11, eivav12, eivav13, lordem,
					eivavori1, eivavori2, eivavori3, eivavori4, eivavori5, eivavori6, eivavori7, eivavori8, eivavori9, eivavori10, eivavori11, eivavori12, eivavori13,
					enaval, evori, moedoc,
					ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora
				)
				values (
					'<<lnReStamp>>', '<<lnRlStamp>>', <<lnRdoc>>, @rno, '<<lnCdesc>>', '<<lnFno>>',
					'<<lnRdata>>', '<<myInvoiceData>>',
					'<<lnFtStamp>>',
					<<lnCm>>, <<lcEtotal>>, <<lcEtotal>>,
					1, '<<ALLTRIM(ft.moeda)>>', '<<myInvoiceData>>',
					<<lcEivaV1>>, <<lcEivaV2>>, <<lcEivaV3>>, <<lcEivaV4>>, <<lcEivaV5>>, <<lcEivaV6>>, <<lcEivaV7>>, <<lcEivaV8>>, <<lcEivaV9>>,  <<lcEivaV10>>,  <<lcEivaV11>>,  <<lcEivaV12>>,  <<lcEivaV13>>, <<lcLordem>>,
					<<lcEivaV1>>, <<lcEivaV2>>, <<lcEivaV3>>, <<lcEivaV4>>, <<lcEivaV5>>, <<lcEivaV6>>, <<lcEivaV7>>, <<lcEivaV8>>, <<lcEivaV9>>,  <<lcEivaV10>>,  <<lcEivaV11>>,  <<lcEivaV12>>,  <<lcEivaV13>>,
					<<lcEtotal>>, <<lcEtotal>>, 'PTE ou EURO',
					'<<m_chinis>>', CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), '<<lnHora>>', '<<m_chinis>>', CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), '<<lnHora>>'
				)
			ENDTEXT

		Endif
	Endif

	lcExecuteSQL  = lcExecuteSQL + Chr(13) + lcSQL

	Local lcTipoTsre
	lcTipoTsre  = 0
	lcTipoTsre  = uf_pagamento_getTsre(lnRdoc)

	If (lnCdesc='N/Factura') Or (lcTipoTsre != 2 And lcTipoTsre != 4) &&Vale de Reembolso
		TEXT to lcSql noshow textmerge
			Update re
			Set desc1=LEFT(re.desc1 + ' ' + '<<lnCdesc>> no. ' + '<<lnFno>>.',250)
			where re.ndoc=<<lnRdoc>> and re.rno=<<lnRno>>
		ENDTEXT
		lcExecuteSQL  = lcExecuteSQL + Chr(13) + lcSQL
	Endif


	Return lcExecuteSQL

Endfunc

************************************
* CRIAR DOCUMENTO DE REGULARIZA��O *
************************************
Function uf_PAGAMENTO_criarDocReg
	Lparameters lcBool, lcBool2

	Local lcFtstamp, lcReStamp, lcPdata, lcNmDoc, lcRmDoc, lcHora
	Store "" To lcFtstamp, lcReStamp, lcNmDoc, lcRmDoc, lcHora

	Local lcClNo, lcClEstab, lcClNome, lcTipoDoc, lcNdoc, lcRdoc
	Store 0 To lcClNo, lcClEstab, lcClNome, lcTipoDoc, lcNdoc, lcRdoc

	Local lcTotQtt, lcQtt1, lcEtotal, llcEtotal, lcEttIliq, lcEcusto, lcEttIva, lcEivaIn1, lcEivaIn2, lcEivaIn3, lcEivaIn4, lcEivaIn5, lcEivaIn6, lcEivaIn7, lcEivaIn8, lcEivaIn9, lcEivaV1, lcEivaV2, lcEivaV3, lcEivaV4, lcEivaV5, lcEivaV6, lcEivaV7, lcEivaV8, lcEivaV9, lcEivaIn10 , lcEivaIn11, lcEivaIn12, lcEivaIn13, lcEivaV10 ,lcEivaV11, lcEivaV12, lcEivaV13
	Store 0 To lcTotQtt, lcQtt1, lcEtotal, llcEtotal, lcEttIliq, lcEcusto, lcEttIva, lcEivaIn1, lcEivaIn2, lcEivaIn3, lcEivaIn4, lcEivaIn5, lcEivaIn6, lcEivaIn7, lcEivaIn8, lcEivaIn9, lcEivaV1, lcEivaV2, lcEivaV3, lcEivaV4, lcEivaV5, lcEivaV6, lcEivaV7, lcEivaV8, lcEivaV9, lcEivaIn10 , lcEivaIn11, lcEivaIn12, lcEivaIn13, lcEivaV10 ,lcEivaV11, lcEivaV12, lcEivaV13

	Public lcSusp
	Store .F. To lcSusp

	Local lcOkFt, lcOkFt2, lcOkFi

	Local lcTotCdesc, lcTotCdescPerc, lcTotCdescValor
	Store 0 To lcTotCdesc, lcTotCdescPerc, lcTotCdescValor
	*********

	&& DATA DO VENCIMENTO
	lcPdata=Datetime()+(difhoraria*3600)
	**lcPdata=datetime()
	*************************


	&& TIPO E N� DO DOCUMENTO A GRAVAR
	If myOffline
		lcNdoc 		= 85
		lcNmDoc		= "Reg. Cli. Offline"
	Else
		If !uf_gerais_getParameter('ADM0000000309', 'BOOL')
			lcNdoc		= myRegcli
			lcNmDoc		= myRegcliNm
		Else
			Local lcNmdocAReg
			lcNmdocAReg = ''
			Select uCrsFi2Reg
			Go Top
			Scan
				If !Empty(uCrsFi2Reg.nmdoc)
					lcNmdocAReg = Alltrim(uCrsFi2Reg.nmdoc)
				Endif
			Endscan
			If Alltrim(lcNmdocAReg) = 'Fatura Recibo'
				lcNdoc		= myRegcli
				lcNmDoc		= myRegcliNm
			Else
				TEXT TO lcSQL TEXTMERGE NOSHOW
					select nmdoc, ndoc from td where nmdoc='Nota de Cr�dito' and site='<<ALLTRIM(mySite)>>'
				ENDTEXT
				uf_gerais_actGrelha("","uCrsNCInfo",lcSQL)
				Select uCrsNCInfo
				lcNdoc		= uCrsNCInfo.ndoc
				lcNmDoc		= uCrsNCInfo.nmdoc
			Endif
		Endif
	Endif

	Select uCrsFi2Reg
	Go Top

	lcTipoDoc	= 3
	lcSusp		= .F.

	If lcBool
		If !uf_gerais_getParameter("ADM0000000308","BOOL")
			If myOffline
				lcRdoc	= 5
				lcRmDoc	= "Vale Reemb. Offline"
			Else
				lcRdoc	= 3
				lcRmDoc	= "Vale de Reembolso"
			Endif
		Else
			TEXT TO lcSQL NOSHOW TEXTMERGE
				select nmdoc, ndoc from tsre(nolock) where site='<<ALLTRIM(mySite)>>' and left(nmdoc,4)='Vale'
			ENDTEXT
			uf_gerais_actGrelha("","uCrsConfRec",lcSQL)
			Select uCrsConfRec
			lcRdoc	= uCrsConfRec.ndoc
			lcRmDoc	= Alltrim(uCrsConfRec.nmdoc)
			fecha("uCrsConfRec")
		Endif
	Endif
	**********************************

	&& Criar FtStamp para a Nt.Cr�dito e Recibo
	lcFtstamp = uf_gerais_stamp()
	If lcBool
		lcReStamp = uf_gerais_stamp()
	Endif
	********************************

	&& Cria Cursor para Posteriormente Permitir a Impress�o dos Tal�es
	If !Used("uCrsImpTalaoPos")
		Create Cursor uCrsImpTalaoPos (stamp c(26), tipo i(4), Lordem i(8), Susp l, AdiReserva l, nrreceita c(26) ,rm l, planocomp l, impcomp1 l, impcomp2 l , lote c(5), impresso l , plano c(5))
	Endif
	Select uCrsImpTalaoPos
	Append Blank
	Replace uCrsImpTalaoPos.stamp		With Alltrim(lcFtstamp)
	Replace uCrsImpTalaoPos.tipo		With lcNdoc
	Replace uCrsImpTalaoPos.Lordem		With 99999
	Replace uCrsImpTalaoPos.Susp		With .F.

	If lcBool
		Select uCrsImpTalaoPos
		Append Blank
		Replace uCrsImpTalaoPos.stamp		With Alltrim(lcReStamp)
		If myOffline
			Replace uCrsImpTalaoPos.tipo		With 905
		Else
			Replace uCrsImpTalaoPos.tipo		With 903
		Endif
		Replace uCrsImpTalaoPos.Lordem		With 99999
		Replace uCrsImpTalaoPos.Susp		With .F.
	Endif



	********************************

	&& GUARDAR DATA e HORA DO SISTEMA
	&&  ::TODO
	*!*		SET HOURS TO 24
	*!*		Atime=ttoc(datetime()+(difhoraria*3600),2)
	*!*		Astr=left(Atime,8)
	*!*		lcHora = Astr
	lcHora			= Time() && � necess�rio remover os segundos durante a inser��o na tabela
	*myInvoiceData	= dtosql(DATE())
	TEXT TO lcSQL TEXTMERGE NOSHOW
		select dateadd(HOUR, <<difhoraria>>, getdate()) data
	ENDTEXT
	uf_gerais_actGrelha("","uCrsMyInvoiceData",lcSQL)
	Select uCrsMyInvoiceData
	myInvoiceData = uf_gerais_getDate(uCrsMyInvoiceData.Data,"SQL")
	fecha("uCrsMyInvoiceData")
	*********************************************

	Select Distinct ofistamp From uCrsFi2Reg Where !Empty(ofistamp) Into Cursor uCrsFi2RegAux Readwrite
	Select uCrsFi2RegAux
	If Reccount("uCrsFi2RegAux")>0
		Select uCrsFi2RegAux
		Go Top
		Scan
			Local lcOfistampAux
			lcOfistampAux = Alltrim(uCrsFi2RegAux.ofistamp)
			Select uCrsFi2Reg
			Go Top
			Scan
				If Alltrim(uCrsFi2Reg.ofistamp) == Alltrim(lcOfistampAux)
					Local lcPos
					lcPos=Recno("uCrsFi2Reg")

					Local lcLordemOri
					lcLordemOri = uCrsFi2Reg.Lordem
					Delete From uCrsFi2Reg Where Alltrim(uCrsFi2Reg.ofistamp) == Alltrim(lcOfistampAux) And uCrsFi2Reg.Lordem <> lcLordemOri And !Empty(ofistamp)

					Select uCrsFi2Reg
					Try
						Go lcPos
					Catch
					Endtry
				Endif
			Endscan
			Select uCrsFi2RegAux
		Endscan
	Endif

	fecha("uCrsFi2RegAux")

	If !Used("ucrsofistamp")
		Create Cursor ucrsofistamp (ofistamp c(30))
	Else
		Select ucrsofistamp
		Delete All
	Endif
	Local lnconta
	lnconta = 1

	Select uCrsFi2Reg
	Go Top
	Select fistamp As ofistamp, fistamp, ftstamp From uCrsFi2Reg Into Cursor uCrsStampsFiReg Readwrite



	&& CALCULAR TOTAIS
	Select uCrsFi2Reg
	Go Top
	Scan
		Select uCrsFi2Reg
		Local lcPosR
		lcPosR=Recno("uCrsFi2Reg")

		&& corrigir linhas que estavam a duplicar nas regulariza��es
		Local lcrepetido
		lcrepetido = .F.

		If lnconta > 1
			Local lcofistampverif
			lcofistampverif = Alltrim(uCrsFi2Reg.fistamp)
			Select ucrsofistamp
			Go Top
			Scan
				If Alltrim(lcofistampverif) == Alltrim(ucrsofistamp.ofistamp) And !Empty(lcofistampverif) And lcrepetido = .F.
					lcrepetido = .T.
				Endif
			Endscan
		Endif

		If lcrepetido = .F.

			lcTotQtt		= lcTotQtt	+ uCrsFi2Reg.qtt
			lcQtt1			= lcQtt1	+ uCrsFi2Reg.qtt
			lcEtotal		= lcEtotal	+ uCrsFi2Reg.etiliquido

			If (Upper(Alltrim(uCrsFi2Reg.nmdoc))="FACTURA") And (!lcBool2)
				Select ucrsvale
				Go Top
				Scan For Alltrim(ucrsvale.fistamp)==Alltrim(uCrsFi2Reg.fistamp)
					If ucrsvale.fmarcada=.T. &&AND uCrsVale.etiliquido!=uCrsVale.eaquisicao
						llcEtotal = llcEtotal + (uCrsFi2Reg.qtt-ucrsvale.eaquisicao) * (ucrsvale.etiliquido2/ucrsvale.qtt2)
					Else
						llcEtotal = llcEtotal + uCrsFi2Reg.etiliquido
					Endif

					** marcar produto como recibado se for uma regulariza��o/devolu��o de produtos nao recibados **
					** rever procedure reg-vendas, campo eaquisicao, para perceber a l�gica **
					TEXT to lcSql noshow textmerge
						update fi set fmarcada=1, eaquisicao=eaquisicao+<<uCrsFi2Reg.qtt-uCrsVale.eaquisicao>>
						where fistamp='<<alltrim(uCrsFi2Reg.fistamp)>>'
					ENDTEXT
					If !uf_gerais_actGrelha("","",lcSQL)
						uf_perguntalt_chama("Erro a marcar produto como recibado.","OK","",16)
					Endif
					**

					Select ucrsvale
				Endscan
			Endif

			Select uCrsFi2Reg
			lcEttIliq		= lcEttIliq	+ (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1))
			lcEcusto		= lcEcusto	+ uCrsFi2Reg.ecusto

			Do Case
				Case uCrsFi2Reg.tabiva == 1
					lcEivaIn1	= Round(lcEivaIn1 + (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1)),2)
					lcEivaV1	= Round(lcEivaV1 + uCrsFi2Reg.etiliquido - (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1)),2)
				Case uCrsFi2Reg.tabiva == 2
					lcEivaIn2	= Round(lcEivaIn2	+ (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1)),2)
					lcEivaV2	= Round(lcEivaV2 + uCrsFi2Reg.etiliquido - (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1)),2)
				Case uCrsFi2Reg.tabiva == 3
					lcEivaIn3	= Round(lcEivaIn3	+ (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1)),2)
					lcEivaV3	= Round(lcEivaV3 + uCrsFi2Reg.etiliquido - (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1)),2)
				Case uCrsFi2Reg.tabiva == 4
					lcEivaIn4	= Round(lcEivaIn4	+ (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1)),2)
					lcEivaV4	= Round(lcEivaV4 + uCrsFi2Reg.etiliquido - (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1)),2)
				Case uCrsFi2Reg.tabiva == 5
					lcEivaIn5	= Round(lcEivaIn5	+ (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1)),2)
					lcEivaV5	= Round(lcEivaV5 + uCrsFi2Reg.etiliquido - (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1)),2)
				Case uCrsFi2Reg.tabiva == 6
					lcEivaIn6	= Round(lcEivaIn6	+ (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1)),2)
					lcEivaV6 	= Round(lcEivaV6 + uCrsFi2Reg.etiliquido - (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1)),2)
				Case uCrsFi2Reg.tabiva == 7
					lcEivaIn7	= Round(lcEivaIn7	+ (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1)),2)
					lcEivaV7 	= Round(lcEivaV7 + uCrsFi2Reg.etiliquido - (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1)),2)
				Case uCrsFi2Reg.tabiva == 8
					lcEivaIn8	= Round(lcEivaIn8	+ (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1)),2)
					lcEivaV8 	= Round(lcEivaV8 + uCrsFi2Reg.etiliquido - (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1)),2)
				Case uCrsFi2Reg.tabiva == 9
					lcEivaIn9	= Round(lcEivaIn9	+ (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1)),2)
					lcEivaV9 	= Round(lcEivaV9 + uCrsFi2Reg.etiliquido - (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1)),2)
				Case uCrsFi2Reg.tabiva == 10
					lcEivaIn10	= Round(lcEivaIn10 + (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1)),2)
					lcEivaV10	= Round(lcEivaV10 + uCrsFi2Reg.etiliquido - (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1)),2)
				Case uCrsFi2Reg.tabiva == 11
					lcEivaIn11	= Round(lcEivaIn11 + (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1)),2)
					lcEivaV11	= Round(lcEivaV11 + uCrsFi2Reg.etiliquido - (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1)),2)
				Case uCrsFi2Reg.tabiva == 12
					lcEivaIn12	= Round(lcEivaIn12	+ (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1)),2)
					lcEivaV12	= Round(lcEivaV12 + uCrsFi2Reg.etiliquido - (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1)),2)
				Case uCrsFi2Reg.tabiva == 13
					lcEivaIn13	= Round(lcEivaIn13	+ (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1)),2)
					lcEivaV13	= Round(lcEivaV13 + uCrsFi2Reg.etiliquido - (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1)),2)
			Endcase


			lcRdata		= uf_gerais_getDate(uCrsFi2Reg.rdata,"SQL")
		Endif


		lnconta = lnconta + 1

		Select uCrsFi2Reg
		Try
			Go lcPosR
		Catch
		Endtry
		Local ofistamplin
		ofistamplin = Alltrim(uCrsFi2Reg.fistamp)

		Select ucrsofistamp
		Go Bottom
		Append Blank
		Replace ucrsofistamp.ofistamp With Alltrim(ofistamplin)

		Select uCrsFi2Reg
		Try
			Go lcPosR
		Catch
		Endtry

		Select uCrsFi2Reg
	Endscan
	lcEtotal	= lcEtotal * (-1)
	lcEttIliq	= lcEttIliq * (-1)
	lcEcusto	= lcEcusto * (-1)
	lcEivaIn1	= lcEivaIn1 * (-1)
	lcEivaV1	= lcEivaV1 * (-1)
	lcEivaIn2	= lcEivaIn2 * (-1)
	lcEivaV2	= lcEivaV2 * (-1)
	lcEivaIn3	= lcEivaIn3 * (-1)
	lcEivaV3	= lcEivaV3 * (-1)
	lcEivaV4	= lcEivaV4 * (-1)
	lcEivaIn4	= lcEivaIn4 * (-1)
	lcEivaIn5	= lcEivaIn5 * (-1)
	lcEivaV5	= lcEivaV5 * (-1)
	lcEivaV6	= lcEivaV6 * (-1)
	lcEivaIn6	= lcEivaIn6 * (-1)
	lcEivaV7	= lcEivaV7 * (-1)
	lcEivaIn7	= lcEivaIn7 * (-1)
	lcEivaV8	= lcEivaV8 * (-1)
	lcEivaIn8	= lcEivaIn8 * (-1)
	lcEivaV9	= lcEivaV9 * (-1)
	lcEivaIn9	= lcEivaIn9 * (-1)
	lcEivaIn10	= lcEivaIn10 * (-1)
	lcEivaV10	= lcEivaV10 * (-1)
	lcEivaIn11	= lcEivaIn11 * (-1)
	lcEivaV11	= lcEivaV11 * (-1)
	lcEivaIn12	= lcEivaIn12 * (-1)
	lcEivaV12	= lcEivaV12 * (-1)
	lcEivaIn13	= lcEivaIn13 * (-1)
	lcEivaV13	= lcEivaV13 * (-1)


	If !Used("uCrsValIvaCabFTAux")
		Create Cursor uCrsValIvaCabFTAux;
			( lnEivaIn6 numeric(19,4), lnEivaIn7 numeric(19,4), lnEivaIn8 numeric(19,4), lnEivaIn9 numeric(19,4), lnEivaIn10 numeric(19,4), lnEivaIn11 numeric(19,4), lnEivaIn12 numeric(19,4), lnEivaIn13 numeric(19,4),;
			lnEivaV6 numeric(19,4), lnEivaV7 numeric(19,4), lnEivaV8 numeric(19,4), lnEivaV9 numeric(19,4), lnEivaV10 numeric(19,4), lnEivaV11 numeric(19,4), lnEivaV12 numeric(19,4), lnEivaV13 numeric(19,4))
	Else
		Select uCrsValIvaCabFTAux
		Delete All
	Endif


	Select uCrsValIvaCabFTAux
	Go Top
	Append Blank
	Replace uCrsValIvaCabFTAux.lnEivaIn6 With lcEivaIn6
	Replace uCrsValIvaCabFTAux.lnEivaIn7 With lcEivaIn7
	Replace uCrsValIvaCabFTAux.lnEivaIn8 With lcEivaIn8
	Replace uCrsValIvaCabFTAux.lnEivaIn9 With lcEivaIn9
	Replace uCrsValIvaCabFTAux.lnEivaIn10 With lcEivaIn10
	Replace uCrsValIvaCabFTAux.lnEivaIn11 With lcEivaIn11
	Replace uCrsValIvaCabFTAux.lnEivaIn12 With lcEivaIn12
	Replace uCrsValIvaCabFTAux.lnEivaIn13 With lcEivaIn13
	Replace uCrsValIvaCabFTAux.lnEivaV6 With lcEivaV6
	Replace uCrsValIvaCabFTAux.lnEivaV7 With lcEivaV7
	Replace uCrsValIvaCabFTAux.lnEivaV8 With lcEivaV8
	Replace uCrsValIvaCabFTAux.lnEivaV9 With lcEivaV9
	Replace uCrsValIvaCabFTAux.lnEivaV10 With lcEivaV10
	Replace uCrsValIvaCabFTAux.lnEivaV11 With lcEivaV11
	Replace uCrsValIvaCabFTAux.lnEivaV12 With lcEivaV12
	Replace uCrsValIvaCabFTAux.lnEivaV13 With lcEivaV13


	&& GRAVAR DOCUMENTOS DE REGULARIZA��O
	Store .F. To lcOkFt, lcOkFt2, lcOkFi

	If !Used("uCrsFt")
		Select ft
		Go Top
		Select * From ft Where 1=0 Into Cursor uCrsFt Readwrite
	Endif
	&&criar cursor auxiliar para a impress�o de tal�es
	If !Used("uCrsFt2")
		Select *, u_vddom As 'ft2_u_vddom', contacto As 'ft2_contacto', u_viatura As 'ft2_u_viatura', morada As 'ft2_morada', codpost As 'ft2_codpost', Local As 'ft2_local', telefone As 'ft2_telefone', email As 'ft2_email', horaentrega As 'ft2_horaentrega' From ft2 Where 1=0 Into Cursor uCrsFt2 Readwrite
	Endif
	If !Used("uCrsFi")
		Select * From fi Where 1=0 Into Cursor uCrsFi Readwrite
	Endif


	lcSqlFt2 = uf_PAGAMENTO_GravarRegFt2(lcFtstamp, lcHora)
	lcSqlFt2 = uf_gerais_trataPlicasSQL(lcSqlFt2)

	TEXT TO lcSQLFtDeclare NOSHOW TEXTMERGE
		DECLARE @fno as numeric(9,0)
		DECLARE @tabNewDocNr TABLE (newDocNr NUMERIC(9,0))
	ENDTEXT



	lcSQLFt = uf_PAGAMENTO_GravarRegFt(lcFtstamp, lcNmDoc, lcNdoc, lcTipoDoc, lcTotQtt, lcQtt1, Round(lcEtotal,2), Round(lcEttIliq,2), lcEcusto, Round(lcEivaV1+lcEivaV2+lcEivaV3+lcEivaV4+lcEivaV5+lcEivaV6+lcEivaV7+lcEivaV8+lcEivaV9+lcEivaV10+lcEivaV11+lcEivaV12+lcEivaV13,2), Round(lcEivaIn1,2), Round(lcEivaIn2,2), Round(lcEivaIn3,2), Round(lcEivaIn4,2), Round(lcEivaIn5,2), Round(lcEivaV1,2), Round(lcEivaV2,2), Round(lcEivaV3,2), Round(lcEivaV4,2), Round(lcEivaV5,2), lcPdata, lcHora)
	**,round(lcEivaV6,2),round(lcEivaV7,2),round(lcEivaV8,2),round(lcEivaV9,2),round(lcEivaV10,2),round(lcEivaV11,2),round(lcEivaV12,2),round(lcEivaV13,2), round(lcEivaIn6,2), round(lcEivaIn7,2), round(lcEivaIn8,2), round(lcEivaIn9,2), round(lcEivaIn10,2), round(lcEivaIn11,2), round(lcEivaIn12,2), round(lcEivaIn13,2)
	lcSQLFt = uf_gerais_trataPlicasSQL(lcSQLFtDeclare + lcSQLFt)

	lcSqlFi	= uf_PAGAMENTO_GravarRegFi(lcFtstamp, lcNmDoc, lcNdoc, lcTipoDoc, lcNrVenda, lcHora)

	lcSqlFi = uf_gerais_trataPlicasSQL(lcSqlFi)

	lcSQLInsertDoc = lcSqlFt2 + lcSQLFt + lcSqlFi

	**_cliptext=lcSQLInsertDoc

	If !Empty(lcSQLInsertDoc)
		**_cliptext = lcSQLInsertDoc
		lcSQL = ''
		TEXT TO lcSQL NOSHOW TEXTMERGE
			exec up_gerais_execSql 'Atendimento - Reg. Insert', 1,'<<lcSQLInsertDoc>>', '', '', '' , '', ''
			SELECT fno FROM ft WHERE ftstamp = '<<lcFtStamp>>'
		ENDTEXT

		If !uf_gerais_actGrelha("","ucrsFnoAt",lcSQL)
			uf_perguntalt_chama("OCORREU UMA ANOMALIA AO GRAVAR O DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			Return .F.
		Else
			&& Tudo Ok
			lcValidaInsercaoDocumento = .T.

			If Used("ucrsFt")

				Select ucrsFnoAt
				&&
				Update uCrsFt Set fno = ucrsFnoAt.fno Where Alltrim(ftstamp) = Alltrim(lcFtstamp)
				Update uCrsFi Set fno = ucrsFnoAt.fno Where Alltrim(ftstamp) = Alltrim(lcFtstamp)
			Endif
		Endif
	Endif

	If lcValidaInsercaoDocumento == .T. && Inseriu o Documento na BD

		Local lnTipoRec
		lnTipoRec = 0

		TEXT TO lcSQL_No NOSHOW TEXTMERGE
			SELECT fno FROM ft WHERE ftstamp = '<<lcFtStamp>>'
		ENDTEXT
		If !uf_gerais_actGrelha("","UcrsCalculaFno",lcSQL_No)
			uf_perguntalt_chama("OCORREU UMA ANOMALIA AO GRAVAR O DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			Return .F.
		Endif

		lcNrVenda = UcrsCalculaFno.fno

		&& gera certifica��o
		uf_gravarCert(lcFtstamp, lcNrVenda, lcNdoc, myInvoiceData, myInvoiceData, lcHora, lcEtotal, 'FT')

		If ATENDIMENTO.pgfDados.page1.chkPontos.Tag == "true"
			uf_CARTAOCLIENTE_eliminarPontosNC(lcFtstamp, uCrsClReg.no, uCrsClReg.estab)
		Endif

		If lcBool

			Local lcExecuteSQLRe
			Store '' To lcExecuteSQLRe

			&& gravar recibos
			If !lcBool2
				**	uf_perguntalt_chama("WAIT 3","OK","",16)
				If myOffline
					lcSqlRe = uf_PAGAMENTO_GravarRegRe(5, lcReStamp, lcRmDoc, lcRdoc, lcNrVenda, Round(lcEtotal+llcEtotal,2), lcHora, 0, 0)
					lcSqlRe = uf_gerais_trataPlicasSQL(lcSqlRe)
					lcExecuteSQLRe = lcExecuteSQLRe + Chr(13) + lcSqlRe
					lnTipoRec = 5
				Else
					lcSqlRe = uf_PAGAMENTO_GravarRegRe(3, lcReStamp, lcRmDoc, lcRdoc, lcNrVenda, Round(lcEtotal+llcEtotal,2), lcHora, 0, 0)
					lcSqlRe = uf_gerais_trataPlicasSQL(lcSqlRe)
					lcExecuteSQLRe = lcExecuteSQLRe + Chr(13) + lcSqlRe
					lnTipoRec = 3
				Endif
			Else
				**	uf_perguntalt_chama("WAIT 4","OK","",16)
				If myOffline
					lcSqlRe = uf_PAGAMENTO_GravarRegRe(5, lcReStamp, lcRmDoc, lcRdoc, lcNrVenda, Round(lcEtotal+llcEtotal,2), lcHora, 0, 0, .T.)
					lcSqlRe = uf_gerais_trataPlicasSQL(lcSqlRe)
					lcExecuteSQLRe = lcExecuteSQLRe + Chr(13) + lcSqlRe
					lnTipoRec = 5
				Else
					lcSqlRe = uf_PAGAMENTO_GravarRegRe(3, lcReStamp, lcRmDoc, lcRdoc, lcNrVenda, Round(lcEtotal+llcEtotal,2), lcHora, 0, 0, .T.)
					lcSqlRe = uf_gerais_trataPlicasSQL(lcSqlRe)
					lcExecuteSQLRe = lcExecuteSQLRe + Chr(13) + lcSqlRe
					lnTipoRec = 3
				Endif
			Endif



			If myOffline

				lcSQLRl = uf_PAGAMENTO_GravarRegRl(5, lcReStamp, lcFtstamp, lcRdoc, lcNrRecibo, lcNrVenda, Round(lcEtotal,2), Round(lcEivaV1,2), Round(lcEivaV2,2), Round(lcEivaV3,2), Round(lcEivaV4,2), Round(lcEivaV5,2), Round(lcEivaV6,2), Round(lcEivaV7,2), Round(lcEivaV8,2), Round(lcEivaV9,2), lcHora, lcRdata, Round(lcEtotal,2), Round(lcEivaV10,2), Round(lcEivaV11,2),Round(lcEivaV12,2), Round(lcEivaV13,2))
				lcSQLRl = uf_gerais_trataPlicasSQL(lcSQLRl)
				lcExecuteSQLRe = lcExecuteSQLRe + Chr(13) + lcSQLRl
			Else

				lcSQLRl = uf_PAGAMENTO_GravarRegRl(3, lcReStamp, lcFtstamp, lcRdoc, lcNrRecibo, lcNrVenda, Round(lcEtotal,2), Round(lcEivaV1,2), Round(lcEivaV2,2), Round(lcEivaV3,2), Round(lcEivaV4,2), Round(lcEivaV5,2), Round(lcEivaV6,2), Round(lcEivaV7,2), Round(lcEivaV8,2), Round(lcEivaV9,2), lcHora, lcRdata, Round(lcEtotal,2), Round(lcEivaV10,2), Round(lcEivaV11,2),Round(lcEivaV12,2), Round(lcEivaV13,2))
				lcSQLRl = uf_gerais_trataPlicasSQL(lcSQLRl)
				lcExecuteSQLRe = lcExecuteSQLRe + Chr(13) + lcSQLRl
			Endif

			If !lcBool2
				** CALCULAR TOTAIS PARA INCLUIR FACTURA NO RECIBO **
				Local llcFtStamp1, llcFno, lcEtotals
				Store 0 To lcEtotal, lcEtotals, lcEivaV1, lcEivaV2, lcEivaV3, lcEivaV4, lcEivaV5, lcEivaV6, lcEivaV7, lcEivaV8, lcEivaV9, llcFno
				Store "" To llcFtStamp1

				Select uCrsFi2Reg
				**SELECT distinct ftstamp FROM uCrsFi2Reg WHERE UPPER(ALLTRIM(nmdoc))="FACTURA" AND fmarcada=.F. INTO CURSOR uCrsFi2RegStamps readwrite
				Select Distinct ftstamp From uCrsFi2Reg Where (Upper(Alltrim(nmdoc))="FACTURA" Or Upper(Alltrim(nmdoc))="FACTURA IMPORTADA") And fmarcada=.F. Into Cursor uCrsFi2RegStamps Readwrite


				Select uCrsFi2RegStamps
				Go Top
				Scan
					Select uCrsFi2Reg
					Go Top
					Scan For Alltrim(uCrsFi2Reg.ftstamp)==Alltrim(uCrsFi2RegStamps.ftstamp)
						**Scan for uCrsFi2Reg.ndoc=1 &&AND (uCrsFi2Reg.fmarcada!=.T.)
						Select ucrsvale
						Go Top
						Scan For uCrsFi2Reg.fistamp==ucrsvale.fistamp
							If ucrsvale.fmarcada=.T.
								lcEtotals	= (uCrsFi2Reg.qtt-ucrsvale.eaquisicao)*(ucrsvale.etiliquido2/ucrsvale.qtt2)

								lcEtotal	= lcEtotal + lcEtotals

								Do Case
									Case uCrsFi2Reg.tabiva == 1
										lcEivaV1	= lcEivaV1 + lcEtotals - (lcEtotals/(uCrsFi2Reg.iva/100+1))
									Case uCrsFi2Reg.tabiva == 2
										lcEivaV2	= lcEivaV2 + lcEtotals - (lcEtotals/(uCrsFi2Reg.iva/100+1))
									Case uCrsFi2Reg.tabiva == 3
										lcEivaV3	= lcEivaV3 + lcEtotals - (lcEtotals/(uCrsFi2Reg.iva/100+1))
									Case uCrsFi2Reg.tabiva == 4
										lcEivaV4	= lcEivaV4 + lcEtotals - (lcEtotals/(uCrsFi2Reg.iva/100+1))
									Case uCrsFi2Reg.tabiva == 5
										lcEivaV5	= lcEivaV5 + lcEtotals - (lcEtotals/(uCrsFi2Reg.iva/100+1))
									Case uCrsFi2Reg.tabiva == 6
										lcEivaV6 	= lcEivaV6 + lcEtotals - (lcEtotals/(uCrsFi2Reg.iva/100+1))
									Case uCrsFi2Reg.tabiva == 7
										lcEivaV7 	= lcEivaV7 + lcEtotals - (lcEtotals/(uCrsFi2Reg.iva/100+1))
									Case uCrsFi2Reg.tabiva == 8
										lcEivaV8 	= lcEivaV8 + lcEtotals - (lcEtotals/(uCrsFi2Reg.iva/100+1))
									Case uCrsFi2Reg.tabiva == 9
										lcEivaV9 	= lcEivaV9 + lcEtotals - (lcEtotals/(uCrsFi2Reg.iva/100+1))
									Case uCrsFi2Reg.tabiva == 10
										lcEivaV10 	= lcEivaV10 + lcEtotals - (lcEtotals/(uCrsFi2Reg.iva/100+1))
									Case uCrsFi2Reg.tabiva == 11
										lcEivaV11 	= lcEivaV11 + lcEtotals - (lcEtotals/(uCrsFi2Reg.iva/100+1))
									Case uCrsFi2Reg.tabiva == 12
										lcEivaV12 	= lcEivaV12 + lcEtotals - (lcEtotals/(uCrsFi2Reg.iva/100+1))
									Case uCrsFi2Reg.tabiva == 13
										lcEivaV13 	= lcEivaV13 + lcEtotals - (lcEtotals/(uCrsFi2Reg.iva/100+1))
								Endcase

								llcFtStamp1 = uCrsFi2Reg.ftstamp
								llcFno		= uCrsFi2Reg.fno
							Else
								lcEtotal	= lcEtotal + uCrsFi2Reg.etiliquido

								Do Case
									Case uCrsFi2Reg.tabiva == 1
										lcEivaV1	= lcEivaV1 + uCrsFi2Reg.etiliquido - (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1))
									Case uCrsFi2Reg.tabiva == 2
										lcEivaV2	= lcEivaV2 + uCrsFi2Reg.etiliquido - (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1))
									Case uCrsFi2Reg.tabiva == 3
										lcEivaV3	= lcEivaV3 + uCrsFi2Reg.etiliquido - (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1))
									Case uCrsFi2Reg.tabiva == 4
										lcEivaV4	= lcEivaV4 + uCrsFi2Reg.etiliquido - (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1))
									Case uCrsFi2Reg.tabiva == 5
										lcEivaV5	= lcEivaV5 + uCrsFi2Reg.etiliquido - (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1))
									Case uCrsFi2Reg.tabiva == 6
										lcEivaV6 	= lcEivaV6 + uCrsFi2Reg.etiliquido - (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1))
									Case uCrsFi2Reg.tabiva == 7
										lcEivaV7 	= lcEivaV7 + uCrsFi2Reg.etiliquido - (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1))
									Case uCrsFi2Reg.tabiva == 8
										lcEivaV8 	= lcEivaV8 + uCrsFi2Reg.etiliquido - (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1))
									Case uCrsFi2Reg.tabiva == 9
										lcEivaV9 	= lcEivaV9 + uCrsFi2Reg.etiliquido - (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1))
									Case uCrsFi2Reg.tabiva == 10
										lcEivaV10 	= lcEivaV10 + uCrsFi2Reg.etiliquido - (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1))
									Case uCrsFi2Reg.tabiva == 11
										lcEivaV11 	= lcEivaV11 + uCrsFi2Reg.etiliquido - (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1))
									Case uCrsFi2Reg.tabiva == 12
										lcEivaV12 	= lcEivaV12 + uCrsFi2Reg.etiliquido - (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1))
									Case uCrsFi2Reg.tabiva == 13
										lcEivaV13 	= lcEivaV13 + uCrsFi2Reg.etiliquido - (uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1))
								Endcase

								llcFtStamp1 = uCrsFi2Reg.ftstamp
								llcFno	= uCrsFi2Reg.fno
							Endif

							Select ucrsvale
						Endscan

						lcRdata		= uf_gerais_getDate(uCrsFi2Reg.rdata,"SQL")

						Select uCrsFi2Reg
					Endscan


					If !Empty(llcFtStamp1)


						If myOffline

							lcSQLRl = uf_PAGAMENTO_GravarRegRl(4, lcReStamp, llcFtStamp1, lcRdoc, lcNrRecibo, llcFno, Round(lcEtotal,2), Round(lcEivaV1,2), Round(lcEivaV2,2), Round(lcEivaV3,2), Round(lcEivaV4,2), Round(lcEivaV5,2), Round(lcEivaV6,2), Round(lcEivaV7,2), Round(lcEivaV8,2), Round(lcEivaV9,2), lcHora, lcRdata, Round(lcEtotal,2),  Round(lcEivaV10,2),  Round(lcEivaV11,2),  Round(lcEivaV12,2),  Round(lcEivaV13,2))
							lcSQLRl = uf_gerais_trataPlicasSQL(lcSQLRl)
							lcExecuteSQLRe = lcExecuteSQLRe + Chr(13) + lcSQLRl
						Else

							lcSQLRl = uf_PAGAMENTO_GravarRegRl(1, lcReStamp, llcFtStamp1, lcRdoc, lcNrRecibo, llcFno, Round(lcEtotal,2), Round(lcEivaV1,2), Round(lcEivaV2,2), Round(lcEivaV3,2), Round(lcEivaV4,2), Round(lcEivaV5,2), Round(lcEivaV6,2), Round(lcEivaV7,2), Round(lcEivaV8,2), Round(lcEivaV9,2), lcHora, lcRdata, Round(lcEtotal,2), Round(lcEivaV10,2), Round(lcEivaV11,2), Round(lcEivaV12,2), Round(lcEivaV13,2))
							lcSQLRl = uf_gerais_trataPlicasSQL(lcSQLRl)
							lcExecuteSQLRe = lcExecuteSQLRe + Chr(13) + lcSQLRl
						Endif
						Store 0 To lcEtotal, lcEtotals, lcEivaV1, lcEivaV2, lcEivaV3, lcEivaV4, lcEivaV5, lcEivaV6, lcEivaV7, lcEivaV8, lcEivaV9, llcFno
						Store "" To llcFtStamp1
					Endif

					Select uCrsFi2RegStamps
				Endscan
				**
			Endif


			TEXT TO lcSQL NOSHOW TEXTMERGE
				update re set restamp=restamp where restamp='<<alltrim(lcReStamp)>>'
			ENDTEXT
			lcSQL = uf_gerais_trataPlicasSQL(lcSQL)
			lcExecuteSQLRe = lcExecuteSQLRe + Chr(13) + Alltrim(lcSQL)
			&& Grava Recibos
			TEXT TO lcSQL NOSHOW TEXTMERGE
				exec up_gerais_execSql 'Atendimento - Recibos', 1,'<<lcExecuteSQLRe>>', '', '', '' , '', ''
			ENDTEXT

			If !uf_gerais_actGrelha("","",lcSQL)
				uf_perguntalt_chama("OCORREU UMA ANOMALIA AO GRAVAR RECIBOS ASSOCIADOS AO ATENDIMENTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				Return .F.
			Else
				lcSQL = ''
				TEXT TO lcSQL NOSHOW TEXTMERGE
					select * from re (nolock) where reano=year(getdate())
					and etotal=(select etotal from re ree (nolock) where ree.rno=re.rno-1 and ree.nmdoc=re.nmdoc and ree.reano=re.reano)
					and no=(select no from re ree (nolock) where ree.rno=re.rno-1 and ree.nmdoc=re.nmdoc  and ree.reano=re.reano)
					and desc1=(select desc1 from re ree (nolock) where ree.rno=re.rno-1 and ree.nmdoc=re.nmdoc  and ree.reano=re.reano)
					and etotal<>0
					and desc1<>''
					and re.restamp='<<ALLTRIM(lcReStamp)>>'
				ENDTEXT

				If !uf_gerais_actGrelha("","ucrsRecAntRepetido",lcSQL)
					uf_perguntalt_chama("OCORREU UM ERRO A VERIFICAR O RECIBO ANTERIOR. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
					Return .F.
				Else
					Select ucrsRecAntRepetido
					If Reccount("ucrsRecAntRepetido")>0
						lcSQL = ''
						TEXT TO lcSQL NOSHOW TEXTMERGE
							DELETE FROM re WHERE restamp='<<ALLTRIM(lcReStamp)>>'
							DELETE FROM rl WHERE restamp='<<ALLTRIM(lcReStamp)>>'
							DELETE FROM b_cert WHERE stamp='<<ALLTRIM(lcReStamp)>>'
						ENDTEXT

						If !uf_gerais_actGrelha("","",lcSQL)
							uf_perguntalt_chama("OCORREU UM ERRO A ANULAR O RECIBO ANTERIOR. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
							Return .F.
						Endif

						Select uCrsImpTalaoPos
						Delete From uCrsImpTalaoPos Where Alltrim(uCrsImpTalaoPos.stamp) = Alltrim(lcReStamp)

						Local lcMensagem
						lcMensagem = Alltrim(ucrse1.id_lt) + ' - ' + Alltrim(ucrse1.nomecomp) + ' - ' + Alltrim(m_chinis) + ' - Recibo duplicado removido '
						uf_startup_sendmail_errors('Erro Recibo Repetido', lcMensagem )
					Endif
				Endif

				fecha("ucrsRecAntRepetido")

				lcSqlT = ''
				TEXT TO lcSQLT NOSHOW TEXTMERGE
					select restamp, etotal, efinv, rno, ndoc, (select sum(erec) from rl (nolock) where re.restamp=rl.restamp) as totlin from re (nolock) where restamp='<<ALLTRIM(lcReStamp)>>'
				ENDTEXT

				If !uf_gerais_actGrelha("","uCrsRETotDif",lcSqlT)
					uf_perguntalt_chama("OCORREU UM ERRO A CONSULTAR OS DETALHES DOS TOTAIS DOS RECIBOS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
					Return .F.
				Endif
				Select uCrsRETotDif
				If uCrsRETotDif.etotal + uCrsRETotDif.efinv <> uCrsRETotDif.totlin

					Local lcTorReCert, lcNrRecCert, lcNrDocRecCert
					lcTorReCert = uCrsRETotDif.totlin
					lcNrRecCert = uCrsRETotDif.rno
					lcNrDocRecCert = uCrsRETotDif.ndoc

					lcSql2 = ''
					TEXT TO lcSQL2 NOSHOW TEXTMERGE
						UPDATE re SET etotal = <<uCrsRETotDif.totlin>> where restamp='<<ALLTRIM(lcReStamp)>>'
						--DELETE FROM b_cert WHERE stamp='<<ALLTRIM(lcReStamp)>>'
					ENDTEXT

					If !uf_gerais_actGrelha("","",lcSql2)
						uf_perguntalt_chama("OCORREU UM ERRO A ANULAR O RECIBO ANTERIOR. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
						Return .F.
					Endif

					**lcSql3 = ''
					**TEXT TO lcSQL3 NOSHOW TEXTMERGE
					**uf_gravarCert(lcReStamp, lcNrRecCert , lcNrDocRecCert , lcRdata, lcRdata, lcHora, lcTorReCert , 'RE')
					**ENDTEXT

					**If !uf_gerais_actGrelha("","",lcSQL3)
					**	uf_perguntalt_chama("OCORREU UM ERRO A ANULAR O RECIBO ANTERIOR. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
					**	Return .f.
					**ENDIF

					Local lcMensagem
					lcMensagem = Alltrim(ucrse1.id_lt) + ' - ' + Alltrim(ucrse1.nomecomp) + ' - ' + Alltrim(m_chinis) + ' - Recibo com linhas diferente do cabe�alho '
					uf_startup_sendmail_errors('Erro Recibo linhas dif cabecalho', lcMensagem )
				Endif
				fecha("uCrsRETotDif")
			Endif
			**

		Endif
		**

		If lnTipoRec <> 6 And Type("lnReStamp") <> 'U'
			uf_gravarCert(lnReStamp, uf_gerais_getUmValor("re", "rno", "restamp = '" + lnReStamp + "'"), lnRdoc, lcRdata, lcRdata, lcHora, uf_gerais_getUmValor("re", "etotal", "restamp = '" + lnReStamp + "'"), 'RE')
		Endif

		&& gravar arquivo digital
		*tabela
		If myArquivoDigital
			uf_arquivoDigital_TaloesLt(lcFtstamp)
		Endif
		*PDF
		If myArquivoDigitalPDF
			If uf_gerais_getParameter("ADM0000000191","BOOL")
				Select * From uCrsFt Into Cursor uCrsFtBak Readwrite
				Select * From uCrsFt2 Into Cursor uCrsFt2Bak Readwrite
				Select * From uCrsFi Into Cursor uCrsFiBak Readwrite

				Select uCrsCabVendas
				Select uCrsFt
				Delete For Alltrim(uCrsFt.ftstamp) != Alltrim(lcFtstamp)
				Select uCrsFt2
				Delete For Alltrim(uCrsFt2.ft2stamp) != Alltrim(lcFtstamp)
				Select uCrsFi
				Delete For Alltrim(uCrsFi.ftstamp) != Alltrim(lcFtstamp)
				uf_arquivodigital_EmitirPdfsTalao('PAGAMENTO','')

				Select * From uCrsFtBak Into Cursor uCrsFt Readwrite
				Select * From uCrsFt2Bak Into Cursor uCrsFt2 Readwrite
				Select * From uCrsFiBak Into Cursor uCrsFi Readwrite
			Endif
		Endif
		**

	Endif

	If Used("uCrsFi2RegStamps")
		fecha("uCrsFi2RegStamps")
	Endif
Endfunc


** Gravar documento de regulariza��o na FT
Function uf_PAGAMENTO_GravarRegFt
	Lparameters lnFtStamp, lnNmDoc, lnNdoc, lnTipoDoc, lnTotQtt, lnQtt1, lnEtotal, lnEttIliq, lnEcusto, lnEttIva, lnEivaIn1, lnEivaIn2, lnEivaIn3, lnEivaIn4, lnEivaIn5, lnEivaV1, lnEivaV2, lnEivaV3, lnEivaV4, lnEivaV5, lnPdata, lnHora

	Local lcEivaV6,lcEivaV7,lcEivaV8,lcEivaV9,lcEivaV10,lcEivaV11,lcEivaV12,lcEivaV13,lcEivaIn6,lcEivaIn7,lcEivaIn8,lcEivaIn9,lcEivaIn10,lcEivaIn11,lcEivaIn12,lcEivaIn13
	Store 0 To lcEivaV6,lcEivaV7,lcEivaV8,lcEivaV9,lcEivaV10,lcEivaV11,lcEivaV12,lcEivaV13,lcEivaIn6,lcEivaIn7,lcEivaIn8,lcEivaIn9,lcEivaIn10,lcEivaIn11,lcEivaIn12,lcEivaIn13

	Select uCrsValIvaCabFTAux
	lcEivaV6 = uCrsValIvaCabFTAux.lnEivaV6
	lcEivaV7 = uCrsValIvaCabFTAux.lnEivaV7
	lcEivaV8 = uCrsValIvaCabFTAux.lnEivaV8
	lcEivaV9 = uCrsValIvaCabFTAux.lnEivaV9
	lcEivaV10 = uCrsValIvaCabFTAux.lnEivaV10
	lcEivaV11 = uCrsValIvaCabFTAux.lnEivaV11
	lcEivaV12 = uCrsValIvaCabFTAux.lnEivaV12
	lcEivaV13 = uCrsValIvaCabFTAux.lnEivaV13
	lcEivaIn6 = uCrsValIvaCabFTAux.lnEivaIn6
	lcEivaIn7 = uCrsValIvaCabFTAux.lnEivaIn7
	lcEivaIn8 = uCrsValIvaCabFTAux.lnEivaIn8
	lcEivaIn9 = uCrsValIvaCabFTAux.lnEivaIn9
	lcEivaIn10 = uCrsValIvaCabFTAux.lnEivaIn10
	lcEivaIn11 = uCrsValIvaCabFTAux.lnEivaIn11
	lcEivaIn12 = uCrsValIvaCabFTAux.lnEivaIn12
	lcEivaIn13 = uCrsValIvaCabFTAux.lnEivaIn13

	Select ft

	Local lcExecuteSQL
	Store "" To lcExecuteSQL

	TEXT TO lcSql NOSHOW TEXTMERGE
		--SET @fno = (select ISNULL(MAX(ft.fno),0) + 1 from ft (nolock) where (ft.ftano between Year('<<myInvoiceData>>')-<<IIF(uf_gerais_getParameter_site('ADM0000000142', 'BOOL', mySite),0,1)>> AND Year('<<myInvoiceData>>')) and ft.ndoc=<<Str(lnNdoc)>>)

		SET @fno = 0
		DELETE FROM @tabNewDocNr

		INSERT INTO @tabNewDocNr (newDocNr)
		EXEC up_get_newDocNr 'FT', <<lnNdoc>>, '<<mySite>>', 1

		SET @fno = (SELECT newDocNr FROM @tabNewDocNr)

		Insert Into FT (
			ftstamp
			,pais
			,nmdoc
			,fno
			,[no]
			,nome
			,morada
			,[local]
			,codpost
			,ncont
			,telefone
			,estab
			,ccusto
			,vendedor
			,vendnm
			,fdata
			,ftano
			,pdata
			,carga
			,descar
			,saida
			,ivatx1
			,ivatx2
			,ivatx3
			,ivatx4
			,ivatx5
			,ndoc
			,moeda
			,cdata
			,totqtt
			,qtt1
			,qtt2
			,tipo
			,tipodoc
			,chora
			,memissao
			,total
			,etotal
			,ttiliq
			,ettiliq
			,custo
			,ecusto
			,ivain1
			,eivain1
			,ivain2
			,eivain2
			,ivain3
			,eivain3
			,ivain4
			,eivain4
			,ivain5
			,eivain5
			,ivav1
			,eivav1
			,ivav2
			,eivav2
			,ivav3
			,eivav3
			,ivav4
			,eivav4
			,ivav5
			,eivav5
			,ttiva
			,ettiva
			,cambio
			,[site]
			,pnome
			,pno
			,cxstamp
			,cxusername
			,ssstamp
			,ssusername
			,valorm2
			,nome2
			,u_nratend
			,ousrinis
			,ousrdata
			,ousrhora
			,usrinis
			,usrdata
			,usrhora
			,id_tesouraria_conta
			, ivatx10 ,ivatx11, ivatx12, ivatx13
			, eivain10 , eivain11 ,eivain12, eivain13
			, eivav10 , eivav11 ,eivav12, eivav13
		)
		Values (
			'<<alltrim(lnFtStamp)>>'
			, <<ft.pais>>
			, '<<lnNmDoc>>'
			, @fno
			, <<uCrsClReg.no>>
			, '<<Alltrim(uCrsClReg.nome)>>'
			, '<<Alltrim(uCrsClReg.morada)>>'
			, '<<Alltrim(uCrsClReg.local)>>'
			, '<<Alltrim(uCrsClReg.codpost)>>'
			, '<<Alltrim(uCrsClReg.ncont)>>'
			, '<<Alltrim(uCrsClReg.telefone)>>'
			, <<uCrsClReg.estab>>
			, '<<ALLTRIM(uCrsClReg.ccusto)>>'
			, <<ft.vendedor>>
			, '<<Alltrim(ft.vendnm)>>'
			, '<<myInvoiceData>>'
			, YEAR('<<(myInvoiceData)>>')
			, '<<Iif(lnNdoc=myFact,uf_gerais_getDate(lnPdata,"SQL"),uf_gerais_getDate((datetime()+(difhoraria*3600)),"SQL"))>>'
			, '<<ft.carga>>'
			, '<<ft.descar>>'
			, Left('<<lnHora>>',5)
			, <<ft.ivatx1>>
			, <<ft.ivatx2>>
			, <<ft.ivatx3>>
			, <<ft.ivatx4>>
			, <<ft.ivatx5>>
			, <<lnNdoc>>
			, '<<ft.moeda>>'
			, convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
			, <<lnTotQtt>>
			, <<lnQtt1>>
			, <<ft.qtt2>>
			, 'Normal'
			, <<lnTipoDoc>>
			, Left('<<Chrtran(lnHora,":","")>>',4)
			, '<<ft.memissao>>'
			, <<lnEtotal*200.482>>
			, <<lnEtotal>>
			, <<lnEttIliq*200.482>>
			, <<lnEttIliq>>
			, <<lnEcusto*200.482>>
			, <<lnEcusto>>
			, <<lnEivaIn1*200.482>>
			, <<lnEivaIn1>>
			, <<lnEivaIn2*200.482>>
			, <<lnEivaIn2>>
			, <<lnEivaIn3*200.482>>
			, <<lnEivaIn3>>
			, <<lnEivaIn4*200.482>>
			, <<lnEivaIn4>>
			, <<lnEivaIn5*200.482>>
			, <<lnEivaIn5>>
			, <<lnEivaV1*200.482>>
			, <<lnEivaV1>>
			, <<lnEivaV2*200.482>>
			, <<lnEivaV2>>
			, <<lnEivaV3*200.482>>
			, <<lnEivaV3>>
			, <<lnEivaV4*200.482>>
			, <<lnEivaV4>>
			, <<lnEivaV5*200.482>>
			, <<lnEivaV5>>
			, <<lnEttIva*200.482>>
			, <<lnEttIva>>
			, 0
			, '<<ALLTRIM(mySite)>>'
			, '<<ALLTRIM(myTerm)>>'
			, <<myTermNo>>
			, '<<ALLTRIM(myCxStamp)>>'
			, '<<myCxUser>>'
			, '<<ALLTRIM(myssstamp)>>'
			, '<<ft.ssusername>>'
			, <<lnEtotal>>
			, '<<uCrsClReg.nome2>>'
			, '<<nrAtendimento>>'
			, '<<m_chinis>>'
			, CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
			, '<<lnHora>>'
			, '<<m_chinis>>'
			, CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
			, '<<lnHora>>'
			, <<ft.id_tesouraria_conta>>,<<ft.ivatx10>>, <<ft.ivatx11>>,<<ft.ivatx12>>, <<ft.ivatx13>>
			,<<lcEivaIn10>>, <<lcEivaIn11>>,<<lcEivaIn12>>, <<lcEivaIn13>>
			,<<lcEivaV10>>, <<lcEivaV11>>, <<lcEivaV12>>,<<lcEivaV13>>
		)
	ENDTEXT

	lcExecuteSQL  = lcExecuteSQL + Chr(13) + lcSQL

	*!*		lcSQLPagCentral = ''
	*!*		TEXT TO lcSQLPagCentral TEXTMERGE NOSHOW
	*!*			insert into B_pagCentral
	*!*				(stamp,nrAtend,nrVendas,Total,ano,no,estab,vendedor,nome
	*!*				,terminal,terminal_nome,dinheiro,mb,visa,cheque,troco,oData,uData,fechado,abatido,fechaStamp,fechaUser
	*!*				,evdinheiro,epaga1,epaga2,echtotal,evdinheiro_semTroco,etroco,total_bruto,devolucoes,ntCredito,creditos,cxstamp,ssstamp,site,obs,epaga3,epaga4,epaga5,epaga6,exportado,pontosAt,campanhas
	*!*				,id_contab,valpagmoeda,moedapag,nrMotivoMov)
	*!*				select '<<astr(YEAR(DATE())) + ALLTRIM(nrAtendimento)>>','<<ALLTRIM(nrAtendimento)>>',1,0.00,YEAR(getdate()),<<ft.no>>,0,1,'<<ALLTRIM(ch_vendnm)>>'
	*!*				,<<myTermNo>> ,'<<ALLTRIM(myTerm)>>',0,0,0,0,0,getdate(),getdate(),1,0,'',0
	*!*				,0,0,0,0,0,0,0,0,0,0,'<<ALLTRIM(myCxStamp)>>','<<ALLTRIM(mySsStamp)>>','<<ALLTRIM(mySite)>>','',0,0,0,0,0,0,''
	*!*				,0,0,'',''
	*!*		ENDTEXT
	*!*
	*!*		lcExecuteSQL  = lcExecuteSQL + CHR(13) + lcSQLPagCentral

	&& acrescentar nova linha ao cursor
	Select uCrsFt
	Go Top
	*APPEND FROM DBF("ft")
	Select * From uCrsFt Union All Select * From ft Into Cursor uCrsFt Readwrite
	Select uCrsFt
	Go Bottom
	Replace;
		ftstamp With lnFtStamp;
		nmdoc 	With lnNmDoc;
		no 		With uCrsClReg.no;
		nome 	With Alltrim(uCrsClReg.Nome);
		morada	With Alltrim(uCrsClReg.morada);
		local	With Alltrim(uCrsClReg.Local);
		codpost	With Alltrim(uCrsClReg.codpost);
		ncont	With Alltrim(uCrsClReg.ncont);
		telefone With Alltrim(uCrsClReg.telefone);
		ccusto	With Alltrim(uCrsClReg.ccusto);
		fdata	With Date();
		ftano	With Year(Date());
		pdata	With Iif(lnNdoc=myFact,lnPdata,(Datetime()+(difhoraria*3600)));
		saida	With Left(lnHora,5);
		ndoc	With lnNdoc;
		estab	With uCrsClReg.estab;
		cdata	With Date();
		totqtt	With lnTotQtt;
		qtt1	With lnQtt1;
		cobrado	With lcSusp;
		tipodoc	With lnTipoDoc;
		tipo	With 'Normal';
		chora	With Left(Chrtran(lnHora,":",""),4);
		chtotal	With Iif(lnNdoc!=myFact,ft.chtotal,0);
		echtotal With Iif(lnNdoc!=myFact,ft.echtotal,0);
		custo	With lnEcusto*200.482;
		EivaIn1	With lnEivaIn1;
		EivaIn2	With lnEivaIn2;
		EivaIn3	With lnEivaIn3;
		EivaV1	With lnEivaV1;
		EivaV2	With lnEivaV2;
		EivaV3	With lnEivaV3;
		ettiliq	With lnEttIliq;
		ettiva	With lnEttIva;
		etotal	With lnEtotal;
		EivaIn4	With lnEivaIn4;
		EivaV4	With lnEivaV4;
		ecusto	With lnEcusto;
		EivaIn5 With lnEivaIn5;
		EivaV5	With lnEivaV5;
		total	With lnEtotal*200.482;
		ivain1	With lnEivaIn1*200.482;
		ivain2	With lnEivaIn2*200.482;
		ivain3	With lnEivaIn3*200.482;
		ivain4	With lnEivaIn4*200.482;
		ivain5	With lnEivaIn5*200.482;
		ivav1	With lnEivaV1*200.482;
		ivav2	With lnEivaV2*200.482;
		ivav3	With lnEivaV3*200.482;
		ivav4	With lnEivaV4*200.482;
		ivav5	With lnEivaV5*200.482;
		ttiliq	With lnEttIliq*200.482;
		ttiva	With lnEttIva*200.482;
		cambio	With Iif(lnNdoc=myFact,1,0);
		pnome	With myTerm;
		pno		With myTermNo;
		ousrinis	With m_chinis;
		ousrdata	With Date();
		ousrhora	With lnHora;
		usrinis	With m_chinis;
		usrdata	With Date();
		usrhora	With lnHora;
		nome2	With uCrsClReg.nome2;
		u_nratend	With nrAtendimento;
		EivaIn10	With lcEivaIn10;
		EivaIn11	With lcEivaIn11;
		EivaIn12	With lcEivaIn12;
		EivaIn13	With lcEivaIn13;
		EivaV10	With lcEivaV10;
		EivaV11	With lcEivaV11;
		EivaV12	With lcEivaV12;
		EivaV13	With lcEivaV13

	Select ucrsGeralTd
	Scan For ndoc=lnNdoc
		Select uCrsFt
		Replace;
			u_tipodoc	With ucrsGeralTd.u_tipodoc;
			tiposaft	With ucrsGeralTd.tiposaft
	Endscan

	Return lcExecuteSQL
Endfunc

********************************************
* Gravar documento de regulariza��o na FT2 *
********************************************
Function uf_PAGAMENTO_GravarRegFt2
	Lparameters lnFtStamp, lnHora

	Local lcSQL, lcExecuteSQL
	Store "" To lcExecuteSQL, lcSQL

	&&Notas:
	* vdollocal = "Caixa"
	* vdlocal = "C"
	TEXT to lcSql noshow textmerge
		insert into ft2 (
			ft2stamp, vdollocal, vdlocal,modop1, modop2,ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora , token_efectivacao_compl, token_anulacao_compl, u_docorig, u_hclstamp1, nomeUtPag, id_efr_externa
		)Values (
			'<<alltrim(lnFtStamp)>>', 'Caixa', 'C','Visa', 'MultiBanco','<<m_chinis>>', CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), '<<lnHora>>', '<<m_chinis>>', CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), '<<lnHora>>', '', '', '<<ft2.u_docorig>>', '<<ALLTRIM(ft2.u_hclstamp1)>>', '<<ft2.nomeUtPag>>', '<<ft2.id_efr_externa>>'
		)
	ENDTEXT
	lcExecuteSQL  = lcExecuteSQL + Chr(13) + lcSQL

	&& acrescentar nova linha ao cursor
	Select uCrsFt2
	Append From Dbf("ft2")
	Replace;
		ft2stamp 	With lnFtStamp;
		vdollocal	With 'Caixa';
		vdlocal		With 'C';
		modop1		With 'Visa';
		modop2		With 'MultiBanco';
		ousrinis	With m_chinis;
		ousrdata	With Date();
		ousrhora	With lnHora;
		usrinis		With m_chinis;
		usrdata		With Date();
		usrhora		With lnHora


	Return lcExecuteSQL
Endfunc

***********************************************
* Gravar linhas do documento de regulariza��o *
***********************************************
Function uf_PAGAMENTO_GravarRegFi
	Lparameters lnFtStamp, lnNmDoc, lnNdoc, lnTipoDoc, lnFno, lnHora

	Local lcSQL, lcExecuteSQL
	Store "" To lcExecuteSQL, lcSQL

	Local lnLordem, lnFiStamp
	Local lcRefDoc, lcRefDoc2, lcRegDesign
	lnLordem = 0
	Store "" To lnFiStamp, lcRefDoc, lcRefDoc2, lcRegDesign

	Local lcCampos, lcValores
	Store "" To lcCampos, lcValores

	Local lcValidaFiStamp
	Store .F. To lcValidaFiStamp


	If !Used("ucrsofistamp")
		Create Cursor ucrsofistamp (ofistamp c(30))
	Else
		Select ucrsofistamp
		Delete All
	Endif
	Local lnconta
	lnconta = 1

	Select uCrsFi2Reg
	Go Top
	Select fistamp As ofistamp, fistamp, ftstamp From uCrsFi2Reg Into Cursor uCrsStampsFiReg Readwrite
	&& Notas:
	* epvori = epv
	* pvori  = pv

	Select uCrsFi2Reg
	Go Top
	Scan
		Select uCrsFi2Reg
		Local lcPosR
		lcPosR=Recno("uCrsFi2Reg")

		&& corrigir linhas que estavam a duplicar nas regulariza��es
		Local lcrepetido
		lcrepetido = .F.

		If lnconta > 1
			Local lcofistampverif
			lcofistampverif = Alltrim(uCrsFi2Reg.fistamp)
			Select ucrsofistamp
			Go Top
			Scan
				If Alltrim(lcofistampverif) == Alltrim(ucrsofistamp.ofistamp) And !Empty(lcofistampverif) And lcrepetido = .F.
					lcrepetido = .T.
				Endif
			Endscan
		Endif

		*!*			SELECT uCrsFi2Reg
		*!*			TRY
		*!*				GO lcPosR
		*!*			CATCH
		*!*			ENDTRY

		If lcrepetido = .F.

			lnFiStamp 	= uf_gerais_stamp()
			lnLordem 	= lnLordem + 10000

			uf_gerais_actGrelha("",[uCrsCorrigeCc],[select edebf from cc where ftstamp=']+Alltrim(uCrsFi2Reg.ftstamp)+['])

			&& Por cada documento a regularizar criar linha a fazer referencia
			lcRefDoc = uCrsFi2Reg.ftstamp

			If !(lcRefDoc == lcRefDoc2)
				** guardar dados do documento de origem **
				TEXT TO lcSql NOSHOW TEXTMERGE
					SELECT TOP 1 fi.nmdoc, fi.fno, convert(varchar,rdata,103) as rdata, fi.ndoc,td.tiposaft + ' ' + ltrim(str(ft.ndoc)) + '/' + ltrim(str(ft.fno))  as docorig
					from fi (nolock)
					inner join ft (nolock) on ft.ftstamp=fi.ftstamp
					inner join td (nolock) on ft.ndoc=td.ndoc
					where fistamp = '<<ALLTRIM(uCrsFi2Reg.fistamp)>>'
				ENDTEXT
				If !uf_gerais_actGrelha("", [uCrsRefVdOrig],lcSQL)
					uf_perguntalt_chama("ATEN��O, OCORREU UM ERRO A GUARDAR REFER�NCIA DA VENDA ORIGINAL. POR FAVOR CONTACTE O SUPORTE","OK","",16)
				Else
					** criar design da refer�ncia **
					If Upper(Alltrim(ucrse1.pais)) == 'PORTUGAL'
						If Val(Right(Alltrim(uCrsRefVdOrig.rdata),4)) > 2012
							**lcRegDesign	= 'Ref. a Fatura Nr. ' + astr(uCrsRefVdOrig.fno) + '/' + astr(uCrsRefVdOrig.ndoc) + ' em ' + ALLTRIM(uCrsRefVdOrig.rdata)
							lcRegDesign	= 'Ref. a ' + Alltrim(Alltrim(uCrsRefVdOrig.nmdoc)) + ' Nr. ' + Alltrim(uCrsRefVdOrig.docorig)
						Else
							If !Empty(uCrsRefVdOrig.nmdoc)
								**lcRegDesign	= 'Ref. a ' + ALLTRIM(uCrsRefVdOrig.nmdoc) + ' Nr. ' + astr(uCrsRefVdOrig.fno) + ' em ' + ALLTRIM(uCrsRefVdOrig.rdata)
								lcRegDesign	= 'Ref. a ' + Alltrim(Alltrim(uCrsRefVdOrig.nmdoc)) + ' Nr. ' + Alltrim(uCrsRefVdOrig.docorig)
							Else
								lcRegDesign	= 'Ref. a documento externo  Nr. ' + Alltrim(ft2.u_docorig) + ' . '
							Endif
						Endif
					Else
						If Val(Right(Alltrim(uCrsRefVdOrig.rdata),4)) > 2012
							**lcRegDesign	= 'Anula��o ou rectifica��o de Fatura Nr. ' + astr(uCrsRefVdOrig.fno) + '/' + astr(uCrsRefVdOrig.ndoc) + ' em ' + ALLTRIM(uCrsRefVdOrig.rdata)
							lcRegDesign	= 'Anula��o de ' + Alltrim(Alltrim(uCrsRefVdOrig.nmdoc)) + ' Nr. ' + Alltrim(uCrsRefVdOrig.docorig)

						Else
							If !Empty(uCrsRefVdOrig.nmdoc)
								**lcRegDesign	= 'Anula��o ou rectifica��o de ' + ALLTRIM(uCrsRefVdOrig.nmdoc) + ' Nr. ' + astr(uCrsRefVdOrig.fno) + ' em ' + ALLTRIM(uCrsRefVdOrig.rdata)
								lcRegDesign	= 'Anula��o de ' + Alltrim(Alltrim(uCrsRefVdOrig.nmdoc)) + ' Nr. ' + Alltrim(uCrsRefVdOrig.docorig)
							Else
								lcRegDesign	= 'Anula��o de documento externo  Nr. ' + Alltrim(ft2.u_docorig) + ' . '
							Endif
						Endif
					Endif

					** Criar linha **
					TEXT TO lcSql NOSHOW TEXTMERGE
						INSERT INTO fi (
							fistamp, ftstamp
							,nmdoc, ndoc, fno, design
							,armazem, rdata, lordem
							,fivendedor, fivendnm
							,lobs2, tipodoc
							,ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora
							,id_Dispensa_Eletronica_D
							)
						values (
							'<<ALLTRIM(lnFiStamp)>>', '<<ALLTRIM(lnFtStamp)>>'
							,'<<lnNmDoc>>', <<lnNdoc>>, @fno, '<<ALLTRIM(lcRegDesign)>>'
							,<<myArmazem>>, '<<myInvoiceData>>', <<lnLordem>>
							,<<ft.vendedor>>, '<<Alltrim(ft.vendnm)>>'
							,'Ref.Vd.Orig.', '3'
							,'<<m_chinis>>', CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), '<<lnHora>>', '<<m_chinis>>', CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), '<<lnHora>>'
							,'<<ALLTRIM(fi.id_validacaoDem)>>'
							)
					ENDTEXT

					lcExecuteSQL  = lcExecuteSQL + Chr(13) + lcSQL

					** recalcular stamp e lordem **
					lnFiStamp 	= uf_gerais_stamp()
					lnLordem 	= lnLordem + 10000
					*******************************

					&& acrescentar nova linha ao cursor
					Select uCrsFi
					Append Blank
					Replace	fistamp		With Alltrim(lnFiStamp)
					Replace	ftstamp		With Alltrim(lnFtStamp)
					Replace	nmdoc		With lnNmDoc
					Replace	ndoc		With lnNdoc
					&&REPLACE	fno			WITH lnFno
					Replace	Design		With Alltrim(lcRegDesign)
					Replace	armazem		With myArmazem
					Replace	rdata		With Date()
					Replace	Lordem		With lnLordem
					Replace	fivendedor 	With ft.vendedor
					Replace	fivendnm	With Alltrim(ft.vendnm)
					Replace	lobs2		With 'Ref.Vd.Orig.'
					Replace	ousrinis	With m_chinis
					Replace	ousrdata	With Date()
					Replace	ousrhora	With lnHora
					Replace	usrinis		With m_chinis
					Replace	usrdata		With Date()
					Replace	usrhora		With lnHora

					If Used("uCrsRefVdOrig")
						fecha("uCrsRefVdOrig")
					Endif
				Endif
			Endif
			*********************************************************************

			&& Velifica��o dos pontos stribuidos anteriormente para retificar
			If myTipoCartao == 'Valor'
				**MESSAGEBOX('regulariza')
				Local lcTmpValcartao, lcEvaldesc, lcQtt, lcFtStampReg, lcShouldMultipl
				Store 0  To lcTmpValcartao, lcEvaldesc
				Store 1  To lcQtt
				Store '' To lcFtStampReg

				lcShouldMultipl = .F.

				If(Used("uCrsFi2Reg"))
					Select uCrsFi2Reg
					If(uCrsFi2Reg.qtt>0)
						lcQtt = uCrsFi2Reg.qtt
					Endif
				Endif

				If !Empty(uCrsFi2Reg.fistamp)
					TEXT TO lcSqlvalc NOSHOW TEXTMERGE

						EXEC up_vendas_getValCartaoDev <<lcQtt>>, '<<ALLTRIM(uCrsFi2Reg.fistamp)>>'

					ENDTEXT

					uf_gerais_actGrelha("", "uCrsVerifValCartao",lcSqlvalc )


					Select uCrsVerifValCartao
					lcTmpValcartao    = uCrsVerifValCartao.valcartao
					lcEvaldesc        = uCrsVerifValCartao.evaldesc
					lcFtStampReg      = Alltrim(uCrsVerifValCartao.ftstamp)

					** se deve multiplicar pela qtt, visto que esta a regularizar qtt menores que a original (feito para evitar problemas de arredondamento em qttRegularizada=qttARegularizar)
					lcShouldMultipl   = uCrsVerifValCartao.shouldMultipl


					** o desconto � total n�o deve ser alterado (feito para evitar problemas de arredondamento em qttRegularizada=qttARegularizar)
					If(Empty(lcShouldMultipl))
						lcQtt = 1
					Endif

					lcTmpValcartao  = Round(lcTmpValcartao * lcQtt,2)
					lcEvaldesc      = Round(lcEvaldesc     * lcQtt,2)

					fecha("uCrsVerifValCartaoOutrasLinhas")

					**valida se linha por valor na mesma venda
					TEXT TO lcSqlvalc NOSHOW TEXTMERGE
						SELECT valcartao = ISNULL(SUM(valcartao),0) FROM fi(nolock) WHERE ftstamp='<<lcFtStampReg>>' AND ref='V000001'
					ENDTEXT



					uf_gerais_actGrelha("", "uCrsVerifValCartaoOutrasLinhas",lcSqlvalc )


					If(uCrsVerifValCartaoOutrasLinhas.valcartao==0)
						lcEvaldesc = 0
					Endif


					Select uCrsFi2Reg

					Replace uCrsFi2Reg.valcartao With ((lcTmpValcartao * (-1)) + lcEvaldesc)

					Select uCrsFi2Reg


					fecha("uCrsVerifValCartao")
					fecha("uCrsVerifValCartaoOutrasLinhas")
				Endif
				Select uCrsFi2Reg
			Endif

			*!*				SELECT uCrsFi2Reg
			*!*				TRY
			*!*					GO lcPosR
			*!*				CATCH
			*!*				ENDTRY

			&& Notas:
			* campos rvpstamp e szzstamp s�o utilizados no atendimento para guardar o local de prescri��o e o medico, logo n�o devem ser guardados estes valores (campos)
			TEXT TO lcSql NOSHOW TEXTMERGE
				insert into fi (
					fistamp,
					nmdoc, fno, ref, design, qtt, tiliquido,
					etiliquido, unidade, unidad2, iva, ivaincl,
					codigo, tabiva, ndoc, armazem, fnoft, ndocft, ftanoft,
					ftregstamp,
					lobs2, litem2, litem, lobs3,
					rdata,
					cpoc, composto,
					lrecno,
					lordem,
					fmarcada, partes, altura, largura,
					oref, lote, usr1, usr2, usr3,
					usr4, usr5,
					ftstamp,
					cor, tam, stipo, fifref,
					tipodoc, familia, bistamp,
					stns, ficcusto, fincusto,
					ofistamp,
					pv, pvmoeda, epv,
					tmoeda,
					eaquisicao, custo, ecusto, slvu, eslvu,
					sltt, esltt, slvumoeda, slttmoeda,
					nccod, epromo,
					fivendedor, fivendnm,
					desconto, desc2, desc3, desc4, desc5, desc6, u_descval,
					iectin,eiectin,
					vbase,evbase,
					tliquido,num1,
					pcp,epcp,
					pvori,epvori,
					zona,
					morada,[local],codpost,
					telefone,email,
					tkhposlstamp,
					u_generico,
					u_cnp,u_psicont,u_bencont,
					u_psico, u_benzo, u_ettent1, u_ettent2
					,u_epref
					,pic
					,pvpmaxre,opcao,
					u_stock, u_epvp, u_ip, u_comp, u_diploma,
					ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora,
					marcada, u_genalt, u_txcomp
					,EVALDESC, VALDESC
					,u_codemb
					,pontos
					,campanhas
					,id_Dispensa_Eletronica_D
					,pvp4_fee
					,posologia
					,valcartao
				)
				Values (
					'<<alltrim(lnFiStamp)>>',
					'<<lnNmDoc>>', @fno, '<<uCrsFi2Reg.ref>>', '<<alltrim(uCrsFi2Reg.design)>>', <<uCrsFi2Reg.qtt>>, <<uCrsFi2Reg.tiliquido*(-1)>>,
					<<uCrsFi2Reg.etiliquido*(-1)>>, '<<uCrsFi2Reg.unidade>>', '<<uCrsFi2Reg.unidad2>>', <<uCrsFi2Reg.iva>>, <<IIF(uCrsFi2Reg.ivaincl,1,0)>>,
					'<<uCrsFi2Reg.codigo>>', <<uCrsFi2Reg.tabiva>>, <<lnNdoc>>, <<myArmazem>>, <<uCrsFi2Reg.fnoft>>, <<uCrsFi2Reg.ndocft>>, <<uCrsFi2Reg.ftanoft>>,
					'<<uCrsFi2Reg.ftstamp>>',
					'<<uCrsFi2Reg.lobs2>>', '<<uCrsFi2Reg.litem2>>', '<<uCrsFi2Reg.litem>>', '<<uCrsFi2Reg.lobs3>>',
					'<<myInvoiceData>>',
					<<uCrsFi2Reg.cpoc>>, <<IIF(uCrsFi2Reg.composto,1,0)>>,
					'<<alltrim(lnFiStamp)>>',
					<<lnLordem>>,
					<<IIF(uCrsFi2Reg.fmarcada,1,0)>>, <<uCrsFi2Reg.partes>>, <<uCrsFi2Reg.altura>>, <<uCrsFi2Reg.largura>>,
					'<<uCrsFi2Reg.oref>>', '<<uCrsFi2Reg.lote>>', '<<uCrsFi2Reg.usr1>>', '<<uCrsFi2Reg.usr2>>', '<<uCrsFi2Reg.usr3>>',
					'<<uCrsFi2Reg.usr4>>', '<<uCrsFi2Reg.usr5>>',
					'<<lnFtStamp>>',
					'<<uCrsFi2Reg.cor>>', '<<uCrsFi2Reg.tam>>', <<uCrsFi2Reg.stipo>>, '<<uCrsFi2Reg.fifref>>',
					<<lnTipoDoc>>, '<<uCrsFi2Reg.familia>>', '<<uCrsFi2Reg.bistamp>>',
					<<IIF(uCrsFi2Reg.stns,1,0)>>, '<<uCrsFi2Reg.ficcusto>>', '<<uCrsFi2Reg.fincusto>>',
					'<<uCrsFi2Reg.fistamp>>',
					<<uCrsFi2Reg.pv>>, <<uCrsFi2Reg.pvmoeda>>, <<uCrsFi2Reg.epv>>,
					<<uCrsFi2Reg.tmoeda>>,
					<<uCrsFi2Reg.eaquisicao>>, <<uCrsFi2Reg.custo>>, <<uCrsFi2Reg.ecusto>>, <<round(uCrsFi2Reg.pv/(uCrsFi2Reg.iva/100+1),2)>>, <<Round(uCrsFi2Reg.epv/(uCrsFi2Reg.iva/100+1),2)>>,
					<<uCrsFi2Reg.tiliquido/(uCrsFi2Reg.iva/100+1)>>, <<uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1)>>, <<uCrsFi2Reg.slvumoeda>>, <<uCrsFi2Reg.slttmoeda>>,
					'<<uCrsFi2Reg.nccod>>', <<IIF(uCrsFi2Reg.epromo,1,0)>>,
					<<ft.vendedor>>, '<<Alltrim(ft.vendnm)>>',
					<<uCrsFi2Reg.desconto>>, <<uCrsFi2Reg.desc2>>, <<uCrsFi2Reg.desc3>>, <<uCrsFi2Reg.desc4>>, <<uCrsFi2Reg.desc5>>, <<uCrsFi2Reg.desc6>>, <<uCrsFi2Reg.u_descval>>,
					<<uCrsFi2Reg.iectin>>,	<<uCrsFi2Reg.eiectin>>,
					<<uCrsFi2Reg.vbase>>, <<uCrsFi2Reg.evbase>>,
					<<uCrsFi2Reg.tliquido>>, <<uCrsFi2Reg.num1>>,
					<<uCrsFi2Reg.pcp>>, <<uCrsFi2Reg.epcp>>,
					<<uCrsFi2Reg.pv>>, <<uCrsFi2Reg.epv>>,
					'<<uCrsFi2Reg.zona>>',
					'<<uCrsFi2Reg.morada>>', '<<uCrsFi2Reg.local>>', '<<uCrsFi2Reg.codpost>>',
					'<<uCrsFi2Reg.telefone>>', '<<uCrsFi2Reg.email>>',
					'<<uCrsFi2Reg.tkhposlstamp>>',
					<<IIF(uCrsFi2Reg.u_generico,1,0)>>,
					'<<uCrsFi2Reg.u_cnp>>',	<<uCrsFi2Reg.u_psicont>>, <<uCrsFi2Reg.u_bencont>>,
					<<IIF(uCrsFi2Reg.u_psico,1,0)>>, <<IIF(uCrsFi2Reg.u_benzo,1,0)>>, <<uCrsFi2Reg.u_ettent1>>,	<<uCrsFi2Reg.u_ettent2>>
					,<<uCrsFi2Reg.u_epref>>
					,<<uCrsFi2Reg.pic>>
					,<<uCrsFi2Reg.pvpmaxre>>,'<<uCrsFi2Reg.opcao>>'
					,<<uCrsFi2Reg.u_stock>>, <<uCrsFi2Reg.u_epvp>>, <<IIF(uCrsFi2Reg.u_ip,1,0)>>, <<IIF(uCrsFi2Reg.u_comp,1,0)>>, '<<uCrsFi2Reg.u_diploma>>',
					'<<m_chinis>>', CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), '<<lnHora>>', '<<m_chinis>>', CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), '<<lnHora>>',
					<<IIF(uCrsFi2Reg.marcada,1,0)>>, <<IIF(uCrsFi2Reg.u_genalt,1,0)>>, <<uCrsFi2Reg.u_txcomp>>
					,<<IIF(uCrsFi2Reg.qtt==0,0,uCrsFi2Reg.u_descval / uCrsFi2Reg.qtt)>>
					,<<IIF(uCrsFi2Reg.qtt==0,0,uCrsFi2Reg.u_descval / uCrsFi2Reg.qtt)* 200.482>>
					,'<<uCrsFi2Reg.u_codemb>>'
					,<<uCrsFi2Reg.pontos*-1>>
					,'<<uCrsFi2Reg.campanhas>>'
					,'<<ALLTRIM(fi.id_validacaoDem)>>'
					,<<uCrsFi2Reg.pvp4_fee>>
					,'<<ALLTRIM(fi.posologia)>>'
					,<<uCrsFi2Reg.valcartao>>
				)
			ENDTEXT

			lcExecuteSQL  = lcExecuteSQL + Chr(13) + lcSQL

			&& acrescentar nova linha ao cursor
			Select uCrsFi
			Go Top
			Scan
				If Alltrim(uCrsFi.fistamp) == Alltrim(uCrsFi2Reg.fistamp)
					lcValidaFiStamp = .T.
				Endif
			Endscan
			If !lcValidaFiStamp

				*!*					SELECT uCrsFi2Reg
				*!*					TRY
				*!*						GO lcPosR
				*!*					CATCH
				*!*					ENDTRY

				lcStampInsCrs = uCrsFi2Reg.fistamp
				Select * From uCrsFi Union All Select * From uCrsFi2Reg Where uCrsFi2Reg.fistamp=lcStampInsCrs Into Cursor uCrsFi Readwrite
				Go Bottom
				Replace;
					fistamp		With Alltrim(lnFiStamp);
					nmdoc		With lnNmDoc;
					ndoc		With lnNdoc;
					rdata		With Date();
					ftstamp		With lnFtStamp;
					tipodoc		With lnTipoDoc;
					Lordem		With lnLordem;
					ofistamp	With uCrsFi2Reg.fistamp;
					slvu		With Round(uCrsFi2Reg.Pv/(uCrsFi2Reg.iva/100+1),2);
					eslvu		With Round(uCrsFi2Reg.epv/(uCrsFi2Reg.iva/100+1),2);
					sltt		With uCrsFi2Reg.tiliquido/(uCrsFi2Reg.iva/100+1);
					esltt		With uCrsFi2Reg.etiliquido/(uCrsFi2Reg.iva/100+1);
					ousrinis	With m_chinis;
					ousrdata	With Date();
					ousrhora	With lnHora;
					usrinis		With m_chinis;
					usrdata		With Date();
					usrhora		With lnHora;
					marcada		With Iif(uCrsFi2Reg.marcada,.T.,.F.);
					evaldesc 	With Iif(uCrsFi2Reg.qtt==0, 0, (uCrsFi2Reg.u_descval / uCrsFi2Reg.qtt));
					valdesc 	With Iif(uCrsFi2Reg.qtt==0, 0, ((uCrsFi2Reg.u_descval / uCrsFi2Reg.qtt) * 200.482))
			Else
				lcValidaFiStamp = .F.
			Endif

			** Actualizar vendas originais  **
			TEXT to lcSql noshow textmerge
				Update ft
				Set facturada=1, fnoft=@fno , nmdocft='<<alltrim(lnNmDoc)>>'
				where ftstamp='<<alltrim(uCrsFi2Reg.ftstamp)>>'

				Update cc
				Set edebf=<<uCrsCorrigeCc.edebf>>
				where ftstamp='<<alltrim(uCrsFi2Reg.ftstamp)>>'
			ENDTEXT

			lcExecuteSQL  = lcExecuteSQL + Chr(13) + lcSQL

			Select uCrsStampsFiReg
			Update uCrsStampsFiReg Set uCrsStampsFiReg.fistamp=Alltrim(lnFiStamp) Where uCrsStampsFiReg.ofistamp=uCrsFi2Reg.fistamp

			&& Guardar stamp para controlar as linhas de referencia aos documentos a regularizar
			Select uCrsFi2Reg
			lcRefDoc2	= uCrsFi2Reg.ftstamp
			**
		Endif

		lnconta = lnconta + 1

		Select uCrsFi2Reg
		Try
			Go lcPosR
		Catch
		Endtry

		Local ofistamplin
		ofistamplin = Alltrim(uCrsFi2Reg.fistamp)

		Select ucrsofistamp
		Go Bottom
		Append Blank
		Replace ucrsofistamp.ofistamp With Alltrim(ofistamplin)

		Select uCrsFi2Reg
		Try
			Go lcPosR
		Catch
		Endtry
	Endscan

	If myTipoCartao == 'Valor'
		lcSQL = ''

		Local uv_valReporCartao
		Store 0 To uv_valReporCartao

		If uf_gerais_getParameter_site("ADM0000000198","BOOL", mySite) And Wexist("PAGAMENTO")

			Select UcrsConfigModPag
			Go Top

			Locate For UcrsConfigModPag.valcartao And !UcrsConfigModPag.inativo

			If Found()

				Do Case
					Case UcrsConfigModPag.ref = "01"
						uv_valReporCartao = Abs(Val(PAGAMENTO.txtDinheiro.Value))
					Case UcrsConfigModPag.ref = "02"
						uv_valReporCartao = Abs(Val(PAGAMENTO.txtMB.Value))
					Case UcrsConfigModPag.ref = "03"
						uv_valReporCartao = Abs(Val(PAGAMENTO.txtVisa.Value))
					Case UcrsConfigModPag.ref = "04"
						uv_valReporCartao = Abs(Val(PAGAMENTO.txtCheque.Value))
					Case UcrsConfigModPag.ref = "05"
						uv_valReporCartao = Abs(Val(PAGAMENTO.txtModPag3.Value))
					Case UcrsConfigModPag.ref = "06"
						uv_valReporCartao = Abs(Val(PAGAMENTO.txtModPag4.Value))
					Case UcrsConfigModPag.ref = "07"
						uv_valReporCartao = Abs(Val(PAGAMENTO.txtModPag5.Value))
					Case UcrsConfigModPag.ref = "08"
						uv_valReporCartao = Abs(Val(PAGAMENTO.txtModPag6.Value))
				Endcase

			Endif

		Endif

		uv_insertValCartao = .T.

		&& n�o confiar no lnFtStamp, visto que foi gerado
		uf_PAGAMENTO_retornaClienteADescontarValorCartao(ft.no, ft.estab, ft.ftstamp)

		TEXT TO uv_queryCartao TEXTMERGE NOSHOW
			UPDATE b_fidel
			SET valcartao = valcartao + ISNULL((select SUM(fi.valcartao) from fi(nolock) WHERE fi.ftstamp='<<ALLTRIM(lnFtStamp)>>'),0) + <<uv_valReporCartao>>
			,usrdata = GETDATE()
			WHERE b_fidel.clno=<<ucrClNumberCardTemp.no>> and b_fidel.clestab=<<ucrClNumberCardTemp.estab>>
		ENDTEXT

		If uf_gerais_getParameter_site('ADM0000000133', 'BOOL', mySite)

			Set Century On
			uv_curDT = Datetime()


			Select uCrsFi
			Calculate Sum(valcartao) For ftstamp = lnFtStamp To uv_totValcartao

			uv_totValcartao = uv_totValcartao + uv_valReporCartao

			If uv_totValcartao <> 0

				TEXT TO uv_sql TEXTMERGE NOSHOW

					UPDATE b_fidel
					SET valcartao = valcartao + <<uv_totValcartao>>
					,valcartaousado = valcartaoUsado - ABS(<<uv_valReporCartao>>)
					,usrdata = '<<uv_curDT>>'
					WHERE b_fidel.clno=<<ucrClNumberCardTemp.no>> and b_fidel.clestab=<<ucrClNumberCardTemp.estab>>

				ENDTEXT

				If Used("uc_cartaoCentral")
					fecha("uc_cartaoCentral")
				Endif


				uv_returnQuery = "select * from b_fidel(nolock) where b_fidel.clno= " + astr(ucrClNumberCardTemp.no) + " and b_fidel.clestab= " + astr(ucrClNumberCardTemp.estab)

				If !uf_gerais_runSQLCentral(uv_sql, "uc_cartaoCentral", uv_returnQuery)
					uf_perguntalt_chama("OCORREU UM PROBLEMA A ATUALIZAR O CART�O DE CLIENTE NA CENTRAL." + Chr(13) + "N�O SER�O GUARDADOS OS DADOS DO CART�O DE CLIENTE." + Chr(13) + "POR FAVOR ADICIONE OS PONTOS MANUALMENTE.","OK","",16)

					uv_nrCartao = uf_gerais_getUmValor("b_fidel", "nrCartao", "clno = " + astr(ucrClNumberCardTemp.no) + " and clestab = " + astr(ucrClNumberCardTemp.estab) + " and inactivo = 0")
					TEXT TO uv_dadosCliente TEXTMERGE NOSHOW

						No: <<ASTR(ucrClNumberCardTemp.no)>>;
						Estab: <<ASTR(ucrClNumberCardTemp.estab)>>;
						Cart�o: <<ALLTRIM(uv_nrCartao)>>

					ENDTEXT
					uf_gerais_registaOcorrencia("Pontos Cartao", "Erro a atualizar os pontos do cart�o na central.", 1, astr(uv_totValcartao,14,4), Alltrim(uv_dadosCliente), Iif(!Empty(nrAtendimento), Alltrim(nrAtendimento), Alltrim(lnFtStamp)))

					uv_insertValCartao = .F.
				Endif

				If uv_insertValCartao

					Set Century On

					Select uc_cartaoCentral

					TEXT TO uv_queryCartao TEXTMERGE NOSHOW
						UPDATE b_fidel
						SET valcartao = <<uc_cartaoCentral.valcartao>>
						, valcartaohist = <<uc_cartaoCentral.valcartaohist>>
						, valcartaoUsado = <<uc_cartaoCentral.valcartaoUsado>>
						, usrdata = '<<uv_curDT>>'
						WHERE b_fidel.clno=<<uc_cartaoCentral.clno>> and b_fidel.clestab=<<uc_cartaoCentral.clestab>> AND b_fidel.nrcartao = '<<ALLTRIM(uc_cartaoCentral.nrcartao)>>'
					ENDTEXT

				Endif

			Endif

		Endif

		If uv_insertValCartao

			TEXT TO lcSql NOSHOW TEXTMERGE
				UPDATE ft2 SET valcartao=ISNULL((select SUM(fi.valcartao) from fi(nolock) WHERE fi.ftstamp='<<ALLTRIM(lnFtStamp)>>' AND fi.valcartao>0),0) WHERE ft2stamp='<<ALLTRIM(lnFtStamp)>>'
				UPDATE ft2 SET valcartaoutilizado=ISNULL((select SUM(fi.valcartao) from fi(nolock) WHERE fi.ftstamp='<<ALLTRIM(lnFtStamp)>>' AND fi.valcartao<0),0) WHERE ft2stamp='<<ALLTRIM(lnFtStamp)>>'

				<<ALLTRIM(uv_queryCartao)>>
			ENDTEXT
			lcExecuteSQL  = lcExecuteSQL + Chr(13) + lcSQL

		Endif



		If(Used("ucrClNumberCardTemp"))
			fecha("ucrClNumberCardTemp")
		Endif



	Endif



	&& ACTUALIZAR OFISTAMP DAS VENDAS NOVAS
	If Used("uCrsNovosStamps")
		Select uCrsNovosStamps
		Go Top
		Scan
			Select uCrsStampsFiReg
			Go Top
			Scan
				TEXT to lcSql noshow textmerge
					Update fi
					Set ofistamp = '<<alltrim(uCrsStampsFiReg.fistamp)>>'
					where fi.fistamp = '<<alltrim(uCrsNovosStamps.fistamp)>>' AND fi.ofistamp = '<<alltrim(uCrsStampsFiReg.ofistamp)>>'
				ENDTEXT

				lcExecuteSQL  = lcExecuteSQL + Chr(13) + lcSQL

			Endscan

			Select uCrsNovosStamps
		Endscan
	Endif
	**

	&& Marcar Vendas Regularizadas
	Select ucrsvale
	Go Top
	Scan
		TEXT TO lcSql NOSHOW TEXTMERGE
			UPDATE ft2
			SET u_vdsusprg=1
			where ft2stamp = '<<alltrim(uCrsVale.ftstamp)>>'
		ENDTEXT

		lcExecuteSQL  = lcExecuteSQL + Chr(13) + lcSQL

	Endscan
	***************************

	Return lcExecuteSQL
Endfunc


**
Function uf_PAGAMENTO_actualizaValesDescGen
	&& Verificar se usa vale desconto generico
	If !uf_gerais_getParameter("ADM0000000158","BOOL")
		Return .F.
	Endif

	*****************************************

	Local lcValor
	Store 0 To lcValor

	&& CASO EXISTA VALE, ABATE O VALE
	Select fi
	Go Top
	Scan
		If Upper(Left(Alltrim(fi.u_refVale),2))=='VG'
			lcSQL = ''
			TEXT TO lcSql NOSHOW textmerge
				UPDATE B_fidelvalegen
				set abatido=1, usrdata='<<myInvoiceData>>', usr=<<ch_userno>>
				where ref='<<ALLTRIM(fi.u_refvale)>>'
			ENDTEXT
			If !uf_gerais_actGrelha("","",lcSQL)
				uf_perguntalt_chama("N�O FOI POSS�VEL ABATER O VALE DE DESCONTO, POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			Endif
		Endif
	Endscan
	Select fi
	Go Top
	************************************

	**************************************************
	** Verifica promo��o a aplicar ao vale gen�rico **
	**************************************************
	** Para j� fica est�tico						**
	Local lcValor
	Store 0.00 To lcValor

	lcValor = Val(Transform(PAGAMENTO.txtValAtend.Value,'99999999.99'))

	lcValor = Round((lcValor * 0.15),2)

	If !Empty(lcValor)
		If lcValor > 0
			Do While !(lcValor==0)
				If lcValor > 5
					lcValor = lcValor - 5
					Select ft
					uf_pagamento_imprimeValeDescGen(ft.no, ft.estab, 5.00, uCrsCLEF.utstamp,ft.Nome)
				Else
					Select ft
					uf_pagamento_imprimeValeDescGen(ft.no, ft.estab, lcValor, uCrsCLEF.utstamp,ft.Nome)
					Exit
				Endif
			Enddo
		Endif
	Endif
	**************************************************
Endfunc


********************************************
** Impress�o de Vale de Desconto Gen�rico **
********************************************
Function uf_pagamento_imprimeValeDescGen
	Lparameters tcNo, tcEstab, tcValor, tcClStamp, tcClNome, uv_dev

	If !uv_dev

		&& Verificar se usa o Vale de Desconto Generico
		If !uf_gerais_getParameter("ADM0000000158","BOOL")
			Return .F.
		Endif

		** Verificar se avisa que vai imprimir vale **
		If uf_gerais_getParameter("ADM0000000159","BOOL")
			If Empty(uf_gerais_getParameter("ADM0000000260","text")) Or Upper(Alltrim(uf_gerais_getParameter("ADM0000000260","text"))) == "EURO"
				If !uf_perguntalt_chama("ATEN��O: VAI IMPRIMIR UM VALE DE DESCONTO NO VALOR DE: " + Alltrim(Str(tcValor,15,2)) + " EUROS."+Chr(10)+Chr(10)+"TEM A CERTEZA QUE PRETENDE CONTINUAR?","Sim","N�o")
					Return .F.
				Endif
			Else
				If !uf_perguntalt_chama("ATEN��O: VAI IMPRIMIR UM VALE DE DESCONTO NO VALOR DE: " + Alltrim(Str(tcValor,15,2)) + " " + Alltrim(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite)) + "."+Chr(10)+Chr(10)+"TEM A CERTEZA QUE PRETENDE CONTINUAR?","Sim","N�o")
					Return .F.
				Endif
			Endif
		Endif

	Endif
	**********************************************

	If Empty(tcNo)
		uf_perguntalt_chama("N�O PODE IMPRIMIR VALE DE DESCONTO GEN�RICO, SEM IDENTIFICAR O CLIENTE.","OK","",16)
		Return .F.
	Endif

	If Empty(tcValor)
		Return .F.
	Endif

	Local lcSQL, lcUltRef, nrVale
	Store '' To lcSQL, lcUltRef
	Store 0 To nrVale

	&& verificar ultimo vale
	If uf_gerais_actGrelha("",[uCrsUltValeGen],[select ISNULL(MAX(nr),0) as nr from b_fidelValeGen (nolock) where YEAR(ousrdata)=YEAR(getdate())])
		If Reccount("uCrsUltValeGen") > 0
			** incrementar ref vale **
			nrVale = uCrsUltValeGen.nr + 1
			lcUltRef = astr(nrVale)
			Do While Len(lcUltRef) < 6
				lcUltRef = '0' + lcUltRef
			Enddo
			lcUltRef = 'VG' + Right(Str(Year(Date())),2) + lcUltRef
			**************************
		Else
			nrVale		= 1
			lcUltRef 	= 'VG' + Right(Str(Year(Date())),2) + '000001'
		Endif
		fecha("uCrsUltValeGen")
	Else
		uf_perguntalt_chama("OCORREU UM PROBLEMA A REGISTAR O VALE. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		Return .F.
	Endif
	***************************

	TEXT TO lcSql TEXTMERGE NOSHOW
		INSERT INTO b_fidelValeGen
			(
			stamp
			,ref, nr, valor
			,clstamp
			,ousr, usr
			)
		values
			(
			'<<ALLTRIM(str(YEAR(DATE())))+ALLTRIM(lcUltRef)>>'
			,'<<ALLTRIM(lcUltRef)>>', <<nrVale>>, <<tcValor>>
			,'<<ALLTRIM(tcClStamp)>>'
			,'<<ALLTRIM(m_chinis)>>', '<<ALLTRIM(m_chinis)>>'
			)
	ENDTEXT
	If !uf_gerais_actGrelha("","",lcSQL)
		uf_perguntalt_chama("OCORREU UM PROBLEMA A REGISTAR O VALE. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		Return .F.
	Endif
	**
	Messagebox(18)
	**IF uf_gerais_getParameter_site('ADM0000000136', 'BOOL', mySite)
	If uf_gerais_getUmValor("B_terminal", "usaDriverWin", "terminal = '" + Alltrim(myTerm) + "'")

		Public upv_moeda, upv_validade, upv_intrans, upv_tcNo, upv_tcEstab, upv_tcValor, upv_tcClNome, upv_lcUltRef

		upv_moeda =  Alltrim(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))
		upv_validade = uf_gerais_getParameter("ADM0000000017","NUM")
		upv_intrans = uf_gerais_getParameter("ADM0000000020","BOOL")
		upv_tcNo = tcNo
		upv_tcEstab = tcEstab
		upv_tcValor = tcValor
		upv_tcClNome = tcClNome
		upv_lcUltRef = lcUltRef

		uv_idImp = uf_gerais_getUmValor("B_impressoes", "id", "tabela = 'FT' and nomeImpressao = 'Tal�o Vale Cliente Atendimento'")

		uf_imprimirgerais_sendprinter_talao(uv_idImp , .F., .F.)

	Else

		*** validar impressora ***
		Local lcValidaImp
		Store .F. To lcValidaImp
		lcValidaImp = uf_gerais_setImpressoraPOS(.T.)
		If lcValidaImp=.F.
			uf_perguntalt_chama("N�O FOI POSS�VEL IMPRIMIR O VALE PORQUE A IMPRESSORA N�O EST� OPERACIONAL.","OK","",48)
			Return .F.
		Endif
		**

		uf_gerais_criarCabecalhoPOS()

		uf_gerais_textoVermPOS()

		** IMPRIMIR O TIPO DE DOCUMENTO **
		?"	*** VALE DE DESCONTO ***"
		?"DATA IMPRESSAO: " + astr(Datetime()+(difhoraria*3600))


		If uf_gerais_getParameter("ADM0000000017","NUM")>0
			?"VALIDADE: "+ Alltrim(Str(Round(uf_gerais_getParameter("ADM0000000017","NUM"),0))) +" MESES"
		Endif

		If uf_gerais_getParameter("ADM0000000020","BOOL")
			?"VALE PESSOAL E INTRANSMISSIVEL"
		Endif

		?"VALIDO A PARTIR DO DIA SEGUINTE"
		If tcValor == 5
			If Empty(uf_gerais_getParameter("ADM0000000260","text")) Or Upper(Alltrim(uf_gerais_getParameter("ADM0000000260","text"))) == "EURO"
				?"VALIDO EM COMPRAS SUPERIORES A 20 EUROS"
			Else
				?"VALIDO EM COMPRAS SUPERIORES A 20 " + Alltrim(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))
			Endif
		Endif

		uf_gerais_resetCorPOS()

		uf_gerais_separadorPOS()

		?"Operador: " + Alltrim(ch_vendnm) + " [" + astr(ch_vendedor) + "]"
		?"Cliente: " + Alltrim(tcClNome) + " [" + astr(tcNo) + "][" + astr(tcEstab) + "]"
		uf_gerais_separadorPOS()
		?

		uf_gerais_textoCJPOS()

		???	Chr(27)+Chr(33)+Chr(2)
		If Empty(uf_gerais_getParameter("ADM0000000260","text")) Or Upper(Alltrim(uf_gerais_getParameter("ADM0000000260","text"))) == "EURO"
			?"VALOR DO VALE: " + Alltrim(Str(tcValor,15,2)) + " EUROS*"
		Else
			?"VALOR DO VALE: " + Alltrim(Str(tcValor,15,2)) + " " + Alltrim(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))+ "*"
		Endif
		???	Chr(27)+Chr(33)+Chr(1)
		?
		???	Chr(29)+Chr(104)+Chr(50) + Chr(29)+Chr(72)+Chr(2) + Chr(29)+Chr(119)+Chr(2) + Chr(29)+Chr(107)+Chr(4) + Upper(Alltrim(Left(lcUltRef,10))) + Chr(0)

		****** IMPRESS�O DO RODAP� ******
		uf_gerais_separadorPOS()

		?"Processado por Computador"
		?"* O codigo de barras ilegivel invalida o vale. "
		?"Gratos pela sua compreensao. *"

		uf_gerais_textoLJPOS()

		uf_gerais_feedCutPOS()

		** LIBERTAR IMPRESSORA, COLOCAR NO DEFAULT **
		uf_gerais_setImpressoraPOS(.F.)

	Endif
Endfunc


** funcao para imprimir tal�es no final do Pagamento -
Function uf_PAGAMENTO_eventoImpressaoPos
	Lparameters lcTipoImp, uv_reImp

	Local i, lcNrVd, lcNrAtend
	Store 0 To i
	Store 1 To lcNrVd
	Messagebox(19)

	If ATENDIMENTO.impA4
		If Used("UCRSIMPTALAOPOS")
			Select uCrsImpTalaoPos
			Go Top
			
			lcNrAtend = uf_gerais_getUmValor("ft", "u_nrAtend", "ftstamp = '" + Alltrim(uCrsImpTalaoPos.stamp) + "'")
			uf_imprimirgerais_Chama('ATENDIMENTO',.F., .F.,.F., lcNrAtend)
					
			
		ENDIF 
	Else
		If !uf_gerais_getUmValor("B_terminal", "usaDriverWin", "terminal = '" + Alltrim(myTerm) + "'")
			** tratar tipo de tal�es

			If Used("UCRSIMPTALAOPOS")
				Select uCrsImpTalaoPos
				Go Top
				Scan
					TEXT TO lcSQL TEXTMERGE NOSHOW
					SELECT cobrado FROM ft (nolock) WHERE ftstamp='<<ALLTRIM(uCrsImpTalaoPos.stamp)>>'
					ENDTEXT
					uf_gerais_actGrelha("", "uCrscobrado", lcSQL)
					If uCrscobrado.cobrado=.T.
						Select uCrsImpTalaoPos
						Replace uCrsImpTalaoPos.Susp With .T.
					Else
						Select uCrsImpTalaoPos
						Replace uCrsImpTalaoPos.Susp With .F.
					Endif
					fecha("uCrscobrado")

					Select uCrsImpTalaoPos
					TEXT TO lcSQL TEXTMERGE NOSHOW
					select design from fi (nolock) where ftstamp='<<ALLTRIM(uCrsImpTalaoPos.stamp)>>' and design like '%Ad. Reserva%'
					ENDTEXT
					uf_gerais_actGrelha("", "uCrsResAd", lcSQL)
					If Reccount("uCrsResAd")>0
						Select uCrsImpTalaoPos
						Replace uCrsImpTalaoPos.AdiReserva With .T.
					Else
						Select uCrsImpTalaoPos
						Replace uCrsImpTalaoPos.AdiReserva With .F.
					Endif
					fecha("uCrsResAd")

					Select uCrsImpTalaoPos
				Endscan
			Endif


			lcNrVd = PAGAMENTO.nTaloes

			** Valida��o para n�o imprimir tal�es de psicotr�picos repetidos 2015/01/19
			If Used("uCrsValidaImpPsico")
				fecha("uCrsValidaImpPsico")
			Endif

			&& definir valores de pagamento (modos de pagamento e troco) para imprimir tal�o
			Local lcTroco, lcTotal, lcModoPag1, lcModoPag2, lcModoPag3, lcModoPag4, lcModoPag5, lcModoPag6, lcModoPag7, lcModoPag8
			Store 0 To lcTroco, lcTotal, lcModoPag1, lcModoPag2, lcModoPag3, lcModoPag4, lcModoPag5, lcModoPag6, lcModoPag7, lcModoPag8

			lcTroco 	= Iif(Empty(PAGAMENTO.txtTroco.Value), 0, PAGAMENTO.txtTroco.Value)
			lcTotal 	= Iif(Empty(PAGAMENTO.txtValAtend.Value), 0, PAGAMENTO.txtValAtend.Value)
			lcModoPag1 = Iif(Empty(PAGAMENTO.txtDinheiro.Value), 0, Val(PAGAMENTO.txtDinheiro.Value))
			lcModoPag2 = Iif(Empty(PAGAMENTO.txtMB.Value), 0, Val(PAGAMENTO.txtMB.Value))
			lcModoPag3 = Iif(Empty(PAGAMENTO.txtVisa.Value), 0, Val(PAGAMENTO.txtVisa.Value))
			lcModoPag4 = Iif(Empty(PAGAMENTO.txtCheque.Value), 0, Val(PAGAMENTO.txtCheque.Value))
			lcModoPag5 = Iif(Empty(PAGAMENTO.txtModPag3.Value), 0, Val(PAGAMENTO.txtModPag3.Value))
			lcModoPag6 = Iif(Empty(PAGAMENTO.txtModPag4.Value), 0, Val(PAGAMENTO.txtModPag4.Value))
			lcModoPag7 = Iif(Empty(PAGAMENTO.txtModPag5.Value), 0, Val(PAGAMENTO.txtModPag5.Value))
			lcModoPag8 = Iif(Empty(PAGAMENTO.txtModPag6.Value), 0, Val(PAGAMENTO.txtModPag6.Value))

			&& chama funcoes de impressao
			Select ucrse1
			If Used("uCrsNrReserva")
				Select * From uCrsNrReserva Into Cursor uCrsNrReservaAux Readwrite
			Endif
			If lcNrVd != 0 Or (lcNrVd == 0 And ft2.u_vddom == .T.)
				If pTalSep = .F.

					If Used("UCRSIMPTALAOPOS")
						Select uCrsImpTalaoPos
						Locate For uCrsImpTalaoPos.tipo==myFact And uCrsImpTalaoPos.Susp==.F.

						If Found()

							uv_numTaloes = Floor(uf_gerais_getParameter("ADM0000000008","NUM"))
							If Type("upv_altNumTaloes") <> "U"
								If !upv_altNumTaloes
									If uv_numTaloes <> 0
										lcNrVd = uv_numTaloes
									Endif
								Endif
							Else
								If uv_numTaloes <> 0
									lcNrVd = uv_numTaloes
								Endif
							Endif
						Else
							lcNrVd = PAGAMENTO.nTaloes
						Endif
					Endif

					&& Imprimie talao conjunto
					For i = 1 To lcNrVd
						If !uf_gerais_getParameter("ADM0000000126","BOOL")
							If Used("UCRSIMPTALAOPOS")
								uf_imprimirpos_impTalaoConjuntoPos(i, lcTroco, lcTotal, lcModoPag1, lcModoPag2, lcModoPag3, lcModoPag4, lcModoPag5, lcModoPag6, lcModoPag7, lcModoPag8)
							Endif
							** uf_imprimirPOS_ValImpAtendimento_AT(lcNratendPrint, lcAnoPrint, lcNoClPrint)
						Endif
					Endfor
				Else

					&& VENDAS A DINHEIRO (n�o suspensas)
					For i=1 To lcNrVd
						If Used("UCRSIMPTALAOPOS")
							Select uCrsImpTalaoPos
							Go Top
							Scan For (uCrsImpTalaoPos.tipo == myVd And uCrsImpTalaoPos.Susp == .F.)
								If i==1
									uf_imprimirPOS_ImpTalao(uCrsImpTalaoPos.stamp,"ATENDIMENTO", .F., 0)
									Select uCrsImpTalaoPos
								Else
									uf_imprimirPOS_ImpTalao(uCrsImpTalaoPos.stamp,"ATENDIMENTO", .T., 0)
									Select uCrsImpTalaoPos
								Endif
							Endscan
						Endif
					Endfor

					lcNrVdCred = lcNrVd

					uv_numTaloes = Floor(uf_gerais_getParameter("ADM0000000008","NUM"))
					If Type("upv_altNumTaloes") <> "U"
						If !upv_altNumTaloes
							If uv_numTaloes <> 0
								lcNrVdCred = uv_numTaloes
							Endif
						Endif
					Else
						If uv_numTaloes <> 0
							lcNrVdCred = uv_numTaloes
						Endif
					Endif

					&& FACTURAS (n�o suspensas)
					For i=1 To lcNrVdCred
						If Used("UCRSIMPTALAOPOS")
							Select uCrsImpTalaoPos
							Go Top
							Scan For (uCrsImpTalaoPos.tipo==myFact And uCrsImpTalaoPos.Susp==.F.)
								If i==1
									uf_imprimirPOS_ImpTalao(uCrsImpTalaoPos.stamp,"ATENDIMENTO",.F.,0)
									Select uCrsImpTalaoPos
								Else
									uf_imprimirPOS_ImpTalao(uCrsImpTalaoPos.stamp,"ATENDIMENTO",.T.,0)
									Select uCrsImpTalaoPos
								Endif
							Endscan
						Endif
					Endfor
				Endif
			Endif

			** imprimir reserva

			If Used("uCrsNrReservaAux") And uf_gerais_getParameter("ADM0000000318","BOOL")
				Select uCrsNrReservaAux
				Go Top
				If !Empty(uCrsNrReservaAux.nrReserva)
					For i=1 To uf_gerais_getParameter("ADM0000000318","NUM")
						uf_imprimirPOS_ImpTalao_reservas_comAD(Alltrim(uCrsNrReservaAux.stamp))
					Endfor
				Endif
				fecha("uCrsNrReservaAux")
			Endif

			If Used("uCrsReservaBo") And Reccount("uCrsReservaBo")>0

				For i=1 To uf_gerais_getParameter("ADM0000000148","NUM")
					uf_imprimirPOS_ImpTalao_reservas(Alltrim(uCrsReservaBo.bostamp))
				Endfor
				Select uCrsReservaBo
				Delete All

				If(Used("uCrsImpTalaoPos"))
					Select uCrsImpTalaoPos
				Endif

			Endif

			&& VENDAS E FACTURAS SUSPENSAS
			&& Vendas com adiantamento Lu�s Leal 25/10/2016
			If lcNrVd != 0
				i=0
				For i=1 To Round(uf_gerais_getParameter("ADM0000000007","NUM"),0)
					If Used("UCRSIMPTALAOPOS")
						Select uCrsImpTalaoPos
						Go Top
						Scan For ((uCrsImpTalaoPos.tipo == myVd Or uCrsImpTalaoPos.tipo == myFact) And uCrsImpTalaoPos.Susp == .T.) Or uCrsImpTalaoPos.AdiReserva = .T.
							TEXT TO lcSql NOSHOW TEXTMERGE
								select nmdoc from ft (nolock) where ftstamp='<<ALLTRIM(uCrsImpTalaoPos.stamp)>>'
							ENDTEXT
							uf_gerais_actGrelha("","ucrsnomedoc",lcSQL)
							If Alltrim(ucrsnomedoc.nmdoc) <> 'Reg. a Cliente'
								Select uCrsImpTalaoPos
								uf_imprimirPOS_ImpTalao(uCrsImpTalaoPos.stamp,"ATENDIMENTO",.F.,0, uCrsImpTalaoPos.AdiReserva)
								Select uCrsImpTalaoPos
							Endif
						Endscan
					Endif
				Endfor
			Endif

			&& regulariza��es
			If Used("uCrsVale")
				If lcNrVd!=0
					If uf_gerais_getParameter("ADM0000000009","BOOL") == .T.
						i=0
						For i=1 To Round(uf_gerais_getParameter("ADM0000000016","NUM"),0)
							If myOffline
								If Used("UCRSIMPTALAOPOS")
									Select uCrsImpTalaoPos
									Go Top
									Scan For (uCrsImpTalaoPos.tipo==85 And uCrsImpTalaoPos.Susp==.F.)
										If i==1
											uf_imprimirPOS_ImpTalao(uCrsImpTalaoPos.stamp,"ATENDIMENTO",.F.,85)
											Select uCrsImpTalaoPos
										Else
											uf_imprimirPOS_ImpTalao(uCrsImpTalaoPos.stamp,"ATENDIMENTO",.T.,85)
											Select uCrsImpTalaoPos
										Endif
									Endscan
								Endif
							Else
								If Used("UCRSIMPTALAOPOS")
									Select uCrsImpTalaoPos
									Go Top
									Scan For (uCrsImpTalaoPos.tipo==myRegcli And uCrsImpTalaoPos.Susp==.F.)
										If i==1
											uf_imprimirPOS_ImpTalao(uCrsImpTalaoPos.stamp,"ATENDIMENTO",.F.,myRegcli)
											Select uCrsImpTalaoPos
										Else
											uf_imprimirPOS_ImpTalao(uCrsImpTalaoPos.stamp,"ATENDIMENTO",.T.,myRegcli)
											Select uCrsImpTalaoPos
										Endif
									Endscan
								Endif
							Endif
						Endfor
					Endif

					If uf_gerais_getParameter("ADM0000000010","BOOL")=.T.
						If myOffline
							If !uf_gerais_getParameter("ADM0000000126","BOOL")
								uf_PAGAMENTO_impTalaoRecPos(905)
							Endif
						Else
							If !uf_gerais_getParameter("ADM0000000126","BOOL")
								uf_PAGAMENTO_impTalaoRecPos(903)
							Endif
						Endif
					Endif

					For i=1 To lcNrVd
						If myOffline
							If !uf_gerais_getParameter("ADM0000000126","BOOL")
								uf_PAGAMENTO_impTalaoRecPos(904)
							Endif
						Else
							If !uf_gerais_getParameter("ADM0000000126","BOOL")
								uf_PAGAMENTO_impTalaoRecPos(901)
							Else
								uf_PAGAMENTO_impTalaoRecPosA6(901)
							Endif
						Endif
					Endfor

					fecha("uCrsImpRec")
				Else
					*!*					** Guarda PDF das Notas de cr�dito quando o operador escolhe nrTaloes==0 **
					*!*					IF myArquivoDigitalPdf==.t.
					*!*						uf_arquivaTalaoPdfAd(.f.,.t.,.t.)
					*!*					ENDIF
				Endif
			Endif
		Else

			If Used("UCRSIMPTALAOPOS")
				Select uCrsImpTalaoPos
				Go Top
				Scan
					TEXT TO lcSQL TEXTMERGE NOSHOW
					SELECT cobrado FROM ft (nolock) WHERE ftstamp='<<ALLTRIM(uCrsImpTalaoPos.stamp)>>'
					ENDTEXT
					uf_gerais_actGrelha("", "uCrscobrado", lcSQL)
					If uCrscobrado.cobrado=.T.
						Select uCrsImpTalaoPos
						Replace uCrsImpTalaoPos.Susp With .T.
					Else
						Select uCrsImpTalaoPos
						Replace uCrsImpTalaoPos.Susp With .F.
					Endif
					fecha("uCrscobrado")

					Select uCrsImpTalaoPos
					TEXT TO lcSQL TEXTMERGE NOSHOW
					select design from fi (nolock) where ftstamp='<<ALLTRIM(uCrsImpTalaoPos.stamp)>>' and design like '%Ad. Reserva%'
					ENDTEXT
					uf_gerais_actGrelha("", "uCrsResAd", lcSQL)
					If Reccount("uCrsResAd")>0
						Select uCrsImpTalaoPos
						Replace uCrsImpTalaoPos.AdiReserva With .T.
					Else
						Select uCrsImpTalaoPos
						Replace uCrsImpTalaoPos.AdiReserva With .F.
					Endif
					fecha("uCrsResAd")

					Select uCrsImpTalaoPos
				Endscan
			Endif


			lcNrVd = PAGAMENTO.nTaloes

			If !uv_reImp
				lcNrVd = PAGAMENTO.nTaloes

				lcNrVdCred = lcNrVd

				uv_numTaloes = Floor(uf_gerais_getParameter("ADM0000000008","NUM"))
				If Type("upv_altNumTaloes") <> "U"
					If !upv_altNumTaloes
						If uv_numTaloes <> 0
							lcNrVdCred = uv_numTaloes
						Endif
					Endif
				Else
					If uv_numTaloes <> 0
						lcNrVdCred = uv_numTaloes
					Endif
				Endif

			Else
				lcNrVd = 1
				lcNrVdCred = 1
			Endif

			If Used("uCrsNrReserva")
				Select * From uCrsNrReserva Into Cursor uCrsNrReservaAux Readwrite
			Endif

			uv_impTalConj = .F.

			uv_talSep = Iif(Type("pTalSep") <> "U", pTalSep, uf_gerais_getParameter('ADM0000000111', 'bool'))



			If !uv_talSep

				If Used("uCrsImpTalaoPos")

					Select uCrsImpTalaoPos
					Go Top
					Locate For uCrsImpTalaoPos.tipo==myFact And uCrsImpTalaoPos.Susp==.F.

					If Found()

						lcNrVd = lcNrVdCred

					Endif

				Endif

			Endif

			If lcNrVd != 0 Or (!uv_reImp And lcNrVd == 0 And ft2.u_vddom == .T.)

				&& VENDAS A DINHEIRO (n�o suspensas)
				For i=1 To lcNrVd

					If Used("UCRSIMPTALAOPOS")

						Select uCrsImpTalaoPos
						Go Top
						Locate For ((uCrsImpTalaoPos.tipo == myVd And uCrsImpTalaoPos.Susp == .F.) And uCrsImpTalaoPos.tipo <> myRegcli)

						If Found()

							If !uv_talSep

								lcPrintFRX = uf_gerais_getUmValor("b_impressoes", "id", "tabela = 'FT' and nomeImpressao = 'Tal�o Atendimento'")

								lcSelstamp = uf_gerais_getUmValor("ft", "u_nrAtend", "ftstamp = '" + Alltrim(uCrsImpTalaoPos.stamp) + "'")

								If Upper(myPaisConfSoftw)=='PORTUGAL'

									uf_gerais_actGrelha("","FTprint","exec up_print_ft '', '" + Alltrim(lcSelstamp) + "', 0, 0")
									uf_gerais_actGrelha("","FIprint","exec up_print_fi '', '" + Alltrim(lcSelstamp) + "',0, 0")
									uf_gerais_actGrelha("","DocListprint","exec up_print_docs_atendimento '', '" + Alltrim(lcSelstamp) + "', ''")
									uf_gerais_actGrelha("","DocPagprint","exec up_print_pagamentos '"+lcSelstamp+"' ")
									uf_gerais_actGrelha("","DOCPRINTRODAPE","exec up_print_rodape '" + mySite + "' ")

								Else
									uf_gerais_actGrelha("","FTprint","exec up_print_ft '', '" + Alltrim(lcSelstamp) + "', 0, 0")
									uf_gerais_actGrelha("","FIprint","exec up_print_fi '', '" + Alltrim(lcSelstamp) + "', 0, 0")
									uf_gerais_actGrelha("","DocListprint","exec up_print_docs_atendimento '', '" + Alltrim(lcSelstamp) + "', ''")
									uf_gerais_actGrelha("","DocPagprint","exec up_print_pagamentos '"+lcSelstamp+"' ")
									uf_gerais_actGrelha("","DOCPRINTRODAPE","exec up_print_rodape '" + mySite + "' ")

								Endif

								Select FTprint
								Delete From FTprint Where FTprint.ndoc = myRegcli

								Select FIprint
								Delete From FIprint Where FIprint.ftstamp Not In (Select ftstamp From FTprint With (Buffering = .T.))

								Select FIprint
								Go Bottom

								uv_lastStamp = FIprint.ftstamp

								Select FIprint
								Replace FIprint.laststamp With uv_lastStamp For 1=1


								Select FIprint
								Go Top
								Locate For !Empty(FIprint.obsint)

								uv_numPrint = 1



								If usacampanha=1 And uf_gerais_getParameter('ADM0000000283','BOOL')
									uv_numPrint = 2
								Endif

								For j=1 To uv_numPrint

									If Found() And uf_gerais_getParameter_site('ADM0000000151', 'BOOL', mySite)

										uf_imprimirgerais_sendprinter_talao(lcPrintFRX, .T., .F.)

										Select FIprint
										Go Top

										Replace FIprint.obsint With "" For 1=1

										uf_imprimirgerais_sendprinter_talao(lcPrintFRX, Iif(j = 1, .F., .T.), .F.)

										j = j + 1

									Else

										Replace FIprint.obsint With "" For 1=1

										uf_imprimirgerais_sendprinter_talao(lcPrintFRX, Iif(j = 1, .F., .T.), .F.)
									Endif
								Endfor


								Select uCrsImpTalaoPos
								Replace uCrsImpTalaoPos.impresso With .T.

								uv_impTalConj = .T.

							Else

								lcPrintFRX = uf_gerais_getUmValor("b_impressoes", "id", "tabela = 'FT' and nomeImpressao = 'Tal�o Atendimento'")

								Select uCrsImpTalaoPos
								Go Top

								Scan For ((uCrsImpTalaoPos.tipo == myVd And uCrsImpTalaoPos.Susp == .F.) And uCrsImpTalaoPos.tipo <> myRegcli)

									lcSelstamp = uf_gerais_getUmValor("ft", "u_nrAtend", "ftstamp = '" + Alltrim(uCrsImpTalaoPos.stamp) + "'")

									If Upper(myPaisConfSoftw)=='PORTUGAL'

										uf_gerais_actGrelha("","FTprint","exec up_print_ft '"+Alltrim(uCrsImpTalaoPos.stamp)+"', '', 0, 0")
										uf_gerais_actGrelha("","FIprint","exec up_print_fi '"+Alltrim(uCrsImpTalaoPos.stamp)+"', '', 0, 0")
										uf_gerais_actGrelha("","DocListprint","exec up_print_docs_atendimento '"+Alltrim(uCrsImpTalaoPos.stamp)+"', '" + Alltrim(lcSelstamp) + "', ''")
										uf_gerais_actGrelha("","DocPagprint","exec up_print_pagamentos 'XXX', 0")
										uf_gerais_actGrelha("","DOCPRINTRODAPE","exec up_print_rodape '" + mySite + "' ")

									Else
										uf_gerais_actGrelha("","FTprint","exec up_print_ft '"+Alltrim(uCrsImpTalaoPos.stamp)+"', '', 0, 0")
										uf_gerais_actGrelha("","FIprint","exec up_print_fi '"+Alltrim(uCrsImpTalaoPos.stamp)+"', '', 0, 0")
										uf_gerais_actGrelha("","DocListprint","exec up_print_docs_atendimento '"+Alltrim(uCrsImpTalaoPos.stamp)+"', '" + Alltrim(lcSelstamp) + "', ''")
										uf_gerais_actGrelha("","DocPagprint","exec up_print_pagamentos 'XXX', 0")
										uf_gerais_actGrelha("","DOCPRINTRODAPE","exec up_print_rodape '" + mySite + "' ")

									Endif

									uv_numPrint = 1



									If usacampanha = 1 And uf_gerais_getParameter('ADM0000000283','BOOL')
										Select FIprint
										Go Top
										Locate For !Empty(FIprint.campanhas)
										If Found()
											uv_numPrint = 2
										Endif
									Endif

									For j=1 To uv_numPrint

										Select FIprint
										Locate For !Empty(FIprint.obsint)

										If Found() And uf_gerais_getParameter_site('ADM0000000151', 'BOOL', mySite)

											uf_imprimirgerais_sendprinter_talao(lcPrintFRX, .T., .F.)

											Select FIprint
											Go Top

											Replace FIprint.obsint With "" For 1=1

											uf_imprimirgerais_sendprinter_talao(lcPrintFRX, Iif(j = 1, .F., .T.), .F.)

											j = j + 1

										Else

											Replace FIprint.obsint With "" For 1=1

											uf_imprimirgerais_sendprinter_talao(lcPrintFRX, Iif(j = 1, .F., .T.), .F.)
										Endif


										Select uCrsImpTalaoPos
										Replace uCrsImpTalaoPos.impresso With .T.

									Endfor

									Select uCrsImpTalaoPos
								Endscan

							Endif

						Endif

					Endif
				Endfor

			Endif



			If Used("uCrsImpTalaoPos")

				For i=1 To lcNrVdCred

					Select uCrsImpTalaoPos
					Go Top
					Locate For uCrsImpTalaoPos.tipo==myFact And uCrsImpTalaoPos.Susp==.F.

					If Found()

						If !uv_talSep

							If !uv_impTalConj

								lcPrintFRX = uf_gerais_getUmValor("b_impressoes", "id", "tabela = 'FT' and nomeImpressao = 'Tal�o Atendimento'")

								lcSelstamp = uf_gerais_getUmValor("ft", "u_nrAtend", "ftstamp = '" + Alltrim(uCrsImpTalaoPos.stamp) + "'")

								If Upper(myPaisConfSoftw)=='PORTUGAL'

									uf_gerais_actGrelha("","FTprint","exec up_print_ft '', '" + Alltrim(lcSelstamp) + "', 0")
									uf_gerais_actGrelha("","FIprint","exec up_print_fi '', '" + Alltrim(lcSelstamp) + "', 0")
									uf_gerais_actGrelha("","DocListprint","exec up_print_docs_atendimento '', '" + Alltrim(lcSelstamp) + "', ''")
									uf_gerais_actGrelha("","DocPagprint","exec up_print_pagamentos '"+lcSelstamp+"' ")
									uf_gerais_actGrelha("","DOCPRINTRODAPE","exec up_print_rodape '" + mySite + "' ")

								Else
									uf_gerais_actGrelha("","FTprint","exec up_print_ft '', '" + Alltrim(lcSelstamp) + "', 0")
									uf_gerais_actGrelha("","FIprint","exec up_print_fi '', '" + Alltrim(lcSelstamp) + "', 0")
									uf_gerais_actGrelha("","DocListprint","exec up_print_docs_atendimento '', '" + Alltrim(lcSelstamp) + "', ''")
									uf_gerais_actGrelha("","DocPagprint","exec up_print_pagamentos '"+lcSelstamp+"' ")
									uf_gerais_actGrelha("","DOCPRINTRODAPE","exec up_print_rodape '" + mySite + "' ")

								Endif


								Select FTprint
								Delete From FTprint Where FTprint.ndoc = myRegcli


								Select FIprint
								Delete From FIprint Where FIprint.ftstamp Not In (Select ftstamp From FTprint With (Buffering = .T.))

								Select FIprint
								Go Bottom

								uv_lastStamp = FIprint.ftstamp

								Select FIprint
								Replace FIprint.laststamp With uv_lastStamp For 1=1

								Select FIprint
								Locate For !Empty(FIprint.obsint)

								If Found() And uf_gerais_getParameter_site('ADM0000000151', 'BOOL', mySite)

									uf_imprimirgerais_sendprinter_talao(lcPrintFRX, .T., .F.)

									Select FIprint
									Go Top

									Replace FIprint.obsint With "" For 1=1

									uf_imprimirgerais_sendprinter_talao(lcPrintFRX, Iif(i = 1, .F., .T.), .F.)

									i = i + 1

								Else

									Replace FIprint.obsint With "" For 1=1

									uf_imprimirgerais_sendprinter_talao(lcPrintFRX, Iif(i = 1, .F., .T.), .F.)
								Endif

								Select uCrsImpTalaoPos
								Replace uCrsImpTalaoPos.impresso With .T.

							Endif

						Else

							lcPrintFRX = uf_gerais_getUmValor("b_impressoes", "id", "tabela = 'FT' and nomeImpressao = 'Tal�o Atendimento'")

							Select uCrsImpTalaoPos
							Go Top

							Scan For uCrsImpTalaoPos.tipo == myFact And uCrsImpTalaoPos.Susp == .F.

								lcSelstamp = uf_gerais_getUmValor("ft", "u_nrAtend", "ftstamp = '" + Alltrim(uCrsImpTalaoPos.stamp) + "'")

								If Upper(myPaisConfSoftw)=='PORTUGAL'

									uf_gerais_actGrelha("","FTprint","exec up_print_ft '"+Alltrim(uCrsImpTalaoPos.stamp)+"', '', 0")
									uf_gerais_actGrelha("","FIprint","exec up_print_fi '"+Alltrim(uCrsImpTalaoPos.stamp)+"', '', 0")
									uf_gerais_actGrelha("","DocListprint","exec up_print_docs_atendimento '"+Alltrim(uCrsImpTalaoPos.stamp)+"', '" + Alltrim(lcSelstamp) + "', ''")
									uf_gerais_actGrelha("","DocPagprint","exec up_print_pagamentos 'XXX', 0")
									uf_gerais_actGrelha("","DOCPRINTRODAPE","exec up_print_rodape '" + mySite + "' ")

								Else
									uf_gerais_actGrelha("","FTprint","exec up_print_ft '"+Alltrim(uCrsImpTalaoPos.stamp)+"', '', 0")
									uf_gerais_actGrelha("","FIprint","exec up_print_fi '"+Alltrim(uCrsImpTalaoPos.stamp)+"', '', 0")
									uf_gerais_actGrelha("","DocListprint","exec up_print_docs_atendimento '"+Alltrim(uCrsImpTalaoPos.stamp)+"', '" + Alltrim(lcSelstamp) + "', ''")
									uf_gerais_actGrelha("","DocPagprint","exec up_print_pagamentos 'XXX', 0")
									uf_gerais_actGrelha("","DOCPRINTRODAPE","exec up_print_rodape '" + mySite + "' ")

								Endif

								Select FIprint
								Locate For !Empty(FIprint.obsint)

								If Found() And uf_gerais_getParameter_site('ADM0000000151', 'BOOL', mySite)

									uf_imprimirgerais_sendprinter_talao(lcPrintFRX, .T., .F.)

									Select FIprint
									Go Top

									Replace FIprint.obsint With "" For 1=1

									uf_imprimirgerais_sendprinter_talao(lcPrintFRX, Iif(i = 1, .F., .T.), .F.)

									i = i + 1

								Else

									Replace FIprint.obsint With "" For 1=1

									uf_imprimirgerais_sendprinter_talao(lcPrintFRX, Iif(i = 1, .F., .T.), .F.)
								Endif

								Select uCrsImpTalaoPos
								Replace uCrsImpTalaoPos.impresso With .T.

								Select uCrsImpTalaoPos
							Endscan

						Endif

					Endif

				Endfor

			Endif

			lcNrVd = PAGAMENTO.nTaloes

			If Used("UCRSIMPTALAOPOS") And lcNrVd <> 0
				Select uCrsImpTalaoPos
				Go Top
				Scan For (uCrsImpTalaoPos.tipo == myVd And uCrsImpTalaoPos.AdiReserva == .T.)

					lcPrintFRX = uf_gerais_getUmValor("b_impressoes", "id", "tabela = 'FT' and nomeImpressao = 'Tal�o Atendimento'")

					lcSelstamp = uf_gerais_getUmValor("ft", "u_nrAtend", "ftstamp = '" + Alltrim(uCrsImpTalaoPos.stamp) + "'")

					If Upper(myPaisConfSoftw)=='PORTUGAL'

						uf_gerais_actGrelha("","FTprint","exec up_print_ft '"+Alltrim(uCrsImpTalaoPos.stamp)+"', '', 0")
						uf_gerais_actGrelha("","FIprint","exec up_print_fi '"+Alltrim(uCrsImpTalaoPos.stamp)+"', '', 0")
						uf_gerais_actGrelha("","DocListprint","exec up_print_docs_atendimento '"+Alltrim(uCrsImpTalaoPos.stamp)+"', '" + Alltrim(lcSelstamp) + "', ''")
						uf_gerais_actGrelha("","DocPagprint","exec up_print_pagamentos '"+lcSelstamp+"'")
						uf_gerais_actGrelha("","DOCPRINTRODAPE","exec up_print_rodape '" + mySite + "' ")

					Else
						uf_gerais_actGrelha("","FTprint","exec up_print_ft '"+Alltrim(uCrsImpTalaoPos.stamp)+"', '', 0")
						uf_gerais_actGrelha("","FIprint","exec up_print_fi '"+Alltrim(uCrsImpTalaoPos.stamp)+"', '', 0")
						uf_gerais_actGrelha("","DocListprint","exec up_print_docs_atendimento '"+Alltrim(uCrsImpTalaoPos.stamp)+"', '" + Alltrim(lcSelstamp) + "', ''")
						uf_gerais_actGrelha("","DocPagprint","exec up_print_pagamentos '"+lcSelstamp+"'")
						uf_gerais_actGrelha("","DOCPRINTRODAPE","exec up_print_rodape '" + mySite + "' ")

					Endif


					If Used("FTprint") And Reccount("FTprint") <> 0

						For i=1 To lcNrVd

							Select FIprint
							Locate For !Empty(FIprint.obsint)

							If Found() And uf_gerais_getParameter_site('ADM0000000151', 'BOOL', mySite)

								uf_imprimirgerais_sendprinter_talao(lcPrintFRX, .T., .F.)

								Select FIprint
								Go Top
								Replace FIprint.obsint With "" For 1=1

								uf_imprimirgerais_sendprinter_talao(lcPrintFRX, Iif(i = 1, .F., .T.), .F.)

							Else

								Replace FIprint.obsint With "" For 1=1

								uf_imprimirgerais_sendprinter_talao(lcPrintFRX, Iif(i = 1, .F., .T.), .F.)
							Endif

						Endfor

					Endif

					Select uCrsImpTalaoPos
					Replace uCrsImpTalaoPos.impresso With .T.

				Endscan
			Endif

			If Used("UCRSIMPTALAOPOS") And lcNrVd <> 0
				Select uCrsImpTalaoPos
				Go Top
				Scan For ((uCrsImpTalaoPos.tipo == myVd Or uCrsImpTalaoPos.tipo == myFact) And uCrsImpTalaoPos.Susp == .T.)

					lcPrintFRX = uf_gerais_getUmValor("b_impressoes", "id", "tabela = 'FT' and nomeImpressao = 'Tal�o Atendimento'")

					lcSelstamp = uf_gerais_getUmValor("ft", "u_nrAtend", "ftstamp = '" + Alltrim(uCrsImpTalaoPos.stamp) + "'")

					If Upper(myPaisConfSoftw)=='PORTUGAL'

						uf_gerais_actGrelha("","FTprint","exec up_print_ft '"+Alltrim(uCrsImpTalaoPos.stamp)+"', '', 1")
						uf_gerais_actGrelha("","FIprint","exec up_print_fi '"+Alltrim(uCrsImpTalaoPos.stamp)+"', '', 1")
						uf_gerais_actGrelha("","DocListprint","exec up_print_docs_atendimento '"+Alltrim(uCrsImpTalaoPos.stamp)+"', '" + Alltrim(lcSelstamp) + "', ''")
						uf_gerais_actGrelha("","DocPagprint","exec up_print_pagamentos '"+lcSelstamp+"'")
						uf_gerais_actGrelha("","DOCPRINTRODAPE","exec up_print_rodape '" + mySite + "' ")

					Else
						uf_gerais_actGrelha("","FTprint","exec up_print_ft '"+Alltrim(uCrsImpTalaoPos.stamp)+"', '', 1")
						uf_gerais_actGrelha("","FIprint","exec up_print_fi '"+Alltrim(uCrsImpTalaoPos.stamp)+"', '', 1")
						uf_gerais_actGrelha("","DocListprint","exec up_print_docs_atendimento '"+Alltrim(uCrsImpTalaoPos.stamp)+"', '" + Alltrim(lcSelstamp) + "', ''")
						uf_gerais_actGrelha("","DocPagprint","exec up_print_pagamentos '"+lcSelstamp+"'")
						uf_gerais_actGrelha("","DOCPRINTRODAPE","exec up_print_rodape '" + mySite + "' ")

					Endif


					If Used("FTprint") And Reccount("FTprint") <> 0
						For i=1 To Round(uf_gerais_getParameter("ADM0000000007","NUM"),0)


							Select FIprint
							Locate For !Empty(FIprint.obsint)

							If Found() And uf_gerais_getParameter_site('ADM0000000151', 'BOOL', mySite)

								uf_imprimirgerais_sendprinter_talao(lcPrintFRX, .T., .F.)

								Select FIprint
								Go Top
								Replace FIprint.obsint With "" For 1=1

								uf_imprimirgerais_sendprinter_talao(lcPrintFRX, Iif(i = 1, .F., .T.), .F.)

								i = i + 1

							Else

								Replace FIprint.obsint With "" For 1=1

								uf_imprimirgerais_sendprinter_talao(lcPrintFRX, Iif(i = 1, .F., .T.), .F.)
							Endif

						Endfor
					Endif

					Select uCrsImpTalaoPos
					Replace uCrsImpTalaoPos.impresso With .T.

				Endscan
			Endif

			If Used("UCRSIMPTALAOPOS") And lcNrVd <> 0 And Used("ucrsvale")

				Select uCrsImpTalaoPos
				Go Top
				Scan For ((uCrsImpTalaoPos.tipo == myRegcli) And uCrsImpTalaoPos.Susp == .F.)

					Select uCrsImpTalaoPos

					lcPrintFRX = uf_gerais_getUmValor("b_impressoes", "id", "tabela = 'FT' and nomeImpressao = 'Tal�o Atendimento'")

					lcSelstamp = uf_gerais_getUmValor("ft", "u_nrAtend", "ftstamp = '" + Alltrim(uCrsImpTalaoPos.stamp) + "'")

					If Upper(myPaisConfSoftw)=='PORTUGAL'

						uf_gerais_actGrelha("","FTprint","exec up_print_ft '"+Alltrim(uCrsImpTalaoPos.stamp)+"', '', 0")
						uf_gerais_actGrelha("","FIprint","exec up_print_fi '"+Alltrim(uCrsImpTalaoPos.stamp)+"', '',0, 0,1,'" + mySite + "'")
						uf_gerais_actGrelha("","DocListprint","exec up_print_docs_atendimento '"+Alltrim(uCrsImpTalaoPos.stamp)+"', '" + Alltrim(lcSelstamp) + "', ''")
						uf_gerais_actGrelha("","DocPagprint","exec up_print_pagamentos '"+lcSelstamp+"' ")
						uf_gerais_actGrelha("","DOCPRINTRODAPE","exec up_print_rodape '" + mySite + "' ")

					Else
						uf_gerais_actGrelha("","FTprint","exec up_print_ft '"+Alltrim(uCrsImpTalaoPos.stamp)+"', '', 0")
						uf_gerais_actGrelha("","FIprint","exec up_print_fi '"+Alltrim(uCrsImpTalaoPos.stamp)+"', '', 0")
						uf_gerais_actGrelha("","DocListprint","exec up_print_docs_atendimento '"+Alltrim(uCrsImpTalaoPos.stamp)+"', '" + Alltrim(lcSelstamp) + "', ''")
						uf_gerais_actGrelha("","DocPagprint","exec up_print_pagamentos '"+lcSelstamp+"' ")
						uf_gerais_actGrelha("","DOCPRINTRODAPE","exec up_print_rodape '" + mySite + "' ")

					Endif


					If Used("FTprint") And Reccount("FTprint") <> 0
						For i=1 To Round(uf_gerais_getParameter("ADM0000000016","NUM"),0)

							Select FIprint
							Locate For !Empty(FIprint.obsint)

							If Found() And uf_gerais_getParameter_site('ADM0000000151', 'BOOL', mySite)

								uf_imprimirgerais_sendprinter_talao(lcPrintFRX, .T., .F.)

								Select FIprint
								Go Top
								Replace FIprint.obsint With "" For 1=1

								uf_imprimirgerais_sendprinter_talao(lcPrintFRX, Iif(i = 1, .F., .T.), .F.)

								i = i + 1

							Else

								Replace FIprint.obsint With "" For 1=1

								uf_imprimirgerais_sendprinter_talao(lcPrintFRX, Iif(i = 1, .F., .T.), .F.)
							Endif

						Endfor
					Endif

					Select uCrsImpTalaoPos
					Replace uCrsImpTalaoPos.impresso With .T.

				Endscan
			Endif

			If Used("uCrsimpTalaoPos")

				Select uCrsImpTalaoPos
				Go Top
				Scan For Inlist(uCrsImpTalaoPos.tipo, 901, 904)
					uf_PAGAMENTO_impTalaoRecPos(uCrsImpTalaoPos.tipo, uCrsImpTalaoPos.stamp)
				Endscan

			Endif

			If uf_gerais_getParameter("ADM0000000010", "BOOL")

				If Used("uCrsimpTalaoPos")

					Select uCrsImpTalaoPos
					Go Top
					Scan For Inlist(uCrsImpTalaoPos.tipo, 903)
						uf_PAGAMENTO_impTalaoRecPos(uCrsImpTalaoPos.tipo, uCrsImpTalaoPos.stamp)
					Endscan

				Endif

			Endif

			If uf_gerais_getParameter("ADM0000000207","BOOL")

				uv_atend = nrAtendimento

				uf_gerais_actGrelha("","uc_totAtendPrint", "exec up_print_getTotAtend '" + Alltrim(uv_atend) + "'")
				lcPrintFRX = uf_gerais_getUmValor("b_impressoes", "id", "tabela = 'FT' and nomeImpressao = 'Tal�o N�mero Atendimento'")

				uf_imprimirgerais_sendprinter_talao(lcPrintFRX, .F., .F.)

			Endif

			If uf_gerais_getParameter("ADM0000000318","BOOL") And !uv_reImp

				lcSelstamp = nrAtendimento

				If Used("UCRSIMPTALAOPOS")
					Select uCrsImpTalaoPos
					Go Top
					lcSelstampTMP = uf_gerais_getUmValor("ft", "u_nrAtend", "ftstamp = '" + Alltrim(uCrsImpTalaoPos.stamp) + "'")

					If !Empty(lcSelstampTMP) And Alltrim(lcSelstamp) <> Alltrim(lcSelstampTMP)
						lcSelstamp = lcSelstampTMP
					Endif

				Endif

				uf_gerais_actGrelha("","Reservaprint", "exec up_print_reserva '" + Alltrim(lcSelstamp) + "', '', 1")
				uf_gerais_actGrelha("","DOCPRINTRODAPERESERVA","exec up_print_rodape_reserva '" + mySite + "' ")
				If Reccount("Reservaprint") <> 0
					For i=1 To uf_gerais_getParameter("ADM0000000318","NUM")

						Public upv_moeda

						upv_moeda = Alltrim(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))

						lcPrintFRX = uf_gerais_getUmValor("b_impressoes", "id", "tabela = 'FT' and nomeImpressao = 'Tal�o Atendimento Reserva'")

						Select Reservaprint
						Locate For !Empty(Reservaprint.obsint)

						If Found() And uf_gerais_getParameter_site('ADM0000000151', 'BOOL', mySite)

							uf_imprimirgerais_sendprinter_talao(lcPrintFRX, .F., .F.)

							Replace Reservaprint.obsint With "" For 1=1

							uf_imprimirgerais_sendprinter_talao(lcPrintFRX, .F., .F.)

							i = i + 1

						Else

							Replace Reservaprint.obsint With "" For 1=1

							uf_imprimirgerais_sendprinter_talao(lcPrintFRX, .F., .F.)
						Endif

					Endfor
				Endif
			Endif

			lcSelstamp = nrAtendimento

			If Used("UCRSIMPTALAOPOS")
				Select uCrsImpTalaoPos
				Go Top
				lcSelstampTMP = uf_gerais_getUmValor("ft", "u_nrAtend", "ftstamp = '" + Alltrim(uCrsImpTalaoPos.stamp) + "'")

				If !Empty(lcSelstampTMP) And Alltrim(lcSelstamp) <> Alltrim(lcSelstampTMP)
					lcSelstamp = lcSelstampTMP
				Endif
			Endif

			uf_gerais_actGrelha("","Reservaprint", "exec up_print_reserva '" + Alltrim(lcSelstamp) + "', '', 0")
			uf_gerais_actGrelha("","DOCPRINTRODAPERESERVA","exec up_print_rodape_reserva '" + mySite + "' ")
			If Reccount("Reservaprint") <> 0

				For i=1 To uf_gerais_getParameter("ADM0000000148","NUM")

					Public upv_moeda

					upv_moeda = Alltrim(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))

					lcPrintFRX = uf_gerais_getUmValor("b_impressoes", "id", "tabela = 'FT' and nomeImpressao = 'Tal�o Atendimento Reserva'")

					Select Reservaprint
					Locate For !Empty(Reservaprint.obsint)

					If Found() And uf_gerais_getParameter_site('ADM0000000151', 'BOOL', mySite)

						uf_imprimirgerais_sendprinter_talao(lcPrintFRX, .F., .F.)

						Replace Reservaprint.obsint With "" For 1=1

						uf_imprimirgerais_sendprinter_talao(lcPrintFRX, .F., .F.)

						i = i + 1

					Else

						Replace Reservaprint.obsint With "" For 1=1

						uf_imprimirgerais_sendprinter_talao(lcPrintFRX, .F., .F.)

					Endif

				Endfor
			Endif

			If Used("uCrsValidaImpPsico")
				fecha("uCrsValidaImpPsico")
			Endif

			If Used("uCrsImpTalaoPos")

				Select uCrsImpTalaoPos
				Go Top

				Scan

					uf_imprimirpos_ImpPrevPsico(uCrsImpTalaoPos.stamp, .F., 'ATENDIMENTO')

				Endscan
			Endif

		Endif
	Endif

	**IF !uf_gerais_getParameter_site('ADM0000000136', 'BOOL', mySite)


Endfunc


**
Function uf_PAGAMENTO_impTalaoRecPos
	Lparameters lcPrintType, uv_stamp, uv_prev

	Messagebox(20)
	**IF uf_gerais_getParameter_site('ADM0000000136', 'BOOL', mySite)
	If uf_gerais_getUmValor("B_terminal", "usaDriverWin", "terminal = '" + Alltrim(myTerm) + "'")

		If Empty(uv_stamp)

			Select uCrsImpTalaoPos
			Go Top
			Scan For uCrsImpTalaoPos.tipo == lcPrintType
				lcStamp = uCrsImpTalaoPos.stamp

				Public upv_moeda, upv_nifVD, upv_numcharlin

				upv_moeda =  Alltrim(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))
				upv_nifVD = Alltrim(uf_gerais_getParameter_site('ADM0000000075', 'text', mySite))
				upv_numcharlin = uf_gerais_getParameter("ADM0000000194","NUM")

				If Used("uCrsFi2Lreg")
					uv_idImp = uf_gerais_getUmValor("B_impressoes", "id", "tabela = 'RE' and nomeImpressao = 'Tal�o Recibo Cliente Produtos'")
				Else
					uv_idImp = uf_gerais_getUmValor("B_impressoes", "id", "tabela = 'RE' and nomeImpressao = 'Tal�o Recibo Cliente'")
				Endif

				Select uCrsImpTalaoPos

				If !uf_imprimirgerais_createCursTalao(uv_idImp, uCrsImpTalaoPos.stamp, '', '', 'REGVENDAS', lcPrintType)
					uf_perguntalt_chama("ERRO A GERAR CURSORES DO TAL�O." + Chr(13) + "POR FAVOR CONTACTE O SUPORTE.", "OK", "", 48)
					Return .F.
				Endif

				uf_gerais_actGrelha("","ucrsRodapeCert",[exec up_cert_TextoRodape 'RE'])

				uf_imprimirgerais_sendprinter_talao(uv_idImp , .F., uv_prev)

			Endscan

		Else

			lcStamp = uv_stamp

			Public upv_moeda, upv_nifVD, upv_numcharlin

			upv_moeda =  Alltrim(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))
			upv_nifVD = Alltrim(uf_gerais_getParameter_site('ADM0000000075', 'text', mySite))
			upv_numcharlin = uf_gerais_getParameter("ADM0000000194","NUM")

			uv_idImp = uf_gerais_getUmValor("B_impressoes", "id", "tabela = 'RE' and nomeImpressao = 'Tal�o Recibo Cliente'")

			If !uf_imprimirgerais_createCursTalao(uv_idImp, lcStamp, '', '', 'REGVENDAS', lcPrintType)
				uf_perguntalt_chama("ERRO A GERAR CURSORES DO TAL�O." + Chr(13) + "POR FAVOR CONTACTE O SUPORTE.", "OK", "", 48)
				Return .F.
			Endif

			uf_gerais_actGrelha("","ucrsRodapeCert",[exec up_cert_TextoRodape 'RE'])

			uf_imprimirgerais_sendprinter_talao(uv_idImp , .F., uv_prev)

		Endif

	Else

		Local lcStamp, lcValidaImp, lcCount
		lcStamp = ""
		Store .F. To lcValidaImp, lcCount

		Store '' To lcMoeda

		&& verifica se deve imprimir com que simbolo - Euro // USD
		lcMoeda = uf_gerais_getParameter("ADM0000000260","text")


		If Empty(lcMoeda)
			lcMoeda = 'EURO'
		Endif

		lcValidaImp = uf_gerais_setImpressoraPOS(.T.)
		If lcValidaImp == .F.
			Return
		Endif

		Select uCrsImpTalaoPos
		Go Top
		Scan For uCrsImpTalaoPos.tipo == lcPrintType
			lcStamp = uCrsImpTalaoPos.stamp

			** CRIAR CURSORES COM DADOS DAS VENDAS **
			uf_gerais_actGrelha("",[uCrsRe],[select * from re (nolock) where restamp=']+lcStamp+['])
			Select uCrsRe

			uf_gerais_actGrelha("",[uCrsRl],[select * from rl (nolock) where restamp=']+lcStamp+[' order by datalc, cm, nrdoc] )
			Select uCrsRl

			uv_atcud = "ATCUD:"+Alltrim(uf_gerais_getUmValor("TSRE","ISNULL((CASE WHEN ATCUD<>'' then ATCUD ELSE (select case when ATCUD<>'' then atcud else '0' end FROM cm1(nolock) WHERE cm="+astr(uCrsRe.ndoc)+") END),'')","ndoc="+astr(uCrsRe.ndoc)))+"-"+astr(uCrsRe.rno)

			uf_gerais_criarCabecalhoPOS()

			** IMPRIMIR O TIPO DE DOCUMENTO DE VENDA **
			uf_gerais_textoVermPOS()

			&&If (uCrsRe.ndoc==myVd) OR (uCrsRe.ndoc==myFact) Comentado em 2014.01.29, n�o existe seres de recibos por lojas
			?"Recibo Nr."
			??Transform(uCrsRe.rno,"999999999")+" "
			&&EndIf

			** IMPRIMIR TAG IDENTIFICADORA DO DOCUMENTO DE VENDA **
			&&If uCrsRe.ndoc==myVd OR uCrsRe.ndoc==myFact  Comentado em 2014.01.29, n�o existe seres de recibos por lojas
			??"(RR)"
			&&EndIf
			If  uCrsRe.ndoc==4 &&uCrsRe.ndoc==myFact OR Comentado em 2014.01.29, n�o existe seres de recibos por lojas
				??"(RC)"
			Endif

			** Imprimir serie de recibo **
			??" / " + astr(uCrsRe.ndoc)


			?"Op: "
			??Substr(uCrsRe.vendnm,1,30) + " (" + Alltrim(Str(uCrsRe.vendedor)) + ")"
			?"Data: "
			??uf_gerais_getDate(uCrsRe.rdata,"DATA")
			??"      Hora: "
			??uCrsRe.ousrhora

			uf_gerais_resetCorPOS()

			uf_gerais_separadorPOS()

			?"Cliente: "
			??Alltrim(uCrsRe.Nome) + Iif(uCrsRe.no==200, '', ' (' + Alltrim(Str(uCrsRe.no)) + ')' + '(' + Alltrim(Str(uCrsRe.estab)) + ')')
			?"Morada: "
			??Left(Alltrim(uCrsRe.morada),uf_gerais_getParameter("ADM0000000194","NUM")-8)

			?"Contrib. Nr.: "
			If (Alltrim(uCrsRe.ncont)!="999999990") And !Empty(Alltrim(uCrsRe.ncont)) And (Alltrim(uCrsRe.ncont)!="9999999999")
				??Transform(uCrsRe.ncont,"99999999999999999999")
			Else
				??Alltrim(uf_gerais_getParameter_site('ADM0000000075', 'text', mySite))
			Endif

			****** IMPRESS�O DAS COLUNAS ******
			uf_gerais_separadorPOS()
			?"Documento"
			If uf_gerais_getParameter("ADM0000000194","NUM") >= 46
				?"     Nr.      Data.       Por Reg.       Pago " &&46
			Else
				?"     Nr.    Data.     Por Reg.     Pago " &&40
			Endif
			uf_gerais_separadorPOS()
			************************************

			****** IMPRESS�O DAS LINHAS ******
			Select uCrsRl
			Go Top
			Scan
				?Substr(uCrsRl.CDESC,1,30)
				If uf_gerais_getParameter("ADM0000000194","NUM") >= 46
					?Transform(uCrsRl.NRDOC,"99999999")+"   "
					??uf_gerais_getDate(uCrsRl.datalc,"DATA")
					??Transform(uCrsRl.Eval,"99999999.99")+"    "
					??Transform(uCrsRl.EREC,"99999999.99")+"    "
				Else
					?Transform(uCrsRl.NRDOC,"99999999")+"  " &&10
					??uf_gerais_getDate(uCrsRl.datalc,"DATA") &&10
					??Transform(uCrsRl.Eval,"99999999.99")+"  " &&11
					??Transform(uCrsRl.EREC,"99999999.99") &&9
				Endif

				If lcPrintType=901 Or lcPrintType=904
					If Used("uCrsFi2Lreg")
						Select uCrsFi2Lreg
						Go Top
						Scan For uCrsFi2Lreg.fno = uCrsRl.NRDOC
							If lcCount=.F.
								?" Referente aos Produtos:"
							Endif
							?" " + Substr(Alltrim(uCrsFi2Lreg.Design),1,uf_gerais_getParameter("ADM0000000194","NUM")-12) + " Qtt: "
							??Transform(uCrsFi2Lreg.qtt,"99999")
							lcCount=.T.
							Select uCrsFi2Lreg
						Endscan
					Endif

					lcCount=.F.
				Endif

				Select uCrsRl
			Endscan

			uf_gerais_separadorPOS()

			***** IMPRESS�O DOS TOTAIS *****
			uf_gerais_textoVermPOS()

			If Upper(Alltrim(lcMoeda)) == 'EURO'
				If uCrsRe.efinv!=0
					?"Desconto Financeiro:"
					??Transform(uCrsRe.efinv,"9999999999.99 ") + Chr(27)+Chr(116)+Chr(19)+Chr(213)
				Endif
				?"Total Documento:"
				??Transform(uCrsRe.etotal,"99999999999999.99 ") + Chr(27)+Chr(116)+Chr(19)+Chr(213)
			Else
				If uCrsRe.efinv!=0
					?"Desconto Financeiro:"
					??Transform(uCrsRe.efinv,"9999999999.99 ")  + " " + Alltrim(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))
				Endif
				?"Total Documento:"
				??Transform(uCrsRe.etotal,"99999999999999.99 ")  + " " + Alltrim(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))
			Endif

			uf_gerais_resetCorPOS()

			** IMPRESSAO DO CODIGO DO ATENDIMENTO **
			If !Empty(uCrsRe.u_nratend)
				?
				?
				uf_gerais_textoCJPOS()
				?'Atendimento:'
				?
				???	 Chr(29)+Chr(104)+Chr(50) + Chr(29)+Chr(119)+Chr(2) + Chr(29)+Chr(107)+Chr(4) + Substr(astr(uCrsRe.u_nratend),1,10) + Chr(0)
				uf_gerais_textoLJPOS()
			Endif

			If Upper(Alltrim(ucrse1.pais)) == 'PORTUGAL'

				?"	  " + Alltrim(uv_atcud)

			Endif

			****** IMPRESS�O DO RODAP� *****
			uf_gerais_separadorPOS()
			If uf_gerais_actGrelha("","ucrsRodapeCert",[exec up_cert_TextoRodape 'RE'])
				**Alterado novas regras certifica��o 20120313
				?"	  "+Alltrim(ucrsRodapeCert.rodape)
			Else
				?"	  Processado por Computador "
			Endif

			uf_gerais_feedCutPOS()
			**		uf_gerais_openDrawPOS()

			**fechar cursores**
			fecha("uCrsRe")
			fecha("uCrsRl")

			Select uCrsImpTalaoPos
		Endscan
		Go Top

		**LIBERTAR IMPRESSORA, COLOCAR NO DEFAULT
		uf_gerais_setImpressoraPOS(.F.)
	Endif
Endfunc


**
Function uf_PAGAMENTO_impTalaoRecPosA6
	Lparameters lcPrintType

	If !Used("uCrsFi2Lreg")
		Return .F.
	Endif

	Local lcStamp, lcValidaImp, lcCount, lcOpCode
	Public myPrintType, myOpCode1
	Store .F. To lcValidaImp, lcCount
	Store '' To myOpCode1, lcStamp
	myPrintType = lcPrintType


	Select uCrsImpTalaoPos
	Go Top
	Scan For uCrsImpTalaoPos.tipo=lcPrintType
		*** PREPARAR DEFINI��ES GERAIS DA IMPRESSORA ***
		lcValidaImp = uf_gerais_setImpressoraPOS(.T.)
		************************************************

		If lcValidaImp
			********************************************************************************
			** Nota: devido � natureza do IDU � necess�rio criar cursor j� preparado para **
			fecha("uCrsRlx")
			Create Cursor uCrsRlx (rlstamp c(28), ndoc N(5), rno N(10), NRDOC N(10), CDESC c(60), rdata d, Eval N(13,3), EREC N(13,3), Design c(60), qttt c(4),qtt N(7))
			********************************************************************************

			lcStamp = uCrsImpTalaoPos.stamp

			** CRIAR CURSORES COM DADOS DAS VENDAS **
			uf_gerais_actGrelha("",[uCrsRe],[select * from re (nolock) where restamp=']+lcStamp+['])
			Select uCrsRe

			uf_gerais_actGrelha("",[uCrsRl],[select * from rl (nolock) where restamp=']+lcStamp+['  order by cm, nrdoc])
			Select uCrsRl

			** guardar dados do operador **
			Store 0 To lcOpCode
			Select uCrsRe
			Go Top
			If myOpAltCod == 0
				lcOpCode = uCrsRe.vendedor
			Else
				lcOpCode = myOpAltCod
			Endif

			myOpCode1 = "Op: " + Substr(uCrsRe.vendnm,1,30) + " (" + Alltrim(Str(lcOpCode)) + ")"

			Select uCrsRl
			Go Top
			Scan

				Insert Into uCrsRlx (rlstamp, ndoc, rno, NRDOC, CDESC, rdata, Eval, EREC) ;
					VALUES (uCrsRl.rlstamp, uCrsRl.ndoc, uCrsRl.rno, uCrsRl.NRDOC, uCrsRl.CDESC, uCrsRl.rdata, uCrsRl.Eval, uCrsRl.EREC)

				If lcPrintType==901 Or lcPrintType==904
					Select uCrsFi2Lreg
					Go Top
					Scan For uCrsFi2Lreg.fno = uCrsRl.NRDOC
						If lcCount=.F.
							Select uCrsRlx
							Append Blank
							Replace uCrsRlx.Design	With 'Referente aos Produtos'
						Endif

						Select uCrsRlx
						Append Blank
						Replace uCrsRlx.Design	With Alltrim(uCrsFi2Lreg.Design)
						Replace uCrsRlx.qttt	With 'Qtt:'
						Replace uCrsRlx.qtt 	With uCrsFi2Lreg.qtt

						lcCount=.T.
						Select uCrsFi2Lreg
					Endscan

					lcCount=.F.
				Endif

				Select uCrsRl
			Endscan


			Select ucrse1
			Go Top
			Select uCrsRe
			Go Top
			Select uCrsRlx
			Go Top
			Set REPORTBEHAVIOR 80
			Select uCrsRlx
			Report Form Alltrim(myPath)+'\analises\reciboA6.frx' To Printer Noconsole

			** fechar cursores **
			If Used("uCrsRe")
				fecha("uCrsRe")
			Endif
			If Used("uCrsRl")
				fecha("uCrsRl")
			Endif
			If Used("uCrsRlx")
				fecha("uCrsRlx")
			Endif
			*********************

			uf_gerais_setImpressoraPOS(.F.)
		Endif

		Select uCrsImpTalaoPos
	Endscan
	Go Top

	Release myOpCode1, myPrintType
Endfunc


**
Function uf_PAGAMENTO_limparVendasFimVd
	Lparameters tcBool

	Local clStamp

	If !tcBool
		uf_PAGAMENTO_actDataUltVendaCl()
	Endif

	&&cursores dos tal�es
	fecha("uCrsFt")
	fecha("uCrsFt2")
	fecha("uCrsFi")
	fecha("uCrsFiPromo")
	fecha("uCrsPsicoFi")
	************************

	If Used("ft2")
		Select ft2
		Delete All
	Endif

	If Used("ft")
		Select ft
		Delete All
	Endif

	fecha("dadosPsico")

	If !uf_gerais_getParameter("ADM0000000254", "BOOL")

		If Used("uCrsImpTalaoPos")
			Select uCrsImpTalaoPos
			Go Top
			Scan
				TEXT TO lcSQL NOSHOW TEXTMERGE
					select u_tlote, u_tlote2 from ft (nolock) where ftstamp='<<ALLTRIM(uCrsImpTalaoPos.stamp)>>'
				ENDTEXT
				uf_gerais_actGrelha("", "ucrsCompartLote", lcSQL)
				If Alltrim(ucrsCompartLote.u_tlote)=='98' Or Alltrim(ucrsCompartLote.u_tlote)=='99'
					Select uCrsImpTalaoPos
					Replace uCrsImpTalaoPos.lote With Alltrim(ucrsCompartLote.u_tlote)
				Endif
				If Alltrim(ucrsCompartLote.u_tlote2)=='98' Or Alltrim(ucrsCompartLote.u_tlote2)=='99'
					Select uCrsImpTalaoPos
					Replace uCrsImpTalaoPos.lote With Alltrim(ucrsCompartLote.u_tlote2)
					Replace uCrsImpTalaoPos.planocomp With .T.
				Endif
				fecha("ucrsCompartLote")
			Endscan

			Select uCrsImpTalaoPos
			Go Top
			Scan
				If uCrsImpTalaoPos.tipo=75
					Local nrreceitadel
					nrreceitadel = Alltrim(uCrsImpTalaoPos.nrreceita)
					If Used("uCrsImpTalaoPosTMP ")
						Select uCrsImpTalaoPosTMP
						Delete From uCrsImpTalaoPosTMP Where Alltrim(uCrsImpTalaoPosTMP.nrreceita) == Alltrim(nrreceitadel) And uCrsImpTalaoPosTMP.tipo<>75
					Endif
				Endif
				Select uCrsImpTalaoPos
			Endscan

			Select * From uCrsImpTalaoPos Where ((!Empty(uCrsImpTalaoPos.nrreceita) And (uCrsImpTalaoPos.rm=.T. Or (uCrsImpTalaoPos.rm=.F. And uCrsImpTalaoPos.planocomp=.T.))) Or (Alltrim(uCrsImpTalaoPos.lote)=='98' Or Alltrim(uCrsImpTalaoPos.lote)=='99')) And impresso=.F.  Into Cursor uCrsImpTalaoPosTMP Readwrite

			Select uCrsImpTalaoPosTMP
			Go Top
			Scan
				TEXT TO lcSQL NOSHOW TEXTMERGE
					select top 1 token from Dispensa_Eletronica (nolock) where receita_nr='<<ALLTRIM(uCrsImpTalaoPosTMP.nrreceita)>>' and resultado_efetivacao_descr='Pedido processado com sucesso.'
				ENDTEXT
				uf_gerais_actGrelha("", "ucrsCompartVal", lcSQL)
				If Reccount("ucrsCompartVal")>0
					Select uCrsImpTalaoPosTMP
					Replace uCrsImpTalaoPosTMP.rm With .F.
					Replace uCrsImpTalaoPosTMP.Lordem With 99999
				Endif
				fecha("ucrsCompartVal")

				Select uCrsImpTalaoPosTMP
			Endscan
			Select * From uCrsImpTalaoPosTMP Order By Lordem Into Cursor uCrsImpTalaoPosTMP Readwrite
		Endif
		**uf_perguntalt_chama("3 verifica uCrsImpTalaoPos?","Sim","N�o")
		If Used("uCrsImpTalaoPosTMP")
			Go Top
			Local lcpedeconf
			Store .F. To lcpedeaviso
			Select uCrsImpTalaoPosTMP
			If Reccount("uCrsImpTalaoPosTMP")>1
				lcpedeaviso = .T.
			Endif

			Select uCrsImpTalaoPosTMP
			Go Top
			Scan
				If ucrse1.OrdemImpressaoReceita == .F.
					If uCrsImpTalaoPosTMP.Lordem <> 99999 Or (Alltrim(uCrsImpTalaoPosTMP.lote)=='98' Or Alltrim(uCrsImpTalaoPosTMP.lote)=='99')
						**IF uf_gerais_getParameter_site('ADM0000000136', 'BOOL', mySite)
						Messagebox(21)
						If uf_gerais_getUmValor("B_terminal", "usaDriverWin", "terminal = '" + Alltrim(myTerm) + "'")
							Select uCrsImpTalaoPosTMP
							uf_imprimirpos_impRecTalaoComum_talaoVerso(uCrsImpTalaoPosTMP.stamp, 1, .F., .T.)
						Else
							Select uCrsImpTalaoPosTMP
							uf_imprimirpos_impRec_V2(uCrsImpTalaoPosTMP.stamp, 1, "ATENDIMENTO", lcpedeaviso)
						Endif
					Endif
					Select uCrsImpTalaoPosTMP
					If uCrsImpTalaoPosTMP.rm=.T. Or (uCrsImpTalaoPosTMP.planocomp = .T. And (Alltrim(uCrsImpTalaoPosTMP.lote)=='98' Or Alltrim(uCrsImpTalaoPosTMP.lote)=='99'))
						**IF uf_gerais_getParameter_site('ADM0000000136', 'BOOL', mySite)
						Messagebox(22)
						If uf_gerais_getUmValor("B_terminal", "usaDriverWin", "terminal = '" + Alltrim(myTerm) + "'")
							Select uCrsImpTalaoPosTMP
							uf_imprimirpos_impRecTalaoComum_talaoVerso(uCrsImpTalaoPosTMP.stamp, 2, .F., .T.)
						Else
							uf_imprimirpos_impRec_V2(uCrsImpTalaoPosTMP.stamp, 2, "ATENDIMENTO", lcpedeaviso)
						Endif
					Endif
				Else
					Select uCrsImpTalaoPosTMP
					If uCrsImpTalaoPosTMP.rm=.T. Or (uCrsImpTalaoPosTMP.planocomp = .T. And (Alltrim(uCrsImpTalaoPosTMP.lote)=='98' Or Alltrim(uCrsImpTalaoPosTMP.lote)=='99'))
						**IF uf_gerais_getParameter_site('ADM0000000136', 'BOOL', mySite)
						Messagebox(23)
						If uf_gerais_getUmValor("B_terminal", "usaDriverWin", "terminal = '" + Alltrim(myTerm) + "'")
							uf_imprimirpos_impRecTalaoComum_talaoVerso(uCrsImpTalaoPosTMP.stamp, 2, .F., .T.)
						Else
							uf_imprimirpos_impRec_V2(uCrsImpTalaoPosTMP.stamp, 2, "ATENDIMENTO", lcpedeaviso)
						Endif
					Endif
					If uCrsImpTalaoPosTMP.Lordem <> 99999 Or (Alltrim(uCrsImpTalaoPosTMP.lote)=='98' Or Alltrim(uCrsImpTalaoPosTMP.lote)=='99')
						**IF uf_gerais_getParameter_site('ADM0000000136', 'BOOL', mySite)
						Messagebox(24)
						If uf_gerais_getUmValor("B_terminal", "usaDriverWin", "terminal = '" + Alltrim(myTerm) + "'")
							Select uCrsImpTalaoPosTMP
							uf_imprimirpos_impRecTalaoComum_talaoVerso(uCrsImpTalaoPosTMP.stamp, 1, .F., .T.)
						Else
							Select uCrsImpTalaoPosTMP
							uf_imprimirpos_impRec_V2(uCrsImpTalaoPosTMP.stamp, 1, "ATENDIMENTO", lcpedeaviso)
						Endif
					Endif
				Endif
				Select uCrsImpTalaoPosTMP
				Replace uCrsImpTalaoPosTMP.impresso With .T.
				Update uCrsImpTalaoPos Set impresso=.T. Where Alltrim(uCrsImpTalaoPos.stamp)==Alltrim(uCrsImpTalaoPosTMP.stamp)
			Endscan
		Endif

		*!*		IF USED("uCrsImpTalaoPos")
		*!*			SELECT uCrsImpTalaoPos
		*!*			GO TOP
		*!*			SCAN
		*!*				LOCAL lcPosImpR
		*!*				SELECT uCrsImpTalaoPos
		*!*				lcPosImpR= RECNO("uCrsImpTalaoPos")
		*!*				uf_imprimirpos_impRec(uCrsImpTalaoPos.stamp, 1, "ATENDIMENTO")
		*!*				IF uCrsImpTalaoPos.rm=.t. AND uCrsImpTalaoPos.planocomp=.t. AND uCrsImpTalaoPos.impcomp2=.f.
		*!*					uf_imprimirpos_impRec(uCrsImpTalaoPos.stamp, 2, "ATENDIMENTO")
		*!*				ENDIF
		*!*				SELECT uCrsImpTalaoPos
		*!*				TRY
		*!*				GO lcPosImpR
		*!*				CATCH
		*!*					GO bottom
		*!*				ENDTRY
		*!*				SELECT uCrsImpTalaoPos
		*!*			ENDSCAN
		*!*		ENDIF
		**uf_perguntalt_chama("3 verifica uCrsImpTalaoPos?","Sim","N�o")
		fecha("uCrsImpTalaoPosTMP")
		fecha("uCrsImpTalaoPos")
	Endif

	If Used("uCrsImpTalaoPosTMP")
		fecha("uCrsImpTalaoPosTMP")
	Endif
	If Used("uCrsImpTalaoPos")
		fecha("uCrsImpTalaoPos")
	Endif

	If uf_gerais_getParameter("ADM0000000118","BOOL") && Caso este parametro esteja activo n�o precisa mais do cursor
		fecha("uCrsCabVendas")
	Endif

	If Used("uCrsVale")
		fecha("uCrsVale")
		fecha("uCrsStampsFiReg")
		fecha("uCrsFi2Reg")
		fecha("uCrsNovosStamps")
		fecha("uCrsActAcerto")
		fecha("uCrsFi2Lreg")
		fecha("uCrsValidaClReg")
		fecha("uCrsValidaClRegNo")
	Endif

	If Used("uCrsVerificaRespostaDEM")
		Select uCrsVerificaRespostaDEM
		Delete All
	Endif
	If Used("uCrsVerificaRespostaDEMTotal")
		Select uCrsVerificaRespostaDEMTotal
		Delete All
	Endif
	If Used("ucrsDemEfetivada")
		Select ucrsDemEfetivada
		Delete All
	Endif
	If Used("uCrsVerificaRespostaDEMTotalAux")
		Select uCrsVerificaRespostaDEMTotalAux
		Delete All
	Endif
	If Used("uCrsVerificaRespostaDEMAuxiliar")
		Select uCrsVerificaRespostaDEMAuxiliar
		Delete All
	Endif
	If Used("uCrsVerificaRespostaDEM_nd")
		Select uCrsVerificaRespostaDEM_nd
		Delete All
	Endif
	If Used("uCrsDEMndisp")
		Select uCrsDEMndisp
		Delete All
	Endif
	If Used("uCrsVerificaRespostaDEMAux")
		Select uCrsVerificaRespostaDEMAux
		Delete All
	Endif
	If Used("uCrsVerificaRespostaDEM_tot")
		Select uCrsVerificaRespostaDEM_tot
		Delete All
	Endif
	If Used("ucrsReceitaEfetivar")
		Select ucrsReceitaEfetivar
		Delete All
	Endif
	If Used("ucrsDadosValidacaoEfetivarDEM")
		Select ucrsDadosValidacaoEfetivarDEM
		Delete All
	Endif
	If Used("ucrsReceitaEfetivarAp")
		Select ucrsReceitaEfetivarAp
		Delete All
	Endif
	If Used("ucrsReceitaValidar")
		Select ucrsReceitaValidar
		Delete All
	Endif
	If Used("ucrsReceitaValidarAux")
		Select ucrsReceitaValidarAux
		Delete All
	Endif
	If Used("ucrsReceitaValidarLinhas")
		Select ucrsReceitaValidarLinhas
		Delete All
	Endif
	If Used("ucrsDadosValidacaoDEM")
		Select ucrsDadosValidacaoDEM
		Delete All
	Endif
	If Used("ucrsDadosValidacaoDEMLinhas")
		Select ucrsDadosValidacaoDEMLinhas
		Delete All
	Endif
	If Used("uCrsreceitas")
		Select uCrsreceitas
		Delete All
	Endif
	If Used("ucrsReceitaValidar")
		Select ucrsReceitaValidar
		Delete All
	Endif
	If Used("uCrsReceitaValidarAp")
		Select uCrsReceitaValidarAp
		Delete All
	Endif

	If uf_gerais_getParameter_site('ADM0000000221', 'BOOL', mySite) = .T.

		TEXT TO lcSql NOSHOW TEXTMERGE
			exec up_questionario_refsQuest '<<nrAtendimento>>'
		ENDTEXT
		If !uf_gerais_actGrelha("","uc_refsQuest",lcSQL)
			uf_perguntalt_chama("OCORREU UM ERRO A CARREGAR O QUESTIONARIO.","OK","",16)
		Endif

		Select uc_refsQuest
		Scan
			uf_questionario_chama(uc_refsQuest.questionario, uc_refsQuest.ref, nrAtendimento)
		Endscan

		fecha("uc_refsQuest")

	Endif


	****************************************************
	* Mapeia as linhas dispensadas sem picagem de qrcode *
	****************************************************
	Try
		TEXT TO lcSql NOSHOW TEXTMERGE
			exec up_sem_picagem '<<nrAtendimento>>', '<<mysite>>'
		ENDTEXT
		If !uf_gerais_actGrelha("","",lcSQL)
			uf_perguntalt_chama("OCORREU UM ERRO A GUARDAR PRODUTOS SEM PICAGEM MVO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		Endif
	Catch
		uf_perguntalt_chama("GIULIANA A SP DEU ERRO!!","OK","",16)
	Endtry


	uf_atendimento_ValoresDefeito()

	If Type("usacampanha") <> "U"
		usacampanha = 0
	Endif

	If PrecisaRecibo = .T.
		uf_pagamento_utilplafond()
		UtilPlafond = .F.
	Endif

	UtilPlafond = .F.

	Release lcNrVenda
	Release lcNrRecibo
	Release myTotalPagCc

Endfunc


*********************************************
* Guarda a ultima venda na ficha do cliente *
*********************************************
Function uf_PAGAMENTO_actDataUltVendaCl

	Select ft
	Go Top
	lcSQL=''
	TEXT TO lcSql NOSHOW TEXTMERGE
		update b_utentes
		set b_utentes.ultvenda='<<uf_gerais_getDate(date(),"SQL")>>'
		where b_utentes.no=<<ft.no>> and b_utentes.estab=<<ft.estab>>
	ENDTEXT
	If !uf_gerais_actGrelha("","",lcSQL)
		uf_perguntalt_chama("OCORREU UM ERRO A ACTUALIZAR OS DADOS DA �LTIMA VENDA NA FICHA DO CLIENTE.","OK","",16)
	Endif

Endfunc


**
Function uf_PAGAMENTO_clickXpag
	Local lcValida, lcValidaCred, lcValidaCredDev, lcPos, lcValorAplicarVale
	Store .F. To lcValida, lcValidaCred, lcValidaCredDev
	Store 0 To lcValorAplicarVale

	Select uCrsCabVendas
	lcPos = Recno()

	&& Valida Se Permite Vendas a Cr�dito no Atendimento
	Select ft
	If !(myVendasCredito)
		uf_perguntalt_chama("ESTA FUNCIONALIDADE EST� BLOQUEADA PARA O ATENDIMENTO.","OK","",64)
		Return .F.
	Endif
	*****************************

	&& Valida Posto Offline
	If myOffline
		uf_perguntalt_chama("N�O PODE REALIZAR CR�DITOS EM MODO OFFLINE.","OK","",64)
		Return .F.
	Endif
	**

	**** Alterado por Pedro Oliveira, 08-04-2021
	**** Permitir colocar documento a cr�dito para o utente caso alterem a entidade de Fatura��o
	&& valida cliente
	**	Select ft
	**	If !Empty(ft.no)
	**		If ft.no == 200
	**			uf_perguntalt_chama("N�O PODE COLOCAR DOCUMENTOS A CR�DITO PARA O CLIENTE N� 200.","OK","",64)
	**			RETURN .f.
	**		EndIf
	**	EndIf
	**

	&& valida entidade de fatura��o
	Select uCrsCLEF
	If uCrsCLEF.no == 200
		uf_perguntalt_chama("N�O PODE COLOCAR DOCUMENTOS A CR�DITO PARA A ENTIDADE DE FATURA��O N� 200.","OK","",64)
		Return .F.
	Endif
	**

	Select uCrsCabVendas
	If (uCrsCabVendas.saved=.T.)
		uf_perguntalt_chama("N�O PODE ALTERAR O TIPO DE VENDA DEPOIS DE FINALIZAR O ATENDIMENTO.","OK","",64)
		If uCrsCabVendas.modoPag=.T.
			Return .F.
		Else
			Return .T.
		Endif
	Endif
	If uCrsCabVendas.etiliquido=0
		lordemchk=Left(Alltrim(Str(uCrsCabVendas.Lordem)),1)
		Select * From fi Where Left(Alltrim(Str(Lordem)),1)==Alltrim(lordemchk) And Alltrim(tipor)=='RES' Into Cursor ucrsficredverif Readwrite
		Select ucrsficredverif
		If Reccount("ucrsficredverif")>0
			uf_perguntalt_chama("N�O PODE COLOCAR A CR�DITO DOCUMENTOS COM RESERVAS E TOTAL 0.","OK","",64)
			fecha("ucrsficredverif ")
			Return .F.
		Endif
		fecha("ucrsficredverif ")
	Endif


	** VERIFICAR SE OBRIGA SUPERVISOR NA ALTERA��O PARA DOCUMENTO A CR�DITO
	If uf_gerais_getParameter_site('ADM0000000054', 'BOOL', mySite) = .T. And Alltrim(ch_grupo) <> 'Administrador' And Alltrim(ch_grupo) <> 'Supervisores'
		If !uf_gerais_valida_password_admin('PAGAMENTO', 'ALTERA��O DE DOC. PARA CR�DITO')
			Return .F.
		Endif
	Endif

	** Valida vendas a cr�dito apenas para clientes importados, mecofarma
	If uf_gerais_getParameter_site('ADM0000000079', 'BOOL', mySite)
		lcSQL = ""
		TEXT TO lcSQL NOSHOW TEXTMERGE
			select no_ext,* from b_utentes where no=<<ft.no>>
		ENDTEXT
		uf_gerais_actGrelha("","ucrsnoext",lcSQL)
		If Vartype(ucrsnoext.no_ext) == 'C'
			If Empty(ucrsnoext.no_ext)
				uf_perguntalt_chama("N�O PODE COLOCAR A CR�DITO VENDAS COM UM CLIENTE QUE N�O � IMPORTADO.","OK","",64)
				Return .F.
			Endif
		Endif
	Endif

	&&valida se tem adiantamento
	Select uCrsCabVendas
	If uCrsCabVendas.TemAdiantamento == .T.
		uf_perguntalt_chama("N�O PODE COLOCAR A CR�DITO VENDAS COM ADIANTAMENTO.","OK","",64)
		Return .F.
	Endif

	Select uCrsCabVendas
	If At('Reserva',Alltrim(uCrsCabVendas.Design))>0
		uf_perguntalt_chama("N�O PODE COLOCAR A CR�DITO UMA RESERVA.","OK","",64)
		Return .F.
	Endif

	If uCrsCabVendas.receita And uCrsCabVendas.Fact
		If !Empty(Alltrim(uCrsCabVendas.u_ltstamp)) && se tiver registo nos lotes
			uf_perguntalt_chama("N�O PODE CONVERTER EM FACTURA UMA VENDA COM REGISTO NOS LOTES MARCADA PARA PAGAMENTO VIA RECIBO.","OK","",64)
			If uCrsCabVendas.modoPag=.T.
				Return .F.
			Else
				Return .T.
			Endif
		Else
			If !uf_perguntalt_chama("ATEN��O: AO COLOCAR NOVAMENTE ESTA VENDA A CR�DITO, A MESMA VAI SER ABATIDA E DAR ORIGEM A UMA NOVA VENDA."+Chr(10)+Chr(10)+"TEM A CERTEZA QUE PRETENDE CONTINUAR?","Sim","N�o")
				If uCrsCabVendas.modoPag=.T.
					Return .F.
				Else
					Return .T.
				Endif

			Endif
		Endif
	Endif

	** Valida se vai ser necess�rio emitir um acerto de CC (acontece quando se coloca a cr�dito e tem que se devolver dinheiro ao cliente)
	Select uCrsCabVendas
	lnPosCabVendas = Recno("uCrsCabVendas")
	Select uCrsCabVendas
	Go Top
	Scan For !Empty(uCrsCabVendas.ofistamp) And uCrsCabVendas.modoPag==.T.
		lcValida = .F.
		Select fi
		Go Top
		Scan
			If lcValida && se j� encontrou venda
				If Left(fi.Design,1)=="."
					Exit && j� percorreu todas as linhas desta venda -> sair do scan
				Else
					If !Empty(fi.ofistamp) And (fi.marcada=.F. Or fi.fmarcada=.T.)
						lcValidaCred=.T.

						Select ucrsvale
						Go Top
						Scan For ucrsvale.fistamp=fi.ofistamp
							If ucrsvale.etiliquido > fi.etiliquido
								lcValorAplicarVale = lcValorAplicarVale + ucrsvale.etiliquido - fi.etiliquido
							Endif
						Endscan

						Select fi
					Endif
					If fi.partes!=0
						lcValidaCredDev=.T.
					Endif
				Endif
			Endif

			If ((Alltrim(fi.Design)==Alltrim(uCrsCabVendas.Design)) Or (Left(Alltrim(fi.Design),Len(Alltrim(uCrsCabVendas.Design)))==Alltrim(uCrsCabVendas.Design))) And (fi.Lordem==uCrsCabVendas.Lordem)
				lcValida=.T. && encontrou venda
			Endif

			Select fi
		Endscan

		** guardar valor de acerto no cursor cabVendas
		Select uCrsCabVendas
		If lcValorAplicarVale > 0
			Replace uCrsCabVendas.eacerto With lcValorAplicarVale
		Endif

		Select uCrsCabVendas
	Endscan


	Select uCrsCabVendas
	Try

		Go lnPosCabVendas

	Catch To oExc
	Endtry

	If !Empty(uCrsCabVendas.ofistamp) And uCrsCabVendas.modoPag==.T.
		If lcValidaCredDev
			uf_perguntalt_chama("N�O PODE COLOCAR A CR�DITO DOCUMENTOS COM PRODUTOS A DEVOLVER.","OK","",64)
			Return .F.
		Endif
		If lcValidaCred=.T.
			Local lcTextoPerg
			**lcTextoPerg = "Aten��o: esta venda cont�m produtos j� pagos. Pretende, mesmo assim, coloc�-los a cr�dito?"
			lcTextoPerg = "Aten��o: este atendimento contem produtos j� pagos. Pretende colocar a cr�dito o valor remanescente (valor n�o pago)?"
			If lcValorAplicarVale != 0
				lcTextoPerg = lcTextoPerg + Chr(13) + "Ser� aplicado um acerto no total de: " + astr(lcValorAplicarVale,15,2)
			Endif
			If myRemoveCompart=.F.
				If !uf_perguntalt_chama(lcTextoPerg,"Sim","N�o")
					Return .F.
				Else
					lcProdJaPagos = .T.
				Endif
			Else
				lcProdJaPagos = .T.
			Endif
		Endif
	Endif

	Local lcClickPos, lcTotalPag, lcTroco
	Store 0 To lcClickPos, lcTotalPag, lcTroco

	Select uCrsCabVendas
	lcClickPos = Recno("uCrsCabVendas")

	*!*		If !USED("uCrsVale")
	*!*			CALCULATE SUM(uCrsCabVendas.etiliquido) to lcTotalPag for uCrsCabVendas.ModoPag = .f.
	*!*			PAGAMENTO.txtValAtend.value = Round(lcTotalPag,2)
	*!*		ELSE
	*!*			CALCULATE SUM(uCrsCabVendas.valorpagar) TO lcTotalPag FOR uCrsCabVendas.ModoPag = .f.
	*!*			PAGAMENTO.txtValAtend.value = Round(lcTotalPag,2) - lcValorAplicarVale
	*!*		ENDIF
	Select uCrsCabVendas
	Go Top

	Calculate Sum(uCrsCabVendas.valorPagar) To lcTotalPag For uCrsCabVendas.modoPag = .F.
	PAGAMENTO.txtValAtend.Value = Round(lcTotalPag,2)

	** Mostra o contra valor em moeda no pagamento
	If uf_gerais_getParameter_site('ADM0000000051', 'BOOL', mySite) = .T.
		Local lccambio
		lccambio=0
		lcSQL = ''
		TEXT TO lcSQL Noshow Textmerge
			select top 1 cambio from cb where moeda= '<<ALLTRIM(uf_gerais_getParameter_site('ADM0000000052', 'TEXT', mySite))>>' and moedaOrigem = '<<uf_gerais_getParameter('ADM0000000260', 'TEXT')>>' order by data desc
		ENDTEXT
		If !uf_gerais_actGrelha("","curcambio",lcSQL)
			uf_perguntalt_chama("Aten��o: Ocorreu uma anomalia ao verificar o acesso do utilizador. Por favor contacte o Suporte.","OK","",16)
			Return .F.
		Endif
		If Reccount("curcambio")>0
			lccambio= curcambio.cambio
		Endif
		fecha("curcambio")
		PAGAMENTO.txtValAtendCVMoeda.Value = Round(lcTotalPag*lccambio,2)
	Endif

	If PAGAMENTO.txtValAtend.Value > 0
		lcTroco = Round(Val(astr(PAGAMENTO.txtValRec.Value,15,2)),2) - PAGAMENTO.txtValAtend.Value
		If lcTroco > 0
			PAGAMENTO.txtTroco.Value = lcTroco
		Else
			PAGAMENTO.txtTroco.Value = 0
		Endif
	Else
		PAGAMENTO.txtValRec.Value = ""
		PAGAMENTO.txtTroco.Value = 0
		uf_PAGAMENTO_resetValoresModoPag()
	Endif

	uf_pagamento_InactivaObjectosNegativo()

	If Used("uCrsVale")
		Store .F. To lcValida

		Select uCrsCabVendas
		Try
			Go lcClickPos
		Catch
		Endtry

		Select fi
		Go Top
		Scan
			If lcValida && se j� encontrou venda
				If Left(fi.Design,1)=="."
					Exit && j� percorreu todas as linhas desta venda -> sair do scan
				Else
					If (uCrsCabVendas.modoPag == .T.) And (!Empty(fi.ofistamp)) And (fi.fmarcada ==.T. Or (fi.fmarcada==.F. And fi.marcada==.F.))
						Select ucrsvale
						Replace ucrsvale.factPago With .T. For Alltrim(ucrsvale.fistamp)==Alltrim(fi.ofistamp)
						Go Top
					Else
						Select ucrsvale
						Replace ucrsvale.factPago With .F. For Alltrim(ucrsvale.fistamp)==Alltrim(fi.ofistamp)
						Go Top
					Endif

					If (uCrsCabVendas.Fact == .T.) And (uCrsCabVendas.modoPag ==.T.)
						Select ucrsvale
						Replace ucrsvale.Fact With .F. For Alltrim(ucrsvale.fistamp)==Alltrim(fi.ofistamp)
						Go Top
					Else
						If uCrsCabVendas.Fact == .T.
							Select ucrsvale
							Replace ucrsvale.Fact With .T. For Alltrim(ucrsvale.fistamp)==Alltrim(fi.ofistamp)
							Go Top
						Endif
					Endif
				Endif
			Endif

			If ((Alltrim(fi.Design)==Alltrim(uCrsCabVendas.Design)) Or (Left(Alltrim(fi.Design),Len(Alltrim(uCrsCabVendas.Design)))==Alltrim(uCrsCabVendas.Design))) And (fi.Lordem==uCrsCabVendas.Lordem)
				lcValida=.T. && encontrou venda
			Endif

			Select fi
		Endscan
	Endif

	Select uCrsCabVendas
	Try
		Go lcClickPos
	Catch
	Endtry

	Return .T.
Endfunc


**
Function uf_PAGAMENTO_resetValoresModoPag

	With PAGAMENTO
		.txtDinheiro.Value = ""
		.txtMB.Value = ""
		.txtVisa.Value = ""
		.txtCheque.Value = ""
		.txtModPag4.Value = ""
		.txtModPag5.Value = ""
		.txtModPag6.Value = ""

		.Init(.T.)
	Endwith

	If Upper(Alltrim(ucrse1.pais)) == 'PORTUGAL'
		With PAGAMENTO
			.txtModPag3.Value = ""
			.txtValRec.Value = ""
			.txtTroco.Value = ""

			.Init(.T.)
		Endwith
		PrecisaRecibo = .F.
		UtilPlafond = .F.
	Endif
	uf_PAGAMENTO_actValoresPg()

Endfunc

Function uf_pagamento_validaMultiTipoPagComPlafond


	Local  lcModoPag1, lcModoPag2, lcModoPag3, lcModoPag4, lcModoPag5, lcModoPag6, lcModoPag7, lcModoPag8
	Store 0 To  lcModoPag1, lcModoPag2, lcModoPag3, lcModoPag4, lcModoPag5, lcModoPag6, lcModoPag7, lcModoPag8


	lcModoPag1  = Iif(Empty(PAGAMENTO.txtDinheiro.Value), 0, Val(PAGAMENTO.txtDinheiro.Value))
	lcModoPag2  = Iif(Empty(PAGAMENTO.txtMB.Value), 0, Val(PAGAMENTO.txtMB.Value))
	lcModoPag3  = Iif(Empty(PAGAMENTO.txtVisa.Value), 0, Val(PAGAMENTO.txtVisa.Value))
	lcModoPag4  = Iif(Empty(PAGAMENTO.txtCheque.Value), 0, Val(PAGAMENTO.txtCheque.Value))
	lcModoPag5  = Iif(Empty(PAGAMENTO.txtModPag3.Value), 0, Val(PAGAMENTO.txtModPag3.Value))
	lcModoPag6  = Iif(Empty(PAGAMENTO.txtModPag4.Value), 0, Val(PAGAMENTO.txtModPag4.Value))
	lcModoPag7  = Iif(Empty(PAGAMENTO.txtModPag5.Value), 0, Val(PAGAMENTO.txtModPag5.Value))
	lcModoPag8  = Iif(Empty(PAGAMENTO.txtModPag6.Value), 0, Val(PAGAMENTO.txtModPag6.Value))

	If(lcModoPag8>0) And (lcModoPag1>0 Or lcModoPag2>0 Or lcModoPag3>0 Or lcModoPag4>0 Or lcModoPag5>0 Or lcModoPag6>0 Or lcModoPag7>0  )
		Return .T.
	Endif

	Return .F.

Endfunc


**
Function uf_pagamento_importClVD

	If !Used("ft2")
		Return .F.
	Else
		Select ft2
		If Empty(Alltrim(ft2.ft2stamp))
			Return .F.
		Endif
	Endif

	Select ft2
	If !ft2.u_vddom
		Return .F.
	Endif

	Select ft2
	Replace ;
		ft2.morada		With	uCrsCLEF.morada ;
		ft2.codpost		With	uCrsCLEF.codpost ;
		ft2.Local		With	uCrsCLEF.Local ;
		ft2.telefone	With	uCrsCLEF.telefone

	PAGAMENTO.Refresh
Endfunc


**
Function uf_pagamento_alterarEntFact
	uf_pesqUtentes_chama("PAGAMENTO")
Endfunc


**
Function uf_pagamento_pesqEntFact
	Lparameters lcNo, lcEstab

	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_touch_dadosCliente <<lcNo>>, <<lcEstab>>
	ENDTEXT

	uf_gerais_actGrelha("","uCrsCLEF",lcSQL)
	Select uCrsCLEF

	If (Alltrim(uCrsCLEF.Nome) != Alltrim(uCrsatendCl.Nome)) ;
			OR (uCrsCLEF.no != uCrsatendCl.no) ;
			OR (uCrsCLEF.estab != uCrsatendCl.estab)

		PAGAMENTO.pgFrVDP.page1.txtNomeEF.DisabledBackColor = Rgb(230,242,255)
		PAGAMENTO.pgFrVDP.page1.txtNoEF.DisabledBackColor = Rgb(230,242,255)
		PAGAMENTO.pgFrVDP.page1.txtEstabEF.DisabledBackColor = Rgb(230,242,255)
	Else
		PAGAMENTO.pgFrVDP.page1.txtNomeEF.DisabledBackColor = Rgb(255,255,255)
		PAGAMENTO.pgFrVDP.page1.txtNoEF.DisabledBackColor = Rgb(255,255,255)
		PAGAMENTO.pgFrVDP.page1.txtEstabEF.DisabledBackColor = Rgb(255,255,255)
	Endif

	PAGAMENTO.Refresh()

	TEXT TO lcSQL NOSHOW TEXTMERGE
		SELECT utstamp, autorizado, ncont FROM b_utentes WHERE no=<<lcNo>> and estab=<<lcEstab>>
	ENDTEXT
	If !uf_gerais_actGrelha("", "utstamp",lcSQL)
		Messagebox("DESCULPE, MAS OCORREU UM ERRO A VERIFICAR DADOS DOS SERVI�OS. POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		Return .F.
	Endif
	If Left(Alltrim(utstamp.ncont),1) <> '5' And Left(Alltrim(utstamp.ncont),1) <> '9' And Left(Alltrim(utstamp.ncont),1) <> '6'
		If utstamp.autorizado = .F.
			uf_enviotoken_chama(Alltrim(utstamp.utstamp), 'b_utentes')
		Endif
	Endif

	If Used("utstamp")
		fecha("utstamp")
	Endif

Endfunc

** Funcao que controla a impress�o das receitas atrav�s da leitura do scanner
Function uf_pagamento_scanner

	Try
		Do Case
			Case PAGAMENTO.pgFrVDP.ActivePage == 1

				&& valida preenchimento
				If Empty(Alltrim(PAGAMENTO.txtscanner.Value)) Or Len(Alltrim(PAGAMENTO.txtscanner.Value)) < 13
					Return .F.
				Endif

				Local lcPosCab

				Select uCrsCabVendas
				Go Top
				Scan For uCrsCabVendas.sel == .F.
					If Alltrim(Strextract(uCrsCabVendas.Design, ':', ' -', 1, 0)) == Alltrim(PAGAMENTO.txtscanner.Value)
						lcPosCab = Recno("uCrsCabVendas")

						Replace uCrsCabVendas.sel With .T.

						PAGAMENTO.pgFrVDP.page1.grdVendas.Columns(2).check1.Value = .T.

						uf_PAGAMENTO_imprimeReceita()

						Select uCrsCabVendas
						Try
							Go lcPosCab
						Catch
						Endtry
					Endif
				Endscan

			Case PAGAMENTO.pgFrVDP.ActivePage == 3

				If Len(Alltrim(PAGAMENTO.txtscanner.Value)) >= 13
					PAGAMENTO.pgFrVDP.page3.txtReceita.Value = PAGAMENTO.txtscanner.Value
					PAGAMENTO.pgFrVDP.page3.txtReceita.Refresh
					PAGAMENTO.pgFrVDP.page3.txtReceita.SetFocus
				Endif
				If Upper(Left(Alltrim(PAGAMENTO.txtscanner.Value),1)) == "M"
					PAGAMENTO.pgFrVDP.page3.txtMedico.Value = PAGAMENTO.txtscanner.Value
					PAGAMENTO.pgFrVDP.page3.txtMedico.Refresh
					PAGAMENTO.pgFrVDP.page3.txtMedico.SetFocus
				Endif


			Otherwise
				***
		Endcase
	Catch
	Endtry
Endfunc


**
Function uf_pagamento_criarAcertoCC
	Lparameters lnCcValorAcerto, lnCcNo, lnCcEstab, lnCcNome, lnCcTipo, lnCcPais

	**
	TEXT TO lcSql TEXTMERGE NOSHOW
		declare @taxas table (taxa numeric(5,2), codigo int)
		insert into @taxas
		select taxa,codigo from taxasiva

		INSERT INTO CC (
		    ccstamp
		    ,datalc
		    ,no,estab,nome,tipo,pais
		    ,cm,cmdesc,origem, obs
		    ,edeb,ecred
		    ,IVATX1,IVATX2,IVATX3,IVATX4,IVATX5,IVATX6,IVATX7,IVATX8,IVATX9
		    ,formapag
		    ,situacao
		    ,moeda
		    ,dataven
		    ,vendedor,vendnm
		    ,usrdata,usrhora,usrinis,ousrdata,ousrhora,ousrinis )
		SELECT
		    left(newid(),23)
		    ,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),112)
		    ,<<lnCcNo>>,<<lnCcEstab>>,'<<ALLTRIM(lnCcNome)>>','<<ALLTRIM(lnCcTipo)>>',<<lnCcPais>>
		    ,td.cmcc, td.cmccn,'CC', 'Valor Acerto Conta Corrente'
		    ,0,<<lnCcValorAcerto>>
		    ,(select taxa from @taxas where codigo=1)
		    ,(select taxa from @taxas where codigo=2)
		    ,(select taxa from @taxas where codigo=3)
		    ,(select taxa from @taxas where codigo=4)
		    ,(select taxa from @taxas where codigo=5)
		    ,(select taxa from @taxas where codigo=6)
		    ,(select taxa from @taxas where codigo=7)
		    ,(select taxa from @taxas where codigo=8)
		    ,(select taxa from @taxas where codigo=9)
		    ,1
		    ,'1'
		    ,'EURO'
		    ,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),112)
			,<<ch_vendedor>>,'<<ALLTRIM(ch_vendnm)>>'
		    ,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),112),convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108),'ADM',convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),112),convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108),'ADM'
		from
		    td
		where
		    td.ndoc=<<myregcli>>
	ENDTEXT

	If !uf_gerais_actGrelha("","",lcSQL)
		uf_perguntalt_chama("Ocorreu uma anomalia a criar o documento de acerto na conta corrente do cliente. Por favor contacte o suporte.","OK","",16)
		Return .F.
	Endif

Endfunc


**
Function uf_PAGAMENTO_SelVale
	Local lcFvstamp, lcTotalValesSel
	lcTotalValesSel = 0

	Select uCrsVales
	lcFvstamp = uCrsVales.Fvstamp

	Select uCrsVales
	Go Top
	Calculate Sum(valor) For uCrsVales.sel == .T. To lcTotalValesSel

	If PAGAMENTO.txtValAtend.Value < lcTotalValesSel

		uf_perguntalt_chama("N�o pode aplicar um Vale com valor superior ao Atendimento.","OK","",32)

		Select uCrsVales
		Go Top
		Locate For Alltrim(lcFvstamp) == Alltrim(uCrsVales.Fvstamp)
		If Found()
			Select uCrsVales
			Replace uCrsVales.sel With .F.
		Endif

		Return .F.
	Else
		Select uCrsVales
		Go Top
		Locate For Alltrim(lcFvstamp) == Alltrim(uCrsVales.Fvstamp)


		PAGAMENTO.txtModPag3.Value = astr(lcTotalValesSel,9,2)
		PAGAMENTO.txtModPag3.Refresh
		uf_PAGAMENTO_actValoresPg()

	Endif

Endfunc


** Abate vales seleccionados como meio de pagamento
Function uf_PAGAMENTO_AbateValesSelecionados
	Local lcExecuteSQL
	lcExecuteSQL  = ""

	Select u_refVale From fi Where !Empty(u_refVale) Into Cursor ucrsFIRefsValesTemp Readwrite

	&& Vales usados no atendimento
	Select ucrsFIRefsValesTemp
	Go Top
	Scan
		TEXT TO lcSQL NOSHOW TEXTMERGE
			UPDATE b_fidelVale SET abatido = 1, usrdata = dateadd(HOUR, <<difhoraria>>, getdate()), usr = <<ch_userno>> Where b_fidelVale.ref = '<<ALLTRIM(ucrsFIRefsValesTemp.u_refVale)>>'
		ENDTEXT

		lcExecuteSQL  = lcExecuteSQL + Chr(13) + lcSQL

	Endscan
	If Used("ucrsFIRefsValesTemp")
		fecha("ucrsFIRefsValesTemp")
	Endif

	&& Vales usados no pagamento
	Select uCrsVales
	Go Top
	Scan For !Empty(uCrsVales.sel)
		TEXT TO lcSQL NOSHOW TEXTMERGE
			UPDATE b_fidelVale SET abatido = 1, usrdata = dateadd(HOUR, <<difhoraria>>, getdate()), usr = <<ch_userno>> Where b_fidelVale.fvstamp = '<<ALLTRIM(ucrsVales.fvstamp)>>'
		ENDTEXT

		lcExecuteSQL  = lcExecuteSQL + Chr(13) + lcSQL

	Endscan

	lcExecuteSQL = uf_gerais_trataPlicasSQL(lcExecuteSQL)

	**_cliptext = lcExecuteSQL

	If !Empty(lcExecuteSQL)

		lcSQL = ''
		TEXT TO lcSQL NOSHOW TEXTMERGE
			exec up_gerais_execSql 'Atendimento - Abate Vales', 1,'<<lcExecuteSQL>>', '', '', '' , '', ''
		ENDTEXT
		If !uf_gerais_actGrelha("", "", lcSQL)
			uf_perguntalt_chama("N�o foi poss�vel abater Vales associados ao atendimento. Contacte o suporte.", "", "OK", 32)
			Return .F.
		Endif
	Endif

Endfunc


**
Function uf_pagamento_preparaCabVendas
	Local lcValida, lcComp, lcCompFact, lcValidaDev, lcValidaFact, lcValidaFactDev, olcCod, lcValorDoc, lcValorReg, lcValidaFactPago, lcValidaDiploma, lcValidaReceita
	Store .F. To lcValida, lcCompFact, lcComp, lcValidaDev, lcValidaFact, lcValidaFactDev, lcValidaFactPago, lcValidaDiploma, lcValidaReceita
	Store "xxx" To olcCod
	Store 0 To lcValorReg, lcValorDoc

	&& update para n�o efetuar o calculo a duplicar visto que a fun��o � chamdada duas vezes.
	Update uCrsCabVendas Set uCrsCabVendas.etiliquido = 0
	Update uCrsCabVendas Set uCrsCabVendas.etReg = 0



	&& Preencher Cursor da Grelha - cursor criado na fun��o uf_atendimento_finalizarvenda
	Select uCrsCabVendas
	Go Top
	Scan
		Select fi
		Go Top
		Scan
			If lcValida && se j� encontrou venda
				If Left(fi.Design,1) == "."
					Exit && j� percorreu todas as linhas desta venda -> sair do scan
				Else
					If Empty(fi.tipor)
						If fi.partes == 0 && produto marcado para devolu��o
							Replace uCrsCabVendas.etiliquido With Round(uCrsCabVendas.etiliquido + fi.etiliquido,2)
							lcValidaDev = .T.
						Endif
						If fi.u_ettent1 + fi.u_ettent2 > 0 && se o produto � comparticipado
							lcComp = .T.
						Endif
						If (fi.marcada != .T.) Or (fi.fmarcada == .T. And fi.marcada == .T.) Or (fi.partes != 0) && marcada = .t. � uma factura
							lcValidaFact = .T.
						Endif
						If (fi.marcada != .T.) Or (fi.partes == 0) && ??
							lcValidaFactDev = .T.
						Endif
						If (fi.fmarcada == .T.) And (fi.partes == 0) && fmarcada = .t. � uma factura j� paga
							lcValidaFactPago = .T.
						Endif
						If (!Empty(fi.ofistamp))
							If fi.marcada != .T.
								Replace uCrsCabVendas.etReg With Round(uCrsCabVendas.etReg + fi.etiliquido,2)
							Endif
							Replace uCrsCabVendas.ofistamp With fi.ofistamp
							If Alltrim(olcCod) == "xxx" Or !(Alltrim(fi.nccod)==Strextract(uCrsCabVendas.Design, '[', ']', 1, 0))
								If fi.u_ettent1+fi.u_ettent2>0
									olcCod = Alltrim(fi.nccod) && validar altera��es de plano
								Endif
							Endif


							** validar altera��o de diplomas e nrs de receita
							If fi.marcada == .T.
								If Used("uCrsVale")
									Select ucrsvale
									Go Top
									Scan For Alltrim(ucrsvale.fistamp) == Alltrim(fi.ofistamp)
										&& validar diploma

										If !(Alltrim(ucrsvale.u_diploma) == Alltrim(fi.u_diploma))
											lcValidaDiploma=.T.
										Endif


										If !(Alltrim(ucrsvale.u_receita) == Alltrim(fi.receita))
											lcValidaReceita=.T.
										Endif
										** ate aqui

										Select ucrsvale
									Endscan
								Endif
							Endif
							**
						Endif
					Endif
				Endif
			Endif

			If (Alltrim(fi.Design) == Alltrim(uCrsCabVendas.Design)) And (fi.Lordem == uCrsCabVendas.Lordem)
				lcValida=.T. && encontrou venda
			Endif

			Select fi
		Endscan

		&& Condi��o que controla se deve ou n�o pedir impress�o de Receita - adicionada condi��o para n�o pedir no caso de serem RSPs - 20160725
		&& IF (lcComp == .f.) OR (Right(Alltrim(uCrsCabVendas.design),10) == "*SUSPENSA*") Or Right(Alltrim(uCrsCabVendas.design),18)=="*SUSPENSA RESERVA*" OR !EMPTY(uCrsCabVendas.mantemReceita)






		If (lcComp == .F.) Or (Right(Alltrim(uCrsCabVendas.Design),10) == "*SUSPENSA*") Or Right(Alltrim(uCrsCabVendas.Design),18)=="*SUSPENSA RESERVA*" Or !Empty(uCrsCabVendas.mantemreceita) Or (Upper(Alltrim(uCrsCabVendas.receita_tipo)) == "RSP" And Empty(uCrsCabVendas.planocomplem))

			Select uCrsCabVendas
			Replace uCrsCabVendas.sel 		With .T.
			Replace uCrsCabVendas.sel2 		With .T.
			Replace uCrsCabVendas.receita	With .T.
		Endif



		If lcValidaDev == .F.

			Select uCrsCabVendas
			Replace uCrsCabVendas.partes 	With 1
			Replace uCrsCabVendas.sel 		With .T.
			Replace uCrsCabVendas.sel2 		With .T.
			Replace uCrsCabVendas.receita	With .T.
		Endif

		&& Valida se Venda � Fatura ; Valida diferen�a de Planos de forma a anular a receita da Venda Original
		Select uCrsCabVendas



		If lcValidaReceita==.F. And lcValidaFact == .F. And lcValidaDiploma == .F. And ((Strextract(uCrsCabVendas.Design, '[', ']', 1, 0) == Alltrim(olcCod)) Or (Strextract(uCrsCabVendas.Design, '[', ']', 1, 0) == ''))

			Select uCrsCabVendas
			Replace uCrsCabVendas.Fact 		With .T.
		Endif

		If lcValidaFactDev == .F.
			Select uCrsCabVendas
			Replace uCrsCabVendas.FactDev 	With .T.
		Endif



		If lcValidaFact == .F. Or lcValidaFactDev == .F.
			Select uCrsCabVendas
			If !Empty(uCrsCabVendas.u_ltstamp) And (Strextract(uCrsCabVendas.Design, '[', ']', 1, 0) == Alltrim(olcCod)) And lcValidaDiploma == .F.

				Replace uCrsCabVendas.sel 		With .T.
				Replace uCrsCabVendas.sel2 		With .T.
				Replace uCrsCabVendas.receita	With .T.
			Endif
		Endif

		If (lcValidaFactPago=.T.)
			Select uCrsCabVendas
			Replace uCrsCabVendas.factPago With .T.
		Endif

		Store .F. To lcValida, lcComp, lcValidaDev, lcValidaFact, lcValidaFactDev, lcValidaDiploma
		olcCod="xxx"

		Select uCrsCabVendas
	Endscan

Endfunc


** funcao para gravar certificacao no atendimento
Function uf_pagamento_gravarCert
	Lparameters lnFtStamp, lnInvoiceData, lnHora, lnNdoc, lnEtotal, lnDocType, lcFno
	Local lcExecuteSQL, lcFno
	Store "" To lcExecuteSQL


	lcSqlCert = ''


	&& se usar a FT
	If(Empty(lnDocType))
		If Upper(Alltrim(ucrse1.pais)) == 'PORTUGAL'
			TEXT TO lcSQLCert NOSHOW TEXTMERGE
				exec up_pagamento_certificacao '<<lnFtStamp>>', '<<lnInvoiceData>>', '<<lnHora>>', <<lnNdoc>>, '<<lnEtotal>>', '<<mysite>>', @fno
			ENDTEXT
		Else
			TEXT TO lcSQLCert NOSHOW TEXTMERGE
				exec up_pagamento_certificacao_AO '<<lnFtStamp>>', '<<lnInvoiceData>>', '<<lnHora>>', <<lnNdoc>>, '<<lnEtotal>>', '<<mysite>>', @fno
			ENDTEXT
		Endif

	Else

		&& se utilizar BO ou RE
		TEXT TO lcSQLCert NOSHOW TEXTMERGE
			exec up_pagamento_certificacao '<<lnFtStamp>>', '<<lnInvoiceData>>', '<<lnHora>>', <<lnNdoc>>, '<<lnEtotal>>', '<<mysite>>', <<lcFno>> , '<<lnDocType>>'
		ENDTEXT

	Endif

	lcExecuteSQL  = lcExecuteSQL + Chr(13) + lcSqlCert

	Return lcExecuteSQL

Endfunc


** funcao p anular receitas qd a gravacao da venda d� erro e as vendas n�o s�o gravadas
Function uf_pagamento_receitasAnular
	Local lcNrReceita, lcToken


	If Used("uCrsReceitaEfetivar")
		Select ucrsReceitaEfetivar
		Go Top
		Scan For !Empty(Alltrim(ucrsReceitaEfetivar.resultado_comprovativo_registo))
			regua(0, 3, "A anular receita: "+Alltrim(ucrsReceitaEfetivar.receita_nr)+" no Sistema Central de Prescri��es...")
			regua(1, 1, "A anular receita: "+Alltrim(ucrsReceitaEfetivar.receita_nr)+" no Sistema Central de Prescri��es...")
			&&TODO 100003050001 : comentar linha baixo
			If(uf_atendimento_receitasAnular(Alltrim(ucrsReceitaEfetivar.receita_nr), Alltrim(ucrsReceitaEfetivar.token)))
				Select ucrsReceitaEfetivar
				Replace ucrsReceitaEfetivar.resultado_anulacao_cod With '100003050001'
			Endif
			regua(2)
		Endscan
	Endif
Endfunc


** funcao que mostra as receitas DEM que ficaram alocadas em lotes eletr�nicos - LL 20161126
Function uf_Pagamento_validarReceitas

	Create Cursor uCrsAuxReceitasComErro (nrreceita c(19))


	Select uCrsCabVendas
	Go Top
	Scan
		If !Empty(Alltrim(uCrsCabVendas.token)) And uCrsCabVendas.partes = 0 And Empty(uCrsCabVendas.mantemreceita)
			lcSQL = ''
			TEXT TO lcSQL NOSHOW TEXTMERGE
				exec up_dem_tipoLote '<<ALLTRIM(uCrsCabVendas.token)>>'
			ENDTEXT
			If !uf_gerais_actGrelha("", "uCrsValidacaoReceita", lcSQL)
				uf_perguntalt_chama("N�o foi poss�vel verificar o lote a que ficou atribuida a receita. Por favor contacte o suporte.", "", "OK", 32)
				Return .F.
			Else
				**MESSAGEBOX(uCrsValidacaoReceita.tipoLote)
				If Alltrim(uCrsValidacaoReceita.tipoLote) = '96' Or Alltrim(uCrsValidacaoReceita.tipoLote) = '98'
					Select uCrsAuxReceitasComErro
					Append Blank
					Replace uCrsAuxReceitasComErro.nrreceita With Strextract(uCrsCabVendas.Design, ':', ' -', 1, 0)
				Endif
			Endif
		Endif
	Endscan

	If Used("uCrsValidacaoReceita")
		fecha("uCrsValidacaoReceita")
	Endif

Endfunc

Function uf_PAGAMENTO_Posterior
	Select uCrsCabVendas
	Go Top
	Scan
		If uCrsCabVendas.modoPag=.T. Then
			Replace uCrsCabVendas.modoPag With .F.
			Replace ft.tpdesc With ""
			pPagPost=.F.
			Calculate Sum(uCrsCabVendas.valorPagar) To lcTotalPag For uCrsCabVendas.modoPag = .F.
			PAGAMENTO.txtValAtend.Value = Round(lcTotalPag,2)
			** Apresenta��o de contravalor em moeda no pagamento
			If uf_gerais_getParameter_site('ADM0000000051', 'BOOL', mySite) = .T.
				Local lccambio
				lccambio=0
				lcSQL = ''
				TEXT TO lcSQL Noshow Textmerge
					select top 1 cambio from cb where moeda= '<<ALLTRIM(uf_gerais_getParameter_site('ADM0000000052', 'TEXT', mySite))>>' and moedaOrigem = '<<uf_gerais_getParameter('ADM0000000260', 'TEXT')>>' order by data desc
				ENDTEXT
				If !uf_gerais_actGrelha("","curcambio",lcSQL)
					uf_perguntalt_chama("Aten��o: Ocorreu uma anomalia ao verificar o acesso do utilizador. Por favor contacte o Suporte.","OK","",16)
					Return .F.
				Endif
				If Reccount("curcambio")>0
					lccambio= curcambio.cambio
				Endif
				fecha("curcambio")
				PAGAMENTO.txtValAtendCVMoeda.Value = Round(lcTotalPag*lccambio,2)
			Endif

		Else
			Replace uCrsCabVendas.modoPag With .T.
			Replace ft.tpdesc With "Pagam. Posterior"
			pPagPost=.T.
			PAGAMENTO.txtValAtend.Value = 0
			** Apresenta��o de contravalor em moeda no pagamento
			If uf_gerais_getParameter_site('ADM0000000051', 'BOOL', mySite) = .T.
				PAGAMENTO.txtValAtendCVMoeda.Value = 0
			Endif

			PAGAMENTO.txtValRec.Value = "0.00"
			PAGAMENTO.txtTroco.Value = 0
		Endif
	Endscan
	**uf_pagamento_InactivaObjectosNegativo()
	If pPagPost=.T.
		PAGAMENTO.txtDinheiro.Enabled = .F.
		PAGAMENTO.txtMB.Enabled = .F.
		PAGAMENTO.txtVisa.Enabled = .F.
		PAGAMENTO.txtCheque.Enabled = .F.
		PAGAMENTO.txtModPag3.Enabled = .F.
		PAGAMENTO.txtModPag4.Enabled = .F.
		PAGAMENTO.txtModPag5.Enabled = .F.
		PAGAMENTO.txtModPag6.Enabled = .T.
		PAGAMENTO.btnDinheiro.Enable(.F.)
		PAGAMENTO.btnMB.Enable(.F.)
		PAGAMENTO.btnVisa.Enable(.F.)
		PAGAMENTO.btnCheque.Enable(.F.)
		PAGAMENTO.btnModPag3.Enable(.F.)
		PAGAMENTO.btnModPag4.Enable(.F.)
		PAGAMENTO.btnModPag5.Enable(.F.)
		PAGAMENTO.btnModPag6.Enable(.T.)
		PAGAMENTO.pgFrVDP.page1.grdVendas.credito.check1.ReadOnly = .T.
		PAGAMENTO.pgFrVDP.page1.grdVendas.credito.check1.Enabled = .F.
		**		pagamento.pgFrVDP.Page1.grdVendas.enabled = .f.


	Else
		PAGAMENTO.txtDinheiro.Enabled = .T.
		PAGAMENTO.txtMB.Enabled = .T.
		PAGAMENTO.txtVisa.Enabled = .T.
		PAGAMENTO.txtCheque.Enabled = .T.
		PAGAMENTO.txtModPag3.Enabled = .T.
		PAGAMENTO.txtModPag4.Enabled = .T.
		PAGAMENTO.txtModPag5.Enabled = .T.
		PAGAMENTO.txtModPag6.Enabled = .T.
		PAGAMENTO.btnDinheiro.Enable(.T.)
		PAGAMENTO.btnMB.Enable(.T.)
		PAGAMENTO.btnVisa.Enable(.T.)
		PAGAMENTO.btnCheque.Enable(.T.)
		PAGAMENTO.btnModPag3.Enable(.T.)
		PAGAMENTO.btnModPag4.Enable(.T.)
		PAGAMENTO.btnModPag5.Enable(.T.)
		PAGAMENTO.btnModPag6.Enable(.T.)
		PAGAMENTO.pgFrVDP.page1.grdVendas.credito.check1.ReadOnly = .F.
		PAGAMENTO.pgFrVDP.page1.grdVendas.credito.check1.Enabled = .T.
		**		pagamento.pgFrVDP.Page1.grdVendas.enabled = .t.
	Endif
	PAGAMENTO.Refresh()
	uf_PAGAMENTO_resetValoresModoPag()
Endfunc

Function uf_PAGAMENTO_GravarReg_fi_trans_info_line

	Local lcSQL
	lcSQL =''

	If(!Used("fi_trans_info"))
		Return lcSQL
	Endif

	Local lcPosTempAux
	Select fi_trans_info
	lcPosTempAux= Recno("fi_trans_info")



	Select fi_trans_info
	TEXT TO lcSql NOSHOW TEXTMERGE
		insert into fi_trans_info (
		token
		,recStamp
		,recTable
		,productCode
		,productCodeScheme
		,batchId
		,batchExpiryDate
		,packSerialNumber
		,clientTrxId
		,posTerminal
		,country_productNhrn
		,productNhrn
		,nmvsTrxId
		,state
		,reasons
		,code
		,description
		,Ousrinis
		,ousrdata
		,reqType
		,reqManual
		,nmvsTimeStamp
		,BatchIdReturn
		,BatchExpiryDateReturn
		,ProductCodeReturn
		,ProductNameReturn
		,ProductCommonNameReturn
		,SnReturn
		,UndoPossibleReturn
		,UserMatchesReturn
		,ProductNhrnReturn
		,alertIdReturn
		,site
		)
		values

			('<<ALLTRIM(fi_trans_info.token)>>'
			,'<<ALLTRIM(fi_trans_info.recStamp)>>'
			,'<<ALLTRIM(fi_trans_info.recTable)>>'
			,'<<ALLTRIM(fi_trans_info.productCode)>>'
			,'<<ALLTRIM(fi_trans_info.productCodeScheme)>>'
			,'<<ALLTRIM(fi_trans_info.batchId)>>'
			,'<<dtosql(fi_trans_info.batchExpiryDate)>>'
			,'<<ALLTRIM(fi_trans_info.packSerialNumber)>>'
			,'<<ALLTRIM(fi_trans_info.clientTrxId)>>'
			,'<<ALLTRIM(fi_trans_info.posTerminal)>>'
			,<<fi_trans_info.country_productNhrn>>
			,'<<ALLTRIM(fi_trans_info.productNhrn)>>'
			,'<<ALLTRIM(fi_trans_info.nmvsTrxId)>>'
			,'<<ALLTRIM(fi_trans_info.state)>>'
			,'<<ALLTRIM(fi_trans_info.reasons)>>'
			,'<<ALLTRIM(fi_trans_info.code)>>'
			,'<<ALLTRIM(fi_trans_info.description)>>'
			,'<<ALLTRIM(fi_trans_info.Ousrinis)>>'
			, getdate()
			,'<<ALLTRIM(fi_trans_info.reqType)>>'
			,<<IIF(fi_trans_info.reqManual,1,0)>>
			,'<<ALLTRIM(fi_trans_info.nmvsTimeStamp)>>'
			,'<<IIF(TYPE("fi_trans_info.BatchIdReturn") <> "U", ALLTRIM(fi_trans_info.BatchIdReturn), "")>>'
			,'<<IIF(TYPE("fi_trans_info.BatchExpiryDateReturn") <> "U", ALLTRIM(fi_trans_info.BatchExpiryDateReturn), "")>>'
			,'<<IIF(TYPE("fi_trans_info.ProductCodeReturn") <> "U", ALLTRIM(fi_trans_info.ProductCodeReturn), "")>>'
			,'<<IIF(TYPE("fi_trans_info.ProductNameReturn") <> "U", ALLTRIM(fi_trans_info.ProductNameReturn), "")>>'
			,'<<IIF(TYPE("fi_trans_info.ProductCommonNameReturn") <> "U", ALLTRIM(fi_trans_info.ProductCommonNameReturn), "")>>'
			,'<<IIF(TYPE("fi_trans_info.SnReturn") <> "U", ALLTRIM(fi_trans_info.SnReturn), "")>>'
			,<<IIF(TYPE("fi_trans_info.UndoPossibleReturn") <> 'U', IIF(fi_trans_info.UndoPossibleReturn, "1", "0"), "0")>>
			,<<IIF(TYPE("fi_trans_info.UserMatchesReturn") <> 'U', IIF(fi_trans_info.UserMatchesReturn, "1", "0"), "0")>>
			,'<<IIF(TYPE("fi_trans_info.ProductNhrnReturn") <> "U", ALLTRIM(fi_trans_info.ProductNhrnReturn), "")>>'
			,'<<IIF(TYPE("fi_trans_info.alertIdReturn") <> "U", ALLTRIM(fi_trans_info.alertIdReturn), "")>>'
			,'<<ALLTRIM(mysite)>>'
			)


	ENDTEXT

	Select fi_trans_info
	**TRY
	**   GOTO lcPosTempAux
	**ENDTRY

	uv_newClientTrxId = uf_gerais_stamp()

	Select fi_trans_info
	Replace fi_trans_info.clientTrxId With Alltrim(uv_newClientTrxId)

	Return lcSQL

Endfunc



Function uf_PAGAMENTO_GravarReg_fi_trans_info
	Local lcExecuteSQL, lcNrLin, lcL, uv_newClientTrxId
	Store "" To lcExecuteSQL, uv_newClientTrxId

	TEXT TO lcSql NOSHOW TEXTMERGE
		insert into fi_trans_info (
		token
		,recStamp
		,recTable
		,productCode
		,productCodeScheme
		,batchId
		,batchExpiryDate
		,packSerialNumber
		,clientTrxId
		,posTerminal
		,country_productNhrn
		,productNhrn
		,nmvsTrxId
		,state
		,reasons
		,code
		,description
		,Ousrinis
		,ousrdata
		,reqType
		,reqManual
		,nmvsTimeStamp

		)
		values
	ENDTEXT
	lcExecuteSQL  = lcExecuteSQL + Chr(13) + lcSQL
	lcL = 1
	Select fi_trans_info
	lnRecNo = Recn()
	Count For !Deleted() To lcNrLin
	Go Top
	Scan
		TEXT TO lcSql NOSHOW TEXTMERGE
			('<<ALLTRIM(fi_trans_info.token)>>'
			,'<<ALLTRIM(fi_trans_info.recStamp)>>'
			,'<<ALLTRIM(fi_trans_info.recTable)>>'
			,'<<ALLTRIM(fi_trans_info.productCode)>>'
			,'<<ALLTRIM(fi_trans_info.productCodeScheme)>>'
			,'<<ALLTRIM(fi_trans_info.batchId)>>'
			,'<<dtosql(fi_trans_info.batchExpiryDate)>>'
			,'<<ALLTRIM(fi_trans_info.packSerialNumber)>>'
			,'<<ALLTRIM(fi_trans_info.clientTrxId)>>'
			,'<<ALLTRIM(fi_trans_info.posTerminal)>>'
			,<<fi_trans_info.country_productNhrn)>>
			,'<<ALLTRIM(fi_trans_info.productNhrn)>>'
			,'<<ALLTRIM(fi_trans_info.nmvsTrxId)>>'
			,'<<ALLTRIM(fi_trans_info.state)>>'
			,'<<ALLTRIM(fi_trans_info.reasons)>>'
			,'<<ALLTRIM(fi_trans_info.code)>>'
			,'<<ALLTRIM(fi_trans_info.description)>>'
			,'<<ALLTRIM(fi_trans_info.Ousrinis)>>'
			, getdate()
			,'<<ALLTRIM(fi_trans_info.reqType)>>'
			,<<IIF(fi_trans_info.reqManual,1,0)>>
			,'<<ALLTRIM(fi_trans_info.nmvsTimeStamp)>>'
			)

		ENDTEXT
		If lcL < lcNrLin
			lcSQL = lcSQL + ','
		Endif
		lcL =lcL + 1
		lcExecuteSQL  = lcExecuteSQL + Chr(13) + lcSQL

		uv_newClientTrxId = uf_gerais_stamp()

		Select fi_trans_info
		Replace fi_trans_info.clientTrxId With Alltrim(uv_newClientTrxId)

	Endscan

	Return lcExecuteSQL

Endfunc


Function uf_PAGAMENTO_GravarReg_nrserie_com_results
	Local lcExecuteSQL, lcNrLin, lcL
	Store "" To lcExecuteSQL

	Select nrserie_com_results
	Go Top
	Scan
		Local lcRequestString, lcResponseString
		Store "" To lcRequestString, lcResponseString

		lcRequestString  = Strtran(Alltrim(nrserie_com_results.sent_string),Chr(39),'')
		lcResponseString = Strtran(Alltrim(nrserie_com_results.response_string),Chr(39),'')
		TEXT TO lcSql NOSHOW TEXTMERGE
			insert into nrserie_com_results ( stamp	,type ,nrserie ,source_table ,source_stamp ,sent_string ,response_string ,Ousrinis ,ousrdata)
			values	((select LEFT(newid(),20)) ,'<<ALLTRIM(nrserie_com_results.type)>>' ,'<<ALLTRIM(nrserie_com_results.nrserie)>>' ,'<<ALLTRIM(nrserie_com_results.source_table)>>' ,'<<ALLTRIM(nrserie_com_results.source_stamp)>>' ,'<<lcRequestString>>' ,'<<lcResponseString>>' ,'<<ALLTRIM(nrserie_com_results.Ousrinis)>>' ,getdate())
		ENDTEXT
		lcExecuteSQL  = lcExecuteSQL + Chr(13) + lcSQL
	Endscan

	Return lcExecuteSQL

Endfunc


Function uf_PAGAMENTO_GravarReg_mixed_bulk_pend

	Local lcExecuteSQL, lcNrLin, lcL, uv_newClientTrxId
	Store "" To lcExecuteSQL

	lcNrLin= 0

	If(Used("mixed_bulk_pend"))

		Select mixed_bulk_pend
		lnRecNo = Recn()
		Count For !Deleted() To lcNrLin

		If(lcNrLin <1)
			Return ''
		Endif

	Endif



	TEXT TO lcSql NOSHOW TEXTMERGE
		insert into mixed_bulk_pend
			(mbstamp
			,token
			,recStamp
			,recTable
			,productCode
			,productCodeScheme
			,batchId
			,batchExpiryDate
			,packSerialNumber
			,clientTrxId
			,posTerminal
			,country_productNhrn
			,productNhrn
			,Ousrinis
			,ousrdata
			,tipo
			,nmvsTrxId
			,reqManual
			,reasons
			,code
			,state
			,description
			,nmvsTimeStamp
			,site
			,reqType
			)
		values
	ENDTEXT
	lcExecuteSQL  = lcExecuteSQL + Chr(13) + lcSQL
	lcL = 1
	Select mixed_bulk_pend
	lnRecNo = Recn()
	Count For !Deleted() To lcNrLin
	Go Top
	Scan

		uv_newClientTrxId = uf_gerais_stamp()

		Set Century On

		TEXT TO lcSQL NOSHOW TEXTMERGE
				(LEFT(newid(),21)
				,'<<ALLTRIM(mixed_bulk_pend.token)>>'
				,'<<ALLTRIM(mixed_bulk_pend.recStamp)>>'
				,'<<ALLTRIM(mixed_bulk_pend.recTable)>>'
				,'<<ALLTRIM(mixed_bulk_pend.productCode)>>'
				,'<<ALLTRIM(mixed_bulk_pend.productCodeScheme)>>'
				,'<<ALLTRIM(mixed_bulk_pend.batchId)>>'
				,'<<dtosql(mixed_bulk_pend.batchExpiryDate)>>'
				,'<<ALLTRIM(mixed_bulk_pend.packSerialNumber)>>'
				,'<<ALLTRIM(uv_newClientTrxId)>>'
				,'<<ALLTRIM(mixed_bulk_pend.posTerminal)>>'
				,<<mixed_bulk_pend.country_productNhrn>>
				,'<<ALLTRIM(mixed_bulk_pend.productNhrn)>>'
				,'<<ALLTRIM(mixed_bulk_pend.ousrinis)>>'
				,'<<mixed_bulk_pend.ousrdata>>'
				,'<<ALLTRIM(mixed_bulk_pend.tipo)>>'
				,'<<ALLTRIM(mixed_bulk_pend.nmvsTrxId)>>'
				,<<IIF(mixed_bulk_pend.reqManual,1,0)>>
				,'<<ALLTRIM(mixed_bulk_pend.reasons)>>'
				,'<<ALLTRIM(mixed_bulk_pend.code)>>'
				,'<<ALLTRIM(mixed_bulk_pend.state)>>'
				,'<<ALLTRIM(mixed_bulk_pend.description)>>'
				,'<<ALLTRIM(mixed_bulk_pend.nmvsTimeStamp)>>'
				,'<<ALLTRIM(mysite)>>'
				,ISNULL((SELECT campo FROM b_multidata(nolock) where tipo = 'MVO-<<ALLTRIM(mixed_bulk_pend.tipo)>>'), '')
				)
		ENDTEXT
		If lcL < lcNrLin
			lcSQL = lcSQL + ','
		Endif
		lcL =lcL + 1
		lcExecuteSQL  = lcExecuteSQL + Chr(13) + lcSQL
		Select mixed_bulk_pend
	Endscan



	Return lcExecuteSQL

Endfunc

Function uf_pagamento_send_maqdinheiro

Endfunc

Function uf_pagamento_moeda
	Lparameters moeda
	Public lcValPagMOEDA
	Store 0 To lcValPagMOEDA
	uf_tecladonumerico_chama("lcValPagMOEDA", "Qual o valor pago em "+Alltrim(moeda)+"?:", 0, .T., .F., 6, 2)

	TEXT TO lcSql NOSHOW textmerge
		select top 1 cambio from cb where moeda='<<ALLTRIM(moeda)>>' AND moedaOrigem = '<<uf_gerais_getParameter('ADM0000000260', 'TEXT')>>' order by data desc
	ENDTEXT
	If !uf_gerais_actGrelha("","curcamb",lcSQL)
		uf_perguntalt_chama("OCORREU UM ERRO A OBTER O C�MBIO.","OK","",64)
		Return .F.
	Endif
	Local lcRetValMoeda
	Store 0 To lcRetValMoeda
	Select curcamb
	If Reccount("curcamb")=0
		uf_perguntalt_chama("N�O EXISTE NENHUM C�MBIO REGISTADO PARA A MOEDA "+Alltrim(moeda)+". POR FAVOR VERIFIQUE!","OK","",64)
		fecha("curcamb")
	Else
		lcRetValMoeda = Round(lcValPagMOEDA * (1/curcamb.cambio),2)
		fecha("curcamb")
	Endif

	If Type("PAGAMENTO") != 'U'
		PAGAMENTO.txtValRecMoeda.Visible=.T.
		PAGAMENTO.txtValRecMoedaSimb.Visible=.T.
		PAGAMENTO.txtValRecMoeda.Value = astr(lcValPagMOEDA,15,2)
	Endif

	TEXT TO lcSql NOSHOW textmerge
		select top 1 simbolo from moedas where moeda='<<ALLTRIM(moeda)>>'
	ENDTEXT
	If !uf_gerais_actGrelha("","curcambs",lcSQL)
		uf_perguntalt_chama("OCORREU UM ERRO OBTER O SIMBOLO DA MOEDA.","OK","",64)
		Return .F.
	Endif
	Local lcRetValMOEDAS
	Store '' To lcRetValMOEDAS
	Select curcambs
	If Reccount("curcambs")=0
		uf_perguntalt_chama("N�O EXISTE NENHUM S�MBOLO REGISTADO PARA A MOEDA "+Alltrim(moeda)+". POR FAVOR VERIFIQUE!","OK","",64)
		fecha("curcambs")
	Else
		lcRetValMOEDAS = Alltrim(curcambs.simbolo)
		fecha("curcambs")
	Endif
	If Type("PAGAMENTO") != 'U'
		PAGAMENTO.txtValRecMoedaSimb.Caption = lcRetValMOEDAS
	Endif

	valpagmoeda = lcValPagMOEDA
	moedapag = moeda

	Return lcRetValMoeda
Endfunc

Function uf_pagamento_verifplafond

	&& Novo Controle Plafond Entidade de Fatura��o - 20150515 - Lu�s Leal
	If ucrse1.controla_plafond == .T.
		If lcCntC > 0 && tem cr�ditos
			Select uCrsCLEF
			If uCrsCLEF.no != 200 And ucrse1.plafond_cliente > 0
				Local lcValorTotal
				Store 0 To lcValorTotal

				Select uCrsCabVendas
				Calculate Sum(uCrsCabVendas.valorPagar) For uCrsCabVendas.modoPag == .T. To lcValorTotal
				If uCrsCLEF.eplafond == 0
					If (uCrsCLEF.esaldo + lcValorTotal) > ucrse1.plafond_cliente
						** uf_perguntalt_chama("O plafond do cliente foi ultrapassado." + CHR(13) + "Pretende continuar com o atendimento?","Sim","N�o",64)

					Endif
				Else
					If (uCrsCLEF.esaldo+lcValorTotal) > uCrsCLEF.eplafond
						** uf_perguntalt_chama("O plafond do cliente foi ultrapassado." + CHR(13) + "Pretende continuar com o atendimento?","Sim","N�o",64)

					Endif
				Endif
			Else
				Select uCrsCLEF
				If uCrsCLEF.no != 200 &&AND uCrsCLEF.eplafond > 0
					Local lcValorTotal
					Store 0 To lcValorTotal

					Select uCrsCabVendas
					Calculate Sum(uCrsCabVendas.valorPagar) For uCrsCabVendas.modoPag==.T. To lcValorTotal

					If (uCrsCLEF.esaldo+lcValorTotal) > uCrsCLEF.eplafond And pPagPost=.F.
						**IF uf_perguntalt_chama("O plafond do cliente foi ultrapassado." + CHR(13) + "Pretende continuar com o atendimento?","Sim","N�o",64)

					Endif
				Endif
			Endif s
		Endif
	Endif
Endfunc

Function uf_pagamento_utilplafond
	If UtilPlafond = .T.
		uf_utentes_chamaRecebimentos(.T.)
	Endif
Endfunc

Function uf_pagamento_fact_seguradora
	Local lcCabStampSeg, lcCodPlano, lcCabStamp, lcStampLin, lcNdoc, lcCatIVA, lcPercCatIVA, lcTotCativa, lcPCatIVA, uv_totDesc, uv_valDesc
	Store '' To lcCabStampSeg, lcCabStamp, lcStampLin
	Store 0 To lcCodPlano, lcNdoc, lcPercCatIVA, lcTotCativa, lcPCatIVA, uv_totDesc, uv_valDesc
	Store .F. To lcCatIVA


	regua(1,2,"A processar o documento para a Seguradora. Por favor aguarde.")

	Select uCrsCabFactSeg
	Go Top
	Scan
		lcCabStampSeg = Alltrim(uCrsCabFactSeg.ftstamp)
		lcCabStamp = uf_gerais_stamp()

		uf_gerais_actGrelha("","SFT","exec up_touch_ft '"+Alltrim(lcCabStampSeg)+"'")
		uf_gerais_actGrelha("","SFT2","exec up_facturacao_ft2 '"+Alltrim(lcCabStampSeg)+"'")

		lcSQL = ""
		TEXT TO lcSQL NOSHOW TEXTMERGE
            exec up_facturacao_linhas 0, <<mysite_nr>>, '<<ALLTRIM(lcCabStampSeg)>>'
		ENDTEXT
		uf_gerais_actGrelha("","SFI",lcSQL)
		uf_gerais_actGrelha("","SFI2","select * from fi2 where ftstamp = '"+Alltrim(lcCabStampSeg)+"'")

		Select sft
		Replace sft.ftstamp With Alltrim(lcCabStamp)
		Select sfi
		Replace sfi.ftstamp With Alltrim(lcCabStamp)
		Select sft2
		Replace sft2.stamporigproc With sft2.ft2stamp
		Replace sft2.ft2stamp With Alltrim(lcCabStamp)
		Select sfi2
		Replace sfi2.ftstamp With  Alltrim(lcCabStamp)

		Select sfi
		Go Top
		Delete From sfi Where sfi.u_ettent1 = 0

		Local lcContaDocSeg
		lcContaDocSeg = 0

		Local lnPrecisao
		lnPrecisao = 4

		Select sfi
		Go Top
		Scan
			Local lnUettent1
			lnUettent1 = sfi.u_ettent1


			**comparticipa��o externa original, apenas valida para comparts eletronicas
			Select sfi2
			Go Top
			Locate For Alltrim(sfi2.fistamp) = Alltrim(sfi.fistamp)
			If Found()
				If(sfi2.returnContributedValue>0)
					lnUettent1 = Round(Round(sfi2.returnContributedValue,lnPrecisao) / (1-(sfi2.descontoHabitual/100)),lnPrecisao)
				Endif
			Endif

			lcStampLin = uf_gerais_stamp()
			Update sfi2 Set fistamp = Alltrim(lcStampLin) Where sfi2.fistamp=sfi.fistamp



			Select sfi
			Replace sfi.ofistamp With Alltrim(sfi.fistamp)
			Replace sfi.fistamp With Alltrim(lcStampLin)
			Replace sfi.etiliquido With lnUettent1
			Replace sfi.Pv With Round(lnUettent1/sfi.qtt,lnPrecisao)
			Replace sfi.pvmoeda With Round(lnUettent1/sfi.qtt,lnPrecisao)
			Replace sfi.epv With Round(lnUettent1/sfi.qtt,lnPrecisao)
			Replace sfi.u_ettent1 With 0
			Replace sfi.u_txcomp With 100-sfi.u_txcomp
			Replace sfi.u_comp With .F.
			Replace sfi.Comp With 0
			Replace sfi.comp_diploma With 0
			lcContaDocSeg = lcContaDocSeg + 1
		Endscan



		If lcContaDocSeg = 0
			regua(2)
			Return .F.
		Endif

		Select sft2
		lcCodPlano = Alltrim(sft2.u_codigo)

		TEXT TO lcObterCodigoUtenteSQL NOSHOW TEXTMERGE
			exec up_obter_Utente_No_from_Plano '<<lcCodPlano>>'
		ENDTEXT

		If (!uf_gerais_actGrelha("","ucrsDadosSeg",lcObterCodigoUtenteSQL)) Then
			regua(2)
			uf_perguntalt_chama("OCORREU UMA ANOMALIA AO GRAVAR O DOCUMENTO DE FATURA��O DA SEGURADORA. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			Return .F.
		Else
			Select ucrsDadosSeg
			If Reccount("ucrsDadosSeg") = 0 Then
				regua(2)
				uf_perguntalt_chama("OCORREU UMA ANOMALIA AO GRAVAR O DOCUMENTO DE FATURA��O DA SEGURADORA. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				Return .F.
			Else
				Select ucrsDadosSeg
				Go Top
				If (ucrsDadosSeg.no) <= 0 Then
					regua(2)
					uf_perguntalt_chama("OCORREU UMA ANOMALIA AO GRAVAR O DOCUMENTO DE FATURA��O DA SEGURADORA. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
					Return .F.
				Endif
			Endif
		Endif

		lcCatIVA = ucrsDadosSeg.cativa
		lcPercCatIVA = ucrsDadosSeg.perccativa/100
		lcPCatIVA = ucrsDadosSeg.perccativa
		lcTotCativa = 0.00

		Select sft
		Replace sft.no With ucrsDadosSeg.no
		Replace sft.estab With 0
		Replace sft.Nome With ucrsDadosSeg.Nome
		Replace sft.morada With ucrsDadosSeg.morada
		Replace sft.Local With ucrsDadosSeg.Local
		Replace sft.codpost With ucrsDadosSeg.codpost
		Replace sft.ncont With ucrsDadosSeg.ncont
		Replace sft.telefone With ucrsDadosSeg.telefone
		Replace sft.zona With ucrsDadosSeg.zona
		Replace sft.tipo With ucrsDadosSeg.tipo
		Replace sft.u_lote With 0
		Replace sft.u_nslote With 0
		Replace sft.u_ltstamp With ''
		Replace sft.u_slote With 0

		Select sft2
		Replace sft2.morada With ucrsDadosSeg.morada
		Replace sft2.Local With ucrsDadosSeg.Local
		Replace sft2.codpost With ucrsDadosSeg.codpost
		Replace sft2.telefone With ucrsDadosSeg.telefone
		Replace sft2.email With ucrsDadosSeg.email
		Replace sft2.evdinheiro With 0
		Replace sft2.u_acertovs With 0
		Replace sft2.cativa With lcCatIVA
		Replace sft2.perccativa With lcPCatIVA

		TEXT TO lcSQL NOSHOW TEXTMERGE
			SELECT ndoc, tipodoc from td WHERE site='<<ALLTRIM(mysite)>>' AND nmdoc='Ft. Seguradoras'
		ENDTEXT
		uf_gerais_actGrelha("","ucrsDadosSegTD",lcSQL)
		Select sft
		Replace sft.ndoc With ucrsDadosSegTD.ndoc
		Replace sft.nmdoc With 'Ft. Seguradoras'
		lcNdoc = ucrsDadosSegTD.ndoc
		Replace sft.tipodoc With ucrsDadosSegTD.tipodoc

		Update sfi Set sfi.ndoc = ucrsDadosSegTD.ndoc
		Update sfi Set sfi.nmdoc = 'Ft. Seguradoras'
		Update sfi Set sfi.tipodoc = ucrsDadosSegTD.tipodoc


		** validar dados fatura
		Local lcTotQtt, lcQtt1, lcEtotal, lcEttIliq, lcEcusto, lcEttIva, EivaIn1, EivaIn2, EivaIn3, EivaIn4, EivaIn5, EivaIn6, EivaIn7, EivaIn8, EivaIn9, EivaV1, EivaV2, EivaV3, EivaV4, EivaV5, EivaV6, EivaV7, EivaV8, EivaV9, lcTotDesc , lcNrVenda , EivaIn10 , EivaIn11, EivaIn12, EivaIn13, EivaV10, EivaV11, EivaV12, EivaV13
		Store 0 To lcTotQtt, lcQtt1, lcEtotal, lcEttIliq, lcEcusto, lcEttIva, EivaIn1, EivaIn2, EivaIn3, EivaIn4, EivaIn5, EivaIn6, EivaIn7, EivaIn8, EivaIn9, EivaV1, EivaV2, EivaV3, EivaV4, EivaV5, EivaV6, EivaV7, EivaV8, EivaV9, lcTotDesc , lcNrVenda , EivaIn10 , EivaIn11, EivaIn12, EivaIn13, EivaV10, EivaV11, EivaV12, EivaV13

		Select sfi
		Go Top
		Scan
			&& Calculo da base de incid�ncia (lcEttIliq), calculo total documento (lcEtotal) � feito linha � linha
			lcTotQtt		= lcTotQtt	+ sfi.qtt
			lcQtt1			= lcQtt1	+ sfi.qtt
			lcEcusto		= lcEcusto	+ sfi.ecusto

			Select sfi
			Replace sfi.desc2 With 0, sfi.u_descval With 0

			Select sfi2
			Go Top
			Locate For Alltrim(sfi2.fistamp) = Alltrim(sfi.fistamp)
			If Found()
				Select sfi
				Replace  sfi.desconto With sfi2.descontoHabitual
			Endif




			Select sfi
			*IF sFi.desconto > 0  OR sfi.desc2>0 OR sfi.u_descval>0 && And fi.etiliquido >0
			If sfi.ivaincl
				lcValorSemIva = Round((sfi.epv*fi.qtt) / (sfi.iva / 100 + 1), lnPrecisao)
			Else
				lcValorSemIva = Round(sfi.epv*sfi.qtt,lnPrecisao)
			Endif

			If sfi.desconto>0
				lcDesc = lcValorSemIva - Round(lcValorSemIva * Round(Round((sfi.epv*(sfi.desconto/100)),lnPrecisao) * qtt,lnPrecisao), lnPrecisao)
				lcTotDesc = lcTotDesc + lcDesc
				lcValorSemIva = lcValorSemIva - lcTotDesc

				uv_valDesc = Round(Round((sfi.epv*(sfi.desconto/100)),lnPrecisao) * qtt, lnPrecisao)
				uv_totDesc = uv_totDesc + uv_valDesc

			Endif

			Select sfi

			Replace sfi.etiliquido With Round((sfi.epv - (sfi.epv*(sfi.desconto/100))) * qtt,2)

			If sfi.desc2 > 0
				lcDesc = lcValorSemIva - Round(lcValorSemIva * ((100-sfi.desc2)/100), lnPrecisao)
				lcTotDesc = lcTotDesc + lcDesc
				lcValorSemIva = lcValorSemIva - lcTotDesc
			Endif

			If sfi.u_descval>0
				If sfi.ivaincl
					lcDesc = sfi.u_descval/(sfi.iva/100+1)
				Else
					lcDesc = sfi.u_descval
				Endif
				lcTotDesc = lcTotDesc + lcDesc
			Endif
			*ENDIF

			lcEtotal		= lcEtotal	+ sfi.etiliquido
			lcEttIliq		= lcEttIliq	+ (sfi.etiliquido/(sfi.iva/100+1))

			&& Calculo do IVA � feito tamb�m � linha
			Do Case
				Case sfi.tabiva = 1
					EivaIn1	= EivaIn1 + (sfi.etiliquido/(sfi.iva/100+1))
					EivaV1	= EivaV1 + sfi.etiliquido - (sfi.etiliquido/(sfi.iva/100+1))
				Case sfi.tabiva = 2
					EivaIn2	= EivaIn2 + (sfi.etiliquido/(sfi.iva/100+1))
					EivaV2	= EivaV2 + sfi.etiliquido - (sfi.etiliquido/(sfi.iva/100+1))
				Case sfi.tabiva = 3
					EivaIn3	= EivaIn3 + (sfi.etiliquido/(sfi.iva/100+1))
					EivaV3	= EivaV3 + sfi.etiliquido - (sfi.etiliquido/(sfi.iva/100+1))
				Case sfi.tabiva = 4
					EivaIn4	= EivaIn4 + (sfi.etiliquido/(sfi.iva/100+1))
					EivaV4	= EivaV4 + sfi.etiliquido - (sfi.etiliquido/(sfi.iva/100+1))
				Case sfi.tabiva = 5
					EivaIn5	= EivaIn5 + (sfi.etiliquido/(sfi.iva/100+1))
					EivaV5	= EivaV5 + sfi.etiliquido - (sfi.etiliquido/(sfi.iva/100+1))
				Case sfi.tabiva = 6
					EivaIn6	= EivaIn6 + (sfi.etiliquido/(sfi.iva/100+1))
					EivaV6	= EivaV6 + sfi.etiliquido - (sfi.etiliquido/(sfi.iva/100+1))
				Case sfi.tabiva = 7
					EivaIn7	= EivaIn7 + (sfi.etiliquido/(sfi.iva/100+1))
					EivaV7	= EivaV7 + sfi.etiliquido - (sfi.etiliquido/(sfi.iva/100+1))
				Case sfi.tabiva = 8
					EivaIn8	= EivaIn8 + (fi.etiliquido/(fi.iva/100+1))
					EivaV8	= EivaV8 + sfi.etiliquido - (sfi.etiliquido/(sfi.iva/100+1))
				Case sfi.tabiva = 9
					EivaIn9	= EivaIn9 + (sfi.etiliquido/(sfi.iva/100+1))
					EivaV9	= EivaV9 + sfi.etiliquido - (sfi.etiliquido/(sfi.iva/100+1))
				Case sfi.tabiva = 10
					EivaIn10	= EivaIn10 + (sfi.etiliquido/(sfi.iva/100+1))
					EivaV10	= EivaV10 + sfi.etiliquido - (sfi.etiliquido/(sfi.iva/100+1))
				Case sfi.tabiva = 11
					EivaIn11	= EivaIn11 + (sfi.etiliquido/(sfi.iva/100+1))
					EivaV11	= EivaV11 + sfi.etiliquido - (sfi.etiliquido/(sfi.iva/100+1))
				Case sfi.tabiva = 12
					EivaIn12	= EivaIn12 + (sfi.etiliquido/(sfi.iva/100+1))
					EivaV12	= EivaV12 + sfi.etiliquido - (sfi.etiliquido/(sfi.iva/100+1))
				Case sfi.tabiva = 13
					EivaIn13	= EivaIn13 + (sfi.etiliquido/(sfi.iva/100+1))
					EivaV13	= EivaV13 + sfi.etiliquido - (sfi.etiliquido/(sfi.iva/100+1))
			Endcase

		Endscan

		**IF !EMPTY(uv_totDesc)
		**	lcEtotal = lcEtotal - uv_totDesc
		**ENDIF

		**uv_descFin = uf_gerais_getUmValor("b_utentes", "desconto", "no = " + ASTR(sft.no) + " and estab = " + ASTR(sft.estab))

		**IF !EMPTY(uv_descFin)

		**	SELECT sft
		**	REPLACE sft.fin WITH uv_descFin
		**	REPLACE sft.efinv WITH ((sft.fin*lcEtotal)/100)

		**	lcEtotal = lcEtotal - sft.efinv

		**ENDIF

		If !Used("uCrsValIva")
			Create Cursor uCrsValIva ;
				(lcEivaIn1 numeric(19,4), lcEivaIn2 numeric(19,4), lcEivaIn3 numeric(19,4), lcEivaIn4 numeric(19,4), lcEivaIn5 numeric(19,4), lcEivaIn6 numeric(19,4), lcEivaIn7 numeric(19,4), lcEivaIn8 numeric(19,4), lcEivaIn9 numeric(19,4), lcEivaIn10 numeric(19,4), lcEivaIn11 numeric(19,4), lcEivaIn12 numeric(19,4), lcEivaIn13 numeric(19,4),;
				lcEivaV1 numeric(19,4), lcEivaV2 numeric(19,4), lcEivaV3 numeric(19,4), lcEivaV4 numeric(19,4), lcEivaV5 numeric(19,4), lcEivaV6 numeric(19,4), lcEivaV7 numeric(19,4), lcEivaV8 numeric(19,4), lcEivaV9 numeric(19,4), lcEivaV10 numeric(19,4), lcEivaV11 numeric(19,4), lcEivaV12 numeric(19,4), lcEivaV13 numeric(19,4))
		Else
			fecha("uCrsValIva")
			**SELECT uCrsValIva
			**DELETE ALL
			Create Cursor uCrsValIva ;
				(lcEivaIn1 numeric(19,4), lcEivaIn2 numeric(19,4), lcEivaIn3 numeric(19,4), lcEivaIn4 numeric(19,4), lcEivaIn5 numeric(19,4), lcEivaIn6 numeric(19,4), lcEivaIn7 numeric(19,4), lcEivaIn8 numeric(19,4), lcEivaIn9 numeric(19,4), lcEivaIn10 numeric(19,4), lcEivaIn11 numeric(19,4), lcEivaIn12 numeric(19,4), lcEivaIn13 numeric(19,4),;
				lcEivaV1 numeric(19,4), lcEivaV2 numeric(19,4), lcEivaV3 numeric(19,4), lcEivaV4 numeric(19,4), lcEivaV5 numeric(19,4), lcEivaV6 numeric(19,4), lcEivaV7 numeric(19,4), lcEivaV8 numeric(19,4), lcEivaV9 numeric(19,4), lcEivaV10 numeric(19,4), lcEivaV11 numeric(19,4), lcEivaV12 numeric(19,4), lcEivaV13 numeric(19,4))
		Endif
		Select uCrsValIva
		Append Blank
		Replace ;
			uCrsValIva.lcEivaIn1 With Round(EivaIn1,2) ;
			uCrsValIva.lcEivaIn2 With Round(EivaIn2,2) ;
			uCrsValIva.lcEivaIn3 With Round(EivaIn3,2) ;
			uCrsValIva.lcEivaIn4 With Round(EivaIn4,2) ;
			uCrsValIva.lcEivaIn5 With Round(EivaIn5,2) ;
			uCrsValIva.lcEivaIn6 With Round(EivaIn6,2) ;
			uCrsValIva.lcEivaIn7 With Round(EivaIn7,2) ;
			uCrsValIva.lcEivaIn8 With Round(EivaIn8,2) ;
			uCrsValIva.lcEivaIn9 With Round(EivaIn9,2) ;
			uCrsValIva.lcEivaIn10 With Round(EivaIn10,2) ;
			uCrsValIva.lcEivaIn11 With Round(EivaIn11,2) ;
			uCrsValIva.lcEivaIn12 With Round(EivaIn12,2) ;
			uCrsValIva.lcEivaIn13 With Round(EivaIn13,2) ;
			uCrsValIva.lcEivaV1 With Round(EivaV1,2) ;
			uCrsValIva.lcEivaV2 With Round(EivaV2,2) ;
			uCrsValIva.lcEivaV3 With Round(EivaV3,2) ;
			uCrsValIva.lcEivaV4 With Round(EivaV4,2) ;
			uCrsValIva.lcEivaV5 With Round(EivaV5,2) ;
			uCrsValIva.lcEivaV6 With Round(EivaV6,2) ;
			uCrsValIva.lcEivaV7 With Round(EivaV7,2) ;
			uCrsValIva.lcEivaV8 With Round(EivaV8,2) ;
			uCrsValIva.lcEivaV9 With Round(EivaV9,2) ;
			uCrsValIva.lcEivaV10 With Round(EivaV10,2) ;
			uCrsValIva.lcEivaV11 With Round(EivaV11,2) ;
			uCrsValIva.lcEivaV12 With Round(EivaV12,2) ;
			uCrsValIva.lcEivaV13 With Round(EivaV13,2)


		** preencher a cativa��o do IVA
		If uf_gerais_getParameter_site('ADM0000000071', 'BOOL', mySite) And lcCatIVA
			Local lcValIvaLin
			Store 0 To lcValIvaLin
			Select sfi
			Go Top
			Scan
				lcValIvaLin = 0.00
				If sfi.iva <> 0
					lcValIvaLin  = Iif(sfi.ivaincl=.T., Round(sfi.etiliquido-(sfi.etiliquido/(1+(sfi.iva/100))),2),Round(sfi.etiliquido*(sfi.iva/100),2))
					If sfi.partes=0
						Update sfi2 Set valcativa = Round(lcValIvaLin * lcPercCatIVA ,lnPrecisao) Where sfi2.fistamp = sfi.fistamp
						lcTotCativa = lcTotCativa + Round(lcValIvaLin * lcPercCatIVA ,lnPrecisao)
					Else
						Update sfi2 Set valcativa = Round(lcValIvaLin * lcPercCatIVA ,lnPrecisao)*(-1) Where sfi2.fistamp = sfi.fistamp
						lcTotCativa = lcTotCativa + (Round(lcValIvaLin * lcPercCatIVA ,lnPrecisao)*(-1))
					Endif
				Endif
			Endscan
			Select sft2
			Replace sft2.valcativa With lcTotCativa
		Endif

		** inserir fatura

		Select sft2
		lcSqlFt2 = uf_PAGAMENTO_GravarVendaFt2_Seg( Alltrim(sft2.ft2stamp) , lcNdoc, Alltrim(sft2.u_receita), '', '', '', '', '', '', sft2.ousrhora, '', '', '')
		lcSqlFt2 = uf_gerais_trataPlicasSQL(lcSqlFt2)

		TEXT TO lcSQLFtDeclare NOSHOW TEXTMERGE
			DECLARE @fno as numeric(9,0)
			DECLARE @tabNewDocNr TABLE (newDocNr NUMERIC(9, 0))
		ENDTEXT

		Select sft
		lcSQLFt = uf_PAGAMENTO_GravarVendaFt_Seg(;
			ALLTRIM(sft.ftstamp), Alltrim(sft.nmdoc), sft.ndoc, sft.tipodoc, sft.no, sft.Nome, sft.estab, lcTotQtt, lcQtt1, Round(lcEtotal,2), Round(lcEttIliq,2), lcEcusto,;
			ROUND(uCrsValIva.lcEivaV1+uCrsValIva.lcEivaV2+uCrsValIva.lcEivaV3+uCrsValIva.lcEivaV4+uCrsValIva.lcEivaV5+uCrsValIva.lcEivaV6+uCrsValIva.lcEivaV7+uCrsValIva.lcEivaV8+uCrsValIva.lcEivaV9+uCrsValIva.lcEivaV10+uCrsValIva.lcEivaV11+uCrsValIva.lcEivaV12+uCrsValIva.lcEivaV13,2),;
			ROUND(lcTotDesc,2), sft.pdata, sft.ousrhora, .F.,;
			uf_gerais_getDate(Date(),'SQL'),;
			sft.ncont,;
			uf_gerais_getDate(Date(),'SQL'), Alltrim(nrAtendimento))
		lcSQLFt = uf_gerais_trataPlicasSQL(lcSQLFtDeclare + lcSQLFt)

		Select sft
		lcSqlFi	= uf_PAGAMENTO_gravarVendaFi_Seg(Alltrim(sft.ftstamp), Alltrim(sft.nmdoc), sft.ndoc, sft.tipodoc, lcNrVenda, sft.ousrhora, '')
		lcSqlFi = uf_gerais_trataPlicasSQL(lcSqlFi)

		Select sft
		lcSqlCert = uf_pagamento_gravarCert(Alltrim(sft.ftstamp), uf_gerais_getDate(Date(),'SQL'), sft.ousrhora, sft.ndoc, sft.etotal)
		lcSqlCert = uf_gerais_trataPlicasSQL(lcSqlCert)

		lcSQLInsertDoc = lcSqlFt2 + Chr(13) + lcSQLFt + Chr(13) + lcSqlFi + Chr(13) +  lcSqlCert

		If !Empty(lcSQLInsertDoc)
			lcSQL = ''
			**_cliptext = lcSQLInsertDoc
			TEXT TO lcSQL NOSHOW TEXTMERGE
				exec up_gerais_execSql 'Atendimento - Insert', 1,'<<lcSQLInsertDoc>>', '', '', '' , '', ''
			ENDTEXT

			If !uf_gerais_actGrelha("","uCrsFnoAT",lcSQL)
				uf_perguntalt_chama("OCORREU UMA ANOMALIA AO GRAVAR O DOCUMENTO DE COMPARTICIPA��O DA SEGURADORA. POR FAVOR VERIFIQUE.","OK","",16)
			Endif
		Endif

		If uf_gerais_getParameter_site("ADM0000000176", "BOOL", mySite)
			Select sft2
			uf_pagamento_codbar_app_seguradora(Alltrim(sft2.ft2stamp))
		Endif

		regua(2)

		&& Enviar faturas comparticipadas via WS

		If CompSemSimul = .T.
			Select sft
			TEXT TO lcSQLdescr NOSHOW TEXTMERGE
                select td.tiposaft+ ' '+ltrim(rtrim(str(ft.ndoc)))+ltrim(rtrim(str(ft.ftano)))+'/'+ltrim(rtrim(str(fno))) as docdescr from ft (nolock)
                inner join td (nolock) on ft.ndoc=td.ndoc where ftstamp='<<ALLTRIM(sft.FtStamp)>>'
			ENDTEXT


			If !uf_gerais_actGrelha("","docdescr",lcSQLdescr)
				uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VALIDAR O N� DO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE. OBRIGADO.","OK","",16)
			Endif


			uf_gerais_get_efrId(sft.no)

			If !Used("ucrTempLCefrID")
				uf_perguntalt_chama("N�O FOI POSS�VEL OBTER O ID DA SEGURADORA. POR FAVOR CONTACTE O SUPORTE. OBRIGADO.","OK","",16)
			Else
				Local lcFs2stamp,lcNrelegibilidade, lcStamporigproc, lcEfrId, lcAbrev
				Select sft2
				lcFs2stamp = Alltrim(sft2.ft2stamp)
				lcNrelegibilidade= Alltrim(sft2.nrelegibilidade)
				lcStamporigproc= Alltrim(sft2.stamporigproc)


				Select ucrTempLCefrID
				lcEfrId = Alltrim(ucrTempLCefrID.efrID)
				lcAbrev= Alltrim(ucrTempLCefrID.abrev)


				Select docdescr
				Go Top
				uf_compart_envia(lcStamporigproc, Alltrim(docdescr.docdescr), Alltrim(lcEfrId), lcNrelegibilidade,lcFs2stamp,lcAbrev)
			Endif
			fecha("ucrTempLCefrID")
			fecha("docdescr")
		Endif

		Local lcFindNdoc, lcFindDocNr, lcSqlFinal
		Select sft
		lcFindNdoc = sft.ndoc
		lcFindDocNr = sft.fno
		lcSqlFinal = lcSQLFt


		fecha("sft")
		fecha("sft2")
		fecha("sfi")
		fecha("sfi2")
		fecha("ucrsDadosSeg")
		fecha("ucrsDadosSegTD")

		If uf_perguntalt_chama("QUER IMPRIMIR OU ASSOCIAR O DOCUMENTO AUTOM�TICAMENTE AO PROCESSO?","Imprimir","Associar")
			FTSeguradora = 'IMPRIME'
			StampFTSeguradora = Alltrim(lcCabStamp)
		Else
			FTSeguradora = 'GUARDA'
			StampFTSeguradora = Alltrim(lcCabStamp)
		Endif


		uf_pagamento_validaDocumentoEnviaEmail(Alltrim(StampFTSeguradora),lcFindDocNr,lcFindNdoc,lcSqlFinal, nrAtendimento)

		Select uCrsCabFactSeg
	Endscan

	regua(2)

Endfunc


Function uf_pagamento_nc_seguradora
	Lparameters stampdoc

	Local lcCabStampSeg, lcNrSeguradora, lcCabStamp, lcStampLin, lcNdoc, lnNnDocOri, lnNrDocOri, lnDtDocOri
	Store '' To lcCabStampSeg, lcCabStamp, lcStampLin, lnNnDocOri
	Store 0 To lcNrSeguradora, lcNdoc, lnNrDocOri
	Store Date() To lnDtDocOri

	regua(1,2,"A processar o Cr�dito para a Seguradora. Por favor aguarde.")

	lcCabStampSeg = Alltrim(stampdoc)
	lcCabStamp = uf_gerais_stamp()
	myStampNCSeg = Alltrim(lcCabStamp)

	uf_gerais_actGrelha("","SFT","exec up_touch_ft '"+Alltrim(lcCabStampSeg)+"'")
	uf_gerais_actGrelha("","SFT2","exec up_facturacao_ft2 '"+Alltrim(lcCabStampSeg)+"'")

	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE
        exec up_facturacao_linhas 0, <<mysite_nr>>, '<<ALLTRIM(lcCabStampSeg)>>'
	ENDTEXT
	uf_gerais_actGrelha("","SFI",lcSQL)
	uf_gerais_actGrelha("","SFI2","select * from fi2 where ftstamp = '"+Alltrim(lcCabStampSeg)+"'")

	Select sft
	Replace sft.ftstamp With Alltrim(lcCabStamp)
	Replace sft.etotal With sft.etotal*(-1)
	Replace sft.EivaIn1 With sft.EivaIn1*(-1)
	Replace sft.EivaIn2 With sft.EivaIn2*(-1)
	Replace sft.EivaIn3 With sft.EivaIn3*(-1)
	Replace sft.EivaIn4 With sft.EivaIn4*(-1)
	Replace sft.EivaIn5 With sft.EivaIn5*(-1)
	Replace sft.EivaIn6 With sft.EivaIn6*(-1)
	Replace sft.EivaIn7 With sft.EivaIn7*(-1)
	Replace sft.EivaIn8 With sft.EivaIn8*(-1)
	Replace sft.EivaIn9 With sft.EivaIn9*(-1)
	Replace sft.EivaIn10 With sft.EivaIn10*(-1)
	Replace sft.EivaIn11 With sft.EivaIn11*(-1)
	Replace sft.EivaIn12 With sft.EivaIn12*(-1)
	Replace sft.EivaIn13 With sft.EivaIn13*(-1)
	Replace sft.EivaV1 With sft.EivaV1*(-1)
	Replace sft.EivaV2 With sft.EivaV2*(-1)
	Replace sft.EivaV3 With sft.EivaV3*(-1)
	Replace sft.EivaV4 With sft.EivaV4*(-1)
	Replace sft.EivaV5 With sft.EivaV5*(-1)
	Replace sft.EivaV6 With sft.EivaV6*(-1)
	Replace sft.EivaV7 With sft.EivaV7*(-1)
	Replace sft.EivaV8 With sft.EivaV8*(-1)
	Replace sft.EivaV9 With sft.EivaV9*(-1)
	Replace sft.EivaV10 With sft.EivaV10*(-1)
	Replace sft.EivaV11 With sft.EivaV11*(-1)
	Replace sft.EivaV12 With sft.EivaV12*(-1)
	Replace sft.EivaV13 With sft.EivaV13*(-1)
	Replace sft.Total With sft.Total*(-1)
	Replace sft.ttiliq With sft.ttiliq*(-1)
	Replace sft.ousrhora With Time()
	Replace sft.usrhora With Time()

	Select sfi
	Replace sfi.ftstamp With Alltrim(lcCabStamp)

	Select sft2
	StampProcNC = Alltrim(sft2.ft2stamp)
	Replace sft2.ft2stamp With Alltrim(lcCabStamp)

	Select sfi2
	Replace sfi2.ftstamp With  Alltrim(lcCabStamp)


	Select sfi
	Go Top
	Scan
		lcStampLin = uf_gerais_stamp()
		Select sfi
		Replace sfi.ofistamp With Alltrim(sfi.fistamp)
		Replace sfi.fistamp With Alltrim(lcStampLin)
		Replace sfi.etiliquido With sfi.etiliquido*(-1)
		Replace sfi.oref With sfi.ref
		Replace sfi.tipodoc With 3
		Replace sfi.esltt With sfi.esltt*(-1)
		Replace sfi.amostra With .T.
		Replace sfi.ousrhora With Time()
		Replace sfi.usrhora With Time()
		lnNnDocOri = Alltrim(sfi.nmdoc)
		lnNrDocOri = Alltrim(Str(sfi.ndoc))+'/'+Alltrim(Str(sfi.fno))
		lnDtDocOri = sfi.rdata
	Endscan

	**lnNnDocOri, lnNrDocOri, lnDtDocOri

	Select sfi
	Go Top
	Append Blank
	lcStampLin = uf_gerais_stamp()
	Select sfi
	Replace sfi.ofistamp With ''
	Replace sfi.fistamp With Alltrim(lcStampLin)
	Replace sfi.Design With 'Anula��o ou retifica��o de ' + Alltrim(lnNnDocOri) + ' Nr. ' + Alltrim(lnNrDocOri) + ' de ' + uf_gerais_getDate(lnDtDocOri) + '.'
	Replace sfi.etiliquido With 0
	Replace sfi.oref With ''
	Replace sfi.tipodoc With 3
	Replace sfi.esltt With 0
	Replace sfi.amostra With .T.
	Replace sfi.ousrhora With Time()
	Replace sfi.usrhora With Time()
	Replace sfi.armazem With myArmazem
	Replace sfi.Lordem With 10
	Replace sfi.fivendedor With sft.vendedor
	Replace sfi.fivendnm With sft.vendnm


	TEXT TO lcSQL NOSHOW TEXTMERGE
		SELECT ndoc, tipodoc from td WHERE site='<<ALLTRIM(mysite)>>' AND nmdoc='Nt. Cr�d. Segur.'
	ENDTEXT
	uf_gerais_actGrelha("","ucrsDadosSegTD",lcSQL)
	Select sft
	Replace sft.ndoc With ucrsDadosSegTD.ndoc
	Replace sft.nmdoc With 'Nt. Cr�d. Segur.'
	lcNdoc = ucrsDadosSegTD.ndoc
	Replace sft.tipodoc With ucrsDadosSegTD.tipodoc

	Update sfi Set sfi.ndoc = ucrsDadosSegTD.ndoc
	Update sfi Set sfi.nmdoc = 'Nt. Cr�d. Segur.'
	Update sfi Set sfi.tipodoc = ucrsDadosSegTD.tipodoc

	** validar dados fatura
	Local lcTotQtt, lcQtt1, lcEtotal, lcEttIliq, lcEcusto, lcEttIva, EivaIn1, EivaIn2, EivaIn3, EivaIn4, EivaIn5, EivaIn6, EivaIn7, EivaIn8, EivaIn9, EivaV1, EivaV2, EivaV3, EivaV4, EivaV5, EivaV6, EivaV7, EivaV8, EivaV9, lcTotDesc , lcNrVenda , EivaIn10, EivaIn11, EivaIn12, EivaIn13, EivaV10, EivaV11, EivaV12, EivaV13
	Store 0 To lcTotQtt, lcQtt1, lcEtotal, lcEttIliq, lcEcusto, lcEttIva, EivaIn1, EivaIn2, EivaIn3, EivaIn4, EivaIn5, EivaIn6, EivaIn7, EivaIn8, EivaIn9, EivaV1, EivaV2, EivaV3, EivaV4, EivaV5, EivaV6, EivaV7, EivaV8, EivaV9, lcTotDesc , lcNrVenda , EivaIn10, EivaIn11, EivaIn12, EivaIn13, EivaV10, EivaV11, EivaV12, EivaV13

	Select sfi
	Go Top
	Scan
		&& Calculo da base de incid�ncia (lcEttIliq), calculo total documento (lcEtotal) � feito linha � linha
		lcTotQtt		= lcTotQtt	+ sfi.qtt
		lcQtt1			= lcQtt1	+ sfi.qtt
		lcEtotal		= lcEtotal	+ sfi.etiliquido
		lcEttIliq		= lcEttIliq	+ (sfi.etiliquido/(sfi.iva/100+1))
		lcEcusto		= lcEcusto	+ sfi.ecusto

		&& Calculo do IVA � feito tamb�m � linha
		Do Case
			Case sfi.tabiva = 1
				EivaIn1	= EivaIn1 + (sfi.etiliquido/(sfi.iva/100+1))
				EivaV1	= EivaV1 + sfi.etiliquido - (sfi.etiliquido/(sfi.iva/100+1))
			Case sfi.tabiva = 2
				EivaIn2	= EivaIn2 + (sfi.etiliquido/(sfi.iva/100+1))
				EivaV2	= EivaV2 + sfi.etiliquido - (sfi.etiliquido/(sfi.iva/100+1))
			Case sfi.tabiva = 3
				EivaIn3	= EivaIn3 + (sfi.etiliquido/(sfi.iva/100+1))
				EivaV3	= EivaV3 + sfi.etiliquido - (sfi.etiliquido/(sfi.iva/100+1))
			Case sfi.tabiva = 4
				EivaIn4	= EivaIn4 + (sfi.etiliquido/(sfi.iva/100+1))
				EivaV4	= EivaV4 + sfi.etiliquido - (sfi.etiliquido/(sfi.iva/100+1))
			Case sfi.tabiva = 5
				EivaIn5	= EivaIn5 + (sfi.etiliquido/(sfi.iva/100+1))
				EivaV5	= EivaV5 + sfi.etiliquido - (sfi.etiliquido/(sfi.iva/100+1))
			Case sfi.tabiva = 6
				EivaIn6	= EivaIn6 + (sfi.etiliquido/(sfi.iva/100+1))
				EivaV6	= EivaV6 + sfi.etiliquido - (sfi.etiliquido/(sfi.iva/100+1))
			Case sfi.tabiva = 7
				EivaIn7	= EivaIn7 + (sfi.etiliquido/(sfi.iva/100+1))
				EivaV7	= EivaV7 + sfi.etiliquido - (sfi.etiliquido/(sfi.iva/100+1))
			Case sfi.tabiva = 8
				EivaIn8	= EivaIn8 + (fi.etiliquido/(fi.iva/100+1))
				EivaV8	= EivaV8 + sfi.etiliquido - (sfi.etiliquido/(sfi.iva/100+1))
			Case sfi.tabiva = 9
				EivaIn9	= EivaIn9 + (sfi.etiliquido/(sfi.iva/100+1))
				EivaV9	= EivaV9 + sfi.etiliquido - (sfi.etiliquido/(sfi.iva/100+1))
			Case sfi.tabiva = 10
				EivaIn10	= EivaIn10 + (sfi.etiliquido/(sfi.iva/100+1))
				EivaV10	= EivaV10 + sfi.etiliquido - (sfi.etiliquido/(sfi.iva/100+1))
			Case sfi.tabiva = 11
				EivaIn11	= EivaIn11 + (sfi.etiliquido/(sfi.iva/100+1))
				EivaV11	= EivaV11 + sfi.etiliquido - (sfi.etiliquido/(sfi.iva/100+1))
			Case sfi.tabiva = 12
				EivaIn12	= EivaIn12 + (sfi.etiliquido/(sfi.iva/100+1))
				EivaV12	= EivaV12 + sfi.etiliquido - (sfi.etiliquido/(sfi.iva/100+1))
			Case sfi.tabiva = 13
				EivaIn13	= EivaIn13 + (sfi.etiliquido/(sfi.iva/100+1))
				EivaV13	= EivaV13 + sfi.etiliquido - (sfi.etiliquido/(sfi.iva/100+1))
		Endcase

		If sfi.desconto > 0  Or sfi.desc2>0 Or sfi.u_descval>0 && And fi.etiliquido >0
			If sfi.ivaincl
				lcValorSemIva = (sfi.epv*fi.qtt) / (sfi.iva / 100 + 1)
			Else
				lcValorSemIva = sfi.epv*sfi.qtt
			Endif

			If sfi.desconto>0
				lcDesc = lcValorSemIva - Round(lcValorSemIva * ((100-sfi.desconto)/100), 2)
				lcTotDesc = lcTotDesc + lcDesc
				lcValorSemIva = lcValorSemIva - lcTotDesc

			Endif

			If sfi.desc2 > 0
				lcDesc = lcValorSemIva - (lcValorSemIva * ((100-sfi.desc2)/100))
				lcTotDesc = lcTotDesc + lcDesc
				lcValorSemIva = lcValorSemIva - lcTotDesc
			Endif

			If sfi.u_descval>0
				If sfi.ivaincl
					lcDesc = sfi.u_descval/(sfi.iva/100+1)
				Else
					lcDesc = sfi.u_descval
				Endif
				lcTotDesc = lcTotDesc + lcDesc
			Endif

		Endif

	Endscan

	**uv_descFin = uf_gerais_getUmValor("b_utentes", "desconto", "no = " + ASTR(sft.no) + " and estab = " + ASTR(sft.estab))

	**IF !EMPTY(uv_descFin)

	**	SELECT sft
	**	REPLACE sft.fin WITH uv_descFin
	**	REPLACE sft.efinv WITH ((sft.fin*lcEtotal)/100)

	**	lcEtotal = lcEtotal - sft.efinv

	**ENDIF

	If !Used("uCrsValIva")
		Create Cursor uCrsValIva ;
			(lcEivaIn1 numeric(19,4), lcEivaIn2 numeric(19,4), lcEivaIn3 numeric(19,4), lcEivaIn4 numeric(19,4), lcEivaIn5 numeric(19,4), lcEivaIn6 numeric(19,4), lcEivaIn7 numeric(19,4), lcEivaIn8 numeric(19,4), lcEivaIn9 numeric(19,4), lcEivaIn10 numeric(19,4), lcEivaIn11 numeric(19,4), lcEivaIn12 numeric(19,4), lcEivaIn13 numeric(19,4),;
			lcEivaV1 numeric(19,4), lcEivaV2 numeric(19,4), lcEivaV3 numeric(19,4), lcEivaV4 numeric(19,4), lcEivaV5 numeric(19,4), lcEivaV6 numeric(19,4), lcEivaV7 numeric(19,4), lcEivaV8 numeric(19,4), lcEivaV9 numeric(19,4), lcEivaV10 numeric(19,4), lcEivaV11 numeric(19,4), lcEivaV12 numeric(19,4), lcEivaV13 numeric(19,4))
	Else
		**SELECT uCrsValIva
		**DELETE ALL
		fecha("uCrsValIva")
		Create Cursor uCrsValIva ;
			(lcEivaIn1 numeric(19,4), lcEivaIn2 numeric(19,4), lcEivaIn3 numeric(19,4), lcEivaIn4 numeric(19,4), lcEivaIn5 numeric(19,4), lcEivaIn6 numeric(19,4), lcEivaIn7 numeric(19,4), lcEivaIn8 numeric(19,4), lcEivaIn9 numeric(19,4), lcEivaIn10 numeric(19,4), lcEivaIn11 numeric(19,4), lcEivaIn12 numeric(19,4), lcEivaIn13 numeric(19,4),;
			lcEivaV1 numeric(19,4), lcEivaV2 numeric(19,4), lcEivaV3 numeric(19,4), lcEivaV4 numeric(19,4), lcEivaV5 numeric(19,4), lcEivaV6 numeric(19,4), lcEivaV7 numeric(19,4), lcEivaV8 numeric(19,4), lcEivaV9 numeric(19,4), lcEivaV10 numeric(19,4), lcEivaV11 numeric(19,4), lcEivaV12 numeric(19,4), lcEivaV13 numeric(19,4))
	Endif
	Select uCrsValIva
	Append Blank
	Replace ;
		uCrsValIva.lcEivaIn1 With Round(EivaIn1,2) ;
		uCrsValIva.lcEivaIn2 With Round(EivaIn2,2) ;
		uCrsValIva.lcEivaIn3 With Round(EivaIn3,2) ;
		uCrsValIva.lcEivaIn4 With Round(EivaIn4,2) ;
		uCrsValIva.lcEivaIn5 With Round(EivaIn5,2) ;
		uCrsValIva.lcEivaIn6 With Round(EivaIn6,2) ;
		uCrsValIva.lcEivaIn7 With Round(EivaIn7,2) ;
		uCrsValIva.lcEivaIn8 With Round(EivaIn8,2) ;
		uCrsValIva.lcEivaIn9 With Round(EivaIn9,2) ;
		uCrsValIva.lcEivaIn10 With Round(EivaIn10,2) ;
		uCrsValIva.lcEivaIn11 With Round(EivaIn11,2) ;
		uCrsValIva.lcEivaIn12 With Round(EivaIn12,2) ;
		uCrsValIva.lcEivaIn13 With Round(EivaIn13,2) ;
		uCrsValIva.lcEivaV1 With Round(EivaV1,2) ;
		uCrsValIva.lcEivaV2 With Round(EivaV2,2) ;
		uCrsValIva.lcEivaV3 With Round(EivaV3,2) ;
		uCrsValIva.lcEivaV4 With Round(EivaV4,2) ;
		uCrsValIva.lcEivaV5 With Round(EivaV5,2) ;
		uCrsValIva.lcEivaV6 With Round(EivaV6,2) ;
		uCrsValIva.lcEivaV7 With Round(EivaV7,2) ;
		uCrsValIva.lcEivaV8 With Round(EivaV8,2) ;
		uCrsValIva.lcEivaV9 With Round(EivaV9,2) ;
		uCrsValIva.lcEivaV10 With Round(EivaV10,2) ;
		uCrsValIva.lcEivaV11 With Round(EivaV11,2) ;
		uCrsValIva.lcEivaV12 With Round(EivaV12,2) ;
		uCrsValIva.lcEivaV13 With Round(EivaV13,2)

	** inserir fatura

	Local NrATNCSeg
	NrATNCSeg = uf_atendimento_geraNrAtendimento_return()

	Select sft2
	lcSqlFt2 = uf_PAGAMENTO_GravarVendaFt2_Seg( Alltrim(sft2.ft2stamp) , lcNdoc, Alltrim(sft2.u_receita), '', '', '', '', '', '', sft2.ousrhora, '', '', '')
	lcSqlFt2 = uf_gerais_trataPlicasSQL(lcSqlFt2)

	TEXT TO lcSQLFtDeclare NOSHOW TEXTMERGE
		DECLARE @fno as numeric(9,0)
		DECLARE @tabNewDocNr TABLE (newDocNr NUMERIC(9, 0))
	ENDTEXT

	Select sft
	lcSQLFt = uf_PAGAMENTO_GravarVendaFt_Seg(;
		ALLTRIM(sft.ftstamp), Alltrim(sft.nmdoc), sft.ndoc, sft.tipodoc, sft.no, sft.Nome, sft.estab, lcTotQtt, lcQtt1, Round(lcEtotal,2), Round(lcEttIliq,2), lcEcusto,;
		ROUND(uCrsValIva.lcEivaV1+uCrsValIva.lcEivaV2+uCrsValIva.lcEivaV3+uCrsValIva.lcEivaV4+uCrsValIva.lcEivaV5+uCrsValIva.lcEivaV6+uCrsValIva.lcEivaV7+uCrsValIva.lcEivaV8+uCrsValIva.lcEivaV9+uCrsValIva.lcEivaV10+uCrsValIva.lcEivaV11+uCrsValIva.lcEivaV12+uCrsValIva.lcEivaV13,2),;
		ROUND(lcTotDesc,2), sft.pdata, sft.ousrhora, .F.,;
		uf_gerais_getDate(Date(),'SQL'),;
		sft.ncont,;
		uf_gerais_getDate(Date(),'SQL'), Alltrim(NrATNCSeg))
	lcSQLFt = uf_gerais_trataPlicasSQL(lcSQLFtDeclare + lcSQLFt)

	Select sft
	lcSqlFi	= uf_PAGAMENTO_gravarVendaFi_Seg(Alltrim(sft.ftstamp), Alltrim(sft.nmdoc), sft.ndoc, sft.tipodoc, lcNrVenda, sft.ousrhora, '')
	lcSqlFi = uf_gerais_trataPlicasSQL(lcSqlFi)

	Select sft
	lcSqlCert = uf_pagamento_gravarCert(Alltrim(sft.ftstamp), uf_gerais_getDate(Date(),'SQL'), sft.ousrhora, sft.ndoc, sft.etotal)
	lcSqlCert = uf_gerais_trataPlicasSQL(lcSqlCert)

	uf_gerais_regista_logs_passos("B_PAGCENTRAL",nrAtendimento,"b_pag7","insert antes uf_pagamento_nc_seguradora")

	Select sft
	lcSQLPagCentral = ''
	TEXT TO lcSQLPagCentral TEXTMERGE NOSHOW
		insert into B_pagCentral
			(stamp,nrAtend,nrVendas,Total,ano,no,estab,vendedor,nome
			,terminal,terminal_nome,dinheiro,mb,visa,cheque,troco,oData,uData,fechado,abatido,fechaStamp,fechaUser
			,evdinheiro,epaga1,epaga2,echtotal,evdinheiro_semTroco,etroco,total_bruto,devolucoes,ntCredito,creditos,cxstamp,ssstamp,site,obs,epaga3,epaga4,epaga5,epaga6,exportado,pontosAt,campanhas
			,id_contab,valpagmoeda,moedapag,nrMotivoMov)
			select '<<astr(YEAR(DATE())) + ALLTRIM(NrATNCSeg)>>','<<ALLTRIM(NrATNCSeg)>>',1,0.00,YEAR(getdate()),<<sft.no>>,0,1,'<<ALLTRIM(ch_vendnm)>>'
			,<<myTermNo>> ,'<<ALLTRIM(myTerm)>>',0,0,0,0,0,getdate(),getdate(),1,0,'',0
			,0,0,0,0,0,0,0,0,0,0,'<<ALLTRIM(myCxStamp)>>','<<ALLTRIM(mySsStamp)>>','<<ALLTRIM(mySite)>>','',0,0,0,0,0,0,''
			,0,0,'',''
	ENDTEXT

	lcSQLPagCentral = uf_gerais_trataPlicasSQL(lcSQLPagCentral)

	lcSQLInsertDoc = lcSqlFt2 + Chr(13) + lcSQLFt + Chr(13) + lcSqlFi + Chr(13) +  lcSqlCert + Chr(13) + lcSQLPagCentral

	If !Empty(lcSQLInsertDoc)
		lcSQL = ''
		**_cliptext = lcSQLInsertDoc
		TEXT TO lcSQL NOSHOW TEXTMERGE
			exec up_gerais_execSql 'Atendimento - Insert', 1,'<<lcSQLInsertDoc>>', '', '', '' , '', ''
		ENDTEXT

		If !uf_gerais_actGrelha("","uCrsFnoAT",lcSQL)
			uf_perguntalt_chama("OCORREU UMA ANOMALIA AO GRAVAR O DOCUMENTO DE COMPARTICIPA��O DA SEGURADORA. POR FAVOR VERIFIQUE.","OK","",16)
		Endif
	Endif

	uf_gerais_regista_logs_passos("B_PAGCENTRAL",nrAtendimento,"b_pag7","insert depois uf_pagamento_nc_seguradora")

	If uf_gerais_getParameter_site("ADM0000000176", "BOOL", mySite)
		Select sft2
		uf_pagamento_codbar_app_seguradora(Alltrim(sft2.ft2stamp))
	Endif

	TEXT TO lcSQL NOSHOW TEXTMERGE
		select * from ft_compart (nolock) where ftstamp=(select stamporigproc from ft2 (nolock) where ft2stamp='<<ALLTRIM(StampProcNC)>>') and type=2
	ENDTEXT
	If !uf_gerais_actGrelha("","uCrsTemCompart",lcSQL)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VERIFICAR O DOCUMENTO ORIGINAL DE COMPARTICIPA��O DA SEGURADORA. POR FAVOR VERIFIQUE.","OK","",16)
	Endif
	Select uCrsTemCompart
	If Reccount("uCrsTemCompart")>0
		uf_compart_anula(Alltrim(uCrsTemCompart.token), Alltrim(uCrsTemCompart.ftstamp))
	Endif
	fecha("uCrsTemCompart")


	Local lcFindNdoc, lcFindDocNr, lcSqlFinal
	Select sft
	lcFindNdoc = sft.ndoc
	lcFindDocNr = sft.fno
	lcSqlFinal = lcSQLFt

	fecha("sft")
	fecha("sft2")
	fecha("sfi")
	fecha("sfi2")
	fecha("ucrsDadosSeg")
	fecha("ucrsDadosSegTD")

	regua(2)

	FTSeguradora = 'NCSEGIMPRIME'
	StampFTSeguradora = Alltrim(lcCabStamp)


	uf_pagamento_validaDocumentoEnviaEmail(Alltrim(StampFTSeguradora),lcFindDocNr,lcFindNdoc,lcSqlFinal, nrAtendimento)

Endfunc



Function uf_pagamento_codbar_app_seguradora
	Lparameters ValPrint

	Local lcValPrint
	lcValPrint = ValPrint
	uf_gerais_setImpressoraPOS(.T.)

	??? Chr(29) + Chr(104) +Chr(50) + Chr(29)+ Chr(72) + Chr(2) + Chr(29)+ Chr(119) +Chr(1) + Chr(29)+Chr(107)+Chr(4) + Upper(Alltrim(lcValPrint)) + Chr(0)
	uf_gerais_textoLJPOS()

	??	Chr(27)+Chr(97)+Chr(1)
	?'UTILIZE ESTE CODIGO DE BARRAS'
	?'PARA IDENTIFICAR O DOCUMENTO'
	?'NA APP INSURANCEDOC'
	?

	&& Centrar Informa��o
	uf_gerais_textoCJPOS()
	uf_gerais_feedCutPOS()
	uf_gerais_setImpressoraPOS(.F.)

Endfunc

Function uf_pagamento_procreservas

	Select ucrsCabVendasres


	Local lcresusanifgenerico
	lcresusanifgenerico = .F.
	Select ucrsCabVendasres
	Go Top
	Scan
		If ucrsCabVendasres.nifgenerico=.T.
			lcresusanifgenerico = .T.
		Endif
	Endscan

	regua(1,3,"A gerar Reservas...")
	If lcResSAD = .T.
		If !uf_reservas_automatica(.T., 'RN', 0, lcresusanifgenerico )
			**	RETURN .f.
		Endif
	Endif

	If lcResAD100 = .T.
		If !uf_reservas_automatica(.T., 'RAD100', 100, lcresusanifgenerico )
			**	RETURN .f.
		Endif
	Endif

	If lcResAdParc = .T.
		If !uf_reservas_automatica(.T., 'RADP', 100, lcresusanifgenerico )
			**	RETURN .f.
		Endif
	Endif



	regua(1,3,"A gerar Reservas...")
	Select uCrsCabVendas
	Go Top
	Delete From uCrsCabVendas Where Left(Alltrim(uCrsCabVendas.Design),7)='Reserva'
	Select uCrsCabVendas
	Go Top

	**PAGAMENTO.pgFrVDP.page1.grdVendas.refresh
	regua(2)
	Select fi
	Go Top
	**pagamento.show

	lcResSAD = .F.
	lcResAD100 = .F.
	lcResAdParc = .F.

	Return .T.

Endfunc

Function uf_pagamento_proclinhasenc

	Local lcStampEncAct
	lcStampEncAct = ''
	Select ucrsLinFiBi
	Go Top
	Scan
		If !Empty(ucrsLinFiBi.bistamp)

			Local lcbistampchk, lcTotLin
			lcbistampchk = Alltrim(ucrsLinFiBi.bistamp)
			lcTotLin = ucrsLinFiBi.etiliquido
			TEXT TO lcSQLdescr NOSHOW TEXTMERGE
				select ettdeb from bi where bistamp='<<ALLTRIM(lcbistampchk)>>'
			ENDTEXT
			If !uf_gerais_actGrelha("","ucrsBoFecharC",lcSQLdescr)
				uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VALIDAR AS LINHAS DOS DOCUMENTOS A SEREM FECHADOS. POR FAVOR CONTACTE O SUPORTE. OBRIGADO.","OK","",16)
			Endif

			Select ucrsBoFecharC
			If ucrsBoFecharC.ettdeb <> lcTotLin
				Select ucrsLinFiBi
				TEXT TO lcSQLdescr NOSHOW TEXTMERGE
					update bi SET qtt = <<ucrsLinFiBi.qtt>>, epu=<<ucrsLinFiBi.epv>>, edebito=<<ucrsLinFiBi.epv>>, ettdeb=<<ucrsLinFiBi.etiliquido>>, desconto=<<ucrsLinFiBi.desconto>> where bistamp='<<ALLTRIM(ucrsLinFiBi.bistamp)>>'
				ENDTEXT
				If !uf_gerais_actGrelha("","",lcSQLdescr)
					uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VALIDAR AS LINHAS DOS DOCUMENTOS A SEREM FECHADOS. POR FAVOR CONTACTE O SUPORTE. OBRIGADO.","OK","",16)
				Endif
				Select ucrsLinFiBi
				Replace ucrsLinFiBi.act With .T.
			Endif
			fecha("ucrsBoFecharC")
			Select ucrsLinFiBi
		Else

			Local lcnmdos, lcndos, lcobrano, lcNo, lcnome, lclocal, lcmorada, lccodpost
			Store '' To lcnmdos, lcnome, lclocal, lcmorada, lccodpost, lcdataobra
			Store 0 To lcndos, lcobrano, lcNo
			Select ucrsLinFiBi
			TEXT TO lcSQLdescr NOSHOW TEXTMERGE
				select nmdos, ndos, obrano, no, nome, local, morada, codpost, dataobra from bo where bostamp='<<ALLTRIM(ucrsLinFiBi.bostamp)>>'
			ENDTEXT
			If !uf_gerais_actGrelha("","ucrsInfoBoI",lcSQLdescr)
				uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VALIDAR AS LINHAS DOS DOCUMENTOS A SEREM FECHADOS. POR FAVOR CONTACTE O SUPORTE. OBRIGADO.","OK","",16)
			Endif
			Select ucrsInfoBoI
			lcnmdos = ucrsInfoBoI.nmdos
			lcndos = ucrsInfoBoI.ndos
			lcobrano = ucrsInfoBoI.obrano
			lcNo = ucrsInfoBoI.no
			lcnome = ucrsInfoBoI.Nome
			lclocal = ucrsInfoBoI.Local
			lcmorada = ucrsInfoBoI.morada
			lccodpost = ucrsInfoBoI.codpost
			lcdataobra  = ucrsInfoBoI.dataobra
			fecha("ucrsInfoBoI")

			Select ucrsLinFiBi
			TEXT TO lcSQLBi1 NOSHOW TEXTMERGE PRETEXT 8

				INSERT into bi (bistamp, bostamp, nmdos, ndos, obrano
								, ref, codigo, design, qtt, qtt2
								,iva, tabiva, armazem, ar2mazem
								, desconto, no,nome,local,morada,codpost
								,epu,edebito,epcusto,ettdeb,rdata,dataobra
								,dataopen,obistamp, rescli,lordem
								,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora, datafinal,u_reserva, vendedor, vendnm
								, eslvu,esltt, fechada
				)
				SELECT LEFT(newid(),21)	,'<<ALLTRIM(ucrsLinFiBi.bostamp)>>','<<alltrim(lcnmdos)>>', <<lcndos>>,<<lcobrano>>,
					'<<Alltrim(ucrsLinFiBi.ref)>>', '<<Alltrim(ucrsLinFiBi.ref)>>', '<<Alltrim(ucrsLinFiBi.design)>>', <<ucrsLinFiBi.qtt>>, <<ucrsLinFiBi.qtt>>,
					<<ucrsLinFiBi.IVA>>,<<ucrsLinFiBi.TABIVA>>,<<ucrsLinFiBi.ARMAZEM>>,0, <<ucrsLinFiBi.desconto>>,
					<<lcno >>,'<<ALLTRIM(lcnome )>>','<<ALLTRIM(lclocal )>>', '<<ALLTRIM(lcmorada )>>', '<<ALLTRIM(lccodpost )>>',
					<<ucrsLinFiBi.EPv>>, <<ucrsLinFiBi.etiliquido>>, <<ucrsLinFiBi.ECUSTO>>, <<ucrsLinFiBi.etiliquido>>, '<<uf_gerais_getDate(lcdataobra,"SQL")>>','<<uf_gerais_getDate(lcdataobra,"SQL")>>',
					'<<uf_gerais_getDate(lcdataobra,"SQL")>>','', 1, 100000,
					'<<m_chinis>>', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102),convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8),'<<m_chinis>>',convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
					, convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8), '<<uf_gerais_getDate(getdate(),"SQL")>>',0,<<ch_vendedor>>, '<<ALLTRIM(ch_vendnm)>>'
					,<<ucrsLinFiBi.epv>>,<<ucrsLinFiBi.etiliquido>>, 0

			ENDTEXT
			If !uf_gerais_actGrelha("","",lcSQLBi1)
				uf_perguntalt_chama("OCORREU UMA ANOMALIA AO INSERIR AS LINHAS EM FALTA NO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE. OBRIGADO.","OK","",16)
			Endif
			Select ucrsLinFiBi
			Replace ucrsLinFiBi.act With .T.

		Endif
	Endscan

	If Used("ucrsLinFiBo") And Reccount("ucrsLinFiBo")>0
		Select ucrsLinFiBo
		Go Top
		Scan
			TEXT TO lcSQLBiupd NOSHOW TEXTMERGE

				update bo set etotal=(select sum(ettdeb) from bi where bi.bostamp=bo.bostamp) where bostamp='<<ALLTRIM(ucrsLinFiBo.bostamp)>>'

			ENDTEXT
			If !uf_gerais_actGrelha("","",lcSQLBiupd)
				uf_perguntalt_chama("OCORREU UMA ANOMALIA AO RECALCULAR O TOTAL DO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE. OBRIGADO.","OK","",16)
			Endif

		Endscan
	Endif
Endfunc


Function uf_pagamento_fechalinhasenc

	If Used("ucrsLinFiBo") And Reccount("ucrsLinFiBo")>0
		Select ucrsLinFiBo
		Go Top
		Scan

			Local lcBostampScan
			lcBostampScan = Alltrim(ucrsLinFiBo.bostamp)
			TEXT TO lcSQLdescr NOSHOW TEXTMERGE
				select count(bistamp) as nrreg from bi where bostamp='<<ALLTRIM(lcBostampScan)>>' and qtt>qtt2
			ENDTEXT
			If !uf_gerais_actGrelha("","ucrsBoFecharC",lcSQLdescr)
				uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VALIDAR AS LINHAS DOS DOCUMENTOS A SEREM FECHADOS. POR FAVOR CONTACTE O SUPORTE. OBRIGADO.","OK","",16)
			Endif

			Select ucrsBoFecharC
			If ucrsBoFecharC.nrreg = 0
				TEXT TO lcSQL NOSHOW TEXTMERGE
					Update BO
					Set FECHADA = 1,
						DATAFECHO = dateadd(HOUR, <<difhoraria>>, getdate()),
						usrdata = dateadd(HOUR, <<difhoraria>>, getdate()),
						usrhora = convert(varchar,getdate(),8)
					from bo (nolock)
					inner join bo2 (nolock) on bo2.bo2stamp=bo.bostamp
					Where BOSTAMP = '<<ALLTRIM(lcBostampScan)>>'
					and bo2.nrReceita=''

					UPDATE	BI
					SET	FECHADA = 1,
						usrdata = dateadd(HOUR, <<difhoraria>>, getdate()),
						usrhora = convert(varchar,getdate(),8)
					FROM BI
					WHERE BOSTAMP ='<<ALLTRIM(lcBostampScan)>>'

					UPDATE bo2 SET status='Faturada' WHERE bo2stamp='<<ALLTRIM(lcBostampScan)>>'
				ENDTEXT
				If !uf_gerais_actGrelha("","",lcSQL)
					uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VALIDAR AS LINHAS DOS DOCUMENTOS A SEREM FECHADOS. POR FAVOR CONTACTE O SUPORTE. OBRIGADO.","OK","",16)
				Endif
			Endif

			Local lcStatusEnc, lcmodoe, lcmetodopag, lcmomentopag, lctipor
			Store '' To lcStatusEnc , lcmodoe, lcmetodopag, lcmomentopag, lctipor

			TEXT To lcSql Noshow textmerge
				select modo_envio ,momento_pagamento, pagamento from bo2 (nolock) where bo2stamp='<<ALLTRIM(lcBostampScan)>>'
			ENDTEXT
			If !uf_gerais_actGrelha("", "uCrsPagamEncom",lcSQL)
				uf_perguntalt_chama("OCORREU UM ERRO A VALIDAR ACESSOS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				Return .F.
			Endif
			Select uCrsPagamEncom
			lcmodoenv = Alltrim(uCrsPagamEncom.modo_envio)
			lcmetodopag = Alltrim(uCrsPagamEncom.PAGAMENTO)
			lcmomentopag = Alltrim(uCrsPagamEncom.momento_pagamento)

			Select ucrsLinFiBo
			lctipor = Alltrim(ucrsLinFiBo.tipor)

			fecha("uCrsPagamEncom")

			Do Case
				Case lctipor == 'RES'
					lcStatusEnc  = 'Reservada'
				Case Alltrim(lcmodoenv) == 'storepickup' And Alltrim(lcmomentopag) == 'Acobranca'
					lcStatusEnc  = 'Recolha_Entrega'
				Case Alltrim(lcmodoenv) == 'homedelivery' And Alltrim(lcmomentopag) == 'Acobranca'
					lcStatusEnc  = 'Distribuicao_Entrega'
				Case Alltrim(lcmodoenv) == 'storepickup' And Alltrim(lcmomentopag) == 'Antecipado'
					lcStatusEnc  = 'Recolha_Pago'
				Case Alltrim(lcmodoenv) == 'homedelivery' And Alltrim(lcmomentopag) == 'Antecipado'
					lcStatusEnc  = 'Distribuicao_Pago'
				Case Alltrim(lcmodoenv) == 'storepickup' And Alltrim(lcmomentopag) == 'Credito'
					lcStatusEnc  = 'Recolha_Credito'
				Case Alltrim(lcmodoenv) == 'homedelivery' And Alltrim(lcmomentopag) == 'Credito'
					lcStatusEnc  = 'Distribuicao_Credito'
			Endcase

			TEXT To lcSql Noshow textmerge
				update bo2 SET status='<<ALLTRIM(lcStatusEnc)>>' where bo2stamp='<<ALLTRIM(lcBostampScan)>>'
			ENDTEXT
			If !uf_gerais_actGrelha("", "uCrsPagamEncom",lcSQL)
				uf_perguntalt_chama("OCORREU UM ERRO A VALIDAR ACESSOS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				Return .F.
			Endif

			fecha("ucrsBoFecharC")
			Select ucrsLinFiBo


		Endscan
	Endif

	** fecho de reservas origin�rias em encomendas de cliente do site com receita
	If Used("ucrsLinBoFecho") And Reccount("ucrsLinBoFecho")>0
		Select ucrsLinBoFecho
		Go Top
		Scan

			Local lcBostampScan
			lcBostampScan = Alltrim(ucrsLinBoFecho.bostamp)
			TEXT TO lcSQLdescr NOSHOW TEXTMERGE
				select distinct bo.bostamp from bo (nolock)
				inner join bi (nolock) on bo.bostamp=bi.bostamp
				inner join bo2 (nolock) on bo.bostamp=bo2.bo2stamp
				where
				bo.nmdos='Encomenda de Cliente' and
				bi.ref in (select distinct bii.ref from bi bii (nolock) where bii.bostamp='<<ALLTRIM(lcBostampScan)>>')
				and bo.no = (select boo.no from bo boo (nolock) where boo.bostamp='<<ALLTRIM(lcBostampScan)>>')
				and bo.estab = (select boo.estab from bo boo (nolock) where boo.bostamp='<<ALLTRIM(lcBostampScan)>>')
				and bo2.nrReceita<>''
				and bo2.status='Reservada'
				and bo.fechada=0
			ENDTEXT
			If !uf_gerais_actGrelha("","ucrsBoFecharC",lcSQLdescr)
				uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VALIDAR OS DOCUMENTOS A SEREM FECHADOS. POR FAVOR CONTACTE O SUPORTE. OBRIGADO.","OK","",16)
			Endif


			Select ucrsBoFecharC
			If !Empty(ucrsBoFecharC.bostamp)

				TEXT TO lcSQL NOSHOW TEXTMERGE
					Update BO
					Set FECHADA = 1,
						DATAFECHO = dateadd(HOUR, <<difhoraria>>, getdate()),
						usrdata = dateadd(HOUR, <<difhoraria>>, getdate()),
						usrhora = convert(varchar,getdate(),8)
					from bo (nolock)
					inner join bo2 (nolock) on bo2.bo2stamp=bo.bostamp
					Where BOSTAMP = '<<ALLTRIM(ucrsBoFecharC.bostamp)>>'

					UPDATE	BI
					SET	FECHADA = 1,
						usrdata = dateadd(HOUR, <<difhoraria>>, getdate()),
						usrhora = convert(varchar,getdate(),8)
					FROM BI
					WHERE BOSTAMP ='<<ALLTRIM(ucrsBoFecharC.bostamp)>>'

					UPDATE bo2 SET status='Faturada' WHERE bo2stamp='<<ALLTRIM(ucrsBoFecharC.bostamp)>>'
				ENDTEXT
				If !uf_gerais_actGrelha("","",lcSQL)
					uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VALIDAR AS LINHAS DOS DOCUMENTOS A SEREM FECHADOS. POR FAVOR CONTACTE O SUPORTE. OBRIGADO.","OK","",16)
				Endif

				Local lcBostampScan
				lcBostampScan = Alltrim(ucrsBoFecharC.bostamp)
				fecha("ucrsBoFecharC")

				****
				Local lcStatusEnc, lcmodoe, lcmetodopag, lcmomentopag, lctipor
				Store '' To lcStatusEnc , lcmodoe, lcmetodopag, lcmomentopag, lctipor

				TEXT To lcSql Noshow textmerge
					select modo_envio ,momento_pagamento, pagamento from bo2 (nolock) where bo2stamp='<<ALLTRIM(lcBostampScan)>>'
				ENDTEXT
				If !uf_gerais_actGrelha("", "uCrsPagamEncom",lcSQL)
					uf_perguntalt_chama("OCORREU UM ERRO A VALIDAR ACESSOS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
					Return .F.
				Endif
				Select uCrsPagamEncom
				lcmodoenv = Alltrim(uCrsPagamEncom.modo_envio)
				lcmetodopag = Alltrim(uCrsPagamEncom.PAGAMENTO)
				lcmomentopag = Alltrim(uCrsPagamEncom.momento_pagamento)

				**SELECT ucrsLinFiBo
				**lctipor = ALLTRIM(ucrsLinFiBo.tipor)

				fecha("uCrsPagamEncom")

				Do Case
						**CASE lctipor == 'RES'
						**	lcStatusEnc  = 'Reservada'
					Case Alltrim(lcmodoenv) == 'storepickup' And Alltrim(lcmomentopag) == 'Acobranca'
						lcStatusEnc  = 'Recolha_Entrega'
					Case Alltrim(lcmodoenv) == 'homedelivery' And Alltrim(lcmomentopag) == 'Acobranca'
						lcStatusEnc  = 'Distribuicao_Entrega'
					Case Alltrim(lcmodoenv) == 'storepickup' And Alltrim(lcmomentopag) == 'Antecipado'
						lcStatusEnc  = 'Recolha_Pago'
					Case Alltrim(lcmodoenv) == 'homedelivery' And Alltrim(lcmomentopag) == 'Antecipado'
						lcStatusEnc  = 'Distribuicao_Pago'
					Case Alltrim(lcmodoenv) == 'storepickup' And Alltrim(lcmomentopag) == 'Credito'
						lcStatusEnc  = 'Recolha_Credito'
					Case Alltrim(lcmodoenv) == 'homedelivery' And Alltrim(lcmomentopag) == 'Credito'
						lcStatusEnc  = 'Distribuicao_Credito'
					Otherwise
						lcStatusEnc  = 'Faturada'
				Endcase

				TEXT To lcSql Noshow textmerge
					update bo2 SET status='<<ALLTRIM(lcStatusEnc)>>' where bo2stamp='<<ALLTRIM(lcBostampScan)>>'
				ENDTEXT
				If !uf_gerais_actGrelha("", "uCrsPagamEncom",lcSQL)
					uf_perguntalt_chama("OCORREU UM ERRO A VALIDAR ACESSOS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
					Return .F.
				Endif

				****

				Select ucrsLinFiBo

			Endif
		Endscan
	Endif

Endfunc


Function uf_pagamento_existedadospsico
	Lparameters lcFtstamp
	Local lcexiste
	Store .F. To lcexiste

	TEXT TO lcSQL TEXTMERGE NOSHOW
		select * from B_dadosPsico (nolock) where ftstamp='<<ALLTRIM(lcftstamp)>>'
	ENDTEXT
	uf_gerais_actGrelha("", "uCrsexistePsi", lcSQL)
	Select uCrsexistePsi
	If Reccount("uCrsexistePsi")>0
		lcexiste = .T.
	Endif
	fecha("uCrsexistePsi")

	Return lcexiste

Endfunc

Procedure uf_pagamento_checkModPagDev

	If !Used("ucrsVale")
		Return .F.
	Endif

	If PAGAMENTO.txtValAtend.Value > 0
		Return .F.
	Endif

	Select ucrsvale
	Go Top

	uv_hasDev = .F.

	Scan For ucrsvale.dev = 1

		TEXT TO msel TEXTMERGE NOSHOW

            SELECT
                (CASE
                    when ft2.evdinheiro <> 0
                        then 'dinheiro'
                    when ft.echtotal <> 0
                        then 'cheque'
                    when ft2.epaga2 <> 0
                        then 'mb'
                    when ft2.epaga1 <> 0
                        then 'visa'
                    when ft2.epaga3 <> 0
                        then 'modpag3'
                    when ft2.epaga4 <> 0
                        then 'modpag4'
                    when ft2.epaga5 <> 0
                        then 'modpag5'
                    when ft2.epaga6 <> 0
                        then 'modpag6'
                    else
                        ''
                END) as modPag
            FROM
                ft(nolock)
                join ft2(NOLOCK) on ft.ftstamp = ft2.ft2stamp
            WHERE
                ftstamp = '<<ALLTRIM(uCrsVale.ftstamp)>>'

		ENDTEXT

		If !uf_gerais_actGrelha("", "uc_modPag", msel)

			uf_perguntalt_chama("Erro a verificar m�todo de pagamento original.","OK","",64)
			Return .F.

		Endif

		If Reccount("uc_modPag") = 0
			Loop
		Endif

		PAGAMENTO.txtDinheiro.Value = ''
		PAGAMENTO.txtMB.Value = ''
		PAGAMENTO.txtVisa.Value = ''
		PAGAMENTO.txtCheque.Value = ''
		PAGAMENTO.txtModPag3.Value = ''
		PAGAMENTO.txtModPag4.Value = ''
		PAGAMENTO.txtModPag5.Value = ''
		PAGAMENTO.txtModPag6.Value = ''

		uv_modPag = Alltrim(uc_modPag.modPag)

		Do Case
			Case uv_modPag=="dinheiro"
				PAGAMENTO.txtDinheiro.Value = astr(PAGAMENTO.txtValAtend.Value,15,2)
				uv_hasDev = .T.
			Case uv_modPag=="mb"
				PAGAMENTO.txtMB.Value = astr(PAGAMENTO.txtValAtend.Value,15,2)
				uv_hasDev = .T.
			Case uv_modPag=="visa"
				PAGAMENTO.txtVisa.Value = astr(PAGAMENTO.txtValAtend.Value,15,2)
				uv_hasDev = .T.
			Case uv_modPag=="cheque"
				PAGAMENTO.txtCheque.Value = astr(PAGAMENTO.txtValAtend.Value,15,2)
				uv_hasDev = .T.
			Case uv_modPag=="modpag3"
				PAGAMENTO.txtModPag3.Value = astr(PAGAMENTO.txtValAtend.Value,15,2)
				uv_hasDev = .T.
			Case uv_modPag=="modpag4"
				PAGAMENTO.txtModPag4.Value = astr(PAGAMENTO.txtValAtend.Value,15,2)
				uv_hasDev = .T.
			Case uv_modPag=="modpag5"
				PAGAMENTO.txtModPag5.Value = astr(PAGAMENTO.txtValAtend.Value,15,2)
				uv_hasDev = .T.
			Case uv_modPag=="modpag6"
				If uf_gerais_getParameter('ADM0000000294', 'BOOL') = .T.
					PAGAMENTO.txtModPag6.Value = astr(PAGAMENTO.txtValAtend.Value,15,2)
					uv_hasDev = .T.
				Else
					Public ValUtPLF
					Store 0 To ValUtPLF
					PLafondDispUt = 0
					Select uCrsatendCl
					PLafondDispUt= uCrsatendCl.eplafond - uCrsatendCl.esaldo
					If PLafondDispUt>0 Then

						uf_PAGAMENTO_resetValoresModoPag()
						uf_PAGAMENTO_actValoresPg()

						If PLafondDispUt > PAGAMENTO.txtValAtend.Value
							PAGAMENTO.txtModPag6.Value = astr(PAGAMENTO.txtValAtend.Value,15,2)
							uv_hasDev = .T.
						Else
							PAGAMENTO.txtModPag6.Value = astr(PLafondDispUt,15,2)
							PrecisaRecibo = .T.
							uv_hasDev = .T.
						Endif

						If Val(PAGAMENTO.txtModPag6.Value) > 0
							PLafondDispUt = Val(PAGAMENTO.txtModPag6.Value)
							UtilPlafond = .T.
						Else
							UtilPlafond = .F.
						Endif
						uf_PAGAMENTO_actValoresPg()

					Endif
				Endif
		Endcase
		If 	uv_hasDev

			PAGAMENTO.txtDinheiro.ReadOnly = .T.
			PAGAMENTO.txtMB.ReadOnly = .T.
			PAGAMENTO.txtVisa.ReadOnly = .T.
			PAGAMENTO.txtCheque.ReadOnly = .T.
			PAGAMENTO.txtModPag3.ReadOnly = .T.
			PAGAMENTO.txtModPag4.ReadOnly = .T.
			PAGAMENTO.txtModPag5.ReadOnly = .T.
			PAGAMENTO.txtModPag6.ReadOnly = .T.

		Endif

		Exit

		Select ucrsvale
	Endscan


	**PAGAMENTO.refresh()

	fecha("uc_modPag")

Endproc

Function uf_PAGAMENTO_grava_controlados
	Local lcContemControlados, lcValidou
	Store .F. To lcContemControlados
	Store .T. To lcValidou
	**uf_perguntalt_chama("verifica.", "", "OK", 16)
	Select ucrsatendst
	Go Top
	Scan
		If Alltrim(ucrsatendst.usr3)='04'
			lcContemControlados=.T.
		Endif
	Endscan
	If lcContemControlados = .T.
		Local lcrefcontr
		Store '' To lcrefcontr
		Select ucrsatendst
		Go Top
		Scan
			If Alltrim(ucrsatendst.usr3)='04'
				lcrefcontr = Alltrim(ucrsatendst.ref)
				Select fi
				Go Top
				Scan
					If Alltrim(fi.ref)==Alltrim(lcrefcontr)
						Local lcfistampcontr
						lcfistampcontr = Alltrim(fi.fistamp)
						Select uCrsAtendComp
						Go Top
						TEXT TO lcSQL NOSHOW TEXTMERGE
							insert into medcontrolados
								(fistamp
								,datareceita
								,dataDispensa
								,plano
								,nrelegibilidade
								,nomemedico
								,contactomedico
								,especialidademedico
								,localprescricao
								,nomeutente
								,contactoutente
								,nCartao)
							values
								('<<ALLTRIM(lcfistampcontr)>>'
								,'<<uf_gerais_getDate(uCrsAtendComp.datareceita,"SQL")>>'
								,'<<uf_gerais_getDate(uCrsAtendComp.dataDispensa,"SQL")>>'
								,'<<ALLTRIM(uCrsAtendComp.design)>>'
								,''
								,'<<uCrsAtendComp.nomemedico>>'
								,'<<uCrsAtendComp.contactomedico>>'
								,'<<uCrsAtendComp.especialidademedico>>'
								,'<<uCrsAtendComp.localprescricao>>'
								,'<<uCrsAtendComp.nomeutente>>'
								,'<<uCrsAtendComp.contactoutente>>'
								,'<<uCrsAtendComp.nCartao>>')
						ENDTEXT

						uf_gerais_actGrelha("","",lcSQL)

						Select fi
					Endif
				Endscan
			Endif
		Endscan
	Endif
Endfunc


Function uf_pagamento_validaSeEfectivado

	Lparameters lcToken

	TEXT TO lcsql TEXTMERGE NOSHOW
		exec up_gerais_verif_token_compart '<<ALLTRIM(lcToken)>>'
	ENDTEXT
	uf_gerais_actGrelha("", "ucrsjacompart", lcSQL)


	If Reccount("ucrsjacompart")=0
		fecha("ucrsjacompart")
		Return .F.
	Else
		fecha("ucrsjacompart")
		Return .T.
	Endif

Endfunc

Function uf_pagamento_permsDev
	Lparameters uv_metodo

	If !uf_gerais_getParameter_site('ADM0000000162', 'BOOL', mySite)
		Return .F.
	Endif

	uv_hasPerm = .F.

	uv_perms = uf_gerais_getParameter_site('ADM0000000162', 'TEXT', mySite)

	For i=1 To Alines(ua_perms, uv_perms, ";")

		If Upper(Alltrim(uv_metodo)) == Upper(Alltrim(ua_perms(i)))
			uv_hasPerm = .T.
		Endif

	Next

	Return uv_hasPerm

Endfunc

Function uf_pagamento_getTsre
	Lparameters lcNdoc

	Local lcSQL, lcTipoTsre
	lcTipoTsre = 0
	fecha("ucrsFaturacaoGetTsre")

	TEXT TO lcsql TEXTMERGE NOSHOW
		exec up_faturacao_getTsre <<lcNdoc>>,'<<ALLTRIM(mysite)>>'
	ENDTEXT
	uf_gerais_actGrelha("", "ucrsFaturacaoGetTsre", lcSQL)

	If Used("ucrsFaturacaoGetTsre")
		Select ucrsFaturacaoGetTsre
		Go Top
		lcTipoTsre = ucrsFaturacaoGetTsre.u_tipodoc
		fecha("ucrsFaturacaoGetTsre")
	Endif

	Return lcTipoTsre

Endfunc

Function uf_pagamento_modoPagExterno
	Lparameters uv_modoPag

	If !Used("uCrsCLEF") Or Empty(uv_modoPag)
		Return .F.
	Endif
	
	
	
	***TODO: Mudar Valida��o para Devolu��es por pagamento externo****
	If PAGAMENTO.txtValAtend.Value < 0
		uf_perguntalt_chama("N�o pode utilizar este modo de pagamento para devolu��es.","OK","",16)
		Return .F.
	Endif

	If !uf_pagExterno_chama(uv_modoPag, uCrsCLEF.no, uCrsCLEF.estab)
		Return .F.
	Endif

	If !Used("uc_dadosPagExterno" + Alltrim(uv_modoPag))
		Return .F.
	Endif

	Return .T.

Endfunc


Function uf_pagamento_reqPagExterno

	If !Used("uc_pagsExterno")
		Return .F.
	Endif
	
	Local uv_userName, uv_userPass, uv_entityType, uv_teste, uv_token, uv_sql

	uv_userName = uf_gerais_getParameter_site("ADM0000000187", "TEXT", mySite)
	uv_userPass = uf_gerais_getParameter_site("ADM0000000188", "TEXT", mySite)
	uv_entityType = uf_gerais_getParameter_site("ADM0000000189", "TEXT", mySite)
	
	uf_gerais_meiaRegua("A efectuar o pedido de pagamento...")


	If !uf_gerais_getParameter("ADM0000000227", "BOOL")
		uv_teste = 0
	Else
		uv_teste = 1
	Endif

	Select uc_pagsExterno
	Go Top

	Scan

		uv_token = uf_gerais_stamp()

		TEXT TO uv_sql TEXTMERGE NOSHOW

         EXEC up_vendas_requestPagExterno
            '<<ALLTRIM(uv_token)>>',
            '<<ALLTRIM(uv_userName)>>',
            '<<ALLTRIM(uv_userPass)>>',
            1,
            <<ASTR(uc_pagsExterno.tipoPagExt)>>,
            '<<ALLTRIM(uc_pagsExterno.tipoPagExtDesc)>>',
            '<<ALLTRIM(uc_pagsExterno.receiverID)>>',
            '<<ALLTRIM(uc_pagsExterno.receiverName)>>',
            '<<ALLTRIM(uv_entityType)>>',
            <<ASTR(uc_pagsExterno.valor,14,2)>>,
            '<<ALLTRIM(uc_pagsExterno.email)>>',
            '<<ALLTRIM(uc_pagsExterno.tlmvl)>>',
			'<<ALLTRIM(uc_pagsExterno.descricao)>>',
            '<<ALLTRIM(mySite)>>',
            <<ASTR(uv_teste)>>,
            <<ASTR(VAL(uc_pagsExterno.duracao))>>,
            '<<ALLTRIM(m_chinis)>>',
            '',
            '<<ALLTRIM(uc_pagsExterno.nrAtend)>>'
            

		ENDTEXT

		If !uf_gerais_actGrelha("","",uv_sql)
			regua(2)
			Return .F.
		Endif

		Local lcNomeJar, LcWsPath, lcWsDir, lcAdd

		lcNomeJar = "ltsPaymentMethods.jar"
		LcWsPath  = Alltrim(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\ltsPaymentMethods\'+Alltrim(lcNomeJar)+' ' +' --TOKEN='+Alltrim(uv_token) +' --IDCL='+Alltrim(ucrse1.id_lt)
		lcWsDir	  =  Alltrim(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\ltsPaymentMethods\'
		lcAdd  	  = "&"

		LcWsPath  = "cmd /c cd /d "+lcWsDir+" "+lcAdd+lcAdd+" javaw -jar " +LcWsPath
		owsshell  = Createobject("WScript.Shell")
		owsshell.Run(LcWsPath, 0, .T.)

		If uv_teste = 1
			_Cliptext = LcWsPath
		Endif

		If !uf_gerais_actGrelha("","uc_pagExternoResp", "SELECT * FROM paymentMethodResponse(nolock) where token = '" + Alltrim(uv_token) + "'")
			regua(2)
			Return .F.
		Endif

		Select uc_pagExternoResp

		If uc_pagExternoResp.statusID <> 200 and uc_pagExternoResp.statusID <> 201

			uf_perguntalt_chama(Alltrim(Left(uc_pagExternoResp.statusDesc,254)), "OK", "", 16)
			regua(2)
			Return .F.

		Endif

		If Empty(uc_pagExternoResp.operationID)
			regua(2)
			Return .F.
		Endif

		TEXT TO uv_sql TEXTMERGE NOSHOW

         INSERT INTO b_pagCentral_ext (token,nrAtend,operationId,typePayment,typePaymentDesc,status,statusDesc,
                           statePaymentID,statePaymentIDDesc,receiverid,receivername,site,ousrinis,ousrdata,usrinis,usrdata, limitDate)
                  VALUES (
                           '<<ALLTRIM(uv_token)>>',
                           '<<ALLTRIM(uc_pagsExterno.nrAtend)>>',
                           '<<ALLTRIM(uc_pagExternoResp.operationId)>>',
                           <<ASTR(uc_pagsExterno.tipoPagExt)>>,
                           '<<ALLTRIM(uc_pagsExterno.tipoPagExtDesc)>>',
                           '',
                           '',
                           0,
                           '',
                           '<<ALLTRIM(uc_pagsExterno.receiverID)>>',
                           '<<ALLTRIM(uc_pagsExterno.receiverName)>>',
                           '<<ALLTRIM(mySite)>>',
                           '<<ALLTRIM(m_chinis)>>',
                           GETDATE(),
                           '<<ALLTRIM(m_chinis)>>',
                           GETDATE(),
						   '<<IIF(VAL(uc_pagsExterno.duracao) = 0, "19000101", uf_gerais_getDate((DATE() + VAL(uc_pagsExterno.duracao)+ 1),"SQL")))>>'
                     )

		ENDTEXT

		If !uf_gerais_actGrelha("","",uv_sql)
			regua(2)
			Return .F.
		Endif

		Select uc_pagsExterno
	Endscan
	Return .T.
Endfunc
**

Function uf_PAGAMENTO_imprimirA4
	If ATENDIMENTO.impA4 == .F.
		PAGAMENTO.menu1.impA4.config("Imp. A4", myPath + "\imagens\icons\checked_w.png"		, "uf_PAGAMENTO_imprimirA4","F4")
		ATENDIMENTO.impA4 = .T.
	Else
		PAGAMENTO.menu1.impA4.config("Imp. A4", myPath + "\imagens\icons\unchecked_w.png"	, "uf_PAGAMENTO_imprimirA4","F4")
		ATENDIMENTO.impA4= .F.
	Endif
	PAGAMENTO.txtDinheiro.SetFocus()
Endfunc
