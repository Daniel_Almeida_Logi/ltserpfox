** Carrega APPs



** Valida estado do cashdro
FUNCTION uf_cashdro_operation
	LPARAMETERS lcResponseArray, lcWaitingMessage, lcMessageType, lcAmount, lcObs, lcMsgId
	

	LOCAL lcWsPath,lcWsParams,lcWsCmd,lcToken
	STORE '' TO lcWsPath, lcWsParams, lcToken, lcWsCmd
	STORE '&' TO lcAdd
	&& validate if terminal has cashDro
	IF EMPTY(ALLTRIM(myCashDroStamp))         
		RETURN .f.	
	ENDIF
	

	&&maximo a pagar
	
	IF(!EMPTY(lcAmount))
		IF(lcAmount>1000000)
			uf_perguntalt_chama("Valor m�ximo ultrapassado.","OK","",16)
			RETURN .f.	
		ENDIF
	ENDIF
	
	IF (ALLTRIM(lcMessageType)=="app.msg.menu.operation_sale_request")
		IF(EMPTY(lcAmount))
			uf_perguntalt_chama("Tem de definir um valor a pagar","OK","",16)
			RETURN .f.	
		ENDIF	
	ENDIF

	
	
	regua(0,4,lcWaitingMessage)
	
	lcNomeJar = 'CashDroCli.jar'	
	lcWsPath = ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\ltsCashDro\' + ALLTRIM(lcNomeJar)
	lcWsDir	= ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\ltsCashDro'
	&& validate path to file
	IF !FILE(lcWsPath)
		uf_perguntalt_chama("FICHEIRO DE CONFIGURA��O N�O ENCONTRADO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		regua(2)
		RETURN .f.
	ENDIF 
	

	&&construct param to java
	lcToken = uf_gerais_stamp()
	
	
	regua(1,3,lcWaitingMessage)
	 &&' "--AMOUNT=' 						+  ALLTRIM(PADL(lcAmount,20))	 + ["] +; 	
	
	lcWsParams = ' "--TOKEN=' 						+  ALLTRIM(lcToken)  	  	 	 + ["] +;
				 ' "--MSG_TYPE=' 					+  ALLTRIM(lcMessageType) 	 	 + ["] +; 
				 ' "--OBS=' 						+  ALLTRIM(lcObs) 	  	 	 	 + ["] +;
				 ' "--AMOUNT=' 						+  ALLTRIM(PADL(lcAmount*100,20))+ ["] +; 	
				 ' "--TERMINAL=' 					+  ALLTRIM(STR(myTermNo))	 	 + ["] +;
				 ' "--TERMINAL_NAME='				+  ALLTRIM(myTerm)				 + ["] +;
				 ' "--IDCL=' 						+  ALLTRIM(uCrsE1.id_lt)   	 	 + ["] +; 			
				 ' "--SELLER=' 						+  ALLTRIM(m_chinis) 	  	 	 + ["] +; 
				 ' "--STAMP=' 						+  ALLTRIM(myCashDroStamp)  	 + ["] +; 
				 ' "--MSG_ID=' 						+  ALLTRIM(lcMsgId)  	 		 + ["] +; 
				 ' "--WAINTING_MESSAGE=' 			+  ALLTRIM(lcWaitingMessage)   	 + ["] 
				 
	
	regua(2)
	
	
	
	
	&&execute
    lcWsCmd = "cmd /c cd /d " + lcWsDir+ " "+  lcAdd + lcAdd + " javaw -Dfile.encoding=UTF-8  -jar " + lcWsPath + " " + lcWsParams 
    

	
	oWSShell = CREATEOBJECT("WScript.Shell")
	oWSShell.Run(lcWsCmd, 0, .t.)
	
	
	
	&&regua(1,4,lcWaitingMessage)

	
	&& resultado to pedido
	TEXT TO lcSQL NOSHOW TEXTMERGE
		EXEC [dbo].[up_cashdro_resultadoOp]  '<<ALLTRIM(lcToken)>>'
	ENDTEXT 
	IF !uf_gerais_actgrelha("", "uCrsResOp", lcSQL)
		uf_perguntalt_chama("N�o foi poss�vel comunicar com a base de dados. Por favor contacte o suporte." ,"OK","",32)
		regua(2)
		fecha("uCrsResOp")
		RETURN .f.		
	ENDIF
	
	&&regua(1,4,lcWaitingMessage)

	
	uf_cashdro_processResponse(lcMessageType,@lcResponseArray)


	fecha("uCrsResOp")

	&&regua(2)
	
	RETURN lcResponseArray
	

ENDFUNC

FUNCTION  uf_cashdro_messagemResposta
	LPARAMETERS lcMessage
	
	
	IF(EMPTY(lcMessage))
		RETURN "N�o foi poss�vel realizar a opera��o no CashDro."
	ELSE
		RETURN  ALLTRIM(lcMessage)
	ENDIF
	

ENDFUNC



FUNCTION uf_cashdro_processResponse
	LPARAMETERS lcMessageType, lcResponseArray
	
*!*	  	lcResponseArray(1) - codigo da resposta do cashdro
*!*	  	lcResponseArray(2) - mensagem enviada pelo cashdro
*!*	  	lcResponseArray(3) - Estado no caso da mensagem de status
*!*	  	lcResponseArray(4) - Ultimo request cashDro
*!*	  	lcResponseArray(5) - Msg_ID
*!*	  	lcResponseArray(6) - Stauts Code Request 

	
	STORE uCrsResOp.cashdro_code TO lcResponseArray(1)
  	STORE ALLTRIM(uCrsResOp.msg)  TO lcResponseArray(2)
  	STORE ALLTRIM(uCrsResOp.cashDroFinalStatus)  TO lcResponseArray(3)  
  	STORE ALLTRIM(uCrsResOp.request)  TO lcResponseArray(4)  
  	STORE ALLTRIM(uCrsResOp.cashdro_msg_id)  TO lcResponseArray(5) 
  	STORE uCrsResOp.code  TO lcResponseArray(6) 
  	
  	  	
  	IF(EMPTY(lcResponseArray(1)))
  		lcResponseArray(1)="-1"
  	ENDIF
  	
  	IF(EMPTY(lcResponseArray(2)) )
  		lcResponseArray(2)=""
  	ENDIF
  	
  	IF(EMPTY(lcResponseArray(3)))
  		lcResponseArray(3)=""
  	ENDIF
  	
  	IF(EMPTY(lcResponseArray(4)))
  		lcResponseArray(4)=""
  	ENDIF
  	
  	IF(EMPTY(lcResponseArray(5)))
  		lcResponseArray(5)=""
  	ENDIF
  	
  	IF(EMPTY(lcResponseArray(6)))
  		lcResponseArray(6)=0
  	ENDIF
  	
  		

ENDFUNC




