DO pesqcampanhas

**
FUNCTION uf_campanhas_chama
	LPARAMETERS lcId
	IF EMPTY(lcId)
		lcID = 0
	ENDIF 
	
	** Controle de Permiss�es	
	IF !uf_gerais_validaPermUser(ch_userno,ch_grupo,'Campanhas - Visualizar')
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ACEDER AO PAINEL CAMPANHAS.","OK","",48)
		RETURN .f.
	ENDIF
	
	PUBLIC myCampanhasIntroducao, myCampanhasAlteracao
	myCampanhasIntroducao = .f.
	myCampanhasAlteracao = .f.
	
	
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		SELECT * FROM campanhas WHERE id = <<lcID>>
	ENDTEXT  

	If !uf_gerais_actGrelha("", "ucrsCampanhas", lcSql)
		uf_perguntalt_chama("N�o foi possivel carregar informa��o da campanha.","OK","",32)
		RETURN .f.
	ENDIF 

	IF TYPE("CAMPANHAS")=="U"
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			SELECT case when excecao=1 then 'EXCECAO' else 'REGRA' END as tipoExcecao, * FROM campanhas_ut WHERE campanhas_id = <<lcID>>
		ENDTEXT  
		If !uf_gerais_actGrelha("", "ucrsCampanhasUt", lcSql)
			uf_perguntalt_chama("N�o foi possivel carregar informa��o da campanha. (Regras Utentes)","OK","",32)
			RETURN .f.
		ENDIF 

		TEXT TO lcSQL NOSHOW TEXTMERGE 
			SELECT case when excecao=1 then 'EXCECAO' else 'REGRA' END as tipoExcecao, * FROM campanhas_st WHERE campanhas_id = <<lcID>>
		ENDTEXT  
		IF !uf_gerais_actGrelha("", "ucrsCampanhasSt", lcSql)
			uf_perguntalt_chama("N�o foi possivel carregar informa��o da campanha. (Regras Stocks)","OK","",32)
			RETURN .f.
		ENDIF 
	
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			Select * from campanhas_pNiveis WHERE campanhas_id = <<lcID>>
		ENDTEXT  
		IF !uf_gerais_actGrelha("", "ucrsRebatimentosNiveisPontos", lcSql)
			uf_perguntalt_chama("N�o foi possivel carregar informa��o da campanha. (Niveis Pontos)","OK","",32)
			RETURN .f.
		ENDIF 
		
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			SELECT case when excecao=1 then 'EXCECAO' else 'REGRA' END as tipoExcecao, * FROM campanhas_pOfertas WHERE campanhas_id = <<lcID>>
		ENDTEXT  
		IF !uf_gerais_actGrelha("", "ucrsCampanhasPOfertas", lcSql)
			uf_perguntalt_chama("N�o foi poss�vel carregar informa��o da campanha. (Produtos Ofertas)","OK","",32)
			RETURN .f.
		ENDIF 
	
		**
		DO FORM CAMPANHAS
	ELSE
		
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			SELECT case when excecao=1 then 'EXCECAO' else 'REGRA' END as tipoExcecao, * FROM campanhas_ut WHERE campanhas_id = <<lcID>>
		ENDTEXT  
		If !uf_gerais_actGrelha("CAMPANHAS.GridUt", "ucrsCampanhasUt", lcSql)
			uf_perguntalt_chama("N�o foi possivel carregar informa��o da campanha. (Regras Utentes)","OK","",32)
			RETURN .f.
		ENDIF 

		TEXT TO lcSQL NOSHOW TEXTMERGE 
			SELECT case when excecao=1 then 'EXCECAO' else 'REGRA' END as tipoExcecao, * FROM campanhas_st WHERE campanhas_id = <<lcID>>
		ENDTEXT  
		IF !uf_gerais_actGrelha("CAMPANHAS.GridSt", "ucrsCampanhasSt", lcSql)
			uf_perguntalt_chama("N�o foi possivel carregar informa��o da campanha. (Regras Stocks)","OK","",32)
			RETURN .f.
		ENDIF 
		
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			Select * from campanhas_pNiveis WHERE campanhas_id = <<lcID>>
		ENDTEXT  
		IF !uf_gerais_actGrelha("CAMPANHAS.PageFrame1.Page7.GridNiveis", "ucrsRebatimentosNiveisPontos", lcSql)
			uf_perguntalt_chama("N�o foi possivel carregar informa��o da campanha. (Niveis Pontos)","OK","",32)
			RETURN .f.
		ENDIF 
		
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			SELECT case when excecao=1 then 'EXCECAO' else 'REGRA' END as tipoExcecao, * FROM campanhas_pOfertas WHERE campanhas_id = <<lcID>>
		ENDTEXT  
		IF !uf_gerais_actGrelha("CAMPANHAS.PageFrame1.Page6.GridOfertas", "ucrsCampanhasPOfertas", lcSql)
			uf_perguntalt_chama("N�o foi poss�vel carregar informa��o da campanha. (Produtos Ofertas)","OK","",32)
			RETURN .f.
		ENDIF 
				
		CAMPANHAS.show
	ENDIF 

	FOR i = 1 TO CAMPANHAS.pageFrame1.pageCount
		lcObj = "CAMPANHAS.pageFrame1.page"+ASTR(i)
		&lcObj..refresh
	ENDFOR 
	uf_CAMPANHAS_alternaMenu()
ENDFUNC 



**
FUNCTION uf_CAMPANHAS_CarregaMenu
	
	** Confgura Barra Lateral
	WITH CAMPANHAS.menu1
		.adicionaOpcao("opcoes","Op��es",myPath + "\imagens\icons\opcoes_w.png","","")
		.adicionaOpcao("pesquisar", "Pesquisar", myPath + "\imagens\icons\lupa_w.png", "uf_pesqCampanhas_Chama","")
		.adicionaOpcao("novo","Novo",myPath + "\imagens\icons\mais_w.png","uf_CAMPANHAS_novo","")
		.adicionaOpcao("editar","Editar",myPath + "\imagens\icons\lapis_w.png","uf_CAMPANHAS_editar","")
		&&
		.adicionaOpcao("processar","Processar",myPath + "\imagens\icons\ferramentas_w_lateral.png","uf_campanhas_actualizaCampanhaAtual","")
	ENDWITH 
	
	&& configura menu de op��es
	WITH CAMPANHAS.menu_opcoes	
		.adicionaOpcao("eliminar","Eliminar","","uf_CAMPANHAS_eliminar")
		.adicionaOpcao("gerarcartoes","Gerar Cart�es","","uf_gerais_gera_nrcartao_chkdigit")
	ENDWITH
	
	uf_CAMPANHAS_alternaMenu()

ENDFUNC


**
FUNCTION uf_CAMPANHAS_eliminar
	
	IF !uf_gerais_validaPermUser(ch_userno,ch_grupo,'Campanhas - Eliminar')
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ELIMINAR CAMPANHAS.","OK","",48)
		RETURN .f.
	ENDIF
	
	IF myCampanhasIntroducao == .t. OR myCampanhasAlteracao == .t.
		uf_perguntalt_chama("A Campanha est� em modo de INTRODU��O/EDI��O.","OK","",64)
		RETURN .f.
	ENDIF
	
	If uf_perguntalt_chama("Vai eliminar a Campanha. Pretende continuar?","Sim","N�o")
		SELECT ucrsCampanhas
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			UPDATE campanhas 
			SET 
				id_us_alt = <<ch_userno>>
				,data_alt = dateadd(HOUR, <<difhoraria>>, getdate())
				,eliminada = 1
			WHERE
				campanhas.id = <<ucrsCampanhas.id>>
				
			update campanhas_ut_lista set campanhas = REPLACE(campanhas,'(<<ucrsCampanhas.id>>)','') where campanhas like '%(<<ucrsCampanhas.id>>)%'	
			update campanhas_st_lista set campanhas = REPLACE(campanhas,'(<<ucrsCampanhas.id>>)','') where campanhas like '%(<<ucrsCampanhas.id>>)%'	
			
		ENDTEXT 
		If !uf_gerais_actGrelha("","",lcSQL)
			uf_perguntalt_chama("Ocorreu um erro ao eliminar a Campanha. Por favor contacte o suporte.","OK","",16)
			Return .f.
		ELSE
			uf_perguntalt_chama("Campanha eliminada com sucesso.","OK","",16)
		ENDIF

		IF USED("uCrsPesqCampanhas")
			SELECT uCrsPesqCampanhas
			GO Top
			LOCATE FOR uCrsPesqCampanhas.id == ucrsCampanhas.id
			IF FOUND()
				SELECT uCrsPesqCampanhas
				DELETE 
			ENDIF 
			SELECT uCrsPesqCampanhas
			GO Top
		ENDIF 

		myCampanhasIntroducao = .f.
		myCampanhasAlteracao = .f.
		
		uf_campanhas_actualizaCampanhasAtuais()
		uf_atendimento_CarregaCampanhasUt()
		uf_atendimento_CarregaCampanhasSt()
		
		uf_CAMPANHAS_chama(0)
		
	ENDIF 

ENDFUNC 


**
FUNCTION uf_CAMPANHAS_alternaMenu

	IF myCampanhasIntroducao == .t. OR myCampanhasAlteracao == .t. 
		lcReadOnly = .f.
		CAMPANHAS.menu1.estado("pesquisar,novo,editar,processar", "HIDE", "Gravar", .t., "Cancelar", .T.)
	ELSE
		lcReadOnly = .t.
		CAMPANHAS.menu1.estado("pesquisar,novo,editar,processar", "SHOW", "Gravar", .f., "Sair", .T.)
	ENDIF 
	
	WITH CAMPANHAS
		IF myCampanhasAlteracao == .t. OR myCampanhasIntroducao == .T.
			.descricao.readonly = .f.
		ELSE 
			.descricao.readonly = .t.
		ENDIF  
		.dtinicio.readonly = lcReadOnly 
		.dtFim.readonly = lcReadOnly 
		IF !EMPTY(lcReadOnly)
			.inativo.enable(.f.)
		ELSE
			.inativo.enable(.t.)
		ENDIF 
		IF !EMPTY(lcReadOnly)
			.acumuladesc.enable(.f.)
		ELSE
			.acumuladesc.enable(.t.)
		ENDIF 
		
		FOR i=1 TO .GridSt.columncount
			IF .GridSt.Columns(i).name == "qt"
				.GridSt.Columns(i).readonly = lcReadOnly 
				.GridSt.Columns(i).Text1.readonly = lcReadOnly 
			ELSE
				.GridSt.Columns(i).readonly = .t. 
				.GridSt.Columns(i).Text1.readonly = .t.
			ENDIF 
			
		ENDFOR
	ENDWITH
	
	WITH CAMPANHAS.PageFrame1.Page1
		.pontos.readonly = lcReadOnly 
		.valor.readonly = lcReadOnly 
		.nat.readonly = lcReadOnly 
		.pontosat.readonly = lcReadOnly 
		.nvd.readonly = lcReadOnly 
		.pontosvd.readonly = lcReadOnly 
		.valcartao.readonly = lcReadOnly
		.vallimite.readonly = lcReadOnly
	ENDWITH 
	
	
	
	WITH CAMPANHAS.PageFrame1.Page2
		.desconto.readonly = lcReadOnly 
		.descv.readonly = lcReadOnly 
	ENDWITH 
	
	WITH CAMPANHAS.PageFrame1.Page3
		.valorValeDireto.readonly = lcReadOnly 
		.descValorAt.readonly = lcReadOnly 
		.valorValesDistribuir.readonly = lcReadOnly 
		.mensagem.readonly = lcReadOnly 
		.mensagemValeRem.readonly = lcReadOnly 
		
		IF !EMPTY(lcReadOnly)
			.remanescente.enable(.f.)
		ELSE
			.remanescente.enable(.t.)
		ENDIF 
	ENDWITH 
	
	WITH CAMPANHAS.PageFrame1.Page6
		FOR i=1 TO .GridOfertas.columncount
			IF .GridOfertas.Columns(i).name == "qt"
				.GridOfertas.Columns(i).readonly = lcReadOnly 
				.GridOfertas.Columns(i).Text1.readonly = lcReadOnly 
			ELSE
				.GridOfertas.Columns(i).readonly = .t. 
				.GridOfertas.Columns(i).Text1.readonly = .t.
			ENDIF 
		ENDFOR
	ENDWITH 
	
	WITH CAMPANHAS.PageFrame1.Page7
		FOR i=1 TO .GridNiveis.columncount
			IF .GridNiveis.Columns(i).name == "pontos" OR .GridNiveis.Columns(i).name == "valor"
				.GridNiveis.Columns(i).readonly = lcReadOnly 
				.GridNiveis.Columns(i).Text1.readonly = lcReadOnly 
			ENDIF 
		ENDFOR
	ENDWITH 
	
	WITH CAMPANHAS.PageFrame1.Page8
		.mensagem.readonly = lcReadOnly 
	ENDWITH 
	
	WITH CAMPANHAS.PageFrame1.Page9
		.obs.readonly = lcReadOnly 
	ENDWITH 
	
	CAMPANHAS.refresh
ENDFUNC


**
FUNCTION uf_Campanhas_NovaPesqUt
	IF TYPE("PESQUTENTES") != "U"
		uf_PESQUTENTES_sair()
	ENDIF 
	
	uf_pesqutentes_chama('CAMPANHAS')

ENDFUNC 


**
FUNCTION uf_CAMPANHAS_selPesquisaUt
	
	LOCAL lcSQL, lcTop, lcNome, lcNo, lcEstab, lcMorada, lcNcont, lcNCartao, lcTipo, lcProfissao, lcSexo, lcNascimento
	LOCAL lcTlmvl, lcOperador, lcIdade, lcObs, lcMail, lcEFR, lcPatologia, lcEntre, lcE, lcRef
	LOCAL lcInactivo, lctlf, lcutenteno, lcnbenef, lcValTouch, lcValEntidadeClinica,lcID
	
	** var defaults
	STORE '' TO lcSQL, lcNome, lcMorada, lcNcont, lcNCartao, lcTipo, lcProfissao, lcSexo
	STORE '' TO lcObs, lcEFR, lcPatologia, lcRef, lctlf, lcutenteno, lcnbenef,lcID
	STORE 0 TO lcNo, lcTlmvl, lcMail, lcInactivo, lcValTouch, lcValEntidadeClinica
	STORE '19000101' TO lcNascimento, lcEntre

	lcEstab 		= "-1"
	lcOperador 		= "="
	lcIdade			= "-9999"
	lcE				= '30000101'
	
	** validar se front-office ou back-oficce
	LOCAL lcObj
	
	lcObj = "PESQUTENTES.pageframe1.page1"
	lcId = ALLTRIM(&lcObj..id.value)
		
	** Atribui parametros de pesquisa
	
	** campos dispon�veis em front-office e back-office
	lcTop 		= IIF(INT(VAL(&lcObj..topo.value))== 0,100000,INT(VAL(&lcObj..topo.value)))
	lcNome		= ALLTRIM(&lcObj..nome.value)
	lcNo		= IIF(EMPTY(&lcObj..numero.value), 0, STRTRAN(&lcObj..numero.value,' ',''))
	lcEstab		= IIF(EMPTY(&lcObj..estab.value), "-1", &lcObj..estab.value)
	lcMorada	= ALLTRIM(&lcObj..morada.value)
	lcNcont		= ALLTRIM(&lcObj..ncont.value)
	lcNCartao	= ALLTRIM(&lcObj..nCartao.value)
	lctlf		= ALLTRIM(&lcObj..tlf.value)
	lcutenteno	= ALLTRIM(&lcObj..utenteno.value)
	lcnbenef	= ALLTRIM(&lcObj..nbenef.value)
	lcTipo		= ALLTRIM(&lcObj..cmbTipo.value)
	lcProfissao	= ALLTRIM(&lcObj..profissao.value)
	lcSexo		= IIF(&lcObj..sexo.value == 'M/F', '', &lcObj..sexo.value)
	lcOperador	= ALLTRIM(&lcObj..OperadorLogico.value)
	lcInactivo	= IIF(&lcObj..image4.tag == "true", 1, 0)
	lcID		= ALLTRIM(&lcObj..id.value)
				

	IF &lcObj..label6.caption == 'Idade'
		IF EMPTY(&lcObj..idade.value)
			lcIdade	= "-9999"
			lcOperador = '>'
		ELSE
			lcIdade	= &lcObj..idade.value
			lcNascimento = '19000101'
		ENDIF
	ENDIF

	IF &lcObj..label6.caption == 'Dt. Nasc.'
		lcNascimento = IIF(EMPTY(ALLTRIM(&lcObj..txtnascimento.value)), '19000101', uf_gerais_getDate(&lcObj..txtnascimento.value, "SQL"))
		lcNascimentoReport = IIF(EMPTY(ALLTRIM(&lcObj..txtnascimento.value)), '1900.01.01', uf_gerais_getDate(&lcObj..txtnascimento.value))
		lcIdade	= "-9999"
		lcOperador = '>'
	ELSE
		lcNascimento = '19000101'
		lcNascimentoReport = '1900.01.01'
	ENDIF 
	lcNascimento2 = CTOD(lcNascimentoReport)

	lcMail	 	= IIF(&lcObj..image2.tag == "true", 1, 0)
	lcTlmvl 	= IIF(&lcObj..image3.tag == "true", 1, 0)
	lcObs		= ALLTRIM(&lcObj..observacoes.value)
	lcEFR		= ALLTRIM(&lcObj..efr.value)
		
	** patologias
	lcPatologia	= ALLTRIM(&lcObj..patologias.value)

	** produto
	IF used("uCrsProdutosCLI")
		SELECT uCrsProdutosCLI
		GO TOP
		SCAN
			lcRef = lcRef + astr(uCrsProdutosCLI.ref) + '|'
		ENDSCAN
	ENDIF 
		
	IF !EMPTY(lcRef)
		&& retirar pipe no final
		lcRef=substr(lcRef, 1, len(lcRef)-1)
	ENDIF 

	** Periodo de tempo
	lcEntre		= IIF(EMPTY(ALLTRIM(&lcObj..txtEntre.value)), '19000101', uf_gerais_getDate(&lcObj..txtEntre.value, "SQL"))
	lcE			= IIF(EMPTY(ALLTRIM(&lcObj..txtE.value)), '30000101', uf_gerais_getDate(&lcObj..txtE.value, "SQL"))
	
	
	lcEntre2	= CTOD(ALLTRIM(&lcObj..txtEntre.value))
	lcE2		= CTOD(ALLTRIM(&lcObj..txtE.value))
	

	lcRegraTxt = ""
	IF !EMPTY(lcNome)
		lcRegraTxt = lcRegraTxt + " Nome: " + ALLTRIM(lcNome) + ";"
	ENDIF 
	IF !EMPTY(lcNo)
		lcRegraTxt = lcRegraTxt + " Nr.: " + ALLTRIM(lcNo) + ";"
	ENDIF 
	IF lcEstab != "-1"
		lcRegraTxt = lcRegraTxt + " Dep.: " + lcEstab + ";"
	ENDIF 
	IF !EMPTY(lcID)
		lcRegraTxt = lcRegraTxt + " ID: " + ALLTRIM(lcID) + ";"
	ENDIF 
	IF !EMPTY(lcMorada)
		lcRegraTxt = lcRegraTxt + " Morada: " + ALLTRIM(lcMorada) + ";"
	ENDIF 
	IF !EMPTY(lcNcont)
		lcRegraTxt = lcRegraTxt + " Contrib.: " + ALLTRIM(lcNcont) + ";"
	ENDIF 
	IF !EMPTY(lcNCartao)
		lcRegraTxt = lcRegraTxt + " Cart�o: " + ALLTRIM(lcNCartao) + ";"
	ENDIF 
	IF !EMPTY(lcTipo)
		lcRegraTxt = lcRegraTxt + " Tipo: " + ALLTRIM(lcTipo) + ";"
	ENDIF 
	IF !EMPTY(lcProfissao)
		lcRegraTxt = lcRegraTxt + " Profissao: " + ALLTRIM(lcProfissao) + ";"
	ENDIF 
	IF !EMPTY(lcSexo)
		lcRegraTxt = lcRegraTxt + " Sexo: " + ALLTRIM(lcSexo) + ";"
	ENDIF
	IF !EMPTY(lctlf)
		lcRegraTxt = lcRegraTxt + " Telefone/Tlm.: " + ALLTRIM(lctlf) + ";"
	ENDIF 
	IF !EMPTY(lcTlmvl)
		lcRegraTxt = lcRegraTxt + " Tlmvl: Sim;"
	ENDIF

	IF &lcObj..label6.caption == 'Idade'
		IF !EMPTY(&lcObj..idade.value)
			lcRegraTxt = lcRegraTxt + " Idade " + ALLTRIM(lcOperador) + lcIdade + ";"
		ENDIF
	ENDIF
	IF &lcObj..label6.caption == 'Dt. Nasc.'
		IF lcNascimento != '19000101'
			lcRegraTxt = lcRegraTxt + " Dt. Nasc.: " + lcNascimento + ";"
		ENDIF
	ENDIF
	IF !EMPTY(lcObs)
		lcRegraTxt = lcRegraTxt + " Obs: " + ALLTRIM(lcObs) + ";"
	ENDIF
	IF !EMPTY(lcMail)
		lcRegraTxt = lcRegraTxt + " eMail: Sim;"
	ENDIF
	IF !EMPTY(lcEFR)
		lcRegraTxt = lcRegraTxt + " EFR: " + ALLTRIM(lcEFR) + ";"
	ENDIF
	IF !EMPTY(lcPatologia)
		lcRegraTxt = lcRegraTxt + " Patologia: " + ALLTRIM(lcPatologia) + ";"
	ENDIF
	IF !EMPTY(lcnbenef)
		lcRegraTxt = lcRegraTxt + " Nbenef: " + ALLTRIM(lcnbenef) + ";"
	ENDIF
	IF !EMPTY(lcInactivo)
		lcRegraTxt = lcRegraTxt + " Incl. Inativos: Sim;"
	ENDIF
	IF !EMPTY(lcRef)
		lcRegraTxt = lcRegraTxt + "  Adquiriu os seguintes Produtos: " + lcRef + IIF(lcEntre="19000101"," desde sempre", " entre: " + lcEntre + " e " + lcE) + ";"
	ENDIF 
	
	IF empty(lcno)
		lcNo = "0"
	ENDIF 
	
	SELECT ucrsCampanhasUt
	APPEND BLANK 
	replace ucrsCampanhasUt.tipoExcecao WITH "REGRA"
	replace ucrsCampanhasUt.campanhas_id WITH ucrsCampanhas.id
	Replace ucrsCampanhasUt.regra WITH lcRegraTxt
	Replace ucrsCampanhasUt.nome WITH lcNome
	Replace ucrsCampanhasUt.No WITH VAL(lcNo)
	Replace ucrsCampanhasUt.Estab WITH Val(lcEstab)
	Replace ucrsCampanhasUt.Morada WITH lcMorada
	Replace ucrsCampanhasUt.Ncont WITH lcNcont
	Replace ucrsCampanhasUt.Ncartao WITH lcNCartao
	Replace ucrsCampanhasUt.Tipo WITH lcTipo
	Replace ucrsCampanhasUt.Profissao WITH lcProfissao
	Replace ucrsCampanhasUt.Sexo WITH lcSexo
	Replace ucrsCampanhasUt.Tlmvl WITH IIF(EMPTY(lcTlmvl),.f.,.t.)
	Replace ucrsCampanhasUt.Operador WITH lcOperador
	Replace ucrsCampanhasUt.Idade WITH VAL(lcIdade)
	Replace ucrsCampanhasUt.Obs WITH lcObs
	Replace ucrsCampanhasUt.Mail WITH IIF(EMPTY(lcMail),.f.,.t.)
	Replace ucrsCampanhasUt.EntPla WITH lcEFR
	Replace ucrsCampanhasUt.Patologia WITH lcPatologia
	Replace ucrsCampanhasUt.Ref WITH lcRef
	Replace ucrsCampanhasUt.Entre WITH lcEntre2
	Replace ucrsCampanhasUt.E WITH lcE2
	Replace ucrsCampanhasUt.sempre WITH IIF(lcEntre="19000101",.t.,.f.)
	Replace ucrsCampanhasUt.DtNasc WITH lcNascimento2 
	Replace ucrsCampanhasUt.Inactivo WITH IIF(EMPTY(lcInactivo),.f.,.t.)
	Replace ucrsCampanhasUt.Tlf WITH lctlf
	Replace ucrsCampanhasUt.Nbenef WITH lcnbenef
&&	Replace ucrsCampanhasUt.Nbenef2
	Replace ucrsCampanhasUt.Touch WITH .f.
	Replace ucrsCampanhasUt.EntCompart WITH .f.
	Replace ucrsCampanhasUt.utId WITH lcID
	
	CAMPANHAS.GridUt.refresh
	
	
	uf_PESQUTENTES_sair()
ENDFUNC 


**
FUNCTION uf_Campanhas_ApagaPesqUt
	SELECT ucrsCampanhasUt
	DELETE 
	TRY
		SELECT ucrsCampanhasUt
		SKIP -1
	ENDTRY
	CAMPANHAS.GridUt.refresh
ENDFUNC 



**
FUNCTION uf_CAMPANHAS_novo

	IF !uf_gerais_validaPermUser(ch_userno,ch_grupo,'Campanhas - Introduzir')
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE INTRODUZIR CAMPANHAS.","OK","",48)
		RETURN .f.
	ENDIF

	SELECT ucrsCampanhas
	GO Top
	SCAN
		DELETE 
	ENDSCAN 
	SELECT ucrsCampanhasUt
	GO Top
	SCAN
		DELETE 
	ENDSCAN 	
	SELECT ucrsCampanhasSt
	GO Top
	SCAN
		DELETE 
	ENDSCAN 	
	SELECT ucrsRebatimentosNiveisPontos
	GO Top
	SCAN
		DELETE 
	ENDSCAN 	
	SELECT ucrsCampanhasPOfertas
	GO Top
	SCAN
		DELETE 
	ENDSCAN 	
	
	
	myCampanhasIntroducao = .t.
	uf_CAMPANHAS_alternaMenu()
	
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		SELECT ISNULL(MAX(id),0) +1 as id FROM campanhas
	ENDTEXT 
	If !uf_gerais_actGrelha("", "ucrsCampanhasId", lcSql)
		uf_perguntalt_chama("N�o foi possivel carregar informa��o da campanha.","OK","",32)
		RETURN .f.
	ENDIF 

	SELECT ucrsCampanhasId
	lcID = 	ucrsCampanhasId.id
	SELECT ucrsCampanhas
	APPEND BLANK
	replace ucrsCampanhas.id WITH lcID
	replace ucrsCampanhas.dataInicio WITH datetime()+(difhoraria*3600)
	replace ucrsCampanhas.dataFim WITH datetime()+(difhoraria*3600)
	replace ucrsCampanhas.site WITH mysite
	
	CAMPANHAS.GridUt.refresh
	CAMPANHAS.GridSt.refresh
	CAMPANHAS.PageFrame1.Page7.refresh
	CAMPANHAS.PageFrame1.Page6.refresh

	CAMPANHAS.refresh
ENDFUNC


**
FUNCTION uf_CAMPANHAS_editar
	
	IF !uf_gerais_validaPermUser(ch_userno,ch_grupo,'Campanhas - Alterar')
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ALTERAR CAMPANHAS.","OK","",48)
		RETURN .f.
	ENDIF
	
	myCampanhasAlteracao = .t.
	uf_CAMPANHAS_alternaMenu()
ENDFUNC


**
FUNCTION uf_Campanhas_NovaPesqSt
	CAMPANHAS.Oferta = .f.
	uf_pesqstocks_chama('CAMPANHAS')
ENDFUNC 


**
FUNCTION uf_Campanhas_ApagaPesqSt
	SELECT ucrsCampanhasST
	DELETE 
	TRY
		SELECT ucrsCampanhasST
		SKIP -1
	ENDTRY
	CAMPANHAS.GridSt.refresh
ENDFUNC 


**
FUNCTION uf_Campanhas_NovaPesqStOferta

	CAMPANHAS.Oferta = .t.
	uf_pesqstocks_chama('CAMPANHAS')
ENDFUNC 


**
FUNCTION uf_Campanhas_ApagaPesqStOferta
	SELECT ucrsCampanhasPOfertas
	DELETE 
	TRY
		SELECT ucrsCampanhasPOfertas
		SKIP -1
	ENDTRY
	CAMPANHAS.PageFrame1.Page6.GridOfertas.refresh
ENDFUNC 



**
FUNCTION uf_CAMPANHAS_selPesquisaSt
	LOCAL lcSQL, lcTop, lcRef, lcFamilia, lcLab, lcMarca, lcDci, lcStock, lcGenerico, lcCNPEM, lcRecurso, lcAtributos, lcConvencao , lcTipo
	LOCAL lcOperadorLogico, lcInactivos, lcServicos, lcSiteNr,lcSite, lcParafarmacia
	STORE "" TO lcSQL, lcRef, lcFamilia, lcLab, lcMarca, lcDci, lcCNPEM,lcRecurso, lcAtributos, lcSite, lcConvencao ,lcTipo
	STORE 0 TO lcTop, lcInactivos, lcGenerico, lcServicos, lcSiteNr, lcParafarmacia
	
	
	LOCAL lcObj
	lcObj = "pesqstocks.pageframe1.page1"
	
	**Trata parametros
	lcRef = ALLTRIM(&lcObj..ref.value)
	lcDci = ALLTRIM(&lcObj..dci.value)
	lcFamilia = ALLTRIM(&lcObj..familia.value)
	lcLab = ALLTRIM(&lcObj..lab.value)
	lcMarca = ALLTRIM(&lcObj..marca.value)

	lcGenerico	= ALLTRIM(&lcObj..genericos.value)
	lcGenerico2	= IIF(ALLTRIM(&lcObj..genericos.value)=="", -1, IIF(ALLTRIM(&lcObj..genericos.value)=="Gen�ricos",1,0))
	
	** Quantidade de stock
	lcOperadorLogico	= ALLTRIM(&lcObj..OperadorLogico.value)
			
	&& Valida Stock
	If !empty(&lcObj..stock.value)
		IF TYPE(&lcObj..stock.value) == "N"
			lcStock = &lcObj..stock.value
		ELSE
			lcStock = "0"
		ENDIF
	ELSE
		lcStock = "-999999"
	ENDIF

	lcServicos	= 0 
	IF UPPER(ALLTRIM(&lcObj..tipo.value)) == "SERVICOS"
		lcServicos	= 1 
	ENDIF 
	IF UPPER(ALLTRIM(&lcObj..tipo.value)) == "PRODUTOS"
		lcServicos	= 2		
	ENDIF 
	
	lcInactivos		= IIF(&lcObj..chkinactivos.tag == "true", 1, 0)
	lcParafarmacia 	= IIF (&lcObj..chkParafarmacia.tag == "true", 1,0)
	lcComFicha		= IIF(&lcObj..chkComFicha.tag == "true", 1, 0)
	lcT5			= IIF(&lcObj..chkT5.tag == "true", 1, 0)
	lctxIva 		= &lcObj..txIva.value
	lctxIvaDesc		= ASTR(&lcObj..txIva.value)
	lcTipo			= ALLTRIM(&lcObj..tipoproduto.value)
	
	lcRegraTxt = ""
	IF !EMPTY(lcRef)
		lcRegraTxt = lcRegraTxt + " Ref/Design/CNPEM: " + ALLTRIM(lcRef) + ";"
	ENDIF 
	IF !EMPTY(lcDci)
		lcRegraTxt = lcRegraTxt + " DCI: " + ALLTRIM(lcDci) + ";"
	ENDIF 
	IF !EMPTY(lcFamilia)
		lcRegraTxt = lcRegraTxt + " Class.Infarmed: " + ALLTRIM(lcFamilia) + ";"
	ENDIF 
	IF !EMPTY(lcGenerico)
		lcRegraTxt = lcRegraTxt + " Gen�ricos: " + ALLTRIM(lcGenerico) + ";"
	ENDIF 
	IF !EMPTY(lcLab)
		lcRegraTxt = lcRegraTxt + " Lab.: " + ALLTRIM(lcLab) + ";"
	ENDIF 	
	IF !EMPTY(lcMarca)
		lcRegraTxt = lcRegraTxt + " Marca: " + ALLTRIM(lcMarca) + ";"
	ENDIF 		
*!*		IF !empty(&lcObj..stock.value)
*!*			lcRegraTxt = lcRegraTxt + " Stock " + lcOperadorLogico + " " + ASTR(lcStock) + ";"
*!*		ENDIF 
	IF !EMPTY(UPPER(ALLTRIM(&lcObj..tipo.value)))
		lcRegraTxt = lcRegraTxt + " Tipo " + ALLTRIM(UPPER(ALLTRIM(&lcObj..tipo.value))) + ";"
	ENDIF 
	IF !EMPTY(lcInactivos)
		lcRegraTxt = lcRegraTxt + " Incl. Inativos: Sim;"
	ENDIF
*!*		IF !EMPTY(lcComFicha)
*!*			lcRegraTxt = lcRegraTxt + " Com ficha: Sim;"
*!*		ENDIF
	IF !EMPTY(lcParafarmacia)
		lcRegraTxt = lcRegraTxt + " ParaFarm�cia: Sim;"
	ENDIF
	IF !EMPTY(lcT5)
		lcRegraTxt = lcRegraTxt + " T5: Sim;"
	ENDIF
	IF !(lctxIvaDesc == "")
		lcRegraTxt = lcRegraTxt + " Tx.Iva: " + lctxIvaDesc + ";"
	ENDIF
	IF !EMPTY(lcTipo)
		lcRegraTxt = lcRegraTxt + " Tipo: " + ALLTRIM(lcTipo) + ";"
	ENDIF
	
	
	IF EMPTY(CAMPANHAS.Oferta)
		SELECT ucrsCampanhasSt
		APPEND BLANK 
		replace ucrsCampanhasSt.qt WITH 1
		replace ucrsCampanhasSt.tipoExcecao WITH "REGRA"
		replace ucrsCampanhasSt.campanhas_id WITH ucrsCampanhas.id
		Replace ucrsCampanhasSt.regra WITH lcRegraTxt
		Replace ucrsCampanhasSt.Artigo WITH lcRef
		Replace ucrsCampanhasSt.Familia WITH lcFamilia
		Replace ucrsCampanhasSt.Lab WITH lcLab
		Replace ucrsCampanhasSt.Marca WITH lcMarca
		Replace ucrsCampanhasSt.Dci WITH lcDci
		Replace ucrsCampanhasSt.Servicos WITH lcServicos
		Replace ucrsCampanhasSt.OperadorLogico WITH lcOperadorLogico
		Replace ucrsCampanhasSt.Generico WITH lcGenerico2
		Replace ucrsCampanhasSt.Inactivo WITH IIF(EMPTY(lcInactivos),.f.,.t.)
		Replace ucrsCampanhasSt.Atendimento WITH .f.
		Replace ucrsCampanhasSt.Recurso WITH ""
		Replace ucrsCampanhasSt.Site_nr WITH mysite_nr
		Replace ucrsCampanhasSt.Parafarmacia WITH IIF(EMPTY(lcParafarmacia),.f.,.t.)
		Replace ucrsCampanhasSt.ComFicha WITH IIF(EMPTY(lcComFicha),.f.,.t.)
		Replace ucrsCampanhasSt.T5 WITH IIF(EMPTY(lcT5),.f.,.t.)
		IF !EMPTY(lctxIva)
			Replace ucrsCampanhasSt.txIva WITH lctxIva 
		ENDIF
		Replace ucrsCampanhasSt.stock WITH VAL(lcStock)
		Replace ucrsCampanhasSt.tipo WITH lcTipo
		

		
		CAMPANHAS.GridST.refresh
	ELSE
		SELECT ucrsCampanhasPOfertas
		APPEND BLANK 
		replace ucrsCampanhasPOfertas.qt WITH 1
		replace ucrsCampanhasPOfertas.tipoExcecao WITH "REGRA"
		replace ucrsCampanhasPOfertas.campanhas_id WITH ucrsCampanhas.id
		Replace ucrsCampanhasPOfertas.regra WITH lcRegraTxt
		Replace ucrsCampanhasPOfertas.Artigo WITH lcRef
		Replace ucrsCampanhasPOfertas.Familia WITH lcFamilia
		Replace ucrsCampanhasPOfertas.Lab WITH lcLab
		Replace ucrsCampanhasPOfertas.Marca WITH lcMarca
		Replace ucrsCampanhasPOfertas.Dci WITH lcDci
		Replace ucrsCampanhasPOfertas.Servicos WITH lcServicos
		Replace ucrsCampanhasPOfertas.OperadorLogico WITH lcOperadorLogico
		Replace ucrsCampanhasPOfertas.Generico WITH lcGenerico2
		Replace ucrsCampanhasPOfertas.Inactivo WITH IIF(EMPTY(lcInactivos),.f.,.t.)
		Replace ucrsCampanhasPOfertas.Atendimento WITH .f.
		Replace ucrsCampanhasPOfertas.Recurso WITH ""
		Replace ucrsCampanhasPOfertas.Site_nr WITH mysite_nr
		Replace ucrsCampanhasPOfertas.Parafarmacia WITH IIF(EMPTY(lcParafarmacia),.f.,.t.)
		Replace ucrsCampanhasPOfertas.ComFicha WITH IIF(EMPTY(lcComFicha),.f.,.t.)
		Replace ucrsCampanhasPOfertas.T5 WITH IIF(EMPTY(lcT5),.f.,.t.)
		IF !EMPTY(lctxIva)
			Replace ucrsCampanhasSt.txIva WITH lctxIva 
		ENDIF
		Replace ucrsCampanhasSt.stock WITH VAL(lcStock)
		Replace ucrsCampanhasSt.tipo WITH lcTipo
		
		CAMPANHAS.PageFrame1.Page6.GridOfertas.refresh
	ENDIF 
	
	**
	uf_pesqstocks_sair()

ENDFUNC 

**
FUNCTION uf_CAMPANHAS_gravar
	LOCAL lcExecuteSQL, lcSelSites ,lcSelLojas , lcSiteesc, lcSitenoesc, lcMulti
	lcExecuteSQL = ""
	lcSelSites = ""
	lcSelLojas = ""
	lcSiteesc = ""
	lcSitenoesc = 0 
	lcMulti=0
		
	SELECT ucrsCampanhas		
	IF EMPTY(ALLTRIM(ucrsCampanhas.descricao))
		uf_perguntalt_chama("Existem campos obrigat�rios por preencher.","OK","",64)
		RETURN .f.
	ENDIF 

 	IF myCampanhasIntroducao == .t.	
		** Verificar se existir mais que uma loja pergunta em que lojas quer inserir
		TEXT To lcSQL TEXTMERGE NOSHOW
			select site from empresa order by site
		ENDTEXT 
		uf_gerais_actGrelha("","ucrsEmpresasTMP",lcSQL)
		IF RECCOUNT("ucrsEmpresasTMP")>1 then
			lcMulti=1
			uf_altgr_escolheEmpresasCRM()
		ENDIF 
		
		IF !USED("ucrsEmpresasSel") AND lcMulti=1 then
			RETURN .f.
		ENDIF 

		IF lcMulti=1 		
			lcCount=1
			SELECT ucrsEmpresasSel
			GO top
			SCAN
				IF lcCount=1 then
					lcSelSites = ALLTRIM(STR(ucrsEmpresasSel.siteno))
					lcSelLojas = ALLTRIM(ucrsEmpresasSel.local)
				ELSE
					lcSelSites = lcSelSites + ', ' + ALLTRIM(STR(ucrsEmpresasSel.siteno))
					lcSelLojas = lcSelLojas + ', ' + ALLTRIM(ucrsEmpresasSel.local)
				ENDIF 
				lcCount=lcCount+1
			ENDSCAN 
		ELSE
			lcSelSites = ''
			lcSelLojas = ''
		ENDIF 
		**MESSAGEBOX(lcSelSites)
	ENDIF 
		
	SELECT ucrsCampanhas
	lcSQL = ""
	
	IF myCampanhasIntroducao == .t.

		** Verifica se � mono ou multi empresa
		IF lcMulti=1 THEN 
			** Criar as campanhas paras as empresas escolhidas
			SELECT ucrsEmpresasSel
			GO TOP 
			SCAN
				lcSiteesc = ALLTRIM(ucrsEmpresasSel.local)
				lcSitenoesc = ucrsEmpresasSel.siteno
				TEXT TO lcSQL NOSHOW TEXTMERGE 
										
					INSERT INTO campanhas (
						id
						,descricao
						,dataInicio
						,dataFim
						,inativo
						,valor
						,pontos
						,nat
						,pontosat
						,pordiaat
						,nvd
						,pontosvd
						,site
						,id_us
						,id_us_alt
						,data_cri
						,data_alt
						,desconto
						,descv
						,valorValeDireto
						,qtProdutos
						,obs
						,mensagem
						,descValorAtPerc
						,valorValesDistribuir
						,remanescente
						,mensagemVale
						,mensagemValeRem
						,maisBarato
						,valcartao
						,vallimite
						,acumula
					) VALUES(
						<<ucrsCampanhas.id>>+<<lcSitenoesc>>
						,'<<ALLTRIM(ucrsCampanhas.descricao)>>'
						,'<<uf_gerais_getdate(ucrsCampanhas.dataInicio,"SQL")>>'
						,'<<uf_gerais_getdate(ucrsCampanhas.dataFim,"SQL")>>'
						,<<IIF(ucrsCampanhas.inativo,1,0)>>
						,<<ucrsCampanhas.valor>>
						,<<ucrsCampanhas.pontos>>
						,<<ucrsCampanhas.nat>>
						,<<ucrsCampanhas.pontosat>>
						,<<IIF(ucrsCampanhas.pordiaat,1,0)>>
						,<<ucrsCampanhas.nvd>>
						,<<ucrsCampanhas.pontosvd>>
						,'<<ALLTRIM(lcSiteesc)>>'
						--,'<<ALLTRIM(ucrsCampanhas.site)>>'
						,<<ch_userno>>
						,<<ch_userno>>
						,dateadd(HOUR, <<difhoraria>>, getdate())
						,dateadd(HOUR, <<difhoraria>>, getdate())
						,<<ucrsCampanhas.desconto>>
						,<<ucrsCampanhas.descv>>
						,<<ucrsCampanhas.valorValeDireto>>
						,<<ucrsCampanhas.qtProdutos>>
						,'<<ALLTRIM(ucrsCampanhas.obs)>>'
						,'<<ALLTRIM(ucrsCampanhas.mensagem)>>'
						,<<ucrsCampanhas.descValorAtPerc>>
						,<<ucrsCampanhas.valorValesDistribuir>>
						,<<IIF(ucrsCampanhas.remanescente,1,0)>>
						,'<<ALLTRIM(ucrsCampanhas.mensagemVale)>>'
						,'<<ALLTRIM(ucrsCampanhas.mensagemValeRem)>>'
						,<<IIF(ucrsCampanhas.maisBarato,1,0)>>
						,<<ucrsCampanhas.valcartao>>
						,<<ucrsCampanhas.vallimite>>
						,<<IIF(ucrsCampanhas.acumula,1,0)>>
					)
			
				ENDTEXT 
				lcExecuteSQL  = lcExecuteSQL + CHR(13) + CHR(13) + lcSQL
			ENDSCAN 
			SELECT ucrsCampanhas
		ELSE 
		
			TEXT TO lcSQL NOSHOW TEXTMERGE 
				DELETE FROM campanhas_ut WHERE campanhas_id = <<ucrsCampanhas.id>>
				DELETE FROM campanhas_st WHERE campanhas_id = <<ucrsCampanhas.id>>
				DELETE FROM campanhas_pNiveis WHERE campanhas_id = <<ucrsCampanhas.id>>
				DELETE FROM campanhas_pofertas WHERE campanhas_id = <<ucrsCampanhas.id>>
				
				INSERT INTO campanhas (
					id
					,descricao
					,dataInicio
					,dataFim
					,inativo
					,valor
					,pontos
					,nat
					,pontosat
					,pordiaat
					,nvd
					,pontosvd
					,site
					,id_us
					,id_us_alt
					,data_cri
					,data_alt
					,desconto
					,descv
					,valorValeDireto
					,qtProdutos
					,obs
					,mensagem
					,descValorAtPerc
					,valorValesDistribuir
					,remanescente
					,mensagemVale
					,mensagemValeRem
					,maisBarato
					,valcartao
					,vallimite
					,acumula
				) VALUES(
					<<ucrsCampanhas.id>>
					,'<<ALLTRIM(ucrsCampanhas.descricao)>>'
					,'<<uf_gerais_getdate(ucrsCampanhas.dataInicio,"SQL")>>'
					,'<<uf_gerais_getdate(ucrsCampanhas.dataFim,"SQL")>>'
					,<<IIF(ucrsCampanhas.inativo,1,0)>>
					,<<ucrsCampanhas.valor>>
					,<<ucrsCampanhas.pontos>>
					,<<ucrsCampanhas.nat>>
					,<<ucrsCampanhas.pontosat>>
					,<<IIF(ucrsCampanhas.pordiaat,1,0)>>
					,<<ucrsCampanhas.nvd>>
					,<<ucrsCampanhas.pontosvd>>
					,'<<ALLTRIM(ucrsCampanhas.site)>>'
					,<<ch_userno>>
					,<<ch_userno>>
					,dateadd(HOUR, <<difhoraria>>, getdate())
					,dateadd(HOUR, <<difhoraria>>, getdate())
					,<<ucrsCampanhas.desconto>>
					,<<ucrsCampanhas.descv>>
					,<<ucrsCampanhas.valorValeDireto>>
					,<<ucrsCampanhas.qtProdutos>>
					,'<<ALLTRIM(ucrsCampanhas.obs)>>'
					,'<<ALLTRIM(ucrsCampanhas.mensagem)>>'
					,<<ucrsCampanhas.descValorAtPerc>>
					,<<ucrsCampanhas.valorValesDistribuir>>
					,<<IIF(ucrsCampanhas.remanescente,1,0)>>
					,'<<ALLTRIM(ucrsCampanhas.mensagemVale)>>'
					,'<<ALLTRIM(ucrsCampanhas.mensagemValeRem)>>'
					,<<IIF(ucrsCampanhas.maisBarato,1,0)>>
					,<<ucrsCampanhas.valcartao>>
					,<<ucrsCampanhas.vallimite>>
					,<<IIF(ucrsCampanhas.acumula,1,0)>>
				)
			
			ENDTEXT 
			lcExecuteSQL  = lcExecuteSQL + CHR(13) + CHR(13) + lcSQL
		ENDIF 		

	ELSE
		lcSQL = ""
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			
			DELETE FROM campanhas_ut WHERE campanhas_id = <<ucrsCampanhas.id>>
			DELETE FROM campanhas_st WHERE campanhas_id = <<ucrsCampanhas.id>>
			DELETE FROM campanhas_pNiveis WHERE campanhas_id = <<ucrsCampanhas.id>>
			DELETE FROM campanhas_pofertas WHERE campanhas_id = <<ucrsCampanhas.id>>

			UPDATE campanhas 
			SET 
				descricao = '<<ALLTRIM(ucrsCampanhas.descricao)>>'
				,dataInicio = '<<uf_gerais_getdate(ucrsCampanhas.dataInicio,"SQL")>>'
				,dataFim = '<<uf_gerais_getdate(ucrsCampanhas.dataFim,"SQL")>>'
				,inativo = <<IIF(ucrsCampanhas.inativo,1,0)>>
				,valor = <<ucrsCampanhas.valor>>
				,pontos = <<ucrsCampanhas.pontos>>
				,nat = <<ucrsCampanhas.nat>>
				,pontosat = <<ucrsCampanhas.pontosat>>
				,pordiaat = <<IIF(ucrsCampanhas.pordiaat,1,0)>>
				,nvd = <<ucrsCampanhas.nvd>>
				,pontosvd = <<ucrsCampanhas.pontosvd>>
				,site = '<<ALLTRIM(ucrsCampanhas.site)>>'
				,id_us_alt = <<ch_userno>>
				,data_alt = dateadd(HOUR, <<difhoraria>>, getdate())
				,desconto = <<ucrsCampanhas.desconto>>
				,descv = <<ucrsCampanhas.descv>>
				,valorValeDireto = <<ucrsCampanhas.valorValeDireto>>
				,qtProdutos = <<ucrsCampanhas.qtProdutos>>
				,obs = '<<ALLTRIM(ucrsCampanhas.obs)>>'
				,mensagem = '<<ALLTRIM(ucrsCampanhas.mensagem)>>'
				,descValorAtPerc = <<ucrsCampanhas.descValorAtPerc>>
				,valorValesDistribuir = <<ucrsCampanhas.valorValesDistribuir>>
				,remanescente= <<IIF(ucrsCampanhas.remanescente,1,0)>>
				,mensagemVale= '<<ALLTRIM(ucrsCampanhas.mensagemVale)>>'
				,mensagemValeRem= '<<ALLTRIM(ucrsCampanhas.mensagemValeRem)>>'
				,maisBarato = <<IIF(ucrsCampanhas.maisBarato,1,0)>>
				,valcartao = <<ucrsCampanhas.valcartao>>
				,vallimite = <<ucrsCampanhas.vallimite>>
				,acumula = <<IIF(ucrsCampanhas.acumula,1,0)>>
			WHERE
				campanhas.id = <<ucrsCampanhas.id>>
		
		ENDTEXT 
		lcExecuteSQL  = lcExecuteSQL + CHR(13) + CHR(13) + lcSQL
	ENDIF 
	
	IF myCampanhasIntroducao == .t. AND lcMulti=1 THEN 
	
		SELECT ucrsEmpresasSel
		GO TOP 
		SCAN
			lcSiteesc = ALLTRIM(ucrsEmpresasSel.local)
			lcSitenoesc = ucrsEmpresasSel.siteno
			SELECT ucrsCampanhasUt
			GO Top
			SCAN
				lcSQL = ""
				TEXT TO lcSQL NOSHOW TEXTMERGE 
					INSERT INTO campanhas_ut (
						id
						,campanhas_id
						,nome
						,No
						,Estab
						,Morada
						,Ncont
						,Ncartao
						,Tipo
						,Profissao
						,Sexo
						,Tlmvl
						,Operador
						,Idade
						,Obs
						,Mail
						,EntPla
						,Patologia
						,Ref
						,Entre
						,E
						,sempre
						,DtNasc
						,Inactivo
						,Tlf
						,Nbenef
						,Nbenef2
						,Touch
						,EntCompart
						,utId
						,regra
						,excecao
					) VALUES(
						(select ISNULL(MAX(id),0) + 1 from campanhas_ut)
						,<<ucrsCampanhas.id>>+<<lcSitenoesc>>
						,'<<ucrsCampanhasUt.nome>>'
						,<<ucrsCampanhasUt.No>>
						,<<ucrsCampanhasUt.Estab>>
						,'<<ucrsCampanhasUt.Morada>>'
						,'<<ucrsCampanhasUt.Ncont>>'
						,'<<ucrsCampanhasUt.Ncartao>>'
						,'<<ucrsCampanhasUt.Tipo>>'
						,'<<ucrsCampanhasUt.Profissao>>'
						,'<<ucrsCampanhasUt.Sexo>>'
						,<<IIF(ucrsCampanhasUt.Tlmvl,1,0)>>
						,'<<ucrsCampanhasUt.Operador>>'
						,<<ucrsCampanhasUt.Idade>>
						,'<<ucrsCampanhasUt.Obs>>'
						,<<IIF(ucrsCampanhasUt.Mail,1,0)>>
						,'<<ucrsCampanhasUt.EntPla>>'
						,'<<ucrsCampanhasUt.Patologia>>'
						,'<<ucrsCampanhasUt.Ref>>'
						,'<<uf_gerais_getdate(ucrsCampanhasUt.Entre,"SQL")>>'
						,'<<uf_gerais_getdate(ucrsCampanhasUt.E,"SQL")>>'
						,<<IIF(ucrsCampanhasUt.sempre,1,0)>>
						,'<<uf_gerais_getdate(ucrsCampanhasUt.DtNasc,"SQL")>>'
						,<<IIF(ucrsCampanhasUt.Inactivo,1,0)>>
						,'<<ucrsCampanhasUt.Tlf>>'
						,'<<ucrsCampanhasUt.Nbenef>>'
						,'<<ucrsCampanhasUt.Nbenef2>>'
						,<<IIF(ucrsCampanhasUt.Touch,1,0)>>
						,<<IIF(ucrsCampanhasUt.EntCompart,1,0)>>
						,'<<ucrsCampanhasUt.utId>>'
						,'<<ucrsCampanhasUt.regra>>'
						,<<IIF(ucrsCampanhasUt.tipoExcecao == 'EXCECAO',1,0)>>
					)
				ENDTEXT
				lcExecuteSQL  = lcExecuteSQL + CHR(13) + CHR(13) + lcSQL
			ENDSCAN 
			
			SELECT ucrsEmpresasSel
		ENDSCAN
	
	ELSE 
		SELECT ucrsCampanhasUt
		GO Top
		SCAN
			lcSQL = ""
			TEXT TO lcSQL NOSHOW TEXTMERGE 
				INSERT INTO campanhas_ut (
					id
					,campanhas_id
					,nome
					,No
					,Estab
					,Morada
					,Ncont
					,Ncartao
					,Tipo
					,Profissao
					,Sexo
					,Tlmvl
					,Operador
					,Idade
					,Obs
					,Mail
					,EntPla
					,Patologia
					,Ref
					,Entre
					,E
					,sempre
					,DtNasc
					,Inactivo
					,Tlf
					,Nbenef
					,Nbenef2
					,Touch
					,EntCompart
					,utId
					,regra
					,excecao
				) VALUES(
					(select ISNULL(MAX(id),0) + 1 from campanhas_ut)
					,<<ucrsCampanhas.id>>
					,'<<ucrsCampanhasUt.nome>>'
					,<<ucrsCampanhasUt.No>>
					,<<ucrsCampanhasUt.Estab>>
					,'<<ucrsCampanhasUt.Morada>>'
					,'<<ucrsCampanhasUt.Ncont>>'
					,'<<ucrsCampanhasUt.Ncartao>>'
					,'<<ucrsCampanhasUt.Tipo>>'
					,'<<ucrsCampanhasUt.Profissao>>'
					,'<<ucrsCampanhasUt.Sexo>>'
					,<<IIF(ucrsCampanhasUt.Tlmvl,1,0)>>
					,'<<ucrsCampanhasUt.Operador>>'
					,<<ucrsCampanhasUt.Idade>>
					,'<<ucrsCampanhasUt.Obs>>'
					,<<IIF(ucrsCampanhasUt.Mail,1,0)>>
					,'<<ucrsCampanhasUt.EntPla>>'
					,'<<ucrsCampanhasUt.Patologia>>'
					,'<<ucrsCampanhasUt.Ref>>'
					,'<<uf_gerais_getdate(ucrsCampanhasUt.Entre,"SQL")>>'
					,'<<uf_gerais_getdate(ucrsCampanhasUt.E,"SQL")>>'
					,<<IIF(ucrsCampanhasUt.sempre,1,0)>>
					,'<<uf_gerais_getdate(ucrsCampanhasUt.DtNasc,"SQL")>>'
					,<<IIF(ucrsCampanhasUt.Inactivo,1,0)>>
					,'<<ucrsCampanhasUt.Tlf>>'
					,'<<ucrsCampanhasUt.Nbenef>>'
					,'<<ucrsCampanhasUt.Nbenef2>>'
					,<<IIF(ucrsCampanhasUt.Touch,1,0)>>
					,<<IIF(ucrsCampanhasUt.EntCompart,1,0)>>
					,'<<ucrsCampanhasUt.utId>>'
					,'<<ucrsCampanhasUt.regra>>'
					,<<IIF(ucrsCampanhasUt.tipoExcecao == 'EXCECAO',1,0)>>
				)
			ENDTEXT
			lcExecuteSQL  = lcExecuteSQL + CHR(13) + CHR(13) + lcSQL
				
		ENDSCAN 
	ENDIF 

	IF myCampanhasIntroducao == .t. AND lcMulti=1 THEN 
	
		SELECT ucrsEmpresasSel
		GO TOP 
		SCAN
			lcSiteesc = ALLTRIM(ucrsEmpresasSel.local)
			lcSitenoesc = ucrsEmpresasSel.siteno
			SELECT ucrsCampanhasSt
			GO Top
			SCAN
				lcSQL = ""
				TEXT TO lcSQL NOSHOW TEXTMERGE 
					INSERT INTO campanhas_st (
						id
						,campanhas_id
						,Artigo
						,Familia
						,Lab
						,Marca
						,Dci
						,Servicos
						,OperadorLogico
						,Generico
						,Inactivo
						,Atendimento
						,Recurso
						,Site_nr
						,Parafarmacia
						,ComFicha
						,T5
						,regra
						,excecao
						,qt
						,txIva
						,stock
						,tipo
					) VALUES(
						(select ISNULL(MAX(id),0) + 1 from campanhas_st)
						,<<ucrsCampanhas.id>>+<<lcSitenoesc>>
						,'<<ALLTRIM(ucrsCampanhasSt.Artigo)>>'
						,'<<ALLTRIM(ucrsCampanhasSt.Familia)>>'
						,'<<ALLTRIM(ucrsCampanhasSt.Lab)>>'
						,'<<ALLTRIM(ucrsCampanhasSt.Marca)>>'
						,'<<ALLTRIM(ucrsCampanhasSt.Dci)>>'
						,<<ucrsCampanhasSt.Servicos>>
						,'<<ALLTRIM(ucrsCampanhasSt.OperadorLogico)>>'
						,<<ucrsCampanhasSt.Generico>>
						,<<IIF(ucrsCampanhasSt.Inactivo,1,0)>>
						,<<IIF(ucrsCampanhasSt.Atendimento,1,0)>>
						,'<<ALLTRIM(ucrsCampanhasSt.Recurso)>>'
						,<<ucrsCampanhasSt.Site_nr>>
						,<<IIF(ucrsCampanhasSt.Parafarmacia,1,0)>>
						,<<IIF(ucrsCampanhasSt.ComFicha,1,0)>>
						,<<IIF(ucrsCampanhasSt.T5,1,0)>>
						,'<<ALLTRIM(ucrsCampanhasSt.regra)>>'
						,<<IIF(ucrsCampanhasSt.tipoExcecao == 'EXCECAO',1,0)>>
						,<<ucrsCampanhasSt.qt>>
						,<<ucrsCampanhasSt.txIva>>
						,<<ucrsCampanhasSt.stock>>
						,'<<ucrsCampanhasSt.tipo>>'
					)
				ENDTEXT
				lcExecuteSQL  = lcExecuteSQL + CHR(13) + CHR(13) + lcSQL
			ENDSCAN 
			SELECT ucrsEmpresasSel
		ENDSCAN
			
	ELSE
		SELECT ucrsCampanhasSt
		GO Top
		SCAN
			lcSQL = ""
			TEXT TO lcSQL NOSHOW TEXTMERGE 
				INSERT INTO campanhas_st (
					id
					,campanhas_id
					,Artigo
					,Familia
					,Lab
					,Marca
					,Dci
					,Servicos
					,OperadorLogico
					,Generico
					,Inactivo
					,Atendimento
					,Recurso
					,Site_nr
					,Parafarmacia
					,ComFicha
					,T5
					,regra
					,excecao
					,qt
					,txIva
					,stock
					,tipo
				) VALUES(
					(select ISNULL(MAX(id),0) + 1 from campanhas_st)
					,<<ucrsCampanhas.id>>
					,'<<ALLTRIM(ucrsCampanhasSt.Artigo)>>'
					,'<<ALLTRIM(ucrsCampanhasSt.Familia)>>'
					,'<<ALLTRIM(ucrsCampanhasSt.Lab)>>'
					,'<<ALLTRIM(ucrsCampanhasSt.Marca)>>'
					,'<<ALLTRIM(ucrsCampanhasSt.Dci)>>'
					,<<ucrsCampanhasSt.Servicos>>
					,'<<ALLTRIM(ucrsCampanhasSt.OperadorLogico)>>'
					,<<ucrsCampanhasSt.Generico>>
					,<<IIF(ucrsCampanhasSt.Inactivo,1,0)>>
					,<<IIF(ucrsCampanhasSt.Atendimento,1,0)>>
					,'<<ALLTRIM(ucrsCampanhasSt.Recurso)>>'
					,<<ucrsCampanhasSt.Site_nr>>
					,<<IIF(ucrsCampanhasSt.Parafarmacia,1,0)>>
					,<<IIF(ucrsCampanhasSt.ComFicha,1,0)>>
					,<<IIF(ucrsCampanhasSt.T5,1,0)>>
					,'<<ALLTRIM(ucrsCampanhasSt.regra)>>'
					,<<IIF(ucrsCampanhasSt.tipoExcecao == 'EXCECAO',1,0)>>
					,<<ucrsCampanhasSt.qt>>
					,<<ucrsCampanhasSt.txIva>>
					,<<ucrsCampanhasSt.stock>>
					,'<<ucrsCampanhasSt.tipo>>'
				)
			ENDTEXT
			lcExecuteSQL  = lcExecuteSQL + CHR(13) + CHR(13) + lcSQL
		
		ENDSCAN 
	ENDIF 
	
	IF myCampanhasIntroducao == .t. AND lcMulti=1 THEN 

		SELECT ucrsEmpresasSel
		GO TOP 
		SCAN
			lcSiteesc = ALLTRIM(ucrsEmpresasSel.local)
			lcSitenoesc = ucrsEmpresasSel.siteno
			
			SELECT ucrsCampanhasPOfertas
			GO Top
			SCAN
				lcSQL = ""
				TEXT TO lcSQL NOSHOW TEXTMERGE 
					INSERT INTO campanhas_pofertas (
						id
						,campanhas_id
						,Artigo
						,Familia
						,Lab
						,Marca
						,Dci
						,Servicos
						,OperadorLogico
						,Generico
						,Inactivo
						,Atendimento
						,Recurso
						,Site_nr
						,Parafarmacia
						,ComFicha
						,T5
						,regra
						,excecao
						,qt
					) VALUES(
						(select ISNULL(MAX(id),0) + 1 from campanhas_pofertas)
						,<<ucrsCampanhas.id>>+<<lcSitenoesc>>
						,'<<ALLTRIM(ucrsCampanhasPOfertas.Artigo)>>'
						,'<<ALLTRIM(ucrsCampanhasPOfertas.Familia)>>'
						,'<<ALLTRIM(ucrsCampanhasPOfertas.Lab)>>'
						,'<<ALLTRIM(ucrsCampanhasPOfertas.Marca)>>'
						,'<<ALLTRIM(ucrsCampanhasPOfertas.Dci)>>'
						,<<ucrsCampanhasPOfertas.Servicos>>
						,'<<ALLTRIM(ucrsCampanhasPOfertas.OperadorLogico)>>'
						,<<ucrsCampanhasPOfertas.Generico>>
						,<<IIF(ucrsCampanhasPOfertas.Inactivo,1,0)>>
						,<<IIF(ucrsCampanhasPOfertas.Atendimento,1,0)>>
						,'<<ALLTRIM(ucrsCampanhasPOfertas.Recurso)>>'
						,<<ucrsCampanhasPOfertas.Site_nr>>
						,<<IIF(ucrsCampanhasPOfertas.Parafarmacia,1,0)>>
						,<<IIF(ucrsCampanhasPOfertas.ComFicha,1,0)>>
						,<<IIF(ucrsCampanhasPOfertas.T5,1,0)>>
						,'<<ALLTRIM(ucrsCampanhasPOfertas.regra)>>'
						,<<IIF(ucrsCampanhasPOfertas.tipoExcecao == 'EXCECAO',1,0)>>
						,<<ucrsCampanhasPOfertas.qt>>
					)
				ENDTEXT
				lcExecuteSQL  = lcExecuteSQL + CHR(13) + CHR(13) + lcSQL
			ENDSCAN 
		
			SELECT ucrsEmpresasSel
		ENDSCAN 
	ELSE
		SELECT ucrsCampanhasPOfertas
		GO Top
		SCAN
			lcSQL = ""
			TEXT TO lcSQL NOSHOW TEXTMERGE 
				INSERT INTO campanhas_pofertas (
					id
					,campanhas_id
					,Artigo
					,Familia
					,Lab
					,Marca
					,Dci
					,Servicos
					,OperadorLogico
					,Generico
					,Inactivo
					,Atendimento
					,Recurso
					,Site_nr
					,Parafarmacia
					,ComFicha
					,T5
					,regra
					,excecao
					,qt
				) VALUES(
					(select ISNULL(MAX(id),0) + 1 from campanhas_pofertas)
					,<<ucrsCampanhas.id>>
					,'<<ALLTRIM(ucrsCampanhasPOfertas.Artigo)>>'
					,'<<ALLTRIM(ucrsCampanhasPOfertas.Familia)>>'
					,'<<ALLTRIM(ucrsCampanhasPOfertas.Lab)>>'
					,'<<ALLTRIM(ucrsCampanhasPOfertas.Marca)>>'
					,'<<ALLTRIM(ucrsCampanhasPOfertas.Dci)>>'
					,<<ucrsCampanhasPOfertas.Servicos>>
					,'<<ALLTRIM(ucrsCampanhasPOfertas.OperadorLogico)>>'
					,<<ucrsCampanhasPOfertas.Generico>>
					,<<IIF(ucrsCampanhasPOfertas.Inactivo,1,0)>>
					,<<IIF(ucrsCampanhasPOfertas.Atendimento,1,0)>>
					,'<<ALLTRIM(ucrsCampanhasPOfertas.Recurso)>>'
					,<<ucrsCampanhasPOfertas.Site_nr>>
					,<<IIF(ucrsCampanhasPOfertas.Parafarmacia,1,0)>>
					,<<IIF(ucrsCampanhasPOfertas.ComFicha,1,0)>>
					,<<IIF(ucrsCampanhasPOfertas.T5,1,0)>>
					,'<<ALLTRIM(ucrsCampanhasPOfertas.regra)>>'
					,<<IIF(ucrsCampanhasPOfertas.tipoExcecao == 'EXCECAO',1,0)>>
					,<<ucrsCampanhasPOfertas.qt>>
				)
			ENDTEXT
			lcExecuteSQL  = lcExecuteSQL + CHR(13) + CHR(13) + lcSQL
		ENDSCAN 
	ENDIF 
	
	IF myCampanhasIntroducao == .t. AND lcMulti=1 THEN 

		SELECT ucrsEmpresasSel
		GO TOP 
		SCAN
			lcSiteesc = ALLTRIM(ucrsEmpresasSel.local)
			lcSitenoesc = ucrsEmpresasSel.siteno
		
			SELECT ucrsRebatimentosNiveisPontos
			GO Top
			SCAN
				lcSQL = ""
				TEXT TO lcSQL NOSHOW TEXTMERGE 
					INSERT INTO campanhas_pNiveis (
						id
						,campanhas_id
						,nivel
						,valor
						,pontos
					) VALUES(
						(select ISNULL(MAX(id),0) + 1 from campanhas_pNiveis)
						,<<ucrsCampanhas.id>>
						,<<ucrsRebatimentosNiveisPontos.nivel>>
						,<<ucrsRebatimentosNiveisPontos.valor>>
						,<<ucrsRebatimentosNiveisPontos.pontos>>
					)
				ENDTEXT
				lcExecuteSQL  = lcExecuteSQL + CHR(13) + CHR(13) + lcSQL
		
			ENDSCAN 
			SELECT ucrsEmpresasSel		
		ENDSCAN 
	ELSE 	
		SELECT ucrsRebatimentosNiveisPontos
		GO Top
		SCAN
			lcSQL = ""
			TEXT TO lcSQL NOSHOW TEXTMERGE 
				INSERT INTO campanhas_pNiveis (
					id
					,campanhas_id
					,nivel
					,valor
					,pontos
				) VALUES(
					(select ISNULL(MAX(id),0) + 1 from campanhas_pNiveis)
					,<<ucrsCampanhas.id>>
					,<<ucrsRebatimentosNiveisPontos.nivel>>
					,<<ucrsRebatimentosNiveisPontos.valor>>
					,<<ucrsRebatimentosNiveisPontos.pontos>>
				)
			ENDTEXT
			lcExecuteSQL  = lcExecuteSQL + CHR(13) + CHR(13) + lcSQL
			
		ENDSCAN 
	ENDIF 
	
	
	lcExecuteSQL = uf_gerais_trataPlicasSQL(lcExecuteSQL) 	
	
	TEXT TO lcSQL NOSHOW TEXTMERGE
		exec up_gerais_execSql 'CAMPANHAS - Insert', 1,'<<lcExecuteSQL>>', '', '', '' , '', ''
	ENDTEXT 

*!*	_cliptext = lcSQL 
*!*	MESSAGEBOX(lcSQL )	
	
	If !uf_gerais_actGrelha("","",lcSQL)
		uf_perguntalt_chama("OCORREU UM ERRO A GRAVAR A CAMPANHA. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		Return .f.
	ENDIF

	myCampanhasIntroducao = .f.
	myCampanhasAlteracao = .f.
	
	&&uf_campanhas_actualizaCampanhasAtuais()
	uf_CAMPANHAS_chama(ucrsCampanhas.id)
	uf_CAMPANHAS_alternaMenu()
	
	uf_perguntalt_chama("Regra gravada com sucesso. � necess�rio processar as regras para que entrem em vigor.","OK","",16)
ENDFUNC 


**
FUNCTION uf_campanhas_PontosNovoNivel
	LOCAL lcNovoNivel 

	lcNovoNivel = 1
	SELECT ucrsRebatimentosNiveisPontos	
 	CALCULATE MAX(nivel) TO lcNovoNivel
 	
	SELECT ucrsRebatimentosNiveisPontos
	APPEND BLANK
	Replace ucrsRebatimentosNiveisPontos.nivel WITH lcNovoNivel +1

	Campanhas.PageFrame1.Page7.GridNiveis.refresh
ENDFUNC 


**
FUNCTION uf_campanhas_PontosApagaNivel
	
	SELECT ucrsRebatimentosNiveisPontos
	DELETE 
	TRY
		SELECT ucrsRebatimentosNiveisPontos
		SKIP -1
	ENDTRY
	Campanhas.PageFrame1.Page7.GridNiveis.refresh

ENDFUNC 


** funcao que as campanhas no utente
FUNCTION uf_campanhas_actualizaCampanhasNovoUtente
	LPARAMETERS lcNoNovoUt, lcEstabNovoUt
	
	&& N�o actualiza campanhas
	IF !uf_gerais_getParameter("ADM0000000264","BOOL")
		RETURN .f.
	ENDIF 
	
	** Informa��o Utente
	lcSQL = ''
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		SELECT no, estab FROM b_utentes (nolock) where no = <<lcNoNovoUt>> and estab = <<lcEstabNovoUt>> ORDER BY no, estab
	ENDTEXT 
	IF !uf_gerais_actGrelha("","uCrsCampanhasTodosUtentesOriginal",lcSql)
		uf_perguntalt_chama("Ocorreu um problema ao verificar defini��o das campanhas.","OK","",16)
		RETURN .f.
	ENDIF

	** Campanhas ativas na BD - alterado em 04-12-2017 por JC para filtrar campanhas loja
	lcSQL = ''
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_campanhas_listar '<<ALLTRIM(mysite)>>'
	ENDTEXT 	
	IF !uf_gerais_actGrelha("","uCrsCampanhasList",lcSql)
		uf_perguntalt_chama("Ocorreu um problema ao verificar as campanhas.","OK","",16)
		RETURN .f.
	ENDIF
		
	** Campanhas associadas a Utentes ??
	lcSQL = ''
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_campanhas_configUt ''
	ENDTEXT 	
	If !uf_gerais_actGrelha("","ucrsCampanhasConfigUt",lcSql)
		uf_perguntalt_chama("Ocorreu um problema ao verificar defini��o das campanhas.","OK","",16)
		RETURN .f.
	ENDIF
	
	** Insere Utente na tabela de Campanhas que n�o tenham sido inseridos
	lcSQL = ''
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		insert into campanhas_ut_lista (no,estab)	
		Select   
			b_utentes.no
			,b_utentes.estab
		from  
			b_utentes (nolock) 
			left join campanhas_ut_lista on b_utentes.no = campanhas_ut_lista.no and b_utentes.estab = campanhas_ut_lista.estab
		where 
			campanhas_ut_lista.no is null
	ENDTEXT 
	IF !uf_gerais_actGrelha("","",lcSql)
		uf_perguntalt_chama("Ocorreu um problema ao verificar defini��o das campanhas.","OK","",16)
		RETURN .f.
	ENDIF
		
	** Precorre todas as campanhas 
	SELECT uCrsCampanhasList
	GO TOP 
	SCAN 

		SELECT * FROM ucrsCampanhasTodosUtentesOriginal INTO CURSOR ucrsCampanhasTodosUtentes READWRITE 
		
		** Retira a Campanha
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			update 
				campanhas_ut_lista 
			set 
				campanhas = REPLACE(campanhas,'(<<ASTR(ucrsCampanhasList.id)>>)','') 
			where 
				no = <<lcNoNovoUt>>
				and estab = <<lcEstabNovoUt>>
				and campanhas like '%(<<ASTR(ucrsCampanhasList.id)>>)%'
		ENDTEXT 
		IF !uf_gerais_actGrelha("","",lcSql)
			uf_perguntalt_chama("Ocorreu um problema ao verificar defini��o das campanhas.","OK","",16)
			RETURN .f.
		ENDIF

		SELECT ucrsCampanhasConfigUt
		LOCATE FOR ucrsCampanhasList.id == ucrsCampanhasConfigUt.campanhas_id
		IF !FOUND() && Se n�o tiver nenhuma regra aplicada aplica 
		
			lcSQL = ''
			TEXT TO lcSQL NOSHOW TEXTMERGE 
				update campanhas_ut_lista set campanhas = campanhas + '(<<astr(ucrsCampanhasList.id)>>)' WHERE no = <<lcNoNovoUt>> AND estab = <<lcEstabNovoUt>>
			ENDTEXT 
			IF !uf_gerais_actGrelha("","",lcSQL)
				uf_perguntalt_chama("Ocorreu um problema ao verificar defini��o das campanhas.","OK","",16)
				RETURN .f.
			ENDIF
		ELSE	
			&& Lentid�o Aqui
			SELECT ucrsCampanhasConfigUt
			GO TOP 
			SCAN FOR ucrsCampanhasList.id == ucrsCampanhasConfigUt.campanhas_id
				
				lcExcecao 	= ucrsCampanhasConfigUt.excecao
				lcEstab 	= -1
				lcOperador 	= "="
				lcIdade		= -999999
				lcE			= '30000101'
							
				lcId 		= ALLTRIM(ucrsCampanhasConfigUt.utid)
				lcTop 		= 100000
				lcNome 		= ALLTRIM(ucrsCampanhasConfigUt.nome)
				**lcNo 		= ucrsCampanhasConfigUt.no
				**lcEstab		= ucrsCampanhasConfigUt.estab
				
				lcNo 		= uCrsCampanhasTodosUtentesOriginal.no
				lcEstab		= uCrsCampanhasTodosUtentesOriginal.estab				
				
				lcMorada 	= ALLTRIM(ucrsCampanhasConfigUt.morada)
				lcNcont		= ALLTRIM(ucrsCampanhasConfigUt.ncont)
				lcNCartao 	= ALLTRIM(ucrsCampanhasConfigUt.nCartao)
				lctlf 		= ALLTRIM(ucrsCampanhasConfigUt.tlf)
				lcutenteno	= ""
				lcnbenef	= ALLTRIM(ucrsCampanhasConfigUt.nbenef)
				lcTipo		= ALLTRIM(ucrsCampanhasConfigUt.tipo)
				lcProfissao	= ALLTRIM(ucrsCampanhasConfigUt.profissao)
				lcSexo		= ALLTRIM(ucrsCampanhasConfigUt.sexo)
				lcOperador	= ALLTRIM(ucrsCampanhasConfigUt.Operador)
				lcInactivo	= IIF(ucrsCampanhasConfigUt.inactivo, 1, 0)

				IF YEAR(ucrsCampanhasConfigUt.dtnasc) == 1900
					IF EMPTY(ucrsCampanhasConfigUt.idade)
						lcIdade	= -999999
						lcOperador = '>'
					ELSE
						lcIdade	= astr(ucrsCampanhasConfigUt.idade)
						lcNascimento = '19000101'
					ENDIF
				ELSE
					lcNascimento = uf_gerais_getDate(ucrsCampanhasConfigUt.dtnasc, "SQL")
					lcIdade	= -999999
					lcOperador = '>'
				ENDIF	

				lcMail	 	= IIF(ucrsCampanhasConfigUt.mail, 1, 0)
				lcTlmvl 	= IIF(ucrsCampanhasConfigUt.Tlmvl, 1, 0)
				lcObs		= ALLTRIM(ucrsCampanhasConfigUt.obs)
				lcEFR		= ALLTRIM(ucrsCampanhasConfigUt.entpla)
				lcPatologia	= ALLTRIM(ucrsCampanhasConfigUt.patologia)
				lcRef 		= ALLTRIM(ucrsCampanhasConfigUt.ref)
				lcEntre		= uf_gerais_getDate(ucrsCampanhasConfigUt.Entre, "SQL")
				lcE			= uf_gerais_getDate(ucrsCampanhasConfigUt.E, "SQL")
				lcValTouch  = 0
				lcValEntidadeClinica = 0

				&& tem de usar a SP para verificar se o utente est� dentro dos parametros que necessita para saber se aplica a campanha ou n�o no Utente;
				lcSQL = ''
				TEXT TO lcSQL TEXTMERGE NOSHOW	
   					exec up_campanhas_pesquisaClientes <<lcTop>>, '<<lcNome>>', <<lcNo>>, <<lcEstab>>, '<<lcMorada>>', '<<lcNcont>>'
						,'<<lcNCartao>>', '<<lcTipo>>', '<<lcProfissao>>'
						,'<<lcSexo>>', <<lcTlmvl>>, '<<lcOperador>>', <<lcIdade>>, '<<lcObs>>', <<lcMail>>, '<<lcEFR>>'
						,'<<lcPatologia>>', '<<lcRef>>', '<<lcEntre>>', '<<lcE>>', '<<lcNascimento>>', <<lcInactivo>>
						,'<<lctlf>>', '<<lcutenteno>>', '<<lcnbenef>>', <<lcValTouch>>, <<lcValEntidadeClinica>>, '<<lcId>>'
				ENDTEXT
				
				IF !uf_gerais_actGrelha("","UcrsTempUtCamp",lcSql)
					uf_perguntalt_chama("Ocorreu um problema ao verificar defini��o das campanhas.","OK","",16)
					RETURN .f.
				ENDIF

				IF EMPTY(lcExcecao) && Regra
					
					&& Redundante - n�o alterei pq depois tenho de rever isto
					SELECT ucrsCampanhasTodosUtentes.no, ucrsCampanhasTodosUtentes.estab;
					FROM;
						ucrsCampanhasTodosUtentes;
						left join UcrsTempUtCamp on ucrsCampanhasTodosUtentes.no == UcrsTempUtCamp.no AND ucrsCampanhasTodosUtentes.estab == UcrsTempUtCamp.estab;
					Where;
						UcrsTempUtCamp.no is null INTO CURSOR ucrsTempDeleteUt READWRITE 
					**
					
					SELECT ucrsTempDeleteUt 	
					GO TOP
					SCAN 
						SELECT ucrsCampanhasTodosUtentes
						GO TOP
						SELECT ucrsCampanhasTodosUtentes
						LOCATE FOR ucrsCampanhasTodosUtentes.no == ucrsTempDeleteUt.no AND ucrsCampanhasTodosUtentes.estab == ucrsTempDeleteUt.estab
						IF FOUND()
							SELECT ucrsCampanhasTodosUtentes
							DELETE
						ENDIF						
					ENDSCAN
					
					IF USED("ucrsTempDeleteUt")
						fecha("ucrsTempDeleteUt")
					ENDIF 	
									
				ELSE && Excecao
								
					SELECT ucrsCampanhasTodosUtentes.no, ucrsCampanhasTodosUtentes.estab;
					FROM;
						ucrsCampanhasTodosUtentes;
						left join UcrsTempUtCamp on ucrsCampanhasTodosUtentes.no == UcrsTempUtCamp.no AND ucrsCampanhasTodosUtentes.estab == UcrsTempUtCamp.estab;
					Where; 
						UcrsTempUtCamp.no is not null INTO CURSOR ucrsTempDeleteUt READWRITE 
					
					SELECT ucrsTempDeleteUt 	
					GO TOP
					SCAN 
						SELECT ucrsCampanhasTodosUtentes
						GO TOP
						SELECT ucrsCampanhasTodosUtentes
						LOCATE FOR ucrsCampanhasTodosUtentes.no == ucrsTempDeleteUt.no AND ucrsCampanhasTodosUtentes.estab == ucrsTempDeleteUt.estab
						IF FOUND()
							SELECT ucrsCampanhasTodosUtentes
							DELETE
						ENDIF						
					ENDSCAN
					
					IF USED("ucrsTempDeleteUt")
						fecha("ucrsTempDeleteUt")
					ENDIF 	
					
				ENDIF 			

				IF USED("UcrsTempUtCamp")
					fecha("UcrsTempUtCamp")
				ENDIF 
			
			ENDSCAN
			
			** atualiza as campanhas no Utente
			SELECT ucrsCampanhasTodosUtentes
			GO TOP 
			LOCATE FOR ucrsCampanhasTodosUtentes.no == lcNoNovoUt AND ucrsCampanhasTodosUtentes.estab == lcEstabNovoUt
			IF FOUND()

				TEXT TO lcSQL NOSHOW TEXTMERGE 
					update campanhas_ut_lista set campanhas = campanhas + '(<<astr(ucrsCampanhasList.id)>>)' WHERE no = <<lcNoNovoUt>> AND estab = <<lcEstabNovoUt>>
				ENDTEXT 

				IF !uf_gerais_actGrelha("","",lcSQL)
					uf_perguntalt_chama("Ocorreu um problema ao verificar defini��o das campanhas.","OK","",16)
					RETURN .f.
				ENDIF
			ENDIF

		ENDIF
	ENDSCAN 

	IF USED("ucrsCampanhasList")
		fecha("ucrsCampanhasList")
	ENDIF
	IF USED("ucrsCampanhasConfigUt")
		fecha("ucrsCampanhasConfigUt")
	ENDIF
	IF USED("ucrsCampanhasTodosUtentes")
		fecha("ucrsCampanhasTodosUtentes")
	ENDIF 
	IF USED("ucrsCampanhasTodosUtentesOriginal")
		fecha("ucrsCampanhasTodosUtentesOriginal")
	ENDIF 

	** Tiver o Atendimento aberto carrega defini��es
	IF !(type("atendimento") == "U")
		uf_atendimento_CarregaCampanhasUt()
	ENDIF
	
ENDFUNC


** 
FUNCTION uf_campanhas_actualizaCampanhasProduto
	LPARAMETERS lcRefVerificar
	LOCAL lcExecuteSQL
	lcExecuteSQL = ""

	&& N�o actualiza campanhas
	IF !uf_gerais_getParameter("ADM0000000263","BOOL")
		RETURN .f.
	ENDIF 

	** alterado porque s� estava a atualizar as campnhas na loja atual 20160321
	**TEXT TO lcSql NOSHOW TEXTMERGE 
		**exec up_campanhas_listar '<<ALLTRIM(mysite)>>'
	**ENDTEXT 	
	TEXT TO lcSql NOSHOW TEXTMERGE 
		exec up_campanhas_listar ''
	ENDTEXT 	
	
	If !uf_gerais_actGrelha("","ucrsCampanhasList",lcSql)
		uf_perguntalt_chama("Ocorreu um problema ao verificar as campanhas.","OK","",16)
		RETURN .f.
	ENDIF
	
	IF RECCOUNT("ucrsCampanhasList") == 0
		RETURN .f.
	ENDIF 

	regua(0,IIF(EMPTY(RECCOUNT("ucrsCampanhasList")),3,RECCOUNT("ucrsCampanhasList")),"A atualizar campanhas do produto..." + lcRefVerificar)
	
	** exec up_campanhas_configSt '<<ALLTRIM(mysite)>>'
	TEXT TO lcSql NOSHOW TEXTMERGE 
		exec up_campanhas_configSt ''
	ENDTEXT 	
	If !uf_gerais_actGrelha("","ucrsCampanhasConfigSt",lcSql)
		uf_perguntalt_chama("Ocorreu um problema ao verificar defini��o das campanhas.","OK","",16)
		RETURN .f.
	ENDIF
	
	&& Cursor de Oferta de Produtos - exec up_campanhas_ProdutosOferta '<<ALLTRIM(mysite)>>'
	TEXT TO lcSql NOSHOW TEXTMERGE 
		exec up_campanhas_ProdutosOferta ''
	ENDTEXT 	
	If !uf_gerais_actGrelha("","ucrsCampanhasProdutosOferta",lcSql)
		uf_perguntalt_chama("Ocorreu um problema ao verificar defini��o das campanhas.","OK","",16)
		RETURN .f.
	ENDIF

	&& Cursor de Todos os Produtos, que vai ser filtrado
*!*		lcSQL = ''
*!*		TEXT TO lcSQL NOSHOW TEXTMERGE 
*!*			exec up_stocks_pesquisa 1000000,'<<ALLTRIM(lcRefVerificar)>>','','','','',0,'maior',-999999,-1,0,1,0,<<mysite_nr>>,'',1,0,1,0,-1, '', '', '', ''	
*!*		ENDTEXT 
**		exec up_stocks_pesquisa 1000000,'<<ALLTRIM(lcRefVerificar)>>','','','','',0,'maior',-999999,-1,0,<<myArmazem>>,0,1,'',<<mysite_nr>>,0,1,0,-1, '', '', '', ''	
	
	lcSQL = ''
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_stocks_pesquisa 1000000,'<<ALLTRIM(lcRefVerificar)>>','','','','',0,'maior',-999999,-1,0,<<myArmazem>>,0,1,'',<<mysite_nr>>,0,1,0,-1, '', '', '', ''	
	ENDTEXT 
	
	
	IF !uf_gerais_actGrelha("","ucrsCampanhasTodosProdutosOriginal",lcSql)
		uf_perguntalt_chama("Ocorreu um problema ao verificar defini��o das campanhas.","OK","",16)
		RETURN .f.
	ENDIF
	
	&& introduz ref na tabela que relaciona refs com campanhas
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		IF NOT exists (select ref from campanhas_st_lista where ref = '<<ALLTRIM(lcRefVerificar)>>')
		BEGIN
			insert into campanhas_st_lista (ref) VALUES ('<<ALLTRIM(lcRefVerificar)>>')
		END
		
		IF NOT exists (select ref from campanhas_pOfertas_lista where ref = '<<ALLTRIM(lcRefVerificar)>>')
		BEGIN
			insert into campanhas_pOfertas_lista (ref) VALUES ('<<ALLTRIM(lcRefVerificar)>>')
		END
		
		update campanhas_st_lista set campanhas = '' WHERE ref = '<<ALLTRIM(lcRefVerificar)>>'
		update campanhas_pOfertas_lista set campanhas = '' WHERE ref = '<<ALLTRIM(lcRefVerificar)>>'
	ENDTEXT 
	
	lcExecuteSQL  = lcExecuteSQL + CHR(13) + CHR(13) + lcSQL
	

	regua(1,1,"A atualizar campanhas do produto..." + lcRefVerificar)

	SELECT ucrsCampanhasList
	GO TOP
	SCAN
	
		regua(1,RECNO(),"A atualizar campanha: " + ASTR(ucrsCampanhasList.id))	
		
		IF used("ucrsCampanhasTodosProdutos")
			fecha("ucrsCampanhasTodosProdutos")
		ENDIF 
		SELECT .f. as apagar,* FROM ucrsCampanhasTodosProdutosOriginal INTO CURSOR ucrsCampanhasTodosProdutos READWRITE 
		IF used("ucrsCampanhasTodosProdutosOferta")
			fecha("ucrsCampanhasTodosProdutosOferta ")
		ENDIF 
		SELECT .f. as apagar,* FROM ucrsCampanhasTodosProdutosOriginal INTO CURSOR ucrsCampanhasTodosProdutosOferta READWRITE
		
		SELECT ucrsCampanhasConfigSt
		LOCATE FOR ucrsCampanhasList.id == ucrsCampanhasConfigSt.campanhas_id 
		IF !FOUND() && Se n�o tiver nenhuma regra aplicada aplica 
			
			TEXT TO lcSQL NOSHOW TEXTMERGE 
				update campanhas_st_lista set campanhas = campanhas + '(<<astr(ucrsCampanhasList.id)>>)' WHERE ref = '<<ALLTRIM(lcRefVerificar)>>'
			ENDTEXT 
			lcExecuteSQL  = lcExecuteSQL + CHR(13) + CHR(13) + lcSQL

		ELSE			
			
			SELECT * FROM ucrsCampanhasTodosProdutos INTO CURSOR ucrsCampanhasSTFiltrar READWRITE 
			
			SELECT ucrsCampanhasConfigSt
			GO Top
			SCAN FOR ucrsCampanhasList.id == ucrsCampanhasConfigSt.campanhas_id

				lcExcecao = ucrsCampanhasConfigSt.excecao
				lcTop = 10000
				lcRef = ALLTRIM(ucrsCampanhasConfigSt.artigo)
				lcMarca = ALLTRIM(ucrsCampanhasConfigSt.marca)
				lctxIva = ASTR(ucrsCampanhasConfigSt.txIva)
				lcFamilia 	= ALLTRIM(ucrsCampanhasConfigSt.familia)
				lcLab = ALLTRIM(ucrsCampanhasConfigSt.lab)
				lcDci = ALLTRIM(ucrsCampanhasConfigSt.dci)
				lcGenerico	= ucrsCampanhasConfigSt.generico
				lcOperadorLogico	= IIF(ALLTRIM(ucrsCampanhasConfigSt.OperadorLogico)==">", "maior", IIF(ALLTRIM(ucrsCampanhasConfigSt.OperadorLogico)=="<", "menor", "igual"))
				lcStock = ucrsCampanhasConfigSt.stock
				lcServicos = ucrsCampanhasConfigSt.servicos
				lcInactivos	= IIF(ucrsCampanhasConfigSt.inactivo, 1, 0)
				lcParafarmacia 	= IIF(ucrsCampanhasConfigSt.parafarmacia, 1,0)
				lcComFicha		= IIF(ucrsCampanhasConfigSt.comficha, 1, 0)
				lcT5			= IIF(ucrsCampanhasConfigSt.t5, 1, 0)
				lcRecurso = ""
				lcTipo = ALLTRIM(ucrsCampanhasConfigSt.tipo)

				** Trata Acentos
				lcRef = CHRTRAN(lcRef, "�������������������", "aouaaaeeeiiiooouuuc")
				lcMarca = CHRTRAN(lcMarca, "�������������������", "aouaaaeeeiiiooouuuc")
				lcFamilia = CHRTRAN(lcFamilia, "�������������������", "aouaaaeeeiiiooouuuc")
				lcLab = CHRTRAN(lcLab, "�������������������", "aouaaaeeeiiiooouuuc")
				lcDci = CHRTRAN(lcDci, "�������������������", "aouaaaeeeiiiooouuuc")

				&& Atribui novamente todos os produtos para filtrar
				SELECT * FROM ucrsCampanhasTodosProdutosOriginal INTO CURSOR ucrsCampanhasSTFiltrar READWRITE 
				
				LOCAL lcTipoValidacao
				IF EMPTY(lcExcecao)
					lcTipoValidacao=0
				ELSE
					lcTipoValidacao=1
				ENDIF 
				
				lcCursorConfigUtValidaSt = "ucrsCampanhasSTFiltrar"	
				

				uf_campanhas_filtraProdutos(lcCursorConfigUtValidaSt, lcTop, lcRef, lcMarca, lctxIva, lcFamilia, lcLab, lcDci, lcGenerico, lcOperadorLogico, lcStock, lcServicos, lcInactivos, lcParafarmacia, lcComFicha, lcT5, lcRecurso, lcTipoValidacao, lcTipo)
				
				IF EMPTY(lcExcecao) && N�o � excecao

*!*						delete ucrsCampanhasTodosProdutos;
*!*						FROM;
*!*							ucrsCampanhasTodosProdutos;
*!*							LEFT JOIN &lcCursorConfigUtValidaSt ON ALLTRIM(&lcCursorConfigUtValidaSt..ref) == ALLTRIM(ucrsCampanhasTodosProdutos.ref);
*!*						WHERE;
*!*							&lcCursorConfigUtValidaSt..ref is null 

					SELECT ucrsCampanhasTodosProdutos.ref;
					FROM;
						ucrsCampanhasTodosProdutos;
						LEFT JOIN &lcCursorConfigUtValidaSt ON ALLTRIM(&lcCursorConfigUtValidaSt..ref) == ALLTRIM(ucrsCampanhasTodosProdutos.ref);
					WHERE;
						&lcCursorConfigUtValidaSt..ref is null INTO CURSOR ucrsTempDeleteSt READWRITE 
						
					
					SELECT ucrsTempDeleteSt	
					GO TOP
					SCAN 
						SELECT ucrsCampanhasTodosProdutos
						GO TOP
						SELECT ucrsCampanhasTodosProdutos
						LOCATE FOR ALLTRIM(ucrsCampanhasTodosProdutos.ref) == ALLTRIM(ucrsTempDeleteSt.ref)
						IF FOUND()
							SELECT ucrsCampanhasTodosProdutos
							DELETE
						ENDIF						
					ENDSCAN
					
					IF USED("ucrsTempDeleteSt")
						fecha("ucrsTempDeleteSt")
					ENDIF 	

				ELSE && Excecao, todos os produtos excepto os devolvidos
*!*						
*!*						DELETE ucrsCampanhasTodosProdutos;
*!*						FROM;
*!*							ucrsCampanhasTodosProdutos;
*!*							LEFT JOIN &lcCursorConfigUtValidaSt ON ALLTRIM(&lcCursorConfigUtValidaSt..ref) == ALLTRIM(ucrsCampanhasTodosProdutos.ref);
*!*						WHERE;
*!*							&lcCursorConfigUtValidaSt..ref is not null
*!*							
					
					SELECT ucrsCampanhasTodosProdutos.ref;
					FROM;
						ucrsCampanhasTodosProdutos;
						LEFT JOIN &lcCursorConfigUtValidaSt ON ALLTRIM(&lcCursorConfigUtValidaSt..ref) == ALLTRIM(ucrsCampanhasTodosProdutos.ref);
					WHERE;
						&lcCursorConfigUtValidaSt..ref is not null INTO CURSOR ucrsTempDeleteSt READWRITE 
						
					
					SELECT ucrsTempDeleteSt	
					GO TOP
					SCAN 
						SELECT ucrsCampanhasTodosProdutos
						GO TOP
						SELECT ucrsCampanhasTodosProdutos
						LOCATE FOR ALLTRIM(ucrsCampanhasTodosProdutos.ref) == ALLTRIM(ucrsTempDeleteSt.ref)
						IF FOUND()
							SELECT ucrsCampanhasTodosProdutos
							DELETE
						ENDIF						
					ENDSCAN
					
					IF USED("ucrsTempDeleteSt")
						fecha("ucrsTempDeleteSt")
					ENDIF 	
				ENDIF

			ENDSCAN

			IF USED(lcCursorConfigUtValidaSt)
				fecha(lcCursorConfigUtValidaSt)
			ENDIF
				
			lcSQLFinal = ""
			lcContador = 0

			SELECT ucrsCampanhasTodosProdutos
			GO TOP 
			LOCATE FOR ALLTRIM(ucrsCampanhasTodosProdutos.ref) == ALLTRIM(lcRefVerificar)
			IF FOUND()
				TEXT TO lcSQL NOSHOW TEXTMERGE 
					update campanhas_st_lista set campanhas = campanhas + '(<<astr(ucrsCampanhasList.id)>>)' WHERE ref = '<<ALLTRIM(lcRefVerificar)>>'
				ENDTEXT 
				lcExecuteSQL  = lcExecuteSQL + CHR(13) + CHR(13) + lcSQL
			ENDIF
		
		ENDIF 

		** Produto de Oferta
		SELECT ucrsCampanhasProdutosOferta
		LOCATE FOR ucrsCampanhasList.id == ucrsCampanhasProdutosOferta.campanhas_id 
		IF FOUND()
			
			SELECT ucrsCampanhasProdutosOferta
			GO Top
			SCAN FOR ucrsCampanhasList.id == ucrsCampanhasProdutosOferta.campanhas_id

				lcExcecao = ucrsCampanhasProdutosOferta.excecao
				lcTop = 10000
				lcRef = ALLTRIM(ucrsCampanhasProdutosOferta.artigo)
				lcMarca = ALLTRIM(ucrsCampanhasProdutosOferta.marca)
				lctxIva = ASTR(ucrsCampanhasProdutosOferta.txIva)
				lcFamilia 	= ALLTRIM(ucrsCampanhasProdutosOferta.familia)
				lcLab = ALLTRIM(ucrsCampanhasProdutosOferta.lab)
				lcDci = ALLTRIM(ucrsCampanhasProdutosOferta.dci)
				lcGenerico	= ucrsCampanhasProdutosOferta.generico
				lcOperadorLogico	= IIF(ALLTRIM(ucrsCampanhasProdutosOferta.OperadorLogico)==">", "maior", IIF(ALLTRIM(ucrsCampanhasProdutosOferta.OperadorLogico)=="<", "menor", "igual"))
				lcStock = ucrsCampanhasProdutosOferta.stock
				lcServicos = ucrsCampanhasProdutosOferta.servicos
				lcInactivos	= IIF(ucrsCampanhasProdutosOferta.inactivo, 1, 0)
				lcParafarmacia 	= IIF(ucrsCampanhasProdutosOferta.parafarmacia, 1,0)
				lcComFicha		= IIF(ucrsCampanhasProdutosOferta.comficha, 1, 0)
				lcT5			= IIF(ucrsCampanhasProdutosOferta.t5, 1, 0)
				lcRecurso = ""
			
				** Trata Acentos
				lcRef = CHRTRAN(lcRef, "�������������������", "aouaaaeeeiiiooouuuc")
				lcMarca = CHRTRAN(lcMarca, "�������������������", "aouaaaeeeiiiooouuuc")
				lcFamilia = CHRTRAN(lcFamilia, "�������������������", "aouaaaeeeiiiooouuuc")
				lcLab = CHRTRAN(lcLab, "�������������������", "aouaaaeeeiiiooouuuc")
				lcDci = CHRTRAN(lcDci, "�������������������", "aouaaaeeeiiiooouuuc")

				&& Atribui nova mente todos os produtos para filtrar
				lcCursorProdOfertaValidaSt = "ucrsCampanhasProdOfertaValidaSt"
				SELECT * FROM ucrsCampanhasTodosProdutosOriginal INTO CURSOR &lcCursorProdOfertaValidaSt READWRITE 
				LOCAL lcTipoValidacao
				IF EMPTY(lcExcecao)
					lcTipoValidacao=0
				ELSE
					lcTipoValidacao=1
				ENDIF 
					
				uf_campanhas_filtraProdutos(lcCursorProdOfertaValidaSt, lcTop, lcRef, lcMarca, lctxIva, lcFamilia, lcLab, lcDci, lcGenerico, lcOperadorLogico, lcStock, lcServicos, lcInactivos, lcParafarmacia, lcComFicha, lcT5, lcRecurso, lcTipoValidacao, '')

				IF EMPTY(lcExcecao) && N�o � excecao
			
*!*						delete ucrsCampanhasTodosProdutosOferta;
*!*						FROM;
*!*							ucrsCampanhasTodosProdutosOferta;
*!*							LEFT JOIN &lcCursorProdOfertaValidaSt ON ALLTRIM(&lcCursorProdOfertaValidaSt..ref) == ALLTRIM(ucrsCampanhasTodosProdutosOferta.ref);
*!*						WHERE;
*!*							&lcCursorProdOfertaValidaSt..ref is null 
*!*			
					SELECT ucrsCampanhasTodosProdutosOferta.ref;
					FROM;
						ucrsCampanhasTodosProdutosOferta;
						LEFT JOIN &lcCursorProdOfertaValidaSt ON ALLTRIM(&lcCursorProdOfertaValidaSt..ref) == ALLTRIM(ucrsCampanhasTodosProdutosOferta.ref);
					WHERE;
						&lcCursorProdOfertaValidaSt..ref is null INTO CURSOR ucrsTempDeleteSt READWRITE 
					
					SELECT ucrsTempDeleteSt	
					GO TOP
					SCAN 
						SELECT ucrsCampanhasTodosProdutosOferta
						GO TOP
						SELECT ucrsCampanhasTodosProdutosOferta
						LOCATE FOR ALLTRIM(ucrsCampanhasTodosProdutosOferta.ref) == ALLTRIM(ucrsTempDeleteSt.ref)
						IF FOUND()
							SELECT ucrsCampanhasTodosProdutosOferta
							DELETE
						ENDIF						
					ENDSCAN
					
					IF USED("ucrsTempDeleteSt")
						fecha("ucrsTempDeleteSt")
					ENDIF 	
		
				ELSE && Excecao, todos os produtos excepto os devolvidos
					
*!*						DELETE ucrsCampanhasTodosProdutosOferta;
*!*						FROM;
*!*							ucrsCampanhasTodosProdutosOferta;
*!*							LEFT JOIN &lcCursorProdOfertaValidaSt ON ALLTRIM(&lcCursorProdOfertaValidaSt..ref) == ALLTRIM(ucrsCampanhasTodosProdutosOferta.ref);
*!*						WHERE;
*!*							&lcCursorProdOfertaValidaSt..ref is not null
						
					SELECT ucrsCampanhasTodosProdutosOferta.ref;
					FROM;
						ucrsCampanhasTodosProdutosOferta;
						LEFT JOIN &lcCursorProdOfertaValidaSt ON ALLTRIM(&lcCursorProdOfertaValidaSt..ref) == ALLTRIM(ucrsCampanhasTodosProdutosOferta.ref);
					WHERE;
						&lcCursorProdOfertaValidaSt..ref is not null INTO CURSOR ucrsTempDeleteSt READWRITE 
						
					
					SELECT ucrsTempDeleteSt	
					GO TOP
					SCAN 
						SELECT ucrsCampanhasTodosProdutosOferta
						GO TOP
						SELECT ucrsCampanhasTodosProdutosOferta
						LOCATE FOR ALLTRIM(ucrsCampanhasTodosProdutosOferta.ref) == ALLTRIM(ucrsTempDeleteSt.ref)
						IF FOUND()
							SELECT ucrsCampanhasTodosProdutosOferta
							DELETE
						ENDIF						
					ENDSCAN
					
					IF USED("ucrsTempDeleteSt")
						fecha("ucrsTempDeleteSt")
					ENDIF 	
				ENDIF
				
			ENDSCAN 
			
			IF USED(lcCursorProdOfertaValidaSt)
				fecha(lcCursorProdOfertaValidaSt)
			ENDIF
			
			SELECT ucrsCampanhasTodosProdutosOferta
			GO TOP 
			LOCATE FOR ALLTRIM(ucrsCampanhasTodosProdutosOferta.ref) == ALLTRIM(lcRefVerificar)
			IF FOUND()
				TEXT TO lcSQL NOSHOW TEXTMERGE 
					update campanhas_pOfertas_lista set campanhas = campanhas + '(<<astr(ucrsCampanhasList.id)>>)' WHERE ref = '<<ALLTRIM(lcRefVerificar)>>'
				ENDTEXT 
				lcExecuteSQL  = lcExecuteSQL + CHR(13) + CHR(13) + lcSQL
			ENDIF
			IF USED("ucrsRefsCamp")
				fecha("ucrsRefsCamp")
			ENDIF 
			
		ENDIF 	
	ENDSCAN 

	IF USED("ucrsCampanhasTodosProdutosOriginal")
		fecha("ucrsCampanhasTodosProdutosOriginal")
	ENDIF 
	IF USED("ucrsCampanhasTodosProdutos")
		fecha("ucrsCampanhasTodosProdutos")
	ENDIF 
	IF USED("ucrsCampanhasTodosProdutosOferta")	
		fecha("ucrsCampanhasTodosProdutosOferta")
	ENDIF 

	IF !EMPTY(lcExecuteSQL)
		IF !uf_gerais_actGrelha("","",lcExecuteSQL)
			uf_perguntalt_chama("Ocorreu um problema ao verificar defini��o das campanhas.","OK","",16)
			regua(2)
			RETURN .f.
		ENDIF
	ENDIF

	regua(2)
ENDFUNC 


**
FUNCTION uf_campanhas_actualizaCampanhaAtual
	LOCAL lcCampanhaID
	
	**SELECT ucrsCampanhas	
	**lcCampanhaID = ucrsCampanhas.id
	
	**
	**IF EMPTY(lcCampanhaID)
		uf_campanhas_actualizaCampanhasAtuais()
	**ELSE
	**	uf_campanhas_actualizaCampanhasAtuais(lcCampanhaID)	
	**ENDIF 

ENDFUNC 



**
FUNCTION uf_campanhas_actualizaCampanhasAtuais
	LPARAMETERS lnIDCampanhaIndividual
	LOCAL lcIDCampanhaIndividual 
	
	IF EMPTY(lnIDCampanhaIndividual)
		lcIDCampanhaIndividual = 0
	ELSE
		lcIDCampanhaIndividual = lnIDCampanhaIndividual
	ENDIF 	
	
	IF EMPTY(lcIDCampanhaIndividual)
		**If !uf_perguntalt_chama("Vai aplicar as defini��es de todas as campanhas." + CHR(13) + CHR(13) +"Esta opera��o pode ser demorada, pondere a op��o de processamento individual de campanhas." + CHR(13) + CHR(13) + "� necess�rio reiniciar a aplica��o de forma que as altera��es sejam refletidas. Pretende continuar?","Sim","N�o")	
		If !uf_perguntalt_chama("Vai aplicar as defini��es das campanhas." + CHR(13) + "Pretende continuar?" + CHR(13) + CHR(13) +"Reinicie por favor o software em todos os postos ap�s o processamento para que as altera��es sejam refletidas.","Sim","N�o")	
			RETURN .f.
		ENDIF 	
	ELSE
		If !uf_perguntalt_chama("Vai aplicar as defini��es desta campanha. Pretende continuar?","Sim","N�o")	
			RETURN .f.
		ENDIF
	ENDIF
	
	** Inativar campanhas que ultrapassaram data fim
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		update campanhas set inativo=1 where inativo=0 and datafim < dateadd(HOUR, <<difhoraria>>, getdate())-1
	ENDTEXT 	
	If !uf_gerais_actGrelha("","",lcSql)
		uf_perguntalt_chama("Ocorreu uma anomalia ao atualizar as campanhas que ultrapassaram a data de fim.","OK","",16)
		RETURN .f.
	ENDIF


	** 
	lcSQL = ''
	**exec up_campanhas_listar '<<ALLTRIM(mysite)>>'
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_campanhas_listar ''
	ENDTEXT 	
	If !uf_gerais_actGrelha("","ucrsCampanhasList",lcSql)
		uf_perguntalt_chama("Ocorreu uma anomalia ao verificar as campanhas.","OK","",16)
		RETURN .f.
	ENDIF
	
	regua(0,RECCOUNT("ucrsCampanhasList")+4,"A carregar campanhas...")
		
	** 
	lcSQL = ''
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_campanhas_configUt ''
	ENDTEXT 	
	If !uf_gerais_actGrelha("","ucrsCampanhasConfigUt",lcSql)
		uf_perguntalt_chama("Ocorreu uma anomalia ao verificar defini��o das campanhas.","OK","",16)
		RETURN .f.
	ENDIF

	regua(1,1,"A carregar configura��es utentes")

	** 
	lcSQL = ''
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_campanhas_configSt ''
	ENDTEXT 	
	If !uf_gerais_actGrelha("","ucrsCampanhasConfigSt",lcSql)
		uf_perguntalt_chama("Ocorreu uma anomalia ao verificar defini��o das campanhas.","OK","",16)
		RETURN .f.
	ENDIF
	
	regua(1,2,"A carregar configura��es produtos")
	
	** Cursor de Oferta de Produtos
	lcSQL = ''
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_campanhas_ProdutosOferta ''
	ENDTEXT 	
	If !uf_gerais_actGrelha("","ucrsCampanhasProdutosOferta",lcSql)
		uf_perguntalt_chama("Ocorreu uma anomalia ao verificar defini��o das campanhas.","OK","",16)
		RETURN .f.
	ENDIF
	
	regua(1,3,"A carregar campanhas... Defini��es gerais")
	
	lcSQL = ''
	
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_stocks_pesquisa 1000000,'','','','','',0,'maior' ,-999999, -1, 0,<<myArmazem>>,0, 1,'', <<mysite_nr>>, 0, 1, 0, -1, '', '', '', ''
	ENDTEXT 
	
	IF !uf_gerais_actGrelha("","ucrsCampanhasTodosProdutosOriginal",lcSql)
		uf_perguntalt_chama("Ocorreu uma anomalia ao verificar defini��o das campanhas.","OK","",16)
		RETURN .f.
	ENDIF
		
	lcSQL = ''	
	TEXT TO lcSQL NOSHOW TEXTMERGE 
	
		insert into campanhas_st_lista (ref)
		Select 
			distinct st.ref 
		from 
			st (nolock)
			left join campanhas_st_lista (nolock) on st.ref = campanhas_st_lista.ref
		where 
			campanhas_st_lista.ref is null
	
		insert into campanhas_pOfertas_lista(ref)
		Select 
			distinct st.ref 
		from 
			st (nolock)
			left join campanhas_pOfertas_lista (nolock) on st.ref = campanhas_pOfertas_lista.ref
		where 
			campanhas_pOfertas_lista.ref is null
	
	
		insert into campanhas_ut_lista (no,estab)	
		Select 
			b_utentes.no
			,b_utentes.estab
		from 
			b_utentes (nolock) 
			left join campanhas_ut_lista (nolock) on b_utentes.no = campanhas_ut_lista.no and b_utentes.estab = campanhas_ut_lista.estab
		where
			campanhas_ut_lista.no is null
		ORDER BY 
			b_utentes.no
	
		IF 0 = <<IIF(EMPTY(lcIDCampanhaIndividual),0,lcIDCampanhaIndividual)>>
		BEGIN
			update campanhas_st_lista set campanhas = ''
			update campanhas_ut_lista set campanhas = ''
			update campanhas_pOfertas_lista set campanhas = ''
		END
		ELSE
		BEGIN
			update campanhas_st_lista set campanhas = REPLACE(campanhas,'(<<lcIDCampanhaIndividual>>)','')
			update campanhas_ut_lista set campanhas = REPLACE(campanhas,'(<<lcIDCampanhaIndividual>>)','')
			update campanhas_pOfertas_lista set campanhas = REPLACE(campanhas,'(<<lcIDCampanhaIndividual>>)','')
		END
	ENDTEXT 

	IF !uf_gerais_actGrelha("","",lcSql)
		uf_perguntalt_chama("Ocorreu uma anomalia ao verificar defini��o das campanhas.","OK","",16)
		RETURN .f.
	ENDIF

	lcSQL = ''
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		SELECT no, estab FROM b_utentes (nolock) ORDER BY no, estab
	ENDTEXT 
	IF !uf_gerais_actGrelha("","ucrsCampanhasTodosUtentesOriginal",lcSql)
		uf_perguntalt_chama("Ocorreu um problema ao verificar defini��o das campanhas.","OK","",16)
		RETURN .f.
	ENDIF

	** Precorre todas as campanhas
	SELECT uCrsCampanhasList
	GO TOP 
	SCAN 
	
		IF EMPTY(lcIDCampanhaIndividual) OR ucrsCampanhasList.id == lcIDCampanhaIndividual
			regua(1,RECNO()+4,"A carregar configura��es campanha: "+ ASTR(ucrsCampanhasList.id))
			
			SELECT * FROM ucrsCampanhasTodosUtentesOriginal INTO CURSOR ucrsCampanhasTodosUtentes READWRITE 
			
			IF used("ucrsCampanhasTodosProdutos")
				fecha("ucrsCampanhasTodosProdutos")
			ENDIF 
			
			SELECT .f. as apagar,* FROM ucrsCampanhasTodosProdutosOriginal INTO CURSOR ucrsCampanhasTodosProdutos READWRITE 
			
			IF used("ucrsCampanhasTodosProdutosOferta")
				fecha("ucrsCampanhasTodosProdutosOferta ")
			ENDIF 
			
			SELECT .f. as apagar,* FROM ucrsCampanhasTodosProdutosOriginal INTO CURSOR ucrsCampanhasTodosProdutosOferta READWRITE

			SELECT uCrsCampanhasConfigUt
			LOCATE FOR uCrsCampanhasList.id == uCrsCampanhasConfigUt.campanhas_id
			IF !FOUND() && Se n�o tiver nenhuma regra aplicada aplica 
				
				&& insere Lista de Utentes na Tabela campanhas_ut_lista
				TEXT TO lcSQL NOSHOW TEXTMERGE 
					exec up_campanhas_insereListaUt <<ucrsCampanhasList.id>>
				ENDTEXT 
				If !uf_gerais_actGrelha("","",lcSql)
					uf_perguntalt_chama("Ocorreu uma anomalia ao atualizar lista de utentes da campanha. Contacte o suporte.","OK","",16)
					RETURN .f.
				ENDIF
				
			ELSE				
				SELECT ucrsCampanhasConfigUt
				GO TOP 
				SCAN FOR ucrsCampanhasList.id == ucrsCampanhasConfigUt.campanhas_id
					
					lcExcecao = ucrsCampanhasConfigUt.excecao
					lcEstab 		= -1
					lcOperador 		= "="
					lcIdade			= -999999
					lcE				= '30000101'
								
					lcId = ALLTRIM(ucrsCampanhasConfigUt.utid)
					lcTop = 100000
					lcNome = ALLTRIM(ucrsCampanhasConfigUt.nome)
					lcNo = ucrsCampanhasConfigUt.no
					lcEstab	= ucrsCampanhasConfigUt.estab
					lcMorada = ALLTRIM(ucrsCampanhasConfigUt.morada)
					lcNcont	= ALLTRIM(ucrsCampanhasConfigUt.ncont)
					lcNCartao = ALLTRIM(ucrsCampanhasConfigUt.nCartao)
					lctlf = ALLTRIM(ucrsCampanhasConfigUt.tlf)
					lcutenteno	= ""
					lcnbenef	= ALLTRIM(ucrsCampanhasConfigUt.nbenef)
					lcTipo		= ALLTRIM(ucrsCampanhasConfigUt.tipo)
					lcProfissao	= ALLTRIM(ucrsCampanhasConfigUt.profissao)
					lcSexo		= ALLTRIM(ucrsCampanhasConfigUt.sexo)
					lcOperador	= ALLTRIM(ucrsCampanhasConfigUt.Operador)
					lcInactivo	= IIF(ucrsCampanhasConfigUt.inactivo, 1, 0)

					IF YEAR(ucrsCampanhasConfigUt.dtnasc) == 1900
						IF EMPTY(ucrsCampanhasConfigUt.idade)
							lcIdade	= -999999
							lcOperador = '>'
						ELSE
							lcIdade	= astr(ucrsCampanhasConfigUt.idade)
							lcNascimento = '19000101'
						ENDIF
					ELSE
						lcNascimento = uf_gerais_getDate(ucrsCampanhasConfigUt.dtnasc, "SQL")
						lcIdade	= -999999
						lcOperador = '>'
					ENDIF	

					lcMail	 	= IIF(ucrsCampanhasConfigUt.mail, 1, 0)
					lcTlmvl 	= IIF(ucrsCampanhasConfigUt.Tlmvl, 1, 0)
					lcObs		= ALLTRIM(ucrsCampanhasConfigUt.obs)
					lcEFR		= ALLTRIM(ucrsCampanhasConfigUt.entpla)
					lcPatologia	= ALLTRIM(ucrsCampanhasConfigUt.patologia)
					lcRef 		= ALLTRIM(ucrsCampanhasConfigUt.ref)
					lcEntre		= uf_gerais_getDate(ucrsCampanhasConfigUt.Entre, "SQL")
					lcE			= uf_gerais_getDate(ucrsCampanhasConfigUt.E, "SQL")
					lcValTouch = 0
					lcValEntidadeClinica = 0

					TEXT TO lcSQL TEXTMERGE NOSHOW	
	   					exec up_campanhas_pesquisaClientes <<lcTop>>, '<<lcNome>>', <<lcNo>>, <<lcEstab>>, '<<lcMorada>>', '<<lcNcont>>'
							,'<<lcNCartao>>', '<<lcTipo>>', '<<lcProfissao>>'
							,'<<lcSexo>>', <<lcTlmvl>>, '<<lcOperador>>', <<lcIdade>>, '<<lcObs>>', <<lcMail>>, '<<lcEFR>>'
							,'<<lcPatologia>>', '<<lcRef>>', '<<lcEntre>>', '<<lcE>>', '<<lcNascimento>>', <<lcInactivo>>
							,'<<lctlf>>', '<<lcutenteno>>', '<<lcnbenef>>', <<lcValTouch>>,<<lcValEntidadeClinica>>, '<<lcId>>'
					ENDTEXT

					IF !uf_gerais_actGrelha("","UcrsTempUtCamp",lcSql)
						uf_perguntalt_chama("Ocorreu um problema ao verificar defini��o das campanhas.","OK","",16)
						RETURN .f.
					ENDIF
				
					IF EMPTY(lcExcecao) && Regra

	*!*						DELETE ucrsCampanhasTodosUtentes;
	*!*						FROM;
	*!*							ucrsCampanhasTodosUtentes;
	*!*							left join UcrsTempUtCamp on ucrsCampanhasTodosUtentes.no == UcrsTempUtCamp.no AND ucrsCampanhasTodosUtentes.estab == UcrsTempUtCamp.estab;
	*!*						Where;
	*!*							ISNULL(UcrsTempUtCamp.no) == .t.
						
						SELECT ucrsCampanhasTodosUtentes.no, ucrsCampanhasTodosUtentes.estab;
						FROM;
							ucrsCampanhasTodosUtentes;
							left join UcrsTempUtCamp on ucrsCampanhasTodosUtentes.no == UcrsTempUtCamp.no AND ucrsCampanhasTodosUtentes.estab == UcrsTempUtCamp.estab;
						Where;
							UcrsTempUtCamp.no is null INTO CURSOR ucrsTempDeleteUt READWRITE 
							
						SELECT ucrsTempDeleteUt 	
						GO TOP
						SCAN 
							SELECT ucrsCampanhasTodosUtentes
							GO TOP
							SELECT ucrsCampanhasTodosUtentes
							LOCATE FOR ucrsCampanhasTodosUtentes.no == ucrsTempDeleteUt.no AND ucrsCampanhasTodosUtentes.estab == ucrsTempDeleteUt.estab
							IF FOUND()
								SELECT ucrsCampanhasTodosUtentes
								DELETE
							ENDIF						
						ENDSCAN
						
						IF USED("ucrsTempDeleteUt")
							fecha("ucrsTempDeleteUt")
						ENDIF 	
						
					ELSE && Excecao
					
	*!*						DELETE ucrsCampanhasTodosUtentes;
	*!*						FROM;
	*!*							ucrsCampanhasTodosUtentes;
	*!*							left join UcrsTempUtCamp on ucrsCampanhasTodosUtentes.no == UcrsTempUtCamp.no AND ucrsCampanhasTodosUtentes.estab == UcrsTempUtCamp.estab;
	*!*						Where; 
	*!*							ISNULL(UcrsTempUtCamp.no) == .f.
						
						SELECT ucrsCampanhasTodosUtentes.no, ucrsCampanhasTodosUtentes.estab;
						FROM;
							ucrsCampanhasTodosUtentes;
							left join UcrsTempUtCamp on ucrsCampanhasTodosUtentes.no == UcrsTempUtCamp.no AND ucrsCampanhasTodosUtentes.estab == UcrsTempUtCamp.estab;
						Where; 
							UcrsTempUtCamp.no is not null INTO CURSOR ucrsTempDeleteUt READWRITE 
							
						SELECT ucrsTempDeleteUt 	
						GO TOP
						SCAN 
							SELECT ucrsCampanhasTodosUtentes
							GO TOP
							SELECT ucrsCampanhasTodosUtentes
							LOCATE FOR ucrsCampanhasTodosUtentes.no == ucrsTempDeleteUt.no AND ucrsCampanhasTodosUtentes.estab == ucrsTempDeleteUt.estab
							IF FOUND()
								SELECT ucrsCampanhasTodosUtentes
								DELETE
							ENDIF						
						ENDSCAN
						
						IF USED("ucrsTempDeleteUt")
							fecha("ucrsTempDeleteUt")
						ENDIF 	
					ENDIF 			
					
					IF USED("UcrsTempUtCamp")
						fecha("UcrsTempUtCamp")
					ENDIF 
				
				ENDSCAN

				lcSQLFinal = ""
				lcContador = 0
				SELECT ucrsCampanhasTodosUtentes
				GO TOP 
				SCAN FOR !EMPTY(ucrsCampanhasTodosUtentes.no)
					lcNo = ucrsCampanhasTodosUtentes.no
					lcEstab = ucrsCampanhasTodosUtentes.estab
					
					TEXT TO lcSQL NOSHOW TEXTMERGE 
						update campanhas_ut_lista set campanhas = campanhas + '(<<astr(ucrsCampanhasList.id)>>)' WHERE no = <<lcNo>> AND estab = <<lcEstab>>
					ENDTEXT 
					
					lcSQLFinal = lcSQLFinal + CHR(13) + CHR(10) + lcSql
					lcContador = lcContador + 1
					IF lcContador > 400
						lcContador = 0		

						IF !uf_gerais_actGrelha("","",lcSQLFinal)
							uf_perguntalt_chama("Ocorreu uma anomalia ao verificar defini��o das campanhas.","OK","",16)
							RETURN .f.
						ENDIF
						
						lcSQLFinal = ""
					ENDIF 
				ENDSCAN 

				IF !EMPTY(lcSQLFinal)
					IF !uf_gerais_actGrelha("","",lcSQLFinal)
						uf_perguntalt_chama("Ocorreu uma anomalia ao verificar defini��o das campanhas.","OK","",16)
						RETURN .f.
					ENDIF
				ENDIF

				regua(1,5,"A carregar campanhas...")
			ENDIF		

			SELECT ucrsCampanhasConfigSt
			LOCATE FOR ucrsCampanhasList.id == uCrsCampanhasConfigSt.campanhas_id 
			IF !FOUND() && Se n�o tiver nenhuma regra aplicada aplica 
				
				&& insere e atualiza produtos na tabela campanhas_st_lista
				TEXT TO lcSQL NOSHOW TEXTMERGE 
					exec up_campanhas_insereListaSt <<ucrsCampanhasList.id>>
				ENDTEXT 			
				
				If !uf_gerais_actGrelha("","",lcSql)
					uf_perguntalt_chama("Ocorreu uma anomalia ao atualizar lista de produtos da campanha. Contacte o suporte.","OK","",16)
					RETURN .f.
				ENDIF
			
			ELSE			
				&&			
				SELECT ucrsCampanhasConfigSt
				GO TOP 
				SCAN FOR ucrsCampanhasList.id == ucrsCampanhasConfigSt.campanhas_id

					lcExcecao = ucrsCampanhasConfigSt.excecao
					lcTop = 10000
					lcRef = ALLTRIM(ucrsCampanhasConfigSt.artigo)
					lcMarca = ALLTRIM(ucrsCampanhasConfigSt.marca)
					lctxIva = ASTR(ucrsCampanhasConfigSt.txIva)
					lcFamilia 	= ALLTRIM(ucrsCampanhasConfigSt.familia)
					lcLab = ALLTRIM(ucrsCampanhasConfigSt.lab)
					lcDci = ALLTRIM(ucrsCampanhasConfigSt.dci)
					lcGenerico	= ucrsCampanhasConfigSt.generico
					lcOperadorLogico	= IIF(ALLTRIM(ucrsCampanhasConfigSt.OperadorLogico)==">", "maior", IIF(ALLTRIM(ucrsCampanhasConfigSt.OperadorLogico)=="<", "menor", "igual"))
					lcStock = ucrsCampanhasConfigSt.stock
					lcServicos = ucrsCampanhasConfigSt.servicos
					lcInactivos	= IIF(ucrsCampanhasConfigSt.inactivo, 1, 0)
					lcParafarmacia 	= IIF(ucrsCampanhasConfigSt.parafarmacia, 1,0)
					lcComFicha		= IIF(ucrsCampanhasConfigSt.comficha, 1, 0)
					lcT5			= IIF(ucrsCampanhasConfigSt.t5, 1, 0)
					lcRecurso = ""
					lcTipo 	= ALLTRIM(ucrsCampanhasConfigSt.tipo)

					&& Trata Acentos
					lcRef = CHRTRAN(lcRef, "�������������������", "aouaaaeeeiiiooouuuc")
					lcMarca = CHRTRAN(lcMarca, "�������������������", "aouaaaeeeiiiooouuuc")
					lcFamilia = CHRTRAN(lcFamilia, "�������������������", "aouaaaeeeiiiooouuuc")
					lcLab = CHRTRAN(lcLab, "�������������������", "aouaaaeeeiiiooouuuc")
					lcDci = CHRTRAN(lcDci, "�������������������", "aouaaaeeeiiiooouuuc")


					&& Atribui novamente todos os produtos para filtrar
					SELECT * FROM ucrsCampanhasTodosProdutosOriginal INTO CURSOR ucrsCampanhasSTFiltrar READWRITE 
					LOCAL lcTipoValidacao
					IF EMPTY(lcExcecao)
						lcTipoValidacao=0
					ELSE
						lcTipoValidacao=1
					ENDIF 
					
					lcCursorConfigUtValidaSt = "ucrsCampanhasSTFiltrar"	
					uf_campanhas_filtraProdutos(lcCursorConfigUtValidaSt, lcTop, lcRef, lcMarca, lctxIva, lcFamilia, lcLab, lcDci, lcGenerico, lcOperadorLogico, lcStock, lcServicos, lcInactivos, lcParafarmacia, lcComFicha, lcT5, lcRecurso, lcTipoValidacao, LcTipo)
					IF EMPTY(lcExcecao) && N�o � excecao

*!*						DELETE uCrsCampanhasTodosProdutos;
*!*						FROM;
*!*							uCrsCampanhasTodosProdutos;
*!*							LEFT JOIN &lcCursorConfigUtValidaSt ON ALLTRIM(&lcCursorConfigUtValidaSt..ref) == ALLTRIM(uCrsCampanhasTodosProdutos.ref);
*!*						WHERE;
*!*							&lcCursorConfigUtValidaSt..ref is null 
						
						SELECT uCrsCampanhasTodosProdutos.ref;
						FROM;
							uCrsCampanhasTodosProdutos;
							LEFT JOIN &lcCursorConfigUtValidaSt ON ALLTRIM(&lcCursorConfigUtValidaSt..ref) == ALLTRIM(uCrsCampanhasTodosProdutos.ref);
						WHERE;
							&lcCursorConfigUtValidaSt..ref is null INTO CURSOR ucrsTempDeleteSt READWRITE 
							
						SELECT ucrsTempDeleteSt	
						GO TOP
						SCAN 
							SELECT ucrsCampanhasTodosProdutos
							GO TOP
							SELECT ucrsCampanhasTodosProdutos
							LOCATE FOR ALLTRIM(ucrsCampanhasTodosProdutos.ref) == ALLTRIM(ucrsTempDeleteSt.ref)
							IF FOUND()
								SELECT ucrsCampanhasTodosProdutos
								DELETE
							ENDIF						
						ENDSCAN
						
						IF USED("ucrsTempDeleteSt")
							fecha("ucrsTempDeleteSt")
						ENDIF 
						
					ELSE && Excecao, todos os produtos excepto os devolvidos

*!*						DELETE ucrsCampanhasTodosProdutos;
*!*						FROM;
*!*							ucrsCampanhasTodosProdutos;
*!*							LEFT JOIN &lcCursorConfigUtValidaSt ON ALLTRIM(&lcCursorConfigUtValidaSt..ref) == ALLTRIM(ucrsCampanhasTodosProdutos.ref);
*!*						WHERE;
*!*							&lcCursorConfigUtValidaSt..ref is not null

						SELECT uCrsCampanhasTodosProdutos.ref;
						FROM;
							ucrsCampanhasTodosProdutos;
							LEFT JOIN &lcCursorConfigUtValidaSt ON ALLTRIM(&lcCursorConfigUtValidaSt..ref) == ALLTRIM(ucrsCampanhasTodosProdutos.ref);
						WHERE;
							&lcCursorConfigUtValidaSt..ref is not null INTO CURSOR ucrsTempDeleteSt READWRITE 

						SELECT ucrsTempDeleteSt	
						GO TOP
						SCAN 
							SELECT ucrsCampanhasTodosProdutos
							GO TOP
							SELECT ucrsCampanhasTodosProdutos
							LOCATE FOR ALLTRIM(ucrsCampanhasTodosProdutos.ref) == ALLTRIM(ucrsTempDeleteSt.ref)
							IF FOUND()
								SELECT ucrsCampanhasTodosProdutos
								DELETE
							ENDIF						
						ENDSCAN

						IF USED("ucrsTempDeleteSt")
							fecha("ucrsTempDeleteSt")
						ENDIF 	
					ENDIF

				ENDSCAN
			
			
				IF USED(lcCursorConfigUtValidaSt)
					fecha(lcCursorConfigUtValidaSt)
				ENDIF
					
				lcSQLFinal = ""
				lcContador = 0
				
				SELECT ucrsCampanhasTodosProdutos
				GO TOP 
				SCAN FOR !EMPTY(ucrsCampanhasTodosProdutos.ref)
					lcRef = ucrsCampanhasTodosProdutos.ref
					TEXT TO lcSQL NOSHOW TEXTMERGE 
						update campanhas_st_lista set campanhas = campanhas + '(<<astr(ucrsCampanhasList.id)>>)' WHERE ref = '<<ALLTRIM(lcRef)>>'
					ENDTEXT 
					
					lcSQLFinal = lcSQLFinal + CHR(13) + CHR(10) + lcSql
					lcContador = lcContador + 1
					IF lcContador > 400
						lcContador = 0		

						IF !uf_gerais_actGrelha("","",lcSQLFinal)
							uf_perguntalt_chama("Ocorreu uma anomalia ao verificar a defini��o das campanhas.","OK","",16)
							RETURN .f.
						ENDIF
						
						lcSQLFinal = ""
					ENDIF 
				ENDSCAN 
				IF USED("ucrsRefsCamp")
					fecha("ucrsRefsCamp")
				ENDIF 
				
				IF !EMPTY(lcSQLFinal)
					IF !uf_gerais_actGrelha("","",lcSQLFinal)
						uf_perguntalt_chama("Ocorreu um problema ao verificar as defini��es das campanhas.","OK","",16)
						RETURN .f.
					ENDIF
				ENDIF
			ENDIF 

			** Produto de Oferta ***
			SELECT ucrsCampanhasProdutosOferta
			LOCATE FOR ucrsCampanhasList.id == ucrsCampanhasProdutosOferta.campanhas_id 
			IF FOUND()
			
				SELECT ucrsCampanhasProdutosOferta
				GO Top
				SCAN FOR ucrsCampanhasList.id == ucrsCampanhasProdutosOferta.campanhas_id

					lcExcecao = ucrsCampanhasProdutosOferta.excecao
					lcTop = 10000
					lcRef = ALLTRIM(ucrsCampanhasProdutosOferta.artigo)
					lcMarca = ALLTRIM(ucrsCampanhasProdutosOferta.marca)
					lctxIva = ASTR(ucrsCampanhasProdutosOferta.txIva)
					lcFamilia 	= ALLTRIM(ucrsCampanhasProdutosOferta.familia)
					lcLab = ALLTRIM(ucrsCampanhasProdutosOferta.lab)
					lcDci = ALLTRIM(ucrsCampanhasProdutosOferta.dci)
					lcGenerico	= ucrsCampanhasProdutosOferta.generico
					lcOperadorLogico	= IIF(ALLTRIM(ucrsCampanhasProdutosOferta.OperadorLogico)==">", "maior", IIF(ALLTRIM(ucrsCampanhasProdutosOferta.OperadorLogico)=="<", "menor", "igual"))
					lcStock = ucrsCampanhasProdutosOferta.stock
					lcServicos = ucrsCampanhasProdutosOferta.servicos
					lcInactivos	= IIF(ucrsCampanhasProdutosOferta.inactivo, 1, 0)
					lcParafarmacia 	= IIF(ucrsCampanhasProdutosOferta.parafarmacia, 1,0)
					lcComFicha		= IIF(ucrsCampanhasProdutosOferta.comficha, 1, 0)
					lcT5			= IIF(ucrsCampanhasProdutosOferta.t5, 1, 0)
					lcRecurso = ""
				
					** Trata Acentos
					lcRef = CHRTRAN(lcRef, "�������������������", "aouaaaeeeiiiooouuuc")
					lcMarca = CHRTRAN(lcMarca, "�������������������", "aouaaaeeeiiiooouuuc")
					lcFamilia = CHRTRAN(lcFamilia, "�������������������", "aouaaaeeeiiiooouuuc")
					lcLab = CHRTRAN(lcLab, "�������������������", "aouaaaeeeiiiooouuuc")
					lcDci = CHRTRAN(lcDci, "�������������������", "aouaaaeeeiiiooouuuc")

					&& Atribui nova mente todos os produtos para filtrar
					lcCursorProdOfertaValidaSt = "ucrsCampanhasProdOfertaValidaSt"
					SELECT * FROM ucrsCampanhasTodosProdutosOriginal INTO CURSOR &lcCursorProdOfertaValidaSt READWRITE 

					uf_campanhas_filtraProdutos(lcCursorProdOfertaValidaSt, lcTop, lcRef, lcMarca, lctxIva, lcFamilia, lcLab, lcDci, lcGenerico, lcOperadorLogico, lcStock, lcServicos, lcInactivos, lcParafarmacia, lcComFicha, lcT5, lcRecurso)

					IF EMPTY(lcExcecao) && N�o � excecao
				
	*!*						delete ucrsCampanhasTodosProdutosOferta;
	*!*						FROM;
	*!*							ucrsCampanhasTodosProdutosOferta;
	*!*							LEFT JOIN &lcCursorProdOfertaValidaSt ON ALLTRIM(&lcCursorProdOfertaValidaSt..ref) == ALLTRIM(ucrsCampanhasTodosProdutosOferta.ref);
	*!*						WHERE;
	*!*							&lcCursorProdOfertaValidaSt..ref is null 
			
						SELECT ucrsCampanhasTodosProdutosOferta.ref;
						FROM;
							ucrsCampanhasTodosProdutosOferta;
							LEFT JOIN &lcCursorProdOfertaValidaSt ON ALLTRIM(&lcCursorProdOfertaValidaSt..ref) == ALLTRIM(ucrsCampanhasTodosProdutosOferta.ref);
						WHERE;
							&lcCursorProdOfertaValidaSt..ref is null INTO CURSOR ucrsTempDeleteSt READWRITE 
							
						SELECT ucrsTempDeleteSt	
						GO TOP
						SCAN 
							SELECT ucrsCampanhasTodosProdutosOferta
							GO TOP
							SELECT ucrsCampanhasTodosProdutosOferta
							LOCATE FOR ALLTRIM(ucrsCampanhasTodosProdutosOferta.ref) == ALLTRIM(ucrsTempDeleteSt.ref)
							IF FOUND()
								SELECT ucrsCampanhasTodosProdutosOferta
								DELETE
							ENDIF						
						ENDSCAN
						
						IF USED("ucrsTempDeleteSt")
							fecha("ucrsTempDeleteSt")
						ENDIF 	
			
					ELSE && Excecao, todos os produtos excepto os devolvidos
						
	*!*						DELETE ucrsCampanhasTodosProdutosOferta;
	*!*						FROM;
	*!*							ucrsCampanhasTodosProdutosOferta;
	*!*							LEFT JOIN &lcCursorProdOfertaValidaSt ON ALLTRIM(&lcCursorProdOfertaValidaSt..ref) == ALLTRIM(ucrsCampanhasTodosProdutosOferta.ref);
	*!*						WHERE;
	*!*							&lcCursorProdOfertaValidaSt..ref is not null
							
						SELECT ucrsCampanhasTodosProdutosOferta.ref;
						FROM;
							ucrsCampanhasTodosProdutosOferta;
							LEFT JOIN &lcCursorProdOfertaValidaSt ON ALLTRIM(&lcCursorProdOfertaValidaSt..ref) == ALLTRIM(ucrsCampanhasTodosProdutosOferta.ref);
						WHERE;
							&lcCursorProdOfertaValidaSt..ref is not null INTO CURSOR ucrsTempDeleteSt READWRITE 
							
						SELECT ucrsTempDeleteSt	
						GO TOP
						SCAN 
							SELECT ucrsCampanhasTodosProdutosOferta
							GO TOP
							SELECT ucrsCampanhasTodosProdutosOferta
							LOCATE FOR ALLTRIM(ucrsCampanhasTodosProdutosOferta.ref) == ALLTRIM(ucrsTempDeleteSt.ref)
							IF FOUND()
								SELECT ucrsCampanhasTodosProdutosOferta
								DELETE
							ENDIF						
						ENDSCAN
						
						IF USED("ucrsTempDeleteSt")
							fecha("ucrsTempDeleteSt")
						ENDIF 		
					ENDIF
					
				ENDSCAN 
				
				IF USED(lcCursorProdOfertaValidaSt)
					fecha(lcCursorProdOfertaValidaSt)
				ENDIF
				
				lcSQLFinal = ""
				lcContador = 0
				
				SELECT ucrsCampanhasTodosProdutosOferta
				GO Top
				SCAN FOR !EMPTY(ucrsCampanhasTodosProdutosOferta.ref)
					lcRef = ucrsCampanhasTodosProdutosOferta.ref
					TEXT TO lcSQL NOSHOW TEXTMERGE 
						update campanhas_pOfertas_lista set campanhas = campanhas + '(<<astr(ucrsCampanhasList.id)>>)' WHERE ref = '<<ALLTRIM(lcRef)>>'
					ENDTEXT 
					
					lcSQLFinal = lcSQLFinal + CHR(13) + CHR(10) + lcSql
					lcContador = lcContador + 1
					IF lcContador > 400
						lcContador = 0		

						IF !uf_gerais_actGrelha("","",lcSQLFinal)
							uf_perguntalt_chama("Ocorreu um problema ao verificar defini��o das campanhas.","OK","",16)
							RETURN .f.
						ENDIF
						
						lcSQLFinal = ""
					ENDIF 
				ENDSCAN 
				IF USED("ucrsRefsCamp")
					fecha("ucrsRefsCamp")
				ENDIF 
				
				IF !EMPTY(lcSQLFinal)
					IF !uf_gerais_actGrelha("","",lcSQLFinal)
						uf_perguntalt_chama("Ocorreu um problema ao verificar defini��o das campanhas.","OK","",16)
						RETURN .f.
					ENDIF
				ENDIF

			ENDIF 	
		ENDIF	
		regua(1,7,"A carregar campanhas...")
	ENDSCAN 


	**
	IF USED("ucrsCampanhasTodosProdutos")
		fecha("ucrsCampanhasTodosProdutos")
	ENDIF 
	**
	IF USED("ucrsCampanhasList")
		fecha("ucrsCampanhasList")
	ENDIF 
	**
	IF USED("ucrsCampanhasConfigUt")
		fecha("ucrsCampanhasConfigUt")
	ENDIF 
	**
	IF USED("ucrsCampanhasConfigSt")
		fecha("ucrsCampanhasConfigSt")
	ENDIF 
	**
	IF USED("ucrsCampanhasProdutosOferta")
		fecha("ucrsCampanhasProdutosOferta")
	ENDIF 
	
	IF USED("ucrsCampanhasTodosUtentes")
		fecha("ucrsCampanhasTodosUtentes")
	ENDIF 
	
	IF USED("ucrsCampanhasTodosProdutosOferta")
		fecha("ucrsCampanhasTodosProdutosOferta")
	ENDIF 

	IF USED("ucrsCampanhasTodosProdutosOriginal")
		fecha("ucrsCampanhasTodosProdutosOriginal")
	ENDIF 
	
	IF USED("ucrsCampanhasTodosUtentesOriginal")
		fecha("ucrsCampanhasTodosUtentesOriginal")
	ENDIF 

	regua(2)
	uf_perguntalt_chama("Campanhas processadas com sucesso.","OK","",64)
ENDFUNC 


**
FUNCTION uf_campanhas_filtraProdutos
	LPARAMETERS lcNomeCursor, lcTop, lcRef, lcMarca, lctxIva, lcFamilia, lcLab, lcDci, lcGenerico, lcOperadorLogico, lcStock, lcServicos, lcInactivos, lcParafarmacia, lcComFicha, lcT5, lcRecurso, lcTipoValidacao, LcTipo
	
	
	LOCAL lcCaractEspecial, lcCaractEspecialCorrigido 
	
	STORE '�������������������������������' TO lcCaractEspecial
	STORE 'aouaaaaeeAAEeiiiIIOOOoooUUUuuuc' TO lcCaractEspecialCorrigido 
	
	
	IF !EMPTY(ALLTRIM(lcRef))
		lcRef = STRTRAN(lcRef,"%","*")
		IF vartype(lcTipoValidacao) == 'N'
			IF lcTipoValidacao = 0
				DELETE &lcNomeCursor;
				FROM ;
					&lcNomeCursor ;
				WHERE;
					!(UPPER(ALLTRIM(&lcNomeCursor..ref)) == UPPER(ALLTRIM(lcRef)));
					AND !LIKE('*'+ STRTRAN(UPPER(ALLTRIM(lcRef))," ","") + '*',STRTRAN(UPPER(ALLTRIM(&lcNomeCursor..design))," ",""));
					AND !(UPPER(ALLTRIM(&lcNomeCursor..cnpem)) == UPPER(ALLTRIM(lcRef)))
			ELSE
				DELETE &lcNomeCursor;
				FROM ;
					&lcNomeCursor ;
				WHERE;
					!(UPPER(ALLTRIM(&lcNomeCursor..ref)) == UPPER(ALLTRIM(lcRef)));
					AND !LIKE('*'+ STRTRAN(UPPER(ALLTRIM(lcRef))," ","") + '*',UPPER(ALLTRIM(&lcNomeCursor..design)));
					AND !(UPPER(ALLTRIM(&lcNomeCursor..cnpem)) == UPPER(ALLTRIM(lcRef)))		
			ENDIF 
		ELSE
			DELETE &lcNomeCursor;
			FROM ;
				&lcNomeCursor ;
			WHERE;
				!(UPPER(ALLTRIM(&lcNomeCursor..ref)) == UPPER(ALLTRIM(lcRef)));
				AND !LIKE('*'+ STRTRAN(UPPER(ALLTRIM(lcRef))," ","") + '*',STRTRAN(UPPER(ALLTRIM(&lcNomeCursor..design))," ",""));
				AND !(UPPER(ALLTRIM(&lcNomeCursor..cnpem)) == UPPER(ALLTRIM(lcRef)))
		ENDIF 
	ENDIF 
	


	IF !EMPTY(ALLTRIM(lcFamilia))
		DELETE ;
		FROM ;
			&lcNomeCursor;
		WHERE;
			(UPPER(ALLTRIM(CHRTRAN(&lcNomeCursor..faminome, lcCaractEspecial, lcCaractEspecialCorrigido ))) != UPPER(ALLTRIM(lcFamilia)))
	ENDIF
	
	IF !EMPTY(ALLTRIM(lcLab))
		DELETE ;
		FROM ;
			&lcNomeCursor;
		WHERE;
			!(UPPER(ALLTRIM(CHRTRAN(&lcNomeCursor..u_lab, lcCaractEspecial, lcCaractEspecialCorrigido ))) == UPPER(ALLTRIM(lcLab)))
			
	ENDIF
	

	
	IF !EMPTY(ALLTRIM(lcMarca))
		DELETE ;
		FROM ;
			&lcNomeCursor;
		WHERE;
			!(UPPER(ALLTRIM(CHRTRAN(&lcNomeCursor..usr1, lcCaractEspecial, lcCaractEspecialCorrigido ))) == UPPER(ALLTRIM(lcMarca)))
	ENDIF

	IF !EMPTY(ALLTRIM(lcDci))
		DELETE ;
		FROM ;
			&lcNomeCursor;
		WHERE;
			!LIKE('*'+ UPPER(ALLTRIM(&lcNomeCursor..dci)) + '*', UPPER(ALLTRIM(lcDCI)))
	ENDIF

	IF lcServicos == 1
		DELETE ;
		FROM ;
			&lcNomeCursor;
		WHERE;
			!(&lcNomeCursor..stns == .t.)
	ENDIF 
	
	IF lcServicos == 2
		DELETE ;
		FROM ;
			&lcNomeCursor;
		WHERE;
			&lcNomeCursor..stns == .t.
	ENDIF 
	
	IF lcGenerico == 1
		DELETE ;
		FROM ;
			&lcNomeCursor;
		WHERE;
			!(&lcNomeCursor..generico == .t.)
	ENDIF 
	
	IF lcGenerico == 2
		DELETE ;
		FROM ;
			&lcNomeCursor;
		WHERE;
			(&lcNomeCursor..generico == .t.)
	ENDIF 

	IF lcInactivos == 0
		DELETE ;
		FROM ;
			&lcNomeCursor;
		WHERE;
			&lcNomeCursor..inactivo == .t.
	ENDIF 
	
	IF VARTYPE(lctipo)=='C'
		IF !EMPTY(ALLTRIM(lctipo))
			DELETE ;
			FROM ;
				&lcNomeCursor;
			WHERE;
				!(UPPER(ALLTRIM(CHRTRAN(&lcNomeCursor..tipo, lcCaractEspecial, lcCaractEspecialCorrigido ))) == UPPER(ALLTRIM(lctipo)))
		ENDIF
	ENDIF 

&& Stock n�o � usado, porque seria muito lento calcular campanhas semp+re que alterasse o stock do produto
*!*		IF lcStock != -999999 
*!*			DO CASE 
*!*				CASE lcOperadorLogico  = 'maior'
*!*					DELETE ;
*!*					FROM ;
*!*						&lcNomeCursor;
*!*					WHERE;
*!*						&lcNomeCursor..stock <= lcStock and !(&lcNomeCursor..stns == .t.)
*!*					
*!*				CASE lcOperadorLogico  = 'menor'
*!*					
*!*					DELETE ;
*!*					FROM ;
*!*						&lcNomeCursor;
*!*					WHERE;
*!*						&lcNomeCursor..stock >= lcStock and !(&lcNomeCursor..stns == .t.)
*!*				
*!*				CASE lcOperadorLogico  = 'igual'
*!*					
*!*					DELETE ;
*!*					FROM ;
*!*						&lcNomeCursor;
*!*					WHERE;
*!*						!(&lcNomeCursor..stock == lcStock) and !(&lcNomeCursor..stns == .t.)
*!*			
*!*			ENDCASE
*!*		ENDIF

*!*		IF lcComFicha == 1
*!*			DELETE ;
*!*			FROM ;
*!*				&lcNomeCursor;
*!*			WHERE;
*!*				&lcNomeCursor..temFicha == .f.
*!*		ENDIF 

*!*		IF lcParafarmacia== 1
*!*			DELETE ;
*!*			FROM ;
*!*				&lcNomeCursor;
*!*			WHERE;
*!*				ALLTRIM(&lcNomeCursor..familia) = "1" or ALLTRIM(&lcNomeCursor..u_familia) = "1"
*!*		ENDIF 
*!*		
	IF !EMPTY(VAL(lctxIva))
		
		DELETE ;
		FROM ;
			&lcNomeCursor;
		WHERE;
			!(&lcNomeCursor..iva == VAL(lctxIva))
	ENDIF 
		
	IF lcT5 == 1
		DELETE ;
		FROM ;
			&lcNomeCursor;
		WHERE;
			&lcNomeCursor..t5 == .f.
	ENDIF
ENDFUNC 

**
FUNCTION uf_campanhas_sair

	IF myCampanhasIntroducao == .t. OR myCampanhasAlteracao == .t. 	
		If uf_perguntalt_chama("Quer mesmo cancelar? Ir� perder as �ltimas altera��es feitas.","Sim","N�o")
			myCampanhasIntroducao = .f.
			myCampanhasAlteracao = .f.
			
			uf_CAMPANHAS_chama(ucrsCampanhas.id)
			uf_CAMPANHAS_alternaMenu()
		ENDIF 
	ELSE
		uf_campanhas_exit()
	ENDIF
	 
ENDFUNC 

** 
FUNCTION uf_campanhas_exit
	CAMPANHAS.hide
	CAMPANHAS.release
ENDFUNC 

FUNCTION uf_altgr_escolheEmpresasCRM
	
	** Listam de Empresas
	IF USED("ucrsConfEmpresas")
		SELECT * FROM ucrsConfEmpresas INTO CURSOR ucrsConfEmpresasAux READWRITE 
	ENDIF 
	
	IF !USED("ucrsConfEmpresas")
		TEXT To lcSQL TEXTMERGE NOSHOW
			exec up_gerais_trocaEmpresa <<ch_userno>>,'<<ALLTRIM(ch_grupo)>>'
		ENDTEXT 
		uf_gerais_actGrelha("","ucrsListSiteAux",lcSQL)
		SELECT .f. as selEmpresa,* FROM ucrsListSiteAux INTO CURSOR ucrsConfEmpresas READWRITE 
	ENDIF 
	
	IF USED("ucrsConfEmpresasAux")	
		SELECT ucrsConfEmpresas
		GO TOP
		SCAN 
			SELECT ucrsConfEmpresasAux	
			LOCATE FOR ALLTRIM(UPPER(ucrsConfEmpresas.local)) == ALLTRIM(UPPER(ucrsConfEmpresasAux.local))
			IF FOUND()
				Replace ucrsConfEmpresas.selEmpresa WITH ucrsConfEmpresasAux.selEmpresa
			ENDIF 
		ENDSCAN 
		
	ENDIF 
	
	SELECT ucrsConfEmpresas
	GO top
	SCAN
		IF ucrsConfEmpresas.siteno=mySite_nr then
			replace ucrsConfEmpresas.selempresa WITH .t.
		ENDIF 
	ENDSCAN 
	
	** Na altera��o verifica em que lojas a campanha existe
	IF myCampanhasIntroducao == .f.
		SELECT ucrsConfEmpresas
		GO top
		SCAN
				TEXT To lcSQLverif TEXTMERGE NOSHOW
					SELECT * FROM campanhas WHERE descricao='<<ALLTRIM(ucrsCampanhas.descricao)>>' AND eliminada=0
				ENDTEXT 
				uf_gerais_actGrelha("","ucrsverif",lcSQLverif)
				IF 	RECCOUNT("ucrsverif")=0 then
					SELECT ucrsConfEmpresas
					DELETE 
				ENDIF
		ENDSCAN 
	ENDIF 

	uf_valorescombo_chama("ucrsConfEmpresas", 6, "ucrsConfEmpresas", 2, "LOCAL", "LOCAL",.t.,.f.,.t.,.t.)
ENDFUNC 

FUNCTION uf_calcular_valor_cartao
	LOCAL lcClno, lcEstabno, lcCampID, lcValidaClPValor, lcPosFi, lcRefFi, lcValCartao, lcValCartaoLimite
	STORE '' TO lcCampID, lcRefFi 
	STORE .f. TO lcValidaClPValor
	STORE 0 TO lcValCartao, lcValCartaoLimite
	LOCAL lcDescontouValor
	STORE .f. TO lcDescontouValor
	
	SELECT fi 
	lcPosFi = RECNO("fi")
	GO TOP 
	SCAN 
		IF alltrim(fi.ref) != 'V000001' AND !EMPTY(fi.ofistamp) AND fi.tipodoc=1
			replace fi.valcartao WITH 0
		ENDIF 
		IF alltrim(fi.ref) == 'V000001'
			lcDescontouValor = .t.
		ENDIF 
	ENDSCAN 

	SELECT uCrsCabVendas 
	GO TOP 
	SELECT * FROM uCrsCabVendas INTO CURSOR uCrsCabVendascamp readwrite
	
	SELECT ft	
	uf_PAGAMENTO_retornaClienteADescontarValorCartao(ft.no, ft.estab, ft.ftstamp)
	
	lcClno = ucrClNumberCardTemp.no
	lcEstabno = ucrClNumberCardTemp.estab


	SELECT ucrsValcartaoList
	IF RECCOUNT("ucrsValcartaoList")>0
		GO TOP 
		SCAN 
			lcValidaClPValor = .f.
			lcCampID = "(" + ALLTRIM(STR(ucrsValcartaoList.id)) + ")"
			lcValCartao = ucrsValcartaoList.valcartao/100
			lcValCartaoLimite = ucrsValcartaoList.vallimite
			SELECT uCrsCampanhasListaUtValor 
			LOCATE FOR uCrsCampanhasListaUtValor.no = lcClno  AND uCrsCampanhasListaUtValor.estab = lcEstabno AND AT(lcCampID, uCrsCampanhasListaUtValor.campanhas)>0
			IF FOUND()
				lcValidaClPValor = .t.
			ENDIF 
			IF lcValidaClPValor = .t.
				SELECT ref FROM uCrsCampanhasListaStValor INTO CURSOR uCrsCampanhasListaStValorAux WHERE AT(lcCampID, uCrsCampanhasListaStValor.campanhas)>0 READWRITE 
				SELECT fi
				GO TOP 
				SCAN 
					IF !EMPTY(fi.ref) AND alltrim(fi.ref) != 'V000001' AND fi.valcartao=0 AND (fi.u_epvp < lcValCartaoLimite OR lcValCartaoLimite =0)
						LOCAL lcFistampfaminome, lcFamiNomeSel, lctipolincab, lcReceitaTipo
						lcFamiNomeSel = ''
						lctipolincab = ''
						lcReceitaTipo = ''
						lcFistampfaminome = ALLTRIM(fi.ref)
						SELECT ucrsatendst
						LOCATE FOR ALLTRIM(ucrsatendst.ref) == ALLTRIM(lcFistampfaminome)
						IF FOUND()
							lcFamiNomeSel  = ALLTRIM(ucrsatendst.faminome)					
						ENDIF 
						SELECT fi 		
						IF ALLTRIM(lcFamiNomeSel) == 'MSRM' AND uf_gerais_getParameter_site('ADM0000000135','BOOL')
							LOCAL lcLordemFiS, lcNrReceitaCamp
							lcLordemFiS = fi.lordem
							SELECT uCrsCabVendascamp
							LOCATE FOR  LEFT(ALLTRIM(STR(uCrsCabVendascamp.lordem)),2)==LEFT(ALLTRIM(STR(lcLordemFiS)),2)
							IF FOUND()
								lcReceitaTipo = ALLTRIM(uCrsCabVendascamp.receita_tipo)	
								lcNrReceitaCamp = ALLTRIM(STREXTRACT(uCrsCabVendascamp.design, ':', ' -', 1, 0))				
							ENDIF
						ENDIF 
						
						IF !uf_gerais_getParameter_site('ADM0000000135','BOOL') OR (uf_gerais_getParameter_site('ADM0000000135','BOOL') AND lcReceitaTipo <> 'SR' AND ALLTRIM(lcFamiNomeSel) == 'MSRM' AND len(alltrim(lcNrReceitaCamp))<>0)
						
							IF !uf_gerais_getParameter('ADM0000000319','BOOL')	
								IF !uf_gerais_getParameter('ADM0000000320','BOOL')	
									lcRefFi = ALLTRIM(fi.ref)
									SELECT uCrsCampanhasListaStValorAux 
									LOCATE FOR ALLTRIM(uCrsCampanhasListaStValorAux.ref) == ALLTRIM(lcRefFi)
									IF FOUND()
										SELECT fi 
										replace fi.valcartao WITH fi.valcartao + (fi.etiliquido * lcValCartao)					
									ENDIF 
								ELSE 
									IF (EMPTY(fi.campanhas) AND fi.desconto=0 AND fi.u_descval=0) OR lcDescontouValor = .t.
										lcRefFi = ALLTRIM(fi.ref)
										SELECT uCrsCampanhasListaStValorAux 
										LOCATE FOR ALLTRIM(uCrsCampanhasListaStValorAux.ref) == ALLTRIM(lcRefFi)
										IF FOUND()
											SELECT fi 
											replace fi.valcartao WITH fi.valcartao + (fi.etiliquido * lcValCartao)					
										ENDIF 
									ENDIF 
								ENDIF 
							ELSE
								IF !uf_gerais_getParameter('ADM0000000320','BOOL')
									LOCAL lcFiLordemPontos
									lcFiLordemPontos = fi.lordem
									SELECT uCrsCabVendascamp 
									GO TOP 
									SELECT design FROM uCrsCabVendascamp WHERE LEFT(ALLTRIM(STR(uCrsCabVendascamp.lordem)),2)==LEFT(ALLTRIM(STR(lcFiLordemPontos )),2) INTO CURSOR UcrsChkSusp
									SELECT UcrsChkSusp
									IF AT('*SUSPENSA*', UcrsChkSusp.design)=0
										SELECT fi 
										lcRefFi = ALLTRIM(fi.ref)
										SELECT uCrsCampanhasListaStValorAux 
										LOCATE FOR ALLTRIM(uCrsCampanhasListaStValorAux.ref) == ALLTRIM(lcRefFi)
											IF FOUND()
											SELECT fi 
											replace fi.valcartao WITH fi.valcartao + (fi.etiliquido * lcValCartao)					
										ENDIF 
									ENDIF 	
									fecha("UcrsChkSusp")
								ELSE
									IF EMPTY(fi.campanhas)
										LOCAL lcFiLordemPontos
										lcFiLordemPontos = fi.lordem
										SELECT uCrsCabVendascamp 
										GO TOP 
										SELECT design FROM uCrsCabVendascamp WHERE LEFT(ALLTRIM(STR(uCrsCabVendascamp.lordem)),2)==LEFT(ALLTRIM(STR(lcFiLordemPontos )),2) INTO CURSOR UcrsChkSusp
										SELECT UcrsChkSusp
										IF AT('*SUSPENSA*', UcrsChkSusp.design)=0
											SELECT fi 
											IF (EMPTY(fi.campanhas) AND fi.desconto=0 AND fi.u_descval=0) OR lcDescontouValor = .t.
												lcRefFi = ALLTRIM(fi.ref)
												SELECT uCrsCampanhasListaStValorAux 
												LOCATE FOR ALLTRIM(uCrsCampanhasListaStValorAux.ref) == ALLTRIM(lcRefFi)
												IF FOUND()
													SELECT fi 
													replace fi.valcartao WITH fi.valcartao + (fi.etiliquido * lcValCartao)					
												ENDIF 
											ENDIF 
										ENDIF 	
										fecha("UcrsChkSusp")
									ENDIF 
								ENDIF 											
							ENDIF 
						ENDIF 
						SELECT fi
					ENDIF 
				ENDSCAN 
			ENDIF 
		ENDSCAN 
		SELECT ucrsValcartaoList
	ENDIF
	
	IF USED("uCrsCabVendascamp") 
		fecha("uCrsCabVendascamp") 
	ENDIF 
	
	if(USED("ucrClNumberCardTemp"))
		fecha("ucrClNumberCardTemp")
	ENDIF
	
	
	fecha("uCrsCampanhasListaStValorAux")
	
	SELECT fi
	TRY 
	SELECT fi 
	GO lcPosFi
	CATCH
		GO BOTTOM
	ENDTRY 
	
ENDFUNC 



FUNCTION uf_campanhas_actualizaCampanhasAtuais_cursor
	LOCAL lcIDCampanhaIndividual 
	
	lcIDCampanhaIndividual = 0

	If !uf_perguntalt_chama("Vai aplicar as defini��es das campanhas." + CHR(13) + "Pretende continuar?" + CHR(13) + CHR(13) +"Reinicie por favor o software em todos os postos ap�s o processamento para que as altera��es sejam refletidas.","Sim","N�o")	
		RETURN .f.
	ENDIF 	
	
	** Inativar campanhas que ultrapassaram data fim
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		update campanhas set inativo=1 where inativo=0 and datafim < dateadd(HOUR, <<difhoraria>>, getdate())-1
	ENDTEXT 	
	If !uf_gerais_actGrelha("","",lcSql)
		uf_perguntalt_chama("Ocorreu uma anomalia ao atualizar as campanhas que ultrapassaram a data de fim.","OK","",16)
		RETURN .f.
	ENDIF


	** 
	lcSQL = ''
	**exec up_campanhas_listar '<<ALLTRIM(mysite)>>'
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_campanhas_listar ''
	ENDTEXT 	
	If !uf_gerais_actGrelha("","ucrsCampanhasList",lcSql)
		uf_perguntalt_chama("Ocorreu uma anomalia ao verificar as campanhas.","OK","",16)
		RETURN .f.
	ENDIF
	
	regua(0,RECCOUNT("ucrsCampanhasList")+4,"A carregar campanhas...")
		
	** 
	lcSQL = ''
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_campanhas_configUt ''
	ENDTEXT 	
	If !uf_gerais_actGrelha("","ucrsCampanhasConfigUt",lcSql)
		uf_perguntalt_chama("Ocorreu uma anomalia ao verificar defini��o das campanhas.","OK","",16)
		RETURN .f.
	ENDIF

	regua(1,1,"A carregar configura��es utentes")

	** 
	lcSQL = ''
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_campanhas_configSt ''
	ENDTEXT 	
	If !uf_gerais_actGrelha("","ucrsCampanhasConfigSt",lcSql)
		uf_perguntalt_chama("Ocorreu uma anomalia ao verificar defini��o das campanhas.","OK","",16)
		RETURN .f.
	ENDIF
	
	regua(1,2,"A carregar configura��es produtos")
	
	** Cursor de Oferta de Produtos
	lcSQL = ''
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_campanhas_ProdutosOferta ''
	ENDTEXT 	
	If !uf_gerais_actGrelha("","ucrsCampanhasProdutosOferta",lcSql)
		uf_perguntalt_chama("Ocorreu uma anomalia ao verificar defini��o das campanhas.","OK","",16)
		RETURN .f.
	ENDIF
	
	regua(1,3,"A carregar campanhas... Defini��es gerais")
	
	lcSQL = ''
	
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_stocks_pesquisa 1000000,'','','','','',0,'maior' ,-999999, -1, 0,<<myArmazem>>,0, 1,'', <<mysite_nr>>, 0, 1, 0, -1, '', '', '', ''
	ENDTEXT 
	
	IF !uf_gerais_actGrelha("","ucrsCampanhasTodosProdutosOriginal",lcSql)
		uf_perguntalt_chama("Ocorreu uma anomalia ao verificar defini��o das campanhas.","OK","",16)
		RETURN .f.
	ENDIF
	
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_stocks_pesquisa 1000000,'','','','','',0,'maior' ,-999999, -1, 0,<<myArmazem>>,0, 1,'', <<mysite_nr>>, 0, 1, 0, -1, '', '', '', ''
	ENDTEXT 
	
	IF !uf_gerais_actGrelha("","ucrsCampanhasTodosProdutosOriginal",lcSql)
		uf_perguntalt_chama("Ocorreu uma anomalia ao verificar defini��o das campanhas.","OK","",16)
		RETURN .f.
	ENDIF
	
	
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		select distinct ref, cast('' as varchar(4000)) as campanhas from st (nolock) ORDER BY ref
	ENDTEXT 
	IF !uf_gerais_actGrelha("","ucrsCampanhasTodos_artigos",lcSql)
		uf_perguntalt_chama("Ocorreu uma anomalia ao verificar defini��o das campanhas.","OK","",16)
		RETURN .f.
	ENDIF
	
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		select distinct no, estab, cast('' as varchar(4000)) as campanhas from b_utentes (nolock) order by no, estab
	ENDTEXT 
		IF !uf_gerais_actGrelha("","ucrsCampanhasTodos_clientes",lcSql)
		uf_perguntalt_chama("Ocorreu uma anomalia ao verificar defini��o das campanhas.","OK","",16)
		RETURN .f.
	ENDIF
	
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		select distinct ref, cast('' as varchar(4000)) as campanhas from st (nolock) ORDER BY ref
	ENDTEXT 
	IF !uf_gerais_actGrelha("","ucrsCampanhasTodos_artigosofertas",lcSql)
		uf_perguntalt_chama("Ocorreu uma anomalia ao verificar defini��o das campanhas.","OK","",16)
		RETURN .f.
	ENDIF


	lcSQL = ''
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		SELECT no, estab FROM b_utentes (nolock) ORDER BY no, estab
	ENDTEXT 
	IF !uf_gerais_actGrelha("","ucrsCampanhasTodosUtentesOriginal",lcSql)
		uf_perguntalt_chama("Ocorreu um problema ao verificar defini��o das campanhas.","OK","",16)
		RETURN .f.
	ENDIF

	** Precorre todas as campanhas
	SELECT uCrsCampanhasList
	GO TOP 
	SCAN 
	
		IF EMPTY(lcIDCampanhaIndividual) OR ucrsCampanhasList.id == lcIDCampanhaIndividual
			regua(1,RECNO()+4,"A carregar configura��es campanha: "+ ASTR(ucrsCampanhasList.id))
			
			SELECT * FROM ucrsCampanhasTodosUtentesOriginal INTO CURSOR ucrsCampanhasTodosUtentes READWRITE 
			
			IF used("ucrsCampanhasTodosProdutos")
				fecha("ucrsCampanhasTodosProdutos")
			ENDIF 
			
			SELECT .f. as apagar,* FROM ucrsCampanhasTodosProdutosOriginal INTO CURSOR ucrsCampanhasTodosProdutos READWRITE 
			
			IF used("ucrsCampanhasTodosProdutosOferta")
				fecha("ucrsCampanhasTodosProdutosOferta ")
			ENDIF 
			
			SELECT .f. as apagar,* FROM ucrsCampanhasTodosProdutosOriginal INTO CURSOR ucrsCampanhasTodosProdutosOferta READWRITE

**MESSAGEBOX('UT')
			SELECT uCrsCampanhasConfigUt
			LOCATE FOR uCrsCampanhasList.id == uCrsCampanhasConfigUt.campanhas_id
			IF !FOUND() && Se n�o tiver nenhuma regra aplicada aplica 
				
				&& insere Lista de Utentes na Tabela campanhas_ut_lista
*!*					TEXT TO lcSQL NOSHOW TEXTMERGE 
*!*						exec up_campanhas_insereListaUt <<ucrsCampanhasList.id>>
*!*					ENDTEXT 
*!*					If !uf_gerais_actGrelha("","",lcSql)
*!*						uf_perguntalt_chama("Ocorreu uma anomalia ao atualizar lista de utentes da campanha. Contacte o suporte.","OK","",16)
*!*						RETURN .f.
*!*					ENDIF
				
			ELSE				
				SELECT ucrsCampanhasConfigUt
				GO TOP 
				SCAN FOR ucrsCampanhasList.id == ucrsCampanhasConfigUt.campanhas_id
					
					lcExcecao = ucrsCampanhasConfigUt.excecao
					lcEstab 		= -1
					lcOperador 		= "="
					lcIdade			= -999999
					lcE				= '30000101'
								
					lcId = ALLTRIM(ucrsCampanhasConfigUt.utid)
					lcTop = 100000
					lcNome = ALLTRIM(ucrsCampanhasConfigUt.nome)
					lcNo = ucrsCampanhasConfigUt.no
					lcEstab	= ucrsCampanhasConfigUt.estab
					lcMorada = ALLTRIM(ucrsCampanhasConfigUt.morada)
					lcNcont	= ALLTRIM(ucrsCampanhasConfigUt.ncont)
					lcNCartao = ALLTRIM(ucrsCampanhasConfigUt.nCartao)
					lctlf = ALLTRIM(ucrsCampanhasConfigUt.tlf)
					lcutenteno	= ""
					lcnbenef	= ALLTRIM(ucrsCampanhasConfigUt.nbenef)
					lcTipo		= ALLTRIM(ucrsCampanhasConfigUt.tipo)
					lcProfissao	= ALLTRIM(ucrsCampanhasConfigUt.profissao)
					lcSexo		= ALLTRIM(ucrsCampanhasConfigUt.sexo)
					lcOperador	= ALLTRIM(ucrsCampanhasConfigUt.Operador)
					lcInactivo	= IIF(ucrsCampanhasConfigUt.inactivo, 1, 0)

					IF YEAR(ucrsCampanhasConfigUt.dtnasc) == 1900
						IF EMPTY(ucrsCampanhasConfigUt.idade)
							lcIdade	= -999999
							lcOperador = '>'
						ELSE
							lcIdade	= astr(ucrsCampanhasConfigUt.idade)
							lcNascimento = '19000101'
						ENDIF
					ELSE
						lcNascimento = uf_gerais_getDate(ucrsCampanhasConfigUt.dtnasc, "SQL")
						lcIdade	= -999999
						lcOperador = '>'
					ENDIF	

					lcMail	 	= IIF(ucrsCampanhasConfigUt.mail, 1, 0)
					lcTlmvl 	= IIF(ucrsCampanhasConfigUt.Tlmvl, 1, 0)
					lcObs		= ALLTRIM(ucrsCampanhasConfigUt.obs)
					lcEFR		= ALLTRIM(ucrsCampanhasConfigUt.entpla)
					lcPatologia	= ALLTRIM(ucrsCampanhasConfigUt.patologia)
					lcRef 		= ALLTRIM(ucrsCampanhasConfigUt.ref)
					lcEntre		= uf_gerais_getDate(ucrsCampanhasConfigUt.Entre, "SQL")
					lcE			= uf_gerais_getDate(ucrsCampanhasConfigUt.E, "SQL")
					lcValTouch = 0
					lcValEntidadeClinica = 0

					TEXT TO lcSQL TEXTMERGE NOSHOW	
	   					exec up_campanhas_pesquisaClientes <<lcTop>>, '<<lcNome>>', <<lcNo>>, <<lcEstab>>, '<<lcMorada>>', '<<lcNcont>>'
							,'<<lcNCartao>>', '<<lcTipo>>', '<<lcProfissao>>'
							,'<<lcSexo>>', <<lcTlmvl>>, '<<lcOperador>>', <<lcIdade>>, '<<lcObs>>', <<lcMail>>, '<<lcEFR>>'
							,'<<lcPatologia>>', '<<lcRef>>', '<<lcEntre>>', '<<lcE>>', '<<lcNascimento>>', <<lcInactivo>>
							,'<<lctlf>>', '<<lcutenteno>>', '<<lcnbenef>>', <<lcValTouch>>,<<lcValEntidadeClinica>>, '<<lcId>>'
					ENDTEXT

					IF !uf_gerais_actGrelha("","UcrsTempUtCamp",lcSql)
						uf_perguntalt_chama("Ocorreu um problema ao verificar defini��o das campanhas.","OK","",16)
						RETURN .f.
					ENDIF
				
					IF EMPTY(lcExcecao) && Regra

	*!*						DELETE ucrsCampanhasTodosUtentes;
	*!*						FROM;
	*!*							ucrsCampanhasTodosUtentes;
	*!*							left join UcrsTempUtCamp on ucrsCampanhasTodosUtentes.no == UcrsTempUtCamp.no AND ucrsCampanhasTodosUtentes.estab == UcrsTempUtCamp.estab;
	*!*						Where;
	*!*							ISNULL(UcrsTempUtCamp.no) == .t.
						
						SELECT ucrsCampanhasTodosUtentes.no, ucrsCampanhasTodosUtentes.estab;
						FROM;
							ucrsCampanhasTodosUtentes;
							left join UcrsTempUtCamp on ucrsCampanhasTodosUtentes.no == UcrsTempUtCamp.no AND ucrsCampanhasTodosUtentes.estab == UcrsTempUtCamp.estab;
						Where;
							UcrsTempUtCamp.no is null INTO CURSOR ucrsTempDeleteUt READWRITE 
							
						SELECT ucrsTempDeleteUt 	
						GO TOP
						SCAN 
							SELECT ucrsCampanhasTodosUtentes
							GO TOP
							SELECT ucrsCampanhasTodosUtentes
							LOCATE FOR ucrsCampanhasTodosUtentes.no == ucrsTempDeleteUt.no AND ucrsCampanhasTodosUtentes.estab == ucrsTempDeleteUt.estab
							IF FOUND()
								SELECT ucrsCampanhasTodosUtentes
								DELETE
							ENDIF						
						ENDSCAN
						
						IF USED("ucrsTempDeleteUt")
							fecha("ucrsTempDeleteUt")
						ENDIF 	
						
					ELSE && Excecao
					
	*!*						DELETE ucrsCampanhasTodosUtentes;
	*!*						FROM;
	*!*							ucrsCampanhasTodosUtentes;
	*!*							left join UcrsTempUtCamp on ucrsCampanhasTodosUtentes.no == UcrsTempUtCamp.no AND ucrsCampanhasTodosUtentes.estab == UcrsTempUtCamp.estab;
	*!*						Where; 
	*!*							ISNULL(UcrsTempUtCamp.no) == .f.
						
						SELECT ucrsCampanhasTodosUtentes.no, ucrsCampanhasTodosUtentes.estab;
						FROM;
							ucrsCampanhasTodosUtentes;
							left join UcrsTempUtCamp on ucrsCampanhasTodosUtentes.no == UcrsTempUtCamp.no AND ucrsCampanhasTodosUtentes.estab == UcrsTempUtCamp.estab;
						Where; 
							UcrsTempUtCamp.no is not null INTO CURSOR ucrsTempDeleteUt READWRITE 
							
						SELECT ucrsTempDeleteUt 	
						GO TOP
						SCAN 
							SELECT ucrsCampanhasTodosUtentes
							GO TOP
							SELECT ucrsCampanhasTodosUtentes
							LOCATE FOR ucrsCampanhasTodosUtentes.no == ucrsTempDeleteUt.no AND ucrsCampanhasTodosUtentes.estab == ucrsTempDeleteUt.estab
							IF FOUND()
								SELECT ucrsCampanhasTodosUtentes
								DELETE
							ENDIF						
						ENDSCAN
						
						IF USED("ucrsTempDeleteUt")
							fecha("ucrsTempDeleteUt")
						ENDIF 	
					ENDIF 			
					
					IF USED("UcrsTempUtCamp")
						fecha("UcrsTempUtCamp")
					ENDIF 
				
				ENDSCAN

				lcSQLFinal = ""
				lcContador = 0
				SELECT ucrsCampanhasTodosUtentes
				GO TOP 
				SCAN FOR !EMPTY(ucrsCampanhasTodosUtentes.no)
					lcNo = ucrsCampanhasTodosUtentes.no
					lcEstab = ucrsCampanhasTodosUtentes.estab
					
					SELECT ucrsCampanhasTodos_clientes
					GO TOP 
				**	TEXT TO lcSQL NOSHOW TEXTMERGE 
					update ucrsCampanhasTodos_clientes set campanhas = campanhas + '('+astr(ucrsCampanhasList.id)+')' WHERE no = lcNo AND estab = lcEstab
				**	ENDTEXT 
					SELECT ucrsCampanhasTodosUtentes
*!*						lcSQLFinal = lcSQLFinal + CHR(13) + CHR(10) + lcSql
*!*						lcContador = lcContador + 1
*!*						IF lcContador > 400
*!*							lcContador = 0		

*!*							IF !uf_gerais_actGrelha("","",lcSQLFinal)
*!*								uf_perguntalt_chama("Ocorreu uma anomalia ao verificar defini��o das campanhas.","OK","",16)
*!*								RETURN .f.
*!*							ENDIF
*!*							
*!*							lcSQLFinal = ""
*!*						ENDIF 
				ENDSCAN 

				IF !EMPTY(lcSQLFinal)
					IF !uf_gerais_actGrelha("","",lcSQLFinal)
						uf_perguntalt_chama("Ocorreu uma anomalia ao verificar defini��o das campanhas.","OK","",16)
						RETURN .f.
					ENDIF
				ENDIF

				regua(1,5,"A carregar campanhas...")
			ENDIF		
**MESSAGEBOX('ST')
			SELECT ucrsCampanhasConfigSt
			LOCATE FOR ucrsCampanhasList.id == uCrsCampanhasConfigSt.campanhas_id 
			IF !FOUND() && Se n�o tiver nenhuma regra aplicada aplica 
				
				&& insere e atualiza produtos na tabela campanhas_st_lista
*!*					TEXT TO lcSQL NOSHOW TEXTMERGE 
*!*						exec up_campanhas_insereListaSt <<ucrsCampanhasList.id>>
*!*					ENDTEXT 			
*!*					
*!*					If !uf_gerais_actGrelha("","",lcSql)
*!*						uf_perguntalt_chama("Ocorreu uma anomalia ao atualizar lista de produtos da campanha. Contacte o suporte.","OK","",16)
*!*						RETURN .f.
*!*					ENDIF
			
			ELSE			
				&&			
				SELECT ucrsCampanhasConfigSt
				GO TOP 
				SCAN FOR ucrsCampanhasList.id == ucrsCampanhasConfigSt.campanhas_id

					lcExcecao = ucrsCampanhasConfigSt.excecao
					lcTop = 10000
					lcRef = ALLTRIM(ucrsCampanhasConfigSt.artigo)
					lcMarca = ALLTRIM(ucrsCampanhasConfigSt.marca)
					lctxIva = ASTR(ucrsCampanhasConfigSt.txIva)
					lcFamilia 	= ALLTRIM(ucrsCampanhasConfigSt.familia)
					lcLab = ALLTRIM(ucrsCampanhasConfigSt.lab)
					lcDci = ALLTRIM(ucrsCampanhasConfigSt.dci)
					lcGenerico	= ucrsCampanhasConfigSt.generico
					lcOperadorLogico	= IIF(ALLTRIM(ucrsCampanhasConfigSt.OperadorLogico)==">", "maior", IIF(ALLTRIM(ucrsCampanhasConfigSt.OperadorLogico)=="<", "menor", "igual"))
					lcStock = ucrsCampanhasConfigSt.stock
					lcServicos = ucrsCampanhasConfigSt.servicos
					lcInactivos	= IIF(ucrsCampanhasConfigSt.inactivo, 1, 0)
					lcParafarmacia 	= IIF(ucrsCampanhasConfigSt.parafarmacia, 1,0)
					lcComFicha		= IIF(ucrsCampanhasConfigSt.comficha, 1, 0)
					lcT5			= IIF(ucrsCampanhasConfigSt.t5, 1, 0)
					lcRecurso = ""

					&& Trata Acentos
					lcRef = CHRTRAN(lcRef, "�������������������", "aouaaaeeeiiiooouuuc")
					lcMarca = CHRTRAN(lcMarca, "�������������������", "aouaaaeeeiiiooouuuc")
					lcFamilia = CHRTRAN(lcFamilia, "�������������������", "aouaaaeeeiiiooouuuc")
					lcLab = CHRTRAN(lcLab, "�������������������", "aouaaaeeeiiiooouuuc")
					lcDci = CHRTRAN(lcDci, "�������������������", "aouaaaeeeiiiooouuuc")

					&& Atribui novamente todos os produtos para filtrar
					SELECT * FROM ucrsCampanhasTodosProdutosOriginal INTO CURSOR ucrsCampanhasSTFiltrar READWRITE 
					LOCAL lcTipoValidacao
					IF EMPTY(lcExcecao)
						lcTipoValidacao=0
					ELSE
						lcTipoValidacao=1
					ENDIF 
					
					lcCursorConfigUtValidaSt = "ucrsCampanhasSTFiltrar"	
					uf_campanhas_filtraProdutos(lcCursorConfigUtValidaSt, lcTop, lcRef, lcMarca, lctxIva, lcFamilia, lcLab, lcDci, lcGenerico, lcOperadorLogico, lcStock, lcServicos, lcInactivos, lcParafarmacia, lcComFicha, lcT5, lcRecurso, lcTipoValidacao)
					IF EMPTY(lcExcecao) && N�o � excecao

*!*						DELETE uCrsCampanhasTodosProdutos;
*!*						FROM;
*!*							uCrsCampanhasTodosProdutos;
*!*							LEFT JOIN &lcCursorConfigUtValidaSt ON ALLTRIM(&lcCursorConfigUtValidaSt..ref) == ALLTRIM(uCrsCampanhasTodosProdutos.ref);
*!*						WHERE;
*!*							&lcCursorConfigUtValidaSt..ref is null 
						
						SELECT uCrsCampanhasTodosProdutos.ref;
						FROM;
							uCrsCampanhasTodosProdutos;
							LEFT JOIN &lcCursorConfigUtValidaSt ON ALLTRIM(&lcCursorConfigUtValidaSt..ref) == ALLTRIM(uCrsCampanhasTodosProdutos.ref);
						WHERE;
							&lcCursorConfigUtValidaSt..ref is null INTO CURSOR ucrsTempDeleteSt READWRITE 
							
						SELECT ucrsTempDeleteSt	
						GO TOP
						SCAN 
							SELECT ucrsCampanhasTodosProdutos
							GO TOP
							SELECT ucrsCampanhasTodosProdutos
							LOCATE FOR ALLTRIM(ucrsCampanhasTodosProdutos.ref) == ALLTRIM(ucrsTempDeleteSt.ref)
							IF FOUND()
								SELECT ucrsCampanhasTodosProdutos
								DELETE
							ENDIF						
						ENDSCAN
						
						IF USED("ucrsTempDeleteSt")
							fecha("ucrsTempDeleteSt")
						ENDIF 
						
					ELSE && Excecao, todos os produtos excepto os devolvidos

*!*						DELETE ucrsCampanhasTodosProdutos;
*!*						FROM;
*!*							ucrsCampanhasTodosProdutos;
*!*							LEFT JOIN &lcCursorConfigUtValidaSt ON ALLTRIM(&lcCursorConfigUtValidaSt..ref) == ALLTRIM(ucrsCampanhasTodosProdutos.ref);
*!*						WHERE;
*!*							&lcCursorConfigUtValidaSt..ref is not null

						SELECT uCrsCampanhasTodosProdutos.ref;
						FROM;
							ucrsCampanhasTodosProdutos;
							LEFT JOIN &lcCursorConfigUtValidaSt ON ALLTRIM(&lcCursorConfigUtValidaSt..ref) == ALLTRIM(ucrsCampanhasTodosProdutos.ref);
						WHERE;
							&lcCursorConfigUtValidaSt..ref is not null INTO CURSOR ucrsTempDeleteSt READWRITE 

						SELECT ucrsTempDeleteSt	
						GO TOP
						SCAN 
							SELECT ucrsCampanhasTodosProdutos
							GO TOP
							SELECT ucrsCampanhasTodosProdutos
							LOCATE FOR ALLTRIM(ucrsCampanhasTodosProdutos.ref) == ALLTRIM(ucrsTempDeleteSt.ref)
							IF FOUND()
								SELECT ucrsCampanhasTodosProdutos
								DELETE
							ENDIF						
						ENDSCAN

						IF USED("ucrsTempDeleteSt")
							fecha("ucrsTempDeleteSt")
						ENDIF 	
					ENDIF

				ENDSCAN
			
			
				IF USED(lcCursorConfigUtValidaSt)
					fecha(lcCursorConfigUtValidaSt)
				ENDIF
					
				lcSQLFinal = ""
				lcContador = 0
				
				SELECT ucrsCampanhasTodosProdutos
				GO TOP 
				SCAN FOR !EMPTY(ucrsCampanhasTodosProdutos.ref)
					lcRef = ucrsCampanhasTodosProdutos.ref
					SELECT ucrsCampanhasTodos_artigos 
					GO TOP 
					**TEXT TO lcSQL NOSHOW TEXTMERGE 
						update ucrsCampanhasTodos_artigos set campanhas = campanhas + '('+astr(ucrsCampanhasList.id)+')' WHERE ref = ALLTRIM(lcRef)
					**ENDTEXT 
					SELECT ucrsCampanhasTodosProdutos
*!*						lcSQLFinal = lcSQLFinal + CHR(13) + CHR(10) + lcSql
*!*						lcContador = lcContador + 1
*!*						IF lcContador > 400
*!*							lcContador = 0		

*!*							IF !uf_gerais_actGrelha("","",lcSQLFinal)
*!*								uf_perguntalt_chama("Ocorreu uma anomalia ao verificar a defini��o das campanhas.","OK","",16)
*!*								RETURN .f.
*!*							ENDIF
*!*							
*!*							lcSQLFinal = ""
*!*						ENDIF 
				ENDSCAN 
				IF USED("ucrsRefsCamp")
					fecha("ucrsRefsCamp")
				ENDIF 
				
*!*					IF !EMPTY(lcSQLFinal)
*!*						IF !uf_gerais_actGrelha("","",lcSQLFinal)
*!*							uf_perguntalt_chama("Ocorreu um problema ao verificar as defini��es das campanhas.","OK","",16)
*!*							RETURN .f.
*!*						ENDIF
*!*					ENDIF
			ENDIF 
**MESSAGEBOX('STOF')
			** Produto de Oferta ***
			SELECT ucrsCampanhasProdutosOferta
			LOCATE FOR ucrsCampanhasList.id == ucrsCampanhasProdutosOferta.campanhas_id 
			IF FOUND()
			
				SELECT ucrsCampanhasProdutosOferta
				GO Top
				SCAN FOR ucrsCampanhasList.id == ucrsCampanhasProdutosOferta.campanhas_id

					lcExcecao = ucrsCampanhasProdutosOferta.excecao
					lcTop = 10000
					lcRef = ALLTRIM(ucrsCampanhasProdutosOferta.artigo)
					lcMarca = ALLTRIM(ucrsCampanhasProdutosOferta.marca)
					lctxIva = ASTR(ucrsCampanhasProdutosOferta.txIva)
					lcFamilia 	= ALLTRIM(ucrsCampanhasProdutosOferta.familia)
					lcLab = ALLTRIM(ucrsCampanhasProdutosOferta.lab)
					lcDci = ALLTRIM(ucrsCampanhasProdutosOferta.dci)
					lcGenerico	= ucrsCampanhasProdutosOferta.generico
					lcOperadorLogico	= IIF(ALLTRIM(ucrsCampanhasProdutosOferta.OperadorLogico)==">", "maior", IIF(ALLTRIM(ucrsCampanhasProdutosOferta.OperadorLogico)=="<", "menor", "igual"))
					lcStock = ucrsCampanhasProdutosOferta.stock
					lcServicos = ucrsCampanhasProdutosOferta.servicos
					lcInactivos	= IIF(ucrsCampanhasProdutosOferta.inactivo, 1, 0)
					lcParafarmacia 	= IIF(ucrsCampanhasProdutosOferta.parafarmacia, 1,0)
					lcComFicha		= IIF(ucrsCampanhasProdutosOferta.comficha, 1, 0)
					lcT5			= IIF(ucrsCampanhasProdutosOferta.t5, 1, 0)
					lcRecurso = ""
				
					** Trata Acentos
					lcRef = CHRTRAN(lcRef, "�������������������", "aouaaaeeeiiiooouuuc")
					lcMarca = CHRTRAN(lcMarca, "�������������������", "aouaaaeeeiiiooouuuc")
					lcFamilia = CHRTRAN(lcFamilia, "�������������������", "aouaaaeeeiiiooouuuc")
					lcLab = CHRTRAN(lcLab, "�������������������", "aouaaaeeeiiiooouuuc")
					lcDci = CHRTRAN(lcDci, "�������������������", "aouaaaeeeiiiooouuuc")

					&& Atribui nova mente todos os produtos para filtrar
					lcCursorProdOfertaValidaSt = "ucrsCampanhasProdOfertaValidaSt"
					SELECT * FROM ucrsCampanhasTodosProdutosOriginal INTO CURSOR &lcCursorProdOfertaValidaSt READWRITE 

					uf_campanhas_filtraProdutos(lcCursorProdOfertaValidaSt, lcTop, lcRef, lcMarca, lctxIva, lcFamilia, lcLab, lcDci, lcGenerico, lcOperadorLogico, lcStock, lcServicos, lcInactivos, lcParafarmacia, lcComFicha, lcT5, lcRecurso)

					IF EMPTY(lcExcecao) && N�o � excecao
				
	*!*						delete ucrsCampanhasTodosProdutosOferta;
	*!*						FROM;
	*!*							ucrsCampanhasTodosProdutosOferta;
	*!*							LEFT JOIN &lcCursorProdOfertaValidaSt ON ALLTRIM(&lcCursorProdOfertaValidaSt..ref) == ALLTRIM(ucrsCampanhasTodosProdutosOferta.ref);
	*!*						WHERE;
	*!*							&lcCursorProdOfertaValidaSt..ref is null 
			
						SELECT ucrsCampanhasTodosProdutosOferta.ref;
						FROM;
							ucrsCampanhasTodosProdutosOferta;
							LEFT JOIN &lcCursorProdOfertaValidaSt ON ALLTRIM(&lcCursorProdOfertaValidaSt..ref) == ALLTRIM(ucrsCampanhasTodosProdutosOferta.ref);
						WHERE;
							&lcCursorProdOfertaValidaSt..ref is null INTO CURSOR ucrsTempDeleteSt READWRITE 
							
						SELECT ucrsTempDeleteSt	
						GO TOP
						SCAN 
							SELECT ucrsCampanhasTodosProdutosOferta
							GO TOP
							SELECT ucrsCampanhasTodosProdutosOferta
							LOCATE FOR ALLTRIM(ucrsCampanhasTodosProdutosOferta.ref) == ALLTRIM(ucrsTempDeleteSt.ref)
							IF FOUND()
								SELECT ucrsCampanhasTodosProdutosOferta
								DELETE
							ENDIF						
						ENDSCAN
						
						IF USED("ucrsTempDeleteSt")
							fecha("ucrsTempDeleteSt")
						ENDIF 	
			
					ELSE && Excecao, todos os produtos excepto os devolvidos
						
	*!*						DELETE ucrsCampanhasTodosProdutosOferta;
	*!*						FROM;
	*!*							ucrsCampanhasTodosProdutosOferta;
	*!*							LEFT JOIN &lcCursorProdOfertaValidaSt ON ALLTRIM(&lcCursorProdOfertaValidaSt..ref) == ALLTRIM(ucrsCampanhasTodosProdutosOferta.ref);
	*!*						WHERE;
	*!*							&lcCursorProdOfertaValidaSt..ref is not null
							
						SELECT ucrsCampanhasTodosProdutosOferta.ref;
						FROM;
							ucrsCampanhasTodosProdutosOferta;
							LEFT JOIN &lcCursorProdOfertaValidaSt ON ALLTRIM(&lcCursorProdOfertaValidaSt..ref) == ALLTRIM(ucrsCampanhasTodosProdutosOferta.ref);
						WHERE;
							&lcCursorProdOfertaValidaSt..ref is not null INTO CURSOR ucrsTempDeleteSt READWRITE 
							
						SELECT ucrsTempDeleteSt	
						GO TOP
						SCAN 
							SELECT ucrsCampanhasTodosProdutosOferta
							GO TOP
							SELECT ucrsCampanhasTodosProdutosOferta
							LOCATE FOR ALLTRIM(ucrsCampanhasTodosProdutosOferta.ref) == ALLTRIM(ucrsTempDeleteSt.ref)
							IF FOUND()
								SELECT ucrsCampanhasTodosProdutosOferta
								DELETE
							ENDIF						
						ENDSCAN
						
						IF USED("ucrsTempDeleteSt")
							fecha("ucrsTempDeleteSt")
						ENDIF 		
					ENDIF
					
				ENDSCAN 
				
				IF USED(lcCursorProdOfertaValidaSt)
					fecha(lcCursorProdOfertaValidaSt)
				ENDIF
				
				lcSQLFinal = ""
				lcContador = 0
				
				SELECT ucrsCampanhasTodosProdutosOferta
				GO Top
				SCAN FOR !EMPTY(ucrsCampanhasTodosProdutosOferta.ref)
					lcRef = ucrsCampanhasTodosProdutosOferta.ref
					SELECT ucrsCampanhasTodos_artigosofertas
					GO TOP 
					**TEXT TO lcSQL NOSHOW TEXTMERGE 
						update ucrsCampanhasTodos_artigosofertas set campanhas = campanhas + '('+astr(ucrsCampanhasList.id)+')' WHERE ref = ALLTRIM(lcRef)
					**ENDTEXT 
					
*!*						lcSQLFinal = lcSQLFinal + CHR(13) + CHR(10) + lcSql
*!*						lcContador = lcContador + 1
*!*						IF lcContador > 400
*!*							lcContador = 0		

*!*							IF !uf_gerais_actGrelha("","",lcSQLFinal)
*!*								uf_perguntalt_chama("Ocorreu um problema ao verificar defini��o das campanhas.","OK","",16)
*!*								RETURN .f.
*!*							ENDIF
*!*							
*!*							lcSQLFinal = ""
*!*						ENDIF 
				ENDSCAN 
				IF USED("ucrsRefsCamp")
					fecha("ucrsRefsCamp")
				ENDIF 
				
				IF !EMPTY(lcSQLFinal)
					IF !uf_gerais_actGrelha("","",lcSQLFinal)
						uf_perguntalt_chama("Ocorreu um problema ao verificar defini��o das campanhas.","OK","",16)
						RETURN .f.
					ENDIF
				ENDIF

			ENDIF 	
		ENDIF	
		regua(1,7,"A carregar campanhas...")
	ENDSCAN 


	**
	IF USED("ucrsCampanhasTodosProdutos")
		fecha("ucrsCampanhasTodosProdutos")
	ENDIF 
	**
	IF USED("ucrsCampanhasList")
		fecha("ucrsCampanhasList")
	ENDIF 
	**
	IF USED("ucrsCampanhasConfigUt")
		fecha("ucrsCampanhasConfigUt")
	ENDIF 
	**
	IF USED("ucrsCampanhasConfigSt")
		fecha("ucrsCampanhasConfigSt")
	ENDIF 
	**
	IF USED("ucrsCampanhasProdutosOferta")
		fecha("ucrsCampanhasProdutosOferta")
	ENDIF 
	
	IF USED("ucrsCampanhasTodosUtentes")
		fecha("ucrsCampanhasTodosUtentes")
	ENDIF 
	
	IF USED("ucrsCampanhasTodosProdutosOferta")
		fecha("ucrsCampanhasTodosProdutosOferta")
	ENDIF 

	IF USED("ucrsCampanhasTodosProdutosOriginal")
		fecha("ucrsCampanhasTodosProdutosOriginal")
	ENDIF 
	
	IF USED("ucrsCampanhasTodosUtentesOriginal")
		fecha("ucrsCampanhasTodosUtentesOriginal")
	ENDIF 
	
	IF USED("ucrsCampanhasTodos_artigos")
		fecha("ucrsCampanhasTodos_artigos")
	ENDIF 
	
	IF USED("ucrsCampanhasTodos_clientes")
		fecha("ucrsCampanhasTodos_clientes")
	ENDIF 

	regua(2)
ENDFUNC 
