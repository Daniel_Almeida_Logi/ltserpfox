
**
FUNCTION uf_rebatimentos_chama
	LPARAMETERS lcId
	IF EMPTY(lcId)
		lcID = 0
	ENDIF 
	
	PUBLIC myRebatimentosIntroducao, myRebatimentosAlteracao
	myRebatimentosIntroducao = .f.
	myRebatimentosAlteracao = .f.
		
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		SELECT * FROM rebatimentos WHERE id = <<lcID>>
	ENDTEXT  

	If !uf_gerais_actGrelha("", "ucrsRebatimentos", lcSql)
		uf_perguntalt_chama("N�o foi possivel carregar informa��o do Rebatimento.","OK","",32)
		RETURN .f.
	ENDIF 

	IF TYPE("REBATIMENTOS")=="U"
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			SELECT* FROM rebatimentos_ut WHERE rebatimentos_id = <<lcID>>
		ENDTEXT  
		If !uf_gerais_actGrelha("", "ucrsRebatimentosUt", lcSql)
			uf_perguntalt_chama("N�o foi possivel carregar informa��o do Rebatimento. (Regras Utentes)","OK","",32)
			RETURN .f.
		ENDIF 
		
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			Select * from rebatimentos_pNiveis WHERE rebatimentos_id = <<lcID>>
		ENDTEXT  
		IF !uf_gerais_actGrelha("", "ucrsRebatimentosNiveisPontos", lcSql)
			uf_perguntalt_chama("N�o foi possivel carregar informa��o da campanha. (Niveis Pontos)","OK","",32)
			RETURN .f.
		ENDIF 
	
		**
		DO FORM REBATIMENTOS
	ELSE
		
		
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			SELECT * FROM rebatimentos_ut WHERE rebatimentos_id = <<lcID>>
		ENDTEXT  
		If !uf_gerais_actGrelha("REBATIMENTOS.GridUt", "ucrsRebatimentosUt", lcSql)
			uf_perguntalt_chama("N�o foi possivel carregar informa��o do Rebatimento. (Regras Utentes)","OK","",32)
			RETURN .f.
		ENDIF 
		
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			Select * from rebatimentos_pNiveis WHERE rebatimentos_id = <<lcID>>
		ENDTEXT  
		IF !uf_gerais_actGrelha("REBATIMENTOS.PageFrame1.Page1.GridNiveis", "ucrsRebatimentosNiveisPontos", lcSql)
			uf_perguntalt_chama("N�o foi possivel carregar informa��o da campanha. (Niveis Pontos)","OK","",32)
			RETURN .f.
		ENDIF 
			
	
		REBATIMENTOS.show
	ENDIF 

	uf_REBATIMENTOS_alternaMenu()
ENDFUNC 



**
FUNCTION uf_REBATIMENTOS_CarregaMenu
	
	** Confgura Barra Lateral
	WITH REBATIMENTOS.menu1
		.adicionaOpcao("pesquisar", "Pesquisar", myPath + "\imagens\icons\lupa_w.png", "uf_pesqREBATIMENTOS_Chama","F2")
		.adicionaOpcao("novo","Novo",myPath + "\imagens\icons\mais_w.png","uf_REBATIMENTOS_novo","F3")
		.adicionaOpcao("editar","Editar",myPath + "\imagens\icons\lapis_w.png","uf_REBATIMENTOS_editar","F4")
	ENDWITH 
	
	uf_REBATIMENTOS_alternaMenu()

ENDFUNC



FUNCTION uf_REBATIMENTOS_alternaMenu

	IF myREBATIMENTOSIntroducao == .t. OR myREBATIMENTOSAlteracao == .t. 
		lcReadOnly = .f.
		REBATIMENTOS.menu1.estado("pesquisar,novo,editar", "HIDE", "Gravar", .t., "Cancelar", .T.)
	ELSE
		lcReadOnly = .t.
		REBATIMENTOS.menu1.estado("pesquisar,novo,editar", "SHOW", "Gravar", .f., "Sair", .T.)
	ENDIF 
	
	WITH REBATIMENTOS
		.descricao.readonly = lcReadOnly 
		.dtinicio.readonly = lcReadOnly 
		.dtFim.readonly = lcReadOnly 
		IF !EMPTY(lcReadOnly)
			.inativo.enable(.f.)
		ELSE
			.inativo.enable(.t.)
		ENDIF 
	ENDWITH
	
	
	WITH REBATIMENTOS.PageFrame1.Page1
		FOR i=1 TO .GridNiveis.columncount
			IF .GridNiveis.Columns(i).name == "pontos" OR .GridNiveis.Columns(i).name == "valor"
				.GridNiveis.Columns(i).readonly = lcReadOnly 
				.GridNiveis.Columns(i).Text1.readonly = lcReadOnly 
			ENDIF 
		ENDFOR
	ENDWITH 
	
	REBATIMENTOS.refresh
ENDFUNC


**
FUNCTION uf_REBATIMENTOS_NovaPesqUt
	IF TYPE("PESQUTENTES") != "U"
		uf_PESQUTENTES_sair()
	ENDIF 
	
	uf_pesqutentes_chama('REBATIMENTOS')

ENDFUNC 


**
FUNCTION uf_REBATIMENTOS_selPesquisaUt
	
	LOCAL lcSQL, lcTop, lcNome, lcNo, lcEstab, lcMorada, lcNcont, lcNCartao, lcTipo, lcProfissao, lcSexo, lcNascimento
	LOCAL lcTlmvl, lcOperador, lcIdade, lcObs, lcMail, lcEFR, lcPatologia, lcEntre, lcE, lcRef
	LOCAL lcInactivo, lctlf, lcutenteno, lcnbenef, lcValTouch, lcValEntidadeClinica,lcID
	
	** var defaults
	STORE '' TO lcSQL, lcNome, lcMorada, lcNcont, lcNCartao, lcTipo, lcProfissao, lcSexo
	STORE '' TO lcObs, lcEFR, lcPatologia, lcRef, lctlf, lcutenteno, lcnbenef,lcID
	STORE 0 TO lcNo, lcTlmvl, lcMail, lcInactivo, lcValTouch, lcValEntidadeClinica
	STORE '19000101' TO lcNascimento, lcEntre

	lcEstab 		= "-1"
	lcOperador 		= "="
	lcIdade			= "-9999"
	lcE				= '30000101'
	
	** validar se front-office ou back-oficce
	LOCAL lcObj
	
	lcObj = "PESQUTENTES.pageframe1.page1"
	lcId = ALLTRIM(&lcObj..id.value)
		
	** Atribui parametros de pesquisa
	
	** campos dispon�veis em front-office e back-office
	lcTop 		= IIF(INT(VAL(&lcObj..topo.value))== 0,100000,INT(VAL(&lcObj..topo.value)))
	lcNome		= ALLTRIM(&lcObj..nome.value)
	lcNo		= IIF(EMPTY(&lcObj..numero.value), 0, STRTRAN(&lcObj..numero.value,' ',''))
	lcEstab		= IIF(EMPTY(&lcObj..estab.value), "-1", &lcObj..estab.value)
	lcMorada	= ALLTRIM(&lcObj..morada.value)
	lcNcont		= ALLTRIM(&lcObj..ncont.value)
	lcNCartao	= ALLTRIM(&lcObj..nCartao.value)
	lctlf		= ALLTRIM(&lcObj..tlf.value)
	lcutenteno	= ALLTRIM(&lcObj..utenteno.value)
	lcnbenef	= ALLTRIM(&lcObj..nbenef.value)
	lcTipo		= ALLTRIM(&lcObj..cmbTipo.value)
	lcProfissao	= ALLTRIM(&lcObj..profissao.value)
	lcSexo		= IIF(&lcObj..sexo.value == 'M/F', '', &lcObj..sexo.value)
	lcOperador	= ALLTRIM(&lcObj..OperadorLogico.value)
	lcInactivo	= IIF(&lcObj..image4.tag == "true", 1, 0)
	lcID		= ALLTRIM(&lcObj..id.value)
				

	IF &lcObj..label6.caption == 'Idade'
		IF EMPTY(&lcObj..idade.value)
			lcIdade	= "-9999"
			lcOperador = '>'
		ELSE
			lcIdade	= &lcObj..idade.value
			lcNascimento = '19000101'
		ENDIF
	ENDIF

	IF &lcObj..label6.caption == 'Dt. Nasc.'
		lcNascimento = IIF(EMPTY(ALLTRIM(&lcObj..txtnascimento.value)), '19000101', uf_gerais_getDate(&lcObj..txtnascimento.value, "SQL"))
		lcNascimentoReport = IIF(EMPTY(ALLTRIM(&lcObj..txtnascimento.value)), '1900.01.01', uf_gerais_getDate(&lcObj..txtnascimento.value))
		lcIdade	= "-9999"
		lcOperador = '>'
	ELSE
		lcNascimento = '19000101'
		lcNascimentoReport = '1900.01.01'
	ENDIF 
	lcNascimento2 = CTOD(lcNascimentoReport)

	lcMail	 	= IIF(&lcObj..image2.tag == "true", 1, 0)
	lcTlmvl 	= IIF(&lcObj..image3.tag == "true", 1, 0)
	lcObs		= ALLTRIM(&lcObj..observacoes.value)
	lcEFR		= ALLTRIM(&lcObj..efr.value)
		
	** patologias
	lcPatologia	= ALLTRIM(&lcObj..patologias.value)

	** produto
	IF used("uCrsProdutosCLI")
		SELECT uCrsProdutosCLI
		GO TOP
		SCAN
			lcRef = lcRef + astr(uCrsProdutosCLI.ref) + '|'
		ENDSCAN
	ENDIF 
		
	IF !EMPTY(lcRef)
		&& retirar pipe no final
		lcRef=substr(lcRef, 1, len(lcRef)-1)
	ENDIF 

	** Periodo de tempo
	lcEntre		= IIF(EMPTY(ALLTRIM(&lcObj..txtEntre.value)), '19000101', uf_gerais_getDate(&lcObj..txtEntre.value, "SQL"))
	lcE			= IIF(EMPTY(ALLTRIM(&lcObj..txtE.value)), '30000101', uf_gerais_getDate(&lcObj..txtE.value, "SQL"))
	
	
	lcEntre2	= CTOD(ALLTRIM(&lcObj..txtEntre.value))
	lcE2		= CTOD(ALLTRIM(&lcObj..txtE.value))
	

	lcRegraTxt = ""
	IF !EMPTY(lcNome)
		lcRegraTxt = lcRegraTxt + " Nome: " + ALLTRIM(lcNome) + ";"
	ENDIF 
	IF !EMPTY(lcNo)
		lcRegraTxt = lcRegraTxt + " Nr.: " + ALLTRIM(lcNo) + ";"
	ENDIF 
	IF lcEstab != "-1"
		lcRegraTxt = lcRegraTxt + " Dep.: " + lcEstab + ";"
	ENDIF 
	IF !EMPTY(lcID)
		lcRegraTxt = lcRegraTxt + " ID: " + ALLTRIM(lcID) + ";"
	ENDIF 
	IF !EMPTY(lcMorada)
		lcRegraTxt = lcRegraTxt + " Morada: " + ALLTRIM(lcMorada) + ";"
	ENDIF 
	IF !EMPTY(lcNcont)
		lcRegraTxt = lcRegraTxt + " Contrib.: " + ALLTRIM(lcNcont) + ";"
	ENDIF 
	IF !EMPTY(lcNCartao)
		lcRegraTxt = lcRegraTxt + " Cart�o: " + ALLTRIM(lcNCartao) + ";"
	ENDIF 
	IF !EMPTY(lcTipo)
		lcRegraTxt = lcRegraTxt + " Tipo: " + ALLTRIM(lcTipo) + ";"
	ENDIF 
	IF !EMPTY(lcProfissao)
		lcRegraTxt = lcRegraTxt + " Profissao: " + ALLTRIM(lcProfissao) + ";"
	ENDIF 
	IF !EMPTY(lcSexo)
		lcRegraTxt = lcRegraTxt + " Sexo: " + ALLTRIM(lcSexo) + ";"
	ENDIF
	IF !EMPTY(lctlf)
		lcRegraTxt = lcRegraTxt + " Telefone/Tlm.: " + ALLTRIM(lctlf) + ";"
	ENDIF 
	IF !EMPTY(lcTlmvl)
		lcRegraTxt = lcRegraTxt + " Tlmvl: Sim;"
	ENDIF

	IF &lcObj..label6.caption == 'Idade'
		IF !EMPTY(&lcObj..idade.value)
			lcRegraTxt = lcRegraTxt + " Idade " + ALLTRIM(lcOperador) + lcIdade + ";"
		ENDIF
	ENDIF
	IF &lcObj..label6.caption == 'Dt. Nasc.'
		IF lcNascimento != '19000101'
			lcRegraTxt = lcRegraTxt + " Dt. Nasc.: " + lcNascimento + ";"
		ENDIF
	ENDIF
	IF !EMPTY(lcObs)
		lcRegraTxt = lcRegraTxt + " Obs: " + ALLTRIM(lcObs) + ";"
	ENDIF
	IF !EMPTY(lcMail)
		lcRegraTxt = lcRegraTxt + " eMail: Sim;"
	ENDIF
	IF !EMPTY(lcEFR)
		lcRegraTxt = lcRegraTxt + " EFR: " + ALLTRIM(lcEFR) + ";"
	ENDIF
	IF !EMPTY(lcPatologia)
		lcRegraTxt = lcRegraTxt + " Patologia: " + ALLTRIM(lcPatologia) + ";"
	ENDIF
	IF !EMPTY(lcnbenef)
		lcRegraTxt = lcRegraTxt + " Nbenef: " + ALLTRIM(lcnbenef) + ";"
	ENDIF
	IF !EMPTY(lcInactivo)
		lcRegraTxt = lcRegraTxt + " Incl. Inativos: Sim;"
	ENDIF
	IF !EMPTY(lcRef)
		lcRegraTxt = lcRegraTxt + "  Adquiriu os seguintes Produtos: " + lcRef + IIF(lcEntre="19000101"," desde sempre", " entre: " + lcEntre + " e " + lcE) + ";"
	ENDIF 

	IF empty(lcno)
		lcNo = "0"
	ENDIF 

	SELECT ucrsREBATIMENTOSUt
	APPEND BLANK 
	replace ucrsREBATIMENTOSUt.rebatimentos_id WITH ucrsRebatimentos.id
	Replace ucrsREBATIMENTOSUt.regra WITH lcRegraTxt
	Replace ucrsREBATIMENTOSUt.nome WITH lcNome
	Replace ucrsREBATIMENTOSUt.No WITH VAL(lcNo)
	Replace ucrsREBATIMENTOSUt.Estab WITH Val(lcEstab)
	Replace ucrsREBATIMENTOSUt.Morada WITH lcMorada
	Replace ucrsREBATIMENTOSUt.Ncont WITH lcNcont
	Replace ucrsREBATIMENTOSUt.Ncartao WITH lcNCartao
	Replace ucrsREBATIMENTOSUt.Tipo WITH lcTipo
	Replace ucrsREBATIMENTOSUt.Profissao WITH lcProfissao
	Replace ucrsREBATIMENTOSUt.Sexo WITH lcSexo
	Replace ucrsREBATIMENTOSUt.Tlmvl WITH IIF(EMPTY(lcTlmvl),.f.,.t.)
	Replace ucrsREBATIMENTOSUt.Operador WITH lcOperador
	Replace ucrsREBATIMENTOSUt.Idade WITH VAL(lcIdade)
	Replace ucrsREBATIMENTOSUt.Obs WITH lcObs
	Replace ucrsREBATIMENTOSUt.Mail WITH IIF(EMPTY(lcMail),.f.,.t.)
	Replace ucrsREBATIMENTOSUt.EntPla WITH lcEFR
	Replace ucrsREBATIMENTOSUt.Patologia WITH lcPatologia
	Replace ucrsREBATIMENTOSUt.Ref WITH lcRef
	Replace ucrsREBATIMENTOSUt.Entre WITH lcEntre2
	Replace ucrsREBATIMENTOSUt.E WITH lcE2
	Replace ucrsREBATIMENTOSUt.sempre WITH IIF(lcEntre="19000101",.t.,.f.)
	Replace ucrsREBATIMENTOSUt.DtNasc WITH lcNascimento2 
	Replace ucrsREBATIMENTOSUt.Inactivo WITH IIF(EMPTY(lcInactivo),.f.,.t.)
	Replace ucrsREBATIMENTOSUt.Tlf WITH lctlf
	Replace ucrsREBATIMENTOSUt.Nbenef WITH lcnbenef
&&	Replace ucrsREBATIMENTOSUt.Nbenef2
	Replace ucrsREBATIMENTOSUt.Touch WITH .f.
	Replace ucrsREBATIMENTOSUt.EntCompart WITH .f.
	Replace ucrsREBATIMENTOSUt.utId WITH lcID
	
	
	
	REBATIMENTOS.GridUt.refresh
	
	
	uf_PESQUTENTES_sair()
ENDFUNC 


**
FUNCTION uf_REBATIMENTOS_ApagaPesqUt
	SELECT ucrsREBATIMENTOSUt
	DELETE 
	TRY
		SELECT ucrsREBATIMENTOSUt
		SKIP -1
	ENDTRY
	REBATIMENTOS.GridUt.refresh
ENDFUNC 



**
FUNCTION uf_REBATIMENTOS_novo

	DELETE FROM ucrsREBATIMENTOS
	DELETE FROM ucrsREBATIMENTOSUt
	

	myREBATIMENTOSIntroducao = .t.
	uf_REBATIMENTOS_alternaMenu()
	
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		SELECT ISNULL(MAX(id),0) +1 as id FROM REBATIMENTOS
	ENDTEXT 
	If !uf_gerais_actGrelha("", "ucrsREBATIMENTOSId", lcSql)
		uf_perguntalt_chama("N�o foi possivel carregar informa��o do Rebatimento.","OK","",32)
		RETURN .f.
	ENDIF 

	SELECT ucrsREBATIMENTOSId
	lcID = 	ucrsREBATIMENTOSId.id
	SELECT ucrsRebatimentos
	APPEND BLANK
	replace ucrsRebatimentos.id WITH lcID
	replace ucrsRebatimentos.dataInicio WITH DATE()
	replace ucrsRebatimentos.dataFim WITH DATE()
	replace ucrsRebatimentos.site WITH mysite
	
	REBATIMENTOS.GridUt.refresh
	
	REBATIMENTOS.refresh
ENDFUNC


**
FUNCTION uf_Rebatimentos_editar

	myRebatimentosAlteracao = .t.
	uf_REBATIMENTOS_alternaMenu()
ENDFUNC



**
FUNCTION uf_Rebatimentos_gravar
	LOCAL lcExecuteSQL
	lcExecuteSQL = ""
		
	SELECT ucrsRebatimentos		
	IF EMPTY(ALLTRIM(ucrsRebatimentos.descricao))
		uf_perguntalt_chama("Existem campos obrigat�rios por preencher.","OK","",64)
		RETURN .f.
	ENDIF 
		
	SELECT ucrsRebatimentos
	lcSQL = ""
	
	IF MyRebatimentosIntroducao == .t.
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			DELETE FROM rebatimentos_ut WHERE rebatimentos_id = <<ucrsRebatimentos.id>>
			DELETE FROM rebatimentos_pNiveis WHERE rebatimentos_id = <<ucrsRebatimentos.id>>
			
			INSERT INTO rebatimentos (
				id
				,descricao
				,dataInicio
				,dataFim
				,inativo
				,site
				,id_us
				,id_us_alt
				,data_cri
				,data_alt
			) VALUES(
				<<ucrsRebatimentos.id>>
				,'<<ALLTRIM(ucrsRebatimentos.descricao)>>'
				,'<<uf_gerais_getdate(ucrsRebatimentos.dataInicio,"SQL")>>'
				,'<<uf_gerais_getdate(ucrsRebatimentos.dataFim,"SQL")>>'
				,<<IIF(ucrsRebatimentos.inativo,1,0)>>
				,'<<ALLTRIM(ucrsRebatimentos.site)>>'
				,<<ch_userno>>
				,<<ch_userno>>
				,getdate()
				,getdate()
			)
		
		ENDTEXT 
	ELSE
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			
			DELETE FROM rebatimentos_ut WHERE rebatimentos_id = <<ucrsRebatimentos.id>>
			DELETE FROM rebatimentos_pNiveis WHERE rebatimentos_id = <<ucrsRebatimentos.id>>

			UPDATE rebatimentos
			SET 
				descricao = '<<ALLTRIM(ucrsRebatimentos.descricao)>>'
				,dataInicio = '<<uf_gerais_getdate(ucrsRebatimentos.dataInicio,"SQL")>>'
				,dataFim = '<<uf_gerais_getdate(ucrsRebatimentos.dataFim,"SQL")>>'
				,inativo = <<IIF(ucrsRebatimentos.inativo,1,0)>>
				,site = '<<ALLTRIM(ucrsRebatimentos.site)>>'
				,id_us_alt = <<ch_userno>>
				,data_alt = getdate()
			WHERE
				rebatimentos.id = <<ucrsRebatimentos.id>>
		
		ENDTEXT 
	ENDIF 
	lcExecuteSQL  = lcExecuteSQL + CHR(13) + CHR(13) + lcSQL
	
	SELECT ucrsRebatimentosUt
	GO Top
	SCAN
		lcSQL = ""
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			INSERT INTO rebatimentos_ut (
				id
				,rebatimentos_id
				,nome
				,No
				,Estab
				,Morada
				,Ncont
				,Ncartao
				,Tipo
				,Profissao
				,Sexo
				,Tlmvl
				,Operador
				,Idade
				,Obs
				,Mail
				,EntPla
				,Patologia
				,Ref
				,Entre
				,E
				,sempre
				,DtNasc
				,Inactivo
				,Tlf
				,Nbenef
				,Nbenef2
				,Touch
				,EntCompart
				,utId
				,regra
			) VALUES(
				(select ISNULL(MAX(id),0) + 1 from rebatimentos_ut)
				,<<ucrsRebatimentos.id>>
				,'<<ucrsRebatimentosUt.nome>>'
				,<<ucrsRebatimentosUt.No>>
				,<<ucrsRebatimentosUt.Estab>>
				,'<<ucrsRebatimentosUt.Morada>>'
				,'<<ucrsRebatimentosUt.Ncont>>'
				,'<<ucrsRebatimentosUt.Ncartao>>'
				,'<<ucrsRebatimentosUt.Tipo>>'
				,'<<ucrsRebatimentosUt.Profissao>>'
				,'<<ucrsRebatimentosUt.Sexo>>'
				,<<IIF(ucrsRebatimentosUt.Tlmvl,1,0)>>
				,'<<ucrsRebatimentosUt.Operador>>'
				,<<ucrsRebatimentosUt.Idade>>
				,'<<ucrsRebatimentosUt.Obs>>'
				,<<IIF(ucrsRebatimentosUt.Mail,1,0)>>
				,'<<ucrsRebatimentosUt.EntPla>>'
				,'<<ucrsRebatimentosUt.Patologia>>'
				,'<<ucrsRebatimentosUt.Ref>>'
				,'<<uf_gerais_getdate(ucrsRebatimentosUt.Entre,"SQL")>>'
				,'<<uf_gerais_getdate(ucrsRebatimentosUt.E,"SQL")>>'
				,<<IIF(ucrsRebatimentosUt.sempre,1,0)>>
				,'<<uf_gerais_getdate(ucrsRebatimentosUt.DtNasc,"SQL")>>'
				,<<IIF(ucrsRebatimentosUt.Inactivo,1,0)>>
				,'<<ucrsRebatimentosUt.Tlf>>'
				,'<<ucrsRebatimentosUt.Nbenef>>'
				,'<<ucrsRebatimentosUt.Nbenef2>>'
				,<<IIF(ucrsRebatimentosUt.Touch,1,0)>>
				,<<IIF(ucrsRebatimentosUt.EntCompart,1,0)>>
				,'<<ucrsRebatimentosUt.utId>>'
				,'<<ucrsRebatimentosUt.regra>>'
			)
		ENDTEXT
		lcExecuteSQL  = lcExecuteSQL + CHR(13) + CHR(13) + lcSQL
		
	ENDSCAN 
	
	SELECT ucrsRebatimentosNiveisPontos
	GO Top
	SCAN
		lcSQL = ""
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			INSERT INTO rebatimentos_pNiveis (
				id
				,rebatimentos_id
				,nivel
				,valor
				,pontos
			) VALUES(
				(select ISNULL(MAX(id),0) + 1 from rebatimentos_pNiveis)
				,<<ucrsRebatimentos.id>>
				,<<ucrsRebatimentosNiveisPontos.nivel>>
				,<<ucrsRebatimentosNiveisPontos.valor>>
				,<<ucrsRebatimentosNiveisPontos.pontos>>
			)
		ENDTEXT
		lcExecuteSQL  = lcExecuteSQL + CHR(13) + CHR(13) + lcSQL
		
	ENDSCAN 
	
	lcExecuteSQL = uf_gerais_trataPlicasSQL(lcExecuteSQL) 


	TEXT TO lcSQL NOSHOW TEXTMERGE
		exec up_gerais_execSql 'REBATIMENTOS - Insert', 1,'<<lcExecuteSQL>>', '', '', '' , '', ''
	ENDTEXT 

*!*	_cliptext = lcSQL 
*!*	MESSAGEBOX(lcSQL )	
	
	If !uf_gerais_actGrelha("","",lcSQL)
		uf_perguntalt_chama("OCORREU UM ERRO A GRAVAR A CAMPANHA. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		Return .f.
	ENDIF

	myRebatimentosIntroducao = .f.
	myRebatimentosAlteracao = .f.
	
	uf_REBATIMENTOS_chama(ucrsRebatimentos.id)
	uf_REBATIMENTOS_alternaMenu()
ENDFUNC 



**
FUNCTION uf_Rebatimentos_PontosNovoNivel
	LOCAL lcNovoNivel 

	lcNovoNivel = 1
	SELECT ucrsRebatimentosNiveisPontos	
 	CALCULATE MAX(nivel) TO lcNovoNivel
 	
	SELECT ucrsRebatimentosNiveisPontos
	APPEND BLANK
	Replace ucrsRebatimentosNiveisPontos.nivel WITH lcNovoNivel +1

	Rebatimentos.PageFrame1.Page1.GridNiveis.refresh
ENDFUNC 


FUNCTION uf_Rebatimentos_PontosApagaNivel
	
	SELECT ucrsRebatimentosNiveisPontos
	DELETE 
	TRY
		SELECT ucrsRebatimentosNiveisPontos
		SKIP -1
	ENDTRY
	Rebatimentos.PageFrame1.Page1.GridNiveis.refresh

ENDFUNC 



**
FUNCTION uf_Rebatimentos_sair

	IF myRebatimentosIntroducao == .t. OR myRebatimentosAlteracao == .t. 	
		If uf_perguntalt_chama("Quer mesmo cancelar? Ir� perder as �ltimas altera��es feitas.","Sim","N�o")
			myRebatimentosIntroducao = .f.
			myRebatimentosAlteracao = .f.
			
			uf_Rebatimentos_chama(ucrsRebatimentos.id)
			uf_Rebatimentos_alternaMenu()
		ENDIF 
	ELSE
		uf_rebatimentos_exit()
	ENDIF
	 
ENDFUNC 



** 
FUNCTION uf_rebatimentos_exit
	rebatimentos.hide
	rebatimentos.release
ENDFUNC 