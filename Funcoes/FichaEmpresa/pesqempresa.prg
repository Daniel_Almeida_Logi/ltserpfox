**
FUNCTION uf_PESQEMPRESA_chama

	IF !uf_gerais_validaPermUser(ch_userno,ch_grupo,'Ficha Completa da Empresa')
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ACEDER AO PAINEL FICHA COMPLETA DA EMPRESA.","OK","",48)
		RETURN .f.
	ENDIF
	
	&& Cursor dos fichaempresa
	lcSql = ''
	TEXT TO lcSql NOSHOW TEXTMERGE
		Select sel = convert(bit,0), * From empresa
	ENDTEXT
	uf_gerais_actgrelha("", "ucrsPesqEmpresa", lcSql)

	IF TYPE("PESQEMPRESA") == "U"

		DO FORM PESQEMPRESA
		
		&& Configura menu principal
	
		WITH PESQEMPRESA.menu1
			.adicionaOpcao("actualizar","Actualizar",myPath + "\imagens\icons\actualizar_w.png","uf_PESQEMPRESA_actualiza")
			*.adicionaOpcao("novo", "Novo", myPath + "\imagens\icons\mais_w.png", "uf_PESQEMPRESA_novo","N")
		ENDWITH
	
	ELSE
		PESQEMPRESA.show
	ENDIF
	
	PESQEMPRESA.refresh
ENDFUNC


**
FUNCTION uf_PESQEMPRESA_novo
	**	
	uf_fichaempresa_chama()
	uf_fichaempresa_introducao()
	uf_pesqEmpresa_sair()
ENDFUNC


**
FUNCTION uf_PESQEMPRESA_actualiza
	lcSql = ''
	TEXT TO lcSql NOSHOW TEXTMERGE
		Select 
			sel = convert(bit,0), * 
		From 
			empresa 
		WHERE 
			empresa.nomabrv like '<<pesqEmpresa.nome.value>>%' 
			AND empresa.ncont = case when '<<ALLTRIM(pesqEmpresa.contribuinte.value)>>' = '' then empresa.ncont else '<<ALLTRIM(pesqEmpresa.contribuinte.value)>>' end
	ENDTEXT
	uf_gerais_actgrelha("pesqEmpresa.gridPesq", "ucrsPesqEmpresa", lcSql)
ENDFUNC 


**
FUNCTION uf_pesqEmpresa_Escolhe
	**
	uf_fichaempresa_chama(ucrsPesqEmpresa.no,ucrsPesqEmpresa.estab)
	uf_pesqEmpresa_sair()
ENDFUNC 


**
FUNCTION uf_pesqEmpresa_sair
	
	PESQEMPRESA.hide
	PESQEMPRESA.release
	
ENDFUNC 