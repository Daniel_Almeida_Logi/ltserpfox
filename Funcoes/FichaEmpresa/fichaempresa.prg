**
DO pesqempresa

**
FUNCTION uf_fichaempresa_chama
	LPARAMETERS lcNo, lcEstab
	IF EMPTY(lcNo)
		lcNo = 0
	ENDIF 
	IF EMPTY(lcEstab)
		lcEstab = 0
	ENDIF 
		
	IF !uf_gerais_validaPermUser(ch_userno,ch_grupo,'Ficha Completa da Empresa')
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ACEDER AO PAINEL FICHA COMPLETA DA EMPRESA.","OK","",48)
		RETURN .f.
	ENDIF
	
	LOCAL lcSql
	
	IF TYPE("lcEstab") != "N"
		lcEstab = 0
	ENDIF
	
	&& Cursor dos fichaempresa
	lcSql = ''
	TEXT TO lcSql NOSHOW TEXTMERGE
		Select * From empresa (nolock) Where no = <<lcNo>> and estab = <<lcEstab>>
	ENDTEXT
	uf_gerais_actgrelha("", "uCrsEmpresa", lcSql)
		
	IF TYPE("FICHAEMPRESA") == "U"
		
		&& Cursor dos armazens
		lcSql = ''
		TEXT TO lcSql NOSHOW TEXTMERGE
			Select * From empresa_arm (nolock) Where empresa_no = <<lcNo>>
		ENDTEXT
		uf_gerais_actgrelha("", "uCrsEmpresaArm", lcSql)
		
		&& Cursor Desconto Predefinidos por Cliente
		lcSQL = ''	
		TEXT TO lcSql TEXTMERGE NOSHOW 
			select * from empresa_descclientes where empresa_no = <<ucrsempresa.no>>
		ENDTEXT
		uf_gerais_actgrelha("", "uCrsDescPredef", lcSql)
		

		DO FORM FICHAEMPRESA
		
		&& Configura menu principal
		IF TYPE("fichaempresa.menu1.pesquisar") == "U"
			WITH fichaempresa.menu1
				*.adicionaOpcao("opcoes","Op��es", myPath + "\imagens\icons\opcoes_w.png","","F1")
				.adicionaOpcao("pesquisar","Pesquisar",myPath + "\imagens\icons\lupa_w.png","uf_PESQEMPRESA_chama")
				*.adicionaOpcao("novo","Novo",myPath + "\imagens\icons\mais_w.png","uf_fichaempresa_introducao")
				.adicionaOpcao("editar","Editar",myPath + "\imagens\icons\lapis_w.png","uf_fichaempresa_alteracao")
			ENDWITH
			FICHAEMPRESA.menu_opcoes.adicionaOpcao("eliminar","Eliminar","","uf_fichaempresa_eliminar")
			FICHAEMPRESA.menu_opcoes.adicionaOpcao("criarSeriesFt","Criar Series FT","","uf_fichaempresa_criarSeriesFt")
		ENDIF
		
		FICHAEMPRESA.Menu1.gravar.img1.Picture = myPath + "\imagens\icons\guardar_w.png"
	ELSE
		
		&& Cursor dos armazens
		lcSql = ''
		TEXT TO lcSql NOSHOW TEXTMERGE
			Select * From empresa_arm (nolock) Where empresa_no = <<lcNo>>
		ENDTEXT
		uf_gerais_actgrelha("FICHAEMPRESA.Pageframe1.Page3.GridArmazens", "ucrsEmpresaArm", lcSql)
		
		&& Cursor Desconto Predefinidos por Cliente
		lcSQL = ''	
		TEXT TO lcSql TEXTMERGE NOSHOW 
			select * from empresa_descclientes where empresa_no = <<ucrsempresa.no>>
		ENDTEXT
		uf_gerais_actgrelha("FICHAEMPRESA.Pageframe1.Page3.GridDescPredef", "uCrsDescPredef", lcSql)
		
		FICHAEMPRESA.show
	ENDIF
	
	
	IF TYPE("FICHAEMPRESA") != "U"	
		FICHAEMPRESA.refresh	
	ENDIF
	IF TYPE("PESQEMPRESA") != "U"	
		PESQEMPRESA.refresh	
	ENDIF
	                                                                                                                                                                             
	uf_fichaempresa_controlaObj()
ENDFUNC


**
FUNCTION uf_fichaempresa_pesquisa
	IF !(uf_gerais_actGrelha("","ucrsTempPesqEmp",[select no as Numero, estab as Estabelecimento,nomecomp as Empresa from empresa (nolock) order by estab]))
		uf_perguntalt_chama("Ocorreu uma anomalia a verificar as empresas dispon�veis. Por favor contacte o Suporte. Obrigado.","OK","",16)
		RETURN .f.
	ELSE
		PUBLIC pPesqNo
		uf_valorescombo_chama("pPesqNo", 0, "ucrsTempPesqEmp", 2, "Numero", "Numero, Estabelecimento,Empresa",.f.,.t.)
		
		SELECT ucrsTempPesqEmp
		lcEstab = ucrsTempPesqEmp.Estabelecimento
		fecha("ucrsTempPesqEmp")
		IF !myEmpIntroducao AND !myEmpAlteracao
			uf_fichaempresa_chama(pPesqNo,lcEstab)
		ENDIF
	ENDIF
ENDFUNC 



** Configura Ecra 
FUNCTION uf_fichaempresa_controlaObj

	&& Pagina 1
	WITH fichaempresa.PageFrame1.page1
		.txtnomecomp.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.tipo.disabledbackcolor = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,RGB(255,255,255),RGB(191,223,223))
		.txtMorada.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtCodPost.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtLocal.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtFreguesia.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtConcelho.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtDistrito.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtPais.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtTelefone.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtFax.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtCCusto.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtContacto.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtctacttel.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtctaccarg.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtEmail.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtURL.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtLogo.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtRodape.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtcondGeraisDist.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtAbrev.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)

	ENDWITH 
	
	&& Pagina 2
	WITH fichaempresa.PageFrame1.page2
		.txtasspatr.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtinstss.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtCAE.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtanoconst.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtcodnatjur.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtnatjurid.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtncont.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtncontss.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtsedeid.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtestabid.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtccperm.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtBanco.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtNIB.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtSwift.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtcapsocial.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtUtilizadorDT.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtPasswordDT.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)				
		.txtcentroArb.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtBanco2.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtNIB2.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtSwift2.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
	ENDWITH 
	
	&& Pagina 3
	WITH fichaempresa.PageFrame1.page3
		.txtserie_vd.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtserie_fact.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtserie_regcli.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtserieSNS.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtSerieEnt.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtempresa.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtEntidade.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtCodigo.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtTaxEntity.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtPlafondCliente.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtReservasAdi.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.GridDescPredef.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtSite.readonly = IIF((myEmpIntroducao == .f. AND myEmpAlteracao == .f.) or emdesenvolvimento == .f.,.t.,.f.) 
		.txtIDLogitools.readonly = IIF((myEmpIntroducao == .f. AND myEmpAlteracao == .f.) or emdesenvolvimento == .f.,.t.,.f.) 
	ENDWITH 
	
	
	&& Pagina 4
	WITH fichaempresa.PageFrame1.page4
		.txtdirtec.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtinfarmed.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtassfarm.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtassfarm.enabled = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.f.,.t.)
		.txtGestcli.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.t.)
		.txtGestcli.enabled = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.f.,.t.)
		.txtCodFarm.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtAlvara.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.anfpassfx.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
	**	.txtUserSinave.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
	**	.txtPwSinave.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtCodSinave.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		
	ENDWITH 
	
	&& Pagina 5
	WITH fichaempresa.PageFrame1.page5
		.txtlocalacss.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtzonaacss.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtlocalcod.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtlocalnm.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtlocalnmAbrev.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)		
		.txtLocalPresc.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtGestcli.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.t.)
		.txtGestcli.enabled = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.f.,.t.)
	ENDWITH 
	
	WITH fichaempresa.PageFrame1.page6
		.txtGestcli.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.t.)
		.txtGestcli.enabled = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.f.,.t.)
	ENDWITH 
	
	WITH fichaempresa.PageFrame1.page7
		.mailsign.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.edtMailBody.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtemail_smtp.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtemail_username.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtemail_password.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtemail_port.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.chkemail_ssl.enabled = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.chkemail_tls.enabled = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtemail_sender.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtemail_cc.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtemail_bcc.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtemail_smtp.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
	ENDWITH 

	WITH fichaempresa.PageFrame1.page8

		.txtAboutCompany.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtMorada.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtcontactCompany.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtwebsiteURL.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtinstagramURL.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtfacebookURL.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtwhatsappContact.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtemail_cc.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
		.txtemail_bcc.readonly = IIF(myEmpIntroducao == .f. AND myEmpAlteracao == .f.,.t.,.f.)
	ENDWITH

	
	WITH fichaempresa
		DO CASE 
			CASE myEmpIntroducao == .f. AND myEmpAlteracao == .f. && ecra em modo de consulta
				
				.caption			= "Ficha Completa da Empresa"
				.txtNome.readonly	= .t.
				.pageframe1.page2.chkPmeLider.enable(.f.)
				.pageframe1.page3.chkPagCentral.enable(.f.)
				.pageframe1.page3.chkCxOperador.enable(.f.)
				.pageframe1.page3.chkPlafondcliente.enable(.f.)
				.pageframe1.page3.chkDescPred.enable(.f.)
				.pageframe1.page3.chkOrdemImpressao.enable(.f.)
				.pageframe1.page3.chkObrigaConf.enable(.f.)				
				.pageframe1.page3.chkImpRecOrdenada.enable(.f.)				
				.menu1.estado("pesquisar, editar", "SHOW", "Gravar", .f., "Sair", .t.)
				.menu_opcoes.estado("eliminar","SHOW")
				
			CASE myEmpIntroducao == .t.
					
				.caption = "Ficha Completa da Empresa - Introdu��o"
				.txtNome.readonly	= .f.
				.pageframe1.page2.chkPmeLider.enable(.t.)
				.pageframe1.page3.chkPagCentral.enable(.t.)
				.pageframe1.page3.chkCxOperador.enable(.t.)
				.pageframe1.page3.chkPlafondcliente.enable(.t.)
				.pageframe1.page3.chkDescPred.enable(.t.)				
				.pageframe1.page3.chkOrdemImpressao.enable(.t.)
				.pageframe1.page3.chkObrigaConf.enable(.t.)				
				.pageframe1.page3.chkImpRecOrdenada.enable(.t.)				
				.menu1.estado("pesquisar, editar", "HIDE", "Gravar", .t., "Sair", .t.)
				.menu_opcoes.estado("eliminar","HIDE")
			CASE myEmpAlteracao == .t.
				
				.caption = "Ficha Completa da Empresa - Altera��o"
				.txtNome.readonly	= .f.
				.pageframe1.page2.chkPmeLider.enable(.t.)				
				.pageframe1.page3.chkPagCentral.enable(.t.)
				.pageframe1.page3.chkCxOperador.enable(.t.)				
				.pageframe1.page3.chkPlafondcliente.enable(.t.)				
				.pageframe1.page3.chkDescPred.enable(.t.)	
				.pageframe1.page3.chkOrdemImpressao.enable(.t.)							
				.pageframe1.page3.chkObrigaConf.enable(.t.)	
				.pageframe1.page3.chkImpRecOrdenada.enable(.t.)				
				.menu1.estado("pesquisar, editar", "HIDE", "Gravar", .t., "Sair", .t.)
				.menu_opcoes.estado("eliminar","HIDE")
		ENDCASE 
	ENDWITH 

	uf_fichaempresa_tipoEmpresa()
	fichaempresa.refresh

ENDFUNC


** 
FUNCTION uf_fichaempresa_tipoEmpresa
	SELECT ucrsEmpresa
	
	DO CASE 
		CASE UPPER(ALLTRIM(fichaempresa.pageFrame1.page1.tipo.value)) == "FARMACIA" OR UPPER(ALLTRIM(fichaempresa.pageFrame1.page1.tipo.value)) == "PARAFARMACIA" OR UPPER(ALLTRIM(fichaempresa.pageFrame1.page1.tipo.value)) == "EMPRESA"
			fichaempresa.lbl4.visible = .t.
			fichaempresa.lbl5.visible = .f.
			fichaempresa.lbl6.visible = .f.
			fichaempresa.lbl7.visible = .t.
			fichaempresa.lbl7.left = fichaempresa.lbl5.left
		CASE UPPER(ALLTRIM(fichaempresa.pageFrame1.page1.tipo.value)) == "CLINICA"
			fichaempresa.lbl4.visible = .f.
			fichaempresa.lbl5.visible = .t.
			fichaempresa.lbl5.left = fichaempresa.lbl4.left
			fichaempresa.lbl6.visible = .f.
			fichaempresa.lbl7.visible = .t.
			fichaempresa.lbl7.left = fichaempresa.lbl6.left
		CASE UPPER(ALLTRIM(fichaempresa.pageFrame1.page1.tipo.value)) == "ARMAZEM"
			fichaempresa.lbl4.visible = .f.
			fichaempresa.lbl5.visible = .f.
			fichaempresa.lbl6.visible = .t.
			fichaempresa.lbl7.visible = .t.
			fichaempresa.lbl6.left = fichaempresa.lbl4.left
			fichaempresa.lbl7.left = fichaempresa.lbl5.left
		CASE UPPER(ALLTRIM(fichaempresa.pageFrame1.page1.tipo.value)) == "ASSOCIADOS"
			fichaempresa.lbl7.visible = .t.
			fichaempresa.lbl7.left = fichaempresa.lbl4.left + 15
			fichaempresa.lbl8.left = fichaempresa.lbl5.left + 15
		OTHERWISE
			**
	ENDCASE 
	
	IF UPPER(myPaisConfSoftw) == 'ANGOLA'
		fichaempresa.Pageframe1.Page3.txtserieSNS.visible=.f.
		fichaempresa.Pageframe1.Page3.txtserieEnt.visible=.f.
		fichaempresa.Pageframe1.Page3.label1.visible=.f.
		fichaempresa.Pageframe1.Page3.label2.visible=.f.
		fichaempresa.Pageframe1.Page3.chkOrdemImpressao.visible=.f.
	ENDIF 

	FICHAEMPRESA.resize()
		
ENDFUNC 


**
FUNCTION uf_fichaempresa_introducao
	PUBLIC emdesenvolvimento 
	
	IF EMPTY(emdesenvolvimento)
		uf_perguntalt_chama("N�o tem permiss�es para criar uma nova empresa. Contacte a Logitools.","OK","",48)
		RETURN .f.
	ENDIF 
	
	IF !uf_gerais_validaPermUser(ch_userno,ch_grupo,'Ficha Completa da Empresa - Introduzir')
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE EXECUTAR ESTA AC��O.","OK","",48)
		RETURN .f.
	ENDIF
	
	IF myEmpIntroducao == .t. OR myEmpAlteracao == .t.
		uf_perguntalt_chama("O Ecr� est� em modo de Introdu��o/Edi��o. Deve cancelar ou gravar para proceder com esta ac��o.", "OK", "", 64)
		RETURN .f.
	ENDIF

	myEmpIntroducao = .t.
	
	**Limpa Cursores
	IF USED("ucrsEmpresa")
		DELETE from ucrsEmpresa
		SELECT ucrsEmpresa
		APPEND BLANK
	ENDIF 
	IF USED("ucrsEmpresaArm")
		DELETE from ucrsEmpresaArm
		SELECT ucrsEmpresaArm
		GO TOP
	ENDIF 
	IF USED("uCrsDescPredef")
		DELETE from uCrsDescPredef
		SELECT uCrsDescPredef
		GO TOP
	ENDIF 
	
	uf_fichaempresa_valoresDefeito()
	
	fichaempresa.txtNome.setfocus
	uf_fichaempresa_controlaObj()
	
	FICHAEMPRESA.Pageframe1.Page3.GridArmazens.refresh
	fichaempresa.pageframe1.page3.GridDescPredef.refresh
	fichaempresa.PageFrame1.Page1.refresh
	fichaempresa.PageFrame1.Page2.refresh
	fichaempresa.PageFrame1.Page3.refresh
	fichaempresa.PageFrame1.Page4.refresh
	fichaempresa.PageFrame1.Page5.refresh
	
	fichaempresa.refresh
ENDFUNC 


**
FUNCTION uf_fichaempresa_valoresDefeito

	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		SELECT ISNULL(MAX(no),0) as no  FROM empresa (nolock)
	ENDTEXT
	If !uf_gerais_actgrelha("", "ucrsEmpresaNovoEstab", lcSql)
		uf_perguntalt_chama("N�o foi poss�vel determinar o n�mero a atribuir. Por favor contacte o suporte.", "OK", "", 16)
		RETURN .f.
	ENDIF
	
	SELECT ucrsEmpresa	
	Replace ucrsEmpresa.no WITH ucrsEmpresaNovoEstab.no+1
	Replace ucrsEmpresa.estab WITH 0
	Replace ucrsEmpresa.taxentity WITH "Global"
	
	IF USED("ucrsEmpresaNovoEstab")
		fecha("ucrsEmpresaNovoEstab")
	ENDIF
	
ENDFUNC 


**
FUNCTION uf_fichaempresa_alteracao
	
	IF !uf_gerais_validaPermUser(ch_userno,ch_grupo,'Ficha Completa da Empresa - Alterar')
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE EXECUTAR ESTA AC��O.","OK","",48)
		RETURN .f.
	ENDIF
	
	IF myEmpIntroducao OR myEmpAlteracao
		uf_perguntalt_chama("O Ecr� est� em modo de Introdu��o/Edi��o. Deve cancelar ou gravar para proceder com esta ac��o.", "OK", "", 64)
		RETURN .f.
	ENDIF
	
	
	myEmpAlteracao = .t.
	fichaempresa.txtNome.setfocus
	
	uf_fichaempresa_controlaObj()
	fichaempresa.refresh
ENDFUNC


**
FUNCTION uf_fichaempresa_actualiza
	SELECT ucrsEmpresa
	uf_fichaempresa_chama(ucrsEmpresa.no,ucrsEmpresa.estab)
ENDFUNC 


**
FUNCTION uf_fichaempresa_eliminar

	IF EMPTY(emdesenvolvimento)
		uf_perguntalt_chama("N�o tem permiss�es para eliminar uma empresa. Contacte a Logitools.","OK","",48)
		RETURN .f.
	ENDIF 

	IF !uf_gerais_validaPermUser(ch_userno,ch_grupo,'Ficha Completa da Empresa - Eliminar')
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE EXECUTAR ESTA AC��O.","OK","",48)
		RETURN .f.
	ENDIF
	
	IF uf_perguntalt_chama("Tem a certeza que pretende eliminar os dados referentes � Empresa?","Sim","N�o")
		SELECT ucrsEmpresa
		
		lcSQL = ''
		TEXT TO lcSQL TEXTMERGE NOSHOW
			DELETE FROM empresa WHERE empresa.no = <<ucrsEmpresa.no>> and empresa.estab = <<ucrsEmpresa.estab>>
			DELETE from b_pf WHERE resumo = '<<IIF(EMPTY(ALLTRIM(uCrsEmpresa.site)),'Local ' + ASTR(uCrsEmpresa.no),ALLTRIM(uCrsEmpresa.site))>>' + ' - Acesso'
		ENDTEXT
		
		IF uf_gerais_actGrelha("","",lcSQL)
			uf_perguntalt_chama("Registo apagado com sucesso.","OK","",64)
			uf_fichaempresa_chama(0,0)
		ENDIF
	ENDIF
ENDFUNC


**
FUNCTION uf_fichaempresa_gravar
	
	fichaempresa.txtNome.setfocus
	
	LOCAL lcvalidaCamposObrigatorios, lcValidaInsercao, lcValidaUpdate
	STORE .f. TO lcvalidaCamposObrigatorios, lcValidaInsercao, lcValidaUpdate
	**
	
	&& validacaoes campos e valores
	lcvalidaCamposObrigatorios = uf_fichaempresa_validaCamposObrigatorios()
	
	IF !lcvalidaCamposObrigatorios
		RETURN .f.	&& A mensagem ao utilizador � dada na fun��o anterior
	ENDIF

	DO CASE 
		CASE myEmpIntroducao
			uf_fichaempresa_insere()
		CASE myEmpAlteracao
			uf_fichaempresa_actualiza()
	ENDCASE 
	
	** Actualiza Dados dos Armazens
	uf_fichaempresa_updateArmazens()


	** Atualiza Informa��o do cursor com as altera��es
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_gerais_dadosEmpresa '<<ALLTRIM(mySite)>>'
	ENDTEXT

	If !uf_gerais_actGrelha("", "uCrsE1", lcSql)
		uf_perguntalt_chama("N�o foi atualizar dados empresa.", "", "OK", 16)
		RETURN .f.
	ENDIF


	myEmpIntroducao	= .f.
	myEmpAlteracao	= .f.

	uf_fichaempresa_chama(uCrsEmpresa.no,uCrsEmpresa.estab)
ENDFUNC


**
FUNCTION uf_fichaempresa_validaCamposObrigatorios
	LOCAL lcSQL

	IF EMPTY(ALLTRIM(fichaempresa.txtNome.value))
		uf_perguntalt_chama("O campo nome � de preenchimento obrigat�rio.", "OK", "", 64)
		RETURN .f.
	ENDIF
	
	IF uCrsEmpresa.email_ssl=.t. AND uCrsEmpresa.email_tls=.t.
		uf_perguntalt_chama("O tipo de autentica��o s� pede ser SSL ou TLS. Escolha apenas 1!", "OK", "", 64)
		RETURN .f.
	ENDIF
	
	IF EMPTY(ALLTRIM(fichaempresa.Pageframe1.page1.txtnomecomp.value))
		uf_perguntalt_chama("O campo Designa��o Social � de preenchimento obrigat�rio.", "OK", "", 64)
		RETURN .f.
	ENDIF
	
	Select ucrsEmpresa
	IF EMPTY(ucrsEmpresa.nomecomp)
		uf_perguntalt_chama("O campo Designa��o Social � de preenchimento obrigat�rio.", "OK", "", 64)
		RETURN .f.
	ENDIF 
	
	IF EMPTY(ALLTRIM(fichaempresa.Pageframe1.page1.tipo.value))
		uf_perguntalt_chama("O campo Tipo Empresa � de preenchimento obrigat�rio.", "OK", "", 64)
		RETURN .f.
	ENDIF
	
	IF RECCOUNT("ucrsEmpresaArm") == 0
		uf_perguntalt_chama("Deve atribuir pelo menos um armaz�m � empresa.", "OK", "", 64)
		RETURN .f.
	ENDIF
	
	IF EMPTY(ucrsEmpresa.taxentity)
		uf_perguntalt_chama("O campo Entidade Saft � obrigat�rio.", "OK", "", 64)
		RETURN .f.
	ENDIF
	
	IF uCrsEmpresa.ReservasAdi > 100
		uf_perguntalt_chama("N�o pode efetuar um adiantamento maior do que o valor da reserva. Por favor verique.", "", "OK", 64)
		RETURN .f.
	ENDIF 
	
	
	
	RETURN .t.
ENDFUNC 


**
FUNCTION uf_fichaempresa_insere	

	uf_empresa_codigoMotivo()
	
	SELECT uCrsEmpresa
	
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		INSERT INTO empresa(
			no
			,estab
			,nomecomp
			,morada
			,nomabrv
			,ncont
			,cae
			,natjurid
			,capsocial
			,freguesia
			,concelho
			,distrito
			,telefone
			,ncontss
			,contacto
			,nib
			,email
			,url
			,dataconst
			,datafecho
			,ccusto
			,banco
			,fax
			,pais
			,codpost
			,ctacttel
			,ctaccarg
			,instss
			,anoconst
			,codnatjur
			,sedeid
			,estabid
			,asspatr
			,local
			,ccperm
			,consreg
			,imagem
			,motivo_isencao_iva
			,codmotiseimp
			,ref
			,site
			,tipo
			,status
			,serie_vd
			,serie_fact
			,serie_regcli
			,entreposto
			,empresa
			,entidade
			,basedados
			,codigo
			,localPresc
			,pagcentral
			,posto
			,siteposto
			,serie_entSNS
			,serie_ent
			,TaxEntity
			,dirtec
			,infarmed
			,assfarm
			,codfarm
			,localacss
			,zonaacss
			,localcod
			,localnm
			,prescricao
			,alvara
			,conta
			,anfpassfx
			,ousrinis
			,ousrdata
			,ousrhora
			,usrinis
			,usrdata
			,usrhora
			,tipoempresa
			,id_lt
			,plafond_cliente
			,controla_plafond
			,descpredef
			,ordemimpressaoreceita
			,pme_lider
			,gestao_cx_operador
			,cod_swift
			,utilizadorDT
			,passwordDT
			,centroArb
			,obrigaConf
			,reservasAdi
			,impRecOrdenada
			,localnmAbrev
			,mailsign
			,email_body_ccNR
			,emp_email_smtp
			,emp_email_username
			,emp_email_password
			,emp_email_port
			,emp_email_ssl
			,emp_email_tls
			,emp_email_sender
			,emp_email_cc
			,emp_email_bcc
			,siteExt
			,condGeraisDist
			,abrev
			--,userSinave
			--,pwSinave
			,codsinave
			,AboutCompany
			,addressCompany
			,contactCompany
			,websiteURL
			,instagramURL
			,facebookURL
			,whatsappContact
			,logoetiqueta
			,logoRodape
			,nib2
			,cod_swift2
			,banco2
			,email_bcc
			,email_cc
			,gestCli
		)
		values(
			<<uCrsEmpresa.no>>
			,<<uCrsEmpresa.estab>>
			,'<<ALLTRIM(uCrsEmpresa.nomecomp)>>'
			,'<<ALLTRIM(uCrsEmpresa.morada)>>'
			,'<<ALLTRIM(uCrsEmpresa.nomabrv)>>'
			,'<<ALLTRIM(uCrsEmpresa.ncont)>>'
			,'<<ALLTRIM(uCrsEmpresa.cae)>>'
			,'<<ALLTRIM(uCrsEmpresa.natjurid)>>'
			,<<uCrsEmpresa.capsocial>>
			,'<<ALLTRIM(uCrsEmpresa.freguesia)>>'
			,'<<ALLTRIM(uCrsEmpresa.concelho)>>'
			,'<<ALLTRIM(uCrsEmpresa.distrito)>>'
			,'<<ALLTRIM(uCrsEmpresa.telefone)>>'
			,'<<ALLTRIM(uCrsEmpresa.ncontss)>>'
			,'<<ALLTRIM(uCrsEmpresa.contacto)>>'
			,'<<ALLTRIM(uCrsEmpresa.nib)>>'
			,'<<ALLTRIM(uCrsEmpresa.email)>>'
			,'<<ALLTRIM(uCrsEmpresa.url)>>'
			,'<<uf_gerais_getdate(uCrsEmpresa.dataconst,"SQL")>>'
			,'<<uf_gerais_getdate(uCrsEmpresa.datafecho,"SQL")>>'
			,'<<ALLTRIM(uCrsEmpresa.ccusto)>>'
			,'<<ALLTRIM(uCrsEmpresa.banco)>>'
			,'<<ALLTRIM(uCrsEmpresa.fax)>>'
			,'<<ALLTRIM(uCrsEmpresa.pais)>>'
			,'<<ALLTRIM(uCrsEmpresa.codpost)>>'
			,'<<ALLTRIM(uCrsEmpresa.ctacttel)>>'
			,'<<ALLTRIM(uCrsEmpresa.ctaccarg)>>'
			,'<<ALLTRIM(uCrsEmpresa.instss)>>'
			,<<uCrsEmpresa.anoconst>>
			,<<uCrsEmpresa.codnatjur>>
			,<<uCrsEmpresa.sedeid>>
			,<<uCrsEmpresa.estabid>>
			,'<<ALLTRIM(uCrsEmpresa.asspatr)>>'
			,'<<ALLTRIM(uCrsEmpresa.local)>>'
			,'<<ALLTRIM(uCrsEmpresa.ccperm)>>'
			,'<<ALLTRIM(uCrsEmpresa.consreg)>>'
			,'<<ALLTRIM(uCrsEmpresa.imagem)>>'
			,'<<ALLTRIM(uCrsEmpresa.motivo_isencao_iva)>>'
			,'<<ALLTRIM(uCrsEmpresa.codmotiseimp)>>'
			,'<<ALLTRIM(uCrsEmpresa.ref)>>'
			,'<<IIF(EMPTY(ALLTRIM(uCrsEmpresa.site)),'Loja ' + ASTR(uCrsEmpresa.no),ALLTRIM(uCrsEmpresa.site))>>'
			,'<<ALLTRIM(uCrsEmpresa.tipo)>>'
			,'<<ALLTRIM(uCrsEmpresa.status)>>'
			,<<uCrsEmpresa.serie_vd>>
			,<<uCrsEmpresa.serie_fact>>
			,<<uCrsEmpresa.serie_regcli>>
			,'<<ALLTRIM(uCrsEmpresa.entreposto)>>'
			,<<uCrsEmpresa.empresa>>
			,<<uCrsEmpresa.entidade>>
			,'<<ALLTRIM(uCrsEmpresa.basedados)>>'
			,'<<ALLTRIM(uCrsEmpresa.codigo)>>'
			,'<<ALLTRIM(uCrsEmpresa.localPresc)>>'
			,<<IIF(uCrsEmpresa.pagcentral,1,0)>>
			,<<IIF(uCrsEmpresa.posto,1,0)>>
			,'<<ALLTRIM(uCrsEmpresa.siteposto)>>'
			,<<uCrsEmpresa.serie_entSNS>>
			,<<uCrsEmpresa.serie_ent>>
			,'<<ALLTRIM(uCrsEmpresa.TaxEntity)>>'
			,'<<ALLTRIM(uCrsEmpresa.dirtec)>>'
			,<<uCrsEmpresa.infarmed>>
			,'<<ALLTRIM(uCrsEmpresa.assfarm)>>'
			,'<<ALLTRIM(uCrsEmpresa.codfarm)>>'
			,'<<ALLTRIM(uCrsEmpresa.localacss)>>'
			,<<uCrsEmpresa.zonaacss>>
			,'<<ALLTRIM(uCrsEmpresa.localcod)>>'
			,'<<ALLTRIM(uCrsEmpresa.localnm)>>'
			,<<IIF(uCrsEmpresa.prescricao,1,0)>>
			,'<<ALLTRIM(uCrsEmpresa.alvara)>>'
			,'<<ALLTRIM(uCrsEmpresa.conta)>>'
			,'<<ALLTRIM(uCrsEmpresa.anfpassfx)>>'
			,'<<m_chinis>>'
			,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
			,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108)
			,'<<m_chinis>>'
			,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
			,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108)
			,'<<ALLTRIM(uCrsEmpresa.tipoempresa)>>'
			,'<<ALLTRIM(uCrsEmpresa.id_lt)>>'
			,<<uCrsempresa.plafond_cliente>>
			,<<IIF(uCrsEmpresa.controla_plafond,1,0)>>
			,<<IIF(uCrsEmpresa.descpredef,1,0)>>
			,<<IIF(uCrsEmpresa.ordemimpressaoreceita,1,0)>>
			,<<IIF(uCrsEmpresa.pme_lider,1,0)>>
			,<<IIF(gestao_cx_operador,1,0)>>
			,'<<ALLTRIM(uCrsEmpresa.cod_swift)>>'
			,'<<ALLTRIM(uCrsEmpresa.utilizadorDT)>>'
			,'<<ALLTRIM(uCrsEmpresa.passwordDT)>>'
			,'<<ALLTRIM(uCrsEmpresa.centroArb)>>'
			,<<IIF(obrigaConf,1,0)>>
			,<<uCrsEmpresa.reservasAdi>>
			,<<IIF(uCrsEmpresa.impRecOrdenada,1,0)>>
			,'<<ALLTRIM(uCrsEmpresa.localnmAbrev)>>'			
			,'<<uCrsEmpresa.mailsign>>'
			,'<<uCrsEmpresa.email_body_ccNR>>'
			,'<<ALLTRIM(uCrsEmpresa.emp_email_smtp)>>'
			,'<<ALLTRIM(uCrsEmpresa.emp_email_username)>>'
			,'<<ALLTRIM(uCrsEmpresa.emp_email_password)>>'
			,'<<ALLTRIM(uCrsEmpresa.emp_email_port)>>'
			,<<IIF(uCrsEmpresa.emp_email_ssl,1,0)>>
			,<<IIF(uCrsEmpresa.emp_email_tls,1,0)>>
			,'<<ALLTRIM(uCrsEmpresa.emp_email_sender)>>'
			,'<<ALLTRIM(uCrsEmpresa.emp_email_cc)>>'
			,'<<ALLTRIM(uCrsEmpresa.emp_email_bcc)>>'
			,'<<ALLTRIM(uCrsEmpresa.siteExt)>>'
			,'<<ALLTRIM(uCrsEmpresa.condGeraisDist)>>'
			,'<<ALLTRIM(uCrsEmpresa.abrev)>>'
			--,'<<ALLTRIM(uCrsEmpresa.userSinave)>>'
			--,'<<ALLTRIM(uCrsEmpresa.pwSinave)>>'
			,'<<ALLTRIM(uCrsEmpresa.CodSinave)>>'
			,'<<ALLTRIM(uCrsEmpresa.AboutCompany)>>'
			,'<<ALLTRIM(uCrsEmpresa.addressCompany)>>'
			,'<<ALLTRIM(uCrsEmpresa.contactCompany)>>'
			,'<<ALLTRIM(uCrsEmpresa.websiteURL)>>'
			,'<<ALLTRIM(uCrsEmpresa.instagramURL)>>'
			,'<<ALLTRIM(uCrsEmpresa.facebookURL)>>'
			,'<<ALLTRIM(uCrsEmpresa.whatsappContact)>>'
			,'<<ALLTRIM(uCrsEmpresa.logoetiqueta)>>'
			,'<<ALLTRIM(uCrsEmpresa.logoRodape)>>'
			,'<<ALLTRIM(uCrsEmpresa.nib2)>>'
			,'<<ALLTRIM(uCrsEmpresa.cod_swift2)>>'
			,'<<ALLTRIM(uCrsEmpresa.banco2)>>'
			,'<<ALLTRIM(uCrsEmpresa.email_bcc)>>'
			,'<<ALLTRIM(uCrsEmpresa.email_cc)>>'
			,'<<ALLTRIM(uCrsEmpresa.gestCli)>>'
		)
		
		IF NOT exists (select pfstamp from b_pf where resumo = '<<IIF(EMPTY(ALLTRIM(uCrsEmpresa.site)),'Local ' + ASTR(uCrsEmpresa.no),ALLTRIM(uCrsEmpresa.site))>>' + ' - Acesso')
		BEGIN
			INSERT INTO b_pf(
				[pfstamp]
				,[codigo]
				,[resumo]
				,[descricao]
				,[ousrinis]
				,[ousrdata]
				,[ousrhora]
				,[usrinis]
				,[usrdata]
				,[usrhora]
				,[grupo]
				,[tipo]
				,[visivel]
			)
			SELECT 
				LEFT(NEWID(),25) AS [pfstamp]
				, isnull((select MAX(codigo) from b_pf),0)+1 AS [codigo]
				, '<<IIF(EMPTY(ALLTRIM(uCrsEmpresa.site)),'Local ' + ASTR(uCrsEmpresa.no),ALLTRIM(uCrsEmpresa.site))>>' + ' - Acesso' AS [resumo]
				, 'Atribui permiss�o de acesso � empresa ' + '<<IIF(EMPTY(ALLTRIM(uCrsEmpresa.site)),'Local ' + ASTR(uCrsEmpresa.no),ALLTRIM(uCrsEmpresa.site))>>' AS [descricao]
				,'<<m_chinis>>'
				,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
				,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108)
				,'<<m_chinis>>'
				,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
				,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108)
				, N'Empresa' AS [grupo]
				, N'Atribui Acesso' AS [tipo]
				, N'1' AS [visivel] 	
		END	
		
		--update b_us set codSinave='<<ALLTRIM(uCrsEmpresa.CodSinave)>>' where codSinave='' and loja='<<ALLTRIM(uCrsEmpresa.site)>>'
	ENDTEXT 
	
	** Grava Desconto Predefinidos
	uf_empresa_gravaDescPredef()
	
	IF uf_gerais_actgrelha("", "", lcSql)
		RETURN .t.
	ELSE
		uf_perguntalt_chama("N�o foi possivel introduzir o registo. Contacte o suporte.", "OK", "", 16)
		RETURN .f.
	ENDIF 
ENDFUNC



**
FUNCTION uf_fichaempresa_actualiza

	uf_empresa_codigoMotivo()

	SELECT uCrsEmpresa
	
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		UPDATE 
			empresa
		SET
			nomecomp = '<<ALLTRIM(uCrsEmpresa.nomecomp)>>'
			,morada = '<<ALLTRIM(uCrsEmpresa.morada)>>'
			,nomabrv = '<<ALLTRIM(uCrsEmpresa.nomabrv)>>'
			,ncont = '<<ALLTRIM(uCrsEmpresa.ncont)>>'
			,cae = '<<ALLTRIM(uCrsEmpresa.cae)>>'
			,natjurid = '<<ALLTRIM(uCrsEmpresa.natjurid)>>'
			,capsocial = <<uCrsEmpresa.capsocial>>
			,freguesia = '<<ALLTRIM(uCrsEmpresa.freguesia)>>'
			,concelho = '<<ALLTRIM(uCrsEmpresa.concelho)>>'
			,distrito = '<<ALLTRIM(uCrsEmpresa.distrito)>>'
			,telefone = '<<ALLTRIM(uCrsEmpresa.telefone)>>'
			,ncontss = '<<ALLTRIM(uCrsEmpresa.ncontss)>>'
			,contacto = '<<ALLTRIM(uCrsEmpresa.contacto)>>'
			,nib = '<<ALLTRIM(uCrsEmpresa.nib)>>'
			,email = '<<ALLTRIM(uCrsEmpresa.email)>>'
			,url = '<<ALLTRIM(uCrsEmpresa.url)>>'
			,dataconst = '<<uf_gerais_getdate(uCrsEmpresa.dataconst,"SQL")>>'
			,datafecho = '<<uf_gerais_getdate(uCrsEmpresa.datafecho,"SQL")>>'
			,ccusto = '<<ALLTRIM(uCrsEmpresa.ccusto)>>'
			,banco = '<<ALLTRIM(uCrsEmpresa.banco)>>'
			,fax = '<<ALLTRIM(uCrsEmpresa.fax)>>'
			,pais = '<<ALLTRIM(uCrsEmpresa.pais)>>'
			,codpost = '<<ALLTRIM(uCrsEmpresa.codpost)>>'
			,ctacttel = '<<ALLTRIM(uCrsEmpresa.ctacttel)>>'
			,ctaccarg = '<<ALLTRIM(uCrsEmpresa.ctaccarg)>>'
			,instss = '<<ALLTRIM(uCrsEmpresa.instss)>>'
			,anoconst = <<uCrsEmpresa.anoconst>>
			,codnatjur = <<uCrsEmpresa.codnatjur>>
			,sedeid = <<uCrsEmpresa.sedeid>>
			,estabid = <<uCrsEmpresa.estabid>>
			,asspatr = '<<ALLTRIM(uCrsEmpresa.asspatr)>>'
			,local = '<<ALLTRIM(uCrsEmpresa.local)>>'
			,ccperm = '<<ALLTRIM(uCrsEmpresa.ccperm)>>'
			,consreg = '<<ALLTRIM(uCrsEmpresa.consreg)>>'
			,imagem = '<<ALLTRIM(uCrsEmpresa.imagem)>>'
			,motivo_isencao_iva = '<<ALLTRIM(uCrsEmpresa.motivo_isencao_iva)>>'
			,codmotiseimp = '<<ALLTRIM(uCrsEmpresa.codmotiseimp)>>'
			,ref = '<<ALLTRIM(uCrsEmpresa.ref)>>'
			,site = '<<IIF(EMPTY(ALLTRIM(uCrsEmpresa.site)),'Loja ' + ASTR(uCrsEmpresa.no),ALLTRIM(uCrsEmpresa.site))>>'
			,tipo = '<<ALLTRIM(uCrsEmpresa.tipo)>>'
			,status = '<<ALLTRIM(uCrsEmpresa.status)>>'
			,serie_vd = <<uCrsEmpresa.serie_vd>>
			,serie_fact = <<uCrsEmpresa.serie_fact>>
			,serie_regcli = <<uCrsEmpresa.serie_regcli>>
			,entreposto = '<<ALLTRIM(uCrsEmpresa.entreposto)>>'
			,empresa = <<uCrsEmpresa.empresa>>
			,entidade = <<uCrsEmpresa.entidade>>
			,basedados = '<<ALLTRIM(uCrsEmpresa.basedados)>>'
			,codigo = '<<ALLTRIM(uCrsEmpresa.codigo)>>'
			,localPresc  ='<<ALLTRIM(uCrsEmpresa.localPresc)>>'
			,pagcentral = <<IIF(uCrsEmpresa.pagcentral,1,0)>>
			,posto  =<<IIF(uCrsEmpresa.posto,1,0)>>
			,siteposto = '<<ALLTRIM(uCrsEmpresa.siteposto)>>'
			,serie_entSNS = <<uCrsEmpresa.serie_entSNS>>
			,serie_ent = <<uCrsEmpresa.serie_ent>>
			,TaxEntity = '<<ALLTRIM(uCrsEmpresa.TaxEntity)>>'
			,dirtec = '<<ALLTRIM(uCrsEmpresa.dirtec)>>'
			,infarmed = <<uCrsEmpresa.infarmed>>
			,assfarm = '<<ALLTRIM(uCrsEmpresa.assfarm)>>'
			,codfarm = '<<ALLTRIM(uCrsEmpresa.codfarm)>>'
			,localacss = '<<ALLTRIM(uCrsEmpresa.localacss)>>'
			,zonaacss = <<uCrsEmpresa.zonaacss>>
			,localcod = '<<ALLTRIM(uCrsEmpresa.localcod)>>'
			,localnm = '<<ALLTRIM(uCrsEmpresa.localnm)>>'
			,localnmAbrev = '<<ALLTRIM(uCrsEmpresa.localnmAbrev)>>'
			,prescricao = <<IIF(uCrsEmpresa.prescricao,1,0)>>
			,alvara = '<<ALLTRIM(uCrsEmpresa.alvara)>>'
			,conta = '<<ALLTRIM(uCrsEmpresa.conta)>>'
			,anfpassfx = '<<ALLTRIM(uCrsEmpresa.anfpassfx)>>'
			,usrinis = '<<m_chinis>>'
			,usrdata = convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
			,usrhora = convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108)
			,tipoempresa = '<<ALLTRIM(uCrsEmpresa.tipoempresa)>>'
			,id_lt = '<<ALLTRIM(uCrsEmpresa.id_lt)>>'
			,plafond_cliente = '<<uCrsEmpresa.plafond_cliente>>'
			,controla_plafond = <<IIF(uCrsEmpresa.controla_plafond,1,0)>>
			,descpredef = <<IIF(uCrsEmpresa.descpredef,1,0)>>
			,ordemimpressaoreceita = <<IIF(uCrsEmpresa.ordemimpressaoreceita,1,0)>>
			,pme_lider = <<IIF(uCrsEmpresa.pme_lider,1,0)>>
			,gestao_cx_operador = <<IIF(uCrsEmpresa.gestao_cx_operador,1,0)>>
			,cod_swift = '<<uCrsEmpresa.cod_Swift>>'	
			,utilizadorDT = '<<ALLTRIM(uCrsEmpresa.utilizadorDT)>>'
			,passwordDT = '<<ALLTRIM(uCrsEmpresa.passwordDT)>>'
			,centroArb = '<<ALLTRIM(uCrsEmpresa.centroArb)>>'
			,obrigaConf = <<IIF(uCrsEmpresa.obrigaConf,1,0)>>
			,reservasAdi = <<uCrsEmpresa.reservasAdi>>
			,ImpRecOrdenada = <<IIF(uCrsEmpresa.ImpRecOrdenada,1,0)>>			
			,mailsign = '<<uCrsEmpresa.mailsign>>'
			,email_body_ccNR = '<<uCrsEmpresa.email_body_ccNR>>'
			,emp_email_smtp='<<ALLTRIM(uCrsEmpresa.emp_email_smtp)>>'
			,emp_email_username='<<ALLTRIM(uCrsEmpresa.emp_email_username)>>'
			,emp_email_password='<<ALLTRIM(uCrsEmpresa.emp_email_password)>>'
			,emp_email_port='<<ALLTRIM(uCrsEmpresa.emp_email_port)>>'
			,emp_email_ssl=<<IIF(uCrsEmpresa.emp_email_ssl,1,0)>>
			,emp_email_tls=<<IIF(uCrsEmpresa.emp_email_tls,1,0)>>
			,emp_email_sender='<<ALLTRIM(uCrsEmpresa.emp_email_sender)>>'
			,emp_email_cc='<<ALLTRIM(uCrsEmpresa.emp_email_cc)>>'
			,emp_email_bcc='<<ALLTRIM(uCrsEmpresa.emp_email_bcc)>>'  
			,siteExt='<<ALLTRIM(uCrsEmpresa.siteExt)>>' 
			,condGeraisDist='<<ALLTRIM(uCrsEmpresa.condGeraisDist)>>'		
			,abrev = '<<ALLTRIM(uCrsEmpresa.abrev)>>'	
			--,userSinave = '<<ALLTRIM(uCrsEmpresa.userSinave)>>'	
			--,pwSinave = '<<ALLTRIM(uCrsEmpresa.pwSinave)>>'	
			,codSinave = '<<ALLTRIM(uCrsEmpresa.CodSinave)>>'
			,AboutCompany = '<<ALLTRIM(uCrsEmpresa.AboutCompany)>>'	
			,addressCompany = '<<ALLTRIM(uCrsEmpresa.addressCompany)>>'	
			,contactCompany = '<<ALLTRIM(uCrsEmpresa.contactCompany)>>'	
			,websiteURL = '<<ALLTRIM(uCrsEmpresa.websiteURL)>>'	
			,instagramURL = '<<ALLTRIM(uCrsEmpresa.instagramURL)>>'	
			,facebookURL = '<<ALLTRIM(uCrsEmpresa.facebookURL)>>'	
			,whatsappContact = '<<ALLTRIM(uCrsEmpresa.whatsappContact)>>'	
			,logoetiqueta= '<<ALLTRIM(uCrsEmpresa.logoetiqueta)>>'
			,logoRodape = '<<ALLTRIM(uCrsEmpresa.logoRodape)>>'
			,nib2 		= '<<ALLTRIM(uCrsEmpresa.nib2)>>'
			,cod_swift2 = '<<ALLTRIM(uCrsEmpresa.cod_swift2)>>'
			,banco2 	= '<<ALLTRIM(uCrsEmpresa.banco2)>>'
			,email_bcc  = '<<ALLTRIM(uCrsEmpresa.email_bcc)>>'
			,email_cc   = '<<ALLTRIM(uCrsEmpresa.email_cc)>>'
			,gestCli = '<<ALLTRIM(uCrsEmpresa.gestCli)>>'
		where    
			empresa.no = <<uCrsEmpresa.no>>
			and empresa.estab = <<uCrsEmpresa.estab>>
		
		IF NOT exists (select pfstamp from b_pf where resumo = '<<IIF(EMPTY(ALLTRIM(uCrsEmpresa.site)),'Local ' + ASTR(uCrsEmpresa.no),ALLTRIM(uCrsEmpresa.site))>>' + ' - Acesso')
		BEGIN
			INSERT INTO b_pf(
				[pfstamp]
				,[codigo]
				,[resumo]
				,[descricao]
				,[ousrinis]
				,[ousrdata]
				,[ousrhora]
				,[usrinis]
				,[usrdata]
				,[usrhora]
				,[grupo]
				,[tipo]
				,[visivel]
			)
			SELECT 
				LEFT(NEWID(),25) AS [pfstamp]
				, isnull((select MAX(codigo) from b_pf),0)+1 AS [codigo]
				, '<<IIF(EMPTY(ALLTRIM(uCrsEmpresa.site)),'Local ' + ASTR(uCrsEmpresa.no),ALLTRIM(uCrsEmpresa.site))>>' + ' - Acesso' AS [resumo]
				, 'Atribui permiss�o de acesso � empresa ' + '<<IIF(EMPTY(ALLTRIM(uCrsEmpresa.site)),'Local ' + ASTR(uCrsEmpresa.no),ALLTRIM(uCrsEmpresa.site))>>' AS [descricao]
				,'<<m_chinis>>'
				,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
				,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108)
				,'<<m_chinis>>'
				,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
				,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108)
				, N'Empresa' AS [grupo]
				, N'Atribui Acesso' AS [tipo]
				, N'1' AS [visivel] 	
		END	
		
		--update b_us set codSinave='<<ALLTRIM(uCrsEmpresa.CodSinave)>>' where codSinave='' and loja='<<ALLTRIM(uCrsEmpresa.site)>>'
	ENDTEXT 	
	
	** Grava Desconto Predefinidos
	uf_empresa_gravaDescPredef()

	IF uf_gerais_actgrelha("", "", lcSql)
		RETURN .t.
	ELSE
		uf_perguntalt_chama("N�o foi possivel actualizar o registo. Contacte o suporte.", "OK", "", 16)
		RETURN .f.
	ENDIF 
ENDFUNC	


** Cancelar
FUNCTION uf_fichaempresa_sair
	fichaempresa.txtNome.setfocus
	
	* Verifica se o Documento esta em Modo de Edicao ou Introducao
	If myEmpIntroducao OR myEmpAlteracao
		IF uf_perguntalt_chama("Quer cancelar as altera��es efectuadas?", "SIM", "N�O", 64) == .t.
			STORE .f. TO myEmpIntroducao, myEmpAlteracao
			
			uf_fichaempresa_controlaObj()
			
			SELECT ucrsEmpresa
			**ultimo registo alterado
			uf_fichaempresa_chama(ucrsEmpresa.no, ucrsEmpresa.estab)
		Endif
	Else
		uf_fichaempresa_exit()
	Endif
Endfunc


**
FUNCTION uf_fichaempresa_exit
	fecha("ucrsEmpresa")
	fecha("uCrsDescPredef")	
	
	fichaempresa.hide
	fichaempresa.release
ENDFUNC



**
FUNCTION uf_fichaempresa_actualizaLogo
	LOCAL lcSourceLogoPath, lcTargetLogoPath, lcClienId, lcNomeImage
	STORE '' TO lcSourceLogoPath, lcTargetLogoPath, lcClienId, lcNomeImage
	
	&& obter ID cliente
	SELECT id_lt FROM uCrsEmpresa INTO CURSOR uCrsEmpresaAux WHERE UPPER(ALLTRIM(uCrsEmpresa.site)) == UPPER(ALLTRIM(mySite)) READWRITE 
	
	lcClienId = ALLTRIM(uCrsEmpresaAux.id_lt)

	&& utilizador define direitorio de origem, diretorio destino e nome imagem
	lcSourceLogoPath = GETFILE('','Imagem','',0,'Imagem da Empresa. A localiza��o deve ser partilhada.')
	
	lcTargetLogoPath = LEFT(mypath,3) + "Logitools\Clientes\" + lcClienId + "\Logos"	
	ALINES(uCrsImagem, lcSourceLogoPath, "\")	
	lcNomeImage = uCrsImagem(alen(uCrsImagem))
	
	&& verifica introducao logo
	IF EMPTY(lcSourceLogoPath)
		WITH FICHAEMPRESA.PageFrame1.Page1.logoEmpresa
			.picture = ""
			.Visible =  .f.
		ENDWITH
	ELSE
		&& verifica tipo de ficheiro
		IF UPPER(RIGHT(ALLTRIM(lcSourceLogoPath),3)) ==  "PNG";
			OR UPPER(RIGHT(ALLTRIM(lcSourceLogoPath),3)) ==  "BMP";
			OR UPPER(RIGHT(ALLTRIM(lcSourceLogoPath),3)) ==  "JPG"
	
			&& Copia ficheiro para diretorio //Logitools/logo
			IF !FILE(lcTargetLogoPath)
				COPY FILE (lcSourceLogoPath) TO (lcTargetLogoPath)
			ENDIF
	
			&& Atualiza Imagem
			WITH FICHAEMPRESA.PageFrame1.Page1.logoEmpresa
				.picture = lcTargetLogoPath + "\" + lcNomeImage
				.visible =  .t.
			ENDWITH
		
			&& Atribui caminho para grava��o ficheiro
			FICHAEMPRESA.PageFrame1.Page1.txtLogo.value = lcTargetLogoPath + "\" + lcNomeImage
			
		ELSE
			uf_perguntalt_chama("Ficheiro invalido, Extens�es suportadas: jpg, png, bmp.","OK","",16)

		ENDIF
	ENDIF

ENDFUNC

FUNCTION uf_fichaempresa_actualizaLogoRodape
	LOCAL lcSourceLogoPath, lcTargetLogoPath, lcClienId, lcNomeImage
	STORE '' TO lcSourceLogoPath, lcTargetLogoPath, lcClienId, lcNomeImage
	
	&& obter ID cliente
	SELECT id_lt FROM uCrsEmpresa INTO CURSOR uCrsEmpresaAux WHERE UPPER(ALLTRIM(uCrsEmpresa.site)) == UPPER(ALLTRIM(mySite)) READWRITE 
	
	lcClienId = ALLTRIM(uCrsEmpresaAux.id_lt)

	&& utilizador define direitorio de origem, diretorio destino e nome imagem
	lcSourceLogoPath = GETFILE('','Imagem','',0,'Imagem da Empresa. A localiza��o deve ser partilhada.')
	
	lcTargetLogoPath = LEFT(mypath,3) + "Logitools\Clientes\" + lcClienId + "\Logos"
	
	ALINES(uCrsImagem, lcSourceLogoPath, "\")	
	lcNomeImage = uCrsImagem(alen(uCrsImagem))
	
	&& verifica introducao logo
	IF !EMPTY(lcSourceLogoPath)
		&& verifica tipo de ficheiro
		IF UPPER(RIGHT(ALLTRIM(lcSourceLogoPath),3)) ==  "PNG";
			OR UPPER(RIGHT(ALLTRIM(lcSourceLogoPath),3)) ==  "BMP";
			OR UPPER(RIGHT(ALLTRIM(lcSourceLogoPath),3)) ==  "JPG"
		
			&& Copia ficheiro para diretorio //Logitools/logo
			IF !FILE(lcTargetLogoPath)
				COPY FILE (lcSourceLogoPath) TO (lcTargetLogoPath)
			ENDIF
					
			&& Atribui caminho para grava��o ficheiro
			FICHAEMPRESA.PageFrame1.Page1.txtRodape.value = lcTargetLogoPath + "\" + lcNomeImage
			
		ELSE
			uf_perguntalt_chama("Ficheiro invalido, Extens�es suportadas: jpg, png, bmp.","OK","",16)
		ENDIF
	ENDIF

ENDFUNC


FUNCTION uf_fichaempresa_actualizaLogoEtiq
	LOCAL lcSourceLogoPath, lcTargetLogoPath, lcClienId, lcNomeImage
	STORE '' TO lcSourceLogoPath, lcTargetLogoPath, lcClienId, lcNomeImage
	
	&& obter ID cliente
	SELECT id_lt FROM uCrsEmpresa INTO CURSOR uCrsEmpresaAux WHERE UPPER(ALLTRIM(uCrsEmpresa.site)) == UPPER(ALLTRIM(mySite)) READWRITE 
	
	lcClienId = ALLTRIM(uCrsEmpresaAux.id_lt)

	&& utilizador define direitorio de origem, diretorio destino e nome imagem
	lcSourceLogoPath = GETFILE('','Imagem','',0,'Imagem da Empresa. A localiza��o deve ser partilhada.')
	
	lcTargetLogoPath = LEFT(mypath,3) + "Logitools\Clientes\" + lcClienId + "\Logos"
	
	ALINES(uCrsImagem, lcSourceLogoPath, "\")	
	lcNomeImage = uCrsImagem(alen(uCrsImagem))
	
	
		&& verifica tipo de ficheiro
		IF UPPER(RIGHT(ALLTRIM(lcSourceLogoPath),3)) ==  "PNG";
			OR UPPER(RIGHT(ALLTRIM(lcSourceLogoPath),3)) ==  "BMP";
			OR UPPER(RIGHT(ALLTRIM(lcSourceLogoPath),3)) ==  "JPG"
		
			&& Copia ficheiro para diretorio //Logitools/logo
			IF !FILE(lcTargetLogoPath)
				COPY FILE (lcSourceLogoPath) TO (lcTargetLogoPath)
			ENDIF
		
			&& Atribui caminho para grava��o ficheiro
			FICHAEMPRESA.PageFrame1.Page1.txtLogoEtiq.value = lcTargetLogoPath + "\" + lcNomeImage
			
		ELSE
			uf_perguntalt_chama("Ficheiro invalido, Extens�es suportadas: jpg, png, bmp.","OK","",16)
		ENDIF

ENDFUNC


**
FUNCTION uf_fichaempresa_updateArmazens
	
	TEXT TO lcSql NOSHOW TEXTMERGE
		delete from empresa_arm WHERE empresa_no = <<ucrsEmpresa.no>>
	ENDTEXT 
	IF !uf_gerais_actgrelha("", "", lcSql)
		uf_perguntalt_chama("N�o foi possivel actualizar o registo dos armazens associados � Empresa. Contacte o suporte.", "OK", "", 16)
		RETURN .f.
	ENDIF 
	
	SELECT ucrsEmpresaArm
	GO Top
	SCAN
		TEXT TO lcSql NOSHOW TEXTMERGE
			INSERT INTO empresa_arm (empresa_no, armazem) VALUES (<<ucrsEmpresaArm.empresa_no>>,<<ucrsEmpresaArm.armazem>>)
		ENDTEXT 
		IF !uf_gerais_actgrelha("", "", lcSql)
			uf_perguntalt_chama("N�o foi possivel actualizar o registo dos armazens associados � Empresa. Contacte o suporte.", "OK", "", 16)
			RETURN .f.
		ENDIF 
	ENDSCAN 
	
	
ENDFUNC 


**
FUNCTION uf_fichaempresa_NovoArmazem
	
	TEXT TO lcSql NOSHOW TEXTMERGE
		INSERT INTO empresa_arm 
		SELECT empresa_no = <<ucrsEmpresa.no>>, armazem = (SELECT ISNULL(MAX(Armazem),0) + 1 as armazem_novo FROM empresa_arm) 
		Select * From empresa_arm (nolock) Where empresa_no = <<ucrsEmpresa.no>>
	ENDTEXT 
	IF !uf_gerais_actgrelha("FICHAEMPRESA.Pageframe1.Page3.GridArmazens", "ucrsEmpresaArm", lcSql)
		uf_perguntalt_chama("N�o foi verificar lista de armazens. Contacte o suporte.", "OK", "", 16)
		RETURN .f.
	ENDIF 
	
ENDFUNC 


**
FUNCTION uf_fichaempresa_ApagaArmazem
	TEXT TO lcSql NOSHOW TEXTMERGE
		DELETE from empresa_arm Where empresa_no = <<ucrsEmpresaArm.empresa_no>> and armazem = <<ucrsEmpresaArm.armazem>>
		Select * From empresa_arm (nolock) Where empresa_no = <<ucrsEmpresa.no>>
	ENDTEXT 
	IF !uf_gerais_actgrelha("FICHAEMPRESA.Pageframe1.Page3.GridArmazens", "ucrsEmpresaArm", lcSql)
		uf_perguntalt_chama("N�o foi verificar lista de armazens. Contacte o suporte.", "OK", "", 16)
		RETURN .f.
	ENDIF 

ENDFUNC 


**
FUNCTION uf_empresa_gravaDescPredef
	LOCAL lcSQL
	
	IF USED("uCrsDescPredef")
	
		lcSQL = ""
		TEXT TO lcSQL NOSHOW textmerge
			DELETE FROM empresa_descclientes where empresa_no = <<uCrsEmpresa.no>>
		ENDTEXT
		IF !uf_gerais_actGrelha("","",lcSQL)
			uf_perguntalt_chama("Ocorreu uma anomalia ao editar os descontos predefinidos de Cliente. Por favor contate o Suporte. Obrigado.","OK","",16)
			RETURN .f.
		ENDIF
		
		SELECT  uCrsDescPredef
		GO TOP
		SCAN			
				lcSQL = ""
				TEXT TO lcSQL NOSHOW textmerge
					INSERT INTO empresa_descclientes (
						 id
						,empresa_no
						,design
						,valor
				
					)values (
						(select ISNULL(MAX(id),0) + 1 from empresa_descclientes) 
						,<<uCrsEmpresa.no>>
						,'<<ALLTRIM(uCrsDescPredef.design)>>'
						,<<uCrsDescPredef.valor>>
					)
				ENDTEXT
				IF !uf_gerais_actGrelha("","",lcSQL)
					uf_perguntalt_chama("Ocorreu uma anomalia ao editar os descontos predefinidos de Cliente. Por favor contate o Suporte. Obrigado.","OK","",16)
					RETURN .f.
				ENDIF

		ENDSCAN
	ENDIF
	
	RETURN .t.

ENDFUNC


**
FUNCTION uf_empresa_addDescPredef

	SELECT uCrsDescPredef
	GO BOTTOM 
	APPEND BLANK
	REPLACE  uCrsDescPredef.design WITH 'Normal'
	REPLACE  uCrsDescPredef.valor  WITH 0
	
	fichaempresa.pageframe1.page3.GridDescPredef.refresh
	fichaempresa.pageframe1.page3.GridDescPredef.setfocus
ENDFUNC 


**
FUNCTION uf_empresa_removeDescPredef
	LOCAL lcDescPredefPos 
	
	lcDescPredefPos = RECNO("uCrsDescPredef")

	SELECT uCrsDescPredef
	DELETE

	SELECT uCrsDescPredef
	TRY
		SKIP -1
	CATCH 
		SKIP 1
	ENDTRY
	
	fichaempresa.pageframe1.page3.GridDescPredef.refresh
	fichaempresa.pageframe1.page3.GridDescPredef.setfocus
ENDFUNC

FUNCTION uf_empresa_codigoMotivo
	
	fecha("ucrsCodMotivoInsencao")
	LOCAL lcSQLCodMotivo
	lcSQLCodMotivo = ""
	TEXT TO lcSQLCodMotivo TEXTMERGE NOSHOW
		SELECT code_motive, campo FROM b_multidata (NOLOCK) WHERE tipo = 'motiseimp' AND campo='<<ALLTRIM(uCrsEmpresa.motivo_isencao_iva)>>'
	ENDTEXT
	IF !uf_gerais_actGrelha("", "ucrsCodMotivoInsencao", lcSQLCodMotivo)
		uf_perguntalt_chama("N�o foi possivel encontrar regitos do motivo de insen��o.", "", "OK", 16)
		RETURN .f.
	ENDIF
	
	IF RECCOUNT("ucrsCodMotivoInsencao") = 0
		uf_perguntalt_chama("O motivo de exen��o da empresa n�o se encontra preenchido. Por favor preencha para depois poder proceder a esta altera��o.", "", "OK", 16)
		RETURN .f.
	ENDIF
	
	SELECT uCrsEmpresa
	REPLACE uCrsEmpresa.codmotiseimp WITH ALLTRIM(ucrsCodMotivoInsencao.code_motive)
	fecha("ucrsCodMotivoInsencao")

ENDFUNC
