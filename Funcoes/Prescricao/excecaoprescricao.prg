
**fun��o inicial na cria��o do painel

FUNCTION uf_excecaoPrescricao_chama
	
	PUBLIC  myPrescNomeCod, myPrescNomeDescr, myPrescEmbCod, myPrescEmbDescr, myPrescDateCod, myPrescDateDescr
	myPrescNomeCod =''
	myPrescNomeDescr = '' 
	myPrescEmbCod = ''
	myPrescEmbDescr='' 
	myPrescDateCod = '' 
	myPrescDateDescr =''
	
	uf_excecaoPrescricao_preencheTabela()
	
	
	IF TYPE("EXCECAOPRESCRICAO") != "U"
        uf_excecaoPrescricao_sair()  
    ENDIF    
	
	
	IF TYPE("EXCECAOPRESCRICAO") == "U"
		DO FORM EXCECAOPRESCRICAO
		uf_excecaoPrescricao_menu()
		uf_excecaoPrescricao_preencheValores()
	ELSE
		EXCECAOPRESCRICAO.show()
	ENDIF
	
	
	
ENDFUNC


** cria��o do menu

FUNCTION uf_excecaoPrescricao_menu

	EXCECAOPRESCRICAO.menu1.adicionaOpcao("subir", "Subir", myPath + "\imagens\icons\pg_up_b.png", "uf_gerais_MovePage with .t.,'excecaoprescricao.pageframe1.Page1.grid1','uCrsExcecaoPanel'","05")
	EXCECAOPRESCRICAO.menu1.adicionaOpcao("descer", "Descer", myPath + "\imagens\icons\pg_down_b.png", "uf_gerais_MovePage with .f.,'excecaoprescricao.pageframe1.Page1.grid1','uCrsExcecaoPanel'","24")		
	EXCECAOPRESCRICAO.menu1.adicionaOpcao("vazio","Vazio.",myPath + "\imagens\icons\unchecked_w.png","uf_excecaoPrescricao_Vazio","V")

ENDFUNC

** preenche os valores da tabela 
FUNCTION uf_excecaoPrescricao_preencheTabela

	LOCAL lcSQL
	STORE "" TO lcSQL

	TEXT TO lcSQL TEXTMERGE NOSHOW
		SELECT sel=CAST(0 as bit), descricao = descr ,codigo = code, tipo=type,prescTipo= prescType from panel_exceptions (nolock) WHERE moduleId=2  ORDER BY prescType,code
	ENDTEXT
	If !uf_gerais_actGrelha("", "uCrsExcecaoPanel", lcSql)
		uf_perguntalt_chama("N�o foi possivel encontrar registos.", "", "OK", 16)
		RETURN .f.
	ENDIF	

ENDFUNC 


**fun��o preenche valores preenchidos  
FUNCTION 	uf_excecaoPrescricao_preencheValores


	myPrescNomeCod= uCrsPesqArtigoPresc.prescNomeCod
	myPrescNomeDescr= uCrsPesqArtigoPresc.prescNomeDescr
	myPrescEmbCod=uCrsPesqArtigoPresc.prescEmbCod 
	myPrescEmbDescr= uCrsPesqArtigoPresc.prescEmbDescr 
	myPrescDateCod= uCrsPesqArtigoPresc.prescDateCod 
	myPrescDateDescr=uCrsPesqArtigoPresc.prescDateDescr
	
	uf_excecaoPrescricao_preencheSel(myPrescNomeCod)
	uf_excecaoPrescricao_preencheSel(myPrescEmbCod)
	uf_excecaoPrescricao_preencheSel(myPrescDateCod)
ENDFUNC

FUNCTION uf_excecaoPrescricao_preencheSel
	LPARAM lcCode

**	SELECT ucrsTempPesq
**	GO TOP 
**	LOCATE FOR ALLTRIM(uCrsExcecaoPanel.codigo) == ALLTRIM(lcCode)
**	IF FOUND()
		
**	ENDIF	
**	UPDATE uCrsExcecaoPanel SET sel=.t. WHERE prescTipo =uCrsExcecaoPanel.prescTipo 
	UPDATE uCrsExcecaoPanel SET sel=.t. WHERE codigo =lcCode
	TABLEUPDATE()
		
	SELECT uCrsExcecaoPanel 
	GO TOP
	excecaoprescricao.pageframe1.Page1.grid1.refresh

ENDFUNC

FUNCTION uf_excecaoPrescricao_Gravar
	
	LOCAL myDescJust
	myDescJust=''

	
	Replace uCrsPesqArtigoPresc.prescNomeCod WITH myPrescNomeCod
	Replace uCrsPesqArtigoPresc.prescNomeDescr WITH  myPrescNomeDescr
	Replace uCrsPesqArtigoPresc.prescEmbCod WITH  myPrescEmbCod
	Replace uCrsPesqArtigoPresc.prescEmbDescr WITH  myPrescEmbDescr
	Replace uCrsPesqArtigoPresc.prescDateCod WITH  myPrescDateCod
	Replace uCrsPesqArtigoPresc.prescDateDescr WITH  myPrescDateDescr
	
	IF(!EMPTY(myPrescNomeDescr))
		myDescJust = "-> " + myPrescNomeDescr + CHR(10)
	ENDIF 
	
	IF(!EMPTY(myPrescEmbDescr))
		myDescJust = myDescJust + "-> " +  myPrescEmbDescr
	ENDIF 
	
	IF(!EMPTY(myPrescDateDescr))
		myDescJust = myDescJust + "-> " + myPrescDateDescr
	ENDIF 
	
	Replace uCrsPesqArtigoPresc.descJust WITH myDescJust
	uf_excecaoPrescricao_sair()
		
ENDFUNC



*************************************************
*			limpa selecionados		            *
*************************************************


FUNCTION uf_excecaoPrescricao_Vazio
	

	IF(USED("uCrsExcecaoPanel")) 
		UPDATE uCrsExcecaoPanel SET sel = .f.
		TABLEUPDATE()
	ENDIF
	
	
	SELECT uCrsExcecaoPanel 
	GO TOP
	
	myPrescNomeCod =''
	myPrescNomeDescr = '' 
	myPrescEmbCod = ''
	myPrescEmbDescr='' 
	myPrescDateCod = '' 
	myPrescDateDescr =''
	
	
	excecaoprescricao.pageframe1.Page1.grid1.refresh

ENDFUNC

		


*************************************************
*			valida multisele��o	                *
*************************************************


FUNCTION uf_excecaoPrescricao_validMultiSel

	LOCAL myPrescTipo, myCodigo ,myDescricao , myValidate
	
	
	myPrescTipo = uCrsExcecaoPanel.prescTipo 
	myCodigo = uCrsExcecaoPanel.codigo
	myDescricao = uCrsExcecaoPanel.descricao 

	myValidate = uf_excecaoPrescricao_validaExcepcao(ALLTRIM(myCodigo),uCrsPesqArtigoPresc.marg_terap, uCrsPesqArtigoPresc.dosagem)
	IF myValidate ==.t.

	*	IF(uCrsExcecaoPanel.sel ==.t.)

		UPDATE uCrsExcecaoPanel SET sel=.f. WHERE prescTipo =myPrescTipo AND codigo != myCodigo
			*UPDATE uCrsExcecaoPanel SET sel=.t. WHERE codigo =myCodigo
	*	ENDIF 


		IF  ALLTRIM(UPPER(myPrescTipo)) ==  "PRESC"
			myPrescNomeCod = myCodigo 
			myPrescNomeDescr = myDescricao 
		ENDIF 
		IF ALLTRIM(UPPER(myPrescTipo)) =="EMBPRESC"
			myPrescEmbCod = myCodigo 
			myPrescEmbDescr = myDescricao 
		ENDIF 		
		IF ALLTRIM(UPPER(myPrescTipo)) =="VALIDADEPRESC"
			myPrescDateCod = myCodigo 
			myPrescDateDescr = myDescricao 	
		ENDIF 

		TABLEUPDATE()
		
		SELECT uCrsExcecaoPanel 
		GO TOP
		excecaoprescricao.pageframe1.Page1.grid1.refresh
		
	ELSE 	
		UPDATE uCrsExcecaoPanel SET sel=.f. WHERE codigo = myCodigo
		SELECT uCrsExcecaoPanel 
		GO TOP
	ENDIF  
	
ENDFUNC






*************************************************
*			Ordena grelha pela designa��o		*
*************************************************
FUNCTION uf_excecaoPrescricao_OrderDesign
	uf_gerais_OrdenaGrelha("excecaoPrescicao.pageframe1.page1.grid1","uCrsExcecaoPanel","descr",myOrderExcecaoPrescricao)
	IF myOrderExcecaoPrescricao
		myOrderExcecaoPrescricao= .f.
	ELSE
		myOrderExcecaoPrescricao= .t.
	ENDIF
ENDFUNC


*** Poder mudar a designa��o quando se escolhe JAU4
FUNCTION uf_excecaoPrescricao_AlteraDesignacaoOutro
	
	myCodigo = uCrsExcecaoPanel.codigo
	
	IF  ALLTRIM(UPPER(myCodigo)) ==  "JAU4"
		excecaoprescricao.pageframe1.Page1.grid1.design.readonly=.f.
	ELSE 
		excecaoprescricao.pageframe1.Page1.grid1.design.readonly=.t.
	ENDIF 

ENDFUNC


FUNCTION uf_excecaoPrescricao_validaExcepcao
	LPARAMETERS lcCodigo, lcMargTerap, lcDosagem
	
	DO CASE 
		CASE ALLTRIM(lcCodigo) == "A" AND ALLTRIM(lcMargTerap) != "S"
			uf_perguntalt_chama("N�o � possivel aplicar a Exce��o A num medicamento que n�o tenha margem terap�utica estreita.", "", "OK", 32)
			RETURN .f.
		CASE ALLTRIM(lcCodigo) == "B" AND ALLTRIM(lcMargTerap) != "S"
			uf_perguntalt_chama("N�o � possivel aplicar a Exce��o B.", "", "OK", 32)
			RETURN .f.
		CASE ALLTRIM(lcCodigo) == "C" AND ALLTRIM(lcMargTerap) != "S"  AND ALLTRIM(lcDosagem) != "Associa��o"  
			uf_perguntalt_chama("Ao aplicar a Exce��o C num medicamento que n�o tenha margem terap�utica estreita ou cuja dosagem seja diferente de Associa��o, deve indicar na Posologia dura��o superior a 28 dias.", "", "OK", 32)
			**RETURN .f.
		OTHERWISE
	ENDCASE
	
	RETURN .t.
ENDFUNC


** sair do ecra		
FUNCTION uf_excecaoPrescricao_sair

	fecha("uCrsExcecaoPanel")
	fecha("myPrescNomeCod")
	fecha("myPrescNomeDescr")
	fecha("myPrescEmbCod")
	fecha("myPrescEmbDescr")
	fecha("myPrescDateCod")
	fecha("myPrescDateDescr")
	
	
	&& liberta class
	CLEAR CLASS dropdowncontainer
	
	**RELEASE excecaoPrescricao
	EXCECAOPRESCRICAO.hide
	EXCECAOPRESCRICAO.release
	
	RELEASE myOrderExcecaoPrescricao

ENDFUNC
