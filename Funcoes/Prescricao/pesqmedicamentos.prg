**
FUNCTION uf_pesqmedicamentos_chama
	
	IF !USED("uCrsPesqArtigoPresc")
		** Cria Cursor Inical
		lcSQL = ""
		TEXT TO lcSQL TEXTMERGE NOSHOW
			exec up_prescricao_pesquisaMed 0, '', '', '', '', '', '', 0, '', 0, <<ch_userno>>, 1 
		ENDTEXT
		IF !uf_gerais_actgrelha("", "uCrsPesqArtigoPresc", lcSQL)
			uf_perguntalt_chama("N�o foi poss�vel abrir a Pesquisa de Medicamentos/MCDT. Contacte o suporte.", "", "OK", 16)
			RETURN .F.
		ENDIF
	ENDIF 
	
	&& Controla abertura do painel
	IF TYPE("pesqmedicamentos")=="U"
		DO FORM pesqmedicamentos
	ELSE
		pesqmedicamentos.SHOW
	ENDIF
ENDFUNC 


**
FUNCTION uf_pesqmedicamentos_carregaMenu
	WITH pesqmedicamentos.menu1
		.adicionaopcao("atualizar", "Pesquisar", myPath + "\imagens\icons\actualizar_w.png", "uf_pesqmedicamentos_carregaDados","A")
		.adicionaopcao("DCI", "DCI", myPath + "\imagens\icons\stocks_mais_w.png", "uf_pesqmedicamentos_selDCI","D")
		.adicionaopcao("nomecompleto", "Nome Completo", myPath + "\imagens\icons\stocks_mais_w.png", "uf_pesqmedicamentos_selMed","N")
		.adicionaopcao("posologiaoriginal", "Posologia", myPath + "\imagens\icons\doc_linhas_w.png", "uf_pesqmedicamentos_aplicaPosologia","P")
		.adicionaOpcao("subir", "Subir", myPath + "\imagens\icons\ponta_seta_up.png", "uf_gerais_MovePage with .t.,'pesqmedicamentos.grdPresc','uCrsPesqArtigoPresc'","U")
		.adicionaOpcao("descer", "Descer", myPath + "\imagens\icons\ponta_seta_down.png", "uf_gerais_MovePage with .f.,'pesqmedicamentos.grdPresc','uCrsPesqArtigoPresc'","D")
		.estado("","","",.t.,"Voltar Receita",.t.)
	ENDWITH 
ENDFUNC

FUNCTION uf_limpaFiltros

	pesqmedicamentos.ref.value=""
	pesqmedicamentos.dci.value=""
	pesqmedicamentos.nome_comercial.value=""
	pesqmedicamentos.grupo.value=""
	pesqmedicamentos.forma.value=""
	pesqmedicamentos.cnpem.value=""
	pesqmedicamentos.diabetes.tag=""
	
ENDFUNC




** Fun��es Painel de Pesquisa de Prescri��o
FUNCTION uf_pesqmedicamentos_carregaDados

**	IF EMPTY(ALLTRIM(pesqmedicamentos.ref.value)) AND EMPTY(ALLTRIM(pesqmedicamentos.dci.value));
**		AND EMPTY(ALLTRIM(pesqmedicamentos.nome_comercial.value)) AND EMPTY(ALLTRIM(pesqmedicamentos.grupo.value));
**		AND EMPTY(ALLTRIM(pesqmedicamentos.forma.value)) AND EMPTY(ALLTRIM(pesqmedicamentos.cnpem.value)) AND pesqmedicamentos.diabetes.tag=="false"

	IF EMPTY(ALLTRIM(pesqmedicamentos.ref.value)) AND EMPTY(ALLTRIM(pesqmedicamentos.dci.value));
		AND EMPTY(ALLTRIM(pesqmedicamentos.grupo.value));
		AND EMPTY(ALLTRIM(pesqmedicamentos.forma.value)) AND pesqmedicamentos.diabetes.tag=="false"
		
		uf_perguntalt_chama("Deve preencher pelo menos um par�metro de pesquisa.", "", "OK", 64)
		RETURN .f.
	ENDIF
	
	pesqmedicamentos.enabled= .f.
	
	LOCAL lcIncluiNaoAutorizados
	IF EMPTY(pesqmedicamentos.naoAutorizados.tag) OR pesqmedicamentos.naoAutorizados.tag == "false"
		lcIncluiNaoAutorizados = 0
	ELSE
		lcIncluiNaoAutorizados = 1
	ENDIF
	
	regua(0, 100, "A calcular dados...", .t.)
	
	&& pesquisa prescri��o materializada
	
**		IF !EMPTY(myPrescMaterializada) 
**		IF ALLTRIM(pesqmedicamentos.ref.value) == '' AND ALLTRIM(pesqmedicamentos.dci.value) == '' ;
**			 AND ALLTRIM(pesqmedicamentos.nome_comercial.value) == '' AND ALLTRIM(pesqmedicamentos.grupo.value) == '' ;
**			 AND ALLTRIM(pesqmedicamentos.forma.value) == '' AND ALLTRIM(pesqmedicamentos.cnpem.value) == '' 
	
	IF !EMPTY(myPrescMaterializada) 
		IF ALLTRIM(pesqmedicamentos.ref.value) == '' AND ALLTRIM(pesqmedicamentos.dci.value) == '' ;
			 AND ALLTRIM(pesqmedicamentos.grupo.value) == '' ;
			 AND ALLTRIM(pesqmedicamentos.forma.value) == '' 
			 
			TEXT TO lcSQL TEXTMERGE NOSHOW
				exec up_prescricao_pesquisaMed 500, '', '', '', '', '', '', <<lcIncluiNaoAutorizados>>,'', <<IIF(pesqmedicamentos.diabetes.tag=="true",1,0)>>,<<ch_userno>>, 1
			ENDTEXT
		ELSE
**			TEXT TO lcSQL TEXTMERGE NOSHOW
**				exec up_prescricao_pesquisaMed 500,
**						'<<ALLTRIM(pesqmedicamentos.ref.value)>>',
**						'<<ALLTRIM(pesqmedicamentos.dci.value)>>',
**						'<<ALLTRIM(pesqmedicamentos.nome_comercial.value)>>',
**						'<<ALLTRIM(pesqmedicamentos.grupo.value)>>',
**						'',
**						'<<ALLTRIM(pesqmedicamentos.forma.value)>>'
**						,<<lcIncluiNaoAutorizados>>
**						,'<<ALLTRIM(pesqmedicamentos.cnpem.value)>>'
**						,<<IIF(pesqmedicamentos.diabetes.tag=="true",1,0)>>
**						,<<ch_userno>>
**						,1
**			ENDTEXT
			TEXT TO lcSQL TEXTMERGE NOSHOW
				exec up_prescricao_pesquisaMed 500,
						'<<ALLTRIM(pesqmedicamentos.ref.value)>>',
						'<<ALLTRIM(pesqmedicamentos.dci.value)>>',
						'<<ALLTRIM(pesqmedicamentos.ref.value)>>',
						'<<ALLTRIM(pesqmedicamentos.grupo.value)>>',
						'',
						'<<ALLTRIM(pesqmedicamentos.forma.value)>>'
						,<<lcIncluiNaoAutorizados>>
						,''
						,<<IIF(pesqmedicamentos.diabetes.tag=="true",1,0)>>
						,<<ch_userno>>
						,1
			ENDTEXT
		ENDIF
	ELSE
		&& pesquisa prescri��o desmaterializada
**			IF ALLTRIM(pesqmedicamentos.ref.value) == '' AND ALLTRIM(pesqmedicamentos.dci.value) == '' ;
**			 AND ALLTRIM(pesqmedicamentos.nome_comercial.value) == '' AND ALLTRIM(pesqmedicamentos.grupo.value) == '' ;
**			 AND ALLTRIM(pesqmedicamentos.forma.value) == '' AND ALLTRIM(pesqmedicamentos.cnpem.value) == '' 

		IF ALLTRIM(pesqmedicamentos.ref.value) == '' AND ALLTRIM(pesqmedicamentos.dci.value) == '' ;
			 AND ALLTRIM(pesqmedicamentos.grupo.value) == '' ;
			 AND ALLTRIM(pesqmedicamentos.forma.value) == '' 
			 
			TEXT TO lcSQL TEXTMERGE NOSHOW
				exec up_prescricao_pesquisaMed 500, '', '', '', '', '', '', <<lcIncluiNaoAutorizados>>,'', <<IIF(pesqmedicamentos.diabetes.tag=="true",1,0)>>,<<ch_userno>>, 0
			ENDTEXT
		ELSE
**				TEXT TO lcSQL TEXTMERGE NOSHOW
**				exec up_prescricao_pesquisaMed 500,
**						'<<ALLTRIM(pesqmedicamentos.ref.value)>>',
**						'<<ALLTRIM(pesqmedicamentos.dci.value)>>',
**						'<<ALLTRIM(pesqmedicamentos.nome_comercial.value)>>',
**						'<<ALLTRIM(pesqmedicamentos.grupo.value)>>',
**						'',
**						'<<ALLTRIM(pesqmedicamentos.forma.value)>>'
**						,<<lcIncluiNaoAutorizados>>
**						,'<<ALLTRIM(pesqmedicamentos.cnpem.value)>>'
**						,<<IIF(pesqmedicamentos.diabetes.tag=="true",1,0)>>
**						,<<ch_userno>>
**						,0
**			ENDTEXT

			TEXT TO lcSQL TEXTMERGE NOSHOW
				exec up_prescricao_pesquisaMed 500,
						'<<ALLTRIM(pesqmedicamentos.ref.value)>>',
						'<<ALLTRIM(pesqmedicamentos.dci.value)>>',
						'<<ALLTRIM(pesqmedicamentos.ref.value)>>',
						'<<ALLTRIM(pesqmedicamentos.grupo.value)>>',
						'',
						'<<ALLTRIM(pesqmedicamentos.forma.value)>>'
						,<<lcIncluiNaoAutorizados>>
						,''
						,<<IIF(pesqmedicamentos.diabetes.tag=="true",1,0)>>
						,<<ch_userno>>
						,0
			ENDTEXT
		ENDIF

	ENDIF
	
	IF !uf_gerais_actgrelha("pesqmedicamentos.grdPresc", "uCrsPesqArtigoPresc", lcSQL)
		pesqmedicamentos.enabled= .t.
		RETURN .f.
	ELSE
		pesqmedicamentos.lblregistos.caption = astr(RECCOUNT("uCrsPesqArtigoPresc")) + ' Registos'
		pesqmedicamentos.Refresh
	ENDIF
	regua(2)
	
	uf_pesqmedicamentos_aplicaFiltros()
	
	pesqmedicamentos.enabled= .t.
ENDFUNC


**
FUNCTION uf_pesqmedicamentos_aplicaFiltros
	IF EMPTY(pesqmedicamentos.diabetes.tag) OR pesqmedicamentos.diabetes.tag == "false"
		SELECT uCrsPesqArtigoPresc
		SET FILTER TO
		lcFiltraDiabetes= 1
	ELSE
		SELECT uCrsPesqArtigoPresc
		SET FILTER TO protocolo == .t.
	ENDIF
	pesqmedicamentos.grdPresc.refresh
	pesqmedicamentos.lblregistos.caption = ALLTRIM(str(RECCOUNT("uCrsPesqArtigoPresc"))) + ' Registos'
	pesqmedicamentos.Refresh

ENDFUNC 


**
FUNCTION uf_pesqmedicamentos_naoAutorizados

	uf_pesqmedicamentos_carregaDados()
ENDFUNC


**
FUNCTION uf_pesqmedicamentos_diabetesMellitus

	uf_pesqmedicamentos_carregaDados()
ENDFUNC 

**
FUNCTION uf_pesqmedicamentos_prescmedico
	LOCAL lcIncluiNaoAutorizados
	
	pesqmedicamentos.cronicos.Picture = myPath + "\imagens\icons\unchecked_b_32.png"
	pesqmedicamentos.cronicos.tag = "false"
		
	IF EMPTY(pesqmedicamentos.naoAutorizados.tag) OR pesqmedicamentos.naoAutorizados.tag == "false"
		lcIncluiNaoAutorizados = 0
	ELSE
		lcIncluiNaoAutorizados = 1
	ENDIF
	
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_prescricao_pesquisaRefMPM 1000,'','','','','','',<<lcIncluiNaoAutorizados>>,'<<UcrsCabPresc.drno>>', <<IIF(pesqmedicamentos.diabetes.tag=="true",1,0)>>
	ENDTEXT 

	IF !uf_gerais_actgrelha("pesqmedicamentos.grdPresc", "uCrsPesqArtigoPresc", lcSQL)
		Return
	ELSE
		pesqmedicamentos.lblregistos.caption = astr(RECCOUNT("uCrsPesqArtigoPresc")) + ' Registos'
		pesqmedicamentos.Refresh
	ENDIF
ENDFUNC 


**
FUNCTION uf_pesqmedicamentos_cronicos
	LOCAL lcIncluiNaoAutorizados
	
	pesqmedicamentos.prescmedico.Picture = myPath + "\imagens\icons\unchecked_b_32.png"
	pesqmedicamentos.prescmedico.tag = "false"
	

	IF EMPTY(pesqmedicamentos.naoAutorizados.tag) OR pesqmedicamentos.naoAutorizados.tag == "false"
		lcIncluiNaoAutorizados = 0
	ELSE
		lcIncluiNaoAutorizados = 1
	ENDIF
	
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_Prescricao_pesquisaRefMCU 1000,'','','','','','',0,'<<ALLTRIM(UcrsCabPresc.nrutente)>>',<<lcIncluiNaoAutorizados>>, <<IIF(pesqmedicamentos.diabetes.tag=="true",1,0)>>
	ENDTEXT 

	IF !uf_gerais_actgrelha("pesqmedicamentos.grdPresc", "uCrsPesqArtigoPresc", lcSQL)
		RETURN .f.
	ELSE
		pesqmedicamentos.lblregistos.caption = astr(RECCOUNT("uCrsPesqArtigoPresc")) + ' Registos'
		pesqmedicamentos.Refresh
	ENDIF
ENDFUNC 




**
FUNCTION uf_pesqmedicamentos_sel
	IF uf_gerais_getParameter("ADM0000000233","BOOL")
		IF uf_pesqmedicamentos_verificaPosologia() == .f.
			RETURN .f.
		ENDIF
	ENDIF 
ENDFUNC 


** Adicionar produtos qd s�o adicionados por DCI
FUNCTION uf_pesqmedicamentos_selDCI
	LOCAL lcCodGrupoH
	STORE '' TO lcCodGrupoH
	
	&& Verifica se o medicamento pode ser prescrito por DCI
	
	IF uf_pesqmedicamentos_verificaSePodeSerPrescritoPorDCI() == .f.		
			RETURN .f.
	ENDIF
	
	&& Para que controla o preenchimento obrigat�rio do campo posologia
	IF uf_gerais_getParameter("ADM0000000233","BOOL")
		IF uf_pesqmedicamentos_verificaPosologia() == .f.
			RETURN .f.
		ENDIF
	ENDIF 
	

	IF uCrsPesqArtigoPresc.Grupo != "MN"
		&& Guarda c�digo GH para usar na troca do medicamento Lyrica
		SELECT uCrsPesqArtigoPresc
		lcCodGrupoH = ALLTRIM(uCrsPesqArtigoPresc.CodGrupoH)
			
		&& vari�vel publica p controlar se � p tratamento de Dor neurop�tica
		PUBLIC myDorNp
		STORE .f. TO myDorNp
		
		&& adiciona Produto
		SELECT uCrsPesqArtigoPresc
		IF EMPTY(ALLTRIM(uCrsPesqArtigoPresc.CNPEM))
			IF ALLTRIM(uCrsPesqArtigoPresc.TipoReceita) == "MDB" OR ALLTRIM(uCrsPesqArtigoPresc.TipoReceita) == "OUT"
				
				IF uCrsPesqArtigoPresc.TipoReceita == "OUT"
					Replace uCrsPesqArtigoPresc.ref WITH "99999"
				ENDIF 	
			
				FOR i=1 TO pesqmedicamentos.numerounidades
					uf_prescricao_insereMed(.f.)
				ENDFOR 
			ELSE
				uf_perguntalt_chama("C�digo CNPEM n�o existente. N�o � possivel a precri��o por DCI.", "", "OK", 64)
				RETURN .f.
			ENDIF
		ELSE
			SELECT uCrsPesqArtigoPresc
			
			FOR i=1 TO pesqmedicamentos.numerounidades
				uf_prescricao_insereMed(.t.)
			ENDFOR 
		ENDIF 
	ELSE
		SELECT uCrsPesqArtigoPresc
		FOR i=1 TO pesqmedicamentos.numerounidades
			uf_prescricao_insereMed(.f.)
		ENDFOR 
		RETURN .t.	
	ENDIF 
*!*			&& Medicamento cujo principio seja Pregabalina deve "sugerir" a prescri��o do produto Lyrica
*!*			IF ALLTRIM(uCrsPesqArtigoPresc.listagemDci) == 'Pregabalina' AND ALLTRIM(uCrsPesqArtigoPresc.nome_comercial) != "Lyrica"
*!*				
*!*				
*!*			
*!*				IF uf_perguntalt_chama("O medicamento seleccionada destina-se ao tratamento da dor Neurop�tica?", "SIM", "N�O", 64)

*!*					myDorNp = .t.
*!*				
*!*					**uf_perguntalt_chama("Por raz�es de propriedade industrial, apenas pode pescrever o medicamento Lyrica. O software ir� efetuar a troca do medicamento seleccionado. Obrigado.", "", "OK", 48)				
*!*					uf_perguntalt_chama("POR RAZ�ES DE PROPRIEDADE INDUSTRIAL, APENAS O MEDICAMENTO LYRICA PODE SER PRESCRITO PARA O TRATAMENTO DA DOR NEUROP�TICA. O SOFTWARE IR� EFETUAR A TROCA DO MEDICAMENTO. OBRIGADO.", "", "OK", 48)				
*!*					
*!*					
*!*					&& Troca a posi��o no cursor para o medicamento Lyrica do mesmo grupo homogeneo do medicamento seleccionado	
*!*					SELECT uCrsPesqArtigoPresc
*!*					GO TOP 
*!*					LOCATE FOR UPPER(ALLTRIM(uCrsPesqArtigoPresc.nome_comercial)) == "LYRICA" AND ALLTRIM(uCrsPesqArtigoPresc.CodGrupoH) == ALLTRIM(lcCodGrupoH) AND ALLTRIM(uCrsPesqArtigoPresc.estado) == 'Autorizado'
*!*					IF !FOUND()
*!*											
*!*						 && adiciona os medicamentos com nome comercial Lyrica
*!*						uf_limpaFiltros()
*!*						pesqmedicamentos.ref.value="LYRICA"
*!*						uf_pesqmedicamentos_carregaDados()
*!*					ENDIF
*!*					
*!*					SELECT uCrsPesqArtigoPresc
*!*					GO TOP 
*!*					LOCATE FOR UPPER(ALLTRIM(uCrsPesqArtigoPresc.nome_comercial)) == "LYRICA" AND ALLTRIM(uCrsPesqArtigoPresc.CodGrupoH) == ALLTRIM(lcCodGrupoH) AND ALLTRIM(uCrsPesqArtigoPresc.estado) == 'Autorizado'
*!*					IF FOUND()
*!*						&& Adiciona op��o Exce��o C
*!*						uf_pesqmedicamentos_addExcecaoCaut()
*!*											
*!*						&& Insere medicamnento por Nome Comercial (Para obrigar a dispensa na farm�cia)
*!*						FOR i=1 TO pesqmedicamentos.numerounidades
*!*							uf_prescricao_insereMed(.f.)
*!*						ENDFOR 
*!*					ELSE
*!*						uf_perguntalt_chama("N�o existem produtos com o mesmo principio ativo na pesquisa que efetuou. Por favor efetue nova pesquisa. Obrigado.", "", "OK", 48)				
*!*						RETURN .f.
*!*					ENDIF
*!*					
*!*				ELSE && Segue fluxo Normal
*!*					FOR i=1 TO pesqmedicamentos.numerounidades
*!*						uf_prescricao_insereMed(.t.)
*!*					ENDFOR 
*!*				ENDIF
*!*			ELSE
*!*				
*!*				IF  ALLTRIM(uCrsPesqArtigoPresc.nome_comercial) == "Lyrica"
*!*					uf_perguntalt_chama("POR RAZ�ES DE PROPRIEDADE INDUSTRIAL, APENAS O MEDICAMENTO LYRICA PODE SER PRESCRITO PARA O TRATAMENTO DA DOR NEUROP�TICA. O SOFTWARE IR� EFETUAR A TROCA DO MEDICAMENTO. OBRIGADO.", "", "OK", 48)
*!*				ENDIF
*!*				FOR i=1 TO pesqmedicamentos.numerounidades
*!*			
*!*					uf_prescricao_insereMed(.t.)
*!*				ENDFOR 
*!*			ENDIF
*!*		ENDIF

ENDFUNC



** funcao adiciona medicamento por nome comercial
FUNCTION uf_pesqmedicamentos_selMed
	LOCAL lcCodGrupoH
	STORE '' TO lcCodGrupoH

	SELECT uCrsPesqArtigoPresc
	
	IF uCrsPesqArtigoPresc.TipoReceita == "OUT" OR uCrsPesqArtigoPresc.DCIPT_ID = 99999
		
		&& s� altera ref para prescricao de outro produtos
		IF uCrsPesqArtigoPresc.TipoReceita == "OUT" 
			SELECT uCrsPesqArtigoPresc
			Replace uCrsPesqArtigoPresc.ref WITH "99999"
		ENDIF
		
		FOR i=1 TO pesqmedicamentos.numerounidades
			uf_prescricao_insereMed(.f.)
		ENDFOR 
			
		RETURN .t. 
	ENDIF 
	
	&& Parametro que controla a colocacao de posologia 
	IF uf_gerais_getParameter("ADM0000000233","BOOL")
		IF uf_pesqmedicamentos_verificaPosologia() == .f.
			RETURN .f.
		ENDIF
	ENDIF
	
	IF uCrsPesqArtigoPresc.Grupo != "MN"
		&& Guarda c�digo grupo homogeneo para usar na troca do medicamento Lyrica
		SELECT uCrsPesqArtigoPresc
		lcCodGrupoH = ALLTRIM(uCrsPesqArtigoPresc.CodGrupoH)
		
		&& vari�vel publica p controlar se � p tratamento de Dor neurop�tica
		PUBLIC myDorNp
		STORE .f. TO myDorNp
		
		lcSQL = ''
		TEXT TO lcSQL TEXTMERGE NOSHOW
			exec up_prescricao_MedicamentosGenericosComparticipados '<<ALLTRIM(uCrsPesqArtigoPresc.ref)>>'
		ENDTEXT

		IF !uf_gerais_actgrelha("", "uCrsMedGenSimilaresComp", lcSQL)
			uf_perguntalt_chama("N�o foi possivel verificar a listagem de medicamentos similares. Por favor contacte o suporte.", "", "OK", 16)
			RETURN .f.
		ENDIF 	

		IF RECCOUNT("uCrsMedGenSimilaresComp") == 0 && N�o tem gen�ricos alternativos comparticipados pode prescrever por nome sem justifica��o
			FOR i=1 TO pesqmedicamentos.numerounidades
				uf_prescricao_insereMed(.f.)
			ENDFOR 
		ELSE && tem gen�icos alternativos comparticipados pode prescreve por nome  - obriga � justifica��osem justifica��o
			
			IF EMPTY(uCrsPesqArtigoPresc.prescNomeCod)
				uf_perguntalt_chama("Deve preencher o motivo de justificativo para prescrever por nome comercial ou do titular.", "", "OK", 64)
				pesqmedicamentos.grdPresc.setfocus
				
			ELSE && J� preencheu a justifica��o
			
				** Verifica se o medicamento se enquadra na alinea 1 : 
				**  . Medicamentos que n�o disponham de medicamentos gen�ricos similares comparticipados
				IF EMPTY(ALLTRIM(uCrsPesqArtigoPresc.posologia)) AND ALLTRIM(uCrsPesqArtigoPresc.prescnomecod) == "C"
					uf_perguntalt_chama("O uso da excep��o c) para prescricao por Titular/Nome Comercial, obriga o preenchimento do campo posologia, identificando uma dura��o de tratamento superior a 28 dias. Por favor verifique.", "", "OK", 32)
					RETURN .f.
				ENDIF
				
				FOR i=1 TO pesqmedicamentos.numerounidades
					uf_prescricao_insereMed(.f.)
				ENDFOR 
			ENDIF
		ENDIF
	ELSE
		SELECT uCrsPesqArtigoPresc
		FOR i=1 TO pesqmedicamentos.numerounidades
			uf_prescricao_insereMed(.f.)
		ENDFOR 
		RETURN .t.	
	ENDIF 

	&& Medicamento cujo principio seja Pregabalina deve "sugerir" a prescri��o do produto Lyrica
*!*		IF ALLTRIM(uCrsPesqArtigoPresc.listagemDci) == 'Pregabalina' && AND ALLTRIM(uCrsPesqArtigoPresc.nome_comercial) != "Lyrica"
*!*			IF uf_perguntalt_chama("O medicamento seleccionada destina-se ao tratamento da dor Neurop�tica?", "SIM", "N�O", 64)
*!*				
*!*				myDorNp	= .t.
*!*				
*!*				IF ALLTRIM(uCrsPesqArtigoPresc.nome_comercial) != "Lyrica"
*!*					**uf_perguntalt_chama("Por raz�es de propriedade industrial, apenas pode pescrever o Medicamento Lyrica. O software ir� efetuar a troca do medicamento seleccionado. Obrigado.", "", "OK", 48)				
*!*					uf_perguntalt_chama("POR RAZ�ES DE PROPRIEDADE INDUSTRIAL, APENAS O MEDICAMENTO LYRICA PODE SER PRESCRITO PARA O TRATAMENTO DA DOR NEUROP�TICA. O SOFTWARE IR� EFETUAR A TROCA DO MEDICAMENTO. OBRIGADO.", "", "OK", 48)				

*!*					&& Troca a posi��o no cursor para o medicamento Lyrica do mesmo grupo homogeneo do medicamento seleccionado
*!*					SELECT uCrsPesqArtigoPresc
*!*					GO TOP 
*!*					LOCATE FOR (UPPER(ALLTRIM(uCrsPesqArtigoPresc.nome_comercial)) == "LYRICA" AND ALLTRIM(uCrsPesqArtigoPresc.CodGrupoH) == lcCodGrupoH AND ALLTRIM(uCrsPesqArtigoPresc.estado) == "Autorizado")
*!*					IF FOUND()
*!*						SELECT uCrsPesqArtigoPresc
*!*						&& Adiciona op��o Exce��o C
*!*						uf_pesqmedicamentos_addExcecaoCaut()
*!*					
*!*						&& Insere medicamnento por Nome Comercial (Para obrigar a dispensa na farm�cia)
*!*						FOR i=1 TO pesqmedicamentos.numerounidades
*!*							uf_prescricao_insereMed(.f.)
*!*						ENDFOR 
*!*					ELSE
*!*						uf_perguntalt_chama("N�o existem produtos com o mesmo principio ativo na pesquisa que efetuou. Por favor refa�a a pesquisa. Obrigado.", "", "OK", 48)				
*!*						RETURN .f.
*!*					ENDIF	
*!*				
*!*				ELSE
*!*				
*!*					IF ALLTRIM(uCrsPesqArtigoPresc.nome_comercial) == "Lyrica"
*!*						uf_perguntalt_chama("POR RAZ�ES DE PROPRIEDADE INDUSTRIAL, APENAS O MEDICAMENTO LYRICA PODE SER PRESCRITO PARA O TRATAMENTO DA DOR NEUROP�TICA. O SOFTWARE IR� EFETUAR A TROCA DO MEDICAMENTO. OBRIGADO.", "", "OK", 48)				
*!*					ENDIF
*!*				&& Insere medicamnento por Nome Comercial e Adiciona op��o Exce��o C
*!*					SELECT uCrsPesqArtigoPresc
*!*					uf_pesqmedicamentos_addExcecaoCaut()
*!*					
*!*					FOR i=1 TO pesqmedicamentos.numerounidades
*!*						uf_prescricao_insereMed(.f.)
*!*					ENDFOR 					
*!*				ENDIF
*!*			ELSE
*!*				&& fluxo normal caso n�o seja dor Neurop�tica
*!*				lcSQL = ''
*!*				TEXT TO lcSQL TEXTMERGE NOSHOW
*!*					exec up_prescricao_MedicamentosGenericosComparticipados '<<ALLTRIM(uCrsPesqArtigoPresc.ref)>>'
*!*				ENDTEXT

*!*				IF !uf_gerais_actgrelha("", "uCrsMedGenSimilaresComp", lcSQL)
*!*					uf_perguntalt_chama("N�o foi possivel verificar a listagem de medicamentos similares. Por favor contacte o suporte.", "", "OK", 16)
*!*					RETURN .f.
*!*				ENDIF 	

*!*				IF RECCOUNT("uCrsMedGenSimilaresComp") == 0 && N�o tem gen�ricos alternativos comparticipados pode prescrever por nome sem justifica��o
*!*					FOR i=1 TO pesqmedicamentos.numerounidades
*!*						uf_prescricao_insereMed(.f.)
*!*					ENDFOR 
*!*				ELSE && tem gen�ricos alternativos comparticipados pode prescreve por nome  - obriga � justifica��osem justifica��o
*!*					
*!*					IF EMPTY(uCrsPesqArtigoPresc.prescNomeCod)
*!*						uf_perguntalt_chama("Deve preencher o motivo de justificativo para prescrever por nome comercial ou do titular.", "", "OK", 64)
*!*						pesqmedicamentos.grdPresc.setfocus
*!*						
*!*					ELSE && J� preencheu a justifica��o
*!*					
*!*						** Verifica se o medicamento se enquadra na alinea 1 : 
*!*						**  . Medicamentos que n�o disponham de medicamentos gen�ricos similares comparticipados
*!*						IF EMPTY(ALLTRIM(uCrsPesqArtigoPresc.posologia)) AND ALLTRIM(uCrsPesqArtigoPresc.prescnomecod) == "C"
*!*							uf_perguntalt_chama("O uso da excep��o c) para prescricao por Titular/Nome Comercial, obriga o preenchimento do campo posologia, identificando uma dura��o de tratamento superior a 28 dias. Por favor verifique.", "", "OK", 32)
*!*							RETURN .f.
*!*						ENDIF
*!*						
*!*						FOR i=1 TO pesqmedicamentos.numerounidades
*!*							uf_prescricao_insereMed(.f.)
*!*						ENDFOR 
*!*					ENDIF
*!*				ENDIF
*!*			ENDIF

*!*		ELSE

*!*			&& Fluxo Normal 
*!*			lcSQL = ''
*!*			TEXT TO lcSQL TEXTMERGE NOSHOW
*!*				exec up_prescricao_MedicamentosGenericosComparticipados '<<ALLTRIM(uCrsPesqArtigoPresc.ref)>>'
*!*			ENDTEXT

*!*			IF !uf_gerais_actgrelha("", "uCrsMedGenSimilaresComp", lcSQL)
*!*				uf_perguntalt_chama("N�o foi possivel verificar a listagem de medicamentos similares. Por favor contacte o suporte.", "", "OK", 16)
*!*				RETURN .f.
*!*			ENDIF 	

*!*			IF RECCOUNT("uCrsMedGenSimilaresComp") == 0 && N�o tem gen�ricos alternativos comparticipados pode prescrever por nome sem justifica��o
*!*				FOR i=1 TO pesqmedicamentos.numerounidades
*!*					uf_prescricao_insereMed(.f.)
*!*				ENDFOR 
*!*			ELSE && tem gen�icos alternativos comparticipados pode prescreve por nome  - obriga � justifica��osem justifica��o
*!*				
*!*				IF EMPTY(uCrsPesqArtigoPresc.prescNomeCod)
*!*					uf_perguntalt_chama("Deve preencher o motivo de justificativo para prescrever por nome comercial ou do titular.", "", "OK", 64)
*!*					pesqmedicamentos.grdPresc.setfocus
*!*					
*!*				ELSE && J� preencheu a justifica��o
*!*				
*!*					** Verifica se o medicamento se enquadra na alinea 1 : 
*!*					**  . Medicamentos que n�o disponham de medicamentos gen�ricos similares comparticipados
*!*					IF EMPTY(ALLTRIM(uCrsPesqArtigoPresc.posologia)) AND ALLTRIM(uCrsPesqArtigoPresc.prescnomecod) == "C"
*!*						uf_perguntalt_chama("O uso da excep��o c) para prescricao por Titular/Nome Comercial, obriga o preenchimento do campo posologia, identificando uma dura��o de tratamento superior a 28 dias. Por favor verifique.", "", "OK", 32)
*!*						RETURN .f.
*!*					ENDIF
*!*					
*!*					FOR i=1 TO pesqmedicamentos.numerounidades
*!*						uf_prescricao_insereMed(.f.)
*!*					ENDFOR 
*!*				ENDIF
*!*			ENDIF
*!*		ENDIF

ENDFUNC

** Verifica se o medicamento pode ser prescrito por dci

FUNCTION uf_pesqmedicamentos_verificaSePodeSerPrescritoPorDCI
	
	IF EMPTY(ALLTRIM(uCrsPesqArtigoPresc.dci)) 
		uf_perguntalt_chama("O medicamento n�o tem DCI." , "", "OK", 48)
		RETURN .f.
	ENDIF
				
	Return .t.

ENDFUNC



**
FUNCTION uf_pesqmedicamentos_verificaPosologia
	&& Posologia	
	IF EMPTY(ALLTRIM(uCrsPesqArtigoPresc.posologia))
		uf_perguntalt_chama("N�o preencheu o campo posologia.", "", "OK", 48)
		pesqmedicamentos.Pageframe1.Page1.posologia.SetFocus
		RETURN .f.
	ENDIF
				
	Return .t.

ENDFUNC


**
FUNCTION uf_pesqmedicamentos_corGridPesqMed
	LPARAMETERS lcRef, lcEstadocoerc, lcDataComerc

	DO CASE 
		CASE ALLTRIM(PESQMEDICAMENTOS.cnp) == ALLTRIM(lcRef)
			RETURN RGB[191,223,255]
			
		CASE EMPTY(lcEstadocoerc);
				OR UPPER(ALLTRIM(lcEstadocoerc)) == "";
				OR UPPER(ALLTRIM(lcEstadocoerc)) == "COMERC. CONF. PELO TITULAR";
				OR UPPER(ALLTRIM(lcEstadocoerc)) == "SEM INFORMA��O DO TITULAR"

			RETURN IIF(recno()%2!=0,Rgb[255,255,255],rgb[240,240,240])
		OTHERWISE 
			RETURN RGB[244,194,187]	
	ENDCASE
	
ENDFUNC


**
FUNCTION uf_pesqmedicamentos_actualizaLocalizacao

ENDFUNC 


**
FUNCTION uf_pesqmedicamentos_expandeDetalhe

ENDFUNC


**
FUNCTION uf_pesqmedicamentos_validaExcepcao
	LPARAMETERS lcObj, lcCodigo, lcMargTerap, lcDosagem
	
	DO CASE 
		CASE ALLTRIM(lcCodigo) == "A" AND ALLTRIM(lcMargTerap) != "S"
			uf_perguntalt_chama("N�o � possivel aplicar a Exce��o A num medicamento que n�o tenha margem terap�utica estreita.", "", "OK", 32)
			RETURN .f.
		CASE ALLTRIM(lcCodigo) == "B" AND ALLTRIM(lcMargTerap) != "S"
			uf_perguntalt_chama("N�o � possivel aplicar a Exce��o B.", "", "OK", 32)
			RETURN .f.
		CASE ALLTRIM(lcCodigo) == "C" AND ALLTRIM(lcMargTerap) != "S"  AND ALLTRIM(lcDosagem) != "Associa��o"  
			uf_perguntalt_chama("Ao aplicar a Exce��o C num medicamento que n�o tenha margem terap�utica estreita ou cuja dosagem seja diferente de Associa��o, deve indicar na Posologia dura��o superior a 28 dias.", "", "OK", 32)
			**RETURN .f.
		OTHERWISE
	ENDCASE
	
	RETURN .t.
ENDFUNC


**
FUNCTION uf_pesqmedicamentos_PosologiaDetalhes
	
*!*		WITH PESQMEDICAMENTOS.pageframe1.page3
*!*			IF PESQMEDICAMENTOS.controlaPosologia == .f.
*!*				.posologia.Height = .posologia.Height - 120
*!*				.labelObrigatorio.top = .posologia.Height + .posologia.top + 2
*!*				PESQMEDICAMENTOS.controlaPosologia = .t.
*!*				.detalhesPosologia.visible = .t.
*!*			ELSE && alterar posologia
*!*				.posologia.Height = .posologia.Height + 120
*!*				.labelObrigatorio.top = .posologia.Height + .posologia.top + 2
*!*				PESQMEDICAMENTOS.controlaPosologia = .f.
*!*				.detalhesPosologia.visible = .f.
*!*			ENDIF
*!*		ENDWITH 
ENDFUNC



**
FUNCTION uf_pesqmedicamentos_aplicaPosologiaOriginal
	SELECT uCrsPesqArtigoPresc
	Replace uCrsPesqArtigoPresc.posologiasugerida WITH uCrsPesqArtigoPresc.posologiaOriginal
	pesqmedicamentos.pageframe1.page1.DetalhesPosologia.posologiaConstruida.refresh
ENDFUNC


**
FUNCTION uf_pesqmedicamentos_aplicaPosologia
	SELECT uCrsPesqArtigoPresc
	Replace uCrsPesqArtigoPresc.posologia	WITH uCrsPesqArtigoPresc.posologiasugerida
	pesqmedicamentos.pageframe1.page1.posologia.refresh
ENDFUNC 


** Fechar Ecra de Pesquisa de Prescri��es
FUNCTION uf_pesqmedicamentos_sair
	&& fecha cursores
	IF USED("uCrsPesqArtigoPresc")
		fecha("uCrsPesqArtigoPresc")
	ENDIF
	
	IF USED("ucrsPosologia")
		fecha("ucrsPosologia")
	ENDIF
	
	&&Liberta variaveis publicas
	RELEASE myIntVarIncluir
	RELEASE myDorNp
	
	pesqmedicamentos.hide
	pesqmedicamentos.release
ENDFUNC


** Aplica automaticamente a exce��o C qd existe substitui��o do medicamento com o principio Ativo Pregabalina para o medicamento Lyrica
FUNCTION uf_pesqmedicamentos_addExcecaoCaut
	LOCAL lcSQL
	STORE '' TO lcSQL

	SELECT uCrsPesqArtigoPresc

	TEXT TO lcSQL TEXTMERGE NOSHOW
		select codigo, descricao from b_cli_justPrescNome (nolock) WHERE codigo = 'C'
	ENDTEXT
	IF !uf_gerais_actGrelha("", "uCrsTempPesqAux", lcSql)
		uf_perguntalt_chama("N�o foi poss�vel aplicar a exce��o no medicamento seleccionado. Por favor contacte o Suporte. Obrigado.", "", "OK", 16)
		RETURN .f.
	ENDIF
			
	SELECT uCrsPesqArtigoPresc
	
	replace uCrsPesqArtigoPresc.prescNomeDescr WITH ALLTRIM(uCrsTempPesqAux.descricao)
	replace uCrsPesqArtigoPresc.prescNomeCod   WITH ALLTRIM(uCrsTempPesqAux.codigo)
	
	IF USED("uCrsTempPesqAux")
		fecha("uCrsTempPesqAux")
	ENDIF

ENDFUNC 


FUNCTION uf_pesqmedicamentos_criaCursorPosologia

	IF USED("ucrsPosologia")
		fecha("ucrsPosologia")
	ENDIF

	CREATE CURSOR ucrsPosologia (Posologia c(254), mudar c(1) ,duracaoValor c(10),duracaoUnidade c(10),frequenciaValor c(10),frequenciaUnidade c(10),quantidadeValor  c(10),quantidadeUnidade c(10),AteFim c(10),numEmbalagem n(9,0))
	SELECT ucrsPosologia 
	APPEND BLANK
	replace ucrsPosologia.Posologia 		WITH ALLTRIM(uCrsPesqArtigoPresc.Posologia)
	replace ucrsPosologia.duracaoValor   	WITH ALLTRIM(uCrsPesqArtigoPresc.duracaoValor)
	replace ucrsPosologia.duracaoUnidade 	WITH ALLTRIM(uCrsPesqArtigoPresc.duracaoUnidade)
	replace ucrsPosologia.frequenciaValor 	WITH ALLTRIM(uCrsPesqArtigoPresc.frequenciaValor )
	replace ucrsPosologia.frequenciaUnidade WITH ALLTRIM(uCrsPesqArtigoPresc.frequenciaUnidade)
	replace ucrsPosologia.quantidadeValor  	WITH ALLTRIM(uCrsPesqArtigoPresc.quantidadeValor )
	replace ucrsPosologia.quantidadeUnidade	WITH ALLTRIM(uCrsPesqArtigoPresc.quantidadeUnidade)
	replace ucrsPosologia.mudar 			WITH 'N'
	replace ucrsPosologia.numEmbalagem 		WITH uCrsPesqArtigoPresc.num_unidades

ENDFUNC 

FUNCTION uf_pesqmedicamentos_preenchePosologia


	IF USED("ucrsPosologia")
		IF ucrsPosologia.mudar == 'S'
			replace uCrsPesqArtigoPresc.Posologia 			WITH ALLTRIM(ucrsPosologia.Posologia)
			replace uCrsPesqArtigoPresc.duracaoValor   		WITH ALLTRIM(ucrsPosologia.duracaoValor)
			replace uCrsPesqArtigoPresc.duracaoUnidade 		WITH ALLTRIM(ucrsPosologia.duracaoUnidade)
			replace uCrsPesqArtigoPresc.frequenciaValor 	WITH ALLTRIM(ucrsPosologia.frequenciaValor )
			replace uCrsPesqArtigoPresc.frequenciaUnidade 	WITH ALLTRIM(ucrsPosologia.frequenciaUnidade)
			replace uCrsPesqArtigoPresc.quantidadeValor  	WITH ALLTRIM(ucrsPosologia.quantidadeValor )
			replace uCrsPesqArtigoPresc.quantidadeUnidade	WITH ALLTRIM(ucrsPosologia.quantidadeUnidade)
		ENDIF
	ENDIF

ENDFUNC 