**
FUNCTION uf_pesquisaMcdt_Chama

	**Valida Licenciamento	
	IF (uf_ligacoes_addConnection('MC') == .f.)
		RETURN .F.
	ENDIF

	IF !USED("uCrsPesqArtigoMCDT")
		TEXT TO lcSQL TEXTMERGE NOSHOW
			SET FMTONLY ON
			exec up_prescricao_pesquisaMcdt 500,'','','','','',''
			SET FMTONLY OFF
		ENDTEXT 
		IF !uf_gerais_actgrelha("", "uCrsPesqArtigoMCDT", lcSql)
			MESSAGEBOX("N�o foi possivel aplicar defini��es de MCDT. Contacte o suporte.",16,"LOGITOOLS SOFTWARE")
			RETURN .f.
		ENDIF
	ENDIF
	
	IF TYPE("PESQUISAMCDT")=="U"
		DO FORM PESQUISAMCDT
	ELSE
		PESQUISAMCDT.show
	ENDIF
ENDFUNC 


**
FUNCTION uf_pesquisaMcdt_carregaMenu
	PESQUISAMCDT.menu1.adicionaOpcao("actualizar", "Actualizar", myPath + "\imagens\icons\actualizar_w.png", "uf_pesquisaMcdt_actPesquisa","A")
	PESQUISAMCDT.menu1.adicionaOpcao("limpar", "Limpar", myPath + "\imagens\icons\limpar_w.png", "uf_pesquisaMcdt_limparDados","L")
	PESQUISAMCDT.menu1.adicionaOpcao("subir", "Subir", myPath + "\imagens\icons\ponta_seta_up.png", "uf_gerais_MovePage with .t.,'PESQUISAMCDT.gridpesq','uCrsPesqArtigoMCDT'","U")
	PESQUISAMCDT.menu1.adicionaOpcao("descer", "Descer", myPath + "\imagens\icons\ponta_seta_down.png", "uf_gerais_MovePage with .f.,'PESQUISAMCDT.gridpesq','uCrsPesqArtigoMCDT'","D")
ENDFUNC


**
FUNCTION uf_pesquisaMcdt_actPesquisa

	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_prescricao_pesquisaMcdt 500, 
				'<<ALLTRIM(PESQUISAMCDT.codsns.value)>>',
				'<<ALLTRIM(PESQUISAMCDT.codconv.value)>>', 
				'<<ALLTRIM(PESQUISAMCDT.areadescr.value))>>',
				'<<ALLTRIM(PESQUISAMCDT.grupo.value)>>',
				'<<ALLTRIM(PESQUISAMCDT.subgrupo.value)>>',
				'<<ALLTRIM(PESQUISAMCDT.nomedescr.value)>>'
	ENDTEXT
	
	IF !uf_gerais_actgrelha("PESQUISAMCDT.gridPesq", "uCrsPesqArtigoMCDT", lcSql)
		MESSAGEBOX("N�o foi possivel aplicar defini��es de MCDT. Contacte o suporte.",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF
	PESQUISAMCDT.lblRegistos.caption = ALLTRIM(STR(RECCOUNT("uCrsPesqArtigoMCDT"))) + " Resultados"
	
	PESQUISAMCDT.refresh
ENDFUNC


**
FUNCTION uf_pesquisaMcdt_limparDados
	PESQUISAMCDT.codsns.value = ''
	PESQUISAMCDT.codconv.Value = ''
	PESQUISAMCDT.areadescr.Value = ''
	PESQUISAMCDT.grupo.Value = ''
	PESQUISAMCDT.subgrupo.Value = ''
	PESQUISAMCDT.nomedescr.Value = ''
	uf_pesquisaMcdt_actPesquisa()
ENDFUNC 


**
FUNCTION uf_pesquisaMcdt_sair
	PESQUISAMCDT.hide
	PESQUISAMCDT.release
ENDFUNC
