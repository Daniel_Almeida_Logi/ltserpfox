
**
FUNCTION uf_pesqPrescricao_Chama
	**DO FORM Prescricao
	IF !USED("ucrsPesqPresc")
		uf_pesqPrescricao_actPesquisa(.t.)
	ENDIF
	IF TYPE("PESQPRESCRICAO")=="U"
		DO FORM PESQPRESCRICAO
	ELSE
		PESQPRESCRICAO.show
	ENDIF
ENDFUNC 


**
** tcBool = caso venha a .t. cria apenas o cursor
**
FUNCTION uf_pesqPrescricao_actPesquisa
	LPARAMETERS tcBool
	
	LOCAL lcSQL, lcDataIni, lcDataFim
	STORE "" TO lcSql, lcDataIni, lcDataFim
	
	IF tcBool
		lcSQL = ""
		TEXT TO lcSql NOSHOW TEXTMERGE
			exec up_prescricao_pesquisaTop5 0, ''
		ENDTEXT
**		exec up_prescricao_pesquisaTop5 <<ch_userno>>, '<<ALLTRIM(ch_grupo)>>'
		
		if type("PESQPRESCRICAO")=="U"
			IF !uf_gerais_actGrelha("", "ucrsPesqPresc",lcSQL)
				uf_perguntalt_chama("N�o foi poss�vel actualizar a listagem de Prescri��es. Por favor contacte o Suporte.", "", "OK", 16)
				RETURN .f.
			ENDIF
		ENDIF
		
	ELSE &&Refresca valores da Grid
	
		If !empty(PESQPRESCRICAO.DataIni.value)
			lcDataIni = uf_gerais_getdate(PESQPRESCRICAO.dataIni.value,"SQL")
		Else
			lcDataIni ='19000101'
		EndIf
		If !empty(PESQPRESCRICAO.DataFim.value)
			lcDataFim = uf_gerais_getdate(PESQPRESCRICAO.DataFim.value,"SQL")
		Else
			If !empty(PESQPRESCRICAO.DataIni.value)
				lcDataFim = lcDataIni
			Else
				lcDataFim='30001231'
			EndIf
		ENDIF
	
		lcSQL = ""
		TEXT TO lcSql NOSHOW TEXTMERGE
			exec up_prescricao_pesquisa '<<lcDataIni>>','<<lcDataFim>>','<<ALLTRIM(PESQPRESCRICAO.nrprescricao.value)>>','<<ALLTRIM(PESQPRESCRICAO.utente.value)>>','<<ALLTRIM(PESQPRESCRICAO.nutente.value)>>','<<ALLTRIM(PESQPRESCRICAO.nbenef.value)>>','<<ALLTRIM(PESQPRESCRICAO.especialista.value)>>','',<<IIF(PESQPRESCRICAO.renovavel.tag=="false",0,1)>>,<<IIF(PESQPRESCRICAO.medicamentos.tag=="false",0,1)>>,<<IIF(PESQPRESCRICAO.mcdt.tag=="false",0,1)>>,'<<PESQPRESCRICAO.estado.value>>', <<ch_userno>>, '<<ALLTRIM(ch_grupo)>>', '<<ALLTRIM(PESQPRESCRICAO.prescno.value)>>'
		ENDTEXT
		IF !uf_gerais_actGrelha("PESQPRESCRICAO.GridPesq", "ucrsPesqPresc",lcSQL)
			uf_perguntalt_chama("N�o foi poss�vel actualizar a listagem de Prescri��es. Por favor contacte o Suporte.", "", "OK", 16)
			RETURN .f.
		ENDIF
		
		IF !USED("ucrsPesqPresc")
			uf_pesqPrescricao_actPesquisa(.t.)
		ENDIF			
		
		PESQPRESCRICAO.Refresh
		PESQPRESCRICAO.lblRegistos.caption = ALLTRIM(STR(RECCOUNT("ucrsPesqPresc"))) + " Resultados"
	ENDIF
ENDFUNC

**
FUNCTION uf_pesqPrescricao_limparDados
	PESQPRESCRICAO.prescno.value = ''
	PESQPRESCRICAO.utente.Value = ''
	PESQPRESCRICAO.nutente.Value = ''
	PESQPRESCRICAO.nbenef.Value = ''
	PESQPRESCRICAO.especialista.Value = ''
	uf_pesqPrescricao_actPesquisa(.t.)
ENDFUNC 




**
FUNCTION uf_pesqPrescricao_Escolhe
	SELECT ucrsPesqPresc
	uf_prescricao_Chama(ALLTRIM(ucrsPesqPresc.nrprescricao))
	uf_prescricao_selecionaPg("Utente")
	uf_pesqPrescricao_sair()
ENDFUNC


**
FUNCTION uf_pesqPrescricao_ultimoRegisto

	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_prescricao_ultimoRegisto <<ch_userno>>, '<<mysite>>'
	ENDTEXT 
	
	IF !uf_gerais_actgrelha("", "uCrsNrUltimoRegisto", lcSQL)
		uf_perguntalt_chama("N�o foi poss�vel verificar o ultimo registo de prescri��o. Contacte o suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF
	initpesq=1
	SELECT uCrsNrUltimoRegisto
	IF !EMPTY(uCrsNrUltimoRegisto.nrprescricao)
		uf_prescricao_chama(uCrsNrUltimoRegisto.nrprescricao)
	ENDIF

	PESQPRESCRICAO.hide
**	PESQPRESCRICAO.release
**	initpesq=1
** 	uf_pesqPrescricao_sair()
ENDFUNC


**
FUNCTION uf_pesqPrescricao_sair
	&& fecha cursores
	IF USED("ucrsPesqPresc")
		fecha("ucrsPesqPresc")
	ENDIF
	**initpesq=1
	IF initpesq=0
		uf_prescricao_exit()
	ENDIF 
	PESQPRESCRICAO.hide
	PESQPRESCRICAO.release
ENDFUNC
