DO pesqPrescricao
DO pesquisaMCDT
DO pesqmedicamentos
DO CPM
DO excecaoPrescricao

**
FUNCTION uf_prescricao_chama
	LPARAMETERS lcnrPrescricao, lcEdicao, lcUtstamp, lcPHC, lcPrescTipo
	LOCAL lcSql
	STORE '' TO lcSQL

	** temp para debug
	emdesenvolvimento = .t.

	** Verificar se existe nova vers�o para reiniciar o software
	uf_chk_nova_versao()
	
	PUBLIC myPrescricaoIntroducao , myPrescricaoAlteracao
			
	** Verifica acesso atrav�s de pefil de utilizador
	IF !uf_gerais_validaPermUser(m.ch_userno, m.ch_grupo, 'Remove acesso � Prescri��o')
		uf_perguntalt_chama("O seu perfil n�o permite aceder � Prescri��o Eletr�nica.", "OK", "", 64)
		RETURN	.f.
	ENDIF
		
	** Valida Licenciamento	
	IF (uf_ligacoes_addConnection('PE') == .f.)
		uf_perguntalt_chama("N�o existem licen�as dispon�veis. Por favor contacte o Suporte.", "OK", "", 64)
		RETURN .F.
	ENDIF
	
	IF TYPE("PESQUTENTES") != "U"
		uf_pesqutentes_sair()
	ENDIF
	
	IF uf_prescricao_validaUtilizador() == .f.
		Return .f.
	ENDIF
	
	**
	IF !EMPTY(lcPHC)
		TEXT TO lcSQL NOSHOW TEXTMERGE
			Select 
				TOP 1 utstamp
			from 
				b_utentes
			WHERE 
				b_utentes.idstamp = '<<ALLTRIM(lcUtstamp)>>'
		ENDTEXT
		
		IF uf_gerais_actgrelha("", "ucrsMapeamentoPHC", lcSql)
			IF RECCOUNT("ucrsMapeamentoPHC") > 0
				Select ucrsMapeamentoPHC
				lcUtstamp = ALLTRIM(ucrsMapeamentoPHC.utstamp)
			ENDIF 
		ENDIF 		
	ENDIF 	
	
	
	** se voltar a carregar, limpa form	
	IF(!EMPTY(lcPrescTipo))
		IF TYPE("PESQUTENTES") != "U"					
			PRESCRICAO.hide
			PRESCRICAO.release	
			RELEASE  PRESCRICAO
		ENDIF
	ENDIF
	
	
	
	**
	IF TYPE("PRESCRICAO") == "U" OR !EMPTY(lcPrescTipo)
		uf_presqPrescricao_cartaoUtenteConectado()
		uf_prescricao_carregaCursoresIniciais(lcnrPrescricao, lcEdicao, lcUtstamp)			
		
		*initpesq=1
		DO FORM PRESCRICAO
		uf_prescricao_MostraCCMenu(lcPrescTipo)
	ELSE
		
		PRESCRICAO.show
	ENDIF		
	
	
	
	IF !EMPTY(lcnrPrescricao) && AND lcEdicao == .f.

		uf_prescricao_limpaCursores()

		&& Cursor dos Cabe�alhos
		lcSQL = ''
		TEXT TO lcSQL NOSHOW TEXTMERGE
			SELECT  
				TOP 1 b_cli_efr.internacional, b_cli_presc.*
			from 
				b_cli_presc 
				inner join b_cli_efr on b_cli_presc.entidadeCod = b_cli_efr.cod 			
			WHERE 
				nrPrescricao = '<<ALLTRIM(lcnrPrescricao)>>'
		ENDTEXT
		
		uf_gerais_actgrelha("", "uCrsCabPresc", lcSql)
		
		&& Cursor das Linhas
		lcSQL = ''
		TEXT TO lcSQL NOSHOW TEXTMERGE
			Select 
				posologiaOriginal = posologiasugerida
				,designOutros = design
				,designMDT = design
				,designManipulado = design
				,descJust =CONVERT(varchar(600),(CASE WHEN  prescNomeDescr !='' then  '-> ' + prescNomeDescr  + CHAR(13) + CHAR(10) else '' END ) +		
				   (CASE WHEN  prescEmbDescr !='' then  '-> '  + prescEmbDescr + CHAR(13) + CHAR(10) else '' END ) +		
				   (CASE WHEN  prescDateDescr !='' then  '-> ' + prescDateDescr else '' END )) 
				,* 
			from 
				b_cli_prescl (nolock) 
			WHERE 
				nrPrescricao = '<<ALLTRIM(lcnrPrescricao)>>' 
			ORDER BY 
				lordem
		ENDTEXT
		
		uf_gerais_actgrelha("Prescricao.grdPresc", "ucrsLinPresc", lcSql)
				
		uf_prescricao_aplicaEstadoDescricaoCab()
	ENDIF

	SELECT ucrsCabPresc
	IF !EMPTY(ucrsCabPresc.utstamp)
		SELECT ucrsCabPresc
		TEXT TO lcSQL NOSHOW TEXTMERGE
			Select 
				dplmsstamp
				,u_design
				,diploma
				,patologia
			from 
				b_cli_diplomasID 
			where 
				idstamp = <<IIF(EMPTY(ALLTRIM(ucrsCabPresc.utstamp)) or ISNULL(ucrsCabPresc.utstamp),'',"'" + ALLTRIM(ucrsCabPresc.utstamp) +"'")>>
			order by 
				diploma
		ENDTEXT

		
		IF !uf_gerais_actgrelha("Prescricao.pageframe1.page1.pageframeUtentes.page3.grdPresc", "uCrsDiplomasPresc", lcSql)
			MESSAGEBOX("OCORREU UM ERRO A CARREGAR AS DIPLOMAS DO PACIENTE!",16,"LOGITOOLS SOFTWARE")
			RETURN .f.
		ENDIF
	ENDIF 
	

		
	IF !EMPTY(ucrsCabPresc.utstamp)
		TEXT TO lcSQL NOSHOW TEXTMERGE
			exec up_prescricao_PatologiasUtente <<IIF(EMPTY(ALLTRIM(ucrsCabPresc.utstamp)) or ISNULL(ucrsCabPresc.utstamp),'',"'" + ALLTRIM(ucrsCabPresc.utstamp) +"'")>>
		ENDTEXT
		IF !uf_gerais_actgrelha("Prescricao.pageframe1.page1.pageframeUtentes.page4.grdPresc", "uCrsPatologiasPresc", lcSql)
			MESSAGEBOX("OCORREU UM ERRO A CARREGAR AS PATOLOGIAS DO PACIENTE!",16,"LOGITOOLS SOFTWARE")
			RETURN .f.
		ENDIF
	ENDIF 
	
	SELECT ucrsCabPresc
	IF !EMPTY(ucrsCabPresc.no)
		lcSQL = ""
		Text to lcSql noshow textmerge
			exec up_prescricao_mcdtMenos90 <<ucrsCabPresc.no>>
		ENDTEXT
		IF !uf_gerais_actgrelha("Prescricao.pageframe1.page1.pageframeUtentes.page8.gridPresq", [uCrsmcdtMenosNoventa], lcSQL)
			MESSAGEBOX("Ocorreu um erro a procurar MCDTs prescritos � menos de 90 dias. Contacte o suporte.",16,"LOGITOOLS SOFTWARE")
			RETURN .f.
		ENDIF
	ENDIF
	IF !EMPTY(ucrsCabPresc.utstamp)
		lcSQL = ""
		Text to lcSql noshow textmerge
			exec up_prescricao_HistoricoMedUtente <<IIF(EMPTY(ALLTRIM(ucrsCabPresc.utstamp)) or ISNULL(ucrsCabPresc.utstamp),'',"'" + ALLTRIM(ucrsCabPresc.utstamp) +"'")>>
		ENDTEXT
		IF !uf_gerais_actgrelha("Prescricao.pageframe1.page1.pageframeUtentes.page9.gridPresq", [uCrsHistMedUtente], lcSQL)
			MESSAGEBOX("Ocorreu um erro a procurar hist�rico de medicamentos. Contacte o suporte.",16,"LOGITOOLS SOFTWARE")
			RETURN .f.
		ENDIF
	ENDIF
			
	SELECT ucrsLinPresc
	GO Top
	IF !empty(ucrsLinPresc.prescstamp)
		lcSQL = ""
		TEXT TO lcSQL NOSHOW TEXTMERGE
			exec up_prescricao_RespostaHistoricoEnvioOnline <<IIF(EMPTY(ALLTRIM(ucrsLinPresc.prescstamp)) or ISNULL(ucrsLinPresc.prescstamp),'',"'" + ALLTRIM(ucrsLinPresc.prescstamp) +"'")>>
		ENDTEXT
		IF !uf_gerais_actGrelha("Prescricao.pageframe1.page2.pageframeReceitaMedicamentos.page3.grdPresc", "uCrsRespostaEnvioOnline", lcSql)
			uf_perguntalt_chama("N�o foi possivel verificar historio do envio em modo online. Contacte o suporte.", "", "OK", 16) 
			RETURN .f.
		ENDIF
		IF !uf_gerais_actGrelha("Prescricao.pageframe1.page5.pageframeReceitaMCDT.page2.grdPresc", "uCrsRespostaEnvioOnlineMCDT", lcSql)
			uf_perguntalt_chama("N�o foi possivel verificar historio do envio em modo online. Contacte o suporte.", "", "OK", 16) 
			RETURN .f.
		ENDIF
	ENDIF

	SELECT ucrsCabPresc
	IF !EMPTY(ucrsCabPresc.nrPrescricao)

		uf_prescricao_actualizaQttTotalReceita()
		lcnrPrescricao = ucrsCabPresc.nrPrescricao
		Prescricao.nrPrescricao = lcnrPrescricao 
		uf_prescricao_ActualizaRespostas()
		Prescricao.lblPrescricao.Caption = "Prescri��o: " + lcnrPrescricao
		Prescricao.lblPrescricao.refresh
		
	

	ENDIF	
	
	** Trata Parametros 
	IF !EMPTY(lcEdicao)
		uf_prescricao_novo()
		
		IF !EMPTY(lcUtstamp) AND !EMPTY(lcEdicao) && Caso venha o n�mero de utente preenchido
			
			**
			lcSQL = ""
			Text to lcSql noshow textmerge
				 exec up_utentes_pesquisa <<IIF(EMPTY(ALLTRIM(lcUtstamp)) or ISNULL(lcUtstamp),'',"'" + ALLTRIM(lcUtstamp) +"'")>>,'', '','', '' 
			ENDTEXT
			IF !uf_gerais_actgrelha("", [uCrsPesquisarCL], lcSQL)
				MESSAGEBOX("N�o foi poss�vel verificar informa��o do utente.",16,"LOGITOOLS SOFTWARE")
				RETURN .f.
			ENDIF
			uf_pesqutentes_escolhePresc()
		
		ENDIF
	ENDIF 


	prescricao.refresh
	
	IF type("marcacoes")!="U"
		initpesq=1
	ENDIF 
	
	IF initpesq=0
		prescricao.hide
**		IF TYPE("PESQPRESCRICAO")=="U"
**			DO FORM PESQPRESCRICAO
**		ELSE
**			PESQPRESCRICAO.show
**		ENDIF
		uf_pesqPrescricao_Chama()
		initpesq=1
	endif

ENDFUNC

FUNCTION uf_prescricao_MostraCCMenu
	LPARAMETERS  lcPrescTipo

    && se vazio prescreve por RSP
    IF(EMPTY(lcPrescTipo))
    	lcPrescTipo='RSP'
    ENDIF
    
 
	&& na nova vers�o, prescreve sempre por RSP  defeito
    IF  ALLTRIM(lcPrescTipo)=='RSP' AND uf_gerais_getParameter("ADM0000000295","BOOL") == .f.
    	prescricao.lblCC.visible=.f.
		prescricao.shCC.visible=.f.
    	myPrescMaterializada = .f.
    	IF((VARTYPE(PRESCRICAO.menu_opcoes.NovaReceitaTipo))=='U')
    		PRESCRICAO.menu_opcoes.adicionaOpcao("NovaReceitaTipo","Receita RCP","","uf_prescricao_novaReceitaTipo with 'RCP'")
    		PRESCRICAO.Label2.caption = "Receita RSP"
    	ENDIF
    		
    	RETURN .t.
    ENDIF
    
    && na nova vers�o, prescreve RCP se for explicitamente pedido
    IF(ALLTRIM(lcPrescTipo)=='RCP' AND uf_gerais_getParameter("ADM0000000295","BOOL") == .f.)
    	prescricao.lblCC.visible=.f.
		prescricao.shCC.visible=.f.
    	myPrescMaterializada = .t.	
    	IF((VARTYPE(PRESCRICAO.menu_opcoes.NovaReceitaTipo))=='U')
    		PRESCRICAO.menu_opcoes.adicionaOpcao("NovaReceitaTipo","Receita RSP","","uf_prescricao_novaReceitaTipo with 'RSP'")
    		PRESCRICAO.Label2.caption = "Receita RCP"
    	ENDIF  		   		
    	RETURN .t.
    ENDIF
    

    ** vers�o antiga
    IF(myPrescMaterializada=.f.)
    	prescricao.lblCC.visible=.f.
		prescricao.shCC.visible=.f.
    ENDIF
    
	RETURN .t.
ENDFUNC




** Verifica se tem cartao de utente conectato
FUNCTION uf_presqPrescricao_cartaoUtenteConectado
	** valida se o cart�o de cidad�o est� inserido de forma "ativar" a prescri��o de RSPs
	
    

	IF UPPER(ALLTRIM(uCrsE1.tipoEmpresa)) == "CLINICA"
	
		PUBLIC myPrescMaterializada
		STORE .t. TO myPrescMaterializada
	    
		
		lcSQL = ''
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			SELECT cc.id FROM b_us b (nolock) INNER JOIN dispensa_eletronica_cc cc (nolock) ON b.ncont = cc.nif WHERE b.userno = <<ch_vendedor>>
		ENDTEXT 

		IF !uf_gerais_actgrelha("","uCrsIDAux", lcSQL)
			uf_perguntalt_chama("Ocorreu uma anomalia ao verificar ao informa��o do Cart�o do Cidad�o. Por favor contacte o Suporte.", "OK", "", 64)
			RETURN .F.		
		ELSE
			IF RECCOUNT("uCrsIDAux") != 1
				&&uf_perguntalt_chama("N�o existe registo de utilizador que permita efetuar Prescri��es Desmaterializadas. As Prescri��es a efetuar ser�o Materializadas.", "OK", "", 64)	
				myPrescMaterializada = .t.					
				
			ELSE
				myPrescMaterializada = .f.
				
			ENDIF
		ENDIF
		
	ENDIF
	
ENDFUNC 


**
FUNCTION uf_prescricao_carregaCursoresIniciais
	LPARAMETERS lcnrPrescricao,lcEdicao,lcUtstamp
	
	IF !USED("ucrsCabPresc")
		&& Cursor dos Cabe�alhos
		TEXT TO lcSQL NOSHOW TEXTMERGE
			Select Top 0 * from b_cli_presc (nolock)
		ENDTEXT
		uf_gerais_actgrelha("", "ucrsCabPresc", lcSql)
	ENDIF 
	
	IF !USED("ucrsLinPresc")
		TEXT TO lcSQL NOSHOW TEXTMERGE
			Select Top 0  posologiaOriginal = posologiasugerida, designOutros = design, designMDT = design, designManipulado = design,
			descJust = CONVERT(varchar(600),(CASE WHEN  prescNomeDescr !='' then  '-> ' + prescNomeDescr + CHAR(13) + CHAR(10) else '' END ) +		
				   (CASE WHEN  prescEmbDescr !='' then  '-> '  + prescEmbDescr + CHAR(13) + CHAR(10) else '' END ) +		
				   (CASE WHEN  prescDateDescr !='' then  '-> ' + prescDateDescr else '' END )) 		
			,* 
			from b_cli_prescl (nolock)
		ENDTEXT
		uf_gerais_actgrelha("", "ucrsLinPresc", lcSql)
	ENDIF 
	
	IF !USED("uCrsDiplomasPresc")
		create cursor uCrsDiplomasPresc(dplmsstamp c(25),u_design c(254), diploma c(254), patologia c(200))
	ENDIF 
	IF !USED("uCrsPatologiasPresc")
		create cursor uCrsPatologiasPresc(descricao c(200))
	ENDIF 
	
	IF !used("uCrsRespostaEnvioOnline")
		lcSQL = ""
		TEXT TO lcSQL NOSHOW TEXTMERGE
			exec up_prescricao_RespostaHistoricoEnvioOnline ''
		ENDTEXT
		IF !uf_gerais_actGrelha("", "uCrsRespostaEnvioOnline", lcSql)
			uf_perguntalt_chama("N�o foi possivel verificar historio do envio em modo online. Contacte o suporte.", "", "OK", 16) 
			RETURN .f.
		ENDIF
	ENDIF
	IF !used("uCrsRespostaEnvioOnlineMCDT")
		lcSQL = ""
		TEXT TO lcSQL NOSHOW TEXTMERGE
			exec up_prescricao_RespostaHistoricoEnvioOnline ''
		ENDTEXT
		IF !uf_gerais_actGrelha("", "uCrsRespostaEnvioOnlineMCDT", lcSql)
			uf_perguntalt_chama("N�o foi possivel verificar historio do envio em modo online. Contacte o suporte.", "", "OK", 16) 
			RETURN .f.
		ENDIF
	ENDIF
	
	&&Cursor de MDCT Menos 90 Dias
	IF !used("uCrsmcdtMenosNoventa")
		lcSQL = ""
		Text to lcSql noshow textmerge
			exec up_prescricao_mcdtMenos90 0
		ENDTEXT
		IF !uf_gerais_actgrelha("", "uCrsmcdtMenosNoventa", lcSql)
			MESSAGEBOX("Ocorreu um erro a procurar MCDTs prescritos � menos de 90 dias. Contacte o suporte.",16,"LOGITOOLS SOFTWARE")
			RETURN .f.
		ENDIF
	ENDIF
	
	&&Cursor de Historico Medicamentos por utente
	IF !used("uCrsHistMedUtente")
		lcSQL = ""
		Text to lcSql noshow textmerge
			exec up_prescricao_HistoricoMedUtente ''
		ENDTEXT
		IF !uf_gerais_actgrelha("", "uCrsHistMedUtente", lcSql)
			MESSAGEBOX("Ocorreu um erro a procurar historico de Medicamentos. Contacte o suporte.",16,"LOGITOOLS SOFTWARE")
			RETURN .f.
		ENDIF
	ENDIF
		
	* Cria Cursor de Alergias
	If !Used("uCrsVerAlergias")
		create cursor uCrsVerAlergias (alergia c(254))
	ENDIF

	* Cria Cursor de Contra Indica��es
	If !Used("uCrsContraIndicacoes")
		create cursor uCrsContraIndicacoes(descricao c(254), patol c(254), nvl c(254), frase M)
	ENDIF

	* Cria Cursor de Interacoes
	Text to lcSql noshow textmerge
		exec up_prescricao_interacoes 'XXXXX','',''
	ENDTEXT
	IF !uf_gerais_actgrelha("", "uCrsInteraccoes", lcSql)
		MESSAGEBOX("OCORREU UM ERRO A CARREGAR INTERACCOES!",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF

	* Cria Cursor de Interacoes Temporario
	Text to lcSql noshow textmerge
		exec up_prescricao_interacoes 'XXXXX','',''
	ENDTEXT
	IF !uf_gerais_actgrelha("", "uCrsInteraccoesTemp", lcSql)
		MESSAGEBOX("OCORREU UM ERRO A CARREGAR INTERACCOES!",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF
	
	IF !USED("uCrsProdInteraccoes")
		CREATE cursor uCrsProdInteraccoes(ref c(18), design c(254))
	ENDIF

	* Cria Cursor de Genericos Alternativos
	If !Used("uCrsProdutosGenAlt")
		create cursor uCrsProdutosGenAlt(refori c(18), sel l, ref c(18), design c(254), epv1 n(9,2), comp n(1), grphmg c(254), psico l, benzo l)
	ENDIF
	* Cria Cursor de Produtos Mais Baratos
	If !Used("uCrsProdutosPrescMaisBaratosAux")
		
		Text to lcSql noshow textmerge
			exec up_prescricao_MedEquivComGH 'XXXXXXXXXXX',0.0,'',0.0,0.0
		ENDTEXT
		IF !uf_gerais_actgrelha("", "uCrsProdutosPrescMaisBaratosAux", lcSql)
			MESSAGEBOX("OCORREU UM ERRO A CARREGAR PRODUTOS MAIS BARATOS!",16,"LOGITOOLS SOFTWARE")
			RETURN .f.
		ENDIF
	ENDIF
	
	** Cursor de Mapaeamento entre os cnp e diplomas
	If !Used("uCrsRelacaoCnpDiploma")
		
		Text to lcSql noshow textmerge
			exec up_prescricao_RelacaoCnpDiploma			
		ENDTEXT
		IF !uf_gerais_actgrelha("", "uCrsRelacaoCnpDiploma", lcSql)
			MESSAGEBOX("Ocorreu um erro a determinar a rela��o de diplomas!",16,"LOGITOOLS SOFTWARE")
			RETURN .f.
		ENDIF
	ENDIF
	
ENDFUNC 


**  Calcula o N� de Prescri��o - n� interno software
FUNCTION uf_prescricao_geranrPrescricao
	LOCAL lcTermNo, lcNrPrescricao
	PUBLIC myTermno

	lcSQL = ''
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_prescricao_NrPrescricao <<myTermno>>
	ENDTEXT 
	IF !uf_gerais_actgrelha("", "uCrsNrPrescricao", lcSQL)
		uf_perguntalt_chama("N�o foi poss�vel calcular o n�mero de prescri��o. Contacte o suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF
		
	SELECT uCrsNrPrescricao
	lcNrPrescricao = ALLTRIM(uCrsNrPrescricao.nrPrescricao)
		
	RETURN lcNrPrescricao
ENDFUNC


&& Calculo nr de Receita Local 
FUNCTION uf_prescricao_geranrReceitaLocal
	LPARAMETERS lnRenovavel
	LOCAL lcNrReceitaLocal 

	lcSQL = ''
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_prescricao_NrReceitaLocal '<<mySite>>', '<<lnRenovavel>>'
	ENDTEXT 
	
	
	IF !uf_gerais_actgrelha("", "uCrsNrReceitaLocal", lcSQL)
		uf_perguntalt_chama("N�o foi poss�vel calcular o n�mero de Receita Local. Contacte o suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF
	

	SELECT uCrsNrReceitaLocal

	lcNrReceitaLocal = ALLTRIM(uCrsNrReceitaLocal.nrReceitaLocal)
	
		
	RETURN lcNrReceitaLocal 
ENDFUNC


**
FUNCTION uf_prescricao_init

	&& Configura menu principal
	WITH PRESCRICAO.menu1
	    .adicionaOpcao("opcoes","Op��es",myPath + "\imagens\icons\opcoes_w.png","","O")
   	    .adicionaOpcao("utente","Utente",myPath + "\imagens\icons\ut_mais_w.png","uf_select_utente","O")
		.adicionaOpcao("pesquisar","Pesquisar",myPath + "\imagens\icons\lupa_w.png","uf_pesqPrescricao_Chama","P")
		.adicionaOpcao("novo","Novo",myPath + "\imagens\icons\mais_w.png","uf_prescricao_novo","N")
		.adicionaOpcao("editar","Editar",myPath + "\imagens\icons\lapis_w.png","uf_prescricao_editar","E")
		.adicionaOpcao("novaReceita","NovaReceita",myPath + "\imagens\icons\mais_w.png","uf_prescricao_novaReceita")
		.adicionaOpcao("novoMed","Medicamento",myPath + "\imagens\icons\stocks_mais_w.png","uf_prescricao_chamaCallbackMenu WITH 1")

		IF uf_ligacoes_verificaConfiguracaoLicenca('MC')
			.adicionaOpcao("mcdt","MCDT",myPath + "\imagens\icons\stocks_mais_w.png","uf_prescricao_chamaCallbackMenu WITH 2")
		ENDIF 
		.adicionaOpcao("outros", "Outros", myPath + "\imagens\icons\stocks_mais_w.png", "uf_gerais_menuAplicacoes WITH 'PRESCRICAO','Outros Produtos'","O")
		.adicionaOpcao("limpar", "Limpar", myPath + "\imagens\icons\limpar_w.png","uf_prescricao_limpar", "L")
		&&:TODO
		.adicionaOpcao("imprimir", "Imprimir", myPath + "\imagens\icons\imprimir_w.png", "uf_prescricao_imprimir WITH .t.")
		.adicionaOpcao("envcod", "Enviar Cod.", myPath + "\imagens\icons\utilizador_b_lateral.png", "uf_prescricao_enviar_codigo")
		&&.adicionaOpcao("imprimir", "Imprimir", myPath + "\imagens\icons\imprimir_w.png", "uf_prescricao_Prever")		
		&&.adicionaOpcao("imprimir", "Imprimir", myPath + "\imagens\icons\imprimir_w.png", "uf_prescricao_Prever")
		
	ENDWITH
	
	&& configura menu de op��es
	WITH PRESCRICAO.menu_opcoes	
		.adicionaOpcao("imprimir", "Imprimir", "", "uf_prescricao_imprimir WITH .t.")
		.adicionaOpcao("prever", "Prever Receita", "", "uf_prescricao_Prever")	
		
	ENDWITH
	
	WITH PRESCRICAO.menu_aplicacoes
		** Aplica��es
		.adicionaOpcao("manipulado", "Manipulado", "", "uf_prescricao_adicionaManipulado")
		.adicionaOpcao("genAlimenticio", "Gen.Aliment.", "", "uf_prescricao_adicionaGenAlimenticio")
		.adicionaOpcao("outrosProd", "OutrosProds.", "", "uf_prescricao_adicionaOutrosProds")
		.adicionaOpcao("receitaUE", "Tipo UE", "", "uf_prescricao_adicionaTipoUE")
	ENDWITH
	
	uf_prescricao_alternaMenu()
ENDFUNC

&& 0 - n�o existem linhas
&& 1 - receita
&& 2 - mcdt

FUNCTION uf_prescricao_validaSeReceita
	
	
	If(USED("ucrsLinPresc"))
		
		&& valida se receita
		SELECT ucrsLinPresc
		GO TOP
		LOCATE FOR ALLTRIM(Tiporeceita)  != "MCDT"
		IF FOUND()
		  RETURN "1"
		ENDIF
		
		&& valida se mcdt
		SELECT ucrsLinPresc
		GO TOP
		LOCATE FOR ALLTRIM(Tiporeceita) = "MCDT"
		IF FOUND()
		  RETURN "2"
		ENDIF
		
	ENDIF
	
	&&vazio
	RETURN ""
	
ENDFUNC



FUNCTION uf_prescricao_chamaCallbackMenu
	LPARAMETERS lcTipo 	
	LOCAL lcTipoReceita

	lcTipoReceita= uf_prescricao_validaSeReceita()
	
	
	DO CASE
	CASE lcTipo = 1 AND (lcTipoReceita== "1" OR EMPTY(ALLTRIM(lcTipoReceita) ))
	   uf_pesqmedicamentos_chama()
	CASE lcTipo = 2 AND (lcTipoReceita== "2" OR EMPTY(ALLTRIM(lcTipoReceita) ))
	   uf_pesquisaMcdt_Chama()
	OTHERWISE
		uf_perguntalt_chama("N�o mesma prescri��o n�o podem existir receitas RSP e MCDT", "", "OK", 64)   
	ENDCASE
	

ENDFUNC



FUNCTION uf_select_utente
	uf_prescricao_selecionaPg("Utente")
	**
	IF myPrescricaoIntroducao == .t. OR myPrescricaoAlteracao == .t.
		IF uf_prescricao_validaLinhasProcessadas()== .t.
			uf_perguntalt_chama("J� existem receitas processadas, n�o � possivel trocar o utente.", "", "OK", 64)
		ELSE	
			uf_pesqUtentes_Chama("PRESCRICAO")
		ENDIF	
	ENDIF
ENDFUNC 

**
FUNCTION uf_prescricao_duplicar
	LOCAL lcLinStamp, lcCabstamp, lcDataValidade, i
	lcCabstamp = uf_gerais_stamp()
		
	**
	* 1 - Verificar se o ecra n�o est� em modo de edi��o
	* 2 - Gardar informa��o de cabe�alho e linhas para cursor auxiliar
	* 3 - Chamar fun��o introdu��o
	* 4 - chamar pesquisa utentes e introduzir
	* 5 - Precorrer linhas do cursor auxiliar e introduzir novas linhas
	
	* 1
	IF myPrescricaoIntroducao == .t. OR myPrescricaoAlteracao == .t.
		RETURN .f.
	ENDIF
	* 2
	SELECT * FROM UcrsCabPresc INTO CURSOR  ucrsCabPrescDupl READWRITE 
	SELECT * FROM UcrsLinPresc INTO CURSOR  ucrsLinPrescDupl READWRITE 
	* 3
	uf_prescricao_novo()
	* 4
	SELECT ucrsCabPrescDupl
	IF !empty(ALLTRIM(ucrsCabPrescDupl.Utstamp))
		lcSQL = ""
		Text to lcSql noshow textmerge
			 exec up_utentes_pesquisa <<IIF(EMPTY(ALLTRIM(ucrsCabPrescDupl.Utstamp)) or ISNULL(ucrsCabPrescDupl.Utstamp),'',"'" + ALLTRIM(ucrsCabPrescDupl.Utstamp) +"'")>>,'', '','', '' 
		ENDTEXT
		IF !uf_gerais_actgrelha("", [uCrsPesquisarCL], lcSQL)
			MESSAGEBOX("N�o foi poss�vel verificar informa��o do utente.",16,"LOGITOOLS SOFTWARE")
			RETURN .f.
		ENDIF
		uf_pesqutentes_escolhePresc()
	ENDIF 
	* 5
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE
		Select  posologiaOriginal = posologiasugerida
				, designOutros = design
				, designMDT = design
				, designManipulado = design
				, descJust =CONVERT(varchar(600),(CASE WHEN  prescNomeDescr !='' then  '-> ' + prescNomeDescr + CHAR(13) + CHAR(10) else '' END ) +		
				   (CASE WHEN  prescEmbDescr !='' then  '-> '  + prescEmbDescr + CHAR(13) + CHAR(10) else '' END ) +		
				   (CASE WHEN  prescDateDescr !='' then  '-> ' + prescDateDescr else '' END )) 
				, * 
		from 
			b_cli_prescl (nolock) 
		WHERE 
			nrPrescricao = '<<ALLTRIM(ucrsCabPrescDupl.nrprescricao)>>'
	ENDTEXT
	IF !uf_gerais_actgrelha("prescricao.grdPresc", [uCrsLinPresc], lcSQL)
		MESSAGEBOX("N�o foi poss�vel verificar informa��o da Receita.",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF
	&& recria ordena��o
	SELECT ucrsLinPresc
	index on lordem TAG lordem 
	
	SELECT ucrsLinPresc
	GO Top
	SCAN
	
		IF LEFT(ALLTRIM(ucrsLinPresc.design),1) == "."
			lcCabstamp = uf_gerais_stamp()
		ENDIF
		
		SELECT ucrsLinPresc
		Replace ucrsLinPresc.prescstamp WITH lcCabstamp 
		lcLinstamp = uf_gerais_stamp()
		lcstampEnvioOnline = uf_gerais_stamp()
		SELECT ucrsLinPresc
		Replace ucrsLinPresc.stampEnvioOnline 	WITH lcstampEnvioOnline
		Replace ucrsLinPresc.presclstamp 		WITH lcLinstamp 
		Replace ucrsLinPresc.estado 			WITH "0"
		Replace ucrsLinPresc.estadodescr 		WITH "Receita n�o submetida"
		Replace ucrsLinPresc.anulado 			WITH .f.
		Replace ucrsLinPresc.nrPrescricao 		WITH prescricao.nrPrescricao			
		Replace ucrsLinPresc.terminal 			WITH Prescricao.terminal
		Replace ucrsLinPresc.prescno  			WITH ""
		Replace ucrsLinPresc.prescno2 			WITH ""
		Replace ucrsLinPresc.prescno3  			WITH ""
		Replace ucrsLinPresc.data   			WITH ucrsCabPresc.data
		
		* Calula validade  - por defeito coloca 30 dias na validade para receitas n�o renovaveis e 6 meses para renovaveis
**		lcSQL = ""
**		IF ucrsLinPresc.renovavel == .t.
**			TEXT TO lcSQL NOSHOW TEXTMERGE
**				Select validade = DATEADD(mm,6,'<<uf_gerais_getdate(ucrscabpresc.data,"SQL")>>')  
**			ENDTEXT
**		ELSE
**			TEXT TO lcSQL NOSHOW TEXTMERGE
**				Select validade = DATEADD(dd,30,'<<uf_gerais_getdate(ucrscabpresc.data,"SQL")>>')  
**			ENDTEXT
**		ENDIF
**		IF !uf_gerais_actGrelha("", "UcrsValidadePresc", lcSql)
**			uf_perguntalt_chama("N�o foi possivel calcular a validade da receita. Contacte o suporte.", "", "OK", 16) 
**			RETURN .f.
**		ENDIF
		****
		lcDataValidade = uf_prescricao_PreencheValidadePorLinha(myPrescMaterializada,ucrslinPresc.tipoduracao,ucrslinPresc.prescNomeCod,ucrslinPresc.prescDateCod)
		Replace ucrsLinPresc.validade 	WITH lcDataValidade
		Replace ucrsLinPresc.renovavel	WITH IIF(!EMPTY(ucrslinPresc.prescDateCod),.t.,.f.)		
	ENDSCAN
	SELECT ucrsLinPresc
	GO Top
	
	prescricao.grdPresc.refresh
	uf_prescricao_ActualizaEncargos()

ENDFUNC


**
FUNCTION uf_prescricao_alternaMenu
	
	**
	** Estados:
	** . 0 - Ainda nao foi enviado online
	** . 1 - Enviado e validado online
	** . 2 - Enviado e n�o validado online
	** . 3 - Offline
	
	** Em modo de Leitura
	IF myPrescricaoAlteracao == .f. AND myPrescricaoIntroducao == .f.
	
		Prescricao.menu1.estado("pesquisar, novo, editar, imprimir, envcod", "SHOW", "Gravar", .f., "Sair", .t.) 
		
		IF uf_ligacoes_verificaConfiguracaoLicenca('MC')
			Prescricao.menu1.estado("novaReceita, novoMed, mcdt, outros,  limpar", "HIDE") 
		ELSE
			Prescricao.menu1.estado("novaReceita, novoMed, outros,  limpar", "HIDE")
		ENDIF 
	ELSE
		** menu1
		Prescricao.menu1.estado("pesquisar, novo, editar, imprimir, envcod", "HIDE", "Gravar", .t., "Cancelar", .t.)

		IF uf_ligacoes_verificaConfiguracaoLicenca('MC')
			Prescricao.menu1.estado("novaReceita, novoMed, mcdt, outros, limpar", "SHOW")
		ELSE
			Prescricao.menu1.estado("novaReceita, novoMed, outros, limpar", "SHOW")
		ENDIF 
	ENDIF
	
	Prescricao.refresh
ENDFUNC


**
FUNCTION uf_prescricao_expandeDetalhe

	If Prescricao.detalhesVisiveis == .f.
		Prescricao.grdPresc.height = Prescricao.grdPresc.height + 200
		Prescricao.detalhesVisiveis = .t.
		Prescricao.containerTotal.lblCaminho.visible = .f.
		Prescricao.PageFrame1.visible = .f.

		Prescricao.containerTotal.check1.image1.Picture = myPath + "\imagens\icons\pageup_B.png"
	ELSE
		Prescricao.grdPresc.height = Prescricao.grdPresc.height - 200
		Prescricao.detalhesVisiveis = .f.
		
		DO CASE
			CASE Prescricao.PageFrame1.ActivePage = 1
				Prescricao.containerTotal.lblCaminho.Caption = "Utente"
			CASE Prescricao.PageFrame1.ActivePage = 2
				Prescricao.containerTotal.lblCaminho.Caption = "Receita"
			CASE Prescricao.PageFrame1.ActivePage = 3
				Prescricao.containerTotal.lblCaminho.Caption = "Medicamento"
			CASE Prescricao.PageFrame1.ActivePage = 3
				Prescricao.containerTotal.lblCaminho.Caption = "MCDT"
			OTHERWISE 
				**
		ENDCASE 		

		Prescricao.containerTotal.lblCaminho.visible = .t.
		Prescricao.PageFrame1.visible = .t.
		Prescricao.containerTotal.check1.image1.Picture = myPath + "\imagens\icons\pagedown_B.png"
	ENDIF
	
	Prescricao.containerTotal.top = Prescricao.grdPresc.height + 120
ENDFUNC


**
FUNCTION uf_prescricao_ultimoRegisto

	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_prescricao_ultimoRegisto <<ch_userno>>, '<<mysite>>'
	ENDTEXT 
	
	IF !uf_gerais_actgrelha("", "uCrsNrUltimoRegisto", lcSQL)
		uf_perguntalt_chama("N�o foi poss�vel verificar o ultimo registo de prescri��o. Contacte o suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF

	SELECT uCrsNrUltimoRegisto
	IF !EMPTY(uCrsNrUltimoRegisto.nrprescricao)
		uf_prescricao_chama(uCrsNrUltimoRegisto.nrprescricao)
	ENDIF
 	
ENDFUNC


**
FUNCTION uf_prescricao_editar
	LOCAL lcLinstamp 
	lcLinstamp = ""
	Select ucrsCabPresc
	IF empty(ucrsCabPresc.nrPrescricao)
		uf_perguntalt_chama("Deve selecionar um registo para editar. Verifique por favor.", "", "OK", 64)
		RETURN .f.
	Endif

	IF myPrescricaoAlteracao == .t. OR myPrescricaoIntroducao == .t.
		uf_perguntalt_chama("O ecr� de Prescri��o j� est� em modo de edi��o.", "", "OK", 64)
		RETURN .f.
	ENDIF

	SELECT ucrsLinPresc	
	lcLinstamp = ucrsLinPresc.presclstamp
		
	** Verifica se existem linha passiveis de altera��o (Estado != 1 online Ou Estado != 3 offline)
	IF uf_prescricao_validaAlteracao() == .f.
		uf_perguntalt_chama("Todas as receitas foram processadas pelo modo online ou pelo modo offline. N�o � possivel realizar altera��es."+ CHR(13) + CHR(13) +"Verifique o estado das Receitas.","", "OK", 64)
		RETURN .f.
	ENDIF

	** Actualiza informa��o do Utente
	TEXT TO lcSQL TEXTMERGE noshow
		exec up_utentes_pesquisa <<IIF(EMPTY(ALLTRIM(ucrsCabPresc.utstamp)) or ISNULL(ucrsCabPresc.utstamp),'',"'" + ALLTRIM(ucrsCabPresc.utstamp) +"'")>>,'','','',''
	ENDTEXT
	IF !uf_gerais_actGrelha("", "uCrsPesquisarCL", lcSQL)
		uf_perguntalt_chama("N�o foi possivel encontrar informa��o do utente. Selecione novamente o Utente.","", "OK", 64)
		RETURN .f.
	ENDIF
	IF RECCOUNT("uCrsPesquisarCL") > 0
		SELECT uCrsPesquisarCL
		Go Top
		uf_pesqutentes_escolhePresc()
	ENDIF


	** Actualiza informa��o do Utilizador/Especialista
	uf_prescricao_ActualizaDadosUtilizadorCabecalho()
			
	uf_prescricao_colocaNaLinha(lcLinstamp)

	&& recria ordena��o
	SELECT ucrsLinPresc
	index on lordem TAG lordem 
	

	STORE .t. TO myPrescricaoAlteracao
	STORE .f. TO myPrescricaoIntroducao

	uf_prescricao_alternaMenu()
ENDFUNC 


**
FUNCTION uf_prescricao_validaAlteracao 
	SELECT ucrsLinPresc
	LOCATE FOR (ucrsLinPresc.estado != "1" AND ucrsLinPresc.estado != "3" AND ucrsLinPresc.anulado != .t.)
	IF FOUND()
		RETURN .t.
	ELSE
		RETURN .f.
	ENDIF	
ENDFUNC


**
FUNCTION uf_prescricao_validaLinhasProcessadas 
	SELECT ucrsLinPresc
	LOCATE FOR (ucrsLinPresc.estado == "1" OR ucrsLinPresc.estado == "3")
	IF FOUND()
		RETURN .t.
	ELSE
		RETURN .f.
	ENDIF	
ENDFUNC


**
FUNCTION uf_prescricao_novo

	IF TYPE("PESQPRESCRICAO")<>"U"	
		pesqprescricao.hide
	ENDIF 
	prescricao.show
	IF myPrescricaoAlteracao == .t. OR myPrescricaoIntroducao == .t.
		uf_perguntalt_chama("O ecr� de Prescri��o j� est� em modo de introdu��o.", "", "OK", 64)
		RETURN .f.
	ENDIF
	
	STORE .f. TO myPrescricaoAlteracao
	STORE .t. TO myPrescricaoIntroducao 
	
	** Limpa Cursores
	IF USED("ucrsCabPresc")
		SELECT ucrsCabPresc
		GO TOP 
		SCAN
			Delete		
		ENDSCAN
	ENDIF
	
	IF USED("ucrsLinPresc")
		SELECT ucrsLinPresc
		GO TOP 
		SCAN
			Delete		
		ENDSCAN
	ENDIF

	SELECT ucrsCabPresc
	APPEND BLANK
	Replace ucrsCabPresc.data WITH Date()

	&& Nr de Prescri��o
	lcNrPrescricao 			= uf_prescricao_geranrPrescricao()
	Prescricao.terminal 	= myTermno
	Prescricao.nrPrescricao = lcnrPrescricao 
	Prescricao.lblPrescricao.Caption = "Prescri��o: " + lcnrPrescricao
	Prescricao.lblPrescricao.refresh

	Replace ucrsCabPresc.nrprescricao WITH lcnrPrescricao

	uf_prescricao_ActualizaDadosUtilizadorCabecalho()
	
	uf_prescricao_alternaMenu()
	
	&& Aplica ultimo utente
	IF !EMPTY(Prescricao.ultimoutstamp)
		**
		lcSQL = ""
		TEXT TO lcSql NOSHOW TEXTMERGE 
			 exec up_utentes_pesquisa <<IIF(EMPTY(ALLTRIM(Prescricao.ultimoutstamp)) or ISNULL(Prescricao.ultimoutstamp),'',"'" + ALLTRIM(Prescricao.ultimoutstamp) +"'")>>,'', '','', '' 
		ENDTEXT
		IF !uf_gerais_actgrelha("", [uCrsPesquisarCL], lcSQL)
			MESSAGEBOX("N�o foi poss�vel verificar informa��o do utente.",16,"LOGITOOLS SOFTWARE")
			RETURN .f.
		ENDIF
		
		uf_pesqutentes_escolhePresc()
	ENDIF 
	uf_prescricao_selecionaPg("Utente")
	**
ENDFUNC


**
FUNCTION uf_prescricao_ActualizaDadosUtilizadorCabecalho
	
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		 exec up_prescricao_ValidaUtilizador <<ch_userno>>
	ENDTEXT
	If !uf_gerais_actGrelha("", "uCrsDadosUtilizador", lcSql)
		uf_perguntalt_chama("N�o foi poss�vel verificar os dados do utilizador. Contacte o suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF
	
	SELECT uCrsDadosUtilizador
	GO Top
	SELECT ucrsCabPresc
	Replace ucrsCabPresc.drstamp 			WITH uCrsDadosUtilizador.usstamp
	Replace ucrsCabPresc.drnome 			WITH uCrsDadosUtilizador.nome
	Replace ucrsCabPresc.drno 				WITH uCrsDadosUtilizador.userno
	Replace ucrsCabPresc.drcedula 			WITH uCrsDadosUtilizador.drcedula
	Replace ucrsCabPresc.drinscricao		WITH uCrsDadosUtilizador.drinscri
	Replace ucrsCabPresc.drordem			WITH uCrsDadosUtilizador.drordem
	Replace ucrsCabPresc.drmorada 			WITH uCrsDadosUtilizador.morada
	Replace ucrsCabPresc.drlocal 			WITH uCrsDadosUtilizador.local
	Replace ucrsCabPresc.drcodpost			WITH uCrsDadosUtilizador.codpost
	Replace ucrsCabPresc.drtelemovel		WITH uCrsDadosUtilizador.tlmvl
	Replace ucrsCabPresc.drtelefone			WITH uCrsDadosUtilizador.tlmvl && Campo impresso
	Replace ucrsCabPresc.dremail			WITH uCrsDadosUtilizador.email
	Replace ucrsCabPresc.drsexo				WITH uCrsDadosUtilizador.sexo
	Replace ucrsCabPresc.drnacionalidade	WITH uCrsDadosUtilizador.nacion
	Replace ucrsCabPresc.drtratamento		WITH uCrsDadosUtilizador.tratamento
	Replace ucrsCabPresc.drespecialidade	WITH uCrsDadosUtilizador.especialidade  
	Replace ucrsCabPresc.drnascimento		WITH uCrsDadosUtilizador.dtnasc
	
ENDFUNC


**
FUNCTION uf_prescricao_anular
	IF myPrescricaoIntroducao == .t. OR myPrescricaoAlteracao == .t.
		RETURN .f.
	ENDIF

	SELECT ucrsLinPresc
	IF LEFT(ALLTRIM(ucrsLinPresc.design),1) != "." 
		uf_perguntalt_chama("Deve selecionar o cabe�alho da receita para proceder � anula��o.", "", "OK", 64)
		RETURN .f.
	ENDIF
		
	SELECT ucrsLinPresc
	IF ucrsLinPresc.anulado == .t.
		uf_perguntalt_chama("A receita j� foi anulada.", "", "OK", 64)
		RETURN .f.
	ENDIF
	
	SELECT ucrsCabPresc
	IF ch_userno != ucrsCabPresc.drno
		uf_perguntalt_chama("Uma receita apenas pode ser anulada pelo Prescritor que criou a receita.", "", "OK", 64)
		RETURN .f.
	ENDIF
	
	SELECT ucrsLinPresc
	IF ucrsLinPresc.estado != "1" AND estado != "3"
		uf_perguntalt_chama("A receita ainda n�o foi enviada pelo metodo online ou offline. N�o � poss�vel realizar a anula��o de receita.", "", "OK", 64)
		RETURN .f.
	ENDIF
	
	lcSQL = ""		
	TEXT TO lcSql NOSHOW textmerge
		SELECT DATEDIFF(dd,'<<uf_gerais_getdate(ucrsCabPresc.data,"SQL")>>', dateadd(HOUR, <<difhoraria>>, getdate())) as ndias
	ENDTEXT
	
	If !uf_gerais_actGrelha("", "uCrsPrescDataAnulaValida", lcSql)
		uf_perguntalt_chama("N�o foi poss�vel verificar a idade de receita. Contacte o suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF
	SELECT uCrsPrescDataAnulaValida
	
	IF uCrsPrescDataAnulaValida.ndias >= 30
		uf_perguntalt_chama("A receita j� foi prescrita � mais de 30 dias, n�o pode ser anulada.", "", "OK", 64)
		RETURN .f.
	ENDIF
	
	IF ucrsLinPresc.receitamcdt ==.t.
		uf_prescricao_anularMCDT()
	ELSE
		uf_prescricao_anularMed()
	ENDIF
ENDFUNC


**
FUNCTION uf_prescricao_anularMCDT
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_prescricao_motivosanulacaoMCDT
	ENDTEXT
	If !uf_gerais_actGrelha("", "ucrsMotivosAnulacaoMcdt", lcSql)
		uf_perguntalt_chama("N�o foi possivel encontrar regitos.", "", "OK", 16)
		RETURN .f.
	ENDIF
	uf_valorescombo_chama("ucrsLinPresc.anulamotivoCod", 2, "ucrsMotivosAnulacaoMcdt", 2, "codigo,descricao", "descricao, codigo")
	SELECT ucrsLinPresc
	
	** Preeenche descri��o na linha/Cabe�alho
	SELECT ucrsLinPresc
	lcPrescstamp = ucrsLinPresc.prescstamp
	
	SELECT ucrsMotivosAnulacaoMcdt
	LOCATE FOR ALLTRIM(ucrsLinPresc.prescstamp) == ALLTRIM(lcPrescstamp )
	IF FOUND()
		Replace ucrsLinPresc.anulamotivoDesc WITH ucrsMotivosAnulacaoMcdt.descricao
		Replace ucrsLinPresc.anuladata	WITH ucrsMotivosAnulacaoMcdt.dataanula
		Replace ucrsLinPresc.anulado WITH .t.
	ENDIF	
	
	** Faz update na BD
	lcSQL = ""		
	TEXT TO lcSql NOSHOW textmerge
		UPDATE b_cli_presc SET anulado = 1,anuladata = '<<uf_gerais_getdate(ucrsLinPresc.anuladata,"SQL")>>', anulamotivoDesc = '<<ALLTRIM(ucrsLinPresc.anulamotivoDesc)>>', anulamotivoCod = '<<ALLTRIM(ucrsLinPresc.anulamotivoCod)>>' WHERE prescstamp = '<<ALLTRIM(lcPrescstamp)>>'
		UPDATE b_cli_prescl SET anulado = 1,anuladata = '<<uf_gerais_getdate(ucrsLinPresc.anuladata,"SQL")>>', anulamotivoDesc = '<<ALLTRIM(ucrsLinPresc.anulamotivoDesc)>>', anulamotivoCod = '<<ALLTRIM(ucrsLinPresc.anulamotivoCod)>>' WHERE prescstamp = '<<ALLTRIM(lcPrescstamp)>>'
	ENDTEXT

	If !uf_gerais_actGrelha("", "uCrsPrescAnulaBD", lcSql)
		uf_perguntalt_chama("Ocorreu uma anomalia a anular a receita. Contacte o suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF
	
	** chama Webservice para anula��o de receita
	regua(0,10,"A enviar pedido de anula��o de receita...")
	regua(1,1,"A enviar pedido de anula��o de receita...")
	
	lcstampEnvioAnulacao = uf_gerais_stamp()

	uf_prescricao_enviaAnulacaoReceitaMCDT(lcPrescstamp, lcstampEnvioAnulacao)
	
	regua(1,2,"A enviar pedido de anula��o de receita...")
	
	**Activa Timer para obter respostas de todas as receitas enviadas
	prescricao.tmrAnulacao.enabled = .t.

ENDFUNC

**
*** Pressupostos 
** � possivel anular a receita nas seguintes condi��es:
* a) Se o prescritor que pede a anula��o for o mesmo que a prescreveu originalmente
* b) Ainda n�o tenha sido anulada
* c) Se passaram menos de 30 dias desde que foi prescrita
FUNCTION uf_prescricao_anularMed

	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_prescricao_motivosanulacaoMed
	ENDTEXT
	If !uf_gerais_actGrelha("", "ucrsMotivosAnulacaoMed", lcSql)
		uf_perguntalt_chama("N�o foi possivel encontrar regitos.", "", "OK", 16)
		RETURN .f.
	ENDIF
	uf_valorescombo_chama("ucrsLinPresc.anulamotivoCod", 2, "ucrsMotivosAnulacaoMed", 2, "codigo,descricao", "descricao, codigo")
	SELECT ucrsLinPresc
	
	** Preeenche descri��o na linha/Cabe�alho
	SELECT ucrsLinPresc
	lcPrescstamp = ucrsLinPresc.prescstamp
	Select ucrsLinPresc
	LOCATE FOR ALLTRIM(ucrsLinPresc.prescstamp) == ALLTRIM(lcPrescstamp)
	IF FOUND()
		SELECT ucrsMotivosAnulacaoMed
		Replace ucrsLinPresc.anulamotivoDesc WITH ucrsMotivosAnulacaoMed.descricao
		Replace ucrsLinPresc.anuladata	WITH ucrsMotivosAnulacaoMed.dataanula
		Replace ucrsLinPresc.anulado WITH .t.
	ENDIF 
		
	** chama Webservice para anula��o de receita
	regua(0,10,"A enviar pedido de anula��o de receita...")
	regua(1,1,"A enviar pedido de anula��o de receita...")
	
	lcstampEnvioAnulacao = uf_gerais_stamp()
	
	uf_prescricao_enviaAnulacaoReceitaMed(lcPrescstamp, lcstampEnvioAnulacao)
	
	regua(1,2,"A enviar pedido de anula��o de receita...")
	
	**Activa Timer para obter respostas de todas as receitas enviadas
	prescricao.tmrAnulacao.enabled = .t.
	
	** Faz update na BD
	lcSQL = ""		
	TEXT TO lcSql NOSHOW textmerge
		UPDATE 
			b_cli_presc 
		SET 
			anulado = 1
			,anuladata = '<<uf_gerais_getdate(ucrsLinPresc.anuladata,"SQL")>>'
			,anulamotivoDesc = '<<ALLTRIM(ucrsLinPresc.anulamotivoDesc)>>'
			,anulamotivoCod = '<<ALLTRIM(ucrsLinPresc.anulamotivoCod)>>' 
		WHERE 
			prescstamp = '<<ALLTRIM(lcPrescstamp)>>'
			
		UPDATE 
			b_cli_prescl 
		SET 
			anulado = 1
			,anuladata = '<<uf_gerais_getdate(ucrsLinPresc.anuladata,"SQL")>>'
			,anulamotivoDesc = '<<ALLTRIM(ucrsLinPresc.anulamotivoDesc)>>'
			,anulamotivoCod = '<<ALLTRIM(ucrsLinPresc.anulamotivoCod)>>' 
		WHERE 
			prescstamp = '<<ALLTRIM(lcPrescstamp)>>'
	ENDTEXT
	
	IF !uf_gerais_actGrelha("", "uCrsPrescAnulaBD", lcSql)
		uf_perguntalt_chama("Ocorreu uma anomalia a anular a receita. Contacte o suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF

ENDFUNC


**
FUNCTION uf_prescricao_enviaAnulacaoReceitaMed
	LPARAMETERS lcPrescStamp, lcstampEnvioAnulacao
	LOCAL lcTestes, lcNomeJar, lcOffline
	
	&& 
	prescricao.stampEnvioAnulacao = ALLTRIM(lcstampEnvioAnulacao)
	&&
	lcstampEnvioOnline = ALLTRIM(lcstampEnvioAnulacao) 
	
	
	
	&& verifica se utiliza webservice de testes
	IF uf_gerais_getParameter("ADM0000000227","BOOL") == .f.
		lcTest = 0
	ELSE
		lcTest = 1
	ENDIF 
	
	&& verifica o webservice a usar - RCP Prescri��es Materializadas || RSP Prescri��es Sem Papel
	IF myPrescMaterializada == .t.
		lcNomeJar = 'RPM_RCP\rpm_rcp.jar'
	ELSE
		&&usa sistema de prescri��o antiga
		IF uf_gerais_getParameter("ADM0000000295","BOOL") == .t.
			lcNomeJar = 'RPM_RSP\rpm_rsp.jar'
		ELSE
			lcNomeJar = 'LTS_RPM_RSP\rpm_rsp.jar'
		ENDIF 
	ENDIF
	

		
	&& 	
	IF DIRECTORY(ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\PE')
		lcWsPath = ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\PE\' + ALLTRIM(lcNomeJar);
				+ ' "--PrescStamp=' + ALLTRIM(lcPrescStamp) + ["];
				+ ' "--StampLigacao=' + ALLTRIM(lcstampEnvioOnline) + ["];
				+ ' "--Test=' + ALLTRIM(STR(lcTest)) + ["];
				+ ' "--clID=' + ALLTRIM(uCrsE1.id_lt) + ["];
				+ ' "--Offline= ' + IIF(lcOffline,'1','0') + ["];
				+ ' "--chavePedidoRelacionado= ' + '' + + ["];
				+ ' "--terminal_id=' + ALLTRIM(STR(mytermno)) + ["];
				
				
		
		&& Informa que est� a usar webservice de testes
		IF uf_gerais_getParameter("ADM0000000227","BOOL") == .t.		
			uf_perguntalt_chama("Webservice de teste..." ,"OK","",64)
		ENDIF 		
		
		&& 3� parametro a .t. aguardar reposta do Java
		lcWsPath = "javaw -jar " + lcWsPath 
		oWSShell = CREATEOBJECT("WScript.Shell")
		oWSShell.Run(lcWsPath,1,.f.)
	ENDIF
	
	

ENDFUNC


**
FUNCTION uf_prescricao_enviaAnulacaoReceitaMCDT
	LPARAMETERS lcPrescStamp, lcstampEnvioAnulacao
	&& valida��es
	
	prescricao.stampEnvioAnulacao = lcstampEnvioAnulacao
	
	IF DIRECTORY(ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\PE\MCDT')

		lcWsPath = ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\PE\MCDT\MCDT.jar';
					+ ' "--PrescStamp=' + ALLTRIM(lcPrescStamp) + ["];
					+ ' "--StampLigacao=' + ALLTRIM(lcstampEnvioAnulacao) + ["];
					+ ' "--clID=' + ALLTRIM(uCrsE1.id_lt) + ["];
					+ ' "--chavePedidoRelacionado= ' + '' + ["];
					+ ' "--computerName=' + ALLTRIM(myClientName) + ["];
					+ ' "--terminal_id=' + ALLTRIM(STR(mytermno)) + ["];
					+ ' "--req_type= ' + '2' + ["];
			
		lcWsPath = "javaw -jar " + lcWsPath 
		
		IF(emdesenvolvimento == .t.)
			_cliptext = lcWsPath 
		ENDIF
		
		oWSShell = CREATEOBJECT("WScript.Shell")
		oWSShell.Run(lcWsPath,1,.f.)
	ENDIF
ENDFUNC


**
FUNCTION uf_prescricao_tmrAnulacao
	LOCAL lcSql 
	STORE '' TO lcSql 
	Select ucrsCabPresc
	lcNrPrescricao = ucrsCabPresc.nrprescricao
	SELECT ucrsLinPresc
	lcPrescstamp = ucrsLinPresc.prescstamp
	
	regua(1,3,"A enviar receita em modo online...")
	
	prescricao.ntentativasAnulacao = prescricao.ntentativasAnulacao + 1
	
	IF prescricao.ntentativasAnulacao > 5 && 5  tentativas de 3 segundos
		
		uf_prescricao_CancelaAnulacao(lcPrescstamp)
		regua(2)
		uf_perguntalt_chama("N�o foi poss�vel obter resposta o pedido de anula��o de Receita.","OK","",16)
		prescricao.tmrAnulacao.enabled = .f.
		prescricao.ntentativasAnulacao = 0
		prescricao.stampEnvioAnulacao = ''
		RETURN .f.
	ENDIF
	
	regua(1,prescricao.ntentativasAnulacao + 2,"A enviar pedido de anula��o de receita ...")
	
	TEXT TO lcSql NOSHOW textmerge
		exec up_prescricao_RespostaErrosEnvioOnline <<IIF(EMPTY(ALLTRIM(prescricao.stampEnvioAnulacao)) or ISNULL(prescricao.stampEnvioAnulacao),'',"'" + ALLTRIM(prescricao.stampEnvioAnulacao) +"'")>>
	ENDTEXT

	IF !uf_gerais_actGrelha("","uCrsVerificaRespostaAnulacao",lcSql)
		
		uf_prescricao_CancelaAnulacao(lcPrescstamp)

		regua(2)
		uf_perguntalt_chama("Ocorreu um erro a verificar a resposta do pedido de pedido de anula��o de receita. A receita n�o foi anulada.","OK","",16)
		prescricao.tmrAnulacao.enabled = .f.
		prescricao.ntentativasAnulacao = 0
		prescricao.stampEnvioAnulacao = ''
	ELSE
		IF RECCOUNT("uCrsVerificaRespostaAnulacao") > 0
			SELECT uCrsVerificaRespostaAnulacao
			LOCATE FOR ALLTRIM(uCrsVerificaRespostaAnulacao.Codigo) == "100002010002" OR ALLTRIM(uCrsVerificaRespostaAnulacao.Codigo) == "201101001"
			IF FOUND() && Encontrou Mensagem de Receita registada com sucesso
				
				regua(2)
				uf_perguntalt_chama("Receita anulada com sucesso.","OK","",64)
				prescricao.tmrAnulacao.enabled = .f.
				prescricao.ntentativasAnulacao = 0
				prescricao.stampEnvioAnulacao = ''
			ELSE
				uf_prescricao_CancelaAnulacao(lcPrescstamp)
				regua(2)
				uf_perguntalt_chama("N�o foi possivel anular a receita. Verifique o registo de comunica��es.","OK","",16)
				prescricao.tmrAnulacao.enabled = .f.
				prescricao.ntentativasAnulacao = 0
				prescricao.stampEnvioAnulacao = ''
			ENDIF
		ENDIF 
	ENDIF
	
	IF !EMPTY(lcNrPrescricao)
		uf_prescricao_chama(ALLTRIM(lcNrPrescricao))
	ENDIF

ENDFUNC


**
FUNCTION uf_prescricao_CancelaAnulacao
	LPARAMETERS lcPrescstamp
	**
	** Faz update na BD, para marcar receita como n�o anulada
	lcSQL = ""		
	TEXT TO lcSql NOSHOW textmerge
		UPDATE b_cli_presc SET anulado = 0 WHERE prescstamp = '<<ALLTRIM(lcPrescstamp)>>'
		UPDATE b_cli_prescl SET anulado = 0 WHERE prescstamp = '<<ALLTRIM(lcPrescstamp)>>'
	ENDTEXT
	
	If !uf_gerais_actGrelha("", "uCrsPrescAnulaBD", lcSql)
		uf_perguntalt_chama("Ocorreu uma anomalia a anular a receita. Contacte o suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF
ENDFUNC


**
FUNCTION uf_prescricao_actualizar
	LOCAL lcnrPrescricao 

	IF Prescricao.detalhesVisiveis = .f.
		Prescricao.pageFrame1.activepage = 1
		uf_prescricao_expandeDetalhe()
	ENDIF
	
	SELECT uCrsCabPresc
	lcnrPrescricao = uCrsCabPresc.nrprescricao
	IF !EMPTY(uCrsCabPresc.nrprescricao)
		uf_prescricao_chama(lcnrPrescricao)
	ENDIF
ENDFUNC


**
FUNCTION uf_prescricao_limpaCursores
	IF USED("ucrsCabPresc")
		SELECT ucrsCabPresc
		GO Top
		Scan	
			DELETE
		ENDSCAN
		SELECT ucrsLinPresc
		GO Top
		Scan	
			DELETE
		ENDSCAN
	ENDIF 
ENDFUNC




**
FUNCTION uf_prescricao_insereMed
	LPARAMETERS tcBoolDCI
	
	SELECT uCrsPesqArtigoPresc
	IF uCrsPesqArtigoPresc.u_nomerc == .t.
		uf_prescricao_criaLinha(tcBoolDCI)
	ELSE
		uf_perguntalt_chama("N�o � possivel adicionar o produto selecionado." + CHR(13) + CHR(13) + "Estado comercial: " + ALLTRIM(uCrsPesqArtigoPresc.ESTADOCOMERCIAL), "", "OK", 32)
	ENDIF
ENDFUNC


**
FUNCTION uf_prescricao_actualizaNomeColunas
	SELECT ucrsLinPresc
	lcTipoREceita = ucrsLinPresc.tiporeceita
	WITH Prescricao.grdPresc
		IF ALLTRIM(lcTipoREceita)== "MCDT" 
			.Columns(4).Header1.Caption = "Tx. Moderadora"
		ELSE
			.Columns(4).Header1.Caption = "Utente"
		ENDIF
		.setfocus
	ENDWITH
ENDFUNC


** Cria Nova linha MCDT
FUNCTION uf_prescricao_criaLinhaMCDT
	
	LOCAL lcCabStamp 
	
	&& apaga ordena��o necess�rio para n�o dar erro
	Select uCrsLinPresc
	DELETE TAG ALL

	** EXAMES PERTENCENTES A �REA DE CONVEN��O DIFERENTES - Tipos de Receitas
	
	SELECT uCrsPesqArtigoMCDT
    Select ucrsLinPresc
    GO Top    
    LOCATE FOR ALLTRIM(UPPER(ucrsLinPresc.TipoReceita)) == ALLTRIM(UPPER(uCrsPesqArtigoMCDT.TipoReceita)) AND ALLTRIM(UPPER(ucrsLinPresc.mcdt_area)) == ALLTRIM(UPPER(uCrsPesqArtigoMCDT.area))
    IF !FOUND()
        lcCabstamp = uf_gerais_stamp()
        lcLinstamp = uf_gerais_stamp()
        lcstampEnvioOnline = uf_gerais_stamp()

        SELECT uCrsPesqArtigoMCDT
        Select ucrsLinPresc
        APPEND BLANK 
        Replace ucrsLinPresc.prescstamp WITH lcCabstamp 
        Replace ucrsLinPresc.presclstamp WITH lcLinstamp 
        Replace ucrsLinPresc.stampEnvioOnline WITH lcstampEnvioOnline 
        Replace ucrsLinPresc.design WITH uf_prescricao_textoTipoReceita(ALLTRIM(uCrsPesqArtigoMCDT.TipoReceita),uCrsPesqArtigoMCDT.Area)
        Replace ucrsLinPresc.mcdt_area    WITH uCrsPesqArtigoMCDT.Area
        Replace ucrsLinPresc.lordem WITH uf_prescricao_CalculaPrimeiraOrdemTipoReceita(ALLTRIM(uCrsPesqArtigoMCDT.TipoReceita),uCrsPesqArtigoMCDT.Area)
        Replace ucrsLinPresc.TipoReceita WITH uCrsPesqArtigoMCDT.TipoReceita
        Replace ucrsLinPresc.nrPrescricao WITH prescricao.nrPrescricao
        Replace ucrsLinPresc.terminal WITH Prescricao.terminal
        Replace ucrsLinPresc.vias WITH 1
        Replace ucrsLinPresc.estado WITH "0"
        Replace ucrsLinPresc.estadodescr WITH "Receita n�o submetida"
        replace ucrsLinPresc.receitamcdt WITH .t.
        
    ELSE
        lcCabStamp = ucrsLinPresc.prescstamp
    ENDIF

		
	SELECT TOP 1 prescstamp, prescNomeCod FROM ucrsLinPresc WHERE ALLTRIM(UPPER(ucrsLinPresc.TipoReceita)) = ALLTRIM(UPPER(uCrsPesqArtigoMCDT.TipoReceita)) AND ALLTRIM(UPPER(ucrsLinPresc.mcdt_area)) == ALLTRIM(UPPER(uCrsPesqArtigoMCDT.Area)) ORDER BY lordem DESC INTO CURSOR ucrsUltimoStamp READWRITE 
	SELECT ucrsUltimoStamp
		
	**  Cada Receita pode conter no m�ximo 6 exames
	SELECT sum(mcdt_qtt) as summcdt_qtt FROM ucrsLinPresc WHERE ucrsLinPresc.prescstamp = ucrsUltimoStamp.prescstamp AND LEFT(ALLTRIM(ucrsLinPresc.design),1) != "." GROUP BY ucrsLinPresc.prescstamp INTO CURSOR ucrsNumExames READWRITE 
	
	lcControlaNumMaximoMCDT = .f.
	
	SELECT ucrsNumExames 
	IF ucrsNumExames.summcdt_qtt >= 6
	
		lcCabstamp = uf_gerais_stamp()
		lcLinstamp = uf_gerais_stamp()
		lcstampEnvioOnline = uf_gerais_stamp()

		SELECT uCrsPesqArtigoMCDT
		Select ucrsLinPresc
		APPEND BLANK 
		Replace ucrsLinPresc.prescstamp WITH lcCabstamp 
		Replace ucrsLinPresc.presclstamp WITH lcLinstamp 
		Replace ucrsLinPresc.stampEnvioOnline WITH lcstampEnvioOnline 
		Replace ucrsLinPresc.design WITH uf_prescricao_textoTipoReceita(ALLTRIM(uCrsPesqArtigoMCDT.TipoReceita),uCrsPesqArtigoMCDT.Area)
		Replace ucrsLinPresc.mcdt_area	WITH uCrsPesqArtigoMCDT.Area
		Replace ucrsLinPresc.lordem WITH uf_prescricao_CalculaPrimeiraOrdemTipoReceita(ALLTRIM(uCrsPesqArtigoMCDT.TipoReceita),uCrsPesqArtigoMCDT.Area)
		Replace ucrsLinPresc.TipoReceita WITH uCrsPesqArtigoMCDT.TipoReceita
		Replace ucrsLinPresc.nrPrescricao WITH prescricao.nrPrescricao
		Replace ucrsLinPresc.terminal WITH Prescricao.terminal
		Replace ucrsLinPresc.vias WITH 1
		Replace ucrsLinPresc.estado WITH "0"
		Replace ucrsLinPresc.estadodescr WITH "Receita n�o submetida"
		replace ucrsLinPresc.receitamcdt WITH .t.
		
		lcControlaNumMaximoMCDT = .t.
	ENDIF

	
	** Se j� existir o MCDT incrementa a quantidade 
	SELECT ucrsLinPresc
	LOCATE FOR ALLTRIM(ucrsLinPresc.prescstamp) == ALLTRIM(ucrsUltimoStamp.prescstamp) AND ALLTRIM(ucrsLinPresc.mcdt_codsns) == ALLTRIM(uCrsPesqArtigoMCDT.codsns) AND ALLTRIM(ucrsLinPresc.mcdt_codconv) == ALLTRIM(uCrsPesqArtigoMCDT.codconv) AND lcControlaNumMaximoMCDT == .f.
	IF FOUND()
		lcLinstamp = ucrsLinPresc.presclstamp
		Replace ucrsLinPresc.mcdt_qtt WITH ucrsLinPresc.mcdt_qtt + 1
	ELSE
		lcLinstamp = uf_gerais_stamp()
		lcstampEnvioOnline = uf_gerais_stamp()

		SELECT uCrsPesqArtigoMCDT
		SELECT ucrsLinPresc
		APPEND BLANK 

		** LORDEM - Trata Ordem das Linhas
		Replace ucrsLinPresc.lordem WITH uf_prescricao_CalculaPrimeiraOrdemTipoReceita(uCrsPesqArtigoMCDT.TipoReceita,uCrsPesqArtigoMCDT.Area)
		
		**N�mero Atendimento
		Replace ucrsLinPresc.nrPrescricao WITH prescricao.nrPrescricao			
		Replace ucrsLinPresc.terminal WITH Prescricao.terminal
				
		** STAMPS
		Replace ucrsLinPresc.prescstamp 	WITH lcCabstamp 
		Replace ucrsLinPresc.presclstamp 	WITH lcLinstamp 
		Replace ucrsLinPresc.stampEnvioOnline WITH lcstampEnvioOnline 
					
		SELECT uCrsPesqArtigoMCDT
		SELECT ucrsLinPresc

		Replace ucrsLinPresc.mcdt_codsns	WITH uCrsPesqArtigoMCDT.codsns
		Replace ucrsLinPresc.mcdt_codconv	WITH uCrsPesqArtigoMCDT.codconv
		Replace ucrsLinPresc.mcdt_area		WITH uCrsPesqArtigoMCDT.Area
		Replace ucrsLinPresc.mcdt_areadescr	WITH uCrsPesqArtigoMCDT.areadescr
		Replace ucrsLinPresc.mcdt_grupo		WITH uCrsPesqArtigoMCDT.grupo
		Replace ucrsLinPresc.mcdt_subgrupo	WITH uCrsPesqArtigoMCDT.subgrupo
		Replace ucrsLinPresc.mcdt_nomedescr	WITH uCrsPesqArtigoMCDT.nomedescr
		Replace ucrsLinPresc.design			WITH uCrsPesqArtigoMCDT.nomedescr
		Replace ucrsLinPresc.mcdt_pvp		WITH uCrsPesqArtigoMCDT.pvp
		Replace ucrsLinPresc.mcdt_taxmod	WITH uCrsPesqArtigoMCDT.taxmod
		Replace ucrsLinPresc.mcdt_qtt		WITH 1
		Replace ucrsLinPresc.vias			WITH 1
		Replace ucrsLinPresc.TipoReceita 	WITH uCrsPesqArtigoMCDT.TipoReceita
		Replace ucrsLinPresc.etiliquido 	WITH (ucrsLinPresc.mcdt_pvp + ucrsLinPresc.mcdt_taxmod) * ucrsLinPresc.mcdt_qtt
		Replace ucrsLinPresc.estado 		WITH "0"
		Replace ucrsLinPresc.estadodescr 	WITH ""
		replace ucrsLinPresc.receitamcdt 	WITH .t.
		
	ENDIF
	&& recria ordena��o
	SELECT ucrsLinPresc
	index on lordem TAG lordem 
	**	

	**	
	uf_prescricao_aplicaStampsCabecalho()
	**
	uf_prescricao_actualizaQttTotalReceita()
	**
	uf_prescricao_colocaNaLinha(lcLinstamp)
	**
	uf_prescricao_CalculaPosReceita()
	**
	
	PRESCRICAO.grdPresc.refresh
ENDFUNC


**
FUNCTION uf_prescricao_colocaNaLinha
	LPARAMETERS lcLinstamp
	
	SELECT ucrsLinPresc
	LOCATE FOR ALLTRIM(ucrsLinPresc.presclstamp) == ALLTRIM(lcLinstamp) 
	
ENDFUNC


** Cria nova linha Medicamentos
FUNCTION uf_prescricao_criaLinha
	LPARAMETERS tcBoolDCI

	LOCAL lcTextoTipoReceita, lcControlaValidacaoPrescNomeComercial, lcControlaTipoReceita, lcCriouCabecalho, lcCabStamp, lcTipoUe ,lcQttPerm , lcDataValidade
	STORE '' TO lcTextoTipoReceita, lcCabStamp 
	STORE 0 TO lcOrdem
	STORE 0 TO lcQttPerm
	STORE .f. TO lcControlaValidacaoPrescNomeComercial, lcControlaTipoReceita,lcCriouCabecalho, lcTipoUe 
	
	


	regua(0, 5, "A adicionar....", .f.)
	regua(1, 1, "A adicionar....", .f.)
	
	&& Tipo de produto vem da SP up_prescricao_pesquisaMed
	** Tipos de Receita para receitas Materializadas LL 09-03-2016 - cada receita materializada apenas pode conter med. de um determinado tipo;
	** RN - Receita de Medicamentos;
	** RE - Prescri��o de Psico. e estupefacientes sujeitos a controlo;
	** MM - Receita de Medicamentos Manipulados;
	** MDT - Receita de produtos diet�ticos;
	** MDB - Receita de produtos para autocontrolo da diabetes mellitus;
	** OUT - Receita de outros produtos (ex. produtos cosm�ticos, fraldas. sacos de ostomia, etc.);
	** UE - Receita para aquisi��o noutro estado membro;
	** MA - Prescri��o de medicamentos alerg�nios destinados a um doente especifico;
	** CE - Prescri��o de c�mara expansoras e outros dispositivos M�dicos
	** BIO - Receita de medicamentos biol�gicos
	
	** Tipos de receita para receitas DesMaterializadas LL 09-03-2016 -
	** LN - Receita de Medicamentos;
	** LE - Prescri��o de Psico. e estupefacientes sujeitos a controlo;
	** LMM - Receita de Medicamentos Manipulados;
	** LMDT - Receita de produtos diet�ticos;
	** LMDB - Receita de produtos para autocontrolo da diabetes mellitus;
	** LOUT - Receita de outros produtos (ex. produtos cosm�ticos, fraldas. sacos de ostomia, etc.);
	** LUE - Receita para aquisi��o noutro estado membro;
	** LMA - Prescri��o de medicamentos alerg�nios destinados a um doente especifico;
	** LCE - Prescri��o de c�mara expansoras e outros dispositivos M�dicos

	&& apaga ordena��o necess�rio para n�o dar erro
	Select uCrsLinPresc
	DELETE TAG ALL
		
	&& Calula validade - por defeito coloca 60 dias na validade para receitas n�o renovaveis, 180 nas renovaveis
**	lcSQL = ""
**	TEXT TO lcSQL NOSHOW TEXTMERGE
**		SELECT DATEADD(dd,60,'<<uf_gerais_getdate(ucrscabpresc.data,"SQL")>>') as validade, DATEADD(mm,6,'<<uf_gerais_getdate(ucrscabpresc.data,"SQL")>>') as validadeRenovavel
**	ENDTEXT
**	IF !uf_gerais_actGrelha("", "uCrsValidadePresc", lcSql)
**		uf_perguntalt_chama("N�o foi possivel calcular a validade da receita. Contacte o suporte.", "", "OK", 16) 
**		RETURN .f.
**	ENDIF
	

	&& Informa��o do �ltimo produto adicionado	
	IF ALLTRIM(UPPER(ucrsLinPresc.TipoReceita)) != 'UE'
		IF !EMPTY(myPrescMaterializada) 
			SELECT TOP 1 prescstamp, tipoduracao as tipo_duracao, pordci, tipoReceita, lordem, prescNomeCod;
			FROM uCrsLinPresc;
			WHERE ALLTRIM(UPPER(ucrsLinPresc.TipoReceita)) = ALLTRIM(UPPER(uCrsPesqArtigoPresc.TipoReceita));
			ORDER BY lordem DESC INTO CURSOR ucrsUltimoStamp READWRITE 
		ELSE
			SELECT TOP 1 prescstamp, tipoduracao as tipo_duracao, pordci, tipoReceita, lordem, prescNomeCod;
			FROM uCrsLinPresc;
			ORDER BY lordem DESC INTO CURSOR ucrsUltimoStamp READWRITE 		
		
		ENDIF
	ELSE
		SELECT TOP 1 prescstamp, tipoduracao as tipo_duracao, pordci, tipoReceita, lordem, prescNomeCod;
		FROM uCrsLinPresc;
		WHERE ALLTRIM(UPPER(ucrsLinPresc.TipoReceita)) == 'UE';
		ORDER BY lordem DESC INTO CURSOR ucrsUltimoStamp READWRITE 	
		
		&& variavel para controlar tipo de receita UE
		lcTipoUe = .t.
	ENDIF
	
	regua(1, 2, "A adicionar....", .f.)	
	
	&& Adiciona produto mediante as especifica��es da prescri��o materializada (receita com papel)
	IF !EMPTY(myPrescMaterializada) 
		
		**  Verifica tipo de receita, se for diferente cria nova receita
		SELECT ucrsUltimoStamp
		SELECT ucrsLinPresc
		GO TOP 	 
		LOCATE FOR ALLTRIM(UPPER(uCrsLinPresc.TipoReceita)) == ALLTRIM(UPPER(ucrsUltimoStamp.TipoReceita))
		IF !FOUND()
			**
			IF EMPTY(lcCriouCabecalho)	
				lcCabstamp = uf_prescricao_criaCabecalhoNovaReceita()
				lcCriouCabecalho = .t.
			ENDIF
			lcControlaTipoReceita = .t.
		ELSE
			lcCabStamp = uCrsLinPresc.prescstamp
		ENDIF
		
		&& Em cada receita podem ser prescritos at� 4 Medicamentos distintos (O que corresponde a MED_ID diferentes) - at� 4 emb. por receita
		SELECT COUNT(distinct uCrsLinPresc.med_id) as count_medid FROM uCrsLinPresc WHERE uCrsLinPresc.prescstamp = ucrsUltimoStamp.prescstamp AND LEFT(ALLTRIM(uCrsLinPresc.design),1) != "." GROUP BY uCrsLinPresc.prescstamp INTO CURSOR ucrsControlaMediD READWRITE 
		lcControlaMediD = .f.

		
		&& Se for superior a 4 - Cria Nova Receita
		SELECT ucrsControlaMediD
		IF ucrsControlaMediD.count_medid >= 4 AND lcControlaTipoReceita == .f.
			**	
			IF EMPTY(lcCriouCabecalho)	
				lcCabstamp = uf_prescricao_criaCabecalhoNovaReceita()
				lcCriouCabecalho = .t.
			ENDIF
			lcControlaMediD = .t.
		ENDIF
		
		&& No maximo podem ser prescritas 2 embalagens por medicamento
		SELECT SUM(qtt) as count_cnpem FROM ucrsLinPresc WHERE ucrsLinPresc.prescstamp = ucrsUltimoStamp.prescstamp AND LEFT(ALLTRIM(ucrsLinPresc.design),1) != "." GROUP BY ucrsLinPresc.cnpem INTO CURSOR ucrsControlaCNPEM READWRITE 
		lcControlaCnPem = .f.
		
		&& Se o n� de embalagens for superior a 2 adiciona nova receita
		SELECT ucrsControlaCNPEM 
		IF ucrsControlaCNPEM.count_cnpem >= 2 AND lcControlaMediD == .f. AND lcControlaTipoReceita == .f.
			**
			IF EMPTY(lcCriouCabecalho)
				lcCabstamp = uf_prescricao_criaCabecalhoNovaReceita()
				lcCriouCabecalho = .t.
			ENDIF
		
			lcControlaCnPem = .t.	
		ENDIF
		
		regua(1, 3, "A adicionar....", .f.)

		** At� um total de 4 Embalagens por receita
		SELECT SUM(ucrsLinPresc.qtt) as NumEmb FROM ucrsLinPresc WHERE ucrsLinPresc.prescstamp = ucrsUltimoStamp.prescstamp AND LEFT(ALLTRIM(ucrsLinPresc.design),1) != "." GROUP BY ucrsLinPresc.prescstamp INTO CURSOR ucrsControlaEmbalagensPorReceita READWRITE 
		SELECT ucrsControlaEmbalagensPorReceita 

		lcControlaNumEmb = .f.
		IF ucrsControlaEmbalagensPorReceita.NumEmb >= 4 AND lcControlaMediD == .f. AND lcControlaCnPem == .f.  AND lcControlaTipoReceita == .f. && Para evitar 2 linhas consecutivas
			**
			IF EMPTY(lcCriouCabecalho)					
				lcCabstamp = uf_prescricao_criaCabecalhoNovaReceita()
				lcCriouCabecalho = .t.
			ENDIF
			
			lcControlaNumEmb = .t.
		ENDIF	 
			
		** Separa medicamentos de Longa Dura��o, para possibilitar receitas renov�veis
		** Verifica se o ultimo medicamento da receita � de longa dura��o, se n�o for propoe a cria��o de novo cabe�alho
		lcControlaDuracao = .f.
		SELECT ucrsUltimoStamp
		IF ALLTRIM(UPPER(uCrsPesqArtigoPresc.tipoduracao)) != ALLTRIM(UPPER(ucrsUltimoStamp.tipo_duracao)) 
			IF uf_perguntalt_chama("A dura��o do medicamento que selecionou (" + ALLTRIM(UPPER(uCrsPesqArtigoPresc.tipoduracao)) + ") � diferente do medicamento anterior, pretende criar uma nova receita?", "SIM", "N�O", 64)
				**
				IF EMPTY(lcCriouCabecalho)						
					lcCabstamp = uf_prescricao_criaCabecalhoNovaReceita()	
					lcCriouCabecalho = .t.					
				ENDIF 

				lcControlaDuracao = .t.			
			ENDIF
		ENDIF

		** Se j� existir o Medicamento incrementa a quantidade (CNPEM ou REF)
		SELECT uCrsLinPresc
		LOCATE FOR ALLTRIM(ucrsLinPresc.prescstamp) == ALLTRIM(ucrsUltimoStamp.prescstamp);
				AND (;
					(ALLTRIM(ucrsLinPresc.ref) == ALLTRIM(uCrsPesqArtigoPresc.ref) AND tcBoolDCI == .f. AND ucrsLinPresc.pordci == .f.);
					 OR (ALLTRIM(ucrsLinPresc.cnpem) == ALLTRIM(uCrsPesqArtigoPresc.cnpem) AND tcBoolDCI == .t. AND ucrsLinPresc.pordci == .t.);
				);
				AND lcControlaMediD == .f. AND lcControlaNumEmb == .f. AND lcControlaDuracao == .f. AND lcControlaValidacaoPrescNomeComercial = .f. AND lcControlaCnPem == .f. AND lcControlaTipoReceita == .f.
		IF FOUND()
		
			lcLinstamp = uf_gerais_stamp()
			SELECT ucrsLinPresc
			Replace ucrsLinPresc.qtt 		WITH ucrsLinPresc.qtt + 1
			Replace ucrsLinPresc.utente		WITH uCrsPesqArtigoPresc.utente 
			Replace ucrsLinPresc.etiliquido WITH uCrsPesqArtigoPresc.utente * ucrsLinPresc.qtt
			
			uf_prescricao_calculaEncargosUtenteLinha(ucrsLinPresc.porDci,ucrsLinPresc.codGrupoH,ucrsLinPresc.ref,ucrsLinPresc.cnpem,ucrsLinPresc.diplomacod, ucrsLinPresc.PrescNomeCod, ucrsLinPresc.qtt)
		ELSE
			** Na mesma receita N�o pode conter medicamentos com justifica��o e sem justifica��o ao mesmo tempo 
			IF EMPTY(uCrsPesqArtigoPresc.prescNomeCod) OR tcBoolDCI == .t. && No caso de prescrever por dci, ignora prescNomeCod
				SELECT ucrsLinPresc
				LOCATE FOR ALLTRIM(ucrsLinPresc.prescstamp) == ALLTRIM(ucrsUltimoStamp.prescstamp) ;
						AND !empty(ucrsLinPresc.prescNomeCod)
				IF FOUND()
					IF EMPTY(lcCriouCabecalho)	
						lcCabstamp = uf_prescricao_criaCabecalhoNovaReceita()
						lcCriouCabecalho = .t.
					ENDIF
				ENDIF
			ELSE
			 	SELECT ucrsLinPresc
				LOCATE FOR ALLTRIM(ucrsLinPresc.prescstamp) == ALLTRIM(ucrsUltimoStamp.prescstamp) ;
						AND empty(ucrsLinPresc.prescNomeCod)
				IF FOUND()
					IF EMPTY(lcCriouCabecalho)	
						lcCabstamp = uf_prescricao_criaCabecalhoNovaReceita()
						lcCriouCabecalho = .t.
					ENDIF
				ENDIF
			ENDIF
			
			** N�o pode existir na mesma receita o mesmo medicamento, prescrito por DCI e Nome comercial 
			SELECT ucrsLinPresc
			LOCATE FOR ALLTRIM(ucrsLinPresc.prescstamp) == ALLTRIM(ucrsUltimoStamp.prescstamp) ;
					AND (ALLTRIM(ucrsLinPresc.ref) == ALLTRIM(uCrsPesqArtigoPresc.ref));
					AND (tcBoolDCI != ucrsLinPresc.pordci)
			IF FOUND()

				IF EMPTY(lcCriouCabecalho)	
					lcCabstamp = uf_prescricao_criaCabecalhoNovaReceita()
					lcCriouCabecalho = .t.
				ENDIF
				
				lcControlaValidacaoPrescNomeComercial = .t.
			ENDIF
			
			** Adiciona Produto 
			uf_prescricao_acrescentaMaisBaratos(ALLTRIM(uCrsPesqArtigoPresc.ref),uCrsPesqArtigoPresc.pmu,uCrsPesqArtigoPresc.utente, uCrsPesqArtigoPresc.psns, uCrsPesqArtigoPresc.codGrupoH, tcBoolDCI)
			uf_prescricao_acrescentaGenAlt(ALLTRIM(uCrsPesqArtigoPresc.ref),uCrsPesqArtigoPresc.codGrupoH)
			uf_prescricao_calculaAlergias()
			**
			lcLinstamp = uf_gerais_stamp()
			lcstampEnvioOnline = uf_gerais_stamp()

			SELECT ucrsLinPresc
			APPEND BLANK 

			&& N�mero Atendimento
			Replace ucrsLinPresc.nrPrescricao WITH prescricao.nrPrescricao			
			Replace ucrsLinPresc.terminal 	  WITH Prescricao.terminal
					
			** STAMPS
			Replace ucrsLinPresc.prescstamp 		WITH lcCabstamp 
			Replace ucrsLinPresc.presclstamp 		WITH lcLinstamp 
			Replace ucrsLinPresc.stampEnvioOnline 	WITH lcstampEnvioOnline
			
			** LORDEM - Trata Ordem das Linhas
			&& se forem tipo UE o controle � manual por causa da forma foi implementado o "calculo" do tipo de receita
			IF lcTipoUe  == .t.
				replace ucrsLinPresc.lordem WITH uf_prescricao_CalculaPrimeiraOrdemTipoReceita('UE', .f.)
			ELSE
				replace ucrsLinPresc.lordem WITH uf_prescricao_CalculaPrimeiraOrdemTipoReceita(uCrsPesqArtigoPresc.TipoReceita, .f.)
			ENDIF		
			
			SELECT uCrsPesqArtigoPresc
			SELECT ucrsLinPresc 
			Replace ucrsLinPresc.TipoReceita 		WITH uCrsPesqArtigoPresc.TipoReceita
			Replace ucrsLinPresc.ref 				WITH uCrsPesqArtigoPresc.ref
			Replace ucrsLinPresc.vias 				WITH 1
		
			&& 
			IF tcBoolDCI == .t.
				SELECT uCrsPesqArtigoPresc
				SELECT ucrsLinPresc
				Replace ucrsLinPresc.pordci 		WITH .t. 
				Replace ucrsLinPresc.design 		WITH ALLTRIM(uCrsPesqArtigoPresc.ListagemDci) + ", " + ALLTRIM(uCrsPesqArtigoPresc.DOSAGEM) + ", " + ALLTRIM(uCrsPesqArtigoPresc.FORMA_FARMACEUTICA) + ", " + ALLTRIM(uCrsPesqArtigoPresc.DESCRICAO)
				Replace ucrsLinPresc.posologia 		WITH uCrsPesqArtigoPresc.posologia &&Preeenche posologia obrigatorio
			ELSE
				Replace ucrsLinPresc.pordci 		WITH .f. 
				Replace ucrsLinPresc.design 		WITH uCrsPesqArtigoPresc.descr
				Replace ucrsLinPresc.posologia 		WITH uCrsPesqArtigoPresc.posologia &&Preeenche posologia obrigatorio uso exececao C
			ENDIF
				
			Replace ucrsLinPresc.prescNomeDescr 	WITH uCrsPesqArtigoPresc.prescNomeDescr
			Replace ucrsLinPresc.prescNomeCod 		WITH uCrsPesqArtigoPresc.prescNomeCod 
			Replace ucrsLinPresc.med_id 			WITH uCrsPesqArtigoPresc.medid
			Replace ucrsLinPresc.qtt 				WITH 1
			Replace ucrsLinPresc.u_epvp				WITH uCrsPesqArtigoPresc.pvp
			Replace ucrsLinPresc.utente				WITH uCrsPesqArtigoPresc.utente 
			Replace ucrsLinPresc.etiliquido 		WITH uCrsPesqArtigoPresc.utente 
			Replace ucrsLinPresc.estadocomercial	WITH uCrsPesqArtigoPresc.estadocomercial
			Replace ucrsLinPresc.dci				WITH uCrsPesqArtigoPresc.dci
			Replace ucrsLinPresc.codGrupoH			WITH uCrsPesqArtigoPresc.codGrupoH
			Replace ucrsLinPresc.designGrupoH		WITH uCrsPesqArtigoPresc.designGrupoH
			Replace ucrsLinPresc.familia			WITH uCrsPesqArtigoPresc.familia
			Replace ucrsLinPresc.fabricanteno		WITH uCrsPesqArtigoPresc.fabricanteno
			Replace ucrsLinPresc.fabricantenome		WITH uCrsPesqArtigoPresc.fabricantenome
			Replace ucrsLinPresc.generico			WITH uCrsPesqArtigoPresc.generico
			Replace ucrsLinPresc.psico				WITH uCrsPesqArtigoPresc.psico
			Replace ucrsLinPresc.benzo				WITH uCrsPesqArtigoPresc.benzo
			Replace ucrsLinPresc.protocolo			WITH uCrsPesqArtigoPresc.protocolo
			Replace ucrsLinPresc.manipulado			WITH uCrsPesqArtigoPresc.manipulado
			Replace ucrsLinPresc.tipembdescr		WITH uCrsPesqArtigoPresc.tipembdescr
			Replace ucrsLinPresc.grupo				WITH uCrsPesqArtigoPresc.grupo
			Replace ucrsLinPresc.posologiasugerida	WITH uCrsPesqArtigoPresc.posologiasugerida
			Replace ucrsLinPresc.posologiaOriginal  WITH uCrsPesqArtigoPresc.posologiasugerida
			Replace ucrsLinPresc.pmu 				WITH uCrsPesqArtigoPresc.pmu
			Replace ucrsLinPresc.tipoduracao		WITH uCrsPesqArtigoPresc.tipoduracao
			Replace ucrsLinPresc.cnpem				WITH uCrsPesqArtigoPresc.cnpem
			Replace ucrsLinPresc.pgenerico			WITH 1
			Replace ucrsLinPresc.estado 			WITH "0"
			Replace ucrsLinPresc.estadodescr 		WITH ""
			Replace ucrsLinPresc.prescEmbCod		WITH uCrsPesqArtigoPresc.prescEmbCod
			Replace ucrsLinPresc.prescEmbDescr		WITH uCrsPesqArtigoPresc.prescEmbDescr
			Replace ucrsLinPresc.prescDateCod		WITH uCrsPesqArtigoPresc.prescDateCod
			Replace ucrsLinPresc.prescDateDescr		WITH uCrsPesqArtigoPresc.prescDateDescr
			Replace ucrsLinPresc.descJust 			WITH uCrsPesqArtigoPresc.descJust		
			replace ucrsLinPresc.duracaoValor   	WITH uCrsPesqArtigoPresc.duracaoValor   	
			replace ucrsLinPresc.duracaoUnidade 	WITH uCrsPesqArtigoPresc.duracaoUnidade 	
			replace ucrsLinPresc.frequenciaValor 	WITH uCrsPesqArtigoPresc.frequenciaValor 
			replace ucrsLinPresc.frequenciaUnidade 	WITH uCrsPesqArtigoPresc.frequenciaUnidade
			replace ucrsLinPresc.quantidadeValor  	WITH uCrsPesqArtigoPresc.quantidadeValor  
			replace ucrsLinPresc.quantidadeUnidade	WITH uCrsPesqArtigoPresc.quantidadeUnidade
			
			
			
			
			lcDataValidade = uf_prescricao_PreencheValidadePorLinha(myPrescMaterializada,ucrsLinPresc.tipoduracao,ucrsLinPresc.prescNomeCod,ucrsLinPresc.prescDateCod)
			Replace ucrsLinPresc.validade 			WITH lcDataValidade 

			&& Subtitui Diploma do Utente
			IF RECCOUNT("uCrsDiplomasPresc") > 0
				SELECT uCrsDiplomasPresc

					SELECT uCrsRelacaoCnpDiploma
					GO TOP 
					LOCATE FOR UPPER(ALLTRIM(uCrsRelacaoCnpDiploma.diploma)) == UPPER(ALLTRIM(ALLTRIM(uCrsDiplomasPresc.diploma)));
						AND UPPER(ALLTRIM(uCrsRelacaoCnpDiploma.cnp)) == UPPER(ALLTRIM(ucrsLinPresc.ref))
					IF FOUND()
						SELECT uCrsDiplomasPresc
						Replace ucrsLinPresc.diploma WITH ALLTRIM(uCrsDiplomasPresc.diploma)
					ENDIF		
			ENDIF 
			
	
			&& caso seja pregabalina para tratamento da dor neuropatica - altera campo diploma (mesmo comportamento que software da SPMS)
*!*				IF TYPE("myDorNp") = "U" 
*!*				    **
*!*				ELSE 
*!*					IF !EMPTY(myDorNp)
*!*						SELECT ucrsLinPresc
*!*						replace ucrsLinPresc.diploma 	WITH 'CFT 2.10'
*!*						replace ucrsLinPresc.diplomaCod WITH '63'				
*!*					ENDIF
*!*				ENDIF
			
			uf_prescricao_calculaEncargosUtenteLinha(ucrsLinPresc.porDci, ucrsLinPresc.codGrupoH, ucrsLinPresc.ref, ucrsLinPresc.cnpem, ucrsLinPresc.diplomacod, ucrsLinPresc.PrescNomeCod, ucrsLinPresc.qtt)
		ENDIF
		
	ELSE
		&& adiciona produto mediante as especifica��es da prescri��o DesMaterializada (receita sem papel)
		&& adiciona qtt caso o produto j� exista no painel de prescri��o
		&& M�ximo 2 embalagens por linha ou 6 caso seja produto tratamento prolongado

		lcQttPerm = uf_prescricao_numeroEmbalagemPorReceita(myPrescMaterializada,uCrsPesqArtigoPresc.tipoduracao,uCrsPesqArtigoPresc.embUnit)

		&& fui obrigado devido a limita��es de framework (impress�o de codigos QR) 24 linhas por receita no m�ximo - 
		SELECT COUNT(distinct uCrsLinPresc.med_id) as count_medid FROM uCrsLinPresc WHERE uCrsLinPresc.prescstamp = uCrsUltimoStamp.prescstamp AND LEFT(ALLTRIM(uCrsLinPresc.design),1) != "." GROUP BY uCrsLinPresc.prescstamp INTO CURSOR ucrsControlaMediD READWRITE 
		lcControlaMediD = .f.
		
		&& Se for superior a 24 - Cria Nova Receita
		SELECT ucrsControlaMediD
		IF ucrsControlaMediD.count_medid >= 24 AND lcControlaTipoReceita == .f.
			**	
			IF EMPTY(lcCriouCabecalho)	
				lcCabstamp = uf_prescricao_criaCabecalhoNovaReceita()
				lcCriouCabecalho = .t.
			ENDIF
			lcControlaMediD = .t.
		ENDIF
		**
		

		LOCAL lcValidaMesmoMed
		STORE .t. TO lcValidaMesmoMed
		
		SELECT uCrsLinPresc
		LOCATE FOR ALLTRIM(uCrsLinPresc.prescstamp) == ALLTRIM(uCrsUltimoStamp.prescstamp);
				AND (;
					(ALLTRIM(ucrsLinPresc.ref) == ALLTRIM(uCrsPesqArtigoPresc.ref) AND tcBoolDCI == .f. AND ucrsLinPresc.pordci == .f.);
					 OR (ALLTRIM(ucrsLinPresc.cnpem) == ALLTRIM(uCrsPesqArtigoPresc.cnpem) AND tcBoolDCI == .t. AND ucrsLinPresc.pordci == .t.))
				**);
				**AND lcControlaMediD == .f. AND lcControlaNumEmb == .f. AND lcControlaDuracao == .f. AND lcControlaValidacaoPrescNomeComercial = .f. AND lcControlaCnPem == .f. AND lcControlaTipoReceita == .f.
		IF FOUND()
	
			lcLinstamp = uf_gerais_stamp()

			SELECT ucrsLinPresc
			GO BOTTOM 

	
			IF ucrsLinPresc.qtt < lcQttPerm  				
				Replace ucrsLinPresc.qtt 				WITH ucrsLinPresc.qtt + 1
				Replace ucrsLinPresc.utente				WITH uCrsPesqArtigoPresc.utente 
				Replace ucrsLinPresc.etiliquido 		WITH uCrsPesqArtigoPresc.utente * ucrsLinPresc.qtt
				lcValidaMesmoMed = .f.
			ELSE
				lcValidaMesmoMed = .t.
			ENDIF
			
			uf_prescricao_aplicaDiplomaAuto(ALLTRIM(ucrsLinPresc.ref))
					
			uf_prescricao_calculaEncargosUtenteLinha(ucrsLinPresc.porDci, ucrsLinPresc.codGrupoH, ucrsLinPresc.ref, ucrsLinPresc.cnpem, ucrsLinPresc.diplomacod, ucrsLinPresc.PrescNomeCod, ucrsLinPresc.qtt)
		ENDIF	

		&& vai buscar cabstamp da receita
		IF lcValidaMesmoMed = .t.	
			
			&& Adiciona cabe�alho de receita caso n�o exista
			SELECT ucrsUltimoStamp
			SELECT ucrsLinPresc
			GO TOP 	 
			**LOCATE FOR ALLTRIM(UPPER(uCrsLinPresc.TipoReceita)) == ALLTRIM(UPPER(ucrsUltimoStamp.TipoReceita)) AND !EMPTY(ucrsUltimoStamp.TipoReceita)
			LOCATE FOR ucrsLinPresc.pos = 0
			IF !FOUND()
				**			
								
				IF EMPTY(lcCriouCabecalho)	
					lcCabstamp = uf_prescricao_criaCabecalhoNovaReceita()
					lcCriouCabecalho = .t.
				ENDIF

				lcControlaTipoReceita = .t.
				
			ELSE
				lcCabStamp = uCrsLinPresc.prescstamp
			ENDIF

				
			** Adiciona Produto caso ainda n�o esteja no painel de prescri��o
			uf_prescricao_acrescentaMaisBaratos(ALLTRIM(uCrsPesqArtigoPresc.ref),uCrsPesqArtigoPresc.pmu,uCrsPesqArtigoPresc.utente, uCrsPesqArtigoPresc.psns, uCrsPesqArtigoPresc.codGrupoH, tcBoolDCI)
			uf_prescricao_acrescentaGenAlt(ALLTRIM(uCrsPesqArtigoPresc.ref),uCrsPesqArtigoPresc.codGrupoH)
			uf_prescricao_calculaAlergias()
			**
			
			lcLinstamp = uf_gerais_stamp()
			lcstampEnvioOnline = uf_gerais_stamp()

			SELECT ucrsLinPresc
			APPEND BLANK 

			** N�mero Atendimento
			Replace ucrsLinPresc.nrPrescricao WITH prescricao.nrPrescricao			
			Replace ucrsLinPresc.terminal 	  WITH Prescricao.terminal

			** STAMPS
			Replace ucrsLinPresc.prescstamp 		WITH lcCabstamp 
			Replace ucrsLinPresc.presclstamp 		WITH lcLinstamp 
			Replace ucrsLinPresc.stampEnvioOnline 	WITH lcstampEnvioOnline
			
			** LORDEM - Trata Ordem das Linhas
			Replace ucrsLinPresc.lordem 	WITH uf_prescricao_CalculaPrimeiraOrdemTipoReceita(uCrsPesqArtigoPresc.TipoReceita,.f.)

			SELECT uCrsPesqArtigoPresc
			SELECT ucrsLinPresc 
			Replace ucrsLinPresc.TipoReceita 		WITH uCrsPesqArtigoPresc.TipoReceita
			Replace ucrsLinPresc.ref 				WITH uCrsPesqArtigoPresc.ref
			Replace ucrsLinPresc.vias 				WITH 1

			&& prescrito por DCI
			IF tcBoolDCI == .t.
				SELECT uCrsPesqArtigoPresc
				SELECT ucrsLinPresc
				Replace ucrsLinPresc.pordci 		WITH .t. 
				Replace ucrsLinPresc.design 		WITH ALLTRIM(uCrsPesqArtigoPresc.ListagemDci) + ", " + ALLTRIM(uCrsPesqArtigoPresc.DOSAGEM) + ", " + ALLTRIM(uCrsPesqArtigoPresc.FORMA_FARMACEUTICA) + ", " + ALLTRIM(uCrsPesqArtigoPresc.DESCRICAO)
				Replace ucrsLinPresc.posologia 		WITH uCrsPesqArtigoPresc.posologia &&Preeenche posologia obrigatorio
			ELSE
				Replace ucrsLinPresc.pordci 		WITH .f. 
				Replace ucrsLinPresc.design 		WITH uCrsPesqArtigoPresc.descr
				Replace ucrsLinPresc.posologia 		WITH uCrsPesqArtigoPresc.posologia && Preeenche posologia obrigatorio uso exececao C
			ENDIF
				
			Replace ucrsLinPresc.prescNomeDescr 	WITH uCrsPesqArtigoPresc.prescNomeDescr
			Replace ucrsLinPresc.prescNomeCod 		WITH uCrsPesqArtigoPresc.prescNomeCod
			Replace ucrsLinPresc.med_id 			WITH uCrsPesqArtigoPresc.medid
			Replace ucrsLinPresc.qtt 				WITH 1
			Replace ucrsLinPresc.u_epvp				WITH uCrsPesqArtigoPresc.pvp
			Replace ucrsLinPresc.utente				WITH uCrsPesqArtigoPresc.utente 
			Replace ucrsLinPresc.etiliquido 		WITH uCrsPesqArtigoPresc.utente 
			Replace ucrsLinPresc.estadocomercial	WITH uCrsPesqArtigoPresc.estadocomercial
			Replace ucrsLinPresc.dci				WITH uCrsPesqArtigoPresc.dci
			Replace ucrsLinPresc.codGrupoH			WITH uCrsPesqArtigoPresc.codGrupoH
			Replace ucrsLinPresc.designGrupoH		WITH uCrsPesqArtigoPresc.designGrupoH
			Replace ucrsLinPresc.familia			WITH uCrsPesqArtigoPresc.familia
			Replace ucrsLinPresc.fabricanteno		WITH uCrsPesqArtigoPresc.fabricanteno
			Replace ucrsLinPresc.fabricantenome		WITH uCrsPesqArtigoPresc.fabricantenome
			Replace ucrsLinPresc.generico			WITH uCrsPesqArtigoPresc.generico
			Replace ucrsLinPresc.psico				WITH uCrsPesqArtigoPresc.psico
			Replace ucrsLinPresc.benzo				WITH uCrsPesqArtigoPresc.benzo
			Replace ucrsLinPresc.protocolo			WITH uCrsPesqArtigoPresc.protocolo
			Replace ucrsLinPresc.manipulado			WITH uCrsPesqArtigoPresc.manipulado
			Replace ucrsLinPresc.tipembdescr		WITH uCrsPesqArtigoPresc.tipembdescr
			Replace ucrsLinPresc.grupo				WITH uCrsPesqArtigoPresc.grupo
			Replace ucrsLinPresc.posologiasugerida	WITH uCrsPesqArtigoPresc.posologiasugerida
			Replace ucrsLinPresc.posologiaOriginal  WITH uCrsPesqArtigoPresc.posologiasugerida
			Replace ucrsLinPresc.pmu 				WITH uCrsPesqArtigoPresc.pmu
			Replace ucrsLinPresc.tipoduracao		WITH uCrsPesqArtigoPresc.tipoduracao
			Replace ucrsLinPresc.cnpem				WITH uCrsPesqArtigoPresc.cnpem
			Replace ucrsLinPresc.pgenerico			WITH 1
			Replace ucrsLinPresc.estado 			WITH "0"
			Replace ucrsLinPresc.estadodescr 		WITH ""
			Replace ucrsLinPresc.prescEmbCod		WITH uCrsPesqArtigoPresc.prescEmbCod
			Replace ucrsLinPresc.prescEmbDescr		WITH uCrsPesqArtigoPresc.prescEmbDescr
			Replace ucrsLinPresc.prescDateCod		WITH uCrsPesqArtigoPresc.prescDateCod
			Replace ucrsLinPresc.prescDateDescr		WITH uCrsPesqArtigoPresc.prescDateDescr
			Replace ucrsLinPresc.descJust 			WITH uCrsPesqArtigoPresc.descJust 
			replace ucrsLinPresc.duracaoValor   	WITH uCrsPesqArtigoPresc.duracaoValor   	
			replace ucrsLinPresc.duracaoUnidade 	WITH uCrsPesqArtigoPresc.duracaoUnidade 	
			replace ucrsLinPresc.frequenciaValor 	WITH uCrsPesqArtigoPresc.frequenciaValor 
			replace ucrsLinPresc.frequenciaUnidade 	WITH uCrsPesqArtigoPresc.frequenciaUnidade
			replace ucrsLinPresc.quantidadeValor  	WITH uCrsPesqArtigoPresc.quantidadeValor  
			replace ucrsLinPresc.quantidadeUnidade	WITH uCrsPesqArtigoPresc.quantidadeUnidade
			
			lcDataValidade = uf_prescricao_PreencheValidadePorLinha(myPrescMaterializada,ucrsLinPresc.tipoduracao,ucrsLinPresc.prescNomeCod,ucrsLinPresc.prescDateCod)
			Replace ucrsLinPresc.validade 			WITH lcDataValidade
			Replace ucrsLinPresc.renovavel			WITH IIF(!EMPTY(uCrsPesqArtigoPresc.prescDateCod),.t.,.f.)
			
			&&	Replace ucrsLinPresc.validade 			WITH IIF(UPPER(ALLTRIM(uCrsPesqArtigoPresc.tipoduracao)) == 'CURTA OU M�DIA DURA��O' OR !EMPTY(uCrsPesqArtigoPresc.protocolo), uCrsValidadePresc.validade, uCrsValidadePresc.validadeRenovavel)
			&&	Replace ucrsLinPresc.renovavel			WITH IIF(UPPER(ALLTRIM(uCrsPesqArtigoPresc.tipoduracao)) == 'CURTA OU M�DIA DURA��O' OR ALLTRIM(ucrsLinPresc.grupo)=="SO", .f., IIF(!EMPTY(uCrsPesqArtigoPresc.protocolo),.f., .t.)) 

			&& Subtitui Diploma do Utente
			IF RECCOUNT("uCrsDiplomasPresc") > 0
				SELECT uCrsDiplomasPresc

				SELECT uCrsRelacaoCnpDiploma
				GO Top
				LOCATE FOR UPPER(ALLTRIM(uCrsRelacaoCnpDiploma.diploma)) == UPPER(ALLTRIM(ALLTRIM(uCrsDiplomasPresc.diploma)));
					AND UPPER(ALLTRIM(uCrsRelacaoCnpDiploma.cnp)) == UPPER(ALLTRIM(ucrsLinPresc.ref))
				IF FOUND()
					SELECT uCrsDiplomasPresc
					Replace ucrsLinPresc.diploma WITH ALLTRIM(uCrsDiplomasPresc.diploma)
				ENDIF		
			ENDIF
			
			&& caso seja pregabalina para tratamento da dor neuropatica - altera campo diploma (mesmo comportamento que software da SPMS)
*!*				IF TYPE("myDorNp") = "U" 
*!*				    **
*!*				ELSE 
*!*					IF !EMPTY(myDorNp)
*!*						replace ucrsLinPresc.diploma 	WITH 'CFT 2.10'
*!*						replace ucrsLinPresc.diplomaCod WITH '63'				
*!*					ENDIF
*!*				ENDIF			

			uf_prescricao_aplicaDiplomaAuto(ALLTRIM(ucrsLinPresc.ref))

			uf_prescricao_calculaEncargosUtenteLinha(ucrsLinPresc.porDci,ucrsLinPresc.codGrupoH,ucrsLinPresc.ref,ucrsLinPresc.cnpem, ucrsLinPresc.diplomacod, ucrsLinPresc.PrescNomeCod, ucrsLinPresc.qtt)	
		ENDIF
		**
	ENDIF
		
	regua(1, 4, "A adicionar....", .f.)
	**
	uf_prescricao_actualizaInteraccoes()
	**
	uf_prescricao_actualizaContraIndicacoes()
	**
	&& recria ordena��o
	SELECT ucrsLinPresc
	INDEX ON lordem TAG lordem 
	
	**	
	uf_prescricao_aplicaStampsCabecalho()
	**
	uf_prescricao_actualizaQttTotalReceita()
	**
	uf_prescricao_colocaNaLinha(lcLinstamp)
	**
	uf_prescricao_CalculaPosReceita()
	
	regua(1, 5, "A adicionar....", .f.)
	
	regua(2)

	PRESCRICAO.grdPresc.refresh	  

ENDFUNC

** para RSP existe varias dura��es consoante o tipo de dura��o ou tipo de embalagem 
FUNCTION uf_prescricao_numeroEmbalagemPorReceita
	LPARAMETERS lcTipoPrescricao,lcTipoduracao,lcEmbUn
	LOCAL lcQTT 
	STORE 0 TO lcQTT 
	IF EMPTY(lcTipoPrescricao) 		
		IF ALLTRIM(lcTipoduracao) != 'Longa Dura��o'
			IF ALLTRIM(lcEmbUn) == 'Embalagem Unit�ria'		
				lcQTT = 4			
			ELSE 
				lcQTT = 2			
			ENDIF 					
		ELSE		
			IF ALLTRIM(lcEmbUn) == 'Embalagem Unit�ria'
				lcQTT = 12				
			ELSE 
				lcQTT = 6		
			ENDIF 	
		ENDIF	
	ENDIF	

	
	RETURN lcQTT 
ENDFUNC


** Existe varias validades consoante o tipo de dura��o do medicamento e justifica��es 
FUNCTION uf_prescricao_PreencheValidadePorLinha
	LPARAMETERS lcTipoPrescricao,lcTipoduracao,lcPrescNomeCod, lcPrescDateCod
	LOCAL LcDate 

	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE
		SELECT DATEADD(dd,30,'<<uf_gerais_getdate(ucrscabpresc.data,"SQL")>>') as validade1Meses,
			   DATEADD(dd,60,'<<uf_gerais_getdate(ucrscabpresc.data,"SQL")>>') as validade2Meses, 
			   DATEADD(mm,6,'<<uf_gerais_getdate(ucrscabpresc.data,"SQL")>>') as validade6Meses,
			   DATEADD(year,1,'<<uf_gerais_getdate(ucrscabpresc.data,"SQL")>>') as validade1Ano
	ENDTEXT
	IF !uf_gerais_actGrelha("", "uCrsValidadePresc", lcSql)
		uf_perguntalt_chama("N�o foi possivel calcular a validade da receita. Contacte o suporte.", "", "OK", 16) 
		RETURN .f.
	ENDIF
	
	
	IF EMPTY(lcTipoPrescricao) 		
		IF ALLTRIM(lcTipoduracao) != 'Longa Dura��o'
			IF !EMPTY(lcPrescNomeCod) 
				IF !EMPTY(lcPrescDateCod)	&& existe justificacao e tambem o prescritor indicou aumento 	
					LcDate = uCrsValidadePresc.validade1Ano  
				ELSE && existe justificacao e n�o tem aumento pelo prescritor 
					LcDate = uCrsValidadePresc.validade2Meses
				ENDIF 	 		
			ELSE  && n�o existe justificacao e � diferente de longa dura��o logo � os 2 meses 
					LcDate = uCrsValidadePresc.validade2Meses
			ENDIF 					
		ELSE		
			IF !EMPTY(lcPrescNomeCod) 
				IF !EMPTY(lcPrescDateCod)	&& existe justificacao e tambem o prescritor indicou aumento 	
					LcDate = uCrsValidadePresc.validade1Ano  
				ELSE && existe justificacao e n�o tem aumento pelo prescritor 
					LcDate = uCrsValidadePresc.validade6Meses
				ENDIF 	 		
			ELSE  && n�o existe justificacao
				IF !EMPTY(lcPrescDateCod)	&& n�o existe justificacao mas o prescritor indicou aumento de validade	
					LcDate =  uCrsValidadePresc.validade6Meses 
				ELSE && existe justificacao e n�o tem aumento pelo prescritor 
					LcDate = uCrsValidadePresc.validade2Meses 
				ENDIF 
			ENDIF 
		ENDIF	
	ELSE 
		LcDate = uCrsValidadePresc.validade1Meses
	ENDIF	

	RETURN LcDate 

ENDFUNC


**
FUNCTION uf_prescricao_criaCabecalhoNovaReceita

	LOCAL lcDataValidade 

	lcCabstamp = uf_gerais_stamp()
	lcLinstamp = uf_gerais_stamp()
	lcstampEnvioOnline = uf_gerais_stamp()
	
	Select ucrsLinPresc
	APPEND BLANK 
	Replace ucrsLinPresc.prescstamp 		WITH lcCabstamp 
	Replace ucrsLinPresc.presclstamp 		WITH lcLinstamp 
	Replace ucrsLinPresc.stampEnvioOnline 	WITH lcstampEnvioOnline
	Replace ucrsLinPresc.design 			WITH uf_prescricao_textoTipoReceita(ALLTRIM(uCrsPesqArtigoPresc.TipoReceita))
	Replace ucrsLinPresc.lordem 			WITH uf_prescricao_CalculaPrimeiraOrdemTipoReceita(ALLTRIM(uCrsPesqArtigoPresc.TipoReceita),.f.)
	Replace ucrsLinPresc.TipoReceita 		WITH uCrsPesqArtigoPresc.TipoReceita
	Replace ucrsLinPresc.nrPrescricao 		WITH prescricao.nrPrescricao
	Replace ucrsLinPresc.terminal 			WITH Prescricao.terminal
	Replace ucrsLinPresc.vias 				WITH 1
	Replace ucrsLinPresc.estado 			WITH "0"
	Replace ucrsLinPresc.estadodescr 		WITH "Receita n�o submetida"	
	
	lcDataValidade = uf_prescricao_PreencheValidadePorLinha(myPrescMaterializada,uCrsPesqArtigoPresc.tipoduracao,uCrsPesqArtigoPresc.prescNomeCod,uCrsPesqArtigoPresc.prescDateCod)
	Replace ucrsLinPresc.validade 			WITH lcDataValidade
	Replace ucrsLinPresc.renovavel			WITH IIF(!EMPTY(uCrsPesqArtigoPresc.prescDateCod),.t.,.f.)
	
	RETURN lcCabstamp 
ENDFUNC


**
FUNCTION uf_prescricao_acrescentaMaisBaratos
	LPARAMETERS lcRef, lcPMU, lcUtente, lcPSNS, lcCodGrupoH, tcBoolDCI

	** Se a prescri��o dor efectuada por dci n�o adiciona ao cursor dos mais baratos
	IF tcBoolDCI == .t.
		RETURN .t.
	ENDIF 
	
	SELECT uCrsProdutosPrescMaisBaratosAux
	LOCATE FOR ALLTRIM(uCrsProdutosPrescMaisBaratosAux.ref) == ALLTRIM(lcRef)
	IF !FOUND()
		** Medicamentos Mais Baratos 
		lcSQL = ""
		&& Sem Grupo Homogeneo
		IF ALLTRIM(lcCodGrupoH) == "GH0000"
			TEXT TO lcSql noshow textmerge
				exec up_prescricao_MedEquivSemGH '<<ALLTRIM(lcRef)>>',<<lcPMU>>,'', <<lcUtente>>, <<lcPSNS>>
			ENDTEXT
		ELSE && Com Grupo Homogeneo
			TEXT to lcSql noshow textmerge
				exec up_prescricao_MedEquivComGH '<<ALLTRIM(lcRef)>>',<<lcPMU>>,'', <<lcUtente>>, <<lcPSNS>>
			ENDTEXT
		ENDIF
		If !uf_gerais_actGrelha("", "uCrsProdutosPrescMaisBaratosAuxTemp", lcSql)
			uf_perguntalt_chama("N�o foi poss�vel verificar informa��o de medicamentos mais baratos. Contacte o suporte.", "", "OK", 32)
			RETURN .f.
		ENDIF	
		SELECT uCrsProdutosPrescMaisBaratosAux
		APPEND FROM DBF("uCrsProdutosPrescMaisBaratosAuxTemp")
		
		SELECT uCrsProdutosPrescMaisBaratosAux
	ENDIF
ENDFUNC


**
FUNCTION uf_prescricao_acrescentaGenAlt
	LPARAMETERS lcRef, lcCodGrupoH

	lcSQL = ""

	SELECT uCrsProdutosGenAlt
	LOCATE FOR ALLTRIM(uCrsProdutosGenAlt.ref) == ALLTRIM(lcRef)
	IF !FOUND()
	
		* Genericos Alternativos
		lcSQL = ""
		SELECT ucrsLinPresc
		IF UPPER(ALLTRIM(lcCodGrupoH)) == "GH0000" OR  EMPTY(UPPER(ALLTRIM(lcCodGrupoH)))
			Text to lcSql noshow textmerge
				exec up_prescricao_pesquisaProdutosGenAltSGH '<<Alltrim(lcRef)>>', 300
			ENDTEXT
		ELSE
			Text to lcSql noshow textmerge
				exec up_prescricao_pesquisaProdutosGenAltCGH '<<Alltrim(lcRef)>>', 300
			ENDTEXT
		ENDIF

		If !uf_gerais_actGrelha("", "uCrsProdutosGenAltTemp", lcSql)
			uf_perguntalt_chama("N�o foi poss�vel verificar informa��o de Gen�rico Alternativos. Contacte o suporte.", "", "OK", 32)
			RETURN .f.
		ENDIF
		
		SELECT uCrsProdutosGenAlt
		APPEND FROM DBF("uCrsProdutosGenAltTemp")
	ENDIF
ENDFUNC


**
FUNCTION uf_prescricao_calculaAlergias
	LOCAL lcRefs 	
	lcRefs = ""
	
	&& Junta todas as referencias numa variavel	
	SELECT * FROM ucrsLinPresc INTO CURSOR ucrsTempAlergias READWRITE 

	SELECT ucrsTempAlergias 
	GO TOP
	SCAN FOR !empty(ucrsTempAlergias.ref)
		IF lcRefs == ''
			lcRefs = alltrim(ucrsTempAlergias.ref)
		ELSE 
			lcRefs = lcRefs + "," + alltrim(ucrsTempAlergias.ref)
		ENDIF 
	ENDSCAN
	
	IF USED("ucrsTempAlergias")
		fecha("ucrsTempAlergias")
	ENDIF 

	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_prescricao_VerAlergias '<<ALLTRIM(lcRefs)>>'
	ENDTEXT 
	If !uf_gerais_actGrelha("Prescricao.pageframe1.page1.pageframeUtentes.page5.grdPresc", "uCrsVerAlergias", lcSql)
		uf_perguntalt_chama("N�o foi poss�vel verificar informa��o de Alergias. Contacte o suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF
		
ENDFUNC 



**
FUNCTION uf_prescricao_actualizaQttTotalReceita
	
	** Coloca Totais a Zero para n�o influenciar calculo dos totais
	Select ucrsLinPresc
	GO Top	
	SCAN
		IF LEFT(ALLTRIM(ucrsLinPresc.design),1) == "." 
			Select ucrsLinPresc
			Replace ucrsLinPresc.qtt WITH 0
			Replace ucrsLinPresc.epv WITH 0
			Replace ucrsLinPresc.u_epvp WITH 0
			Replace ucrsLinPresc.etiliquido WITH 0
			Replace ucrsLinPresc.utente WITH 0
			Replace ucrslinPresc.mcdt_qtt WITH 0
			Replace ucrsLinPresc.MCDT_pvp WITH 0
			Replace uCrsLinPresc.mcdt_taxmod WITH 0
		ENDIF
	ENDSCAN
		
	SELECT prescstamp, SUM(qtt+mcdt_qtt) as totalQtt, SUM(utente) as TotalUtente, SUM(u_epvp+MCDT_pvp) as totalEpv, SUM(mcdt_taxmod) as totalTaxaMod, SUM(etiliquido) as Etotal FROM ucrsLinPresc GROUP BY prescstamp INTO CURSOR ucrsTotalQttPorReceita READWRITE 
	SELECT ucrsTotalQttPorReceita
	Select ucrsLinPresc
	GO Top	
	SCAN
		IF LEFT(ALLTRIM(ucrsLinPresc.design),1) == "." 
			SELECT ucrsTotalQttPorReceita
			GO TOP
			SCAN
				IF ALLTRIM(ucrsTotalQttPorReceita.prescstamp) == ALLTRIM(ucrsLinPresc.prescstamp)
					Select ucrsLinPresc
					Replace ucrsLinPresc.qtt WITH ucrsTotalQttPorReceita.totalQtt 
					Replace ucrsLinPresc.u_epvp WITH ucrsTotalQttPorReceita.totalEpv
					Replace ucrsLinPresc.utente WITH ucrsTotalQttPorReceita.TotalUtente
					Replace ucrsLinPresc.mcdt_qtt WITH ucrsTotalQttPorReceita.totalQtt 
					Replace ucrsLinPresc.MCDT_pvp WITH ucrsTotalQttPorReceita.totalEpv
					Replace ucrsLinPresc.mcdt_taxmod WITH ucrsTotalQttPorReceita.totalTaxaMod
					Replace ucrsLinPresc.etiliquido WITH ucrsTotalQttPorReceita.Etotal
				ENDIF		
			ENDSCAN
		ENDIF
		Select ucrsLinPresc
	ENDSCAN
	
	SELECT SUM(etiliquido) as Etotal FROM ucrsLinPresc WHERE LEFT(ALLTRIM(ucrsLinPresc.design),1) != "."  INTO CURSOR ucrsTotalValorReceita READWRITE 
	SELECT ucrsTotalValorReceita
	SELECT ucrsCabPresc
	Replace ucrsCabPresc.total WITH IIF(ISNULL(ucrsTotalValorReceita.Etotal),0,ucrsTotalValorReceita.Etotal)
	PRESCRICAO.containerTotal.txtTotalUtente.refresh
ENDFUNC

FUNCTION uf_prescricao_limitaNovaReceitaDesmaterializada
	LOCAL numeroReceitas
	STORE 0 TO numeroReceitas
	

	IF EMPTY(myPrescMaterializada)
			
		Select ucrsLinPresc
		GO Top	
		SCAN
			IF LEFT(ALLTRIM(ucrsLinPresc.design),1) == "." 
				numeroReceitas=numeroReceitas+1
			ENDIF
		ENDSCAN
		
		IF numeroReceitas>0 
			uf_perguntalt_chama("Apenas � poss�vel prescrever uma receita desmaterializada de cada vez", "", "OK", 16)
			RETURN .t.
		ELSE
			RETURN .f.
		ENDIF
	ENDIF
	
	RETURN .f.
	
ENDFUNC


** volta a arrancar o painel trocando o tipo de prescricao
FUNCTION uf_prescricao_novaReceitaTipo
	LPARAMETERS lcTipo
	
	** Verificar se existe nova vers�o para reiniciar o software
	uf_chk_nova_versao()
	
	uf_prescricao_clean()	
	uf_prescricao_chama('',.f.,'',.f.,lcTipo)
	
ENDFUNC	



**
FUNCTION uf_prescricao_novaReceita
	PUBLIC myTipoReceita	
	LOCAL lordem, lcLinstamp, lcSQL, lcNovaReceitaDesmaterializada
	
	** Verificar se existe nova vers�o para reiniciar o software
	uf_chk_nova_versao()
	
	IF initviz=0
		prescricao.show
		initviz=1
	endif
	
	STORE '' TO lcLinstamp, myTipoReceita
	STORE 1000 TO lcOrdem
	
	&&lcNovaReceitaDesmaterializada=uf_prescricao_limitaNovaReceitaDesmaterializada()
	IF(lcNovaReceitaDesmaterializada==.t.)
		RETURN
	ENDIF
	
	&& apaga ordena��o necess�rio para n�o dar erro
	Select ucrsLinPresc
	DELETE TAG ALL
	
	&& Prescricao Materializada
	IF !EMPTY(myPrescMaterializada) 
		
		lcSQL = ""
		TEXT TO lcSQL TEXTMERGE NOSHOW
			Select 'RN - receita de medicamentos' as design, 'RN' as tipoReceita
			union all   
			Select 'RE - receita especial (psicotr�picos e estupfacientes)', 'RE' as tipoReceita
			union all 
			Select 'MM - receita de medicamentos manipulados', 'MM' as tipoReceita
			union all 
			Select 'MDT - receita de produtos diet�ticos', 'MDT' as tipoReceita
			union all 
			Select 'MDB - receita de produtos para autocontrolo da diabetes mellitus', 'MDB' as tipoReceita
			union all 
			Select 'OUT - receita de outros produtos', 'OUT' as tipoReceita
			union all 
			Select 'UE - receita para aquisica��o noutro Estado-membro', 'UE' as tipoReceita
		ENDTEXT
		IF !uf_gerais_actGrelha("", "ucrsTiposDeReceita", lcSql)
			uf_perguntalt_chama("N�o foi possivel verificar os tipos de receita disponiveis. Contacte o suporte.", "", "OK", 16)
			RETURN .f.
		ENDIF
		
		uf_valorescombo_chama("myTipoReceita", 0, "ucrsTiposDeReceita", 2, "tipoReceita, design", "tipoReceita")
	ELSE
		uf_perguntalt_chama("Apenas � permitida uma receita desmaterializada.", "", "OK", 16)
		RETURN .f.
	
		myTipoReceita = "LN"
		
	ENDIF		

	lcTextoTipoReceita = ""
	
	IF !EMPTY(myTipoReceita)
		lcCabstamp = uf_gerais_stamp()
		lcLinstamp = uf_gerais_stamp()
		lcstampEnvioOnline = uf_gerais_stamp()

		Select ucrsLinPresc
		APPEND BLANK 
		Replace ucrsLinPresc.prescstamp 		WITH lcCabstamp 
		Replace ucrsLinPresc.presclstamp 		WITH lcLinstamp 
		Replace ucrsLinPresc.stampEnvioOnline 	WITH lcstampEnvioOnline
		Replace ucrsLinPresc.design 			WITH uf_prescricao_textoTipoReceita(myTipoReceita)
		Replace ucrsLinPresc.lordem 			WITH uf_prescricao_CalculaPrimeiraOrdemTipoReceita(myTipoReceita,.f.)
		Replace ucrsLinPresc.TipoReceita 		WITH myTipoReceita
		
		**N�mero Prescri��o
		Replace ucrsLinPresc.nrPrescricao 		WITH prescricao.nrPrescricao
		Replace ucrsLinPresc.terminal 			WITH Prescricao.terminal
		Replace ucrsLinPresc.estado 			WITH "0"
		Replace ucrsLinPresc.estadodescr 		WITH ""
		Replace ucrsLinPresc.vias 				WITH 1
	ENDIF 
		
	**	
	uf_prescricao_aplicaStampsCabecalho()
	**
	uf_prescricao_actualizaQttTotalReceita()

	**
	IF !EMPTY(lcLinstamp)
		uf_prescricao_colocaNaLinha(lcLinstamp)
	ENDIF
	**
	uf_prescricao_CalculaPosReceita()
	
	&& recria ordena��o
	SELECT ucrsLinPresc
	index on lordem TAG lordem 
				
	PRESCRICAO.grdPresc.refresh
ENDFUNC


** Calcula o Nr. de Ordem
FUNCTION uf_prescricao_CalculaOrdem
	LPARAMETERS lcOrdem
	
	lcOrdem = lcOrdem + 1
	DO WHILE .t.
	SELECT ucrsLinPresc
		LOCATE FOR ucrsLinPresc.lordem == lcOrdem
		IF FOUND()
			lcOrdem = lcOrdem + 1
		ELSE
			EXIT
		ENDIF 
	ENDDO 
	RETURN lcOrdem
ENDFUNC


**
FUNCTION uf_prescricao_CalculaPrimeiraOrdemTipoReceita
	LPARAMETERS lcTipoReceita, lcMcdtArea
	LOCAL lcOrdem, lcMcdtAreaaux 
	Store 0 TO lcOrdem
	STORE '' TO lcMcdtAreaaux 

	IF !EMPTY(lcMcdtArea)
		lcMcdtAreaaux = lcMcdtArea
	ENDIF

	** Valores por defeito
	IF !EMPTY(myPrescMaterializada)
		DO CASE 
			CASE ALLTRIM(lcTipoReceita) == "RN"
				lcOrdem = 100000
			CASE ALLTRIM(lcTipoReceita) == "RE"
				lcOrdem = 200000
			CASE ALLTRIM(lcTipoReceita) == "MM" 
				lcOrdem = 300000
			CASE ALLTRIM(lcTipoReceita) == "MDT" 
				lcOrdem = 400000
			CASE ALLTRIM(lcTipoReceita) == "MDB"
				lcOrdem = 500000
			CASE ALLTRIM(lcTipoReceita) == "OUT" 
				lcOrdem = 600000
			CASE ALLTRIM(lcTipoReceita) == "UE" 
				lcOrdem = 700000
			CASE ALLTRIM(lcTipoReceita) == "MCDT" AND ALLTRIM(UPPER(lcMcdtAreaaux)) == "A"
				lcOrdem = 800000	
			CASE ALLTRIM(lcTipoReceita) == "MCDT" AND ALLTRIM(UPPER(lcMcdtAreaaux)) == "B"
				lcOrdem = 801000	
			CASE ALLTRIM(lcTipoReceita) == "MCDT" AND ALLTRIM(UPPER(lcMcdtAreaaux)) == "C"
				lcOrdem = 802000	
			CASE ALLTRIM(lcTipoReceita) == "MCDT" AND ALLTRIM(UPPER(lcMcdtAreaaux)) == "D"
				lcOrdem = 803000	
			CASE ALLTRIM(lcTipoReceita) == "MCDT" AND ALLTRIM(UPPER(lcMcdtAreaaux)) == "E"
				lcOrdem = 804000	
			CASE ALLTRIM(lcTipoReceita) == "MCDT" AND ALLTRIM(UPPER(lcMcdtAreaaux)) == "F"
				lcOrdem = 805000	
			CASE ALLTRIM(lcTipoReceita) == "MCDT" AND ALLTRIM(UPPER(lcMcdtAreaaux)) == "G"
				lcOrdem = 806000	
			CASE ALLTRIM(lcTipoReceita) == "MCDT" AND ALLTRIM(UPPER(lcMcdtAreaaux)) == "H"
				lcOrdem = 807000	
			CASE ALLTRIM(lcTipoReceita) == "MCDT" AND ALLTRIM(UPPER(lcMcdtAreaaux)) == "I"
				lcOrdem = 808000	
			CASE ALLTRIM(lcTipoReceita) == "MCDT" AND ALLTRIM(UPPER(lcMcdtAreaaux)) == "J"
				lcOrdem = 809000	
			CASE ALLTRIM(lcTipoReceita) == "MCDT" AND ALLTRIM(UPPER(lcMcdtAreaaux)) == "L"
				lcOrdem = 810000	
			CASE ALLTRIM(lcTipoReceita) == "MCDT" AND ALLTRIM(UPPER(lcMcdtAreaaux)) == "M"
				lcOrdem = 811000	
			CASE ALLTRIM(lcTipoReceita) == "MCDT" AND ALLTRIM(UPPER(lcMcdtAreaaux)) == "N"
				lcOrdem = 812000	
			CASE ALLTRIM(lcTipoReceita) == "MCDT" AND ALLTRIM(UPPER(lcMcdtAreaaux)) == "O"
				lcOrdem = 813000	
			CASE ALLTRIM(lcTipoReceita) == "MCDT" AND ALLTRIM(UPPER(lcMcdtAreaaux)) == "Z"
				lcOrdem = 814000	
		ENDCASE 
	ELSE
		DO CASE 
			CASE ALLTRIM(lcTipoReceita) == "LN"
					lcOrdem = 100000
			CASE ALLTRIM(lcTipoReceita) == "LE"
				lcOrdem = 200000
			CASE ALLTRIM(lcTipoReceita) == "LMM" 
				lcOrdem = 300000
			CASE ALLTRIM(lcTipoReceita) == "LMDT" 
				lcOrdem = 400000
			CASE ALLTRIM(lcTipoReceita) == "LMDB"
				lcOrdem = 500000
			CASE ALLTRIM(lcTipoReceita) == "LOUT" 
				lcOrdem = 600000
			CASE ALLTRIM(lcTipoReceita) == "UE" 
				lcOrdem = 700000
			CASE ALLTRIM(lcTipoReceita) == "MCDT" AND ALLTRIM(UPPER(lcMcdtAreaaux)) == "A"
				lcOrdem = 800000	
			CASE ALLTRIM(lcTipoReceita) == "MCDT" AND ALLTRIM(UPPER(lcMcdtAreaaux)) == "B"
				lcOrdem = 801000	
			CASE ALLTRIM(lcTipoReceita) == "MCDT" AND ALLTRIM(UPPER(lcMcdtAreaaux)) == "C"
				lcOrdem = 802000	
			CASE ALLTRIM(lcTipoReceita) == "MCDT" AND ALLTRIM(UPPER(lcMcdtAreaaux)) == "D"
				lcOrdem = 803000	
			CASE ALLTRIM(lcTipoReceita) == "MCDT" AND ALLTRIM(UPPER(lcMcdtAreaaux)) == "E"
				lcOrdem = 804000	
			CASE ALLTRIM(lcTipoReceita) == "MCDT" AND ALLTRIM(UPPER(lcMcdtAreaaux)) == "F"
				lcOrdem = 805000	
			CASE ALLTRIM(lcTipoReceita) == "MCDT" AND ALLTRIM(UPPER(lcMcdtAreaaux)) == "G"
				lcOrdem = 806000	
			CASE ALLTRIM(lcTipoReceita) == "MCDT" AND ALLTRIM(UPPER(lcMcdtAreaaux)) == "H"
				lcOrdem = 807000	
			CASE ALLTRIM(lcTipoReceita) == "MCDT" AND ALLTRIM(UPPER(lcMcdtAreaaux)) == "I"
				lcOrdem = 808000	
			CASE ALLTRIM(lcTipoReceita) == "MCDT" AND ALLTRIM(UPPER(lcMcdtAreaaux)) == "J"
				lcOrdem = 809000	
			CASE ALLTRIM(lcTipoReceita) == "MCDT" AND ALLTRIM(UPPER(lcMcdtAreaaux)) == "L"
				lcOrdem = 810000	
			CASE ALLTRIM(lcTipoReceita) == "MCDT" AND ALLTRIM(UPPER(lcMcdtAreaaux)) == "M"
				lcOrdem = 811000	
			CASE ALLTRIM(lcTipoReceita) == "MCDT" AND ALLTRIM(UPPER(lcMcdtAreaaux)) == "N"
				lcOrdem = 812000	
			CASE ALLTRIM(lcTipoReceita) == "MCDT" AND ALLTRIM(UPPER(lcMcdtAreaaux)) == "O"
				lcOrdem = 813000	
			CASE ALLTRIM(lcTipoReceita) == "MCDT" AND ALLTRIM(UPPER(lcMcdtAreaaux)) == "Z"
				lcOrdem = 814000
		
		ENDCASE
	ENDIF

	SELECT IIF(IsNull(max(lordem)),lcOrdem,max(lordem)+1) as ordem from ucrsLinPresc WHERE ALLTRIM(UPPER(ucrsLinPresc.TipoReceita)) = ALLTRIM(UPPER(lcTipoReceita)) AND ALLTRIM(UPPER(ucrsLinPresc.mcdt_area)) = ALLTRIM(UPPER(lcMcdtAreaaux)) into cursor ucrsMaxLordem
	lcOrdem = ucrsMaxLordem.ordem

	RETURN lcOrdem

ENDFUNC


**
FUNCTION uf_prescricao_CalculaPosReceita
	lcPos = 0
	SELECT ucrsLinPresc
	GO Top
	SCAN
		IF LEFT(ALLTRIM(ucrsLinPresc.design),1) == "."
			lcPos = 0
		ELSE
			lcPos = lcPos + 1
		ENDIF
		
		Replace ucrsLinPresc.pos WITH lcPos
	ENDSCAN
	SELECT ucrsLinPresc
	GO Top
ENDFUNC


**
FUNCTION uf_prescricao_textoTipoReceita
	LPARAMETERS lcTipoReceita , lcMcdtArea
	
	LOCAL lcTextoTipoReceita
	lcTextoTipoReceita = ""
	
	DO CASE 
		CASE ALLTRIM(lcTipoReceita) == "RN"
			Store ". RN - Receita de medicamentos" TO lcTextoTipoReceita
		CASE ALLTRIM(lcTipoReceita) == "RE"
			Store ". RE - Receita especial (psicotr�picos e estupfacientes)" TO lcTextoTipoReceita
		CASE ALLTRIM(lcTipoReceita) == "MM" 
			Store ". MM - Receita de medicamentos manipulados" TO lcTextoTipoReceita
		CASE ALLTRIM(lcTipoReceita) == "MDT" 
			Store ". MDT - Receita de produtos Diet�ticos" TO lcTextoTipoReceita
		CASE ALLTRIM(lcTipoReceita) == "MDB"
			Store ". MDB - Receita de produtos para autocontrolo da diabetes mellitus"  TO lcTextoTipoReceita
		CASE ALLTRIM(lcTipoReceita) == "OUT" 
			Store ". OUT - Receita de outros produtos" TO lcTextoTipoReceita
		CASE ALLTRIM(lcTipoReceita) == "UE" 
			Store ". UE - Receita para aquisi��o noutro Estado-membro" TO lcTextoTipoReceita	
		CASE ALLTRIM(lcTipoReceita) == "MCDT" 
			IF EMPTY(lcMcdtArea)
				Store ". MCDT - Meios complementares de diagn�stico e terap�utica" TO lcTextoTipoReceita
			ELSE
				Store ". (" + ALLTRIM(lcMcdtArea) + ") MCDT - Meios complementares de diagn�stico e terap�utica" TO lcTextoTipoReceita
			ENDIF
		**CASE ALLTRIM(lcTipoReceita) == "LN" OR ALLTRIM(lcTipoReceita) == "LE"
		OTHERWISE 
			Store ". RSP - Receita de medicamentos Desmaterializada" TO lcTextoTipoReceita
	ENDCASE 
		
	RETURN lcTextoTipoReceita
ENDFUNC


**
FUNCTION uf_prescricao_textoTipoReceitaIDU
	LPARAMETERS lcTipoReceita
	
	LOCAL lcTextoTipoReceita
	lcTextoTipoReceita = ""
	
	DO CASE 
		CASE ALLTRIM(lcTipoReceita) == "RN"
			Store "Receita de Medicamentos" TO lcTextoTipoReceita
		CASE ALLTRIM(lcTipoReceita) == "RE"
			Store "Receita Especial (psicotr�picos e estupfacientes)" TO lcTextoTipoReceita
		CASE ALLTRIM(lcTipoReceita) == "MM" 
			Store "Receita de Medicamentos Manipulados" TO lcTextoTipoReceita
		CASE ALLTRIM(lcTipoReceita) == "MDT" 
			Store "Receita de Produtos Diet�ticos" TO lcTextoTipoReceita
		CASE ALLTRIM(lcTipoReceita) == "MDB"
			Store "Receita de Produtos para autocontrolo da diabetes mellitus"  TO lcTextoTipoReceita
		CASE ALLTRIM(lcTipoReceita) == "OUT" 
			Store "Receita de Outros produtos" TO lcTextoTipoReceita
		CASE ALLTRIM(lcTipoReceita) == "MCDT" 
			Store "Meios Complementares de Diagn�stico e Terap�utica" TO lcTextoTipoReceita
	ENDCASE 
		
	RETURN lcTextoTipoReceita
ENDFUNC


**
FUNCTION uf_prescricao_apagaLinha
	SELECT ucrsLinPresc
	LOCAL lcCabStamp, lcLinstamp 
	STORE '' TO lcCabStamp, lcLinstamp 
	
	IF myPrescricaoIntroducao == .f. AND myPrescricaoAlteracao == .f.
		RETURN .f.
	ENDIF
	 
	** Verifica Estado da Linha 
	IF (EMPTY(ucrsLinPresc.estado) OR ucrsLinPresc.estado == "0" OR ucrsLinPresc.estado == "2") AND ucrsLinPresc.anulado != .t.
		
		** Verifica se � cabe�alho
		IF LEFT(ALLTRIM(ucrsLinPresc.design),1) != "."
			SELECT ucrsLinPresc
			lcLinstamp = ucrsLinPresc.presclstamp
			SELECT ucrsLinPresc
			LOCATE FOR ALLTRIM(ucrsLinPresc.presclstamp) == ALLTRIM(lcLinstamp) 
			IF FOUND()
				DELETE 
			ENDIF 
		ELSE
			SELECT ucrsLinPresc
			IF uf_perguntalt_chama("Pretende eliminar as linhas associadas � receita selecionada?", "Sim", "N�o", 16)
				lcCabStamp = ALLTRIM(ucrsLinPresc.prescstamp)
				SELECT ucrsLinPresc
				GO Top
				SCAN FOR ALLTRIM(ucrsLinPresc.prescstamp) == ALLTRIM(lcCabStamp) 
					DELETE 
				ENDSCAN 
				SELECT ucrsLinPresc
				GO Top
			ENDIF
		ENDIF 
		
		SELECT ucrsLinPresc
		uf_prescricao_aplicaOrdem()
		uf_prescricao_actualizaQttTotalReceita()

		SELECT ucrsLinPresc
		GO BOTTOM 
		
		PRESCRICAO.grdPresc.refresh
	ELSE
		uf_perguntalt_chama("Esta receita j� foi processada ou anulada, n�o � possivel efectuar altera��es.","OK", "", 64)
		RETURN .f.
	ENDIF
ENDFUNC


**
FUNCTION uf_prescricao_aplicaStampsCabecalho
	LOCAL lcCabstamp
	lcCabstamp = ""

	SELECT ucrsLinPresc
	lcListamp = ucrsLinPresc.presclstamp

	SELECT ucrsLinPresc
	GO Top
	SCAN
		IF LEFT(ALLTRIM(ucrsLinPresc.design),1) == "."
			lcCabstamp = uf_gerais_stamp()
		ENDIF
		SELECT ucrsLinPresc
		Replace ucrsLinPresc.prescstamp WITH lcCabstamp 
	ENDSCAN
	
	SELECT ucrsLinPresc
	LOCATE FOR ALLTRIM(ucrsLinPresc.presclstamp) == ALLTRIM(lcListamp) 
	
ENDFUNC


**
FUNCTION uf_prescricao_aplicaOrdem
	STORE .f. TO LcTipoRN, LcTipoRE, LcTipoMM, LcTipoMDT, LcTipoMDB, LcTipoOUT, LcTipoMCDT, LcControlaPrimeiroCab
	lcOrdem = 0

	SELECT ucrsLinPresc
	GO Top
	SCAN
		lcControlaPrimeiroCab = .f.
		lcTipoReceita = ucrsLinPresc.tiporeceita
				
		IF ALLTRIM(ucrsLinPresc.TipoReceita) == "RN" AND LcTipoRN == .f. AND LEFT(ALLTRIM(ucrsLinPresc.design),1) == "."
			lcOrdem = 100000 
			LcTipoRN = .t.
			LcControlaPrimeiroCab = .t.
		ENDIF		
				
		IF ALLTRIM(ucrsLinPresc.TipoReceita) == "RE" AND LcTipoRE == .f. AND LEFT(ALLTRIM(ucrsLinPresc.design),1) == "."
			lcOrdem = 200000 
			LcTipoRE = .t.
			LcControlaPrimeiroCab = .t.
		ENDIF	
		
		IF ALLTRIM(ucrsLinPresc.TipoReceita) == "MM" AND LcTipoMM == .f. AND LEFT(ALLTRIM(ucrsLinPresc.design),1) == "."
			lcOrdem = 300000 
			LcTipoMM = .t.
			LcControlaPrimeiroCab = .t.
		ENDIF	
		
		IF ALLTRIM(ucrsLinPresc.TipoReceita) == "MDT" AND LcTipoMDT == .f. AND LEFT(ALLTRIM(ucrsLinPresc.design),1) == "."
			lcOrdem = 400000 
			LcTipoMDT = .t.
			LcControlaPrimeiroCab = .t.
		ENDIF	
		
		IF ALLTRIM(ucrsLinPresc.TipoReceita) == "MDB" AND LcTipoMDB == .f. AND LEFT(ALLTRIM(ucrsLinPresc.design),1) == "."
			lcOrdem = 500000 
			LcTipoMDB = .t.
			LcControlaPrimeiroCab = .t.
		ENDIF	
		
		IF ALLTRIM(ucrsLinPresc.TipoReceita) == "OUT" AND LcTipoOUT == .f. AND LEFT(ALLTRIM(ucrsLinPresc.design),1) == "."
			lcOrdem = 600000 
			LcTipoOUT = .t.
			LcControlaPrimeiroCab = .t.
		ENDIF
		
		IF ALLTRIM(ucrsLinPresc.TipoReceita) == "MCDT" AND LcTipoMCDT == .f. AND LEFT(ALLTRIM(ucrsLinPresc.design),1) == "."
			lcOrdem = 700000 
			LcTipoMCDT = .t.
			LcControlaPrimeiroCab = .t.
		ENDIF	

		IF LcControlaPrimeiroCab == .f.
			lcOrdem = lcOrdem + 1
		ENDIF
		
		SELECT ucrsLinPresc
		REplace ucrsLinPresc.lordem WITH lcOrdem
	ENDSCAN

	SELECT ucrsLinPresc
	GO Top
ENDFUNC


**
FUNCTION uf_prescricao_eliminaLinhasTipoReceita
	LPARAMETERS lcTipoReceita
	SELECT ucrsLinPresc
	SCAN
		IF ALLTRIM(ucrsLinPresc.TipoReceita) == ALLTRIM(lcTipoReceita)
			DELETE 
		ENDIF
	ENDSCAN
	SELECT ucrsLinPresc
	GO TOP 
ENDFUNC


**
FUNCTION uf_prescricao_moverLinhasCima

	IF myPrescricaoIntroducao == .f. AND myPrescricaoAlteracao == .f.
		RETURN .f.
	ENDIF 

*!*		** Se a prescri��o foi efectuada por nome comercial, avisa que n�o � possivel existir receitas com mais de um medicamento prescrito por nome, e deixa continuar
*!*		IF ucrsLinPresc.pordci == .f. 
*!*			uf_perguntalt_chama("Uma Receita apenas pode ter um medicamento por Nome Comercial. N�o � possivel alterar a posi��o das linhas.","", "OK", 64)
*!*		ENDIF 
	
	LOCAL lcNumLinhas,lcPresclstamp2 
	lcNumLinhas =0
	
	SELECT ucrsLinPresc
	lcPresclstamp = ucrsLinPresc.presclstamp
	lcOrdem = ucrsLinPresc.lordem
	lcTipoReceita = ucrsLinPresc.tiporeceita
	lcEstado = ucrsLinPresc.estado

	Select ucrsLinPresc
	COUNT TO lcNumLinhas
	IF lcNumLinhas == 0
		RETURN .f.
	ENDIF 
	
	Select Lordem, presclstamp,estado,design FROM ucrsLinPresc WHERE lordem = lcOrdem -1 INTO CURSOR ucrsLinPrescTempAux READWRITE
	
	Select ucrsLinPrescTempAux 
	lcEstado2 =ucrsLinPrescTempAux.estado
	lcPresclstamp2 = ucrsLinPrescTempAux.presclstamp
		
	*Impede Movimentos de Linhas em receitas processadas online e offline
	IF lcEstado == "1" OR lcEstado == "3" OR lcEstado2 == "1" OR lcEstado2 == "3"
		uf_perguntalt_chama("N�o � possivel realizar movimentos de linhas para receitas j� processadas.","", "OK", 64)
		return .f.
	ENDIF


	**
	Select MIN(lordem) as MinOrdem FROM ucrsLinPresc WHERE ALLTRIM(ucrsLinPresc.tiporeceita) = ALLTRIM(lcTipoReceita) INTO CURSOR ucrsLinPrescTempAuxTipoReceita READWRITE
	Select ucrsLinPrescTempAuxTipoReceita 
	*Impede Movimentos de Linhas entre Tipos de Receita diferentes
	IF lcOrdem -1 <= ucrsLinPrescTempAuxTipoReceita.MinOrdem 
		uf_perguntalt_chama("N�o � possivel realizar movimentos de linhas para receitas de tipos diferentes e para receitas j� processadas.","", "OK", 64)
		return .f.
	ENDIF
	
	
	UPDATE ucrsLinPresc SET Lordem = lcOrdem WHERE ALLTRIM(ucrsLinPresc.presclstamp)  = ALLTRIM(lcPresclstamp2)  
	UPDATE ucrsLinPresc SET Lordem = lcOrdem - 1 WHERE ALLTRIM(ucrsLinPresc.presclstamp)  = ALLTRIM(lcPresclstamp)
	
	uf_prescricao_aplicaStampsCabecalho()
	uf_prescricao_actualizaQttTotalReceita()

	**Necessario para refrescar o Ecra
	SELECT ucrsLinPresc
	LOCATE FOR ALLTRIM(ucrsLinPresc.presclstamp) == ALLTRIM(lcPresclstamp)
	IF FOUND()
	ENDIF

	PRESCRICAO.grdPresc.setFocus
	PRESCRICAO.grdPresc.click
	PRESCRICAO.grdPresc.refresh

ENDFUNC


**
FUNCTION uf_prescricao_moverLinhasBaixo
	
	IF myPrescricaoIntroducao == .f. AND myPrescricaoAlteracao == .f.
		RETURN .f.
	ENDIF 

*!*		** Se a prescri��o foi efectuada por nome comercial, avisa que n�o � possivel existir receitas com mais de um medicamento prescrito por nome, e deixa continuar
*!*		IF ucrsLinPresc.pordci == .f. 
*!*			uf_perguntalt_chama("Uma Receita apenas pode ter um medicamento por Nome Comercial. N�o � possivel alterar a posi��o das linhas.","", "OK", 64)
*!*		ENDIF 
	
	LOCAL lcNumLinhas,lcPresclstamp2 
	lcNumLinhas =0
	
	SELECT ucrsLinPresc
	lcPresclstamp = ucrsLinPresc.presclstamp
	lcOrdem = ucrsLinPresc.lordem
	lcTipoReceita = ucrsLinPresc.tiporeceita
	lcEstado = ucrsLinPresc.estado

	Select ucrsLinPresc
	COUNT TO lcNumLinhas
	IF lcNumLinhas == 0
		RETURN .f.
	ENDIF 
	
	Select Lordem, presclstamp,estado, design FROM ucrsLinPresc WHERE lordem = lcOrdem +1 INTO CURSOR ucrsLinPrescTempAux READWRITE
	
	Select ucrsLinPrescTempAux 
	lcEstado2 =ucrsLinPrescTempAux.estado
	lcPresclstamp2 = ucrsLinPrescTempAux.presclstamp
		
	Select MAX(lordem) as MaxOrdem FROM ucrsLinPresc WHERE ALLTRIM(ucrsLinPresc.tiporeceita) = ALLTRIM(lcTipoReceita) INTO CURSOR ucrsLinPrescTempAuxTipoReceita READWRITE
	Select ucrsLinPrescTempAuxTipoReceita 
	*Impede Movimentos de Linhas entre Tipos de Receita diferentes
	IF lcOrdem = ucrsLinPrescTempAuxTipoReceita.MaxOrdem 
		return .f.
	ENDIF
	
	
	UPDATE ucrsLinPresc SET Lordem = lcOrdem WHERE ALLTRIM(ucrsLinPresc.presclstamp)  = ALLTRIM(lcPresclstamp2 )  
	UPDATE ucrsLinPresc SET Lordem = lcOrdem + 1 WHERE ALLTRIM(ucrsLinPresc.presclstamp)  = ALLTRIM(lcPresclstamp)
	
	uf_prescricao_aplicaStampsCabecalho()
	uf_prescricao_actualizaQttTotalReceita()

	**Necessario para refrescar o Ecra
	SELECT ucrsLinPresc
	LOCATE FOR ALLTRIM(ucrsLinPresc.presclstamp) == ALLTRIM(lcPresclstamp)
	IF FOUND()
	ENDIF

	PRESCRICAO.grdPresc.setFocus
	PRESCRICAO.grdPresc.click
	PRESCRICAO.grdPresc.refresh

ENDFUNC


**
FUNCTION uf_prescricao_validaUtilizador
	
	&& VERIFICAR SE O C�DIGO DE ESPECIALISTA � VALIDO
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		 exec up_prescricao_ValidaUtilizador <<ch_userno>>
	ENDTEXT
	If !uf_gerais_actGrelha("", "uCrsValidaFormatoEsp", lcSql)
		uf_perguntalt_chama("N�o foi poss�vel verificar os dados do utilizador. Contacte o suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF
	
	SELECT uCrsValidaFormatoEsp && N�o tem definido a Ordem a que pretence
	IF uCrsValidaFormatoEsp.drinscri == 0
		uf_perguntalt_chama("O utilizador actual n�o tem definido a Ordem a que pertence."+  chr(13) + chr(13) + "Verifique os dados de utilizador.", "", "OK", 32)
		Return .f.
	ENDIF

	** Campos de Ordem Vazios
	IF Empty(uCrsValidaFormatoEsp.drcedula) OR Empty(uCrsValidaFormatoEsp.drordem)
		uf_perguntalt_chama("Informa��o de C�dula Profissonal e/ou N� de Ordem Profissional inv�lida."+  chr(13) + chr(13) + "Verifique os dados de utilizador.", "", "OK", 32)
		Return .f.
	ENDIF
	**

	DO CASE
		CASE  uCrsValidaFormatoEsp.DRINSCRI == 1
			IF LEFT(ALLTRIM(uCrsValidaFormatoEsp.drcedula),1) != "M" OR LEN(ALLTRIM(uCrsValidaFormatoEsp.drcedula))!=6
				uf_perguntalt_chama("O utilizador actual n�o tem a C�dula Profissional v�lida."+  chr(13) + chr(13) + "Formato: Mnnnnn. "+  chr(13) + chr(13) + "Verifique os dados de utilizador.", "", "OK", 32)
				Return .f.
			ENDIF
			
		CASE  uCrsValidaFormatoEsp.DRINSCRI == 2
			IF LEFT(ALLTRIM(uCrsValidaFormatoEsp.drcedula),1) != "D" OR LEN(ALLTRIM(uCrsValidaFormatoEsp.drcedula))!=6
				uf_perguntalt_chama("O utilizador actual n�o tem a C�dula Profissional v�lida."+  chr(13) + chr(13) + "Formato: Dnnnnn. "+  chr(13) + chr(13) + "Verifique os dados de utilizador.", "", "OK", 32)
				Return .f.
			ENDIF
		
		CASE  uCrsValidaFormatoEsp.DRINSCRI == 3
			IF LEFT(ALLTRIM(uCrsValidaFormatoEsp.drcedula),1) != "O" OR LEN(ALLTRIM(uCrsValidaFormatoEsp.drcedula))!=6
				uf_perguntalt_chama("O utilizador actual n�o tem a C�dula Profissional v�lida."+  chr(13) + chr(13) + "Formato: Onnnnn. "+  chr(13) + chr(13) + "Verifique os dados de utilizador.", "", "OK", 32)
				Return .f.
			ENDIF
							
		OTHERWISE
			**
	ENDCASE
ENDFUNC 


**
FUNCTION uf_prescricao_dynamicBackColor
	DO CASE
		CASE Left(ucrsLinPresc.design,1)=='.' AND ucrsLinPresc.anulado == .t.
			RETURN Rgb(127,127,127)
		CASE Left(ucrsLinPresc.design,1)=='.' AND ALLTRIM(ucrsLinPresc.estado) != "2"
			RETURN Rgb(42,143,154)
		CASE Left(ucrsLinPresc.design,1)=='.' AND ALLTRIM(ucrsLinPresc.estado) == "2"
			RETURN Rgb(255,0,0)
		OTHERWISE
			RETURN Rgb(255,255,255)
	ENDCASE
ENDFUNC


**
FUNCTION uf_prescricao_dynamicForeColor
	DO CASE
		CASE Left(ucrsLinPresc.design,1)!='.'
			RETURN Rgb(0,0,0)
		OTHERWISE
			RETURN Rgb(255,255,255)
	ENDCASE
	
ENDFUNC


**
FUNCTION uf_prescricao_gravar
	LOCAL lcValidaInsertCab, lcValidaInsertLin
	STORE .f. TO lcValidaInsertCab, lcValidaInsertLin
	
	** Verificar se existe nova vers�o para reiniciar o software
	uf_chk_nova_versao()
	
	&& Valida Obrigatorios
	IF uf_prescricao_CamposObrigatorios() == .f.
		RETURN .f.
	ENDIF

	Prescricao.txtno.setfocus
	Prescricao.txtno.click
	
	uf_prescricao_selecionaPg("Utente")
	
	&& For�a calculo da posi��o 
	uf_prescricao_CalculaPosReceita()	
	
	IF myPrescricaoIntroducao == .t.

		SELECT uCrsLinPresc
		GO TOP 
		
		Select ucrsCabPresc
		lcNrPrescricao = ucrsCabPresc.nrprescricao
		
		SELECT * FROM ucrsLinPresc WHERE (estado == '0' OR estado == '2') INTO CURSOR ucrsLinPrescInsert READWRITE 

		SELECT ucrsLinPrescInsert 
		GO TOP 
		SCAN FOR LEFT(ucrsLinPrescInsert.design,1) == "."
			IF uf_prescricao_validaDadosAntesGravacao(ucrsLinPrescInsert.prescstamp) == .f.
				RETURN .f.
			ENDIF
		ENDSCAN	


		SELECT ucrsLinPrescInsert
		GO TOP 
		SCAN
			IF LEFT(ucrsLinPrescInsert.design,1) == "."
								
				IF uf_gerais_actGrelha("", "", [BEGIN TRANSACTION])
						
					** A data da receita e o numero de prescri��o ser� adicionado posteriormente no modo online e offline
					SELECT ucrsLinPresc
					GO TOP 
					LOCATE FOR LEFT(ucrsLinPresc.design,1)=='.' AND ALLTRIM(ucrsLinPresc.prescstamp) == ALLTRIM(ucrsLinPrescInsert.prescstamp)
					IF FOUND()
						**Insere CAB
						
						SELECT ucrsLinPresc
						
						lcValidaInsertCab = uf_prescricao_insertCabPresc()
					ENDIF
										
					SELECT ucrsLinPresc
					GO TOP 
					SCAN FOR ALLTRIM(ucrsLinPresc.prescstamp) == ALLTRIM(ucrsLinPrescInsert.prescstamp)
						SELECT ucrsLinPresc
						lcValidaInsertLin = uf_prescricao_insertLinPresc()
					ENDSCAN
			
				
					IF lcValidaInsertCab == .f. OR lcValidaInsertLin == .f.
						uf_gerais_actGrelha("", "", [ROLLBACK])
					ELSE
						uf_gerais_actGrelha("", "", [COMMIT TRANSACTION])
					ENDIF
					
				ENDIF
			ENDIF
			SELECT ucrsLinPrescInsert
		ENDSCAN

		uf_prescricao_chama(ALLTRIM(lcNrPrescricao))
		
		STORE .f. TO myPrescricaoIntroducao , myPrescricaoAlteracao
		
		uf_prescricao_alternaMenu()
	ELSE && Altera��o
		
		SELECT ucrsLinPresc
		GO TOP 
		
		Select ucrsCabPresc
		lcNrPrescricao = ucrsCabPresc.nrprescricao
		
		** Apaga as linhas e cabe�alhos existentes e volta a inserir			
		uf_prescricao_apagaLinPresc(lcNrPrescricao)
		
		SELECT ucrsLinPresc 
		GO Top		
		&& Linhas ainda n�o sumetidas no processo online ou com erro no processo online, mas que ainda n�o foram gravadas no processo offline (0 e 2)
		SELECT * FROM ucrsLinPresc WHERE ucrsLinPresc.estado != '1' AND ucrsLinPresc.estado != '3' INTO CURSOR ucrsLinPrescUpdate READWRITE 

		SELECT ucrsLinPrescUpdate 
		GO Top
		SCAN FOR LEFT(ucrsLinPrescUpdate.design,1) == "." 
			IF uf_prescricao_validaDadosAntesGravacao(ucrsLinPrescUpdate.prescstamp) == .f.
				RETURN .f.
			ENDIF
		ENDSCAN	
									
		SELECT ucrsLinPrescUpdate 
		GO TOP 
		SCAN
			IF LEFT(ucrsLinPrescUpdate.design,1) == "."
				IF uf_gerais_actGrelha("", "", [BEGIN TRANSACTION])			
					** A data da receita e o numero de prescri��o ser� adicionado posteriormente no modo online e offline
					SELECT ucrsCabPresc
					SELECT ucrsLinPresc
					GO TOP 
					LOCATE FOR Left(ucrsLinPresc.design,1)=='.' AND ALLTRIM(ucrsLinPresc.prescstamp) == ALLTRIM(ucrsLinPrescUpdate.prescstamp)
					IF FOUND()
						**Update CAB
						SELECT ucrsLinPresc
						lcValidaInsertCab = uf_prescricao_insertCabPresc()
					ENDIF
					
					SELECT ucrsLinPresc
					GO Top
					SCAN FOR ALLTRIM(ucrsLinPresc.prescstamp) == ALLTRIM(ucrsLinPrescUpdate.prescstamp) AND (EMPTY(estado) OR ucrsLinPresc.estado == '0' OR ucrsLinPresc.estado == '2')
						**Insere Linhas 
						SELECT ucrsLinPresc
						lcValidaInsertLin = uf_prescricao_insertLinPresc()
					ENDSCAN
				ENDIF
				
				IF lcValidaInsertCab == .f. OR lcValidaInsertLin == .f.
					uf_gerais_actGrelha("", "", [ROLLBACK])
				ELSE
					uf_gerais_actGrelha("", "", [COMMIT TRANSACTION])
				ENDIF
			ENDIF
		ENDSCAN

		uf_prescricao_chama(ALLTRIM(lcNrPrescricao))
		STORE .f. TO myPrescricaoIntroducao , myPrescricaoAlteracao
		uf_prescricao_alternaMenu()
	ENDIF
		
	uf_prescricao_actualizaLocalizacao()	

	&& naoenviareceita
	&& return .f.

	&& Efetua envio das receitas
	uf_prescricao_EnviaReceitas()
	
ENDFUNC


**
FUNCTION uf_prescricao_validaDadosAntesGravacao
	LPARAMETERS lcPrescstamp
	LOCAL lcRetorno
	
	SELECT receitamcdt FROM ucrsLinPresc WHERE ALLTRIM(prescstamp) == ALLTRIM(lcPrescstamp) AND LEFT(ALLTRIM(design),1) == "." INTO CURSOR ucrsValidaTipoReceita READWRITE 
	SELECT ucrsValidaTipoReceita 
	
	lcRetorno = .f.
	IF ucrsValidaTipoReceita.receitamcdt == .t.
		lcRetorno = uf_prescricao_validaDadosMcdt(lcPrescstamp)
	ELSE
		lcRetorno = uf_prescricao_validaDadosMed(lcPrescstamp)
	ENDIF
	
	IF USED("ucrsValidaTipoReceita")
		fecha("ucrsValidaTipoReceita")
	ENDIF 	
	
	RETURN lcRetorno
	
ENDFUNC


**
FUNCTION uf_prescricao_validaDadosMcdt
	LPARAMETERS lcPrescstamp
	
	IF USED("ucrsValidaDadosCabecalho")
		fecha("ucrsValidaDadosCabecalho")
	ENDIF
	
	IF USED("ucrsValidaDadosLinhas")
		fecha("ucrsValidaDadosLinhas")
	ENDIF
	
	SELECT * FROM ucrsLinPresc WHERE ALLTRIM(prescstamp) == ALLTRIM(lcPrescstamp) AND LEFT(ALLTRIM(design),1) == "." AND !EMPTY(receitamcdt) INTO CURSOR ucrsValidaDadosCabecalho READWRITE 
	SELECT * FROM ucrsLinPresc WHERE ALLTRIM(prescstamp) == ALLTRIM(lcPrescstamp) AND !(LEFT(ALLTRIM(design),1) == ".") AND !EMPTY(receitamcdt) INTO CURSOR ucrsValidaDadosLinhas READWRITE 
	SELECT ucrsValidaDadosCabecalho
	Go Top
	SELECT ucrsValidaDadosLinhas
	Go Top

	** N�o podem existir Cabe�alhos sem linhas
	IF RECCOUNT("ucrsValidaDadosLinhas")==0
		uf_perguntalt_chama("Existem receitas sem linhas, n�o � possivel proceder � grava��o. Por favor verifique.", "", "OK", 32)
		RETURN .f.
	ENDIF		

	** Cada Receita pode conter no m�ximo 6 exames
	** Verifica se existem mais de 6 posicoes na receita 
	IF RECCOUNT("ucrsValidaDadosLinhas") > 6
		uf_perguntalt_chama("Uma receita pode no maximo conter 6 posi��es de MCDTs. Por favor verifique.", "", "OK", 32)
		RETURN .f.
	 ENDIF
	
	IF(USED("ucrsCabPresc"))
		
		SELECT ucrsCabPresc
		GO TOP
			
		TEXT TO lcSQL TEXTMERGE NOSHOW
			SELECT  TOP 1
				email, tlmvl 
			FROM 
				b_utentes
			WHERE
				no = <<ucrsCabPresc.no>>
				and estab = <<ucrsCabPresc.estab>>
			ENDTEXT 
			
			IF !uf_gerais_actgrelha("", "uCrsTempUser", lcSql)
				MESSAGEBOX("Ocorreu uma anomalia a pesquisar o cliente",16,"LOGITOOLS SOFTWARE")
				RETURN .f.
			ENDIF
		
		
		&&nao validar papel
		if(ucrsCabPresc.mcdt_notificacaoRequisicaoID > 1)
		
*!*				1	Papel
*!*				2	SMS
*!*				3	Email
*!*				4	SMS e Email
*!*				5	Papel e SMS
*!*				6	Papel e Email
*!*				7	Papel, SMS e Email
			
			&&valida sms
			IF(ucrsCabPresc.mcdt_notificacaoRequisicaoID == 2 OR ucrsCabPresc.mcdt_notificacaoRequisicaoID == 4 OR ucrsCabPresc.mcdt_notificacaoRequisicaoID == 5 OR ucrsCabPresc.mcdt_notificacaoRequisicaoID == 7)
			
					If(LEN(ALLTRIM(uCrsTempUser.tlmvl))<8)
						uf_perguntalt_chama("Por favor, preencha o numero de telemovel na ficha do utente.", "", "OK", 64)
						RETURN .f.
					ENDIF
					
				
			&&valida email			
			ELSE IF(ucrsCabPresc.mcdt_notificacaoRequisicaoID == 3 OR ucrsCabPresc.mcdt_notificacaoRequisicaoID == 4 OR ucrsCabPresc.mcdt_notificacaoRequisicaoID == 6 OR  ucrsCabPresc.mcdt_notificacaoRequisicaoID == 7)
					
					If(LEN(ALLTRIM(uCrsTempUser.email))<4)
						uf_perguntalt_chama("Por favor, preencha o email na ficha do utente.", "", "OK", 64)
						RETURN .f.
					ENDIF
					
			ENDIF	
		
		
		ENDIF
		

	ENDIF
	

	IF USED("uCrsTempUser")
		fecha("uCrsTempUser")	
	ENDIF 
	
	
	RETURN .t.
ENDFUNC


**
FUNCTION uf_prescricao_validaDadosMed
	LPARAMETERS lcPrescstamp
	LOCAL ARRAY lcCountRef[1]

	
	IF USED("ucrsValidaDadosCabecalho")
		fecha("ucrsValidaDadosCabecalho")
	ENDIF
	
	IF USED("ucrsValidaDadosLinhas")
		fecha("ucrsValidaDadosLinhas")
	ENDIF
	
	SELECT * FROM ucrsLinPresc WHERE ALLTRIM(prescstamp) == ALLTRIM(lcPrescstamp) AND LEFT(ALLTRIM(design),1) == "." AND EMPTY(receitamcdt) INTO CURSOR ucrsValidaDadosCabecalho READWRITE 
	SELECT * FROM ucrsLinPresc WHERE ALLTRIM(prescstamp) == ALLTRIM(lcPrescstamp) AND !(LEFT(ALLTRIM(design),1) == ".") AND EMPTY(receitamcdt) INTO CURSOR ucrsValidaDadosLinhas READWRITE 
	SELECT ucrsValidaDadosCabecalho
	GO TOP 
	SELECT ucrsValidaDadosLinhas
	GO TOP 
		
	** N�o podem existir Cabe�alhos sem linhas
	IF RECCOUNT("ucrsValidaDadosLinhas")==0
		uf_perguntalt_chama("Existem receitas sem linhas, n�o � possivel proceder � grava��o. Por favor verifique.", "", "OK", 32)
		RETURN .f.
	ENDIF	
	
	
	** Verifica se existem mais de 4 posicoes na receita - apenas para prescricaos materializadas
	IF !EMPTY(myprescmaterializada)
		IF RECCOUNT("ucrsValidaDadosLinhas") > 4
			uf_perguntalt_chama("Uma receita pode no maximo conter 4 posi��es. Por favor verifique.", "", "OK", 32)
			RETURN .f.
		ENDIF
	ENDIF
	
	** N�o podem existir linhas sem designa��o
	SELECT * FROM ucrsValidaDadosLinhas WHERE EMPTY(ALLTRIM(design)) == .t. INTO CURSOR ucrsValidaLinhaSemDesignacao READWRITE 
	SELECT ucrsValidaLinhaSemDesignacao 
	IF RECCOUNT("ucrsValidaLinhaSemDesignacao")>0
		uf_perguntalt_chama("Existem linhas sem designa��o, n�o � possivel proceder � grava��o. Por favor verifique.", "", "OK", 32)
		RETURN .f.
	ENDIF	
			
	** Caso a prescricao seja por Nome comercial e a excecao seja a c), obriga o preencimento de posologia
	SELECT * FROM ucrsValidaDadosLinhas WHERE ALLTRIM(prescNomeCod) == "C" AND EMPTY(ALLTRIM(posologia))==.t. INTO CURSOR ucrsValidaExcecaoC READWRITE 
	IF RECCOUNT("ucrsValidaExcecaoC")>0
		uf_perguntalt_chama("O uso da excep��o c) para prescricao por Titular/Nome Comercial, obriga o preenchimento do campo posologia, identificando uma dura��o de tratamento superior a 28 dias. Por favor verifique.", "", "OK", 32)
		RETURN .f.
	ENDIF
	IF USED("ucrsValidaExcecaoC")
		fecha("ucrsValidaExcecaoC")
	ENDIF 
	
*!*		**Cada receita apenas pode conter um medicamento prescrito por nome comercial/titular
*!*		SELECT COUNT(ref) as count_refs FROM ucrsValidaDadosLinhas WHERE pordci ==.f. AND manipulado == .f. AND ALLTRIM(UPPER(tiporeceita)) != "OUT" AND ALLTRIM(UPPER(tiporeceita)) != "MDT" INTO CURSOR ucrsValidaNomeTitular READWRITE 
*!*		SELECT ucrsValidaNomeTitular
*!*		IF ucrsValidaNomeTitular.count_refs > 1
*!*			uf_perguntalt_chama("Cada receita apenas pode conter um medicamento prescrito por nome comercial/titular. Existem receitas que n�o obdecem a esta regra. Por favor verifique.", "", "OK", 32)
*!*			RETURN .f.
*!*		ENDIF
*!*		IF USED("ucrsValidaNomeTitular")
*!*			fecha("ucrsValidaNomeTitular")
*!*		ENDIF
	
	** Numero maximo de Embalagens - at� um total de 4 Embalagens por receita
	IF !EMPTY(myprescmaterializada)
		SELECT SUM(qtt) as NumEmb FROM ucrsValidaDadosLinhas INTO CURSOR ucrsValidaEmbalagensPorReceita READWRITE 
		SELECT ucrsValidaEmbalagensPorReceita 
		IF ucrsValidaEmbalagensPorReceita.NumEmb > 4
			uf_perguntalt_chama("Cada receita apenas pode conter at� um total de 4 embalagens. Existem receitas que n�o obdecem a esta regra. Por favor verifique.", "", "OK", 32)
			RETURN .f.
		ENDIF
		IF USED("ucrsValidaEmbalagensPorReceita")
			fecha("ucrsValidaEmbalagensPorReceita")
		ENDIF
	ENDIF
	**
	
	**Em receitas desmaterializadas apenas se pode ter uma linha por medicamentos
	IF EMPTY(myprescmaterializada)	
		SELECT ref  FROM ucrsLinPresc INTO CURSOR ucrsValidaRefUnico nofilter
		SELECT COUNT(ref) as qttRef FROM ucrsValidaRefUnico WHERE REF IN (SELECT REF FROM ucrsValidaRefUnico HAVING Count(ref) > 1 group by ref) ORDER BY qttRef INTO ARRAY lcCountRef
		IF(lcCountRef)>1
			uf_perguntalt_chama("Cada receita apenas pode conter uma linha do mesmo medicamento. Existem linhas  que n�o obedecem a esta regra. Por favor verifique.", "", "OK", 32)
			IF USED("ucrsValidaRefUnicoRef")
				fecha("ucrsValidaRefUnicoRef")
			ENDIF
			RETURN .f.	
		ENDIF
		IF USED("ucrsValidaRefUnicoRef")
				fecha("ucrsValidaRefUnicoRef")
		ENDIF	
	ENDIF
	
	
	
	LOCAL maxPrescEmb
	STORE 2 TO  maxPrescEmb
	
	IF EMPTY(myprescmaterializada)
		STORE 6 TO  maxPrescEmb
	ENDIF
		
	** Numero maximo de Embalagens por Linha - at� um total de 2 ou 6 Embalagens por receita
	SELECT * FROM ucrsValidaDadosLinhas WHERE ucrsValidaDadosLinhas.qtt > maxPrescEmb INTO CURSOR ucrsValidaEmbalagensPorLinha READWRITE 
	SELECT ucrsValidaEmbalagensPorLinha
	IF RECCOUNT("ucrsValidaEmbalagensPorLinha") > maxPrescEmb 
		uf_perguntalt_chama("Cada linha da receita pode conter no m�ximo "+ STR(maxPrescEmb) +" embalagens. Existem receitas que n�o obdecem a esta regra. Por favor verifique.", "", "OK", 32)
		RETURN .f
	ENDIF
	IF USED("ucrsValidaEmbalagensPorLinha")
		fecha("ucrsValidaEmbalagensPorLinha")
	ENDIF

		
	** Verifica se existem receitas de medicamentos de curta dura��o em receitas renovaeis 
	SELECT ucrsValidaDadosCabecalho 
	IF ucrsValidaDadosCabecalho.renovavel == .t.
		SELECT COUNT(ref) as count_refs FROM ucrsValidaDadosLinhas WHERE ALLTRIM(tipoduracao) != "Longa Dura��o" INTO CURSOR ucrsValidaRenovaveis READWRITE 
		SELECT ucrsValidaRenovaveis
		IF ucrsValidaRenovaveis.count_refs >0
			uf_perguntalt_chama("Existem medicamentos em receitas renov�veis, que n�o s�o de longa dura��o. Por favor verifique.", "", "OK", 32)
			RETURN .f.
		ENDIF
		IF USED("ucrsValidaRenovaveis")
			fecha("ucrsValidaRenovaveis")
		ENDIF
		**
	ENDIF

	** Se a prescri��o foi efectuada por nome comercial com justifica��o e sem justifica��o
	SELECT COUNT(ref) as NumRefs FROM ucrsValidaDadosLinhas Where EMPTY(prescNomeCod) == .t. AND (ucrsValidaDadosLinhas.tipoReceita == "RE" OR ucrsValidaDadosLinhas.tipoReceita == "RN") INTO CURSOR ucrsValidaExisteRefsPorNomeCjust READWRITE 
	SELECT COUNT(ref) as NumRefs FROM ucrsValidaDadosLinhas Where EMPTY(prescNomeCod) == .f. AND (ucrsValidaDadosLinhas.tipoReceita == "RE" OR ucrsValidaDadosLinhas.tipoReceita == "RN") INTO CURSOR ucrsValidaExisteRefsPorNomeSjust READWRITE 
	SELECT ucrsValidaExisteRefsPorNomeCjust
	SELECT ucrsValidaExisteRefsPorNomeSjust
	IF ucrsValidaExisteRefsPorNomeCjust.NumRefs > 0 AND ucrsValidaExisteRefsPorNomeSjust.NumRefs > 0
		uf_perguntalt_chama("Uma Receita n�o pode conter medicamentos com justifica��o t�cnica e sem justifica��o t�cnica em simult�neo. Por favor verifique.","", "OK", 64)
		return .f.
	ENDIF
	
	RETURN .t.
ENDFUNC


**
FUNCTION uf_prescricao_atribuiNumeracaoMCDT

	LcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_prescricao_NumeracaoPrescMCDT 1, '<<mysite>>'
	ENDTEXT

	IF !uf_gerais_actgrelha("", "uCrsNumeroMCDT", lcSql)
		MESSAGEBOX("N�o foi possivel atribuir numera��o local � receita MCDT. Contacte o suporte.",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF
	

	lcNovoNumero  = ALLTRIM(uCrsNumeroMCDT.numeracao)	
	
	IF USED("uCrsNumeroMCDT")
		fecha("uCrsNumeroMCDT")
	ENDIF


	RETURN lcNovoNumero  
	
ENDFUNC


**
FUNCTION uf_prescricao_verificaExistemReceitasOffline
	SELECT ucrsLinPresc
	GO Top
	LOCATE FOR ALLTRIM(ucrsLinPresc.estado) == "2"
	IF FOUND()
		RETURN .t.
	ELSE
		RETURN .f.
	ENDIF
ENDFUNC


*!*	**
*!*	FUNCTION uf_prescricao_GravaReceitasOffline
*!*		LOCAL lcTemReceitasParaEnviarOffline
*!*		STORE .f. to lcTemReceitasParaEnviarOffline
*!*		LOCAL lcNumeracao, lcNumeracao2, lcNumeracao3

*!*		IF !(uf_perguntalt_chama("A grava��o de receitas offline � excepcional." + CHR(13) + CHR(13) + "S� deve ocorrer caso n�o seja poss�vel a utiliza��o de processos online." + CHR(13) + CHR(13) + "Pretende continuar?","Sim", "N�o", 64))
*!*			RETURN .f.
*!*		ENDIF 

*!*		SELECT ucrsCabPresc
*!*		lcNrPrescricao = ucrsCabPresc.nrPrescricao
*!*		
*!*		SELECT * FROM ucrsLinPresc INTO CURSOR ucrsLinPrescTemp Readwrite

*!*		SELECT ucrsLinPresc
*!*		GO TOP 
*!*		SCAN
*!*			IF Left(ucrsLinPresc.design,1)=='.' AND (ALLTRIM(ucrsLinPresc.estado) = '2' OR ALLTRIM(ucrsLinPresc.estado) = '0')
*!*				
*!*				lcSQL = ''
*!*				TEXT TO lcSQL NOSHOW TEXTMERGE 
*!*					up_prescricao_verificaRespostaWebservice '<<ucrsLinPresc.prescstamp>>'
*!*				ENDTEXT
*!*				IF !uf_gerais_actgrelha("", "uCrsverificaRespostaWebservice", lcSql)
*!*					MESSAGEBOX("Ocorreu uma anomalia na verifica��o de resposta do Webservice em modo online. Contacte o suporte.",16,"LOGITOOLS SOFTWARE")
*!*					RETURN .f.
*!*				ENDIF
*!*				
*!*				** S� permite a grava��o offline quando existe uma mensagem devolvida pelo metodo online com o c�digo Webservice n�o est� a funcionar, ou o paramtero est� activo
*!*				IF RECCOUNT("uCrsverificaRespostaWebservice") > 0 OR uf_gerais_getParameter("ADM0000000230","BOOL") == .t.
*!*					
*!*					SELECT ucrsLinPresc
*!*					lcNumeracao = uf_prescricao_numeracaoReceitaMedicamentos(IIF(ucrsLinPresc.renovavel==.t.,'02','01'), "1", ucrsLinPresc.renovavel)
*!*					lcNumeracao2 = IIF(ucrsLinPresc.renovavel == .t. and ucrsLinPresc.vias > 1, uf_prescricao_numeracaoReceitaMedicamentos(IIF(ucrsLinPresc.renovavel==.t.,'02','01'), "2", ucrsLinPresc.renovavel),"")
*!*					lcNumeracao3 = IIF(ucrsLinPresc.renovavel == .t. and ucrsLinPresc.vias > 2, uf_prescricao_numeracaoReceitaMedicamentos(IIF(ucrsLinPresc.renovavel==.t.,'02','01'), "3", ucrsLinPresc.renovavel),"")

*!*					** Marca como receita offline
*!*				
*!*					Replace ucrsLinPresc.estado WITH "3"
*!*					Replace ucrsLinPresc.estadodescr WITH "Receita Offline."
*!*					Replace ucrsLinPresc.data WITH uf_gerais_getDateServidor()
*!*					Replace ucrsLinPresc.prescno WITH lcNumeracao
*!*					Replace ucrsLinPresc.prescno2 WITH lcNumeracao2
*!*					Replace ucrsLinPresc.prescno3 WITH lcNumeracao3
*!*					
*!*								
*!*					TEXT TO lcSQL TEXTMERGE NOSHOW
*!*						UPDATE 
*!*							b_cli_presc
*!*						SET 
*!*							estado = '3'
*!*							,estadodescr = 'Receita Offline'
*!*							,data = '<<uf_gerais_getdate(ucrsLinPresc.data,"SQL")>>'
*!*							,prescno = '<<ALLTRIM(lcNumeracao)>>'
*!*							,prescno2 = '<<ALLTRIM(lcNumeracao2)>>'
*!*							,prescno3 = '<<ALLTRIM(lcNumeracao3)>>'
*!*						Where
*!*							prescstamp = '<<ALLTRIM(ucrsLinPresc.prescstamp)>>'
*!*						
*!*						UPDATE 
*!*							b_cli_prescl
*!*						SET 
*!*							estado = '3'
*!*							,estadodescr = 'Receita Offline'
*!*							,data = '<<uf_gerais_getdate(ucrsLinPresc.data,"SQL")>>'
*!*							,prescno = '<<ALLTRIM(lcNumeracao)>>'
*!*							,prescno2 = '<<ALLTRIM(lcNumeracao2)>>'
*!*							,prescno3 = '<<ALLTRIM(lcNumeracao3)>>'
*!*						Where
*!*							prescstamp = '<<ALLTRIM(ucrsLinPresc.prescstamp)>>'
*!*					ENDTEXT 
*!*					IF !uf_gerais_actgrelha("", "uCrsUpdateEstadoOffline", lcSql)
*!*						MESSAGEBOX("Ocorreu uma anomalia na grava��o em modo offline. Contacte o suporte.",16,"LOGITOOLS SOFTWARE")
*!*						RETURN .f.
*!*					ENDIF
*!*					
*!*					lcTemReceitasParaEnviarOffline = .t.
*!*				ELSE
*!*					uf_perguntalt_chama("A grava��o de receitas offline apenas � permitido caso o metodo online n�o esteja em funcionamento.","OK", "", 64)
*!*					RETURN .f.
*!*				ENDIF
*!*			ENDIF
*!*			SELECT ucrsLinPresc
*!*		ENDSCAN
*!*		
*!*		IF lcTemReceitasParaEnviarOffline == .t. && OR uf_gerais_getParameter("ADM0000000194","BOOL") == .t.
*!*			SELECT ucrsLinPresc
*!*			GO Top
*!*			SCAN FOR Left(ucrsLinPresc.design,1)=='.' AND ALLTRIM(ucrsLinPresc.estado) = '3'
*!*				lcPrescStamp = ucrsLinPresc.prescstamp
*!*				lcstampEnvioOffline = uf_gerais_stamp()
*!*			
*!*				IF ucrsLinPresc.receitamcdt == .t. && MCDTs
*!*					lcMcdt = .t.
*!*					uf_prescricao_enviaReceitaMedOnOffline(lcPrescStamp, lcstampEnvioOffline, lcMcdt, .t.)
*!*				ELSE
*!*					lcMcdt = .f.
*!*					uf_prescricao_enviaReceitaMedOnOffline(lcPrescStamp, lcstampEnvioOffline, lcMcdt, .t.)
*!*				ENDIF
*!*				
*!*			ENDSCAN

*!*			uf_perguntalt_chama("Receitas gravadas pelo processo offline com sucesso." + CHR(13) + CHR(13) + "Para as receitas gravadas em modo offline � obrigat�rio a aposi��o de vinheta do prescritor.","OK", "", 64)
*!*		ELSE
*!*			uf_perguntalt_chama("N�o existem receitas para enviar pelo modo offline." + CHR(13) + CHR(13) + "O processo offline s� pode ser usado depois de submeter as receitas em modo online." + CHR(13) + CHR(13) + "Verifique por favor.","", "OK", 64)
*!*		ENDIF	
*!*		
*!*		IF !EMPTY(lcNrPrescricao)
*!*			uf_prescricao_chama(ALLTRIM(lcNrPrescricao))
*!*		ENDIF
*!*	ENDFUNC


**
FUNCTION uf_prescricao_EnviaReceitaIndividual
	
	&& verifica estado da prescri��o
	IF myPrescricaoIntroducao == .t. or myPrescricaoAlteracao == .t.
		uf_perguntalt_chama("Deve gravar a receita antes de proceder ao envio.","OK","",64)
		RETURN .f.
	ENDIF
		
	&& Controla envio de receita mediante estado da receita
	Select ucrsLinPresc
	IF Left(ucrsLinPresc.design,1)=='.' AND (EMPTY(ucrsLinPresc.estado) OR ALLTRIM(ucrsLinPresc.estado) == '0' OR ALLTRIM(ucrsLinPresc.estado) == '2') 
		uf_prescricao_EnviaReceitasOnline()
	ELSE
		IF Left(ucrsLinPresc.design,1) == '.' AND ALLTRIM(uCrsLinPresc.estado) == '3' && Envia Receita Modo Offline
			uf_prescricao_EnviaReceitasOffline()
		ELSE
			uf_perguntalt_chama("A receita n�o se encontra preparada para envio. Por favor verifique.", "OK", "", 48)
			RETURN .f.
		ENDIF
	ENDIF			
ENDFUNC 


**
FUNCTION uf_prescricao_EnviaReceitas
	LOCAL lcTemReceitasParaEnviarOnline,lcTemReceitasParaEnviarOffline, lcReceitasNaoValidadasOnline
	STORE .f. to lcTemReceitasParaEnviarOnline, lcReceitasNaoValidadasOnline
	
	&& verifica estado na Prescricao
	IF myPrescricaoIntroducao == .t. or myPrescricaoAlteracao == .t.
		uf_perguntalt_chama("Deve gravar a receita antes de proceder ao envio.","OK","",64)
		RETURN .f.
	ENDIF 
	
	SELECT ucrsCabPresc
	lcNrPrescricao = ucrsCabPresc.nrPrescricao

	prescricao.lockscreen = .t.
	
	&& Envia receitas em situa��o normais
	SELECT * FROM ucrsLinPresc WHERE Left(ucrsLinPresc.design,1)=='.' AND (EMPTY(ucrsLinPresc.estado) OR ALLTRIM(ucrsLinPresc.estado) == '0' OR ALLTRIM(ucrsLinPresc.estado) == '2') INTO CURSOR ucrsListaPrescEnviarOnline READWRITE
	SELECT ucrsListaPrescEnviarOnline
	IF RECCOUNT("ucrsListaPrescEnviarOnline") != 0
		uf_prescricao_EnviaReceitasOnline()
	ENDIF
	
	&& Envia receitas que est�o em estado offline porque n�o foram submetidas anteriormente devido a indisponibilidade dos servi�os centrais.
	SELECT * FROM ucrsLinPresc WHERE Left(ucrsLinPresc.design,1) == '.' AND ALLTRIM(uCrsLinPresc.estado) == '3' INTO CURSOR ucrsListaPrescEnviarOffline READWRITE
	SELECT ucrsListaPrescEnviarOffline
	IF RECCOUNT("ucrsListaPrescEnviarOffline") != 0
		uf_prescricao_EnviaReceitasOffline()
	ENDIF

	IF !EMPTY(lcNrPrescricao)
		uf_prescricao_chama(ALLTRIM(lcNrPrescricao))
	ENDIF
ENDFUNC


** "Opera��o Online" 4.6.1 Normais T�cnicas
FUNCTION uf_prescricao_EnviaReceitasOnline
	LPARAMETERS lnPrescStamp
	
	LOCAL lcTemReceitasParaEnviarOnline, lcReceitasNaoValidadasOnline
	STORE .f. to lcTemReceitasParaEnviarOnline, lcReceitasNaoValidadasOnline

	** 
	IF !uf_gerais_verifyInternet()
		uf_perguntalt_chama("ESTA FUNCIONALIDADE REQUER LIGA��O � INTERNET."+CHR(13)+"Por favor valide.","OK","",64)
		RETURN .f.
	ENDIF

	SELECT ucrsCabPresc
	lcNrPrescricao = ucrsCabPresc.nrPrescricao
		
	SELECT * FROM ucrsLinPresc WHERE Left(ucrsLinPresc.design,1)=='.' AND (EMPTY(ucrsLinPresc.estado) OR ALLTRIM(ucrsLinPresc.estado) == '0' OR ALLTRIM(ucrsLinPresc.estado) == '2') INTO CURSOR ucrsListaPrescEnviarOnline READWRITE

	IF RECCOUNT("ucrsListaPrescEnviarOnline") == 0
		uf_perguntalt_chama("N�o existem receitas a enviar pelo metodo online.","OK","",64)
		RETURN .f.
	ENDIF
	
	regua(0,5,"A enviar receita em modo online...")
	regua(1,1,"A enviar receita em modo online...")

	SELECT ucrsListaPrescEnviarOnline
	GO TOP 
	SCAN FOR EMPTY(lnPrescStamp) OR ALLTRIM(lnPrescStamp) == ALLTRIM(ucrsListaPrescEnviarOnline.prescstamp)
		lcPrescStamp = ucrsListaPrescEnviarOnline.prescstamp
		lcstampEnvioOnline = uf_gerais_stamp()

		SELECT ucrsListaPrescEnviarOnline
		**Gerar stamp unico para cada pedido
		Replace ucrsListaPrescEnviarOnline.stampEnvioOnline WITH lcstampEnvioOnline 
		
		IF ucrsListaPrescEnviarOnline.receitamcdt == .t. &&MCDTs
			lcMcdt = .t.
			uf_prescricao_enviaReceitaMedOnOffline(lcPrescStamp, lcstampEnvioOnline, lcMcdt, .f.)
		ELSE
			lcMcdt = .f.
			uf_prescricao_enviaReceitaMedOnOffline(lcPrescStamp, lcstampEnvioOnline, lcMcdt, .f.)
		ENDIF
	ENDSCAN
		
	** Activa Timer para obter respostas de todas as receitas enviadas
	prescricao.tmrEnvioOnline.enabled = .t.
	
	regua(1,2,"A enviar receita em modo online...")
	
	IF !EMPTY(lcNrPrescricao)
		uf_prescricao_chama(ALLTRIM(lcNrPrescricao))
	ENDIF
ENDFUNC


** "Opera��o Offline" 4.6.2 Normais T�cnicas
FUNCTION uf_prescricao_EnviaReceitasOffline
	LOCAL lcTemReceitasParaEnviarOnline, lcReceitasNaoValidadasOnline
	STORE .f. to lcTemReceitasParaEnviarOnline, lcReceitasNaoValidadasOnline

**MESSAGEBOX('Opera��o Offline')
		
	*internet
	IF !uf_gerais_verifyInternet()
		uf_perguntalt_chama("ESTA FUNCIONALIDADE REQUER LIGA��O � INTERNET."+CHR(13)+"Por favor valide.","OK","",64)
		RETURN .f.
	ENDIF
	**

	SELECT ucrsCabPresc
	lcNrPrescricao = ucrsCabPresc.nrPrescricao
		
	SELECT * FROM ucrsLinPresc WHERE Left(ucrsLinPresc.design,1)=='.' AND ALLTRIM(ucrsLinPresc.estado) == '3' INTO CURSOR ucrsListaPrescEnviarOffline READWRITE
	
	IF RECCOUNT("uCrsListaPrescEnviarOffline") == 0
		uf_perguntalt_chama("N�o existem receitas a enviar pelo metodo offline.","OK","",64)
		RETURN .f.
	ENDIF
	
	**Return .f.
	
	regua(0,13,"A enviar receita em modo online...")
	regua(1,1,"A enviar receita em modo online...")


	SELECT ucrsListaPrescEnviarOffline
	GO TOP 
	SCAN
		lcPrescStamp = ucrsListaPrescEnviarOffline.prescstamp
		lcstampEnvioOffline = uf_gerais_stamp()

		SELECT ucrsListaPrescEnviarOffline
		
		**Gerar stamp unico para cada pedido
		REplace ucrsListaPrescEnviarOffline.stampEnvioOnline WITH lcstampEnvioOffline
		
		
		IF ucrsListaPrescEnviarOffline.receitamcdt == .t. &&MCDTs
			lcMcdt = .t.
			uf_prescricao_enviaReceitaMedOnOffline(lcPrescStamp, lcstampEnvioOffline, lcMcdt, .t.)
		ELSE
			lcMcdt = .f.
			uf_prescricao_enviaReceitaMedOnOffline(lcPrescStamp, lcstampEnvioOffline, lcMcdt, .t.)
		ENDIF
	ENDSCAN
	
	**Activa Timer para obter respostas de todas as receitas enviadas
	prescricao.tmrEnvioOffline.enabled = .t.
	
	&&regua(1,2,"A enviar receita em modo online...")
	regua(1,2,"A enviar receita em modo online...")
	IF !EMPTY(lcNrPrescricao)
		uf_prescricao_chama(ALLTRIM(lcNrPrescricao))
	ENDIF
ENDFUNC


** Envio informa��o Receitas MED || MCDTs
FUNCTION uf_prescricao_enviaReceitaMedOnOffline
	LPARAMETERS lcPrescStamp, lcstampEnvioOnline, lcMcdt, lcOffline
	LOCAL lcQrCodePath
	STORE '' TO lcQrCodePath
	**
	prescricao.stampEnvioOffline = uf_gerais_stamp()
	
	&& verifica se utiliza webservice de testes
	IF uf_gerais_getParameter("ADM0000000227","BOOL") == .f.
		lcTest = 0
	ELSE
		lcTest = 1
	ENDIF 

	&& verifica o webservice a usar - RCP Prescri��es Materializadas || RSP Prescri��es desMaterializadas 
	IF myPrescMaterializada == .t.
		lcNomeJar = 'rpm_rcp.jar'
	ELSE
		&&usa sistema de prescri��o antiga
		IF uf_gerais_getParameter("ADM0000000295","BOOL") == .t.
			lcNomeJar = 'RPM_RSP\rpm_rsp.jar'
		&&usa sistema de prescri��o nova
		ELSE
			lcNomeJar = 'LTS_RPM_RSP\rpm_rsp.jar'
		ENDIF 
		
	ENDIF
	
	&& verifica diretoria onde vai guardar qrCode
	lcQrCodePath = ALLTRIM(ALLTRIM(uf_gerais_getParameter("ADM0000000015","TEXT")) + "\Cache")
	
	&&
	IF DIRECTORY(ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\PE')
		&& Envio de Prescri��es Eletronicas Medicamentos
		IF lcMcdt == .f.
			IF myPrescMaterializada == .t.
				lcWsPath = ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\PE\RPM_RCP\' + ALLTRIM(lcNomeJar);
					+ ' "--PrescStamp=' + ALLTRIM(lcPrescStamp) + ["];
					+ ' "--StampLigacao=' + ALLTRIM(lcstampEnvioOnline) + ["];
					+ ' "--Test=' + ALLTRIM(STR(lcTest)) + ["];
					+ ' "--clID=' + ALLTRIM(uCrsE1.id_lt) + ["];
					+ ' "--Offline=' + IIF(lcOffline,'1','0') + ["];
					+ ' "--chavePedidoRelacionado= ' + '' + ["]
					
					
							
			ELSE
				&& servico de envio de RSPs tem mais parametros
				lcWsPath = ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\PE\' + ALLTRIM(lcNomeJar);
					+ ' "--PrescStamp=' + ALLTRIM(lcPrescStamp) + ["];
					+ ' "--StampLigacao=' + ALLTRIM(lcstampEnvioOnline) + ["];
					+ ' "--Test=' + ALLTRIM(STR(lcTest)) + ["];
					+ ' "--clID=' + ALLTRIM(uCrsE1.id_lt) + ["];
					+ ' "--Offline=' + IIF(lcOffline,'1','0') + ["];
					+ ' "--chavePedidoRelacionado= ' + '' + ["];
					+ ' "--qrCodePath=' + ALLTRIM(lcQrCodePath) + ["];
					+ ' "--computerName=' + ALLTRIM(myClientName) + ["];
					+ ' "--terminal_id=' + ALLTRIM(STR(mytermno)) + ["];
					
			ENDIF
			
	
			
		
		ELSE
			&& Envio de Prescri��es Eletronicas MCDTS
			
			lcWsPath = ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\PE\MCDT\MCDT.jar';
					+ ' "--PrescStamp=' + ALLTRIM(lcPrescStamp) + ["];
					+ ' "--StampLigacao=' + ALLTRIM(lcstampEnvioOnline) + ["];
					+ ' "--clID=' + ALLTRIM(uCrsE1.id_lt) + ["];
					+ ' "--chavePedidoRelacionado= ' + '' + ["];
					+ ' "--computerName=' + ALLTRIM(myClientName) + ["];
					+ ' "--terminal_id=' + ALLTRIM(STR(mytermno)) + ["];
					+ ' "--req_type= ' + '1' + ["];			
			
		ENDIF	
	ENDIF
	
	
	** 

	&& 3� parametro a .t. aguardar reposta do Java
	lcWsPath = "javaw -jar " + lcWsPath 
	
	IF(emdesenvolvimento == .t.)
		_cliptext = lcWsPath 
	ENDIF
	

	oWSShell = CREATEOBJECT("WScript.Shell")
	oWSShell.Run(lcWsPath,1,.f.)	
	
	
	

	
	&& s� envia 
	IF EMPTY(MyPrescMaterializada)
		uf_prescricao_sendMouseMove()
	
		
		&& comando que chama o LTS Cart�o de Cidad�o 
		&& font:  //msdn.microsoft.com/en-us/library/aa266279(v=vs.60).aspx
		&&DECLARE Sleep IN Win32API LONG

		&&local loShell
		&&loShell=CREATEOBJECT("wscript.shell")
		&&loShell.sendKeys("{NUMLOCK}")
		&&Sleep(750.0)
		&&loShell.sendKeys("{NUMLOCK}")
		&&Sleep(750.0)
		&&loShell.sendKeys("{NUMLOCK}")
		&&Sleep(750.0)
		&&loShell.sendKeys("{NUMLOCK}")
		&&Sleep(750.0)
	ENDIF
		
ENDFUNC


**
FUNCTION uf_prescricao_tmrEnvioOnline
	LOCAL lcReceitasNaoValidadasOnline, lcNrPrescricao  
	lcReceitasNaoValidadasOnline = .f.
	lcNrPrescricao = ""
		
	IF USED("uCrsVerificaTodasRespostaEnvioOnline")
		fecha("uCrsVerificaTodasRespostaEnvioOnline")
	ENDIF
	TEXT TO lcSql NOSHOW textmerge
		exec up_prescricao_RespostaEnvioOnline null
	ENDTEXT
	IF !uf_gerais_actGrelha("","uCrsVerificaTodasRespostaEnvioOnline",lcSql)
		uf_perguntalt_chama("Ocorreu um erro a verificar a resposta do envio da receita online. Por favor contacte o suporte.","OK","",16)
		RETURN .f.
	ENDIF
 
	prescricao.ntentativasOnline = prescricao.ntentativasOnline + 1
	

	IF prescricao.ntentativasOnline > 5 && 5 tentativas de 10.0 segundos
		regua(2)
		uf_perguntalt_chama("N�o foi possivel obter resposta pelo servi�o de prescri��o de receitas.","OK","",16)
	
		prescricao.lockscreen = .f.

		prescricao.tmrEnvioOnline.enabled = .f.
		prescricao.ntentativasOnline = 0
		prescricao.stampEnvioOnline = ''
		RETURN .f.
	ENDIF
	regua(1,prescricao.ntentativasOnline + 2,"A enviar receita em modo online...")
	
	SELECT ucrsListaPrescEnviarOnline
	GO TOP
	lcNrPrescricao = ucrsListaPrescEnviarOnline.nrPrescricao
	SCAN
		lcSQL = ''
		TEXT TO lcSql NOSHOW textmerge
			exec up_prescricao_RespostaEnvioOnline <<IIF(EMPTY(ALLTRIM(ucrsListaPrescEnviarOnline.stampEnvioOnline)) or ISNULL(ucrsListaPrescEnviarOnline.stampEnvioOnline),'null',"'" + ALLTRIM(ucrsListaPrescEnviarOnline.stampEnvioOnline) +"'")>>
		ENDTEXT

		IF !uf_gerais_actGrelha("","uCrsVerificaRespostaEnvioOnline",lcSql)
			regua(2)
			uf_perguntalt_chama("Ocorreu um erro a verificar a resposta do envio da receita online. Por favor contacte o suporte.","OK","",16)

			prescricao.lockscreen = .f.

			prescricao.tmrEnvioOnline.enabled = .f.
			prescricao.ntentativasOnline = 0
			prescricao.stampEnvioOnline = ''
		ELSE
					
			SELECT uCrsVerificaTodasRespostaEnvioOnline
			APPEND FROM DBF("uCrsVerificaRespostaEnvioOnline")
			
		ENDIF
	ENDSCAN
	

	
	SELECT * FROM ucrsListaPrescEnviarOnline LEFT JOIN uCrsVerificaTodasRespostaEnvioOnline ON ucrsListaPrescEnviarOnline.prescstamp = uCrsVerificaTodasRespostaEnvioOnline.prescstamp ;
	WHERE uCrsVerificaTodasRespostaEnvioOnline.prescstamp = .NULL. INTO CURSOR uCrsVerificaTodasRespostaEnvioOnlineValida READWRITE 
	SELECT uCrsVerificaTodasRespostaEnvioOnlineValida 
	
	IF RECCOUNT("uCrsVerificaTodasRespostaEnvioOnlineValida") == 0 AND RECCOUNT("uCrsVerificaRespostaEnvioOnline") != 0
		lcTemTodasRespostas = .t.
	ELSE
		lcTemTodasRespostas = .f.
	ENDIF
	
	IF lcTemTodasRespostas == .t.	

		**Verfica se contem Erros
		SELECT uCrsVerificaTodasRespostaEnvioOnline
		GO TOP 
		SCAN
			lcSucesso = .f.
			
			lcSQL = ''
			TEXT TO lcSql NOSHOW textmerge
				exec up_prescricao_RespostaErrosEnvioOnline <<IIF(EMPTY(ALLTRIM(uCrsVerificaTodasRespostaEnvioOnline.stamp)) or ISNULL(uCrsVerificaTodasRespostaEnvioOnline.stamp),'null',"'" + ALLTRIM(uCrsVerificaTodasRespostaEnvioOnline.stamp) +"'")>>
			ENDTEXT
		
			IF !uf_gerais_actGrelha("","uCrsVerificaRespostaErrosEnvioOnline",lcSql)
				uf_perguntalt_chama("Ocorreu um erro a verificar a resposta do envio da receita online. Por favor contacte o suporte.","OK","",16)
			ELSE	
		
						
				SELECT uCrsVerificaRespostaErrosEnvioOnline
				LOCATE FOR ALLTRIM(uCrsVerificaRespostaErrosEnvioOnline.Codigo) == "100002010001" OR ALLTRIM(uCrsVerificaRespostaErrosEnvioOnline.Codigo) == "201101000" OR	ALLTRIM(uCrsVerificaRespostaErrosEnvioOnline.Codigo) == "100007010001"		
				
				IF FOUND() && Encontrou Mensagem de Receita registada com sucesso
					lcSQL = ''
					
					TEXT TO lcSQL TEXTMERGE NOSHOW
						UPDATE 
							b_cli_presc
						SET 
							enviado = 1
							,estado = '1'
							,estadodescr = 'Receita enviada e validada pelo processo Online'
							,prescno = '<<uCrsVerificaRespostaErrosEnvioOnline.nreceita>>'
							,data = '<<uf_gerais_getdate(uCrsVerificaRespostaErrosEnvioOnline.dataRec,"SQL")>>'
							,pin = '<<ALLTRIM(uCrsVerificaRespostaErrosEnvioOnline.pin)>>'
							,pinOpcao = '<<ALLTRIM(uCrsVerificaRespostaErrosEnvioOnline.pinOpcao)>>'
							,prescno2 = '<<uCrsVerificaRespostaErrosEnvioOnline.nreceita2>>'
							,pin2 = '<<ALLTRIM(uCrsVerificaRespostaErrosEnvioOnline.pin2)>>'
							,pinOpcao2 = '<<ALLTRIM(uCrsVerificaRespostaErrosEnvioOnline.pinOpcao2)>>'						
							,prescno3 = '<<uCrsVerificaRespostaErrosEnvioOnline.nreceita3>>'
							,pin3 = '<<ALLTRIM(uCrsVerificaRespostaErrosEnvioOnline.pin3)>>'
							,pinOpcao3 = '<<ALLTRIM(uCrsVerificaRespostaErrosEnvioOnline.pinOpcao3)>>'
							,nreclocal = '<<ALLTRIM(uCrsVerificaRespostaErrosEnvioOnline.nreceitaLocal)>>'
						Where
							prescstamp = '<<ALLTRIM(uCrsVerificaTodasRespostaEnvioOnline.prescstamp)>>'
												
						UPDATE 
							b_cli_prescl
						SET 
							enviado = 1
							,estado = '1'
							,estadodescr = 'Receita enviada e validada pelo processo Online'
							,prescno = '<<uCrsVerificaRespostaErrosEnvioOnline.nreceita>>'
							,data = '<<uf_gerais_getdate(uCrsVerificaRespostaErrosEnvioOnline.dataRec,"SQL")>>'
							,pin = '<<ALLTRIM(uCrsVerificaRespostaErrosEnvioOnline.pin)>>'
							,pinOpcao = '<<ALLTRIM(uCrsVerificaRespostaErrosEnvioOnline.pinOpcao)>>'
							,prescno2 = '<<uCrsVerificaRespostaErrosEnvioOnline.nreceita2>>'
							,pin2 = '<<ALLTRIM(uCrsVerificaRespostaErrosEnvioOnline.pin2)>>'
							,pinOpcao2 = '<<ALLTRIM(uCrsVerificaRespostaErrosEnvioOnline.pinOpcao2)>>'						
							,prescno3 = '<<uCrsVerificaRespostaErrosEnvioOnline.nreceita3>>'
							,pin3 = '<<ALLTRIM(uCrsVerificaRespostaErrosEnvioOnline.pin3)>>'
							,pinOpcao3 = '<<ALLTRIM(uCrsVerificaRespostaErrosEnvioOnline.pinOpcao3)>>'
							,nreclocal = '<<ALLTRIM(uCrsVerificaRespostaErrosEnvioOnline.nreceitaLocal)>>'	
						Where
							prescstamp = '<<ALLTRIM(uCrsVerificaTodasRespostaEnvioOnline.prescstamp)>>'
					ENDTEXT 
				
					IF !uf_gerais_actgrelha("", "uCrsUpdateEstadoOnline", lcSql)
						MESSAGEBOX("Ocorreu uma anomalia na grava��o em modo online. Contacte o suporte.",16,"LOGITOOLS SOFTWARE")
						regua(2)	
						prescricao.lockscreen = .f.

						prescricao.tmrEnvioOnline.enabled = .f.
						RETURN .f.
					ENDIF
				ELSE && N�o encontrou Mensagem de Receita registada com sucesso - Tem erros
					
		
					IF ALLTRIM(uCrsVerificaRespostaErrosEnvioOnline.codigo) != '503'

					** Marca nao validado pelo processo Online caso haja resposta (valida pelo c�digo do erro 503)
						TEXT TO lcSQL TEXTMERGE NOSHOW
							UPDATE 
								b_cli_presc
							SET 
								estado = '2'
								,estadodescr = 'Receita n�o validada pelo processo Online'
								--,nreclocal = '<<ALLTRIM(uCrsVerificaRespostaErrosEnvioOnline.nreceitaLocal)>>'
							Where
								prescstamp = '<<ALLTRIM(uCrsVerificaTodasRespostaEnvioOnline.prescstamp)>>'
							
							UPDATE 
								b_cli_prescl
							SET 
								estado = '2'
								,estadodescr = 'Receita n�o validada pelo processo Online'
								,nreclocal = '<<ALLTRIM(uCrsVerificaRespostaErrosEnvioOnline.nreceitaLocal)>>'
							Where
								prescstamp = '<<ALLTRIM(uCrsVerificaTodasRespostaEnvioOnline.prescstamp)>>'
						ENDTEXT 
						
						IF !uf_gerais_actgrelha("", "uCrsUpdateEstadoOffline", lcSql)
							MESSAGEBOX("Ocorreu uma anomalia na na valida��o em modo online. Contacte o suporte.",16,"LOGITOOLS SOFTWARE")
							regua(2)		
							prescricao.lockscreen = .f.
						
							prescricao.tmrEnvioOnline.enabled = .f.
							RETURN .f.
						ENDIF
						
						lcReceitasNaoValidadasOnline = .t.
					ENDIF
				ENDIF	
			ENDIF
		ENDSCAN	
		
		**Se n�o tiver dados nao apanho resposta n�o enviou
		IF USED("uCrsVerificaRespostaErrosEnvioOnline")
			IF RECCOUNT("uCrsVerificaRespostaErrosEnvioOnline") == 0
				lcReceitasNaoValidadasOnline = .f.
			ENDIF
		ENDIF
					
		IF lcReceitasNaoValidadasOnline == .t.
			uf_perguntalt_chama("Existem receitas que n�o foram registadas pelo processo online. Verifique o registo de comunica��es.","", "OK", 64)
		ELSE
			uf_perguntalt_chama("Receitas submetidas pelo processo online com sucesso.","OK", "", 64)
					
		ENDIF
	
		regua(2)		
		prescricao.lockscreen = .f.
	
		prescricao.tmrEnvioOnline.enabled = .f.
		prescricao.ntentativasOnline = 0
		prescricao.stampEnvioOnline = ''

	ENDIF
	
	IF !EMPTY(lcNrPrescricao)
		uf_prescricao_chama(ALLTRIM(lcNrPrescricao))
		&& temp preview das receitas. isto � mesmo preciso ??
		&& uf_prescricao_Prever()	
	ENDIF

ENDFUNC


** Refresca informa��o de envio de receitas
FUNCTION uf_prescricao_ActualizaRespostas

	SELECT * FROM ucrslinPresc WHERE !(LEFT(ALLTRIM(ucrsLinPresc.design),1) == ".") AND estado != "1" INTO CURSOR  uCrsVerificaTodasRespostaEnvioOnline READWRITE

	SELECT uCrsVerificaTodasRespostaEnvioOnline
	GO TOP 
	SCAN
		
		lcSucesso = .f.
		
		TEXT TO lcSql NOSHOW textmerge
			exec up_prescricao_ActualizaRespostasWebServiceMed <<IIF(EMPTY(ALLTRIM(uCrsVerificaTodasRespostaEnvioOnline.prescstamp)) or ISNULL(uCrsVerificaTodasRespostaEnvioOnline.prescstamp),'null',"'" + ALLTRIM(uCrsVerificaTodasRespostaEnvioOnline.prescstamp) +"'")>>
		ENDTEXT

		IF !uf_gerais_actGrelha("","uCrsVerificaRespostaErrosEnvioOnline",lcSql)
			uf_perguntalt_chama("Ocorreu um erro a verificar a resposta do envio da receita online. Por favor contacte o suporte.","OK","",16)
		ELSE		
			SELECT uCrsVerificaRespostaErrosEnvioOnline
			LOCATE FOR ALLTRIM(uCrsVerificaRespostaErrosEnvioOnline.Codigo) == "100002010001"
			IF FOUND() && Encontrou Mensagem de Receita registada com sucesso
				TEXT TO lcSQL TEXTMERGE NOSHOW
					UPDATE 
						b_cli_presc
					SET 
						enviado = 1
						,estado = '1'
						,estadodescr = 'Receita enviada e validada pelo processo Online'
						,prescno = '<<IIF(EMPTY(ALLTRIM(uCrsVerificaRespostaErrosEnvioOnline.nreceita)), ALLTRIM(uCrsVerificaTodasRespostaEnvioOnline.Prescno), ALLTRIM(uCrsVerificaRespostaErrosEnvioOnline.nreceita))>>'
						,data = '<<uf_gerais_getdate(uCrsVerificaRespostaErrosEnvioOnline.dataRec,"SQL")>>'
						,pin = '<<ALLTRIM(uCrsVerificaRespostaErrosEnvioOnline.pin)>>'
						,pinOpcao = '<<ALLTRIM(uCrsVerificaRespostaErrosEnvioOnline.pinOpcao)>>'
						,prescno2 = '<<IIF(EMPTY(ALLTRIM(uCrsVerificaRespostaErrosEnvioOnline.nreceita2)), ALLTRIM(uCrsVerificaTodasRespostaEnvioOnline.Prescno2), ALLTRIM(uCrsVerificaRespostaErrosEnvioOnline.nreceita2))>>'						
						,pin2 = '<<ALLTRIM(uCrsVerificaRespostaErrosEnvioOnline.pin2)>>'
						,pinOpcao2 = '<<ALLTRIM(uCrsVerificaRespostaErrosEnvioOnline.pinOpcao2)>>'						
						,prescno3 = '<<IIF(EMPTY(ALLTRIM(uCrsVerificaRespostaErrosEnvioOnline.nreceita3)), ALLTRIM(uCrsVerificaTodasRespostaEnvioOnline.Prescno3), ALLTRIM(uCrsVerificaRespostaErrosEnvioOnline.nreceita3))>>'						
						,pin3 = '<<ALLTRIM(uCrsVerificaRespostaErrosEnvioOnline.pin3)>>'
						,pinOpcao3 = '<<ALLTRIM(uCrsVerificaRespostaErrosEnvioOnline.pinOpcao3)>>'	
					Where
						prescstamp = '<<ALLTRIM(uCrsVerificaTodasRespostaEnvioOnline.prescstamp)>>'
					
					UPDATE 
						b_cli_prescl
					SET 
						enviado = 1
						,estado = '1'
						,estadodescr = 'Receita enviada e validada pelo processo Online'
						,prescno = '<<IIF(EMPTY(ALLTRIM(uCrsVerificaRespostaErrosEnvioOnline.nreceita)), ALLTRIM(uCrsVerificaTodasRespostaEnvioOnline.Prescno), ALLTRIM(uCrsVerificaRespostaErrosEnvioOnline.nreceita))>>'
						,data = '<<uf_gerais_getdate(uCrsVerificaRespostaErrosEnvioOnline.dataRec,"SQL")>>'
						,pin = '<<ALLTRIM(uCrsVerificaRespostaErrosEnvioOnline.pin)>>'
						,pinOpcao = '<<ALLTRIM(uCrsVerificaRespostaErrosEnvioOnline.pinOpcao)>>'
						,prescno2 = '<<IIF(EMPTY(ALLTRIM(uCrsVerificaRespostaErrosEnvioOnline.nreceita2)), ALLTRIM(uCrsVerificaTodasRespostaEnvioOnline.Prescno2), ALLTRIM(uCrsVerificaRespostaErrosEnvioOnline.nreceita2))>>'
						,pin2 = '<<ALLTRIM(uCrsVerificaRespostaErrosEnvioOnline.pin2)>>'
						,pinOpcao2 = '<<ALLTRIM(uCrsVerificaRespostaErrosEnvioOnline.pinOpcao2)>>'						
						,prescno3 = '<<IIF(EMPTY(ALLTRIM(uCrsVerificaRespostaErrosEnvioOnline.nreceita3)), ALLTRIM(uCrsVerificaTodasRespostaEnvioOnline.Prescno3), ALLTRIM(uCrsVerificaRespostaErrosEnvioOnline.nreceita3))>>'	
						,pin3 = '<<ALLTRIM(uCrsVerificaRespostaErrosEnvioOnline.pin3)>>'
						,pinOpcao3 = '<<ALLTRIM(uCrsVerificaRespostaErrosEnvioOnline.pinOpcao3)>>'	
					Where
						prescstamp = '<<ALLTRIM(uCrsVerificaTodasRespostaEnvioOnline.prescstamp)>>'
				ENDTEXT 
								
				IF !uf_gerais_actgrelha("", "uCrsUpdateEstadoOnline", lcSql)
					MESSAGEBOX("Ocorreu uma anomalia na grava��o em modo online. Contacte o suporte.",16,"LOGITOOLS SOFTWARE")
					prescricao.lockscreen = .f.
					regua(2)		
					prescricao.tmrEnvioOnline.enabled = .f.
					RETURN .f.
				ENDIF
		
			ENDIF	
		ENDIF
	ENDSCAN
ENDFUNC


**
FUNCTION uf_prescricao_tmrEnvioOffline
	LOCAL lcReceitasNaoValidadasOffline, lcNrPrescricao  
	lcReceitasNaoValidadasOffline = .f.
	lcNrPrescricao = ""
	
	IF USED("uCrsVerificaTodasRespostaEnvioOffline")
		fecha("uCrsVerificaTodasRespostaEnvioOffline")
	ENDIF
	TEXT TO lcSql NOSHOW textmerge
		exec up_prescricao_RespostaEnvioOnline null
	ENDTEXT
	IF !uf_gerais_actGrelha("","uCrsVerificaTodasRespostaEnvioOffline",lcSql)
		uf_perguntalt_chama("Ocorreu um erro a verificar a resposta do envio da receita offline. Por favor contacte o suporte.","OK","",16)
		RETURN .f.
	ENDIF	
 
	prescricao.ntentativasOffline = prescricao.ntentativasOffline + 1
	
	IF prescricao.ntentativasOffline > 5 && 5  tentativas de 3 segundos
		prescricao.tmrEnvioOffline.enabled = .f.
		regua(2)
		uf_perguntalt_chama("N�o foi poss�vel obter resposta pelo servi�o de prescri��o de receitas.","OK","",16)
		
		prescricao.ntentativasOffline = 0
		prescricao.stampEnvioOffline = ''
		RETURN .f.
	ENDIF
	regua(1,prescricao.ntentativasOffline + 2,"A enviar receita em modo online...")

	SELECT ucrsListaPrescEnviarOffline
	GO TOP
	lcNrPrescricao = ucrsListaPrescEnviarOffline.nrPrescricao
	SCAN

		TEXT TO lcSql NOSHOW textmerge
			exec up_prescricao_RespostaEnvioOnline <<IIF(EMPTY(ALLTRIM(ucrsListaPrescEnviarOffline.stampEnvioOnline)) or ISNULL(ucrsListaPrescEnviarOffline.stampEnvioOnline),'null',"'" + ALLTRIM(ucrsListaPrescEnviarOffline.stampEnvioOnline) +"'")>>
		ENDTEXT
		
		IF !uf_gerais_actGrelha("","uCrsVerificaRespostaEnvioOffline",lcSql)
			regua(2)
			uf_perguntalt_chama("Ocorreu um erro a verificar a resposta do envio da receita online. Por favor contacte o suporte.","OK","",16)
			prescricao.tmrEnvioOffline.enabled = .f.
			prescricao.ntentativasOffline = 0
			prescricao.stampEnvioOffline = ''
		ELSE
					
			SELECT uCrsVerificaTodasRespostaEnvioOffline
			APPEND FROM DBF("uCrsVerificaRespostaEnvioOffline")
			
		ENDIF
	ENDSCAN
	
	SELECT * FROM ucrsListaPrescEnviarOffline LEFT JOIN uCrsVerificaTodasRespostaEnvioOffline ON ucrsListaPrescEnviarOffline.prescstamp = uCrsVerificaTodasRespostaEnvioOffline.prescstamp ;
	WHERE uCrsVerificaTodasRespostaEnvioOffline.prescstamp = .NULL. INTO CURSOR uCrsVerificaTodasRespostaEnvioOfflineValida READWRITE 
	SELECT uCrsVerificaTodasRespostaEnvioOfflineValida 
	
	IF RECCOUNT("uCrsVerificaTodasRespostaEnvioOfflineValida") == 0 AND RECCOUNT("uCrsVerificaRespostaEnvioOffline") != 0
		lcTemTodasRespostas = .t.
	ELSE
		lcTemTodasRespostas = .f.
	ENDIF
	
	IF lcTemTodasRespostas == .t.	

		**Verfica se contem Erros
		SELECT uCrsVerificaTodasRespostaEnvioOffline
		GO Top
		SCAN
			
			lcSucesso = .f.
			
			TEXT TO lcSql NOSHOW textmerge
				exec up_prescricao_RespostaErrosEnvioOnline <<IIF(EMPTY(ALLTRIM(uCrsVerificaTodasRespostaEnvioOffline.stamp)) or ISNULL(uCrsVerificaTodasRespostaEnvioOffline.stamp),'null',"'" + ALLTRIM(uCrsVerificaTodasRespostaEnvioOffline.stamp) +"'")>>
			ENDTEXT

			IF !uf_gerais_actGrelha("","uCrsVerificaRespostaErrosEnvioOffline",lcSql)
				uf_perguntalt_chama("Ocorreu um erro a verificar a resposta do envio da receita offline. Por favor contacte o suporte.","OK","",16)
			ELSE		
				SELECT uCrsVerificaRespostaErrosEnvioOffline
				LOCATE FOR ALLTRIM(uCrsVerificaRespostaErrosEnvioOffline.Codigo) == "100002010001" OR ALLTRIM(uCrsVerificaRespostaErrosEnvioOffline.Codigo) == "201101000"
				IF FOUND() && Encontrou Mensagem de Receita registada com sucesso
					TEXT TO lcSQL TEXTMERGE NOSHOW
						UPDATE 
							b_cli_presc
						SET 
							enviado = 1
						Where
							prescstamp = '<<ALLTRIM(uCrsVerificaTodasRespostaEnvioOffline.prescstamp)>>'
						
						UPDATE 
							b_cli_prescl
						SET 
							enviado = 1
						Where
							prescstamp = '<<ALLTRIM(uCrsVerificaTodasRespostaEnvioOffline.prescstamp)>>'
					ENDTEXT 
			
					IF !uf_gerais_actgrelha("", "uCrsUpdateEstadoOffline", lcSql)
						MESSAGEBOX("Ocorreu uma anomalia na grava��o em modo offline. Contacte o suporte.",16,"LOGITOOLS SOFTWARE")
						regua(2)		
						prescricao.tmrEnvioOffline.enabled = .f.
						RETURN .f.
					ENDIF
				ELSE && N�o encontrou Mensagem de Receita registada com sucesso - Tem erros
					lcReceitasNaoValidadasOffline = .t.
				ENDIF	
			ENDIF
		ENDSCAN		

		**Se n�o tiver dados nao apanho resposta n�o enviou
		IF USED("uCrsVerificaRespostaErrosEnvioOffline")
			IF RECCOUNT("uCrsVerificaRespostaErrosEnvioOffline") == 0
				lcReceitasNaoValidadasOffline = .f.
			ENDIF
		ENDIF
					
		IF lcReceitasNaoValidadasOffline == .t.
			uf_perguntalt_chama("Existem receitas que n�o foram registadas pelo processo offline. Verifique o registo de comunica��es.","", "OK", 64)
		ELSE
			uf_perguntalt_chama("Receitas submetidas pelo processo offline com sucesso.","OK", "", 64)
		ENDIF
	
		regua(2)		
		prescricao.tmrEnvioOffline.enabled = .f.
		prescricao.ntentativasOffline = 0
		prescricao.stampEnvioOffline = ''

	ENDIF

	IF !EMPTY(lcNrPrescricao)
		uf_prescricao_chama(ALLTRIM(lcNrPrescricao))
	ENDIF

ENDFUNC


**
FUNCTION uf_prescricao_dynamicForColorEstado
	SELECT ucrsLinPresc
	DO CASE 
		CASE ucrsLinPresc.estado == "0"
			prescricao.pageframe1.page2.pageframeReceitaMedicamentos.page1.esTADODESCR.ForeColor = RGB(0,0,0)
		CASE ucrsLinPresc.estado == "1"		
			prescricao.pageframe1.page2.pageframeReceitaMedicamentos.page1.esTADODESCR.ForeColor = RGB(0,128,64)		
		CASE ucrsLinPresc.estado == "2"
			prescricao.pageframe1.page2.pageframeReceitaMedicamentos.page1.esTADODESCR.ForeColor = RGB(255,0,0)
		CASE ucrsLinPresc.estado == "3"
			prescricao.pageframe1.page2.pageframeReceitaMedicamentos.page1.esTADODESCR.ForeColor = RGB(0,162,232)	
		OTHERWISE
			**
	ENDCASE
ENDFUNC




** Fun��o de inser��o na BD
** Modo online, pode gravar, o numero de receita � devolvido pelo Webservice
** Modo offline � nessario gerar o numero de receita
FUNCTION uf_prescricao_insertCabPresc
	LOCAL lcSQL, lcNrReceitaLocal
	Set Point To "."
	
	&& Calcula o nr de Receita Local aki
	IF !EMPTY(myPrescMaterializada)
		lcNrReceitaLocal = uf_prescricao_geranrReceitaLocal(IIF(!EMPTY(ucrslinPresc.renovavel), '02', '01'))
	ELSE
		lcNrReceitaLocal = uf_prescricao_geranrReceitaLocal('00')	
	ENDIF
	
	&&Gera numero local mcdt
	IF  !EMPTY(ucrsLinPresc.receitamcdt) 
		
		lcNrReceitaLocal = uf_prescricao_atribuiNumeracaoMCDT()
		
	ENDIF
	
	
	&&
	SELECT uCrsCabPresc
	SELECT ucrsLinPresc
	lcSQL = ""
	

	TEXT TO lcSQL NOSHOW textmerge
		INSERT INTO b_cli_presc 
		(	
			prescstamp
			,prescno
			,data
			,no
			,estab
			,nome
			,nome2
			,morada
			,local
			,codpost
			,telefone
			,telemovel
			,email
			,ncont
			,nrutente
			,nrbenef
			,sexo
			,nacionalidade
			,naturalidade
			,tipo
			,dtnasc
			,prescano
			,utstamp
			,drstamp
			,drnome
			,drno
			,drespecialidade
			,drtratamento
			,drnacionalidade
			,drnascimento
			,drsexo
			,drmorada
			,drlocal
			,drcodpost
			,drtelefone
			,drtelemovel
			,dremail
			,drcedula
			,total
			,codigoComp1
			,designComp1
			,codigoComp2
			,designComp2
			,entidade
			,entidadeDescr
			,entidadeCod
			,validade
			,obs
			,localPresc
			,vias
			,u_codigop
			,u_descp
			,RECMtipo
			,RECMcodigo
			,RECMdescricao
			,RECMdataInicio
			,RECMdataFim
			,BENEFcodigo
			,BENEFdescricao
			,BENEFdataInicio
			,BENEFdataFim
			,mcdt_domicilio
			,mcdt_urgente
			,mcdt_justificacaoD
			,mcdt_justificacaoU
			,mcdt_infoclinica
			,mcdt_outrasInfo
			,mcdt_isento
			,ousrinis
			,ousrdata
			,ousrhora
			,usrinis
			,usrdata
			,usrhora
			,marcada
			,drinscricao
			,drordem
			,renovavel
			,TipoReceita
			,receitamcdt
			,estado
			,estadoDescr
			,nrprescricao
			,terminal
			,site
			,nreclocal
			,nprescsns
		
		) 
		VALUES(
			'<<Alltrim(ucrsLinPresc.prescstamp)>>'
			,'<<ALLTRIM(ucrsLinPresc.prescno)>>'
			,'<<uf_gerais_getdate(ucrsLinPresc.data,"SQL")>>'
			,<<ucrsCabPresc.no>>
			,<<ucrsCabPresc.estab>>
			,'<<Alltrim(ucrsCabPresc.nome)>>'
			,'<<Alltrim(ucrsCabPresc.nome2)>>'
			,'<<Alltrim(ucrsCabPresc.morada)>>'
			,'<<Alltrim(ucrsCabPresc.local)>>'
			,'<<Alltrim(ucrsCabPresc.codpost)>>'
			,'<<Alltrim(ucrsCabPresc.telefone)>>'
			,'<<Alltrim(ucrsCabPresc.telemovel)>>'
			,'<<Alltrim(ucrsCabPresc.email)>>'
			,'<<Alltrim(ucrsCabPresc.ncont)>>'
			,'<<Alltrim(ucrsCabPresc.nrutente)>>'
			,'<<Alltrim(ucrsCabPresc.nrbenef)>>'
			,'<<ucrsCabPresc.sexo>>'
			,'<<Alltrim(ucrsCabPresc.nacionalidade)>>'
			,'<<ALLTRIM(ucrsCabPresc.naturalidade)>>'
			,'<<Alltrim(ucrsCabPresc.tipo)>>'
			,'<<uf_gerais_getdate(ucrsCabPresc.dtnasc,"SQL")>>'
			,<<ucrsCabPresc.prescano>>
			,'<<ALLTRIM(ucrsCabPresc.utstamp)>>'
			,'<<Alltrim(ucrsCabPresc.drstamp)>>'
			,'<<ALLTRIM(ucrsCabPresc.drnome)>>'
			,<<ucrsCabPresc.drno>>
			,'<<ALLTRIM(ucrsCabPresc.drespecialidade)>>'
			,'<<ALLTRIM(ucrsCabPresc.drtratamento)>>'
			,'<<ALLTRIM(ucrsCabPresc.drnacionalidade)>>'
			,'<<uf_gerais_getdate(ucrsCabPresc.drnascimento,"SQL")>>'
			,<<ucrsCabPresc.drsexo>>
			,'<<ALLTRIM(ucrsCabPresc.drmorada)>>'
			,'<<ALLTRIM(ucrsCabPresc.drlocal)>>'
			,'<<ALLTRIM(ucrsCabPresc.drcodpost)>>'
			,'<<ALLTRIM(ucrsCabPresc.drtelefone)>>'
			,'<<ALLTRIM(ucrsCabPresc.drtelemovel)>>'
			,'<<ALLTRIM(ucrsCabPresc.dremail)>>'
			,'<<ALLTRIM(ucrsCabPresc.drcedula)>>'
			,<<ucrsCabPresc.total>>
			,'<<ALLTRIM(ucrsCabPresc.codigoComp1)>>'
			,'<<ALLTRIM(ucrsCabPresc.designComp1)>>'
			,'<<ALLTRIM(ucrsCabPresc.codigoComp2)>>'
			,'<<ALLTRIM(ucrsCabPresc.designComp2)>>'
			,'<<ALLTRIM(ucrsCabPresc.entidade)>>'
			,'<<ALLTRIM(ucrsCabPresc.entidadeDescr)>>'
			,'<<ALLTRIM(ucrsCabPresc.entidadeCod)>>'
			,'<<uf_gerais_getdate(ucrsCabPresc.validade,"SQL")>>'
			,'<<ALLTRIM(ucrsCabPresc.obs)>>'
			,'<<ALLTRIM(ucrsCabPresc.localPresc)>>'
			,<<ucrsLinPresc.vias>>
			,'<<ALLTRIM(ucrsCabPresc.u_codigop)>>'
			,'<<ALLTRIM(ucrsCabPresc.u_descp)>>'
			,'<<ALLTRIM(ucrsCabPresc.RECMtipo)>>'
			,'<<ALLTRIM(ucrsCabPresc.RECMcodigo)>>'
			,'<<ALLTRIM(ucrsCabPresc.RECMdescricao)>>', 
			'<<IIF(empty(ucrsCabPresc.RECMdataInicio),'19000101',uf_gerais_getdate(ucrsCabPresc.RECMdataInicio,"SQL"))>>'
			,'<<IIF(empty(ucrsCabPresc.RECMdataFim),'19000101',uf_gerais_getdate(ucrsCabPresc.RECMdataFim,"SQL"))>>'
			,'<<ALLTRIM(ucrsCabPresc.BENEFcodigo)>>'
			,'<<ALLTRIM(ucrsCabPresc.BENEFdescricao)>>'
			,'<<IIF(empty(ucrsCabPresc.BENEFdataInicio), '19000101', uf_gerais_getdate(ucrsCabPresc.BENEFdataInicio,"SQL"))>>'
			,'<<IIF(empty(ucrsCabPresc.BENEFdataFim), '19000101', uf_gerais_getdate(ucrsCabPresc.BENEFdataFim,"SQL"))>>'
			,<<IIF(ucrsLinPresc.mcdt_domicilio,1,0)>>
			,<<IIF(ucrsLinPresc.mcdt_urgente,1,0)>>
			,'<<ALLTRIM(ucrsLinPresc.mcdt_justificacaoD)>>'
			,'<<ALLTRIM(ucrsLinPresc.mcdt_justificacaoU)>>'
			,'<<ALLTRIM(ucrsLinPresc.mcdt_infoclinica)>>'
			,'<<ALLTRIM(ucrsLinPresc.mcdt_outrasInfo)>>'
			,<<IIF(ucrsCabPresc.mcdt_isento,1,0)>>
			,'<<m.m_chinis>>'
			,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
			,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8)
			,'<<m.m_chinis>>'
			, convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
			, convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8)
			,<<IIF(ucrsCabPresc.marcada,1,0)>>
			,<<ucrsCabPresc.drinscricao>>
			,'<<ALLTRIM(ucrsCabPresc.drordem)>>'
			,<<IIF(ucrsLinPresc.renovavel,1,0)>>
			,'<<IIF(LEFT(ALLTRIM(ucrsLinPresc.TipoReceita),1) == "L", 'RSP',ALLTRIM(ucrsLinPresc.TipoReceita))>>'
			,<<IIF(ucrsLinPresc.receitamcdt == .t.,1,0)>>
			,'<<ALLTRIM(ucrsLinPresc.estado)>>'
			,'<<ALLTRIM(ucrsLinPresc.estadodescr)>>'
			,'<<ALLTRIM(ucrsLinPresc.nrprescricao)>>'
			,<<mytermno>>
			,'<<mysite>>'
			,'<<ALLTRIM(lcNrReceitaLocal))>>'
			,<<IIF(ucrsCabPresc.nprescsns,1,0)>>

		)
	ENDTEXT


	
	IF !uf_gerais_actgrelha("", "uCrsInsereCabPresc", lcSql)
		uf_perguntalt_chama("Ocorreu uma anomalia ao gravar a receita. Por favor contacte o Suporte.","OK","", 16)
		RETURN .f.	
	ENDIF
	
	RETURN .t.
ENDFUNC


**
FUNCTION uf_prescricao_apagaLinPresc
	LPARAMETERS lcNrPrescricao
	
	** Apaga as linhas existentes e introduz todas as linhas da receita
	SELECT uCrsCabPresc
	SELECT ucrsLinPresc

	lcSQL = ""
	TEXT TO lcSQL NOSHOW textmerge

		DELETE 
			B_cli_Resposta
		FROM
			B_cli_Resposta
			inner join b_cli_presc on B_cli_Resposta.PrescStamp = b_cli_presc.prescstamp
		Where 
			b_cli_presc.nrPrescricao = '<<Alltrim(lcNrPrescricao)>>'
			AND b_cli_presc.estado != '1' 
			AND b_cli_presc.estado != '3'
	
		DELETE 
			b_cli_prescl 
		FROM
			b_cli_prescl 
		Where 
			b_cli_prescl.nrPrescricao = '<<Alltrim(lcNrPrescricao)>>' 
			AND b_cli_prescl.estado != '1' 
			AND b_cli_prescl.estado != '3'
			
		DELETE 
			b_cli_presc 
		FROM
			b_cli_presc 
		Where 
			b_cli_presc.nrPrescricao = '<<Alltrim(lcNrPrescricao)>>' 
			AND b_cli_presc.estado != '1' 
			AND b_cli_presc.estado != '3'
	ENDTEXT
	
	IF !uf_gerais_actgrelha("", "uCrsUpdateLinPresc", lcSql)
		MESSAGEBOX("Ocorreu uma anomalia na grava��o das linhas de receita. Contacte o suporte.",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF
ENDFUNC


**
FUNCTION uf_prescricao_insertLinPresc
	LOCAL lcSQL
	Set Point To "."

	Select uCrsCabPresc

	SELECT ucrsLinPresc
	lcSQL = ""
	TEXT TO lcSQL NOSHOW textmerge
		INSERT INTO b_cli_prescl 
		(	prescstamp
			,presclstamp
			,stampEnvioOnline
			,prescno
			,data
			,tiporeceita
			,med_id
			,ref
			,design
			,qtt
			,epv
			,unidade
			,etiliquido
			,u_epref
			,u_ettent1
			,u_ettent2
			,diploma
			,diplomacod
			,u_epvp
			,posologia
			,estadocomercial
			,dci
			,codGrupoH
			,designGrupoH
			,faminome
			,familia
			,fabricantenome
			,fabricanteno
			,pgenerico
			,generico
			,psico
			,benzo
			,lordem
			,ousrinis
			,ousrdata
			,ousrhora
			,usrinis
			,usrdata
			,usrhora
			,marcada
			,protocolo
			,manipulado
			,tipembdescr
			,dadosmanipulados
			,grupo
			,pmu
			,taxa_compart
			,utente
			,psns
			,dif_utente
			,dif_sns
			,mostraAoUtente
			,mcdt_codsns
			,mcdt_codconv
			,mcdt_area
			,mcdt_areadescr
			,mcdt_grupo
			,mcdt_subgrupo
			,mcdt_nomedescr
			,mcdt_pvp
			,mcdt_taxmod
			,mcdt_sinonimo
			,mcdt_qtt
			,mcdt_domicilio
			,mcdt_urgente
			,mcdt_justificacaoD
			,mcdt_justificacaoU
			,mcdt_infoclinica
			,mcdt_outrasInfo
			,mcdt_local
			,pordci
			,tipoduracao
			,mcdt_codProduto
			,mcdt_descProduto
			,mcdt_codLateral
			,mcdt_descLateral
			,estadoDescr
			,estado
			,nrprescricao
			,terminal
			,posologiasugerida
			,renovavel
			,vias
			,validade
			,prescNomeCod
			,prescNomeDescr
			,anulamotivoCod
			,anulamotivoDesc
			,anuladata
			,anulado
			,receitamcdt
			,pos
			,cnpem
			,site
			,nreclocal
			,comp_sns
			,comp_snsRe
			,comp_diploma
			,mcdt_nAmostras
			,mcdt_requerAutorizacao
			,mcdt_internalizar						
			,mcdt_episodioID
			,mcdt_emissaoExterna
			,mcdt_notificacaoRequisicaoID
			,mcdt_notificacaoRequisicaoDesc
			,prescEmbCod
			,prescEmbDescr
			,prescDateCod
			,prescDateDescr
			,duracaoValor
			,duracaoUnidade
			,frequenciaValor
			,frequenciaUnidade
			,quantidadeValor
			,quantidadeUnidade
			,posologiaNova
		
		) 
		VALUES(
			'<<Alltrim(ucrsLinPresc.prescstamp)>>'
			,'<<Alltrim(ucrsLinPresc.presclstamp)>>'
			,'<<Alltrim(ucrsLinPresc.stampEnvioOnline)>>'
			,'<<ALLTRIM(ucrsLinPresc.prescno)>>'
			,'<<uf_gerais_getdate(ucrsLinPresc.data,"SQL")>>'
			,'<<ALLTRIM(ucrsLinPresc.tiporeceita)>>'
			,'<<ucrsLinPresc.med_id>>'
			,'<<ALLTRIM(ucrsLinPresc.ref)>>'
			,'<<STRTRAN(STRTRAN(ALLTRIM(strtran(ucrsLinPresc.design,chr(39),'')), "[" + ALLTRIM(ucrsLinPresc.estadodescr) + "]" , ""),"[Anulado]","")>>'
			,<<ucrsLinPresc.qtt>>
			,<<ucrsLinPresc.epv>>
			,'<<ALLTRIM(ucrsLinPresc.unidade)>>'
			,<<ucrsLinPresc.etiliquido>>
			,<<ucrsLinPresc.u_epref>>
			,<<ucrsLinPresc.u_ettent1>>
			,<<ucrsLinPresc.u_ettent2>>
			,'<<ALLTRIM(ucrsLinPresc.diploma)>>'
			,'<<ALLTRIM(ucrsLinPresc.diplomacod)>>'
			,<<ucrsLinPresc.u_epvp>>
			,'<<ALLTRIM(ucrsLinPresc.posologia)>>'
			,'<<ALLTRIM(ucrsLinPresc.estadocomercial)>>'
			,'<<ALLTRIM(ucrsLinPresc.dci)>>'
			,'<<ALLTRIM(ucrsLinPresc.codGrupoH)>>'
			,'<<ALLTRIM(ucrsLinPresc.designGrupoH)>>'
			,'<<ALLTRIM(ucrsLinPresc.faminome)>>'
			,'<<ucrsLinPresc.familia>>'
			,'<<ALLTRIM(ucrsLinPresc.fabricantenome)>>'
			,<<ucrsLinPresc.fabricanteno>>
			,<<ucrsLinPresc.pgenerico>>
			,<<IIF(ucrsLinPresc.generico,1,0)>>
			,<<IIF(ucrsLinPresc.psico,1,0)>>
			,<<IIF(ucrsLinPresc.benzo,1,0)>>
			,<<ucrsLinPresc.lordem>>
			,'<<m.m_chinis>>'
			,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
			,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8)
			,'<<m.m_chinis>>'
			, convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
			,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8)
			,<<IIF(ucrsLinPresc.marcada,1,0)>>
			,<<IIF(ucrsLinPresc.protocolo==.t.,1,0)>>
			,<<IIF(ucrsLinPresc.manipulado,1,0)>>
			,'<<ALLTRIM(ucrsLinPresc.tipembdescr)>>'
			,'<<ALLTRIM(ucrsLinPresc.dadosmanipulados)>>'
			,'<<ALLTRIM(ucrsLinPresc.grupo)>>'
			,<<ucrsLinPresc.pmu>>
			,<<ucrsLinPresc.taxa_compart>>
			,<<ucrsLinPresc.utente>>
			,<<ucrsLinPresc.psns>>
			,<<ucrsLinPresc.dif_utente>>
			,<<ucrsLinPresc.dif_sns>>
			,<<IIF(ucrsLinPresc.mostraAoUtente,1,0)>>
			,'<<ALLTRIM(ucrsLinPresc.mcdt_codsns)>>'
			,'<<ALLTRIM(ucrsLinPresc.mcdt_codconv)>>'
			,'<<ALLTRIM(ucrsLinPresc.mcdt_area)>>'
			,'<<ALLTRIM(ucrsLinPresc.mcdt_areadescr)>>'
			,'<<ALLTRIM(ucrsLinPresc.mcdt_grupo)>>'
			,'<<ALLTRIM(ucrsLinPresc.mcdt_subgrupo)>>'
			,'<<ALLTRIM(strtran(ucrsLinPresc.mcdt_nomedescr,chr(39),''))>>'
			,<<ucrsLinPresc.mcdt_pvp>>
			,<<ucrsLinPresc.mcdt_taxmod>>
			,'<<ucrsLinPresc.mcdt_sinonimo>>'
			,<<ucrsLinPresc.mcdt_qtt>>
			,<<IIF(ucrsLinPresc.mcdt_domicilio,1,0)>>
			,<<IIF(ucrsLinPresc.mcdt_urgente,1,0)>>
			,'<<ALLTRIM(ucrsLinPresc.mcdt_justificacaoD)>>'
			,'<<ALLTRIM(ucrsLinPresc.mcdt_justificacaoU)>>'
			,'<<ALLTRIM(ucrsLinPresc.mcdt_infoclinica)>>'
			,'<<ALLTRIM(ucrsLinPresc.mcdt_outrasInfo)>>'
			,'<<ALLTRIM(ucrsLinPresc.mcdt_local)>>'
			,<<IIF(ucrsLinPresc.pordci,1,0)>>
			,'<<ALLTRIM(ucrsLinPresc.tipoduracao)>>'
			,'<<ucrsLinPresc.mcdt_codProduto>>'
			,'<<ALLTRIM(ucrsLinPresc.mcdt_descProduto)>>'
			,'<<ucrsLinPresc.mcdt_codLateral>>'
			,'<<ALLTRIM(ucrsLinPresc.mcdt_descLateral)>>'
			,'<<ALLTRIM(ucrsLinPresc.estadodescr)>>'
			,'<<ALLTRIM(ucrsLinPresc.estado)>>'
			,'<<ALLTRIM(ucrsLinPresc.nrprescricao)>>'
			,<<ucrsLinPresc.terminal>>
			,'<<ucrsLinPresc.posologiasugerida>>'
			,<<IIF(ucrsLinPresc.renovavel,1,0)>>
			,<<ucrsLinPresc.vias>>
			,'<<uf_gerais_getdate(ucrsLinPresc.validade,"SQL")>>'
			,'<<ALLTRIM(ucrsLinPresc.prescNomeCod)>>'
			,'<<ALLTRIM(ucrsLinPresc.prescNomeDescr)>>'
			,'<<ALLTRIM(ucrsLinPresc.anulamotivoCod)>>'
			,'<<ALLTRIM(ucrsLinPresc.anulamotivoDesc)>>'
			,'<<uf_gerais_getdate(ucrsLinPresc.anuladata,"SQL")>>'
			,<<IIF(ucrsLinPresc.anulado,1,0)>>
			,<<IIF(ucrsLinPresc.receitamcdt,1,0)>>
			,<<ucrsLinPresc.pos>>
			,'<<ALLTRIM(ucrsLinPresc.cnpem)>>'
			,'<<ALLTRIM(mysite)>>'
			,'<<LEFT(ALLTRIM(ucrsLinPresc.prescstamp),20)>>'
			,<<ucrsLinPresc.comp_sns>>
			,<<ucrsLinPresc.comp_snsRe>>
			,<<ucrsLinPresc.comp_diploma>>
			,<<ucrsLinPresc.mcdt_nAmostras>>
			,<<IIF(ucrsLinPresc.mcdt_requerAutorizacao,1,0)>>
			,<<IIF(ucrsLinPresc.mcdt_internalizar,1,0)>>								
			,<<0>>
			,<<1>>
			,<<ucrsLinPresc.mcdt_notificacaoRequisicaoID >>
			,'<<ALLTRIM(ucrsLinPresc.mcdt_notificacaoRequisicaoDesc))>>'
			,'<<ALLTRIM(ucrsLinPresc.prescEmbCod)>>'
			,'<<ALLTRIM(ucrsLinPresc.prescEmbDescr)>>'
			,'<<ALLTRIM(ucrsLinPresc.prescDateCod)>>'
			,'<<ALLTRIM(ucrsLinPresc.prescDateDescr)>>'			
			,'<<ALLTRIM(ucrsLinPresc.duracaoValor)>>'
			,'<<ALLTRIM(ucrsLinPresc.duracaoUnidade)>>'
			,'<<ALLTRIM(ucrsLinPresc.frequenciaValor)>>'
			,'<<ALLTRIM(ucrsLinPresc.frequenciaUnidade)>>'
			,'<<ALLTRIM(ucrsLinPresc.quantidadeValor)>>'
			,'<<ALLTRIM(ucrsLinPresc.quantidadeUnidade)>>'
			, <<1>>
		)
	ENDTEXT

	IF !uf_gerais_actgrelha("", "uCrsInsereLinPresc", lcSQL)
		MESSAGEBOX("Ocorreu uma anomalia na grava��o das Linhas de receita. Contacte o suporte.",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF

	RETURN .t.
ENDFUNC


**
FUNCTION uf_prescricao_selecionaPg
	LPARAMETERS lcOpcao
	
	**Sempre que h� uma acao expande o detalhe
	IF Prescricao.detalhesVisiveis == .t.
		uf_prescricao_expandeDetalhe()
	ENDIF
	
	
	DO CASE
		CASE UPPER(ALLTRIM(lcOpcao)) == "UTENTE"
			Prescricao.PageFrame1.ActivePage = 1
			Prescricao.PageFrame1.Page1.PageFrameUtentes.ActivePage = 1
		
		CASE UPPER(ALLTRIM(lcOpcao)) == "LINHAS"
			
			SELECT ucrsLinPresc
			
			
			DO CASE 
				CASE Left(ALLTRIM(ucrsLinPresc.design),1)=='.' AND ALLTRIM(UPPER(ucrsLinPresc.tipoReceita)) != "MCDT" && Cabe�alho
					Prescricao.PageFrame1.ActivePage = 2
				
				CASE !Left(ALLTRIM(ucrsLinPresc.design),1)=='.' AND ALLTRIM(UPPER(ucrsLinPresc.tipoReceita)) != "MCDT" && Medicamento
					Prescricao.PageFrame1.ActivePage = 3
					
					DO CASE 
						CASE ALLTRIM(UPPER(ucrsLinPresc.tipoReceita)) == "RN" OR ALLTRIM(UPPER(ucrsLinPresc.tipoReceita)) == "LN"
							Prescricao.pageframe1.page3.pageframeMedicamentos.ActivePage = 1
						CASE ALLTRIM(UPPER(ucrsLinPresc.tipoReceita)) == "MM" OR ALLTRIM(UPPER(ucrsLinPresc.tipoReceita)) == "LMM"
							Prescricao.pageframe1.page3.pageframeMedicamentos.ActivePage = 6
						CASE ALLTRIM(UPPER(ucrsLinPresc.tipoReceita)) == "MDT" OR ALLTRIM(UPPER(ucrsLinPresc.tipoReceita)) == "LMDT"
							Prescricao.pageframe1.page3.pageframeMedicamentos.ActivePage = 7
						CASE ALLTRIM(UPPER(ucrsLinPresc.tipoReceita)) == "OUT" OR ALLTRIM(UPPER(ucrsLinPresc.tipoReceita)) == "LOUT"	
							Prescricao.pageframe1.page3.pageframeMedicamentos.ActivePage = 8
						
					ENDCASE
				
				CASE !EMPTY(ALLTRIM(ucrsLinPresc.mcdt_codconv)) OR !EMPTY(ALLTRIM(ucrsLinPresc.mcdt_codsns)) && MCDT
					Prescricao.PageFrame1.ActivePage = 4
			
				CASE Left(ucrsLinPresc.design,1)=='.' AND ALLTRIM(UPPER(ucrsLinPresc.tipoReceita)) == "MCDT"  && Cabe�alho
					Prescricao.PageFrame1.ActivePage = 5
			ENDCASE

		OTHERWISE 
			**
	ENDCASE 
	

ENDFUNC


**
FUNCTION uf_prescricao_actualizaLocalizacao
	LOCAL lcCaption 
	lcCaption = ""
	
	DO CASE
		CASE Prescricao.PageFrame1.ActivePage == 1 && Utentes
			WITH Prescricao.PageFrame1.Page1.PageFrameUtentes
				DO CASE	
					CASE .ActivePage = 1
						lcCaption = "Utente > Dados"
						.page1.refresh
					CASE .ActivePage = 2
						lcCaption = "Utente > Benef�cios"
						.page2.refresh
					CASE .ActivePage = 3
						lcCaption = "Utente > Diplomas"		
						.page3.refresh		
					CASE .ActivePage = 4
						lcCaption = "Utente > Patologias"	
						.page4.refresh
					CASE .ActivePage = 5
						lcCaption = "Utente > Alergias"	
						.page5.refresh
					CASE .ActivePage = 6
						lcCaption = "Utente > Interac��es"	
						.page6.refresh
					CASE .ActivePage = 7
						lcCaption = "Utente > Contra Indica��es"		
						.page7.refresh	
					CASE .ActivePage = 8
						lcCaption = "Utente > Hist�rico MCDTs"		
						.page8.refresh	
					CASE .ActivePage = 9
						lcCaption = "Utente > Hist�rico Medicamentos"		
						.page9.refresh					
					OTHERWISE 
						**
				ENDCASE 
			ENDWITH	
		CASE Prescricao.PageFrame1.ActivePage == 2
			WITH Prescricao.pageframe1.page2.pageframeReceitaMedicamentos
				DO CASE	
					CASE .ActivePage = 1
						.page1.refresh
						lcCaption = "Receita de Medicamentos > Dados"
					CASE .ActivePage = 2
						.page2.refresh
						lcCaption = "Receita de Medicamentos > Renov�vel"		
					CASE .ActivePage = 3
						.page3.refresh
						lcCaption = "Receita de Medicamentos > Registo de Comunica��es"					
					OTHERWISE 
						**
				ENDCASE 
			ENDWITH
		CASE Prescricao.PageFrame1.ActivePage == 3
			WITH Prescricao.PageFrame1.Page3.PageFrameMedicamentos
				DO CASE	
					CASE .ActivePage = 1
						lcCaption = "Medicamento > Dados"
						.page1.refresh
					CASE .ActivePage = 2
						lcCaption = "Medicamento > Posologia"
						.page2.refresh
					CASE .ActivePage = 3
						lcCaption = "Medicamento > Mais Baratos"
						.page3.refresh
					CASE .ActivePage = 4
						lcCaption = "Medicamento > Encargos"
						.page4.refresh
					CASE .ActivePage = 5
						lcCaption = "Medicamento > Gen�ricos Alternativos"
						.page5.refresh
					CASE .ActivePage = 6
						lcCaption = "Medicamento > Manipulado"
						.page6.refresh
					CASE .ActivePage = 7
						lcCaption = "Medicamento > G�neros Aliment�cios"
						.page7.refresh
					CASE .ActivePage = 8
						lcCaption = "Medicamento > Outros Produtos"
						.page8.refresh
					OTHERWISE 
						**
				ENDCASE 
			ENDWITH	
		CASE Prescricao.PageFrame1.ActivePage == 4
			WITH Prescricao.PageFrame1.Page4.PageFrameMCDT
				DO CASE	
					CASE .ActivePage = 1
						lcCaption = "MCDT > Dados"	
						.page1.refresh	
					OTHERWISE 
						**
				ENDCASE 
			ENDWITH
		CASE Prescricao.PageFrame1.ActivePage == 5
			WITH Prescricao.PageFrame1.Page5.PageFrameReceitaMCDT
				DO CASE	
					CASE .ActivePage = 1
						lcCaption = "MCDT > Dados"
						.page1.refresh
					CASE .ActivePage = 2
						lcCaption = "MCDT > Registo de Comunica��es"
						.page2.refresh				
					OTHERWISE 
						**
				ENDCASE 
			ENDWITH
		OTHERWISE 
		OTHERWISE 
				**
	ENDCASE 

	Prescricao.containerTotal.lblCaminho.Caption = lcCaption

ENDFUNC 


**
FUNCTION uf_prescricao_activaModoOffline
	** menu1
	Prescricao.menu1.estado("pesquisar, ultimo, novo, anular, actualizar, anterior, seguinte", "HIDE", "Gravar Offline", .t., "Cancelar", .t.)
	Prescricao.menu1.estado("novaReceita, novoMed, mcdt", "SHOW")
*!*		** menu_opcoes
*!*		Prescricao.menu_opcoes.estado("imprimir", "HIDE")
ENDFUNC



** 
FUNCTION uf_prescricao_ValidaEspecialista
	LOCAL lcSQL
	
	SELECT ucrsCabPresc
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_prescricao_ValidaEspecialista <<IIF(EMPTY(ALLTRIM(ucrsCabPresc.drstamp)) or ISNULL(ucrsCabPresc.drstamp),'null',"'" + ALLTRIM(ucrsCabPresc.drstamp) +"'")>> 
	ENDTEXT


	IF !uf_gerais_actgrelha("", "uCrsValidaEspecialista", lcSql)
		MESSAGEBOX("N�o foi possivel validar os dados do especialista associado ao utilizador actual.",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF
	
	SELECT uCrsValidaEspecialista
	IF !EMPTY(uCrsValidaEspecialista.drcedula) AND !EMPTY(uCrsValidaEspecialista.drinscri) AND !EMPTY(uCrsValidaEspecialista.drordem) AND !EMPTY(uCrsValidaEspecialista.drclprofi)
		RETURN .t.	
	ELSE
		uf_perguntalt_chama("As configura��es do utilizador actual n�o permitem a grava��o de prescri��es. Contacte o Administrador.", "", "OK", 32) 
		return .f.
	ENDIF
ENDFUNC


**
FUNCTION uf_prescricao_CamposObrigatorios
	Local lcQTT,lcRef
	
	&& Informa��o do Especialista
	IF uf_prescricao_ValidaEspecialista() == .f.
		RETURN .f.
	ENDIF
		
	&& Nome	Utente
	IF EMPTY(UcrsCabPresc.nome)
		uf_perguntalt_chama("EXISTEM CAMPOS OBRIGAT�RIOS POR PREENCHER. "+CHR(10)+CHR(10)+"CAMPO: NOME DO UTENTE","", "OK", 48)
		RETURN .f.
	ENDIF

	&& N� utente
	If Empty(UcrsCabPresc.no)
		uf_perguntalt_chama("EXISTEM CAMPOS OBRIGAT�RIOS POR PREENCHER. "+CHR(10)+CHR(10)+"CAMPO: N�MERO DO UTENTE","", "OK", 48)
		Return .f.
	ENDIF

	&& Valida se o numero de utente tem apenas digitos
	IF !EMPTY(ALLTRIM(UcrsCabPresc.nrutente)) AND uf_gerais_isNumeric(ALLTRIM(UcrsCabPresc.nrutente))== .F. 
		uf_perguntalt_chama("N�mero de Utente Inv�lido. Por favor verifique.","", "OK", 48)
		Return .f.
	ENDIF
	
	&& Valida se o numero de utente tem 9 numeros
	IF !EMPTY(ALLTRIM(UcrsCabPresc.nrutente)) AND LEN(ALLTRIM(UcrsCabPresc.nrutente))!= 9 
		uf_perguntalt_chama("N�mero de Utente Inv�lido. Deve conter 9 n�meros.","", "OK", 48)
		Return .f.
	ENDIF	
	
	&& Data	
	If Empty(UcrsCabPresc.data)
		uf_perguntalt_chama("EXISTEM CAMPOS OBRIGAT�RIOS POR PREENCHER. "+CHR(10)+CHR(10)+"CAMPO: DATA DE PRESCRI��O","", "OK", 48)
		Return .f.
	ENDIF
			
	&& Nome do Especialista	
	If Empty(UcrsCabPresc.drnome)
		uf_perguntalt_chama("EXISTEM CAMPOS OBRIGAT�RIOS POR PREENCHER. "+CHR(10)+CHR(10)+"CAMPO: NOME DO ESPECIALISTA","", "OK", 48)
		Return .f.
	ENDIF
	
	&& N� do Especialista	
	If Empty(UcrsCabPresc.drno)
		uf_perguntalt_chama("EXISTEM CAMPOS OBRIGAT�RIOS POR PREENCHER. "+CHR(10)+CHR(10)+"CAMPO: N�MERO DO ESPECIALISTA","", "OK", 48)
		Return .f.
	ENDIF
	
	&& N� do Especialista	
	Select ucrslinPresc
	Count to lcCountNumLinhas
	If lcCountNumLinhas == 0
		uf_perguntalt_chama("EXISTEM CAMPOS OBRIGAT�RIOS POR PREENCHER. "+CHR(10)+CHR(10)+"CAMPO: INFORMA��O DAS LINHAS.","", "OK", 48)
		Return .f.
	ENDIF
	
	&& Posologia	
	IF uf_gerais_getParameter("ADM0000000233","BOOL")
		Select ucrslinPresc
		GO top
		SCAN 
			IF EMPTY(ALLTRIM(LEFT(posologia,254))) AND Left(ucrsLinPresc.design,1) != '.' AND ALLTRIM(tipoReceita) == "RN"
				uf_perguntalt_chama("EXISTEM CAMPOS OBRIGAT�RIOS POR PREENCHER. "+CHR(10)+CHR(10)+"CAMPO: POSOLOGIA."+CHR(10)+CHR(10)+"Medicamento: "+ ALLTRIM(ucrslinPresc.design),"", "OK", 48)
				IF Prescricao.detalhesVisiveis == .t.
					uf_prescricao_expandeDetalhe()
				ENDIF
				PRESCRICAO.pageframe1.activepage = 3
				PRESCRICAO.pageframe1.page3.pageframeMedicamentos.activepage = 2
						
				Return .f.
			ENDIF
		ENDSCAN 
	ENDIF 
		
	&& Receita Validada com sucesso		 
	RETURN .t.
ENDFUNC


**
FUNCTION uf_prescricao_PosologiaDetalhes
	IF myPrescricaoIntroducao == .f. AND myPrescricaoAlteracao  == .f.
		RETURN .f.
	ENDIF

ENDFUNC


** Contru��o de Posologia **
FUNCTION uf_prescricao_constroiPosologia
**
ENDFUNC


**
FUNCTION uf_prescricao_aplicaPosologiaOriginal
	SELECT ucrsLinPresc
	Replace ucrsLinPresc.posologiasugerida	WITH ucrsLinPresc.posologiaOriginal
ENDFUNC


**
FUNCTION uf_prescricao_aplicaPosologia
	SELECT ucrsLinPresc
	Replace ucrsLinPresc.posologia	WITH ucrsLinPresc.posologiasugerida
ENDFUNC 


**
FUNCTION uf_prescricao_actualizaInformacaoRef
	LOCAL lcPorDci 
	
	SELECT ucrsLinPresc
	Prescricao.ref = ALLTRIM(ucrsLinPresc.ref)
	lcPorDci = ucrsLinPresc.pordci
	uf_prescricao_actualizaCursorMaisBaratos(lcPorDci)
	uf_prescricao_actualizaInformacaoEncargos()
	uf_prescricao_actualizaCursorGenAlt(lcPorDci)

	** Valida Alerta de Cores
	IF LEFT(ALLTRIM(ucrsLinPresc.design),1) != "."
		uf_prescricao_actualizaAlertasCoresMed()
	ELSE
		uf_prescricao_actualizaAlertasCoresReceitaMed()
	ENDIF 
ENDFUNC


**
FUNCTION uf_prescricao_actualizaInformacaoMCDT
	SELECT ucrsLinPresc

	prescricao.pageframe1.page4.refresh
	prescricao.pageframe1.page5.refresh
ENDFUNC


**
FUNCTION uf_prescricao_actualizaAlertasCoresUtente
	LOCAL lcAlertaOpcoes 
	lcAlertaOpcoes = .f.

	**Beneficios
 	SELECT ucrsCabPresc
	IF !EMPTY(UcrsCabPresc.RECMcodigo) OR !EMPTY(UcrsCabPresc.BENEFcodigo)
		Prescricao.pageframe1.page1.btnBeneficio.backColor = RGB(210,0,0)	
	ELSE
		Prescricao.pageframe1.page1.btnBeneficio.backColor = RGB(42,143,154)
	ENDIF
	Prescricao.pageframe1.page1.btnBeneficio.refresh
	
	** Diplomas
	IF USED("uCrsDiplomasPresc")	
		i=0
		Select uCrsDiplomasPresc
		go top
		scan
			i=i+1
			exit
		ENDSCAN
		Select uCrsDiplomasPresc
		go top
		
		IF i == 0
			Prescricao.pageframe1.page1.btnDiploma.backColor = RGB(42,143,154)
		ELSE
			lcAlertaOpcoes = .t.
			Prescricao.pageframe1.page1.btnDiploma.backColor = RGB(210,0,0)
		ENDIF
		Prescricao.pageframe1.page1.btnDiploma.refresh
	ENDIF	

	** Patologias
	IF USED("uCrsPatologiasPresc")	
		i=0
		Select uCrsPatologiasPresc
		go top
		scan
			i=i+1
			exit
		ENDSCAN
		Select uCrsPatologiasPresc
		go top
		
		IF i == 0
			Prescricao.pageframe1.page1.btnPatologia.backColor = RGB(42,143,154)
		ELSE
			lcAlertaOpcoes = .t.
			Prescricao.pageframe1.page1.btnPatologia.backColor = RGB(210,0,0)
		ENDIF
		Prescricao.pageframe1.page1.btnPatologia.refresh
	ENDIF

	**Alergias
	IF USED("uCrsVerAlergias")	
		i=0
		Select uCrsVerAlergias
		go top
		scan
			i=i+1
			exit
		ENDSCAN
		Select uCrsVerAlergias
		go top
		
		IF i == 0
			Prescricao.pageframe1.page1.btnAlergia.backColor = RGB(42,143,154)
		ELSE
			lcAlertaOpcoes = .t.
			Prescricao.pageframe1.page1.btnAlergia.backColor = RGB(210,0,0)
		ENDIF
		Prescricao.pageframe1.page1.btnAlergia.refresh
	ENDIF
	

	** Interaccoes
	IF USED("uCrsInteraccoes")	
		i=0
		Select uCrsInteraccoes
		go top
		scan
			i=i+1
			exit
		ENDSCAN
		Select uCrsInteraccoes
		go top
		
		IF i == 0
			Prescricao.pageframe1.page1.btnInteraccoes.backColor = RGB(42,143,154)
		ELSE
			lcAlertaOpcoes = .t.
			Prescricao.pageframe1.page1.btnInteraccoes.backColor = RGB(210,0,0)
		ENDIF
		Prescricao.pageframe1.page1.btnInteraccoes.refresh
	ENDIF
		
	** ContraIndica��es
	IF USED("uCrsContraIndicacoes")	
		i=0
		Select uCrsContraIndicacoes
		go top
		scan
			i=i+1
			exit
		ENDSCAN
		Select uCrsContraIndicacoes
		go top
		
		IF i == 0
			Prescricao.pageframe1.page1.btnContraIndicacoes.backColor = RGB(42,143,154)
		ELSE
			lcAlertaOpcoes = .t.
			Prescricao.pageframe1.page1.btnContraIndicacoes.backColor = RGB(210,0,0)
		ENDIF
		Prescricao.pageframe1.page1.btnContraIndicacoes.refresh
	ENDIF
	
	
	** Historico MCDTS
	IF USED("uCrsmcdtMenosNoventa")	
		i=0
		Select uCrsmcdtMenosNoventa
		go top
		scan
			i=i+1
			exit
		ENDSCAN
		Select uCrsmcdtMenosNoventa
		go top
		
		IF i == 0
			Prescricao.pageframe1.page1.btnMCDTS.backColor = RGB(42,143,154)
		ELSE
			lcAlertaOpcoes = .t.
			Prescricao.pageframe1.page1.btnMCDTS.backColor = RGB(210,0,0)
		ENDIF
		Prescricao.pageframe1.page1.btnMCDTS.refresh
	ENDIF
	
	** Historico Medicamentos
	IF USED("uCrsHistMedUtente")	
		i=0
		Select uCrsHistMedUtente
		go top
		scan
			i=i+1
			exit
		ENDSCAN
		Select uCrsHistMedUtente
		go top
		
		IF i == 0
			Prescricao.pageframe1.page1.btnMeds.backColor = RGB(42,143,154)
		ELSE
			lcAlertaOpcoes = .t.
			Prescricao.pageframe1.page1.btnMeds.backColor = RGB(210,0,0)
		ENDIF
		Prescricao.pageframe1.page1.btnMeds.refresh
	ENDIF
	
ENDFUNC


**
FUNCTION uf_prescricao_actualizaAlertasCoresMed
	LOCAL lcAlertaOpcoes 
	lcAlertaOpcoes = .f.
	
	SELECT ucrsLinPresc
	IF LEFT(ALLTRIM(ucrsLinPresc.design),1) != "."
		**
		IF USED("uCrsProdutosPrescMaisBaratosAux")	
			i=0
			Select uCrsProdutosPrescMaisBaratosAux
			go top
			scan
				i=i+1
				exit
			ENDSCAN
			Select uCrsProdutosPrescMaisBaratosAux
			go top
			
			IF i == 0
				Prescricao.pageframe1.page3.btnMaisBaratos.backColor = RGB(42,143,154)
			ELSE
				lcAlertaOpcoes = .t.
				Prescricao.pageframe1.page3.btnMaisBaratos.backColor = RGB(210,0,0)
			ENDIF
			Prescricao.pageframe1.page3.btnMaisBaratos.refresh
		ENDIF	
		
		** Genericos Alternativos
		IF USED("uCrsProdutosGenAlt")	
			i=0
			Select uCrsProdutosGenAlt
			go top
			scan
				i=i+1
				exit
			Endscan
			Select uCrsProdutosGenAlt
			go top

			IF i == 0
				Prescricao.pageframe1.page3.BtnGenAlt.backColor = RGB(42,143,154)
			ELSE
				lcAlertaOpcoes = .t.
				Prescricao.pageframe1.page3.BtnGenAlt.backColor = RGB(210,0,0)
			ENDIF
			Prescricao.pageframe1.page3.BtnGenAlt.refresh
		ENDIF	
		
		Prescricao.containerTotal.check1.refresh
	ENDIF 
ENDFUNC


**
FUNCTION uf_prescricao_actualizaCursorMaisBaratos
	LPARAMETERS lcPorDci
	
	IF lcPorDci == .t.
		SELECT uCrsProdutosPrescMaisBaratosAux
		SET FILTER TO ALLTRIM(uCrsProdutosPrescMaisBaratosAux.refori) == "XXXXXXXX" 
	ELSE
		**Medicamentos Mais Baratos
		SELECT uCrsProdutosPrescMaisBaratosAux
		SET FILTER TO ALLTRIM(uCrsProdutosPrescMaisBaratosAux.refori) == ALLTRIM(Prescricao.ref)  
	ENDIF
ENDFUNC


**
FUNCTION uf_prescricao_actualizaInformacaoEncargos
	SELECT ucrsCabPresc
	SELECT ucrsLinPresc
	
	WITH prescricao.pageframe1.paGE3.pageframeMedicamentos.paGE4
		
		DO CASE 
			CASE EMPTY(ALLTRIM(ucrsCabPresc.nrutente))
				.regime.value = ""
				.regimecompart.value = 0
			CASE !EMPTY(ALLTRIM(ucrsCabPresc.RECMcodigo))	
				.regime.value = "SNS Regime Especial"
				.regimecompart.value = ucrsLinPresc.comp_snsRe
			OTHERWISE 
				.regime.value = "SNS Regime Geral"
				.regimecompart.value = ucrsLinPresc.comp_sns
		ENDCASE		

		.utente.value = ucrsLinPresc.utente
		.sns.value = (ucrsLinPresc.u_epvp* ucrsLinPresc.qtt) - ucrsLinPresc.utente
		
		SELECT ucrsLinPresc
		IF ucrsLinPresc.pordci == .t. AND !EMPTY(ALLTRIM(ucrsLinPresc.codGrupoH)) AND !(ALLTRIM(ucrsLinPresc.codGrupoH)=="GH0000")
			.tipoprescricao.value = "Prescri��o por DCI com grupo homog�neo"
		ENDIF 	
		IF ucrsLinPresc.pordci == .t. AND (EMPTY(ALLTRIM(ucrsLinPresc.codGrupoH)) OR ALLTRIM(ucrsLinPresc.codGrupoH)=="GH0000")
			.tipoprescricao.value = "Prescri��o por DCI sem grupo homog�neo"
		ENDIF 
		IF ucrsLinPresc.pordci == .f. AND !EMPTY(ucrsLinPresc.prescNomeCod)
			.tipoprescricao.value = "Prescri��o por nome comercial com justifica��o t�cnica"
		ENDIF
		IF ucrsLinPresc.pordci == .f. AND EMPTY(ALLTRIM(ucrsLinPresc.prescNomeCod))
			.tipoprescricao.value = "Prescri��o por nome comercial sem justifica��o t�cnica"
		ENDIF

		lcTextoEncargos = uf_prescricao_calculaTextoEncargos(ucrsLinPresc.u_epvp, ucrsLinPresc.utente, ucrsLinPresc.pordci, ALLTRIM(ucrsLinPresc.codGrupoH), ALLTRIM(ucrsLinPresc.ref), ALLTRIM(ucrsLinPresc.cnpem), ALLTRIM(ucrsLinPresc.diplomacod), ALLTRIM(ucrsLinPresc.PrescNomeCod),ALLTRIM(ucrsLinPresc.Design))		
		IF !EMPTY(lcTextoEncargos )
			.guiatratamento.value = ALLTRIM(lcTextoEncargos)
		ELSE
			.guiatratamento.value = "" 
		ENDIF 
			
		.refresh
	ENDWITH 

ENDFUNC


**
FUNCTION uf_prescricao_actualizaCursorGenAlt
	LPARAMETERS lcPorDci
	
	IF lcPorDci == .t.
		SELECT uCrsProdutosGenAlt
		SET FILTER TO ALLTRIM(uCrsProdutosGenAlt.refori) == "XXXXXXXX" 
	ELSE
		**Genericos Alternativos
		SELECT uCrsLinPresc
		SELECT uCrsProdutosGenAlt
		SET FILTER TO ALLTRIM(uCrsProdutosGenAlt.refori) == ALLTRIM(Prescricao.ref)  
	ENDIF 

ENDFUNC


**
FUNCTION uf_prescricao_actualizaContraIndicacoes
	LOCAL lcRefs 	
	lcRefs = ""
	
	&& Junta todas as referencias numa variavel	
	SELECT ucrsLinPresc
	lcPos = RECNO("ucrsLinPresc")
	GO TOP
	SCAN FOR !empty(ucrsLinPresc.ref)
		IF lcRefs == ''
			lcRefs = alltrim(ucrsLinPresc.ref)
		ELSE 
			lcRefs = lcRefs + "," + alltrim(ucrsLinPresc.ref)
		ENDIF 
	ENDSCAN
	
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_prescricao_verContraIndicacoes '<<ALLTRIM(lcRefs)>>', <<IIF(EMPTY(ALLTRIM(ucrsCabPresc.utstamp)) or ISNULL(ucrsCabPresc.utstamp),'null',"'" + ALLTRIM(ucrsCabPresc.utstamp) +"'")>>
	ENDTEXT 
	
	If !uf_gerais_actGrelha("Prescricao.pageframe1.page1.pageframeUtentes.page7.gridPesq", "uCrsContraIndicacoes", lcSql)
		uf_perguntalt_chama("N�o foi poss�vel verificar informa��o de Contra Indica��es. Contacte o suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF
ENDFUNC


**
FUNCTION uf_prescricao_actualizaInteraccoes
	LOCAL lcRefs, lcPos
	STORE '' TO lcRefs
	STORE 0 TO lcPos
	
	&& valida n� de produtos -> n�o corre quando so existe um
	SELECT ucrsLinPresc
	CALCULATE COUNT() FOR !empty(ucrsLinPresc.ref) TO lcRegistos
	
	IF lcRegistos = 1
		RETURN .T.
	ENDIF
	
	&& Junta todas as referencias numa variavel	
	SELECT ucrsLinPresc
	lcPos = RECNO("ucrsLinPresc")
	GO TOP
	SCAN FOR !empty(ucrsLinPresc.ref)
		IF lcRefs == ''
			lcRefs = alltrim(ucrsLinPresc.ref)
		ELSE 
			lcRefs = lcRefs + "," + alltrim(ucrsLinPresc.ref)
		ENDIF 
	EndScan
	
	**Elimina Registo Anteriores
	SELECT uCrsInteraccoes
	GO Top
	SCAN 
		Delete
	ENDSCAN 
	
	IF USED("uCrsProdInteraccoesTemp ")
		SELECT uCrsProdInteraccoesTemp 
		GO Top
		SCAN 
			Delete
		ENDSCAN 
	ENDIF 
	
	IF USED("uCrsTempInt")
		SELECT uCrsTempInt
		GO Top
		SCAN 
			Delete
		ENDSCAN 
	ENDIF 
	
	&& percorre todas as linhas
	SELECT ucrsLinPresc
	GO top
	SCAN FOR !empty(ucrsLinPresc.ref)
		Text to lcSql noshow textmerge
			exec up_prescricao_interacoes '<<ALLTRIM(ucrsLinPresc.ref)>>','<<ALLTRIM(ucrsLinPresc.design)>>','<<lcRefs>>'
		ENDTEXT
		IF uf_gerais_actGrelha("", "uCrsTempInt", lcSql)
			IF RECCOUNT("uCrsTempInt") > 0
				SELECT uCrsInteraccoesTemp
				APPEND FROM DBF("uCrsTempInt")
			ENDIF 
		ELSE
			MESSAGEBOX("OCORREU UM ERRO A VALIDAR AS INTERAC��ES ENTRE PRODUTOS.",64,"LOGITOOLS SOFTWARE")
			RETURN .F.
		ENDIF
	ENDSCAN

	IF USED("uCrsInteraccoesTemp")
		IF RECCOUNT("uCrsInteraccoesTemp")>0
			SELECT uCrsInteraccoesTemp
			GO Top
			SCAN
				SELECT uCrsInteraccoes
				APPEND BLANK
				replace uCrsInteraccoes.ref WITH uCrsInteraccoesTemp.ref
				replace uCrsInteraccoes.design1 WITH uCrsInteraccoesTemp.design1 
				replace uCrsInteraccoes.design WITH uCrsInteraccoesTemp.design
				replace uCrsInteraccoes.cnp2 WITH uCrsInteraccoesTemp.cnp2
				replace uCrsInteraccoes.grau WITH uCrsInteraccoesTemp.grau
				replace uCrsInteraccoes.explicacao WITH uCrsInteraccoesTemp.explicacao 
				replace uCrsInteraccoes.conselho WITH uCrsInteraccoesTemp.conselho 
			ENDSCAN
		ENDIF
	ENDIF
	 
	SELECT uCrsInteraccoes
	GO Top
	
	SELECT ucrsLinPresc
	try
		GO lcPos
	CATCH
		GO TOP
	ENDTRY
	
	&& cria cursor referencias
	SELECT distinct ref,design1 as design FROM uCrsInteraccoes INTO CURSOR uCrsProdInteraccoesTemp READWRITE
	
	SELECT uCrsProdInteraccoesTemp 
	GO Top
	SCAN
		SELECT uCrsProdInteraccoes
		APPEND BLANK
		Replace uCrsProdInteraccoes.ref WITH uCrsProdInteraccoesTemp.ref
		Replace uCrsProdInteraccoes.design WITH uCrsProdInteraccoesTemp.design
	ENDSCAN 
		
	&& fecha o cursor
	IF USED("uCrsTempInt")
		fecha("uCrsTempInt")
	ENDIF	
ENDFUNC


**
FUNCTION uf_prescricao_receitaRenovavel
	SELECT ucrslinPresc

	LOCAL lcDataValidade 

	IF ucrslinPresc.renovavel == .f.
		Replace ucrslinPresc.vias WITH 1
			
		***Controla VALIDADE
		* Por Defeito coloca 30 dias na validade para receitas n�o renovaveis
**		lcSQL = ""
**		TEXT TO lcSQL NOSHOW TEXTMERGE
**			Select DATEADD(dd,30,'<<uf_gerais_getdate(ucrsCabpresc.data,"SQL")>>') as validade
**		ENDTEXT
**		IF !uf_gerais_actGrelha("", "UcrsValidadePresc", lcSql)
**			uf_perguntalt_chama("N�o foi possivel calcular a validade da receita. Contacte o suporte.", "", "OK", 16) 
**			RETURN .f.
**		ENDIF
		
		lcDataValidade = uf_prescricao_PreencheValidadePorLinha(myPrescMaterializada,ucrslinPresc.tipoduracao,ucrslinPresc.prescNomeCod,ucrslinPresc.prescDateCod)
		Replace ucrsLinPresc.validade 			WITH lcDataValidade
		Replace ucrsLinPresc.renovavel			WITH IIF(!EMPTY(ucrslinPresc.prescDateCod),.t.,.f.)
		
		lcCabStamp = ucrslinPresc.prescstamp
		SELECT ucrslinPresc
		GO Top
		SCAN
			IF ALLTRIM(ucrslinPresc.prescstamp) == ALLTRIM(lcCabStamp)
				Replace ucrslinPresc.validade WITH lcDataValidade 
			ENDIF 
		ENDSCAN 
		SELECT ucrslinPresc
		GO Top
		LOCATE FOR ALLTRIM(ucrslinPresc.prescstamp) == ALLTRIM(lcCabStamp)
		****
	ELSE
	
		Replace ucrslinPresc.vias WITH 1
		
		***Controla VALIDADE
		* Por Defeito coloca 6 meses na validade para receitas renovaveis
**		lcSQL = ""
**		TEXT TO lcSQL NOSHOW TEXTMERGE
**			Select DATEADD(mm,6,'<<uf_gerais_getdate(ucrsCabpresc.data,"SQL")>>') as validade
**		ENDTEXT
**		IF !uf_gerais_actGrelha("", "UcrsValidadePresc", lcSql)
**			uf_perguntalt_chama("N�o foi possivel calcular a validade da receita. Contacte o suporte.", "", "OK", 16) 
**			RETURN .f.
**		ENDIF
		
				
		lcDataValidade = uf_prescricao_PreencheValidadePorLinha(myPrescMaterializada,ucrslinPresc.tipoduracao,ucrslinPresc.prescNomeCod,ucrslinPresc.prescDateCod)
		Replace ucrsLinPresc.validade 			WITH lcDataValidade
		Replace ucrsLinPresc.renovavel			WITH IIF(!EMPTY(ucrslinPresc.prescDateCod),.t.,.f.)
		
		
		
		lcCabStamp = ucrslinPresc.prescstamp
		SELECT ucrslinPresc
		GO Top
		SCAN
			IF ALLTRIM(ucrslinPresc.prescstamp) == ALLTRIM(lcCabStamp)
				Replace ucrslinPresc.validade WITH lcDataValidade 
			ENDIF 
		ENDSCAN 
		SELECT ucrslinPresc
		GO Top
		LOCATE FOR ALLTRIM(ucrslinPresc.prescstamp) == ALLTRIM(lcCabStamp)
		****
		
	ENDIF
	uf_prescricao_actualizaAlertasCoresReceitaMed()
ENDFUNC


**
FUNCTION uf_prescricao_actualizaAlertasCoresReceitaMed
	LOCAL lcAlertaOpcoes 
	lcAlertaOpcoes = .f.
	
	SELECT ucrslinPresc
	IF ucrslinPresc.renovavel == .t.
		Prescricao.pageframe1.page2.BtnRenovavel.backColor = RGB(225,0,0)
	ELSE
		Prescricao.pageframe1.page2.BtnRenovavel.backColor = RGB(42,143,154)
	ENDIF
	
	IF ucrsLinPresc.estado != "2"
		Prescricao.pageframe1.page2.BtnDadosEnvio.backColor = RGB(42,143,154)
	ELSE
		Prescricao.pageframe1.page2.BtnDadosEnvio.backColor = RGB(225,0,0)
	ENDIF

	IF Prescricao.pageframe1.page2.BtnRenovavel.backColor != RGB(42,143,154);
		OR 	Prescricao.pageframe1.page2.BtnDadosEnvio.backColor != RGB(42,143,154)
		lcAlertaOpcoes = .t.
	ELSE
		lcAlertaOpcoes = .f.
	ENDIF
		
ENDFUNC 


**
FUNCTION uf_prescricao_actualizaInformacaoReceitasMedicamentos
	
	**Actualiza Historico de Envio
	SELECT ucrsLinPresc
	
	IF Left(ucrsLinPresc.design,1)=='.'
		lcSQL = ""
		TEXT TO lcSQL NOSHOW TEXTMERGE
			exec up_prescricao_RespostaHistoricoEnvioOnline <<IIF(EMPTY(ALLTRIM(ucrsLinPresc.prescstamp)) or ISNULL(ucrsLinPresc.prescstamp),'null',"'" + ALLTRIM(ucrsLinPresc.prescstamp) +"'")>>
		ENDTEXT
		IF !uf_gerais_actGrelha("Prescricao.pageframe1.page2.pageframeReceitaMedicamentos.page3.grdPresc", "uCrsRespostaEnvioOnline", lcSql)
			uf_perguntalt_chama("N�o foi possivel verificar historio do envio em modo online. Contacte o suporte.", "", "OK", 16) 
			RETURN .f.
		ENDIF
		IF !uf_gerais_actGrelha("Prescricao.pageframe1.page5.pageframeReceitaMCDT.page2.grdPresc", "uCrsRespostaEnvioOnlineMCDT", lcSql)
			uf_perguntalt_chama("N�o foi possivel verificar historio do envio em modo online. Contacte o suporte.", "", "OK", 16) 
			RETURN .f.
		ENDIF
	ENDIF

	Prescricao.pageframe1.page2.refresh
ENDFUNC 


**
FUNCTION uf_prescricao_numeracaoReceitaMedicamentos
	LPARAMETERS lcTipoReceita, lcVia, lcRenovavel

	** 
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE
		exec up_prescricao_NumeracaoPresc 1, '<<lcTipoReceita>>', '109', '<<mysite>>'
	ENDTEXT

	IF !uf_gerais_actGrelha("", "UcrsNumercaoReceitaOffline", lcSql)
		uf_perguntalt_chama("N�o foi possivel calcular o n�mero de receita offline. Contacte o suporte.", "", "OK", 16) 
		RETURN ''
	ENDIF
	
	Select UcrsNumercaoReceitaOffline
	lcNumeracao = uf_prescricao_CheckDigit(ALLTRIM(UcrsNumercaoReceitaOffline.numeracao) + IIF(lcRenovavel == .t., ALLTRIM(lcVia), "0"))
	
	RETURN lcNumeracao
ENDFUNC


**
FUNCTION uf_prescricao_CheckDigit
	LPARAMETERS lcstr

	&&_cliptext = lcSQL

	Local p

	lcstr = ALLTRIM(lcstr)

	p = 0

  	FOR i= 1 TO LEN(lcstr)
		c = ASC(SUBSTR(lcstr, i, 1)) - 48
  		p = 2*(p+c)
  	ENDFOR

  	p = p%11
  	c = (12-p) % 11
  	
	IF c == 10
		return ALLTRIM(lcstr) + 'X'
	ELSE
    	return ALLTRIM(lcstr) + ALLTRIM(str(c))
	ENDIF
  
ENDFUNC




** faz valida��o e manda para app de gerais para efetuar a impress�o
FUNCTION uf_prescricao_imprimir
	LPARAMETERS lcAlteraimpressora
	PUBLIC myRecImprimeTodas 
	myRecImprimeTodas = .t.
	LOCAL receitaMcdt, lcPrescStamp
	receitaMcdt = .f.



	&& setfocus para melhorar impress�o
	prescricao.txtnome.setfocus
	
	&& Verifica se existem receitas para imprimir
	SELECT * FROM ucrsLinPresc WHERE (estado == "1" OR estado == "3") AND LEFT(ALLTRIM(design),1) == "." AND anulado == .f. INTO CURSOR ucrsLinPrescPrintVerify READWRITE 
	
	SELECT ucrsLinPrescPrintVerify 
	IF RECCOUNT("ucrsLinPrescPrintVerify") == 0
		uf_perguntalt_chama("O estado das receitas n�o permitem a sua impress�o. As receitas s� podem ser impressas depois de validadas ou em caso indisponibilidade dos servi�os de valida��o.", "", "OK", 16) 
		RETURN .f.
	ELSE
		
		IF(ucrsLinPrescPrintVerify.receitamcdt==.t.)
			receitaMcdt = .t.
			lcPrescStamp = 	ALLTRIM(ucrsLinPrescPrintVerify.prescstamp)	
		ENDIF	
	
		IF USED("ucrsLinPrescPrintVerify")
			fecha("ucrsLinPrescPrintVerify")
		ENDIF	
	
	
		&&Receita mcdt usam um servi�o que retorna o pdf a imprimir
		IF(receitaMcdt ==.f.)
			&& depende se imprime para a impressora pre definida ou se chama o painel LTS que permite alterar a impressora		
			IF lcAlteraimpressora = .f.	
				uf_imprimirgerais_Chama("PRESCRICAOMED", .t.)
			ELSE
				uf_imprimirgerais_Chama("PRESCRICAOMED", .f.)
			ENDIF
		ELSE
			uf_prescricao_imprimirReceitaMcdtServico(lcPrescStamp)	

		ENDIF
			
			
	ENDIF
	
ENDFUNC

FUNCTION uf_prescricao_imprimirReceitaMcdtServico
	LPARAMETERS lcPrescStamp
	LOCAL lcstampEnvioOnline
	
	lcstampEnvioOnline = uf_gerais_stamp()
	
	
	lcWsPath = ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\PE\MCDT\MCDT.jar';
			+ ' "--PrescStamp=' + ALLTRIM(lcPrescStamp) + ["];
			+ ' "--StampLigacao=' + ALLTRIM(lcstampEnvioOnline) + ["];
			+ ' "--clID=' + ALLTRIM(uCrsE1.id_lt) + ["];
			+ ' "--chavePedidoRelacionado= ' + '' + ["];
			+ ' "--computerName=' + ALLTRIM(myClientName) + ["];
			+ ' "--terminal_id=' + ALLTRIM(STR(mytermno)) + ["];
			+ ' "--req_type= ' + '3' + ["];			
			
	
	** 
	&& 3� parametro a .t. aguardar reposta do Java
	lcWsPath = "javaw -jar " + lcWsPath 
	
	IF(emdesenvolvimento == .t.)
		_cliptext = lcWsPath 
	ENDIF
	

	oWSShell = CREATEOBJECT("WScript.Shell")
	oWSShell.Run(lcWsPath,1,.f.)
	
	

ENDFUNC


** criar cursor para permitir a impress�o
FUNCTION uf_prescricao_imprimirReceitaMcdt
	LPARAMETERS lcImprimeTodas
	PUBLIC myRecImprimeTodas
	
	SELECT ucrsLinPresc
	lcPrescstamp = ucrsLinPresc.prescstamp	

	SELECT * FROM ucrsLinPresc WHERE (estado == "1" OR estado == "3") AND LEFT(ALLTRIM(design),1) == "." AND ALLTRIM(prescstamp) == ALLTRIM(lcPrescstamp) AND !EMPTY(receitamcdt) INTO CURSOR ucrsLinPrescPrint READWRITE 
	
	select ucrsLinPrescPrint 
	IF RECCOUNT("ucrsLinPrescPrint") == 0
		IF EMPTY(myRecImprimeTodas)
			uf_perguntalt_chama("O estado da receita n�o permite a sua impress�o.", "", "OK", 16) 
		ENDIF 
		RETURN .f.
	ENDIF

	**Verifica se existem receitas a imprimir
	SELECT * FROM ucrsLinPresc WHERE (estado == "1" OR estado == "3") AND LEFT(ALLTRIM(design),1) == "." AND ALLTRIM(prescstamp) == ALLTRIM(lcPrescstamp) AND !EMPTY(receitamcdt) INTO CURSOR ucrsLinPrescPrint READWRITE
	SELECT * FROM ucrsLinPresc WHERE (estado == "1" OR estado == "3") AND !(LEFT(ALLTRIM(design),1) == ".") AND pos == 1 AND ALLTRIM(prescstamp) == ALLTRIM(lcPrescstamp) AND !EMPTY(receitamcdt) INTO CURSOR ucrsLinPrescPrint1 READWRITE 
	SELECT * FROM ucrsLinPresc WHERE (estado == "1" OR estado == "3") AND !(LEFT(ALLTRIM(design),1) == ".") AND pos == 2 AND ALLTRIM(prescstamp) == ALLTRIM(lcPrescstamp) AND !EMPTY(receitamcdt) INTO CURSOR ucrsLinPrescPrint2 READWRITE 
	SELECT * FROM ucrsLinPresc WHERE (estado == "1" OR estado == "3") AND !(LEFT(ALLTRIM(design),1) == ".") AND pos == 3 AND ALLTRIM(prescstamp) == ALLTRIM(lcPrescstamp) AND !EMPTY(receitamcdt) INTO CURSOR ucrsLinPrescPrint3 READWRITE 
	SELECT * FROM ucrsLinPresc WHERE (estado == "1" OR estado == "3") AND !(LEFT(ALLTRIM(design),1) == ".") AND pos == 4 AND ALLTRIM(prescstamp) == ALLTRIM(lcPrescstamp) AND !EMPTY(receitamcdt) INTO CURSOR ucrsLinPrescPrint4 READWRITE 
	SELECT * FROM ucrsLinPresc WHERE (estado == "1" OR estado == "3") AND !(LEFT(ALLTRIM(design),1) == ".") AND pos == 5 AND ALLTRIM(prescstamp) == ALLTRIM(lcPrescstamp) AND !EMPTY(receitamcdt) INTO CURSOR ucrsLinPrescPrint5 READWRITE 
	SELECT * FROM ucrsLinPresc WHERE (estado == "1" OR estado == "3") AND !(LEFT(ALLTRIM(design),1) == ".") AND pos == 6 AND ALLTRIM(prescstamp) == ALLTRIM(lcPrescstamp) AND !EMPTY(receitamcdt) INTO CURSOR ucrsLinPrescPrint6 READWRITE 

	select ucrsLinPrescPrint 
	myDataPresc = ALLTRIM(str(YEAR(ucrsLinPrescPrint.data)))+ '-' + IIF(len(ALLTRIM(str(MONTH(ucrsLinPrescPrint.data))))==1,'0'+ALLTRIM(str(month(ucrsLinPrescPrint.data))),ALLTRIM(str(month(ucrsLinPrescPrint.data)))) + '-' + IIF(len(ALLTRIM(str(day(ucrsLinPrescPrint.data))))==1,'0'+alltrim(str(day(ucrsLinPrescPrint.data))),ALLTRIM(str(day(ucrsLinPrescPrint.data))))
	
	**verifica se paciente � migrante
	SELECT uCrsCabPresc
	TEXT TO lcSQL TEXTMERGE noshow
		Select * from b_utentes where utstamp = '<<ALLTRIM(ucrsCabPresc.utstamp)>>'
	ENDTEXT

	IF !uf_gerais_actGrelha("", "uCrsPacientePrint", lcSql)
		uf_perguntalt_chama("N�o foi possivel verificar os dados do utente, n�o ser� possivel imprimir. Contacte o suporte.", "", "OK", 16) 
		RETURN .f.
	ELSE
		SELECT uCrsPacientePrint
		GO TOP 
	ENDIF
	**
	
	IF USED("UcrsNaturezaMcdt")
		fecha("UcrsNaturezaMcdt")
	ENDIF

	CREATE CURSOR UcrsNaturezaMcdt(na l,nb l,nc l,nd l,ne l,nf l,ng l,nh l,ni l,nj l,nl l,nm l,nn l,no l, nz l)
	SELECT UcrsNaturezaMcdt
	APPEND BLANK
	DO CASE
		CASE UPPER(ALLTRIM(ucrsLinPrescPrint1.mcdt_area)) == "A";
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint2.mcdt_area)) == "A" ;
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint3.mcdt_area)) == "A" ;
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint4.mcdt_area)) == "A" ;
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint5.mcdt_area)) == "A" ;
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint6.mcdt_area)) == "A" 

			Replace UcrsNaturezaMcdt.na With .t.
		CASE UPPER(ALLTRIM(uCrsLinPresc.mcdt_area)) == "B" ;
		 	 OR UPPER(ALLTRIM(ucrsLinPrescPrint2.mcdt_area)) == "B" ;
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint3.mcdt_area)) == "B" ;
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint4.mcdt_area)) == "B" ;
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint5.mcdt_area)) == "B" ;
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint6.mcdt_area)) == "B" 
			 
			Replace UcrsNaturezaMcdt.nb With .t.
		CASE UPPER(ALLTRIM(uCrsLinPresc.mcdt_area)) == "C" ;
 			 OR UPPER(ALLTRIM(ucrsLinPrescPrint2.mcdt_area)) == "C" ;
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint3.mcdt_area)) == "C" ;
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint4.mcdt_area)) == "C" ;
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint5.mcdt_area)) == "C" ;
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint6.mcdt_area)) == "C"
			 
			Replace UcrsNaturezaMcdt.nc With .t.
		CASE UPPER(ALLTRIM(uCrsLinPresc.mcdt_area)) == "D" ; 
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint2.mcdt_area)) == "D" ;
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint3.mcdt_area)) == "D" ;
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint4.mcdt_area)) == "D" ;
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint5.mcdt_area)) == "D" 
			 
			Replace UcrsNaturezaMcdt.nd With .t.
		CASE UPPER(ALLTRIM(uCrsLinPresc.mcdt_area)) == "E" ; 
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint2.mcdt_area)) == "E" ;
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint3.mcdt_area)) == "E" ;
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint4.mcdt_area)) == "E" ;
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint5.mcdt_area)) == "E" 
		
			Replace UcrsNaturezaMcdt.ne With .t.
		CASE UPPER(ALLTRIM(uCrsLinPresc.mcdt_area)) == "F" ; 
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint2.mcdt_area)) == "F" ;
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint3.mcdt_area)) == "F" ;
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint4.mcdt_area)) == "F" ;
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint5.mcdt_area)) == "F" 
			Replace UcrsNaturezaMcdt.nf With .t.
		CASE UPPER(ALLTRIM(uCrsLinPresc.mcdt_area)) == "G" ;
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint2.mcdt_area)) == "G" ;
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint3.mcdt_area)) == "G" ;
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint4.mcdt_area)) == "G" ;
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint5.mcdt_area)) == "G" 
			 
			Replace UcrsNaturezaMcdt.ng With .t.
		CASE UPPER(ALLTRIM(uCrsLinPresc.mcdt_area)) == "H" ;
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint2.mcdt_area)) == "H" ;
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint3.mcdt_area)) == "H" ;
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint4.mcdt_area)) == "H" ;
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint5.mcdt_area)) == "H" 
		
			Replace UcrsNaturezaMcdt.nh With .t.
		CASE UPPER(ALLTRIM(uCrsLinPresc.mcdt_area)) == "I" ;
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint2.mcdt_area)) == "I" ;
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint3.mcdt_area)) == "I" ;
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint4.mcdt_area)) == "I" ;
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint5.mcdt_area)) == "I" 
			 
			Replace UcrsNaturezaMcdt.ni With .t.
		CASE UPPER(ALLTRIM(uCrsLinPresc.mcdt_area)) == "J" ;
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint2.mcdt_area)) == "J" ;
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint3.mcdt_area)) == "J" ;
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint4.mcdt_area)) == "J" ;
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint5.mcdt_area)) == "J" 
		
			Replace UcrsNaturezaMcdt.nj With .t.
		
		CASE UPPER(ALLTRIM(uCrsLinPresc.mcdt_area)) == "L" ;
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint2.mcdt_area)) == "L" ;
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint3.mcdt_area)) == "L" ;
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint4.mcdt_area)) == "L" ;
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint5.mcdt_area)) == "L" 
		
			Replace UcrsNaturezaMcdt.nl With .t.
		CASE UPPER(ALLTRIM(uCrsLinPresc.mcdt_area)) == "M" ;
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint2.mcdt_area)) == "M" ;
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint3.mcdt_area)) == "M" ;
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint4.mcdt_area)) == "M" ;
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint5.mcdt_area)) == "M" 
		
			Replace UcrsNaturezaMcdt.nm With .t.
		CASE UPPER(ALLTRIM(uCrsLinPresc.mcdt_area)) == "N" ;
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint2.mcdt_area)) == "N" ;
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint3.mcdt_area)) == "N" ;
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint4.mcdt_area)) == "N" ;
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint5.mcdt_area)) == "N" 
		
			Replace UcrsNaturezaMcdt.nn With .t.
		CASE UPPER(ALLTRIM(uCrsLinPresc.mcdt_area)) == "O" ;
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint2.mcdt_area)) == "O" ;
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint3.mcdt_area)) == "O" ;
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint4.mcdt_area)) == "O" ;
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint5.mcdt_area)) == "O" 
		
			Replace UcrsNaturezaMcdt.no With .t.
		CASE UPPER(ALLTRIM(uCrsLinPresc.mcdt_area)) == "Z" ;
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint2.mcdt_area)) == "Z" ;
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint3.mcdt_area)) == "Z" ;
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint4.mcdt_area)) == "Z" ;
			 OR UPPER(ALLTRIM(ucrsLinPrescPrint5.mcdt_area)) == "Z" 
		
			Replace UcrsNaturezaMcdt.nz With .t.
		OTHERWISE
			**
	ENDCASE		

	** Inicia variavel para a previsao
	myRecViaDesc = "1.� VIA"
	Select UcrsCabPresc
	** 
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE
		Select localPresc, infarmed, localacss, zonaacss, localcod, localnm from empresa where site = '<<ucrsLinPresc.site>>'
	ENDTEXT
	IF !uf_gerais_actGrelha("", "UcrsDadosLocalPresc", lcSql)
		uf_perguntalt_chama("N�o foi possivel verificar os dados do local de prescri��o, n�o ser� possivel imprimir. Contacte o suporte.", "", "OK", 16) 
		RETURN .f.
	ENDIF

	SELECT UcrsCabPresc
	GO Top
	SELECT ucrsLinPrescPrint 
	SELECT ucrsLinPrescPrint1
	SELECT ucrsLinPrescPrint2
	SELECT ucrsLinPrescPrint3
	SELECT ucrsLinPrescPrint4
	SELECT ucrsLinPrescPrint5
	SELECT ucrsLinPrescPrint6
	
	IF !lcImprimeTodas && S� abre caso seja impressao individual
		myRecImprimeTodas = .f.
		uf_imprimirgerais_Chama("MCDT",.t.)
	ENDIF
ENDFUNC 


**
FUNCTION uf_prescricao_decricaoMcdt
	LPARAMETERS lcNrLinha

	LOCAL lcRetorno 
	lcRetorno = ""

	DO CASE 
		CASE EMPTY(lcNrLinha)
			RETURN ""
		CASE lcNrLinha == 1
			lcRetorno = IIF(empty(UPPER(Alltrim(ucrsLinPrescPrint1.mcdt_sinonimo))),UPPER(Alltrim(ucrsLinPrescPrint1.mcdt_nomedescr)), UPPER(Alltrim(ucrsLinPrescPrint1.mcdt_sinonimo)) + chr(13) + UPPER(Alltrim(ucrsLinPrescPrint1.mcdt_nomedescr)))
			lcRetorno = lcRetorno + IIF(EMPTY(Alltrim(ucrsLinPrescPrint1.mcdt_local)),""," / " + Alltrim(ucrsLinPrescPrint1.mcdt_local))
			
			RETURN LEFT(lcRetorno,200)
			
		CASE lcNrLinha == 2
			lcRetorno = IIF(empty(UPPER(Alltrim(ucrsLinPrescPrint2.mcdt_sinonimo))),UPPER(Alltrim(ucrsLinPrescPrint2.mcdt_nomedescr)), UPPER(Alltrim(ucrsLinPrescPrint2.mcdt_sinonimo)) + chr(13) + UPPER(Alltrim(ucrsLinPrescPrint2.mcdt_nomedescr)))
			lcRetorno = lcRetorno + IIF(EMPTY(Alltrim(ucrsLinPrescPrint2.mcdt_local)),""," / " + Alltrim(ucrsLinPrescPrint2.mcdt_local))
			
			RETURN LEFT(lcRetorno,200)
		CASE lcNrLinha == 3
			lcRetorno = IIF(empty(UPPER(Alltrim(ucrsLinPrescPrint3.mcdt_sinonimo))),UPPER(Alltrim(ucrsLinPrescPrint3.mcdt_nomedescr)), UPPER(Alltrim(ucrsLinPrescPrint3.mcdt_sinonimo)) + chr(13) + UPPER(Alltrim(ucrsLinPrescPrint3.mcdt_nomedescr)))
			lcRetorno = lcRetorno + IIF(EMPTY(Alltrim(ucrsLinPrescPrint3.mcdt_local)),""," / " + Alltrim(ucrsLinPrescPrint3.mcdt_local))
			
			RETURN LEFT(lcRetorno,200)
		CASE lcNrLinha == 4
			lcRetorno = IIF(empty(UPPER(Alltrim(ucrsLinPrescPrint4.mcdt_sinonimo))),UPPER(Alltrim(ucrsLinPrescPrint4.mcdt_nomedescr)), UPPER(Alltrim(ucrsLinPrescPrint4.mcdt_sinonimo)) + chr(13) + UPPER(Alltrim(ucrsLinPrescPrint4.mcdt_nomedescr)))
			lcRetorno = lcRetorno + IIF(EMPTY(Alltrim(ucrsLinPrescPrint4.mcdt_local)),""," / " + Alltrim(ucrsLinPrescPrint4.mcdt_local))
			
			RETURN LEFT(lcRetorno,200)
		CASE lcNrLinha == 5
			lcRetorno = IIF(empty(UPPER(Alltrim(ucrsLinPrescPrint5.mcdt_sinonimo))),UPPER(Alltrim(ucrsLinPrescPrint5.mcdt_nomedescr)), UPPER(Alltrim(ucrsLinPrescPrint5.mcdt_sinonimo)) + chr(13) + UPPER(Alltrim(ucrsLinPrescPrint5.mcdt_nomedescr)))
			lcRetorno = lcRetorno + IIF(EMPTY(Alltrim(ucrsLinPrescPrint5.mcdt_local)),""," / " + Alltrim(ucrsLinPrescPrint5.mcdt_local))
			
			RETURN LEFT(lcRetorno,200)
		CASE lcNrLinha == 6
			lcRetorno = IIF(empty(UPPER(Alltrim(ucrsLinPrescPrint6.mcdt_sinonimo))),UPPER(Alltrim(ucrsLinPrescPrint6.mcdt_nomedescr)), UPPER(Alltrim(ucrsLinPrescPrint6.mcdt_sinonimo)) + chr(13) + UPPER(Alltrim(ucrsLinPrescPrint6.mcdt_nomedescr)))
			lcRetorno = lcRetorno + IIF(EMPTY(Alltrim(ucrsLinPrescPrint6.mcdt_local)),""," / " + Alltrim(ucrsLinPrescPrint6.mcdt_local))
			
			RETURN LEFT(lcRetorno,200)
	ENDCASE 
	
	**TODO messagebox
	&&MESSAGEBOX(lcRetorno)
		
	RETURN ""	
ENDFUNC 	


** funcao q criar cursores para impressao de receita materializadas
FUNCTION uf_prescricao_imprimirReceitaMed
	LPARAMETERS lcImprimeTodas
	
	PUBLIC 	myRecViaDesc, myDataPresc, myTextoEncargos1, myTextoEncargos2, myTextoEncargos3, myTextoEncargos4, myTextoEncargos5, myTextoEncargos6, myTextoEncargos7, myTextoEncargos8, myTextoEncargos9, myTextoEncargos10,;
			myTextoExcecao1, myTextoExcecao2, myTextoExcecao3, myTextoExcecao4,  myTextoExcecao5, myTextoExcecao6, myTextoExcecao7, myTextoExcecao8, myTextoExcecao9, myTextoExcecao10, myRecImprimeTodas 
	STORE '' TO myTextoEncargos1, myTextoEncargos2, myTextoEncargos3, myTextoEncargos4, myTextoEncargos5, myTextoEncargos6, myTextoEncargos7, myTextoEncargos8, myTextoEncargos9, myTextoEncargos10,;
			myTextoExcecao1, myTextoExcecao2, myTextoExcecao3, myTextoExcecao4,  myTextoExcecao5, myTextoExcecao6, myTextoExcecao7, myTextoExcecao8, myTextoExcecao9, myTextoExcecao10

	SELECT ucrsLinPresc
	lcPrescstamp = ucrsLinPresc.prescstamp	

	SELECT * FROM ucrsLinPresc WHERE (estado == "1" OR estado == "3") AND LEFT(ALLTRIM(design),1) == "." AND ALLTRIM(prescstamp) == ALLTRIM(lcPrescstamp) INTO CURSOR ucrsLinPrescPrint READWRITE 
	
	select ucrsLinPrescPrint 
	IF RECCOUNT("ucrsLinPrescPrint") == 0
		uf_perguntalt_chama("O estado das receitas n�o permite a sua impress�o.", "", "OK", 16) 
		RETURN .f.
	ENDIF
	
	** Inicia variavel para a previsao
	myRecViaDesc = "1.� VIA"

	** 
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE
		Select localPresc, infarmed, localacss, zonaacss, localcod, localnm, localnmAbrev from empresa where site = '<<ucrsLinPresc.site>>'
	ENDTEXT
	IF !uf_gerais_actGrelha("", "UcrsDadosLocaPresc", lcSql)
		uf_perguntalt_chama("N�o foi possivel verificar os dados do local de prescri��o, n�o ser� possivel imprimir. Contacte o suporte.", "", "OK", 16) 
		RETURN .f.
	ENDIF
	SELECT UcrsDadosLocaPresc
	GO Top
	
	**Verifica se existem receitas a imprimir
	SELECT * FROM ucrsLinPresc WHERE (estado == "1" OR estado == "3") AND LEFT(ALLTRIM(design),1) == "." AND ALLTRIM(prescstamp) == ALLTRIM(lcPrescstamp) INTO CURSOR ucrsLinPrescPrint READWRITE
	SELECT * FROM ucrsLinPresc WHERE (estado == "1" OR estado == "3") AND !(LEFT(ALLTRIM(design),1) == ".") AND pos == 1 AND ALLTRIM(prescstamp) == ALLTRIM(lcPrescstamp) INTO CURSOR ucrsLinPrescPrint1 READWRITE 
	SELECT * FROM ucrsLinPresc WHERE (estado == "1" OR estado == "3") AND !(LEFT(ALLTRIM(design),1) == ".") AND pos == 2 AND ALLTRIM(prescstamp) == ALLTRIM(lcPrescstamp) INTO CURSOR ucrsLinPrescPrint2 READWRITE 
	SELECT * FROM ucrsLinPresc WHERE (estado == "1" OR estado == "3") AND !(LEFT(ALLTRIM(design),1) == ".") AND pos == 3 AND ALLTRIM(prescstamp) == ALLTRIM(lcPrescstamp) INTO CURSOR ucrsLinPrescPrint3 READWRITE 
	SELECT * FROM ucrsLinPresc WHERE (estado == "1" OR estado == "3") AND !(LEFT(ALLTRIM(design),1) == ".") AND pos == 4 AND ALLTRIM(prescstamp) == ALLTRIM(lcPrescstamp) INTO CURSOR ucrsLinPrescPrint4 READWRITE 
	SELECT * FROM ucrsLinPresc WHERE (estado == "1" OR estado == "3") AND !(LEFT(ALLTRIM(design),1) == ".") AND pos == 5 AND ALLTRIM(prescstamp) == ALLTRIM(lcPrescstamp) INTO CURSOR ucrsLinPrescPrint5 READWRITE 
	SELECT * FROM ucrsLinPresc WHERE (estado == "1" OR estado == "3") AND !(LEFT(ALLTRIM(design),1) == ".") AND pos == 6 AND ALLTRIM(prescstamp) == ALLTRIM(lcPrescstamp) INTO CURSOR ucrsLinPrescPrint6 READWRITE 
	SELECT * FROM ucrsLinPresc WHERE (estado == "1" OR estado == "3") AND !(LEFT(ALLTRIM(design),1) == ".") AND pos == 7 AND ALLTRIM(prescstamp) == ALLTRIM(lcPrescstamp) INTO CURSOR ucrsLinPrescPrint7 READWRITE 
	SELECT * FROM ucrsLinPresc WHERE (estado == "1" OR estado == "3") AND !(LEFT(ALLTRIM(design),1) == ".") AND pos == 8 AND ALLTRIM(prescstamp) == ALLTRIM(lcPrescstamp) INTO CURSOR ucrsLinPrescPrint8 READWRITE 
	SELECT * FROM ucrsLinPresc WHERE (estado == "1" OR estado == "3") AND !(LEFT(ALLTRIM(design),1) == ".") AND pos == 9 AND ALLTRIM(prescstamp) == ALLTRIM(lcPrescstamp) INTO CURSOR ucrsLinPrescPrint9 READWRITE 
	SELECT * FROM ucrsLinPresc WHERE (estado == "1" OR estado == "3") AND !(LEFT(ALLTRIM(design),1) == ".") AND pos == 0 AND ALLTRIM(prescstamp) == ALLTRIM(lcPrescstamp) INTO CURSOR ucrsLinPrescPrint10 READWRITE 							


	select ucrsLinPrescPrint 

	
	SELECT ucrsLinPrescPrint1
	
	IF RECCOUNT("ucrsLinPrescPrint1")>0
		myTextoEncargos1 = uf_prescricao_calculaTextoEncargos(ucrsLinPrescPrint1.u_epvp, ucrsLinPrescPrint1.utente, ucrsLinPrescPrint1.pordci, ALLTRIM(ucrsLinPrescPrint1.codGrupoH), ALLTRIM(ucrsLinPrescPrint1.ref), ALLTRIM(ucrsLinPrescPrint1.cnpem), ALLTRIM(ucrsLinPrescPrint1.diplomacod), ALLTRIM(ucrsLinPrescPrint1.PrescNomeCod),ALLTRIM(ucrsLinPrescPrint1.Design))
		myTextoExcecao1 = uf_prescricao_calculaTextoExcecao(ucrsLinPrescPrint1.prescNomeCod)
	ENDIF
	SELECT ucrsLinPrescPrint2
	IF RECCOUNT("ucrsLinPrescPrint2")>0
		myTextoEncargos2 = uf_prescricao_calculaTextoEncargos(ucrsLinPrescPrint2.u_epvp, ucrsLinPrescPrint2.utente, ucrsLinPrescPrint2.pordci, ALLTRIM(ucrsLinPrescPrint2.codGrupoH), ALLTRIM(ucrsLinPrescPrint2.ref), ALLTRIM(ucrsLinPrescPrint2.cnpem),		 ALLTRIM(ucrsLinPrescPrint2.diplomacod), ALLTRIM(ucrsLinPrescPrint2.PrescNomeCod),ALLTRIM(ucrsLinPrescPrint2.Design))
		myTextoExcecao2 = uf_prescricao_calculaTextoExcecao(ucrsLinPrescPrint2.prescNomeCod)
	ENDIF
	SELECT ucrsLinPrescPrint3
	IF RECCOUNT("ucrsLinPrescPrint3")>0
		myTextoEncargos3 = uf_prescricao_calculaTextoEncargos(ucrsLinPrescPrint3.u_epvp, ucrsLinPrescPrint3.utente, ucrsLinPrescPrint3.pordci, ALLTRIM(ucrsLinPrescPrint3.codGrupoH), ALLTRIM(ucrsLinPrescPrint3.ref), ALLTRIM(ucrsLinPrescPrint3.cnpem), ALLTRIM(ucrsLinPrescPrint3.diplomacod), ALLTRIM(ucrsLinPrescPrint3.PrescNomeCod),ALLTRIM(ucrsLinPrescPrint3.Design))
		myTextoExcecao3 = uf_prescricao_calculaTextoExcecao(ucrsLinPrescPrint3.prescNomeCod)
	ENDIF
	SELECT ucrsLinPrescPrint4
	IF RECCOUNT("ucrsLinPrescPrint4")>0
		myTextoEncargos4 = uf_prescricao_calculaTextoEncargos(ucrsLinPrescPrint4.u_epvp, ucrsLinPrescPrint4.utente, ucrsLinPrescPrint4.pordci, ALLTRIM(ucrsLinPrescPrint4.codGrupoH), ALLTRIM(ucrsLinPrescPrint4.ref), ALLTRIM(ucrsLinPrescPrint4.cnpem), ALLTRIM(ucrsLinPrescPrint4.diplomacod), ALLTRIM(ucrsLinPrescPrint4.PrescNomeCod),ALLTRIM(ucrsLinPrescPrint4.Design))
		myTextoExcecao4 = uf_prescricao_calculaTextoExcecao(ucrsLinPrescPrint4.prescNomeCod)
	ENDIF
	
	SELECT ucrsLinPrescPrint5
	IF RECCOUNT("ucrsLinPrescPrint5")>0
		myTextoEncargos5 = uf_prescricao_calculaTextoEncargos(ucrsLinPrescPrint5.u_epvp, ucrsLinPrescPrint5.utente, ucrsLinPrescPrint5.pordci, ALLTRIM(ucrsLinPrescPrint5.codGrupoH), ALLTRIM(ucrsLinPrescPrint5.ref), ALLTRIM(ucrsLinPrescPrint5.cnpem), ALLTRIM(ucrsLinPrescPrint5.diplomacod), ALLTRIM(ucrsLinPrescPrint5.PrescNomeCod),ALLTRIM(ucrsLinPrescPrint5.Design))
		myTextoExcecao5 = uf_prescricao_calculaTextoExcecao(ucrsLinPrescPrint5.prescNomeCod)
	ENDIF
	SELECT ucrsLinPrescPrint6
	IF RECCOUNT("ucrsLinPrescPrint6")>0
		myTextoEncargos6 = uf_prescricao_calculaTextoEncargos(ucrsLinPrescPrint6.u_epvp, ucrsLinPrescPrint6.utente, ucrsLinPrescPrint6.pordci, ALLTRIM(ucrsLinPrescPrint6.codGrupoH), ALLTRIM(ucrsLinPrescPrint6.ref), ALLTRIM(ucrsLinPrescPrint6.cnpem), ALLTRIM(ucrsLinPrescPrint6.diplomacod), ALLTRIM(ucrsLinPrescPrint6.PrescNomeCod),ALLTRIM(ucrsLinPrescPrint6.Design))
		myTextoExcecao6 = uf_prescricao_calculaTextoExcecao(ucrsLinPrescPrint6.prescNomeCod)
	ENDIF
	SELECT ucrsLinPrescPrint7
	IF RECCOUNT("ucrsLinPrescPrint7")>0
		myTextoEncargos7 = uf_prescricao_calculaTextoEncargos(ucrsLinPrescPrint7.u_epvp, ucrsLinPrescPrint7.utente, ucrsLinPrescPrint7.pordci, ALLTRIM(ucrsLinPrescPrint7.codGrupoH), ALLTRIM(ucrsLinPrescPrint7.ref), ALLTRIM(ucrsLinPrescPrint7.cnpem), ALLTRIM(ucrsLinPrescPrint7.diplomacod), ALLTRIM(ucrsLinPrescPrint7.PrescNomeCod),ALLTRIM(ucrsLinPrescPrint7.Design))
		myTextoExcecao7 = uf_prescricao_calculaTextoExcecao(ucrsLinPrescPrint7.prescNomeCod)
	ENDIF
	SELECT ucrsLinPrescPrint8
	IF RECCOUNT("ucrsLinPrescPrint8")>0
		myTextoEncargos8 = uf_prescricao_calculaTextoEncargos(ucrsLinPrescPrint8.u_epvp, ucrsLinPrescPrint8.utente, ucrsLinPrescPrint8.pordci, ALLTRIM(ucrsLinPrescPrint8.codGrupoH), ALLTRIM(ucrsLinPrescPrint8.ref), ALLTRIM(ucrsLinPrescPrint8.cnpem), ALLTRIM(ucrsLinPrescPrint8.diplomacod), ALLTRIM(ucrsLinPrescPrint8.PrescNomeCod),ALLTRIM(ucrsLinPrescPrint8.Design))
		myTextoExcecao8 = uf_prescricao_calculaTextoExcecao(ucrsLinPrescPrint8.prescNomeCod)
	ENDIF
	SELECT ucrsLinPrescPrint9
	IF RECCOUNT("ucrsLinPrescPrint9")>0
		myTextoEncargos9 = uf_prescricao_calculaTextoEncargos(ucrsLinPrescPrint9.u_epvp, ucrsLinPrescPrint9.utente, ucrsLinPrescPrint9.pordci, ALLTRIM(ucrsLinPrescPrint9.codGrupoH), ALLTRIM(ucrsLinPrescPrint9.ref), ALLTRIM(ucrsLinPrescPrint9.cnpem), ALLTRIM(ucrsLinPrescPrint9.diplomacod), ALLTRIM(ucrsLinPrescPrint9.PrescNomeCod),ALLTRIM(ucrsLinPrescPrint9.Design))
		myTextoExcecao9 = uf_prescricao_calculaTextoExcecao(ucrsLinPrescPrint9.prescNomeCod)
	ENDIF
	SELECT ucrsLinPrescPrint10
	IF RECCOUNT("ucrsLinPrescPrint10")>0
		myTextoEncargos10 = uf_prescricao_calculaTextoEncargos(ucrsLinPrescPrint10.u_epvp, ucrsLinPrescPrint10.utente, ucrsLinPrescPrint10.pordci, ALLTRIM(ucrsLinPrescPrint10.codGrupoH), ALLTRIM(ucrsLinPrescPrint10.ref), ALLTRIM(ucrsLinPrescPrint10.cnpem), ALLTRIM(ucrsLinPrescPrint10.diplomacod), ALLTRIM(ucrsLinPrescPrint10.PrescNomeCod),ALLTRIM(ucrsLinPrescPrint10.Design))
		myTextoExcecao10 = uf_prescricao_calculaTextoExcecao(ucrsLinPrescPrint10.prescNomeCod)
	ENDIF		

	myDataPresc = ALLTRIM(str(YEAR(ucrsLinPrescPrint.data)))+ '-' + IIF(len(ALLTRIM(str(MONTH(ucrsLinPrescPrint.data))))==1,'0'+ALLTRIM(str(month(ucrsLinPrescPrint.data))),ALLTRIM(str(month(ucrsLinPrescPrint.data)))) + '-' + IIF(len(ALLTRIM(str(day(ucrsLinPrescPrint.data))))==1,'0'+alltrim(str(day(ucrsLinPrescPrint.data))),ALLTRIM(str(day(ucrsLinPrescPrint.data))))
	
	IF !lcImprimeTodas && S� abre caso seja impressao individual
		myRecImprimeTodas = .f.
		uf_imprimirgerais_Chama("PRESCRICAOMED", .t.)
	ENDIF
ENDFUNC



** O RECTIOPO que � impresso � diferente do que � gravado na BD e enviado via web service
FUNCTION uf_prescricao_RecmTipoImpressao
	LOCAL lcCodigosBenf, lcRetorno
	lcCodigosBenf = ""
	lcRetorno = ""
	
	SELECT ucrsCabPresc
	lcSQL = ''	

	TEXT TO lcSql NOSHOW TEXTMERGE 
		Select	motivo from b_cli_outrosBenef (nolock) Where stamp = '<<ALLTRIM(ucrsCabPresc.utstamp)>>'
		union 
		Select motivo = recmmotivo from b_utentes (nolock) Where utstamp = '<<ALLTRIM(ucrsCabPresc.utstamp)>>'
	ENDTEXT

*!*	_cliptext = lcSQL
*!*	Messagebox(lcSQL)

	IF !uf_gerais_actGrelha("","uCrsRecmBenefImpress",lcSql)
		uf_perguntalt_chama("OCORREU UM ERRO A VERIFICAR OS OUTROS BENEF�CIOS.","OK","",16)
		RETURN .f.
	ENDIF	
	
	**Calculo do Regime de Comparticipa��o RECMTipo - est� dependente dos beneficios e outros beneficios
	** 
	SELECT uCrsRecmBenefImpress
	GO TOP
	SCAN FOR !EMPTY(ALLTRIM(uCrsRecmBenefImpress.motivo))
		IF !EMPTY(ALLTRIM(lcCodigosBenf))
			lcCodigosBenf = ALLTRIM(lcCodigosBenf) + "," + ALLTRIM(uCrsRecmBenefImpress.motivo)
		ELSE
			lcCodigosBenf = ALLTRIM(uCrsRecmBenefImpress.motivo)
		ENDIF
	ENDSCAN 
	
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_prescricao_RecmTipo '<<lcCodigosBenf>>', 1
	ENDTEXT
	
*!*		_cliptext = lcSQL
*!*		Messagebox(lcSQL)	

	IF !uf_gerais_actGrelha("","uCrsTipoRECMImpress",lcSQL)
		uf_perguntalt_chama("N�o foi poss�vel verificar Regime de comparticipa��o.","OK","",16)
		return .f.
	ENDIF
	**
	IF RECCOUNT("uCrsTipoRECMImpress")>0
		SELECT uCrsTipoRECMImpress
		lcRetorno = uCrsTipoRECMImpress.tipo
	ENDIF
	
	RETURN lcRetorno
ENDFUNC


**
FUNCTION uf_prescricao_calculaTextoEncargos 
	LPARAMETERS lcPvp,lcEncargoUtente, LcPorDCI, lcGrupoHmg, lcRef, lcCNPEM, lcDiplomaID, lcPrescNomeCod, lcMedName
	LOCAL lcSQL, lcTextoEncargos
	STORE '' TO lcTextoEncargos, lcSQL
	
	
	IF(EMPTY(lcMedName))
		lcMedName=ALLTRIM("")
	ENDIF

	
	IF LcPorDCI == .t.
		IF lcPvp> 0
			lcTextoEncargos =  "Esta prescri��o custa-lhe, no m�ximo, � " + ALLTRIM(STRTRAN(STR(lcEncargoUtente,9,2),".",",")) +", a n�o ser que opte por um medicamento mais caro." 
		ENDIF 	
	ELSE &&Prescri��o por Ref
		IF lcPvp> 0 
			IF !EMPTY(lcPrescNomeCod)  &&AND !LIKEC('*LYRICA*', UPPER(lcMedName)) && Com justifica��o 
				lcTextoEncargos = "Este medicamento custa-lhe, no m�ximo, � " +  ALLTRIM(STRTRAN(STR(lcEncargoUtente,9,2),".",",")) +", podendo optar por um mais barato." 
			ELSE
				lcTextoEncargos = "Este medicamento custa-lhe, no m�ximo, � " +  ALLTRIM(STRTRAN(STR(lcEncargoUtente,9,2),".",",")) +"." 
			ENDIF
		ENDIF 
	ENDIF
	
	RETURN lcTextoEncargos
ENDFUNC


**
FUNCTION uf_prescricao_ActualizaEncargos

	regua(0, 5, "A actualizar encargos do utente...", .f.)
	regua(1, 1, "A actualizar encargos do utente...", .f.)
	
	SELECT ucrsLinPresc
	lcLinstamp = ucrsLinPresc.presclstamp
	GO top
	SCAN 	
		IF !(LEFT(ALLTRIM(ucrsLinPresc.design),1) == ".") AND EMPTY(ucrsLinPresc.receitamcdt) && linhas
			
			regua(1, 3, "A actualizar encargos do utente...", .f.)
			uf_prescricao_calculaEncargosUtenteLinha(ucrsLinPresc.porDci,ucrsLinPresc.codGrupoH,ucrsLinPresc.ref,ucrsLinPresc.cnpem,ucrsLinPresc.diplomacod, ucrsLinPresc.PrescNomeCod, ucrsLinPresc.qtt)
		ENDIF 
	ENDSCAN
	regua(1, 4, "A actualizar encargos do utente...", .f.)
	**
	uf_prescricao_actualizaQttTotalReceita()
	regua(1, 5, "A actualizar encargos do utente...", .f.)
	**
	uf_prescricao_colocaNaLinha(lcLinstamp)
	regua(2)
ENDFUNC 


** funcao onde s�o calculados os encargos para o utentes - alterada a 10/12/2016 para trabalhar apenas com uma SP
FUNCTION uf_prescricao_calculaEncargosUtenteLinha
	LPARAMETERS LcPorDCI, lcGrupoHmg, lcRef, lcCNPEM, lcDiplomaID, lcPrescNomeCod, lcQTT

	LOCAL lcSQL, lcEncargoUtente , lcRecm, lcCNRP, lcLyricaAux
	STORE '' TO lcSQL
	STORE 0 TO lcEncargoUtente 
	STORE .f. TO lcRecm, lcCNRP, lcLyricaAux
	
	&& valida se faz calculo diferente por causa do caso especifico da Lyrica
*!*		IF ALLTRIM(lcDiplomaID) == '63'
*!*			lcLyricaAux = .t.
*!*		ENDIF

	&& valida se � utente SNS com regime Especial
	SELECT ucrsCabPresc
	IF UPPER(ALLTRIM(ucrsCabPresc.RECMTIPO)) == 'R'
		STORE .t. TO lcRecm
	ENDIF
	
	&& valida se � utente da efr CENTRO NACIONAL DE RISCOS PROFISSIONAIS
	IF ALLTRIM(ucrsCabPresc.entidadeCod) == "930003"
		STORE .t. TO lcCNRP
	ENDIF

	&& 
	DO CASE 
		CASE !EMPTY(ALLTRIM(ucrsCabPresc.nrutente)) && Tem utente pertencente ao SNS

			IF LcPorDCI == .t. &&Prescri��o Por DCI
					
				IF !EMPTY(ALLTRIM(lcGrupoHmg)) AND !(ALLTRIM(lcGrupoHmg) == 'GH0000') && Com Grupo Homogeneo
				
					lcSQL = ''
					TEXT TO lcSQL NOSHOW TEXTMERGE 
						exec up_prescricao_calculoEncargosUtente  @cnpem = '<<ALLTRIM(lcCNPEM)>>', @grphmgcode='<<ALLTRIM(lcGrupoHmg)>>', @plano = '<<ALLTRIM(ucrsCabPresc.entidade)>>', @diploma_ID = '<<ALLTRIM(lcDiplomaID)>>'
					ENDTEXT 
					
				ELSE &&Sem Grupo Homogeneo
				
					lcSQL = ''
					TEXT TO lcSQL NOSHOW TEXTMERGE 
						exec up_prescricao_calculoEncargosUtente @cnpem = '<<ALLTRIM(lcCNPEM)>>', @plano = '<<ALLTRIM(ucrsCabPresc.entidade)>>', @diploma_ID = '<<ALLTRIM(lcDiplomaID)>>'
					ENDTEXT
					
				ENDIF
			
				IF !uf_gerais_actGrelha("", "UcrsTextEncargos", lcSql)
					uf_perguntalt_chama("N�o foi possivel verificar os encargos para o Utente. Contacte o suporte.", "", "OK", 16) 
					RETURN ''
				ENDIF

				**Regime de Comparticipa��o
				IF lcRecm == .t.
					lcEncargoUtente = UcrsTextEncargos.encUtRe
				ELSE
					lcEncargoUtente = UcrsTextEncargos.encUtRg	
				ENDIF
			
				** Substitui taxa de comparticipacao RG, RE		
				SELECT UcrsTextEncargos
				SELECT 	ucrsLinPresc
				Replace ucrsLinPresc.comp_sns 	WITH UcrsTextEncargos.comp_sns
				Replace ucrsLinPresc.comp_snsRe WITH UcrsTextEncargos.comp_snsRe

			ELSE &&Prescri��o por Ref

				IF !EMPTY(lcPrescNomeCod)
				** Com justifica��o t�cnica

					lcSQL = ''
					TEXT TO lcSQL NOSHOW TEXTMERGE 
						exec up_prescricao_calculoEncargosUtente  @cnp = '<<ALLTRIM(lcRef)>>', @compLyrica = <<IIF(!EMPTY(lcLyricaAux),1,0)>>, @plano = '<<ALLTRIM(ucrsCabPresc.entidade)>>', @diploma_ID = '<<ALLTRIM(lcDiplomaID)>>'
					ENDTEXT  
				
					IF !uf_gerais_actGrelha("", "UcrsTextEncargos", lcSql)
						uf_perguntalt_chama("N�o foi possivel verificar os encargos para o Utente. Contacte o suporte.", "", "OK", 16) 
						RETURN ''
					ENDIF
					
					**Regime de Comparticipa��o
					IF lcRecm == .t.
						lcEncargoUtente = UcrsTextEncargos.encUtRe					
					ELSE
						lcEncargoUtente = UcrsTextEncargos.encUtRg
					ENDIF

					** Substitui taxa de comparticipacao RG, RE		
					SELECT UcrsTextEncargos
					SELECT 	ucrsLinPresc
					Replace ucrsLinPresc.comp_sns 	WITH UcrsTextEncargos.comp_sns
					Replace ucrsLinPresc.comp_snsRe WITH UcrsTextEncargos.comp_snsRe
					
				ELSE
					
					** Restantes Situa��es
					** sem justifica��o t�cnica

					lcSQL = ''
					TEXT TO lcSQL NOSHOW TEXTMERGE 
						exec up_prescricao_calculoEncargosUtente  @cnp = '<<ALLTRIM(lcRef)>>', @plano = '<<ALLTRIM(ucrsCabPresc.entidade)>>', @diploma_ID = '<<ALLTRIM(lcDiplomaID)>>'
					ENDTEXT

					IF !uf_gerais_actGrelha("", "UcrsTextEncargos", lcSql)
						uf_perguntalt_chama("N�o foi possivel verificar os encargos para o Utente. Contacte o suporte.", "", "OK", 16) 
						RETURN ''
					ENDIF
					
					**Regime de Comparticipa��o
					IF lcRecm == .t.
						lcEncargoUtente = UcrsTextEncargos.encUtRe
					ELSE
						lcEncargoUtente = UcrsTextEncargos.encUtRg		
					ENDIF
					
					** Substitui taxa de comparticipacao RG, RE		
					SELECT UcrsTextEncargos
					SELECT 	ucrsLinPresc
					Replace ucrsLinPresc.comp_sns with UcrsTextEncargos.comp_sns
					Replace ucrsLinPresc.comp_snsRe with UcrsTextEncargos.comp_snsRe
				
				ENDIF
			ENDIF
			
			** 
			** Diploma 
			**
			**TODO
**MESSAGEBOX(lcDiplomaID)
			IF !EMPTY(lcDiplomaID) AND ALLTRIM(lcDiplomaID) != '63'

				SELECT UcrsTextEncargos
				SELECT 	ucrsLinPresc	
				REPLACE  ucrsLinPresc.comp_diploma WITH UcrsTextEncargos.comp_diploma		
				IF lcRecm == .t.
					lcEncargoUtente = UcrsTextEncargos.encUtReDip
				ELSE

					lcEncargoUtente = UcrsTextEncargos.encUtRgDip
				ENDIF
			
			ELSE
				REPLACE  ucrsLinPresc.comp_diploma WITH 0
			ENDIF
			
	CASE EMPTY(ALLTRIM(ucrsCabPresc.nrutente)) && O utente n�o tem numero SNS

		lcSQL = ''
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			exec up_prescricao_pesquisaMed 1, '<<Alltrim(lcRef)>>','','<<Alltrim(lcRef)>>','','','',1,'',0,<<ch_userno>>, 0
		ENDTEXT 
		IF !uf_gerais_actGrelha("", "UcrsTextEncargos", lcSql)
			uf_perguntalt_chama("N�o foi possivel verificar os encargos para o Utente. Contacte o suporte.", "", "OK", 16) 
			RETURN ''
		ENDIF

		lcEncargoUtente = UcrsTextEncargos.pvp
		
		** Substitui taxa de comparticipacao RG e RE		
		SELECT 	ucrsLinPresc
		Replace ucrsLinPresc.comp_sns 	WITH 0
		Replace ucrsLinPresc.comp_snsRe WITH 0
		
	ENDCASE 
				
*!*		IF !EMPTY(lcDiplomaID) AND ALLTRIM(lcDiplomaID) != '63'
*!*			TEXT TO lcSQL NOSHOW TEXTMERGE 
*!*				exec up_prescricao_DiplomaCompart '<<Alltrim(lcDiplomaID)>>', <<lcEncargoUtente>>
*!*			ENDTEXT
*!*			IF !uf_gerais_actGrelha("", "UcrsEncargosDiplomaCompart", lcSql)
*!*				uf_perguntalt_chama("N�o foi possivel verificar a taxa de comparticipa��o do diploma. Contacte o suporte.", "", "OK", 16) 
*!*				RETURN ''
*!*			ENDIF

*!*			SELECT UcrsEncargosDiplomaCompart
*!*			lcEncargoUtente = UcrsEncargosDiplomaCompart.encargoUt
*!*			
*!*			** Substitui taxa de comparticipacao do diploma	
*!*			SELECT 	ucrsLinPresc	
*!*			Replace ucrsLinPresc.comp_diploma WITH UcrsEncargosDiplomaCompart.compart
*!*		ELSE
*!*			** Substitui taxa de comparticipacao do diploma
*!*			SELECT 	ucrsLinPresc
*!*			Replace ucrsLinPresc.comp_diploma WITH 0
*!*		ENDIF

	**
*!*		IF lcCNRP == .t. AND ucrsLinPresc.comp_sns != 0 
*!*			lcEncargoUtente = 0
*!*		ENDIF

	&& valores a pagar pelo utente
	SELECT 	ucrsLinPresc
	Replace ucrsLinPresc.utente 	WITH lcEncargoUtente * lcQTT
	Replace ucrsLinPresc.etiliquido WITH lcEncargoUtente * lcQTT

ENDFUNC

** Se apenas existir um diploma aplica automaticamente

FUNCTION  uf_prescricao_aplicaDiplomaAuto
	LPARAMETERS lcRef
	
	IF(USED("ucrsListaDiplomas"))	
		fecha("ucrsListaDiplomas")
	ENDIF
	

	IF myPrescricaoIntroducao == .f. and myPrescricaoAlteracao == .f.
		RETURN .f.
	ENDIF
	
		
	With PRESCRICAO.pageframe1.page3.pageframeMedicamentos.page1
		
		**senao a ainda n�o tem diploma associado
	
		IF EMPTY(ALLTRIM(.diploma.value))
			
			lcSQL = ""
			TEXT TO lcSQL TEXTMERGE NOSHOW
				exec up_prescricao_Diplomas '<<ALLTRIM(lcRef)>>'
			ENDTEXT
			IF !uf_gerais_actGrelha("", "ucrsListaDiplomas", lcSql)
				uf_perguntalt_chama("N�o foi possivel encontrar diplomas.", "", "OK", 16)
				RETURN .f.
			ENDIF
			
		
			
			**Se existe apenas um diploma adiciona automaticamente
			IF (RECCOUNT("ucrsListaDiplomas")==1)
				.diplomaid.Value = ucrsListaDiplomas.diploma_ID
				.diplomaid.Refresh		
				.diploma.Value = ALLTRIM(ucrsListaDiplomas.diploma)
				.diploma.Refresh		
			ENDIF
			
			
		ENDIF
		
	ENDWITH 

	
		
	IF(USED("ucrsListaDiplomas"))	
		fecha("ucrsListaDiplomas")
	ENDIF
	
			
ENDFUNC



**
FUNCTION uf_prescricao_aplicaDiploma

	IF myPrescricaoIntroducao == .f. and myPrescricaoAlteracao == .f.
		RETURN .f.
	ENDIF

	With PRESCRICAO.pageframe1.page3.pageframeMedicamentos.page1
		IF EMPTY(ALLTRIM(.diploma.value))
			SELECT ucrsLinPresc
			lcSQL = ""
			TEXT TO lcSQL TEXTMERGE NOSHOW
				exec up_prescricao_Diplomas '<<ALLTRIM(ucrsLinPresc.ref)>>'
			ENDTEXT
			If !uf_gerais_actGrelha("", "ucrsListaDiplomas", lcSql)
				uf_perguntalt_chama("N�o foi possivel encontrar diplomas.", "", "OK", 16)
				RETURN .f.
			ENDIF
			
			uf_valorescombo_chama(SYS(1272, PRESCRICAO.pageframe1.page3.pageframeMedicamentos.page1.diploma), 1, "ucrsListaDiplomas", 2, "diploma, diploma_ID, descricao", "diploma_ID, diploma , descricao")
			
			SELECT ucrsListaDiplomas
			GO top
			LOCATE FOR ALLTRIM(ucrsListaDiplomas.diploma) == ALLTRIM(.diploma.value)
			IF FOUND()
				.diplomaid.Value = ucrsListaDiplomas.diploma_ID
				.diplomaid.Refresh
				
				SELECT ucrsCabPresc 
				replace ucrsCabPresc.RECMTipo WITH "O"
			ENDIF	
		ELSE
			.diploma.Value = ""
			.diplomaid.value = ""
			 
			SELECT ucrsCabPresc 
			IF !EMPTY(ucrsCabPresc.RECMcodigo)
				replace ucrsCabPresc.RECMTipo WITH "R"
			ELSE
				replace ucrsCabPresc.RECMTipo WITH ""
			ENDIF
			
		ENDIF
	ENDWITH 
	
	** Actualiza Encargo
	lcEncargoUtente = 0
	SELECT ucrsLinPresc
	uf_prescricao_calculaEncargosUtenteLinha(ucrsLinPresc.porDci,ucrsLinPresc.codGrupoH,ucrsLinPresc.ref,ucrsLinPresc.cnpem,ucrsLinPresc.diplomacod, ucrsLinPresc.PrescNomeCod, ucrsLinPresc.qtt)
	
	lcLinstamp = ucrsLinPresc.presclstamp
	uf_prescricao_actualizaQttTotalReceita()
	uf_prescricao_colocaNaLinha(lcLinstamp)
	uf_prescricao_actualizaInformacaoEncargos()

ENDFUNC


**
FUNCTION uf_prescricao_calculaTextoExcecao
	LPARAMETERS lcAlinea
	LOCAL lcTextoExcecao
	STORE '' TO lcTextoExcecao
	
	DO case
		CASE ALLTRIM(lcAlinea) == "A"
			lcTextoExcecao = "Exce��o a) do n.� 3 do art. 6.�"
		CASE ALLTRIM(lcAlinea) == "B"
			lcTextoExcecao = "Exce��o b) do n.� 3 do art. 6.� - Rea��o adversa pr�via."
		CASE ALLTRIM(lcAlinea) == "C"
			lcTextoExcecao = "Exce��o c) do n.� 3 do art. 6.� - Continuidade de tratamento superior a 28 dias"
		OTHERWISE
			**
	ENDCASE 

	RETURN lcTextoExcecao
ENDFUNC


**
FUNCTION uf_prescricao_adicionaOutrosProds
	LOCAL lcTextoTipoReceita, lcControlaNumEmb, lcLinstamp, lcPrescStampAux , lcDataValidade
	STORE '' TO lcTextoTipoReceita, lcLinstamp, lcPrescStampAux
	STORE 0 TO lcOrdem
	STORE .f. TO lcControlaNumEmb
	
	&& para os cen�rios das RSPs
	lcPrescStampAux = ALLTRIM(uCrsLinPresc.Prescstamp)

	regua(0, 5, "A adicionar....", .f.)
	regua(1, 1, "A adicionar....", .f.)

	** Tipos de Receita **
	** OUT - Outros Produtos
	** LOUT - Outros Produtos RSP

	&& apaga ordena��o necess�rio para n�o dar erro
	Select uCrsLinPresc
	DELETE TAG ALL

	* Calula validade  - por defeito coloca 30 dias na validade para receitas n�o renovaveis
**	lcSQL = ""
**	TEXT TO lcSQL NOSHOW TEXTMERGE
**		Select DATEADD(dd,30,'<<uf_gerais_getdate(ucrscabpresc.data,"SQL")>>') as validade
**	ENDTEXT
**	IF !uf_gerais_actGrelha("", "UcrsValidadePresc", lcSql)
**		uf_perguntalt_chama("N�o foi possivel calcular a validade da receita. Contacte o suporte.", "", "OK", 16) 
**		RETURN .f.
**	ENDIF
	****
	regua(1, 2, "A adicionar....", .f.)	
		
	SELECT uCrsLinPresc
	GO TOP 	 
	LOCATE FOR ALLTRIM(UPPER(ucrsLinPresc.TipoReceita)) == "OUT" 
	IF !FOUND() AND !EMPTY(myPrescMaterializada)
		
		lcCabstamp = uf_gerais_stamp()
		lcLinstamp = uf_gerais_stamp()
		lcstampEnvioOnline = uf_gerais_stamp()

		Select ucrsLinPresc
		APPEND BLANK 
		Replace ucrsLinPresc.prescstamp WITH lcCabstamp
		Replace ucrsLinPresc.presclstamp WITH lcLinstamp
		Replace ucrsLinPresc.stampEnvioOnline WITH lcstampEnvioOnline
		
		Replace ucrsLinPresc.design WITH uf_prescricao_textoTipoReceita("OUT")
		Replace ucrsLinPresc.qtt 	WITH 1
		Replace ucrsLinPresc.lordem WITH uf_prescricao_CalculaPrimeiraOrdemTipoReceita("OUT",.f.)
		Replace ucrsLinPresc.TipoReceita WITH "OUT"
		Replace ucrsLinPresc.nrPrescricao WITH prescricao.nrPrescricao
		Replace ucrsLinPresc.terminal WITH Prescricao.terminal
		Replace ucrsLinPresc.vias WITH 1
		Replace ucrsLinPresc.estado WITH "0"
		Replace ucrsLinPresc.estadodescr WITH "Receita n�o submetida"
		
		lcDataValidade = uf_prescricao_PreencheValidadePorLinha(myPrescMaterializada,ucrslinPresc.tipoduracao,ucrslinPresc.prescNomeCod,ucrslinPresc.prescDateCod)
		Replace ucrsLinPresc.validade 			WITH lcDataValidade
		Replace ucrsLinPresc.renovavel			WITH IIF(!EMPTY(ucrslinPresc.prescDateCod),.t.,.f.)
		
		
	ELSE
		&& temp fix para adicionar outros produtos em RSPs
		SELECT ucrsLinPresc
		lcCabstamp = ucrsLinPresc.prescstamp
	ENDIF	
	
	&& adiciona cabe�alho em rsps caso n�o exista - LL 20/12/2016
	IF EMPTY(myPrescMaterializada)

		SELECT uCrsLinPresc
		GO TOP 
		LOCATE FOR uCrsLinPresc.Pos = 0
		IF !FOUND()
			lcCabstamp = uf_gerais_stamp()
			lcLinstamp = uf_gerais_stamp()
			lcstampEnvioOnline = uf_gerais_stamp()
			lcPrescStampAux =  lcCabstamp 

			Select ucrsLinPresc
			APPEND BLANK 
			Replace ucrsLinPresc.prescstamp WITH lcCabstamp
			Replace ucrsLinPresc.presclstamp WITH lcLinstamp
			Replace ucrsLinPresc.stampEnvioOnline WITH lcstampEnvioOnline
		
			Replace ucrsLinPresc.design WITH uf_prescricao_textoTipoReceita("LOUT")
			Replace ucrsLinPresc.qtt 	WITH 1
			Replace ucrsLinPresc.lordem WITH uf_prescricao_CalculaPrimeiraOrdemTipoReceita("LOUT",.f.)
			Replace ucrsLinPresc.TipoReceita WITH "LOUT"
			Replace ucrsLinPresc.nrPrescricao WITH prescricao.nrPrescricao
			Replace ucrsLinPresc.terminal WITH Prescricao.terminal
			Replace ucrsLinPresc.vias WITH 1
**			Replace ucrsLinPresc.validade WITH UcrsValidadePresc.validade
			Replace ucrsLinPresc.estado WITH "0"
			Replace ucrsLinPresc.estadodescr WITH "Receita n�o submetida"	
			
			
			lcDataValidade = uf_prescricao_PreencheValidadePorLinha(myPrescMaterializada,ucrslinPresc.tipoduracao,ucrslinPresc.prescNomeCod,ucrslinPresc.prescDateCod)
			Replace ucrsLinPresc.validade 			WITH lcDataValidade
			Replace ucrsLinPresc.renovavel			WITH IIF(!EMPTY(ucrslinPresc.prescDateCod),.t.,.f.)
				
		ENDIF
	ENDIF
	

	
	** Controla Tipos de Receita
	SELECT TOP 1 prescstamp, tipoduracao as tipo_duracao FROM ucrsLinPresc WHERE ALLTRIM(UPPER(ucrsLinPresc.TipoReceita)) == "OUT" ORDER BY lordem DESC INTO CURSOR ucrsUltimoStamp2 READWRITE 
	SELECT ucrsUltimoStamp2
	IF !EMPTY(ALLTRIM(ucrsUltimoStamp2.prescstamp))		
		lcCabstamp = ucrsUltimoStamp2.prescstamp
	ENDIF 
	
	IF !EMPTY(myPrescMaterializada)
		SELECT COUNT(presclstamp) as numOUT FROM ucrsLinPresc WHERE ALLTRIM(ucrsLinPresc.prescstamp) == ALLTRIM(ucrsUltimoStamp2.prescstamp) INTO CURSOR ucrsControlaLinhasOutrosPorReceita READWRITE
		SELECT ucrsControlaLinhasOutrosPorReceita 
		IF ucrsControlaLinhasOutrosPorReceita.numOUT > 4
			
			lcCabstamp = uf_gerais_stamp()
			lcLinstamp = uf_gerais_stamp()
			lcstampEnvioOnline = uf_gerais_stamp()
			
			Select ucrsLinPresc
			APPEND BLANK 
			Replace ucrsLinPresc.prescstamp 		WITH lcCabstamp 
			Replace ucrsLinPresc.presclstamp 		WITH lcLinstamp 
			Replace ucrsLinPresc.stampEnvioOnline 	WITH lcstampEnvioOnline
			
			Replace ucrsLinPresc.design 				WITH uf_prescricao_textoTipoReceita("OUT")
			Replace ucrsLinPresc.qtt 					WITH 1
			Replace ucrsLinPresc.lordem 				WITH uf_prescricao_CalculaPrimeiraOrdemTipoReceita(IIF(!EMPTY(myPrescMaterializada),"OUT","LOUT"),.f.)
			Replace ucrsLinPresc.TipoReceita 			WITH "OUT"
			Replace ucrsLinPresc.nrPrescricao 			WITH prescricao.nrPrescricao
			Replace ucrsLinPresc.terminal 				WITH Prescricao.terminal
			Replace ucrsLinPresc.vias 					WITH 1
			**Replace ucrsLinPresc.validade 				WITH UcrsValidadePresc.validade
			Replace ucrsLinPresc.estado 				WITH "0"
			Replace ucrsLinPresc.estadodescr 			WITH "Receita n�o submetida"
			
			lcDataValidade = uf_prescricao_PreencheValidadePorLinha(myPrescMaterializada,ucrslinPresc.tipoduracao,ucrslinPresc.prescNomeCod,ucrslinPresc.prescDateCod)
			Replace ucrsLinPresc.validade 			WITH lcDataValidade
			Replace ucrsLinPresc.renovavel			WITH IIF(!EMPTY(ucrslinPresc.prescDateCod),.t.,.f.)
			
			
		ENDIF	 
	ENDIF
	

		
	lcLinstamp = uf_gerais_stamp()
	lcstampEnvioOnline = uf_gerais_stamp()
	
	SELECT ucrsLinPresc
	APPEND BLANK 

	**N�mero Atendimento
	Replace ucrsLinPresc.nrPrescricao		WITH prescricao.nrPrescricao			
	Replace ucrsLinPresc.terminal 			WITH Prescricao.terminal
	Replace ucrsLinPresc.prescstamp 		WITH IIF(!EMPTY(myPrescMaterializada),lcCabstamp, lcPrescStampAux)
	Replace ucrsLinPresc.presclstamp 		WITH lcLinstamp 
	Replace ucrsLinPresc.stampEnvioOnline 	WITH lcstampEnvioOnline
	
	SELECT ucrsLinPresc 
	Replace ucrsLinPresc.TipoReceita 		WITH IIF(!EMPTY(myPrescMaterializada), "OUT", "LOUT")
	Replace ucrsLinPresc.ref 				WITH "99999"
	Replace ucrsLinPresc.design 			WITH IIF(!EMPTY(myPrescMaterializada), "OUT.", "LOUT.")
	Replace ucrsLinPresc.qtt 				WITH 1
	Replace ucrsLinPresc.lordem 			WITH IIF(!EMPTY(myPrescMaterializada), uf_prescricao_CalculaPrimeiraOrdemTipoReceita("OUT",.f.), uf_prescricao_CalculaPrimeiraOrdemTipoReceita("LOUT",.f.))
	
	Replace ucrsLinPresc.vias 				WITH 1
	Replace ucrsLinPresc.design 			WITH ""
**	Replace ucrsLinPresc.validade 			WITH UcrsValidadePresc.validade
	Replace ucrsLinPresc.estado 			WITH "0"
	Replace ucrsLinPresc.estadodescr 		WITH ""
	
	lcDataValidade = uf_prescricao_PreencheValidadePorLinha(myPrescMaterializada,ucrslinPresc.tipoduracao,ucrslinPresc.prescNomeCod,ucrslinPresc.prescDateCod)
	Replace ucrsLinPresc.validade 			WITH lcDataValidade
	Replace ucrsLinPresc.renovavel			WITH IIF(!EMPTY(ucrslinPresc.prescDateCod),.t.,.f.)

		
	&& recria ordena��o
	SELECT ucrsLinPresc
	index on lordem TAG lordem 
	regua(1, 3, "A adicionar....", .f.)
	
	**	
	uf_prescricao_CalculaPosReceita()
	**
	uf_prescricao_actualizaQttTotalReceita()
	**
	uf_prescricao_colocaNaLinha(lcLinstamp)
	**
	**
	IF USED("ucrsUltimoStamp2")
		fecha("ucrsUltimoStamp2")
	ENDIF
	IF USED("ucrsControlaLinhasManipuladosPorReceita")
		fecha("ucrsControlaLinhasManipuladosPorReceita")
	ENDIF
	
	regua(1, 4, "A adicionar....", .f.)
	regua(2)

	uf_prescricao_actualizaAlertasCoresMed()

	IF Prescricao.detalhesVisiveis == .t.
		uf_prescricao_expandeDetalhe()
	ENDIF

	Prescricao.grdPresc.refresh
	Prescricao.grdPresc.setfocus
	Prescricao.grdPresc.click
	Prescricao.pageframe1.page3.pageframeMedicamentos.ActivePage = 8
	
ENDFUNC 


**
FUNCTION uf_prescricao_adicionaGenAlimenticio
	LOCAL lcTextoTipoReceita, lcControlaNumEmb, lcLinstamp , lcDataValidade 
	STORE '' TO lcTextoTipoReceita, lcLinstamp
	STORE 0 TO lcOrdem
	STORE .f. TO lcControlaNumEmb

	regua(0, 5, "A adicionar....", .f.)
	regua(1, 1, "A adicionar....", .f.)
	
	** Tipos de Receita **
	** MDT - Receita de Produtos Diet�ticos
	
	&& apaga ordena��o necess�rio para n�o dar erro
	Select ucrsLinPresc
	DELETE TAG ALL

	* Calula validade  - por defeito coloca 30 dias na validade para receitas n�o renovaveis
**	lcSQL = ""
**	TEXT TO lcSQL NOSHOW TEXTMERGE
**		Select DATEADD(dd,30,'<<uf_gerais_getdate(ucrscabpresc.data,"SQL")>>') as validade
**	ENDTEXT
**	IF !uf_gerais_actGrelha("", "UcrsValidadePresc", lcSql)
**		uf_perguntalt_chama("N�o foi possivel calcular a validade da receita. Contacte o suporte.", "", "OK", 16) 
**		RETURN .f.
**	ENDIF
	****
	lcDataValidade = uf_prescricao_PreencheValidadePorLinha(myPrescMaterializada,ucrslinPresc.tipoduracao,ucrslinPresc.prescNomeCod,ucrslinPresc.prescDateCod)
	
	regua(1, 2, "A adicionar....", .f.)	
		
	Select ucrsLinPresc
	GO Top	
	LOCATE FOR ALLTRIM(UPPER(ucrsLinPresc.TipoReceita)) == "MDT"
	IF !FOUND()
		lcCabstamp = uf_gerais_stamp()
		lcLinstamp = uf_gerais_stamp()
		lcstampEnvioOnline = uf_gerais_stamp()

		Select ucrsLinPresc
		APPEND BLANK 
		Replace ucrsLinPresc.prescstamp WITH lcCabstamp
		Replace ucrsLinPresc.presclstamp WITH lcLinstamp
		Replace ucrsLinPresc.stampEnvioOnline WITH lcstampEnvioOnline
		
		Replace ucrsLinPresc.design WITH uf_prescricao_textoTipoReceita("MDT")
		Replace ucrsLinPresc.qtt 	WITH 1
		Replace ucrsLinPresc.lordem WITH uf_prescricao_CalculaPrimeiraOrdemTipoReceita("MDT",.f.)
		Replace ucrsLinPresc.TipoReceita WITH "MDT"
		Replace ucrsLinPresc.nrPrescricao WITH prescricao.nrPrescricao
		Replace ucrsLinPresc.terminal WITH Prescricao.terminal
		Replace ucrsLinPresc.vias WITH 1
**		Replace ucrsLinPresc.validade WITH UcrsValidadePresc.validade
		Replace ucrsLinPresc.estado WITH "0"
		Replace ucrsLinPresc.estadodescr WITH "Receita n�o submetida"
		
		Replace ucrsLinPresc.validade 			WITH lcDataValidade
		Replace ucrsLinPresc.renovavel			WITH IIF(!EMPTY(ucrslinPresc.prescDateCod),.t.,.f.)
		
	ELSE
		lcCabstamp = ucrsLinPresc.prescstamp
	ENDIF	
	
	
	** Controla Tipos de Receita
	SELECT TOP 1 prescstamp, tipoduracao as tipo_duracao FROM ucrsLinPresc WHERE ALLTRIM(UPPER(ucrsLinPresc.TipoReceita)) == "MDT" ORDER BY lordem DESC INTO CURSOR ucrsUltimoStamp2 READWRITE 
	SELECT ucrsUltimoStamp2
	IF !EMPTY(ALLTRIM(ucrsUltimoStamp2.prescstamp))		
		lcCabstamp = ucrsUltimoStamp2.prescstamp
	ENDIF 
	
	SELECT COUNT(presclstamp) as numMDT FROM ucrsLinPresc WHERE ALLTRIM(ucrsLinPresc.prescstamp) == ALLTRIM(ucrsUltimoStamp2.prescstamp) INTO CURSOR ucrsControlaLinhasMDTPorReceita READWRITE
	SELECT ucrsControlaLinhasMDTPorReceita 
	IF ucrsControlaLinhasMDTPorReceita.numMDT > 4
		
		lcCabstamp = uf_gerais_stamp()
		lcLinstamp = uf_gerais_stamp()
		lcstampEnvioOnline = uf_gerais_stamp()
		
		Select ucrsLinPresc
		APPEND BLANK 
		Replace ucrsLinPresc.prescstamp WITH lcCabstamp 
		Replace ucrsLinPresc.presclstamp WITH lcLinstamp 
		Replace ucrsLinPresc.stampEnvioOnline WITH lcstampEnvioOnline
		
		Replace ucrsLinPresc.design WITH uf_prescricao_textoTipoReceita("MDT")
		Replace ucrsLinPresc.qtt 	WITH 1
		Replace ucrsLinPresc.lordem WITH uf_prescricao_CalculaPrimeiraOrdemTipoReceita("MDT",.f.)
		Replace ucrsLinPresc.TipoReceita WITH "MDT"
		Replace ucrsLinPresc.nrPrescricao WITH prescricao.nrPrescricao
		Replace ucrsLinPresc.terminal WITH Prescricao.terminal
		Replace ucrsLinPresc.vias WITH 1
		Replace ucrsLinPresc.validade WITH lcDataValidade
		Replace ucrsLinPresc.renovavel WITH IIF(!EMPTY(ucrslinPresc.prescDateCod),.t.,.f.)
		Replace ucrsLinPresc.estado WITH "0"
		Replace ucrsLinPresc.estadodescr WITH "Receita n�o submetida"
	ENDIF	 
		
	lcLinstamp = uf_gerais_stamp()
	lcstampEnvioOnline = uf_gerais_stamp()
	
	SELECT ucrsLinPresc
	APPEND BLANK 

	**N�mero Atendimento
	Replace ucrsLinPresc.nrPrescricao WITH prescricao.nrPrescricao			
	Replace ucrsLinPresc.terminal WITH Prescricao.terminal
			
	** STAMPS
	Replace ucrsLinPresc.prescstamp 		WITH lcCabstamp 
	Replace ucrsLinPresc.presclstamp 		WITH lcLinstamp 
	Replace ucrsLinPresc.stampEnvioOnline 	WITH lcstampEnvioOnline
	
		
	SELECT ucrsLinPresc 
	Replace ucrsLinPresc.TipoReceita 		WITH "MDT"
	Replace ucrsLinPresc.ref 				WITH "99999"
	Replace ucrsLinPresc.design 			WITH "MDT."
	Replace ucrsLinPresc.qtt 				WITH 1
	Replace ucrsLinPresc.lordem 			WITH uf_prescricao_CalculaPrimeiraOrdemTipoReceita("MDT",.f.)
	
	Replace ucrsLinPresc.vias 				WITH 1
	Replace ucrsLinPresc.design 			WITH ""
	Replace ucrsLinPresc.validade 			WITH lcDataValidade
	Replace ucrsLinPresc.estado 			WITH "0"
	Replace ucrsLinPresc.estadodescr 		WITH ""

		
	&& recria ordena��o
	SELECT ucrsLinPresc
	index on lordem TAG lordem 
	regua(1, 3, "A adicionar....", .f.)
	
	**	
	uf_prescricao_CalculaPosReceita()
	**
	uf_prescricao_actualizaQttTotalReceita()
	**
	uf_prescricao_colocaNaLinha(lcLinstamp)

	**
	IF USED("ucrsUltimoStamp2")
		fecha("ucrsUltimoStamp2")
	ENDIF
	IF USED("ucrsControlaLinhasManipuladosPorReceita")
		fecha("ucrsControlaLinhasManipuladosPorReceita")
	ENDIF
	
	regua(1, 4, "A adicionar....", .f.)
	regua(2)

	uf_prescricao_actualizaAlertasCoresMed()

	IF Prescricao.detalhesVisiveis == .t.
		uf_prescricao_expandeDetalhe()
	ENDIF

	Prescricao.grdPresc.refresh
	Prescricao.grdPresc.setfocus
	Prescricao.grdPresc.click
	Prescricao.pageframe1.page3.pageframeMedicamentos.ActivePage = 7
ENDFUNC 


**
FUNCTION uf_prescricao_adicionaManipulado
	LOCAL lcTextoTipoReceita, lcControlaNumEmb, lcLinstamp , lcDataValidade
	STORE '' TO lcTextoTipoReceita, lcLinstamp
	STORE 0 TO lcOrdem
	STORE .f. TO lcControlaNumEmb

	regua(0, 5, "A adicionar....", .f.)
	regua(1, 1, "A adicionar....", .f.)
	
	** Tipos de Receita **
	** MM - Receita de Medicamentos Manipulados
	
	&& apaga ordena��o necess�rio para n�o dar erro
	Select ucrsLinPresc
	DELETE TAG ALL

	* Calula validade  - por defeito coloca 30 dias na validade para receitas n�o renovaveis
**	lcSQL = ""
**	TEXT TO lcSQL NOSHOW TEXTMERGE
**		Select DATEADD(dd,30,'<<uf_gerais_getdate(ucrscabpresc.data,"SQL")>>') as validade
**	ENDTEXT
**	IF !uf_gerais_actGrelha("", "UcrsValidadePresc", lcSql)
**		uf_perguntalt_chama("N�o foi possivel calcular a validade da receita. Contacte o suporte.", "", "OK", 16) 
**		RETURN .f.
**	ENDIF
	****
	lcDataValidade = uf_prescricao_PreencheValidadePorLinha(myPrescMaterializada,ucrslinPresc.tipoduracao,ucrslinPresc.prescNomeCod,ucrslinPresc.prescDateCod)
	regua(1, 2, "A adicionar....", .f.)	
		
	SELECT ucrsLinPresc
	GO TOP 
	LOCATE FOR ALLTRIM(UPPER(ucrsLinPresc.TipoReceita)) == "MM" 
	IF !FOUND() AND !EMPTY(myPrescMaterializada)
		lcCabstamp = uf_gerais_stamp()
		lcLinstamp = uf_gerais_stamp()
		lcstampEnvioOnline = uf_gerais_stamp()

		Select ucrsLinPresc
		APPEND BLANK 
		Replace ucrsLinPresc.prescstamp WITH lcCabstamp
		Replace ucrsLinPresc.presclstamp WITH lcLinstamp
		Replace ucrsLinPresc.stampEnvioOnline WITH lcstampEnvioOnline
		
		Replace ucrsLinPresc.design WITH uf_prescricao_textoTipoReceita("MM")
		Replace ucrsLinPresc.qtt 	WITH 1
		Replace ucrsLinPresc.lordem WITH uf_prescricao_CalculaPrimeiraOrdemTipoReceita("MM",.f.)
		Replace ucrsLinPresc.TipoReceita WITH "MM"
		Replace ucrsLinPresc.nrPrescricao WITH prescricao.nrPrescricao
		Replace ucrsLinPresc.terminal WITH Prescricao.terminal
		Replace ucrsLinPresc.vias WITH 1
		Replace ucrsLinPresc.validade WITH lcDataValidade 
		Replace ucrsLinPresc.renovavel WITH IIF(!EMPTY(ucrslinPresc.prescDateCod),.t.,.f.)
		Replace ucrsLinPresc.estado WITH "0"
		Replace ucrsLinPresc.estadodescr WITH "Receita n�o submetida"
	ELSE
		&& temp fix
		SELECT ucrsLinPresc
		GO TOP 
		lcCabstamp = ucrsLinPresc.prescstamp
	ENDIF	
	

	
	** Controla Tipos de Receita
	SELECT TOP 1 prescstamp, tipoduracao as tipo_duracao FROM ucrsLinPresc WHERE ALLTRIM(UPPER(ucrsLinPresc.TipoReceita)) = "MM" ORDER BY lordem DESC INTO CURSOR ucrsUltimoStamp2 READWRITE 
	SELECT ucrsUltimoStamp2
	IF !EMPTY(ALLTRIM(ucrsUltimoStamp2.prescstamp))		
		lcCabstamp = ucrsUltimoStamp2.prescstamp
	ENDIF 
	
	SELECT COUNT(ref) as numManipulados FROM ucrsLinPresc WHERE ALLTRIM(ucrsLinPresc.prescstamp) == ALLTRIM(ucrsUltimoStamp2.prescstamp) INTO CURSOR ucrsControlaLinhasManipuladosPorReceita READWRITE
	SELECT ucrsControlaLinhasManipuladosPorReceita
	IF ucrsControlaLinhasManipuladosPorReceita.numManipulados > 4
		
		lcCabstamp = uf_gerais_stamp()
		lcLinstamp = uf_gerais_stamp()
		lcstampEnvioOnline = uf_gerais_stamp()
		
		Select ucrsLinPresc
		APPEND BLANK 
		Replace ucrsLinPresc.prescstamp 		WITH lcCabstamp 
		Replace ucrsLinPresc.presclstamp 		WITH lcLinstamp 
		Replace ucrsLinPresc.stampEnvioOnline 	WITH lcstampEnvioOnline
		
		Replace ucrsLinPresc.design 		WITH uf_prescricao_textoTipoReceita("MM")
		Replace ucrsLinPresc.qtt 			WITH 1
		Replace ucrsLinPresc.lordem 		WITH uf_prescricao_CalculaPrimeiraOrdemTipoReceita("MM",.f.)
		Replace ucrsLinPresc.TipoReceita 	WITH "MM"
		Replace ucrsLinPresc.nrPrescricao 	WITH prescricao.nrPrescricao
		Replace ucrsLinPresc.terminal 		WITH Prescricao.terminal
		Replace ucrsLinPresc.vias 			WITH 1
		Replace ucrsLinPresc.validade 		WITH lcDataValidade 
		Replace ucrsLinPresc.renovavel		WITH IIF(!EMPTY(ucrslinPresc.prescDateCod),.t.,.f.)
		Replace ucrsLinPresc.estado 		WITH "0"
		Replace ucrsLinPresc.estadodescr 	WITH "Receita n�o submetida"
	ENDIF	 
		
	lcLinstamp = uf_gerais_stamp()
	lcstampEnvioOnline = uf_gerais_stamp()
	
	SELECT ucrsLinPresc
	APPEND BLANK 

	**N�mero Atendimento
	Replace ucrsLinPresc.nrPrescricao 	WITH prescricao.nrPrescricao			
	Replace ucrsLinPresc.terminal 		WITH Prescricao.terminal
			
	** STAMPS
	Replace ucrsLinPresc.prescstamp 		WITH lcCabstamp 
	Replace ucrsLinPresc.presclstamp 		WITH lcLinstamp 
	Replace ucrsLinPresc.stampEnvioOnline 	WITH lcstampEnvioOnline
	
		
	SELECT ucrsLinPresc 
	Replace ucrsLinPresc.TipoReceita 		WITH IIF(!EMPTY(myPrescMaterializada),"MM", "LMM")
	Replace ucrsLinPresc.ref 				WITH "99999"
	Replace ucrsLinPresc.design 			WITH IIF(!EMPTY(myPrescMaterializada),"MM.", "LMM.")
	Replace ucrsLinPresc.qtt 				WITH 1
	Replace ucrsLinPresc.lordem 			WITH IIF(!EMPTY(myPrescMaterializada), uf_prescricao_CalculaPrimeiraOrdemTipoReceita("MM",.f.), uf_prescricao_CalculaPrimeiraOrdemTipoReceita("LMM",.f.))
	
	Replace ucrsLinPresc.vias 				WITH 1
	Replace ucrsLinPresc.design 			WITH ""
	Replace ucrsLinPresc.manipulado			WITH .t.
	Replace ucrsLinPresc.validade 			WITH lcDataValidade 
	Replace ucrsLinPresc.estado 			WITH "0"
	Replace ucrsLinPresc.estadodescr 		WITH ""

	&& recria ordena��o
	SELECT ucrsLinPresc
	index on lordem TAG lordem 
	regua(1, 3, "A adicionar....", .f.)
	
	**	
	uf_prescricao_CalculaPosReceita()
	**
	uf_prescricao_actualizaQttTotalReceita()
	**
	uf_prescricao_colocaNaLinha(lcLinstamp)

	**
	IF USED("ucrsUltimoStamp2")
		fecha("ucrsUltimoStamp2")
	ENDIF
	IF USED("ucrsControlaLinhasManipuladosPorReceita ")
		fecha("ucrsControlaLinhasManipuladosPorReceita ")
	ENDIF
	
	regua(1, 4, "A adicionar....", .f.)
	regua(2)

	uf_prescricao_actualizaAlertasCoresMed()

	IF Prescricao.detalhesVisiveis == .t.
		uf_prescricao_expandeDetalhe()
	ENDIF

	Prescricao.grdPresc.refresh
	Prescricao.grdPresc.setfocus
	Prescricao.grdPresc.click
	Prescricao.pageframe1.page3.pageframeMedicamentos.ActivePage = 6
ENDFUNC


**

FUNCTION uf_prescricao_adicionaTipoUE
	LOCAL lcTextoTipoReceita, lcControlaNumEmb, lcLinstamp,lcDataValidade
	STORE '' TO lcTextoTipoReceita, lcLinstamp
	STORE 0 TO lcOrdem
	STORE .f. TO lcControlaNumEmb

	regua(0, 5, "A adicionar....", .f.)
	regua(1, 1, "A adicionar....", .f.)
	
	** Tipos de Receita **
	** UE - Receita para aquisi��o noutro estado membro
	
	&& apaga ordena��o necess�rio para n�o dar erro
	Select ucrsLinPresc
	DELETE TAG ALL

	* Calula validade  - por defeito coloca 30 dias na validade para receitas n�o renovaveis
**	lcSQL = ""
**	TEXT TO lcSQL NOSHOW TEXTMERGE
**		Select DATEADD(dd,30,'<<uf_gerais_getdate(ucrscabpresc.data,"SQL")>>') as validade
**	ENDTEXT
**	IF !uf_gerais_actGrelha("", "uCrsValidadePresc", lcSql)
**		uf_perguntalt_chama("N�o foi possivel calcular a validade da receita. Contacte o suporte.", "", "OK", 16) 
**		RETURN .f.
**	ENDIF
	****
	lcDataValidade = uf_prescricao_PreencheValidadePorLinha(myPrescMaterializada,ucrslinPresc.tipoduracao,ucrslinPresc.prescNomeCod,ucrslinPresc.prescDateCod)
	
	regua(1, 2, "A adicionar....", .f.)	
		
	Select ucrsLinPresc
	GO TOP 	
	LOCATE FOR ALLTRIM(UPPER(ucrsLinPresc.TipoReceita)) == "UE"
	IF !FOUND()
		lcCabstamp = uf_gerais_stamp()
		lcLinstamp = uf_gerais_stamp()
		lcstampEnvioOnline = uf_gerais_stamp()

		Select ucrsLinPresc
		APPEND BLANK 
		Replace ucrsLinPresc.prescstamp 		WITH lcCabstamp
		Replace ucrsLinPresc.presclstamp 		WITH lcLinstamp
		Replace ucrsLinPresc.stampEnvioOnline 	WITH lcstampEnvioOnline
		
		Replace ucrsLinPresc.design 		WITH uf_prescricao_textoTipoReceita("UE")
		Replace ucrsLinPresc.qtt 	WITH 1
		Replace ucrsLinPresc.lordem WITH uf_prescricao_CalculaPrimeiraOrdemTipoReceita("MM",.f.)
		Replace ucrsLinPresc.TipoReceita WITH "MM"
		Replace ucrsLinPresc.nrPrescricao WITH prescricao.nrPrescricao
		Replace ucrsLinPresc.terminal WITH Prescricao.terminal
		Replace ucrsLinPresc.vias WITH 1
		Replace ucrsLinPresc.validade WITH lcDataValidade
		Replace ucrsLinPresc.renovavel WITH IIF(!EMPTY(ucrslinPresc.prescDateCod),.t.,.f.)
		Replace ucrsLinPresc.estado WITH "0"
		Replace ucrsLinPresc.estadodescr WITH "Receita n�o submetida"
	ELSE
		lcCabstamp = ucrsLinPresc.prescstamp
	ENDIF	
	
	
	** Controla Tipos de Receita
	SELECT TOP 1 prescstamp, tipoduracao as tipo_duracao FROM ucrsLinPresc WHERE ALLTRIM(UPPER(ucrsLinPresc.TipoReceita)) = "MM" ORDER BY lordem DESC INTO CURSOR ucrsUltimoStamp2 READWRITE 
	SELECT ucrsUltimoStamp2
	IF !EMPTY(ALLTRIM(ucrsUltimoStamp2.prescstamp))		
		lcCabstamp = ucrsUltimoStamp2.prescstamp
	ENDIF 
	
	SELECT COUNT(ref) as numManipulados FROM ucrsLinPresc WHERE ALLTRIM(ucrsLinPresc.prescstamp) == ALLTRIM(ucrsUltimoStamp2.prescstamp) INTO CURSOR ucrsControlaLinhasManipuladosPorReceita READWRITE
	SELECT ucrsControlaLinhasManipuladosPorReceita
	IF ucrsControlaLinhasManipuladosPorReceita.numManipulados > 4
		
		lcCabstamp = uf_gerais_stamp()
		lcLinstamp = uf_gerais_stamp()
		lcstampEnvioOnline = uf_gerais_stamp()
		
		Select ucrsLinPresc
		APPEND BLANK 
		Replace ucrsLinPresc.prescstamp WITH lcCabstamp 
		Replace ucrsLinPresc.presclstamp WITH lcLinstamp 
		Replace ucrsLinPresc.stampEnvioOnline WITH lcstampEnvioOnline
		
		Replace ucrsLinPresc.design WITH uf_prescricao_textoTipoReceita("MM")
		Replace ucrsLinPresc.qtt 	WITH 1
		Replace ucrsLinPresc.lordem WITH uf_prescricao_CalculaPrimeiraOrdemTipoReceita("MM",.f.)
		Replace ucrsLinPresc.TipoReceita WITH "MM"
		Replace ucrsLinPresc.nrPrescricao WITH prescricao.nrPrescricao
		Replace ucrsLinPresc.terminal WITH Prescricao.terminal
		Replace ucrsLinPresc.vias WITH 1
		Replace ucrsLinPresc.validade WITH lcDataValidade
		Replace ucrsLinPresc.renovavel WITH IIF(!EMPTY(ucrslinPresc.prescDateCod),.t.,.f.)
		Replace ucrsLinPresc.estado WITH "0"
		Replace ucrsLinPresc.estadodescr WITH "Receita n�o submetida"
	ENDIF	 
		
	lcLinstamp = uf_gerais_stamp()
	lcstampEnvioOnline = uf_gerais_stamp()
	
	SELECT ucrsLinPresc
	APPEND BLANK 

	**N�mero Atendimento
	Replace ucrsLinPresc.nrPrescricao WITH prescricao.nrPrescricao			
	Replace ucrsLinPresc.terminal WITH Prescricao.terminal
			
	** STAMPS
	Replace ucrsLinPresc.prescstamp 		WITH lcCabstamp 
	Replace ucrsLinPresc.presclstamp 		WITH lcLinstamp 
	Replace ucrsLinPresc.stampEnvioOnline 	WITH lcstampEnvioOnline
	
		
	SELECT ucrsLinPresc 
	Replace ucrsLinPresc.TipoReceita 		WITH "MM"
	Replace ucrsLinPresc.ref 				WITH "99999"
	Replace ucrsLinPresc.design 			WITH "MM."
	Replace ucrsLinPresc.qtt 				WITH 1
	Replace ucrsLinPresc.lordem 			WITH uf_prescricao_CalculaPrimeiraOrdemTipoReceita("MM",.f.)
	
	Replace ucrsLinPresc.vias 				WITH 1
	Replace ucrsLinPresc.design 			WITH ""
	Replace ucrsLinPresc.manipulado			WITH .t.
	Replace ucrsLinPresc.validade 			WITH lcDataValidade
	Replace ucrsLinPresc.renovavel			WITH IIF(!EMPTY(ucrslinPresc.prescDateCod),.t.,.f.)
	Replace ucrsLinPresc.estado 			WITH "0"
	Replace ucrsLinPresc.estadodescr 		WITH ""

		
	&& recria ordena��o
	SELECT ucrsLinPresc
	index on lordem TAG lordem 
	regua(1, 3, "A adicionar....", .f.)
	
	**	
	uf_prescricao_CalculaPosReceita()
	**
	uf_prescricao_actualizaQttTotalReceita()
	**
	uf_prescricao_colocaNaLinha(lcLinstamp)

	**
	IF USED("ucrsUltimoStamp2")
		fecha("ucrsUltimoStamp2")
	ENDIF
	IF USED("ucrsControlaLinhasManipuladosPorReceita ")
		fecha("ucrsControlaLinhasManipuladosPorReceita ")
	ENDIF
	
	regua(1, 4, "A adicionar....", .f.)
	regua(2)

	uf_prescricao_actualizaAlertasCoresMed()

	IF Prescricao.detalhesVisiveis == .t.
		uf_prescricao_expandeDetalhe()
	ENDIF

	Prescricao.grdPresc.refresh
	Prescricao.grdPresc.setfocus
	Prescricao.grdPresc.click
	Prescricao.pageframe1.page3.pageframeMedicamentos.ActivePage = 6
ENDFUNC



**
FUNCTION uf_prescricao_aplicarDesignOutros
	IF LEN(ALLTRIM(ucrsLinPresc.designOutros)) < 240
		SELECT ucrsLinPresc
		Replace ucrsLinPresc.design WITH "OUT." + ALLTRIM(ucrsLinPresc.designOutros)
		Prescricao.grdPresc.refresh
	ELSE
		uf_perguntalt_chama("N�o � possivel atribuir uma descri��o superior a 240 caracteres. Por favor verifique.", "", "OK", 64)
	ENDIF
ENDFUNC 



**
FUNCTION uf_prescricao_aplicarDesignGenAlimenticios
	IF LEN(ALLTRIM(ucrsLinPresc.designMDT)) < 240
		SELECT ucrsLinPresc
		Replace ucrsLinPresc.design WITH "MDT." + ALLTRIM(ucrsLinPresc.designMDT)
		Prescricao.grdPresc.refresh
	ELSE
		uf_perguntalt_chama("N�o � possivel atribuir uma descri��o superior a 240 caracteres. Por favor verifique.", "", "OK", 64)
	ENDIF
ENDFUNC 

** Usa  java para mover o cursosr do rato

FUNCTION uf_prescricao_sendMouseMove
	
	LOCAL lcQrCodePath,lcWsPath,lcDir 
	STORE '' TO lcQrCodePath, lcWsPath
	
	lcDir=ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\PE\RPM_RSP'
	
	
	IF DIRECTORY(ALLTRIM(lcDir))
		lcWsPath = lcDir + '\MouseMover.jar"'	
		
		lcWsPath = "javaw -jar " + lcWsPath 
		oWSShell = CREATEOBJECT("WScript.Shell")
		oWSShell.Run(lcWsPath,1,.t.)
	ENDIF
	
	
ENDFUNC 	


**
FUNCTION uf_prescricao_aplicarDesignManipulado
	IF LEN(ALLTRIM(ucrsLinPresc.designManipulado)) < 240
		SELECT ucrsLinPresc
		Replace ucrsLinPresc.design WITH "MM." + ALLTRIM(ucrsLinPresc.designManipulado)
		Prescricao.grdPresc.refresh
	ELSE
		uf_perguntalt_chama("N�o � possivel atribuir uma descri��o superior a 240 caracteres. Por favor verifique.", "", "OK", 64)
	ENDIF	
ENDFUNC


**
FUNCTION uf_prescricao_aplicaEstadoDescricaoCab
	SELECT ucrsLinPresc
	GO TOP 
	SCAN FOR LEFT(ALLTRIM(ucrsLinPresc.design),1)=="." 
		DO CASE
			CASE ucrsLinPresc.anulado == .f.
				Replace ucrsLinPresc.design WITH ALLTRIM(ucrsLinPresc.design) + " [" + ALLTRIM(ucrsLinPresc.estadodescr) + "]" 
			OTHERWISE 
				Replace ucrsLinPresc.design WITH ALLTRIM(ucrsLinPresc.design) + " [Anulado]" 
		ENDCASE 
	ENDSCAN 
	
	SELECT ucrsLinPresc
	GO TOP 
ENDFUNC 


**
FUNCTION uf_prescricao_imprimeBC
	LPARAMETERS lcPorDci, lcCNPEM, lcRef 
	
	IF lcRef == "99999"
		RETURN .f.
	ENDIF 
		
	IF lcPorDci == .t.
		IF !EMPTY(ALLTRIM(lcCNPEM))
			RETURN .t.
		ENDIF
	ELSE
		IF !EMPTY(ALLTRIM(lcRef))
			RETURN .t.
		ENDIF
	ENDIF

	RETURN .f.
ENDFUNC


**
FUNCTION uf_prescricao_SINCRONIZARNUIndividual

	
	LPARAMETERS lcutstamp
	LOCAL lcTeste

	**
	IF USED("ucrsUtentesSincRNU")
		fecha("ucrsUtentesSincRNU")
	ENDIF
	
	CREATE CURSOR ucrsUtentesSincRNU(utstamp c(36), respostastamp c(36), executado l)

	**Verifica Internet
	IF !uf_gerais_verifyInternet()
		uf_perguntalt_chama("ESTA FUNCIONALIDADE REQUER LIGA��O � INTERNET."+CHR(13)+"Por favor valide.","OK","",64)
		RETURN .f.
	ENDIF

	TEXT TO lcSQL TEXTMERGE noshow
		exec up_utentes_pesquisa <<IIF(EMPTY(ALLTRIM(lcutstamp)) or ISNULL(lcutstamp),'null',"'" + ALLTRIM(lcutstamp) +"'")>>,'','','',''
	ENDTEXT
	
	uf_gerais_actGrelha("", "uCrsPesquisarUT_RNU", lcSQL)
	
	IF EMPTY(ALLTRIM(uCrsPesquisarUT_RNU.nbenef))
		RETURN .f.
	ENDIF

					
	SELECT uCrsPesquisarUT_RNU 
	GO Top
	SCAN
				
		lcStamp = uf_gerais_stamp()
		SELECT ucrsUtentesSincRNU
		APPEND BLANK
		Replace ucrsUtentesSincRNU.utstamp WITH uCrsPesquisarUT_RNU.utstamp
		Replace ucrsUtentesSincRNU.respostastamp  WITH lcStamp
		
		
		IF uf_gerais_getParameter("ADM0000000227","BOOL")	
			lcTeste = 1
		ELSE
			lcTeste = 0
		ENDIF
			
			
		
		
		
		IF DIRECTORY(ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\PE\RNU')
			
				
				
			IF !FILE(ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\PE\RNU\RNU.jar')
				uf_perguntalt_chama("Ficheiro configura��o n�o encontrado. Contacte o Suporte","OK","",64)
				RETURN .f.
			ENDIF
			
			lcWsPath = ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\PE\RNU\RNU.jar';
			 	+ ' "--connStamp=' + ALLTRIM(lcStamp) + ["];
			 	+ ' "--usernr=' + ALLTRIM(uCrsPesquisarUT_RNU.nbenef) + ["];
			 	+ ' "--cardnr=' + uCrsPesquisarUT_RNU.cesdcart+ ["];
			 	+ ' "--cardtype=' + IIF(!EMPTY(uCrsPesquisarUT_RNU.cesd),"CESD","") + ["];
			 	+ ' "--idCL=' + ALLTRIM(sql_db) + ["];
			 	+ ' "--test=' + ALLTRIM(STR(lcTeste)) + ["]
	
			lcWsPath = "javaw -jar " + lcWsPath 
		
			
			oWSShell = CREATEOBJECT("WScript.Shell")
			oWSShell.Run(lcWsPath,1,.t.)
			**regua(1,2,"A obter dados do Registo Nacional de Utentes...")
		ENDIF
		
		SELECT uCrsPesquisarUT_RNU 
	ENDSCAN

	PRESCRICAO.tmrSinc.enabled = .t.
		
ENDFUNC


**
FUNCTION uf_prescricao_actualizaDadosSincronizados
	LPARAMETERS lcPainel
		
	LOCAL lcstampActual,lcRegistosSinc
	lcRegistosSinc = 0
	
	&lcPainel..ntmrSinc = &lcPainel..ntmrSinc + 1
	
	IF lcPainel == "PRESCRICAO"
		lctentativas = 10
	ELSE
		lctentativas = 10
	ENDIF
	
	IF &lcPainel..ntmrSinc > lctentativas  && 10 tentativas de 3 segundos
		
		uf_perguntalt_chama("N�o foi poss�vel obter resposta do RNU. Por favor volte a tentar sincronizar.","OK","",16)
		&lcPainel..tmrSinc.enabled = .f.
		&lcPainel..ntmrSinc = 0
		regua(2)
		RETURN .f.
	ENDIF
		
	
	SELECT * FROM ucrsUtentesSincRNU WHERE executado == .f. INTO CURSOR ucrsNProcessadosRNU READWRITE 

	**regua(1,5,"A obter dados do Registo Nacional de Utentes...")
	SELECT ucrsNProcessadosRNU
	IF RECCOUNT("ucrsNProcessadosRNU") == 0 
		uf_perguntalt_chama("Dados sincronizados com o RNU com sucesso.","OK","",64)
		&lcPainel..tmrSinc.enabled = .f.
		&lcPainel..ntmrSinc = 0
		regua(2)
		return .f.
	ENDIF
		
	SELECT ucrsUtentesSincRNU
	GO TOP
	SCAN FOR executado == .f.
		
		lcstampActual = ucrsUtentesSincRNU.utstamp
		
		TEXT TO lcSql NOSHOW textmerge
			exec up_prescricao_dados <<IIF(EMPTY(ALLTRIM(ucrsUtentesSincRNU.respostastamp)) or ISNULL(ucrsUtentesSincRNU.respostastamp),'null',"'" + ALLTRIM(ucrsUtentesSincRNU.respostastamp) +"'")>>
		ENDTEXT
		IF !uf_gerais_actGrelha("","uCrsVerificaRespostaUtente",lcSql)
			regua(2)
			uf_perguntalt_chama("OCORREU UM ERRO A VERIFICAR A RESPOSTA DO WEB SERVICE UTENTE. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			&lcPainel..tmrSinc.enabled = .f.
			&lcPainel..ntmrSinc = 0
			RETURN .f.
		ELSE
		
			SELECT uCrsVerificaRespostaUtente
			IF RECCOUNT("uCrsVerificaRespostaUtente") > 0
			
				SELECT uCrsVerificaRespostaUtente
				GO TOP
				
				*Verifica se n�o existe codigo de anomalia
				IF ALLTRIM(uCrsVerificaRespostaUtente.mensagemCodigo) != "2200101001"  AND ALLTRIM(uCrsVerificaRespostaUtente.mensagemCodigo) != "2200201001" 
					regua(2)
					uf_perguntalt_chama("N�o foi possivel actualizar dados do utente." + CHR(13) + "Motivo: "+ UPPER(ALLTRIM(uCrsVerificaRespostaUtente.mensagemDescricao)),"OK","",64)
					&lcPainel..tmrSinc.enabled = .f.
					&lcPainel..ntmrSinc = 0
					RETURN .f.
				ENDIF

				&& Cursor de EFRs
				lcSQL=''
				TEXT TO lcSQL TEXTMERGE NOSHOW 
					
					DELETE FROM b_cli_efrutente WHERE stamp = '<<ucrsUtentesSincRNU.utstamp>>'
					
					UPDATE 
						b_cli_efrutente
					SET 
						stamp = '<<ucrsUtentesSincRNU.utstamp>>'
					where 
						stamp = '<<ucrsUtentesSincRNU.respostastamp>>'
				ENDTEXT 
				IF !uf_gerais_actGrelha("","",lcSql)
					messagebox("N�o foi possivel sincronizar o utente com o n�mero de utente: " + ALLTRIM(uCrsVerificaRespostaUtente.NumeroSNS))
				ENDIF

				*Outros Beneficios
				lcSQL=''
				TEXT TO lcSQL TEXTMERGE NOSHOW 
					DELETE FROM b_cli_outrosBenef WHERE stamp = '<<ucrsUtentesSincRNU.utstamp>>'
				
					UPDATE 
						b_cli_outrosBenef
					SET 
						stamp = '<<ucrsUtentesSincRNU.utstamp>>'
					where 
						stamp = '<<ucrsUtentesSincRNU.respostastamp>>'
				ENDTEXT 
				IF !uf_gerais_actGrelha("","",lcSql)
					messagebox("N�o foi possivel sincronizar o utente com o n�mero de utente: " + ALLTRIM(uCrsVerificaRespostaUtente.NumeroSNS))
				ENDIF

				lcSQL = ''	
				TEXT TO lcSql NOSHOW textmerge
					exec up_prescricao_efrutente <<IIF(EMPTY(ALLTRIM(ucrsUtentesSincRNU.utstamp)) or ISNULL(ucrsUtentesSincRNU.utstamp),'null',"'" + ALLTRIM(ucrsUtentesSincRNU.utstamp) +"'")>>
				ENDTEXT

				IF !uf_gerais_actGrelha("","uCrsEFRs",lcSql)
					uf_perguntalt_chama("OCORREU UM ERRO A VERIFICAR OS OUTROS BENEF�CIOS.","OK","",16)
					&lcPainel..tmrSinc.enabled = .f.
					&lcPainel..ntmrSinc = 0
					RETURN .f.
					**	
				ENDIF

				lcInsereEFR  = .f.
				*EFR
				SELECT uCrsEFRs
				IF RECCOUNT("uCrsEFRs")> 0 
					SELECT uCrsEFRs
					GO Top
					
					lcentPla = uCrsEFRs.abrev 
					lccodPla = uCrsEFRs.Codigo
					lcdesPla = uCrsEFRs.Descricao
					lcnbenef2 = uCrsEFRs.numeroBenefEntidade
					lcvalipla2 = uCrsEFRs.DataValidade
				ELSE
				
					
					IF !EMPTY(uCrsVerificaRespostaUtente.NumeroSNS)
						lcInsereEFR = .t.
						lccodPla = "935601"
						lcentPla = "935601" 
						lcdesPla = 	"Servi�o Nacional de Sa�de" 
						lcnbenef2 = ""
						lcvalipla2 = ctod("1900.01.01")
					ELSE
						lccodPla = ""
						lcentPla = "" 
						lcdesPla = 	""
						lcnbenef2 = ""
						lcvalipla2 = ctod("1900.01.01")
					ENDIF
				ENDIF

				IF lcInsereEFR == .t.
					TEXT TO lcSQL NOSHOW TEXTMERGE
						DELETE FROM b_cli_efrutente WHERE stamp = '<<ucrsUtentesSincRNU.utstamp>>' 
						insert into 
							b_cli_efrutente (stamp, Codigo, Descricao, DataValidade, pais, Numcartao, TipoCartao)
						values(
							'<<ucrsUtentesSincRNU.utstamp>>'
							,'935601'
							,'Servi�o Nacional de Sa�de'
							,'19000101'
							,''
							,''
							,''
						)
					ENDTEXT
					IF !uf_gerais_actGrelha("","",lcSql)
						messagebox("N�o foi possivel sincronizar o utente com o n�mero de utente: " + ALLTRIM(uCrsVerificaRespostaUtente.NumeroSNS))
					ENDIF
				ENDIF
			
				lcSQL = ''	
				TEXT TO lcSql NOSHOW TEXTMERGE 
					exec up_prescricao_outrosBenef <<IIF(EMPTY(ALLTRIM(ucrsUtentesSincRNU.utstamp)) or ISNULL(ucrsUtentesSincRNU.utstamp),'null',"'" + ALLTRIM(ucrsUtentesSincRNU.utstamp) +"'")>>
				ENDTEXT

				IF !uf_gerais_actGrelha("","uCrsOutrosBenef",lcSql)
					uf_perguntalt_chama("OCORREU UM ERRO A VERIFICAR OS OUTROS BENEF�CIOS.","OK","",16)
					&lcPainel..tmrSinc.enabled = .f.
					&lcPainel..ntmrSinc = 0
					RETURN .f.
				ENDIF
					
				** Calculo do Regime de Comparticipa��o RECMTipo - est� dependente dos beneficios e outros beneficios
				** 
				lcCodigosBenf = ""
				SELECT uCrsOutrosBenef
				GO Top
				SCAN FOR !EMPTY(ALLTRIM(uCrsOutrosBenef.motivo))
					IF !EMPTY(ALLTRIM(lcCodigosBenf))
						lcCodigosBenf = ALLTRIM(lcCodigosBenf) + "," + ALLTRIM(uCrsOutrosBenef.motivo)
					ELSE
						lcCodigosBenf = ALLTRIM(uCrsOutrosBenef.motivo)
					ENDIF
				ENDSCAN 
	
				SELECT uCrsVerificaRespostaUtente
				IF !EMPTY(ALLTRIM(lcCodigosBenf))
					lcCodigosBenf = ALLTRIM(lcCodigosBenf) + "," + ALLTRIM(uCrsVerificaRespostaUtente.BenefRECMMotivo)
				ELSE
					lcCodigosBenf = ALLTRIM(uCrsVerificaRespostaUtente.BenefRECMMotivo)
				ENDIF
				lcSQL = ""
				TEXT TO lcSQL NOSHOW TEXTMERGE 
					exec up_prescricao_RecmTipo '<<lcCodigosBenf>>',0
				ENDTEXT
				IF uf_gerais_actGrelha("","uCrsTipoRECM",lcSQL)
					SELECT uCrsTipoRECM
					lcrecmtipo = uCrsTipoRECM.tipo
				ELSE
					lcrecmtipo = ""
				ENDIF
				**
	
				TEXT TO lcSQL NOSHOW TEXTMERGE 
					UPDATE
						b_utentes
					SET 
						nome = '<<ALLTRIM(uCrsVerificaRespostaUtente.NomeCompleto)>>'
						,nascimento = '<<uf_gerais_getdate(uCrsVerificaRespostaUtente.DataNascimento,"SQL")>>'
						,sexo = '<<uCrsVerificaRespostaUtente.Sexo>>'
						,codigoP = '<<ALLTRIM(uCrsVerificaRespostaUtente.PaisNacionalidade)>>'
						,codigoPNat	= '<<ALLTRIM(uCrsVerificaRespostaUtente.PaisNacionalidade)>>'
						,duplicado = '<<uCrsVerificaRespostaUtente.duplicado>>'
						,nomesproprios = '<<uCrsVerificaRespostaUtente.nomesproprios>>' 
						,obito = '<<uCrsVerificaRespostaUtente.obito>>'
						,apelidos = '<<uCrsVerificaRespostaUtente.apelidos>>'
						,nbenef =  '<<uCrsVerificaRespostaUtente.NumeroSNS>>'
						,recmmotivo	= '<<ALLTRIM(uCrsVerificaRespostaUtente.BenefRECMMotivo)>>'
						,recmdescricao = '<<ALLTRIM(uCrsVerificaRespostaUtente.BenefRECMDescricao)>>'
						,recmdatainicio	= '<<uf_gerais_getdate(uCrsVerificaRespostaUtente.BenefRECMDataInicio,"SQL")>>'
						,recmdatafim = '<<uf_gerais_getdate(uCrsVerificaRespostaUtente.BenefRECMDataFim,"SQL")>>'
						,itmotivo = '<<uCrsVerificaRespostaUtente.BenefITMMotivo>>'
						,itdescricao = '<<uCrsVerificaRespostaUtente.BenefITDescricao>>'
						,itdatainicio = '<<uf_gerais_getdate(uCrsVerificaRespostaUtente.BenefITDataInicio,"SQL")>>'
						,itdatafim = '<<uf_gerais_getdate(uCrsVerificaRespostaUtente.BenefITDataFim,"SQL")>>'
						,entPla = '<<ALLTRIM(lcentPla)>>'
						,codPla = '<<ALLTRIM(lccodPla)>>'
						,desPla = '<<ALLTRIM(lcdesPla)>>'
						,nbenef2 = '<<ALLTRIM(lcnbenef2)>>'
						,valipla2 = '<<uf_gerais_getdate(lcvalipla2,"SQL")>>'
						,recmtipo = '<<ALLTRIM(lcrecmtipo)>>'
					WHERE
						utstamp = '<<ucrsUtentesSincRNU.utstamp>>'
				ENDTEXT 

				IF !uf_gerais_actGrelha("","",lcSql)
					messagebox("N�o foi possivel sincronizar o utente com o n�mero de utente: " + ALLTRIM(uCrsVerificaRespostaUtente.NumeroSNS))
				ENDIF
			
				SELECT ucrsUtentesSincRNU
				Replace ucrsUtentesSincRNU.executado WITH .t.
			ENDIF
		ENDIF 
		
	ENDSCAN
	
	
	IF lcPainel == "PRESCRICAO" AND !EMPTY(lcstampActual)
		
		lcSQL = ""
		Text to lcSql noshow textmerge
			 exec up_utentes_pesquisa <<IIF(EMPTY(ALLTRIM(lcstampActual)) or ISNULL(lcstampActual),'null',"'" + ALLTRIM(lcstampActual) +"'")>>,'', '','', '' 
		ENDTEXT
		IF !uf_gerais_actgrelha("", [uCrsPesquisarCL], lcSQL)
			MESSAGEBOX("N�o foi poss�vel verificar informa��o do utente.",16,"LOGITOOLS SOFTWARE")
			RETURN .f.
		ENDIF
		
		SELECT uCrsPesquisarCL
		uf_pesqutentes_escolhePresc()
	ENDIF
	
ENDFUNC 


**
FUNCTION uf_prescricao_limpar
	**Limpa Cursores
	IF USED("ucrsCabPresc")
		SELECT ucrsCabPresc
		GO Top
		SCAN
			Delete		
		ENDSCAN
	ENDIF
	IF USED("ucrsLinPresc")
		SELECT ucrsLinPresc
		GO Top
		SCAN
			Delete		
		ENDSCAN
	ENDIF

	SELECT ucrsCabPresc
	APPEND BLANK
	Replace ucrsCabPresc.data WITH Date()

	lcnrPrescricao = uf_prescricao_geranrPrescricao()
	Prescricao.terminal = myTermno
	Prescricao.nrPrescricao = lcnrPrescricao 
	Prescricao.lblPrescricao.Caption = "Prescri��o: " + lcnrPrescricao
	Prescricao.lblPrescricao.refresh

	Replace ucrsCabPresc.nrprescricao With lcnrPrescricao

	uf_prescricao_ActualizaDadosUtilizadorCabecalho()
	uf_prescricao_alternaMenu()
ENDFUNC 


** Cancelar
FUNCTION uf_prescricao_sair
	IF prescricao.tmrAnulacao.enabled == .t. OR prescricao.tmrEnvioOffline.enabled == .t. OR prescricao.tmrEnvioOnline.enabled == .t.
		RETURN .f.
	ENDIF
	initpesq=0
	* Verifica se o Documento esta em Modo de Edicao ou Introducao
	If myPrescricaoIntroducao == .t. OR myPrescricaoAlteracao == .t.
	
		IF uf_perguntalt_chama("Quer cancelar as altera��es efectuadas?", "SIM", "N�O", 64) == .t.
			
			STORE .f. TO myPrescricaoIntroducao , myPrescricaoAlteracao 			
			uf_prescricao_alternaMenu()
			Prescricao.nrPrescricao = ""
			Prescricao.lblPrescricao.Caption = ""
			Prescricao.lblPrescricao.refresh
			Prescricao.grdPresc.click
			Prescricao.grdPresc.setfocus()
			uf_prescricao_ultimoRegisto()
	
		Endif
	
	ELSE
		uf_prescricao_exit()
	ENDIF
	
ENDFUNC


** 
Function UF_PRESCRICAO_nprescSns

	IF myPrescricaoIntroducao == .f. AND myPrescricaoAlteracao  == .f.
		RETURN .f.
	ENDIF 

	Select uCrsCabPresc
	IF !EMPTY(uCrsCabPresc.nprescsns)
		Select uCrsCabPresc
		Replace uCrsCabPresc.entidade		WITH "SEM COMPARTICIPACA��O P/ SNS"
		Replace uCrsCabPresc.entidadeDescr	WITH "Sem Comparticipa��o pelo SNS"
		replace uCrsCabPresc.entidadeCod	WITH "999998"
		Replace ucrsCabPresc.nrbenef WITH ""
	ELSE
	
		lcSQL = ""
		Text to lcSql noshow textmerge
			 exec up_utentes_pesquisa '<<ALLTRIM(ucrsCabPresc.utstamp)>>','', '','', '' 
		ENDTEXT
		IF !uf_gerais_actgrelha("", [uCrsPesquisarCL], lcSQL)
			MESSAGEBOX("N�o foi poss�vel verificar informa��o do utente.",16,"LOGITOOLS SOFTWARE")
			RETURN .f.
		ENDIF
		uf_pesqutentes_escolhePresc()

	ENDIF 
	
ENDFUNC

 

**
FUNCTION uf_prescricao_exit
	PUBLIC PRESCRICAO
	
	&& apagar registo do cc da tabela de leitor de cart�o cidad�o
*!*		IF USED("uCrsIDAux")
*!*			lcSQL = ''
*!*			TEXT TO lcSQL NOSHOW TEXTMERGE 
*!*				DELETE FROM dispensa_eletronica_cc WHERE id = '<<ALLTRIM(uCrsIDAux.id)>>'
*!*			ENDTEXT 
*!*			IF !uf_gerais_actgrelha("","",lcSQL)
*!*				uf_perguntalt_chama("N�o foi poss�vel eliminar o registo do Cart�o de Cidad�o. Por favor contacte o Suporte", "OK", "", 48)	
*!*				RETURN .F.
*!*			ENDIF 
*!*		ENDIF 

	**
	IF USED("UCRSCABPRESC")
		fecha("UCRSCABPRESC")	
	ENDIF 
	IF USED("UCRSLINPRESC")
		fecha("UCRSLINPRESC")
	ENDIF
	
	IF USED("uCrsIDAux")
		fecha("uCrsIDAux")
	ENDIF
	
	IF USED("ucrsNotificaCliente")
		fecha("ucrsNotificaCliente")
	ENDIF

			
	RELEASE myPrescricaoIntroducao
	RELEASE myPrescricaoAlteracao
	RELEASE mylcCodNotifica
	
	initpesq=0
	PRESCRICAO.hide
	PRESCRICAO.release

ENDFUNC

**
FUNCTION uf_prescricao_clean
	PUBLIC PRESCRICAO
	
	&& apagar registo do cc da tabela de leitor de cart�o cidad�o
*!*		IF USED("uCrsIDAux")
*!*			lcSQL = ''
*!*			TEXT TO lcSQL NOSHOW TEXTMERGE 
*!*				DELETE FROM dispensa_eletronica_cc WHERE id = '<<ALLTRIM(uCrsIDAux.id)>>'
*!*			ENDTEXT 
*!*			IF !uf_gerais_actgrelha("","",lcSQL)
*!*				uf_perguntalt_chama("N�o foi poss�vel eliminar o registo do Cart�o de Cidad�o. Por favor contacte o Suporte", "OK", "", 48)	
*!*				RETURN .F.
*!*			ENDIF 
*!*		ENDIF 

	**
	IF USED("UCRSCABPRESC")
		fecha("UCRSCABPRESC")	
	ENDIF 
	IF USED("UCRSLINPRESC")
		fecha("UCRSLINPRESC")
	ENDIF
	
	IF USED("uCrsIDAux")
		fecha("uCrsIDAux")
	ENDIF
			
	RELEASE myPrescricaoIntroducao
	RELEASE myPrescricaoAlteracao
	initpesq=0
	PRESCRICAO.hide
ENDFUNC


** funcao q criar cursor para impressoes de receitas desmaterializadas (RSP)
FUNCTION uf_prescricao_imprimirReceitaMedRSP	
	
	SELECT ucrsLinPresc
	lcPrescstamp = ucrsLinPresc.prescstamp	

	SELECT * FROM ucrsLinPresc WHERE (estado == "1" OR estado == "3") AND LEFT(ALLTRIM(design),1) == "." AND ALLTRIM(prescstamp) == ALLTRIM(lcPrescstamp) INTO CURSOR ucrsLinPrescPrint READWRITE 
	
	select ucrsLinPrescPrint 
	IF RECCOUNT("ucrsLinPrescPrint") == 0
		uf_perguntalt_chama("O estado das receitas n�o permite a sua impress�o.", "", "OK", 16) 
		RETURN .f.
	ENDIF
	
	** 
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE
		Select localPresc, infarmed, localacss, zonaacss, localcod, localnm from empresa where site = '<<ucrsLinPresc.site>>'
	ENDTEXT
	IF !uf_gerais_actGrelha("", "UcrsDadosLocaPresc", lcSql)
		uf_perguntalt_chama("N�o foi possivel verificar os dados do local de prescri��o, n�o ser� possivel imprimir. Contacte o suporte.", "", "OK", 16) 
		RETURN .f.
	ENDIF
	
	SELECT uCrsDadosLocaPresc
	GO TOP 
	
	&& criar cursor auxiliar apenas com cabe�alho da receita a ser impressa dentro do scan 
	SELECT * FROM uCrsLinPresc WHERE (estado == "1" OR estado == "3") AND LEFT(ALLTRIM(design),1) == "." AND ALLTRIM(prescstamp) == ALLTRIM(lcPrescstamp) INTO CURSOR uCrsLinPrescPrint READWRITE					
	
	&& cria cursor com detalhe da receita a ser impressa dentro do scan
	SELECT CAST("" as c(200)) as TextoEncargos, CAST("" as c(200)) as TextoExcecao, CAST("" as c(200)) as QrCodePAth,  * FROM ucrsLinPresc WHERE (estado == "1" OR estado == "3") AND LEFT(ALLTRIM(design),1) != "." AND ALLTRIM(prescstamp) == ALLTRIM(lcPrescstamp) ORDER BY pos ASC INTO CURSOR ucrsLinPrescPrintLinhas READWRITE					
		
	&& atualiza dados encargos e excecao no cursor
	SELECT ucrsLinPrescPrintLinhas
	GO TOP 
	SCAN 
		replace ucrsLinPrescPrintLinhas.TextoEncargos WITH uf_prescricao_calculaTextoEncargos(ucrsLinPrescPrintLinhas.u_epvp, ucrsLinPrescPrintLinhas.utente, ucrsLinPrescPrintLinhas.pordci, ALLTRIM(ucrsLinPrescPrintLinhas.codGrupoH), ALLTRIM(ucrsLinPrescPrintLinhas.ref), ALLTRIM(ucrsLinPrescPrintLinhas.cnpem), ALLTRIM(ucrsLinPrescPrintLinhas.diplomacod), ALLTRIM(ucrsLinPrescPrintLinhas.PrescNomeCod))
		replace ucrsLinPrescPrintLinhas.TextoExcecao  WITH uf_prescricao_calculaTextoExcecao(ucrsLinPrescPrintLinhas.prescNomeCod)
		replace ucrsLinPrescPrintLinhas.QrCodePAth    WITH ALLTRIM(ALLTRIM(uf_gerais_getParameter("ADM0000000015","TEXT")) + "\Cache\" + ALLTRIM(uCrsE1.id_lt) + '_' + ALLTRIM(ucrsLinPrescPrintLinhas.prescstamp) + '_' + ALLTRIM(STR(ucrsLinPrescPrintLinhas.pos)) + ".png")
		
		SELECT ucrsLinPrescPrintLinhas
	ENDSCAN
	
	SELECT ucrsLinPrescPrintLinhas
	GO TOP 
	
	&& criar cursores para permitir a impress�o da dos c�digos QrCode (m�ximo de 24 por receita [n�o existe limita��o ao n�vel da ceritfica��o mas sim ao n�vel das limita��es da framework])
	select ucrsLinPrescPrintLinhas.pos, ucrsLinPrescPrintLinhas.QrCodePAth from ucrsLinPrescPrintLinhas WHERE ucrsLinPrescPrintLinhas.pos = 1 INTO CURSOR uCrsLinPrescQrCode1
	select IIF(!EMPTY(ucrsLinPrescPrintLinhas.pos),ucrsLinPrescPrintLinhas.pos,0) as pos, IIF(!EMPTY(ucrsLinPrescPrintLinhas.QrCodePAth),ucrsLinPrescPrintLinhas.QrCodePAth,'xx') as QrCodePAth from ucrsLinPrescPrintLinhas WHERE ucrsLinPrescPrintLinhas.pos = 2 INTO CURSOR uCrsLinPrescQrCode2 
	select IIF(!EMPTY(ucrsLinPrescPrintLinhas.pos),ucrsLinPrescPrintLinhas.pos,0) as pos, IIF(!EMPTY(ucrsLinPrescPrintLinhas.QrCodePAth),ucrsLinPrescPrintLinhas.QrCodePAth,'xx') as QrCodePAth from ucrsLinPrescPrintLinhas WHERE ucrsLinPrescPrintLinhas.pos = 3 INTO CURSOR uCrsLinPrescQrCode3 
	select IIF(!EMPTY(ucrsLinPrescPrintLinhas.pos),ucrsLinPrescPrintLinhas.pos,0) as pos, IIF(!EMPTY(ucrsLinPrescPrintLinhas.QrCodePAth),ucrsLinPrescPrintLinhas.QrCodePAth,'xx') as QrCodePAth from ucrsLinPrescPrintLinhas WHERE ucrsLinPrescPrintLinhas.pos = 4 INTO CURSOR uCrsLinPrescQrCode4 
	select IIF(!EMPTY(ucrsLinPrescPrintLinhas.pos),ucrsLinPrescPrintLinhas.pos,0) as pos, IIF(!EMPTY(ucrsLinPrescPrintLinhas.QrCodePAth),ucrsLinPrescPrintLinhas.QrCodePAth,'xx') as QrCodePAth from ucrsLinPrescPrintLinhas WHERE ucrsLinPrescPrintLinhas.pos = 5 INTO CURSOR uCrsLinPrescQrCode5 
	select IIF(!EMPTY(ucrsLinPrescPrintLinhas.pos),ucrsLinPrescPrintLinhas.pos,0) as pos, IIF(!EMPTY(ucrsLinPrescPrintLinhas.QrCodePAth),ucrsLinPrescPrintLinhas.QrCodePAth,'xx') as QrCodePAth from ucrsLinPrescPrintLinhas WHERE ucrsLinPrescPrintLinhas.pos = 6 INTO CURSOR uCrsLinPrescQrCode6 
	select IIF(!EMPTY(ucrsLinPrescPrintLinhas.pos), ucrsLinPrescPrintLinhas.pos,0) as pos, IIF(!EMPTY(ucrsLinPrescPrintLinhas.QrCodePAth), ucrsLinPrescPrintLinhas.QrCodePAth, 'xx') as QrCodePAth FROM ucrsLinPrescPrintLinhas	WHERE ucrsLinPrescPrintLinhas.pos = 7 INTO CURSOR uCrsLinPrescQrCode7
	select IIF(!EMPTY(ucrsLinPrescPrintLinhas.pos), ucrsLinPrescPrintLinhas.pos,0) as pos, IIF(!EMPTY(ucrsLinPrescPrintLinhas.QrCodePAth), ucrsLinPrescPrintLinhas.QrCodePAth, 'xx') as QrCodePAth FROM ucrsLinPrescPrintLinhas	WHERE ucrsLinPrescPrintLinhas.pos = 8 INTO CURSOR uCrsLinPrescQrCode8
	select IIF(!EMPTY(ucrsLinPrescPrintLinhas.pos), ucrsLinPrescPrintLinhas.pos,0) as pos, IIF(!EMPTY(ucrsLinPrescPrintLinhas.QrCodePAth), ucrsLinPrescPrintLinhas.QrCodePAth, 'xx') as QrCodePAth FROM ucrsLinPrescPrintLinhas	WHERE ucrsLinPrescPrintLinhas.pos = 9 INTO CURSOR uCrsLinPrescQrCode9
	select IIF(!EMPTY(ucrsLinPrescPrintLinhas.pos), ucrsLinPrescPrintLinhas.pos,0) as pos, IIF(!EMPTY(ucrsLinPrescPrintLinhas.QrCodePAth), ucrsLinPrescPrintLinhas.QrCodePAth, 'xx') as QrCodePAth FROM ucrsLinPrescPrintLinhas	WHERE ucrsLinPrescPrintLinhas.pos = 10 INTO CURSOR uCrsLinPrescQrCode10
	select IIF(!EMPTY(ucrsLinPrescPrintLinhas.pos), ucrsLinPrescPrintLinhas.pos,0) as pos, IIF(!EMPTY(ucrsLinPrescPrintLinhas.QrCodePAth), ucrsLinPrescPrintLinhas.QrCodePAth, 'xx') as QrCodePAth FROM ucrsLinPrescPrintLinhas	WHERE ucrsLinPrescPrintLinhas.pos = 11 INTO CURSOR uCrsLinPrescQrCode11
	select IIF(!EMPTY(ucrsLinPrescPrintLinhas.pos), ucrsLinPrescPrintLinhas.pos,0) as pos, IIF(!EMPTY(ucrsLinPrescPrintLinhas.QrCodePAth), ucrsLinPrescPrintLinhas.QrCodePAth, 'xx') as QrCodePAth FROM ucrsLinPrescPrintLinhas	WHERE ucrsLinPrescPrintLinhas.pos = 12 INTO CURSOR uCrsLinPrescQrCode12
	select IIF(!EMPTY(ucrsLinPrescPrintLinhas.pos), ucrsLinPrescPrintLinhas.pos,0) as pos, IIF(!EMPTY(ucrsLinPrescPrintLinhas.QrCodePAth), ucrsLinPrescPrintLinhas.QrCodePAth, 'xx') as QrCodePAth FROM ucrsLinPrescPrintLinhas	WHERE ucrsLinPrescPrintLinhas.pos = 13 INTO CURSOR uCrsLinPrescQrCode13
	select IIF(!EMPTY(ucrsLinPrescPrintLinhas.pos), ucrsLinPrescPrintLinhas.pos,0) as pos, IIF(!EMPTY(ucrsLinPrescPrintLinhas.QrCodePAth), ucrsLinPrescPrintLinhas.QrCodePAth, 'xx') as QrCodePAth FROM ucrsLinPrescPrintLinhas	WHERE ucrsLinPrescPrintLinhas.pos = 14 INTO CURSOR uCrsLinPrescQrCode14
	select IIF(!EMPTY(ucrsLinPrescPrintLinhas.pos), ucrsLinPrescPrintLinhas.pos,0) as pos, IIF(!EMPTY(ucrsLinPrescPrintLinhas.QrCodePAth), ucrsLinPrescPrintLinhas.QrCodePAth, 'xx') as QrCodePAth FROM ucrsLinPrescPrintLinhas	WHERE ucrsLinPrescPrintLinhas.pos = 15 INTO CURSOR uCrsLinPrescQrCode15
	select IIF(!EMPTY(ucrsLinPrescPrintLinhas.pos), ucrsLinPrescPrintLinhas.pos,0) as pos, IIF(!EMPTY(ucrsLinPrescPrintLinhas.QrCodePAth), ucrsLinPrescPrintLinhas.QrCodePAth, 'xx') as QrCodePAth FROM ucrsLinPrescPrintLinhas	WHERE ucrsLinPrescPrintLinhas.pos = 16 INTO CURSOR uCrsLinPrescQrCode16
	select IIF(!EMPTY(ucrsLinPrescPrintLinhas.pos), ucrsLinPrescPrintLinhas.pos,0) as pos, IIF(!EMPTY(ucrsLinPrescPrintLinhas.QrCodePAth), ucrsLinPrescPrintLinhas.QrCodePAth, 'xx') as QrCodePAth FROM ucrsLinPrescPrintLinhas	WHERE ucrsLinPrescPrintLinhas.pos = 17 INTO CURSOR uCrsLinPrescQrCode17
	select IIF(!EMPTY(ucrsLinPrescPrintLinhas.pos), ucrsLinPrescPrintLinhas.pos,0) as pos, IIF(!EMPTY(ucrsLinPrescPrintLinhas.QrCodePAth), ucrsLinPrescPrintLinhas.QrCodePAth, 'xx') as QrCodePAth FROM ucrsLinPrescPrintLinhas	WHERE ucrsLinPrescPrintLinhas.pos = 18 INTO CURSOR uCrsLinPrescQrCode18
	select IIF(!EMPTY(ucrsLinPrescPrintLinhas.pos), ucrsLinPrescPrintLinhas.pos,0) as pos, IIF(!EMPTY(ucrsLinPrescPrintLinhas.QrCodePAth), ucrsLinPrescPrintLinhas.QrCodePAth, 'xx') as QrCodePAth FROM ucrsLinPrescPrintLinhas	WHERE ucrsLinPrescPrintLinhas.pos = 19 INTO CURSOR uCrsLinPrescQrCode19
	select IIF(!EMPTY(ucrsLinPrescPrintLinhas.pos), ucrsLinPrescPrintLinhas.pos,0) as pos, IIF(!EMPTY(ucrsLinPrescPrintLinhas.QrCodePAth), ucrsLinPrescPrintLinhas.QrCodePAth, 'xx') as QrCodePAth FROM ucrsLinPrescPrintLinhas	WHERE ucrsLinPrescPrintLinhas.pos = 20 INTO CURSOR uCrsLinPrescQrCode20
	select IIF(!EMPTY(ucrsLinPrescPrintLinhas.pos), ucrsLinPrescPrintLinhas.pos,0) as pos, IIF(!EMPTY(ucrsLinPrescPrintLinhas.QrCodePAth), ucrsLinPrescPrintLinhas.QrCodePAth, 'xx') as QrCodePAth FROM ucrsLinPrescPrintLinhas	WHERE ucrsLinPrescPrintLinhas.pos = 21 INTO CURSOR uCrsLinPrescQrCode21
	select IIF(!EMPTY(ucrsLinPrescPrintLinhas.pos), ucrsLinPrescPrintLinhas.pos,0) as pos, IIF(!EMPTY(ucrsLinPrescPrintLinhas.QrCodePAth), ucrsLinPrescPrintLinhas.QrCodePAth, 'xx') as QrCodePAth FROM ucrsLinPrescPrintLinhas	WHERE ucrsLinPrescPrintLinhas.pos = 22 INTO CURSOR uCrsLinPrescQrCode22
	select IIF(!EMPTY(ucrsLinPrescPrintLinhas.pos), ucrsLinPrescPrintLinhas.pos,0) as pos, IIF(!EMPTY(ucrsLinPrescPrintLinhas.QrCodePAth), ucrsLinPrescPrintLinhas.QrCodePAth, 'xx') as QrCodePAth FROM ucrsLinPrescPrintLinhas	WHERE ucrsLinPrescPrintLinhas.pos = 23 INTO CURSOR uCrsLinPrescQrCode23
	select IIF(!EMPTY(ucrsLinPrescPrintLinhas.pos), ucrsLinPrescPrintLinhas.pos,0) as pos, IIF(!EMPTY(ucrsLinPrescPrintLinhas.QrCodePAth), ucrsLinPrescPrintLinhas.QrCodePAth, 'xx') as QrCodePAth FROM ucrsLinPrescPrintLinhas	WHERE ucrsLinPrescPrintLinhas.pos = 24 INTO CURSOR uCrsLinPrescQrCode24
	
	
	SELECT ucrsLinPrescPrintLinhas
ENDFUNC


**
FUNCTION uf_prescricao_Prever	

	&& setfocus para melhorar impress�o
	prescricao.txtnome.setfocus
	
	&& Verifica se existem receitas para imprimir
	SELECT * FROM ucrsLinPresc WHERE (estado == "1" OR estado == "3") AND LEFT(ALLTRIM(design),1) == "." AND anulado == .f. INTO CURSOR ucrsLinPrescPrintVerify READWRITE 
	
	SELECT ucrsLinPrescPrintVerify 
	IF RECCOUNT("ucrsLinPrescPrintVerify") == 0
		uf_perguntalt_chama("O estado das receitas n�o permite a sua previs�o. As receitas s� podem ser pre visualizadas depois de validadas ou em caso indisponibilidade dos servi�os de valida��o.", "", "OK", 16) 
		RETURN .f.
	ELSE
		IF USED("ucrsLinPrescPrintVerify")
			fecha("ucrsLinPrescPrintVerify")
		ENDIF	
	ENDIF
	
	** prepara dados para pre visualizar receitas
	PUBLIC myFolha, myLogoPathReceita, myImgVersaoSoftware, myLogoPathReceitaSNS, myLtsPrescricao
	LOCAL lcTipoReceita
	lcTipoReceita = 0
	
	** Calcula Caminho Imagens
	myImgVersaoSoftware = mypath + "\imagens\Pe\peversaosoftware13.png"
	myImgMinSaude = mypath + "\imagens\Pe\GovernoMinSaude_PeB.png" 
	myImgRepublica = mypath + "\imagens\Pe\Republica_Portuguesa.png" 
	
	
		
	lcSubRegiao = uf_gerais_getParameter('ADM0000000238', 'TEXT')
	
	DO CASE
		CASE EMPTY(lcSubRegiao) OR UPPER(ALLTRIM(lcSubRegiao)) == "CONTINENTE"
			myLogoPathReceita = mypath + "\imagens\Pe\governominsaude_peb.png" 
		CASE UPPER(ALLTRIM(lcSubRegiao)) == "A�ORES"
			myLogoPathReceita = mypath + "\imagens\Pe\SRSAcores.PNG" 
		CASE UPPER(ALLTRIM(lcSubRegiao)) == "MADEIRA"
			myLogoPathReceita = mypath + "\imagens\Pe\SRSMadeira.png" 
	ENDCASE
	
	myLtsPrescricao = mypath + 	"\imagens\Pe\lts_prescricaoeletronica.png" 
	myLogoPathReceitaSNS = mypath + "\imagens\Pe\logo_sns.png" 
	
	**
	
*!*		PUBLIC myRecImprimeTodas
*!*		
*!*		IF myRecImprimeTodas == .f.
*!*			SELECT ucrsLinPresc
*!*			
*!*			myFolha = 1
*!*		
*!*			IF ucrsLinPresc.receitamcdt == .f.
*!*				uf_imprimirGerais_imprimirMedIndividual(lcTipo,1, lcSemPerguntas)
*!*			ELSE
*!*				uf_imprimirGerais_imprimirMCDTIndividual(lcTipo,1, lcSemPerguntas)
*!*			ENDIF
*!*		ELSE
		
	SELECT ucrsLinPresc
	GO TOP
	SCAN FOR LEFT(ALLTRIM(ucrsLinPresc.design),1) == "." 
		&&
		IF ucrsLinPresc.receitamcdt == .t.
			uf_prescricao_imprimirReceitaMcdt(.t.) &&Refresca informa��o da receita e prepara cursores	
			lcTipoReceita = 2
		ELSE
			IF UPPER(LEFT(ALLTRIM(ucrsLinPresc.tiporeceita),1)) != 'L'	
				uf_prescricao_imprimirReceitaMed(.t.) &&Refresca informa��o da receita e prepara cursores para receitas materializadas
			ELSE
				uf_prescricao_imprimirReceitaMedRSP() &&Refresca informa��o da receita e prepara cursores para receitas desmaterializadas
			ENDIF
				lcTipoReceita = 1
		ENDIF 
			
		EXIT 	
	ENDSCAN 
	
	IF lcTipoReceita = 2
		uf_imprimirGerais_imprimirMCDTIndividual("PREV")
	ENDIF
	IF lcTipoReceita = 1
		uf_imprimirGerais_imprimirMedIndividual("PREV")
	ENDIF

ENDFUNC 

FUNCTION uf_prescricao_enviar_codigo

    LOCAL lcTemCods
    STORE .f. TO lcTemCods

    IF ucrse1.usacoms=.t.     
        SELECT uCrsLinPresc
        GO top
        SCAN
            IF !EMPTY(uCrsLinPresc.pin)
                lcTemCods = .t.
            ENDIF 
        ENDSCAN 
        IF lcTemCods = .f.
            MESSAGEBOX("N�O EXISTEM C�DIGOS PARA ENVIAR.")
            RETURN .f.
        ENDIF 
        IF uf_perguntalt_chama("ESCOLHA A FORMA DE ENVIO","SMS","EMAIL")    

		    LOCAL lcMsg, lcCodEnvio , lcDestino, lcWsPath, lcUtStamp
		    
		    STORE LEFT(ALLTRIM(m_chinis)+dtoc(date())+SYS(2015),25) TO lcCodEnvio 
		    
		    ** VALIDA SE TEM PREENCHIDO O N? DE TELEM?VEL E SE AUTORIZOU O ENVIO DE SMS'S
		    TEXT TO lcSQL TEXTMERGE NOSHOW
		        select utstamp, email, autoriza_emails, tlmvl, autoriza_sms from b_utentes where no=<<ucrsCabPresc.No>> and estab=<<ucrsCabPresc.Estab>>
		    ENDTEXT 
		    IF !uf_gerais_actgrelha("", "ucrsDadosCL", lcSql)
		        uf_perguntalt_chama("ERRO AO CONSULTAR A TABELA DE UTENTES!","OK","",16)
		        RETURN .F.
		    ENDIF 
		    IF !EMPTY(ucrsDadosCL.tlmvl)
		        IF ucrsDadosCL.autoriza_sms = .f.
		            uf_perguntalt_chama("O UTENTE N�O AUTORIZOU O ENVIO DE SMS'S!","OK","",16)
		            RETURN .f.
		        ELSE
		            lcDestino = ALLTRIM(ucrsDadosCL.tlmvl)
		            lcUtStamp = ALLTRIM(ucrsDadosCL.utstamp)
		        ENDIF 
		    ELSE
		        uf_perguntalt_chama("O UTENTE N�O TEM O N�MERO DE TELEMRVEL PREENCHIDO NA SUA FICHA!","OK","",16)
		        RETURN .F.
		    ENDIF 
		    fecha("ucrsDadosCL")
		    
		    lcMsg='Prescri��o Eletr�nica M�dica: '
		    
		    SELECT uCrsLinPresc
		    GO TOP 
		    SCAN 
		        IF !EMPTY(uCrsLinPresc.pin) AND EMPTY(uCrsLinPresc.ref)
		            lcMsg=lcMsg + "Foi registada no dia " + ALLTRIM(DTOC(uCrsLinPresc.data)) + " a receita com o n�mero " + alltrim(uCrsLinPresc.prescno) +;
		            	" com o c�digo de dispensa " + ALLTRIM(uCrsLinPresc.pin) + " e c�digo de direito de op��o " + ALLTRIM(uCrsLinPresc.pinOpcao) + "."
		        ENDIF 
		    ENDSCAN
		    
		    uf_send_sms(lcCodEnvio, 'C�DIGOS', 'Prescri��o', ALLTRIM(lcMsg), lcDestino, 'CL', lcUtStamp, 'Envio de c�digos da receita por SMS para o nr '+ALLTRIM(lcDestino) )


        ELSE
            
            LOCAL lcTo, lcToEmail, lcSubject, lcBody, lcAtatchment, lcToken, lcTexto 
		    STORE '' TO lcTexto 
		    
		    ** VALIDA SE TEM PREENCHIDO O N? DE TELEM?VEL E SE AUTORIZOU O ENVIO DE SMS'S
		    TEXT TO lcSQL TEXTMERGE NOSHOW
		        select utstamp, email, autoriza_emails, tlmvl, autoriza_sms from b_utentes where no=<<ucrsCabPresc.No>> and estab=<<ucrsCabPresc.Estab>>
		    ENDTEXT 
		    IF !uf_gerais_actgrelha("", "ucrsDadosCL", lcSql)
		        uf_perguntalt_chama("ERRO AO CONSULTAR A TABELA DE UTENTES!","OK","",16)
		        RETURN .F.
		    ENDIF 
		    IF !EMPTY(ucrsDadosCL.email)
		        IF ucrsDadosCL.autoriza_emails = .f.
		            uf_perguntalt_chama("O UTENTE N�O AUTORIZOU O ENVIO DE EMAIL'S!","OK","",16)
		            RETURN .f.
		        ELSE
		            lcToEmail = ALLTRIM(ucrsDadosCL.email)
		            lcUtStamp = ALLTRIM(ucrsDadosCL.utstamp)
		        ENDIF 
		    ELSE
		        uf_perguntalt_chama("O UTENTE N�O TEM O ENDERE�O DE EMAIL PREENCHIDO NA SUA FICHA!","OK","",16)
		        RETURN .F.
		    ENDIF 
		    fecha("ucrsDadosCL")
		    
		    lcTexto ='Exmo(a). Sr(a). ' + ALLTRIM(ucrsCabPresc.nome) + ' ' + CHR(13)
		    lcTexto = lcTexto + CHR(13) + 'Vimos por este meio enviar os c�digos das receitas da ' + STRTRAN(ALLTRIM(Prescricao.lblPrescricao.Caption),':','') + CHR(13)
		    lcTexto = lcTexto + CHR(13)
		    
   		    SELECT uCrsLinPresc
		    GO TOP 
		    SCAN 
		        IF !EMPTY(uCrsLinPresc.pin) AND EMPTY(uCrsLinPresc.ref)
		            lcTexto = lcTexto + "Foi registada no dia " + ALLTRIM(DTOC(uCrsLinPresc.data)) + " a receita com o n�mero " + alltrim(uCrsLinPresc.prescno) +;
		            	" com o c�digo de dispensa " + ALLTRIM(uCrsLinPresc.pin) + " e c�digo de direito de op��o " + ALLTRIM(uCrsLinPresc.pinOpcao) + "."
		        ENDIF 
		    ENDSCAN
		    	    
		    lcTexto = lcTexto + CHR(13)
		    
		    lcTo = ""
		    lcToEmail = ALLTRIM(lcToEmail)
		    lcSubject = 'Prescri��o Eletr�nica M�dica'
		    lcBody = lcTexto + CHR(13) + ALLTRIM(ucrse1.mailsign)
		    lcAtatchment = ""
		    regua(0,6,"A enviar email ...")
		    uf_startup_sendmail(lcTo, lcToEmail, lcSubject, lcBody , lcAtatchment, .t.)
		    regua(2)
		    
		    uf_user_log_ins('CL', ALLTRIM(lcUtStamp ), 'PRESCRI��O', 'Envio de c�digos de receita por Email para '+ALLTRIM(lcToEmail))
            
        ENDIF 
    ELSE
        uf_perguntalt_chama("PARA UTILIZAR ESTA FUNCIONALIDADE DEVER� ADQUIRIR O M�DULO DE COMUNICA��ES." + CHR(13) + "PARA MAIS INFORMA��ES POR FAVOR CONTACTE A LOGITOOLS (OPOIO - COMERCIAL).", "OK","",16)
    ENDIF 

ENDFUNC 