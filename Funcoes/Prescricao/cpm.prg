**
FUNCTION uf_CPM_Chama
	
	**Valida Licenciamento	
	IF (uf_ligacoes_addConnection('PE') == .f.)
		RETURN .F.
	ENDIF
	
	IF uf_prescricao_validaUtilizador() == .f.
		Return .f.
	ENDIF
	
	IF !USED("ucrsCPMCab")
		uf_CPM_actPesquisa(.t.)
	ENDIF
	IF TYPE("CPM")=="U"
		DO FORM CPM
	ELSE
		CPM.show
	ENDIF
ENDFUNC 


** tcBool = caso venha a .t. cria apenas o cursor
FUNCTION uf_CPM_actPesquisa
	LPARAMETERS tcBool
	
	LOCAL lcSQL, lcDataIni, lcDataFim
	STORE "" TO lcSql, lcDataIni, lcDataFim
	
	IF tcBool == .t.
		lcSQL = ""
		TEXT TO lcSql NOSHOW TEXTMERGE
			exec up_prescricao_CPM 'xxxxxx'
		ENDTEXT

		if type("CPM")=="U"
			IF !uf_gerais_actGrelha("", "ucrsCPMCab",lcSQL)
				uf_perguntalt_chama("N�o foi poss�vel actualizar a listagem de Prescri��es. Por favor contacte o Suporte.", "", "OK", 16)
				RETURN .f.
			ENDIF
		ENDIF
		
		lcSQL = ""
		TEXT TO lcSql NOSHOW TEXTMERGE
			exec up_prescricao_CPM_Linhas ''
		ENDTEXT

		IF !uf_gerais_actGrelha("", "ucrsCPMLin",lcSQL)
			uf_perguntalt_chama("N�o foi poss�vel actualizar a listagem de Prescri��es. Por favor contacte o Suporte.", "", "OK", 16)
			RETURN .f.
		ENDIF
		
		lcSQL = ""
		TEXT TO lcSql NOSHOW TEXTMERGE
			exec up_prescricao_CPM_Diplomas ''
		ENDTEXT
		
		IF !uf_gerais_actGrelha("", "ucrsCPMDiplomas",lcSQL)
			uf_perguntalt_chama("N�o foi poss�vel actualizar a listagem de Diplomas. Por favor contacte o Suporte.", "", "OK", 16)
			RETURN .f.
		ENDIF
		
		lcSQL = ""
		TEXT TO lcSql NOSHOW TEXTMERGE
			exec up_prescricao_CPM_LinhasVistaMed ''
		ENDTEXT

		IF !uf_gerais_actGrelha("", "ucrsCPMLinMed",lcSQL)
			uf_perguntalt_chama("N�o foi poss�vel actualizar a listagem de vista de medicamentos. Por favor contacte o Suporte.", "", "OK", 16)
			RETURN .f.
		ENDIF
		
		
		
	ELSE &&Refresca valores da Grid
		
		IF uf_cpm_validacampos() == .f.
			RETURN .f.
		ENDIF		
		
		regua(0,15,"A obter resposta do pedido CPM...")
				
		*** Var chamar webservice
		** 1 - Escreve na BD
		** 2 - executar Jar
		** 3 - obtem resposta
		
		**
		
		** 1 - Escreve na BD
		
		lcstamp = uf_gerais_stamp()
		
		lcSQL = ""
		TEXT TO lcSql NOSHOW TEXTMERGE
						
			insert into b_cli_CPM(
					stamp
					,dataInicio
					,dataFim
					,localcod
					,utente
					,cedula
					,ordem
				)values (
					'<<lcstamp>>'
					,'<<uf_gerais_getdate(CPM.dataIni.value,"SQL")>>'
					,'<<uf_gerais_getdate(CPM.DataFim.value,"SQL")>>'
					,'<<ALLTRIM(CPM.localcod.value)>>'
					,'<<ALLTRIM(CPM.nutente.value)>>'
					,'<<ALLTRIM(CPM.cedula.value)>>'
					,'<<ALLTRIM(CPM.ordem.value)>>'
			)
		ENDTEXT
		IF !uf_gerais_actGrelha("", "ucrsCPMInsertPedido",lcSQL)
			uf_perguntalt_chama("N�o foi poss�vel realizar o pedido. Por favor contacte o Suporte.", "", "OK", 16)
			RETURN .f.
		ENDIF
		
		
		** 2 - executar Jar
		IF uf_gerais_getParameter("ADM0000000227","BOOL")
			lcWsPath = ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\PE\cpm_testes.jar "' + ALLTRIM(lcstamp) + '" "' + ALLTRIM(sql_db) + '"'
			MESSAGEBOX(lcWsPath)
		ELSE
			lcWsPath = ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\PE\cpm.jar "' + ALLTRIM(lcstamp) + '" "' + ALLTRIM(sql_db) + '"'
		ENDIF
		
		lcWsPath = "javaw -jar " + lcWsPath 
		oWSShell = CREATEOBJECT("WScript.Shell")
		oWSShell.Run(lcWsPath,1,.f.)
		*****************
		
		** 3 - obtem resposta - Activa Timer para obter respostas de todas as receitas enviadas
		IF!empty(lcstamp)
			CPM.pedidostamp = ALLTRIM(lcstamp)
			CPM.TMRCPM.enabled = .t.
		ENDIF		
	
	ENDIF
ENDFUNC


**
FUNCTION uf_cpm_obtemResposta

	CPM.ntentativasCPM = CPM.ntentativasCPM + 1
	
	IF CPM.ntentativasCPM > 10 && 10 tentativas de 3 segundos
		regua(2)
		uf_perguntalt_chama("N�o foi poss�vel obter resposta do pedido.","OK","",16)
		CPM.TMRCPM.enabled = .f.
		CPM.ntentativasCPM = 0
		RETURN .f.
	ENDIF
	regua(1,CPM.ntentativasCPM + 2,"A obter resposta do pedido CPM...")
	
	lcSQL = ""
	TEXT TO lcSql NOSHOW TEXTMERGE
		exec up_prescricao_CPM '<<CPM.pedidostamp>>'
	ENDTEXT
	IF !uf_gerais_actGrelha("CPM.PageFrame1.page1.GridPesq", "ucrsCPMCab",lcSQL)
		uf_perguntalt_chama("N�o foi poss�vel verificar a resposta. Por favor contacte o Suporte.", "", "OK", 16)
		regua(2)		
		CPM.TMRCPM.enabled = .f.
		CPM.ntentativasCPM = 0
		RETURN .f.
	ENDIF

	lcSQL = ""
	TEXT TO lcSql NOSHOW TEXTMERGE
		exec up_prescricao_CPM_Reposta'<<CPM.pedidostamp>>'
	ENDTEXT
	IF !uf_gerais_actGrelha("", "ucrsCPMResposta",lcSQL)
		uf_perguntalt_chama("N�o foi poss�vel verificar a resposta. Por favor contacte o Suporte.", "", "OK", 16)
		regua(2)		
		CPM.TMRCPM.enabled = .f.
		CPM.ntentativasCPM = 0
		RETURN .f.
	ELSE
		lcSQL = ""
		TEXT TO lcSql NOSHOW TEXTMERGE
			exec up_prescricao_CPM_LinhasVistaMed '<<CPM.pedidostamp>>'
		ENDTEXT
		IF !uf_gerais_actGrelha("CPM.PageFrame1.page2.GridPesq", "ucrsCPMLinMed",lcSQL)
			uf_perguntalt_chama("N�o foi poss�vel verificar a resposta. Por favor contacte o Suporte.", "", "OK", 16)
			regua(2)		
			CPM.TMRCPM.enabled = .f.
			CPM.ntentativasCPM = 0
			RETURN .f.
		ENDIF
		
	ENDIF			
	IF RECCOUNT("ucrsCPMResposta") > 0
		CPM.Refresh
		CPM.lblRegistos.caption = ALLTRIM(STR(RECCOUNT("ucrsCPMCab"))) + " Resultados"
		
		regua(2)		
		CPM.TMRCPM.enabled = .f.
		CPM.ntentativasCPM = 0
	ENDIF
	
	IF RECCOUNT("ucrsCPMCab") == 0
		uf_perguntalt_chama("A pesquisa n�o obteve resultados.", "", "OK", 64)
	ENDIF
ENDFUNC


**
FUNCTION uf_cpm_ActualizaLinhas
	Select ucrsCPMCab
	lcSQL = ""
	TEXT TO lcSql NOSHOW TEXTMERGE
		exec up_prescricao_CPM_Linhas '<<ucrsCPMCab.stamp>>'
	ENDTEXT

	IF !uf_gerais_actGrelha("CPM.PageFrame1.page1.PageFrame1.Page2.GridPesq", "ucrsCPMLin",lcSQL)
		uf_perguntalt_chama("N�o foi poss�vel actualizar a listagem de Medicamentos. Por favor contacte o Suporte.", "", "OK", 16)
		RETURN .f.
	ENDIF

ENDFUNC 


**
FUNCTION uf_cpm_ActualizaDiplomas
	Select ucrsCPMLin
	lcSQL = ""
	TEXT TO lcSql NOSHOW TEXTMERGE
		exec up_prescricao_CPM_Diplomas '<<ucrsCPMLin.stampLinha>>'
	ENDTEXT

	IF !uf_gerais_actGrelha("CPM.PageFrame1.page1.PageFrame1.Page3.GridPesq", "ucrsCPMDiplomas",lcSQL)
		uf_perguntalt_chama("N�o foi poss�vel actualizar a listagem de Diplomas. Por favor contacte o Suporte.", "", "OK", 16)
		RETURN .f.
	ENDIF

ENDFUNC 

**
FUNCTION uf_cpm_validacampos


	** N�mero de utente tem que existir
	IF EMPTY(ALLTRIM(CPM.nutente.value))
		uf_perguntalt_chama("O preenchimento do N�mero de utente � obrigat�rio.", "", "OK", 64) 
		RETURN .f.
	ENDIF
	
	** N�mero de utente tem que ser numerico
	IF !isdigit(CPM.nutente.value)
		uf_perguntalt_chama("N�mero de utente inv�lido.", "", "OK", 64) 
		RETURN .f.
	ENDIF
			
	** datas
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		Select DATEDIFF(MONTH, '<<uf_gerais_getdate(CPM.DataFim.value,"SQL")>>','<<uf_gerais_getdate(CPM.dataIni.value,"SQL")>>') as meses
	ENDTEXT
	IF !uf_gerais_actGrelha("", "ucrsCPMDifDatas",lcSQL)
		uf_perguntalt_chama("N�o foi poss�vel verificar a diferen�a entre as datas. Por favor contacte o Suporte.", "", "OK", 16)
		RETURN .f.
	ENDIF
	SELECT ucrsCPMDifDatas
	IF ucrsCPMDifDatas.meses > 6
		uf_perguntalt_chama("N�o � possivel realizar pesquisa com uma diferen�a de datas superior a 6 meses. Por favor verifique.", "", "OK", 64)
		RETURN .f.
	ENDIF
	
	RETURN .t.

ENDFUNC


**
FUNCTION uf_CPM_init
	CPM.menu1.adicionaOpcao("actualizar", "Actualizar", myPath + "\imagens\icons\actualizar_w.png", "uf_CPM_actPesquisa with .F.", "A")
	CPM.menu1.adicionaOpcao("subir", "Subir", myPath + "\imagens\icons\ponta_seta_up.png", "uf_CPM_gridUp","U")
	CPM.menu1.adicionaOpcao("descer", "Descer", myPath + "\imagens\icons\ponta_seta_down.png", "uf_CPM_gridDown","D")
ENDFUNC


**
FUNCTION uf_CPM_gridUp
	WITH CPM.PageFrame1
		DO CASE 
			CASE .activePage = 1
				uf_gerais_MovePage(.t.,'CPM.PageFrame1.Page1.gridpesq','ucrsCPMCab')
			CASE .activePage = 2
				uf_gerais_MovePage(.t.,'CPM.PageFrame1.Page2.gridpesq','ucrsCPMLinMed')	
			OTHERWISE
		ENDCASE 
	ENDWITH 
ENDFUNC 



**
FUNCTION uf_CPM_gridDown
	WITH CPM.PageFrame1
		DO CASE 
			CASE .activePage = 1
				uf_gerais_MovePage(.f.,'CPM.PageFrame1.Page1.gridpesq','ucrsCPMCab')
			CASE .activePage = 2
				uf_gerais_MovePage(.f.,'CPM.PageFrame1.Page2.gridpesq','ucrsCPMLinMed')	
			OTHERWISE
		ENDCASE 
	ENDWITH 
ENDFUNC 

**
FUNCTION uf_CPM_expandeDetalhe
	WITH CPM.PageFrame1.page1
		If CPM.detalhesVisiveis == .f.
			.gridpesq.height = .gridpesq.height + 200
			CPM.detalhesVisiveis = .t.
			.PageFrame1.visible = .f.
			.lblCaminho.visible = .f.
			.btnDados.visible = .f.
			.btnNome.visible = .f.
			.BtnDiplomas.visible = .f.
		ELSE
			.gridpesq.height = .gridpesq.height - 200
			CPM.detalhesVisiveis = .f.
			.PageFrame1.visible = .t.
			.lblCaminho.visible = .t.
			.btnDados.visible = .t.
			.btnNome.visible = .t.
			.BtnDiplomas.visible = .t.
		ENDIF
		
		.detalhes.top = .gridpesq.top + .gridpesq.height + 5
	ENDWITH 

ENDFUNC


**
FUNCTION uf_CPM_expandeDetalhe2
	WITH CPM.PageFrame1.page2
		If CPM.detalhesVisiveis2 == .f.
			.gridpesq.height = .gridpesq.height + 200
			CPM.detalhesVisiveis2 = .t.
			.PageFrame1.visible = .f.
			.lblCaminho.visible = .f.
		ELSE
			.gridpesq.height = .gridpesq.height - 200
			CPM.detalhesVisiveis2 = .f.
			.PageFrame1.visible = .t.
			.lblCaminho.visible = .t.
		ENDIF
		
		.detalhes.top = .gridpesq.top + .gridpesq.height + 5
	ENDWITH 

ENDFUNC

**
FUNCTION uf_cpm_actualizaLocalizacao
	WITH CPM.PageFrame1.page1
		DO CASE 
			CASE .pageframe1.activePage = 1
				.lblCaminho.caption = "Receita"
			CASE .pageframe1.activePage = 2
				.lblCaminho.caption = "Medicamentos"		
			OTHERWISE
				.lblCaminho.caption = ""		
		ENDCASE 
	ENDWITH 	
ENDFUNC


**
FUNCTION uf_CPM_sair
	&& fecha cursores
	IF USED("ucrsCPMCab")
		fecha("ucrsCPMCab")
	ENDIF

	CPM.hide
	CPM.release
ENDFUNC
