&&
DO funcoes
DO configdemo
ON SHUTDOWN DO uf_cleanUp_ApagarRegistoCC in cleanup.prg

**
FUNCTION uf_startup_chama
	PUBLIC 	ch_vendedor, ch_vendnm, m_chnome, ;
			m_chinis, ch_userno, ch_grupo, ch_especialista, ;
			myLingua, validlicoffline
	IF !USED("uCrsUser")
		RETURN .f.
	ENDIF 
	
	** Temp Fix para limpar variaveis necess�rio rever para v15.13
	uf_startup_releaseAllPubVars()

	Select uCrsUser
	GO TOP 
	&& ALTERAR VARI�VEIS DE SISTEMA
    ch_vendedor		= uCrsUser.userno
    ch_vendnm    	= LEFT(uCrsUser.nome,20)
    m_chnome     	= uCrsUser.nome
    m_chinis     	= uCrsUser.iniciais
    ch_userno    	= uCrsUser.userno
    ch_grupo     	= uCrsUser.grupo
	ch_especialista = uCrsUser.userno
	myLingua		= 'ptPT'
	STORE .f. TO validlicoffline

	IF !EMPTY(m_chnome)
		IF UPPER(ALLTRIM(m_chnome))=="ADMINISTRADOR DE SISTEMA"
			emDesenvolvimento = .T.
		ENDIF
	ENDIF
	
	** Marcar BD como principal para a proxima vez q entrar
	SELECT uCrsEmpresas
	replace uCrsEmpresas.marcado WITH .t.

	uf_startup_startTerminal()
	
	IF (uf_gerais_addConnection('PCENTRAL') == .f.)
		uf_perguntalt_chama("N�o existem licen�as dispon�veis para abrir o software!","OK","",48)
		RETURN .F.
	ENDIF
	
    ** Activa Timer de verifica��o PHC, apenas se o parametro estiver ativo
	IF uf_gerais_getParameter("ADM0000000232", "BOOL") AND !empty(ch_userno)
		Painelcentral.timerIntegracaoPhc.enabled = .t.
	ENDIF 
	
	uf_login_sair()
	painelcentral.resize()
	
	uf_painelcentral_carregamenu(1,.t.)
	IF painelcentral.lcLoginAuto
		IF !EMPTY(ALLTRIM(painelcentral.lcComando))
			TRY 
				LOCAL lcComando
				lcComando = ALLTRIM(painelcentral.lcComando)
				DO &lcComando
			CATCH
				*
			ENDTRY
		ENDIF 
	ENDIF
	
	** VERIFICAR SE UTILIZADOR CONCORDOU COM POL�TICA DE PRIVACIDADE
	PUBLIC myNomeAbrv
	myNomeAbrv = ''
	lcSQL = ''
	TEXT TO lcSQL Noshow Textmerge
		select nomabrv from empresa (nolock) where site='<<ALLTRIM(mysite)>>'
	ENDTEXT
	IF !uf_gerais_actGrelha("","curb_nomabrv",lcSql)			
		uf_perguntalt_chama("Aten��o: Ocorreu uma anomalia ao verificar o nome abreviado da empresa. Por favor contacte o Suporte.","OK","",16)
		RETURN .f.
	ENDIF	
	myNomeAbrv = ALLTRIM(curb_nomabrv.nomabrv)
	fecha("curb_nomabrv")
	
	_screen.Caption = "Logitools Software  - " +  ALLTRIM(sql_db) + " - " + ALLTRIM(myNomeAbrv) + " - " + ALLTRIM(myTerm)
	
	PAinelCentral.Container1.lbSoftwareName.caption = "Logitools Software  - " +  ALLTRIM(sql_db) + " - " + ALLTRIM(myNomeAbrv) + " - " + ALLTRIM(myTerm)
	
	** VERIFICAR SE UTILIZADOR CONCORDOU COM POL�TICA DE PRIVACIDADE
	lcSQL = ''
	TEXT TO lcSQL Noshow Textmerge
		SELECT primacesso FROM B_US where iniciais='<<ALLTRIM(m_chinis)>>'
	ENDTEXT
	IF !uf_gerais_actGrelha("","curb_usacc",lcSql)			
		uf_perguntalt_chama("Aten��o: Ocorreu uma anomalia ao verificar o acesso do utilizador. Por favor contacte o Suporte.","OK","",16)
		RETURN .f.
	ENDIF	
	LOCAL lcconta, lcatualizou
	lcconta = 1
	lcatualizou = 0
	IF curb_usacc.primacesso=.t. then
		DO WHILE lcatualizou = 0
			lcatualizou = uf_startup_passwprimacesso(lcconta , ALLTRIM(m_chinis))
			lcconta = lcconta + 1 
		ENDDO 
		IF lcatualizou = 1
			uf_perguntalt_chama("A sua password foi alterada com sucesso. Por favor volte a entrar no software","OK","",64)
		ENDIF
		QUIT
	ENDIF 
    
    IF uf_gerais_getParameter("ADM0000000351", "BOOL") AND ALLTRIM(m_chinis) <> 'ADM'

        uv_periodo = FLOOR(uf_gerais_getParameter("ADM0000000351", "NUM"))

        uv_dataAltPwTMP = uf_gerais_getUmValor("b_us", "data_alt_pw", "iniciais='" + ALLTRIM(m_chinis) + "'")

		IF TYPE("uv_dataAltPwTMP") = "C"
			uv_dataAltPW = CTOD(uv_dataAltPwTMP)
		ELSE
			uv_dataAltPw = uv_dataAltPwTMP
		ENDIF

        IF (uv_dataAltPw + uv_periodo) <= DATE()

            uf_perguntalt_chama("A sua password expirou." + chr(13) + "Ter� de atualizar a mesma.","OK","",64)

            uv_alt = 0

            DO WHILE uv_alt = 0

                uv_alt = uf_startup_passwprimacesso(0 , ALLTRIM(m_chinis), .T.)

            ENDDO

			IF uv_alt = 1
            	uf_perguntalt_chama("A sua password foi alterada com sucesso. Por favor volte a entrar no software","OK","",64)
			ENDIF
		    QUIT

        ENDIF

    ENDIF
	
	** VERIFICAR SE UTILIZADOR CONCORDOU COM POL�TICA DE PRIVACIDADE
	lcSQL = ''
	TEXT TO lcSQL Noshow Textmerge
		SELECT aceita_termos FROM B_US where iniciais='<<ALLTRIM(m_chinis)>>'
	ENDTEXT
	IF !uf_gerais_actGrelha("","curb_us",lcSql)			
		uf_perguntalt_chama("Aten��o: Ocorreu uma anomalia ao verificar o acesso do utilizador. Por favor contacte o Suporte.","OK","",16)
		RETURN .f.
	ENDIF	
	IF curb_us.aceita_termos=.f. then
		**painelcentral.menu1.estado("inicio, trocaEmpresa, trocaUser, relatorios, mensagens, suporte, sistema", "HIDE")
		*painelcentral.menu1.estado("inicio, trocaEmpresa, trocaUser, relatorios, sistema", "HIDE")
		uf_painelcentral_alternaPagina(8)
	ENDIF 
	fecha("curb_us")
	
	** VERIFICAR EXISTE CAMBIO NOVO PARA SER APLICADO
	IF uf_gerais_getParameter("ADM0000000312", "BOOL")
		lcSQL = ''
		TEXT TO lcSQL Noshow Textmerge
			select * from cb where processado=0 and CONVERT(varchar, data, 112)=CONVERT(varchar, getdate(), 112) order by data desc
		ENDTEXT
		IF !uf_gerais_actGrelha("","cur_cambios",lcSql)			
			uf_perguntalt_chama("Aten��o: Ocorreu uma anomalia ao verificar os cambios. Por favor contacte o Suporte.","OK","",16)
			RETURN .f.
		ENDIF	
		SELECT cur_cambios
		IF RECCOUNT("cur_cambios")>0 then
			uf_perguntalt_chama("Foi inserido um novo c�mbio. Os pre�os v�o ser atualizados.","OK","",64)
			regua(1,2,"A atualizar pre�os dos artigos...")
			TEXT TO lcSQL Noshow Textmerge
				 EXEC x3_servico_ReCalculaValor ''
			ENDTEXT
			IF !uf_gerais_actGrelha("","",lcSql)			
				uf_perguntalt_chama("Aten��o: Ocorreu uma anomalia ao atualizar os artigos com o novo c�mbio. Por favor contacte o Suporte.","OK","",16)
				RETURN .f.
			ENDIF	
			TEXT TO lcSQL Noshow Textmerge
				 update cb set processado=1 where CONVERT(varchar, data, 112)=CONVERT(varchar, getdate(), 112)
			ENDTEXT
			IF !uf_gerais_actGrelha("","",lcSql)			
				uf_perguntalt_chama("Aten��o: Ocorreu uma anomalia ao atualizar os artigos com o novo c�mbio. Por favor contacte o Suporte.","OK","",16)
				RETURN .f.
			ENDIF	
			regua(2)
			uf_perguntalt_chama("Processo conclu�do com sucesso. Obrigado por aguardar","OK","",64)
		ENDIF 
		fecha("cur_cambios")
	ENDIF 
	
	

	IF  UPPER(myPaisConfSoftw) == 'ANGOLA' AND !uf_startup_validalicenca()
**		EXIT
		QUIT
	ENDIF 

ENDFUNC 


**
FUNCTION uf_startup_startTerminal
		
	

	
	&& Vari�veis P�blicas de Sistema
	PUBLIC ;
		myTerm,;					&& nome do terminal
		myTermNo,;					&& n� do terminal
		myTermStamp,;				&& stamp do terminal 
		myTicketTermNo,;			&& n� do terminal usado para xopvision
		mySite,;					&& nome da Empresa
		mySite_nr,;					&& No Empresa 
		myPosto,;					&& se � posto
		mySitePosto,;				&& nome do posto
		myCxStamp,;					&& stamp da caixa
		mySsStamp,;					&& stamp da caixa
		myArmazem,;					&& n� do armazem
		myPad,;						&& terminal m�vel
		myPosPrinter,;				&& nome da impressora de POS
		myOffline,;					&& terminal offline			
		myArquivoDigitalPdf,;		&& cria pdfs automaticamente nas vendas
		myNrVendasTalao,;			&&
		myNotForceImpRec,;			&& n�o imprime receita no atendimento
		myBackOpPass,;				&& password de backoffice
		myVirtualVar,;				
		myVirtualText,;
		myLoginUnica,;				&& usa login unica
		myUserLoginUnica,;			&& nome do utilizador da login unica
		myUsaRobot,;				&& usa robo tecnilab
		myUsaFarmax,;				&& usa robo farmax
		myUsaRobotApostore,;		&& usa robo XPTO
		myUsaRobotConsis,;			&& usa robo CONSIS
		myRobotState,;				&& Estado do robot
		myDropLocal,;				&& local de dispensa de produtos (robo)
		myRobotCodInter,;			&& codigo internacional do robo
		myVersaoLTFarmacia,;		&& Vers�o do software - Farm�cia
		myPath,;					&& Caminho para a pasta de recursos partilhados (ldata)
		myVersaoDoc,;				&&
		myCertActiva,;				&& Certifica��o est� activa 
		myCertID,;					&& Identifica��o do software na AT
		myCertVersaoChave,;			&& Vers�o da chave publica na AT 
		myBrowserPath,;				&& Caminho para o browser
		myIPservidor,;				&& Ip do servidor de analises
		myMetodoConversao,;			&& M�todo de convers�o de valor para pontos
		myValidadeVale,;			&& Prazo de validade nos vales de desconto do cart�o de cliente
		myValeIntr,;				&& Vales de desconto s�o intransmiss�veis?
		myOpAltCod,;				&& Codigo alternativo do vendedor
		nrAtendimento,;				&& usado no atendimento e factura��o
		myHora,;					&& usado no atendimento e factura��o
		myInvoiceData,;				&& usado no atendimento e factura��o
		myNrProd,;					&& N� de produtos a pedir ao robot
		myencconjunta,;				&& Contrala Multiloja
		ch_pack,;					&& controlar moeda em vers�o antigas de software
		myLogoPath,;				&& imagem definida na ficha completa da empresa
		myPmeLogoPath,;				&& impress�o imagem pme lider path fixo em \imagens\Geral\pme_lider.png
		myPmeLogoCustomize,;  		&& variable booleana que diz se temos PME_lider customizada.
		myInputmaskMoeda,;			&& InputMask para os campos do tipo moeda
		myCurencySymbol,;			&& InputMask para os campos do tipo moeda nas impress�es OPOS
		difhoraria,; 				&& Diferen�a hora�ria para grava��o dos documentos
		myDefFolder,;				&& Pasta por defeito para ir buscar ficheiros
		myGDFolder,;				&& Pasta de arquivo da gest�o documental do cliente
		myPrevFolder,;				&& Pasta selecionada anteriormente
		myDefDir,;					&& Diretoria default do programa
		myServicosSite,;			&& Prefixo para o site do envio dos servi�os
		myServicosUser,;			&& Username para o site do envio dos servi�os
		myServicosPassw,;			&& Password para o site do envio dos servi�os
		myServicosEntApp,;			&& Servi�os - Entity app
		myServicosEntId,;			&& Servi�os - Entity id
		myServicosEntName,;			&& Servi�os - Entity name
		myServicosSiteSuf,;			&& Sufixo para o site do envio dos servi�os
		myTipoCartao,;				&& Controla se utiliza o cart�o de fideliza��o para pontos ou valor
		myUsaServicosRobot,;		&& Controla se servi�os para se interligar com os robots
		myTipoRobot,;				&& Identifica o robot a interligar
		myServRobotTOsta,;			&& Timeout servi�o robot - status 
		myServRobotTOGPI,; 			&& Timeout servi�o robot - get product info 
		myServRobotTOret,; 			&& Timeout servi�o robot - retrieve
		myServRobotTOinv,; 			&& Timeout servi�o robot - inventory
		myServRobotDeleteStorPl,;	&& Op��o na dispensa do robot deleteStoragePlace
		myPaisConfSoftw,;			&& Pa�s para as configura��es do software
		myStampNCSeg,;				&& Stamp da NC � seguradora a imprimir
		myTipoClock,;               && id do pica ponto
		myUsaServicosClock,;        && Controla se servi�os para se interligar com os relogios de ponto
		myPagCentalDinherio,;       && Pagamentos Centralizados, apenas envia dinheiro para os pagamentos centralizados
		myAvisaChamadas,;
		myLogoPathEtiq,;			&& Logotipo mais pequeno para utilizar nas etiquetas
		myEncodingFileJar,;			&& criacao de logs em utf8 chamados pelo java
		myServerName,;			    && Nome do servidor actual
		myTermXopPos,;
		myXopPosUrl,;
		myXopPosCred,;
		mySimboloMoeda,;
		myCompanyuserMVO,;
		myCompanypasswMVO	
		
	STORE '' TO myCompanyuserMVO, myCompanypasswMVO, myXopPosUrl, myXopPosCred
		
	**STORE '19000101' TO myInvoiceData
	STORE 0 TO myNrProd, myTermXopPos
	
	&& Inicializa variaveis de sistema
	STORE '' TO ;
		mySitePosto,;
		myDropLocal,;
		myRobotCodInter,;
		myBrowserPath,;
		myIPservidor,;
		myLogoPath,;
		myPmeLogoPath,;
		myInputmaskMoeda,;
		myStampNCSeg,;
		myLogoPathEtiq,;
		myServerName,;
		mySsStamp,;
		myCxStamp
		
	
	&& Default
	STORE 1 TO mySite_nr
	
	&&
	STORE .f. TO mySitePosto, myAvisaChamadas
	
	&&
	STORE 0 TO difhoraria
	
	&&
	STORE 0 TO myPagCentalDinherio
	
	&&
	STORE ' -Dfile.encoding=UTF-8 ' TO myEncodingFileJar
	
	
	myServerName = GETWORDNUM(SYS(0),1)
	
	IF USED("ucrsuser")
	
		myCompanyuserMVO = IIF(TYPE("ucrsuser.companyuser") <> "U", ALLTRIM(ucrsuser.companyuser), "")
		myCompanypasswMVO = IIF(TYPE("ucrsuser.companypassw") <> "U", ALLTRIM(ucrsuser.companypassw), "")

		IF !EMPTY(myCompanypasswMVO)

			loCrypt = CreateObject('Chilkat_9_5_0.Crypt2')
			s = ALLTRIM(myCompanypasswMVO)
			loCrypt.CryptAlgorithm = "none"
			loCrypt.EncodingMode = "base64"
			lcStrBase64 = loCrypt.EncryptStringENC(s)
			myCompanypasswMVO = ALLTRIM(lcStrBase64)

		ENDIF
	ENDIF
	
		
	&& Vari�veis P�blicas de Modulos
	PUBLIC myCartaoClienteLt,;		&& tem acesso ao m�dulo: Cart�o Cliente
		mySMS,;					&& tem acesso ao m�dulo: Alertas SMS
		myInventario,;			&& tem acesso ao m�dulo: Invent�rio
		myPagCentral,;			&& tem acesso ao m�dulo: Pagamentos centralizado
		myReservasCl,;			&& tem acesso ao m�dulo: Reservas de clientes
		myArquivoDigital,;		&& tem acesso ao m�dulo: Arquivo Digital
		myPromocoes				&& tem acesso ao m�dulo: Promo��es
	
	&& Variaveis publicas de apps
	PUBLIC myStartup, myCl, myStock, myProg, myPag, myReg, myRobot, myTpa, myCashDro ,mySFO, myPsico, myForn, myDic, myGerais, myReservas, myCampanhaSMS, myServicos 
	PUBLIC MyDocumentosApp, MyFacturacaoApp, MyLigacoesApp, MyReceituarioApp, MyAnalisesApp, MyPromocoesApp, MyEmpresaApp, MyUtilizadoresApp, MyPrescApp, MyMarcacoesApp, myIntegracoesPHC
	PUBLIC myArqDigitalApp, MyClinicaApp, MyPainelCentralApp, MyAssociadosApp, MyConvencoesApp, MyCampanhasApp, MySmsApp
	
	&& Variaveis publicas para series de documentos
	PUBLIC myVd, myFact, myRegCli, mySEntSNS, mySEnt
	PUBLIC myVdNm, myFactNm, myRegCliNm, mySEntSNSNm, mySEntNm
	
	STORE '' TO myPad, myVersaoDoc, myUserLoginUnica, myVersaoLTFarmacia
	STORE 0 TO myNrVendasTalao
	STORE .f. TO myPagCentral

	** Criar e Inicializar vari�veis publicas para controlo do estado dos paineis
	PUBLIC myDocIntroducao, myDocAlteracao, myTipoDoc, myFtIntroducao, myFtAlteracao, myClIntroducao, myClAlteracao, myStocksIntroducao, myStocksAlteracao, myFlAlteracao, myFlIntroducao, myVacIntroducao, myVacAlteracao
	PUBLIC mySMSIntroducao, mySMSAlteracao, myPromoIntroducao, myPromoAlteracao, myEmpIntroducao, myEmpAlteracao, myUtIntroducao, myUtAlteracao, myUGIntroducao, myUGAlteracao, myInventarioIntroducao, myInventarioAlteracao
	PUBLIC myPagFornIntroducao, myPagFornAlteracao,	myESCALOESPVIntroducao,	myESCALOESPVAlteracao

	** Configura Monitor de Mensagens para ficar centrado (IPAD) 
	FOR EACH MF IN _SCREEN.FORMS
		IF MF.NAME == "SMGREC"
			SMGREC.AUTOCENTER = .T.
			EXIT
		ENDIF
	ENDFOR

	&& Criar vari�veis P�blicas de envio de Email's
	PUBLIC mySMTPServer,;		&& servidor de smtp
		mySMTPUsername,;		&& utilizador de autentica��o smtp
		mySMTPPassword,;		&& password servidor SMTP
		mySMTPPort,;			&& porta do servidor SMTP
		mySMTPSSL,;				&& utiliza SSL no servidor SMTP
		mySMTPTLS,;				&& utiliza TLS no servidor SMTP
		myEmailSendAccount,;	&& Conta de envio de email
		myEmailCC,;				&& Contas para envio CC
		myEmailBCC,;			&& Contas para envio BCC
		mySendEmail
	
	PUBLIC myEMPSMTPServer,;		&& servidor de smtp da empresa
		myEMPSMTPUsername,;		&& utilizador de autentica��o smtp da empresa
		myEMPSMTPPassword,;		&& password servidor SMTP da empresa
		myEMPSMTPPort,;			&& porta do servidor SMTP da empresa
		myEMPSMTPSSL,;				&& utiliza SSL no servidor SMTP da empresa
		myEMPSMTPTLS,;				&& utiliza TLS no servidor SMTP da empresa
		myEMPEmailSendAccount,;	&& Conta de envio de email da empresa
		myEMPEmailCC,;				&& Contas para envio CC da empresa
		myEMPEmailBCC,;			&& Contas para envio BCC da empresa
		myEMPSendEmail
	
	*********************
	** Carrega Funcoes **
	*********************
	uf_startup_carregaFuncoes()
	*********************
	
			*******************************************
	********** MUDA CORES DA FARM�CIA *********
	*******************************************
	
	
	PUBLIC ;
		myColorFundo,;
		myColorMenu,;
		myColorPagina,;
		myColorPaginaAtual,;
		myColorMenuUser,;
		myColorSucesso,;
		myColorAviso,;
		myColorErro,;
		myColorBotao
		
	STORE RGB(0,0,0) TO myColorFundo, myColorMenu, myColorPagina, myColorPaginaAtual, myColorMenuUser, myColorSucesso, myColorAviso, myColorErro,myColorBotao
	
	
	
	LOCAL lcColorFundo, lcColorMenu, lcColorPagina, lcColorPaginaAtual, lcColorMenuUser, lcColorSucesso, lcColorAviso, lcColorErro, lcColorBotao
	STORE '0,0,0' TO lcColorFundo, lcColorMenu, lcColorPagina, lcColorPaginaAtual, lcColorMenuUser, lcColorSucesso, lcColorAviso, lcColorErro, lcColorBotao
	
	lcColorFundo		= uf_gerais_getparameter('ADM0000000235', 'TEXT')
	lcColorMenu			= uf_gerais_getparameter('ADM0000000202', 'TEXT')
	lcColorPagina		= uf_gerais_getparameter('ADM0000000379', 'TEXT')
	lcColorPaginaAtual	= uf_gerais_getparameter('ADM0000000383', 'TEXT')
	lcColorMenuUser 	= uf_gerais_getparameter('ADM0000000333', 'TEXT')
	lcColorSucesso		= uf_gerais_getparameter('ADM0000000380', 'TEXT')
	lcColorAviso		= uf_gerais_getparameter('ADM0000000381', 'TEXT')
	lcColorErro			= uf_gerais_getparameter('ADM0000000382', 'TEXT')
	lcColorBotao		= uf_gerais_getparameter('ADM0000000378', 'TEXT')
	
	
	myColorFundo		= RGB(&lcColorFundo)
	myColorMenu			= RGB(&lcColorMenu)
	myColorPagina		= RGB(&lcColorPagina)
	myColorPaginaAtual	= RGB(&lcColorPaginaAtual)
	myColorMenuUser 	= RGB(&lcColorMenuUser)
	myColorSucesso		= RGB(&lcColorSucesso)
	myColorAviso		= RGB(&lcColorAviso)
	myColorErro			= RGB(&lcColorErro)
	myColorBotao		= RGB(&lcColorBotao)

	
	
	
	***************************
	** PARAMETRIZAR TERMINAL **
	***************************
	PUBLIC myClientName
	IF !EMPTY(myClientName) && Vers�o nova

		** Caso seja uma BD Demo, pergunta configura��o pretendida
		IF  LIKE('*DEV*', UPPER(sql_db))
			uf_configdemo_chama()
		ENDIF 
		**
		
		lcSQL = ''	
		TEXT TO lcSQL NOSHOW TEXTMERGE
			EXEC [dbo].[up_gerais_configTerminal]
				@ClientName = '<<myClientName>>',
				@Conta = '<<myUsername>>',
				@SessionName = '<<mySessionName>>'
		ENDTEXT 
		
		IF uf_gerais_actgrelha("", "uCrsTerm", lcSQL)
			IF reccount("uCrsTerm") > 0
				Select uCrsTerm
				myTerm		= ALLTRIM(uCrsTerm.terminal)
				myTermNo	= uCrsTerm.no
				myPad		= ALLTRIM(uCrsTerm.mobile)
				myDropLocal	= ALLTRIM(uCrsTerm.drop_location)
				myTermStamp = ALLTRIM(uCrsTerm.tstamp)
				IF !IsNull(uCrsTerm.xopvision_terminal_id)
					&& create and assign xopvision terminal id
					Public my_xopvision_term
					 my_xopvision_term = uCrsTerm.xopvision_terminal_id
				ENDIF
				
				myTermXopPos = uCrsTerm.xopvision_pos_terminal_id
			ELSE 
				uf_perguntalt_chama("N�O FOI POSS�VEL CONFIGURAR O TERMINAL. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				RETURN .f.
			ENDIF 
			fecha("uCrsTerm")
		ENDIF
	ELSE
		
		** Caso seja uma BD Demo, pergunta configura��o pretendida
		IF LIKE('*DEV*', UPPER(sql_db))
			uf_configdemo_chama()
		ENDIF 
		
		LOCAL lcCname, lcPath
		STORE '' TO lcCname, lcPath

		lcCname	= GETENV("COMPUTERNAME")
		lcPath = Left(CURDIR(),LEN(CURDIR())-1)
		lcPath = right(lcPath,LEn(lcPath)-1)

		lcSQL = ''
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			SELECT TOP 1 * FROM b_terminal WHERE (machine = '<<ALLTRIM(lcCname)>>' OR machine = '') AND path = '<<ALLTRIM(lcPAth)>>' ORDER BY udata desc
		ENDTEXT
		
		**_cliptext = lcSQL
							
		IF uf_gerais_actgrelha("", "uCrsTerm",lcSQL)
			
			IF RECCOUNT ("uCrsTerm") > 0
				SELECT uCrsTerm
				myTerm		= ALLTRIM(uCrsTerm.terminal)
				myTermNo	= uCrsTerm.no
				myPad		= ALLTRIM(uCrsTerm.mobile)
				myDropLocal	= ALLTRIM(uCrsTerm.drop_location)
				myTermXopPos = uCrsTerm.xopvision_pos_terminal_id
			ELSE
				MESSAGEBOX("N�O FOI ENCONTRADA INFORMA��O RELATIVA AO TERMINAL." + CHR(13) + CHR(13) + "POR CONTACTE O SUPORTE.",64)
				QUIT
			ENDIF   
			
			fecha("uCrsTerm")
		ENDIF
		
	ENDIF 
*!*		IF VARTYPE("ch_userno")<>'N'
		IF ALLTRIM(lcTipoEntrada) = 'PORTAL'
			RELEASE ch_userno
			PUBLIC ch_userno
			ch_userno = 1
			mySite 	= 'Loja 1'
			myArmazem = 1
		ELSE 
		** Controla site baseado na tabela de Utilizadores
			lcSQL = ""
			TEXT TO lcSQL NOSHOW TEXTMERGE
				select TOP 1
					b_us.userno
					,site = loja
					,empresa_arm.armazem
				from 
					B_us (nolock)
					inner join empresa on empresa.site = B_us.loja
					inner join empresa_arm on empresa.no = empresa_arm.empresa_no
				where 
					userno = <<ch_userno>>
				order by 
					empresa_arm.armazem
			ENDTEXT 
			
			
			
			IF uf_gerais_actgrelha("", "uCrsSiteUs",lcSQL)
				IF reccount("uCrsSiteUs") > 0
					&& Aplicada condicao pq quando s�o recarregadas as funcoes o software nao pode atualizar o mysite
					&&IF EMPTY(mysite)
						Select uCrsSiteUs
						mySite 	= ALLTRIM(uCrsSiteUs.site)
						myArmazem = IIF(EMPTY(uCrsSiteUs.armazem),1,uCrsSiteUs.armazem)
					&&ENDIF
				ENDIF 
				fecha("uCrsSiteUs")
			ENDIF
		ENDIF 

	*****************************************
	** Configurar Display Duplo **
	*****************************************

		myXopPosUrl = uf_gerais_getParameter_site("ADM0000000172", "TEXT", mySite)
		myXopPosCred = uf_gerais_getParameter_site("ADM0000000173", "TEXT", mySite)

	*****************************************
	** Configurar Painel Central **
		*****************************************
	IF !uf_gerais_getparameter_site('ADM0000000084', 'BOOL', mySite)
		TEXT TO lcSQL NOSHOW TEXTMERGE
			exec up_manutencao_ActualizaPainelCentralPelosTerminais 0, '<<mySite>>', <<IIF(TYPE("myTermNo") == "U", 0, myTermNo)>>, <<ch_userno>>
		ENDTEXT 
	ELSE
		TEXT TO lcSQL1 NOSHOW TEXTMERGE
        	delete from b_pcentral WHERE terminal = <<IIF(TYPE("myTermNo") == "U", 0, myTermNo)>>
        ENDTEXT
        uf_gerais_actgrelha("", "", lcSQL1)
            
		IF ALLTRIM(ch_grupo) = 'Operadores'
			TEXT TO lcSQL NOSHOW TEXTMERGE
				exec up_manutencao_ActualizaPainelCentralPelosTerminais_soatendimento 0, '<<mySite>>'
			ENDTEXT
		ELSE
			TEXT TO lcSQL NOSHOW TEXTMERGE
				exec up_manutencao_ActualizaPainelCentralPelosTerminais 0, '<<mySite>>', <<IIF(TYPE("myTermNo") == "U", 0, myTermNo)>>, <<ch_userno>>
			ENDTEXT
		ENDIF 
	ENDIF 
	_Cliptext = lcSql
	IF !uf_gerais_actgrelha("", "", lcSQL)
		MESSAGEBOX("OCORREU UMA ANOMALIA A CONFIGURAR O PAINEL CENTRAL.", 16, "LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF 

	
	*****************************************
	** Cria cursor com os dados da empresa **
	*****************************************
	
	
	IF !uf_startup_sqlExec([exec up_gerais_dadosEmpresa ']+ALLTRIM(mySite)+['], "uCrsE1")
		MESSAGEBOX("OCORREU UM ERRO A GERAR DADOS DA EMPRESA.", 16, "LOGITOOLS SOFTWARE")
		QUIT
	ELSE
		SELECT uCrsE1
		myPosto = uCrsE1.posto
		mySitePosto = uCrsE1.siteposto
		mySite_nr = uCrsE1.siteno
	ENDIF
	
	*****************************************
	** Cria cursor com os dados das Lojas  **
	*****************************************
	IF !uf_gerais_actgrelha("", "uCrsLojas", [exec up_gerais_dadosLojas ''])
		MESSAGEBOX("OCORREU UM ERRO A GERAR DADOS DAS LOJAS.",16,"LOGITOOLS SOFTWARE")
		QUIT
	ENDIF
	
	***********************************
	** Cria cursor com os par�metros **
	***********************************
	IF !uf_gerais_actgrelha("", "uCrsParameters", [exec up_parameters])
		MESSAGEBOX("OCORREU UM ERRO A LER OS PAR�METROS.",16,"LOGITOOLS SOFTWARE")
		QUIT
	ENDIF
	
	IF !uf_gerais_actgrelha("", "uCrsParameters_servicos", [exec up_parameters_servicos])
		MESSAGEBOX("OCORREU UM ERRO A LER OS PAR�METROS DOS SERVI�OS.",16,"LOGITOOLS SOFTWARE")
		QUIT
	ENDIF
	
	******************************************
	** Cria cursor com perfis de Utilizador **
	******************************************	
	TEXT TO lcSQL NOSHOW TEXTMERGE
		exec up_perfis_validaAcessoPaineis '<<mySite>>'
	ENDTEXT 

	If !uf_gerais_actgrelha("", "uCrsPfUtilizador", lcSQL)
		MESSAGEBOX("OCORREU UMA ANOMALIA AO VERIFICAR OS PERFIS DE UTILIZADOR.", 16, "LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF 
	
	*********************************
	** Cria cursor com dados da TD **
	*********************************
	IF !uf_gerais_actgrelha("", "uCrsGeralTD", [select * from td(nolock) ])
		MESSAGEBOX("OCORREU UM ERRO A LER A TABELA [TD].",16,"LOGITOOLS SOFTWARE")
		QUIT
	ENDIF
	
	***********************************
	**   Cria cursor com as moedas   **
	***********************************
	IF !uf_gerais_actgrelha("", "uCrsMoedas", [SELECT 'EURO' as MOEDA UNION ALL SELECT distinct MOEDA FROM CB (nolock)])
		MESSAGEBOX("OCORREU UM ERRO A LER OS PAR�METROS.",16,"LOGITOOLS SOFTWARE")
		QUIT
	ENDIF
	

	
	
	***********************************
	**   Cria cursor com as TPA_MESSAGE**
	***********************************
	uf_startup_createTpaMessage()

	***********************************
	** Carrega dados da certifica��o **
	***********************************
	myCertActiva		= uf_gerais_getparameter('ADM0000000078', 'BOOL')
	myCertID			= uf_gerais_getparameter('ADM0000000078', 'TEXT')
	myCertVersaoChave	= uf_gerais_getparameter('ADM0000000078', 'NUM')


	&& Preencher vari�veis P�blicas de envio de Email's
	
	IF !uf_gerais_actgrelha("", "uCrsConfEmail", [select email_smtp, email_username, email_password, email_port, email_ssl, email_tls, email_sender, email_cc, email_bcc from empresa where site=']+ALLTRIM(mySite)+[' ])
		MESSAGEBOX("OCORREU UM ERRO A LER OS DADOS DE CONFIGURA��ES DO ENVIO DOS EMAILS .",16,"LOGITOOLS SOFTWARE")
		mySendEmail = .f.
		QUIT
	ENDIF
	
	IF !EMPTY(ALLTRIM(uCrsConfEmail.email_smtp)) AND !EMPTY(ALLTRIM(uCrsConfEmail.email_username)) AND !EMPTY(ALLTRIM(uCrsConfEmail.email_password));
		AND !EMPTY(ALLTRIM(uCrsConfEmail.email_port)) AND !EMPTY(ALLTRIM(uCrsConfEmail.email_sender))
			mySendEmail = .t.
	ELSE
			mySendEmail = .f.
	ENDIF 
	mySMTPServer = ALLTRIM(uCrsConfEmail.email_smtp)
	mySMTPUsername = ALLTRIM(uCrsConfEmail.email_username)
	mySMTPPassword = ALLTRIM(uCrsConfEmail.email_password)
	mySMTPPort = ALLTRIM(uCrsConfEmail.email_port)
	IF uCrsConfEmail.email_ssl = .t.
		mySMTPSSL = 1
	ELSE 
		mySMTPSSL = 0
	ENDIF 
	IF uCrsConfEmail.email_tls = .t.
		mySMTPTLS = 1
	ELSE 
		mySMTPTLS = 0
	ENDIF 

	myEmailSendAccount = ALLTRIM(uCrsConfEmail.email_sender)
	myEmailCC = ALLTRIM(uCrsConfEmail.email_cc)
	myEmailBCC = ALLTRIM(uCrsConfEmail.email_bcc)
	
	** DADOS EMAIL DA EMPRESA
	IF !uf_gerais_actgrelha("", "uCrsEMPConfEmail", [select emp_email_smtp, emp_email_username, emp_email_password, emp_email_port, emp_email_ssl, emp_email_tls, emp_email_sender, emp_email_cc, emp_email_bcc from empresa where site=']+ALLTRIM(mySite)+[' ])
		MESSAGEBOX("OCORREU UM ERRO A LER OS DADOS DE CONFIGURA��ES DO ENVIO DOS EMAILS .",16,"LOGITOOLS SOFTWARE")
		mySendEmail = .f.
		QUIT
	ENDIF
	
	IF !EMPTY(ALLTRIM(uCrsEMPConfEmail.emp_email_smtp)) AND !EMPTY(ALLTRIM(uCrsEMPConfEmail.emp_email_username)) AND !EMPTY(ALLTRIM(uCrsEMPConfEmail.emp_email_password));
		AND !EMPTY(ALLTRIM(uCrsEMPConfEmail.emp_email_port)) AND !EMPTY(ALLTRIM(uCrsEMPConfEmail.emp_email_sender))
			myEMPSendEmail = .t.
	ELSE
			myEMPSendEmail = .f.
	ENDIF 
	myEMPSMTPServer = ALLTRIM(uCrsEMPConfEmail.emp_email_smtp)
	myEMPSMTPUsername = ALLTRIM(uCrsEMPConfEmail.emp_email_username)
	myEMPSMTPPassword = ALLTRIM(uCrsEMPConfEmail.emp_email_password)
	myEMPSMTPPort = ALLTRIM(uCrsEMPConfEmail.emp_email_port)
	IF uCrsEMPConfEmail.emp_email_ssl = .t.
		myEMPSMTPSSL = 1
	ELSE 
		myEMPSMTPSSL = 0
	ENDIF 
	IF uCrsEMPConfEmail.emp_email_tls = .t.
		myEMPSMTPTLS = 1
	ELSE 
		myEMPSMTPTLS = 0
	ENDIF 

	myEMPEmailSendAccount = ALLTRIM(uCrsEMPConfEmail.emp_email_sender)
	myEMPEmailCC = ALLTRIM(uCrsEMPConfEmail.emp_email_cc)
	myEMPEmailBCC = ALLTRIM(uCrsEMPConfEmail.emp_email_bcc)
	
	fecha("uCrsEMPConfEmail")
	
	** Vari�veis para Gest�o Documental
	** Tem que se criar esta pasta no pc do cliente	
	STORE "\\tsclient\C\Users\Public\Desktop\AnexosLogitools" TO myDefFolder
	STORE "'\\10.100.200.16\storage\"+alltrim(ucrse1.id_lt)+"'" TO myGDFolder
	STORE "" TO myPrevFolder
	STORE FULLPATH(CurDir()) TO myDefDir

	** Vai buscar aos parametros como vai utilizar o cart�o de fideliza��o de cliente
	myTipoCartao = uf_gerais_getparameter('ADM0000000302', 'TEXT')
	
	** Vai buscar aos parametros o pa�s para as configura��es do software
	myPaisConfSoftw = uf_gerais_getParameter_site('ADM0000000050', 'TEXT', mySite)
	
	********************************
	** Vari�vel CH_PACK (ano PHC) **
	********************************
	ch_pack = uf_gerais_getparameter('ADM0000000200', 'TEXT')
	
	** elimina o registo na tabela dispensa_eletronica caso exista nas clinicas - por causa da certifica��o das RSP
*!*		IF UPPER(ALLTRIM(uCrsE1.tipoEmpresa)) == "CLINICA"
*!*			lcSQL = ''
*!*			TEXT TO lcSQL NOSHOW TEXTMERGE 
*!*				delete dcc
*!*				from dispensa_eletronica_cc dcc
*!*				inner join b_us b
*!*					on dcc.nif = b.ncont
*!*				where b.userno = <<ch_vendedor>>
*!*			ENDTEXT 

*!*			IF !uf_gerais_actgrelha("","", lcSQL)
*!*				uf_perguntalt_chama("Ocorreu uma anomalia ao verificar ao informa��o do Cart�o do Cidad�o. Por favor contacte o Suporte.", "OK", "", 64)
*!*				RETURN .t.		
*!*			ENDIF
*!*		ENDIF
	
*!*		
*!*		** valida se o cart�o de cidad�o est� inserido de forma "ativar" a prescri��o de RSPs
*!*		IF UPPER(ALLTRIM(uCrsE1.tipoEmpresa)) == "CLINICA"
*!*			PUBLIC myPrescMaterializada
*!*			STORE .t. TO myPrescMaterializada
*!*			
*!*			lcSQL = ''
*!*			TEXT TO lcSQL NOSHOW TEXTMERGE 
*!*				SELECT cc.id FROM b_us b (nolock) INNER JOIN dispensa_eletronica_cc cc (nolock) ON b.ncont = cc.nif WHERE b.userno = <<ch_vendedor>>
*!*			ENDTEXT 
*!*			IF !uf_gerais_actgrelha("","uCrsIDAux", lcSQL)
*!*				uf_perguntalt_chama("Ocorreu uma anomalia ao verificar ao informa��o do Cart�o do Cidad�o. Por favor contacte o Suporte.", "OK", "", 64)
*!*				RETURN .F.		
*!*			ELSE
*!*				IF RECCOUNT("uCrsIDAux") != 1
*!*					uf_perguntalt_chama("N�o existe registo de utilizador que permita efetuar Prescri��es Desmaterializadas. As Prescri��es a efetuar ser�o Materializadas.", "OK", "", 64)	
*!*					myPrescMaterializada = .t.	
*!*				ELSE
*!*					myPrescMaterializada = .f.
*!*				ENDIF
*!*			ENDIF
*!*		ENDIF
	**
	
	** Logo da empresa 
	IF FILE(ALLTRIM(ucrse1.imagem))
		myLogoPath = ALLTRIM(ucrse1.imagem)
	ENDIF
	IF FILE(ALLTRIM(ucrse1.logoetiqueta))
		myLogoPathEtiq = ALLTRIM(ucrse1.logoetiqueta)
	ENDIF
	
	** imagem para cliente pme_lider
	IF uCrsE1.pme_lider == .t.
	 	myPmeLogoCustomize= uf_gerais_getParameter_site('ADM0000000048', 'BOOL', mySite)
		IF myPmeLogoCustomize ==.f.
			myPmeLogoPath = mypath + "\imagens\Geral\pme_lider.png" 
		ELSE 
			LOCAL namePmeLogoPath
			namePmeLogoPath=uf_gerais_getParameter_site('ADM0000000048', 'TEXT', mySite)
			IF !EMPTY(namePmeLogoPath)   
				myPmeLogoPath = mypath + namePmeLogoPath
			ELSE 
				myPmeLogoCustomize =.f.
				myPmeLogoPath = mypath + "\imagens\Geral\pme_lider.png"
			ENDIF 
		ENDIF 
	ELSE 
		myPmeLogoCustomize =.f.
	ENDIF 
	
	&& valida os dados necess�rios para a certifica��o
	IF EMPTY(ALLTRIM(myCertID)) OR EMPTY(myCertVersaoChave)
		messagebox("OS DADOS DE IDENTIFICA��O DA A.T. N�O EST�O CORRECTAMENTE PREENCHIDOS. "+ CHR(13) +"O SOFTWARE VAI SER ENCERRADO COMO MEDIDA DE PREVEN��O.",16,"LOGITOOLS SOFTWARE")
		QUIT
	ENDIF
		
	*******************************************
	** Verifica Vers�o do Logitools Farmacia **
	*******************************************
	myVersaoLTFarmacia =  uf_gerais_getparameter('ADM0000000156', 'TEXT')
	
	
	
	
	

	****************************************
	** Verifica se usa autentica��o �nica **
	****************************************
	myLoginUnica 		= uf_gerais_getparameter('ADM0000000155', 'BOOL')
	myUserLoginUnica 	= uf_gerais_getparameter('ADM0000000155', 'TEXT')
	
	********************************
	** Verifica se usa inventario **
	********************************
	myInventario = uf_gerais_getparameter('ADM0000000081', 'BOOL')

	**multiloja
	myencconjunta = uf_gerais_getparameter('ADM0000000193', 'BOOL')

	********************
	** Set Impressora **
	********************
	myPosPrinter = set("printer",2)
	myPosPrinter = 'Generic / Text Only'
	
	** caso use A6 no Atendimento **
	IF uf_gerais_getparameter('ADM0000000126', 'BOOL')
		myPosPrinter = 'Talao'
	ENDIF

	&& Verificar se a Impressora � TS ou se tem impressora predifinida 
	IF uf_gerais_actgrelha("", "uCrsTermTs", [select top 1 ts, posprinter from B_terminal where no=]+astr(myTermNo)+[ order by udata desc])
		IF RECCOUNT("uCrsTermTS") > 0
			IF !EMPTY(ALLTRIM(uCrsTermTs.posprinter)) AND LEN(ALLTRIM(uCrsTermTs.posprinter)) > 2
				myPosPrinter = ALLTRIM(uCrsTermTs.posprinter)
			ELSE
				IF uCrsTermTs.ts == .t.
					** verificar vers�o do windows **
					LOCAL lcWindowsVersion, lcWindowsLocal
					STORE 'EN' TO lcWindowsLocal
					lcWindowsVersion = GETENV("homepath")
					
					lcWindowsLocal = uf_gerais_getparameter('ADM0000000085', 'TEXT')

					
					** verificar session id **
					LOCAL b
					STORE '' TO b
					b = uf_gerais_getRdpIdFromStr()
					
					LOCAL idRemote
					idRemote = VAL(ALLTRIM(b))
					b=ALLTRIM(STR(idRemote))

					** guardar nome da impressora
					IF !(ALLTRIM(lcWindowsLocal)=='PT')
						IF UPPER(LEFT(ALLTRIM(lcWindowsVersion),2))=="\U" OR ( UPPER(LEFT(ALLTRIM(lcWindowsVersion),1))=="\" AND !(UPPER(LEFT(ALLTRIM(lcWindowsVersion),2))=="\D") )
							myPosPrinter = myPosPrinter + ' (redirected ' + ALLTRIM(b) + ')'
						ELSE
							myPosPrinter = myPosPrinter + ' (from ' + UPPER(ALLTRIM(getenv("clientname"))) + ') in session ' + ALLTRIM(b)
						ENDIF
					ELSE
						*IF UPPER(LEFT(ALLTRIM(lcWindowsVersion),2))=="\U" OR ( UPPER(LEFT(ALLTRIM(lcWindowsVersion),1))=="\" AND !(UPPER(LEFT(ALLTRIM(lcWindowsVersion),2))=="\D") )
							myPosPrinter = myPosPrinter + ' (redireccionado ' + ALLTRIM(b) + ')'
						*ELSE
						*	myPosPrinter = myPosPrinter + ' (de ' + UPPER(ALLTRIM(getenv("clientname"))) + ') in session ' + ALLTRIM(b)
						*ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		fecha("uCrsTermTs")
	ENDIF
	
	
	** Verificar se os Pagamentos S�o Centralizados 
	IF uf_gerais_getparameter('ADM0000000079', 'BOOL')
		SELECT ucrsLojas
		GO TOP
		SCAN FOR UPPER(ALLTRIM(uCrsLojas.site)) == UPPER(ALLTRIM(mySite))
			IF uCrsLojas.pagcentral == 1
				myPagCentral = .t.
			ENDIF
		ENDSCAN
	ENDIF
	
	IF !uf_gerais_actgrelha("", "uCrsInputMask", [select top 1 inputmask from moedas (nolock) where simbolo=']+ALLTRIM(uf_gerais_getParameter_site('ADM0000000004', 'text', mySite))+['])
		MESSAGEBOX("OCORREU UM ERRO A LER A INPUTMASK DA MOEDA.",16,"LOGITOOLS SOFTWARE")
		QUIT
	ENDIF
	myInputmaskMoeda = ALLTRIM(uCrsInputMask.inputmask)
	FECHA("uCrsInputMask")
	
	difhoraria = uf_gerais_getParameter_site('ADM0000000008', 'num')
	
	mySimboloMoeda = uf_gerais_getParameter_site("ADM0000000004", "TEXT", mysite)
	myCurencySymbol = uf_gerais_getParameter_site("ADM0000000005", "TEXT", mysite)

	** Verifica se Usa Arquivo Digital de Tal�es e PDFs 
	myArquivoDigital = uf_gerais_getparameter('ADM0000000087', 'BOOL')

	IF uf_gerais_getparameter('ADM0000000107', 'BOOL')
		IF uf_startup_existeImpPdfCreator('PDFCREATOR')
			myArquivoDigitalPdf = .t.
		ENDIF
	ENDIF
	
	****************************************
	** Verifica se Usa Modulo de Reservas **
	****************************************
	myReservasCl = uf_gerais_getparameter('ADM0000000142', 'BOOL')
	
	****************************************
	** Verifica se Usa Modulo de Promocoes **
	****************************************
	myPromocoes = uf_gerais_getparameter('ADM0000000074', 'BOOL')
	
	***********************************
	** Verifica se Usa Modulo de SMS **
	***********************************
	**mySMS = uf_gerais_getparameter('ADM0000000143', 'BOOL') - alterado a 20190228 para os parametros da empresa
	mySMS = uf_gerais_getparameter_site('ADM0000000022', 'BOOL', mySite)
	
	
	***************************************************
	** Verifica se interliga aos robots com servi�os **
	***************************************************
	myUsaServicosRobot 		= uf_gerais_getparameter_site('ADM0000000032', 'BOOL', mySite)
	myTipoRobot 			= uf_gerais_getparameter_site('ADM0000000033', 'TEXT', mySite)
	myServRobotTOsta 		=  uf_gerais_getparameter_site('ADM0000000034', 'NUM', mySite)
	myServRobotTOGPI 		=  uf_gerais_getparameter_site('ADM0000000035', 'NUM', mySite)
	myServRobotTOret 		=  uf_gerais_getparameter_site('ADM0000000036', 'NUM', mySite)
	myServRobotTOinv 		=  uf_gerais_getparameter_site('ADM0000000037', 'NUM', mySite)
	myServRobotDeleteStorPl =  uf_gerais_getparameter_site('ADM0000000038', 'BOOL', mySite)
	
	
	***************************************************
	** Verifica se interliga aos relogios de ponto 
	***************************************************	
	myUsaServicosClock 		= uf_gerais_getparameter_site('ADM0000000095', 'BOOL', mySite)
	myTipoClock 			= uf_gerais_getparameter_site('ADM0000000095', 'TEXT', mySite)

	*************************************************
	** Verifica Series de Documentos de Factura��o **
	*************************************************
	myVdNm = "Venda a Dinheiro"
	myFactNm = "Factura"
	myRegCliNm = "Reg. a Cliente"
	mySEntSNSNm = "Factura SNS"
	mySEntNm = "Factura Entidades"
	
	IF uf_gerais_actgrelha("", "uCrsSdf", [select top 1 serie_vd, serie_fact, serie_regcli, serie_EntSNS, serie_Ent from empresa where site=']+Alltrim(mySite)+['])
		IF RECCOUNT("uCrsSdf")>0
		
			** venda a dinheiro **
			myVd = uCrsSdf.serie_vd
			IF uf_startup_sqlExec([select top 1 nmdoc from td where ndoc=]+ALLTRIM(STR(myVd)),[uCrsSdfNmVd])
				IF RECCOUNT("uCrsSdfNmVd")>0
					myVdNm = ALLTRIM(uCrsSdfNmVd.nmdoc)
				ENDIF
				fecha("uCrsSdfNmVd")
			ENDIF
		
			** factura **
			myFact = uCrsSdf.serie_fact
			IF uf_startup_sqlExec([select top 1 nmdoc from td where ndoc=]+ALLTRIM(STR(myFact)),[uCrsSdfNmFact])
				IF RECCOUNT("uCrsSdfNmFact")>0
					myFactNm = ALLTRIM(uCrsSdfNmFact.nmdoc)
				ENDIF
				fecha("uCrsSdfNmFact")
			ENDIF
		
			** Reg. a Cliente **
			myRegCli = uCrsSdf.serie_regcli		
			IF uf_startup_sqlExec([select top 1 nmdoc from td where ndoc=]+ALLTRIM(STR(myRegcli)),[uCrsSdfNmRegcli])
				IF RECCOUNT("uCrsSdfNmRegcli")>0
					myRegcliNm = ALLTRIM(uCrsSdfNmRegcli.nmdoc)
				ENDIF
				fecha("uCrsSdfNmRegcli")
			ENDIF
			
			** Fatura SNS **
			mySEntSNS = uCrsSdf.serie_EntSNS
			IF uf_startup_sqlExec([select top 1 nmdoc from td where ndoc=]+ALLTRIM(STR(mySEntSNS)),[uCrsSdfNmEntSNS])
				IF RECCOUNT("uCrsSdfNmEntSNS")>0
					mySEntSNSNm = ALLTRIM(uCrsSdfNmEntSNS.nmdoc)
				ENDIF
				fecha("uCrsSdfNmEntSNS")
			ENDIF
			
			** Fatura Entidades **
			mySEnt = uCrsSdf.serie_Ent
			IF uf_startup_sqlExec([select top 1 nmdoc from td where ndoc=]+ALLTRIM(STR(mySEnt)),[uCrsSdfNmEnt])
				IF RECCOUNT("uCrsSdfNmEnt")>0
					mySEntNm = ALLTRIM(uCrsSdfNmEnt.nmdoc)
				ENDIF
				fecha("uCrsSdfNmEnt")
			ENDIF
		ENDIF
		
		fecha("uCrsSdf")
	ENDIF
	
	IF EMPTY(myVd)
		myVd = 3
		myVdNm = "Venda a Dinheiro"
	ENDIF
	IF EMPTY(myFact)
		myFact = 1
		myFactNm = "Factura"
	ENDIF
	IF EMPTY(myRegCli)
		myRegCli = 76
		myRegCliNm = "Reg. a Cliente"
	ENDIF
	IF EMPTY(mySEntSNS)
		mySEntSNS = 72
		mySEntSNSNm = "Factura SNS"
	ENDIF
	IF EMPTY(mySEnt)
		mySEnt = 73
		mySEntNm = "Factura Entidades"
	ENDIF
	
	***************************
	** Verifica Modo Offline **
	***************************

	IF uf_gerais_actgrelha("", "uCrsOffLine", [select offline from B_terminal where site=']+Alltrim(mySite)+[' and no=]+ALLTRIM(str(myTermNo)))
		IF RECCOUNT("uCrsOffLine")>0
			myOffline = uCrsOffLine.offline
		ENDIF
		fecha("uCrsSelNdoc")
	ENDIF
	
	************************************
	** Verifica se Usa Cart�o Cliente **
	************************************
	myCartaoClienteLt = uf_gerais_getparameter('ADM0000000052', 'BOOL')

	********************************************
	* M�todo de convers�o de valor para pontos *
	********************************************
	myMetodoConversao = uf_gerais_getparameter('ADM0000000065', 'TEXT')
	
	*****************************************************************
	*  Prazo de validade nos vales de desconto do cart�o de cliente *
	*****************************************************************
	myValidadeVale = uf_gerais_getparameter('ADM0000000017', 'NUM')
	
	********************************************
	*  Vales de desconto s�o intransmiss�veis? *
	********************************************
	myValeIntr =  uf_gerais_getparameter('ADM0000000020', 'BOOL')
	
	****************************************************************
	** Verifica se obriga a Impress�o de Receitas no Front-Office **
	****************************************************************
	myNotForceImpRec = uf_gerais_getparameter('ADM0000000118', 'BOOL')
	
	***************************************
	* Dados para comunica��o dos servi�os *
	***************************************
	myServicosSite = uf_gerais_getParameter_servicos('ADM0000000001', 'TEXT')
	myServicosSiteSuf =  uf_gerais_getparameter_site('ADM0000000020', 'TEXT',mySite)
	IF ALLTRIM(lcTipoEntrada) == 'EMPRESA'
		myServicosUser = uf_gerais_getparameter_site('ADM0000000018', 'TEXT',mySite)
		myServicosPassw = uf_gerais_getparameter_site('ADM0000000019', 'TEXT',mySite)
	ELSE 
		myServicosUser = ''
		myServicosPassw = ''
	ENDIF 

	LOCAL loCrypt, lnSuccess, s, lcStrBase64, lcDecoded
	
	uf_servicos_unblockChilkat()
	
	loCrypt = CreateObject('Chilkat_9_5_0.Crypt2')

	s = ALLTRIM(myServicosPassw)
	loCrypt.CryptAlgorithm = "none"
	loCrypt.EncodingMode = "base64"
	lcStrBase64 = loCrypt.EncryptStringENC(s)
	myServicosPassw = ALLTRIM(lcStrBase64)
	
	RELEASE loCrypt
	
	myServicosEntApp = uf_gerais_getParameter_servicos('ADM0000000002', 'TEXT')
	myServicosEntId = uf_gerais_getParameter_servicos('ADM0000000003', 'TEXT')
	myServicosEntName = uf_gerais_getParameter_servicos('ADM0000000004', 'TEXT')
	
	
	******************************************
	** Configura Pagamentos centralizados   **
	****************************************** 
	myPagCentalDinherio = uf_gerais_getparameter_site('ADM0000000119', 'BOOL',mySite)


	******************************************
	** Obter c�digo alternativo do operador **
	******************************************
	IF uf_gerais_actgrelha("", "uCrsCodAltOp", [select top 1 ISNULL(u_no,0) as no from cm3 where cm=]+ALLTRIM(str(ch_vendedor)))
		IF RECCOUNT("uCrsCodAltOp")>0
			SELECT uCrsCodAltOp
			myOpAltCod = uCrsCodAltOp.no
			fecha("uCrsCodAltOp")
		ELSE
			myOpAltCod = 0
		ENDIF 
	ELSE
		myOpAltCod = 0
	ENDIF

	***********************************
	** Abre Painel central Logitools **
	***********************************
	uf_painelcentral_chama()
	
	IF UPPER(myPaisConfSoftw) == 'ANGOLA'
		uf_painelcentral_timerlic()
	ENDIF 
	
	IF uf_gerais_getParameter('ADM0000000314', 'bool')
		painelcentral.TimerAvisos.Enabled=.t.
		uf_painelcentral_TimerAvisos()
	ENDIF 

	********************
	** Verifica Robot **
	********************
	uf_startup_verifyUsaRobot()
	
	
	********************
	** Verifica TPA	 ***
	********************
	uf_startup_verifyUseTpa()
	
	********************
	** Verifica CashDro	 ***
	********************
	uf_startup_verifyUseCashDro()
	
	
	****************************
	* Utilizador para servi�os *
	****************************
	IF UPPER(ALLTRIM(ucrse1.pais)) == 'PORTUGAL'
		uf_verifica_cria_token_existe()
	ENDIF 
	
	
	IF(VARTYPE(myLogOffComand) <> 'U')
		myLogOffComand = uf_gerais_getParameter('ADM0000000306', 'BOOL')
 	ENDIF
	
	
	&& Corre SP que atualiza PVP dos produtos Automaticamente. Colocado aqui Temporariamente. Lu�s Leal 2015.05
	lcSQL = ''
	TEXT TO lcSQL NOSHOW TEXTMERGE
		exec up_stocks_altpvpdata '<<mysite>>', <<mysite_nr>>
	ENDTEXT
		
	IF !uf_gerais_actgrelha("", "uCrsImpEtiquetas", lcSQL)
		MESSAGEBOX("Ocorreu uma dificuldade a efetuar a atualiza��o autom�tica dos PVPs. Por favor Contacte o Suporte.",48,"LOGITOOLS SOFTWARE")
	ELSE
		** Se existirem altera��es Imprime etiquetas e guarda registo
		IF RECCOUNT("uCrsImpEtiquetas") > 0
		
			lcSQL = ''
			TEXT TO lcSQL Noshow Textmerge
				INSERT into B_ocorrencias
					(stamp, linkstamp, tipo, grau, descr,
					ovalor, dvalor,
					usr, date, site, terminal)
				values
					(LEFT(newid(),25), '', 'Gest�o PVPs', 3, 'Altera��o de PVPs Programada a ' + '<<uf_gerais_getdate(uCrsImpEtiquetas.data,"SQL")>>',
					0, 0,
					<<ch_userno>>, dateadd(HOUR, <<difhoraria>>, getdate()), '<<mySite>>', '<<myTermNo>>')
			ENDTEXT
			uf_gerais_actGrelha("","",lcSql)

			IF !uf_perguntalt_chama("Aten��o: Foram Alterados PVP's de produtos. Deseja imprimir etiquetas para os produtos que alterou o PVP ?"+CHR(13)+"Ir� imprimir etiquetas para os produtos de S/ PVP Fixo.","Sim","N�o")
				RETURN .f.
			ENDIF
		
			uf_etiquetas_chama('PCENTRAL')
		ENDIF
	ENDIF
	**
	
	uf_gerais_chkavisos()
	
	uf_gerais_chkchamadas()
	
	myAvisaChamadas = .t.
	
	&& Corre servi�o SMS para envio anivers�rios e marca��es Colocado aqui Temporariamente. Lu�s Leal 2016.07
	IF !EMPTY(MySMS)
	
		&& valida se sms j� foram enviados no dia de hoje		
		lcSQL = ''
		** alterado a 20190228 para os parametros da empresa
		**TEXT TO lcSQL Noshow Textmerge
		**	SELECT * FROM B_PARAMETERS (nolock) WHERE STAMP = 'ADM0000000143' AND CONVERT(DATE,LDATE) = (SELECT CONVERT(char(10), dateadd(HOUR, <<difhoraria>>, getdate()),126))
		**ENDTEXT
		TEXT TO lcSQL Noshow Textmerge
			EXEC up_sms_getAniversarioAtivo
		ENDTEXT

		IF !uf_gerais_actgrelha("", "uCrsAux", lcSQL)
			uf_perguntalt_chama("Aten��o: N�o foi poss�vel enviar os SMS de Anivers�rio. Por favor contacte o Suporte.","OK","",16)
			RETURN .f.
		ELSE
				IF !uCrsAux.ENVIADO
					uf_param_sms_atualiza()
					
					lcWsPath = ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + "\ltsSMS\LtsSMS.jar scheduler 0 " + ALLTRIM(sql_db)
							
					&& Envia comando Java
					lcWsPath = "javaw -jar " + lcWsPath 	

					&& aguarda que software termine o envio da informa��o 3� PARAMETRO = .T.
					oWSShell = CREATEOBJECT("WScript.Shell")
					oWSShell.Run(lcWsPath, 1, .f.)
				ENDIF 
		ENDIF
		**
	ENDIF
	
ENDFUNC


**
FUNCTION uf_startup_startSystem
	** Colocar em Formato Desenvolvimento **
	IF !EMPTY(m_chnome)
		IF UPPER(ALLTRIM(m_chnome))=="ADMINISTRADOR DE SISTEMA"
			emDesenvolvimento = .T.
		ENDIF
	ENDIF
ENDFUNC


**
FUNCTION uf_startup_existeImpPdfCreator
	LPARAMETERS tcNomeImp
	
	IF EMPTY(tcNomeImp) OR !(TYPE("tcNomeImp") == "C")
		MESSAGEBOX("O PAR�METRO: IMPRESSORA, N�O � V�LIDO.",48,"LOGITOOLS SOFTWARE")
		RETURN .F.
	ENDIF
		
	&& Verifica impressora existe
	DIMENSION lcPrinters[1,1]
	APRINTERS(lcPrinters)


	if(vartype(lcPrinters)=='C')
		FOR lnIndex = 1 TO ALEN(lcPrinters,1)
			if upper(alltrim(lcPrinters(lnIndex,1))) == UPPER(ALLTRIM(tcNomeImp))
				RETURN .T.
			ENDIF
		ENDFOR
	ENDIF
	

	&& N�o encontrou nenhuma impressora
	RETURN .f.
ENDFUNC


**
FUNCTION uf_startup_carregaFuncoes

	** CARREGA FUN��ES DE CLIENTES
	If uf_startup_sqlExec([Exec dbo.up_parameters_Procura 'ADM0000000015'],[uCrsPath])
		If Reccount("uCrsPath")>0
			Select uCrsPath

			IF !EMPTY(ALLTRIM(uCrsPath.textvalue))
				myPath	= ALLTRIM(uCrsPath.textvalue)
			ENDIF 
			
			myStartup			= alltrim(myPath) + '\startup\startup.app'
			myFoxyPreviewer		= alltrim(myPath) + '\FoxyPreviewer\FoxyPreviewer.app'
			myCl				= alltrim(myPath) + '\utentes\utentes.app'
			myStock				= Alltrim(myPath) + '\stocks\stocks.app'
			myProg				= alltrim(myPath) + '\atendimento\atendimento.app'
			myPag				= alltrim(myPath) + '\pagamento\pagamento.app'
			myRobot				= Alltrim(myPath) + '\robot\robot.app'
			myTpa				= Alltrim(myPath) + '\tpa\tpa.app'
			myCashDro			= Alltrim(myPath) + '\cashdro\cashdro.app'
			myServicos 			= Alltrim(myPath) + '\Servicos\servicos.app'
			myPsico 			= alltrim(myPath) + '\psicobenzo\psicobenzo.app'
			myForn	 			= alltrim(myPath) + '\fornecedores\fornecedores.app'
			myDic	 			= Alltrim(myPath) + '\dicionario\dicionario.app'
			myGerais 			= alltrim(myPath) + '\gerais\gerais.app'
			myReservas			= alltrim(myPath) + '\reservas\reservas.app'
			myCampanhaSMS		= alltrim(myPath) + '\sms\sms.app'
			MyPrescApp			= alltrim(myPath) + "\Prescricao_Electronica\prescricao.app"
			MyMarcacoesApp		= alltrim(myPath) + "\Marcacoes\marcacoes.app"
			MyDocumentosApp		= alltrim(myPath) + "\Documentos\documentos.app"
			MyFacturacaoApp		= alltrim(myPath) + "\facturacao\facturacao.app"
			MyLigacoesApp		= alltrim(myPath) + "\Ligacoes\ligacoes.app"
			MyReceituarioApp	= alltrim(myPath) + "\Receituario\receituario.app"
			MyAnalisesApp		= alltrim(myPath) + "\Analises\analises.app"
			MyPromocoesApp		= alltrim(myPath) + "\Promocoes\promocoes.app"
			myArqDigitalApp		= alltrim(myPath) + "\ArquivoDigital\arquivo_digital.app"
			MyEmpresaApp		= ALLTRIM(myPath) + "\FichaEmpresa\fichaempresa.app"
			MyUtilizadoresApp	= ALLTRIM(myPath) + "\utilizadores\utilizadores.app"
			myIntegracoesPHC	= alltrim(myPath) + "\IntegracaoPHC\integracaophc.app"
			MyClinicaApp		= alltrim(myPath) + "\Clinica\clinica.app"
			MyAssociadosApp		= alltrim(myPath) + "\Associados\associados.app"
			MyConvencoesApp		= alltrim(myPath) + "\Convencoes\convencoes.app"
			MyPainelCentralApp	= alltrim(myPath) + "\PainelCentral\painelCentral.app"
			MyCampanhasApp		= alltrim(myPath) + "\Campanhas\campanhas.app"
			MySmsApp			= ALLTRIM(mypath) + "\sms\sms.app"		
	
			
			DO &myGerais
			**DO &myFoxyPreviewer
			**_Screen.oFoxyPreviewer.cPDFSymbolFontsList = "IDAutomationHC39M"
			DO &myCl
			DO &myStock
			DO &myProg
			DO &myPag
			DO &myRobot
			DO &myTpa
			DO &myCashDro
			DO &myServicos
			DO &myPsico
			DO &myForn
			DO &myDic
			DO &myReservas
			DO &myCampanhaSMS
			DO &MyPrescApp
			DO &MyMarcacoesApp
			DO &MyDocumentosApp
			DO &MyFacturacaoApp
			DO &MyLigacoesApp
			DO &MyReceituarioApp
			DO &MyAnalisesApp
			DO &MyPromocoesApp
			DO &myArqDigitalApp
			DO &MyEmpresaApp
			DO &MyUtilizadoresApp
			DO &myIntegracoesPHC
			DO &MyClinicaApp
			DO &MyAssociadosApp
			DO &MyConvencoesApp
			DO &MyCampanhasApp
			DO &MyPainelCentralApp	
			DO &MySmsApp
			
			
				
			FOR EACH mf in _screen.forms
				IF UPPER(ALLTRIM(mf.name))=="MMENUFORM"
					FOR EACH MF2 in mf.objects
						IF like("ToolBar*", mf2.name)
							WITH mf
								mf2.visible = .f.
							ENDWITH
						ENDIF
						IF mf2.name=="Tool_janelas"
							mf.Tool_janelas.visible = .f.
						ENDIF
					ENDFOR
				ENDIF
			ENDFOR
		
*!*				&& CARREGA FICHEIRO DE AJUDA
*!*				IF FILE (ALLTRIM(myPath) + "\Ajuda\logitoolsfarmacia.chm")
*!*					SET HELP ON
*!*					SET HELP TO ALLTRIM(myPath) + "\Ajuda\logitoolsfarmacia.chm"
*!*				ELSE
*!*					SET HELP OFF
*!*				ENDIF
		ELSE
			MESSAGEBOX("ATEN��O, N�O FOI DEFINIDO O CAMINHO DOS RECURSOS ADICIONAIS. ASSIM N�O PODE CONTINUAR!"+CHR(10)+CHR(10)+"POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		EndIf
	ENDIF
ENDFUNC


**  valida se este terminal tem tpa integrado
FUNCTION uf_startup_verifyUseTpa
	
	IF(ucrsE1.usatpa)
		LOCAL lcSQL 
		lcSQL = ''	
		TEXT TO lcSQL NOSHOW TEXTMERGE
			EXEC [dbo].[up_gerais_dadosTpa] <<myTermNo>>, '<<ALLTRIM(mysite)>>'
		ENDTEXT 
		
		
		
		
		IF uf_gerais_actgrelha("", "uCrsTpa", lcSQL)
			IF reccount("uCrsTpa") > 0
			
				PUBLIC 	myTpaStamp;
						,myTpaIp;
						,myTpaPort;
						,myTpaVersion; 
						,myTpaModel; 
						,myTpaSpecVersion 
				
				myTpaStamp  	 = ALLTRIM(uCrsTpa.termstamp)	
				myTpaIp			 = ALLTRIM(uCrsTpa.ip)
				myTpaPort		 = uCrsTpa.port
				myTpaVersion	 = ALLTRIM(uCrsTpa.version)
				myTpaModel		 = ALLTRIM(uCrsTpa.model)
				myTpaSpecVersion = ALLTRIM(uCrsTpa.specification_version)
									
			ENDIF 
			fecha("uCrsTpa")
		ENDIF
		fecha("uCrsTpa")
	ENDIF 	
	
ENDFUNC


**  valida se este terminal tem cashdro integrado
FUNCTION uf_startup_verifyUseCashdro
	
	
	IF(ucrsE1.UsaCashDro==1)
		LOCAL lcSQL 
		lcSQL = ''	
		TEXT TO lcSQL NOSHOW TEXTMERGE
			EXEC [dbo].[up_gerais_dadosCashDro] <<myTermNo>>, '<<ALLTRIM(mysite)>>'
		ENDTEXT 
	
		
		IF uf_gerais_actgrelha("", "uCrsCashDro", lcSQL)
			IF reccount("uCrsCashDro") > 0
			
				PUBLIC 	myCashDroStamp		
				myCashDroStamp   = ALLTRIM(uCrsCashDro.stamp)	
						
			ENDIF 
			fecha("uCrsCashDro")
		ENDIF
		fecha("uCrsCashDro")
	ENDIF 	

	
ENDFUNC



**
FUNCTION uf_startup_verifyUsaRobot
	LOCAL lcValidaRobot

	** Verificar ROB� Tecnilab **
	STORE .f. TO lcValidaRobot
	If uf_startup_sqlExec([exec up_parameters_Procura 'ADM0000000039'],[uCrsUsaRobot]) && verificar se a farm�cia usa Robot
		If Reccount()>0
			If uCrsUsaRobot.bool
				If uf_startup_sqlExec([select drop_location from B_terminal where site=']+Alltrim(mySite)+[' and no=]+astr(myTermNo),[uCrsTermUsaRobot])
					If !(ALLTRIM(uCrsTermUsaRobot.drop_location)=="0") AND !EMPTY(ALLTRIM(uCrsTermUsaRobot.drop_location))
						myUsaRobot = .t.
						lcValidaRobot = .t.
						myDropLocal	= ALLTRIM(uCrsTermUsaRobot.drop_location)
						
						** verficiar c�digo internacional do rob� **
						If uf_startup_sqlExec([Exec dbo.up_parameters_Procura 'ADM0000000161'],[uCrsRoboCodIntern])
							If Reccount("uCrsRoboCodIntern")>0
								Select uCrsRoboCodIntern
								IF !EMPTY(uCrsRoboCodIntern.textvalue)
									myRobotCodInter = ALLTRIM(uCrsRoboCodIntern.textvalue)
								ENDIF
							ENDIF
							fecha("uCrsRoboCodIntern")
						ENDIF
						********************************************
					Endif
					Fecha("uCrsTermUsaRobot")
				Endif
			Endif
		Endif
		Fecha("uCrsUsaRobot")
	Endif
	
	If !lcValidaRobot
		myUsaRobot		= .f.
		myRobotState	= .f.
	ENDIF
	******************************
	
	** Verificar ROB� Farmax ****************
	IF !lcValidaRobot
		If uf_startup_sqlExec([exec up_parameters_Procura 'ADM0000000077'],[uCrsUsaRobot]) && verificar se a farm�cia usa Robot
			If Reccount()>0
				If uCrsUsaRobot.bool
					If uf_startup_sqlExec([select drop_location from B_terminal where site=']+Alltrim(mySite)+[' and no=]+astr(myTermNo),[uCrsTermUsaRobot])
						IF RECCOUNT()>0
							If !(ALLTRIM(uCrsTermUsaRobot.drop_location)=="0") AND !EMPTY(ALLTRIM(uCrsTermUsaRobot.drop_location))
								lcValidaRobot	= .t.
								myDropLocal	= ALLTRIM(uCrsTermUsaRobot.drop_location)
							ENDIF
						ENDIF
						
						fecha("CrsTermUsaRobot")
					ENDIF
				ENDIF
			ENDIF
			
			Fecha("uCrsUsaRobot")
		ENDIF
		
		IF lcValidaRobot
			myUsaFarmax		= .t.
		Else
			myUsaFarmax		= .f.
		ENDIF
	ENDIF
	*****************************************
	
	** Verificar ROB� Apostore **
	IF !lcValidaRobot
		IF uf_startup_sqlExec([exec up_parameters_Procura 'ADM0000000166'],[uCrsUsaRobot]) && verificar se a farm�cia usa Robot
			IF Reccount()>0
				IF uCrsUsaRobot.bool
					IF uf_startup_sqlExec([select drop_location from B_terminal where site=']+Alltrim(mySite)+[' and no=]+astr(myTermNo),[uCrsTermUsaRobot])
						If !(ALLTRIM(uCrsTermUsaRobot.drop_location)=="0") AND !EMPTY(ALLTRIM(uCrsTermUsaRobot.drop_location))
							myUsaRobotApostore = .t.
							lcValidaRobot=.t.
							
							myDropLocal	= ALLTRIM(uCrsTermUsaRobot.drop_location)
						ENDIF
						
						Fecha("uCrsTermUsaRobot")
					ENDIF
				ENDIF
			ENDIF
			
			Fecha("uCrsUsaRobot")
		ENDIF
		
		IF lcValidaRobot
			myUsaRobotApostore	= .t.
			myRobotState		= .t.
		ELSE
			myUsaRobotApostore	= .f.
			myRobotState		= .f.
		ENDIF
	ENDIF
	
	******************************
	** Verificar ROB� CONSIS **
	IF !lcValidaRobot

		IF uf_startup_sqlExec([exec up_parameters_Procura 'ADM0000000221'],[uCrsUsaRobot]) && verificar se a farm�cia usa Robot Consis
			IF Reccount()>0
				IF uCrsUsaRobot.bool
					IF uf_startup_sqlExec([select drop_location from B_terminal where site=']+Alltrim(mySite)+[' and no=]+astr(myTermNo),[uCrsTermUsaRobot])
						If !(ALLTRIM(uCrsTermUsaRobot.drop_location)=="0") AND !EMPTY(ALLTRIM(uCrsTermUsaRobot.drop_location))
							myUsaRobotConsis = .t.
							lcValidaRobot =.t.
							myRobotState = .t.
							myDropLocal	= ALLTRIM(uCrsTermUsaRobot.drop_location)
						ENDIF
						
						Fecha("uCrsTermUsaRobot")
					ENDIF
				ENDIF
			ENDIF
			
			Fecha("uCrsUsaRobot")
		ENDIF
		
	ENDIF
	
ENDFUNC


**
FUNCTION uf_startup_releaseAllPubVars
	LOCAL lcClass
	
*!*		&& fecha Form startup
*!*		FOR EACH mf IN _screen.Forms
*!*			IF UPPER(mf.name) == UPPER("Form")
*!*				mf.release
*!*			ENDIF
*!*		ENDFOR
	
	&& Fecha cursores
	IF USED("uCrsVersaoLt")
		fecha("uCrsVersaoLt")
	ENDIF
	IF USED("uCrsTerm")
		fecha("uCrsTerm")
	ENDIF
	IF USED("uCrsUsaA6")
		fecha("uCrsUsaA6")
	ENDIF
	IF USED("uCrsTermTs")
		fecha("uCrsTermTs")
	ENDIF
	IF USED("uCrsLocaleWin")
		fecha("uCrsLocaleWin")
	ENDIF
	IF USED("uCrsPagCentral")
		fecha("uCrsPagCentral")
	ENDIF
	IF USED("uCrsUadtp")
		fecha("uCrsUadtp")
	ENDIF
	IF USED("uc_validaParaReservas")
		fecha("uc_validaParaReservas")
	ENDIF
	IF USED("uc_validaParaSMS")
		fecha("uc_validaParaSMS")
	ENDIF
	IF USED("uCrsSelNdoc")
		fecha("uCrsSelNdoc")
	ENDIF
	IF USED("uCrsMyCartaoClienteLt")
		fecha("uCrsMyCartaoClienteLt")
	ENDIF
	IF USED("uCrsForceRec")
		fecha("uCrsForceRec")
	ENDIF
	IF USED("uCrsAt")
		fecha("uCrsAt")
	ENDIF
	IF USED("uCrsAt2")
		fecha("uCrsAt2")
	ENDIF
	IF USED("uCrsPath")
		fecha("uCrsPath")
	ENDIF
	IF USED("uCrsUsaRobot")
		fecha("uCrsUsaRobot")
	ENDIF
	IF USED("uCrsRegErroLog")
		fecha("uCrsRegErroLog")
	ENDIF
	IF USED("uCrsPass")
		fecha("uCrsPass")
	ENDIF
	
	IF USED("uCrsUsaInv")
		fecha("uCrsUsaInv")
	ENDIF 	
*!*		
*!*		** libertar programas
*!*		clear program &myCl
*!*		clear program &myStock
*!*		clear program &myProg
*!*		clear program &myPag
*!*		clear program &myRobot
*!*		clear program &myPsico
*!*		clear program &myForn
*!*		clear program &myDic
*!*		clear program &myGerais
*!*		clear program &myReservas
*!*		CLEAR program &myCampanhaSMS
*!*		CLEAR PROGRAM &MyPrescApp
*!*		CLEAR program &MyMarcacoesApp
*!*		CLEAR program &MyDocumentosApp
*!*		CLEAR program &MyFacturacaoApp
*!*		CLEAR program &MyLigacoesApp
*!*		CLEAR PROGRAM &MyReceituarioApp		
*!*		CLEAR PROGRAM &MyAnalisesApp
*!*		CLEAR PROGRAM &MyPromocoesApp
*!*		CLEAR PROGRAM &myArqDigitalApp
*!*		CLEAR PROGRAM &MyEmpresaApp
*!*		CLEAR PROGRAM &MyUtilizadoresApp
*!*		CLEAR PROGRAM &myIntegracoesPHC
*!*		CLEAR PROGRAM &MyClinicaApp
*!*		CLEAR PROGRAM &MyAssociadosApp
*!*		CLEAR PROGRAM &MyConvencoesApp	
*!*		CLEAR PROGRAM &MyPainelCentralApp
*!*		
*!*		**libertar variaveis publicas	
*!*		RELEASE myInventario
*!*		release myTerm
*!*		release myTermNo
*!*		release mySite
*!*		RELEASE myPosto
*!*		RELEASE mySitePosto
*!*		release myArmazem
*!*		release myPad
*!*		release myPosPrinter
*!*		release myPagCentral
*!*		release myOffline
*!*		release myArquivoDigital
*!*		release myArquivoDigitalPdf
*!*		release myNrVendasTalao
*!*		release myNotForceImpRec
*!*		release myReservasCl
*!*		release mySMS
*!*		RELEASE myCampanhaSMS
*!*		release myUsaRobot
*!*		release myUsaFarmax
*!*		release myRobotState
*!*		release myDropLocal
*!*		release myStartup
*!*		release myCl
*!*		release myStock
*!*		release myProg
*!*		release myPag
*!*		release myRobot
*!*		release myPsico
*!*		release myForn
*!*		release myGerais
*!*		release myDic
*!*		release myBackOpPass
*!*		release myVirtualVar
*!*		release myVirtualText
*!*		release myVersaoLt
*!*		release myPath
*!*		release myVersaoDoc
*!*		release myCartaoClienteLt
*!*		RELEASE myMetodoConversao
*!*		RELEASE myValidadeVale
*!*		RELEASE myValeIntr
*!*		RELEASE mystart
*!*		RELEASE MyDocumentosApp
*!*		RELEASE MyAnalisesApp
*!*		RELEASE MyPromocoesApp
*!*		RELEASE MyEmpresaApp
*!*		RELEASE MyUtilizadoresApp
*!*		RELEASE MyFacturacaoApp
*!*		RELEASE MyLigacoesApp
*!*		RELEASE myOpAltCod
*!*		RELEASE MyReceituarioApp
*!*		RELEASE myArqDigitalApp
*!*		RELEASE myIntegracoesPHC
*!*		RELEASE MyClinicaApp
*!*		RELEASE MyAssociadosApp
*!*		RELEASE MyConvencoesApp
*!*		RELEASE MyCampanhasApp
*!*		RELEASE MyPainelCentralApp
	
	RELEASE myDocIntroducao
	RELEASE myDocAlteracao
	RELEASE myFtIntroducao
	RELEASE myFtAlteracao
	RELEASE myStocksAlteracao
	RELEASE myStocksIntroducao
	RELEASE myFlIntroducao
	RELEASE myFlAlteracao
	RELEASE myClIntroducao
	RELEASE myClAlteracao
	RELEASE myEmpIntroducao
	RELEASE myEmpAlteracao
	RELEASE myUtIntroducao
	RELEASE myUtAlteracao
	RELEASE myUGIntroducao
	RELEASE myUGAlteracao
	RELEASE myPagFornIntroducao
	RELEASE myPagFornAlteracao
	
	** libertar programa e variavel startup
*!*		CLEAR PROGRAM &myStartup
*!*		RELEASE myStartup
ENDFUNC


**
FUNCTION uf_startup_sqlExec
	LPARAMETERS lcSQL, lcCursor, lcHideInfoError

	IF TYPE("lcCursor") != "C"
		lcCursor = ""
	ENDIF 

	TRY
		IF !EMPTY(lcCursor)
			TABLEUPDATE(2,.t.,lcCursor)
		ELSE
			TABLEUPDATE()
		ENDIF
	CATCH
		**
	FINALLY 
		**
	ENDTRY

	uv_erro = .F.
	 
	IF SQLEXEC(gnConnHandle, lcSQL, lcCursor, lcArrayResult) < 0

		IF USED("ucrsempresas")
		
			SELECT ucrsempresas

			lcconstr = "DSN="+ALLTRIM(ucrsempresas.odbc)+";UID=logiserv;PWD=ls2k12...;DATABASE="+ALLTRIM(ucrsempresas.nomebd)+";App=LT - "+myusername+" - "+myclientname

			IF uf_login_connectdb(lcconstr)
				PUBLIC sql_db
				sql_db = ALLTRIM(ucrsempresas.nomebd)

				IF SQLEXEC(gnConnHandle, lcSQL, lcCursor, lcArrayResult) < 0
					uv_erro = .T.
				ELSE
					RETURN .T.
				ENDIF

			ELSE
				uv_erro = .T.
			ENDIF


		ENDIF

	ELSE	
		RETURN .t.
	ENDIF 

	IF uv_erro

		IF !EMPTY(lcHideInfoError)
			RETURN .f.
		ENDIF 
				
		LOCAL lcErrMsg
		STORE '' TO lcErrMsg
		aerror(laErr) && Data from most recent error
		for each lxErr in laErr
			lcErrMsg = lcErrMsg + chr(13) + transform(lxErr, "")
		ENDFOR
		
		LOCAL lcMensagem
		lcMensagem = 'ERRO ATUALIZAR GRELHA - SQL'
		lcMensagem = lcMensagem  + CHR(13) + 'Cliente: ' + ALLTRIM(ucrse1.nomecomp)
		lcMensagem = lcMensagem  + CHR(13) + 'Loja: ' + ALLTRIM(ucrse1.siteloja)
		lcMensagem = lcMensagem  + CHR(13) + 'Msg. Erro: : ' + ALLTRIM(lcErrMsg)
		lcMensagem = lcMensagem  + CHR(13) + 'SQL: ' + ALLTRIM(lcSQL)
		
		LOCAL lcSite
		lcSite = ""
		
		
		if(USED("ucrse1"))
			SELECT ucrse1
			lcSite = ALLTRIM(ucrse1.id_lt)
		ENDIF
		
		uf_startup_sendmail_errors('Erro no Logitools: ' + UPPER(ALLTRIM(lcSite))  , lcMensagem )
		**uf_startup_envialMailErroSQL(LEFT(lcErrMsg,254), LEFT(lcSQL,254))
		
		RETURN .f.
	
	ENDIF

ENDFUNC


**
FUNCTION uf_startup_envialMailErroSQL
	LPARAMETERS lcErrMsg, lcSQL
	
	IF emDesenvolvimento == .t.
		uf_aptec_chama()
		aptec.edit2.value = ''
		aptec.edit2.value = 'SQL:' + CHR(13) + lcSQL
		aptec.edit2.value = aptec.edit2.value + CHR(13) + CHR(13) + 'ERRO:' + CHR(13) + lcErrMsg
	ELSE
		LOCAL lcBody, lcFooter, lcSubject, lcTo, lcAttach, lcPath, lcAppprtScreen
		lcBody = ''
		lcFooter = ''
		lcSubject = ''
		lcTo = ''
		lcAttach = ''
		lcPath = ''
		lcAppprtScreen = ''
		
		lcBody = lcBody + " <br> SQL: <br> " + lcSQL
		lcBody = lcBody + " <br> ERRO: <br> " + lcErrMsg

		TRY 
			lcFooter = ALLTRIM(uf_gerais_getParameter("ADM0000000157","TEXT")) + " - " + astr(myTerm)
		CATCH
			lcFooter = ''
		ENDTRY
		
		lcSubject = 'E FARM:' + ALLTRIM(uf_gerais_getParameter("ADM0000000156","TEXT")) + ' CL:' + ALLTRIM(uf_gerais_getParameter("ADM0000000157","TEXT"))
		
		lcTo = IIF(!EMPTY(alltrim(uf_gerais_getParameter("ADM0000000210","TEXT"))),alltrim(uf_gerais_getParameter("ADM0000000210","TEXT")),'suporte@logitools.pt')
	
		&& printscreen
		lcPath = ALLTRIM(uf_gerais_getParameter("ADM0000000015","TEXT")) + "\imagens\anomalia.png"
		lcAppprtScreen = ALLTRIM(uf_gerais_getParameter("ADM0000000015","TEXT")) + "\libs\prtscreen.app"
		DO &lcAppprtScreen
		LOCAL loBmp AS xfcBitmap

		&& Erro nos novos servidores - comentado porque as imagens n�o s�o essenciais 
*!*			WITH _SCREEN.SYSTEM.Drawing
*!*			  loBmp = .BITMAP.FromScreen(_SCREEN)
*!*			  loBmp.SAVE(lcPath, .Imaging.ImageFormat.Png)
*!*			ENDWITH
		************************

		lcAttach = '"' + lcPath + '"'

		IF DIRECTORY(ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\SendMail')
			lcWsPath = ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\SendMail\SendMail.jar "' + ALLTRIM(lcBody) + '" "' + ALLTRIM(lcFooter) + '" "' + ALLTRIM(lcSubject) + '" "' + ALLTRIM(lcTo) + '" ' + ALLTRIM(lcAttach)
		
			lcWsPath = "javaw -jar " + lcWsPath 
			oWSShell = CREATEOBJECT("WScript.Shell")
			oWSShell.Run(lcWsPath,1,.f.)
		ELSE
			uf_perguntalt_chama("N�o foi poss�vel enviar o email. Por favor contacte o suporte.","OK","",16)
		ENDIF
	ENDIF
ENDFUNC 


********************************************************************************
* Func��o que verifica se os apps estao disponiveis e recupera em caso de erro *
********************************************************************************
FUNCTION uf_startup_recoverApps
	RETURN .t.
ENDFUNC

***********************************************************************************************
* Fun��o para validar licen�as dispon�veis para entrar no software utilizando o nosso servi�o *
***********************************************************************************************
FUNCTION uf_startup_validalicenca
	local lcSessionID, lcSessionTime ,loCrypt, lcTxt1, loSbEncrypted, lcDecryptedText, lcRetString, lcSessionUser , lcSessionUserPWD, lcMasterKey
	lcSessionID = ALLTRIM(GETENV("SESSIONNAME")) + '-' + ALLTRIM(GETENV("ClientName")) + '-' + ALLTRIM(GETENV("COMPUTERNAME"))
	lcSessionTime = ALLTRIM(ttoc(datetime()))
	lcSessionUser = ALLTRIM(m_chinis)
	

	
	TEXT TO lcSQL Noshow Textmerge
		select password from b_us(nolock) where iniciais='<<ALLTRIM(m_chinis)>>'
	ENDTEXT
	IF !uf_gerais_actGrelha("","ucrspwdusr",lcSql)			
		uf_perguntalt_chama("Aten��o: Ocorreu uma anomalia pesquisar a password do utilizador. Por favor contacte o Suporte.","OK","",16)
		RETURN .f.
	ENDIF
	SELECT ucrspwdusr
	lcSessionUserPWD = uf_gerais_decrypt(ALLTRIM(ucrspwdusr.password))
	fecha("ucrspwdusr")
	
	loCrypt = CreateObject('Chilkat_9_5_0.Crypt2')
	lnSuccess = loCrypt .UnlockComponent("LTSLDA.CB1052020_PiQd27iz7QkK")
		IF (lnSuccess <> 1) THEN
		  	? loMailman.LastErrorText
		    RELEASE loMailman
		   CANCEL
		ENDIF

	loCrypt.CryptAlgorithm = "aes"
	loCrypt.CipherMode = "cbc"
	loCrypt.KeyLength = 256
	loCrypt.PaddingScheme = 0

	loCrypt.SetEncodedKey("L0g1T00lsFarm@L0g1Lts2k16..LTSSess1onUn1q3ID","hex")
	loCrypt.SetEncodedIV("L0g1T00lsFarm@L0g1Lts2k16","hex")
	loCrypt.EncodingMode = "hex"

	lcTxt1 = uf_gerais_randomString(1,5)+ALLTRIM(lcSessionID )+'||'+lcSessionTime+'||'+lcSessionUser+'||'+lcSessionUserPWD+'||'+alltrim(ucrse1.id_lt)+uf_gerais_randomString(1,3)
	loSbEncrypted = CreateObject('Chilkat_9_5_0.StringBuilder')
	loSbEncrypted.Append(loCrypt.EncryptStringENC(lcTxt1))
	lcRetString=loSbEncrypted.GetAsString()
	RELEASE loCrypt
	RELEASE loSbEncrypted
	
	**MESSAGEBOX(lcRetString)
	LOCAL lcSendMsg, lcPedStamp 
	lcPedStamp = uf_gerais_stamp()
	
	** Envio de mensagem para obter valida��o de entrada
	lcSendMsg = ''
	lcSendMsg = lcSendMsg + '{'
	lcSendMsg = lcSendMsg + '"token":"'+ALLTRIM(lcPedStamp)+'",'
	lcSendMsg = lcSendMsg + '"entity": {'
	lcSendMsg = lcSendMsg + '"id":"lts",'
	lcSendMsg = lcSendMsg + '"app":"Logitools",'
	lcSendMsg = lcSendMsg + '"name":"Logitools app"'
	lcSendMsg = lcSendMsg + '},'
	lcSendMsg = lcSendMsg + '"sender": {'
	lcSendMsg = lcSendMsg + '"id":"'+ALLTRIM(ucrse1.id_lt)+'",'
	lcSendMsg = lcSendMsg + '"name":"'+ALLTRIM(ucrse1.nomabrv)+'",'
	lcSendMsg = lcSendMsg + '"vatNr":"'+ALLTRIM(ucrse1.ncont)+'"'
	lcSendMsg = lcSendMsg + '},'
	lcSendMsg = lcSendMsg + '"receiver": {'
	lcSendMsg = lcSendMsg + '"id":"auth"'
	lcSendMsg = lcSendMsg + '},'
	lcSendMsg = lcSendMsg + '"message": {'
	lcSendMsg = lcSendMsg + '"info":"'+ALLTRIM(lcRetString)+'"'
	lcSendMsg = lcSendMsg + '}'
	lcSendMsg = lcSendMsg + '}'
	
	LOCAL loReq, loHttp, lnSuccess1, lcJsonText,	loResp
	
	uf_servicos_unblockChilkat()
	
	loReq = CreateObject('Chilkat_9_5_0.HttpRequest')
	loHttp = CreateObject('Chilkat_9_5_0.Http')

	loJson = CreateObject('Chilkat_9_5_0.JsonObject')

	loHttp.AcceptCharset = ""
	loHttp.UserAgent = ""
	loHttp.AcceptLanguage = ""

	loHttp.AllowGzip = 0
	
	loHttp.ConnectTimeout  = 20
  	loHttp.ReadTimeout = 20

	lnSuccess = loHttp.AddQuickHeader("Content-Type","application/json")
  	
  	lcPostHTTP='https://app.lts.pt:50001/api/auth/signInSecure'
  	loResp = loHttp.PostJson(lcPostHTTP,lcSendMsg )
  	
  	IF (loHttp.LastMethodSuccess <> 1) THEN
		uf_perguntalt_chama("OCORREU UM ERRO A TENTAR OBTER A LICEN�A PARA UTILIZAR O SOFTWARE! POR FAVOR CONTACTE O SUPORTE!","OK","", 16)
		IF uf_startup_validlicoffline()
			RETURN .t.
		ELSE
			RETURN .f.
		ENDIF 
	ELSE
	
		LOCAL lcReturnI, lcReturnS, lcNmvsTrxId, lcState, lcReasons, lcCode, lcDescription, lcReturnCode, lcReturnStk 
		STORE '' to lcReturnI, lcReturnMsg
		STORE 0 TO lcReturnStk 
		
		RELEASE loReq
		RELEASE loHttp
		loJson.Load(loResp.BodyStr)
		loJson.EmitCompact = 0
		IF !(ISNULL(loJson.ObjectOf("status")))
			lcRev = loJson.ObjectOf("status")
			lcReturnI = (lcRev.StringOf("i"))
			IF ALLTRIM(lcReturnI)='200'
				loJson.Load(loResp.BodyStr)
				loJson.EmitCompact = 0

				IF !(VARTYPE(loJson.ObjectOf("message"))=='U')
					lcRevMsg = loJson.ObjectOf("message")
					lcReturnMsg = (lcRevMsg.StringOf("info"))
					
					** Se est� ok desencripta
					LOCAL loCrypt, lcTxt1, loSbEncrypted, lcDecryptedText, lcResp1, lcResp2

					loCrypt = CreateObject('Chilkat_9_5_0.Crypt2')

					lnSuccess = loCrypt .UnlockComponent("LTSLDA.CB1052020_PiQd27iz7QkK")
					IF (lnSuccess <> 1) THEN
					  	? loMailman.LastErrorText
					    RELEASE loMailman
					   CANCEL
					ENDIF

					loCrypt.CryptAlgorithm = "aes"
					loCrypt.CipherMode = "cbc"
					loCrypt.KeyLength = 256
					loCrypt.PaddingScheme = 0

					loCrypt.SetEncodedKey("L0g1T00lsFarm@L0g1Lts2k16..LTSSess1onUn1q3ID","hex")
					loCrypt.SetEncodedIV("L0g1T00lsFarm@L0g1Lts2k16","hex")
					loCrypt.EncodingMode = "hex"

					lcDecryptedText = loCrypt.DecryptStringENC(lcReturnMsg)

					lcRetString = lcDecryptedText

					RELEASE loCrypt
					RELEASE loSbEncrypted
					
					lcResp1 = SUBSTR(lcRetString , AT('||',lcRetString)-3,3)
					lcResp2 = SUBSTR(lcRetString , AT('||',lcRetString)+2,8)

					IF lcResp1 == 'Sim' AND ALLTRIM(lcResp2) == left(ALLTRIM(ttoc(date()+1)),8)
						** Valida��o OK
						lcMasterKeySTR = uf_gerais_randomString(1,5) + ALLTRIM(ttoc(datetime()))+'|'+'01'+uf_gerais_randomString(1,4)
						lcMasterKey  = uf_gerais_encrypt(lcMasterKeySTR)

						TEXT TO lcSql NOSHOW TEXTMERGE
							update empresa set masterkey='<<ALLTRIM(lcMasterKey)>>' WHERE site = '<<ALLTRIM(mysite)>>'
						ENDTEXT 
						IF !uf_gerais_actgrelha("", "", lcSql)
							uf_perguntalt_chama("N�o foi possivel actualizar a masterkey associada � Empresa. Contacte o suporte.", "OK", "", 16)
							RETURN .f.
						ENDIF 
						RETURN .t. 
					ELSE
						uf_perguntalt_chama("Lamentamos mas esgotou as licen�as dispon�veis para esta farm�cia!", "OK", "", 48)
						return(.f.)
					ENDIF 
				ELSE
					
					IF uf_startup_validlicoffline()
						RETURN .t.
					ELSE
						RETURN .f.
					ENDIF 
					
				ENDIF
			ELSE
				IF uf_startup_validlicoffline()
					RETURN .t.
				ELSE
					RETURN .f.
				ENDIF 
			ENDIF 
		ELSE
			IF uf_startup_validlicoffline()
				RETURN .t.
			ELSE
				RETURN .f.
			ENDIF 
		ENDIF 
	ENDIF 
ENDFUNC 
	
FUNCTION uf_startup_validlicoffline
	
	LOCAL LcValString, lcnrvezes, lcVezesTotal
	
	lcVezesTotal = 40
	
	validlicoffline = .t.
	
	TEXT TO lcSql NOSHOW TEXTMERGE
		select masterkey from empresa WHERE site = '<<ALLTRIM(mysite)>>'
	ENDTEXT 
	IF !uf_gerais_actgrelha("", "uCrsMK", lcSql)
		uf_perguntalt_chama("N�o foi possivel actualizar a masterkey associada � Empresa. Contacte o suporte.", "OK", "", 16)
		RETURN .f.
	ENDIF 
	LcValString = uCrsMK.masterkey
	LcValString  = uf_gerais_decrypt(LcValString )

	lcnrvezes=VAL(substr(LcValString  , rat('|', LcValString  )+1,2))
	
	
	LOCAL lcNrVezesEmFalta
	lcNrVezesEmFalta = 0
	
	if(EMPTY(lcnrvezes))
		lcnrvezes = 0	
	ENDIF
	

	lcNrVezesEmFalta  = lcVezesTotal  -  lcnrvezes
	if(lcNrVezesEmFalta<=0)
		lcNrVezesEmFalta = 0
	ENDIF
	
	
	uf_perguntalt_chama("Neste momento tem " +ALLTRIM(STR(lcNrVezesEmFalta))+  " tentativa(s)!","OK","", 16)
	PUBLIC myChaveSuporte
	myChaveSuporte = ''

		
	IF lcnrvezes < lcVezesTotal 
		lcnrvezes = lcnrvezes + 1
		lcMasterKeySTR = uf_gerais_randomString(1,5) + ALLTRIM(ttoc(datetime()))+'|'+RIGHT('0'+ALLTRIM(STR(lcnrvezes)),2)+uf_gerais_randomString(1,4)
		lcMasterKey  = uf_gerais_encrypt(lcMasterKeySTR)
		
		TEXT TO lcSql NOSHOW TEXTMERGE
			update empresa set masterkey='<<ALLTRIM(lcMasterKey)>>' WHERE site = '<<ALLTRIM(mysite)>>'
		ENDTEXT 
		IF !uf_gerais_actgrelha("", "", lcSql)
			uf_perguntalt_chama("N�o foi possivel actualizar a masterkey associada � Empresa. Contacte o suporte.", "OK", "", 16)
			RETURN .f.
		ENDIF 
		return(.t.)
	ELSE
		
		uf_perguntalt_chama("ATEN��O! ESGOTOU O N�MERO DE ACESSOS OFFLINE. POR FAVOR CONTACTE O SUPORTE", "OK", "", 48)
		
		uf_tecladoalpha_chama("myChaveSuporte ", "Introduza a chave providenciada pelo suporte:", .F., .F., 0)
		uf_startup_validaKeyCentral(myChaveSuporte)		
		release myChaveSuporte
       	QUIT

	ENDIF 
ENDFUNC 


FUNCTION uf_startup_geraKeyValida()

	if(!EMPTY(emdesenvolvimento))
		LOCAL lnnrvezes, lcMasterKeySTR, lcMasterKey   
		lcMasterKeySTR = ALLTRIM(ttoc(date()))
		lcMasterKeySTR = SUBSTR(lcMasterKeySTR,1,8)
		lcMasterKey  = uf_gerais_encrypt(lcMasterKeySTR)
		_cliptext = lcMasterKey  
		uf_perguntalt_chama("Key Gerada e guardada no control + c", "OK", "", 16)
		
	ENDIF
	
	
ENDFUNC


	
FUNCTION uf_startup_validaKeyCentral
	LPARAMETERS lcChaveSuporte
	
	
	lcChaveSuporte = uf_gerais_decrypt(lcChaveSuporte)
	
	LOCAL lcnrvezes, lcMasterKeySTR, lcMasterKey, lcDate
	LOCAL lnnrvezes
	lnnrvezes = 1
	
	lcDate= ALLTRIM(ttoc(date()))
	lcDate= SUBSTR(lcDate,1,8)
	

	if(ALLTRIM(lcChaveSuporte)!=ALLTRIM(lcDate))
		uf_perguntalt_chama("Chave inv�lida ou expirada. Contacte o suporte.", "OK", "", 16)
		RETURN .f.
	ENDIF
	
	
	lcMasterKeySTR = uf_gerais_randomString(1,5) + ALLTRIM(ttoc(datetime()))+'|'+RIGHT('0'+ALLTRIM(STR(lnnrvezes)),2)+uf_gerais_randomString(1,4)
	lcMasterKey  = uf_gerais_encrypt(lcMasterKeySTR)
	
	TEXT TO lcSql NOSHOW TEXTMERGE
		update empresa set masterkey='<<ALLTRIM(lcMasterKey)>>' WHERE site = '<<ALLTRIM(mysite)>>'
	ENDTEXT 
	IF !uf_gerais_actgrelha("", "", lcSql)
		uf_perguntalt_chama("N�o foi possivel actualizar a masterkey associada � Empresa. Contacte o suporte.", "OK", "", 16)
		RETURN .f.
	ELSE
		uf_perguntalt_chama("Chave inserida com sucesso. Por favor volte a iniciar o Logitools.", "OK", "", 16)	
	ENDIF 
	
	

	return(.t.)
ENDFUNC 
	
	
FUNCTION uf_painelcentral_timerlic
	 painelcentral.timerlic.interval=18000000
	 painelcentral.timerlic.enabled=.t.
ENDFUNC 

FUNCTION uf_painelcentral_TimerAvisos
	painelcentral.timeravisos.interval= (uf_gerais_getParameter('ADM0000000314', 'NUM')*60)*1000
	painelcentral.timeravisos.enabled=.t.
ENDFUNC 


FUNCTION uf_startup_createTpaMessage
	IF !USED("uCrsTPAMessage")
		create cursor uCrsTPAMessage(type c(254), code c(254),message c(254))
	ENDIF
	
	TEXT TO lcSQL NOSHOW TEXTMERGE
	select 
		type,
		code,
		message  
	from 
		TPA_Message(nolock)
	ENDTEXT  
	uf_gerais_actgrelha("", "uCrsTPAMessage", lcSQL)
ENDFUNC


FUNCTION uf_startup_passwprimacesso
	PARAMETERS LcPrimeira, lcChinis, uv_updatPW
	
	IF LcPrimeira=1
		uf_perguntalt_chama("Caro utilizador. "+Chr(13)+Chr(10)+"Bem-vindo ao software Logitools"+Chr(13)+Chr(10)+"Verificamos que � a primeira vez que entra no software."+Chr(13)+Chr(10)+"De seguida vai ter de inserir a password que vai utilizar para entrar no sistema.","OK","",64)
	ENDIF 

    IF uv_updatPW

        PUBLIC upv_curPW
        upv_curPW = ''

        uf_tecladoalpha_Chama('upv_curPW', 'INTRODUZA A SUA PASSWORD ATUAL:', .t., .f., 0)

        uv_curPW = upv_curPW

        RELEASE upv_curPW

        uv_curPW = uf_gerais_encrypt(uv_curPW)

    	TEXT TO lcSQL TEXTMERGE NOSHOW 
			select password FROM b_us (nolock) WHERE password = '<<ALLTRIM(uv_curPW)>>' AND iniciais = '<<ALLTRIM(lcChinis)>>'
		ENDTEXT 

		uf_gerais_actGrelha("","uCrsTempPw",lcSQL)
		IF RECCOUNT("uCrsTempPw") = 0
			uf_perguntalt_chama("Password errada." + chr(13) + " Por favor repita o processo.", "OK", "", 46)
			fecha("uCrsTempPw")
			RETURN 0
		ENDIF

    ENDIF
	
	PUBLIC myNewPass, myNewPassVal
	STORE '' TO myNewPass, myNewPassVal
	uf_tecladoalpha_Chama('myNewPass', 'INTRODUZA A SUA PASSWORD:', .t., .f., 0)
	IF ALLTRIM(myNewPass)==''
		uf_perguntalt_chama("A password n�o pode ser vazia. Por favor repita o processo", "OK", "", 48)
		RETURN 0
	ENDIF 
	
	uf_tecladoalpha_Chama('myNewPassVal', 'CONFIRME A SUA PASSWORD:', .t., .f., 0)
	IF ALLTRIM(myNewPassVal)==''
		uf_perguntalt_chama("A password n�o pode ser vazia. Por favor repita o processo", "OK", "", 48)
		RETURN 0
	ENDIF 
	
	IF ALLTRIM(myNewPass)<>ALLTRIM(myNewPassVal)
		uf_perguntalt_chama("As 2 passwords n�o s�o iguais. Por favor repita o processo", "OK", "", 48)
		RETURN 0
	ELSE


        uv_numMin = uf_gerais_getParameter("ADM0000000334", "NUM")
        uv_letras = uf_gerais_getParameter("ADM0000000335", "BOOL")
        uv_nums = uf_gerais_getParameter("ADM0000000336", "BOOL")
        uv_caractEsp = uf_gerais_getParameter("ADM0000000337", "BOOL")

        uv_msgValidPW = "A password tem de cumprir as seguintes regras:"
        uv_regras = ""

        IF uv_numMin <> 0
            uv_regras = chr(9) + "M�nimo de " + ASTR(uv_numMin) + " caracteres"
        ENDIF

        IF uv_letras
            uv_regras = uv_regras + IIF(!EMPTY(uv_regras), ";" + chr(13), "") + chr(9) + "Tem de conter letras"
        ENDIF

        IF uv_nums
            uv_regras = uv_regras + IIF(!EMPTY(uv_regras), ";" + chr(13), "") + chr(9) + "Tem de conter n�meros"
        ENDIF

        IF uv_caractEsp
            uv_regras = uv_regras + IIF(!EMPTY(uv_regras), ";" + chr(13), "") + chr(9) + "Tem de conter caracteres especiais"
        ENDIF

        uv_msgValidPW = uv_msgValidPW + chr(13) + uv_regras + '.'


        IF uv_numMin <> 0

            IF LEN(ALLTRIM(myNewPass)) < uv_numMin
                uf_perguntalt_chama(uv_msgValidPW, "OK", "", 48)
                RETURN 0
            ENDIF

        ENDIF

        IF uv_letras
                        
            IF !uf_gerais_validaPattern("[a-zA-Z]", myNewPass)
            
                uf_perguntalt_chama(uv_msgValidPW, "OK", "", 48)
                RETURN 0		

            ENDIF

        ENDIF

        IF uv_nums
            
            IF !uf_gerais_validaPattern("[0-9]", myNewPass)
            
                uf_perguntalt_chama(uv_msgValidPW, "OK", "", 48)
                RETURN 0		

            ENDIF

        ENDIF

        IF uv_caractEsp
            
            IF !uf_gerais_validaPattern("[^a-z0-9]", myNewPass)
            
                uf_perguntalt_chama(uv_msgValidPW, "OK", "", 48)
                RETURN 0		

            ENDIF

        ENDIF



		LOCAL lcencryptedpassw 
		lcencryptedpassw = uf_gerais_encrypt(myNewPass)
		
		lcSQL  = ''
		**select uCrsUs
		IF TYPE("UTILIZADORES") == "U"
			TEXT TO lcSQL TEXTMERGE NOSHOW 
				select password FROM b_us (nolock) WHERE password = '<<ALLTRIM(lcencryptedpassw)>>' AND iniciais <> '<<ALLTRIM(lcChinis)>>'
			ENDTEXT 
		ELSE 
			TEXT TO lcSQL TEXTMERGE NOSHOW 
				select password FROM b_us (nolock) WHERE password = '<<ALLTRIM(lcencryptedpassw)>>' AND iniciais <> '<<ALLTRIM(ucrsUs.iniciais)>>'
			ENDTEXT 
		ENDIF 
		uf_gerais_actGrelha("","uCrsTempPw",lcSQL)
		IF RECCOUNT("uCrsTempPw") > 0
			uf_perguntalt_chama("J� existe um utilizador com as mesmas credenciais. Por favor verifique.", "OK", "", 46)
			fecha("uCrsTempPw")
			RETURN 0
		ENDIF

		fecha("uCrsTempPw")
		
		lcSQL  = ''
		**select uCrsUs
		IF TYPE("UTILIZADORES") == "U"
			TEXT TO lcSQL TEXTMERGE NOSHOW 
				select password FROM b_us (nolock) WHERE password = '<<ALLTRIM(lcencryptedpassw)>>' AND iniciais = '<<ALLTRIM(lcChinis)>>'
			ENDTEXT 
		ELSE 
			TEXT TO lcSQL TEXTMERGE NOSHOW 
				select password FROM b_us (nolock) WHERE password = '<<ALLTRIM(lcencryptedpassw)>>' AND iniciais = '<<ALLTRIM(ucrsUs.iniciais)>>'
			ENDTEXT 		
		ENDIF 
		uf_gerais_actGrelha("","uCrsTempPw",lcSQL)
		IF RECCOUNT("uCrsTempPw") > 0
			uf_perguntalt_chama("N�o pode utilizar as mesmas credenciais. Por favor verifique.", "OK", "", 46)
			fecha("uCrsTempPw")
			RETURN 0
		ENDIF

		fecha("uCrsTempPw")

			
		uv_sql = ""

		uv_iniciais = IIF(TYPE("UTILIZADORES") == "U", ALLTRIM(lcChinis), ALLTRIM(ucrsUs.iniciais))

		TEXT TO uv_sql NOSHOW TEXTMERGE 
			Update 
				b_us
			SET 
				password = '<<ALLTRIM(lcencryptedpassw)>>'
				,primacesso = 0
				,data_alt_pw = CONVERT(varchar, GETDATE(), 112)
			Where 
				iniciais='<<ALLTRIM(uv_iniciais)>>'
			
		ENDTEXT 	

		IF uf_gerais_getParameter_site("ADM0000000185", "BOOL", mySite)

			IF !uf_gerais_runSQLCentral("select * from b_us(nolock) where iniciais = '" + ALLTRIM(uv_iniciais) + "'", "uc_userCentral")
				uf_perguntalt_chama("N�o foi possivel verificar o utilizador na Central", "OK", "", 16)
				RETURN 2
			ENDIF

			IF !USED("uc_userCentral") OR RECCOUNT("uc_userCentral") = 0
				uf_perguntalt_chama("N�o foi possivel verificar o utilizador na Central", "OK", "", 16)
				RETURN 2
			ENDIF

			IF !uf_gerais_actGrelha("", "uc_tmpUs", "exec up_utilizadores_dadosAtualizarCentral '" + ALLTRIM(uv_iniciais) + "'")
				uf_perguntalt_chama("N�o foi possivel obter os dados do utilizador! Por favor contacte o suporte.", "OK", "", 16)
				RETURN 2
			ENDIF

			uv_update = ""

			FOR i=1 TO AFIELDS(ua_fields)

				DO CASE
					CASE UPPER(ALLTRIM(ua_fields[i,1])) == "LOJA"
					CASE UPPER(ALLTRIM(ua_fields[i,1])) == "PASSWORD"
						uv_update = uv_update + IIF(EMPTY(uv_update), "", CHR(13) + ", ") + ALLTRIM(ua_fields[i,1]) + " = '" + ALLTRIM(lcencryptedpassw) + "'"

					CASE UPPER(ALLTRIM(ua_fields[i,1])) == "PRIMACESSO"
						uv_update = uv_update + IIF(EMPTY(uv_update), "", CHR(13) + ", ") + ALLTRIM(ua_fields[i,1]) + " = 0"

					CASE UPPER(ALLTRIM(ua_fields[i,1])) == "DATA_ALT_PW"
						uv_update = uv_update + IIF(EMPTY(uv_update), "", CHR(13) + ", ") + ALLTRIM(ua_fields[i,1]) + " = CONVERT(varchar, GETDATE(), 112)"

					OTHERWISE

						DO CASE 
							CASE ALLTRIM(ua_fields[i,2]) = "C"
								uv_update = uv_update + IIF(EMPTY(uv_update), "", CHR(13) + ", ") + ALLTRIM(ua_fields[i,1]) + " = '" + ALLTRIM(uc_tmpUs.&ua_fields[i,1].) + "'"
							CASE ALLTRIM(ua_fields[i,2]) = "N"
								uv_update = uv_update + IIF(EMPTY(uv_update), "", CHR(13) + ", ") + ALLTRIM(ua_fields[i,1]) + " = " + ASTR(uc_tmpUs.&ua_fields[i,1].)
							CASE ALLTRIM(ua_fields[i,2]) = "L"
								uv_update = uv_update + IIF(EMPTY(uv_update), "", CHR(13) + ", ") + ALLTRIM(ua_fields[i,1]) + " = " + IIF(uc_tmpUs.&ua_fields[i,1]., "1", "0")
							CASE ALLTRIM(ua_fields[i,2]) = "T"
								uv_update = uv_update + IIF(EMPTY(uv_update), "", CHR(13) + ", ") + ALLTRIM(ua_fields[i,1]) + " = '" + uf_gerais_getDate(uc_tmpUs.&ua_fields[i,1]., "SQL") + "'"
							CASE ALLTRIM(ua_fields[i,2]) = "D"
								uv_update = uv_update + IIF(EMPTY(uv_update), "", CHR(13) + ", ") + ALLTRIM(ua_fields[i,1]) + " = '" + uf_gerais_getDate(uc_tmpUs.&ua_fields[i,1]., "SQL") + "'"
						ENDCASE

				ENDCASE

			NEXT

			TEXT TO uv_syncUs TEXTMERGE NOSHOW

				UPDATE
					b_us
				SET
					<<ALLTRIM(uv_update)>>
				WHERE
					iniciais = '<<ALLTRIM(uv_iniciais)>>'

			ENDTEXT

			IF !uf_gerais_runSQLCentral(uv_syncUs)
				uf_perguntalt_chama("N�o foi possivel atualizar os dados do utilizador na central! Por favor contacte o suporte.", "OK", "", 16)
				RETURN 2
			ENDIF

		ENDIF

		**_cliptext = lcSql
		IF uf_gerais_actgrelha("", "", uv_sql)
			RETURN 1
		ELSE
			uf_perguntalt_chama("N�o foi possivel actualizar a password do utilizador. Contacte o suporte.", "OK", "", 16)
			RETURN 0
		ENDIF 
		
	ENDIF 
	
	
ENDFUNC 
