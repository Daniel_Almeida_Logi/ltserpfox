**
FUNCTION fecha
	LPARAMETERS lcCursor
	
	IF USED(lcCursor)
		SELECT &lcCursor
		USE
		
		RETURN .t.
	ELSE
		RETURN .f.
	ENDIF
ENDFUNC


**
FUNCTION astr
	LPARAMETERS lcVar, lcValInt, lcValDesc
	
	DO CASE 
		CASE TYPE("lcVar") == "L"
			IF lcVar
				RETURN "true"
			ELSE
				RETURN "false"
			ENDIF 
			
		CASE TYPE("lcVar") == "C"
			RETURN ALLTRIM(lcVar)
			
		CASE TYPE("lcVar") == "N"
			IF TYPE("lcValInt") == "N" AND TYPE("lcValDesc") == "N"
				RETURN ALLTRIM(STR(lcVar,lcValInt,lcValDesc))
			ELSE
				RETURN ALLTRIM(STR(lcVar))
			ENDIF
		
		CASE TYPE("lcVar") == "D"
			RETURN ALLTRIM(uf_gerais_getdate(lcVar))
		
		OTHERWISE 
			LOCAL lcRetorno
			STORE "" TO lcRetorno
			TRY 
				lcRetorno = ALLTRIM(STR(lcVar))
			CATCH 
				lcRetorno = ""
			ENDTRY
			
			RETURN lcRetorno
	ENDCASE 
ENDFUNC 


**
FUNCTION dtosql
	LPARAMETERS lcData
	
	RETURN uf_gerais_getDate(lcData,"SQL")
ENDFUNC 


FUNCTION getUmValor
    lparameters uv_tab,uv_exp,uv_cond,uv_order

    uv_curCursor = alias()

    uv_where = ''

    if !empty(uv_cond)
        uv_where = 'where '+alltrim(uv_cond)
    endif

    uv_ordem = ''

    if!empty(uv_order)
        uv_ordem = 'order by ' + alltrim(uv_order)
    endif

    TEXT TO msel TEXTMERGE NOSHOW

        Select 
            top 1 <<alltrim(uv_exp)>> as exp
        From
            <<alltrim(uv_tab)>>(nolock)
        <<alltrim(uv_where)>>    
        <<alltrim(uv_ordem)>>
            
    ENDTEXT
	
	



    uf_gerais_actGrelha("", "uc_getumvalorbd", msel)

    uv_retval = uc_getumvalorbd.exp

    fecha("uc_getumvalorbd")

    IF !EMPTY(uv_curCursor)
        select &uv_curCursor
    ENDIF

    return uv_retval

ENDFUNC