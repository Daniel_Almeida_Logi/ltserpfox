**
FUNCTION uf_configdemo_chama
	IF TYPE('CONFIGDEMO')=="U"
		DO FORM CONFIGDEMO
	ELSE
		CONFIGDEMO.show
	ENDIF

ENDFUNC 


**
FUNCTION uf_configdemo_configura
	LPARAMETERS lcOpcao
	LOCAL lcSQL
	IF EMPTY(lcOpcao)
		lcOpcao = 0
	ENDIF 
	
	TEXT TO lcSQL NOSHOW TEXTMERGE
		DELETE from b_pcentral
		EXEC up_manutencao_ActualizaPainelCentralPelosTerminais <<lcOpcao>>
		
		IF 1 = <<lcOpcao>>
		BEGIN
			UPDATE b_parameters SET  textValue = 'FARMACIA' WHERE stamp = 'ADM0000000082'
		END
		
		IF 2 = <<lcOpcao>>
		BEGIN
			UPDATE b_parameters SET  textValue = 'CLINICA' WHERE stamp = 'ADM0000000082'
		END
		
		IF 3 = <<lcOpcao>>
		BEGIN
			UPDATE b_parameters SET  textValue = 'ARMAZEM' WHERE stamp = 'ADM0000000082'
		END
		
		IF 4 = <<lcOpcao>>
		BEGIN
			UPDATE b_parameters SET  textValue = 'EMPRESA' WHERE stamp = 'ADM0000000082'
		END
		
	ENDTEXT 
	
	If !uf_gerais_actgrelha("", "", lcSQL)
		MESSAGEBOX("N�o foi possivel fazer a configura��o do Painel Central. Contacte o suporte.",64)
		QUIT
	ENDIF 
	
	uf_configdemo_sair()

ENDFUNC 


**
FUNCTION uf_configdemo_sair

	&& Fecha aplica��o
	configdemo.hide
	configdemo.release
	
ENDFUNC 