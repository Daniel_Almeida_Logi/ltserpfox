FUNCTION uf_VacinacaoPesq_Chama
	lcSQL=''
	TEXT TO lcSQL TEXTMERGE noshow
		SET FMTONLY ON
		exec up_vacinacao_pesquisa  '','','','', '', 0
		SET FMTONLY OFF
	ENDTEXT	
	
	uf_gerais_actGrelha("","uCrsVacinacaoPesq",lcSQL)
	
	IF TYPE("VacinacaoPesq")=="U"
		DO FORM VacinacaoPesq
	ELSE
		VacinacaoPesq.show
	ENDIF
ENDFUNC

*****************************************
*	 Fun��o que Pesquisa Vacinacaoes 	*
*****************************************
FUNCTION uf_VacinacaoPesq_Pesquisar
	lcSQL=''
	TEXT TO lcSQL TEXTMERGE noshow
		up_vacinacao_pesquisa  '<<uf_gerais_getDate(VacinacaoPesq.txtDataini.value,"SQL")>>','<<uf_gerais_getDate(VacinacaoPesq.txtDataFim.value,"SQL")>>','<<ALLTRIM(VacinacaoPesq.txtCliente.value)>>','<<ALLTRIM(VacinacaoPesq.txtAdministrante.value)>>'
		, '<<ALLTRIM(VacinacaoPesq.txtTipoReg.value)>>', <<mySite_nr>>
	ENDTEXT	
	
	uf_gerais_actGrelha("VacinacaoPesq.grdPesq","uCrsVacinacaoPesq",lcSQL)
ENDFUNC 

*****************************************************
*		Fechar Ecra de Pesquisa de Vacinca��es		*
*****************************************************
FUNCTION uf_VacinacaoPesq_sair
	IF USED("uCrsVacinacaoPesq")
		fecha("uCrsVacinacaoPesq")
	ENDIF 
	
	VacinacaoPesq.hide
	VacinacaoPesq.release
ENDFUNC

FUNCTION uf_vacinacaoPesq_topo
	IF USED("uCrsVacinacaoPesq")
		IF RECCOUNT("uCrsVacinacaoPesq")>0
			SELECT uCrsVacinacaoPesq
			GO top
		ENDIF
		VacinacaoPesq.grdPesq.refresh
	ENDIF
ENDFUNC

FUNCTION uf_vacinacaoPesq_fundo
	IF USED("uCrsVacinacaoPesq")
		IF RECCOUNT("uCrsVacinacaoPesq")>0
			SELECT uCrsVacinacaoPesq
			GO bottom
		ENDIF
		VacinacaoPesq.grdPesq.refresh
	ENDIF
ENDFUNC 

FUNCTION uf_vacinacaoPesq_teclado
	vacinacaoPesq.tecladoVirtual1.show("vacinacaoPesq.grdPesq", 161, "vacinacaoPesq.shape1", 161)
ENDFUNC 