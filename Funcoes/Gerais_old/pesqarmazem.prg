**
FUNCTION uf_pesqArmazem_Chama
	LPARAMETERS lcEcra, lcObj, lcTipoObj

	** valida��es
	IF !myDocIntroducao AND !myDocAlteracao AND lcEcra == 'DOCUMENTOS'
		RETURN .f.
	ENDIF
	IF !myFtIntroducao AND !myFtAlteracao AND lcEcra == 'FACTURACAO'
		RETURN .f.
	ENDIF

	LOCAL lcSQL
	PUBLIC myOrigemEcraPPArm, myOrigemObjPPArm, myOrigemTipoPPArm

	myOrigemEcraPPArm	= lcEcra
	myOrigemObjPPArm	= lcObj
	myOrigemTipoPPArm	= lcTipoObj
			
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_gerais_armazensDisponiveisLista '<<ALLTRIM(mysite)>>', '<<ch_userno>>', '<<ch_grupo>>'
	ENDTEXT 		
	IF !uf_gerais_actgrelha("", "ucrsPesqArmazens", lcSql)
		RETURN .f.
	ENDIF
	
	**
	IF TYPE("PESQARMAZEM") == 'U'
		DO FORM PESQARMAZEM
	ELSE
		PESQARMAZEM.show
	ENDIF
ENDFUNC


**
FUNCTION uf_pesqArmazem_sel

	SELECT ucrsPesqArmazens
	
	IF !EMPTY(myOrigemObjPPArm) AND !EMPTY(myOrigemTipoPPArm)
	
		DO CASE
			CASE myOrigemTipoPPArm == 0 && variavel
				&myOrigemObjPPArm 			= ALLTRIM(STR(ucrsPesqArmazens.armazem))
				
			CASE myOrigemTipoPPArm == 1 && objecto
				&myOrigemObjPPArm..value	= ALLTRIM(STR(ucrsPesqArmazens.armazem))
			
			CASE myOrigemTipoPPArm == 2 && cursor
				lcCursor = STREXTRACT(myOrigemObjPPArm, "", ".")
				SELECT &lcCursor

				Replace &myOrigemObjPPArm With ALLTRIM(STR(ucrsPesqArmazens.armazem))
		ENDCASE
				
	ELSE
		
		DO case
			CASE myOrigemEcraPPArm == 'INVENTARIO'
				INVENTARIO.armazem.value =  ALLTRIM(STR(ucrsPesqArmazens.armazem))
		
			CASE myOrigemEcraPPArm == "DOCUMENTOS"
				Replace CabDOC.ARMAZEM WITH ucrsPesqArmazens.armazem
	
				IF myTipoDoc == "BO"
					IF USED("BI")
						SELECT ucrsPesqArmazens
						
						SELECT BI
						GO Top
						SCAN
							Replace Bi.armazem WITH ucrsPesqArmazens.armazem
						ENDSCAN
						
						SELECT BI2
						GO Top
						SCAN
							Replace Bi.ar2mazem WITH ucrsPesqArmazens.armazem
						ENDSCAN
						
						SELECT BI
						GO Top
						SELECT BI2
						GO Top
						
					ENDIF
				ENDIF
				
				IF myTipoDoc == "FO"
					IF USED("FN")
						SELECT ucrsPesqArmazens
						
						SELECT FN
						GO Top
						SCAN
							Replace FN.armazem WITH ucrsPesqArmazens.armazem
						ENDSCAN
						
						SELECT FN
						GO Top
					ENDIF
				ENDIF
					
				DOCUMENTOS.REFRESH
				
			CASE myOrigemEcraPPArm == "FACTURACAO"
				IF USED("FI")
					SELECT fi
					GO TOP
					SCAN
						replace fi.armazem WITH ucrsPesqArmazens.armazem
					ENDSCAN
					GO TOP
					
					facturacao.containerCab.armazem.value = ucrsPesqArmazens.armazem
					
					
					IF uf_gerais_getParameter("ADM0000000211","BOOL") && Lotes/Armazem
						
						UF_FACTURACAO_AtribuiLotesDocumento()
						
						uf_perguntalt_chama("Devido � altera��o de armaz�m os Lotes foram recalculados.","OK","",64)
					ENDIF
				ENDIF 
		ENDCASE
	ENDIF
	
	uf_pesqArmazem_sair()
ENDFUNC


**
FUNCTION uf_pesqArmazem_sair
	**Fecha Cursores
	IF USED("ucrsPesqArmazens")
		fecha("ucrsPesqArmazens")
	ENDIF
	
	** release vari�vel
	RELEASE myOrigemEcraPPArm
	RELEASE myOrigemObjPPArm
	release myOrigemTipoPPArm
	
	PESQARMAZEM.hide
	PESQARMAZEM.release
ENDFUNC


