FUNCTION uf_comunicasinave_chama
	LPARAMETERS lcNO, uv_plano, uv_ftStamp, uv_nrAtend, uv_nrReceita

    PUBLIC upv_codPlano

    upv_codPlano = IIF(!EMPTY(uv_plano), ALLTRIM(uv_plano), '')

	IF !uf_gerais_getParameter_site('ADM0000000148', 'BOOL', mySite)
		return .f.
	ENDIF
	
	if(USED("ucrsatendcl"))
		SELECT ucrsatendcl
		if(LEN(ALLTRIM(ucrsatendcl.nbenef)))>=10
			uf_perguntalt_chama("Por favor valide o n�mero de utente!","OK","",16)
			return .f.
		ENDIF					
	ENDIF
	
	IF EMPTY(lcNO)
		lcNO = ""
	ENDIF
	
	IF ALLTRIM(myChamaSinave) = 'N�o chama'
		myChamaSinave = 'Sem Comparticipa��o'
	ENDIF 
	
	LOCAL lcSQL
	STORE "" TO lcSQL
	
	TEXT TO lcSQL TEXTMERGE noshow
		select *,'00:00' as timecolheita, '00:00' as timevalid from ext_sinave where 0=1
	ENDTEXT
	
	IF !(uf_gerais_actGrelha("","uCrsComSinave",lcSQL))
		uf_perguntalt_chama("OCORREU UM ERRO A CRIAR O CURSOR DE COMUNICA��O COM O SINAVE! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
	ENDIF
	SELECT uCrsComSinave
	APPEND BLANK 
	
	TEXT TO lcSQL TEXTMERGE noshow
		select * from ext_sinave_Disease where 0=1
	ENDTEXT
	
	IF !(uf_gerais_actGrelha("","uCrsCom_sinave_Disease",lcSQL))
		uf_perguntalt_chama("OCORREU UM ERRO A CRIAR O CURSOR DE COMUNICA��O COM O SINAVE! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
	ENDIF
	SELECT uCrsCom_sinave_Disease
	APPEND BLANK 
	
	TEXT TO lcSQL TEXTMERGE noshow
		select * from ext_sinave_Disease_Prod where 0=1
	ENDTEXT
	
	IF !(uf_gerais_actGrelha("","uCrsCom_sinave_Disease_Prod",lcSQL))
		uf_perguntalt_chama("OCORREU UM ERRO A CRIAR O CURSOR DE COMUNICA��O COM O SINAVE! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
	ENDIF
	SELECT uCrsCom_sinave_Disease_Prod
	APPEND BLANK 
	
	TEXT TO lcSQL TEXTMERGE noshow
		select * from ext_sinave_Disease_ProdAnalysis where 0=1
	ENDTEXT
	
	IF !(uf_gerais_actGrelha("","uCrsCom_sinave_Disease_ProdAnalysis",lcSQL))
		uf_perguntalt_chama("OCORREU UM ERRO A CRIAR O CURSOR DE COMUNICA��O COM O SINAVE! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
	ENDIF
	SELECT uCrsCom_sinave_Disease_ProdAnalysis
	APPEND BLANK 
	
	TEXT TO lcSQL TEXTMERGE noshow
		select * from ext_sinave_Disease_ProdAnalysisTech where 0=1
	ENDTEXT
	
	IF !(uf_gerais_actGrelha("","uCrsCom_ext_sinave_Disease_ProdAnalysisTech",lcSQL))
		uf_perguntalt_chama("OCORREU UM ERRO A CRIAR O CURSOR DE COMUNICA��O COM O SINAVE! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
	ENDIF
	SELECT uCrsCom_ext_sinave_Disease_ProdAnalysisTech
	APPEND BLANK 
	
	TEXT TO lcSQL TEXTMERGE noshow
		select * from ext_sinaveResponse where 0=1
	ENDTEXT
	
	IF !(uf_gerais_actGrelha("","uCrsext_sinaveResponse",lcSQL))
		uf_perguntalt_chama("OCORREU UM ERRO A CRIAR O CURSOR DE COMUNICA��O COM O SINAVE! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
	ENDIF
	SELECT uCrsext_sinaveResponse
	APPEND BLANK 
	
	&& Controla abertura do painel
	IF TYPE("comunicasinave")=="U"
		DO FORM comunicasinave WITH uv_ftStamp, uv_nrAtend, uv_nrReceita
	ELSE
		comunicasinave.show
	ENDIF 
	
ENDFUNC





FUNCTION uf_comunicasinave_validaNumeroReceitaSinave
	LPARAMETERS lcReceitaSinave, lcPlano
	
	local lcString, lcPos, lcSlipt, lcResultSlipt, lcSlipt1


	lcDelimitador = 'x'	
	lcPos = 0
	lcSlipt = ""
	lcSlipt1 = ""
	

	if(EMPTY(lcReceitaSinave))
		RETURN .t.
	endif	
		
	if(EMPTY(lcPlano))
		RETURN .t.
	endif
	
	if(ALLTRIM(lcPlano)!='RS')
		RETURN .t.
	endif
		
		
	if(LEN(ALLTRIM(lcReceitaSinave))!=19)
		RETURN .f.
	endif		
	
	lcReceitaSinave = lower(ALLTRIM(lcReceitaSinave))	
	
	lcPos  = AT(lcDelimitador,lcReceitaSinave )


	if(lcPos <= 0)
		RETURN .f.
	endif
	

	lcSlipt  = GetWordNum(lcReceitaSinave,2,lcDelimitador)
	lcSlipt1  = GetWordNum(lcReceitaSinave,3,lcDelimitador)
	lcSinave  = GetWordNum(lcReceitaSinave,1,lcDelimitador)
	
	if(!empty(lcSlipt1))
		RETURN .f.
	endif

	if(!empty(lcSinave))


		if(!uf_gerais_eDigito(lcSinave))
			RETURN .f.
		endif
				
		if(VAL(lcSinave)<=9999999 or  VAL(lcSinave)>=10000000000)
			RETURN .f.
		endif
	endif
	
	
	if(!empty(lcSlipt))
	
		FOR i = 1 TO len((ALLTRIM(lcSlipt)))
			if SUBSTR(lcSlipt,i,1) !="0"
				RETURN .f.
			endif		
		ENDFOR
	endif
	

	RETURN .t.
ENDFUNC



*********************************
*		CONFIGURA O PAINEL		*
*********************************
FUNCTION uf_comunicasinave_confEcra


	uf_comunicasinave_pesquisarUltimo()

	SELECT uCrsComSinave
	IF ALLTRIM(ucrsatendcl.nome) <> 'CONSUMIDOR FINAL'
		replace uCrsComSinave.healthUsername WITH ucrsatendcl.nome
	ELSE
		replace uCrsComSinave.healthUsername WITH ''
	ENDIF 
	replace uCrsComSinave.rnu WITH VAL(ucrsatendcl.nbenef)
	replace uCrsComSinave.healthUserContact WITH ucrsatendcl.tlmvl
	replace uCrsComSinave.healthUsersex WITH ucrsatendcl.sexo
	replace uCrsComSinave.healthUserAddress WITH ucrsatendcl.morada
	IF AT('-', ucrsatendcl.codpost)<>0
		replace uCrsComSinave.healthUserCodPostalBase WITH LEFT(ucrsatendcl.codpost, AT('-', ucrsatendcl.codpost)-1 )
		replace uCrsComSinave.healthUserCodPostalSuffix WITH SUBSTR(ucrsatendcl.codpost, AT('-', ucrsatendcl.codpost)+1, 3)
	ENDIF
	replace uCrsComSinave.healthTpDocUserId WITH 'CC'
	replace uCrsComSinave.healthUserId WITH ucrsatendcl.bino
	replace uCrsComSinave.healthUserCountryBorn WITH ucrsatendcl.codigop
	replace uCrsComSinave.infoanalise WITH ''
	replace uCrsComSinave.infotecnica WITH ''
	replace uCrsComSinave.infofinalidade WITH  IIF(USED("ucrsTempLastSinaveRequest"), ALLTRIM(ucrsTempLastSinaveRequest.infofinalidade), '')
	replace uCrsComSinave.regCodOriDate WITH datetime()
	replace uCrsComSinave.codValidDate WITH datetime()
	replace uCrsComSinave.opcolheita WITH ch_vendnm
	replace uCrsComSinave.opregisto WITH ch_vendnm
	replace uCrsComSinave.batch_id WITH  IIF(USED("ucrsTempLastSinaveRequest"), ALLTRIM(ucrsTempLastSinaveRequest.batch_id ), '')
	replace uCrsComSinave.batch_valdate  WITH  IIF(USED("ucrsTempLastSinaveRequest"), ALLTRIM(ucrsTempLastSinaveRequest.batch_valdate), '')
	replace uCrsComSinave.timecolheita WITH time()
	replace uCrsComSinave.timevalid  WITH time()
	replace uCrsComSinave.infoanalise WITH 'Teste r�pido para pesquisa de antig�nio do SARS-CoV-2'
	replace uCrsComSinave.cod_infoanalise WITH '230'
	replace uCrsComSinave.infotecnica WITH 'Imunocromatografia 246 Infe��o pelo SARS-CoV-2/COVID-19'
	replace uCrsComSinave.cod_infotecnica WITH '135'
	replace uCrsComSinave.regCodOri WITH IIF(USED("ucrsTempLastSinaveRequest"), ucrsTempLastSinaveRequest.regCodOri, 1)
	IF AT('-', ucrse1.codpost)<>0
		replace uCrsComSinave.outpatientCodPostalBase WITH LEFT(ucrse1.codpost, AT('-', ucrse1.codpost)-1 )
		replace uCrsComSinave.outpatientCodPostalSuffix WITH SUBSTR(ucrse1.codpost, AT('-', ucrse1.codpost)+1, 3)
	ENDIF
	
	replace uCrsCom_sinave_Disease_Prod.prodName WITH 'Exsudado nasofar�ngeo'
	replace uCrsCom_sinave_Disease_Prod.prodCod WITH '109'
	
	replace uCrsCom_sinave_Disease_ProdAnalysis.manufacturername WITH IIF(USED("ucrsTempLastSinaveRequest"), ALLTRIM(ucrsTempLastSinaveRequest.manufacturername), '')
	replace uCrsCom_sinave_Disease_ProdAnalysis.manufacturercode WITH IIF(USED("ucrsTempLastSinaveRequest"), ALLTRIM(ucrsTempLastSinaveRequest.manufacturerCode), '')
	
	replace uCrsCom_sinave_Disease_ProdAnalysis.name             WITH IIF(USED("ucrsTempLastSinaveRequest"), ALLTRIM(ucrsTempLastSinaveRequest.analiseName), '')
	replace uCrsCom_sinave_Disease_ProdAnalysis.codtest          WITH IIF(USED("ucrsTempLastSinaveRequest"), ALLTRIM(ucrsTempLastSinaveRequest.analiseCode), '')
	
	
	replace uCrsCom_ext_sinave_Disease_ProdAnalysisTech.resqualitative WITH ''
	
	
	fecha("ucrsTempLastSinaveRequest")
	
	comunicasinave.refresh
ENDFUNC


*****************************
*	PESQUISAR CLIENTE		*
*****************************
FUNCTION uf_comunicasinave_pesquisarCliente
	uf_pesqUTENTES_Chama("COMUNICASINAVE")
ENDFUNC


*****************************
*	PESQUISAR CODIGO SINAVE *
*****************************
FUNCTION uf_comunicasinave_codigoSinave


	LOCAL lcSQL, lcCodSinave 
	
	STORE '' TO lcSQL, lcCodSinave 
	
	fecha("uCrsButCod")
	
	LOCAL lcSinaveOpNome 
	lcSinaveOpNome =''
	
	
	fecha("uCrsSinaveOpCodTemp")
	
	if(USED("uCrsComSinave"))
		SELECT uCrsComSinave		
		lcSinaveOpNome = ALLTRIM(uCrsComSinave.opregisto)
	ENDIF
	
	**Pesquisa codigo do sinave escolhido no painel
	
	IF(!EMPTY(ALLTRIM(lcSinaveOpNome )))
		
		TEXT TO lcSQL TEXTMERGE NOSHOW 
			select top 1 ISNULL(codSinave,'') as codSinave from b_us(nolock) where nome='<<alltrim(lcSinaveOpNome)>>' and codSinave!='' and inactivo = 0 order by ousrdata desc
		ENDTEXT		
		IF !uf_gerais_actGrelha("","uCrsSinaveOpCodTemp",lcSQL)
			uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VERIFICAR O CODIGO DO SINAVE. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			RETURN .f.
		ENDIF 
		
		
		** se tem codigo sinave na ficha do operador
		IF(USED("uCrsSinaveOpCodTemp"))
			SELECT uCrsSinaveOpCodTemp
			GO TOP
			lcCodSinave = ALLTRIM(uCrsSinaveOpCodTemp.codSinave)
		ENDIF		
	
	ENDIF
	
	
	fecha("uCrsSinaveOpCodTemp")
	
	
	
	lcSQL = ''
	
	**Se vazio
	**Pesquisa codigo do sinave da ficha do operador
	if(EMPTY(lcCodSinave))

		TEXT TO lcSQL TEXTMERGE NOSHOW 
			select ISNULL(codSinave,'') as codSinave from b_us(nolock) where iniciais='<<alltrim(m_chinis)>>'
		ENDTEXT		
		IF !uf_gerais_actGrelha("","uCrsButCod",lcSQL)
			uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VERIFICAR O CODIGO DO SINAVE. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			RETURN .f.
		ENDIF 	
		
		** se tem codigo sinave na ficha do operador
		IF(USED("uCrsButCod"))
			SELECT uCrsButCod
			GO TOP
			lcCodSinave = ALLTRIM(uCrsButCod.codSinave)
		ENDIF
		
		fecha("uCrsButCod")
	
	ENDIF
	
	
	
		
	**Se vazio
	**Pesquisa codigo do sinave da ficha da empresa
	if(EMPTY(lcCodSinave))		
		if(USED("ucrse1"))
			SELECT ucrse1
			GO TOP
			lcCodSinave=ALLTRIM(ucrse1.codSinave)
		ENDIF	
	ENDIF
	
	RETURN lcCodSinave 


ENDFUNC
*****************************
*	PESQUISAR CODIGO SINAVE *
*****************************

*************************************************
*		GRAVA O REGISTO NA BASE DE DADOS		*
*************************************************
FUNCTION uf_comunicasinave_gravar

	LOCAL lcNifLab
	
	IF !uf_comunicasinave_validacoes()
		RETURN .f.
	ENDIF 
	
	regua(0,6,"A gravar registo...")
	
	SELECT ucrse1
	lcNifLab = ALLTRIM(ucrse1.ncont)
	
	SELECT uCrsComSinave
	SELECT uCrsCom_sinave_Disease
	SELECT uCrsCom_sinave_Disease_Prod
	SELECT uCrsCom_sinave_Disease_ProdAnalysis
	SELECT uCrsCom_ext_sinave_Disease_ProdAnalysisTech

	LOCAL lctokensinave, lcDataColheita, lcSQLDataColheita, lcDataValidacao, lcSQLDataValidacao, lcHoraColheita, lcHoravalidacao, lcRNU, lcNomeEnvio, lcIDUtente, lcTipoIDUtente, lcDtNascimento, lcSexoUt
	LOCAL lcTesteEnvio, lcCodExame, lcNIFFarmacia, lcProdCod, lcProdCodDescr, lcProdCode, lcCodAnalise, lcCodFabricante, lcCodteste, lcCodtesteDescr, lcTecnica, lcResultado, lcDtNascimentotxt, lcCodSinave
	LOCAL lcCodPostFarm, lcCodPostFarmSuf, lcLote, lcValLote, lccod_infoanalise , lccod_infotecnica , lcMorada, lcContacto, lcCodpostPref, lcCodPostSuf, lcTpDocID, lcNacionalidade, lcinfoanalise,lccod_infoanalise
	LOCAL lcinfotecnica ,lccod_infotecnica, lcFinalidade, lcOpColheita, lcOpRegisto, lcNomeFabricante, lcNacionalidade
	
	SELECT uCrsComSinave
	lcSQLDataColheita = uf_gerais_getdate(uCrsComSinave.regCodOriDate,"SQL")
	lcSQLDataValidacao = uf_gerais_getdate(uCrsComSinave.codValidDate,"SQL")
	lcHoraColheita = alltrim(uCrsComSinave.timecolheita)
	lcHoravalidacao = alltrim(uCrsComSinave.timevalid)
	lcDataColheita = ALLTRIM(left(lcSQLDataColheita,4)+'-'+substr(lcSQLDataColheita,5,2)+'-'+substr(lcSQLDataColheita,7,2)+' '+alltrim(lcHoraColheita)+':00')
	lcDataValidacao = ALLTRIM(left(lcSQLDataValidacao,4)+'-'+substr(lcSQLDataValidacao,5,2)+'-'+substr(lcSQLDataValidacao,7,2)+' '+alltrim(lcHoravalidacao)+':00')
	lcRNU = uCrsComSinave.rnu
	lcNomeEnvio = ALLTRIM(uCrsComSinave.healthUsername)
	lcIDUtente = ALLTRIM(uCrsComSinave.healthUserId)
	lcTipoIDUtente = ALLTRIM(uCrsComSinave.healthTpDocUserId)
	lcDtNascimentotxt = uf_gerais_getdate(uCrsComSinave.healthUserBirthDate,"SQL")
	lcDtNascimento = ALLTRIM(left(lcDtNascimentotxt,4)+'-'+substr(lcDtNascimentotxt,5,2)+'-'+substr(lcDtNascimentotxt,7,2))
	lcSexoUt = ALLTRIM(uCrsComSinave.healthUsersex)
	lcCodExame = uCrsComSinave.regCodOri
	lcCodAnalise = ALLTRIM(uCrsComSinave.cod_infoanalise)
	lcTecnica = ALLTRIM(uCrsComSinave.infotecnica)
	lcCodPostFarm = ALLTRIM(uCrsComSinave.outpatientCodPostalBase)
	lcCodPostFarmSuf = ALLTRIM(uCrsComSinave.outpatientCodPostalSuffix)
	lcLote = ALLTRIM(uCrsComSinave.batch_id)
	lcValLote = ALLTRIM(uCrsComSinave.batch_valdate)
	lccod_infoanalise = ALLTRIM(uCrsComSinave.cod_infoanalise)
	lccod_infotecnica = ALLTRIM(uCrsComSinave.cod_infotecnica)
	lcmorada = ALLTRIM(uCrsComSinave.healthUserAddress)
	lcContacto = ALLTRIM(uCrsComSinave.healthUserContact)
	lcCodpostPref = ALLTRIM(uCrsComSinave.healthUserCodPostalBase) 
	lcCodPostSuf = ALLTRIM(uCrsComSinave.healthUserCodPostalSuffix)
	lcTpDocID = ALLTRIM(uCrsComSinave.healthTpDocUserId)
	lcNacionalidade = ALLTRIM(uCrsComSinave.healthUserCountryBorn)
	***
	lcinfoanalise = ALLTRIM(uCrsComSinave.infoanalise)
	lccod_infoanalise = ALLTRIM(uCrsComSinave.cod_infoanalise)
	lcinfotecnica = ALLTRIM(uCrsComSinave.infotecnica)
	lccod_infotecnica = ALLTRIM(uCrsComSinave.cod_infotecnica)
	lcFinalidade = ALLTRIM(uCrsComSinave.infofinalidade)
	lcOpColheita = ALLTRIM(uCrsComSinave.opcolheita)
	lcOpRegisto = ALLTRIM(uCrsComSinave.opregisto)
	lcNacionalidade = ALLTRIM(uCrsComSinave.healthUserCountryBorn)
	
	SELECT uCrsCom_sinave_Disease_Prod
	lcProdCod = ALLTRIM(uCrsCom_sinave_Disease_Prod.prodcod)
	lcProdCodDescr = ALLTRIM(uCrsCom_sinave_Disease_Prod.prodName)
	
	SELECT uCrsCom_sinave_Disease_ProdAnalysis
	lcCodFabricante = ALLTRIM(uCrsCom_sinave_Disease_ProdAnalysis.manufacturercode)
	lcCodteste = ALLTRIM(uCrsCom_sinave_Disease_ProdAnalysis.codtest)
	lcCodtesteDescr = ALLTRIM(uCrsCom_sinave_Disease_ProdAnalysis.name)
	***
	lcNomeFabricante = ALLTRIM(uCrsCom_sinave_Disease_ProdAnalysis.manufacturername)
	
	SELECT uCrsCom_ext_sinave_Disease_ProdAnalysisTech
	lcResultado = ALLTRIM(uCrsCom_ext_sinave_Disease_ProdAnalysisTech.resqualitative)
	

	
	IF uf_gerais_getparameter("ADM0000000227", "BOOL")==.F.
		lcTesteEnvio = '0'
	ELSE
		lcTesteEnvio = '1'
	ENDIF 
	
	LOCAL lcIdLt,lcNomeCmp, lcCodFarm
	STORE "" TO  lcIdLt,lcNomeCmp, lcCodFarm
	
	SELECT ucrse1
	lcNIFFarmacia = ALLTRIM(ucrse1.ncont)
	lcIdLt = ALLTRIM(ucrse1.id_lt)
    lcNomeCmp = ALLTRIM(ucrse1.nomecomp)
    lcCodFarm = ALLTRIM(ucrse1.u_codFarm)
    
    if(EMPTY(lcIdLt))
    	REGUA(2)
		uf_perguntalt_chama("OCORREU UM ERRO REGISTAR O CURSOR DE COMUNICA��O COM O SINAVE. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
    ENDIF
    

	IF USED("uCrsComSinave")

		IF uf_gerais_getUmValor("ext_sinave", "count(*)", "token = '" + ALLTRIM(uCrsComSinave.token) + "'") = 1
			IF !uf_comunicasinave_delete(uCrsComSinave.token)
                REGUA(2)
				RETURN .F.
			ENDIF
		ENDIF

	ENDIF
	
	
	lcCodSinave = uf_comunicasinave_codigoSinave()
	
	lctokensinave = uf_gerais_stamp()
	
	regua(1,1,"A gravar registo...")

   uv_token = ''
   uv_idMcdt = ''
   uv_nrReceita = ''
   uv_numAtend = ''
   uv_ftStamp = ''
   
   SELECT FI
   GO TOP


   IF WEXIST("ATENDIMENTO")
       uv_numAtend = ALLTRIM(nratendimento)
       uv_token = fi.token
       uv_nrReceita = fi.receita
   ELSE
       SELECT FT
       uv_ftStamp = ft.ftstamp
       SELECT FT2
       uv_token = ft2.token
       uv_nrReceita = ft2.u_receita
   ENDIF

   IF LEN(ALLTRIM(fi.id_validacaoDem)) >= 21
       uv_idMcdt = fi.id_validacaoDem
   ELSE

       uv_idMcdt = LEFT(ALLTRIM(uv_nrReceita), 18) + IIF(!EMPTY(fi.id_validacaoDem) AND uf_gerais_isNumeric(fi.id_validacaoDem), RIGHT('0000' + ALLTRIM(fi.id_validacaoDem),4), '0001' )

   ENDIF
   


   lcReport = myPath + "\analises\declaracaosinave.frx"
   lcNomeFicheiroPDF = ALLTRIM(uv_idMcdt) + "_" + ALLTRIM(uf_gerais_stamp()) + ".pdf"
   lcExpFolder = ALLTRIM(uf_gerais_getParameter_site("ADM0000000161", "TEXT", mySite)) + "\ArquivoDigital\Resultados\Presta��es\"
   lcTabCab = "uCrsComSinave"
   uv_path = ALLTRIM(lcExpFolder) + ALLTRIM(lcNomeFicheiroPDF)

   uf_guarda_pdf(ALLTRIM(lcreport), STRTRAN(STRTRAN(ALLTRIM(lcNomeFicheiroPDF),'/',''),' ',''), .T., lcTabCab, ALLTRIM(lcExpFolder) )
	
	TEXT TO lcSQL TEXTMERGE noshow

      INSERT INTO <<ALLTRIM(uf_gerais_getParameter_site("ADM0000000090", "TEXT", mySite))>>.dbo.anexosMCDT
         (path, codInfarmed, idCL, site, nome, mcdtId, nr_receita, token, data_cre, ousrdata, ousrinis, tokenSinave, idSNS)
      VALUES
         ('<<ALLTRIM(uv_path)>>', '<<ALLTRIM(lcCodFarm)>>', '<<ALLTRIM(lcIdLt)>>', '<<ALLTRIM(mySite)>>', '<<ALLTRIM(lcNomeCmp)>>', '<<ALLTRIM(uv_idMcdt)>>', '<<ALLTRIM(uv_nrReceita)>>', '<<ALLTRIM(uv_token)>>', GETDATE(), GETDATE(), '<<ALLTRIM(m_chinis)>>', '<<ALLTRIM(lctokensinave)>>', <<lcRNU>>)
	
		INSERT INTO ext_sinave(token,requestType ,niflab,codValidDate,regCodOri,regCodOriDate,outpatientCodLocalHarvest,rnu,healthUsername,healthUserId,healthUserTypeDocIdent, healthUserBirthDate,healthUsersex,siteTest,site,ousrdata,ousrinis,usrdata,usrinis,codsinave, outpatientCodPostalBase, outpatientCodPostalSuffix, batch_id, batch_valdate, cod_infoanalise, cod_infotecnica
								,healthUserAddress, healthUserContact, healthUserCodPostalBase, healthUserCodPostalSuffix, healthTpDocUserId, infoanalise, infotecnica, infofinalidade, opcolheita, opregisto, healthUserCountryBorn,
                        nr_atend, path, mcdtId, nr_receita, ftstamp, tokenMcdt)
		Values ('<<ALLTRIM(lctokensinave)>>',1,'<<lcNifLab>>','<<lcDataValidacao>>',<<lcCodExame>>,'<<lcDataColheita>>',<<lcNIFFarmacia>>,<<lcRNU>>,'<<lcNomeEnvio>>','<<lcIDUtente>>','<<lcTipoIDUtente>>','<<lcDtNascimento>>','<<lcSexoUt>>',<<lcTesteEnvio>>,'<<ALLTRIM(mySite)>>',getdate(),'<<alltrim(m_chinis)>>',getdate(),'<<alltrim(m_chinis)>>','<<lcCodSinave>>','<<lcCodPostFarm>>','<<lcCodPostFarmSuf>>','<<lcLote>>', '<<lcValLote>>', '<<lccod_infoanalise>>' , '<<lccod_infotecnica>>' 
				,'<<lcmorada>>', '<<lcContacto>>', '<<lcCodpostPref>>', '<<lcCodPostSuf>>', '<<lcTpDocID>>','<<lcinfoanalise>>', '<<lcinfotecnica>>', '<<lcFinalidade>>', '<<lcOpColheita>>', '<<lcOpRegisto>>', '<<lcNacionalidade>>',
            '<<ALLTRIM(uv_numAtend)>>', '<<ALLTRIM(uv_path)>>', '<<ALLTRIM(uv_idMcdt)>>', '<<ALLTRIM(uv_nrReceita)>>', '<<ALLTRIM(uv_ftStamp)>>', '<<ALLTRIM(uv_token)>>')

		DECLARE @prodToken VARCHAR(36)
		SET @prodToken=LEFT( NEWID(),36)

		INSERT INTO ext_sinave_Disease(STAMP,token,diseaseCode,productToken,ousrdata,ousrinis,usrdata,usrinis)
		VALUES(LEFT( NEWID(),36),'<<ALLTRIM(lctokensinave)>>','246',@prodToken,getdate(),'<<alltrim(m_chinis)>>',getdate(),'<<alltrim(m_chinis)>>')

		DECLARE @analysisToken VARCHAR(36)
		SET @analysisToken=LEFT( NEWID(),36)

		INSERT INTO ext_sinave_Disease_Prod(stamp,productToken,prodCod,prodDateHarvest,prodName,prodTable, analysisToken,ousrdata,ousrinis,usrdata,usrinis)
		VALUES(LEFT( NEWID(),36),@prodToken,'<<lcProdCod>>','<<lcDataColheita>>','<<lcProdCodDescr>>','SINAVE',@analysisToken,getdate(),'<<alltrim(m_chinis)>>',getdate(),'<<alltrim(m_chinis)>>')

		DECLARE @techToken VARCHAR(36)
		SET @techToken=LEFT( NEWID(),36)

		INSERT INTO ext_sinave_Disease_ProdAnalysis (stamp,analysisToken,code,manufacturerCode,codTest,name,techToken,ousrdata,ousrinis,usrdata,usrinis, manufacturername)
		VALUES(LEFT( NEWID(),36),@analysisToken,'<<lcCodAnalise>>','<<lcCodFabricante>>','<<lcCodteste>>','<<lcCodtesteDescr>>',@techToken,getdate(),'<<alltrim(m_chinis)>>',getdate(),'<<alltrim(m_chinis)>>', '<<lcNomeFabricante>>')

		INSERT INTO ext_sinave_Disease_ProdAnalysisTech (stamp,techToken,name,resQualitative,ousrdata,ousrinis,usrdata,usrinis)
		VALUES(LEFT( NEWID(),36),@techToken,'<<lcTecnica>>','<<lcResultado>>',getdate(),'<<alltrim(m_chinis)>>',getdate(),'<<alltrim(m_chinis)>>')
	ENDTEXT
	

	
	IF !(uf_gerais_actGrelha("","",lcSQL))
        REGUA(2)
		uf_perguntalt_chama("OCORREU UM ERRO REGISTAR O CURSOR DE COMUNICA��O COM O SINAVE! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	ENDIF

	SELECT uCrsComSinave
	REPLACE uCrsComSinave.token WITH ALLTRIM(lctokensinave)

    uf_comunicasinave_readonly(.T.)

    comunicasinave.menu1.estado("gravar", "HIDE")
    comunicasinave.menu1.estado("comunicar, edit, pesquisar, imprimir", "SHOW")

	REGUA(2)
		
ENDFUNC


FUNCTION uf_comunicaSinave_validaCompart()

	LOCAL lcPosFi, lcFiStamp, lcPosAtendComp, lcCodigoPlan, lcFtstamp, lcLordemFi 
	
	lcCodigoPlan =''
	lcFiStamp  = ''
	


	if(ALLTRIM(myChamaSinave) == 'Com Comparticipa��o')
		RETURN .t.
	ENDIF
	
	if(ALLTRIM(myChamaSinave) == 'Sem Comparticipa��o')
		RETURN .f.
	ENDIF

	IF(!USED("fi"))
		RETURN .f.
	ENDIF
	
	if !USED("ucrsatendcomp") AND EMPTY(upv_codPlano)
		RETURN .f.
	ENDIF
	

	
	lcPosFi = RECNO("fi")
	lcPosAtendComp =   RECNO("ucrsatendcomp")
	
	SELECT fi
	lcLordemFi = ALLTRIM(STR(fi.lordem)) 
	
	
	fecha("ucrTempValFiAtend")

    IF !EMPTY(upv_codPlano)
	
        SELECT fi
        SCAN FOR LEFT(astr(fi.lordem), 2)=lcLordemFi   .AND. LEFT(fi.design, 1)=="."
            lcFiStamp = alltrim(fi.fistamp)
        ENDSCAN
        
        
        SELECT codigo FROM ucrsatendcomp WHERE fistamp = lcFiStamp INTO CURSOR  ucrTempValFiAtend READWRITE 
        
        if(USED("ucrTempValFiAtend"))
            SELECT ucrTempValFiAtend
            GO TOP
            lcCodigoPlan = 	ALLTRIM(ucrTempValFiAtend.codigo)
        ENDIF
        
        if(lcCodigoPlan == 'RS')
            myChamaSinave = 'Com Comparticipa��o'
        ENDIF

        
        fecha("ucrTempValFiAtend")

    ELSE

        IF UPPER(ALLTRIM(upv_codPlano)) == 'RS'
            myChamaSinave = 'Com Comparticipa��o'
        ENDIF

    ENDIF
	
	
	SELECT  Fi
    TRY
    GOTO lcPosFi
    CATCH
    	GOTO TOP
    ENDTRY
    
    SELECT  ucrsatendcomp
    TRY
    GOTO lcPosAtendComp
    CATCH
    	GOTO TOP
    ENDTRY
    

		
	RETURN .t.	
	
ENDFUNC


FUNCTION uf_comunicasinave_validacoes
	LOCAL lcmsgerro, lcreturn
	STORE '' TO lcmsgerro
	STORE .t. TO lcreturn 

	IF EMPTY(uCrsComSinave.healthUsername)
		lcmsgerro = lcmsgerro + 'Nome de utente n�o preenchido. ' + + CHR(13)
		lcreturn = .f.
	ENDIF 
	
	IF uCrsComSinave.rnu=0 AND ALLTRIM(uCrsComSinave.healthUserCountryBorn)='PT'
		lcmsgerro = lcmsgerro + 'N� de Sa�de do utente n�o preenchido. ' + + CHR(13)
		lcreturn = .f.
	ENDIF
	
	IF year(uCrsComSinave.healthUserBirthDate)=0 OR year(uCrsComSinave.healthUserBirthDate)=1900
		lcmsgerro = lcmsgerro + 'Data de nascimento n�o preenchida. ' + + CHR(13)
		lcreturn = .f.
	ENDIF 
	
*!*		IF EMPTY(uCrsComSinave.healthUserContact)
*!*			lcmsgerro = lcmsgerro + 'Contacto do utente n�o preenchido. ' + + CHR(13)
*!*			lcreturn = .f.
*!*		ENDIF 
	
*!*		IF EMPTY(uCrsComSinave.healthUsersex)
*!*			lcmsgerro = lcmsgerro + 'Sexo do utente n�o preenchido. ' + + CHR(13)
*!*			lcreturn = .f.
*!*		ENDIF 
	
	IF EMPTY(uCrsComSinave.healthUserAddress)
		lcmsgerro = lcmsgerro + 'Morada ' + + CHR(13)
		lcreturn = .f.
	ENDIF 
	
	IF (EMPTY(uCrsComSinave.healthUserCodPostalBase) OR EMPTY(uCrsComSinave.healthUserCodPostalSuffix) OR LEN(ALLTRIM(uCrsComSinave.healthUserCodPostalBase))<>4 OR LEN(ALLTRIM(uCrsComSinave.healthUserCodPostalSuffix))<>3) AND ALLTRIM(uCrsComSinave.healthUserCountryBorn)='PT'
		lcmsgerro = lcmsgerro + 'C�digo postal ' + + CHR(13)
		lcreturn = .f.
	ENDIF 
	
	IF EMPTY(uCrsComSinave.healthUserId) AND ALLTRIM(uCrsComSinave.healthUserCountryBorn)<>'PT'
		lcmsgerro = lcmsgerro + 'Nr. Doc. ID ' + + CHR(13)
		lcreturn = .f.
	ENDIF 
	
	IF EMPTY(uCrsComSinave.infofinalidade)
		lcmsgerro = lcmsgerro + 'Finalidade ' + + CHR(13)
		lcreturn = .f.
	ENDIF 
	
	IF EMPTY(uCrsCom_sinave_Disease_ProdAnalysis.manufacturername)
		lcmsgerro = lcmsgerro + 'Fabricante ' + + CHR(13)
		lcreturn = .f.
	ENDIF 
	
	IF EMPTY(uCrsCom_sinave_Disease_ProdAnalysis.name)
		lcmsgerro = lcmsgerro + 'Teste ' + + CHR(13)
	ENDIF 
	
	IF EMPTY(uCrsComSinave.batch_id)
		lcmsgerro = lcmsgerro + 'Lote ' + + CHR(13)
	ENDIF 	
	
	
	
	
	
	IF EMPTY(uCrsComSinave.batch_valdate)
		lcmsgerro = lcmsgerro + 'Data de validade do Lote ' + + CHR(13)
		lcreturn = .f.
	ENDIF 	
	
	IF EMPTY(uCrsCom_ext_sinave_Disease_ProdAnalysisTech.resqualitative)
		lcmsgerro = lcmsgerro + 'Resultado do teste ' + + CHR(13)
		lcreturn = .f.
	ENDIF 
	
	IF EMPTY(uCrsComSinave.regCodOri )
		lcmsgerro = lcmsgerro + 'Num. exame Lab ' + + CHR(13)
		lcreturn = .f.
	ENDIF 
	
	
	IF EMPTY(uCrsComSinave.timecolheita) OR LEN(ALLTRIM(uCrsComSinave.timecolheita)) !=5
		lcmsgerro = lcmsgerro + 'Hora da colheita ' + + CHR(13)
		lcreturn = .f.
	ENDIF
	
	IF EMPTY(uCrsComSinave.timevalid) OR LEN(ALLTRIM(uCrsComSinave.timevalid)) !=5
		lcmsgerro = lcmsgerro + 'Hora valida��o' + + CHR(13)
		lcreturn = .f.
	ENDIF
	
	
	
	IF LEN(lcmsgerro)>0
		IF LEN(ALLTRIM(lcmsgerro))>150
			uf_perguntalt_chama("EXISTEM CAMPOS OBRIGAT�RIOS POR PREENCHER, POR FAVOR VERIFIQUE:"+CHR(13) + LEFT(ALLTRIM(lcmsgerro),150) + ' ...',"OK","",64)
		ELSE 
			uf_perguntalt_chama("EXISTEM CAMPOS OBRIGAT�RIOS POR PREENCHER, POR FAVOR VERIFIQUE:"+CHR(13) + LEFT(ALLTRIM(lcmsgerro),150),"OK","",64)
		endif
	ENDIF 
	
	RETURN lcreturn 
ENDFUNC 


FUNCTION uf_comunicasinave_ValoresDefeito
*!*		SELECT uCrsMedicamentosHospitalares
*!*		replace dataValidade		with	CTOD('01.01.1900')
*!*		replace dataDispensa		with	CTOD('01.01.1900')
*!*		replace dataNascimento		with	CTOD('01.01.1900')
ENDFUNC



*********************************
*		FECHAR O PAINEL			*
*********************************
FUNCTION uf_comunicasinave_sair
		
	&& fecha cursor
	IF USED("uCrsMedicamentosHospitalares")
		fecha("uCrsMedicamentosHospitalares")
	ENDIF
	
	myChamaSinave = 'N�o chama'

	&& fecha painel
	comunicasinave.hide
	comunicasinave.release
	RELEASE comunicasinave
ENDFUNC


FUNCTION uf_comunicasinave_readonly
    LPARAMETERS uv_readOnly

	comunicasinave.txtSns.readonly = uv_readOnly
	comunicasinave.TxtCodpost.readonly = uv_readOnly
	comunicasinave.TxtContacto.readonly = uv_readOnly
	comunicasinave.txtHoraColheita.readonly = uv_readOnly
	comunicasinave.txtHoravalidacao.readonly = uv_readOnly
	comunicasinave.txtMorada.readonly = uv_readOnly
	comunicasinave.txtFinalidade.readonly = uv_readOnly
	comunicasinave.txtLote.readonly = uv_readOnly
	comunicasinave.txtValidade.readonly = uv_readOnly
	comunicasinave.txtNrDocID.readonly = uv_readOnly
	comunicasinave.TxtCodpost2.readonly = uv_readOnly
	comunicasinave.txtNumExameLab.readonly = uv_readOnly
ENDFUNC 


FUNCTION uf_comunicasinave_pesquisarUltimo
	LOCAL lcsql 
		
	LOCAL lcCodSinave 
	lcCodSinave =''	
	

	lcCodSinave = uf_comunicasinave_codigoSinave()
	
	fecha("ucrsTempLastSinaveRequest")
	
	lcsql = ''
    TEXT TO lcsql TEXTMERGE NOSHOW
    		exec up_get_SinaveLastRequest '<<ALLTRIM(lcCodSinave)>>'
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "ucrsTempLastSinaveRequest", lcsql)
       uf_perguntalt_chama("OCORREU A RETORNAR OS DADOS DO �LTIMO PEDIDO SINAVE", "OK", "", 16)
       fecha("ucrsTempLastSinaveRequest")
   ENDIF
   

ENDFUNC 


FUNCTION uf_comunicasinave_pesquisar
	LPARAMETERS lcUltimo, uv_ftStamp, uv_nrAtend, uv_nrReceita	

	PUBLIC stampenc
	stampenc = ''

    IF (!EMPTY(uv_ftStamp) OR !EMPTY(uv_nrAtend)) AND !EMPTY(uv_nrReceita)

        uv_filtro = ''

        IF !EMPTY(uv_ftStamp)
            uv_filtro = "ftstamp = '" + ALLTRIM(uv_ftStamp) + "' AND nr_receita = '" + ALLTRIM(uv_nrReceita) + "'"
        ELSE
            uv_filtro = "nr_atend = '" + ALLTRIM(uv_nrAtend) + "' AND nr_receita = '" + ALLTRIM(uv_nrReceita) + "'"
        ENDIF

        IF !EMPTY(uv_filtro)
            stampenc = uf_gerais_getUmValor("ext_sinave", "token", uv_filtro)
        ENDIF

    ELSE

        public lcDataFind
        uf_perguntalt_chama("POR FAVOR INSIRA A DATA PARA PESQUISA DOS DOCUMENTOS!","OK","",64)
        uf_getdate_chama(.f., "lcDataFind", 0, .f., .f., .t.)
        PUBLIC cval1
        STORE '' TO cval1
        uf_tecladoalpha_chama("cval1", "Introduza o NID ou n� Utente para a pesquisa:", .F., .F., 0)

        IF(EMPTY(lcDataFind))
            lcDataFind = '2000.01.01'
        ENDIF

        TEXT TO lcsql TEXTMERGE NOSHOW
            select ext_sinave.token as stamp, CONVERT(varchar, regCodOriDate, 120) as data , message as mensagem 
            from ext_sinave (nolock) 
            inner join ext_sinaveResponse(nolock) on ext_sinave.token=ext_sinaveResponse.token
            where CONVERT(varchar, regCodOriDate, 112)='<<strtran(lcDataFind,'.','')>>' and (healthUserId='<<cval1>>' or rnu=<<VAL(cval1)>>)
            order by  CONVERT(varchar, regCodOriDate, 120)  desc
        ENDTEXT
        

        uf_gerais_actgrelha("", "curencpend", lcsql)

        SELECT curencpend
        uf_valorescombo_chama("stampenc", 0, "curencpend", 2, "stamp", "data, mensagem ")
        
        IF RECCOUNT("curencpend")=0
            uf_perguntalt_chama("N�o h� registos resultantes da pesquisa","OK","",64)
            RETURN .F.
        ENDIF

    ENDIF
    
    IF !EMPTY(stampenc)
    
        TEXT TO lcSQL TEXTMERGE noshow
            select *, left(CONVERT(varchar, regCodOriDate, 108),5) as timecolheita, left(CONVERT(varchar, codValidDate, 108),5) as timevalid from ext_sinave (nolock) where token='<<ALLTRIM(stampenc)>>'
        ENDTEXT
        IF !(uf_gerais_actGrelha("","uCrsComSinave",lcSQL))
            uf_perguntalt_chama("OCORREU UM ERRO A CRIAR O CURSOR DE COMUNICA��O COM O SINAVE! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
        ENDIF
        
        TEXT TO lcSQL TEXTMERGE noshow
            select * from ext_sinave_Disease (nolock) where token in (select token from ext_sinave (nolock) where token='<<ALLTRIM(stampenc)>>')
        ENDTEXT
        IF !(uf_gerais_actGrelha("","uCrsCom_sinave_Disease",lcSQL))
            uf_perguntalt_chama("OCORREU UM ERRO A CRIAR O CURSOR DE COMUNICA��O COM O SINAVE! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
        ENDIF
        
        TEXT TO lcSQL TEXTMERGE noshow
            select * from ext_sinave_Disease_Prod (nolock) where productToken in (select producttoken from ext_sinave_Disease (nolock) where token in (select token from ext_sinave (nolock) where token='<<ALLTRIM(stampenc)>>'))
        ENDTEXT
        IF !(uf_gerais_actGrelha("","uCrsCom_sinave_Disease_Prod",lcSQL))
            uf_perguntalt_chama("OCORREU UM ERRO A CRIAR O CURSOR DE COMUNICA��O COM O SINAVE! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
        ENDIF
        
        TEXT TO lcSQL TEXTMERGE noshow
            select * from ext_sinave_Disease_ProdAnalysis (nolock) where analysisToken in (select analysisToken from ext_sinave_Disease_Prod (nolock) where productToken in (select producttoken from ext_sinave_Disease (nolock) where token in (select token from ext_sinave (nolock) where token='<<ALLTRIM(stampenc)>>')))
        ENDTEXT
        IF !(uf_gerais_actGrelha("","uCrsCom_sinave_Disease_ProdAnalysis",lcSQL))
            uf_perguntalt_chama("OCORREU UM ERRO A CRIAR O CURSOR DE COMUNICA��O COM O SINAVE! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
        ENDIF

        
        TEXT TO lcSQL TEXTMERGE noshow
            select * from ext_sinave_Disease_ProdAnalysisTech (nolock) where techToken in (select techToken from ext_sinave_Disease_ProdAnalysis (nolock) where analysisToken in (select analysisToken from ext_sinave_Disease_Prod (nolock) where productToken in (select producttoken from ext_sinave_Disease (nolock) where token in (select token from ext_sinave (nolock) where token='<<ALLTRIM(stampenc)>>'))))
        ENDTEXT
        IF !(uf_gerais_actGrelha("","uCrsCom_ext_sinave_Disease_ProdAnalysisTech",lcSQL))
            uf_perguntalt_chama("OCORREU UM ERRO A CRIAR O CURSOR DE COMUNICA��O COM O SINAVE! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
        ENDIF
        
        TEXT TO lcSQL TEXTMERGE noshow
            select * from ext_sinaveResponse WHERE token='<<ALLTRIM(stampenc)>>'
        ENDTEXT
        
        IF !(uf_gerais_actGrelha("","uCrsext_sinaveResponse",lcSQL))
            uf_perguntalt_chama("OCORREU UM ERRO A CRIAR O CURSOR DE COMUNICA��O COM O SINAVE! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
        ENDIF
        
        if(USED("uCrsext_sinaveResponse"))		 	
            SELECT uCrsext_sinaveResponse
            IF LEN(ALLTRIM(uCrsext_sinaveResponse.regsinave))=19
            
                IF uf_gerais_getParameter_site('ADM0000000143', 'BOOL', mySite)
                    comunicasinave.menu1.estado("rnu", "HIDE")
                ENDIF
                comunicasinave.menu1.estado("comunicar, edit", "HIDE")
                comunicasinave.menu1.estado("imprimir", "SHOW")
                lcSinaveReadonly = .t.
                uf_comunicasinave_readonly(.T.)
            ELSE

                comunicasinave.menu1.estado("comunicar, edit, imprimir", "SHOW")
                uf_comunicasinave_readonly(.T.)

            ENDIF 
        ENDIF
            
        comunicasinave.refresh
    ELSE

        comunicasinave.menu1.estado("comunicar, edit", "HIDE")
        comunicasinave.menu1.estado("gravar", "SHOW")
        uf_comunicasinave_readonly(.F.)

    ENDIF 	 
		
ENDFUNC 

FUNCTION uf_comunicasinave_delete
    LPARAMETERS uv_tokenSinave

	IF EMPTY(uv_tokenSinave)
		RETURN .F.
	ENDIF

	IF uf_gerais_getUmValor("ext_sinaveResponse", "count(*)", "token = '" + ALLTRIM(uv_tokenSinave) + "' and send = 1 and LEN(LTRIM(regSinave)) = 19") = 1
		uf_perguntalt_chama("N�o pode eliminar um registo j� foi comunicado ao Sinave!","OK","",16)
		RETURN .F.
	ENDIF

	TEXT TO lcSql TEXTMERGE NOSHOW

		DELETE FROM ext_sinave_Disease_ProdAnalysisTech where techToken in (select techToken FROM ext_sinave_Disease_ProdAnalysis(nolock) where  analysisToken in (select analysisToken FROM ext_sinave_Disease_Prod(nolock) where productToken in (SELECT productToken FROM ext_sinave_Disease(nolock) where token = '<<ALLTRIM(uv_tokenSinave)>>')))
		DELETE FROM ext_sinave_Disease_ProdAnalysis where analysisToken in (select analysisToken FROM ext_sinave_Disease_Prod(nolock) where productToken in (SELECT productToken FROM ext_sinave_Disease(nolock) where token = '<<ALLTRIM(uv_tokenSinave)>>'))
		DELETE FROM ext_sinave_Disease_Prod where productToken in (SELECT productToken FROM ext_sinave_Disease(nolock) where token = '<<ALLTRIM(uv_tokenSinave)>>')
		DELETE FROM ext_sinave_Disease where token = '<<ALLTRIM(uv_tokenSinave)>>'
		DELETE FROM ext_sinave where token = '<<ALLTRIM(uv_tokenSinave)>>'
        DELETE FROM <<ALLTRIM(uf_gerais_getParameter_site("ADM0000000090", "TEXT", mySite))>>.dbo.anexosMCDT WHERE tokenSinave = '<<ALLTRIM(uv_tokenSinave)>>'

	ENDTEXT

	IF !uf_gerais_actGrelha("", "", lcSQL)
		uf_perguntalt_chama("Erro a eliminar registo de Sinave!","OK","",16)
		RETURN .F.
	ENDIF

	RETURN .T.

ENDFUNC

FUNCTION uf_comunicasinave_comunicar

    
	
	IF !USED("uCrsComSinave")
		uf_perguntalt_chama("Tem de gravar o registo antes de poder comunicar.","OK","",16)
		RETURN .F.
	ENDIF

	uv_tokenSinave = uCrsComSinave.token

	IF EMPTY(uv_tokenSinave)
		uf_perguntalt_chama("Tem de gravar o registo antes de poder comunicar.","OK","",16)
		RETURN .F.
	ENDIF

    IF EMPTY(uf_gerais_getUmValor("ext_sinave","codSinave", "token = '" + ALLTRIM(uv_tokenSinave) + "'"))
        uf_perguntalt_chama("Por favor, preencha o c�digo do sinave.","OK","",16)
		RETURN .F.
    ENDIF

	IF uf_gerais_getUmValor("ext_sinaveResponse", "count(*)", "token = '" + ALLTRIM(uv_tokenSinave) + "' and send = 1 and LEN(LTRIM(regSinave)) = 19") = 1
		uf_perguntalt_chama("Este registo j� foi comunicado!","OK","",16)
		RETURN .F.
	ENDIF

	IF uf_gerais_getUmValor("ext_sinave", "count(*)", "token = '" + ALLTRIM(uv_tokenSinave) + "'") = 0
		uf_perguntalt_chama("Tem de gravar o registo antes de poder comunicar.","OK","",16)
		RETURN .F.
	ENDIF

	regua(1,2,"A comunicar com o Sinave...")
	
	LOCAL result,lctestjar ,lcwspath,lcadd, resultjava
	STORE '&' TO lcadd
	
	lcnomejar = "LTSSINAVE.jar"
	lcwspath=ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT") + '\Sinave\' + ALLTRIM(lcNomeJar))
	
	IF !FILE(lcwspath)
		uf_perguntalt_chama("O SOFTWARE DE ENVIO LTS ESB CLI N�O EST� CONFIGURADO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	ENDIF 
 	 lcwsdir = ALLTRIM(uf_gerais_getparameter("ADM0000000185", "TEXT"))+'\Sinave'
     lcwspath= "cmd /c cd /d "+lcwsdir+" "+lcadd+lcadd+" java -jar "+ lcwspath + ' "--IDCL='+ALLTRIM(UPPER(sql_db))+'"' + ' "--TOKEN='+ALLTRIM(uv_tokenSinave)+'"' +' ""'
	 **_CLIPTEXT = lcwspath
	** MESSAGEBOX(lcwspath)
	 
	 oWSShell = CREATEOBJECT("WScript.Shell")
	 oWSShell.Run(lcWsPath, 0, .t.)
	 
	lcSQL = ''
	TEXT TO lcSQL TEXTMERGE NOSHOW 
		select * from ext_sinaveResponse(nolock) where token='<<ALLTRIM(uv_tokenSinave)>>'
	ENDTEXT		
	IF !uf_gerais_actGrelha("","uCrsAuxRespostaSinave",lcSQL)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VERIFICAR A RESPOSTA DO SINAVE. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	ENDIF 	

	
	IF uCrsAuxRespostaSinave.send = 1 AND uCrsAuxRespostaSinave.messageCode= 20160000

        SELECT uCrsext_sinaveResponse
        REPLACE uCrsext_sinaveResponse.regD WITH uCrsAuxRespostaSinave.regD

		**Atendimento.pgfDados.page3.btnReceita.label1.caption = ALLTRIM(uCrsAuxRespostaSinave.regsinave)
		**Atendimento.pgfDados.page3.btnReceita.label1.config(ALLTRIM(uCrsAuxRespostaSinave.regsinave))
	
		IF TYPE("myChamaSinave")!="C"
			RELEASE myChamaSinave
	   		PUBLIC myChamaSinave
	   		STORE 'N�o chama' TO myChamaSinave
		ENDIF 

		
		IF  uf_comunicaSinave_validaCompart() 
			myNrRecSinave = ALLTRIM(uCrsAuxRespostaSinave.regsinave)
		ENDIF 

		comunicasinave.menu1.estado("imprimir", "SHOW")
		comunicasinave.menu1.estado("comunicar, pesquisar, edit", "HIDE")
		lcSinaveReadonly = .t.
		uf_comunicasinave_readonly(.T.)

        uf_perguntalt_chama("COMUNICADO COM SUCESSO.", "OK", "", 32)

	ELSE
		uf_perguntalt_chama("A INFORMA��O ENVIADA N�O FOI VALIDADA COM SUCESSO." + chr(13) + ALLTRIM(uCrsAuxRespostaSinave.message),"OK","",16)
	ENDIF 

	regua(2)

	RETURN .T.

ENDFUNC

PROCEDURE uf_comunicasinave_editar

    uv_tokenSinave = ''

    IF USED("uCrsComSinave")
        uv_tokenSinave = uCrsComSinave.token
    ENDIF

    IF EMPTY(uv_tokenSinave)
        RETURN .F.
    ENDIF

    IF uf_gerais_getUmValor("ext_sinaveResponse", "count(*)", "token = '" + ALLTRIM(uv_tokenSinave) + "' and send = 1 and LEN(LTRIM(regSinave)) = 19") = 1
		uf_perguntalt_chama("Este registo j� foi comunicado!","OK","",16)
		RETURN .F.
	ENDIF

    uf_comunicasinave_readonly(.F.)
    comunicasinave.menu1.estado("gravar", "SHOW")
    comunicasinave.menu1.estado("edit, comunicar, pesquisar, imprimir", "HIDE")

ENDPROC