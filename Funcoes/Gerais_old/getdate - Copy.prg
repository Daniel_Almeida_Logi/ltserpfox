**
** Parametros:
** Data:
**		Vazio ou falso assume data actual, formato: date()
** Objecto:
** 		Deve ser introduzido o nome da vari�vel publica/objecto/cursor, que se pretende que a data seja gravada
** Tipo:
**		0 - vari�vel, 1 - objecto, 2 - cursor
**
** Formato:
**		0 - data
**		1 - Mes
**		2 - Ano
**
FUNCTION uf_getDate_chama
	LPARAMETERS lcdata, lcObj, lcTipo, lcFuncao, lcDataVitalicio, lcModal, lcFormato
	
	** caso o parametro venha vazio coloca data actual
	IF EMPTY(lcData)
		lcData = datetime()+(difhoraria*3600)
		**lcData = DATE()
	ENDIF
	
	*** Abre o ecr� caso n�o esteja aberto
	IF TYPE("GETDATE")=="U"
		DO FORM GETDATE WITH lcdata, lcObj, lcTipo, lcFuncao, lcDataVitalicio, lcModal, lcFormato

	ELSE
		GETDATE.show
	ENDIF
ENDFUNC


**
FUNCTION uf_getdate_EscolheMes
	LPARAMETERS lcBotao

	FOR i = 1 TO 12 &&Mes
		lcObj = "getdate.btn" + ALLTRIM(STR(i))
		&lcObj..backcolor = IIF(VAL(STREXTRACT(lcBotao, "getdate.btn","")) = i, RGB(0,196,0), RGB(225,225,225))		
	ENDFOR

	GETDATE.MES = VAL(STREXTRACT(lcBotao, "getdate.btn", ""))
	
	** Caso se mude para um mes que n�o tenha o dia selecionado aplica o ultimo dia dos mes
	IF GETDATE.DIA > DAY(GOMONTH(DATE(GETDATE.ANO, GETDATE.MES,1),+1)-1)
		GETDATE.DIA = DAY(GOMONTH(DATE(GETDATE.ANO, GETDATE.MES,1),+1)-1)
	ENDIF 
	
	uf_getdate_PreencheCalendario()
ENDFUNC

	
**
FUNCTION uf_getdate_PreencheCalendario
	LOCAL dataMarc, lcAnoActual, lcMesActual, lcPosDia, lcPosDia1, lcPosDia2, lcPosDia3, lcUltimoDiaMesAnterior, lcUltimoDia 
	STORE 0 TO lcAnoActual, lcMesActual, lcPosDia, lcPosDia1, lcPosDia2, lcPosDia3, lcUltimoDiaMesAnterior, lcUltimoDia

	dataMarc = DATE(GETDATE.ANO, GETDATE.MES, 1)
	

	** Marca o Primeiro dia do Mes
	DO CASE 
		*2a
		CASE UPPER(CDOW(GOMONTH(dataMarc, IIF(DAY(dataMarc)=1,1,0)) - DAY(GOMONTH(dataMarc, IIF(DAY(dataMarc)=1,1,0))-1))) == UPPER("Monday")
			GETDATE.d8.label1.caption = "1"
			lcPosDia = 8
			*GETDATE.d8.enabled = .t.
			GETDATE.d8.backstyle 			= 1
			getdate.d8.label1.forecolor 	= RGB(0,0,0)

		*3a
		CASE UPPER(CDOW(GOMONTH(dataMarc, IIF(DAY(dataMarc)=1,1,0)) - DAY(GOMONTH(dataMarc, IIF(DAY(dataMarc)=1,1,0))-1))) == UPPER("Tuesday")
			GETDATE.d2.label1.caption = "1"
			lcPosDia = 2
			GETDATE.d2.backstyle 			= 1
			getdate.d2.label1.forecolor 	= RGB(0,0,0)

		*4a
		CASE UPPER(CDOW(GOMONTH(dataMarc, IIF(DAY(dataMarc)=1,1,0)) - DAY(GOMONTH(dataMarc, IIF(DAY(dataMarc)=1,1,0))-1))) == UPPER("Wednesday")
			GETDATE.d3.label1.caption = "1"
			lcPosDia = 3
			GETDATE.d3.backstyle 			= 1
			getdate.d3.label1.forecolor 	= RGB(0,0,0)

		*5a
		CASE UPPER(CDOW(GOMONTH(dataMarc, IIF(DAY(dataMarc)=1,1,0)) - DAY(GOMONTH(dataMarc, IIF(DAY(dataMarc)=1,1,0))-1))) == UPPER("Thursday")
			GETDATE.d4.label1.caption = "1"
			lcPosDia = 4
			GETDATE.d4.backstyle 			= 1
			getdate.d4.label1.forecolor 	= RGB(0,0,0)

		*6a
		CASE UPPER(CDOW(GOMONTH(dataMarc, IIF(DAY(dataMarc)=1,1,0)) - DAY(GOMONTH(dataMarc, IIF(DAY(dataMarc)=1,1,0))-1))) == UPPER("Friday")
			GETDATE.d5.label1.caption = "1"
			lcPosDia = 5
			GETDATE.d5.backstyle 			= 1
			getdate.d5.label1.forecolor 	= RGB(0,0,0)

		*Sabado
		CASE UPPER(CDOW(GOMONTH(dataMarc, IIF(DAY(dataMarc)=1,1,0)) - DAY(GOMONTH(dataMarc, IIF(DAY(dataMarc)=1,1,0))-1))) == UPPER("Saturday")
			GETDATE.d6.label1.caption = "1"
			lcPosDia = 6
			GETDATE.d6.backstyle 			= 1
			getdate.d6.label1.forecolor 	= RGB(0,0,0)

		*Domingo
		CASE UPPER(CDOW(GOMONTH(dataMarc, IIF(DAY(dataMarc)=1,1,0)) - DAY(GOMONTH(dataMarc, IIF(DAY(dataMarc)=1,1,0))-1))) == UPPER("Sunday")
			GETDATE.d7.label1.caption = "1"		
			lcPosDia = 7
			GETDATE.d7.backstyle 			= 1
			getdate.d7.label1.forecolor 	= RGB(0,0,0)
			
		OTHERWISE
			**
	ENDCASE

	** Calcula o ultimo dia do mes
	LOCAL lnShift, ldShift
	lnShift = 35 - DAY(dataMarc) && enough days to move us into the next month	
	ldShift = dataMarc + lnShift
	lcUltimoDia = DAY(ldShift - DAY(ldShift))
	*********************

	lcPosDia1 = lcPosDia  
	** Preeche caption para o mes actual
	FOR i = 2 TO lcUltimoDia 

		lcPosDia1 = lcPosDia1 + 1
		lcObj = "GETDATE.d" + ALLTRIM(STR(lcPosDia1))
		&lcObj..label1.caption 		= ALLTRIM(STR(i))
		*&lcObj..enabled 			= .t.
		&lcObj..backstyle			= 1
		&lcObj..label1.forecolor 	= RGB(0, 0, 0)
	ENDFOR
	******************************************************

	** Preenche dias Do M�s Anterior
	** Calcula o ultimo dia do mes anterior
	LOCAL lnShift, ldShift
	lnShift = 35-DAY(GOMONTH(dataMarc, -1)) && enough days to move us into the next month	
	ldShift = GOMONTH(dataMarc, -1) + lnShift
	lcUltimoDiaMesAnterior = DAY(ldShift - DAY(ldShift))
	*********************

	lcPosDia2 = lcPosDia - 1
	
	DO WHILE lcPosDia2 > 0
	
		lcObj = "GETDATE.d" + ALLTRIM(STR(lcPosDia2))
		&lcObj..label1.caption 		= ALLTRIM(STR(lcUltimoDiaMesAnterior))
		&lcObj..backColor			= RGB(234,234,234)
		&lcObj..backstyle 			= 0
		&lcObj..label1.forecolor 	= RGB(206, 206, 206)
		lcPosDia2 					= lcPosDia2 - 1
		lcUltimoDiaMesAnterior 		= lcUltimoDiaMesAnterior - 1

	ENDDO
	*****************************************************

	** Preenche dias Do M�s Seguinte ********************
	lcPosDia3 = lcPosDia + lcUltimoDia 
	i = 1
	DO WHILE lcPosDia3 <= 42
		lcObj = "GETDATE.d" + ALLTRIM(STR(lcPosDia3))
		&lcObj..label1.caption 		= 	ALLTRIM(STR(i))
		&lcObj..backColor			=	RGB(234,234,234)
		&lcObj..backstyle 			= 0
		&lcObj..label1.forecolor 	= RGB(206, 206, 206)
		lcPosDia3 = lcPosDia3 + 1
		i= i + 1
	ENDDO
	*****************************************************
	
	uf_getdate_aplicaCores()
ENDFUNC


** 
FUNCTION uf_getdate_anoMais
	GETDATE.ANO 					= GETDATE.ANO + 1
	GETDATE.cntAno.label1.caption 	= ALLTRIM(STR(GETDATE.ANO))
	
	uf_getdate_PreencheCalendario()
ENDFUNC


** 
FUNCTION uf_getdate_anoMenos
	GETDATE.ANO 					= GETDATE.ANO - 1
	GETDATE.cntAno.label1.caption 	= ALLTRIM(STR(GETDATE.ANO))
	
	uf_getdate_PreencheCalendario()
ENDFUNC


** 
FUNCTION uf_getdate_aplicaCores
	** Coloca Cor original nos botoes
	FOR i = 1 TO 42
		lcObj = "GETDATE.d" + ALLTRIM(STR(i))
		**Fim de semana
		IF i == 6 OR i == 7 OR i == 13 OR i == 14 OR i == 20 OR i == 21 OR i == 27 OR i == 28 OR i == 34 OR i == 35 OR i == 41 OR i == 42
			&lcObj..backColor = RGB(255,128,64)
		ELSE
			&lcObj..backColor = RGB(255,255,159)
		ENDIF
		
		IF &lcObj..label1.caption == ALLTRIM(STR(GETDATE.DIA))
			&lcObj..backColor = RGB(0,196,0)
			&lcObj..setFocus
		ENDIF
	ENDFOR
ENDFUNC


** 
FUNCTION uf_getdate_escolheDia
	LPARAMETERS lcDia
	
	LOCAL lcObj
	lcobj = "GETDATE."+ lcDia
	
	IF &lcObj..backstyle == 0
		RETURN .f.
	ENDIF
	
	GETDATE.DIA = VAL(&lcObj..label1.caption)
	
	uf_getdate_aplicaCores()
ENDFUNC


**
FUNCTION uf_getDate_CarregaMenu
	GETDATE.menu1.adicionaopcao("datavazia", "Vazia", myPath + "\imagens\icons\vazio_w.png", "uf_getdate_aplicaDataVazia","V")
	IF getdate.DataVitalicio == .t.
		GETDATE.menu1.adicionaopcao("datavitalicia", "CC Vitalicio", myPath + "\imagens\icons\vazio_w.png", "uf_getdate_aplicaDataVitalicia","V")
	ENDIF
	IF TYPE("INFOEMBAL") != "U"
		GETDATE.menu1.adicionaopcao("semdia", "Sem Dia", myPath + "\imagens\icons\semdia_w.png", "uf_getdate_aplicaSemDia","S")
	ENDIF 
	GETDATE.autocenter = .t.
	GETDATE.menu1.estado("", "", "APLICAR", .t., "SAIR", .t.)
	GETDATE.refresh
ENDFUNC


**
FUNCTION uf_getdate_aplicaDataVazia
	GETDATE.ANO = 1900
	GETDATE.MES = 1
	GETDATE.DIA = 1
	
	uf_getdate_gravar()
ENDFUNC


**
FUNCTION uf_getdate_aplicaDataVitalicia
	GETDATE.ANO = 3000
	GETDATE.MES = 1
	GETDATE.DIA = 1
	
	uf_getdate_gravar()
ENDFUNC

** 
FUNCTION uf_getdate_gravar
	LOCAL lcObj, lcdate
	
	lcObj = GETDATE.OBJ
	
	IF !EMPTY(GETDATE.formatoData)
		lcdate = uf_gerais_getdate(DATE(GETDATE.ANO, GETDATE.MES, GETDATE.DIA),GETDATE.formatoData)
	ELSE
		lcdate = uf_gerais_getdate(DATE(GETDATE.ANO, GETDATE.MES, GETDATE.DIA))
	ENDIF
	
	
	TRY && o try � necess�rio porque o painel n�o � modal...
		DO CASE
			CASE GETDATE.Tipo = 0 &&Variavel
		 		TRY
			 		&lcObj =  lcdate
			 	CATCH
			 		uf_getdate_sair()
			 	ENDTRY
		 		
			CASE GETDATE.Tipo = 1 && objecto
				TRY
					&lcObj..value = lcdate
				CATCH
			 		uf_getdate_sair()
			 	ENDTRY
				
			CASE GETDATE.Tipo = 2 && cursor
				TRY
					lcCursor = STREXTRACT(lcObj,"",".")
					SELECT &lcCursor

					IF TYPE('&lcObj') == "C"
						Replace &lcObj WITH lcdate							
					ELSE			
						Replace &lcObj WITH DATE(GETDATE.ANO, GETDATE.MES, GETDATE.DIA)								
					ENDIF
				CATCH
			 		uf_getdate_sair()
			 	ENDTRY
			CASE GETDATE.Tipo = 3 && cursor e formato trocado
				TRY
					lcCursor = STREXTRACT(lcObj,"",".")
					SELECT &lcCursor

					IF TYPE('&lcObj') == "C"
						IF LEN(ALLTRIM(STR(GETDATE.MES))) == 1
							lcMes = "0"+ALLTRIM(STR(GETDATE.MES))
						ELSE
							lcMes = ALLTRIM(STR(GETDATE.MES))
						ENDIF
					
						Replace &lcObj With lcMes + "." + ALLTRIM(STR(GETDATE.ANO))
					ELSE
						Replace &lcObj With DATE(GETDATE.ANO, GETDATE.MES, GETDATE.DIA)
					ENDIF	
				CATCH
			 		uf_getdate_sair()
			 	ENDTRY	
			OTHERWISE
				uf_getdate_sair()
		ENDCASE

		IF !EMPTY(GETDATE.funcao)
			lcFuncao = GETDATE.funcao
			do &lcFuncao 
		ENDIF
	CATCH
	ENDTRY
	
	&& fix para atualizar a idade no atendimento 
	IF ALLTRIM(UPPER(lcObj)) == "UCRSATENDCL.NASCIMENTO" OR ALLTRIM(UPPER(lcObj)) == "UCRSATENDCL.U_DCUTAVI"
		uf_atendimento_calcIdade()
	ENDIF

	&& fix para atualizar a idade no atendimento	
	IF ALLTRIM(UPPER(lcObj)) == "FT.NASCIMENTO"
		uf_faturacao_calcidade()
	ENDIF
	
	uf_getdate_sair()
ENDFUNC


** 
FUNCTION uf_getdate_sair
	IF TYPE("GETDATE") != "U"
		TRY
			GETDATE.hide
			GETDATE.release
		CATCH
		ENDTRY
	ENDIF
ENDFUNC


FUNCTION uf_getdate_aplicaSemDia
	LOCAL lcObj, lcdate
	
	lcObj = GETDATE.OBJ
	
	**lcdate = uf_gerais_getdate(DATE(GETDATE.ANO, GETDATE.MES, GETDATE.DIA))
	lcdate = GOMONTH(DATE(GETDATE.ANO, GETDATE.MES,1),+1)-1
	TRY && o try � necess�rio porque o painel n�o � modal...
		Replace &lcObj WITH lcdate 
*!*			DO CASE
*!*				CASE GETDATE.Tipo = 0 &&Variavel
*!*			 		TRY
*!*				 		&lcObj =  lcdate
*!*				 	CATCH
*!*				 		uf_getdate_sair()
*!*				 	ENDTRY
*!*			 		
*!*				CASE GETDATE.Tipo = 1 && objecto
*!*					TRY
*!*						&lcObj..value = lcdate
*!*					CATCH
*!*				 		uf_getdate_sair()
*!*				 	ENDTRY
*!*					
*!*				CASE GETDATE.Tipo = 2 && cursor
*!*					TRY
*!*						lcCursor = STREXTRACT(lcObj,"",".")
*!*						SELECT &lcCursor

*!*						IF TYPE('&lcObj') == "C"
*!*							Replace &lcObj WITH lcdate							
*!*						ELSE			
*!*							Replace &lcObj WITH DATE(GETDATE.ANO, GETDATE.MES, GETDATE.DIA)								
*!*						ENDIF
*!*					CATCH
*!*				 		uf_getdate_sair()
*!*				 	ENDTRY
*!*				CASE GETDATE.Tipo = 3 && cursor e formato trocado
*!*					TRY
*!*						lcCursor = STREXTRACT(lcObj,"",".")
*!*						SELECT &lcCursor

*!*						IF TYPE('&lcObj') == "C"
*!*							IF LEN(ALLTRIM(STR(GETDATE.MES))) == 1
*!*								lcMes = "0"+ALLTRIM(STR(GETDATE.MES))
*!*							ELSE
*!*								lcMes = ALLTRIM(STR(GETDATE.MES))
*!*							ENDIF
*!*						
*!*							Replace &lcObj With lcMes + "." + ALLTRIM(STR(GETDATE.ANO))
*!*						ELSE
*!*							Replace &lcObj With DATE(GETDATE.ANO, GETDATE.MES, GETDATE.DIA)
*!*						ENDIF	
*!*					CATCH
*!*				 		uf_getdate_sair()
*!*				 	ENDTRY	
*!*				OTHERWISE
*!*					uf_getdate_sair()
*!*		ENDCASE

		IF !EMPTY(GETDATE.funcao)
			lcFuncao = GETDATE.funcao
			do &lcFuncao 
		ENDIF
	CATCH
	ENDTRY
	
	uf_getdate_sair()
ENDFUNC 