FUNCTION uf_vacinacao_chama
	LPARAMETERS lcNO, uv_modal, uv_applyPlan
	
	PUBLIC myCallVaccineService, myCallVaccineCallendarService
	STORE .f. TO myCallVaccineService, myCallVaccineCallendarService
	
	IF EMPTY(lcNO)
		lcNO = ""
	ENDIF
	
	&& Controla abertura do painel
	IF TYPE("vacinacao")=="U"

		uf_vacinacao_criaCur(lcNO)

		DO FORM Vacinacao WITH uv_modal, uv_applyPlan
	ELSE
		vacinacao.show

      IF TYPE("myVacIntroducao") <> "U"
         IF myVacIntroducao

            SELECT ucrsuser

	         SELECT uCrsVacinacao
	         REPLACE uCrsVacinacao.administrante WITH ALLTRIM(ucrsuser.nome)

            VACINACAO.refresh()

         ENDIF
      ENDIF

	ENDIF 

	IF TYPE("VACINACAO")<>"U"
		VACINACAO.txtMotiBreak.tag = ASTR(uCrsVacinacao.motiveId)
		VACINACAO.txtDestinoBreak.tag = ASTR(uCrsVacinacao.destinyId)
	ENDIF
	
ENDFUNC

FUNCTION uf_vacinacao_criaCur
	LPARAMETERS uv_no

	LOCAL uv_filtro 
	STORE '' TO uv_filtro

	IF !EMPTY(uv_no)
		uv_filtro = uv_no
	ENDIF

	LOCAL lcSQL
	STORE "" TO lcSQL
	
	TEXT TO lcSQL TEXTMERGE noshow
		EXEC up_vacinacao_getVacinacao '<<uv_filtro>>'
	ENDTEXT
	
	IF !(uf_gerais_actGrelha("","uCrsVacinacao",lcSQL))
		uf_perguntalt_chama("OCORREU UM ERRO A CRIAR O CURSOR DE VACINA��O! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RELEASE myCallVaccineService
		RELEASE myCallVaccineCallendarService
	ENDIF

	IF TYPE("VACINACAO")<>"U"
		VACINACAO.txtMotiBreak.tag = ASTR(uCrsVacinacao.motiveId)
		VACINACAO.txtDestinoBreak.tag = ASTR(uCrsVacinacao.destinyId)

		VACINACAO.refresh()

	ENDIF

	RETURN .T.
ENDFUNC

*********************************
*		CONFIGURA O PAINEL		*
*********************************
FUNCTION uf_Vacinacao_confEcra
	LPARAMETERS tcBool
	
	IF TYPE("VACINACAO") == "U"
		RETURN .F.
	ENDIF

	IF tcBool=1	
		vacinacao.menu1.estado("pesquisar,ultimo,novo,editar,eliminar, historico","SHOW", "Gravar", .F., "Sair", .T.)
		IF uf_gerais_getParameter_site('ADM0000000143', 'BOOL', mySite)
        	vacinacao.menu1.estado("sinc_RNUNovo, contraindicacao","HIDE", "Gravar", .F.)
		ENDIF

		IF TYPE("vacinacao.menu1.selReg") <> "U"
			vacinacao.menu1.estado("selReg","SHOW", "Gravar", .F., "Sair", .T.)
		ENDIF

		SELECT ucrsVacinacao
		GO TOP

		IF ucrsVacinacao.comunicado AND uf_gerais_compStr(ucrsVacinacao.tipoReg, "E-boletim")
			IF !ucrsVacinacao.anulado
				vacinacao.menu1.estado("anular","SHOW", "Gravar", .F.)
				vacinacao.menu1.estado("motivosAnulado","HIDE", "Gravar", .F.)
			ELSE
				vacinacao.menu1.estado("anular","HIDE", "Gravar", .F.)
				vacinacao.menu1.estado("motivosAnulado","SHOW", "Gravar", .F.)
			ENDIF
		ELSE
			vacinacao.menu1.estado("anular, motivosAnulado","HIDE", "Gravar", .F.)
		ENDIF

      	vacinacao.menu1.sair.image1.picture = mypath+"\imagens\icons\sair_w.png"
		
		vacinacao.Caption = "Registo de Vacina��o"
		
		&&Dados Painel
		Vacinacao.txtNome.readonly				=	.t.
		Vacinacao.txtDataNasc.readonly			=	.t.
		Vacinacao.txtContacto.readonly			=	.t.
		**Vacinacao.txtAdministrante.readonly		=	.t.
		Vacinacao.txtAdministrante.DisabledBackColor = RGB(254,254,254)
		Vacinacao.txtDataAdministracao.readonly	=	.t.
		Vacinacao.txtDataAdministracao.DisabledBackColor = RGB(254,254,254)
		Vacinacao.txtDescricao.readonly			=	.t.
		Vacinacao.txtLote.readonly				=	.t.
		Vacinacao.txtNrVenda.readonly			=	.t.
		Vacinacao.txtObs.readonly				=	.t.
		Vacinacao.txtSNS.readonly				=	.t.
		Vacinacao.txtUnidades.readonly			=	.t.
		Vacinacao.txtDosagem.readonly			=	.t.
		Vacinacao.txtComercial_id.readonly		=	.t.
		Vacinacao.txtComercial_id.DisabledBackColor = RGB(254,254,254)
        Vacinacao.chkAutResidencia.enabled		=	.f.
        Vacinacao.chkAutResidencia.image1.enabled =	.f.
		Vacinacao.txtTipoVacinacao.readonly		=	.t.
		Vacinacao.txtTipoUtente.readonly		= 	.t.
		Vacinacao.txtLocalAdmistracao.readonly	=	.t.
		Vacinacao.txtnumInoculacao.readonly	=	.t.
		Vacinacao.txtLocalAnatomico.readonly	=	.t.
		Vacinacao.txtlateralidade.readonly		=	.t.
		Vacinacao.txtTipoUtente.enabled		=	.f.
		Vacinacao.txtTipoVacinacao.enabled	=	.f.
		Vacinacao.txtViaAdmin.enabled		=	.f.
		Vacinacao.txtLocalAnatomico.enabled	=	.f.
		Vacinacao.txtLocalAdmistracao.enabled =	.f.
		Vacinacao.txtlateralidade.enabled =	.f.
		Vacinacao.txtValidade.enabled = .F.

		Vacinacao.chkInjetavel.image1.enabled = .F.
        Vacinacao.chkVacinacao.image1.enabled = .F.
		Vacinacao.chkBreak.image1.enabled = .F.
		Vacinacao.txtMotiBreak.enabled = .F.
		Vacinacao.txtDestinoBreak.enabled = .F.

		Vacinacao.btnPesqCliente.enable(.f.)
		**Vacinacao.btnPesqAdministrante.enable(.f.)
		Vacinacao.btnPesqVacina.enable(.f.)
		Vacinacao.btnPesqVenda.enable(.f.)
		
		myVacAlteracao = .f.
		myVacIntroducao = .f.
	ELSE
		vacinacao.menu1.estado("pesquisar,ultimo,novo,editar,eliminar, anular, motivosAnulado","HIDE", "Gravar", .T., "Cancelar", .T.)
		IF uf_gerais_getParameter_site('ADM0000000143', 'BOOL', mySite)
        	vacinacao.menu1.estado("sinc_RNUNovo, historico,contraindicacao","SHOW", "Gravar", .t.)
		ENDIF

		IF TYPE("vacinacao.menu1.selReg") <> "U"
			vacinacao.menu1.estado("selReg","HIDE", "Gravar", .T., "Cancelar", .T.)
		ENDIF

        vacinacao.menu1.sair.image1.picture = mypath+"\imagens\icons\cancelar_w.png"
		
		IF myVacIntroducao
			vacinacao.Caption = "Registo de Vacina��o - Modo de Introdu��o"
		ELSE
			vacinacao.Caption="Registo de Vacina��o - Modo de Altera��o"
		ENDIF


		Vacinacao.setALL("DisabledBackColor", RGB(255,255,255), "Textbox")
		Vacinacao.setALL("BackColor", RGB(255,255,255), "Textbox")

		Vacinacao.setALL("DisabledForeColor", RGB(0,0,0), "Textbox")
		Vacinacao.setALL("ForeColor", RGB(0,0,0), "Textbox")
		
		&&Dados Painel
		IF uf_gerais_compStr(VACINACAO.chkBreak.tag, "false")

			Vacinacao.txtNome.readonly				=	.f.
			Vacinacao.txtDataNasc.readonly			=	.f.
			Vacinacao.txtDataNasc.enabled			=	.T.
			Vacinacao.txtContacto.readonly			=	.f.
			**Vacinacao.txtAdministrante.readonly		=	.f.
			Vacinacao.txtAdministrante.DisabledBackColor = RGB(210,210,210)
			Vacinacao.txtDataAdministracao.readonly	=	.f.
			Vacinacao.txtDataAdministracao.enabled	=	.T.
			Vacinacao.txtDescricao.readonly			=	.f.
			Vacinacao.txtLote.readonly				=	.f.
			Vacinacao.txtNrVenda.readonly			=	.f.
			Vacinacao.txtObs.readonly				=	.f.
			Vacinacao.txtSNS.readonly				=	.f.
			Vacinacao.txtViaAdmin.readonly			=	.t.
			Vacinacao.txtUnidades.readonly			=	.f.
			Vacinacao.txtDosagem.readonly			=	.f.
			Vacinacao.chkAutResidencia.enabled		=	.t.
			Vacinacao.chkAutResidencia.image1.enabled		=	.t.
			Vacinacao.txtTipoVacinacao.readonly		=	.t.
			Vacinacao.txtTipoUtente.readonly		= 	.t.
			Vacinacao.txtLocalAdmistracao.readonly	=	.t.
			Vacinacao.txtnumInoculacao.readonly	=	.f.
			Vacinacao.txtLocalAnatomico.readonly	=	.t.
			Vacinacao.txtlateralidade.readonly		=	.t.
			Vacinacao.txtValidade.enabled = .T.

			Vacinacao.txtMotiBreak.enabled = .F.
			Vacinacao.txtMotiBreak.DisabledBackColor = RGB(210,210,210)
			Vacinacao.txtMotiBreak.value = ''
			Vacinacao.txtMotiBreak.tag = ''

			Vacinacao.txtDestinoBreak.enabled = .F.
			Vacinacao.txtDestinoBreak.DisabledBackColor = RGB(210,210,210)
			Vacinacao.txtDestinoBreak.value = ''
			Vacinacao.txtDestinoBreak.tag = ''

			Vacinacao.txtTipoUtente.enabled		=	.T.
			Vacinacao.txtTipoVacinacao.enabled	=	.T.
			Vacinacao.txtViaAdmin.enabled		=	.T.
			Vacinacao.txtLocalAnatomico.enabled	=	.T.
			Vacinacao.txtLocalAdmistracao.enabled =	.T.
			Vacinacao.txtlateralidade.enabled =	.T.

			Vacinacao.btnPesqCliente.enable(.t.)
			**Vacinacao.btnPesqAdministrante.enable(.t.)
			Vacinacao.btnPesqVacina.enable(.t.)
			Vacinacao.btnPesqVenda.enable(.t.)

			Vacinacao.txtComercial_id.readonly				=	.F.
			Vacinacao.txtComercial_id.DisabledBackColor = RGB(254,254,254)

			IF myVacIntroducao

				SELECT ucrsuser
				
				SELECT uCrsVacinacao
				REPLACE uCrsVacinacao.administrante WITH ALLTRIM(ucrsuser.nome)

			ENDIF

		  	IF uf_gerais_compStr(VACINACAO.chkVacinacao.tag, "TRUE")

				IF myVacAlteracao AND (uCrsVacinacao.send AND uCrsVacinacao.result = 1)

					Vacinacao.txtComercial_id.readonly				=	.T.
					Vacinacao.txtComercial_id.DisabledBackColor = RGB(210,210,210)

					Vacinacao.txtDataAdministracao.readonly	=	.T.
					Vacinacao.txtDataAdministracao.enabled	=	.F.
					Vacinacao.txtDataAdministracao.DisabledBackColor = RGB(210,210,210)		

				ENDIF

		  	ENDIF

         	VACINACAO.refresh()

		ELSE

			SELECT ucrsVacinacao
			REPLACE ucrsVacinacao.nome WITH ''
			REPLACE ucrsVacinacao.contacto WITH ''
			REPLACE ucrsVacinacao.nrSns WITH ''
			REPLACE ucrsVacinacao.typeVacinacao WITH ''
			REPLACE ucrsVacinacao.typeVacinacaoDesc WITH ''
			REPLACE ucrsVacinacao.typeUtente WITH ''
			REPLACE ucrsVacinacao.typeUtenteDesc WITH ''
			REPLACE ucrsVacinacao.dataNasc WITH CTOT('19000101')
			REPLACE ucrsVacinacao.validade WITH CTOT('19000101')
			REPLACE ucrsVacinacao.viaAdmin_id WITH ''
			REPLACE ucrsVacinacao.viaAdminDesc WITH ''
			REPLACE ucrsVacinacao.dosagem WITH 0
			REPLACE ucrsVacinacao.numInoculacao WITH ''
			REPLACE ucrsVacinacao.lateralidade WITH ''
			REPLACE ucrsVacinacao.lateralidadeDesc WITH ''
			REPLACE uCrsVacinacao.localAnatomico_id WITH ''
			REPLACE uCrsVacinacao.localAnatomico_idDesc WITH ''
			REPLACE uCrsVacinacao.localAdmistracao WITH ''
			REPLACE uCrsVacinacao.localAdmistracaoDesc WITH ''
			REPLACE ucrsVacinacao.nrVenda WITH ''
			REPLACE ucrsVacinacao.obs WITH ''

			IF myVacIntroducao

				SELECT ucrsuser
				
				SELECT uCrsVacinacao
				REPLACE uCrsVacinacao.administrante WITH ALLTRIM(ucrsuser.nome)

			ENDIF
         

			VACINACAO.refresh()

			Vacinacao.txtNome.readonly				=	.T.
			Vacinacao.txtNome.DisabledBackColor = RGB(210,210,210)

			Vacinacao.txtDataNasc.readonly			=	.T.
			Vacinacao.txtDataNasc.enabled			=	.F.
			Vacinacao.txtDataNasc.DisabledBackColor = RGB(210,210,210)

			Vacinacao.txtContacto.readonly			=	.T.
			Vacinacao.txtContacto.DisabledBackColor = RGB(210,210,210)
			**Vacinacao.txtAdministrante.readonly		=	.T.
			Vacinacao.txtAdministrante.DisabledBackColor = RGB(210,210,210)
			Vacinacao.txtDataAdministracao.readonly	=	.T.
			Vacinacao.txtDataAdministracao.enabled	=	.F.
			Vacinacao.txtDataAdministracao.DisabledBackColor = RGB(210,210,210)
			Vacinacao.txtDescricao.readonly			=	.f.
			Vacinacao.txtLote.readonly				=	.f.
			Vacinacao.txtNrVenda.readonly			=	.T.
			Vacinacao.txtNrVenda.DisabledBackColor = RGB(210,210,210)
			Vacinacao.txtObs.readonly				=	.T.
			Vacinacao.txtObs.DisabledBackColor = RGB(210,210,210)
			Vacinacao.txtSNS.readonly				=	.T.
			Vacinacao.txtSNS.DisabledBackColor = RGB(210,210,210)
			Vacinacao.txtViaAdmin.readonly			=	.T.
			Vacinacao.txtViaAdmin.DisabledBackColor = RGB(210,210,210)
			Vacinacao.txtUnidades.readonly			=	.f.
			Vacinacao.txtDosagem.readonly			=	.T.
			Vacinacao.txtDosagem.DisabledBackColor = RGB(210,210,210)
			Vacinacao.chkAutResidencia.enabled		=	.F.
			Vacinacao.chkAutResidencia.image1.enabled	=	.F.
			Vacinacao.txtTipoVacinacao.readonly		=	.t.
			Vacinacao.txtTipoVacinacao.DisabledBackColor = RGB(210,210,210)
			Vacinacao.txtTipoUtente.readonly		= 	.t.
			Vacinacao.txtTipoUtente.DisabledBackColor = RGB(210,210,210)
			Vacinacao.txtLocalAdmistracao.readonly	=	.t.
			Vacinacao.txtLocalAdmistracao.DisabledBackColor = RGB(210,210,210)
			Vacinacao.txtnumInoculacao.readonly	=	.t.
			Vacinacao.txtnumInoculacao.DisabledBackColor = RGB(210,210,210)
			Vacinacao.txtLocalAnatomico.readonly	=	.t.
			Vacinacao.txtLocalAnatomico.DisabledBackColor = RGB(210,210,210)
			Vacinacao.txtlateralidade.readonly		=	.t.
			Vacinacao.txtlateralidade.DisabledBackColor = RGB(210,210,210)
			Vacinacao.txtMotiBreak.enabled = .T.
			Vacinacao.txtDestinoBreak.enabled = .T.
			Vacinacao.txtValidade.enabled = .F.
			Vacinacao.txtValidade.DisabledBackColor = RGB(210,210,210)

			Vacinacao.txtTipoUtente.enabled		=	.F.
			Vacinacao.txtTipoVacinacao.enabled	=	.F.
			Vacinacao.txtViaAdmin.enabled		=	.F.
			Vacinacao.txtLocalAnatomico.enabled	=	.F.
			Vacinacao.txtLocalAdmistracao.enabled =	.F.
			Vacinacao.txtlateralidade.enabled =	.F.

			Vacinacao.btnPesqCliente.enable(.F.)
			**Vacinacao.btnPesqAdministrante.enable(.F.)
			Vacinacao.btnPesqVacina.enable(.T.)
			Vacinacao.btnPesqVenda.enable(.F.)

			Vacinacao.txtComercial_id.readonly				=	.F.
			Vacinacao.txtComercial_id.DisabledBackColor = RGB(254,254,254)

			IF myVacAlteracao AND (uCrsVacinacao.send AND uCrsVacinacao.result = 1)

				Vacinacao.txtComercial_id.readonly				=	.T.
				Vacinacao.txtComercial_id.DisabledBackColor = RGB(210,210,210)

			ENDIF

		ENDIF

		IF myVacIntroducao
			Vacinacao.chkInjetavel.image1.enabled = .T.
      		Vacinacao.chkVacinacao.image1.enabled = .T.
			Vacinacao.chkBreak.image1.enabled = .T.
		ENDIF
			
		vacinacao.txtNome.setfocus()
	Endif
ENDFUNC


*****************************
*	PESQUISAR CLIENTE		*
*****************************
FUNCTION uf_Vacinacao_pesquisarCliente
	uf_pesqUTENTES_Chama("VACINACAO")
ENDFUNC

*****************************
*	PESQUISAR ARTIGOS		*
*****************************
FUNCTION uf_Vacinacao_pesquisarArtigo
   	IF uf_gerais_compStr(VACINACAO.chkInjetavel.tag, "true")
    	SELECT ucrsVacinacao
		REPLACE ucrsVacinacao.codeVacina WITH ''
	   	uf_pesqstocks_chama("VACINACAO")
	ENDIF

	IF uf_gerais_compStr(VACINACAO.chkVacinacao.tag, "true") OR uf_gerais_compStr(VACINACAO.chkBreak.tag, "true")

		IF myVacAlteracao
			IF uCrsVacinacao.send AND uCrsVacinacao.result = 1
				RETURN .F.
			ENDIF
		ENDIF

		IF TYPE("myCallVaccineService") <> 'U'
			myCallVaccineService = .F.
		ENDIF

    	LOCAL uv_sql, uv_curCode
		PUBLIC upv_codVac, upv_refVac

		SELECT ucrsVacinacao
		uv_curCode = ALLTRIM(ucrsVacinacao.codeVacina)

		STORE '' TO upv_codVac, upv_refVac

		TEXT TO uv_sql TEXTMERGE NOSHOW
			exec up_vacinacao_getVacinas
		ENDTEXT

		IF !uf_gerais_actGrelha("", "uc_vaccineCode", uv_sql)
			uf_perguntalt_chama("Erro a encontrar C�digo de Vacina��o." + CHR(13)+ "Por favor contacte o suporte.", "OK", "", 48)
			RETURN .F.
		ENDIF

		uf_valorescombo_chama("upv_codVac", 0,"uc_vaccineCode", 2, "Codigo", "description", .F., .F., .F., .T., .F., .F., .F., .F., .T.)

		IF EMPTY(upv_codVac)
			RETURN .F.
		ENDIF

      	LOCAL lcteste 
		IF uf_gerais_getparameter("ADM0000000227", "BOOL")
			lcteste = .T.
		ELSE
			lcteste = .F.
		ENDIF

		SELECT ucrsVacinacao
		REPLACE ucrsVacinacao.codeVacina WITH ALLTRIM(upv_codVac)

		IF(!uf_vacinacao_verifyToCallService(1, 'SPMS', 1, 'vaccineInfo', lcteste ,'A chamar servi�o',.T.,'','', .T.))
			RETURN .F.
		ENDIF

		IF !USED("ucrsCommercialName")
			SELECT ucrsVacinacao
			REPLACE ucrsVacinacao.codeVacina WITH ALLTRIM(uv_curCode)
			uf_perguntalt_chama("N�o foi poss�vel devolver os produtos." + CHR(13) + "Por favor contacte o suporte.", "OK", "")
			RETURN .F.
		ENDIF

		SELECT code as CNP, name as description FROM ucrsCommercialName WITH (BUFFERING = .T.) ORDER BY CNP DESC INTO CURSOR uc_commercialNames

		SELECT uc_commercialNames
		GO TOP

		IF RECCOUNT("uc_commercialNames") = 1

			upv_refVac = uc_commercialNames.CNP

		ELSE

			uf_valorescombo_chama("upv_refVac", 0,"uc_commercialNames", 2, "cnp", "cnp,description", .F., .F., .F., .T., .F., .F., .F., .F., .T.)

		ENDIF

		IF EMPTY(upv_refVac)
			SELECT ucrsVacinacao
			REPLACE ucrsVacinacao.codeVacina WITH ALLTRIM(uv_curCode)
			RETURN .F.
		ENDIF

		LOCAL uv_tokenCommercialName, uv_refCommercialName, uv_designCommercialName
		STORE '' TO uv_tokenCommercialName, uv_refCommercialName, uv_designCommercialName

		SELECT ucrsCommercialName
		GO TOP

		LOCATE FOR uf_gerais_compStr(ucrsCommercialName.code, upv_refVac)

		IF !FOUND()
			SELECT ucrsVacinacao
			REPLACE ucrsVacinacao.codeVacina WITH ALLTRIM(uv_curCode)
			RETURN .F.
		ELSE
			uv_tokenCommercialName = ucrsCommercialName.token
			uv_designCommercialName = ucrsCommercialName.name
			uv_refCommercialName = ucrsCommercialName.code
		ENDIF

		SELECT ucrsVacinacao
		REPLACE ucrsVacinacao.comercial_id WITH ALLTRIM(uv_refCommercialName)
		REPLACE ucrsVacinacao.descricao WITH ALLTRIM(uv_designCommercialName)
		REPLACE uCrsVacinacao.viaadmin_id    WITH ''
		REPLACE uCrsVacinacao.viaadmindesc  WITH ''
		REPLACE ucrsVacinacao.dosagem WITH 0
		REPLACE uCrsVacinacao.lateralidade WITH ''
		REPLACE uCrsVacinacao.lateralidadeDesc WITH ''
		REPLACE uCrsVacinacao.localAnatomico_id WITH ''
		REPLACE uCrsVacinacao.localAnatomico_idDesc WITH ''
		REPLACE uCrsVacinacao.localAdmistracao WITH ''
		REPLACE uCrsVacinacao.localAdmistracaoDesc WITH ''
      IF !EMPTY(ucrsVacinacao.unidades) OR ucrsVacinacao.unidades = 0
         REPLACE ucrsVacinacao.unidades WITH 1
      ENDIF

		uf_vacinacao_schema_createCursor_commercialName(uv_tokenCommercialName, uv_refCommercialName)

		FECHA("uc_commercialNames")

		IF TYPE("myCallVaccineService") == 'U'
			PUBLIC myCallVaccineService
		ENDIF

		myCallVaccineService = .T.

		IF uf_gerais_compStr(VACINACAO.chkVacinacao.tag, "true")

			IF USED("ucrsCommercialNameDosage")
				SELECT ucrsCommercialNameDosage
				CALCULATE CNT() TO uv_count
				IF uv_count = 1
					SELECT ucrsCommercialNameDosage
					GO TOP
					SELECT ucrsVacinacao
					GO TOP
					REPLACE ucrsVacinacao.dosagem WITH ucrsCommercialNameDosage.dose
				ENDIF
			ENDIF

			IF USED("ucrsSide")
				SELECT ucrsSide
				CALCULATE CNT() TO uv_count
				IF uv_count = 1
					SELECT ucrsSide
					GO TOP
					SELECT ucrsVacinacao
					GO TOP
					REPLACE uCrsVacinacao.lateralidade WITH ASTR(ucrsSide.ID)
					REPLACE uCrsVacinacao.lateralidadeDesc WITH ALLTRIM(ucrsSide.description)
            ELSE

               SELECT ucrsSide
               GO TOP

               LOCATE FOR ucrsSide.id = 1
               IF FOUND()
                  SELECT ucrsSide
                  SELECT ucrsVacinacao
                  GO TOP
                  REPLACE uCrsVacinacao.lateralidade WITH ASTR(ucrsSide.ID)
                  REPLACE uCrsVacinacao.lateralidadeDesc WITH ALLTRIM(ucrsSide.description)
               ENDIF

				ENDIF
			ENDIF

			IF USED("ucrsAnatomicLocations")
				SELECT ucrsAnatomicLocations
				CALCULATE CNT() TO uv_count
				IF uv_count = 1
					SELECT ucrsAnatomicLocations
					GO TOP
					SELECT ucrsVacinacao
					GO TOP
					REPLACE uCrsVacinacao.localAnatomico_id WITH ASTR(ucrsAnatomicLocations.ID)
					REPLACE uCrsVacinacao.localAnatomico_idDesc WITH ALLTRIM(ucrsAnatomicLocations.description)
				ENDIF
			ENDIF

         IF USED("ucrsAdministrationWays")
				SELECT ucrsAdministrationWays
				CALCULATE CNT() TO uv_count
				IF uv_count = 1
					SELECT ucrsAdministrationWays
					GO TOP
					SELECT ucrsVacinacao
					GO TOP
					REPLACE uCrsVacinacao.viaadmin_id    WITH ASTR(ucrsAdministrationWays.ID)
					REPLACE uCrsVacinacao.viaadmindesc  WITH ALLTRIM(ucrsAdministrationWays.description)
            ELSE

               SELECT ucrsAdministrationWays
               GO TOP
               LOCATE FOR INLIST(ucrsAdministrationWays.id, 2, 3)
               IF FOUND()

                  SELECT ucrsAdministrationWays
                  SELECT ucrsVacinacao
                  GO TOP
                  REPLACE uCrsVacinacao.viaadmin_id    WITH ASTR(ucrsAdministrationWays.ID)
                  REPLACE uCrsVacinacao.viaadmindesc  WITH ALLTRIM(ucrsAdministrationWays.description)         

               ENDIF
				ENDIF
			ENDIF

         SELECT uCrsVacinacao

         IF !EMPTY(uCrsVacinacao.viaadmin_id) AND EMPTY(uCrsVacinacao.localAnatomico_id) AND USED("ucrsAnatomicLocations")

            IF uf_gerais_compStr(uCrsVacinacao.viaadmin_id, "2") OR uf_gerais_compStr(uCrsVacinacao.viaadmin_id, "3")
               SELECT ucrsAnatomicLocations
               GO TOP
               LOCATE FOR ucrsAnatomicLocations.id = 4
               IF FOUND()
                  SELECT ucrsAnatomicLocations
                  SELECT ucrsVacinacao
                  GO TOP
                  REPLACE uCrsVacinacao.localAnatomico_id WITH ASTR(ucrsAnatomicLocations.ID)
                  REPLACE uCrsVacinacao.localAnatomico_idDesc WITH ALLTRIM(ucrsAnatomicLocations.description)
               ENDIF
            ENDIF

         ENDIF

			IF USED("ucrsLocationAdms")
				SELECT ucrsLocationAdms
				CALCULATE CNT() TO uv_count
				IF uv_count = 1
					SELECT ucrsLocationAdms
					GO TOP
					SELECT ucrsVacinacao
					GO TOP
					REPLACE uCrsVacinacao.localAdmistracao WITH ASTR(ucrsLocationAdms.ID)
					REPLACE uCrsVacinacao.localAdmistracaoDesc WITH ALLTRIM(ucrsLocationAdms.description)
            ELSE

               SELECT ucrsLocationAdms
               GO TOP
               LOCATE FOR ucrsLocationAdms.id = 23
               IF FOUND()

                  SELECT ucrsLocationAdms
                  SELECT ucrsVacinacao
                  GO TOP
                  REPLACE uCrsVacinacao.localAdmistracao WITH ASTR(ucrsLocationAdms.ID)
                  REPLACE uCrsVacinacao.localAdmistracaoDesc WITH ALLTRIM(ucrsLocationAdms.description)  

               ENDIF

				ENDIF
			ENDIF

		ENDIF

		VACINACAO.refresh()

   ENDIF
ENDFUNC

*****************************
*	PESQUISAR FACTURA�AO		*
*****************************
FUNCTION uf_Vacinacao_pesquisarFT
	IF !(uf_gerais_validaPermUser(m.ch_userno,m.ch_grupo,'Gest�o de Factura��o - Visualizar'))
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE CONSULTAR REGISTOS DE FACTURA��O.","OK","",48)
		RETURN .f.
	ENDIF	
	
	uf_pesqFact_chama("VACINACAO")
ENDFUNC


*****************************
*	PESQUISAR VACINA��O		*
*****************************
FUNCTION uf_Vacinacao_pesquisar
	IF !(uf_gerais_validaPermUser(m.ch_userno,m.ch_grupo,'Registo de Vacina��o - Visualizar'))
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE CONSULTAR REGISTOS DE VACINA��O.","OK","",48)
		RETURN .f.
	ENDIF
	
	uf_VacinacaoPesq_Chama()

	IF USED("uCrsVacinacao")

		SELECT uCrsVacinacao

		IF uf_gerais_compStr(VACINACAO.chkVacinacao.tag, "true")
			VACINACAO.chkVacinacao.click(.T.)
		ENDIF

		IF uf_gerais_compStr(VACINACAO.chkInjetavel.tag, "true")
			VACINACAO.chkInjetavel.click(.T.)
		ENDIF

		IF uf_gerais_compStr(VACINACAO.chkBreak.tag, "true")
			VACINACAO.chkBreak.click(.T., .T.)
		ENDIF

		IF uf_gerais_compStr(uCrsVacinacao.tipoReg, "E-boletim") AND uf_gerais_compStr(VACINACAO.chkVacinacao.tag, "false")
			VACINACAO.chkVacinacao.click(.T.)
		ENDIF

		IF uf_gerais_compStr(uCrsVacinacao.tipoReg, "Registo Interno") AND uf_gerais_compStr(VACINACAO.chkInjetavel.tag, "false")
			VACINACAO.chkInjetavel.click(.T.)
		ENDIF

		IF uf_gerais_compStr(uCrsVacinacao.tipoReg, "Quebra") AND uf_gerais_compStr(VACINACAO.chkBreak.tag, "false")
			VACINACAO.chkBreak.click(.T., .T.)
		ENDIF

	ENDIF

ENDFUNC

*********************************
*	PESQUISAR ADMINISTRANTE		*
*********************************
FUNCTION uf_vacinacao_pesquisarAdministrante
	uf_pesqmultidata_chama('VENDEDORES', 'Vacinacao.txtAdministrante')	
ENDFUNC


*********************************
*	PESQUISAR Via Administra��o	*
*********************************

FUNCTION  uf_Vacinacao_pesquisarViaAdministracao()
	uf_pesqmultidata_chama('VIAADMIN', 'Vacinacao.txtViaAdmin')	
ENDFUNC


*************************
*	INSERIR VACINACAO	*
*************************
FUNCTION uf_Vacinacao_inserir
	IF !(uf_gerais_validaPermUser(m.ch_userno,m.ch_grupo,'Registo de Vacina��o - Introduzir'))
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE REGISTAR VACINA��ES.","OK","",48)
		RETURN .f.
	ENDIF	
	
	SELECT uCrsVacinacao
	DELETE ALL
	APPEND BLANK
	
	myVacIntroducao = .t.
	myVacAlteracao = .f.
	uf_Vacinacao_confEcra(2)
					
	&& Valores por defeito
	uf_Vacinacao_ValoresDefeito()

	VACINACAO.chkVacinacao.click()
	
	&& Actualiza ecra
	SELECT uCrsVacinacao
	vacinacao.refresh
ENDFUNC

*****************************
*	 Alterar Vacinacao		*
*****************************
FUNCTION uf_Vacinacao_alterar
	
	IF (uf_gerais_validaPermUser(m.ch_userno,m.ch_grupo,'Registo de Vacina��o - Modo de Altera��o'))		
		&& Valida Cursor
		SELECT uCrsVacinacao
		IF EMPTY(uCrsVacinacao.vacinacaostamp)
			RETURN .f.
		ENDIF
		
		SELECT uCrsVacinacao
		IF uf_gerais_compStr(VACINACAO.chkInjetavel.tag, "false")
			IF !uf_vacinacao_allowsUpdate()
				RETURN .F.
			ENDIF
		ENDIF
		&& Configura ecra
		myVacAlteracao = .t.
		myVacIntroducao = .f.
		uf_Vacinacao_confEcra(2)
	ELSE
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ALTERAR REGISTOS DE VACINA��ES.","OK","",48)
	ENDIF
ENDFUNC

*****************************
* 	ELIMINAR VACINACAO		*
*****************************
FUNCTION uf_Vacinacao_eliminar
	IF !(uf_gerais_validaPermUser(m.ch_userno,m.ch_grupo,'Registo de Vacina��o - Eliminar'))
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ELIMINAR REGISTOS DE VACINA��ES.","OK","",48)
		RETURN .f.
	ENDIF
	
	&&Valida��es
	IF USED("uCrsVacinacao")
		SELECT uCrsVacinacao

		IF uCrsVacinacao.comunicado
			uf_perguntalt_chama("Nao pode eliminar um registo j� comunicado.", "OK", "", 48)
			RETURN .F.
		ENDIF

		IF !EMPTY(uCrsVacinacao.vacinacaostamp)			
			&& Passou valida��es - vai apagar
			IF uf_Perguntalt_chama("Tem a certeza que deseja apagar o registo de vacina��o?" + CHR(13) + "Vai perder todos os dados para sempre.", "Sim", "N�o")
				SELECT uCrsVacinacao
				IF uf_gerais_actGrelha("","",[delete from B_vacinacao where vacinacaostamp=']+uCrsVacinacao.vacinacaostamp+['])
					uf_perguntalt_chama("REGISTO APAGADO COM SUCESSO!","OK","",64)
					&& navega para ultimo registo
					uf_Vacinacao_ant()
				ELSE
					uf_perguntalt_chama("OCORREU UM ERRO AO APAGAR O REGISTO DE VACINA��O!","OK","",16)
				ENDIF
			endif		
		endif
	ENDIF
ENDFUNC

FUNCTION uf_Vacinacao_actualiza_result
	lparam lcResult, lcStamp
	
	LOCAL lcSQL

	if(lcResult==1)

		TEXT TO lcSQL TEXTMERGE noshow
			UPDATE b_vacinacao 
			SET 
				dateSend            = '<<uf_gerais_getdate(date(),"SQL")>>',
				send                = 1,
				result              = 1,
            	correlationId       = ISNULL((select TOP 1 correlationId from sendVaccine(nolock) inner join sendVaccineResp(nolock) on sendVaccine.token = sendVaccineResp.token where saveStamp = '<<ALLTRIM(lcStamp)>>' order by sendVaccine.ousrdata desc), '')
			WHERE vacinacaostamp = '<<lcStamp>>'
		ENDTEXT

		IF !uf_gerais_actGrelha("","",lcSQL)	
			uf_perguntalt_chama("OCORREU UM ERRO AO ATUALIZAR O REGISTO DA COMUNICA��O DE VACINA��O!","OK","",16)
			RETURN .f.
		ENDIF
		
	ENDIF
	
ENDFUNC



*************************************************
*		GRAVA O REGISTO NA BASE DE DADOS		*
*************************************************
FUNCTION uf_Vacinacao_Gravar
	LOCAL lcSQL

	IF uf_gerais_compStr(VACINACAO.chkInjetavel.tag, "false")
		IF !uf_gerais_checkCedula()
			RETURN .F.
		ENDIF
	ENDIF

	SELECT ucrsuser

	IF myVacIntroducao

		SELECT ucrsuser
		
		SELECT uCrsVacinacao
		REPLACE uCrsVacinacao.administrante WITH ALLTRIM(ucrsuser.nome)

	ENDIF

	IF uf_gerais_compStr(VACINACAO.chkVacinacao.tag, "false") AND uf_gerais_compStr(VACINACAO.chkInjetavel.tag, "false") AND uf_gerais_compStr(VACINACAO.chkBreak.tag, "false")
		uf_perguntalt_chama("POR FAVOR PREENCHA O TIPO DE REGISTO!","OK","",48)
		RETURN .f.
	ENDIF	

   IF !uf_vacinacao_camposObrigatorios()
      RETURN .F.
   ENDIF
	
&& INSERT	
	IF myVacIntroducao
		&& Calcula numero
		uf_gerais_actGrelha("","uCrsTempNo",[Select Isnull(Max(convert(int,NO)),0)+1 as no From B_vacinacao (nolock)])
		SELECT uCrsTempNo
		replace uCrsVacinacao.NO	with	astr(uCrsTempNo.no)
		fecha("uCrsTempNo")
		
		lcSQL=""
		SELECT uCrsVacinacao
		GO TOP
		
		LOCAL lcStamp
		lcStamp=ALLTRIM(uf_gerais_stamp())
		
		TEXT TO lcSQL TEXTMERGE noshow
			INSERT INTO b_vacinacao (vacinacaostamp,no,Administrante,dataAdministracao,nome,dataNasc,contacto,Descricao,lote,nrVenda,obs,nrSns,codeVacina,dosagem,unidades,
			viaAdmin_id,localAnatomico_id,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,site_nr,comercial_id, autResidencia,typeUtente,localAdmistracao,
			numInoculacao,typeVacinacao,lateralidade,typeUtenteDesc,localAdmistracaoDesc,typeVacinacaoDesc,lateralidadeDesc,localAnatomico_idDesc,viaAdminDesc, tipoReg,
			motiveId, destinyId, validade)
			values
			(
				'<<LEFT(lcStamp,25)>>',
				'<<ALLTRIM(uCrsVacinacao.no)>>',
				'<<ALLTRIM(uCrsVacinacao.administrante)>>',
				'<<uf_gerais_getDate(uCrsVacinacao.dataAdministracao,"SQL")>>',
				'<<ALLTRIM(uCrsVacinacao.nome)>>',
				'<<uf_gerais_getDate(uCrsVacinacao.dataNasc,"SQL")>>',
				'<<ALLTRIM(uCrsVacinacao.contacto)>>',
				'<<LEFT(ALLTRIM(uCrsVacinacao.Descricao),254)>>',
				'<<ALLTRIM(uCrsVacinacao.lote)>>',
				'<<ALLTRIM(uCrsVacinacao.nrVenda)>>',
				'<<LEFT(ALLTRIM(uCrsVacinacao.obs),254)>>',
				'<<ALLTRIM(uCrsVacinacao.nrSns)>>',
				'<<ALLTRIM(uCrsVacinacao.codeVacina)>>',
				<<uCrsVacinacao.dosagem>>,
				<<uCrsVacinacao.unidades>>,
				<<IIF(EMPTY(uCrsVacinacao.viaAdmin_id), 0, uCrsVacinacao.viaAdmin_id)>>,
				'<<ALLTRIM(uCrsVacinacao.localAnatomico_id)>>',
				'<<m.m_chinis>>',
				convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102),
				convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108),
				'<<m.m_chinis>>',
				convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102),
				convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108),
				'<<mysite_nr>>',
				'<<uCrsVacinacao.comercial_id>>',
                <<IIF(uCrsVacinacao.autResidencia, 1, 0)>>,
				'<<IIF( uf_gerais_compStr(VACINACAO.chkVacinacao.tag, "true"), ALLTRIM(uCrsVacinacao.typeUtente), "")>>',
				'<<IIF( uf_gerais_compStr(VACINACAO.chkVacinacao.tag, "true"), ALLTRIM(uCrsVacinacao.localAdmistracao), "")>>',
				'<<ALLTRIM(uCrsVacinacao.numInoculacao)>>',
				'<<IIF( uf_gerais_compStr(VACINACAO.chkVacinacao.tag, "true"), ALLTRIM(uCrsVacinacao.typeVacinacao), "")>>',
				'<<ALLTRIM(uCrsVacinacao.lateralidade)>>',
				'<<IIF( uf_gerais_compStr(VACINACAO.chkVacinacao.tag, "true"), LEFT(ALLTRIM(uCrsVacinacao.typeUtenteDesc),200), "")>>',
				'<<IIF( uf_gerais_compStr(VACINACAO.chkVacinacao.tag, "true"), LEFT(ALLTRIM(uCrsVacinacao.localAdmistracaoDesc),200), "")>>',
				'<<IIF( uf_gerais_compStr(VACINACAO.chkVacinacao.tag, "true"), LEFT(ALLTRIM(uCrsVacinacao.typeVacinacaoDesc),200), "")>>',
				'<<LEFT(ALLTRIM(uCrsVacinacao.lateralidadeDesc),200)>>',
				'<<LEFT(ALLTRIM(uCrsVacinacao.localAnatomico_idDesc),200)>>',
				'<<LEFT(ALLTRIM(uCrsVacinacao.viaAdminDesc),200)>>',
				'<<IIF( uf_gerais_compStr(VACINACAO.chkVacinacao.tag, "true"), "E-boletim", IIF( uf_gerais_compStr(VACINACAO.chkInjetavel.tag, "true"), "Registo Interno", "Quebra"))>>',
				'<<IIF(EMPTY(VACINACAO.txtMotiBreak.tag), "0", VACINACAO.txtMotiBreak.tag)>>',
				'<<IIF(EMPTY(VACINACAO.txtDestinoBreak.tag), "0", VACINACAO.txtDestinoBreak.tag)>>',
				'<<uf_gerais_getDate(ucrsVacinacao.validade, "SQL")>>'
			)		
		ENDTEXT
			
		IF !uf_gerais_actGrelha("","",lcSQL)	
			uf_perguntalt_chama("OCORREU UM ERRO A GRAVAR O REGISTO DE VACINA��O! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			RETURN		
		ENDIF
		
		LOCAL lcteste 
		IF uf_gerais_getparameter("ADM0000000227", "BOOL")
			lcteste = .T.
		ELSE
			lcteste = .F.
		ENDIF
		
		IF(uf_gerais_compStr(VACINACAO.chkVacinacao.tag, "true"))
			uf_vacinacao_verifyToCallService(1, 'SPMS', 3, 'saveVaccine', lcteste ,'A chamar servi�o',.T.,lcStamp,'')
		ENDIF

		IF(uf_gerais_compStr(VACINACAO.chkBreak.tag, "true"))
			uf_vacinacao_verifyToCallService(1, 'SPMS', 4, 'break', lcteste ,'A chamar servi�o',.T., '', lcStamp)
		ENDIF
		
		&& configura o ecra
		uf_perguntalt_chama("Dados Gravados com sucesso","OK","",64)
		uf_Vacinacao_confEcra(1)
		uf_vacinacao_actualizar()	
	ENDIF 
&& UPDATE
	IF myVacAlteracao
		lcSQL=""
		SELECT uCrsVacinacao
		GO top

		IF uCrsVacinacao.send AND uCrsVacinacao.result = 1

			TEXT TO lcSQL TEXTMERGE noshow
				EXEC up_vacinacao_getVacinacao '<<uCrsVacinacao.no>>'
			ENDTEXT
				
			IF !(uf_gerais_actGrelha("","uCrsVacinacao_bck",lcSQL))
				uf_perguntalt_chama("Erro a registar altera��es." + chr(13) + "Por favor contacte o suporte.", "OK", "", 48)
				RETURN .F.
			ENDIF

		ENDIF
		
		TEXT TO lcSQL TEXTMERGE noshow
			UPDATE b_vacinacao 
			SET 
				Administrante		= '<<ALLTRIM(uCrsVacinacao.administrante)>>',
				dataAdministracao	= '<<uf_gerais_getDate(uCrsVacinacao.dataAdministracao,"SQL")>>',
				nome 				= '<<ALLTRIM(uCrsVacinacao.nome)>>',
				dataNasc			= '<<uf_gerais_getDate(uCrsVacinacao.dataNasc,"SQL")>>',
				contacto			= '<<ALLTRIM(uCrsVacinacao.contacto)>>',
				Descricao			= '<<LEFT(ALLTRIM(uCrsVacinacao.Descricao),254)>>',
				lote				= '<<ALLTRIM(uCrsVacinacao.lote)>>',
				nrVenda				= '<<ALLTRIM(uCrsVacinacao.nrVenda)>>',
				obs					= '<<LEFT(ALLTRIM(uCrsVacinacao.obs),254)>>',
				nrSns				= '<<ALLTRIM(uCrsVacinacao.nrSns)>>',
				codeVacina			= '<<ALLTRIM(uCrsVacinacao.codeVacina)>>',
				unidades			= <<uCrsVacinacao.unidades>>,
				dosagem 			= <<uCrsVacinacao.dosagem>>,
				viaAdmin_id			= '<<uCrsVacinacao.viaAdmin_id>>',
				usrinis				= '<<m.m_chinis>>',
				usrdata				= convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102),
				usrhora				= convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108),
				comercial_id 		= '<<uCrsVacinacao.comercial_id>>',
                autResidencia       = <<IIF(uCrsVacinacao.autResidencia, 1, 0)>>,
				typeUtente			= '<<IIF( uf_gerais_compStr(VACINACAO.chkVacinacao.tag, "true"), ALLTRIM(uCrsVacinacao.typeUtente), "")>>',
				localAdmistracao	= '<<IIF( uf_gerais_compStr(VACINACAO.chkVacinacao.tag, "true"), ALLTRIM(uCrsVacinacao.localAdmistracao), "")>>',
				numInoculacao		= '<<ALLTRIM(uCrsVacinacao.numInoculacao)>>',
				typeVacinacao		= '<<IIF( uf_gerais_compStr(VACINACAO.chkVacinacao.tag, "true"), ALLTRIM(uCrsVacinacao.typeVacinacao), "")>>',
				lateralidade		= '<<ALLTRIM(uCrsVacinacao.lateralidade)>>',
				typeUtenteDesc		= '<<IIF( uf_gerais_compStr(VACINACAO.chkVacinacao.tag, "true"), LEFT(ALLTRIM(uCrsVacinacao.typeUtenteDesc),200), "")>>',
				localAdmistracaoDesc = '<<IIF( uf_gerais_compStr(VACINACAO.chkVacinacao.tag, "true"), LEFT(ALLTRIM(uCrsVacinacao.localAdmistracaoDesc),200), "")>>',
				typeVacinacaoDesc	= '<<IIF( uf_gerais_compStr(VACINACAO.chkVacinacao.tag, "true"), LEFT(ALLTRIM(uCrsVacinacao.typeVacinacaoDesc),200), "")>>',
				lateralidadeDesc	= '<<LEFT(ALLTRIM(uCrsVacinacao.lateralidadeDesc),200)>>',
				localAnatomico_idDesc = '<<LEFT(ALLTRIM(uCrsVacinacao.localAnatomico_idDesc),200)>>',
				viaAdminDesc		= '<<LEFT(ALLTRIM(uCrsVacinacao.viaAdminDesc),200)>>',
				tipoReg				= '<<IIF( uf_gerais_compStr(VACINACAO.chkVacinacao.tag, "true"), "E-boletim", IIF( uf_gerais_compStr(VACINACAO.chkInjetavel.tag, "true"), "Registo Interno", "Quebra"))>>',
				motiveId = '<<IIF(EMPTY(VACINACAO.txtMotiBreak.tag), "0", VACINACAO.txtMotiBreak.tag)>>',
				destinyId = '<<IIF(EMPTY(VACINACAO.txtDestinoBreak.tag), "0", VACINACAO.txtDestinoBreak.tag)>>',
				validade = '<<uf_gerais_getDate(ucrsVacinacao.validade, "SQL")>>'
			WHERE vacinacaostamp 	= '<<ALLTRIM(uCrsVacinacao.vacinacaostamp)>>'
		ENDTEXT

		IF !uf_gerais_actGrelha("","",lcSQL)	
			uf_perguntalt_chama("OCORREU UM ERRO AO ATUALIZAR O REGISTO DE VACINA��O!","OK","",16)
			RETURN .f.
		ELSE

			IF uCrsVacinacao.send AND uCrsVacinacao.result = 1

				IF USED("uCrsVacinacao_bck")

					LOCAL uv_count, uv_curField, uv_curType, uv_newValue, uv_oriValue, i

					SELECT uCrsVacinacao_bck
					uv_count = aFields(ua_campos)

					FOR i=1 TO uv_count

						uv_curField = ALLTRIM(ua_campos[i, 1])
						uv_curType = ua_campos[i, 2]

						DO CASE
							CASE uv_curType = 'N'
								uv_newValue = astr(uCrsVacinacao.&uv_curField., ua_campos[i, 3], ua_campos[i, 4])
								uv_oriValue = astr(uCrsVacinacao_bck.&uv_curField., ua_campos[i, 3], ua_campos[i, 4])
							CASE uv_curType = 'L'
								uv_newValue = IIF(uCrsVacinacao.&uv_curField.,'1','0')
								uv_oriValue = IIF(uCrsVacinacao_bck.&uv_curField.,'1','0')
							CASE uv_curType = 'D'
								uv_newValue = DTOS(uCrsVacinacao.&uv_curField.)
								uv_oriValue = DTOS(uCrsVacinacao_bck.&uv_curField.)
							CASE uv_curType = 'T'
								uv_newValue = DTOS(uCrsVacinacao.&uv_curField.)
								uv_oriValue = DTOS(uCrsVacinacao_bck.&uv_curField.)
							CASE INLIST(uv_curType, 'C', 'M')
								uv_newValue = ALLTRIM(uCrsVacinacao.&uv_curField.)
								uv_oriValue = ALLTRIM(uCrsVacinacao_bck.&uv_curField.)
							CASE uv_curType = 'I'
								uv_newValue = astr(uCrsVacinacao.&uv_curField., ua_campos[i, 3], ua_campos[i, 4])
								uv_oriValue = astr(uCrsVacinacao_bck.&uv_curField., ua_campos[i, 3], ua_campos[i, 4])
							OTHERWISE
								messageb(uv_curField)
								messageb(uv_curType)
						ENDCASE	

						SELECT uCrsVacinacao

						IF uCrsVacinacao.&uv_curField. <> uCrsVacinacao_bck.&uv_curField.
							uf_gerais_registaocorrencia('Vacina��o', 'Registo N�' + ALLTRIM(uCrsVacinacao.no) + ' ' +ALLTRIM(uv_curField), 2, uv_oriValue, uv_newValue, uCrsVacinacao.vacinacaoStamp, ch_userno, myTerm, uf_gerais_getDate(Date(), "SQL"))
						ENDIF

					NEXT

				ENDIF

			ELSE

				LOCAL lcteste 
				IF uf_gerais_getparameter("ADM0000000227", "BOOL")
					lcteste = .T.
				ELSE
					lcteste = .F.
				ENDIF

				IF(uf_gerais_compStr(VACINACAO.chkVacinacao.tag, "true"))
					uf_vacinacao_verifyToCallService(1, 'SPMS', 3, 'saveVaccine', lcteste ,'A chamar servi�o',.T.,uCrsVacinacao.vacinacaostamp,'')				          			
				ENDIF

				IF(uf_gerais_compStr(VACINACAO.chkBreak.tag, "true"))
					uf_vacinacao_verifyToCallService(1, 'SPMS', 4, 'break', lcteste ,'A chamar servi�o',.T., '', uCrsVacinacao.vacinacaostamp)
				ENDIF

			ENDIF
		ENDIF
				
		uf_perguntalt_chama("Dados Atualizados com sucesso","OK","",64)
		
		uf_Vacinacao_confEcra(1)
		uf_vacinacao_actualizar()
			
	ENDIF	

	IF VACINACAO.applyPlan AND USED("ucrsVacinacao") AND !EMPTY(ucrsVacinacao.correlationId)
		uf_vacinacao_selVacCode()
	ENDIF

	uf_Vacinacao_confEcra(1)

ENDFUNC

*********************************
*	 NAVEGA REGISTO ANTERIOR	*
*********************************
FUNCTION uf_Vacinacao_ant
		
	SELECT uCrsVacinacao
	lcSQL=""
		
	TEXT TO lcSQL TEXTMERGE NOSHOW 
		SELECT TOP 1 no FROM B_vacinacao (nolock)
		WHERE no < <<IIF(EMPTY(uCrsVacinacao.no),0,uCrsVacinacao.no)>>
		order by no desc
	ENDTEXT  

	If !uf_gerais_actGrelha("","uCrstempVacinacao",lcSQL)
		RETURN .f.
	ENDIF
	
	uf_vacinacao_criaCur(uCrstempVacinacao.no)
	
	fecha("uCrstempVacinacao")
ENDFUNC 	



*********************************
*	 NAVEGA PROXIMO REGISTO 	*
*********************************
FUNCTION uf_Vacinacao_prox
	
	SELECT uCrsVacinacao
	lcSQL=""
	
	TEXT TO lcSQL TEXTMERGE NOSHOW 
		SELECT TOP 1 no FROM B_vacinacao (nolock)
		WHERE no > <<IIF(EMPTY(uCrsVacinacao.no),0,uCrsVacinacao.no)>>
		order by no asc
	ENDTEXT  

	If !uf_gerais_actGrelha("","uCrstempVacinacao",lcSQL)
		uf_Vacinacao_prox()
		RETURN .f.
	ENDIF
	
	uf_vacinacao_criaCur(uCrstempVacinacao.no)
	
	Fecha("uCrstempVacinacao")
ENDFUNC



FUNCTION uf_Vacinacao_ValoresDefeito
	SELECT uCrsVacinacao
	replace dataAdministracao	with	DATE()
	replace DataNasc			with	CTOD('01.01.1900')
ENDFUNC



*********************************
*		FECHAR O PAINEL			*
*********************************
FUNCTION uf_Vacinacao_sair
		
	IF myVacAlteracao OR myVacIntroducao
		IF uf_perguntalt_chama("Deseja cancelar as altera��es?" + CHR(13) + "Ir� perder as �ltimas altera��es feitas", "Sim", "N�o")

			IF myVacIntroducao
				vacinacao.menu1.ultimo.click() 
			ELSE && Altera��o
				uf_vacinacao_criaCur(uCrsVacinacao.no)
			ENDIF

			IF USED("uCrsVacinacao")

				SELECT uCrsVacinacao

				IF uf_gerais_compStr(VACINACAO.chkVacinacao.tag, "true")
					VACINACAO.chkVacinacao.click(.T.)
				ENDIF

				IF uf_gerais_compStr(VACINACAO.chkInjetavel.tag, "true")
					VACINACAO.chkInjetavel.click(.T.)
				ENDIF

				IF uf_gerais_compStr(VACINACAO.chkBreak.tag, "true")
					VACINACAO.chkBreak.click(.T., .T.)
				ENDIF

				IF uf_gerais_compStr(uCrsVacinacao.tipoReg, "E-boletim") AND uf_gerais_compStr(VACINACAO.chkVacinacao.tag, "false")
					VACINACAO.chkVacinacao.click(.T.)
				ENDIF

				IF uf_gerais_compStr(uCrsVacinacao.tipoReg, "Registo Interno") AND uf_gerais_compStr(VACINACAO.chkInjetavel.tag, "false")
					VACINACAO.chkInjetavel.click(.T.)
				ENDIF

				IF uf_gerais_compStr(uCrsVacinacao.tipoReg, "Quebra") AND uf_gerais_compStr(VACINACAO.chkBreak.tag, "false")
					VACINACAO.chkBreak.click(.T., .T.)
				ENDIF

			ENDIF

			&&Actualizar Painel	
			uf_Vacinacao_confEcra(1)
			vacinacao.Refresh
		ENDIF	
	ELSE
		&& fecha cursor
		IF USED("uCrsVacinacao")
			fecha("uCrsVacinacao")
		ENDIF
		
		uf_vacinacao_closeCursors()

		&& fecha painel
		vacinacao.hide
		vacinacao.release
		RELEASE vacinacao
	ENDIF
	
	
ENDFUNC

FUNCTION uf_vacinacao_ultRegisto
	IF uf_gerais_actGrelha("","uCrsPesqVacinacao",[select LTRIM(MAX(convert(int,no))) as no from B_Vacinacao (nolock) where site_nr = ] + ASTR(mySite_nr))
		IF RECCOUNT()>0
			uf_vacinacao_criaCur(uCrsPesqVacinacao.no)

			IF USED("uCrsVacinacao")

				SELECT uCrsVacinacao

				IF uf_gerais_compStr(VACINACAO.chkVacinacao.tag, "true")
					VACINACAO.chkVacinacao.click(.T.)
				ENDIF

				IF uf_gerais_compStr(VACINACAO.chkInjetavel.tag, "true")
					VACINACAO.chkInjetavel.click(.T.)
				ENDIF

				IF uf_gerais_compStr(VACINACAO.chkBreak.tag, "true")
					VACINACAO.chkBreak.click(.T., .T.)
				ENDIF

				IF uf_gerais_compStr(uCrsVacinacao.tipoReg, "E-boletim") AND uf_gerais_compStr(VACINACAO.chkVacinacao.tag, "false")
					VACINACAO.chkVacinacao.click(.T.)
				ENDIF

				IF uf_gerais_compStr(uCrsVacinacao.tipoReg, "Registo Interno") AND uf_gerais_compStr(VACINACAO.chkInjetavel.tag, "false")
					VACINACAO.chkInjetavel.click(.T.)
				ENDIF

				IF uf_gerais_compStr(uCrsVacinacao.tipoReg, "Quebra") AND uf_gerais_compStr(VACINACAO.chkBreak.tag, "false")
					VACINACAO.chkBreak.click(.T., .T.)
				ENDIF

			ENDIF

			IF TYPE("VACINACAO") <> "U"
				uf_Vacinacao_confEcra(1)
			ENDIF

		ENDIF
		fecha("uCrsPesqVacinacao")
	ENDIF
ENDFUNC 

FUNCTION uf_vacinacao_actualizar
	IF USED("uCrsVacinacao")
		SELECT uCrsVacinacao
		uf_vacinacao_criaCur(uCrsVacinacao.no)
	ELSE
		uf_vacinacao_criaCur()
	ENDIF
ENDFUNC 

FUNCTION uf_vacinacao_getDescricaoPorId
	LPARAMETERS LcId
	LOCAL lcDescricao 

	lcDescricao =""
	
	if(!used("ucResultCombo"))
		RETURN ""
	endif

	IF USED("ucrsTempNomeDesc")
		fecha("ucrsTempNomeDesc")
	ENDIF
	
	SELECT Descricao FROM ucResultCombo WHERE ALLTRIM(STR(ucResultCombo.ID)) == ALLTRIM(LcId) INTO CURSOR ucrsTempNomeDesc readwrite
	
	
	IF(USED("ucrsTempNomeDesc"))
		SELECT ucrsTempNomeDesc
		GO TOP

		lcDescricao  = ALLTRIM(ucrsTempNomeDesc.descricao) 
	ENDIF
	
	IF USED("ucrsTempNomeDesc")
		fecha("ucrsTempNomeDesc")
	ENDIF

	RETURN lcDescricao  

ENDFUNC

Procedure uf_vacinacao_setUpCursorDetails
	LPARAMETERS lcId, lcCursorId, lcCursorDescr
	
	SELECT uCrsVacinacao
	GO TOP
	local lcdescricao 
	lcdescricao = ""


	if(vartype(lcId)=='N')
		lcId= ALLTRIM(STR(lcId))
	ENDIF


	lcdescricao = uf_vacinacao_getDescricaoPorId(lcId)
	

	if(EMPTY(lcdescricao ) OR EMPTY(lcId) )
		lcId= ""
		lcdescricao = ""
	ENDIF
	



	SELECT uCrsVacinacao
	go top
	replace uCrsVacinacao.&lcCursorId.    WITH lcId
	replace uCrsVacinacao.&lcCursorDescr.  WITH lcdescricao


ENDFUNC

FUNCTION uf_vacinacao_scanner

	IF !myVacAlteracao AND !myVacIntroducao
		RETURN .F.
	ENDIF

	LOCAL uv_scanned

	uv_scanned = VACINACAO.txtScanner.value

	VACINACAO.txtScanner.value = ""

	IF EMPTY(uv_scanned)
		RETURN .F.
	ENDIF

	DO CASE 
		CASE LEN(ALLTRIM(uv_scanned)) > 20

			LOCAL lcreftempid, lcRefID, lcloteid
			STORE '' TO lcRefID, lcloteid

			lcreftempid = ALLTRIM(uv_scanned)

			lcreftempid = STRTRAN(lcreftempid , "'", "-")
			lcreftempid = uf_gerais_remove_last_caracter_by_char(lcreftempid,'#')

			IF uf_gerais_retorna_campos_datamatrix_sql(lcreftempid)

				SELECT ucrDataMatrixTemp
				IF ucrDataMatrixTemp.valido

					lcRefID  = uf_gerais_retornaApenasAlphaNumericos(ucrDataMatrixTemp.ref)
					lcloteid = ucrDataMatrixTemp.lote

				ENDIF

			ENDIF

			IF EMPTY(lcrefid)
			
				lcrefid = uf_gerais_getFromString(uv_scanned, '714', 7, .T.)

			ENDIF

			IF EMPTY(lcrefid)
				uf_perguntalt_chama("ERRO NA LEITURA OU C�DIGO DE BARRAS INV�LIDO.", "OK", "", 64)
				RETURN .F.
			ENDIF

			SELECT uCrsVacinacao

			REPLACE uCrsVacinacao.comercial_id WITH ALLTRIM(lcrefid)
			REPLACE uCrsVacinacao.descricao WITH ALLTRIM(uf_gerais_getUmValor("st", "design", "ref = '" + ALLTRIM(lcrefid) + "' and site_nr = " + ASTR(mysite_nr)))
			REPLACE uCrsVacinacao.lote WITH ALLTRIM(lcloteid)

			VACINACAO.refresh()

		OTHERWISE

			LOCAL uv_design
			STORE '' TO uv_design

			uv_design = ALLTRIM(uf_gerais_getUmValor("st", "design", "ref = '" + ALLTRIM(uv_scanned) + "' and site_nr = " + ASTR(mysite_nr)))

			IF !EMPTY(uv_design)

				SELECT uCrsVacinacao

				REPLACE uCrsVacinacao.comercial_id WITH ALLTRIM(uv_scanned)
				REPLACE uCrsVacinacao.descricao WITH ALLTRIM(uv_design)

			ENDIF

			VACINACAO.refresh()

	ENDCASE

ENDFUNC

FUNCTION uf_vacinacao_alteraNomeComercial
	
	IF USED("ucrsCommercialName") AND RECCOUNT("ucrsCommercialName") >0
	
		LOCAL lcComercialName
		lcComercialName = ''
		SELECT ucrsCommercialName
		GO TOP
		lcComercialName = ALLTRIM(ucrsCommercialName.name)	
		
		IF(!EMPTY(lcComercialName))			
			IF(USED("uCrsVacinacao"))
				SELECT uCrsVacinacao
				GO top
				replace  uCrsVacinacao.descricao WITH ALLTRIM(lcComercialName)
			ENDIF		
		ENDIF
		
	ENDIF
			

ENDFUNC


FUNCTION uf_vacinacao_schema_createCursor
	LPARAMETERS lcToken, lcRef
	

		
	regua(1,2,"A obter resposta...")
	
	
	uf_vacinacao_schema_createCursor_gerais()
	
	
	&&tables that need token from another table
	
	uf_vacinacao_createCursor_sendVaccineResp(lcToken)

	
	IF USED("ucrsSendVaccineResp") AND RECCOUNT("ucrsSendVaccineResp")> 0
		SELECT ucrsSendVaccineResp
		IF(ucrsSendVaccineResp.statusCode == 200)
		
			SELECT ucrsSendVaccineResp
			IF(!EMPTY(ucrsSendVaccineResp.registrationsToken))
				uf_vacinacao_createCursor_registrations(ALLTRIM(ucrsSendVaccineResp.registrationsToken))
			ENDIF
			
			SELECT ucrsSendVaccineResp		
			IF(!EMPTY(ucrsSendVaccineResp.sendVaccineRespNoVacMotivesToken))		
				uf_vacinacao_createCursor_sendVaccineRespNoVacMotives(ALLTRIM(ucrsSendVaccineResp.sendVaccineRespNoVacMotivesToken))
			ENDIF
			
			SELECT ucrsSendVaccineResp	
			IF(!EMPTY(ucrsSendVaccineResp.infectionDatesToken))
				uf_vacinacao_createCursor_infectionDates(ALLTRIM(ucrsSendVaccineResp.infectionDatesToken))
			ENDIF
			
			SELECT ucrsSendVaccineResp
			IF(!EMPTY(ucrsSendVaccineResp.vaccineInfoToken))
				uf_vacinacao_schema_createCursor_vaccineInfo(ALLTRIM(ucrsSendVaccineResp.vaccineInfoToken))
			ENDIF
			
		    SELECT ucrsSendVaccineResp
			IF(!EMPTY(ucrsSendVaccineResp.commercialNameToken))		
				uf_vacinacao_schema_createCursor_commercialName(ALLTRIM(ucrsSendVaccineResp.commercialNameToken),ALLTRIM(lcRef))
			ENDIF
			
			**Se receber novo nome comercial altera
			uf_vacinacao_alteraNomeComercial()


			
		ELSE
			SELECT ucrsSendVaccineResp
			uf_perguntalt_chama(ALLTRIM(ucrsSendVaccineResp.statusCodeDesc), "OK", "", 48)
			regua(2)
			RETURN .F.
		ENDIF	
	ENDIF
	regua(2)
	RETURN .T.
ENDFUNC

FUNCTION uf_vacinacao_schema_createCursor_gerais
	LPARAMETERS isRegInterno
	&&tables with no dependises
	uf_vacinacao_createCursor_anatomicLocations(0, isRegInterno)
	
	uf_vacinacao_createCursor_administrationWays(0)
	
	uf_vacinacao_createCursor_lotOrigin(0)
	
	uf_vacinacao_createCursor_side(0)
		
	uf_vacinacao_createCursor_locationAdms(0)
		
	uf_vacinacao_schema_createCursor_comercial_name_clean()
		
ENDFUNC

FUNCTION uf_vacinacao_schema_createCursor_comercial_name_clean

	uf_vacinacao_createCursor_commercialName("","")
	
	uf_vacinacao_createCursor_commercialNameDosage("")
		
	uf_vacinacao_createCursor_commercialNameLots("")
	
	uf_vacinacao_createCursor_commercialNameCode("")


ENDFUNC


FUNCTION uf_vacinacao_schema_createCursor_commercialName
	LPARAMETERS lcToken, lcRef
	
	uf_vacinacao_createCursor_commercialName(lcToken, lcRef)
	
	SELECT ucrsCommercialName
	IF(!EMPTY(ucrsCommercialName.commercialNameDosageToken))
		uf_vacinacao_createCursor_commercialNameDosage(ALLTRIM(ucrsCommercialName.commercialNameDosageToken))		
	ENDIF
	
	SELECT ucrsCommercialName
	IF(!EMPTY(ucrsCommercialName.commercialNameLotsToken))
		uf_vacinacao_createCursor_commercialNameLots(ALLTRIM(ucrsCommercialName.commercialNameLotsToken))
	ENDIF
	
	SELECT ucrsCommercialName
	IF(!EMPTY(ucrsCommercialName.commercialNameCodeToken))
		uf_vacinacao_createCursor_commercialNameCode(ALLTRIM(ucrsCommercialName.commercialNameCodeToken))
	ENDIF
	
ENDFUNC

FUNCTION uf_vacinacao_schema_createCursor_vaccineInfo
	LPARAMETERS lcToken
	
	uf_vacinacao_createCursor_vaccineInfo(lcToken)
	
	IF(USED("ucrsVaccineInfo") AND RECCOUNT("ucrsVaccineInfo")> 0)
				
		SELECT ucrsVaccineInfo	
		IF(!EMPTY(ucrsVaccineInfo.sendVaccineRespNoVacMotivesToken))						
			uf_vacinacao_createCursor_sendVaccineRespNoVacMotives(ALLTRIM(ucrsVaccineInfo.sendVaccineRespNoVacMotivesToken))
		ENDIF
		
		SELECT ucrsVaccineInfo
		IF(!EMPTY(ucrsVaccineInfo.vaccineInfoDiseasesToken))
			uf_vacinacao_createCursor_vaccineInfoDiseases(ALLTRIM(ucrsVaccineInfo.vaccineInfoDiseasesToken))
		ENDIF
		
		SELECT ucrsVaccineInfo 
		IF(!EMPTY(ucrsVaccineInfo.vaccineInfoWarningsToken))
			uf_vacinacao_createCursor_vaccineInfoWarnings(ALLTRIM(ucrsVaccineInfo.vaccineInfoWarningsToken))
		ENDIF
		
		SELECT ucrsVaccineInfo
		IF(!EMPTY(ucrsVaccineInfo.vaccineInfoAdverseReactionToken))			
			uf_vacinacao_createCursor_vaccineInfoAdverseReaction(ALLTRIM(ucrsVaccineInfo.vaccineInfoAdverseReactionToken))
		ENDIF
		
		SELECT ucrsVaccineInfo
		IF(!EMPTY(ucrsVaccineInfo.vaccineInfoAnatomicLocationToken))
			
			uf_vacinacao_createCursor_vaccineInfoAnatomicLocation(ALLTRIM(ucrsVaccineInfo.vaccineInfoAnatomicLocationToken))
		
			IF (USED("ucrsVaccineInfoAnatomicLocation") AND RECCOUNT("ucrsVaccineInfoAnatomicLocation")> 0)
				SELECT ucrsAnatomicLocations
				GO TOP
				DELETE FROM ucrsAnatomicLocations
				
				INSERT INTO ucrsAnatomicLocations(id, description) SELECT id, description FROM ucrsVaccineInfoAnatomicLocation
		
				fecha("ucrsVaccineInfoAnatomicLocation")
			ENDIF
		ENDIF
		SELECT ucrsVaccineInfo
		IF(!EMPTY(ucrsVaccineInfo.vaccineInfoAdministrationWayToken))
			
			uf_vacinacao_createCursor_vaccineInfoAdministrationWay(ALLTRIM(ucrsVaccineInfo.vaccineInfoAdministrationWayToken))
			
			IF (USED("ucrsVaccineInfoAdministrationWay") AND RECCOUNT("ucrsVaccineInfoAdministrationWay")> 0)

				SELECT ucrsAdministrationWays
				GO TOP
				DELETE FROM ucrsAdministrationWays
				
				INSERT INTO ucrsAdministrationWays(id, description) SELECT id, description FROM ucrsVaccineInfoAdministrationWay
	
				fecha("ucrsVaccineInfoAdministrationWay")
			ENDIF
			
		ENDIF
		
	ENDIF
	
ENDFUNC

FUNCTION uf_vacinacao_createCursor_sendVaccineResp
	LPARAMETERS lcToken
	
	LOCAL lcSQLResp
	STORE "" TO lcSQLResp
	
	IF(USED("ucrsSendVaccineResp"))
		fecha("ucrsSendVaccineResp")
	ENDIF
	
	TEXT TO lcSQLResp TEXTMERGE noshow
		exec up_get_sendVaccineResp '<<lcToken>>'
	ENDTEXT
	
	IF !(uf_gerais_actGrelha("","ucrsSendVaccineResp",lcSQLResp))
		uf_perguntalt_chama("OCORREU UM ERRO A CRIAR O CURSOR DE VACINA��O! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .F.
	ENDIF

ENDFUNC

FUNCTION uf_vacinacao_createCursor_anatomicLocations
	LPARAMETERS inactive, isRegInterno
	
	LOCAL lcSQLAnatomic
	STORE "" TO lcSQLAnatomic
	
	IF(USED("ucrsAnatomicLocations"))
		fecha("ucrsAnatomicLocations")
	ENDIF
	
	IF(EMPTY(inactive))
		inactive = 0
	ENDIF
	
	
	
	IF isRegInterno
		TEXT TO lcSQLAnatomic TEXTMERGE noshow
			exec up_get_localoptionsvac'VACINACAO_LOCAL_ANATOMICO'
		ENDTEXT
	ELSE 
		TEXT TO lcSQLAnatomic TEXTMERGE noshow
			exec up_get_anatomicLocations <<inactive>>
		ENDTEXT
	ENDIF 
	
	IF !(uf_gerais_actGrelha("","ucrsAnatomicLocations",lcSQLAnatomic))
		uf_perguntalt_chama("OCORREU UM ERRO A CRIAR O CURSOR DE VACINA��O! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .F.
	ENDIF

ENDFUNC

FUNCTION uf_vacinacao_createCursor_administrationWays
	LPARAMETERS inactives
	
	LOCAL lcSQLAdmniwAy
	STORE "" TO lcSQLAdmniwAy
	
	IF(USED("ucrsAdministrationWays"))
		fecha("ucrsAdministrationWays")
	ENDIF
	
	IF(EMPTY(inactives))
		inactives= 0
	ENDIF
			
	TEXT TO lcSQLAdmniwAy TEXTMERGE noshow
		exec up_get_administrationWays <<inactives>>
	ENDTEXT
		
	IF !(uf_gerais_actGrelha("","ucrsAdministrationWays",lcSQLAdmniwAy))
		uf_perguntalt_chama("OCORREU UM ERRO A CRIAR O CURSOR DE VACINA��O! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .F.
	ENDIF

	RETURN .T.
ENDFUNC

FUNCTION uf_vacinacao_createCursor_lotOrigin
	LPARAMETERS inacti
	
	LOCAL lcSQLLotOrigin
	STORE "" TO lcSQLLotOrigin
	
	IF(USED("ucrsLotOrigin"))
		fecha("ucrsLotOrigin")
	ENDIF
		
	IF(EMPTY(inacti))
		inacti= 0
	ENDIF		

	TEXT TO lcSQLLotOrigin TEXTMERGE noshow
		exec up_get_lotOrigin <<inacti>>
	ENDTEXT
	
	IF !(uf_gerais_actGrelha("","ucrsLotOrigin",lcSQLLotOrigin))
		uf_perguntalt_chama("OCORREU UM ERRO A CRIAR O CURSOR DE VACINA��O! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .F.
	ENDIF
	
	RETURN .T.
ENDFUNC

FUNCTION uf_vacinacao_createCursor_side
	LPARAMETERS inact
	
	LOCAL lcSQLSide
	STORE "" TO lcSQLSide
	
	IF(USED("ucrsSide"))
		fecha("ucrsSide")
	ENDIF
	
	IF(EMPTY(inact))
		inact= 0
	ENDIF
		
	TEXT TO lcSQLSide TEXTMERGE noshow
		exec up_get_side <<inact>>
	ENDTEXT
	
	IF !(uf_gerais_actGrelha("","ucrsSide",lcSQLSide))
		uf_perguntalt_chama("OCORREU UM ERRO A CRIAR O CURSOR DE VACINA��O! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .F.
	ENDIF
	
	RETURN .T.	
ENDFUNC

FUNCTION uf_vacinacao_createCursor_locationAdms
	LPARAMETERS inac
	
	LOCAL lcSQLLocationAdms
	STORE "" TO lcSQLLocationAdms
	
	IF(USED("ucrsLocationAdms"))
		fecha("ucrsLocationAdms")
	ENDIF
	
	IF(EMPTY(inac))
		inac= 0
	ENDIF	
	
	TEXT TO lcSQLLocationAdms TEXTMERGE noshow
		exec up_get_locationAdms <<inac>>
	ENDTEXT
	
	IF !(uf_gerais_actGrelha("","ucrsLocationAdms",lcSQLLocationAdms))
		uf_perguntalt_chama("OCORREU UM ERRO A CRIAR O CURSOR DE VACINA��O! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .F.
	ENDIF

	RETURN .T.
ENDFUNC

FUNCTION uf_vacinacao_createCursor_infectionDates
	LPARAMETERS lcToken
	
	LOCAL lcSQLInfectionDates
	STORE "" TO lcSQLInfectionDates
	
	IF(USED("ucrsInfectionDates"))
		fecha("ucrsInfectionDates")
	ENDIF
	
	TEXT TO lcSQLInfectionDates TEXTMERGE noshow
		exec up_get_infectionDates '<<lcToken>>'
	ENDTEXT
	
	IF !(uf_gerais_actGrelha("","ucrsInfectionDates",lcSQLInfectionDates))
		uf_perguntalt_chama("OCORREU UM ERRO A CRIAR O CURSOR DE VACINA��O! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .F.
	ENDIF
	
	RETURN .T.
ENDFUNC

FUNCTION uf_vacinacao_createCursor_sendVaccineRespNoVacMotives
	LPARAMETERS lcToken
	
	LOCAL lcSQLNoVacMotive
	STORE "" TO lcSQLNoVacMotive
	
	IF(USED("ucrsSendVaccineRespNoVacMotives"))
		fecha("ucrsSendVaccineRespNoVacMotives")
	ENDIF
	
	TEXT TO lcSQLNoVacMotive TEXTMERGE noshow
		exec up_get_sendVaccineRespNoVacMotives '<<lcToken>>'
	ENDTEXT

	IF !(uf_gerais_actGrelha("","ucrsSendVaccineRespNoVacMotives",lcSQLNoVacMotive))
		uf_perguntalt_chama("OCORREU UM ERRO A CRIAR O CURSOR DE VACINA��O! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .F.
	ENDIF

	RETURN .T.
ENDFUNC

FUNCTION uf_vacinacao_createCursor_registrations
	LPARAMETERS lcToken
	

	
	LOCAL lcSQLRegistration
	STORE "" TO lcSQLRegistration
	
	IF(USED("ucrsRegistrations"))
		fecha("ucrsRegistrations")
	ENDIF
	

	lcToken = ALLTRIM(lcToken)
	
	TEXT TO lcSQLRegistration TEXTMERGE noshow
		exec up_get_registrations '<<lcToken>>'
	ENDTEXT

	IF !(uf_gerais_actGrelha("","ucrsRegistrations",lcSQLRegistration))
		uf_perguntalt_chama("OCORREU UM ERRO A CRIAR O CURSOR DE VACINA��O! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .F.
	ENDIF
	
	RETURN .T.
ENDFUNC

FUNCTION uf_vacinacao_createCursor_sendVaccinesCache
	LPARAMETERS lcToken
	
	LOCAL lcSQLCache
	STORE "" TO lcSQLCache
	
	IF(USED("ucrsSendVaccinesCache"))
		fecha("ucrsSendVaccinesCache")
	ENDIF
	
	TEXT TO lcSQLCache TEXTMERGE noshow
		exec up_get_sendVaccinesCache '<<lcToken>>'
	ENDTEXT
	
	IF !(uf_gerais_actGrelha("","ucrsSendVaccinesCache",lcSQLCache))
		uf_perguntalt_chama("OCORREU UM ERRO A CRIAR O CURSOR DE VACINA��O! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .F.
	ENDIF
	
	RETURN .T.
ENDFUNC

FUNCTION uf_vacinacao_createCursor_vaccineInfoDiseases
	LPARAMETERS lcToken
	
	LOCAL lcSQLDiseases
	STORE "" TO lcSQLDiseases
	
	IF(USED("ucrsVaccineInfoDiseases"))
		fecha("ucrsVaccineInfoDiseases")
	ENDIF
	
	TEXT TO lcSQLDiseases TEXTMERGE noshow
		exec up_get_vaccineInfoDiseases '<<lcToken>>'
	ENDTEXT
	
	IF !(uf_gerais_actGrelha("","ucrsVaccineInfoDiseases",lcSQLDiseases))
		uf_perguntalt_chama("OCORREU UM ERRO A CRIAR O CURSOR DE VACINA��O! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .F.
	ENDIF
	
	RETURN .T.
ENDFUNC

FUNCTION uf_vacinacao_createCursor_vaccineInfoWarnings
	LPARAMETERS lcToken
	
	LOCAL lcSQLWarnings
	STORE "" TO lcSQLWarnings
	
	IF(USED("ucrsVaccineInfoWarnings"))
		fecha("ucrsVaccineInfoWarnings")
	ENDIF
	
	TEXT TO lcSQLWarnings TEXTMERGE noshow
		exec up_get_vaccineInfoWarnings '<<lcToken>>'
	ENDTEXT
	
	IF !(uf_gerais_actGrelha("","ucrsVaccineInfoWarnings",lcSQLWarnings))
		uf_perguntalt_chama("OCORREU UM ERRO A CRIAR O CURSOR DE VACINA��O! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .F.
	ENDIF

	RETURN .T.
ENDFUNC

FUNCTION uf_vacinacao_createCursor_vaccineInfoAdverseReaction
	LPARAMETERS lcToken
	
	LOCAL lcSQLReaction
	STORE "" TO lcSQLReaction
	
	IF(USED("ucrsVaccineInfoAdverseReaction"))
		fecha("ucrsVaccineInfoAdverseReaction")
	ENDIF
	
	TEXT TO lcSQLReaction TEXTMERGE noshow
		exec up_get_vaccineInfoAdverseReaction '<<lcToken>>'
	ENDTEXT
	
	IF !(uf_gerais_actGrelha("","ucrsVaccineInfoAdverseReaction",lcSQLReaction))
		uf_perguntalt_chama("OCORREU UM ERRO A CRIAR O CURSOR DE VACINA��O! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .F.
	ENDIF

	RETURN .T.
ENDFUNC

FUNCTION uf_vacinacao_createCursor_vaccineInfoAnatomicLocation
	LPARAMETERS lcToken
	
	LOCAL lcSQLAnatomicLoc
	STORE "" TO lcSQLAnatomicLoc
	
	IF(USED("ucrsVaccineInfoAnatomicLocation"))
		fecha("ucrsVaccineInfoAnatomicLocation")
	ENDIF
	
	TEXT TO lcSQLAnatomicLoc TEXTMERGE noshow
		exec up_get_vaccineInfoAnatomicLocation '<<lcToken>>'
	ENDTEXT
	
	IF !(uf_gerais_actGrelha("","ucrsVaccineInfoAnatomicLocation",lcSQLAnatomicLoc))
		uf_perguntalt_chama("OCORREU UM ERRO A CRIAR O CURSOR DE VACINA��O! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .F.
	ENDIF

	RETURN .T.
ENDFUNC

FUNCTION uf_vacinacao_createCursor_vaccineInfoAdministrationWay
	LPARAMETERS lcToken
	
	LOCAL lcSQLAdminWay
	STORE "" TO lcSQLAdminWay
	
	IF(USED("ucrsVaccineInfoAdministrationWay"))
		fecha("ucrsVaccineInfoAdministrationWay")
	ENDIF
	
	TEXT TO lcSQLAdminWay TEXTMERGE noshow
		exec up_get_vaccineInfoAdministrationWay '<<lcToken>>'
	ENDTEXT
	
	IF !(uf_gerais_actGrelha("","ucrsVaccineInfoAdministrationWay",lcSQLAdminWay))
		uf_perguntalt_chama("OCORREU UM ERRO A CRIAR O CURSOR DE VACINA��O! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .F.
	ENDIF

	RETURN .T.
ENDFUNC

FUNCTION uf_vacinacao_createCursor_vaccineInfo
	LPARAMETERS lcToken
	
	LOCAL lcSQLVaccineInfo
	STORE "" TO lcSQLVaccineInfo
	
	IF(USED("ucrsVaccineInfo"))
		fecha("ucrsVaccineInfo")
	ENDIF
	
	TEXT TO lcSQLVaccineInfo TEXTMERGE noshow
		exec up_get_vaccineInfo '<<lcToken>>'
	ENDTEXT
	
	IF !(uf_gerais_actGrelha("","ucrsVaccineInfo",lcSQLVaccineInfo))
		uf_perguntalt_chama("OCORREU UM ERRO A CRIAR O CURSOR DE VACINA��O! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .F.
	ENDIF

	RETURN .T.
ENDFUNC

FUNCTION uf_vacinacao_createCursor_commercialNameDosage
	LPARAMETERS lcToken
	
	LOCAL lcSQLComerDosage
	STORE "" TO lcSQLComerDosage
	
	IF(USED("ucrsCommercialNameDosage"))
		fecha("ucrsCommercialNameDosage")
	ENDIF
	
	TEXT TO lcSQLComerDosage TEXTMERGE noshow
		exec up_get_commercialNameDosage '<<lcToken>>'
	ENDTEXT
	
	IF !(uf_gerais_actGrelha("","ucrsCommercialNameDosage",lcSQLComerDosage))
		uf_perguntalt_chama("OCORREU UM ERRO A CRIAR O CURSOR DE VACINA��O! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .F.
	ENDIF

	RETURN .T.
ENDFUNC

FUNCTION uf_vacinacao_createCursor_commercialNameLots
	LPARAMETERS lcToken
	
	LOCAL lcSQLLots
	STORE "" TO lcSQLLots
	
	IF(USED("ucrsCommercialNameLots"))
		fecha("ucrsCommercialNameLots")
	ENDIF
	
	TEXT TO lcSQLLots TEXTMERGE noshow
		exec up_get_commercialNameLots '<<lcToken>>'
	ENDTEXT
	
	IF !(uf_gerais_actGrelha("","ucrsCommercialNameLots",lcSQLLots))
		uf_perguntalt_chama("OCORREU UM ERRO A CRIAR O CURSOR DE VACINA��O! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .F.
	ENDIF

	RETURN .T.
ENDFUNC

FUNCTION uf_vacinacao_createCursor_commercialNameCode
	LPARAMETERS lcToken
	
	LOCAL lcSQLCode
	STORE "" TO lcSQLCode
	
	IF(USED("ucrsCommercialNameCode"))
		fecha("ucrsCommercialNameCode")
	ENDIF
	
	TEXT TO lcSQLCode TEXTMERGE noshow
		exec up_get_commercialNameCode '<<lcToken>>'
	ENDTEXT
	
	IF !(uf_gerais_actGrelha("","ucrsCommercialNameCode",lcSQLCode))
		uf_perguntalt_chama("OCORREU UM ERRO A CRIAR O CURSOR DE VACINA��O! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .F.
	ENDIF

	RETURN .T.
ENDFUNC

FUNCTION uf_vacinacao_createCursor_commercialName
	LPARAMETERS lcToken, lcRef
	
	LOCAL lcSQLComercialName
	STORE "" TO lcSQLComercialName
	
	IF(USED("ucrsCommercialName"))
		fecha("ucrsCommercialName")
	ENDIF
	
	IF(EMPTY(lcToken))
		lcToken = ""
	ENDIF
	
	IF(EMPTY(lcRef))
		lcRef= ""
	ENDIF
	
	
	
	TEXT TO lcSQLComercialName TEXTMERGE noshow
		exec up_get_commercialName '<<ALLTRIM(lcToken)>>', '<<ALLTRIM(lcRef)>>'
	ENDTEXT
	
	IF !(uf_gerais_actGrelha("","ucrsCommercialName",lcSQLComercialName))
		uf_perguntalt_chama("OCORREU UM ERRO A CRIAR O CURSOR DE VACINA��O! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .F.
	ENDIF

	RETURN .T.
ENDFUNC

FUNCTION uf_vacinacao_closeCursors

	IF(USED("ucrsSendVaccineResp"))
		fecha("ucrsSendVaccineResp")
	ENDIF
	
	IF(USED("ucrsAnatomicLocations"))
		fecha("ucrsAnatomicLocations")
	ENDIF
	
	IF(USED("ucrsAdministrationWays"))
		fecha("ucrsAdministrationWays")
	ENDIF
	
	IF(USED("ucrsLotOrigin"))
		fecha("ucrsLotOrigin")
	ENDIF
	
	IF(USED("ucrsSide"))
		fecha("ucrsSide")
	ENDIF
	
	IF(USED("ucrsLocationAdms"))
		fecha("ucrsLocationAdms")
	ENDIF
	
	IF(USED("ucrsInfectionDates"))
		fecha("ucrsInfectionDates")
	ENDIF
	
	IF(USED("ucrsSendVaccineRespNoVacMotives"))
		fecha("ucrsSendVaccineRespNoVacMotives")
	ENDIF
	
	IF(USED("ucrsRegistrations"))
		fecha("ucrsRegistrations")
	ENDIF
		
	IF(USED("ucrsSendVaccinesCache"))
		fecha("ucrsSendVaccinesCache")
	ENDIF
		
	IF(USED("ucrsVaccineInfoDiseases"))
		fecha("ucrsVaccineInfoDiseases")
	ENDIF
		
	IF(USED("ucrsVaccineInfoWarnings"))
		fecha("ucrsVaccineInfoWarnings")
	ENDIF
		
	IF(USED("ucrsVaccineInfoAdverseReaction"))
		fecha("ucrsVaccineInfoAdverseReaction")
	ENDIF
		
	IF(USED("ucrsVaccineInfoAnatomicLocation"))
		fecha("ucrsVaccineInfoAnatomicLocation")
	ENDIF
		
	IF(USED("ucrsVaccineInfoAdministrationWay"))
		fecha("ucrsVaccineInfoAdministrationWay")
	ENDIF
		
	IF(USED("ucrsVaccineInfo"))
		fecha("ucrsVaccineInfo")
	ENDIF
		
	IF(USED("ucrsCommercialNameDosage"))
		fecha("ucrsCommercialNameDosage")
	ENDIF
		
	IF(USED("ucrsCommercialNameLots"))
		fecha("ucrsCommercialNameLots")
	ENDIF
		
	IF(USED("ucrsCommercialNameCode"))
		fecha("ucrsCommercialNameCode")
	ENDIF
		
	IF(USED("ucrsCommercialName"))
		fecha("ucrsCommercialName")
	ENDIF
	
	IF(USED("ucrsVaccineRequest"))
		fecha("ucrsVaccineRequest")
	ENDIF
	
	IF(USED("ucrsVacCache"))
		fecha("ucrsVacCache")
	ENDIF
			
	RELEASE myCallVaccineService
	RELEASE myCallVaccineCallendarService
ENDFUNC

FUNCTION uf_vacinacao_request
	LPARAMETERS lcType, lcTypeDesc, lcTypeSend, lcTypeSendDesc, lcTeste, lcSaveStamp, lcBreakStamp
	
	LOCAL lcSQLInsert, lcToken, lcSearch, lcProfId, lcProfName, lcCode, lcSenderAssoc, lcSenderAssocId, lcSenderId, lcSenderName, lcTeste, lcSite, lcSigla
	STORE '' TO lcSQLInsert, lcToken, lcSearch, lcProfId, lcProfName, lcCode, lcSenderAssoc, lcSenderAssocId, lcSenderId, lcSenderName, lcSite, lcSigla
	
	IF uf_gerais_getparameter("ADM0000000227", "BOOL")
		lcteste = 1
	ELSE
		lcteste = 0
	ENDIF
	
	lcSearch = IIF(uf_gerais_compStr(VACINACAO.chkBreak.tag, "true"), '0', Vacinacao.txtSNS.value)
	IF(EMPTY(lcSearch)) &&AND EMPTY(lcBreakStamp)
		uf_perguntalt_chama("POR FAVOR PREENCHA UM N�MERO DO SNS V�LIDO!","OK","",48)
		RETURN
	ENDIF
	
	&& n�mero da cedula
	IF uf_gerais_checkCedula()
		lcProfId  = uf_gerais_removeAllLeterFromString(ucrsuser.drcedula)
    ENDIF
    
	SELECT ucrsuser

    && professional nome
	lcProfName = ucrsuser.nome &&Vacinacao.txtAdministrante.value
	IF(EMPTY(lcProfName))
		IF EMPTY(ucrsuser.nome)   
      		uf_perguntalt_chama("Tem de preencher o campo 'Nome' na ficha do utilizador para utilizar esta funcionalidade.", "OK", "", 48)
      		RETURN .F.
      	ELSE
      		lcProfName = ALLTRIM(ucrsuser.nome)
    	ENDIF
	ENDIF
	
	&& iniciais do utilizador	
	IF(EMPTY(ucrsuser.iniciais))
		lcSigla = ""
    ELSE
    	lcSigla = ALLTRIM(ucrsuser.iniciais)
	ENDIF
	
	SELECT ucrsVacinacao

	&& codigo da vaccina
   IF EMPTY(ucrsVacinacao.codeVacina)
      IF(EMPTY(Vacinacao.txtComercial_id.value) and lcTypeSend!=2)
         uf_perguntalt_chama("Tem de preencher o campo 'Referencia' na vacina��o.", "OK", "", 48)
         Vacinacao.txtComercial_id.setfocus()
         RETURN .F.
      ELSE
         TEXT TO lcSQL NOSHOW TEXTMERGE
            select TOP 1 isnull(vaccineCode,'') as vacCode, ref from st(nolock)  where ref ='<<Vacinacao.txtComercial_id.value>>' and site_nr= <<mySite_nr>>
         ENDTEXT  
         
         uf_gerais_actgrelha("", "ucrsInfoVacTemp", lcSQL)
         
         lcCode = ALLTRIM(ucrsInfoVacTemp.vacCode)
         fecha("ucrsInfoVacTemp")
      ENDIF
   ELSE
      lcCode = ALLTRIM(ucrsVacinacao.codeVacina)
   ENDIF

	SELECT ucrse1
	GO TOP
	&& associacao
	IF(EMPTY(ucrse1.u_assfarm))
	    uf_perguntalt_chama("Tem de preencher o campo 'Associa��o' na ficha da empresa para utilizar esta funcionalidade.", "OK", "", 48)
    	RETURN .F.
	ELSE
		lcSenderAssoc = ucrse1.u_assfarm
	ENDIF
	
	&& codigo da associacao
	IF(EMPTY(ucrse1.u_codfarm))
	    uf_perguntalt_chama("Tem de preencher o campo 'N� da Associa��o' na ficha da empresa para utilizar esta funcionalidade.", "OK", "", 48)
    	RETURN .F.
	ELSE
		lcSenderAssocId = ucrse1.u_codfarm
	ENDIF
	
	&& id da base dados
	IF(EMPTY(ucrse1.id_lt))
	    uf_perguntalt_chama("Erro ao obter informa��es da base de dados. Por favor Contacte o suporte.", "OK", "", 48)
    	RETURN .F.
	ELSE
		lcSenderId = ucrse1.id_lt
	ENDIF
	
	&& nome da empresa
	IF(EMPTY(ucrse1.nomecomp))
	    uf_perguntalt_chama("Tem de preencher o campo 'Nome da empresa' na ficha da empresa para utilizar esta funcionalidade.", "OK", "", 48)
    	RETURN .F.
	ELSE
		lcSenderName = ucrse1.nomecomp
	ENDIF
	
	&& loja
	IF(EMPTY(mySite))
		uf_perguntalt_chama("Erro ao obter loja. Por favor Contacte o suporte.", "OK", "", 48)
    	RETURN .F.
	ELSE
		lcSite = mySite
	ENDIF

	lcToken = uf_gerais_gerarIdentifiers(36)

	
	if(EMPTY(lcBreakStamp))
		lcBreakStamp = ""
	ENDIF
	if(EMPTY(lcSaveStamp))
		lcSaveStamp = ""
	ENDIF
		
	TEXT TO lcSQLInsert NOSHOW TEXTMERGE 
		exec up_save_sendVaccine '<<ALLTRIM(lcToken)>>', '<<ALLTRIM(lcSearch)>>', '<<ALLTRIM(lcProfId)>>', '<<ALLTRIM(lcProfName)>>', '<<ALLTRIM(lcCode)>>', '<<ALLTRIM(lcSenderAssoc)>>', '<<ALLTRIM(lcSenderAssocId)>>', '<<ALLTRIM(lcSenderId)>>', '<<ALLTRIM(lcSenderName)>>', <<lcType>>, '<<ALLTRIM(lcTypeDesc)>>', <<lcTypeSend>>, '<<ALLTRIM(lcTypeSendDesc)>>', <<lcTeste>>, '<<ALLTRIM(lcSite)>>', '<<ALLTRIM(lcSaveStamp)>>', '<<ALLTRIM(lcBreakStamp)>>', '<<ALLTRIM(lcSigla)>>',
		'<<ALLTRIM(uCrsVacinacao.comercial_id)>>', <<IIF(EMPTY(uCrsVacinacao.dosagem), '0', uCrsVacinacao.dosagem)>>, 
		<<IIF(EMPTY(uCrsVacinacao.unidades), '0', uCrsVacinacao.unidades)>>, 
		<<IIF(EMPTY(VACINACAO.txtMotiBreak.tag), '0', ALLTRIM(VACINACAO.txtMotiBreak.tag))>>, <<IIF(EMPTY(VACINACAO.txtDestinoBreak.tag), '0', ALLTRIM(VACINACAO.txtDestinoBreak.tag))>>
	ENDTEXT
	
	IF !uf_gerais_actGrelha("", "", lcSQLInsert)
		uf_perguntalt_chama("OCORREU UM ERRO A CRIAR O CURSOR DE VACINA��O! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .F.
	ENDIF

	RETURN lcToken

ENDFUNC

FUNCTION uf_vacinacao_getRequest
	LPARAMETERS lcToken
	
	IF TYPE("lcToken") <> "C"
		RETURN .F.
	ENDIF

	LOCAL lcSQLInsert
	STORE '' TO lcSQLInsert
	
	TEXT TO lcSQLInsert NOSHOW TEXTMERGE 
		exec up_get_sendVaccine '<<ALLTRIM(lcToken)>>'
	ENDTEXT
	
	
	IF !uf_gerais_actGrelha("", "ucrsVaccineRequest", lcSQLInsert)
		uf_perguntalt_chama("OCORREU UM ERRO A CRIAR O CURSOR DE VACINA��O! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
	ENDIF
ENDFUNC

FUNCTION uf_vacinacao_callServise
	LPARAMETERS lcWaitingMessage
	
	LOCAL lcWsPath,lcWsParams,lcWsCmd,lcToken, lcIdLt
	STORE '' TO lcWsPath, lcWsParams, lcToken, lcWsCmd, lcIdLt
	STORE '&' TO lcAdd

	
	regua(0,4,lcWaitingMessage)
	
	lcNomeJar = 'ltsVaccineCli.jar'	
	lcWsPath = ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\ltsVaccineCli\' + ALLTRIM(lcNomeJar)
	lcWsDir	= ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\ltsVaccineCli'
	
	&& validate path to file
	IF !FILE(lcWsPath)
		uf_perguntalt_chama("FICHEIRO DE CONFIGURA��O N�O ENCONTRADO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		regua(2)
		RETURN .f.
	ENDIF 
	
	IF(!USED("ucrsVaccineRequest"))
		uf_perguntalt_chama("Cursor para efetuar o pedido nao encontrado. Por favor contacte o suporte.","OK","",16)
		regua(2)
		RETURN .f.
	ENDIF
	
	SELECT ucrsVaccineRequest
	GO TOP
	
	lcToken = ucrsVaccineRequest.token
	
	lcIdLt = ucrsVaccineRequest.senderId
	
	IF(EMPTY(lcToken) OR EMPTY(lcIdLt))
		uf_perguntalt_chama("Erro ou obter o token e base de dados liga��o. Por favor contacte o suporte.","OK","",16)
		regua(2)
		RETURN .f.
	ENDIF
	
	regua(1,3,lcWaitingMessage)
	
	lcWsParams = ' "--idCl=' 	+  ALLTRIM(lcIdLt) 	+ ["] +; 
				 ' "--TOKEN=' 		+  ALLTRIM(lcToken) + ["] 
				 				 
	regua(1,5,lcWaitingMessage)

	&&execute
    lcWsCmd = "cmd /c cd /d " + lcWsDir+ " "+  lcAdd + lcAdd + " javaw -Dfile.encoding=UTF-8  -jar " + lcWsPath + " " + lcWsParams 
	
	IF uf_gerais_getparameter("ADM0000000227", "BOOL")==.T.
	   	uf_perguntalt_chama("Webservice de teste...", "OK", "", 64)
	    _CLIPTEXT = lcWsCmd
	ENDIF

	oWSShell = CREATEOBJECT("WScript.Shell")
	oWSShell.Run(lcWsCmd, 0, .t.)
	
	regua(1,7,lcWaitingMessage)
	
	regua(2)
	
ENDFUNC

FUNCTION uf_vacinacao_getRequesCallServiseCreateCursors
	LPARAMETERS lcType, lcTypeDesc, lcTypeSend, lcTypeSendDesc, lcTeste, lcRegua, lcSaveStamp, lcBreakStamp, lcRef
	
	LOCAL lcTokenRequest
	STORE '' TO lcTokenRequest

	lcTokenRequest = uf_vacinacao_request(lcType, lcTypeDesc, lcTypeSend, lcTypeSendDesc, lcTeste, lcSaveStamp, lcBreakStamp)

	IF TYPE("lcTokenRequest") <> "C"
		RETURN .F.
	ENDIF

	uf_vacinacao_getRequest(lcTokenRequest)
	
	IF(!uf_vacinacao_callServise(lcRegua))
		RETURN .F.
	ENDIF
	
	IF(!uf_vacinacao_schema_createCursor(lcTokenRequest, lcRef))
		RETURN .F.
	ENDIF
	
	RETURN .T.
ENDFUNC

FUNCTION uf_vacinacao_verifyToCallService
	LPARAMETERS lcType, lcTypeDesc, lcTypeSend, lcTypeSendDesc, lcTeste, lcRegua, lcUsedReadOnly, lcSaveStamp, lcBreakStamp, lcSemRef
	
	local lcRef
	lcRef = ""
	
	IF uf_gerais_compStr(VACINACAO.chkVacinacao.tag, "true")
		&& codigo da vaccina
		&& a consulta historico n�o valida 

	  SELECT ucrsVacinacao

      IF EMPTY(ucrsVacinacao.codeVacina)
		
         IF(EMPTY(Vacinacao.txtComercial_id.value) and lcTypeSend!=2)
            uf_perguntalt_chama("Tem de preencher o campo 'Referencia' na vacina��o.", "OK", "", 48)
            Vacinacao.txtComercial_id.setfocus()
            RETURN .F.
         ENDIF
         
         lcRef = ALLTRIM(Vacinacao.txtComercial_id.value)

      ENDIF

		IF EMPTY(ucrsuser.nome)   
			uf_perguntalt_chama("Tem de preencher o campo 'Nome' na ficha do operador para utilizar esta funcionalidade.", "OK", "", 48)
			RETURN .F.
		ENDIF

		IF !uf_gerais_checkCedula()
			RETURN .F.
		ENDIF

		IF(EMPTY(Vacinacao.txtSNS.value)) 
			uf_perguntalt_chama("POR FAVOR PREENCHA UM N�MERO DO SNS V�LIDO!","OK","",48)
			Vacinacao.txtSNS.setfocus()
			RETURN .F.
		ENDIF

		IF !lcSemRef
			lcRef = ALLTRIM(Vacinacao.txtComercial_id.value)
		ELSE
			lcRef = ""
		ENDIF
				
		IF(!uf_vacinacao_getRequesCallServiseCreateCursors(lcType, lcTypeDesc, lcTypeSend, lcTypeSendDesc, lcTeste, lcRegua, lcSaveStamp, lcBreakStamp, lcRef))
			RETURN .F.
		ENDIF
		
		RETURN .T.
	ENDIF
	
	IF uf_gerais_compStr(VACINACAO.chkInjetavel.tag, "true")
		
		uf_vacinacao_schema_createCursor_gerais(.T.)
			
	ENDIF

	IF uf_gerais_compStr(VACINACAO.chkBreak.tag, "true")
	
		IF !lcSemRef
			lcRef = ALLTRIM(Vacinacao.txtComercial_id.value)
		ELSE
			lcRef = ""
		ENDIF

		SELECT ucrsuser

		IF EMPTY(ucrsuser.nome)   
			uf_perguntalt_chama("Tem de preencher o campo 'Nome' na ficha do operador para utilizar esta funcionalidade.", "OK", "", 48)
			RETURN .F.
		ENDIF

		IF !uf_gerais_checkCedula()
			RETURN .F.
		ENDIF

		IF(!uf_vacinacao_getRequesCallServiseCreateCursors(lcType, lcTypeDesc, lcTypeSend, lcTypeSendDesc, lcTeste, lcRegua, lcSaveStamp, lcBreakStamp, lcRef))
			RETURN .F.
		ENDIF

	ENDIF
	
	RETURN .T.
ENDFUNC

FUNCTION uf_vacinacao_checkCacheToUpdate

	LOCAL lcSQLCache, lcResult
	STORE '' TO lcSQLCache
	
	TEXT TO lcSQLCache NOSHOW TEXTMERGE 
		exec up_checkToUpdate_sendVaccinesCache
	ENDTEXT
	
	IF !uf_gerais_actGrelha("", "ucrsVacCache", lcSQLCache)
		uf_perguntalt_chama("OCORREU UM ERRO A CRIAR O CURSOR DE VACINA��O! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
	ENDIF
	
	SELECT ucrsVacCache
	GO TOP 
	
	lcResult = ucrsVacCache.timeLeft
	
	RETURN lcResult
	
ENDFUNC

FUNCTION uf_vacinacao_checkRefCovdGripe
	LPARAMETERS lcRef, lcTipoVacina
	
	LOCAL lcSQLVcin
	STORE '' TO lcSQLVcin
	
	TEXT TO lcSQLVcin NOSHOW TEXTMERGE 
		SELECT dbo.uf_verifyGripeCovid( '<<ALLTRIM(lcRef)>>', <<lcTipoVacina>>) AS res
	ENDTEXT
	
	IF !uf_gerais_actGrelha("", "ucrsVacGripe", lcSQLVcin)
		uf_perguntalt_chama("OCORREU UM ERRO A CRIAR O CURSOR DE VACINA��O! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
	ENDIF
	
	SELECT ucrsVacGripe
	IF(ucrsVacGripe.res = .T.)
		IF USED("ucrsVacGripe")
			fecha("ucrsVacGripe")
		ENDIF
		RETURN .T.
	ELSE
		IF USED("ucrsVacGripe")
			fecha("ucrsVacGripe")
		ENDIF
		RETURN .F.
	ENDIF	
		
ENDFUNC

FUNCTION uf_vacinacao_allowsUpdate
	
	IF uf_gerais_compStr("ADM", m_chinis)
		RETURN .T.
	ENDIF

	LOCAL lcStamp, lcSQLAllowUpdate
	STORE '' TO lcStamp, lcSQLAllowUpdate	
	
	SELECT uCrsVacinacao
	IF uCrsVacinacao.send AND uCrsVacinacao.result = 1
		uf_perguntalt_chama("N�o � permitido editar um registo de vacina j� comunicada com sucesso.","OK","",16)
		RETURN .F.
	ENDIF

	RETURN .T.
ENDFUNC

FUNCTION uf_vacinacao_camposObrigatorios
   
   	IF uf_gerais_compStr(VACINACAO.chkBreak.tag, "false")

		&& Valida��es
		SELECT uCrsVacinacao
		
		IF EMPTY(ALLTRIM(uCrsVacinacao.nome))
			uf_perguntalt_chama("POR FAVOR PREENCHA O NOME DO CLIENTE!","OK","",48)
			RETURN .f.
		ENDIF
		
		IF EMPTY(ALLTRIM(uCrsVacinacao.comercial_id))
			uf_perguntalt_chama("POR FAVOR PREENCHA A REF� DA VACINA!","OK","",48)
			RETURN .f.
		ENDIF
		
		IF EMPTY(DTOC(uCrsVacinacao.DataNasc))
			uf_perguntalt_chama("A DATA DE NASCIMENTO � INV�LIDA!","OK","",48)
			RETURN .f.
		ENDIF		
		
		IF uf_vacinacao_checkRefCovdGripe(uCrsVacinacao.codeVacina, 3)	
			IF uf_vacinacao_valIdade() AND uf_gerais_getIdade(uCrsVacinacao.DataNasc)< uf_gerais_getParameter("ADM0000000347", "NUM")
				uf_perguntalt_chama("A idade � inv�lida para receber esta vacina!","OK","",48)
				RETURN .f.
			ENDIF
		ENDIF	
		
		IF (EMPTY(uCrsVacinacao.nrSns) OR LEN(ALLTRIM(uCrsVacinacao.nrSns))!=9) AND !uCrsVacinacao.autResidencia AND uf_gerais_compStr(VACINACAO.chkInjetavel.tag, "false")
			uf_perguntalt_chama("POR FAVOR PREENCHA UM N�MERO DO SNS V�LIDO!","OK","",48)
			RETURN .f.
		ENDIF

		IF uCrsVacinacao.autResidencia 
			IF !EMPTY(uCrsVacinacao.nrSns) AND LEN(ALLTRIM(uCrsVacinacao.nrSns))!=9 AND uf_gerais_compStr(VACINACAO.chkInjetavel.tag, "false")
				uf_perguntalt_chama("POR FAVOR PREENCHA UM N�MERO DO SNS V�LIDO!","OK","",48)
				RETURN .f.
			ENDIF
		ENDIF

		IF EMPTY(ALLTRIM(uCrsVacinacao.typeVacinacao)) AND uf_gerais_compStr(VACINACAO.chkVacinacao.tag, "true")
			uf_perguntalt_chama("POR FAVOR PREENCHA O TIPO DE VACINACAO!","OK","",48)
			RETURN .f.
		ENDIF

		IF EMPTY(ALLTRIM(uCrsVacinacao.typeUtente)) AND uf_gerais_compStr(VACINACAO.chkVacinacao.tag, "true")
			uf_perguntalt_chama("POR FAVOR PREENCHA O TIPO DE UTENTE!","OK","",48)
			RETURN .f.
		ENDIF

		
		IF EMPTY(ALLTRIM(uCrsVacinacao.ADMINISTRANTE))
			uf_perguntalt_chama("POR FAVOR PREENCHA O NOME DO ADMINISTRANTE!","OK","",48) 
			RETURN .f.
		ENDIF
		
		IF EMPTY(DTOC(uCrsVacinacao.dataAdministracao))
			uf_perguntalt_chama("A DATA DE ADMINISTRA��O � INV�LIDA!","OK","",48)
			RETURN .f.
		ENDIF		
		
			
		IF EMPTY(ALLTRIM(uCrsVacinacao.lote))
			uf_perguntalt_chama("POR FAVOR PREENCHA O LOTE!","OK","",48)
			RETURN .f.
		ENDIF
		
		IF EMPTY(ALLTRIM(uCrsVacinacao.viaAdmin_id))
			uf_perguntalt_chama("POR FAVOR VALIDE A VIA DE ADMINISTRA��O","OK","",48)
			RETURN .f.
		ENDIF
			
		IF EMPTY(uCrsVacinacao.unidades) AND uf_gerais_compStr(VACINACAO.chkInjetavel.tag, "false")
			uf_perguntalt_chama("POR FAVOR PREENCHA AS UNIDADES!","OK","",48)
			RETURN .f.
		ENDIF
			
		IF EMPTY(uCrsVacinacao.dosagem) AND uf_gerais_compStr(VACINACAO.chkInjetavel.tag, "false")
			uf_perguntalt_chama("POR FAVOR PREENCHA A DOSAGEM!","OK","",48)
			RETURN .f.
		ENDIF

		IF EMPTY(ALLTRIM(uCrsVacinacao.localAdmistracao)) AND uf_gerais_compStr(VACINACAO.chkVacinacao.tag, "true")
			uf_perguntalt_chama("POR FAVOR PREENCHA O LOCAL DE ADMINISTRACAO!","OK","",48)
			RETURN .f.
		ENDIF

		IF EMPTY(ALLTRIM(uCrsVacinacao.localAnatomico_id)) AND uf_gerais_compStr(VACINACAO.chkInjetavel.tag, "false")
			uf_perguntalt_chama("POR FAVOR PREENCHA O LOCAL ANATOMICO!","OK","",48)
			RETURN .f.
		ENDIF
		
		IF EMPTY(ALLTRIM(uCrsVacinacao.descricao)) 
			uf_perguntalt_chama("POR FAVOR PREENCHA A DESCRI��O!","OK","",48)
			RETURN .f.
		ENDIF	

   	ELSE

		IF EMPTY(uCrsVacinacao.unidades)
			uf_perguntalt_chama("POR FAVOR PREENCHA AS UNIDADES!","OK","",48)
			RETURN .f.
		ENDIF
			
		IF EMPTY(ALLTRIM(uCrsVacinacao.descricao)) 
			uf_perguntalt_chama("POR FAVOR PREENCHA A DESCRI��O!","OK","",48)
			RETURN .f.
		ENDIF	

		IF EMPTY(ALLTRIM(uCrsVacinacao.motivoBreak))
			uf_perguntalt_chama("POR FAVOR PREENCHA O MOTIVO DE QUEBRA!","OK","",48)
			RETURN .f.
		ENDIF

		IF EMPTY(ALLTRIM(uCrsVacinacao.destinoBreak))
			uf_perguntalt_chama("POR FAVOR PREENCHA O DESTINO DE QUEBRA!","OK","",48)
			RETURN .f.
		ENDIF

   	ENDIF

   RETURN .T.

ENDFUNC

FUNCTION uf_vacinacao_selVacCode

	IF myVacIntroducao OR myVacAlteracao
		RETURN .F.
	ENDIF

	IF !USED("ucrsVacinacao")
		uf_perguntalt_chama("N�o selecionou nenhum registo.", "OK", "", 48)
		RETURN .F.
	ENDIF

	IF EMPTY(ucrsVacinacao.correlationId)
		uf_perguntalt_chama("Este registo n�o tem C�digo de Vacina��o.", "OK", "", 48)
		RETURN .F.
	ENDIF

	IF ucrsVacinacao.anulado
		uf_perguntalt_chama("Este registo est� marcado como anulado.", "OK", "", 48)
		RETURN .F.
	ENDIF

	IF VACINACAO.applyPlan

		IF TYPE("upv_nrCodVac") == "U"
			uf_perguntalt_chama("Erro a encontrar C�digo de Vacina��o." + CHR(13) + "Por favor contacte o suporte.", "OK", "", 48)
			RETURN .F.
		ENDIF

		upv_nrCodVac = ALLTRIM(ucrsVacinacao.correlationId)

		uf_Vacinacao_sair()

	ELSE

		IF WEXIST("ATENDIMENTO")
			
			IF !USED("fi")
				uf_perguntalt_chama("N�o foi poss�vel encontrar o cabe�alho.", "OK", "", 48)
				RETURN .F.
			ENDIF

			IF !uf_gerais_compStr(LEFT(fi.design,1), '.')
				uf_perguntalt_chama("A linha selecionado no atendimento n�o � um cabe�alho de venda.", "OK", "", 48)
				RETURN .F.
			ENDIF

			IF !USED("UCRSATENDCOMP")
				uf_perguntalt_chama("N�o selecionou nenhum plano.", "OK", "", 48)
				RETURN .F.
			ENDIF

			FECHA("uc_tmpAtendComp")

			SELECT FI

			LOCAL lcfistamp
			lcfistamp = fi.fistamp

			SELECT * FROM UCRSATENDCOMP WITH (BUFFERING = .T.) WHERE uf_gerais_compStr(UCRSATENDCOMP.fistamp, lcfistamp) INTO CURSOR uc_tmpAtendComp

			IF RECCOUNT("uc_tmpAtendComp") = 0
				uf_perguntalt_chama("O cabe�alho selecionado n�o tem nenhum plano associado.", "OK", "", 48)
				RETURN .F.
			ENDIF

			SELECT uc_tmpAtendComp

			IF !uc_tmpAtendComp.obrigaRegistoVacinacao
				uf_perguntalt_chama("O plano selecionada n�o � para Registo de Vacina��o.", "OK", "", 48)
				RETURN .F.
			ENDIF

			VACINACAO.hide()

			SELECT ucrsVacinacao

			uf_atendimento_pedenrreceitafo(UPPER(ALLTRIM(uc_tmpAtendComp.cptorgabrev)), UPPER(ALLTRIM(uc_tmpAtendComp.cptorgabrev2)), uc_tmpAtendComp.obriga_nrreceita, lcfistamp, ALLTRIM(ucrsVacinacao.correlationId))

			uf_vacinacao_sair()

		ENDIF

		IF WEXIST("FACTURACAO")

			IF !USED("ft")
				uf_perguntalt_chama("N�o foi poss�vel encontrar o cabe�alho.", "OK", "", 48)
				RETURN .F.
			ENDIF

			IF !USED("ft2")
				uf_perguntalt_chama("N�o foi poss�vel encontrar o cabe�alho.", "OK", "", 48)
				RETURN .F.
			ENDIF

			SELECT FT2 
			IF EMPTY(ft2.u_codigo)
				uf_perguntalt_chama("N�o selecionou nenhum plano.", "OK", "", 48)
				RETURN .F.
			ENDIF

			IF !uf_gerais_getUmValor("cptpla", "obrigaRegistoVacinacao", "codigo = '" + ALLTRIM(UPPER(ft2.u_codigo)) + "'")
				uf_perguntalt_chama("O plano selecionada n�o � para Registo de Vacina��o.", "OK", "", 48)
				RETURN .F.
			ENDIF

			SELECT ft2
			REPLACE ft2.u_receita WITH ALLTRIM(ucrsVacinacao.correlationId)

			FACTURACAO.refresh()

			uf_vacinacao_sair()

		ENDIF

	ENDIF

ENDFUNC

FUNCTION uf_vacinacao_hist

	uf_vacinacaoHistorico_chama(ALLTRIM(VACINACAO.txtSNS.value))

ENDFUNC

FUNCTION uf_vacinacao_actIdade
	LPARAMETERS uv_dataNasc

	LOCAL uv_idade

	uv_idade = uf_gerais_getIdade(uv_dataNasc)

	IF !INLIST(uv_idade, -1, 0)
		RETURN ASTR(uv_idade)
	ELSE
		RETURN ''
	ENDIF

ENDFUNC

FUNCTION uf_vacinacao_valIdade

	IF !USED("ucrsVacinacao")
		RETURN .T.
	ENDIF

	SELECT ucrsVacinacao

	LOCAL uv_sql, uv_valIdade

	TEXT TO uv_sql TEXTMERGE NOSHOW
		exec up_vacinacao_validaIdade '<<ALLTRIM(ucrsVacinacao.typeUtente)>>'
	ENDTEXT

	IF !uf_gerais_actGrelha("", "uc_valIdade", uv_sql)
		uf_perguntalt_chama("Erro a verificar 'valIdade' para o Tipo de Utente." + CHR(13) + "Por favor contacte o suporte.", "OK", "", 48)
		RETURN .T.
	ENDIF

	SELECT uc_valIdade
	GO TOP

	uv_valIdade = uc_valIdade.valIdade

	FECHA("uc_valIdade")

	RETURN uv_valIdade

ENDFUNC

FUNCTION uf_vacinacao_anular

	SELECT ucrsVacinacao
	GO TOP

	IF !ucrsVacinacao.comunicado
		uf_perguntalt_chama("N�o pode anular um registo que n�o foi comunicado.", "OK", "", 48)
		RETURN .F.
	ENDIF

	IF ucrsVacinacao.anulado
		uf_perguntalt_chama("N�o pode anular um registo j� anulado.", "OK", "", 48)
		RETURN .F.
	ENDIF

	LOCAL uv_operador, uv_profId

	SELECT ucrsuser
	uv_operador = ALLTRIM(ucrsuser.nome)

	IF uf_gerais_checkCedula()
		uv_profId  = uf_gerais_removeAllLeterFromString(ucrsuser.drcedula)
    ENDIF


	SELECT ucrsVacinacao
	GO TOP

	uf_anularVacinacao_chama(.T., ucrsVacinacao.no, '', uv_operador, uf_gerais_getDate(DATE()), uv_profId, ucrsVacinacao.vacinacaoStamp)

ENDFUNC

FUNCTION uf_vacinacao_motivosAnulado

	IF !ucrsVacinacao.anulado
		uf_perguntalt_chama("Este registo n�o est� marcado como anulado.", "OK", "", 48)
		RETURN .F.
	ENDIF


	SELECT ucrsVacinacao
	GO TOP

	uf_anularVacinacao_chama(.F., ucrsVacinacao.no, ucrsVacinacao.motivoAnulado, ucrsVacinacao.usrAnulado, uf_gerais_getDate(ucrsVacinacao.dataAnulado), ucrsVacinacao.profIdAnulado, ucrsVacinacao.vacinacaoStamp)

ENDFUNC