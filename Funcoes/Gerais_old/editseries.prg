FUNCTION uf_editSeries_chama
	LPARAMETERS uv_tabela

	IF !uf_editSeries_criarCurs(uv_tabela)
		RETURN .F.
	ENDIF

	IF WEXIST("EDITSERIES")
		uf_editSeries_sair()
	ENDIF
	
	DO FORM EDITSERIES

ENDFUNC

FUNCTION uf_editSeries_sair

	IF WEXIST("EDITSERIES")
	
		EDITSERIES.hide()
		EDITSERIES.release()
	
	ENDIF

ENDFUNC

FUNCTION uf_editSeries_criarCurs
	LPARAMETERS uv_tabela

	IF EMPTY(uv_tabela)
		RETURN .F.
	ENDIF

	IF WEXIST("EDITSERIES")

		loGrid = EDITSERIES.grdEditSerie
		lcCursor = "uc_editSeries"

		lnColumns = loGrid.ColumnCount

		IF lnColumns > 0

			DIMENSION laControlSource[lnColumns]
			FOR lnColumn = 1 TO lnColumns
				laControlSource[lnColumn] = loGrid.Columns[lnColumn].ControlSource
			ENDFOR

		ENDIF

		loGrid.RecordSource = ""

	ENDIF

	LOCAL uv_sql

	TEXT TO uv_sql TEXTMERGE NOSHOW

		EXEC up_get_editSeries<<ALLTRIM(uv_tabela)>> '<<ALLTRIM(mySite)>>'

	ENDTEXT

	IF !uf_gerais_ActGrelha_semerro('', 'uc_editSeries', uv_sql)
		uf_perguntalt_chama("Erro a encontrar series." + CHR(13) + "Por favor contacte o suporte.", "OK", "", 48)
		RETURN .F.
	ENDIF

	SELECT uc_editSeries
	GO TOP

	IF WEXIST("EDITSERIES")

		loGrid.RecordSource = lcCursor

		FOR lnColumn = 1 TO lnColumns
			loGrid.Columns[lnColumn].ControlSource = laControlSource[lnColumn]
		ENDFOR

   	ENDIF

	RETURN .T.

ENDFUNC

FUNCTION uf_editSeries_gravar

	IF !USED("uc_editSeries")
		RETURN .F.
	ENDIF

	LOCAL uv_sql, uv_update, i, uv_ocorrencias, uv_curOcor
	STORE '' TO uv_sql, uv_update, uv_ocorrencias, uv_curOcor

	SELECT uc_editSeries
	GO TOP

	COUNT TO uv_edited FOR uc_editSeries.resetNumeracaoAnual <> uc_editSeries.resetNumeracaoAnualOri

	IF uv_edited = 0
		uf_gerais_ActGrelha_semerro("N�o alterou nenhum registo.", "OK", "", 48)
		RETURN .F.
	ENDIF

	regua(0, uv_edited, "A guardar altera��es...")

	i = 1

	SELECT uc_editSeries
	GO TOP

	SCAN FOR uc_editSeries.resetNumeracaoAnual <> uc_editSeries.resetNumeracaoAnualOri

		regua(1, i, "A guardar altera��es...")

		uv_update = "UPDATE " + ALLTRIM(uc_editSeries.tabela) + " SET";
					+ " resetNumeracaoAnual = " + IIF(uc_editSeries.resetNumeracaoAnual, "1", "0");
					+ " WHERE " + ALLTRIM(uc_editSeries.tabela) + "stamp = '" + uc_editSeries.stamp + "'"

		uv_sql = uv_sql + CHR(13) + uv_update

		TEXT TO uv_curOcor TEXTMERGE NOSHOW

			uf_gerais_registaocorrencia('S�ries', 'Reset Numera��o <<ALLTRIM(uc_editSeries.nome)>>', 2, '<<IIF(uc_editSeries.resetNumeracaoAnualOri, "Ativo", "Inativo")>>', '<<IIF(uc_editSeries.resetNumeracaoAnual, "Ativo", "Inativo")>>', '<<uc_editSeries.stamp>>', ch_userno, myTerm, uf_gerais_getDate(Date(), "SQL"))

		ENDTEXT

		uv_ocorrencias = uv_ocorrencias + IIF(EMPTY(uv_ocorrencias), '', CHR(13)) + ALLTRIM(uv_curOcor)

		i = i + 1

	ENDSCAN

	**_clipText = uv_sql

	IF !EMPTY(uv_sql)

		IF !uf_gerais_ActGrelha_semerro("", "", uv_sql)
			regua(2)
			uf_perguntalt_chama("Erro a atualizar registos." + CHR(13) + "Por favor contacte o suporte.", "OK", "", 48)
			RETURN .F.
		ELSE

			IF !EMPTY(uv_ocorrencias)

				FOR i = 1 TO ALINES(ua_gravaOcurrencias, uv_ocorrencias, CHR(13))
					&ua_gravaOcurrencias[i].
				NEXT

			ENDIF

			uf_perguntalt_chama("Series atualizadas com sucesso!", "OK", "", 64)
			regua(2)
			uf_editSeries_sair()

		ENDIF

	ENDIF

ENDFUNC

FUNCTION uf_editSeries_sel

	IF !USED("uc_editSeries")
		RETURN .F.
	ENDIF

	SELECT uc_editSeries

	IF uc_editSeries.resetNumeracaoAnual
    	IF !EMPTY(uc_editSeries.ATCUD)
			uf_perguntalt_chama('N�o pode ativar a op��o "Reinicia Numera��o" em s�ries registadas com ATCUD.', "OK", "", 48)
			SELECT uc_editSeries
			REPLACE uc_editSeries.resetNumeracaoAnual WITH .F.
			RETURN .F.
		ENDIF
	ENDIF
ENDFUNC