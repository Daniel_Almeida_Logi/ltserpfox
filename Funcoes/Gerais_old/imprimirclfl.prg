FUNCTION uf_ImprimirCLFL_chama
	LPARAMETERS tcTable

	IF EMPTY(tcTable)
		uf_perguntalt_chama("O PAR�METRO: TABELA, N�O PODE SER NULO!","OK","",16)
		RETURN .f.
	ENDIF
	
	PUBLIC myOrigemTabelaImpClFl
	myOrigemTabelaImpClFl = UPPER(tcTable)
	
	&& Controla abertura do Painel
	IF type("ImprimirCLFL")=="U"
		DO FORM ImprimirCLFL
	ELSE
		ImprimirCLFL.show
	ENDIF	
	
		&& Configura o ecr�
	DO case
		&& Painel de Clientes - CL
		CASE myOrigemTabelaImpClFl == "B_UTENTES"
			IF USED("cl")
				SELECT cl
				ImprimirCLFL.txtDO.value 	= cl.no
				ImprimirCLFL.txtAO.value 	= cl.no
				ImprimirCLFL.txtEstab.value = cl.estab
			ELSE
				ImprimirCLFL.txtDO.value 	= 0
				ImprimirCLFL.txtAO.value 	= 0
				ImprimirCLFL.txtEstab.value = 0
			ENDIF
			
			&& Verifica se o report existe
			IF !File(Alltrim(mypath)+'\analises\clientes.frx')
				uf_perguntalt_chama("O REPORT DE CLIENTES N�O EST� A SER DETECTADO! POR FAVOR CONTACTE O SUPORTE.","OK","", 48)
				RETURN .f.
			ENDIF
			
			&& Muda a caption do Formulario
			imprimirCLFL.caption = "Impress�o de Clientes"
				
				
		&& Painel de Fornecedores - FL
		CASE myOrigemTabelaImpClFl == "FL"	
			IF USED("Fl")
				SELECT Fl
				ImprimirCLFL.txtDO.value 	= FL.no
				ImprimirCLFL.txtAO.value 	= FL.no
				ImprimirCLFL.txtEstab.value = fl.estab
			ELSE
				ImprimirCLFL.txtDO.value 	= 0
				ImprimirCLFL.txtAO.value 	= 0
				ImprimirCLFL.txtEstab.value = 0
			ENDIF			
			
			&& Verifica se o report existe
			If !File(Alltrim(mypath)+'\analises\fornecedores.frx')
				uf_perguntalt_chama("O REPORT DE FORNECEDORES N�O EST� A SER DETECTADO! POR FAVOR CONTACTE O SUPORTE.","OK","",48)
				RETURN .f.
			ENDIF
			
			&& Muda a caption do Formulario
			imprimirCLFL.caption = "Impress�o de Fornecedores"	
			
		&& Painel de Clinica - ID	
			
		OTHERWISE
			uf_perguntalt_chama("O PARAMETRO: TABELA, N�O FOI ACEITE!","OK","",16)
			RETURN .f.
	ENDCASE 		
ENDFUNC 


**
FUNCTION uf_imprimirClFl_carregaMenu
	imprimirClFl.menu1.adicionaopcao("previsao", "Previs�o", myPath + "\imagens\icons\prever_imp_w.png", "uf_ImprimirCLFL_previsao","P")
	imprimirClFl.menu1.adicionaopcao("imprimir", "Imprimir", myPath + "\imagens\icons\imprimir_w.png", "uf_ImprimirCLFL_imprimir","I")
ENDFUNC 



**
FUNCTION uf_ImprimirCLFL_validacoes
	&& Valida��es
		&& Campo inicial preenchido
	IF EMPTY(ImprimirCLFL.txtDO.value) OR (ImprimirCLFL.txtDO.value <= 0)
		uf_perguntalt_chama("PREENCHA UM N�MERO INICIAL V�LIDO!","OK","",48)
		ImprimirCLFL.txtDO.SETFOCUS
		RETURN .F.
	ENDIF
		
		&& Campo final preenchido
	IF EMPTY(ImprimirCLFL.txtAO.value) OR (ImprimirCLFL.txtAO.value <= 0)
		uf_perguntalt_chama("PREENCHA UM N�MERO FINAL V�LIDO!","OK","",48)
		ImprimirCLFL.txtAO.SETFOCUS
		RETURN .F.
	endif	
		
		&& && Campo final > que o inicial
	IF ImprimirCLFL.txtAO.value < ImprimirCLFL.txtDO.value 	
		uf_perguntalt_chama("O N�MERO FINAL N�O PODE SER INFERIOR AO INICIAL!","OK","",48)
		ImprimirCLFL.txtAO.SETFOCUS
		RETURN .F.
	ENDIF
		
		&& Valida se o N� inicial existe
	DO CASE
		CASE myOrigemTabelaImpClFl == 'B_UTENTES'
			IF uf_gerais_actgrelha("", "uCrsCheckImprimirCLFL", [select top 1 utstamp as stamp from b_utentes (nolock) where no=]+ALLTRIM(STR(ImprimirCLFL.txtDO.value))+[ and estab=]+ALLTRIM(STR(ImprimirCLFL.txtEstab.value)))
				IF RECCOUNT("uCrsCheckImprimirCLFL")=0
					uf_perguntalt_chama("O N�MERO INICIAL INTRODUZIDO N�O TEM FICHA ASSOCIADA! POR FAVOR PREENCHA UM N�MERO V�LIDO.","OK","",48)
					FECHA("uCrsCheckImprimirCLFL")
					RETURN .F.
				ENDIF
			ELSE
				uf_perguntalt_chama("OCORREU UM ERRO AO VALIDAR O N�MERO INICIAL!. POR FAVOR TENTE NOVAMENTE.","OK","",16)
				RETURN .F.
			ENDIF	
		
		CASE myOrigemTabelaImpClFl == 'FL'
			IF uf_gerais_actgrelha("", "uCrsCheckImprimirCLFL", [select top 1 flstamp as stamp from fl (nolock) where no=]+ALLTRIM(STR(ImprimirCLFL.txtDO.value))+[ and estab=]+ALLTRIM(STR(ImprimirCLFL.txtEstab.value)))
				IF RECCOUNT("uCrsCheckImprimirCLFL")=0
					uf_perguntalt_chama("O N�MERO INICIAL INTRODUZIDO N�O TEM FICHA ASSOCIADA! POR FAVOR PREENCHA UM N�MERO V�LIDO.","OK","",48)
					FECHA("uCrsCheckImprimirCLFL")
					RETURN .F.
				ENDIF
			ELSE
				uf_perguntalt_chama("OCORREU UM ERRO AO VALIDAR O N�MERO INICIAL! POR FAVOR TENTE NOVAMENTE.","OK","",16)
				RETURN .F.
			ENDIF
	ENDCASE 				
					
		&& Valida se o N� final existe
	DO case
		CASE myOrigemTabelaImpClFl == 'B_UTENTES'
			IF uf_gerais_actgrelha("", "uCrsCheckImprimirCLFL", [select top 1 utstamp as stamp from b_utentes (nolock) where no=]+ALLTRIM(STR(ImprimirCLFL.txtAO.value))+[ and estab=]+ALLTRIM(STR(ImprimirCLFL.txtEstab.value)))
				IF RECCOUNT("uCrsCheckImprimirCLFL")=0
					uf_perguntalt_chama("O N�MERO FINAL INTRODUZIDO N�O TEM FICHA ASSOCIADA! POR FAVOR PREENCHA UM N�MERO V�LIDO.","OK","",48)
					FECHA("uCrsCheckImprimirCLFL")
					RETURN .F.
				ENDIF
			ELSE
				uf_perguntalt_chama("OCORREU UM ERRO AO VALIDAR O N�MERO FINAL! POR FAVOR TENTE NOVAMENTE.","OK","",16)
				RETURN .F.
			ENDIF	
		
		CASE myOrigemTabelaImpClFl == 'FL'
			IF uf_gerais_actgrelha("", "uCrsCheckImprimirCLFL", [select top 1 flstamp as stamp from fl (nolock) where no=]+ALLTRIM(STR(ImprimirCLFL.txtAO.value))+[ and estab=]+ALLTRIM(STR(ImprimirCLFL.txtEstab.value)))
				IF RECCOUNT("uCrsCheckImprimirCLFL")=0
					uf_perguntalt_chama("O N�MERO FINAL INTRODUZIDO N�O TEM FICHA ASSOCIADA! POR FAVOR PREENCHA UM N�MERO V�LIDO.","OK","",48)
					FECHA("uCrsCheckImprimirCLFL")
					RETURN .F.
				ENDIF
			ELSE
				uf_perguntalt_chama("OCORREU UM ERRO AO VALIDAR O N�MERO FINAL! POR FAVOR TENTE NOVAMENTE.","OK","",16)
				RETURN .F.
			ENDIF
	ENDCASE 	
	
	IF USED("uCrsCheckImprimirCLFL")
		fecha("uCrsCheckImprimirCLFL")
	ENDIF
	
		&& Carrega dados Cliente/fornecedor
	LOCAL lcSQL
	STORE '' TO lcSQL
		
	TEXT TO lcSQL TEXTMERGE noshow
		SELECT
			* 
		from
			<<alltrim(myOrigemTabelaImpClFl)>> (nolock) 
		where
			no between <<ImprimirCLFL.txtDO.value>> and <<ImprimirCLFL.txtAO.value>>
			AND estab = <<IIF(ImprimirCLFL.checkEstab.tag == 'false', ImprimirCLFL.txtEstab.value, 0))>>
		order by
			no			
	ENDTEXT

	IF !uf_gerais_actgrelha("", "ucrsImprimirCLFL", lcSql)
		uf_perguntalt_chama("OCORREU UM ERRO AO CARREGAR OS DADOS PARA IMPRIMIR! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .F.
	ENDIF	

		&& Carrega Dados da empresa
	IF !USED("uCrsE1")
		uf_perguntalt_chama("N�O EST� A SER POSS�VEL OBTER OS DADOS DA EMPRESA! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	ENDIF	
		
	RETURN .t.
ENDFUNC 		



FUNCTION uf_ImprimirCLFL_imprimir
	LOCAL lcSQL
	
	&& recupera fun��es
	*uf_recoverApps()
	
	&& Valida Dados	
	IF !uf_ImprimirCLFL_validacoes()
		return
	ENDIF
	
	&& Prepara impressora
	lcReport			=	IIF(myOrigemTabelaImpClFl == 'B_UTENTES', ALLTRIM(mypath)+'\analises\clientes.frx', ALLTRIM(mypath)+'\analises\fornecedores.frx')
	lcPrinter			=	Getprinter()
	lcDefaultPrinter	=	set("printer",2)
	
	If !Empty(lcPrinter)			
		&& Preparar Regua 
		Select uCrsImprimirCLFL
		mntotal	=	reccount("uCrsImprimirCLFL")
		regua(0,mntotal,"IMPRIMINDO...",.f.)

		&& Preparar impressora
		Set Printer To Name ''+lcPrinter+''
		Set Device To print
		Set Console off
			
		SELECT uCrsImprimirCLFL
		GO TOP
		SCAN
			lcSQL = ""
			&& Estabelecimentos
			IF ImprimirCLFL.checkEstab.tag == 'false'
				lcSQL = "set fmtonly on select * from " + myOrigemTabelaImpClFl + " set fmtonly off"
			ELSE
				lcSQL = "select * from " + myOrigemTabelaImpClFl + " (nolock) where no =" + ALLTRIM(STR(uCrsImprimirCLFL.no)) + " and estab!=" + ALLTRIM(STR(ImprimirCLFL.txtEstab.value)) + " order by estab"
			ENDIF
		
			IF !uf_gerais_actgrelha("", [ucrsEstabCLFL], lcSql)
				uf_perguntalt_chama("OCORREU UM ERRO AO CARREGAR OS DADOS PARA IMPRIMIR! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				RETURN .F.
			ENDIF
						
			&& A ordem dos select aos cursores � muito importante, N�O ALTERAR
			IF ImprimirCLFL.checkEstab.tag == 'true' AND RECCOUNT("ucrsEstabCLFL")>0 &&Quando inclui estab
				SELECT ucrsImprimirCLFL
				SELECT uCrsE1
				SELECT ucrsEstabCLFL
			ELSE
				SELECT ucrsEstabCLFL
				SELECT ucrsImprimirCLFL
				SELECT uCRsE1
			ENDIF 
			
			SET REPORTBEHAVIOR 90
			REPORT FORM Alltrim(lcReport) To Printer
			SET REPORTBEHAVIOR 80
		ENDSCAN
	
		&& fecha a regua
		regua(2)
	Else
		uf_perguntalt_chama("N�O � POSS�VEL IMPRIMIR SEM ESCOLHER UMA IMPRESSORA.","OK","",48)
	Endif	
	
	&& por impressora no default
	Set Device To screen
	Set printer to Name ''+lcDefaultPrinter+''
	Set Console On		
ENDFUNC 
	

**
FUNCTION uf_ImprimirCLFL_previsao
	LOCAL lcSQl
	STORE "" TO lcSQL		
	
	&& A previs�o s� funciona para um cliente de cada vez
	IF ImprimirCLFL.txtDO.value <> ImprimirCLFL.txtAO.value
		uf_perguntalt_chama("ESTA FUNCIONALIDADE S� EST� DISPON�VEL PARA PR�-VISUALIZAR UM CLIENTE DE CADA VEZ.","OK","",48)
		return
	ENDIF
	
	&& Valida Dados
	IF !uf_ImprimirCLFL_validacoes()
		RETURN .f.
	ENDIF
	
	&& Prepara impressora
	lcReport = IIF(myOrigemTabelaImpClFl == 'B_UTENTES', ALLTRIM(mypath)+'\analises\clientes.frx', ALLTRIM(mypath)+'\analises\fornecedores.frx')

	IF (ImprimirCLFL.checkEstab.tag == 'false')
		&& Cursor vazio
		lcSQL = "set fmtonly on select top 1 * from "+myOrigemTabelaImpClFl+" set fmtonly off"
	ELSE
		lcSQL = "select * from "+myOrigemTabelaImpClFl+" (nolock) where no between "+ALLTRIM(STR(ImprimirCLFL.txtDO.value))+" and "+STR(ImprimirCLFL.txtDO.value)+" and estab !="+ALLTRIM(STR(ImprimirCLFL.txtEstab.value))+" order by no"
	ENDIF 	
	IF !uf_gerais_actgrelha("", [ucrsEstabCLFL], lcSql)
		uf_perguntalt_chama("OCORREU UM ERRO AO CARREGAR OS DADOS PARA IMPRIMIR! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .F.
	ENDIF
				
	IF ImprimirCLFL.checkEstab.tag == 'true' &&Quando inclui estab
		SELECT ucrsEstabCLFL
		IF RECCOUNT("ucrsEstabCLFL") == 0
			uf_perguntalt_chama("O n�mero seleccinado n�o tem dependentes." + CHR(13) + "Por favor valide.","OK","",64)
			RETURN .f.
		ENDIF
	ENDIF
	
	&& A ordem dos select aos cursores � muito importante, N�O ALTERAR
	IF ImprimirCLFL.checkEstab.tag == 'true' AND RECCOUNT("ucrsEstabCLFL")>0 &&Quando inclui estab
		SELECT uCrsE1
		GO TOP 
		SELECT ucrsImprimirCLFL		
		SELECT ucrsEstabCLFL
	ELSE
		SELECT ucrsEstabCLFL
		SELECT ucrsImprimirCLFL
		SELECT uCRsE1
	ENDIF 
	
	SET REPORTBEHAVIOR 90
	REPORT FORM Alltrim(lcReport) TO PRINTER PROMPT NODIALOG PREVIEW
	SET REPORTBEHAVIOR 80
ENDFUNC 
	


**
FUNCTION uf_ImprimirCLFL_sair
	&& recupera fun��es
	*uf_recoverApps()
	
	IF USED("uCrsImprimirCLFL")
		Fecha("uCrsImprimirCLFL")
	ENDIF
	
	IF USED("uCrsEstabCLFL")
		Fecha("uCrsEstabCLFL")
	ENDIF
	
	IF USED("uCrsCheckImprimirCLFL")
		Fecha("uCrsCheckImprimirCLFL")
	ENDIF
	
	IF USED("uCrsChkCrtImprimir")
		Fecha("uCrsChkCrtImprimir")
	ENDIF

	RELEASE myOrigemTabelaImpClFl
	
	ImprimirCLFL.hide
	ImprimirCLFL.Release
ENDFUNC 


	