DEFINE CLASS myTxtBoxClass AS TextBox

   	PROCEDURE Click 
   		uf_getdate_chama(.f., "lcFData", 0, .f., .f., .t.)
   		Questionario.quest.mytxtBox.Value = lcFData
ENDDEFINE




DEFINE CLASS opgMyOptGrp AS OptionGroup 

   	Top = 30

   	Left = 10

   	Height = 600

   	Width = 320

	BackColor = RGB(255,255,255)
	
	BorderStyle = 0



   	PROCEDURE Click 
   		LOCAL lcButtonNr
   		
   		
		
   		lcButtonNr = IIF(EMPTY(Questionario.quest.opgOptionGroup1.Value), IIF(type("Questionario.quest.opgOptionGroup2") == "O",Questionario.quest.opgOptionGroup2.Value,Questionario.quest.opgOptionGroup1.Value), Questionario.quest.opgOptionGroup1.Value)
   		
   		IF type("Questionario.quest.opgOptionGroup1") == "O" AND type("Questionario.quest.opgOptionGroup2") == "O"
			IF EMPTY(Questionario.quest.opgOptionGroup1.Value) AND EMPTY(Questionario.quest.opgOptionGroup2.Value)
   				RETURN .F.
   			ENDIF 
   		ELSE 
   			IF EMPTY(Questionario.quest.opgOptionGroup1.Value) 
	   			RETURN .F.
	   		ENDIF 
		ENDIF
   		
   		
   		IF !EMPTY(Questionario.quest.opgOptionGroup1.Value)
   		
	   		SELECT uCrsQuestionario
			GOTO TOP 
			LOCATE FOR uCrsQuestionario.Quest_pergunta_respStamp = Questionario.quest.opgOptionGroup1.Buttons(lcButtonNr).Tag 
   			WITH Questionario.quest
		   		IF uf_gerais_compStr(.opgOptionGroup1.Buttons(lcButtonNr).Tag, lcSelResp)
		   			Questionario.quest.opgOptionGroup1.Value = 0 
		   			lcSelResp = ''
		   			Replace uCrsQuestionario.sel WITH .F.
		   		ELSE
		   			lcSelResp = .opgOptionGroup1.Buttons(lcButtonNr).Tag
		   			Replace uCrsQuestionario.sel WITH .T.
			   			
					IF type("Questionario.quest.opgOptionGroup2") == "O" 
						Questionario.quest.opgOptionGroup2.Value = 0 
					ENDIF
		   				
		   		ENDIF
		   		
		   		IF type("Questionario.quest.mytxtbox") == "O" 
					IF .myTxtbox.Tag = .opgOptionGroup1.Buttons(lcButtonNr).Tag  
			   			.myTxtBox.Enabled = .T.
			   		ELSE 
			   			.myTxtBox.Enabled = .F.
			   			.myTxtBox.Value = ""
			   		ENDIF	
		   		ENDIF
		   		
				   		
			ENDWITH 	
   		ELSE
	   		
	   		
	   			
	   		SELECT uCrsQuestionario
			GOTO TOP 
			LOCATE FOR uCrsQuestionario.Quest_pergunta_respStamp = Questionario.quest.opgOptionGroup2.Buttons(lcButtonNr).Tag
			WITH Questionario.quest
		   		IF uf_gerais_compStr(.opgOptionGroup2.Buttons(lcButtonNr).Tag, lcSelResp)
		   			Questionario.quest.opgOptionGroup2.Value = 0 
		   			lcSelResp = ''
		   			Replace uCrsQuestionario.sel WITH .F.
		   		ELSE
		   			lcSelResp = .opgOptionGroup2.Buttons(lcButtonNr).Tag
		   			Replace uCrsQuestionario.sel WITH .T.	
		   		ENDIF
		   		
		   		IF type("Questionario.quest.mytxtbox") == "O" AND uCrsQuestionario.id <> 1
					IF .myTxtbox.Tag = .opgOptionGroup2.Buttons(lcButtonNr).Tag  
			   			.myTxtBox.Enabled = .T.
			   		ELSE 
			   			.myTxtBox.Enabled = .F.
			   		ENDIF	
		   		ENDIF
		   		
			ENDWITH 	
   		ENDIF
   		uf_questionario_limpaSel(lcPerguntaNr)

ENDDEFINE


FUNCTION uf_questionario_chama
	LPARAMETERS lcQuestStamp, lcRef
	PUBLIC lcRefQuest, lcPerguntaNr, lcGrpRespostas, lcSelResp, myRetornoPergunta, myRetornoCancelar
	
	myRetornoPergunta = .f.
	myRetornoCancelar = .f.
	lcRefQuest = lcRef
	lcPerguntaNr = 1
	lcGrpRespostas = uf_gerais_stamp()
	lcSelResp  = ''

	
	&& Controla Abertura do Painel
	if type("QUESTIONARIO")=="U"
		DO FORM QUESTIONARIO WITH lcQuestStamp, lcRef

	
	ELSE
	
		QUESTIONARIO.show
		
	ENDIF
		
	
	uf_questionario_carregaQuest()
	
	RETURN myRetornoPergunta
ENDFUNC


FUNCTION uf_questionario_carregaQuest
	LPARAMETERS lcQuestSatmp


	TEXT TO lcsql TEXTMERGE NOSHOW
			exec up_questionario_getQuest '<<lcQuestSatmp>>'
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "uCrsQuestionario", lcsql)
       uf_perguntalt_chama("Ocorreu uma anomalia ao criar o question�rio. Por Favor contacte o Suporte.", "OK", "", 16)
       RETURN .F.
    ENDIF


ENDFUNC 




FUNCTION uf_questionario_controlaEstadoMenus 
	
	LOCAL lcMaxPergunta, lcMinPergunta
	
	SELECT MAX(ordem) as maxPerg, MIN(ordem) as minPerg FROM uCrsQuestionario INTO CURSOR uCrsTemp READWRITE 
	
	
	DO CASE
	CASE maxPerg = lcPerguntaNr 
		questionario.menu1.anterior.visible = .T.
		questionario.menu1.proximo.visible 	= .F.
		questionario.menu1.gravar.Visible 	= .T.
		
	CASE minPerg = lcPerguntaNr 
		questionario.menu1.anterior.visible = .F.
		questionario.menu1.proximo.visible 	= .T.
		questionario.menu1.gravar.Visible 	= .F.
		
	OTHERWISE
		questionario.menu1.anterior.visible = .T.
		questionario.menu1.proximo.visible 	= .T.
		questionario.menu1.gravar.Visible 	= .F.
		
	ENDCASE
	
	Questionario.refresh()
	
	fecha("uCrsTemp")
ENDFUNC 


FUNCTION uf_questionario_limpaForm
		IF type("Questionario.quest.opgOptionGroup1") == "O"
			Questionario.quest.removeobject("opgOptionGroup1")
		ENDIF
		
		IF type("Questionario.quest.opgOptionGroup2") == "O"
			Questionario.quest.removeobject("opgOptionGroup2")
		ENDIF
		
		IF type("Questionario.quest.mytxtbox") == "O"
			Questionario.quest.removeobject("mytxtbox")
		ENDIF
ENDFUNC 




FUNCTION uf_questionario_geraEcra
	
	uf_questionario_limpaForm()
	
	SELECT uCrsQuestionario
	GOTO TOP 
	SELECT * FROM uCrsQuestionario WHERE uCrsQuestionario.ordem = lcPerguntaNr INTO CURSOR uCrsQuestionarioTemp READWRITE
	
	
	SELECT uCrsQuestionarioTemp
	GOTO TOP 
	Questionario.quest.txtPergunta.Caption = uCrsQuestionarioTemp.Pergunta
	
	
	IF RECcount("uCrsQuestionarioTemp") > 1
		uf_questionario_adicionaOpcoes()
	ELSE
	
		WITH Questionario.quest
			
			IF 	uCrsQuestionarioTemp.ID = 2			
				lcSelResp = uCrsQuestionarioTemp.Quest_pergunta_respStamp
				
				.AddObject('myTxtBox','TextBox')
				.myTxtBox.top = 100
				.myTxtBox.left = 20
				.myTxtBox.width = 100
				.myTxtBox.Visible =  .T.
				.myTxtBox.MAxLength = 100
				.myTxtBox.Enabled = .T.
				.myTxtBox.Tag =  uCrsQuestionarioTemp.Quest_pergunta_respStamp
				.myTxtBox.Value =  uCrsQuestionarioTemp.resposta
			ENDIF
			
			
			IF 	uCrsQuestionarioTemp.ID = 3
				
				lcSelResp = uCrsQuestionarioTemp.Quest_pergunta_respStamp

				.AddObject('myTxtBox','TextBox')
				.myTxtBox.top = 100
				.myTxtBox.left = 20
				.myTxtBox.width = 100
				.myTxtBox.Visible =  .T.
				.myTxtBox.Enabled = .T.
				.myTxtBox.InputMask = '999999'
				.myTxtBox.Tag =  uCrsQuestionarioTemp.Quest_pergunta_respStamp
				.myTxtBox.Value =  uCrsQuestionarioTemp.resposta
				
			ENDIF
			
			
			
			
			IF 	uCrsQuestionarioTemp.ID = 4
				
				lcSelResp = uCrsQuestionarioTemp.Quest_pergunta_respStamp	

				.AddObject('myTxtBox','myTxtBoxClass')
				.myTxtBox.top = 100
				.myTxtBox.left = 20
				.myTxtBox.width = 100
				.myTxtBox.Visible =  .T.
				.myTxtBox.ReadOnly  = .T.
				.myTxtBox.Tag =  uCrsQuestionarioTemp.Quest_pergunta_respStamp
				.myTxtBox.Value =  uCrsQuestionarioTemp.resposta

			ENDIF

		ENDWITH
		
	ENDIF
	
ENDFUNC






FUNCTION uf_questionario_Proximo
	LOCAL lcMultipla
	lcMultipla = .F.
	
	SELECT uCrsQuestionarioTemp
	GOTO TOP 
	lcMultipla = RECcount("uCrsQuestionarioTemp") > 1
	SELECT uCrsQuestionarioTemp
	GOTO TOP 
	
	SELECT uCrsQuestionario
	GOTO TOP 
	LOCATE FOR uCrsQuestionario.Quest_pergunta_respStamp = lcSelResp
	IF uCrsQuestionario.Prox_ordem > 0 AND !lcMultipla &&AND uCrsQuestionario.id <> 1   		
			
		IF EMPTY(Questionario.quest.mytxtbox.Value)
			uf_perguntalt_chama("Necessita Responder.", "OK", "", 16)
			RETURN .F.		
		ELSE
			SELECT uCRsQuestionario
			Replace uCrsQuestionario.resposta  	WITH ALLTRIM(Questionario.quest.mytxtbox.Value)
			Replace uCrsQuestionario.sel  		WITH .T.
		
		ENDIF
		
		lcPerguntaNr = uCrsQuestionario.Prox_Ordem 
   		lcSelResp = ''	
		
	ELSE 
	
		IF EMPTY(lcSelResp)
			uf_perguntalt_chama(" Necessita selecionar uma resposta.", "OK", "", 16)
	       	RETURN .F.
		ENDIF 
		SELECT uCrsQuestionario
		IF uCrsQuestionario.ID <> 1
		
				lcRespostaString = alines(uc_RespostaString,STRTRAN( uCrsQuestionarioTemp.resposta,"**",chr(13)))
				
				IF lcRespostaString = 1
					
					SELECT uCrsQuestionario
					Replace uCrsQuestionario.resposta  	WITH  ALLTRIM(uCrsQuestionario.resposta)+"**"+Questionario.quest.mytxtbox.Value
					REplace uCrsQuestionario.sel  		WITH .T.  
	
				ELSE
	
					SELECT uCrsQuestionario
					Replace uCrsQuestionario.resposta  	WITH  ALLTRIM(uc_RespostaString[1])+"**"+Questionario.quest.mytxtbox.Value
					REplace uCrsQuestionario.sel  		WITH .T.  

				ENDIF
				
		ELSE
				Replace uCrsQuestionario.sel  		WITH .T.  
		ENDIF
		
		lcPerguntaNr_old = lcPerguntaNr 
		lcPerguntaNr = uCrsQuestionario.Prox_Ordem
		
		uf_questionario_limpaSel(lcPerguntaNr_old)
		lcSelResp = ''
   	ENDIF 
   	
   	uf_questionario_limpaForm()
   	
   	uf_questionario_geraEcra()
   	
   	uf_questionario_controlaEstadoMenus()
   	   
ENDFUNC


FUNCTION uf_questionario_limpaSel
	LPARAMETERS lcPerguntaNr_old


	SELECT uCrsQuestionario
	GOTO TOP 
	SELECT * FROM uCrsQuestionario WHERE uCrsQuestionario.ordem = lcPerguntaNr_old INTO CURSOR uCrsQuestionarioLimpaSel READWRITE
	
	SELECT uCrsQuestionarioLimpaSel
	GOTO TOP 
	SCAN For uCrsQuestionarioLimpaSel.quest_pergunta_respStamp != lcSelResp 
		SELECT uCrsQuestionario
		GOTO TOP 
		LOCATE FOR  uCrsQuestionario.ordem = lcPerguntaNr and uCrsQuestionarioLimpaSel.quest_pergunta_respStamp = uCrsQuestionario.quest_pergunta_respStamp 
		SELECT uCrsQuestionario
		Replace uCrsQuestionario.sel WITH .F.
	
	ENDSCAN
	
	
	SELECT uCrsQuestionario
	GOTO TOP
	
	fecha("uCrsQuestionarioLimpaSel")

ENDFUNC 




FUNCTION uf_questionario_Anterior
	
	lcSelResp = '' 
	
	questionario.menu1.proximo.visible = .T.
	
	SELECT uCrsQuestionario.quest_perguntaStamp FROM uCrsQuestionario WHERE sel = .T. AND Prox_ordem = lcPerguntaNr  INTO CURSOR uCrsAntTemp ReadWrite
	
	SELECT uCrsAntTemp 
	lcPerguntaNr = uf_gerais_getUmValor("quest_pergunta", "ordem", "quest_pergunta.quest_perguntaStamp = '"+uCrsAntTemp .quest_perguntaStamp +"'")
	
	uf_questionario_controlaEstadoMenus()
	
	uf_questionario_limpaform()
	
		
	uf_questionario_geraEcra()
	uf_questionario_controlaEstadoMenus()
	
	fecha("uCrsAntTemp")
	
ENDFUNC




FUNCTION uf_questionario_adicionaOpcoes

	LOCAL lOptNr , lcCrsSize
	lOptNr  = 1
	lcCrsSize = 0
	
	SELECT uCrsQuestionarioTemp
	GOTO TOP
	lcCrsSize = RECCOUNT("uCrsQuestionarioTemp")
	
	WITH QUESTIONARIO.quest
		
		.AddObject('opgOptionGroup1','opgMyOptGrp') 
		.opgOptionGroup1.ButtonCount  = IIF(lcCrsSize > 10, 10, lcCrsSize)
		.opgOptionGroup1.ControlSource = uCrsQuestionarioTemp.Sel
		.Parent.height = IIF(120 + (lcCrsSize* 40) < 350, 350, IIF(120 + (lcCrsSize* 40)>500,500,120 + (lcCrsSize* 40)))
		.height = IIF(100 + (lcCrsSize * 40) >500 , 700, 120 + (lcCrsSize* 40))
		
		SELECT uCrsQuestionarioTemp
		GOTO TOP
		SCAN
			IF lcCrsSize < lOptNr 
				lOptNr  = 1
				RETURN .T. 
			ENDIF 

			lcRespostaString = alines(uc_RespostaString,STRTRAN( uCrsQuestionarioTemp.resposta,"**",chr(13)))
			IF lOptNr <  11 

				.opgOptionGroup1.Buttons(lOptNr).BackColor = RGB(255,255,255)
				.opgOptionGroup1.Buttons(lOptNr).Width  = 350
				.opgOptionGroup1.Buttons(lOptNr).height  = 30
				.opgOptionGroup1.Buttons(lOptNr).top  = 40 * lOptNr 
				.opgOptionGroup1.Buttons(lOptNr).Tag = uCrsQuestionarioTemp.Quest_pergunta_respStamp
				.opgOptionGroup1.Buttons(lOptNr).Caption = IIF(lcRespostaString  > 1,uc_RespostaString[1], uCrsQuestionarioTemp.resposta)
				.opgOptionGroup1.Buttons(lOptNr).value = IIF(uCrsQuestionarioTemp.sel, 1,0) 
				
				 
				 **Adicionar Caixa de texto nas op��es em que o utilizador pode escrever
				IF 	uCrsQuestionarioTemp.ID = 2
					.AddObject('myTxtBox','TextBox')
					.myTxtBox.top = .opgOptionGroup1.Buttons(lOptNr).Top + .opgOptionGroup1.top 
					.myTxtBox.left = .opgOptionGroup1.Buttons(lOptNr).Width +50
					.myTxtBox.width = 100
					.myTxtBox.Visible =  .T.
					.myTxtBox.Enabled = .F.
					.myTxtBox.Tag =  uCrsQuestionarioTemp.Quest_pergunta_respStamp
					.myTxtBox.Value = IIF(lcRespostaString  > 1,uc_RespostaString[2], "")
					
				ENDIF

			
			ELSE
			
				IF !(type("Questionario.quest.opgOptionGroup2") == "O")
					.AddObject('opgOptionGroup2','opgMyOptGrp')
					.opgOptionGroup2.ButtonCount  = lcCrsSize - 10
					.opgOptionGroup2.left = 350
					.opgOptionGroup2.top  = 30
					.opgOptionGroup2.ControlSource = uCrsQuestionarioTemp.Sel
				ENDIF
				
				.opgOptionGroup2.Buttons(lOptNr-10).BackColor = RGB(255,255,255)
				.opgOptionGroup2.Buttons(lOptNr-10).Width  = 300
				.opgOptionGroup2.Buttons(lOptNr-10).height  = 30
				.opgOptionGroup2.Buttons(lOptNr-10).top  = 40 * (lOptNr - 10)
				.opgOptionGroup2.Buttons(lOptNr-10).Tag = uCrsQuestionarioTemp.Quest_pergunta_respStamp
				.opgOptionGroup2.Buttons(lOptNr-10).Caption = IIF(lcRespostaString  > 1,uc_RespostaString[1], uCrsQuestionarioTemp.resposta)
				.opgOptionGroup2.Buttons(lOptNr-10).Value = uCrsQuestionarioTemp.sel
				 
				 **Adicionar Caixa de texto nas op��es em que o utilizador pode escrever
				IF 	uCrsQuestionarioTemp.ID = 2
					.AddObject('myTxtBox','TextBox')
					.myTxtBox.top = .opgOptionGroup2.Buttons(lOptNr).Top + .opgOptionGroup2.top 
					.myTxtBox.left = .opgOptionGroup2.Buttons(lOptNr).Width +50
					.myTxtBox.width = 100
					.myTxtBox.Visible =  .T.
					.myTxtBox.Enabled = .F.
					.myTxtBox.Tag =  uCrsQuestionarioTemp.Quest_pergunta_respStamp
					.myTxtBox.Value = IIF(lcRespostaString  > 1,uc_RespostaString[2], "")
				ENDIF
				
			ENDIF 
			
			lOptNr = lOptNr + 1
		ENDSCAN

		.opgOptionGroup1.click
		.opgOptionGroup1.Visible = .T.  
		.opgOptionGroup1.refresh
	
		IF type("Questionario.quest.opgOptionGroup2") == "O"
			.opgOptionGroup2.Visible = .T.  
			.opgOptionGroup2.refresh
		ENDIF
	ENDWITH 

ENDFUNC



FUNCTION uf_questionario_sair
	IF !uf_perguntalt_chama("Deseja cancelar o preenchimento do question�rio?" , "Sim", "N�o")
		RETURN .F.
	ENDIF   
			
	
	fecha("uCrsQuestionario")
	TRY 
		QUESTIONARIO.hide
		QUESTIONARIO.release
		
		myRetornoPergunta = .F.		
	ENDTRY 
	
	
	
	RELEASE lcRefQuest, lcPerguntaNr , lcGrpRespostas, lcSelResp, myCursPos  
	

ENDFUNC
**
FUNCTION uf_questionario_gravar 
	LOCAL lcMultipla
	lcMultipla = .F.
	
	SELECT uCrsQuestionarioTemp
	GOTO TOP 
	lcMultipla = RECcount("uCrsQuestionarioTemp") > 1
	SELECT uCrsQuestionarioTemp
	GOTO TOP 
	
	SELECT uCrsQuestionario
	GOTO TOP 
	LOCATE FOR uCrsQuestionario.Quest_pergunta_respStamp = lcSelResp

	IF uCrsQuestionario.Prox_ordem > 0 AND !lcMultipla &&AND uCrsQuestionario.id <> 1   		
			
		IF EMPTY(Questionario.quest.mytxtbox.Value)
			uf_perguntalt_chama("Necessita Responder.", "OK", "", 16)
			RETURN .F.		
		ENDIF 
	ELSE 
	
		IF EMPTY(lcSelResp)
			uf_perguntalt_chama(" Necessita selecionar uma resposta.", "OK", "", 16)
	       	RETURN .F.
		ENDIF 
   	ENDIF 





	SELECT uCrsQuestionario
	GOTO TOP 
	SCAN FOR uCrsQuestionario.sel = .T.
		lcStampTemp = uf_gerais_Stamp()
		lcRespostaString = alines(uc_RespostaString,STRTRAN( uCrsQuestionario.resposta,"**",chr(13)))
		
		
		
		TEXT TO lcsql TEXTMERGE NOSHOW
	
			INSERT INTO [dbo].[quest_respostas]
	           ([quest_respostasStamp]
	           ,[questStamp]
	           ,[quest_respostas_grpStamp]
	           ,[quest_perguntaStamp]
	           ,[fistamp]
	           ,[resposta]
	           ,[ref]
	           ,[no]
	           ,[operador]
	           ,[site]
	           ,[ousrdata]
	           ,[ousrinis]
	           ,[usrdata]
	           ,[usrinis])
	     	VALUES
	           ('<<lcStampTemp>>'
	           ,'<<uCrsQuestionario.questStamp>>'
	           ,'<<lcGrpRespostas>>'
	           ,'<<uCrsQuestionario.quest_perguntaStamp>>'
	           ,'<<fi.fistamp>>'
	           ,'<<IIF( lcRespostaString > 1 ,uc_RespostaString[2],uCrsQuestionario.resposta)>>'
	           ,'<<lcRefQuest>>'
	           ,'<<UcrsAtendCl.no>>'
	           ,'<<ch_userno>>'
	           ,'<<mysite>>'
	           ,getdate()
	           ,''
	           ,getdate()
	           ,'')
	           
	           
	    ENDTEXT
	    IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
	       uf_perguntalt_chama("Ocorreu uma anomalia ao gravar as respostas. Por Favor contacte o Suporte.", "OK", "", 16)
	       RETURN .F.
	    ENDIF
	
	ENDSCAN
	
	uf_perguntalt_chama("Question�rio Gravado com Sucesso.", "OK", "", 16)
	
	
	fecha("uCrsQuestionario")
	
	TRY 
		QUESTIONARIO.hide
		QUESTIONARIO.release
		
		myRetornoPergunta = .t.
	ENDTRY 
	
	
	
	RELEASE lcRefQuest, lcPerguntaNr , lcGrpRespostas, lcSelResp, myCursPos  
	

	
ENDFUNC

