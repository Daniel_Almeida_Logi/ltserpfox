**
FUNCTION uf_VALORESCOMBOFILE_chama
	LPARAMETERS lcObjecto, lcTipo, lcSQL, lcTipoFill, lcRetorno, lcPesquisa, lcMultiSelecao, lcDefault, lcModal, lcNaoMostraVazio, lcNaoSelecciona, lcValorManual, lcNoLength, lcSaidaTecladoNumerico

	IF EMPTY(lcPesquisa)
		lcPesquisa = lcRetorno
	ENDIF

	IF type("VALORESCOMBOFILE")=="U"
		DO FORM VALORESCOMBOFILE WITH lcObjecto, lcTipo, lcSQL, lcTipoFill, lcRetorno, lcPesquisa, lcMultiSelecao, lcDefault, lcModal, lcNaoMostraVazio, lcNaoSelecciona, lcValorManual, lcNoLength, lcSaidaTecladoNumerico
	ELSE
		VALORESCOMBOFILE.show
	ENDIF
	
			
ENDFUNC


**
FUNCTION uf_VALORESCOMBOFILE_FiltraCursor
	LOCAL lcRetorno

	IF EMPTY(VALORESCOMBOFILE.pesquisa)
		lcRetorno = VALORESCOMBOFILE.retorno
	ELSE
		lcRetorno = VALORESCOMBOFILE.pesquisa
	ENDIF

	** colunas
	ALINES(arrayCampos, lcRetorno , ",")
	
	lcFiltro = ''
	
	FOR i=1 TO ALEN(arrayCampos)
	
		lcCampo = arrayCampos[i]
		
		*
		IF i=1
			IF type("uCrsPesqMultiFile."+ALLTRIM(lcCampo)) == "N"
				lcFiltro = [LIKE('*'+UPPER(ALLTRIM(VALORESCOMBOFILE.valor.value))+'*',UPPER(STR(uCrsPesqMultiFile.] + arrayCampos[i] +[)))]
			ELSE
				lcFiltro = [LIKE('*'+UPPER(ALLTRIM(VALORESCOMBOFILE.valor.value))+'*',UPPER(uCrsPesqMultiFile.] + arrayCampos[i] +[))]
			ENDIF
		ELSE
			IF type("uCrsPesqMultiFile."+ALLTRIM(lcCampo)) == "N"
				lcFiltro = lcFiltro + [ OR LIKE('*'+UPPER(ALLTRIM(VALORESCOMBOFILE.valor.value))+'*',UPPER(STR(uCrsPesqMultiFile.] + arrayCampos[i] +[)))]
			ELSE
				lcFiltro = lcFiltro + [ OR LIKE('*'+UPPER(ALLTRIM(VALORESCOMBOFILE.valor.value))+'*',UPPER(uCrsPesqMultiFile.] + arrayCampos[i] +[))]
			ENDIF
		ENDIF
	ENDFOR

	** manter sempre os j� seleccionados no filtro
	IF !EMPTY(lcFiltro)
		lcFiltro = lcFiltro + ' ' + 'OR uCrsPesqMultiFile.sel = .t.'
	ENDIF

	SELECT uCrsPesqMultiFile
	GO TOP
	IF !EMPTY(lcFiltro)
		SET FILTER TO &lcFiltro 
	ELSE
		SET FILTER TO
	ENDIF

	VALORESCOMBOFILE.GRIDPESQMULTI.refresh
ENDFUNC


**
FUNCTION uf_VALORESCOMBOFILE_vazio
	LOCAL lcObj
	lcObj = VALORESCOMBOFILE.objecto
	
	SELECT uCrsPesqMultiFile
	DO CASE
		CASE VALORESCOMBOFILE.tipoObj == 0 && vari�vel
			&lcObj = ""
			
		CASE VALORESCOMBOFILE.tipoObj == 1 && objecto
			&lcObj..value = ""
			
		CASE VALORESCOMBOFILE.tipoObj == 2 && cursor
			TRY
				LOCAL lcCursor
				lcCursor = STREXTRACT(lcObj, "", ".")
							
				SELECT &lcCursor
				Replace &lcObj With ''
			CATCH
			ENDTRY

		CASE VALORESCOMBOFILE.tipoObj == 3 && container class
			&lcObj..config(ALLTRIM(""))
	ENDCASE
		
	uf_VALORESCOMBOFILE_sair()
ENDFUNC


***
FUNCTION uf_VALORESCOMBOFILE_selTodos
	
	IF EMPTY(VALORESCOMBOFILE.menu1.seltodos.tag) OR VALORESCOMBOFILE.menu1.seltodos.tag == "false"
		&& aplica sele��o
		REPLACE ALL sel WITH .t. IN uCrsPesqMultiFile
		
		&& altera o botao
		VALORESCOMBOFILE.menu1.seltodos.tag = "true"
		VALORESCOMBOFILE.menu1.seltodos.config("Sel.Todos", myPath + "\imagens\icons\checked_w.png", "uf_VALORESCOMBOFILE_selTodos","E")
		
	ELSE
		&& aplica sele��o
		REPLACE ALL sel WITH .f. IN uCrsPesqMultiFile
		
		&& altera o botao
		VALORESCOMBOFILE.menu1.seltodos.tag = "false"
		VALORESCOMBOFILE.menu1.seltodos.config("Sel.Todos", myPath + "\imagens\icons\unchecked_w.png", "uf_VALORESCOMBOFILE_selTodos","E")
		
	ENDIF
	
ENDFUNC


**Evento Click na selec��o
FUNCTION uf_VALORESCOMBOFILE_escolhe

	** Caso esteja definido multiselecao, nao faz nada
	IF VALORESCOMBOFILE.MultiSelecao == .t.
		RETURN .f.
	ENDIF

	LOCAL lcObj, lcRetorno
	lcObj = VALORESCOMBOFILE.objecto
	
	** colunas inicial � a de retorno, para permitir a pesquisa em mais que um campo
	ALINES(arrayRetorno, VALORESCOMBOFILE.retorno , ",")
	lcRetorno = arrayRetorno[1]
	
	SELECT uCrsPesqMultiFile
	DO CASE
		CASE VALORESCOMBOFILE.tipoObj == 0 && vari�vel
			&lcObj = uCrsPesqMultiFile.&lcRetorno 
			
		CASE VALORESCOMBOFILE.tipoObj == 1 && objecto
			&lcObj..value = uCrsPesqMultiFile.&lcRetorno 
			
		CASE VALORESCOMBOFILE.tipoObj == 2 && cursor
			TRY

				lcCursor = STREXTRACT(lcObj, "", ".")

				SELECT &lcCursor
				Replace &lcObj With uCrsPesqMultiFile.&lcRetorno 
			CATCH
			ENDTRY

		CASE VALORESCOMBOFILE.tipoObj == 3 && container class
			&lcObj..config(ALLTRIM(uCrsPesqMultiFile.&lcRetorno ))
		
		CASE VALORESCOMBOFILE.tipoObj == 4 && cursor com v�rias posi��es
			LOCAL lcCursor
			ALINES(arrayCampos, lcObj, ".")
			lcCursor = arrayCampos[1]
			SELECT &lcCursor
			GO BOTTOM 
			FOR i=2 TO ALEN(arrayCampos)
				SELECT &lcCursor
				Replace &arrayCampos[i] With uCrsPesqMultiFile.&arrayCampos[i]
			ENDFOR
		CASE VALORESCOMBOFILE.tipoObj == 5 && Propriedade de formulario
			&lcObj = uCrsPesqMultiFile.&lcRetorno 
	ENDCASE


	uf_VALORESCOMBOFILE_sair()
ENDFUNC


** Aplicar multiselecao
FUNCTION uf_VALORESCOMBOFILE_gravar
	LOCAL lcCursor
	
	** Caso n�o esteja definido multiselecao, nao faz nada
	IF VALORESCOMBOFILE.MultiSelecao == .f.
		RETURN .f.
	ENDIF 
	
	LOCAL lcObj, lcRetorno, lcMultiSelecaoRetorno 
	lcObj = VALORESCOMBOFILE.objecto
	lcRetorno = VALORESCOMBOFILE.retorno
	
	lcMultiSelecaoRetorno = ""
	SELECT uCrsPesqMultiFile
	GO TOP 
	SCAN

		IF uCrsPesqMultiFile.sel == .t.
		
			IF EMPTY(lcMultiSelecaoRetorno)
				lcMultiSelecaoRetorno = ALLTRIM(uCrsPesqMultiFile.&lcRetorno)
			ELSE
				IF UPPER(ALLTRIM(lcRetorno)) == 'LAB' && se for lab separa por ; por causa dos reports
					lcMultiSelecaoRetorno = ALLTRIM(lcMultiSelecaoRetorno) + ";" + ALLTRIM(uCrsPesqMultiFile.&lcRetorno)
				ELSE
					lcMultiSelecaoRetorno = ALLTRIM(lcMultiSelecaoRetorno) + "," + ALLTRIM(uCrsPesqMultiFile.&lcRetorno)				
				ENDIF
			ENDIF
		ENDIF
	ENDSCAN

	**
	**
	
	IF LEN(lcMultiSelecaoRetorno) > 254 AND !VALORESCOMBOFILE.noLength
		uf_perguntalt_chama("N�o � possivel aplicar valores com mais de 254 caracteres. Verifique a sua selec��o.", "Ok", "", 48)
		RETURN .f.
	ENDIF
	
	SELECT uCrsPesqMultiFile
	DO CASE

		CASE ALLTRIM(UPPER(VALORESCOMBOFILE.objecto)) == UPPER("ucrsConfPvpEmpresas")
			SELECT uCrsPesqMultiFile
			GO Top
			SCAN
				SELECT ucrsConfPvpEmpresas
				GO TOP
				LOCATE FOR UPPER(ALLTRIM(ucrsConfPvpEmpresas.local)) == UPPER(ALLTRIM(uCrsPesqMultiFile.local))
				IF FOUND()
					Replace ucrsConfPvpEmpresas.selEmpresa With uCrsPesqMultiFile.sel
				ENDIF 
			ENDSCAN
			
		CASE ALLTRIM(UPPER(VALORESCOMBOFILE.objecto)) == "UCRSLISTARECURSOSST.NOME"
			
			lcStamp = uf_gerais_getId(1)
			Select St
			SELECT uCrsPesqMultiFile
			GO Top
			SCAN FOR uCrsPesqMultiFile.sel == .t.
				
				SELECT ucrsListaRecursosST
				APPEND BLANK
				Replace ucrsListaRecursosST.strecstamp WITH lcStamp
				Replace ucrsListaRecursosST.ref WITH St.ref
				Replace ucrsListaRecursosST.nome WITH uCrsPesqMultiFile.nome
				Replace ucrsListaRecursosST.no WITH uCrsPesqMultiFile.no
				Replace ucrsListaRecursosST.stamp WITH uCrsPesqMultiFile.stamp 
				Replace ucrsListaRecursosST.duracao WITH 0
				Replace ucrsListaRecursosST.tipo WITH uCrsPesqMultiFile.tipo 

			ENDSCAN
		
		CASE ALLTRIM(UPPER(VALORESCOMBOFILE.objecto)) == "UCRSRECURSOSDASERIE.NOME"
			
			
			uf_VALORESCOMBOFILE_chama("uCrsRecursosDaSerie.nome", 2, "ucrsTempListaRecursos", 2, "nome", "nome")

			lcStamp = uf_gerais_getId(1)
			Select ucrsServicosSerie
			SELECT uCrsPesqMultiFile
			GO Top
			SCAN FOR uCrsPesqMultiFile.sel == .t.
				
				SELECT uCrsRecursosDaSerie
				APPEND BLANK
				
				Replace uCrsRecursosDaSerie.serierecstamp WITH lcStamp
				Replace uCrsRecursosDaSerie.seriestamp WITH ucrsSeriesServ.seriestamp
				Replace uCrsRecursosDaSerie.stamp WITH uCrsPesqMultiFile.stamp 
				Replace uCrsRecursosDaSerie.ref WITH ucrsServicosSerie.ref
				Replace uCrsRecursosDaSerie.nome WITH uCrsPesqMultiFile.nome
				Replace uCrsRecursosDaSerie.no WITH uCrsPesqMultiFile.no
				Replace uCrsRecursosDaSerie.tipo WITH uCrsPesqMultiFile.tipo 

			ENDSCAN			
		
		CASE ALLTRIM(UPPER(VALORESCOMBOFILE.objecto)) == UPPER("uCrsDiplomasID.patologia")
			
			Select uCrsDiplomasID
			= TABLEUPDATE(.T.) 
			
			SELECT Cl
			LcClStamp = cl.utstamp
			SELECT uCrsPesqMultiFile
			GO Top
			SCAN FOR uCrsPesqMultiFile.sel == .t.
				
				SELECT uCrsDiplomasID
				APPEND BLANK
				
				Replace uCrsDiplomasID.patologia WITH uCrsPesqMultiFile.patologia

				SELECT uCrsDiplomasID
				SELECT uCrsTempListaDiplomas 
				LOCATE FOR uCrsDiplomasID.patologia = uCrsTempListaDiplomas.patologia 
				IF FOUND()
				
					SELECT uCrsDiplomasID
					Replace uCrsDiplomasID.idstamp WITH LcClStamp
					Replace uCrsDiplomasID.diploma WITH uCrsTempListaDiplomas.despacho
					Replace uCrsDiplomasID.u_design WITH uCrsTempListaDiplomas.diploma
					&&Replace uCrsDiplomasID.patologia WITH uCrsTempListaDiplomas.patologia
					
				ENDIF 
				
		
				UTENTES.Pageframe1.page7.GridDiplomas.refresh				


			ENDSCAN		
		
		CASE VALORESCOMBOFILE.tipoObj == 0 && vari�vel
			&lcObj = lcMultiSelecaoRetorno  
			
		CASE VALORESCOMBOFILE.tipoObj == 1 && objecto
			&lcObj..value = lcMultiSelecaoRetorno 
			
		CASE VALORESCOMBOFILE.tipoObj == 2 && cursor
				
			SELECT uCrsPesqMultiFile
			GO TOP 
			SCAN FOR uCrsPesqMultiFile.sel == .t.
				lcCursor = STREXTRACT(lcObj, "", ".")
				SELECT &lcCursor
				APPEND BLANK
				Replace &lcObj With uCrsPesqMultiFile.&lcRetorno
			ENDSCAN

		CASE VALORESCOMBOFILE.tipoObj == 3 && container class
			&lcObj..config(ALLTRIM(lcMultiSelecaoRetorno))
			
		CASE VALORESCOMBOFILE.tipoObj == 5 && Objecto
			&lcObj = lcMultiSelecaoRetorno
			
		CASE VALORESCOMBOFILE.tipoObj == 6 && cursor

			IF USED("ucrsConfEmpresas")
				fecha("ucrsConfEmpresas")
			ENDIF 
			SELECT sel as selempresa, local, designacao, siteno, armazem1 FROM uCrsPesqMultiFile INTO CURSOR ucrsConfEmpresas READWRITE 
				
			SELECT uCrsPesqMultiFile
			GO TOP 
			SCAN 
				IF uCrsPesqMultiFile.sel == .f.
					DELETE
				ENDIF 
			ENDSCAN
			SELECT * FROM uCrsPesqMultiFile INTO CURSOR ucrsEmpresasSel READWRITE 

	ENDCASE
	
	uf_VALORESCOMBOFILE_sair()
ENDFUNC


**
FUNCTION uf_VALORESCOMBOFILE_sair
	**	
	fecha("uCrsPesqMultiFile")
	VALORESCOMBOFILE.hide
	VALORESCOMBOFILE.release
ENDFUNC


**
FUNCTION uf_VALORESCOMBOFILE_Teclado
	VALORESCOMBOFILE.tecladoVirtual1.show("VALORESCOMBOFILE.gridPesqMulti", 161, "VALORESCOMBOFILE.shpGrid", 161)
ENDFUNC


** Funcao utilizada apenas para introduzir PVP's manualmente. Usada no atendimento e faturacao, cuidado ao alterar.
FUNCTION uf_VALORESCOMBOFILE_tecladochama
	LPARAMETERS lcCampoSaida

	**criou se esta situacao porque foi criado o parametro de entrada e como pode ter sitios onde nao se descobriu que tem 
	** colocou se como default este campo
	IF EMPTY(lcCampoSaida)
		lcCampoSaida = "fi.u_epvp"
	ENDIF
	
	&& verifica se pede password para alterar pvp 
	IF !EMPTY(alltrim(uf_gerais_getParameter('ADM0000000072', 'TEXT')))
		** pedir password **
		PUBLIC cval
		STORE '' TO cval
		
		uf_tecladoalpha_chama("cval", "INTRODUZA PASSWORD PARA ALTERA��O DE PVP'S:", .t., .f., 0)
		
		IF EMPTY(cval)
			uf_perguntalt_chama("DEVE INTRODUZIR A PASSWORD DE SUPERVISOR PARA ALTERAR PVP.","OK","",64)
			RETURN .f.		
		ELSE
			IF !(UPPER(ALLTRIM(cval))==UPPER(ALLTRIM(uf_gerais_getParameter('ADM0000000072', 'TEXT'))))
				uf_perguntalt_chama("A PASSWORD DE SUPERVISOR N�O EST� CORRECTA.","OK","",64)
				RETURN .F.
			ENDIF 
		ENDIF
	ENDIF
	
	uf_VALORESCOMBOFILE_sair()
	
	uf_tecladonumerico_chama(lcCampoSaida , "Introduza o PVP:", 2, .t., .f., 9, 2)
	
ENDFUNC 


**
FUNCTION uf_VALORESCOMBOFILE_ImportarDados_XLS
	LOCAL lcFileName, lcFileExt, lcSQL, lcCursor, nrRegistos
	STORE '' TO lcSQL
	STORE 0 to nrRegistos

	uv_msg = "O ficheiro pode incluir os seguintes campos:"
	uv_msg = uv_msg + chr(13) + CHR(9) + "Ref (Obrigat�rio)"
	uv_msg = uv_msg + chr(13) + CHR(9) + "Design"

	uf_perguntalt_chama(uv_msg ,"OK","",64)
	
	&& verificar nome do ficheiro
	**lcFileName = getfile()
	lcFileName = uf_gerais_getFile(ALLTRIM(uf_gerais_getUmValor("B_terminal", "pathReport", "terminal = '" + ALLTRIM(myTerm) + "'")), 'xls')

	IF EMPTY(lcFileName)
		uf_perguntalt_chama("Deve especificar o ficheiro de importa��o. Por favor verifique.", "OK", "", 64)
		RETURN .f.
	ENDIF

	&& Valida ficheiro
	lcLocal = ["] + ALLTRIM(lcFileName) + ["]
	IF !FILE(lcLocal)
		uf_perguntalt_chama("A localiza��o especificada para importa��o do ficheiro � inv�lida. Por favor verifique.", "OK", "", 64)
		RETURN .f.
	ENDIF
	
	&& Informa��o do ficheiro 
	uf_perguntalt_chama("O documento deve estar no formato .XLS e conter uma coluna com o nome REF, com as refer�ncias a importar. O nome do ficheiro n�o deve conter espa�os.","OK","",64)	

	&& cursor auxiliar p convert ref
	IF used("uCrsTempFile")
		fecha("uCrsTempFile")
	ENDIF

	create cursor uCrsTempFile (sel l, ref c(18), design c(100))
		
				
	&& Valida extensoes dos ficheiros
	lcFileExt = UPPER(RIGHT(ALLTRIM(lcFileName),3))
	
	&& eliminar dados do ficheiro de pesquisa
	SELECT uCrsPesqMultiFile
	DELETE all 
	
	DO CASE
		&& ficheiro do tipo XLS
		CASE lcFileExt == "XLS"
			&& Ler o xls para um cursor
			IF !uf_gerais_xls2Cursor(lcFileName, "", "uCrsAuxProdutos", .T.)
				uf_perguntalt_chama("N�o foi poss�vel ler o ficheiro indicado. Por favor contacte o Suporte.","OK","",64)	
				RETURN .f.
			ENDIF
		
			&& verifica se todos os campos est�o ok
			SELECT uCrsAuxProdutos
			IF TYPE('uCrsAuxProdutos.ref') = "U" 
				uf_perguntalt_chama("O nome da(s) coluna(s) no ficheiro n�o � o correto. Por favor valide.", "OK", "", 64)
				RETURN  .f.
			ENDIF
			
			nrRegistos = reccount("uCrsAuxProdutos")
			
			regua(0, nrRegistos, "A processar importa��o do ficheiro...")

			SELECT uCrsAuxProdutos
			GO TOP 
			SCAN FOR !EMPTY(uCrsAuxProdutos.ref) 
				
				SELECT uCrsTempFile 
				APPEND BLANK 
			
				IF TYPE('uCrsAuxProdutos.ref') == "N"
					replace uCrsTempFile.ref WITH astr(uCrsAuxProdutos.ref)
				ELSE 
					replace uCrsTempFile.ref WITH uCrsAuxProdutos.ref
				ENDIF 		
			
				&& atualiza regua 
				regua(1, RECNO("uCrsAuxProdutos"), "A verificar produtos") 
			
				** Verifica se a referencia existe
				lcSQL = ''
				TEXT TO lcSQL TEXTMERGE NOSHOW
					SELECT  Sel = CONVERT(bit,0), ref, design FROM st (nolock) WHERE ref = '<<ALLTRIM(uCrsTempFile.ref)>>' AND site_nr = <<mySite_nr>>
				ENDTEXT 
				IF !uf_gerais_actGrelha("", "uCrsAuxRef", lcSql)
					uf_perguntalt_chama("N�O FOI POSSIVEL VERIFICAR A INFORMA��O DO ARTIGO:" + STR(uCrsAuxProdutos.ref) + ". POR FAVOR CONTACTE O SUPORTE.","OK","", 16)
					RETURN .f.
				ENDIF

				&& insere linhas
				IF RECCOUNT("uCrsAuxRef") > 0
					SELECT uCrsPesqMultiFile
					INSERT INTO uCrsPesqMultiFile SELECT * FROM uCrsAuxRef
				ENDIF

				SELECT uCrsAuxProdutos
			ENDSCAN
		OTHERWISE
			**
			uf_perguntalt_chama("Tipo de Ficheiro inv�lido. Deve indicar um ficheiro do tipo .XLS.", "OK", "", 32)
			RETURN .f.
	ENDCASE

	&& fecha regua e informa utilizador da importacao conclu�da
	SELECT uCrsPesqMultiFile
	GO TOP 
	
	regua(2)
	uf_perguntalt_chama("Importa��o conclu�da com sucesso.","OK","",64)	
	
	&& fecha cursores utilizados
	IF USED("uCrsTempFile")
		fecha("uCrsTempFile")
	ENDIF

	IF USED("uCrsAuxProdutos")
		fecha("uCrsAuxProdutos")
	ENDIF	
	
	IF USED("uCrsAuxRef")
		fecha("uCrsAuxRef")
	ENDIF	
	
ENDFUNC

FUNCTION uf_VALORESCOMBOFILE_docs_vertodos

	IF USED("ucrsComboDoc")
		FECHA("ucrsComboDoc")
	ENDIF
	lcSQL = ""
	DO CASE 
        CASE UPPER(ALLTRIM(VALORESCOMBOFILE.objecto)) == "IMPORTARFACT.TXTDOCS"
	
			TEXT TO lcSQL TEXTMERGE NOSHOW
				exec up_perfis_validaAcessoDocs '<<ch_userno>>', '<<ALLTRIM(ch_grupo)>>', 'Introduzir', 'FT,BO,FO', 0, '<<ALLTRIM(mySite)>>'
			ENDTEXT

        CASE UPPER(ALLTRIM(VALORESCOMBOFILE.objecto)) == "IMPORTARDOC.TXTDOCS"
       
			TEXT TO lcSQL TEXTMERGE NOSHOW
				exec up_perfis_validaAcessoDocs '<<ch_userno>>', '<<ALLTRIM(ch_grupo)>>', 'Visualizar', 'BO,FO,FT', 0, '<<ALLTRIM(mySite)>>'
			ENDTEXT

		CASE TYPE("DOCUMENTOS") != "U"
		
			TEXT TO lcSQL TEXTMERGE NOSHOW
				exec up_perfis_validaAcessoDocs '<<ch_userno>>', '<<ALLTRIM(ch_grupo)>>', 'Introduzir', 'FO,BO', 0, '<<ALLTRIM(mySite)>>'
			ENDTEXT
		CASE TYPE("FACTURACAO") != "U"
	
			TEXT TO lcSQL TEXTMERGE NOSHOW
				exec up_perfis_validaAcessoDocs '<<ch_userno>>', '<<ALLTRIM(ch_grupo)>>', 'Introduzir', 'FT', 0, '<<ALLTRIM(mysite)>>'
			ENDTEXT
		OTHERWISE 
		
	ENDCASE 

	
	If !uf_gerais_actGrelha("", "ucrsComboDoc", lcSql)
		uf_perguntalt_chama("N�O FOI POSS�VEL ENCONTRAR DOCUMENTOS.","OK","",16)
		RETURN .f.
	ENDIF 
	
	IF TYPE("FACTURACAO") != "U"
		SELECT ucrsComboDoc
		GO TOP 
		DELETE FROM ucrsComboDoc WHERE inativo=.t.
		SELECT ucrsComboDoc
		GO TOP
	ENDIF 
		
	IF USED("ucrsComboDoc2")
		FECHA("ucrsComboDoc2")
	ENDIF 


	
	IF VARTYPE(lcorigempesq)!="U" AND !INLIST(UPPER(ALLTRIM(VALORESCOMBOFILE.objecto)),"IMPORTARDOC.TXTDOCS", "IMPORTARFACT.TXTDOCS")
		DO CASE 
			CASE ALLTRIM(lcorigempesq) == 'CLIENTES'
				SELECT nomedoc FROM ucrsComboDoc WHERE bdempresas = 'CL' into CURSOR ucrsComboDoc2 READWRITE 
			CASE ALLTRIM(lcorigempesq) == 'FORNECEDORES'
				SELECT nomedoc FROM ucrsComboDoc WHERE bdempresas = 'FL' OR bdempresas = 'AG' into CURSOR ucrsComboDoc2 READWRITE 
			OTHERWISE 
				SELECT nomedoc FROM ucrsComboDoc into CURSOR ucrsComboDoc2 READWRITE 
		ENDCASE 
	ELSE
		SELECT .f. as sel, nomedoc FROM ucrsComboDoc into CURSOR ucrsComboDoc2 READWRITE 
	ENDIF 
	

	
	SELECT uCrsPesqMultiFile 
	DELETE ALL 
	SELECT ucrsComboDoc2 
	GO TOP 
	SCAN 
		SELECT uCrsPesqMultiFile 
		APPEND BLANK
		replace uCrsPesqMultiFile.nomedoc WITH ALLTRIM(ucrsComboDoc2.nomedoc)
		SELECT ucrsComboDoc2 
	ENDSCAN 
	SELECT uCrsPesqMultiFile 
	GO TOP 
	VALORESCOMBOFILE.refresh
ENDFUNC
