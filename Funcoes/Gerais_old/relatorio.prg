**
FUNCTION uf_relatorio_chama
	IF !USED("ucrsMeses")
		CREATE CURSOR ucrsMeses (mes c(2), nome c(20))
		SELECT ucrsMeses 
		APPEND BLANK
		Replace ucrsMeses.mes WITH '1'
		Replace ucrsMeses.nome WITH 'Janeiro'

		APPEND BLANK
		Replace ucrsMeses.mes WITH '2'
		Replace ucrsMeses.nome WITH 'Fevereiro'
		
		APPEND BLANK
		Replace ucrsMeses.mes WITH '3'
		Replace ucrsMeses.nome WITH 'Mar�o'
		
		APPEND BLANK
		Replace ucrsMeses.mes WITH '4'
		Replace ucrsMeses.nome WITH 'Abril'
		
		APPEND BLANK
		Replace ucrsMeses.mes WITH '5'
		Replace ucrsMeses.nome WITH 'Maio'
		
		APPEND BLANK
		Replace ucrsMeses.mes WITH '6'
		Replace ucrsMeses.nome WITH 'Junho'
		
		APPEND BLANK
		Replace ucrsMeses.mes WITH '7'
		Replace ucrsMeses.nome WITH 'Julho'
		
		APPEND BLANK
		Replace ucrsMeses.mes WITH '8'
		Replace ucrsMeses.nome WITH 'Agosto'
		
		APPEND BLANK
		Replace ucrsMeses.mes WITH '9'
		Replace ucrsMeses.nome WITH 'Setembro'
		
		APPEND BLANK
		Replace ucrsMeses.mes WITH '10'
		Replace ucrsMeses.nome WITH 'Outubro'
		
		APPEND BLANK
		Replace ucrsMeses.mes WITH '11'
		Replace ucrsMeses.nome WITH 'Novembro'
		
		APPEND BLANK
		Replace ucrsMeses.mes WITH '12'
		Replace ucrsMeses.nome WITH 'Dezembro'
	ENDIF

	IF !USED("ucrsAnos")
		
		CREATE CURSOR ucrsAnos(ano c(20))
		lcAno = YEAR(DATE())

		DO WHILE lcAno > YEAR(DATE())-10
			Select ucrsAnos
			Append Blank
			replace ucrsAnos.ano With ALLTRIM(Str(lcAno))
			lcAno = lcAno - 1
		ENDDO		
	ENDIF
	
	IF TYPE("RELATORIO") == "U"
		DO FORM RELATORIO
	ELSE
		RELATORIO.show
	ENDIF

ENDFUNC 



**
FUNCTION uf_relatorio_gravar
	LOCAL lcCont
	lcCont = 0
	
	** valida��es **
	IF EMPTY(ALLTRIM(relatorio.pagina2.wdgselreport.txtAnalise.value))
		uf_perguntalt_chama("DEVE SELECIONAR UMA AN�LISE PARA EXECUTAR. POR FAVOR VERIFIQUE.","OK","",64)
		RETURN .f.
	ENDIF
	

	FOR EACH mf IN Relatorio.pagina2.objects
		IF LIKE("*WDGBROWSER*",UPPER(mf.name)) == .t.
			lcCont = lcCont + 1
		ENDIF
	ENDFOR

	IF lcCont >= uf_gerais_getParameter("ADM0000000201","NUM")
		uf_perguntalt_chama("Atingiu o limite de relat�rios em simult�neo.","OK","",64)
		RETURN .f.
	ELSE
	
		&&colocar outros browser invisiveis
		LOCAL lcObjInv
		FOR EACH mf IN Relatorio.pagina2.objects
			IF LIKE("*WDGBROWSER*",UPPER(mf.name)) == .t.
				lcObjInv = "Relatorio.pagina2." + mf.name + "._webBrowser41"
				&lcObjInv..width = 0
			ENDIF
		ENDFOR	
		
		IF lcCont == 0
			lcNome = "WDGBROWSER"
		ELSE
			lcNome = "WDGBROWSER" + astr(lcCont + 1)
			lcCont = lcCont + 1
		ENDIF

		&&renomeia objectos
		IF USED("uCrsListaBrowser")
			fecha("uCrsListaBrowser")
		ENDIF 
		
		CREATE CURSOR uCrsListaBrowser (nome C(254), top N)
		SELECT uCrsListaBrowser
		index on uCrsListaBrowser.top TAG top ASCENDING

		LOCAL lcObjBr
		FOR EACH mf IN Relatorio.pagina2.objects
			IF LIKE("*WDGBROWSER*",UPPER(mf.name)) == .t.
				SELECT uCrsListaBrowser
				APPEND BLANK
				replace uCrsListaBrowser.nome WITH SYS(1272,Relatorio.pagina2) +  "." + ALLTRIM(mf.name)
				lcObjBr = "Relatorio.pagina2." + mf.name
				replace uCrsListaBrowser.top WITH &lcObjBr..top
			ENDIF
		ENDFOR

		uf_wdgfiltroreport_renomeia()
		**********************			
		
		Relatorio.pagina2.wdgfiltroreport.browser = "relatorio.pagina2." + lcNome
		relatorio.pagina2.adicionaopcao(lcNome,"W",3,6,1,4,"","","RELAT�RIO",[uf_wdgbrowser_abreURL with "about:blank", sys(1272,this)],0,"240,240,240","204,204,204",0,"","")
		Relatorio.pagina2.wdgfiltroreport.numreports = Relatorio.pagina2.wdgfiltroreport.numreports + 1
		uf_analises_executar(Relatorio.pagina2.wdgfiltroreport.browser)
	ENDIF 	


	
	
ENDFUNC 


**
FUNCTION uf_relatorio_sair
	
	relatorio.hide
	relatorio.release
ENDFUNC



