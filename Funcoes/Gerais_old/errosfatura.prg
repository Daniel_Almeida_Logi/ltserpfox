**
FUNCTION uf_comlogsloja_chama
	IF !uf_gerais_validaPermUser(ch_userno,ch_grupo,'Configura��o do sistema')
		**
	ELSE
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE CONSULTAR AS CONFIGURA��ES DE SISTEMA.","OK","",48)
		RETURN .f.
	ENDIF
	
	IF !USED("uCrscomlogs")
*!*			lcSQL = ''
*!*			TEXT TO lcSql NOSHOW TEXTMERGE
*!*				select type, cast(description as varchar(200)) as description, date, site, stamp from LogsExternal.dbo.ExternalCommunicationLogs
*!*				where convert(varchar(8),date,112) = convert(varchar(8),getdate(),112) and 0=1
*!*			ENDTEXT
*!*						
*!*			IF !uf_gerais_actGrelha("",[uCrscomlogs],lcSql)
*!*				uf_perguntalt_chama("N�O FOI POSS�VEL OBTER O LOG DE COMUNICA��ES.","OK","",16)
*!*				RETURN .f.
*!*			ENDIF 

        TEXT TO lcSql TEXTMERGE NOSHOW 
            exec up_logs_getDocsFarm
        ENDTEXT

			
		IF !uf_gerais_actGrelha("",[uCrscomlogs],lcSql)
			uf_perguntalt_chama("N�O FOI POSS�VEL OBTER O LOG DE COMUNICA��ES.","OK","",16)
			RETURN .f.
		ENDIF 
	ENDIF
	PUBLIC lcTipo, lcServico, lcTabela, lcDatade, LcDataa, lcSite
	STORE '' TO lcTipo, lcServico, lcTabela, lcSite
	STORE DATE() TO lcDatade, LcDataa
	IF TYPE("COMLOGSLOJA") == "U"
		DO FORM COMLOGSLOJA
	ELSE
		COMLOGSLOJA.show
	ENDIF

    

ENDFUNC

FUNCTION uf_comlogsloja_gravar

ENDFUNC 

** adiciona op��es ao menu lateral
FUNCTION uf_comlogsloja_menu

	&& Configura menu principal
	COMLOGSLOJA.menu1.adicionaOpcao("actualizar","Actualizar",myPath + "\imagens\icons\actualizar_w.png","uf_comlogsloja_aplicaFiltros", "A")
	**COMLOGSLOJA.menu1.adicionaOpcao("limpar","Limpar",myPath + "\imagens\icons\limpar_w.png","uf_comlogsloja_LimpaFiltros", "L")
	COMLOGSLOJA.menu1.adicionaOpcao("verdescr","Descri��o",myPath + "\imagens\icons\detalhe.png","uf_comlogsloja_verdescr", "D")
	COMLOGSLOJA.menu1.adicionaOpcao("verxml","XML",myPath + "\imagens\icons\detalhe.png","uf_comlogsloja_verxml", "X")

	COMLOGSLOJA.menu1.estado("", "SHOW", "", .t., "Sair", .t.)
		
	COMLOGSLOJA.Icon = ALLTRIM(mypath) + "\imagens\icons\logitools.ico"
ENDFUNC


**
FUNCTION uf_comlogsloja_aplicaFiltros
	LOCAL lcRecordSource , lcRemoteSQL 

    loGrid = comlogsloja.grdpesq
    lcCursor = "uCrscomlogs"

    lnColumns = loGrid.ColumnCount

    IF lnColumns > 0

        DIMENSION laControlSource[lnColumns]
        FOR lnColumn = 1 TO lnColumns
            laControlSource[lnColumn] = loGrid.Columns[lnColumn].ControlSource
        ENDFOR

    ENDIF

    loGrid.RecordSource = ""
		
    LOCAL lcbd, lcremotesql
    lcbd = SUBSTR(uf_gerais_getparameter_site('ADM0000000090', 'TEXT', mysite), AT('.[', uf_gerais_getparameter_site('ADM0000000090', 'TEXT', mysite))+1, 30)
    lcremotesql = LEFT(uf_gerais_getparameter_site('ADM0000000090', 'TEXT', mysite), AT('.[', uf_gerais_getparameter_site('ADM0000000090', 'TEXT', mysite))-1)

    DO CASE
        CASE COMLOGSLOJA.porintegrarext

            TEXT TO lcsql TEXTMERGE NOSHOW
                DECLARE @RunStoredProcSQL VARCHAR(1000);
                DECLARE @dataFim VARCHAR(8) = CONVERT(varchar, GETDATE(), 112)
                SET @RunStoredProcSQL = 'EXEC <<lcbd>>.[dbo].up_get_erros_x3_central ''<<ALLTRIM(mysite)>>'', ''19000101'', ''' + @dataFim + ''', ''PORINTEGRAR''';
                EXEC (@RunStoredProcSQL) AT <<lcRemoteSQL >>;
            ENDTEXT

        CASE COMLOGSLOJA.porintegrarErro

            TEXT TO lcsql TEXTMERGE NOSHOW
                DECLARE @RunStoredProcSQL VARCHAR(1000);
                DECLARE @dataFim VARCHAR(8) = CONVERT(varchar, GETDATE(), 112)
                SET @RunStoredProcSQL = 'EXEC <<lcbd>>.[dbo].up_get_erros_x3_central ''<<ALLTRIM(mysite)>>'', ''19000101'', ''' + @dataFim + ''', ''ERRO''';
                EXEC (@RunStoredProcSQL) AT <<lcRemoteSQL >>;
            ENDTEXT

        OTHERWISE

            TEXT TO lcSql TEXTMERGE NOSHOW
                exec up_logs_getDocsFarm
            ENDTEXT

    ENDCASE

    _clipText = lcSql

    regua(1,2,"A carregar dados...")
        
    IF !uf_gerais_actGrelha("",[uCrscomlogs],lcSql)
        uf_perguntalt_chama("N�O FOI POSS�VEL OBTER O LOG DE COMUNICA��ES.","OK","",16)
        regua(2)
        RETURN .f.
    ENDIF 

    regua(2)
    
    loGrid.RecordSource = lcCursor
	
    FOR lnColumn = 1 TO lnColumns
    loGrid.Columns[lnColumn].ControlSource = laControlSource[lnColumn]
    ENDFOR
	
	SELECT uCrscomlogs
	**GO Top
	COMLOGSLOJA.grdpesq.refresh

ENDFUNC

**
*!*	FUNCTION uf_comlogsloja_LimpaFiltros
*!*		COMLOGSLOJA.txttype.value=''
*!*		COMLOGSLOJA.txtname.value=''
*!*		COMLOGSLOJA.txttablename.value=''
*!*		COMLOGSLOJA.txtsite.value=''
*!*		COMLOGSLOJA.txtde.Value = uf_gerais_getdate(DATE())
*!*		COMLOGSLOJA.txta.Value = uf_gerais_getdate(DATE())
*!*	ENDFUNC


FUNCTION uf_comlogsloja_sair	

	&& fecha painel
	COMLOGSLOJA.hide
	COMLOGSLOJA.release 
ENDFUNC

FUNCTION uf_comlogsloja_verdescr
	LOCAL lcStampPesq, lcRemoteSQL 
	lcRemoteSQL = left(uf_gerais_getParameter_site('ADM0000000090', 'TEXT', mySite),at('.[',uf_gerais_getParameter_site('ADM0000000090', 'TEXT', mySite))-1)
	SELECT uCrscomlogs
	lcStampPesq = ALLTRIM(uCrscomlogs.stamp)
	lcSQL = ''
	TEXT TO lcSql NOSHOW TEXTMERGE
		select description, externalResponse from <<lcRemoteSQL>>.LogsExternal.dbo.ExternalCommunicationLogs where stamp='<<lcStampPesq>>'
	ENDTEXT
		
	IF !uf_gerais_actGrelha("",[uCrsLogDescr],lcSql)
		uf_perguntalt_chama("N�O FOI POSS�VEL OBTER O LOG DE COMUNICA��ES.","OK","",16)
		RETURN .f.
	ENDIF 
	**MESSAGEBOX(ALLTRIM(uCrsLogDescr.description))
	comlogsloja.Edit1.value=ALLTRIM(uCrsLogDescr.description)
	comlogsloja.Edit1.refresh
	fecha("uCrsLogDescr")
ENDFUNC 

FUNCTION uf_comlogsloja_verxml
	LOCAL lcStampPesq, lcRemoteSQL 
	lcRemoteSQL = left(uf_gerais_getParameter_site('ADM0000000090', 'TEXT', mySite),at('.[',uf_gerais_getParameter_site('ADM0000000090', 'TEXT', mySite))-1)
	SELECT uCrscomlogs
	lcStampPesq = ALLTRIM(uCrscomlogs.stamp)
	lcSQL = ''
	TEXT TO lcSql NOSHOW TEXTMERGE
		select description, externalResponse from <<lcRemoteSQL>>.LogsExternal.dbo.ExternalCommunicationLogs where stamp='<<lcStampPesq>>'
	ENDTEXT
		
	IF !uf_gerais_actGrelha("",[uCrsLogDescr],lcSql)
		uf_perguntalt_chama("N�O FOI POSS�VEL OBTER O LOG DE COMUNICA��ES.","OK","",16)
		RETURN .f.
	ENDIF 
	**MESSAGEBOX(ALLTRIM(uCrsLogDescr.externalResponse))
	comlogsloja.Edit1.value=ALLTRIM(uCrsLogDescr.externalResponse)
	comlogsloja.Edit1.refresh
	fecha("uCrsLogDescr")
ENDFUNC 
