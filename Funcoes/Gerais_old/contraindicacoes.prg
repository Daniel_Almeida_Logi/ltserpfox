FUNCTION uf_contraindicacoes_chama
	
	LOCAL lcteste 
	IF uf_gerais_getparameter("ADM0000000227", "BOOL")
		lcteste = .T.
	ELSE
		lcteste = .F.
	ENDIF
	IF(myCallVaccineService == .F.)
		IF(!uf_vacinacao_verifyToCallService(1, 'SPMS', 1, 'vaccineInfo', .T.,'A chamar servi�o',lcteste ,'',''))
			RETURN .F.
		ENDIF
	ENDIF
	
	IF !USED("ucrsSendVaccineRespNoVacMotives")
		uf_perguntalt_chama("N�o foram devolvidas contra-indica��es","OK","",16)
		myCallVaccineService  = .F.
		RETURN .F.
	ENDIF
	

	
	myCallVaccineService  = .T.
	
	DO FORM CONTRAINDICACOES
	
ENDFUNC


FUNCTION uf_contraindicacoes_sair
			
	CONTRAINDICACOES.hide
	CONTRAINDICACOES.release
	RELEASE CONTRAINDICACOES
ENDFUNC