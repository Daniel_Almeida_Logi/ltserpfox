**
FUNCTION uf_COMLOGS_chama
	IF !uf_gerais_validaPermUser(ch_userno,ch_grupo,'Integra��es')
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE CONSULTAR O MENU DE INTEGRA��ES","OK","",48)
		RETURN .f.
	ENDIF
	
	IF !USED("uCrscomlogs")
		lcSQL = ''
		TEXT TO lcSql NOSHOW TEXTMERGE
			exec up_logs_importados '', '', '', '', '', ''
		ENDTEXT
					
		IF !uf_gerais_actGrelha("",[uCrscomlogs],lcSql)
			uf_perguntalt_chama("N�O FOI POSS�VEL OBTER O LOG DE COMUNICA��ES.","OK","",16)
			RETURN .f.
		ENDIF 
	ENDIF
	PUBLIC lcTipo, lcServico, lcTabela, lcDatade, LcDataa, lcSite
	STORE '' TO lcTipo, lcServico, lcTabela, lcSite
	STORE DATE() TO lcDatade, LcDataa
	IF TYPE("COMLOGS") == "U"
		DO FORM COMLOGS
	ELSE
		COMLOGS.show
	ENDIF

ENDFUNC

FUNCTION uf_COMLOGS_gravar

ENDFUNC 

** adiciona op��es ao menu lateral
FUNCTION uf_comlogs_menu

	&& Configura menu principal
	COMLOGS.menu1.adicionaOpcao("actualizar","Actualizar",myPath + "\imagens\icons\actualizar_w.png","uf_comlogs_aplicaFiltros", "A")
	COMLOGS.menu1.adicionaOpcao("limpar","Limpar",myPath + "\imagens\icons\limpar_w.png","uf_comlogs_LimpaFiltros", "L")
	COMLOGS.menu1.adicionaOpcao("verdescr","Descri��o",myPath + "\imagens\icons\detalhe.png","uf_comlogs_verdescr", "D")
	COMLOGS.menu1.adicionaOpcao("verxml","XML",myPath + "\imagens\icons\detalhe.png","uf_comlogs_verxml", "X")

	COMLOGS.menu1.estado("", "SHOW", "", .t., "Sair", .t.)
		
	COMLOGS.Icon = ALLTRIM(mypath) + "\imagens\icons\logitools.ico"
ENDFUNC


**
FUNCTION uf_comlogs_aplicaFiltros
	LOCAL lcRecordSource 
	with comlogs.grdpesq
		lcRecordSource = .RecordSource
		.RecordSource = ""

		lcSQL = ''

		IF !COMLOGS.porIntegrar
   
			TEXT TO lcSql NOSHOW TEXTMERGE
				exec up_logs_importados '<<ALLTRIM(comlogs.txttype.value)>>', '<<ALLTRIM(comlogs.txtname.value)>>', '<<ALLTRIM(comlogs.txtsite.value)>>', '<<uf_gerais_getDate(comlogs.txtDe.value, "SQL")>>', '<<uf_gerais_getDate(comlogs.txtA.value, "SQL")>>', '<<ALLTRIM(comlogs.txttablename.value)>>'
			ENDTEXT
		
		ELSE

			TEXT TO lcSql TEXTMERGE NOSHOW
				exec up_logs_porImportar '<<ALLTRIM(comlogs.txttype.value)>>', '<<ALLTRIM(comlogs.txtname.value)>>', '<<ALLTRIM(comlogs.txtsite.value)>>', '<<uf_gerais_getDate(comlogs.txtDe.value, "SQL")>>', '<<uf_gerais_getDate(comlogs.txtA.value, "SQL")>>'
			ENDTEXT

		ENDIF
		
			
		IF !uf_gerais_actGrelha("",[uCrscomlogs],lcSql)
			uf_perguntalt_chama("N�O FOI POSS�VEL OBTER O LOG DE COMUNICA��ES.","OK","",16)
			RETURN .f.
		ENDIF 
		
		.RecordSource = lcRecordSource
	endwith
	SELECT uCrscomlogs
	**GO Top
	COMLOGS.grdpesq.refresh

ENDFUNC

**
FUNCTION uf_comlogs_LimpaFiltros
	comlogs.txttype.value=''
	comlogs.txtname.value=''
	comlogs.txttablename.value=''
	comlogs.txtsite.value=''
	comlogs.txtde.Value = uf_gerais_getdate(DATE())
	comlogs.txta.Value = uf_gerais_getdate(DATE())
ENDFUNC


FUNCTION uf_COMLOGS_sair	

	&& fecha painel
	COMLOGS.hide
	COMLOGS.release 
ENDFUNC

FUNCTION uf_comlogs_verdescr
	LOCAL lcStampPesq
	SELECT uCrscomlogs
	lcStampPesq = ALLTRIM(uCrscomlogs.stamp)
	lcSQL = ''
	TEXT TO lcSql NOSHOW TEXTMERGE
		select description, externalResponse from LogsExternal.dbo.ExternalCommunicationLogs where stamp='<<lcStampPesq>>'
	ENDTEXT
		
	IF !uf_gerais_actGrelha("",[uCrsLogDescr],lcSql)
		uf_perguntalt_chama("N�O FOI POSS�VEL OBTER O LOG DE COMUNICA��ES.","OK","",16)
		RETURN .f.
	ENDIF 
	**MESSAGEBOX(ALLTRIM(uCrsLogDescr.description))
	comlogs.Edit1.value=ALLTRIM(uCrsLogDescr.description)
	comlogs.Edit1.refresh
	fecha("uCrsLogDescr")
ENDFUNC 

FUNCTION uf_comlogs_verxml
	LOCAL lcStampPesq
	SELECT uCrscomlogs
	lcStampPesq = ALLTRIM(uCrscomlogs.stamp)
	lcSQL = ''
	TEXT TO lcSql NOSHOW TEXTMERGE
		select description, externalResponse from LogsExternal.dbo.ExternalCommunicationLogs where stamp='<<lcStampPesq>>'
	ENDTEXT
		
	IF !uf_gerais_actGrelha("",[uCrsLogDescr],lcSql)
		uf_perguntalt_chama("N�O FOI POSS�VEL OBTER O LOG DE COMUNICA��ES.","OK","",16)
		RETURN .f.
	ENDIF 
	**MESSAGEBOX(ALLTRIM(uCrsLogDescr.externalResponse))
	comlogs.Edit1.value=ALLTRIM(uCrsLogDescr.externalResponse)
	comlogs.Edit1.refresh
	fecha("uCrsLogDescr")
ENDFUNC 
