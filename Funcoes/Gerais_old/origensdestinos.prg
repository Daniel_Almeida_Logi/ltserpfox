
**
FUNCTION uf_OrigensDestinos_Chama
	LPARAMETERS lcOperacao
	
	PUBLIC myStampOriDest, myTipoDoc
	myStampOriDest = ''
	
	IF EMPTY(lcOperacao)
		RETURN .f.
	ENDIF
	**Cria cursor inicial
	TEXT TO lcSQL TEXTMERGE NOSHOW
		SELECT TIPO = '', DOC='SEM RESULTADOS', Informacao = 'SEM RESULTADOS',stamporidest = ''
	Endtext
	IF !uf_gerais_actGrelha("","ucrsOriDest",lcSQL)	
		uf_perguntalt_chama("OCORREU UMA ANOMALIA NA VERIFICA플O DAS ORIGENS E DESTINOS DO DOCUMENTO.","OK","",16)
		RETURN .f.
	ENDIF 	
	
	DO CASE
		CASE lcOperacao == "FTORIGENS"
					
			TEXT TO lcSQL TEXTMERGE NOSHOW
				exec up_gerais_FTORIGENS '<<Alltrim(Fi.oFistamp)>>', '<<Alltrim(Fi.Bistamp)>> '
			ENDTEXT 
**Messagebox(lcSQL)				
			IF !uf_gerais_actGrelha("","ucrsOriDest",lcSQL)
				uf_perguntalt_chama("OCORREU UMA ANOMALIA NA VERIFICA플O DAS ORIGENS DO DOCUMENTO.","OK","",16)
				RETURN .f.
			ENDIF
		CASE lcOperacao == "FTDESTINOS"
			Select Fi
			TEXT TO lcSQL TEXTMERGE NOSHOW
				exec up_gerais_FTDESTINOS '<<Alltrim(Fi.Fistamp)>>','<<Ft.Ftstamp>> ' 
			ENDTEXT
**Messagebox(lcSQL)				
			IF !uf_gerais_actGrelha("","ucrsOriDest",lcSQL)
				uf_perguntalt_chama("OCORREU UMA ANOMALIA NA VERIFICA플O DOS DESTINOS DO DOCUMENTO.","OK","",16)
				RETURN .f.
			ENDIF
		CASE lcOperacao == "DOCORIGENS"
			If myTipoDoc == "FO"
				TEXT TO lcSQL TEXTMERGE NOSHOW
					exec up_gerais_DOCORIGENS_FO '<<Alltrim(Fn.oFnstamp)>>', '<<Alltrim(Fn.Bistamp)>>'
				ENDTEXT
**Messagebox(lcSQL)				
				IF !uf_gerais_actGrelha("","ucrsOriDest",lcSQL)
					uf_perguntalt_chama("OCORREU UMA ANOMALIA NA VERIFICA플O DAS ORIGENS DO DOCUMENTO.","OK","",16)
					RETURN .f.
				ENDIF
			Endif
			If myTipoDoc == "BO"
				Select Bi
				Select Bi2
				LOCATE FOR bi2.bi2stamp = bi.bistamp
				
				lcSQL = ""
				TEXT TO lcSQL TEXTMERGE NOSHOW
					exec up_gerais_DOCORIGENS_BO '<<Alltrim(Bi.oBistamp)>>', '<<cabDoc.cabstamp>>', '<<Alltrim(Bi2.fnstamp)>>'
				ENDTEXT 
**Messagebox(lcSQL)				
				IF !uf_gerais_actGrelha("","ucrsOriDest",lcSQL)
					uf_perguntalt_chama("OCORREU UMA ANOMALIA NA VERIFICA플O DAS ORIGENS DO DOCUMENTO.","OK","",16)
					RETURN .f.
				ENDIF
			Endif
		CASE lcOperacao == "DOCDESTINOS"
			If myTipoDoc == "FO"
				lcSQL = ""
				TEXT TO lcSQL TEXTMERGE NOSHOW
					exec up_gerais_DOCDESTINOS_FO '<<Alltrim(Fn.Fnstamp)>>', '<<Alltrim(cabDoc.cabstamp)>>'
				ENDTEXT
				
				IF !uf_gerais_actGrelha("","ucrsOriDest",lcSQL)
					uf_perguntalt_chama("OCORREU UMA ANOMALIA NA VERIFICA플O DOS DESTINOS DO DOCUMENTO.","OK","",16)
					RETURN .f.
				ENDIF
			ENDIF
			If myTipoDoc == "BO"
				Select Bi
				Select Bi2
				lcSQL = ""
				TEXT TO lcSQL TEXTMERGE NOSHOW
					exec up_gerais_DOCDESTINOS_BO '<<Alltrim(Bi.Bistamp)>>'
				Endtext
**Messagebox(lcSQL)				
				IF !uf_gerais_actGrelha("","ucrsOriDest",lcSQL)
					uf_perguntalt_chama("OCORREU UMA ANOMALIA NA VERIFICA플O DOS DESTINOS DO DOCUMENTO.","OK","",16)
					RETURN .f.
				ENDIF
			ENDIF
		CASE lcOperacao == "FTASSOCIADOS"
		
			Select ucrsDadosFacturacaoAssociados
			TEXT TO lcSQL TEXTMERGE NOSHOW
				SELECT fistamp, ftstamp FROM fi (nolock) WHERE fistamp = '<<Alltrim(ucrsDadosFacturacaoAssociados.Fistamp)>>'
			ENDTEXT
			IF !uf_gerais_actGrelha("","ucrsOriDestAux",lcSQL)
				uf_perguntalt_chama("OCORREU UMA ANOMALIA NA VERIFICA플O DOS DESTINOS DO DOCUMENTO.","OK","",16)
				RETURN .f.
			ENDIF
			
			IF RECCOUNT("ucrsOriDestAux") > 0
				Select ucrsOriDestAux
				TEXT TO lcSQL TEXTMERGE NOSHOW
					exec up_gerais_FTORIGENS '<<Alltrim(ucrsOriDestAux.Fistamp)>>','<<ucrsOriDestAux.Ftstamp>> ' 
				ENDTEXT
				IF !uf_gerais_actGrelha("","ucrsOriDest",lcSQL)
					uf_perguntalt_chama("OCORREU UMA ANOMALIA NA VERIFICA플O DOS DESTINOS DO DOCUMENTO.","OK","",16)
					RETURN .f.
				ENDIF
			ENDIF 
		CASE lcOperacao == "MARCACOESSERV"
			
			

			Select uCrsServMarcacao
			
			TEXT TO lcSQL TEXTMERGE NOSHOW
				SELECT fistamp, ftstamp FROM fi (nolock) WHERE (fi.fistamp = '<<Alltrim(uCrsServMarcacao.uFistamp)>>' OR fi.fistamp = '<<Alltrim(uCrsServMarcacao.eFistamp)>>')
			ENDTEXT
			IF !uf_gerais_actGrelha("","ucrsOriDestAux",lcSQL)
				uf_perguntalt_chama("OCORREU UMA ANOMALIA NA VERIFICA플O DOS DESTINOS DO DOCUMENTO.","OK","",16)
				RETURN .f.
			ENDIF
			
			IF RECCOUNT("ucrsOriDestAux") > 0
				Select ucrsOriDestAux
				TEXT TO lcSQL TEXTMERGE NOSHOW
					exec up_gerais_FTORIGENS '<<Alltrim(ucrsOriDestAux.Fistamp)>>','<<ucrsOriDestAux.Ftstamp>> ' 
				ENDTEXT
				IF !uf_gerais_actGrelha("","ucrsOriDest",lcSQL)
					uf_perguntalt_chama("OCORREU UMA ANOMALIA NA VERIFICA플O DOS DESTINOS DO DOCUMENTO.","OK","",16)
					RETURN .f.
				ENDIF
			ENDIF 
		OTHERWISE
		
	
	
			****************************
	ENDCASE
	

	IF RECCOUNT("ucrsOriDest") == 0
		**Cria cursor inicial
		TEXT TO lcSQL TEXTMERGE NOSHOW
			SELECT TIPO = '', DOC='SEM RESULTADOS', Informacao = 'SEM RESULTADOS',stamporidest = ''
		Endtext
		IF !uf_gerais_actGrelha("","ucrsOriDest",lcSQL)	
			uf_perguntalt_chama("OCORREU UMA ANOMALIA NA VERIFICA플O DAS ORIGENS E DESTINOS DO DOCUMENTO.","OK","",16)
			RETURN .f.
		ENDIF 
	ENDIF
	
	IF TYPE("ORIGENSDESTINOS")=="U"
		DO FORM ORIGENSDESTINOS
	ELSE
		ORIGENSDESTINOS.show
	ENDIF
ENDFUNC


**
FUNCTION uf_OrigensDestinos_Navega
	PUBLIC myStampOriDest, myTipoDoc 

	IF EMPTY(myStampOriDest)
		RETURN .f.
	ENDIF
	
	SELECT ucrsOriDest
	LOCATE FOR ALLTRIM(ucrsOriDest.stamporidest) == ALLTRIM(myStampOriDest)
	IF FOUND()
		Do Case
			Case Alltrim(ucrsOriDest.tipo) == "FT"
				uf_facturacao_chama(Alltrim(ucrsOriDest.stamporidest))
				uf_ORIGENSDESTINOS_sair()
			Case Alltrim(ucrsOriDest.tipo) == "BO" OR Alltrim(ucrsOriDest.tipo) == "FO"
				uf_documentos_Chama(Alltrim(ucrsOriDest.stamporidest))
				uf_ORIGENSDESTINOS_sair()
			Case Alltrim(ucrsOriDest.tipo) == "RE"
				*navega("RE",Alltrim(ucrsOriDest.stamporidest))
				uf_regvendas_consultarRecibos(Alltrim(ucrsOriDest.stamporidest))
				uf_ORIGENSDESTINOS_sair()
			Otherwise
				****
				****
		ENDCASE
	ENDIF 

ENDFUNC


**
FUNCTION uf_OrigensDestinos_DoubleClick
	PUBLIC myStampOriDest, myTipoDoc 

	IF EMPTY(myStampOriDest)
		RETURN .f.
	ENDIF
	
	SELECT ucrsOriDest
	SCAN FOR ucrsOriDest.stamporidest = myStampOriDest
		Do Case
			Case Alltrim(ucrsOriDest.tipo) == "FT"
				uf_facturacao_chama(Alltrim(ucrsOriDest.stamporidest))
				uf_ORIGENSDESTINOS_sair()
			Case Alltrim(ucrsOriDest.tipo) == "BO" OR Alltrim(ucrsOriDest.tipo) == "FO"
				uf_documentos_Chama(Alltrim(ucrsOriDest.stamporidest))
				uf_ORIGENSDESTINOS_sair()
			Case Alltrim(ucrsOriDest.tipo) == "RE"
				*navega("RE",Alltrim(ucrsOriDest.stamporidest))
				uf_regvendas_consultarRecibos(Alltrim(ucrsOriDest.stamporidest))
				uf_ORIGENSDESTINOS_sair()
			Otherwise
				****
				****
		Endcase		
	ENDSCAN
	**

ENDFUNC

** && fecha painel
FUNCTION uf_ORIGENSDESTINOS_sair
	ORIGENSDESTINOS.hide
	ORIGENSDESTINOS.release	
ENDFUNC 

