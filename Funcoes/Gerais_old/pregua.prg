**
FUNCTION regua
	LPARAMETERS lcAccao, lcRegisto, lcCaption, lcBool, lcDescText
	
	TRY 
		IF TYPE("lcCaption") != "C"
			lcCaption = "Logitools Software"
		ENDIF 
		
		&& Controla abertura do Painel
		IF TYPE("PREGUA") == "U"
			DO FORM PREGUA
		ELSE
			*PREGUA.show
		ENDIF
		
		IF TYPE("PREGUA") != "U"
			PREGUA.caption = lcCaption
			IF !EMPTY(lcDescText)
				PREGUA.height = 75
				PREGUA.LabelDesc.Caption= lcDescText		
			ENDIF 
			
			
			IF lcBool &&lcBool poder� ser utilizado para colocar a r�gua em execu��o permanente.
				PREGUA.olecontrol1.visible = .f.
				PREGUA.image1.visible = .t.
			ELSE
				PREGUA.olecontrol1.visible = .t.
				PREGUA.image1.visible = .f.
				DO CASE 
					CASE lcAccao == 0 &&inicializa
						IF lcRegisto <= PREGUA.olecontrol1.MIN
							lcRegisto = PREGUA.olecontrol1.MIN + 1
						ENDIF
						PREGUA.olecontrol1.MAX = lcRegisto
						PREGUA.olecontrol1.VALUE = PREGUA.olecontrol1.MIN
						
					CASE lcAccao == 1 &&incrementa
						IF lcRegisto > PREGUA.olecontrol1.MAX
							lcRegisto = PREGUA.olecontrol1.MAX
						ENDIF
						PREGUA.olecontrol1.VALUE = lcRegisto
					CASE lcAccao == 2 &&fecha
						IF TYPE("PREGUA") != "U"
							pregua.release()
						ENDIF 
				ENDCASE
			ENDIF
		ENDIF
	CATCH
		** Erro desconhecido
	ENDTRY
	
ENDFUNC

