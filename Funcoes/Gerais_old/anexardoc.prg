**
FUNCTION uf_anexardoc_chama
	LPARAMETERS lcTable, lcRegStamp, lcDescr, lcKeywords, lcType
	PUBLIC  mylcTable, mylcRegStamp, mylcDescr, mylcKeywords, mylcType
	mylcTable = lcTable
	mylcRegStamp = lcRegStamp
	mylcDescr = lcDescr
	mylcKeywords = lcKeywords
	
	
	LOCAL lcSQL

	
	
	if(Empty(lcType))
		lcType=""
	ENDIF
	
	mylcType= lcType
	
	
	IF USED("uCrsAnexosReg")
		fecha("uCrsAnexosReg")
	ENDIF 
	
	
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_anexos_pesquisaAnexos '<<mylcRegStamp>>', '<<ALLTRIM(mylcTable)>>', '<<m_chinis>>'
	ENDTEXT

	
	IF !uf_gerais_actgrelha("", "uCrsAnexosReg", lcSQL)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA NA VERIFICA��O DOS ANEXOS.","OK","",16)
		RETURN .f.
	ENDIF
	

	
	&& Cursor ficheiros permitidos
	CREATE CURSOR uCrsTipoFx(ext c(10))
	LOCAL lcExtensoes, lcContinua, lcSpacePos, lcExt
	lcExtensoes = alltrim(uf_gerais_getParameter_site('ADM0000000012', 'text'))
	STORE .t. TO lcContinua
	STORE 0 TO lcSpacePos 
	STORE '' TO lcExt
	DO WHILE lcContinua = .t.
		lcSpacePos = AT(' ', ALLTRIM(lcExtensoes))
		IF lcSpacePos =0
			SELECT uCrsTipoFx
			APPEND BLANK 
			replace uCrsTipoFx.ext WITH ALLTRIM(lcExtensoes)
			lcContinua  = .f.
		ELSE 
			lcExt = ALLTRIM(SUBSTR(lcExtensoes, 1, lcSpacePos-1))
			SELECT uCrsTipoFx
			APPEND BLANK 
			replace uCrsTipoFx.ext WITH ALLTRIM(lcExt)
			lcExtensoes = SUBSTR(lcExtensoes, lcSpacePos + 1, LEN(lcExtensoes)-lcSpacePos )
		ENDIF 

	ENDDO 
	
	&& Cursor de anexos a inserir na grelha
	IF USED("uCrsAnexos")
		fecha("uCrsAnexos")
	ENDIF 
	CREATE CURSOR uCrsAnexos(sel l, regstamp c(30), tabela c(30), filename c(254), descr c(254), keyword c(254), tipe c(50))
	SELECT uCrsAnexos
	DELETE ALL 
	APPEND BLANK 
	replace uCrsAnexos.filename WITH ''
	replace uCrsAnexos.tabela WITH ALLTRIM(lcTable)
	replace uCrsAnexos.regstamp WITH ALLTRIM(lcRegStamp)
	replace uCrsAnexos.descr WITH ALLTRIM(lcDescr)
	replace uCrsAnexos.keyword WITH ALLTRIM(lcKeywords)
	replace uCrsAnexos.tipe WITH ALLTRIM(lcType)
	
	&& Controla Abertura do Painel
	IF type("ANEXARDOC")=="U"
		DO FORM ANEXARDOC
	ELSE
		ANEXARDOC.show
	ENDIF

ENDFUNC 


FUNCTION uf_ANEXARDOC_actualizaProdData
	LPARAM	lcTabela,lcStampProd
	
	LOCAL lcSQL
	IF(LOWER(ALLTRIM(lcTabela))!="st")
		RETURN .t.
	ENDIF
	

	lcSQL=''
	TEXT TO lcSQL TEXTMERGE NOSHOW		
	
		UPDATE 
			st
		SET 
			usrdata = GETDATE(),
			usrhora = convert(varchar,getdate(),108)
		WHERE
			ststamp = '<<ALLTRIM(lcStampProd)>>'
			and site_nr = <<mysite_nr>>
			
	ENDTEXT 
	
	IF !uf_gerais_actGrelha("","",lcSQL)
		uf_perguntalt_chama("Ocorreu um erro a actualizar a data de altera��o do producto.","OK","",16)
	endif
	
	
	RETURN .t.
	
ENDFUNC


**
FUNCTION uf_ANEXARDOC_anexar
	&&valida��es
	IF EMPTY(ALLTRIM(uCrsanexos.filename))
		uf_perguntalt_chama("Tem que selecionar o documento a anexar.","OK","",64)
		RETURN .f.
	ENDIF
	
	LOCAL lcCurDir, lcWhatFile, lcDestDir

	lcDestDir="'"+alltrim(uf_gerais_getParameter_site('ADM0000000010', 'text'))+"\"+ALLTRIM(uCrsAnexos.tabela)+"'"
	lcWhatFile =  "'" + ALLTRIM(uCrsAnexos.filename) + "'"
	    
	copy file &lcWhatFile to &lcDestDir
	
	LOCAL lcSQL, lcStamp, lcSqlTotal
	lcLenPath=RAT("\", uCrsAnexos.filename)
	lcLenPatht=len(uCrsAnexos.filename)
	replace uCrsAnexos.filename WITH substr(uCrsAnexos.filename,lcLenPath+1, lcLenPatht-lcLenPath)
	
	

	

	lcStamp = uf_gerais_stamp()
	lcSQL=''
	TEXT TO lcSQL TEXTMERGE NOSHOW
		INSERT INTO anexos (
			anexosstamp
			,regstamp
			,tabela
			,filename
			,descr
			,keyword
			,protected
			,ousrdata
			,ousrinis
			,usrdata
			,usrinis
			,tipo 
		)
		values(
			'<<lcStamp>>'
			,'<<ALLTRIM(uCrsAnexos.regstamp)>>'
			,'<<ALLTRIM(uCrsAnexos.tabela)>>'
			,'<<ALLTRIM(uCrsAnexos.filename)>>'
			,'<<ALLTRIM(uCrsAnexos.descr)>>'
			,'<<ALLTRIM(uCrsAnexos.keyword)>>'
			,0
			,getdate()
			,'<<m_chinis>>'
			,getdate()
			,'<<m_chinis>>'
			,'<<ALLTRIM(uCrsAnexos.tipe)>>'
		)
	ENDTEXT 

	IF !uf_gerais_actGrelha("","",lcSQL)
		uf_perguntalt_chama("Ocorreu um erro a associar o anexo. Por favor contacte o suporte.","OK","",16)
	ELSE
		
		LOCAL lcTabela, lcStampProd, lcFileName, lcDescr
		STORE '' TO  lcStampProd, lcTabela, lcFileName, lcDescr
	
		SELECT uCrsAnexos
		lcTabela    = uCrsAnexos.tabela
		lcStampProd = uCrsAnexos.regstamp
		lcFileName  = ALLTRIM(uCrsAnexos.filename)
		lcDescr     = ALLTRIM(uCrsAnexos.descr)
	
		uf_ANEXARDOC_actualizaProdData(lcTabela,lcStampProd)

		SELECT uCrsAnexosReg
		APPEND BLANK 
		replace uCrsAnexosReg.nomeficheiro WITH alltrim(lcFileName)
		replace uCrsAnexosReg.descricao WITH ALLTRIM(lcDescr)
		replace uCrsAnexosReg.descr WITH ALLTRIM(lcDescr)
		replace uCrsAnexosReg.anexosstamp WITH ALLTRIM(lcStamp)
		replace uCrsAnexosReg.dateins WITH ALLTRIM(STR(YEAR(DATE())))+'.'+RIGHT('0'+ALLTRIM(STR(MONTH(DATE()))),2)+'.'+RIGHT('0'+ALLTRIM(STR(DAY(DATE()))),2)
		replace uCrsAnexosReg.protected WITH .f.
		replace uCrsAnexosReg.ousrinis WITH ALLTRIM(m_chinis)
		replace uCrsAnexosReg.tabela WITH ALLTRIM(lcTabela)
		replace uCrsAnexosReg.regStamp WITH ALLTRIM(lcStampProd )
		uf_perguntalt_chama("Documento anexado com sucesso.","OK","",64)
		uf_anexardoc_refresh(ALLTRIM(mylcTable), ALLTRIM(mylcRegStamp ), ALLTRIM(mylcDescr), ALLTRIM(mylcKeywords))
	ENDIF 	
	
ENDFUNC

FUNCTION uf_ANEXARDOC_carregaCursorOutrosPaineis
	
	
	** painel productos
	if(USED("st"))
		if(vartype(stocks)!='U')
			if(vartype(stocks.pageframe1)!='U')
				uf_stocks_criaCursorImagens()
				&& primeira imagem
				uf_stocks_trocaImg(0)	
			endif	
		endif
	endif
	
endfunc


**
FUNCTION uf_ANEXARDOC_sair
	
	&& fecha cursores
	Fecha("uCrsAnexos")
	Fecha("uCrsAnexosReg")
	Fecha("uCrsTipoFx")
	
	uf_ANEXARDOC_carregaCursorOutrosPaineis()
	
	RELEASE mylcType
					
	ANEXARDOC.hide
	ANEXARDOC.Release

	RELEASE ANEXARDOC
ENDFUNC

FUNCTION uf_anexardoc_selfile
	

	LOCAL lcCurDir, lcWhatFile, lcDestDirL, lcLenPath, lcLenPathT, lcFileSize, lcFileSizeKB
	public lcfineselname
	lcfineselname=''
	STORE '' TO lcWhatFile
	STORE 0 TO lcLenPath, lcLenPatht, lcFileSize, lcFileSizeKB
	
	** Verificar se pode anexar mais ficheiros - ferificar n� de ficheiros e tamanho
	public oMyFiler
	oMyFiler = CREATEOBJECT('Filer.FileUtil')
	oMyFiler.SearchPath = alltrim(uf_gerais_getParameter_site('ADM0000000010', 'text')) && Search Directory
	oMyFiler.isHostedbyFox=0
	oMyFiler.SubFolder = 1 && includes sub folders
	oMyFiler.Find(1) && cumulate sub-folders
	LOCAL nSize, ncount
	nSize = 0
	ncount=0
	FOR nFileCount = 1 TO oMyFiler.Files.Count
	   	if omyfiler.FILES.ITEM(nfilecount).NAME !="." and omyfiler.FILES.ITEM(nfilecount).NAME  != ".." and len(omyfiler.FILES.ITEM(nfilecount).NAME)>10
		   	nSize=nSize+oMyFiler.Files.Item(nFileCount).Size && add the size	
			ncount=ncount+1
		endif
	ENDFOR
	** sem adquirir o m�dulo s� podem anexar x ficheiros (definido no par�metro da empresa ADM0000000015)
	IF uf_gerais_getParameter_site('ADM0000000013', 'BOOL') == .f. AND ncount+1>uf_gerais_getParameter_site('ADM0000000015', 'NUM')
		uf_perguntalt_chama("EXCEDEU O N� DE FICHEIROS QUE PODE ANEXAR SEM ADQUIRIR O M�DULO!" + CHR(13) + "SE DESEJAR ANEXAR MAIS POR FAVOR CONTACTE O DEPT. COMERCIAL","OK","",64)
		RETURN .F.
	ENDIF 
	IF (nSize/1000000)>uf_gerais_getParameter_site('ADM0000000014', 'NUM')
		uf_perguntalt_chama("EXCEDEU O LIMITE DE ARMAZENAMENTO PERMITIDO!" + CHR(13) + "PARA AUMENTAR O MESMO POR FAVOR CONTACTE O DEPT. COMERCIAL","OK","",64)
		RETURN .F.
	ENDIF 

	lcDestDir= myGDFolder
	
	** Verifica se est� pasta criado no pc local do utilizador para partilha de ficheiros
	** \\tsclient\C\Users\Public\Desktop\AnexosLogitools
**	IF directory(myDefFolder) = .f.
**		uf_perguntalt_chama("A PASTA 'AnexosLogitools' N�O EST� CRIADA NO AMBIENTE DE TRABALHO P�BLICO DO SEU PC!" + CHR(13) + "POR FAVOR VERIFIQUE.","OK","",64)		
**		RETURN .f.		
**	ENDIF 

    **uv_dirAnexos = uf_gerais_getUmValor("B_Terminal","pathAnexos","terminal = '" + alltrim(myTerm) + "' and site = '" + alltrim(mySite) + "'")
    uv_dirAnexos = uf_gerais_getUmValor("B_Terminal","pathAnexos","terminal = '" + alltrim(myTerm) + "'")


    IF EMPTY(uv_dirAnexos)

        uf_perguntalt_chama("N�o configurou a pasta de anexos para este Terminal." + CHR(13) + "Por favor verifique.","OK","",64)		
        RETURN .f.		

    ENDIF

    IF !uf_gerais_createDir(uv_dirAnexos)
        RETURN .F.
    ENDIF
    
    uv_nomePasta = uf_gerais_getFromString(uv_dirAnexos, '\', 25, .T.)
	
	CD(uv_dirAnexos)
	
	ADIR(aFiles ,"*.*")
	IF TYPE("aFiles")=='U'
		uf_perguntalt_chama("N�O EXISTEM FICHEIROS NA PASTA '" + ALLTRIM(uv_nomePasta) + "' PARA SELECIONAR!" + CHR(13) + "POR FAVOR VERIFIQUE.","OK","",64)		
		RETURN .f.
	ENDIF
	
	CREATE CURSOR curFiles (FileName C(200), FileSize I, FileDate D, FileTime C(8), FileAttr C(5))
	INSERT INTO curFiles FROM ARRAY aFiles 
	uf_valorescombo_chama("lcfineselname", 0, "curFiles", 2, "FileName", "FileName")
	lcWhatFile = ALLTRIM(uv_dirAnexos) + "\"+ALLTRIM(lcfineselname)
	fecha("curFiles")	


	**lcWhatFile = GETFILE("All Files (*.*):*.*", "Anexar Ficheiro:", "Selecionar", 0, "Escolha o ficheiro a anexar")
	IF lcWhatFile<>'' OR !EMPTY(lcWhatFile)

		LOCAL lcExtFx, lcValidaExt
		STORE '' TO lcExtFx
		STORE .f. TO lcValidaExt
		
		lcExtFx = SUBSTR(ALLTRIM(lcWhatFile), RAT(".", ALLTRIM(lcWhatFile))+1, LEN(lcWhatFile))
		myPrevFolder = SUBSTR(ALLTRIM(lcWhatFile), 1, RAT("\", ALLTRIM(lcWhatFile))-1)
		
		SELECT uCrsTipoFx
		GO TOP 
		SCAN 
			IF ALLTRIM(lcExtFx) == ALLTRIM(uCrsTipoFx.ext) AND lcValidaExt=.f.
				lcValidaExt = .t.
			ENDIF 
		ENDSCAN 
		IF lcValidaExt = .f.
			uf_perguntalt_chama("O FICHEIRO SELECIONADO N�O � PERMITIDO." + CHR(13)+ "AS EXTENS�ES PERMITIDAS S�O AS SEGUINTES: " + alltrim(uf_gerais_getParameter_site('ADM0000000012', 'text')) + " ","OK","",64)
			RETURN .f. 
		ENDIF 

		if ADIR(arArrayName, lcWhatFile) = 1
    		lcFileSize = (arArrayName(2)/1000000)
    		lcFileSizeKb = (arArrayName(2)/1000)
		ENDIF
		

		
		if(mylcType=="imagemProd")
			IF lcFileSizeKb > uf_gerais_getParameter_site('ADM0000000110', 'num')
				uf_perguntalt_chama("A IMAGEM DO PRODUCTO EXCEDEU O TAMANHO M�XIMO AUTORIZADO DE "+ alltrim(str(uf_gerais_getParameter_site('ADM0000000110', 'num'))) +" KB!","OK","",64)
				RETURN .f.
			ENDIF		
		endif
		
		IF lcFileSize > uf_gerais_getParameter_site('ADM0000000011', 'num')
			uf_perguntalt_chama("O FICHEIRO EXCEDEU O TAMANHO M�XIMO AUTORIZADO DE "+ alltrim(str(uf_gerais_getParameter_site('ADM0000000011', 'num'))) +" MB!","OK","",64)
			RETURN .f.
		ELSE 
			SELECT uCrsAnexos
			replace uCrsAnexos.filename WITH ALLTRIM(lcWhatFile)
			**myDefFolder = ALLTRIM(substr(lcWhatFile,1, lcLenPath-1))
			anexardoc.txtfilename.refresh
		ENDIF 
	ELSE
		uf_perguntalt_chama("N�O FOI SELECIONADO NENHUM FICHEIRO!","OK","",64)
	ENDIF 
	
	CD(uv_dirAnexos)
	
ENDFUNC 


FUNCTION uf_anexardoc_refresh
	LPARAMETERS lcTable, lcRegStamp, lcDescr, lcKeywords
	LOCAL  mylcTable, mylcRegStamp, mylcDescr, mylcKeywords
	mylcTable = lcTable
	mylcRegStamp = lcRegStamp
	mylcDescr = lcDescr
	mylcKeywords = lcKeywords
	
	SELECT uCrsAnexos
	replace uCrsAnexos.filename WITH ''
	replace uCrsAnexos.tabela WITH ALLTRIM(mylcTable)
	replace uCrsAnexos.regstamp WITH ALLTRIM(mylcRegStamp)
	replace uCrsAnexos.descr WITH ALLTRIM(mylcDescr)
	replace uCrsAnexos.keyword WITH ALLTRIM(mylcKeywords)
	replace uCrsAnexos.tipe  WITH ALLTRIM(mylcType)
	
	ANEXARDOC.GridPesq.refresh
	ANEXARDOC.refresh
ENDFUNC 


FUNCTION uf_anexardoc_unsel
	
	SELECT uCrsAnexosReg
	GO TOP 
	SCAN 
		replace uCrsAnexosReg.sel WITH .f. 
	ENDSCAN 
	
	SELECT uCrsAnexosReg
	GO TOP 
	
	ANEXARDOC.GridPesq.refresh
	ANEXARDOC.refresh
ENDFUNC 


FUNCTION uf_anexardoc_valsel
	
	LOCAL lcValSel
	STORE .f. TO lcValSel
	SELECT uCrsAnexosReg
	GO TOP 
	SCAN 
		if uCrsAnexosReg.sel == .t. 
			lcValSel = .t.
		ENDIF 
	ENDSCAN
	SELECT uCrsAnexosReg
	GO TOP 
	RETURN lcValSel 
		
ENDFUNC 


FUNCTION uf_ANEXARDOC_proteger
	IF !uf_anexardoc_valsel()
		uf_perguntalt_chama("N�O SELECIONOU NENHUM REGISTO!","OK","",64)
	ELSE
		SELECT uCrsAnexosReg
		GO TOP 
		SCAN 
			IF uCrsAnexosReg.sel=.t.
				IF ALLTRIM(uCrsAnexosReg.ousrinis) = ALLTRIM(m_chinis)  
					IF uCrsAnexosReg.protected=.t.
						uf_perguntalt_chama("O FICHEIRO SELECIONADO J� SE ENCONTRA PROTEGIDO!","OK","",64)
					ELSE
						REPLACE uCrsAnexosReg.protected WITH .T.
						TEXT TO lcSQL TEXTMERGE NOSHOW
							UPDATE anexos SET protected = 1 WHERE anexosstamp = '<<ALLTRIM(uCrsAnexosReg.anexosstamp)>>'
						ENDTEXT 
					
						IF !uf_gerais_actGrelha("","",lcSQL)
							uf_perguntalt_chama("Ocorreu um erro ao proteger o documento. Por favor contacte o suporte.","OK","",16)
						ELSE
							uf_perguntalt_chama("O DOCUMENTO " + ALLTRIM(uCrsAnexosReg.nomeficheiro) + " FOI PROTEGIDO COM SUCESSO.","OK","",64)
						ENDIF 
					ENDIF 
				ELSE
					uf_perguntalt_chama("O FICHEIRO " + ALLTRIM(uCrsAnexosReg.nomeficheiro) + " S� PODE SER PROTEGIDO PELO UTILIZADOR QUE O INSERIU!","OK","",64)
				ENDIF 
			ENDIF 
		ENDSCAN 
		
		uf_anexardoc_unsel()
		
		SELECT uCrsAnexosReg
		GO TOP
		ANEXARDOC.GridPesq.refresh
		ANEXARDOC.refresh
	ENDIF 
ENDFUNC 


FUNCTION uf_ANEXARDOC_desproteger
	IF !uf_anexardoc_valsel()
		uf_perguntalt_chama("N�O SELECIONOU NENHUM REGISTO!","OK","",64)
	ELSE
		SELECT uCrsAnexosReg
		GO TOP 
		SCAN 
			IF uCrsAnexosReg.sel=.t.
				IF ALLTRIM(uCrsAnexosReg.ousrinis) = ALLTRIM(m_chinis)  
					IF uCrsAnexosReg.protected=.f.
						uf_perguntalt_chama("O FICHEIRO SELECIONADO J� SE ENCONTRA DESPROTEGIDO!","OK","",64)
					ELSE
						REPLACE uCrsAnexosReg.protected WITH .F.
						TEXT TO lcSQL TEXTMERGE NOSHOW
							UPDATE anexos SET protected = 0 WHERE anexosstamp = '<<ALLTRIM(uCrsAnexosReg.anexosstamp)>>'
						ENDTEXT 
					
						IF !uf_gerais_actGrelha("","",lcSQL)
							uf_perguntalt_chama("Ocorreu um erro ao proteger o documento. Por favor contacte o suporte.","OK","",16)
						ELSE
							uf_perguntalt_chama("O DOCUMENTO " + ALLTRIM(uCrsAnexosReg.nomeficheiro) + " FOI DESPROTEGIDO COM SUCESSO.","OK","",64)
						ENDIF 
					ENDIF 
				ELSE
					uf_perguntalt_chama("O FICHEIRO " + ALLTRIM(uCrsAnexosReg.nomeficheiro) + " S� PODE SER DESPROTEGIDO PELO UTILIZADOR QUE O INSERIU!","OK","",64)
				ENDIF 
			ENDIF 
		ENDSCAN 
		
		uf_anexardoc_unsel()
		
		SELECT uCrsAnexosReg
		GO TOP
		ANEXARDOC.GridPesq.refresh
		ANEXARDOC.refresh
	ENDIF 
ENDFUNC 


FUNCTION uf_ANEXARDOC_Visualizar
	
	IF !uf_anexardoc_valsel()
		uf_perguntalt_chama("N�O SELECIONOU NENHUM REGISTO!","OK","",64)
	ELSE
		&&TODO
		&&validar se tem path
		LOCAL lcFile
		STORE "" TO lcFile
		
		SELECT uCrsAnexosReg
		GO TOP
		SCAN
			IF uCrsAnexosReg.sel=.t.
			
				lcFile = uf_anexardoc_retornaCaminhoFicheiro()
				if(!FILE(lcFile))
					uf_perguntalt_chama("O FICHEIRO " + ALLTRIM(uCrsAnexosReg.nomeficheiro) + " N�O EXISTE.","OK","",64)
					RETURN .f. 
				ENDIF
					
				
				IF uCrsAnexosReg.protected=.f. 
					DECLARE INTEGER ShellExecute IN shell32.dll ; 
				  	INTEGER hndWin, ; 
				  	STRING cAction, ; 
				  	STRING cFileName, ; 
				  	STRING cParams, ;  
				  	STRING cDir, ; 
					INTEGER nShowWin
					ShellExecute(0,"open",lcFile ,"","",1)
				ELSE
					IF ALLTRIM(uCrsAnexosReg.ousrinis) == ALLTRIM(m_chinis)
						DECLARE INTEGER ShellExecute IN shell32.dll ; 
					  	INTEGER hndWin, ; 
					  	STRING cAction, ; 
					  	STRING cFileName, ; 
					  	STRING cParams, ;  
					  	STRING cDir, ; 
						INTEGER nShowWin

						ShellExecute(0,"open",lcFile ,"","",1)
					ELSE 
						uf_perguntalt_chama("O FICHEIRO EST� PROTEGIDO E POR ESSE MOTIVO S� PODE SER CONSULTADO PELO UTILIZADOR QUE O INSERIU!","OK","",64)
					ENDIF 
				ENDIF 
			ENDIF 
		ENDSCAN 
		
		SELECT uCrsAnexosReg
		GO TOP
		ANEXARDOC.GridPesq.refresh
		ANEXARDOC.refresh
	ENDIF 
ENDFUNC 

FUNCTION uf_ANEXARDOC_email

	IF !uf_anexardoc_valsel()
		uf_perguntalt_chama("N�O SELECIONOU NENHUM REGISTO!","OK","",64)
	ELSE

		LOCAL lccount
		lccount=0
		IF ucrse1.usacoms=.t. 
			SELECT uCrsAnexosReg
			GO TOP
			SCAN
				IF uCrsAnexosReg.sel=.t.
					lccount=lccount + 1
				ENDIF 
			ENDSCAN
			
			
		
			IF lccount=1 then
				SELECT uCrsAnexosReg
				GO TOP
				SCAN
					IF uCrsAnexosReg.sel=.t.
						LOCAL mDir, lcTo, lcSubject, lcBody, lcFile
						STORE "" TO lcFile
						PUBLIC lcToEmail
						
					
						lcFile= uf_anexardoc_retornaCaminhoFicheiro()
						if(!FILE(lcFile))
							uf_perguntalt_chama("O FICHEIRO " + ALLTRIM(uCrsAnexosReg.nomeficheiro) + " N�O EXISTE.","OK","",64)
							regua(2)
							RETURN .f. 
						ENDIF
						
						lcTo = ""
						lcToEmail = ""
						uf_tecladoalpha_chama("lcToEmail", "Introduza o email para o envio", .f., .f., 0)
						lcSubject = "Envio de ficheiro"
						lcBody = "Envio de anexo do software Logitools " + CHR(13) + CHR(13) + ALLTRIM(ucrse1.mailsign)
						
						regua(0,6,"A enviar email ...")
						uf_startup_sendmail_emp(lcTo, lcToEmail, lcSubject, lcBody , lcFile, .t.)
						regua(2)
					ENDIF 
				ENDSCAN 
			ELSE
				uf_perguntalt_chama("S� PODE SER EFETUADO O ENVIO DE 1 DOCUMENTO DE CADA VEZ!","OK","",64)
			ENDIF 		
			
		ELSE
			uf_perguntalt_chama("PARA UTILIZAR ESTA FUNCIONALIDADE DEVER� ADQUIRIR O M�DULO DE COMUNICA��ES." + CHR(13) + "PARA MAIS INFORMA��ES POR FAVOR CONTACTE A LOGITOOLS (OP��O - COMERCIAL).", "OK","",16)
		ENDIF
		
		uf_anexardoc_unsel()
		
		SELECT uCrsAnexosReg
		GO TOP
		ANEXARDOC.GridPesq.refresh
		ANEXARDOC.refresh
	ENDIF 
ENDFUNC


FUNCTION uf_ANEXARDOC_remove
	LOCAL lcAtatchment, lcDestDir, lcFileName 
	
	IF !uf_anexardoc_valsel()
		uf_perguntalt_chama("N�O SELECIONOU NENHUM REGISTO!","OK","",64)
	ELSE

		IF ucrse1.usacoms=.t. 
			SELECT uCrsAnexosReg
			GO TOP
			SCAN
				IF uCrsAnexosReg.sel=.t.
					IF ALLTRIM(uCrsAnexosReg.ousrinis) == ALLTRIM(m_chinis)
						If uf_perguntalt_chama("TEM A CERTEZA QUE QUER REMOVER O ANEXO " + ALLTRIM(uCrsAnexosReg.nomeficheiro) + " ?","Sim","N�o")
						
						
							&&TODO
							&&validar se tem path

							lcDestDir=""+alltrim(uf_gerais_getParameter_site('ADM0000000010', 'text'))+"\"+ALLTRIM(uCrsAnexos.tabela)+"\"
							lcAtatchment = lcDestDir + ALLTRIM(uCrsAnexosReg.nomeficheiro)
							
							
							lcSQL = ''
							TEXT TO lcSQL TEXTMERGE NOSHOW
								DELETE FROM anexos  WHERE anexosstamp = '<<ALLTRIM(uCrsAnexosReg.anexosstamp)>>'
							ENDTEXT 
							
						
							IF !uf_gerais_actGrelha("","",lcSQL)
								uf_perguntalt_chama("Ocorreu um erro ao remover o anexo. Por favor contacte o suporte.","OK","",16)
							ELSE

								DECLARE INTEGER DeleteFile IN kernel32 STRING lpFileName
								lnRetVal = DeleteFile(lcAtatchment + CHR(0))
								IF lnRetVal = 0
								    uf_perguntalt_chama("Ocorreu um erro ao eliminar o anexo. Por favor contacte o suporte.","OK","",16)
								ENDIF

								
								LOCAL lcTabela, lcStampProd 
								STORE '' TO  lcStampProd, lcTabela
							
								SELECT uCrsAnexos
								lcTabela    = uCrsAnexosReg.tabela
								lcStampProd = uCrsAnexosReg.regstamp
							
								uf_ANEXARDOC_actualizaProdData(lcTabela,lcStampProd)
														

								
								SELECT uCrsAnexosReg
								DELETE
								
								uf_perguntalt_chama("DOCUMENTO REMOVIDO COM SUCESSO.","OK","",64)
								
							ENDIF 
						ELSE
							uf_perguntalt_chama("OPERA��O CANCELADA!","OK","",16)
						ENDIF 
					ELSE
						uf_perguntalt_chama("O FICHEIRO S� PODE SER REMOVIDO PELO UTILIZADOR QUE O INSERIU!","OK","",64)
					ENDIF 
				ENDIF 
			ENDSCAN 
			
			SELECT uCrsAnexosReg
			GO TOP
			ANEXARDOC.GridPesq.refresh
			ANEXARDOC.refresh
		ENDIF 
	ENDIF 
ENDFUNC 


FUNCTION uf_ANEXARDOC_download

	LOCAL lcCurDir, lcDestDir, lcDestFolder, lcFile , lcDestfile 
	STORE '' TO lcCurDir, lcDestDir, lcDestFolder, lcFile , lcDestfile 
	
	IF !uf_anexardoc_valsel()
		uf_perguntalt_chama("N�O SELECIONOU NENHUM REGISTO!","OK","",64)
	ELSE

        **uv_dirAnexos = uf_gerais_getUmValor("B_Terminal","pathAnexos","terminal = '" + alltrim(myTerm) + "' and site = '" + alltrim(mySite) + "'")
        uv_dirAnexos = uf_gerais_getUmValor("B_Terminal","pathAnexos","terminal = '" + alltrim(myTerm) + "'")

**		IF !DIRECTORY(myDefFolder)
**			uf_perguntalt_chama("A PASTA 'AnexosLogitools' N�O EST� CRIADA NO AMBIENTE DE TRABALHO P�BLICO DO SEU PC!" + CHR(13) + "POR FAVOR VERIFIQUE.","OK","",64)		
**			RETURN .f.		
**		ENDIF 

        IF EMPTY(uv_dirAnexos)

			uf_perguntalt_chama("N�o configurou a pasta de anexos para este Terminal." + CHR(13) + "Por favor verifique.","OK","",64)		
			RETURN .f.		

        ENDIF

        IF !uf_gerais_createDir(uv_dirAnexos)
            RETURN .F.
        ENDIF
		
		lcCurDir = FULLPATH(CurDir())
		lcDestDir= myGDFolder
		
		**IF EMPTY(myPrevFolder)	
			CD(uv_dirAnexos)
		**ELSE
		**	CD(myPrevFolder)
		**ENDIF 
		
		**lcDestFolder=GETDIR(myDefFolder, "Selecione a diretoria de destino:", "Pasta 'AnexosLogitools' do Ambiente de trabalho", "", .t.)
		lcDestFolder=uv_dirAnexos
		
		IF !EMPTY(lcDestFolder)
			LOCAL lcFile, lcDestDir, lcDestfile, lcCont, lcCont2
			calculate count() for uCrsAnexosReg.sel==.t. to lcCont
			lcCont2 = 1
			regua(0,lcCont,"A transferir ficheiro(s) ...")
			SELECT uCrsAnexosReg
			GO TOP
			SCAN
				IF uCrsAnexosReg.sel=.t.
					regua(1,lcCont2,"A transferir ficheiro(s) ...")
				
					lcFile = uf_anexardoc_retornaCaminhoFicheiro()
					if(!FILE(lcFile))
						uf_perguntalt_chama("O FICHEIRO " + ALLTRIM(uCrsAnexosReg.nomeficheiro) + " N�O EXISTE.","OK","",64)
						regua(2)
						RETURN .f. 
					ENDIF
					

					lcDestfile = alltrim(lcDestFolder)+ "\" + ALLTRIM(uCrsAnexosReg.nomeficheiro)
			

					IF uCrsAnexosReg.protected=.t.
						IF ALLTRIM(uCrsAnexosReg.ousrinis) != ALLTRIM(m_chinis)
							uf_perguntalt_chama("O FICHEIRO " + ALLTRIM(uCrsAnexosReg.nomeficheiro) + " EST� PROTEGIDO E POR ESSE MOTIVO S� PODE SER TRANSFERIDO PELO UTILIZADOR QUE O INSERIU!","OK","",64)
							regua(2)
							RETURN .f. 
						ENDIF 
					ENDIF
					
					
					COPY FILE (lcFile)  TO (lcDestfile) 							
					uf_perguntalt_chama("TRANSFER�NCIA DO FICHEIRO " + ALLTRIM(uCrsAnexosReg.nomeficheiro) + " CONCLU�DA PARA A PASTA DE ANEXOS!","OK","",64)
					
					lcCont2 =lcCont2 + 1
				ENDIF 
			ENDSCAN 
			regua(2)
			
			uf_anexardoc_unsel()

		ELSE
			uf_perguntalt_chama("N�O FOI SELECIONADA NENHUMA DIRETORIA!","OK","",64)
		ENDIF 
		
		CD(myDefDir)
		
		uf_gerais_open_remote_dir()
	ENDIF 
ENDFUNC 


FUNCTION uf_anexardoc_retornaCaminhoFicheiro
	
	LOCAL lcDestDir , lcFilePath 
	STORE "" TO lcDestDir, lcFilePath 
	
	if(USED("uCrsAnexosReg"))
		SELECT uCrsAnexosReg
		if(EMPTY(ALLTRIM(uCrsAnexosReg.path)))
			lcDestDir=""+alltrim(uf_gerais_getParameter_site('ADM0000000010', 'text'))+"\"+ALLTRIM(uCrsAnexosReg.tabela)+"\"
			lcFilePath = lcDestDir + ALLTRIM(uCrsAnexosReg.nomeficheiro)
		ELSE
			lcFilePath =  ALLTRIM(uCrsAnexosReg.path)
		ENDIF
				
	ENDIF
	
	RETURN  lcFilePath 
	
ENDFUNC
