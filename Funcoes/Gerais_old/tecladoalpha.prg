
**
FUNCTION uf_tecladoalpha_Chama
	LPARAMETERS lcObjecto, lcTexto, lcPassChar, lcFecha, lcTipo, lcUsaValorOrig, lcValorPrePreenchido

	IF TYPE("TECLADOALPHA") == "U"
		IF USED("ucrsVirtualText")
			fecha("ucrsVirtualText")
		ENDIF
		
		CREATE CURSOR ucrsVirtualText(Objecto c(200), Texto c(200), PassChar l, fecha l, Tipo n(2,0), usaValor l)
		Select ucrsVirtualText
		APPEND BLANK
		replace ucrsVirtualText.Objecto 		WITH lcObjecto
		replace ucrsVirtualText.Texto  			WITH lcTexto
		replace ucrsVirtualText.PassChar 		WITH lcPassChar
		replace ucrsVirtualText.fecha	 		WITH lcFecha
		replace ucrsVirtualText.Tipo 			WITH lcTipo
		replace uCrsVirtualText.usaValor		WITH lcUsaValorOrig

		DO FORM TECLADOALPHA WITH lcValorPrePreenchido
		IF !(TYPE('TECLADOALPHA') == "U")
			IF uCrsVirtualText.fecha == .t.	
				TECLADOALPHA.menu1.estado("", "", "GRAVAR", .t., "SAIR", .t.)
			ELSE
				TECLADOALPHA.menu1.estado("", "", "GRAVAR", .t., "SAIR", .f.)
			ENDIF 

			TECLADOALPHA.refresh
		ENDIF 
	ELSE	
		TECLADOALPHA.show
		
		TECLADOALPHA.menu1.estado("", "", "GRAVAR", .t., "SAIR", .t.)
		TECLADOALPHA.refresh
		
	ENDIF
ENDFUNC


**
FUNCTION uf_tecladoVirtual_CarregaMenu

	SELECT ucrsVirtualText
	IF uCrsVirtualText.fecha == .t.
		IF TYPE('ATENDIMENTO') == "U"
			TECLADOALPHA.menu1.adicionaOpcao("fechar", "Fechar", myPath + "\imagens\icons\sair2_32.png", "uf_painelcentral_quit","F")
			
		ENDIF
	ENDIF
*!*		IF uCrsVirtualText.fecha == .t.	
*!*			TECLADOALPHA.menu1.estado("", "", "GRAVAR", .t., "SAIR", .t.)
*!*		ELSE
*!*			TECLADOALPHA.menu1.estado("", "", "GRAVAR", .t., "SAIR", .f.)
*!*		ENDIF 
*!*		TECLADOALPHA.refresh


ENDFUNC


**
FUNCTION uf_tecladoalpha_gravar
	LOCAL lcObj, lcCursor
	
	TECLADOALPHA.valor.Value = strtran(TECLADOALPHA.valor.Value, chr(39), '')
	TECLADOALPHA.valor.Value = strtran(TECLADOALPHA.valor.Value, chr(0160), ' ')
	
	SELECT uCrsVirtualText
	lcObj = uCrsVirtualText.Objecto
	
	DO CASE
		CASE uCrsVirtualText.Tipo == 0 &&Variavel
	 		&lcObj =  Alltrim(TECLADOALPHA.valor.value)
	 		
		CASE uCrsVirtualText.Tipo == 1 && objecto
			&lcObj..value = ALLTRIM(TECLADOALPHA.valor.value)
			
		CASE uCrsVirtualText.Tipo == 2 && cursor
			lcCursor = STREXTRACT(lcObj,"",".")
			SELECT &lcCursor 
			Replace &lcObj With ALLTRIM(TECLADOALPHA.valor.value)
		CASE uCrsVirtualText.Tipo == 3 && label
			&lcObj..caption = ALLTRIM(TECLADOALPHA.valor.value)
			
		OTHERWISE
			**
	ENDCASE
		
	uf_tecladoalpha_sair()
ENDFUNC


**
FUNCTION uf_tecladoalpha_sair
	&& fecha painel
	IF !(TYPE("TECLADOALPHA") == "U")
		TECLADOALPHA.hide
		TECLADOALPHA.release
	ENDIF
ENDFUNC 