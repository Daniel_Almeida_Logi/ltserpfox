DO pesqutilizadores	
DO especialidades
DO gruposut
DO disponibilidadesrecursos

**
FUNCTION uf_utilizadores_chama
	LPARAMETERS lcNO
	
*!*		IF !uf_gerais_validaPermUser(ch_userno,ch_grupo,'Gest�o de Utilizadores')
*!*			uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ACEDER AO PAINEL GEST�O DE UTILIZADORES.","OK","",48)
*!*			RETURN .f.
*!*		ENDIF
	
	LOCAL lcSql
	
	IF EMPTY(lcNo)
		lcNo = ch_userno
	ENDIF
	IF USED("uCrsUs")
		fecha("uCrsUs")
	ENDIF
	
	IF USED("ucrsUsAux")
		fecha("ucrsUsAux")
	ENDIF
	
	&& Cursor dos Utilizadores
	lcSql = ''
	TEXT TO lcSql NOSHOW TEXTMERGE
		Select Top 1 * From b_us (nolock) Where userno = <<lcNo>>
	ENDTEXT
	uf_gerais_actgrelha("", "ucrsUs", lcSql)
	
	&& copy cursor for future matches
	SELECT * FROM ucrsUs INTO CURSOR ucrsUsAux READWRITE
	
	
	&& Cursor de Especialidades					
	IF !(uf_gerais_actGrelha(IIF(TYPE("UTILIZADORES")=="U","","utilizadores.pageframe1.page2.grdEsp"),"uCrsDREsp",[SELECT espno,especialidade FROM b_usesp (NOLOCK) where userno=]+ASTR(lcNo)+[ order by espno]))
		uf_perguntalt_chama("OCORREU UM ERRO A CARREGAR OS DADOS DE ESPECIALIDADES DO ESPECIALISTA!","OK","",16)
		RETURN .f.
	ENDIF
		
	&& Cursor de Especialidades		
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE
		;WITH CTE1 AS (
			SELECT especialidade AS div FROM b_especialidades (nolock)
		)
		SELECT * FROM CTE1 GROUP BY DIV ORDER BY DIV
	ENDTEXT
	
	IF !uf_gerais_actGrelha("","UcrsEspecialidadesEsp",lcSQL)
		uf_perguntalt_chama("OCORREU UM ERRO A CARREGAR OS DADOS DAS ESPECIALIDADES DO ESPECIALISTA!","OK","",16)
	ENDIF
	
	&& Cursor Dias
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE
		Select 	'' as dia
		Union all
		Select UPPER('Segunda-Feira')
		union all
		Select UPPER('Ter�a-Feira')
		union all
		Select UPPER('Quarta-Feira')
		union all
		Select UPPER('Quinta-Feira')
		union all
		Select UPPER('Sexta-Feira')
		union all
		Select UPPER('S�bado')
		union all
		Select UPPER('Domingo')
	ENDTEXT
	IF !uf_gerais_actGrelha("","UcrsDiasSemanaPeriodos",lcSQL)
		uf_perguntalt_chama("OCORREU UM ERRO A VALIDAR DADOS DOS EPECIALISTAS.","OK","",16)
	ENDIF
	
	IF TYPE("UTILIZADORES") == "U"
	
		&& Cursor de CONVENCOES		
		lcSQL = ""
		TEXT TO lcSQL NOSHOW TEXTMERGE
			exec up_utilizadores_Convencoes <<lcNo>>
		ENDTEXT
		
		IF !uf_gerais_actGrelha("","uCrsUsConvencoes",lcSQL)
			uf_perguntalt_chama("OCORREU UM ERRO A CARREGAR OS DADOS DAS CONVEN��ES DO ESPECIALISTA!","OK","",16)
		ENDIF
		
		&& Cursor de TP		
		lcSQL = ""
		TEXT TO lcSQL NOSHOW TEXTMERGE
			exec up_utilizadores_tabelaPrecos <<lcNo>>
		ENDTEXT
		
		IF !uf_gerais_actGrelha("","uCrsUsTP",lcSQL)
			uf_perguntalt_chama("OCORREU UM ERRO A CARREGAR OS DADOS DA TABELA DE PRE�OS DO ESPECIALISTA!","OK","",16)
			RETURN .f.
		ENDIF
		
		** Cria cursor de disponibilidades
		lcSQL=''
		TEXT TO lcSQL TEXTMERGE noshow
			exec up_gerais_disponibilidades 'utilizadores',<<lcNo>>,0
		ENDTEXT
		IF !uf_gerais_actGrelha("","ucrsUsDisponibilidades",lcSQL)
			uf_perguntalt_chama("OCORREU UM ERRO A CARREGAR DISPONIBILIDADES DO ESPECIALISTA!","OK","",16)
			RETURN .f.
		ENDIF
		
		** Cria cursor de indisponibilidades
		lcSQL=''
		TEXT TO lcSQL TEXTMERGE noshow
			exec up_gerais_indisponibilidades <<lcNo>>
		ENDTEXT
		IF !uf_gerais_actGrelha("","ucrsUsIndisponibilidades",lcSQL)
			uf_perguntalt_chama("OCORREU UM ERRO A CARREGAR INDISPONIBILIDADES DO ESPECIALISTA!","OK","",16)
		ENDIF
		
		**cria cursor de Servi�os Disponibilidades
		TEXT TO lcSQL NOSHOW TEXTMERGE
			exec up_utilizadores_disponibilidadesRecursos 'utilizadores',<<lcNo>>,0
		ENDTEXT
		IF !uf_gerais_actGrelha("","ucrsUsServicos",lcSQL)
			uf_perguntalt_chama("OCORREU UM ERRO A CARREGAR SERVI�OS DAS DISPONIBILIDADES!","OK","",16)
		ENDIF
		
		DO FORM UTILIZADORES
		
		&& Configura menu principal
		IF TYPE("UTILIZADORES.menu1.pesquisar") == "U"
			WITH UTILIZADORES.menu1
				.adicionaOpcao("aplicacoes", "Aplica��es", myPath + "\imagens\icons\aplicacoes_w.png", "uf_gerais_menuAplicacoes With 'Utilizadores'","A")
				.adicionaOpcao("opcoes","Op��es", myPath + "\imagens\icons\opcoes_w.png","", "O")
				.adicionaOpcao("pesquisar","Pesquisar",myPath + "\imagens\icons\lupa_w.png","uf_pesqutilizadores_chama", "P")
				.adicionaOpcao("novo","Novo",myPath + "\imagens\icons\mais_w.png","uf_utilizadores_introducao", "N")
				.adicionaOpcao("editar","Editar",myPath + "\imagens\icons\lapis_w.png","uf_utilizadores_alteracao", "E")
				**.adicionaOpcao("altpassw","Alterar Passw.",myPath + "\imagens\icons\Proteger_w.png","uf_utilizadores_altpass", "P")
			ENDWITH
			
			WITH utilizadores.menu_opcoes
				.adicionaOpcao("grupos","Grupos de Utilizadores","","uf_gruposut_chama with '"+astr(ch_grupo)+"'")
				.adicionaOpcao("ultimo","�ltimo","","uf_utilizadores_ultimoRegisto")
				.adicionaOpcao("eliminar","Eliminar","","uf_utilizadores_eliminar")
				.adicionaOpcao("fornecedor","Criar Fornecedor","","uf_utilizadores_criarFl")
				.adicionaOpcao("altpassw","Alterar Passw.","","uf_gerais_altpassword with 'B_US', ALLTRIM(ucrsUs.iniciais)")
			ENDWITH
			
			WITH utilizadores.menu_aplicacoes
				.adicionaOpcao("confighonorarios", "Configura��o Honor�rios", "", "uf_CONFIGHONORARIOS_chama")
				.adicionaOpcao("honorarios","Gest�o Honor�rios","","uf_CONFIGHONORARIOS_chama")
				**.adicionaOpcao("honorarios","Gest�o Honor�rios","","uf_HONORARIOS_chama")
			ENDWITH 
			UTILIZADORES.Menu1.gravar.img1.Picture = myPath + "\imagens\icons\guardar_w.png"
		ENDIF
	ELSE

		&& Cursor de CONVENCOES		
		lcSQL = ""
		TEXT TO lcSQL NOSHOW TEXTMERGE
			exec up_utilizadores_Convencoes <<lcNo>>
		ENDTEXT
		
		IF !uf_gerais_actGrelha("UTILIZADORES.PageFrame1.Page4.GridPesq","uCrsUsConvencoes",lcSQL)
			uf_perguntalt_chama("OCORREU UM ERRO A CARREGAR OS DADOS DAS CONVEN��ES DO ESPECIALISTA!","OK","",16)
		ENDIF
		
		&& Cursor de TP		
		lcSQL = ""
		TEXT TO lcSQL NOSHOW TEXTMERGE
			exec up_utilizadores_tabelaPrecos <<lcNo>>
		ENDTEXT
		
		IF !uf_gerais_actGrelha("UTILIZADORES.PageFrame1.Page4.GridTP","uCrsUsTP",lcSQL)
			uf_perguntalt_chama("OCORREU UM ERRO A CARREGAR OS DADOS DA TABELA DE PRE�OS DO ESPECIALISTA!","OK","",16)
		ENDIF
		
		** Cursor de disponibilidades
		lcSQL=''
		TEXT TO lcSQL TEXTMERGE noshow
			exec up_gerais_disponibilidades 'utilizadores',<<lcNo>>,0
		ENDTEXT
		IF !uf_gerais_actGrelha("UTILIZADORES.PageFrame1.Page5.GridDisp","ucrsUsDisponibilidades",lcSQL)
			uf_perguntalt_chama("OCORREU UM ERRO A CARREGAR DISPONIBILIDADES DO ESPECIALISTA!","OK","",16)
		ENDIF
		
		** Cria cursor de indisponibilidades
		lcSQL=''
		TEXT TO lcSQL TEXTMERGE noshow
			exec up_gerais_indisponibilidades <<lcNo>>
		ENDTEXT
		IF !uf_gerais_actGrelha("UTILIZADORES.PageFrame1.Page5.GridIndisp","ucrsUsIndisponibilidades",lcSQL)
			uf_perguntalt_chama("OCORREU UM ERRO A CARREGAR INDISPONIBILIDADES DO ESPECIALISTA!","OK","",16)
		ENDIF
		
		**Cursor de Servi�os Disponibilidades
		TEXT TO lcSQL NOSHOW TEXTMERGE
			exec up_utilizadores_disponibilidadesRecursos 'utilizadores',<<lcNo>>,0
		ENDTEXT
		IF !uf_gerais_actGrelha("UTILIZADORES.PageFrame1.Page5.GridServicos","ucrsUsServicos",lcSQL)
			uf_perguntalt_chama("OCORREU UM ERRO A CARREGAR SERVI�OS DAS DISPONIBILIDADES!","OK","",16)
		ENDIF
		
			
		UTILIZADORES.show
	ENDIF
	
	uf_utilizadores_controlaObj()
	
	&& Preencher Sexo
	WITH UTILIZADORES.pageframe1.page1
		SELECT uCrsUs
		DO CASE
			CASE uCrsUs.sexo == 0
				.OptiongroupSexo.Option1.value=0
				.OptiongroupSexo.Option2.value=0
			CASE uCrsUs.sexo == 1
				.OptiongroupSexo.Option1.value=1
				.OptiongroupSexo.Option2.value=0			
			CASE uCrsUs.sexo == 2
				.OptiongroupSexo.Option1.value=0
				.OptiongroupSexo.Option2.value=1			
			OTHERWISE
				***
		ENDCASE
	ENDWITH
	
	&& Preencher Local Inscri��o
	WITH UTILIZADORES.pageframe1.page2
		SELECT uCrsUs
		DO CASE
			CASE uCrsUs.drinscri == 1
				.OptiongroupInscrito.OptIOM.value=1
				.OptiongroupInscrito.OptIOMD.value=0
				.OptiongroupInscrito.OptIMS.value=0
				.OptiongroupInscrito.OptFarm.value=0
				.OptiongroupInscrito.OptNone.value=0
				.OptiongroupInscrito.OptEnf.value=0
			CASE uCrsUs.drinscri == 2
				.OptiongroupInscrito.OptIOM.value=0
				.OptiongroupInscrito.OptIOMD.value=1
				.OptiongroupInscrito.OptIMS.value=0	
				.OptiongroupInscrito.OptFarm.value=0
				.OptiongroupInscrito.OptNone.value=0		
				.OptiongroupInscrito.OptEnf.value=0
			CASE uCrsUs.drinscri == 3
				.OptiongroupInscrito.OptIOM.value=0
				.OptiongroupInscrito.OptIOMD.value=0
				.OptiongroupInscrito.OptIMS.value=1	
				.OptiongroupInscrito.OptFarm.value=0
				.OptiongroupInscrito.OptNone.value=0
				.OptiongroupInscrito.OptEnf.value=0
			CASE uCrsUs.drinscri == 4
				.OptiongroupInscrito.OptIOM.value=0
				.OptiongroupInscrito.OptIOMD.value=0
				.OptiongroupInscrito.OptIMS.value=0	
				.OptiongroupInscrito.OptFarm.value=1
				.OptiongroupInscrito.OptNone.value=0		
				.OptiongroupInscrito.OptEnf.value=0
			CASE uCrsUs.drinscri == 5
				.OptiongroupInscrito.OptIOM.value=0
				.OptiongroupInscrito.OptIOMD.value=0
				.OptiongroupInscrito.OptIMS.value=0	
				.OptiongroupInscrito.OptFarm.value=0
				.OptiongroupInscrito.OptNone.value=0		
				.OptiongroupInscrito.OptEnf.value=1
			OTHERWISE
				.OptiongroupInscrito.OptIOM.value=0
				.OptiongroupInscrito.OptIOMD.value=0
				.OptiongroupInscrito.OptIMS.value=0	
				.OptiongroupInscrito.OptFarm.value=0
				.OptiongroupInscrito.OptNone.value=1
				.OptiongroupInscrito.OptEnf.value=0
		ENDCASE
	ENDWITH
	
	IF !EMPTY(uCrsUs.userno)
		UTILIZADORES.Pageframe1.Page3.Disponibilidades1.Atualizar("UTILIZADORES",uCrsUs.userno,0)
	ENDIF
ENDFUNC


**
** Configura Ecra 
FUNCTION uf_utilizadores_controlaObj

**	IF LEFT(ALLTRIM(ch_grupo),13) == 'Administrador'
	
		IF !myUtIntroducao AND !myUtAlteracao && ecra em modo de consulta
			
			**
			WITH utilizadores
				.caption			= uf_gerais_aplicaLanguageMsg("Gest�o de Utilizadores")
				.txtNome.readonly	= .t.
	**			.txtNo.readonly	= .t.
				.chkInactivo.enable(.f.)
			ENDWITH

			WITH utilizadores.pageFrame1.page1
				.check_Faltas.enable(.f.)
				.check_Actualizacoes.enable(.f.)
				.txtIniciais.readonly = .t.
				.txtLogin.readonly = .t.
				.txtPass.readonly = .t.
				.txtLoja.readonly = .t.
				.txtGrupo.readonly = .t.
				.txtDep.readonly = .t.
				.txtArea.readonly = .t.
				.txtLingua.readonly = .t.
				.txtMorada.readonly = .t.
				.txtCodPost.readonly = .t.
				.txtnCont.readonly = .t.
				.txtLocal.readonly = .t.
				.txtDataNascimento.readonly = .t.
				.txtTlm.readonly = .t.
				.txtEmail.readonly = .t.
				.txtEmailProf.readonly = .t.
				.txtNacionalidade.readonly = .t.
				**.txtTratamento.readonly = .t.
				.txtObs.readonly = .t.
				.OptiongroupSexo.enabled = .f.
				.txtextensao.readonly = .t.
			ENDWITH
			
			WITH utilizadores.pageFrame1.page2
				.txtOrdem.readonly = .t.
				.txtCedula.readonly = .t.
				.txtdrClProfi.readonly = .t.
				.optiongroupInscrito.enabled = .f.
				.txtCodSinave.readonly = .t.
			ENDWITH

			WITH UTILIZADORES.pageframe1.page2
				SELECT uCrsUs
				DO CASE
					CASE uCrsUs.drinscri == 1
						.OptiongroupInscrito.OptIOM.value=1
						.OptiongroupInscrito.OptIOMD.value=0
						.OptiongroupInscrito.OptIMS.value=0
						.OptiongroupInscrito.OptFarm.value=0
						.OptiongroupInscrito.OptNone.value=0
						.OptiongroupInscrito.OptEnf.value=0
					CASE uCrsUs.drinscri == 2
						.OptiongroupInscrito.OptIOM.value=0
						.OptiongroupInscrito.OptIOMD.value=1
						.OptiongroupInscrito.OptIMS.value=0	
						.OptiongroupInscrito.OptFarm.value=0
						.OptiongroupInscrito.OptNone.value=0		
						.OptiongroupInscrito.OptEnf.value=0
					CASE uCrsUs.drinscri == 3
						.OptiongroupInscrito.OptIOM.value=0
						.OptiongroupInscrito.OptIOMD.value=0
						.OptiongroupInscrito.OptIMS.value=1	
						.OptiongroupInscrito.OptFarm.value=0
						.OptiongroupInscrito.OptNone.value=0
						.OptiongroupInscrito.OptEnf.value=0
					CASE uCrsUs.drinscri == 4
						.OptiongroupInscrito.OptIOM.value=0
						.OptiongroupInscrito.OptIOMD.value=0
						.OptiongroupInscrito.OptIMS.value=0	
						.OptiongroupInscrito.OptFarm.value=1
						.OptiongroupInscrito.OptNone.value=0	
						.OptiongroupInscrito.OptEnf.value=0	
					CASE uCrsUs.drinscri == 5
						.OptiongroupInscrito.OptIOM.value=0
						.OptiongroupInscrito.OptIOMD.value=0
						.OptiongroupInscrito.OptIMS.value=0	
						.OptiongroupInscrito.OptFarm.value=0
						.OptiongroupInscrito.OptNone.value=0	
						.OptiongroupInscrito.OptEnf.value=1
					OTHERWISE
						.OptiongroupInscrito.OptIOM.value=0
						.OptiongroupInscrito.OptIOMD.value=0
						.OptiongroupInscrito.OptIMS.value=0	
						.OptiongroupInscrito.OptFarm.value=0
						.OptiongroupInscrito.OptNone.value=1
						.OptiongroupInscrito.OptEnf.value=0
				ENDCASE
			ENDWITH
			
			
			utilizadores.menu1.estado("opcoes, pesquisar, novo, editar", "SHOW", "Gravar", .f., "Sair", .t.)
			utilizadores.menu_opcoes.estado("eliminar, fornecedor, altpassw", "SHOW")
			**utilizadores.menu1.estado("altpassw", "HIDE")

		ELSE

			utilizadores.alterapass = 0

			**
			WITH utilizadores
				IF myUtIntroducao == .t.
					.caption = uf_gerais_aplicaLanguageMsg("Gest�o de Utilizadores - Introdu��o")
				ELSE
					.caption = uf_gerais_aplicaLanguageMsg("Gest�o de Utilizadores - Altera��o")
				ENDIF

				IF myUtAlteracao
					.txtNome.readonly	= .t.
				ELSE
					.txtNome.readonly	= .f.
				ENDIF
		**		.txtNo.readonly	= .f.
				.chkInactivo.enable(.t.)
				
			ENDWITH

			WITH utilizadores.pageFrame1.page1
				.check_Faltas.enable(.t.)
				.check_Actualizacoes.enable(.t.)
				.txtIniciais.readonly = .f.
				.txtLogin.readonly = .f.
				.txtPass.readonly = .f.
				.txtLoja.readonly = .f.				
				.txtGrupo.readonly = .f.
				.txtDep.readonly = .f.
				.txtArea.readonly = .f.
				.txtLingua.readonly = .f.
				.txtMorada.readonly = .f.
				.txtCodPost.readonly = .f.
				.txtnCont.readonly = .f.
				.txtLocal.readonly = .f.
				.txtDataNascimento.readonly = .f.
				.txtTlm.readonly = .f.
				.txtLingua.readonly = .f.
				.txtEmail.readonly = .f.
				.txtEmailProf.readonly = .f.
				.txtNacionalidade.readonly = .f.
			**	.txtTratamento.readonly = .f.
				.txtObs.readonly = .f.
				.OptiongroupSexo.enabled = .t.
				.txtextensao.readonly = .f.
			ENDWITH
			
			
			
			WITH utilizadores.pageFrame1.page2
				.txtOrdem.readonly = .f.
				.txtCedula.readonly = .f.
				.txtdrClProfi.readonly = .f.
				.optiongroupInscrito.enabled = .t.	
				.txtCodSinave.readonly = .f.	
			ENDWITH		
			
			WITH UTILIZADORES.pageframe1.page2
				SELECT uCrsUs
				DO CASE
					CASE uCrsUs.drinscri == 1
						.OptiongroupInscrito.OptIOM.value=1
						.OptiongroupInscrito.OptIOMD.value=0
						.OptiongroupInscrito.OptIMS.value=0
						.OptiongroupInscrito.OptFarm.value=0
						.OptiongroupInscrito.OptEnf.value=0
						.OptiongroupInscrito.OptNone.value=0
					CASE uCrsUs.drinscri == 2
						.OptiongroupInscrito.OptIOM.value=0
						.OptiongroupInscrito.OptIOMD.value=1
						.OptiongroupInscrito.OptIMS.value=0	
						.OptiongroupInscrito.OptFarm.value=0		
						.OptiongroupInscrito.OptEnf.value=0
						.OptiongroupInscrito.OptNone.value=0
					CASE uCrsUs.drinscri == 3
						.OptiongroupInscrito.OptIOM.value=0
						.OptiongroupInscrito.OptIOMD.value=0
						.OptiongroupInscrito.OptIMS.value=1	
						.OptiongroupInscrito.OptFarm.value=0
						.OptiongroupInscrito.OptEnf.value=0
						.OptiongroupInscrito.OptNone.value=0
					CASE uCrsUs.drinscri == 4
						.OptiongroupInscrito.OptIOM.value=0
						.OptiongroupInscrito.OptIOMD.value=0
						.OptiongroupInscrito.OptIMS.value=0	
						.OptiongroupInscrito.OptFarm.value=1		
						.OptiongroupInscrito.OptEnf.value=0
						.OptiongroupInscrito.OptNone.value=0
					CASE uCrsUs.drinscri == 5
						.OptiongroupInscrito.OptIOM.value=0
						.OptiongroupInscrito.OptIOMD.value=0
						.OptiongroupInscrito.OptIMS.value=0	
						.OptiongroupInscrito.OptFarm.value=0
						.OptiongroupInscrito.OptEnf.value=1
						.OptiongroupInscrito.OptNone.value=0
					OTHERWISE
						.OptiongroupInscrito.OptIOM.value=0
						.OptiongroupInscrito.OptIOMD.value=0
						.OptiongroupInscrito.OptIMS.value=0	
						.OptiongroupInscrito.OptFarm.value=0
						.OptiongroupInscrito.OptEnf.value=0
						.OptiongroupInscrito.OptNone.value=1
				ENDCASE
			ENDWITH
			
			uf_utilizadores_camposObrigatorios()
			utilizadores.menu1.estado("opcoes, pesquisar, novo, editar", "HIDE", "Gravar", .t., "Sair", .t.)
			utilizadores.menu_opcoes.estado("eliminar,fornecedor, altpassw", "HIDE")

		ENDIF
		
		IF myUtIntroducao 
			WITH utilizadores.pageFrame1.page1
				.txtPass.readonly = .f.
			ENDWITH
		ELSE
			WITH utilizadores.pageFrame1.page1
				.txtPass.readonly = .t.
			ENDWITH		
		ENDIF 
		
**	ELSE
**		utilizadores.menu1.estado("aplicacoes, opcoes, novo, editar", "HIDE", "Gravar", .f., "Sair", .t.)
**		utilizadores.menu_opcoes.estado("eliminar,fornecedor", "HIDE")
**		utilizadores.menu1.estado("pesquisar, altpassw", "SHOW")

**	ENDIF 

ENDFUNC



**
FUNCTION uf_utilizadores_camposObrigatorios

	IF UPPER(ALLTRIM(uCrsE1.tipoempresa)) == 'CLINICA'
		WITH utilizadores
			.txtNome.backcolor = RGB(191,223,223)
			.txtNo.backcolor = RGB(191,223,223)
		ENDWITH
		WITH utilizadores.pageFrame1.page1
			.txtIniciais.backcolor = RGB(191,223,223)
			.txtLogin.backcolor = RGB(191,223,223)	
			.txtPass.backcolor = RGB(191,223,223)
			.txtGrupo.backcolor = RGB(191,223,223)
			.txtTlm.backcolor = RGB(191,223,223)	
			.txtnCont.backcolor = RGB(191,223,223)
		ENDWITH
	ELSE
		WITH utilizadores
			.txtNome.backcolor = RGB(191,223,223)
			.txtNo.backcolor = RGB(191,223,223)
		ENDWITH
		WITH utilizadores.pageFrame1.page1
			.txtIniciais.backcolor = RGB(191,223,223)
			.txtLogin.backcolor = RGB(191,223,223)	
			.txtPass.backcolor = RGB(191,223,223)
			.txtGrupo.backcolor = RGB(191,223,223)
			.txtLoja.backcolor = RGB(191,223,223) && acrescento de JG a 20200813
		ENDWITH
	ENDIF
ENDFUNC


**
FUNCTION uf_utilizadores_introducao
	IF !uf_gerais_validaPermUser(ch_userno,ch_grupo,'Gest�o de Utilizadores - Introduzir')
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE EXECUTAR ESTA AC��O.","OK","",48)
		RETURN .f.
	ENDIF
	
	IF myUtIntroducao == .t. OR myUtAlteracao == .t.
		uf_perguntalt_chama("O Ecr� de Gest�o de Utilizadores est� em modo de Introdu��o/Edi��o. Deve cancelar ou gravar para proceder com esta ac��o.", "OK", "", 64)
		RETURN .f.
	ENDIF

	IF uf_gerais_getParameter_site("ADM0000000185", "BOOL", mySite) AND !uf_gerais_getParameter("ADM0000000353", "BOOL")
		uf_perguntalt_chama("N�o pode criar novos utilizadores sem ser na Base de Dados Central", "OK", "", 64)
		RETURN .f.	
	ENDIF

	myUtIntroducao = .t.
	
	**Limpa Cursores
	IF USED("ucrsUs")
		SELECT ucrsUs
		GO TOP 
		SCAN 
			DELETE
		ENDSCAN 

		SELECT ucrsUs
		APPEND BLANK
	ENDIF 

	IF USED("ucrsUsAux")
		SELECT ucrsUsAux
		GO TOP 
		SCAN 
			DELETE
		ENDSCAN 
			
		SELECT ucrsUsAux
		APPEND BLANK
	ENDIF 
	
	IF USED("UCRSDRESP")
		SELECT UCRSDRESP
		GO TOP 
		SCAN 
			DELETE
		ENDSCAN 
	ENDIF
	
	IF USED("UcrsPeriodosData")
		SELECT UcrsPeriodosData
		GO TOP 
		SCAN 
			DELETE
		ENDSCAN 
	ENDIF
	
	IF USED("UcrsPeriodosClinica")
		SELECT UcrsPeriodosClinica
		GO TOP 
		SCAN 
			DELETE
		ENDSCAN 
	ENDIF
	IF USED("uCrsUsTP")
		SELECT uCrsUsTP
		GO TOP 
		SCAN 
			DELETE
		ENDSCAN 
	ENDIF
	IF USED("ucrsUsDisponibilidades")
		SELECT ucrsUsDisponibilidades
		GO TOP 
		SCAN 
			DELETE
		ENDSCAN 
	ENDIF
	IF USED("ucrsUsIndisponibilidades")
		SELECT ucrsUsIndisponibilidades
		GO TOP 
		SCAN 
			DELETE
		ENDSCAN 
	ENDIF
	IF USED("ucrsUsServicos")
		SELECT ucrsUsServicos
		GO TOP 
		SCAN 
			DELETE
		ENDSCAN 
	ENDIF
	IF USED("uCrsUsConvencoes")
		SELECT uCrsUsConvencoes
		GO TOP 
		SCAN 
			DELETE
		ENDSCAN 
	ENDIF
	
	uf_utilizadores_valoresDefeito()

	utilizadores.txtNome.setfocus
	uf_utilizadores_controlaObj()
	utilizadores.pageframe1.page2.optiongroupinscrito.optiom.value=0
	utilizadores.pageframe1.page2.optiongroupinscrito.optiomd.value=0
	utilizadores.pageframe1.page2.optiongroupinscrito.optims.value=0
	utilizadores.pageframe1.page2.optiongroupinscrito.OptFarm.value=0
	utilizadores.pageframe1.page2.optiongroupinscrito.OptNone.value=0
	utilizadores.pageframe1.page2.optiongroupinscrito.OptEnf.value=0
	utilizadores.pageframe1.refresh
	utilizadores.pageframe1.page1.refresh
	utilizadores.pageframe1.page2.refresh
	utilizadores.pageframe1.page3.refresh
	utilizadores.pageframe1.page4.refresh
	utilizadores.pageframe1.page5.refresh

	utilizadores.refresh
ENDFUNC 


**
FUNCTION uf_utilizadores_valoresDefeito
	lcstamp = uf_gerais_stamp()
	
	select ucrsUs
	replace ;
		ucrsUs.usstamp 	WITH lcstamp ;
		ucrsUs.dtnasc 	WITH DATETIME(1900, 01, 01, 00, 00, 00) ;
		uCrsUs.loja		WITH mySite ;
		uCrsUs.lingua	WITH 'ptPT' ;
		uCrsUs.drinscri WITH 0
		
	uf_utilizadores_controlaNumeroUt()
ENDFUNC 


** Coloca N�mero de Documento
Function uf_utilizadores_controlaNumeroUt

	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_utilizadores_calculaNumero
	Endtext
	If !uf_gerais_actgrelha("", "ucrsUtNovoNumero", lcSql)
		uf_perguntalt_chama("N�o foi poss�vel determinar o n�mero a atribuir. Por favor contacte o suporte.", "OK", "", 16)
		RETURN .f.
	ENDIF
	
	SELECT ucrsUs	
	Replace ucrsUs.userno WITH ucrsUtNovoNumero.userno
	
	IF USED("ucrsUtNovoNumero")
		fecha("ucrsUtNovoNumero")
	ENDIF
	
ENDFUNC


**
FUNCTION uf_utilizadores_alteracao
	IF !uf_gerais_validaPermUser(ch_userno,ch_grupo,'Gest�o de Utilizadores - Alterar')
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE EXECUTAR ESTA AC��O.","OK","",48)
		RETURN .f.
	ENDIF

	IF myUtIntroducao == .t. OR myUtAlteracao == .t.
		uf_perguntalt_chama("O Ecr� de Gest�o de Utilizadores est� em modo de Introdu��o/Edi��o. Deve cancelar ou gravar para proceder com esta ac��o.", "OK", "", 64)
		RETURN .f.
	ENDIF
	
	IF empty(ucrsUs.userno)
		RETURN .f.
	Endif
	
	myUtAlteracao = .t.
	utilizadores.txtNome.setfocus

	
	uf_utilizadores_controlaObj()
	utilizadores.refresh
	
ENDFUNC 


**
FUNCTION uf_utilizadores_ultimoRegisto
	**Se estiver em modo de edi��o n�o � possivel usar esta fun��o
	IF myUtIntroducao == .t. OR myUtAlteracao == .t.
		uf_perguntalt_chama("O Ecr� de Gest�o de Utilizadores est� em modo de Introdu��o/Edi��o. Deve cancelar ou gravar para proceder com esta ac��o.", "OK", "", 64)
		RETURN .f.
	ENDIF
		
	**App Gerais (O parametro � irrelevante, tratamento igual para BO ou FO)
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_utilizadores_ultimo
	ENDTEXT 
	IF uf_gerais_actgrelha("", "ucrsUltimoUs", lcSql) == .t.
		SELECT ucrsUltimoUs
		IF !empty(ucrsUltimoUs.userno)
			uf_utilizadores_chama(ucrsUltimoUs.userno)
		ENDIF
	ENDIF
ENDFUNC


**
FUNCTION uf_utilizadores_actualiza
	SELECT ucrsUs
	uf_utilizadores_chama(ucrsUs.userno)
ENDFUNC 


**
FUNCTION uf_utilizadores_eliminar
	IF !uf_gerais_validaPermUser(ch_userno,ch_grupo,'Gest�o de Utilizadores - Eliminar')
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE EXECUTAR ESTA AC��O.","OK","",48)
		RETURN .f.
	ENDIF
	
	&& Se tem prescri��es
	uf_gerais_actgrelha("","uCrstempEspPresc",[Select top 1 drno from b_cli_presc (nolock) where drno =]+ALLTRIM(STR(uCrsUs.userno)))
	IF RECCOUNT("uCrstempEspPresc")>0
		uf_perguntalt_chama("N�O PODE APAGAR ESPECIALISTAS COM PRESCRI��ES ASSOCIADAS.","OK","",48)	
		RETURN .f.
	ENDIF
	FECHA("uCrstempEspPresc")
	
	SELECT ucrsUs
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_utilizadores_ValidaEliminacao  <<ucrsUs.userno>>
	ENDTEXT

	IF uf_gerais_actgrelha("", "ucrsValidaEliminacao", lcSql)
		SELECT ucrsValidaEliminacao
		IF ucrsValidaEliminacao.cont > 0
			uf_perguntalt_chama("O utilizador tem vendas associadas. N�o � possivel eliminar.", "OK", "", 64)
			RETURN .f.
		ELSE
			IF uf_perguntalt_chama("Tem a certeza que pretende eliminar o Utilizador?","Sim","N�o")
				IF uf_gerais_actGrelha("","",[BEGIN TRANSACTION])
					lcSQL = ""
					TEXT TO lcSQL NOSHOW textmerge
						DELETE FROM b_usesp WHERE userno=<<astr(uCrsUs.userno)>>
						DELETE FROM b_cli_drdv WHERE drno=<<astr(uCrsUs.userno)>>
						DELETE FROM b_cli_pd WHERE drno=<<astr(uCrsUs.userno)>>
						DELETE FROM b_series where tipo  = 'Utilizadores' and no = <<astr(uCrsUs.userno)>>
						DELETE FROM b_us_disp_resursos where seriestamp in (select seriestamp from b_series where tipo  = 'Utilizadores' and no = <<astr(uCrsUs.userno)>>)
					ENDTEXT
					IF uf_gerais_actGrelha("","",lcSQL)
						&& APAGA FICHA DO ESPECIALISTA
						
						lcSQl = ""
						TEXT TO lcSQL NOSHOW TEXTMERGE
							DELETE FROM b_us WHERE usstamp='<<ALLTRIM(uCrsUs.usstamp)>>'
							DELETE FROM cm3 WHERE cm =<<uCrsUs.userno>>
						ENDTEXT 
						IF uf_gerais_actGrelha("","",lcSQL)
							
							
							uf_gerais_actGrelha("","",[COMMIT TRANSACTION])
							uf_perguntalt_chama("Registo eliminado com sucesso!", "OK", "", 64)
							
							&& navega para ultimo registo
							uf_utilizadores_ultimoRegisto()
						ELSE
							u_sqlexec([ROLLBACK])
							uf_perguntalt_chama("N�o foi possivel eliminar o registo. Por favor contacte o suporte.", "OK", "", 16)
						ENDIF
					ELSE
						u_sqlexec([ROLLBACK])
						uf_perguntalt_chama("N�o foi possivel eliminar o registo. Por favor contacte o suporte.", "OK", "", 16)
					ENDIF
				ENDIF
			ENDIF		
		ENDIF
	ENDIF
ENDFUNC


**
FUNCTION uf_utilizadores_gravar
	
	utilizadores.txtNome.setfocus
	
	LOCAL lcvalidaCamposObrigatorios, lcValidaInsercao, lcValidaUpdate, lcValidaInsEsp, lcValidaUpdEsp, lcValidaInsereDisponibilidades
	STORE .f. TO lcvalidaCamposObrigatorios, lcValidaInsercao, lcValidaUpdate, lcValidaInsEsp, lcValidaUpdEsp, lcValidaInsereDisponibilidades, lcValidaAlteracaoIniciais
	**
	
	lcvalidaCamposObrigatorios = uf_utilizadores_validaCamposObrigatorios()
	
	

	If !lcvalidaCamposObrigatorios
		RETURN .f.	&& A mensagem ao utilizador � dada na fun��o anterior
	ENDIF

	IF uf_gerais_actgrelha("","",[Begin Transaction])
		IF myUtIntroducao
			lcValidaInsercao = uf_utilizadores_insere()
			lcValidaInsEsp = uf_utilizadores_insEspecialidades()
			lcValidaInsereVendedor = uf_utilizadores_insVendedor()

			IF lcValidaInsercao AND lcValidaInsEsp
				IF uf_gerais_actgrelha("","",[commit Transaction])
					uf_utilizadores_gravarDiponibilidade()
					uf_utilizadores_gravarIndiponibilidade()
					uf_utilizadores_gravarDiponibilidadeServ()
					uf_perguntalt_chama("Registo introduzido com sucesso!", "OK", "", 64)
					
				ELSE
					uf_gerais_actgrelha("","",[rollback transaction])
				ENDIF
			ELSE
				uf_gerais_actgrelha("","",[rollback transaction])			
			ENDIF
		ELSE
			If myUtAlteracao
				
				&& valida iniciais utilizador
				&& se forem alteradas as iniciais, valida se o operador tem vendas		
		
					
				lcValidaAlteracaoIniciais = uf_utilizadores_validaAlteracaoIniciais()
				
				IF(lcValidaAlteracaoIniciais = .t.)
				
					lcValidaUpdate = uf_utilizadores_actualiza()
				
					lcValidaUpdEsp = uf_utilizadores_updEspecialidades()
					lcValidaUpdVendedor = uf_utilizadores_updVendedor()
					
				ELSE
					uf_perguntalt_chama("N�o � permitido alterar as inicias deste utilizador", "OK", "", 64)		
				ENDIF
				
				IF lcValidaUpdate AND lcValidaUpdEsp
					IF uf_gerais_actgrelha("","",[commit Transaction])
						uf_utilizadores_gravarDiponibilidade()
						uf_utilizadores_gravarIndiponibilidade()
						uf_utilizadores_gravarDiponibilidadeServ()
						uf_perguntalt_chama("Registo actualizado com sucesso!", "OK", "", 64)
						
					ELSE
						uf_gerais_actgrelha("","",[rollback transaction])
					ENDIF
				ELSE
					uf_gerais_actgrelha("","",[rollback transaction])			
				ENDIF
			ENDIF
		ENDIF
	ELSE
		uf_gerais_actgrelha("","",[rollback transaction])
	ENDIF

	myUtIntroducao	= .f.
	myUtAlteracao	= .f.
	uf_utilizadores_controlaObj()
	
	
	** valida comunica��es com conroladores de assiduidade
	if(!EMPTY(myUsaServicosClock))
		uf_utilizadores_clock_adiciona()
	ENDIF
	
ENDFUNC

FUNCTION uf_utilizadores_clock_adiciona

	LOCAL lcNo, lcNome, lcPrivil, lcActivo, lcRfId, lcEstado, lcHttpPost,lcIdLt, lcToken 
	
	lcPrivil= 0
	lcNo= 0
	lcNome= ""
	lcActivo= .f.
	lcRfId= ""
	lcEstado = .f.
	lcIdLt = ""
	lcToken  = ""
	
	SELECT ucrse1
	GO TOP
	lcIdLt = ALLTRIM(LOWER(ucrse1.id_lt))
	
	lcHttpPost = ALLTRIM("https://app.lts.pt:50001/api/clock/relay?id=" + lcIdLt)

	
	** valida se parameterizado
	** param_empresa ADM0000000095
	
	if(EMPTY(myTipoClock))
		uf_perguntalt_chama("Id do equipamento n�o parametrizado. Por favor contacte o suporte.","OK","",16)
		RETURN  .f.
	ENDIF
	
	**valida estado da maquina
**	lcToken = ALLTRIM(uf_gerais_stamp())
	lcEstado =  uf_servicos_clock_status(lcHttpPost,"" ,.t.)
	
	
	** se a maquina n�o estiver funcional
	if(EMPTY(lcEstado))
		RETURN  .f.
	ENDIF
		

	** se a maquina estiver funcional
	if(USED("ucrsUs"))
		SELECT ucrsUs
		GO TOP
		IF RECCOUNT("ucrsUs") > 0		
			SELECT ucrsUs
			GO TOP
			
			lcNo  = ucrsUs.userNo
			lcName  = ALLTRIM(ucrsUs.nome)
			lcActivo= !ucrsUs.inactivo
		**	lcToken = ALLTRIM(uf_gerais_stamp())
			RETURN uf_servicos_clock_addUser(lcHttpPost ,lcNo  ,lcActivo,lcName ,"",0,"" )

		ENDIF
	ELSE
		RETURN  .f.
	ENDIF	
	
	
	

ENDFUNC





**
FUNCTION uf_utilizadores_updEspecialidades
	SELECT uCrsUs
		
	&& procurar especialidades associadas ao m�dico
	lcSQL=''
	TEXT TO lcSQL TEXTMERGE NOSHOW
		SELECT 
			usespstamp
			,espno
		FROM 
			b_usesp (nolock)
		WHERE 
			userno=<<uCrsUs.userno>>
	ENDTEXT
	IF !(uf_gerais_actgrelha("","uCrsEspMedico",lcSQL))
		uf_perguntalt_chama("OCORREU UM ERRO A ATUALIZAR O ESPECIALISTA. Cod:b_usesp","OK","",16)
		RETURN	.f.
	ENDIF 
	
	select uCrsEspMedico
	GO TOP
	select uCrsDrEsp
	GO TOP
	&& acrescentar especialidades novas ao m�dico
	SELECT * FROM uCrsDrEsp WHERE uCrsDrEsp.espno not in (select uCrsEspMedico.espno from uCrsEspMedico) into cursor lcCursorInsertEsp READWRITE

	LOCAL lcStamp
	SELECT lcCursorInsertEsp 
	SCAN 					
		lcSQL=''
		lcStamp=uf_gerais_stamp()
		TEXT TO lcSQL TEXTMERGE NOSHOW
			INSERT INTO b_usesp
			(
				usespstamp, userno, 
				espno, especialidade, lordem,
				ousrinis, ousrdata, ousrhora, 
				usrinis, usrdata, usrhora
			)
			VALUES
			(
				'<<lcStamp>>', <<uCrsUs.userno>>,
				'<<ALLTRIM(lcCursorInsertEsp.espno)>>', '<<ALLTRIM(lcCursorInsertEsp.especialidade)>>', (select ISNULL(MAX(lordem),0)+1 from b_usesp (nolock) where userno=<<uCrsUs.userno>>),
				'<<m_chinis>>', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108), 
				'<<m_chinis>>', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108)
			)
		ENDTEXT 
		IF !(uf_gerais_actgrelha("","",lcSQL))
			uf_perguntalt_chama("OCORREU UM ERRO A ATUALIZAR O ESPECIALISTA. Cod:b_usesp","OK","",16)
			RETURN	.f.
		ENDIF  
		
		SELECT lcCursorInsertEsp
	ENDSCAN 
	
	&& eliminar especialidades associadas ao m�dico
	SELECT * FROM uCrsEspMedico WHERE espno not in (select espno from uCrsDrEsp) into cursor lcCursorDeleteEsp READWRITE
	
	SELECT lcCursorDeleteEsp
	SCAN
		lcSQL = ""
		TEXT TO lcSQL textmerge NOSHOW
			DELETE FROM b_usesp
			WHERE usespstamp = '<<ALLTRIM(lcCursorDeleteEsp.usespstamp)>>'
		ENDTEXT
		IF !uf_gerais_actgrelha("","",lcSQL)
			uf_perguntalt_chama("OCORREU UM ERRO A ATUALIZAR O ESPECIALISTA. Cod:b_usesp","OK","",16)
			RETURN .f.
		ENDIF
		
		SELECT lcCursorDeleteEsp
	ENDSCAN

	Fecha("uCrsEspMedico")
	
	Fecha("lcCursorDeleteEsp")
	
	Fecha("lcCursorInsertEsp")

	RETURN .t.
ENDFUNC 


**
FUNCTION uf_utilizadores_insEspecialidades
	LOCAL lcOrdem, lcStamp
	STORE 1 TO lcOrdem
	
	SELECT uCrsUs 
	
	SELECT uCrsDrEsp
	IF RECCOUNT("uCrsDrEsp") > 0
		SCAN
			lcSQL=''
			lcStamp = uf_gerais_stamp()
			TEXT TO lcSQL TEXTMERGE NOSHOW
				INSERT INTO b_usesp
				(
					usespstamp
					,userno
					,espno
					,especialidade
					,lordem
					,ousrinis
					,ousrdata
					,ousrhora
					,usrinis
					,usrdata
					,usrhora
				)
				VALUES
				(
					'<<lcStamp>>'
					,<<uCrsUs.userno>>
					,'<<uCrsDrEsp.espno>>'
					,'<<ALLTRIM(uCrsDrEsp.especialidade)>>'
					,<<lcOrdem>>
					,'<<m_chinis>>'
					,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
					,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108)
					,'<<m_chinis>>'
					,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
					,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108)
				)
			ENDTEXT 
			IF !(uf_gerais_actGrelha("","",lcSQL))
				uf_perguntalt_chama("OCORREU UM ERRO A INSERIR O ESPECIALISTA. Cod:b_usesp","OK","",16)
				RETURN .f.
			ENDIF 
			lcOrdem = lcOrdem + 1
		ENDSCAN 
	ENDIF
	
	RETURN .t.
ENDFUNC

** Verfica se pode alterar as iniciais
**
FUNCTION uf_utilizadores_validaAlteracaoIniciais
	lcOk = .t.
	
	WITH utilizadores.pageFrame1.page1	
			
		SELECT uCrsUsAux
		IF ALLTRIM(.txtIniciais.value) != ALLTRIM(uCrsUsAux.iniciais)
		
		    **Valida se o utilizador j� est� mapeado com algum Fornecedor, Venda ou Documento
			TEXT TO lcSQL NOSHOW TEXTMERGE
				exec up_utilizadores_ValidaIniciais <<uCrsUsAux.userno>>, <<uCrsUsAux.iniciais>>
			ENDTEXT 
			If !uf_gerais_actGrelha("", "uCrsValidaMovUser", lcSql)
			
				uf_perguntalt_chama("N�o foi possivel encontrar o Utilizador", "", "OK", 32)
				
				lcOk = .f.
			Else
			
				&&se as iniciais est�o a ser usadas -> n�o deixa alterar	
				SELECT uCrsValidaMovUser	
				IF (RECCOUNT("uCrsValidaMovUser")>0)
				
					SELECT uCrsUs
					replace iniciais WITH ALLTRIM(uCrsUsAux.iniciais)
					
					lcOk = .f.
					
				ELSE
					lcOk = .t.		
				ENDIF
			ENDIF
		ELSE
			lcOk = .t.	
		ENDIF

	ENDWITH
	
	IF used("uCrsValidaMovUser")
		fecha("uCrsValidaMovUser")
	ENDIF
	
	RETURN lcOk
	
ENDFUNC 




**
FUNCTION uf_utilizadores_validaCamposObrigatorios
	WITH utilizadores
		IF EMPTY(ALLTRIM(.txtNome.value)) OR EMPTY(.txtNo.value)
			uf_perguntalt_chama("Existem campos obrigat�rios por preencher.", "OK", "", 64)
			RETURN .f.
		ENDIF
	ENDWITH
	
	WITH utilizadores.pageFrame1.page1
	
		IF UPPER(ALLTRIM(ucrsE1.tipoempresa)) = 'CLINICA'
			IF EMPTY(ALLTRIM(.txtIniciais.value));
				OR EMPTY(ALLTRIM(.txtLogin.value));
				OR EMPTY(ALLTRIM(.txtPass.value));
				OR EMPTY(ALLTRIM(.txtGrupo.value));
				OR EMPTY(ALLTRIM(.txtTlm.value));
				OR EMPTY(ALLTRIM(.txtnCont.value))
				
				uf_perguntalt_chama("Existem campos obrigat�rios por preencher. Por favor verifique.", "OK", "", 64)
				RETURN .f.
			ENDIF
		ELSE
			IF EMPTY(ALLTRIM(.txtIniciais.value));
				OR EMPTY(ALLTRIM(.txtLogin.value));
				OR EMPTY(ALLTRIM(.txtPass.value));
				OR EMPTY(ALLTRIM(.txtGrupo.value));
				OR EMPTY(ALLTRIM(.txtLoja.value));   
				
				&& altera��o JG a 20200813 para contemplar obgrigatoriedade de colocar Loja
					
				uf_perguntalt_chama("Existem campos obrigat�rios por preencher. Por favor verifique.", "OK", "", 64)
				RETURN .f.
			ENDIF		
		ENDIF
		
		&& E-mail v�lido
		IF !EMPTY(ALLTRIM(.txtEMail.value))
			IF(at("@",.txtEMail.value)) == 0 &&verifica se cont�m o caracter @ no email
				uf_perguntalt_chama("O CAMPO E-MAIL � INV�LIDO.","OK","",48)
				utilizadores.lbl1.click
				.txtEMail.setfocus
				RETURN .f.
			ENDIF 
		ENDIF
	ENDWITH
	
	SELECT ucrsUs
	WITH utilizadores.pageFrame1.page2
		DO CASE
			CASE  ucrsUs.DRINSCRI == 1
				IF !EMPTY(ALLTRIM(.txtCedula.value))
					IF LEFT(ALLTRIM(.txtCedula.value),1) != "M" OR LEN(ALLTRIM(.txtCedula.value))!=6
						uf_perguntalt_chama("O N�MERO DE C�DULA PROFISSIONAL EST� INV�LIDO, POR FAVOR VERIFIQUE." +  chr(13) + " Formato: Mnnnnn", "OK","",46)
						utilizadores.lbl2.click
						.txtCedula.setfocus
						RETURN .f.
					ENDIF
				ENDIF 
				
			CASE  ucrsUs.DRINSCRI == 2
				IF !EMPTY(ALLTRIM(.txtCedula.value))
					IF LEFT(ALLTRIM(.txtCedula.value),1) != "D" OR LEN(ALLTRIM(.txtCedula.value))!=6 
						uf_perguntalt_chama("O N�MERO DE C�DULA PROFISSIONAL EST� INV�LIDO, POR FAVOR VERIFIQUE"+  chr(13) + " Formato: Dnnnnn", "OK","",46)
						utilizadores.lbl2.click
						.txtCedula.setfocus
						RETURN .f.
					ENDIF
				ENDIF 
			
			CASE  ucrsUs.DRINSCRI == 3
				IF !EMPTY(ALLTRIM(.txtCedula.value))
					IF LEFT(ALLTRIM(.txtCedula.value),1) != "O" OR LEN(ALLTRIM(.txtCedula.value))!=6
						uf_perguntalt_chama("O N�MERO DE C�DULA PROFISSIONAL EST� INV�LIDO, POR FAVOR VERIFIQUE"+  chr(13) + " Formato: Onnnnn", "OK","",46)
						utilizadores.lbl2.click
						.txtCedula.setfocus
						RETURN .f.
					ENDIF
				ENDIF 

			CASE  ucrsUs.DRINSCRI == 4
				IF LEN(ALLTRIM(.txtCedula.value))>5 OR EMPTY(ALLTRIM(.txtCedula.value))
					uf_perguntalt_chama("O N�MERO DE C�DULA PROFISSIONAL EST� INV�LIDO, POR FAVOR VERIFIQUE"+  chr(13) + " Formato: nnnnn", "OK","",46)
					utilizadores.lbl2.click
					.txtCedula.setfocus
					RETURN .f.
				ENDIF

			CASE  ucrsUs.DRINSCRI == 5
				IF EMPTY(ALLTRIM(.txtCedula.value))
					uf_perguntalt_chama("TEM DE PREENCHER O N�MERO DE C�DULA PROFISSIONAL.", "OK","",46)
					utilizadores.lbl2.click
					.txtCedula.setfocus
					RETURN .f.
				ENDIF
				
			OTHERWISE
				**
		ENDCASE
	ENDWITH
	
	&& valida se j� existe o numero
	SELECT uCrsUs
	TEXT TO lcSQL TEXTMERGE NOSHOW
		SELECT iniciais FROM b_us (nolock) WHERE userno = <<uCrsUs.userno>> and b_us.usstamp != '<<ALLTRIM(uCrsUs.usstamp)>>'
	ENDTEXT
	uf_gerais_actGrelha("","uCrsTempUserNo",lcSQL)
	IF RECCOUNT("uCrsTempUserNo") > 0
		uf_perguntalt_chama("J� existe um utilizador com esse n�mero. Por favor valide.", "OK","",46)
		RETURN .f.
	ENDIF
	IF USED("uCrsTempUserNo")
		fecha("uCrsTempUserNo")
	ENDIF 

	&& valida se j� existem as mesmas inicias
	IF myUtIntroducao
		SELECT uCrsUs
		TEXT TO lcSQL TEXTMERGE NOSHOW
			SELECT iniciais FROM b_us (nolock) WHERE iniciais = '<<ALLTRIM(uCrsUs.iniciais)>>'
		ENDTEXT
		uf_gerais_actGrelha("","uCrsTempIniciais",lcSQL)
		IF RECCOUNT("uCrsTempIniciais") > 0
			uf_perguntalt_chama("J� existe um utilizador com as mesmas iniciais. Por favor verifique.", "OK","",46)
			RETURN .f.
		ENDIF
		fecha("uCrsTempIniciais")
	ENDIF

	&& Valida se j� existe utilizador com as mesmas credenciais - Lu�s Leal 03112015
	LOCAL lcencryptedpassw 
	lcencryptedpassw = uf_gerais_encrypt(ucrsUs.userpass)
	
	lcSQL  = ''
	select uCrsUs
	TEXT TO lcSQL TEXTMERGE NOSHOW 
		select password FROM b_us (nolock) WHERE password = '<<ALLTRIM(lcencryptedpassw)>>' AND username != '<<ALLTRIM(uCrsUs.username)>>'
	ENDTEXT 
	uf_gerais_actGrelha("","uCrsTempPw",lcSQL)
	IF RECCOUNT("uCrsTempPw") > 0
		uf_perguntalt_chama("J� existe um utilizador com as mesmas credenciais. Por favor verifique.", "OK", "", 46)
		fecha("uCrsTempPw")
		RETURN .f.
	ENDIF

	fecha("uCrsTempPw")
		
	RETURN .t.
ENDFUNC 


**
FUNCTION uf_utilizadores_insere

	uf_utilizadores_controlaNumeroUt()
	
	LOCAL lcencryptedpassw 
	lcencryptedpassw = uf_gerais_encrypt(ucrsUs.userpass)
	
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		
		Insert into b_us
		(	usstamp, iniciais, username, userpass
			,userno, grupo, ugstamp
			,inactivo, supermsg, superact, d500
			,nome, tipo, morada, local, codpost
			,tlmvl, email, dtnasc, sexo, nacion
			,tratamento, obs, ncont, drcedula
			,drinscri, drordem, ousrdata, ousrhora, ousrinis
			,usrdata, usrhora, usrinis, lingua, loja, departamento
			,area, drclprofi
			,HonorarioDeducao
			,HonorarioValor
			,HonorarioBi
			,HonorarioTipo
			,Encryptedpassw
			,password
			,primacesso
			,extensaotelefone
			,emailprofissional	
			,codsinave		
		)
		Values (
			'<<Alltrim(ucrsUs.usstamp)>>','<<ALLTRIM(ucrsUs.iniciais)>>', '<<ALLTRIM(ucrsUs.username)>>', '<<ALLTRIM(ucrsUs.userpass)>>'
			,<<ucrsUs.userno>>, '<<Alltrim(ucrsUs.grupo)>>', '<<Alltrim(ucrsUs.ugstamp)>>'
			,<<IIF(ucrsUs.inactivo,1,0)>>,  <<IIF(ucrsUs.supermsg,1,0)>>,  <<IIF(ucrsUs.superact,1,0)>>,  <<IIF(ucrsUs.d500,1,0)>>
			,'<<Alltrim(ucrsUs.nome)>>', '<<Alltrim(ucrsUs.tipo)>>', '<<Alltrim(ucrsUs.morada)>>', '<<Alltrim(ucrsUs.local)>>', '<<Alltrim(ucrsUs.codpost)>>'
			,'<<ALLTRIM(ucrsUs.tlmvl)>>', '<<Alltrim(ucrsUs.email)>>', '<<uf_gerais_getdate(ucrsUs.dtnasc,"SQL")>>', '<<ucrsUs.sexo>>', '<<Alltrim(ucrsUs.nacion)>>'
			,'<<Alltrim(ucrsUs.tratamento)>>', '<<Alltrim(ucrsUs.obs)>>', '<<ALLTRIM(ucrsUs.ncont)>>', '<<ALLTRIM(ucrsUs.drcedula)>>'
			,'<<ucrsUs.drinscri>>', '<<ALLTRIM(ucrsUs.drordem)>>'
			,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102),convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8),'<<m.m_chinis>>',convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102),convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8),'<<m.m_chinis>>'
			,'<<Alltrim(ucrsUs.lingua)>>', '<<Alltrim(ucrsUs.loja)>>', '<<Alltrim(ucrsUs.departamento)>>'
			,'<<Alltrim(ucrsUs.area)>>', '<<Alltrim(ucrsUs.drclprofi)>>'
			,<<ucrsUs.HonorarioDeducao>>
			,<<ucrsUs.HonorarioValor>>
			,'<<Alltrim(ucrsUs.HonorarioBi)>>'
			,'<<Alltrim(ucrsUs.HonorarioTipo)>>'
			, 0x
			, '<<ALLTRIM(lcencryptedpassw)>>'
			,1
			,'<<ucrsUs.extensaotelefone>>'
			,'<<Alltrim(ucrsUs.emailprofissional)>>'
			,'<<ALLTRIM(ucrsUs.codsinave)>>'
		)

	ENDTEXT 
	
	IF uf_gerais_actgrelha("", "", lcSql)
		RETURN .t.
	ELSE
		uf_perguntalt_chama("N�o foi possivel introduzir o registo. Contacte o suporte.", "OK", "", 16)
		RETURN .f.
	ENDIF 
ENDFUNC


**
FUNCTION uf_utilizadores_actualiza

	LOCAL lcencryptedpassw 
	lcencryptedpassw = uf_gerais_encrypt(ucrsUs.userpass)
	
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE 
	
		Update 
			b_us
		SET 
			iniciais = '<<ALLTRIM(ucrsUs.iniciais)>>'
			,username = '<<ALLTRIM(ucrsUs.username)>>'
			,userpass = '<<ALLTRIM(ucrsUs.userpass)>>'
			,userno = <<ucrsUs.userno>>
			,grupo =  '<<Alltrim(ucrsUs.grupo)>>'
			,ugstamp = '<<Alltrim(ucrsUs.ugstamp)>>'
			,inactivo = <<IIF(ucrsUs.inactivo,1,0)>>
			,supermsg = <<IIF(ucrsUs.supermsg,1,0)>>
			,superact = <<IIF(ucrsUs.superact,1,0)>>
			,d500 = <<IIF(ucrsUs.d500,1,0)>>
			,nome = '<<Alltrim(ucrsUs.nome)>>'
			,tipo = '<<Alltrim(ucrsUs.tipo)>>'
			,morada = '<<Alltrim(ucrsUs.morada)>>'
			,local = '<<Alltrim(ucrsUs.local)>>'
			,codpost = '<<Alltrim(ucrsUs.codpost)>>'
			,tlmvl = '<<ALLTRIM(ucrsUs.tlmvl)>>'
			,email = '<<Alltrim(ucrsUs.email)>>'
			,dtnasc = '<<uf_gerais_getdate(ucrsUs.dtnasc,"SQL")>>'
			,sexo = '<<ucrsUs.sexo>>'
			,nacion = '<<Alltrim(ucrsUs.nacion)>>'
			,tratamento = '<<Alltrim(ucrsUs.tratamento)>>'
			,obs = '<<Alltrim(ucrsUs.obs)>>'
			,ncont = '<<ALLTRIM(ucrsUs.ncont)>>'
			,drcedula = '<<ALLTRIM(ucrsUs.drcedula)>>'
			,drinscri = '<<ucrsUs.drinscri>>'
			,drordem = '<<ALLTRIM(ucrsUs.drordem)>>'
			,usrdata = convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
			,usrhora = convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8)
			,usrinis = '<<m.m_chinis>>'
			,lingua = '<<Alltrim(ucrsUs.lingua)>>'
			,loja = '<<Alltrim(ucrsUs.loja)>>'
			,departamento = '<<Alltrim(ucrsUs.departamento)>>'
			,area = '<<Alltrim(ucrsUs.area)>>'
			,drclprofi = '<<Alltrim(ucrsUs.drclprofi)>>'
			,HonorarioDeducao = <<ucrsUs.HonorarioDeducao>>
			,HonorarioValor = <<ucrsUs.HonorarioValor>>
			,HonorarioBi = '<<Alltrim(ucrsUs.HonorarioBi)>>'
			,HonorarioTipo = '<<Alltrim(ucrsUs.HonorarioTipo)>>'
			,Encryptedpassw = 0x
			--,password='<<lcencryptedpassw>>'
			,extensaotelefone = '<<ucrsUs.extensaotelefone>>'
			,emailprofissional = '<<Alltrim(ucrsUs.emailprofissional)>>'
			,codsinave = '<<Alltrim(ucrsUs.codsinave)>>'
		Where 
			usstamp = '<<Alltrim(ucrsUs.usstamp)>>'
		
	ENDTEXT 	

	IF uf_gerais_actgrelha("", "", lcSql)
		RETURN .t.
	ELSE
		uf_perguntalt_chama("N�o foi possivel actualizar o registo. Contacte o suporte.", "OK", "", 16)
		RETURN .f.
	ENDIF 
ENDFUNC	


** Cancelar
FUNCTION uf_utilizadores_sair
	utilizadores.txtNome.setfocus
	
	* Verifica se o Documento esta em Modo de Edicao ou Introducao
	If myUtIntroducao OR myUtAlteracao
		IF uf_perguntalt_chama("Quer cancelar as altera��es efectuadas?", "SIM", "N�O", 64) == .t.
			STORE .f. TO myUtIntroducao, myUtAlteracao
			
			uf_utilizadores_controlaObj()
			
			SELECT uCrsUs
			**ultimo registo alterado
			uf_utilizadores_chama(uCrsUs.userno)
		Endif
	Else
		uf_utilizadores_exit()
	Endif
Endfunc

**
FUNCTION uf_utilizadores_exit
	fecha("ucrsUs")
	fecha("ucrsUsAux")
	Fecha("uCrsEspCl")
	Fecha("uCrsDRTratamento")
	Fecha("uCrsDRNacion")
	FECHA("uCrsDREsp")
	fecha("UcrsPeriodosClinica")
	fecha("UcrsPeriodosData")
	
	UTILIZADORES.hide
	UTILIZADORES.release
ENDFUNC

**
FUNCTION uf_utilizadores_insVendedor
	
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE 	
		Insert into cm3
		(	
			cm3stamp, cmdesc, cm
			,nome, area
			, ousrdata, ousrhora, ousrinis
			,usrdata, usrhora, usrinis
		)
		Values (
			LEFT(newid(),25), '<<ALLTRIM(ucrsUs.username)>>', <<uCrsUs.userno>>
			,'<<ALLTRIM(ucrsUs.username)>>', '<<ALLTRIM(mySite)>>'
			,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102),convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8),'<<m_chinis>>'
			,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102),convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8),'<<m_chinis>>'
		)
	ENDTEXT 

	IF uf_gerais_actgrelha("", "", lcSql)
		RETURN .t.
	ELSE
		uf_perguntalt_chama("N�o foi possivel introduzir o registo. Contacte o suporte.", "OK", "", 16)
		RETURN .f.
	ENDIF 
ENDFUNC


**
FUNCTION uf_utilizadores_updVendedor
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE 	
		UPDATE cm3
		SET 	
			cmdesc = '<<ALLTRIM(ucrsUs.username)>>'
			,nome = '<<ALLTRIM(ucrsUs.username)>>'
			,usrdata = convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
			,usrhora = convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8)
			,usrinis = '<<m_chinis>>'
		where
			cm = <<ucrsUs.userno>>
	ENDTEXT 
	
	IF uf_gerais_actgrelha("", "", lcSql)
		RETURN .t.
	ELSE
		uf_perguntalt_chama("N�o foi possivel actualizar o registo. Contacte o suporte.", "OK", "", 16)
		RETURN .f.
	ENDIF 
ENDFUNC


**
FUNCTION uf_utilizadores_disponibilidades
	
	SELECT userno, username, nome FROM ucrsUs INTO CURSOR ucrsDadosUsDisp READWRITE
	
	SELECT ucrsUs
	uf_pesqSeries_Chama("",ucrsUs.userno,"Utilizadores")
ENDFUNC


**
FUNCTION uf_utilizadores_criaNovaLinhaDisp
	LOCAL lcid_disp,lcstamp 
	lcid_disp = 0
	SELECT ucrsUsDisponibilidades
	CALCULATE MAX(ucrsUsDisponibilidades.id_disp) TO lcid_disp
	lcstamp = uf_gerais_stamp()
	
	SELECT ucrsUsDisponibilidades
	APPEND BLANK
	Replace ucrsUsDisponibilidades.seriestamp WITH lcstamp 
	Replace ucrsUsDisponibilidades.site WITH ALLTRIM(mysite)
	Replace ucrsUsDisponibilidades.dataInicio WITH datetime()+(difhoraria*3600)
	Replace ucrsUsDisponibilidades.dataFim WITH datetime()+(difhoraria*3600)
	Replace ucrsUsDisponibilidades.HoraInicio WITH "00:00"
	Replace ucrsUsDisponibilidades.HoraFim WITH "23:59"
	Replace ucrsUsDisponibilidades.id_disp WITH lcid_disp+1
	
	UTILIZADORES.PageFrame1.Page5.GridDisp.refresh
ENDFUNC


**	
FUNCTION uf_utilizadores_apagaLinhaDisp
	SELECT ucrsUsDisponibilidades
	DELETE
	
	TRY
		SELECT ucrsUsDisponibilidades
		SKIP -1
	ENDTRY
		
	
	UTILIZADORES.PageFrame1.Page5.GridDisp.refresh
ENDFUNC


**
FUNCTION uf_utilizadores_gravarDiponibilidade
	LOCAL lcCamposObrigatorios, lcValidaInsert, lcValidaUpdate, lcStamp, lcActualizaRecursos, lcValidaSequenciaPeriodos 

	STORE .f. TO lcCamposObrigatorios, lcValidaInsert, lcValidaUpdate, lcActualizaRecursos, lcValidaSequenciaPeriodos 
	
	SELECT ucrsUs
	**Numeracao da serie
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE
		DELETE b_series where tipo = 'utilizadores' AND no = <<ucrsUs.userno>>
	ENDTEXT
	
	IF !uf_gerais_actGrelha("", "", lcSql)
		uf_perguntalt_chama("Ocorreu um erro na grava��o das disponibilidades. Contacte o suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF

	SELECT ucrsUsDisponibilidades
	GO Top
	SCAN
		
		lcSQL = ""
		TEXT TO lcSQL NOSHOW TEXTMERGE
			INSERT INTO	b_series (	
				seriestamp
				,serieno
				,serienome
				,nome
				,no
				,estab
				,dirClinico
				,dirServico
				,dataInicio
				,sessoes
				,ousrinis
				,ousrdata
				,ousrhora
				,usrinis
				,usrdata
				,usrhora
				,horaInicio
				,horafim
				,segunda
				,terca
				,quarta
				,quinta
				,sexta
				,sabado
				,sempre
				,tododia
				,domingo
				,dataFim
				,dataIniRep
				,duracao
				,tipo
				,tempoinatividade
				,repetir
				,criterio
				,valorrep
				,repnunca
				,repapos
				,repdia
				,dataFimRep
				,template
				,site
			)VALUES (
					'<<ALLTRIM(ucrsUsDisponibilidades.seriestamp)>>'
					,(select ISNULL(MAX(serieno),0) +1 from b_series)
					,'<<ALLTRIM(ucrsUsDisponibilidades.serienome)>>'
					,'<<ALLTRIM(ucrsUs.nome)>>'
					,<<ucrsUs.userno>>,
					0,
					'',
					'',
					'<<uf_gerais_getdate(ucrsUsDisponibilidades.dataInicio,"SQL")>>',
					1
					,'<<m.m_chinis>>'
					,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
					,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8)
					,'<<m.m_chinis>>'
					,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
					,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8)
					,'<<ALLTRIM(ucrsUsDisponibilidades.horaInicio)>>'
					,'<<ALLTRIM(ucrsUsDisponibilidades.horafim)>>'
					,<<IIF(ucrsUsDisponibilidades.segunda,1,0)>>
					,<<IIF(ucrsUsDisponibilidades.terca,1,0)>>
					,<<IIF(ucrsUsDisponibilidades.quarta,1,0)>>
					,<<IIF(ucrsUsDisponibilidades.quinta,1,0)>>
					,<<IIF(ucrsUsDisponibilidades.sexta,1,0)>>
					,<<IIF(ucrsUsDisponibilidades.sabado,1,0)>>
					,<<IIF(ucrsUsDisponibilidades.sempre,1,0)>>
					,<<IIF(ucrsUsDisponibilidades.tododia,1,0)>>
					,<<IIF(ucrsUsDisponibilidades.domingo,1,0)>>
					,'<<uf_gerais_getdate(ucrsUsDisponibilidades.dataFim,"SQL")>>'
					,'<<uf_gerais_getdate(ucrsUsDisponibilidades.dataInicio,"SQL")>>'
					,<<ucrsUsDisponibilidades.duracao>>
					,'Utilizadores'
					,0
					,0
					,''
					,0
					,0
					,0
					,0
					,'<<uf_gerais_getdate(ucrsUsDisponibilidades.dataFim,"SQL")>>'
					,0
					,'<<ALLTRIM(ucrsUsDisponibilidades.site)>>'
			)
		ENDTEXT
	
		
		If !uf_gerais_actGrelha("", "", lcSql)
			uf_perguntalt_chama("Ocorreu um erro na grava��o das disponibilidades. Contacte o suporte.", "", "OK", 32)
			RETURN .f.
		ENDIF	
		
	ENDSCAN 

ENDFUNC


**
FUNCTION uf_utilizadores_gravarDiponibilidadeServ
	SELECT ucrsUsServicos
	SET FILTER TO
	SELECT ucrsUsServicos
	GO Top
	
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE
		DELETE FROM b_us_disp_resursos WHERE b_us_disp_resursos.seriestamp in (Select seriestamp FROM b_series where tipo = 'utilizadores' AND no = <<ucrsUs.userno>>)
	ENDTEXT 	
	IF !uf_gerais_actGrelha("", "",lcSQL)
		MESSAGEBOX("N�O FOI POSSIVEL ACTUALIZAR A LISTAGEM DE RECURSOS. POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF
	
	SELECT ucrsUsServicos
	GO Top
	SCAN
		lcSQL = ""
		TEXT TO lcSQL NOSHOW TEXTMERGE
			INSERT INTO b_us_disp_resursos (
				id
				,seriestamp
				,nome
				,ref
				,tipo
			)VALUES(
				(Select ISNULL(MAX(id),0)+1 from b_us_disp_resursos)
				,'<<ALLTRIM(ucrsUsServicos.Seriestamp)>>'
				,'<<ALLTRIM(ucrsUsServicos.nome)>>'
				,'<<ALLTRIM(ucrsUsServicos.ref)>>'
				,'<<ALLTRIM(ucrsUsServicos.tipo)>>'
			)
		ENDTEXT 	
		IF !uf_gerais_actGrelha("", "",lcSQL)
			MESSAGEBOX("N�O FOI POSSIVEL ACTUALIZAR A LISTAGEM DE RECURSOS. POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
			RETURN .f.
		ENDIF
	ENDSCAN 
	
ENDFUNC 


**
FUNCTION uf_utilizadores_criaNovaLinhaIndisp
	SELECT ucrsUsIndisponibilidades
	APPEND BLANK
	Replace ucrsUsIndisponibilidades.site WITH ALLTRIM(mysite)
	Replace ucrsUsIndisponibilidades.dataInicio WITH DATE()
	Replace ucrsUsIndisponibilidades.dataFim WITH DATE()
	Replace ucrsUsIndisponibilidades.HoraInicio WITH "00:00"
	Replace ucrsUsIndisponibilidades.HoraFim WITH "23:59"
	UTILIZADORES.PageFrame1.Page5.GridIndisp.refresh
ENDFUNC


**	
FUNCTION uf_utilizadores_apagaLinhaIndisp
	SELECT ucrsUsIndisponibilidades
	DELETE
	
	TRY
		SELECT ucrsUsIndisponibilidades
		SKIP -1
	ENDTRY
		
	
	UTILIZADORES.PageFrame1.Page5.GridIndisp.refresh
ENDFUNC


**
FUNCTION uf_utilizadores_gravarIndiponibilidade
	LOCAL lcCamposObrigatorios, lcValidaInsert, lcValidaUpdate, lcStamp, lcActualizaRecursos, lcValidaSequenciaPeriodos 

	STORE .f. TO lcCamposObrigatorios, lcValidaInsert, lcValidaUpdate, lcActualizaRecursos, lcValidaSequenciaPeriodos 
	
	
	SELECT ucrsUs
	**Numeracao da serie
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE
		DELETE b_us_indisponibilidades where userno = <<ucrsUs.userno>>
	ENDTEXT
	

	
	If !uf_gerais_actGrelha("", "", lcSql)
		uf_perguntalt_chama("Ocorreu um erro na grava��o das indisponibilidades. Contacte o suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF

	SELECT ucrsUsIndisponibilidades
	GO Top
	SCAN
		
		lcSQL = ""
		TEXT TO lcSQL NOSHOW TEXTMERGE
			INSERT INTO	b_us_indisponibilidades (	
				id
				,userno
				,dataInicio
				,dataFim
				,horaInicio
				,horafim
				,segunda
				,terca
				,quarta
				,quinta
				,sexta
				,sabado
				,sempre
				,tododia
				,domingo
				,site
				,design
				,ousrinis
				,ousrdata
				,ousrhora
				,usrinis
				,usrdata
				,usrhora
			)VALUES (
				(select ISNULL(MAX(id),0) + 1 from b_us_indisponibilidades )
				,<<ucrsUs.userno>>
				,'<<uf_gerais_getdate(ucrsUsIndisponibilidades.dataInicio,"SQL")>>'
				,'<<uf_gerais_getdate(ucrsUsIndisponibilidades.dataFim,"SQL")>>'
				,'<<ALLTRIM(ucrsUsIndisponibilidades.horaInicio)>>'
				,'<<ALLTRIM(ucrsUsIndisponibilidades.horafim)>>'
				,<<IIF(ucrsUsIndisponibilidades.segunda,1,0)>>
				,<<IIF(ucrsUsIndisponibilidades.terca,1,0)>>
				,<<IIF(ucrsUsIndisponibilidades.quarta,1,0)>>
				,<<IIF(ucrsUsIndisponibilidades.quinta,1,0)>>
				,<<IIF(ucrsUsIndisponibilidades.sexta,1,0)>>
				,<<IIF(ucrsUsIndisponibilidades.sabado,1,0)>>
				,<<IIF(ucrsUsIndisponibilidades.sempre,1,0)>>
				,<<IIF(ucrsUsIndisponibilidades.tododia,1,0)>>
				,<<IIF(ucrsUsIndisponibilidades.domingo,1,0)>>
				,'<<ALLTRIM(ucrsUsIndisponibilidades.site)>>'
				,'<<ALLTRIM(ucrsUsIndisponibilidades.design)>>'
				,'<<m.m_chinis>>'
				,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
				,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8)
				,'<<m.m_chinis>>'
				,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
				,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8)
			)
		ENDTEXT
	
		
		If !uf_gerais_actGrelha("", "", lcSql)
			uf_perguntalt_chama("Ocorreu um erro na grava��o das indisponibilidades. Contacte o suporte.", "", "OK", 32)
			RETURN .f.
		ENDIF	
	ENDSCAN 
	
ENDFUNC


**
FUNCTION uf_utilizadores_criarFl
	LOCAL lcUserNo, lcSQL
	lcUserNo = 0
	lcSQL = ""
	
	SELECT ucrsUs
	lcUserNo = ucrsUs.userno
	
	IF EMPTY(lcUserNo)
		uf_perguntalt_chama("Selecione o utilizador.", "", "OK", 64)
		RETURN .f.
	ENDIF 
	
	**Valida se o utilizador j� est� mapeado com algum Fornecedor
	TEXT TO lcSQL NOSHOW TEXTMERGE
		SELECT nome from fl WHERE fl.u_no = <<ucrsUs.userno>>
	ENDTEXT 
	If !uf_gerais_actGrelha("", "ucrsFLUs", lcSql)
		uf_perguntalt_chama("N�o foi possivel validar se o Utilizador existe com Fornecedor.", "", "OK", 32)
		RETURN .f.
	ENDIF
	
	IF RECCOUNT("ucrsFLUs") != 0
		SELECT ucrsFLUs
		uf_perguntalt_chama("O Utilizador j� est� ligado ao Fornecedor: " + ALLTRIM(ucrsFLUs.nome), "", "OK", 32)
		RETURN .f.
	ENDIF 
	
	IF USED("ucrsFLUs")
		fecha("ucrsFLUs")
	ENDIF 
	
	
	**Valida se o utilizador j� est� mapeado com algum Fornecedor
	TEXT TO lcSQL NOSHOW TEXTMERGE
		exec up_utilizadores_insereFl <<ucrsUs.userno>>
	ENDTEXT 
	If !uf_gerais_actGrelha("", "", lcSql)
		uf_perguntalt_chama("N�o foi possivel criar o Fornecedor.", "", "OK", 32)
		RETURN .f.
	ENDIF
	
	**
	uf_perguntalt_chama("Fornecedor criado com Sucesso.", "", "OK", 64)
	
ENDFUNC 


**
FUNCTION uf_utilizadores_actualizaServDisp
	SELECT ucrsUsDisponibilidades
	IF !EMPTY(ucrsUsDisponibilidades.id_disp)
		SELECT ucrsUsServicos
		SET FILTER TO ucrsUsServicos.id_disp==ucrsUsDisponibilidades.id_disp
	ELSE
		SELECT ucrsUsServicos
		SET FILTER TO
	ENDIF
	SELECT ucrsUsServicos
	GO TOP 
	UTILIZADORES.PageFrame1.Page5.GridServicos.refresh 
ENDFUNC 


**
FUNCTION uf_utilizadores_novoServDisp
	uf_pesqrefsagrupadas_chama(.t.,'DISPONIBILIDADESRECURSOS')
ENDFUNC 


**
FUNCTION uf_utilizadores_ApagaServDisp

	SELECT ucrsUsServicos
	DELETE 
	TRY
		SELECT ucrsUsServicos
		SKIP -1
	ENDTRY 
	
	UTILIZADORES.PageFrame1.Page5.GridServicos.refresh 

ENDFUNC 

FUNCTION uf_utilizadores_altpass
	LOCAL lcatualizou
	lcatualizou = 0
	DO WHILE lcatualizou = 0
		lcatualizou = uf_startup_passwprimacesso(2 , ALLTRIM(ucrsUs.iniciais)) 
	ENDDO 
	uf_perguntalt_chama("A sua password foi alterada com sucesso. Por favor volte a entrar no software","OK","",64)
	QUIT
ENDFUNC 