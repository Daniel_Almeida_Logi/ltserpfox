**
FUNCTION uf_especialidades_chama
	&& Cursor da DV
	IF !(uf_gerais_actGrelha("","uCrsDV","select convert(bit,0) as 'sel', especialidade, espno, especialidadestamp from b_especialidades (nolock) order by especialidade"))
		uf_perguntalt_chama("OCORREU UM ERRO A CARREGAR OS DADOS DAS ESPECIALIDADES!","OK","",16)
		RETURN .f.
	ENDIF
	
	SELECT uCrsDV

	&& Controla Abertura do Painel			
	if type("ESPECIALIDADES")=="U"
		DO FORM ESPECIALIDADES
	ELSE
		ESPECIALIDADES.show
	ENDIF
ENDFUNC 

******************************
* Seleccionar Especialidades *
******************************
FUNCTION uf_especialidades_gravar
	SELECT uCrsDV
	GO TOP 
	SCAN
		IF uCrsDV.Sel == .t.
			SELECT * FROM uCrsDrEsp WHERE uCrsDrEsp.espno == uCrsDV.espno into cursor lcRegistoJaExiste READWRITE
			IF RECCOUNT("lcRegistoJaExiste") == 0
				SELECT uCrsDrEsp
				APPEND BLANK
				replace uCrsDrEsp.especialidade WITH uCrsDV.especialidade
				replace uCrsDrEsp.espno WITH uCrsDV.espno
			ENDIF 
		ENDIF 
	ENDSCAN 
	
	utilizadores.pageframe1.page2.grdEsp.refresh
	
	uf_especialidades_sair()
ENDFUNC 

**********************************
* Sair do ecra de Especialidades *
**********************************
FUNCTION uf_especialidades_sair
	&& fecha cursores
	Fecha("uCrsDV")
				
	ESPECIALIDADES.hide
	ESPECIALIDADES.Release

	RELEASE ESPECIALIDADES
ENDFUNC

***************************
* Pesquisa Especialidades *
***************************
FUNCTION uf_especialidades_Pesquisar
	SELECT uCrsDV
	GO TOP
	
	IF !empty(ESPECIALIDADES.nome.value)
		SET FILTER TO LIKE('*'+UPPER(ALLTRIM(ESPECIALIDADES.nome.value))+'*',UPPER(uCrsDV.especialidade))
	ENDIF
	IF !empty(ESPECIALIDADES.no.value)
		SET FILTER TO UPPER(astr(ESPECIALIDADES.no.value)) = UPPER(astr(uCrsDV.espno))
	ENDIF
	IF empty(ESPECIALIDADES.no.value) AND empty(ESPECIALIDADES.nome.value)
		SET FILTER TO
	ENDIF
	
	SELECT uCrsDV
	GO TOP
	
	ESPECIALIDADES.grdEsp.Refresh
ENDFUNC 

************************
* Insere Especialidade *
************************
FUNCTION uf_especialidades_Insere
	LOCAL lcValor, lcStamp
	STORE '' TO lcValor, lcStamp
	lcValor = INPUTBOX("Qual a designa��o da nova entidade?", "Inserir Especialidade", "", 0, "")
	
	IF !EMPTY(ALLTRIM(lcValor))
		lcStamp=uf_gerais_stamp()
		lcSQL=''
		TEXT TO lcSQL TEXTMERGE NOSHOW 
			INSERT INTO b_especialidades
			(
				especialidadestamp, espno, especialidade, 
				ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora
			)
			VALUES
			(
				'<<lcStamp>>'
				,(select ISNULL(MAX(convert(numeric(9,0),espno)),0)+1 from b_especialidades (nolock))
				,'<<ALLTRIM(lcValor)>>'
				,'<<m_chinis>>'
				,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
				,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108)
				,'<<m_chinis>>'
				,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
				,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108)
			)
		ENDTEXT 

		IF !(uf_gerais_actGrelha("","",lcSQL))
			uf_perguntalt_chama("OCORREU UM ERRO A INSERIR A ESPECIALIDADE!","OK","",16)
		ENDIF 
		
		&& Cursor da DV
		IF !(uf_gerais_actGrelha("ESPECIALIDADES.grdEsp","uCrsDV","select convert(bit,0) as 'sel', especialidade, espno, especialidadestamp from b_especialidades (nolock) order by especialidade"))
			uf_perguntalt_chama("OCORREU UM ERRO A CARREGAR OS DADOS DAS ESPECIALIDADES!","OK","",16)
			RETURN .f.
		ENDIF
		
		ESPECIALIDADES.nome.value = ""
		ESPECIALIDADES.no.value = ""
		uf_especialidades_Pesquisar()
	ENDIF 
ENDFUNC 

************************
* Elimina Especialidade *
************************
FUNCTION uf_especialidades_Elimina
	IF uf_perguntalt_chama("TEM A CERTEZA QUE PRETENDE ELIMINAR A ESPECIALIDADE: " + ALLTRIM(ucrsDV.especialidade) + "?","Sim","N�o")
		lcSQL=''
		TEXT TO lcSQL TEXTMERGE NOSHOW 
			DELETE FROM b_especialidades
			WHERE especialidadestamp = '<<uCrsDV.especialidadestamp>>'
		ENDTEXT 
		
		IF !(uf_gerais_actGrelha("","",lcSQL))
			uf_perguntalt_chama("OCORREU UM ERRO A ELIMINAR A ESPECIALIDADE!","OK","",16)
		ELSE
			uf_perguntalt_chama("ESPECIALIDADE ELIMINADA COM SUCESSO.","OK","",64)
		ENDIF 
		
		&& actualizar grelha
		SELECT uCrsDV
		DELETE 
		
		ESPECIALIDADES.nome.value = ""
		ESPECIALIDADES.no.value = ""
		uf_especialidades_Pesquisar()
	ENDIF 
ENDFUNC 