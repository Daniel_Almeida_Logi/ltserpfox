FUNCTION uf_pesqutilizadores_chama
	LPARAMETERS locOrigem

	IF !uf_gerais_validaPermUser(ch_userno,ch_grupo,'Gest�o de Utilizadores')
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ACEDER AO PAINEL GEST�O DE UTILIZADORES.","OK","",48)
		RETURN .f.
	ENDIF
		
	IF !USED("uCrsPesqUtilizadores")
		uf_pesqutilizadores_actPesquisa(.t.)
	ENDIF
	
	IF TYPE("pesqutilizadores")=="U"
		DO FORM pesqutilizadores
		pesqutilizadores.show
	ELSE
		pesqutilizadores.show
	ENDIF
	
	pesqutilizadores.origem = locOrigem
	UPDATE uCrsPesqUtilizadores SET uCrsPesqUtilizadores.sel = .f.
	
ENDFUNC 


** tcBool = caso venha a .t. cria apenas o cursor
FUNCTION uf_pesqutilizadores_actPesquisa
	LPARAMETERS tcBool
	
	LOCAL lcSQL
	STORE "" TO lcSql
	
	IF tcBool
		lcSQL = ""
		IF ALLTRIM(ch_grupo) = 'Administrador' OR ALLTRIM(ch_grupo) = 'Supervisores'
			TEXT TO lcSql NOSHOW TEXTMERGE
				exec up_utilizadores_pesquisa '',0,'','','','','<<m_chinis>>'
			ENDTEXT
		ELSE
			TEXT TO lcSql NOSHOW TEXTMERGE 
				exec up_utilizadores_pesquisa '',<<ch_userno>>,'','','','','<<m_chinis>>'
			ENDTEXT
		ENDIF 

		if type("pesqutilizadores")=="U"
			IF !uf_gerais_actGrelha("", "uCrsPesqUtilizadores",lcSQL)
				MESSAGEBOX("N�O FOI POSSIVEL ACTUALIZAR A LISTAGEM DE UTILIZADORES. POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
				RETURN .f.
			ENDIF
		ENDIF
		
	ELSE &&Refresca valores da Grid
		lcSQL = ""
		IF ALLTRIM(ch_grupo) = 'Administrador' OR  ALLTRIM(ch_grupo) = 'Supervisores' OR !uf_gerais_validaPermUser(ch_userno,ch_grupo,'Gest�o de Utilizadores - Pesquisa todos') = .t.
			TEXT TO lcSql NOSHOW TEXTMERGE 
				exec up_utilizadores_pesquisa '<<ALLTRIM(pesqutilizadores.nome.value)>>',<<IIF(EMPTY(ALLTRIM(pesqutilizadores.no.value)),0,pesqutilizadores.no.value)>>,'<<ALLTRIM(pesqutilizadores.login.value)>>','<<ALLTRIM(pesqutilizadores.grupo.value)>>','<<ALLTRIM(pesqutilizadores.especialidade.value)>>','<<ALLTRIM(pesqutilizadores.site.value)>>','<<m_chinis>>'
			ENDTEXT
		ELSE
			TEXT TO lcSql NOSHOW TEXTMERGE 
				exec up_utilizadores_pesquisa '',<<ch_userno>>,'','','','','<<m_chinis>>'
			ENDTEXT
		ENDIF 
		uf_gerais_actGrelha("pesqutilizadores.GridPesq", "uCrsPesqUtilizadores",lcSQL)
				
		pesqutilizadores.Refresh
		pesqutilizadores.lblRegistos.caption = ALLTRIM(STR(RECCOUNT("uCrsPesqUtilizadores"))) + " Resultados"
	ENDIF
ENDFUNC


**
FUNCTION uf_pesqutilizadores_sair
	
	pesqutilizadores.hide
	pesqutilizadores.release
ENDFUNC


**
FUNCTION uf_pesqutilizadores_CarregaMenu

	WITH PESQUTILIZADORES.menu1
		&&.adicionaOpcao("aplicacoes", "Aplica��es", myPath + "\imagens\icons\aplicacoes_w.png", "uf_gerais_menuAplicacoes With 'PESQUTILIZADORES'","O")
		.adicionaOpcao("opcoes","Op��es", myPath + "\imagens\icons\opcoes_w.png","", "O")
		.adicionaOpcao("actualizar", "Pesquisar", myPath + "\imagens\icons\lupa_w.png" , "uf_pesqutilizadores_actPesquisa with .F.", "A")
		.adicionaOpcao("novo","Novo",myPath + "\imagens\icons\mais_w.png","uf_pesqutilizadores_introducao", "N")
		.adicionaOpcao("subir", "Subir", myPath + "\imagens\icons\pg_up_b.png", "uf_gerais_MovePage with .t.,'pesqutilizadores.gridpesq','uCrsPesqUtilizadores'", "05")
		.adicionaOpcao("descer", "Descer", myPath + "\imagens\icons\pg_down_b.png", "uf_gerais_MovePage with .f.,'pesqutilizadores.gridpesq','uCrsPesqUtilizadores'", "24")
	ENDWITH 
	
*!*		WITH PESQUTILIZADORES.menu_aplicacoes
*!*			.adicionaOpcao("honorarios","Gest�o Honor�rios","","uf_HONORARIOS_chama")
*!*		ENDWITH 

	WITH PESQUTILIZADORES.menu_opcoes
		.adicionaOpcao("grupos","Grupos de Utilizadores","","uf_gruposut_chama with ''")
	ENDWITH
			
	
	PESQUTILIZADORES.refresh
	
ENDFUNC


**
FUNCTION uf_pesqutilizadores_limparDados
	PESQUTILIZADORES.nome.Value = ''
	PESQUTILIZADORES.no.Value = ''
	PESQUTILIZADORES.login.Value = ''
	PESQUTILIZADORES.grupo.Value = ''

	uf_pesqutilizadores_actPesquisa(.t.)
ENDFUNC 


**
FUNCTION uf_pesqutilizadores_escolhe
	** Ecra de utilizadores
	IF EMPTY(pesqutilizadores.origem)
		SELECT uCrsPesqUtilizadores
		uf_utilizadores_chama(uCrsPesqUtilizadores.userno)
		uf_pesqutilizadores_sair()
		RETURN .f.
	ENDIF

	DO CASE 
		CASE pesqutilizadores.origem == "SERIES"
			IF USED("ucrsSeriesServ")
				SELECT ucrsSeriesServ
				SELECT uCrsPesqUtilizadores
				REPLACE ucrsSeriesServ.nome WITH ALLTRIM(uCrsPesqUtilizadores.nome)
				REPLACE ucrsSeriesServ.no WITH uCrsPesqUtilizadores.userno
				REPLACE ucrsSeriesServ.estab WITH 0
			ENDIF

			SERIESSERV.REFRESH
		CASE pesqutilizadores.origem == "CONVENCOES"
		
			IF USED("uCrsConvencoesUs")
				SELECT uCrsConvencoesUs
				APPEND BLANK	
				SELECT uCrsPesqUtilizadores
				REPLACE uCrsConvencoesUs.nome WITH ALLTRIM(uCrsPesqUtilizadores.nome)
				REPLACE uCrsConvencoesUs.no WITH uCrsPesqUtilizadores.userno
			ENDIF
			
			
			CONVENCOES.PageFrame1.Page1.REFRESH
		OTHERWISE
			**
	ENDCASE 
	
	IF pesqutilizadores.origem != "CONVENCOES"
		uf_pesqutilizadores_sair()
	ENDIF 
ENDFUNC



**
FUNCTION uf_pesqutilizadores_introducao

	IF uf_gerais_getParameter_site("ADM0000000185", "BOOL", mySite) AND !uf_gerais_getParameter("ADM0000000353", "BOOL")
		uf_perguntalt_chama("N�o pode criar novos utilizadores sem ser na Base de Dados Central", "OK", "", 64)
		RETURN .f.	
	ENDIF
	
	uf_utilizadores_chama()
	uf_utilizadores_introducao()
	uf_pesqutilizadores_sair()

ENDFUNC 