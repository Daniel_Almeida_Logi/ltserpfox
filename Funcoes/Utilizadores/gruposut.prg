**
FUNCTION uf_gruposut_chama
	LPARAMETERS lcGrupo
	
	IF TYPE("lcGrupo") != "C"
		lcGrupo = ""
	ENDIF
	
	
	
	
	&& Cursor de grupo
	fecha("uCrsGruposUt")
	IF !(uf_gerais_actGrelha("","uCrsGruposUt","select * from b_ug (nolock) where nome = '"+ALLTRIM(lcGrupo)+"'"))
		uf_perguntalt_chama("OCORREU UM ERRO A CARREGAR OS DADOS DO GRUPO.","OK","",16)
		RETURN .f.
	ENDIF
	SELECT uCrsGruposUt
	
	IF !(uf_gerais_actGrelha(IIF(TYPE("gruposut")=="U","","gruposut.pageframe1.page2.grdUsers"),"uCrsUtilizadoresGrupo","select nome,username,iniciais,userno from b_us (nolock) where grupo = '"+ALLTRIM(lcGrupo)+"'"))
		uf_perguntalt_chama("OCORREU UM ERRO A CARREGAR OS UTILIZADORES DO GRUPO.","OK","",16)
		RETURN .f.
	ENDIF
	SELECT uCrsUtilizadoresGrupo

	
	
	&& cursor para todos os grupos, 20201016 JG
	fecha("uCrsGruposTodos")
	IF !(uf_gerais_actGrelha("","uCrsGruposTodos",[Select distinct  ugstamp, nome, descricao,extensaotelefone, email from b_ug (nolock)]))
		uf_perguntalt_chama("OCORREU UM ERRO A CARREGAR OS GRUPOS!","OK","",16)
		RETURN .f.
	ENDIF
	SELECT uCrsGruposTodos



	&& Controla Abertura do Painel
	if type("gruposut")=="U"
		DO FORM GRUPOSUT
	ELSE
		GRUPOSUT.show
	ENDIF
	
	&&menu
	IF TYPE("gruposut.menu1.pesquisar") == "U"
		WITH gruposut.menu1
			.adicionaopcao("pesquisar","Pesquisar",myPath + "\imagens\icons\lupa_w.png","uf_gruposut_Pesquisar", "P")
			.adicionaOpcao("novo","Novo",myPath + "\imagens\icons\mais_w.png","uf_gruposut_novo", "N")
			.adicionaOpcao("editar","Editar",myPath + "\imagens\icons\lapis_w.png","uf_gruposut_Editar", "E")
			.adicionaOpcao("eliminar","Eliminar",myPath + "\imagens\icons\cruz_w.png","uf_gruposut_Elimina", "X")
		ENDWITH
	ENDIF
	
	uf_gruposut_controlaObj()
ENDFUNC 


**
FUNCTION uf_gruposut_gravar
	&&valida��es
	IF EMPTY(ALLTRIM(uCrsGruposUt.nome))
		uf_perguntalt_chama("O campo nome � de preenchimento obrigat�rio.","OK","",64)
		RETURN .f.
	ENDIF 
	
	LOCAL lcSQL, lcStamp
	IF myUGIntroducao
		lcStamp = uf_gerais_stamp()
		lcSQL=''
		TEXT TO lcSQL TEXTMERGE NOSHOW
			INSERT INTO b_ug (
				ugstamp,nome,descricao,
				ousrinis,ousrdata,ousrhora,
				usrinis,usrdata,usrhora,
				extensaotelefone, email, loja
			)
			values(
				'<<ALLTRIM(lcStamp)>>','<<ALLTRIM(uCrsGruposUt.nome)>>','<<ALLTRIM(uCrsGruposUt.descricao)>>',
				'<<m_chinis>>', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108), 
				'<<m_chinis>>', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108)
				,'<<ALLTRIM(uCrsGruposUt.extensaotelefone)>>','<<ALLTRIM(uCrsGruposUt.email)>>','<<mysite>>'
			)
		ENDTEXT 
		*_CLIPTEXT = lcSQL 
		IF !uf_gerais_actGrelha("","",lcSQL)
			uf_perguntalt_chama("Ocorreu um erro a introduzir o Grupo. Por favor contacte o suporte.","OK","",16)
		ENDIF 
	ELSE
		lcSQL = ''
		TEXT TO lcSQL TEXTMERGE NOSHOW
			update b_ug 
			set
				nome = '<<ALLTRIM(uCrsGruposUt.nome)>>',
				descricao = '<<ALLTRIM(uCrsGruposUt.descricao)>>',
				usrinis = '<<m_chinis>>',
				usrdata = convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102),
				usrhora = convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108),
				extensaotelefone = '<<ALLTRIM(uCrsGruposUt.extensaotelefone)>>',
				email = '<<ALLTRIM(uCrsGruposUt.email)>>',
				loja = '<<mysite>>'
				
			where ugstamp = '<<ALLTRIM(uCrsGruposUt.ugstamp)>>'
		ENDTEXT 
		
		IF !uf_gerais_actGrelha("","",lcSQL)
			uf_perguntalt_chama("Ocorreu um erro a actualizar o Grupo. Por favor contacte o suporte.","OK","",16)
		ENDIF 
	ENDIF
	
	myUGIntroducao = .f. 
	myUGAlteracao = .f.
	uf_gruposut_chama(ALLTRIM(uCrsGruposUt.nome))
ENDFUNC 


**
FUNCTION uf_gruposut_sair
	
	* Verifica se o Documento esta em Modo de Edicao ou Introducao
	If myUGIntroducao OR myUGAlteracao
		IF uf_perguntalt_chama("Quer cancelar as altera��es efectuadas?", "SIM", "N�O", 64)
			STORE .f. TO myUGIntroducao, myUGAlteracao
			
			SELECT uCrsGruposUt
			uf_gruposut_chama(ALLTRIM(uCrsGruposUt.nome))
		Endif
	Else
		&& fecha cursores
		Fecha("uCrsGruposUt")
		fecha("uCrsUtilizadoresGrupo")
		fecha("uCrsGruposTodos")
		
					
		gruposut.hide
		gruposut.Release

		RELEASE gruposut
	Endif
ENDFUNC


**
FUNCTION uf_gruposut_Pesquisar
	&& Cursor para comboBox Tratamento
	IF !(uf_gerais_actGrelha("","uCrsPesqGrupo",[Select distinct nome from b_ug (nolock)]))
		uf_perguntalt_chama("OCORREU UM ERRO A PESQUISAR OS GRUPOS!","OK","",16)
		RETURN .f.
	ELSE
		PUBLIC pValorPesqGr
		pValorPesqGr = ''
		uf_valorescombo_chama("pValorPesqGr", 0, "uCrsPesqGrupo", 2, "nome", "nome")	
		fecha("uCrsPesqGrupo")
		IF !EMPTY(ALLTRIM(pValorPesqGr))
			uf_gruposut_chama(ALLTRIM(pValorPesqGr))
		ENDIF
	ENDIF
ENDFUNC 


**
FUNCTION uf_gruposut_Elimina
	&&valida��es
	IF RECCOUNT("uCrsUtilizadoresGrupo") > 0
		uf_perguntalt_chama("Existem utilizadores associados ao grupo. Para eliminar o grupo altere o grupo dos respectivos utilizadores.", "OK", "", 64)
		RETURN .f.
	ELSE
		IF uf_perguntalt_chama("Tem a certeza que pretende eliminar o Grupo?","Sim","N�o")
			LOCAL lcSQL
			lcSQL=''
			TEXT TO lcSQL TEXTMERGE noshow
				DELETE FROM b_ug WHERE ugstamp='<<ALLTRIM(uCrsGruposUt.ugstamp)>>'
			ENDTEXT 
			IF uf_gerais_actGrelha("","",lcSQL)
				uf_perguntalt_chama("Grupo eliminado com sucesso.", "OK", "", 64)
				uf_gerais_actGrelha("","uCrsTempGr","select top 1 nome from b_ug order by nome")
				uf_gruposut_chama(ALLTRIM(uCrsTempGr.nome))
				fecha("uCrsTempGr")
			ENDIF 
		ENDIF
	ENDIF 
ENDFUNC 


**
FUNCTION uf_gruposut_Editar
	IF myUGIntroducao == .t. OR myUGAlteracao == .t.
		uf_perguntalt_chama("O Ecr� de Gest�o de Grupos est� em modo de Introdu��o/Edi��o. Deve cancelar ou gravar para proceder com esta ac��o.", "OK", "", 64)
		RETURN .f.
	ENDIF
	
	IF empty(ALLTRIM(uCrsGruposUt.ugstamp))
		RETURN .f.
	Endif
	
	gruposut.lbl1.click
	
	myUGAlteracao = .t.
	gruposut.Nome.setfocus
	
	uf_gruposut_controlaObj()
	gruposut.refresh
ENDFUNC


**
FUNCTION uf_gruposut_novo
	IF myUGIntroducao == .t. OR myUGAlteracao == .t.
		uf_perguntalt_chama("O Ecr� de Gest�o de Grupos est� em modo de Introdu��o/Edi��o. Deve cancelar ou gravar para proceder com esta ac��o.", "OK", "", 64)
		RETURN .f.
	ENDIF

	myUGIntroducao = .t.
	
	gruposut.lbl1.click
	
	**Limpa Cursores
	IF USED("uCrsGruposUt")
		SELECT uCrsGruposUt
		DELETE ALL
		SELECT uCrsGruposUt
		APPEND BLANK
	ENDIF 

	gruposut.Nome.setfocus
	uf_gruposut_controlaObj()
	gruposut.refresh
ENDFUNC


**
FUNCTION uf_gruposut_controlaObj
	
	IF !myUGIntroducao AND !myUGAlteracao && ecra em modo de consulta
		
		**
		WITH gruposut
			.caption			= uf_gerais_aplicaLanguageMsg("Grupos de Utilizadores")
			.Nome.readonly	= .t.
		ENDWITH

		gruposut.pageFrame1.enabled = .f.
		
		gruposut.menu1.estado("pesquisar, novo, editar, eliminar", "SHOW", "Gravar", .f., "Sair", .t.)
		
	&&	gruposut.line3.visible = .t.
		gruposut.lbl2.visible = .t.
	ELSE
		**
		WITH gruposut
			IF myUGIntroducao == .t.
				.caption = uf_gerais_aplicaLanguageMsg("Grupos de Utilizadores - Introdu��o")
			ELSE
				.caption = uf_gerais_aplicaLanguageMsg("Grupos de Utilizadores - Altera��o")
			ENDIF
			.Nome.readonly	= .f.
		ENDWITH

		gruposut.pageFrame1.enabled = .t.
		
		gruposut.menu1.estado("pesquisar, novo, editar, eliminar", "HIDE", "Gravar", .t., "Sair", .t.)
		
		&&gruposut.line3.visible = .f.
		gruposut.lbl2.visible = .f.
	ENDIF

ENDFUNC