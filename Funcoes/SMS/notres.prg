**
FUNCTION uf_NotRes_chama
	LOCAL lcSQL

	**
	TEXT TO lcSQL TEXTMERGE NOSHOW
		Select 
			TOP 1 *
			,left(texto,250) m
			,SUBSTRING(texto,251,250) m1 
		from 
			b_res_notificacoes (nolock)
	ENDTEXT
	IF !uf_gerais_actGrelha("","ucrsResNotificacoes",lcSQL)
		uf_perguntalt_chama("N�O FOI POSSIVEL DETERMINAR OS DADOS EXISTENTES PARA AS NOTIFICA��ES. POR FAVOR CONTACTE O SUPORTE","","",16)
		RETURN .f.
	ENDIF	
	
	**
	IF RECCOUNT("ucrsResNotificacoes")==0
		SELECT ucrsResNotificacoes
		APPEND BLANK
		Replace ucrsResNotificacoes.texto		WITH ""
		Replace ucrsResNotificacoes.activo		WITH .f.
		Replace ucrsResNotificacoes.m			WITH ""
		Replace ucrsResNotificacoes.m1			WITH ""
	ENDIF
	
	**
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_res_NotificacoesTextoAutomatico
	ENDTEXT
	IF !uf_gerais_actGrelha("","ucrsResTextoAutomatico",lcSQL)
		uf_perguntalt_chama("N�O FOI POSSIVEL DETERIMNAR OS DADOS EXISTENTES PARA AS NOTIFICA��ES. POR FAVOR CONTACTE O SUPORTE","OK","",16)
		RETURN .f.
	ENDIF	
	
	DO FORM NOTRES
		
	&&Centra
	NOTRES.autocenter = .t.
	
	&&menu
	NOTRES.menu1.estado("","","Gravar",.t.)
	
	&& Actualiza texto da mensagem
	NOTRES.txtObservacoes.value = ucrsResNotificacoes.m + ucrsResNotificacoes.m1
	NOTRES.txtObservacoes.value = ALLTRIM(NOTRES.txtObservacoes.value)
	
	uf_NOTRES_ContaCaracteres()
ENDFUNC 


**
FUNCTION uf_NOTRES_gravar
	LOCAL lcSQL
	
	uf_gerais_actGrelha("","","BEGIN TRANSACTION")	
	SELECT ucrsResNotificacoes
	
	LOCAL lcStamp
	lcStamp = uf_gerais_stamp()
	
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		DELETE FROM b_res_notificacoes
		INSERT INTO b_res_notificacoes 
		values(
			'<<ALLTRIM(lcStamp)>>'
			,'<<ucrsResNotificacoes.texto>>'
			,'<<m_chinis>>'
			,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
			,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8)
			,0
			,<<IIF(ucrsResNotificacoes.activo,1,0)>>) 
	ENDTEXT
	IF !uf_gerais_actGrelha("","",lcSQL)
		uf_gerais_actGrelha("","",[ROLLBACK])
		uf_perguntalt_chama("N�O FOI POSSIVEL ACTUALIZAR O REGISTO DE NOTIFICA��ES!","OK","",64)	
	ELSE
		uf_gerais_actGrelha("","",[COMMIT TRANSACTION])
		uf_perguntalt_chama("REGISTO GRAVADO COM SUCESSO!","OK","",64)
	ENDIF

ENDFUNC


**
FUNCTION uf_NOTRES_insereTexto
	SELECT ucrsResTextoAutomatico
	
	IF !EMPTY(ALLTRIM(NOTRES.tautomatico.Value))
		select ucrsResTextoAutomatico
		LOCATE for ALLTRIM(ucrsResTextoAutomatico.desc) == ALLTRIM(NOTRES.tautomatico.Value)
		
		SELECT ucrsResNotificacoes
		REPLACE ucrsResNotificacoes.texto WITH ALLTRIM(ucrsResNotificacoes.texto) + ucrsResTextoAutomatico.cod

		NOTRES.txtObservacoes.refresh
	ENDIF	
ENDFUNC


**
FUNCTION uf_NOTRES_sair
	NOTRES.hide
	NOTRES.release
ENDFUNC


**
FUNCTION uf_NOTRES_ContaCaracteres
	LOCAL lcCont
	STORE 0 TO lcCont
	
	lcCont = LEN(NOTRES.txtObservacoes.value)
	DO CASE 
		CASE lcCont <= 160
			NOTRES.lblCont.caption = STR(lcCont) + ' / 160 [ 1 SMS ]'
			RETURN
		CASE lcCont <= 306
			NOTRES.lblCont.caption = STR(lcCont) + ' / 306 [ 2 SMS ]'
			RETURN
		CASE lcCont <= 459
			NOTRES.lblCont.caption = STR(lcCont) + ' / 459 [ 3 SMS ]'
			RETURN 
		OTHERWISE 
			*****
	ENDCASE 
ENDFUNC