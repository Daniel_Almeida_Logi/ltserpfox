**
FUNCTION uf_pesquisasms_Chama
	
	TEXT TO lcSQL TEXTMERGE noshow
		SET fmtonly on
		exec up_sms_pesquisa 0,'','','','',0, '<<ALLTRIM(mySite)>>'
		SET fmtonly off
	ENDTEXT 	
	IF !(uf_gerais_actGrelha("","uCrsPesqSMS",lcSQL))
		uf_perguntalt_chama("OCORREU UMA ANOMALIA NA PESQUISA DE CAMPANHAS.","OK","",16)
		RETURN .f.
	ENDIF
	
	IF TYPE("pesquisaSMS")=="U"
		DO FORM pesquisasms
	ELSE
		pesquisasms.show
	ENDIF
	
	&&menu
	IF TYPE("pesquisasms.menu1.actualizar") == "U"
		pesquisasms.menu1.adicionaOpcao("actualizar", "Actualizar", myPath + "\imagens\icons\actualizar_w.png", "uf_pesquisaSMS_pesquisar","A")
		pesquisasms.menu1.adicionaOpcao("tecladoVirtual","Teclado",myPath + "\imagens\icons\teclado_w.png","uf_pesquisasms_tecladoVirtual","T")
		pesquisasms.menu1.adicionaOpcao("subir","Subir",myPath + "\imagens\icons\ponta_seta_up.png","uf_gerais_MovePage with .t., 'pesquisasms.grdpesq','uCrsPesqSMS'","05")
		pesquisasms.menu1.adicionaOpcao("descer","Descer",myPath + "\imagens\icons\ponta_seta_down.png","uf_gerais_MovePage with .f., 'pesquisasms.grdpesq','uCrsPesqSMS'","24")
	ENDIF
	
	PESQUISASMS.ref.setfocus
ENDFUNC


**
FUNCTION uf_pesquisasms_tecladoVirtual
	pesquisasms.tecladoVirtual1.show("pesquisasms.grdPesq", 161, "pesquisasms.shape1", 161)
ENDFUNC



** Fun��o que Pesquisa na SMS	 	
FUNCTION uf_pesquisaSMS_pesquisar
	LOCAL lcSQL,lcTop,lcRef,lcDesign,lcDe,lcA, lcEmitido
	STORE '' TO lcSQL
	STORE 0 TO lcEmitido
	
	lcTop 		= INT(VAL(pesquisasms.topo.value))
	lcRef 		= ALLTRIM(pesquisasms.ref.value)
	lcDesign 	= ALLTRIM(pesquisasms.design.value)
	lcDe		= IIF(EMPTY(ALLTRIM(pesquisasms.de.value)), '19000101', uf_gerais_getdate(pesquisasms.de.value,"SQL")) 
	lcA			= IIF(EMPTY(ALLTRIM(pesquisasms.a.value)), '30000101', uf_gerais_getdate(pesquisasms.a.value,"SQL"))
	lcEmitido   = IIF(pesquisasms.chkEmitidas.tag=="true",1,0)
	
	lcSQL = ''
	TEXT TO lcSQL TEXTMERGE noshow
		exec up_sms_pesquisa <<lcTop>> ,'<<lcRef>>','<<lcDesign>>','<<lcDe>>','<<lcA>>',<<lcEmitido>>, '<<ALLTRIM(mySite)>>'
	ENDTEXT 	

	IF !(uf_gerais_actGrelha("pesquisasms.grdpesq","uCrsPesqSMS",lcSQL))
		uf_perguntalt_chama("OCORREU UM ERRO NA PESQUISA DE CAMPANHAS.","OK","",16)
		RETURN .f.
	ENDIF
	***

	pesquisasms.lblRegistos.caption = astr(RECCOUNT("uCrsPesqSMS")) + ' Registos'
ENDFUNC 



** Fechar Ecra de Pesquisa		
FUNCTION uf_PesquisaSMS_sair
	pesquisasms.hide
	pesquisasms.release
ENDFUNC