FUNCTION uf_NotAni_chama
	LOCAL lcSQL

	**
	TEXT TO lcSQL TEXTMERGE NOSHOW
		Select TOP 1 *,left(texto,250) m,SUBSTRING(texto,251,250) m1 from b_sms_aniversario (nolock)
	ENDTEXT
	IF !uf_gerais_actGrelha("","ucrsAniNotificacoes",lcSQL)
		uf_perguntalt_chama("N�O FOI POSSIVEL DETERMINAR OS DADOS EXISTENTES PARA AS NOTIFICA��ES. POR FAVOR CONTACTE O SUPORTE","OK","",16)
		RETURN .f.
	ENDIF	
	
	**
	IF RECCOUNT("ucrsAniNotificacoes") == 0
		SELECT ucrsAniNotificacoes
		APPEND BLANK
		Replace ucrsAniNotificacoes.texto		WITH ""
		Replace ucrsAniNotificacoes.activo		WITH .f.
		**replace ucrsAniNotificacoes.ultenvio	WITH "1900.01.01"
		Replace ucrsAniNotificacoes.m			WITH ""
		Replace ucrsAniNotificacoes.m1			WITH ""
	ENDIF
	
	**
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_sms_AniTextoAutomatico
	ENDTEXT
	IF !uf_gerais_actGrelha("","ucrsAniTextoAutomatico",lcSQL)
		uf_perguntalt_chama("N�O FOI POSSIVEL DETERIMNAR OS DADOS EXISTENTES PARA AS NOTIFICA��ES. POR FAVOR CONTACTE O SUPORTE","OK","",16)
		RETURN .f.
	ENDIF	
	
	DO FORM NOTANI
		
	&&Centra
	NOTANI.autocenter = .t.
	
	&&menu
	NOTANI.menu1.estado("","","Gravar",.t.)
	
	&& Actualiza texto da mensagem
	NOTANI.txtObservacoes.value = ucrsAniNotificacoes.m + ucrsAniNotificacoes.m1
	NOTANI.txtObservacoes.value = ALLTRIM(NOTANI.txtObservacoes.value)
	
	uf_NOTANI_ContaCaracteres()
ENDFUNC 


**
FUNCTION uf_NOTANI_gravar
	LOCAL lcSQL
	
	uf_gerais_actGrelha("","","BEGIN TRANSACTION")
	SELECT ucrsAniNotificacoes
	
	LOCAL lcStamp
	lcStamp = uf_gerais_stamp()
	
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		DELETE FROM b_sms_aniversario
		INSERT INTO b_sms_aniversario 
		values(
			'<<ALLTRIM(lcStamp)>>'
			,'<<ucrsAniNotificacoes.texto>>'
			,'<<m_chinis>>'
			,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
			,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8)
			,0
			,<<IIF(ucrsAniNotificacoes.activo,1,0)>>
			,'<<uf_gerais_getDate(ucrsAniNotificacoes.ultenvio,"SQL")>>'
			,'<<ucrsAniNotificacoes.horaenvio>>')
	ENDTEXT
	IF !uf_gerais_actGrelha("","",lcSQL)
		uf_gerais_actGrelha("","",[ROLLBACK])
		uf_perguntalt_chama("N�O FOI POSSIVEL ACTUALIZAR O REGISTO DE NOTIFICA��ES!","OK","",64)
	ELSE
		uf_gerais_actGrelha("","",[COMMIT TRANSACTION])
		uf_perguntalt_chama("REGISTO GRAVADO COM SUCESSO!","OK","",64)
	ENDIF
ENDFUNC


**
FUNCTION uf_NOTANI_insereTexto
	SELECT ucrsAniTextoAutomatico
	
	IF !EMPTY(ALLTRIM(NOTANI.tautomatico.Value))
		
		select ucrsAniTextoAutomatico
		LOCATE for ALLTRIM(ucrsAniTextoAutomatico.desc) == ALLTRIM(NOTANI.tautomatico.Value)
		
		SELECT ucrsAniNotificacoes
		REPLACE ucrsAniNotificacoes.texto WITH ALLTRIM(ucrsAniNotificacoes.texto) + ucrsAniTextoAutomatico.cod

		NOTANI.txtObservacoes.refresh
	ENDIF	
ENDFUNC


**
FUNCTION uf_NOTANI_sair
	NOTANI.hide
	NOTANI.release
ENDFUNC


**
FUNCTION uf_NOTANI_ContaCaracteres
	LOCAL lcCont
	STORE 0 TO lcCont
	
	lcCont = LEN(NOTANI.txtObservacoes.value)
	DO CASE 
		CASE lcCont <= 160
			NOTANI.lblCont.caption = STR(lcCont) + ' / 160 [ 1 SMS ]'
			RETURN
		CASE lcCont <= 306
			NOTANI.lblCont.caption = STR(lcCont) + ' / 306 [ 2 SMS ]'
			RETURN
		CASE lcCont <= 459
			NOTANI.lblCont.caption = STR(lcCont) + ' / 459 [ 3 SMS ]'
			RETURN 
		OTHERWISE 
			*****
	ENDCASE 
ENDFUNC