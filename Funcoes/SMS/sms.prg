DO pesquisaSMS
DO smsfact
DO listasms
DO notres
DO notani
DO notificacoesmarc

**
FUNCTION uf_SMS_chama
	LPARAMETERS lcRef
		
	IF EMPTY(ALLTRIM(astr(lcRef)))
		lcRef = ""
	ENDIF
	
	IF TYPE("PESQUTENTES") != "U"
		uf_pesqutentes_sair()
	ENDIF
	
	IF !mySMS
		uf_perguntalt_chama("PARA ADQUIRIR ESTE M�DULO POR FAVOR ENTRE EM CONTACTO COM O SUPORTE. PE�A J� A SUA DEMONSTRA��O.","OK","", 64)
		RETURN .f.
	ENDIF
	
	&& CONTROLA PERFIS DE ACESSO
	IF uf_gerais_validaPermUser(ch_userno,ch_grupo,'Alertas SMS')
		***
	ELSE
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ACEDER AO PAINEL DE COMUNICA��ES.","OK","",48)
		RETURN .f.
	ENDIF
	
	&& Cursor da SMS
	IF !(uf_gerais_actGrelha("","uCrsSMS","SELECT Top 1 *,left(mensagem,250) m,SUBSTRING(mensagem,251,250) m1 FROM B_sms (NOLOCK) WHERE ref='" + alltrim(astr(lcRef)) + "'"))
		uf_perguntalt_chama("OCORREU UM ERRO A CARREGAR OS DADOS DA COMUNICA��O!","OK","",16)
		RETURN .f.
	ENDIF

	
	SELECT uCrsSMS
	
	&& Cursor de Clientes da COMUNICA��O
	LOCAL lcSQL
	lcSQL=''
	TEXT TO lcSQL TEXTMERGE NOSHOW 
		SELECT 
			 b_utentes.nome
			,b_utentes.no
			,b_utentes.estab
			,b_utentes.tlmvl
			,B_smscl.relatorio
			,b_smscl.relatoriodatahora as relatoriodata
			,b_smscl.resposta
			,b_smscl.respostadatahora
			,b_utentes.utstamp
			,B_smscl.smsstamp 
			,b_utentes.email
		FROM 
			b_utentes (NOLOCK) 
			inner join B_smscl (nolock) on B_smscl.clstamp=b_utentes.utstamp 
		where 
			B_smscl.smsstamp='<<ALLTRIM(uCrsSMS.smsstamp)>>' 
		order by 
			ordem
	ENDTEXT 				
	IF !(uf_gerais_actGrelha(IIF(TYPE("CAMPANHAS_SMS")=="U","","CAMPANHAS_SMS.pageframe1.page1.grdCl"),"uCrsCLSMS",lcSQL))
		uf_perguntalt_chama("OCORREU UM ERRO A CARREGAR OS CLIENTES DA COMUNICA��O!","OK",16)
		RETURN .f.
	ENDIF
		 
	&& Cursores de Palavras-Chave
	IF !(uf_gerais_actGrelha("","uCrslstRef","SELECT valor FROM B_smspc (NOLOCK) where smsstamp='" + ALLTRIM(uCrsSMS.smsstamp) + "' and tipo = 'ref' order by ordem"))
		uf_perguntalt_chama("OCORREU UM ERRO A CARREGAR AS PALAVRAS-CHAVE!","OK","",16)
		RETURN .f.
	ENDIF
	
	IF !(uf_gerais_actGrelha("","uCrslstDesign","SELECT valor FROM B_smspc (NOLOCK) where smsstamp='" + ALLTRIM(uCrsSMS.smsstamp) + "' and tipo = 'design' order by ordem"))
		uf_perguntalt_chama("OCORREU UM ERRO A CARREGAR AS PALAVRAS-CHAVE!","OK","",16)
		RETURN .f.
	ENDIF
	
	IF !(uf_gerais_actGrelha("","uCrslstFam","SELECT valor FROM B_smspc (NOLOCK) where smsstamp='" + ALLTRIM(uCrsSMS.smsstamp) + "' and tipo = 'fam' order by ordem"))
		uf_perguntalt_chama("OCORREU UM ERRO A CARREGAR AS PALAVRAS-CHAVE!","OK","",16)
		RETURN .f.
	ENDIF
	
	IF !(uf_gerais_actGrelha("","uCrslstLab","SELECT valor FROM B_smspc (NOLOCK) where smsstamp='" + ALLTRIM(uCrsSMS.smsstamp) + "' and tipo = 'lab' order by ordem"))
		uf_perguntalt_chama("OCORREU UM ERRO A CARREGAR AS PALAVRAS-CHAVE!","OK","",16)
		RETURN .f.
	ENDIF
	
	IF !(uf_gerais_actGrelha("","uCrslstMarca","SELECT valor FROM B_smspc (NOLOCK) where smsstamp='" + ALLTRIM(uCrsSMS.smsstamp) + "' and tipo = 'marca' order by ordem"))
		uf_perguntalt_chama("OCORREU UM ERRO A CARREGAR AS PALAVRAS-CHAVE!","OK","",16)
		RETURN .f.
	ENDIF
		
	&& Controla Abertura do Painel			
	if type("CAMPANHAS_SMS")=="U"
	
		**Valida Licenciamento	
		IF (uf_gerais_addConnection('SMS') == .f.)
			RETURN .F.
		ENDIF
		
		DO FORM CAMPANHAS_SMS
	ELSE
		CAMPANHAS_SMS.show
	ENDIF
	
	
	&& menu
	IF TYPE("CAMPANHAS_SMS.menu1.pesquisar") == "U"
		CAMPANHAS_SMS.menu1.adicionaOpcao("opcoes", "Op��es", myPath + "\imagens\icons\opcoes_w.png", "", "O")
		CAMPANHAS_SMS.menu1.adicionaOpcao("pesquisar", "Pesquisar", myPath + "\imagens\icons\lupa_w.png", "uf_sms_pesquisar","P")
		CAMPANHAS_SMS.menu1.adicionaOpcao("actualizar", "Actualizar", myPath + "\imagens\icons\actualizar_w.png", "uf_sms_actualizar","A")
		CAMPANHAS_SMS.menu1.adicionaOpcao("novo", "Novo", myPath + "\imagens\icons\mais_w.png", "uf_SMS_novo","N")
		CAMPANHAS_SMS.menu1.adicionaOpcao("editar", "Editar", myPath + "\imagens\icons\lapis_w.png", "uf_SMS_editar","E")
		**CAMPANHAS_SMS.menu1.adicionaOpcao("apagar", "Apagar", myPath + "\imagens\icons\cruz_w.png", "uf_SMS_eliminar","X")
		CAMPANHAS_SMS.menu1.adicionaOpcao("anterior", "Anterior", myPath + "\imagens\icons\ponta_seta_left_w.png", "uf_SMS_ant","19")
		CAMPANHAS_SMS.menu1.adicionaOpcao("seguinte", "Seguinte", myPath + "\imagens\icons\ponta_seta_rigth_w.png", "uf_SMS_prox","04")
		CAMPANHAS_SMS.menu1.adicionaOpcao("verTlm", "Verificar Tlm", myPath + "\imagens\icons\tlm_w.png", "uf_SMS_verificaTlmvl","V")
		CAMPANHAS_SMS.menu1.adicionaOpcao("limparPalavras", "Limpar", myPath + "\imagens\icons\limpar_w.png", "uf_sms_limparPalavras","L")
		
		CAMPANHAS_SMS.menu_opcoes.adicionaOpcao("duplicar", "Duplicar Campanha", "", "uf_SMS_duplica")
		CAMPANHAS_SMS.menu_opcoes.adicionaOpcao("relResp", "Relat�rios e Respostas", "", "uf_SMS_ActualizaDadosClienteWS")
		CAMPANHAS_SMS.menu_opcoes.adicionaOpcao("aniversarios", "Anivers�rios", "", "uf_NotAni_chama")
		CAMPANHAS_SMS.menu_opcoes.adicionaOpcao("reservas", "Reservas", "", "uf_NotRes_chama")
		CAMPANHAS_SMS.menu_opcoes.adicionaOpcao("marcacoes", "Marca��es", "", "uf_notificacoesMarcChama")
		**CAMPANHAS_SMS.menu_opcoes.adicionaOpcao("contadores", "Contadores", "", "uf_sms_abrirContadores")
		CAMPANHAS_SMS.menu_opcoes.adicionaOpcao("analises", "An�lises", "", "uf_sms_analises")
		CAMPANHAS_SMS.menu_opcoes.adicionaOpcao("ultimo", "�ltimo", "", "uf_sms_ultimo")
		CAMPANHAS_SMS.menu_opcoes.adicionaOpcao("pesqchamadas", "Pesq. Chamadas", "", "uf_emails_callPesCall")
		CAMPANHAS_SMS.menu_opcoes.adicionaOpcao("eliminar", "Eliminar", "", "uf_SMS_eliminar")
		
	ENDIF
	CAMPANHAS_SMS.menu1.estado("verTlm,limparPalavras","HIDE")
		
	&& Navega para pageframe
	DO uf_sms_confEcra WITH 1	

	&& Actualiza Grelha Clientes
	CAMPANHAS_SMS.pageframe1.page1.lblRegistos.caption = astr(RECCOUNT("uCrsCLSMS")) + " Registos"
	
	&&Actualiza Listas de Palavras Chave
	campanhas_sms.pageframe1.page3.lstRef.rowsource=''
	campanhas_sms.pageframe1.page3.lstRef.rowsource='uCrslstRef'
	campanhas_sms.pageframe1.page3.lstDesign.rowsource=''
	campanhas_sms.pageframe1.page3.lstDesign.rowsource='uCrslstDesign'
	campanhas_sms.pageframe1.page3.lstFam.rowsource=''
	campanhas_sms.pageframe1.page3.lstFam.rowsource='uCrslstFam'
	campanhas_sms.pageframe1.page3.lstLab.rowsource=''
	campanhas_sms.pageframe1.page3.lstLab.rowsource='uCrslstLab'
	campanhas_sms.pageframe1.page3.lstMarca.rowsource=''
	campanhas_sms.pageframe1.page3.lstMarca.rowsource='uCrslstMarca'		
	
	&& Actualiza texto da mensagem
	campanhas_sms.pageframe1.page2.txtMensagem.value = uCrsSMS.m + uCrsSMS.m1
	
	&& Actualiza Ecra
	SELECT uCrsSMS
	
	CAMPANHAS_SMS.refresh
	CAMPANHAS_SMS.lbl1.click

	&& Actualiza informa��o de sms enviados - confirmar se � necess�rio 
	&& uf_ListaSMS_chama()
	
	** Menu do container abrir lotes
	CAMPANHAS_SMS.ctnContadores.menu1.estado("", "", "", .f., "Sair", .t.)
	CAMPANHAS_SMS.ctnContadores.menu1.funcaoSair = "uf_sms_ctnContadoresFechar"

ENDFUNC 


**
FUNCTION uf_sms_ctnContadoresFechar
	** Esconder o painel
	CAMPANHAS_SMS.ctnContadores.visible = .f.
		
	uf_sms_sombra(.f.)
ENDFUNC


**
FUNCTION uf_sms_abrirContadores
	
	** Colocar container visivel
	CAMPANHAS_SMS.ctnContadores.visible = .t.
	
	** Calcular Contadores
	TEXT TO lcSQL TEXTMERGE NOSHOW
		select 
			'enviados' = isnull(sum(total),0)
		from 
			b_sms_fact (nolock)
		where 
			username = '<<ALLTRIM(uf_gerais_getParameter("ADM0000000157","TEXT"))>>'
			and status in (0,1,2)
	ENDTEXT

	uf_gerais_actGrelha("","uCrsEnviados",lcSQL)
	SELECT uCrsEnviados
	CAMPANHAS_SMS.ctnContadores.txtEnviados.value = uCrsEnviados.enviados
	
	IF USED("uCrsEnviados")
		fecha("uCrsEnviados")
	ENDIF 
	
	WITH CAMPANHAS_SMS.ctnContadores
		IF .txtEnviados.value > .txtAdquiridos.value
			.txtEnviados.disabledBackColor = RGB(237,28,36)
			.txtEnviados.ForeColor = RGB(255,255,255)
			
			.txtAdquiridos.disabledBackColor = RGB(255,255,255)
			.txtAdquiridos.ForeColor = RGB(0,0,0)
		ELSE
			.txtEnviados.disabledBackColor = RGB(255,255,255)
			.txtEnviados.ForeColor = RGB(0,0,0)
			
			.txtAdquiridos.disabledBackColor = RGB(42,143,154)
			.txtAdquiridos.ForeColor = RGB(255,255,255)
		ENDIF 
		
	ENDWITH
	
	** Colocar sombra
	uf_sms_sombra(.t.)
ENDFUNC


**
FUNCTION uf_sms_sombra
	LPARAMETERS tcBool
	
	IF tcBool
		** remover foco da grelha
		CAMPANHAS_SMS.txtDesign.setfocus
		
		** Colocar sombra
		CAMPANHAS_SMS.sombra.width	= CAMPANHAS_SMS.width
		CAMPANHAS_SMS.sombra.height	= CAMPANHAS_SMS.height
		CAMPANHAS_SMS.sombra.left 	= 0
		CAMPANHAS_SMS.sombra.top 	= 0
		
		CAMPANHAS_SMS.sombra.visible 	= .t.
	ELSE
		** remover sombra
		CAMPANHAS_SMS.sombra.visible = .f.
	ENDIF
ENDFUNC


**
FUNCTION uf_sms_limparPalavras
	If uf_perguntalt_chama("TEM A CERTEZA QUE PRETENDE ELIMINAR TODAS AS PALAVRAS-CHAVE DA COMUNICA��O?","Sim","N�o")
		SELECT uCrslstRef
		DELETE ALL
		SELECT uCrslstDesign
		DELETE ALL
		SELECT uCrslstFam
		DELETE ALL
		SELECT uCrslstLab
		DELETE ALL
		SELECT uCrslstMarca
		DELETE ALL
		
		CAMPANHAS_SMS.pageframe1.page3.lstRef.rowsource=''
		CAMPANHAS_SMS.pageframe1.page3.lstRef.rowsource='uCrslstRef'
		CAMPANHAS_SMS.pageframe1.page3.lstDesign.rowsource=''
		CAMPANHAS_SMS.pageframe1.page3.lstDesign.rowsource='uCrslstDesign'
		CAMPANHAS_SMS.pageframe1.page3.lstFam.rowsource=''
		CAMPANHAS_SMS.pageframe1.page3.lstFam.rowsource='uCrslstFam'
		CAMPANHAS_SMS.pageframe1.page3.lstLab.rowsource=''
		CAMPANHAS_SMS.pageframe1.page3.lstLab.rowsource='uCrslstLab'
		CAMPANHAS_SMS.pageframe1.page3.lstMarca.rowsource=''
		CAMPANHAS_SMS.pageframe1.page3.lstMarca.rowsource='uCrslstMarca'
	ENDIF 
ENDFUNC 


**
FUNCTION uf_sms_analises
	
	&& Actualiza informa��o de sms enviados - feito diretamente qd s�o executodos os relat�rios
	&& uf_ListaSMS_chama()
	
	uf_analises_chama()
	SELECT uCrsAnalisesGrupo
	LOCATE FOR ALLTRIM(uCrsAnalisesGrupo.grupo) == "Clientes"
	Analises.containerGrupo.gridGrupo.setfocus
	SELECT uCrsAnalisesSubGrupo
	LOCATE FOR ALLTRIM(uCrsAnalisesSubGrupo.SubGrupo) == "Campanhas e Alertas"
	Analises.containerGrupo.gridSubGrupo.setfocus
ENDFUNC 


**
FUNCTION uf_SMS_verificaTlmvl

	IF uf_perguntalt_chama("Vai eliminar da lista todos os n� de telem�vel que n�o comecem por 9, +351 ou 00351." + CHR(13) + "Tem a certeza que pretende continuar?","Sim","N�o")
		&& Prepara Regua
		regua(0,3,"A validar telem�veis vazios, n� com menos/mais de 9 d�gitos e comece n�o comecem por 9, +351 ou 00351...",.F.)
		&& incrementa regua
		regua[0,1,"A validar telem�veis vazios, n� com menos/mais de 9 d�gitos e comece n�o comecem por 9, +351 ou 00351...",.F.]
		&&vazios, 9 digitos e comece por 9
		SELECT uCrsClSMS
		SCAN 		
			LOCAL lcTlmvl
		    lcTlmvl = ALLTRIM(uCrsClSMS.tlmvl)

		    IF EMPTY(lcTlmvl) OR ;
		        (LEFT(lcTlmvl, 1) != "9" AND LEFT(lcTlmvl, 5) != "00351" AND LEFT(lcTlmvl, 4) != "+351")
		        DELETE
		    ENDIF
		ENDSCAN
		
		SELECT uCrsClSMS
		SCAN 
			LOCAL lcTlmvl
		    lcTlmvl = ALLTRIM(uCrsClSMS.tlmvl)		
			REPLACE uCrsClSMS.tlmvl WITH uf_sms_NormalizaNrTlmvl(lcTlmvl)
		ENDSCAN
			
		&& incrementa regua
		regua[0,2,"A validar telem�veis repetidos...",.F.]	
		&&repetidos
		DO WHILE .t.
			select * from uCrsClSMS where tlmvl in (SELECT tlmvl FROM uCrsClSMS GROUP BY tlmvl HAVING (COUNT(tlmvl) > 1) ) order by uCrsClSMS.tlmvl INTO CURSOR ucrsTlmvlRepetidos READWRITE
			SELECT ucrsTlmvlRepetidos
			IF RECCOUNT("ucrsTlmvlRepetidos") > 0
				SELECT ucrsTlmvlRepetidos
				GO TOP
				SELECT uCrsClSMS
				LOCATE FOR ALLTRIM(uCrsClSMS.utstamp) == ALLTRIM(ucrsTlmvlRepetidos.utstamp)
				DELETE
			ELSE
				EXIT
			ENDIF
		ENDDO
		
		&& incrementa regua
		regua[0,3,"A actualzar grelha de clientes...",.F.]	
		SELECT uCrsClSMS
		GO TOP
		campanhas_sms.pageframe1.page1.refresh
		
		&& FECHA REGUA
		regua(2)
	ENDIF 
ENDFUNC 

FUNCTION uf_sms_NormalizaNrTlmvl
	LPARAMETERS phone
    LOCAL normPhone
    normPhone = phone
    IF LEFT(normPhone, 5) == "00351"
        normPhone = SUBSTR(normPhone, 6)
    ENDIF
    IF LEFT(normPhone, 4) == "+351"
        normPhone = SUBSTR(normPhone, 5)
    ENDIF
    RETURN normPhone
ENDFUNC 

**
FUNCTION uf_sms_editar
	IF CAMPANHAS_SMS.pageframe1.ActivePage == 4
		CAMPANHAS_SMS.lbl1.Click
	ENDIF 
	uf_SMS_alterar()
ENDFUNC 


**
FUNCTION uf_sms_ultimo
	LOCAL lcSQL
	lcSQL=''
	TEXT TO lcSQL TEXTMERGE noshow
		SELECT 
			TOP 1 b_sms.ref
		from 
			b_ultReg (nolock)
			inner join b_sms (nolock) on b_ultReg.stamp=b_sms.smsstamp
		where 
			b_ultReg.tabela='b_sms'
	ENDTEXT 
	IF uf_gerais_actGrelha("","uCrsUltCampSMS",lcSQL)
		IF RECCOUNT()>0		
			uf_sms_chama(uCrsUltCampSMS.ref)
			CAMPANHAS_SMS.lbl1.Click
		ENDIF
		fecha("uCrsUltCampSMS")
	ENDIF
ENDFUNC 


**
FUNCTION uf_sms_actualizar
	IF USED("uCrsSMS")
		SELECT uCrsSMS
		uf_sms_chama(ucrsSMS.ref)
	ELSE
		uf_sms_chama('')
	ENDIF
ENDFUNC


**
FUNCTION uf_sms_pesquisar
	PUBLIC myIntVarPesqSMS
	STORE 0 TO myIntVarPesqSMS

	uf_pesquisaSMS_chama()
ENDFUNC  


** Configura Ecra SMS		
FUNCTION uf_sms_confEcra
	LPARAMETERS tcBool
	
	CAMPANHAS_SMS.pageframe1.tabs		=	.f.
	
	IF tcBool == 1		
		CAMPANHAS_SMS.lbl1.click
		
		&&Dados Campanha
		CAMPANHAS_SMS.txtDesign.backcolor		=	RGB(255,255,255)
		CAMPANHAS_SMS.txtDesign.readonly		=	.t.
		CAMPANHAS_SMS.txtRef.readonly			=	.t.
		CAMPANHAS_SMS.txtde.enabled				=	.f.
		CAMPANHAS_SMS.txta.enabled				=	.f.
		CAMPANHAS_SMS.txtTipoEnvio.disabledBackColor	=	RGB(255,255,255)
		CAMPANHAS_SMS.txtTipoEnvio.enabled		=	.f.
		CAMPANHAS_SMS.txtTipoMinuta.enabled		=	.f.
		&&Dados PageFrame
		CAMPANHAS_SMS.pageframe1.enabled			=	.f.
		CAMPANHAS_SMS.pageframe1.page2.txtmensagem.readonly = .t.
						
		&&Menu
		CAMPANHAS_SMS.menu1.estado("opcoes,pesquisar,novo,editar,actualizar,anterior,seguinte","SHOW","Gravar",.f.)
		
	ELSE	
		CAMPANHAS_SMS.txtDesign.backcolor 	= RGB(230,242,255)
		CAMPANHAS_SMS.txtDesign.readonly	= .f.
		CAMPANHAS_SMS.txtde.enabled			= .t.
		CAMPANHAS_SMS.txta.enabled			= .t.
		CAMPANHAS_SMS.txtTipoEnvio.disabledBackColor 	= RGB(230,242,255)
		CAMPANHAS_SMS.txtTipoEnvio.enabled		=	.t.
		CAMPANHAS_SMS.txtTipoMinuta.enabled		=	.t.
		CAMPANHAS_SMS.pageframe1.enabled	= .T.
	
		CAMPANHAS_SMS.pageframe1.page2.txtmensagem.readonly	= .F.
			
		&&Menu
		CAMPANHAS_SMS.menu1.estado("opcoes,pesquisar,novo,editar,actualizar,anterior,seguinte","HIDE","Gravar",.t.)
		
		
		IF CAMPANHAS_SMS.pageframe1.ActivePage == 1
			CAMPANHAS_SMS.lbl1.Click
		ENDIF 
		IF CAMPANHAS_SMS.pageframe1.ActivePage == 3
			CAMPANHAS_SMS.lbl3.Click
		ENDIF 
	ENDIF 		
ENDFUNC


**	NOVO SMS		
FUNCTION uf_SMS_novo
	uf_SMS_chama("")
	SELECT uCrsSMS
	APPEND BLANK
		
	CAMPANHAS_SMS.Caption="COMUNICA��ES - Modo de Introdu��o"
	mySMSIntroducao = .t.
	mySMSAlteracao = .f.
	
	DO uf_sms_confEcra WITH 2
	CAMPANHAS_SMS.lbl1.click
					
	&& Valores por defeito
	DO uf_SMS_ValoresDefeito
	
	&& Actualiza ecra
	SELECT uCrsSMS
	CAMPANHAS_SMS.refresh
	CAMPANHAS_SMS.txtDesign.SetFocus
ENDFUNC


**	DUPLICA SMS		
FUNCTION uf_SMS_duplica
	&& Valida Cursor
	SELECT uCrsSMS
	IF EMPTY(uCrsSMS.smsstamp)
		RETURN .f.
	ENDIF
		
		
	CAMPANHAS_SMS.Caption="COMUNICA��ES - Modo de Introdu��o"
	mySMSIntroducao = .t.
	mySMSAlteracao = .f.
	
	DO uf_sms_confEcra WITH 2
	CAMPANHAS_SMS.lbl1.click
					
	&& Valores por defeito
		DO uf_SMS_ValoresDefeito
	
	&& Actualiza ecra
		SELECT uCrsSMS
		CAMPANHAS_SMS.refresh
ENDFUNC



**	ALTERAR SMS	
FUNCTION uf_SMS_alterar
	&& Valida Cursor
		SELECT uCrsSMS
		IF EMPTY(uCrsSMS.smsstamp)
			return
		ENDIF
	
	uf_gerais_actGrelha("","uCrstempUpCamp","Select emitido from b_sms (nolock) where smsstamp = '" + ALLTRIM(astr(uCrsSMS.smsstamp)) + "'")
	IF uCrstempUpCamp.emitido == .f.
		&& Configura ecra
		CAMPANHAS_SMS.Caption="COMUNICA��ES - Modo de Altera��o"	
		mySMSIntroducao = .f.
		mySMSAlteracao = .t.	
		DO uf_sms_confEcra WITH 2
	ELSE
		uf_perguntalt_chama("N�O PODE EDITAR COMUNICA��ES J� EMITIDAS.","OK","",48)	
	ENDIF 
	fecha("uCrstempUpCamp")
ENDFUNC


** INSERIR SMS			
FUNCTION uf_SMS_insere
	LOCAL lcSQL
	STORE '' TO lcSQL
	
	&&Validar n� da campanha
		If uf_gerais_actGrelha("","uCrsTempNo",[select isnull(cast(MAX(cast(ref as numeric(10,0))) as varchar(8)),'0000000') as no from B_sms (nolock)])
			If Reccount()>0
				IF EMPTY(uCrsTempNo.no)
					SELECT uCrsSMS
					replace uCrsSMS.no WITH '0000001'
				ELSE
					LOCAL lcRef
					STORE '' TO lcRef
					lcRef = astr(Val(alltrim(uCrsTempNo.no))+1)
					IF LEN(lcRef) < 7
						FOR i=1 TO 7-LEN(lcRef)
							lcRef = '0' + lcRef
						ENDFOR 
					ENDIF 
					SELECT uCrsSMS
					replace uCrsSMS.ref WITH lcRef
				ENDIF
			Endif
			Fecha("uCrsTempNo")
		ENDIF
		
		
	TEXT TO lcSQl TEXTMERGE NOSHOW
		INSERT INTO B_sms 
		(
			smsstamp
			,ref
			,designacao
			,CampValDe
			,CampValA
			,mensagem
			,emitido
			,ousrinis
			,ousrdata
			,ousrhora
			,usrinis
			,usrdata
			,usrhora 
			,tipoenvio
			,email_body
			,codminuta
			,site
		) 
		VALUES
		(
			'<<uCrsSMS.smsstamp>>'
			,'<<uCrsSMS.ref>>'
			,'<<ALLTRIM(uCrsSMS.designacao)>>'
			,'<<uf_gerais_getDate(uCrsSMS.CampValDe,"SQL")>>'
			,'<<uf_gerais_getDate(uCrsSMS.CampValA,"SQL")>>'
			,'<<alltrim(uCrsSMS.mensagem)>>'
			,0
			,'<<m_chinis>>'
			,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
			,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108)
			,'<<m_chinis>>'
			,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
			,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108)
			,'<<alltrim(uCrsSMS.tipoenvio)>>'
			, '<<uCrsSMS.email_body>>'
			, '<<uCrsSMS.codminuta>>'
			, '<<ALLTRIM(mySite)>>'
		)
	ENDTEXT

	IF !(uf_gerais_actGrelha("","",lcSQL))
		uf_perguntalt_chama("OCORREU UM ERRO A INSERIR A COMUNICA��O.","OK","",16)
		RETURN	.f.
	ELSE

		* Inserir Clientes da Campanha 
		LOCAL lcOrdem
		STORE 1 TO lcOrdem
		
		SELECT uCrsSMS 
		
		SELECT uCrsCLSMS
		IF RECCOUNT("uCrsCLSMS")>0
			GO TOP 
			SCAN 
				lcSQL=''
				TEXT TO lcSQL TEXTMERGE NOSHOW
					INSERT INTO B_smscl
					(
						smsstamp
						,clstamp
						,ordem
						,ousrinis
						,ousrdata
						,ousrhora
						,usrinis
						,usrdata
						,usrhora 
					) 
					VALUES
					( 
						'<<uCrsSMS.smsstamp>>','<<uCrsCLSMS.utstamp>>', <<lcOrdem>>,
						'<<m_chinis>>', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108), '<<m_chinis>>', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108)
					) 
				ENDTEXT
				IF !(uf_gerais_actGrelha("","",lcSQL))
					uf_perguntalt_chama("OCORREU UM ERRO A ADICIIONAR O CLIENTE. Cod:b_clsms","OK","",16)
					RETURN	.f.
				ENDIF
				lcOrdem = lcOrdem + 1
			ENDSCAN 
		ENDIF
		
		**************************
		* Inserir Palavras-Chave *
		**************************
		IF uf_sms_insertB_smspc("uCrslstRef", "ref") AND uf_sms_insertB_smspc("uCrslstDesign", "design") AND uf_sms_insertB_smspc("uCrslstFam", "fam") AND uf_sms_insertB_smspc("uCrslstLab", "lab") AND uf_sms_insertB_smspc("uCrslstMarca", "marca")
			&& inseriu palavras-chave
		ELSE
			RETURN .f.
		ENDIF 
		
	ENDIF
	
	RETURN	.t.	
endfunc


**
FUNCTION uf_sms_insertB_smspc
	LPARAMETERS lcCursor, lcTipo
	
	STORE 1 TO lcOrdem
		
	SELECT uCrsSMS 
	
	LOCAL lcStamp
	
	SELECT &lcCursor
	IF RECCOUNT(lcCursor)>0
		GO TOP 
		SCAN 
			lcStamp = uf_gerais_stamp()
			SELECT &lcCursor
			lcValor = &lcCursor..valor
			lcSQL=''
			TEXT TO lcSQL TEXTMERGE NOSHOW
				INSERT INTO B_smspc
				(
					pcstamp, smsstamp, tipo, valor, ordem,
					ousrinis ,ousrdata ,ousrhora ,usrinis ,usrdata ,usrhora 
				) 
				VALUES
				( 
					'<<lcStamp>>','<<uCrsSMS.smsstamp>>', '<<lcTipo>>', '<<lcValor>>', <<lcOrdem>>,
					'<<m_chinis>>', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108), '<<m_chinis>>', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108)
				) 
			ENDTEXT
			IF !(uf_gerais_actGrelha("","",lcSQL))
				uf_perguntalt_chama("OCORREU UM ERRO A ADICIONAR AS PALAVRAS-CHAVE. Cod:b_smspc","OK","",16)
				RETURN	.f.
			ENDIF
			lcOrdem = lcOrdem + 1
		ENDSCAN 
	ENDIF 
	
	RETURN .t.
endfunc

*************************************
*			UPDATE CAMPANHA			*
*************************************
FUNCTION uf_SMS_update
	LOCAL lcSQL
	STORE '' TO lcSQl
	TEXT TO lcSQL TEXTMERGE noshow
		UPDATE B_sms
		SET 
			designacao		= '<<uCrsSMS.designacao>>',
			CampValDe		= '<<uf_gerais_getDate(uCrsSMS.CampValDe,"SQL")>>', 
			CampValA		= '<<uf_gerais_getDate(uCrsSMS.CampValA,"SQL")>>',
			mensagem		= '<<uCrsSMS.mensagem>>',
			usrinis			= '<<m_chinis>>',
			usrdata			= convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102),
			usrhora			= convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108),
			tipoenvio		= '<<uCrsSMS.tipoenvio>>',
			email_body		= '<<uCrsSMS.email_body>>',
			codminuta		= '<<uCrsSMS.codminuta>>'
		WHERE smsstamp='<<uCrsSMS.smsstamp>>'
	ENDTEXT

	IF !(uf_gerais_actGrelha("","",lcSQL))
		uf_perguntalt_chama("OCORREU UM ERRO AO GRAVAR OS DADOS DA COMUNICA��O.","OK","",16)
		RETURN .f.
	ELSE
		*******************************
		* Editar Clientes da Campanha *
		*******************************
		SELECT uCrsSMS
		
		&& procurar clientes associados � campanha
		lcSQL=''
		TEXT TO lcSQL TEXTMERGE noshow
			SELECT smsstamp, clstamp
			FROM b_smscl (nolock)
			WHERE smsstamp='<<uCrsSMS.smsstamp>>'
		ENDTEXT
		IF !(uf_gerais_actGrelha("","uCrsClCamp",lcSQL))
			uf_perguntalt_chama("OCORREU UM ERRO A ACTUALIZAR A COMUNICA��O. Cod:b_smscl","OK","",16)
			RETURN	.f.
		ENDIF 
		
		select uCrsClCamp
		GO TOP
		select uCrsClSMS
		GO TOP 
		&& acrescentar clientes novos � campanha
		SELECT * FROM uCrsClSMS WHERE uCrsClSMS.utstamp not in (select uCrsCLCamp.clstamp from uCrsClCamp) into cursor lcCursorInsertCl
		
		SELECT lcCursorInsertCl
		GO TOP 
		SCAN 					
			lcSQL=''
			TEXT TO lcSQL TEXTMERGE NOSHOW
				INSERT INTO b_smscl
				(
					smsstamp, clstamp, ordem,
					ousrinis ,ousrdata ,ousrhora ,usrinis ,usrdata ,usrhora 
				) 
				VALUES
				( 
					'<<uCrsSMS.smsstamp>>','<<lcCursorInsertCl.utstamp>>', (select ISNULL(MAX(ordem),1) from b_smscl (nolock) where smsstamp='<<uCrsSMS.smsstamp>>'),
					'<<m_chinis>>', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108), '<<m_chinis>>', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108)
				)
			ENDTEXT 
			IF !(uf_gerais_actGrelha("","",lcSQL))
				uf_perguntalt_chama("OCORREU UM ERRO A ACTUALIZAR A COMUNICA��O. Cod:b_smscl","OK","",16)
				RETURN	.f.
			ENDIF  
			
			SELECT lcCursorInsertCl
		ENDSCAN 
		
		&& eliminar clientes associados � campanha
		SELECT * FROM uCrsClCamp WHERE uCrsClCamp.clstamp not in (select uCrsClSMS.utstamp from uCrsClSMS) into cursor lcCursorDeleteCl
		
		SELECT lcCursorDeleteCl
		GO TOP 
		SCAN
			lcSQL = ""
			TEXT TO lcSQL textmerge NOSHOW
				DELETE FROM b_smscl
				WHERE smsstamp='<<uCrsSMS.smsstamp>>' and clstamp = '<<ALLTRIM(lcCursorDeleteCl.clstamp)>>'
			ENDTEXT
			IF !uf_gerais_actGrelha("","",lcSQL)
				uf_perguntalt_chama("OCORREU UM ERRO A ACTUALIZAR A COMUNICA��O. Cod:b_smscl","OK","",16)
				RETURN .f.
			ENDIF
			
			SELECT lcCursorDeleteCl
		ENDSCAN

		IF USED("uCrsClCamp")
			Fecha("uCrsClCamp")
		ENDIF
		
		IF USED("lcCursorDeleteCl")
			Fecha("lcCursorDeleteCl")
		ENDIF
		
		IF USED("lcCursorInsertCl")
			Fecha("lcCursorInsertCl")
		ENDIF
	ENDIF 
	
	*************************
	* Editar Palavras-Chave *
	*************************
	SELECT uCrsSMS
	IF !uf_gerais_actGrelha("","","DELETE FROM B_smspc WHERE smsstamp='" + ALLTRIM(astr(uCrsSMS.smsstamp)) + "'")
		uf_perguntalt_chama("OCORREU UM ERRO A ACTUALIZAR A COMUNICA��O. Cod:b_smspc","OK","",16)
		RETURN .f.
	ENDIF 
	IF uf_sms_insertB_smspc("uCrslstRef", "ref") AND uf_sms_insertB_smspc("uCrslstDesign", "design") AND uf_sms_insertB_smspc("uCrslstFam", "fam") AND uf_sms_insertB_smspc("uCrslstLab", "lab") AND uf_sms_insertB_smspc("uCrslstMarca", "marca")
		&& inseriu palavras-chave
	ELSE
		RETURN .f.
	ENDIF
	
	RETURN .t.
endfunc


*******************************
* 		Gravar Dados		  *
*******************************
FUNCTION uf_campanhas_sms_gravar
	LOCAL lcSQL, validaInsereSMS, validaUpdateSMS
	STORE '' TO lcSQL
	STORE .f. TO validaInsereSMS, validaUpdateSMS
	SET Point to '.'
	
	*************
	*Valida��es *
	*************
		SELECT uCrsSMS
		&& Tipo de campanha
			IF EMPTY(uCrsSMS.tipoenvio)
				uf_perguntalt_chama("O CAMPO: TIPO DE ENVIO, � DE PREENCHIMENTO OBRIGAT�RIO.","OK","",48)
				CAMPANHAS_SMS.txtTipoEnvio.SETFOCUS
				RETURN .f.
			ENDIF 
		
		&& Tipo minuta e c�digo da minuta vazio
			IF ALLTRIM(uCrsSMS.tipoenvio)=='MINUTA' AND EMPTY(uCrsSMS.codminuta)
				uf_perguntalt_chama("SE ESCOLHEU O TIPO MINUTA TEM DE SELECCIONAR A MESMA NO RESPETIVO CAMPO.","OK","",48)
				CAMPANHAS_SMS.txtTipoEnvio.SETFOCUS
				RETURN .f.
			ENDIF 

		
		&& Designa��o da Campanha
			IF empty(ALLTRIM(CAMPANHAS_SMS.txtDesign.value))
				uf_perguntalt_chama("O CAMPO: DESIGNA��O, � DE PREENCHIMENTO OBRIGAT�RIO.","OK","",48)
				CAMPANHAS_SMS.txtDesign.SETFOCUS
				RETURN .f.
			ENDIF		
		
		&& Refer�ncia da Campanha
			IF empty(ALLTRIM(CAMPANHAS_SMS.txtRef.value))
				uf_perguntalt_chama("O CAMPO: REFER�NCIA, � DE PREENCHIMENTO OBRIGAT�RIO.","OK","",48)
				RETURN .f.
			ENDIF
		
		&& Data de Validade da Campanha - De
			IF empty(CAMPANHAS_SMS.txtde.value) AND ALLTRIM(uCrsSMS.tipoenvio)=='SMS'
				uf_perguntalt_chama("O CAMPO: DATA DE VALIDADE DA COMUNICA��O, � DE PREENCHIMENTO OBRIGAT�RIO.","OK","",48)
				RETURN .f.
			ENDIF
		
		&& Data de Validade da Campanha - A
			IF empty(CAMPANHAS_SMS.txtA.value) AND ALLTRIM(uCrsSMS.tipoenvio)=='SMS'
				uf_perguntalt_chama("O CAMPO: DATA DE VALIDADE DA COMUNICA��O, � DE PREENCHIMENTO OBRIGAT�RIO.","OK","",48)
				RETURN .f.
			ENDIF
			
		&& Validar se a Data A n�o � inferior � Data De
			IF uf_gerais_getDate(CAMPANHAS_SMS.txtA.value,"SQL") < uf_gerais_getDate(CAMPANHAS_SMS.txtDe.value,"SQL") AND ALLTRIM(uCrsSMS.tipoenvio)=='SMS'
				uf_perguntalt_chama("AS DATAS DE VALIDADE DA COMUNICA��O N�O S�O COMPAT�VEIS. POR FAVOR VERIFIQUE.","OK","",48)
				RETURN .f.
			ENDIF
			
		&& Verificar se seleccionou algum cliente
			SELECT uCrsClSMS
			IF RECCOUNT("uCrsClSMS")<=0 AND ALLTRIM(uCrsSMS.tipoenvio)=='SMS'
				IF !uf_perguntalt_chama("TEM A CERTEZA QUE DESEJA GRAVAR A COMUNICA��O SEM NENHUM CLIENTE ASSOCIADO?","Sim","N�o")
					CAMPANHAS_SMS.lbl1.click
					RETURN .f.
				ENDIF 
			ENDIF
		
		&& Verificar se escreveu alguma msg
			IF EMPTY(ALLTRIM(astr(CAMPANHAS_SMS.pageframe1.page2.txtmensagem.value))) AND ALLTRIM(uCrsSMS.tipoenvio)=='SMS'
				IF !uf_perguntalt_chama("TEM A CERTEZA QUE DESEJA GRAVAR A COMUNICA��O COM A MENSAGEM VAZIA?","Sim","N�o")
					CAMPANHAS_SMS.lbl2.click
					CAMPANHAS_SMS.pageframe1.page2.txtmensagem.setfocus
					RETURN
				ENDIF 
			ENDIF
			IF EMPTY(ALLTRIM(astr(CAMPANHAS_SMS.pageframe1.page2.email_body.value))) AND ALLTRIM(uCrsSMS.tipoenvio)=='EMAIL'
				IF !uf_perguntalt_chama("TEM A CERTEZA QUE DESEJA GRAVAR O EMAIL COM A MENSAGEM VAZIA?","Sim","N�o")
					CAMPANHAS_SMS.lbl2.click
					CAMPANHAS_SMS.pageframe1.page2.email_body.setfocus
					RETURN
				ENDIF 
			ENDIF
	
	&& Passou valida��es - Grava Dados
	IF uf_gerais_actGrelha("","",[Begin Transaction])
		IF mySMSIntroducao

			validaInsereSMS = uf_SMS_insere()		
			IF validaInsereSMS
				IF uf_gerais_actGrelha("","",[commit Transaction])
					uf_gerais_gravaUltRegisto('b_sms',uCrsSMS.smsstamp)
					uf_perguntalt_chama("COMUNICA��O INSERIDA COM SUCESSO.","OK","",64)	
					&& Configura ecra
						CAMPANHAS_SMS.Caption	= "COMUNICA��ES"
						mySMSIntroducao = .f.
						mySMSAlteracao = .f.
						DO uf_sms_confEcra WITH 1
						uf_SMS_chama(uCrsSMS.ref)		
						CAMPANHAS_SMS.refresh
				ELSE
					uf_gerais_actGrelha("","",[rollback transaction])
				ENDIF
			ELSE
				uf_gerais_actGrelha("","",[rollback transaction])			
			endif
		ELSE
			validaUpdateSMS = uf_SMS_update()
			IF validaUpdateSMS
				IF uf_gerais_actGrelha("","",[commit Transaction])
					uf_perguntalt_chama("DADOS ALTERADOS COM SUCESSO.","OK","",64)
					&& Configura ecra
						CAMPANHAS_SMS.Caption	= "COMUNICA��ES"
						mySMSIntroducao = .f.
						mySMSAlteracao = .f.
						DO uf_sms_confEcra WITH 1
						uf_SMS_chama(uCrsSMS.ref)		
						CAMPANHAS_SMS.refresh
				ELSE
					uf_gerais_actGrelha("","",[rollback transaction])			
				endif	
			ELSE
				uf_gerais_actGrelha("","",[rollback transaction])
			ENDIF
		ENDIF
	ELSE
		uf_gerais_actGrelha("","",[rollback transaction])
	endif	
ENDFUNC


*****************************
* 	ELIMINAR CAMPANHA		*
*****************************
FUNCTION uf_SMS_eliminar
	LOCAL lcSQL
	STORE '' TO lcSQL	
	
	&&Valida��es
	IF USED("uCrsSMS")
		SELECT uCrsSMS
		IF !EMPTY(uCrsSMS.smsstamp)
			&& Se j� foi emitido SMS
				uf_gerais_actGrelha("","uCrstempDelCamp","Select emitido from b_sms (nolock) where smsstamp = '" + ALLTRIM(astr(uCrsSMS.smsstamp)) + "'")
				SELECT 	uCrstempDelCamp
				IF uCrstempDelCamp.emitido == .t.
					uf_perguntalt_chama("N�O PODE APAGAR COMUNICA��ES J� EMITIDAS.","OK","",48)	
					RETURN .f.
				ENDIF
				IF USED("uCrstempDelCamp")
					FECHA("uCrstempDelCamp")
				ENDIF

			&& Passou valida��es - vai apagar				
				IF uf_perguntalt_chama("TEM A CERTEZA QUE DESEJA APAGAR A COMUNICA��O?","Sim","N�o")
					IF uf_gerais_actGrelha("","",[BEGIN TRANSACTION])	
						SELECT uCrsSMS
						&& APAGAR CLIENTES ASSOCIADOS
						IF uf_gerais_actGrelha("","","DELETE FROM B_smscl WHERE smsstamp='" + ALLTRIM(astr(uCrsSMS.smsstamp)) + "'")
							&& APAGA PALAVRAS-CHAVE
							IF uf_gerais_actGrelha("","","DELETE FROM B_smspc WHERE smsstamp='" + ALLTRIM(astr(uCrsSMS.smsstamp)) + "'")
								&& APAGA CAMPANHA
									IF uf_gerais_actGrelha("","","DELETE FROM b_sms WHERE smsstamp='" + ALLTRIM(astr(uCrsSMS.smsstamp)) + "'")
										uf_gerais_actGrelha("","",[COMMIT TRANSACTION])
										uf_perguntalt_chama("REGISTO APAGADO COM SUCESSO!","OK","",64)
										
										&& navega para ultimo registo
											uf_gerais_actGrelha("","ucrsTemp6",[select top 1 ref,smsstamp from b_sms (nolock) order by ousrdata desc ,ousrhora desc])
											IF RECCOUNT()>0
												uf_gerais_gravaUltRegisto('b_sms',ucrsTemp6.smsstamp)
												uf_SMS_chama(ucrsTemp6.ref)
											else
												uf_SMS_chama("")
											ENDIF
											IF USED("ucrsTemp6")
												fecha("ucrsTemp6")
											endif	
									ELSE
										uf_gerais_actGrelha("","",[ROLLBACK])
										uf_perguntalt_chama("OCORREU UM ERRO AO APAGAR A COMUNICA��O!","OK","",16)
									ENDIF
							ELSE
								uf_gerais_actGrelha("","",[ROLLBACK])
								uf_perguntalt_chama("OCORREU UM ERRO AO APAGAR AS PALAVRAS-CHAVE DA COMUNICA��O!","OK","",16)
							ENDIF 
						ELSE
							uf_gerais_actGrelha("","",[ROLLBACK])
							uf_perguntalt_chama("OCORREU UM ERRO AO APAGAR OS CLIENTES DA COMUNICA��O!","OK","",16)
						ENDIF
					ELSE
						RETURN .f.
					ENDIF 			
				ENDIF && fim pergunta
		ENDIF &&smsstamp not empty
	ENDIF
ENDFUNC


***********************
* Sair do ecra de SMS *
***********************
FUNCTION uf_campanhas_sms_sair
	IF mySMSIntroducao OR mySMSAlteracao
		If uf_perguntalt_chama("DESEJA CANCELAR AS ALTERA��ES?","sim","N�o")

			IF mySMSIntroducao
				IF !(uf_gerais_actGrelha("","IDtemp",[select top 1 ref from b_sms (NOLOCK) order by ousrdata desc ,ousrhora desc]))
					uf_perguntalt_chama("OCORREU UM ERRO AO RECHAMAR OS DADOS DA COMUNICA��O!","OK","",16)
				ENDIF
				CAMPANHAS_SMS.Caption = "COMUNICA��ES"	
				mySMSIntroducao = .f.
				mySMSAlteracao = .f.
				IF RECCOUNT()>0	
					uf_SMS_chama(IDtemp.ref)
				ELSE
					uf_SMS_chama("")
				endif	
				IF USED("IDtemp")
					fecha("IDtemp")
				endif
			ELSE && Altera��o
				CAMPANHAS_SMS.Caption = "COMUNICA��ES"	
				mySMSIntroducao = .f.
				mySMSAlteracao = .f.
				uf_SMS_chama(uCrsSMS.ref)
			ENDIF

			&&Actualizar Painel	
			CAMPANHAS_SMS.lbl1.click
			CAMPANHAS_SMS.Refresh
		ENDIF	
	ELSE
		&& Evitar erros de rowsource/recordsource ao sair
			CAMPANHAS_SMS.pageframe1.page1.grdCl.recordsource = ''
	
		&& fecha cursores
			IF USED("uCrsSMS")
				Fecha("uCrsSMS")
			ENDIF	
						
			IF USED ("uCrsClSMS")
				FECHA("uCrsClSMS")
			ENDIF	
				
		CAMPANHAS_SMS.hide
		CAMPANHAS_SMS.Release
		
		RELEASE CAMPANHAS_SMS
	ENDIF
ENDFUNC


*********************************
*	 NAVEGA REGISTO ANTERIOR	*
*********************************
FUNCTION uf_SMS_ant
	SELECT uCrsSMS
	lcSQL=""
	
	TEXT TO lcSQL TEXTMERGE NOSHOW 
		SELECT TOP 1 ref FROM B_SMS (nolock)
		WHERE ISNULL(convert(int,ref),0) < <<IIF(EMPTY(uCrsSMS.smsstamp),0,uCrsSMS.ref)>>
		order by ref desc
	ENDTEXT  
	If !uf_gerais_actGrelha("","uCrstempSMS",lcSQL)
		uf_perguntalt_chama("OCORREU UM ERRO A LER O REGISTO ANTERIOR.","OK","",48)
		RETURN .f.
	ENDIF
	
	IF RECCOUNT("uCrstempSMS")>0
		uf_SMS_chama(uCrstempSMS.ref)
	ENDIF
	
	fecha("uCrstempSMS")
endfunc	


*********************************
*	 NAVEGA PROXIMO REGISTO 	*
*********************************
FUNCTION uf_SMS_prox
	SELECT uCrsSMS
	lcSQL=""
	TEXT TO lcSQL TEXTMERGE NOSHOW 
		SELECT TOP 1 ref FROM B_SMS (nolock)
		WHERE ISNULL(convert(int,ref),0) > <<IIF(EMPTY(uCrsSMS.smsstamp),9999999,uCrsSMS.ref)>>
		order by ref desc
	ENDTEXT
	
	If !uf_gerais_actGrelha("","uCrstempSMS",lcSQL)
		uf_perguntalt_chama("OCORREU UM ERRO A LER O REGISTO SEGUINTE.","OK","",48)
		RETURN .f.
	ENDIF
	
	IF RECCOUNT("uCrstempSMS")>0
		uf_SMS_chama(uCrstempSMS.ref)
	ENDIF
	
	fecha("uCrstempSMS")
endfunc	


*****************************************
*		VALORES POR DEFEITO DOS SMS		*
*****************************************
FUNCTION uf_SMS_ValoresDefeito
	**************
	* Valida��es *
	**************
	IF !USED("uCrsSMS")
		RETURN .f.
	ENDIF
	***************
	
	SELECT uCrsSMS
	
	LOCAL lcStamp
	lcStamp = uf_gerais_stamp()
	
	&& Stamp	
	SELECT uCrsSMS
	replace uCrsSMS.smsstamp WITH lcStamp
		
	&& Ref. da Campanha
	If uf_gerais_actGrelha("","uCrsTempNo",[select isnull(cast(MAX(cast(ref as numeric(10,0))) as varchar(8)),'0000000') as no from B_sms (nolock)])
		If Reccount()>0
			IF EMPTY(uCrsTempNo.no)
				SELECT uCrsSMS
				replace uCrsSMS.ref WITH '0000001'
			ELSE
				LOCAL lcRef
				STORE '' TO lcRef
				lcRef = astr(Val(alltrim(uCrsTempNo.no))+1)
				IF LEN(lcRef) < 7
					FOR i=1 TO 7-LEN(lcRef)
						lcRef = '0' + lcRef
					ENDFOR 
				ENDIF 
				SELECT uCrsSMS
				replace uCrsSMS.ref WITH lcRef
			ENDIF
		Endif
		Fecha("uCrsTempNo")
	ENDIF
	
	&& CheckBoxes do painel
	SELECT uCrsSMS
	replace emitido WITH .f.
		
	&& Dados de Cria��o
	SELECT uCrsSMS
	replace ousrdata	WITH	datetime()+(difhoraria*3600)
	replace ousrinis	WITH	m_chinis
	replace usrinis		WITH	m_chinis
	replace usrdata		WITH	datetime()+(difhoraria*3600)
		
	CAMPANHAS_SMS.refresh	
ENDFUNC


**
FUNCTION uf_SMS_RemoverClienteGrid
	SELECT uCrsCLSMS
	DELETE
	GO TOP 
	CAMPANHAS_SMS.pageframe1.page1.grdCl.refresh
	
	SELECT uCrsCLSMS
	count for !deleted() TO X
	GO TOP 
	CAMPANHAS_SMS.pageframe1.page1.lblRegistos.caption = astr(X) + " Registos"
ENDFUNC 


**
FUNCTION uf_SMS_Imprimir
	SELECT uCrsSMS
	GO TOP 
	IF !EMPTY(ALLTRIM(uCrsSMS.smsstamp))
		REPORT FORM sms.frx TO PRINTER PROMPT NODIALOG PREVIEW 	
	ENDIF
ENDFUNC


**
FUNCTION uf_SMS_GetDirectorio
	CAMPANHAS_SMS.pageframe1.page4.txtCaminho.value=GETDIR("","Caminho para Ficheiro SMS","LOGITOOLS SOFTWARE")
ENDFUNC 

**
FUNCTION uf_SMS_GetAnexo
	CAMPANHAS_SMS.pageframe1.page4.txtAnexo.value=GETFILE("","Ficheiro a anexar ao email")
ENDFUNC 


**
FUNCTION uf_SMS_GerarFicheiro
	IF !uf_gerais_actGrelha("","uCrsTempEmitido","select emitido from B_sms (nolock) where smsstamp='" + uCrsSMS.smsstamp + "'")
		uf_perguntalt_chama("OCORREU UM ERRO AO LER A TABELA DE COMUNICA��ES.","OK","", 16)
		RETURN .f.
	ENDIF
	IF ucrsTempEmitido.emitido
		IF !uf_perguntalt_chama("A COMUNICA��O J� FOI EMITIDA. TEM A CERTEZA QUE DESEJA GERAR NOVO FICHEIRO?","Sim","N�o")
			RETURN
		ENDIF 
	ENDIF 
	fecha("uCrsTempEmitido")
	
	LOCAL lcSQL, lcCount
	STORE 2 TO lcCount
	
	oExcel = CreateObject("Excel.Application")
	IF vartype(oExcel) != "O"
	  * could not instantiate Excel object
	  uf_perguntalt_chama("N�O FOI POSS�VEL CRIAR UM OBJECTO DO TIPO EXCEL.","OK","",16)
	  return .F.
	ENDIF

	oWorkbook = oExcel.Application.Workbooks.Add()

	*oExcel.DisplayAlerts = .F.

	oExcel.Range("A1").Value = "Telefone"
	oExcel.Range("B1").Value = "Nome"
	oExcel.Range("C1").Value = "Variavel"
	
	SELECT uCrsCLSMS
	GO top
	SCAN
		lcSQL=''
		TEXT TO lcSQL TEXTMERGE NOSHOW
			SELECT tlmvl, nome
			FROM b_utentes (nolock)
			where b_utentes.utstamp = '<<uCrsCLSMS.utstamp>>'
		ENDTEXT
		IF !uf_gerais_actGrelha("","uCrsTempCL",lcSQL)
			uf_perguntalt_chama("OCORREU UM ERRO AO LER OS DADOS DO CLIENTE: " + ASTR(uCrsCLSMS.no),"OK","", 48)
		ELSE
			IF !EMPTY(ALLTRIM(astr(uCrsTempCL.tlmvl)))
				oExcel.Range("A" + astr(lcCount)).value = astr(uCrsTempCL.tlmvl)
				oExcel.Range("B" + astr(lcCount)).value = uCrsTempCL.nome
				oExcel.Range("C" + astr(lcCount)).value = uCrsSMS.mensagem
				lcCount = lcCount + 1
			ENDIF
		ENDIF
	ENDSCAN
	
	&&Resize colunas
	oExcel.ActiveSheet.UsedRange.EntireColumn.Autofit

	if val(oExcel.Version) > 11
	    oWorkbook.SaveAs(CAMPANHAS_SMS.pageframe1.page4.txtCaminho.value + "CAMPANHA " + astr(uCrsSMS.ref) + ".xls", 56) && xlExcel8
	else
	    oWorkbook.SaveAs(CAMPANHAS_SMS.pageframe1.page4.txtCaminho.value + "CAMPANHA " + astr(uCrsSMS.ref) + ".xls")
	ENDIF

	oWorkbook.Close()
	
	uf_perguntalt_chama("FICHEIRO GERADO COM SUCESSO.", "OK","",64)
	
	IF CAMPANHAS_SMS.pageframe1.page4.chkAbreFicheiro.tag == "true"
		ShellExecute(0,"open",CAMPANHAS_SMS.pageframe1.page4.txtCaminho.value + "CAMPANHA " + astr(uCrsSMS.ref) + ".xls","","",1)
	ENDIF
	IF CAMPANHAS_SMS.pageframe1.page4.chkAbrePasta.tag == "true"
		ShellExecute(0,"open",CAMPANHAS_SMS.pageframe1.page4.txtCaminho.value,"","",1)
	ENDIF 
	
	lcSQL=''
	TEXT TO lcSQL TEXTMERGE noshow
		UPDATE B_sms
		SET emitido = 1
		where smsstamp = '<<uCrsSMS.smsstamp>>'
	ENDTEXT
	
	IF !uf_gerais_actGrelha("","",lcSQL)
		uf_perguntalt_chama("OCORREU UM ERRO A ACTUALIZAR A TABELA DE COMUNICA��ES.","OK","",48)
	ELSE
		CAMPANHAS_SMS.menu1.Actualizar.click
	ENDIF
ENDFUNC


**
FUNCTION uf_sms_AcrescentaPalavraChave
	LPARAMETERS lcTipo
	
	LOCAL lcText
	STORE '' to lcText
	
	DO CASE 
		CASE lcTipo=='ref'
			lcText = INPUTBOX("Introduza o valor:", "LOGITOOLS SOFTWARE", "")
			
			IF !EMPTY(lcText)
				SELECT uCrslstRef
				APPEND BLANK
				replace uCrslstRef.valor WITH lcText
			ENDIF 
			
			campanhas_sms.pageframe1.page3.lstRef.rowsource=''
			campanhas_sms.pageframe1.page3.lstRef.rowsource='uCrslstRef'
			campanhas_sms.pageframe1.page3.lstRef.displayvalue=lcText
			
		CASE lcTipo=='design'
			lcText = INPUTBOX("Introduza o valor:", "LOGITOOLS SOFTWARE", "")
			
			IF !EMPTY(lcText)
				SELECT uCrslstDesign
				APPEND BLANK
				replace uCrslstDesign.valor WITH lcText
			ENDIF 
			
			campanhas_sms.pageframe1.page3.lstDesign.rowsource=''
			campanhas_sms.pageframe1.page3.lstDesign.rowsource='uCrslstDesign'
			campanhas_sms.pageframe1.page3.lstDesign.displayvalue=lcText
			
		CASE lcTipo=='fam'
			lcText = INPUTBOX("Introduza o valor:", "LOGITOOLS SOFTWARE", "")
			
			IF !EMPTY(lcText)
				SELECT uCrslstFam
				APPEND BLANK
				replace uCrslstFam.valor WITH lcText
			ENDIF 
			
			campanhas_sms.pageframe1.page3.lstFam.rowsource=''
			campanhas_sms.pageframe1.page3.lstFam.rowsource='uCrslstFam'
			campanhas_sms.pageframe1.page3.lstFam.displayvalue=lcText
			
		CASE lcTipo=='lab'
			lcText = INPUTBOX("Introduza o valor:", "LOGITOOLS SOFTWARE", "")
			
			IF !EMPTY(lcText)
				SELECT uCrslstLab
				APPEND BLANK
				replace uCrslstLab.valor WITH lcText
			ENDIF 
			
			campanhas_sms.pageframe1.page3.lstLab.rowsource=''
			campanhas_sms.pageframe1.page3.lstLab.rowsource='uCrslstLab'
			campanhas_sms.pageframe1.page3.lstLab.displayvalue=lcText
			
		CASE lcTipo=='marca'
			lcText = INPUTBOX("Introduza o valor:", "LOGITOOLS SOFTWARE", "")
			
			IF !EMPTY(lcText)
				SELECT uCrslstMarca
				APPEND BLANK
				replace uCrslstMarca.valor WITH lcText
			ENDIF
			
			campanhas_sms.pageframe1.page3.lstMarca.rowsource = ''
			campanhas_sms.pageframe1.page3.lstMarca.rowsource = 'uCrslstMarca'
			campanhas_sms.pageframe1.page3.lstMarca.displayvalue=lcText
			
		OTHERWISE 
			***
	ENDCASE 
	
ENDFUNC 


**
FUNCTION uf_SMS_RemovePalavraChave
	LPARAMETERS lcTipo
	
	
	DO CASE 
		CASE lcTipo=='ref'
			select uCrslstRef
			delete
			
			campanhas_sms.pageframe1.page3.lstRef.rowsource=''
			campanhas_sms.pageframe1.page3.lstRef.rowsource='uCrslstRef'
			
		
			SELECT uCrslstRef
			go Top
			campanhas_sms.pageframe1.page3.lstRef.displayvalue=uCrslstRef.valor
	
			
		CASE lcTipo=='design'
			select uCrslstDesign
			delete
			
			campanhas_sms.pageframe1.page3.lstDesign.rowsource=''
			campanhas_sms.pageframe1.page3.lstDesign.rowsource='uCrslstDesign'
		
			SELECT uCrslstDesign
			go top
			campanhas_sms.pageframe1.page3.lstDesign.displayvalue=uCrslstDesign.valor
			
			
		CASE lcTipo=='fam'
			select uCrslstFam
			delete
			
			campanhas_sms.pageframe1.page3.lstFam.rowsource=''
			campanhas_sms.pageframe1.page3.lstFam.rowsource='uCrslstFam'
			
			SELECT uCrslstFam
			go top
			campanhas_sms.pageframe1.page3.lstFam.displayvalue=uCrslstFam.valor
			
		CASE lcTipo=='lab'
			select uCrslstLab
			delete
			
			campanhas_sms.pageframe1.page3.lstLab.rowsource=''
			campanhas_sms.pageframe1.page3.lstLab.rowsource='uCrslstLab'
			
			
			SELECT uCrslstLab
			go top
			campanhas_sms.pageframe1.page3.lstLab.displayvalue=uCrslstLab.valor
			
			
		CASE lcTipo=='marca'
			select uCrslstMarca
			delete
			
			campanhas_sms.pageframe1.page3.lstMarca.rowsource=''
			campanhas_sms.pageframe1.page3.lstMarca.rowsource='uCrslstMarca'
			
			SELECT uCrslstMarca
			go top
			campanhas_sms.pageframe1.page3.lstMarca.displayvalue=uCrslstMarca.valor
			
		OTHERWISE 
			***
	ENDCASE
ENDFUNC 


** funcao que corre quando chamada no form SMS - "ENVIAR CAMPANHAS"
FUNCTION uf_SMS_enviarWebService
	LOCAL lcWsPath
	STORE '' TO lcWsPath
	
	IF dtoc(uCrsSMS.ousrdata)< '18/05/25' AND DTOC(DATE())>'18/05/24' 
		uf_perguntalt_chama("N�o pode enviar comunica��es criadas antes de 25/08/2018 devido �s normas do RGPD.", "OK","",16)		
		RETURN .f.
	ENDIF 

	IF !FILE(ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + "\ltsSMS\LtsSMS.jar")
		uf_perguntalt_chama("Ficheiro de configura��o n�o encontrado, por favor contacte o suporte.", "OK","",16)
		RETURN .f.
	ENDIF

	&& Verifica se ainda tem SMS dispon�veis - o controlo � feito por calcudo do valor definido no parametro ADM0000000188 vs n� de SMS enviados na tabela b_sms_fact 
	lcSQL = ''
	TEXT TO lcSQL TEXTMERGE NOSHOW
		select 
			'enviados' = isnull(sum(total),0)
		from b_sms_fact (nolock)
		where 
			username = '<<ALLTRIM(uf_gerais_getParameter("ADM0000000157","TEXT"))>>'
			and status in (0,1,2)
	ENDTEXT

	uf_gerais_actGrelha("","uCrsEnviados",lcSQL)
	SELECT uCrsEnviados
	**IF uf_gerais_getParameter("ADM0000000188","NUM") <= (uCrsEnviados.enviados + reccount("uCrsClSMS")) - alterado a 20190228 para os parametros da empresa
	IF uf_gerais_getParameter_site("ADM0000000024","NUM", mySite) <= (uCrsEnviados.enviados + reccount("uCrsClSMS"))
		uf_perguntalt_chama("N�O ADQUIRIU SMS SUFICIENTES PARA ENVIAR ESTA COMUNICA��O." + CHR(13) + "PARA ADQUIRIR MAIS SMS POR FAVOR CONTACTE A LOGITOOLS (OP��O - COMERCIAL).", "OK","",16)
		RETURN .F.
	ENDIF 
	**
	
	IF !uf_gerais_actGrelha("","uCrsTempEmitido","select emitido from B_sms (nolock) where smsstamp='" + uCrsSMS.smsstamp + "'")
		uf_perguntalt_chama("OCORREU UMA ANOMALIA AO LER A TABELA DE COMUNICA��ES.", "OK","",16)
		RETURN .f.
	ENDIF
	
	&& verifica se campanha j� foi emitida
	IF ucrsTempEmitido.emitido
		IF uf_perguntalt_chama("A COMUNICA��O J� FOI EMITIDA.","OK")
			RETURN .f.
		ENDIF 
	ENDIF 
	
	fecha("uCrsTempEmitido")

	** Valida��es 
	&& Data e Hora de envio
	IF CAMPANHAS_SMS.pageframe1.page4.chkEImediato.tag == "false"
		IF uf_gerais_getDate(campanhas_sms.pageframe1.page4.txtEnviarEm.value,"SQL") < uf_gerais_getDate(date(),"SQL")
			uf_perguntalt_chama("TEM QUE INDICAR UMA DATA DE ENVIO DO SMS V�LIDA.","OK","", 48)
			CAMPANHAS_SMS.pageframe1.page4.txtEnviarEm.CLICK
			RETURN .F.
		ENDIF 
		IF len(alltrim(campanhas_sms.pageframe1.page4.txtHoras.value))<5 or val(left(campanhas_sms.pageframe1.page4.txtHoras.value,2))>23 or val(left(campanhas_sms.pageframe1.page4.txtHoras.value,2))<0 or val(right(campanhas_sms.pageframe1.page4.txtHoras.value,2))>59 or val(right(campanhas_sms.pageframe1.page4.txtHoras.value,2))<0
			uf_perguntalt_chama("TEM QUE INDICAR UMA HORA DE ENVIO DO SMS V�LIDA.","OK","", 48)
			CAMPANHAS_SMS.pageframe1.page4.txtHoras.setfocus
			RETURN .F.
		ENDIF 
	ENDIF 
	
	Local lcDataEnvio
	lcDataEnvio = iif(Year(uCrsSMS.dataenvio)= 1900, uf_gerais_getDate(DATETIME(),"SQL")+' '+ TIME(),uf_gerais_getDate(uCrsSMS.dataenvio,"SQL") +' ' +  uCrsSMS.horaenvio)
	&& Guardar Dados da Campanha
	lcSQL=''
	TEXT TO lcSQL TEXTMERGE noshow
		UPDATE B_sms
		SET envimediato		= <<IIF(uCrsSMS.envimediato,1,0)>>,
			dataenvio 		= '<<lcDataEnvio>>',
			horaenvio		= '<<uCrsSMS.horaenvio>>',
			resposta		= <<IIF(uCrsSMS.resposta,1,0)>>,
			dataresposta	= '<<uf_gerais_getDate(uCrsSMS.dataresposta,"SQL")>>',
			horaresposta	= '<<uCrsSMS.horaresposta>>',
			confirmacao		= <<IIF(uCrsSMS.confirmacao,1,0)>>,
			campconf		= '<<uCrsSMS.campconf>>',
			custo			= '<<ALLTRIM(uCrsSMS.custo)>>',
			aceitatodas		= <<IIF(uCrsSMS.aceitatodas,1,0)>>,
			relatorio		= <<IIF(uCrsSMS.relatorio,1,0)>>,
			instrucoes		= <<IIF(uCrsSMS.instrucoes,1,0)>>,
			remetente		= '<<ALLTRIM(uCrsSMS.remetente)>>',
			idremetente		= '<<ALLTRIM(uf_gerais_getParameter_site("ADM0000000026","TEXT", mySite))>>',
			tipoenvio		= '<<uCrsSMS.tipoenvio>>',
			codminuta		= '<<uCrsSMS.codminuta>>'
		where smsstamp = '<<uCrsSMS.smsstamp>>'
	ENDTEXT
	
	IF !uf_gerais_actGrelha("","",lcSQL)
		uf_perguntalt_chama("OCORREU UM ERRO A ACTUALIZAR A TABELA DE COMUNICA��ES.","OK","",48)
	ELSE

		IF !(uf_sms_validaInternet())
			RETURN .F.
		ENDIF 
		    		
		IF DIRECTORY(ALLTRIM(myPath))
			pEnvCamp = .t.
			
			&&limpar respostas que j� possam existir para esta campanha
			uf_sms_eliminaRegistoResposta("enviar")
			
			uf_param_sms_atualiza()
			
			SELECT uCrsSMS
			lcWsPath = ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + "\ltsSMS\LtsSMS.jar enviar " + ALLTRIM(uCrsSMS.smsstamp) + " " + ALLTRIM(sql_db)
					
			&& Envia comando Java
			lcWsPath = "javaw -jar " + lcWsPath 	
		
			&& aguarda que software termine o envio da informa��o 3� PARAMETRO = .T.
			oWSShell = CREATEOBJECT("WScript.Shell")
			oWSShell.Run(lcWsPath, 1, .t.)

			**uf_SMS_setTimer(.t., "enviar")
			
			uf_sms_confirmaEnvio(.t., "enviar")
			
		ENDIF
		
		CAMPANHAS_SMS.menu1.Actualizar.click
	ENDIF
	
ENDFUNC


** uf_sms_confirmaEnvio(.f.,'ENVIAR')
FUNCTION uf_sms_confirmaEnvio
	LPARAMETERS tcBool, lcTipo

	LOCAL lcNrRecs
	STORE 0 TO lcNrRecs
	
	PUBLIC mySmsTipoAcao
	mySmsTipoAcao = UPPER(ALLTRIM(lctipo))
	
	SELECT uCrsSMS
	lcSQL = ''
	TEXT TO lcSql NOSHOW textmerge
		SELECT TOP 1 CodResp FROM b_sms_resposta WHERE smsstamp='<<ALLTRIM(uCrsSMS.smsstamp)>>' AND tipo = '<<ALLTRIM(mySmsTipoAcao)>>' ORDER BY DataEnvio DESC 
	ENDTEXT
	IF !uf_gerais_actGrelha("","uCrsVerificaResposta",lcSql)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VERIFICAR A RESPOSTA DO WEB SERVICE. POR FAVOR CONTACTE O SUPORTE.","OK","",64)
		RETURN .f.
	ELSE
		SELECT uCrsVerificaResposta
		COUNT TO lcNrRecs

		** se existe resposta
		IF lcNrRecs > 0
			SELECT uCrsVerificaResposta
			GO TOP
				
			** retorna c�digo de envio s/ sucesso ( != 0 )
			IF uCrsVerificaResposta.CodResp != 0
				uf_SMS_CodigosErroWS(uCrsVerificaResposta.CodResp)
				RETURN .f.
			ELSE
				DO CASE
					CASE mySmsTipoAcao == "ENVIAR" 
						uf_perguntalt_chama("MENSAGEM ENVIADA COM SUCESSO.","OK","",64)
						&& Actualizar Tabela de Factura��o de SMS - n�o � necess�rio estar sempre a correr a atualiza��o de dados - 
						&& corre numa das 2 seguintes situa��es:
						&& 1 - utilizador consulta An�lises; 2 - logitools consulta an�lises para faturar ao cliente
						** uf_ListaSMS_chamaRapido()
						
					CASE mySmsTipoAcao == "RELATORIOS"
						uf_perguntalt_chama("RELAT�RIOS DE ENTREGA OBTIDOS COM SUCESSO.","OK","",64)
						
					CASE mySmsTipoAcao == "RESPOSTAS"
						uf_perguntalt_chama("RESPOSTAS OBTIDAS COM SUCESSO.","OK","",64)
						
					CASE mySmsTipoAcao == "CANCELAR"
						uf_perguntalt_chama("MENSAGEM CANCELADA COM SUCESSO.","OK","",64)
						&& Actualizar Tabela de Factura��o de SMS
						uf_ListaSMS_chama()
						
					CASE mySmsTipoAcao == "TERMINAR"
						uf_perguntalt_chama("MENSAGEM TERMINADA COM SUCESSO.","OK","",64)
						&& Actualizar Tabela de Factura��o de SMS
						uf_ListaSMS_chama()
				ENDCASE 
			ENDIF
		ENDIF 
	ENDIF
ENDFUNC


**
FUNCTION uf_sms_validaInternet
	PUBLIC myTimerSMSVar
	**Verificar se existe liga��o � internet
	#DEFINE  FLAG_ICC_FORCE_CONNECTION 1
 	DECLARE Long InternetCheckConnection IN Wininet.dll String Url, Long dwFlags, Long Reserved
	 
	* Fast and reliable web site
	LOCAL lcValidaLigInternet
	STORE .t. TO lcvalidaLigInternet
	
	lcUrl = "http://www.google.com"
	IF InternetCheckConnection(lcUrl, FLAG_ICC_FORCE_CONNECTION, 0) <> 0
		*? "Connection is available"
		lcValidaLigInternet = .t.
	ELSE
		lcvalidaLigInternet = .f.
	ENDIF
	
	IF lcValidaLigInternet == .f.
		lcUrl = "http://www.sapo.pt"
		IF InternetCheckConnection(lcUrl, FLAG_ICC_FORCE_CONNECTION, 0) <> 0
			lcValidaLigInternet = .t.
		ELSE
			lcvalidaLigInternet = .f.
		ENDIF
	ENDIF
	
	IF lcValidaLigInternet == .f.
		uf_perguntalt_chama("ESTA FUNCIONALIDADE REQUER LIGA��O � INTERNET. POR FAVOR VERIFIQUE.","OK","",48)
		RETURN .f.
	ENDIF
	**
ENDFUNC 


**
*!*	FUNCTION uf_SMS_setTimer
*!*		LPARAMETERS tcBool, lcTipo
*!*		
*!*		PUBLIC myTimerSMSVar
*!*		myTimerSMSVar = lctipo
*!*		
*!*		IF tcBool == .t.
*!*			
*!*			IF TYPE("CAMPANHAS_SMS.MyTimerSMS") # "O"
*!*				
*!*				CAMPANHAS_SMS.AddObject('MyTimerSMS','MyTimerSMS')
*!*			ELSE
*!*				IF !(CAMPANHAS_SMS.MyTimerSMS.enabled)
*!*					CAMPANHAS_SMS.MyTimerSMS.enabled = .t.
*!*				ELSE
*!*					uf_perguntalt_chama("J� EXISTE UM PEDIDO EM CURSO.","OK","",64)
*!*				ENDIF
*!*			ENDIF
*!*		ELSE
*!*			CAMPANHAS_SMS.RemoveObject('MyTimerSMS')
*!*		ENDIF
*!*		
*!*	ENDFUNC


**
*!*	DEFINE CLASS myTimerSMS as Timer
*!*		* Interval is in milliseconds [2,5 segundos]
*!*		interval = 2500
*!*		
*!*		FUNCTION Timer
*!*			SELECT uCrsSMS
*!*			lcSQL = ''
*!*			TEXT TO lcSql NOSHOW textmerge
*!*				SELECT TOP 1 CodResp FROM b_sms_resposta WHERE smsstamp='<<ALLTRIM(uCrsSMS.smsstamp)>>' AND tipo = '<<ALLTRIM(myTimerSMSVar)>>' order by DataEnvio desc
*!*			ENDTEXT
*!*			IF !uf_gerais_actGrelha("","uCrsVerificaResposta",lcSql)
*!*				uf_perguntalt_chama("OCORREU UM ERRO A VERIFICAR A RESPOSTA DO WEB SERVICE. POR FAVOR CONTACTE O SUPORTE.","OK","",64)
*!*				uf_SMS_setTimer(.f., lcTipo)
*!*				RETURN .f.
*!*			ELSE
*!*				lcNrRecs = 0
*!*				SELECT uCrsVerificaResposta
*!*				Count To lcNrRecs

*!*				IF lcNrRecs > 0
*!*					SELECT uCrsVerificaResposta
*!*					GO TOP
*!*					
*!*					*Verifica
*!*					IF uCrsVerificaResposta.CodResp != 0
*!*						uf_SMS_CodigosErroWS(uCrsVerificaResposta.CodResp)
*!*						uf_SMS_setTimer(.f., myTimerSMSVar)
*!*						RETURN
*!*					ELSE
*!*					
*!*	MESSAGEBOX(myTimerSMSVar)				

*!*						DO CASE
*!*							CASE myTimerSMSVar == "enviar"
*!*								uf_perguntalt_chama("MENSAGEM ENVIADA COM SUCESSO.","OK","",64)
*!*								&&Actualizar Tabela de Factura��o de SMS
*!*								uf_ListaSMS_chamaRapido()
*!*							CASE myTimerSMSVar == "relatorios"
*!*								uf_perguntalt_chama("RELAT�RIOS DE ENTREGA OBTIDOS COM SUCESSO.","OK","",64)
*!*							CASE myTimerSMSVar == "respostas"
*!*								uf_perguntalt_chama("RESPOSTAS OBTIDAS COM SUCESSO.","OK","",64)
*!*							CASE myTimerSMSVar == "cancelar"
*!*								uf_perguntalt_chama("MENSAGEM CANCELADA COM SUCESSO.","OK","",64)
*!*								&&Actualizar Tabela de Factura��o de SMS
*!*								uf_ListaSMS_chama()
*!*							CASE myTimerSMSVar == "terminar"
*!*								uf_perguntalt_chama("MENSAGEM TERMINADA COM SUCESSO.","OK","",64)
*!*								&&Actualizar Tabela de Factura��o de SMS
*!*								uf_ListaSMS_chama()
*!*						ENDCASE 
*!*							
*!*						uf_SMS_setTimer(.f., myTimerSMSVar)
*!*					ENDIF
*!*				ENDIF 
*!*			ENDIF
*!*		ENDFUNC
*!*	ENDDEFINE


**
FUNCTION uf_SMS_ActualizaDadosClienteWS

	SELECT uCrsSMS
	IF uCrsSMS.emitido == .t.
		Local lcWsPath
			
		IF !(uf_sms_validaInternet())
			RETURN .f.
		ENDIF 
		IF !FILE(ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + "\ltsSMS\LtsSMS.jar")
			MESSAGEBOX("FICHEIRO SMS N�O ENCONTRADO.")
			RETURN .f.
		ENDIF
				    		
		IF DIRECTORY(ALLTRIM(myPath))
			
			IF uCrsSMS.Relatorio == .t.
				&&limpar respostas que j� possam existir para esta campanha
				uf_sms_eliminaRegistoResposta("relatorios")
				
				*Obter Relat�rios de Entrega
				uf_param_sms_atualiza()
				
				SELECT uCrsSMS
				lcWsPath = ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + "\ltsSMS\LtsSMS.jar relatorio " + ALLTRIM(uCrsSMS.smsstamp) + " " + ALLTRIM(sql_db)

				oWSShell = CREATEOBJECT("WScript.Shell")
				oWSShell.Run(lcWsPath,1,.f.)

				uf_SMS_setTimer(.t., "relatorios")
			ENDIF 
			
			IF uCrsSMS.Resposta == .t.
				&&limpar respostas que j� possam existir para esta campanha
				uf_sms_eliminaRegistoResposta("respostas")
				
				*Obter Respostas
				uf_param_sms_atualiza()
				
				SELECT uCrsSMS
				lcWsPath = ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + "\ltsSMS\LtsSMS.jar resposta " + ALLTRIM(uCrsSMS.smsstamp) + " " + ALLTRIM(sql_db)

				oWSShell = CREATEOBJECT("WScript.Shell")
				oWSShell.Run(lcWsPath,1,.f.)

				uf_SMS_setTimer(.t., "respostas")
			ENDIF 
		ENDIF
		
		CAMPANHAS_SMS.menu1.Actualizar.click
	ELSE
		uf_perguntalt_chama("A COMUNICA��O AINDA N�O FOI EMITIDA.","OK","",64)
	ENDIF 
ENDFUNC 


**
FUNCTION uf_SMS_CodigosErroWS
	LPARAMETERS lcCod
	
	DO CASE
		CASE lcCod==0
			uf_perguntalt_chama("Processo conclu�do com sucesso.","OK","",64)
		CASE lcCod==1
			uf_perguntalt_chama("Excep��o no stub do Web Service. Erro reportado no Service Center.","OK","",16)
		CASE lcCod==2
			uf_perguntalt_chama("Erro n�o esperado. Reportado no Service Center.","OK","",16)
		CASE lcCod==3
			uf_perguntalt_chama("Utilizador ou password inv�lida.","OK","",16)
		CASE lcCod==4
			uf_perguntalt_chama("Permiss�es insuficientes. O utilizador n�o � administrador.","OK","",16)
		CASE lcCod==5
			uf_perguntalt_chama("Lista j� existente.","OK","",16)
		CASE lcCod==6
			uf_perguntalt_chama("Lista n�o encontrada.","OK","",16)
		CASE lcCod==7
			uf_perguntalt_chama("A lista j� se encontra activa.","OK","",16)
		CASE lcCod==8
			uf_perguntalt_chama("A lista j� se encontra desactivada.","OK","",16)
		CASE lcCod==9
			uf_perguntalt_chama("A lista encontra-se em utiliza��o e n�o poder� ser alterada.","OK","",16)
		CASE lcCod==10
			uf_perguntalt_chama("MSISDN j� existente na lista.","OK","",16)
		CASE lcCod==11
			uf_perguntalt_chama("MSISDN com tamanho inv�lido. Dever� ter 9 d�gitos.","OK","",16)
		CASE lcCod==12
			uf_perguntalt_chama("MSISDN n�o � OnNET.","OK","",16)
		CASE lcCod==13
			uf_perguntalt_chama("MSISDN n�o pertence a uma rede m�vel. Dever� come�ar por 91, 93, 95 ou 96.","OK","",16)
		CASE lcCod==14
			uf_perguntalt_chama("MSISDN n�o encontrado na lista.","OK","",16)
		CASE lcCod==15
			uf_perguntalt_chama("MSISDN encontra-se referenciado em respostas e n�o poder� ser apagado.","OK","",16)
		CASE lcCod==16
			uf_perguntalt_chama("A COMUNICA��O n�o existe.","OK","",16)
		CASE lcCod==17
			uf_perguntalt_chama("A mensagem j� foi enviada.","OK","",16)
		CASE lcCod==18
			uf_perguntalt_chama("Formato de data inv�lido.","OK","",16)
		CASE lcCod==19
			uf_perguntalt_chama("Formato de hora inv�lido.","OK","",16)
		CASE lcCod==20
			uf_perguntalt_chama("C�digo de mensagem inv�lido (m�ximo 15 caracteres).","OK","",16)
		CASE lcCod==21
			uf_perguntalt_chama("A mensagem ainda n�o foi enviada.","OK","",16)
		CASE lcCod==22
			uf_perguntalt_chama("Utilizador n�o encontrado.","OK","",16)
		CASE lcCod==23
			uf_perguntalt_chama("O utilizador j� se encontra activo.","OK","",16)
		CASE lcCod==24
			uf_perguntalt_chama("O utilizador j� se encontra desactivado.","OK","",16)
		CASE lcCod==25
			uf_perguntalt_chama("Calendariza��o da mensagem encontra-se no passado.","OK","",16)
		CASE lcCod==26
			uf_perguntalt_chama("Calendariza��o da mensagem � posterior ao pr�ximo per�odo.","OK","",16)
		CASE lcCod==27
			uf_perguntalt_chama("T�rmino da mensagem mais cedo do que o seu envio.","OK","",16)
		CASE lcCod==28
			uf_perguntalt_chama("Dura��o da mensagem superior a um m�s.","OK","",16)
		CASE lcCod==29
			uf_perguntalt_chama("Mensagem de confirma��o muito longa ou mensagem de confirma��o vazia.","OK","",16)
		CASE lcCod==30
			uf_perguntalt_chama("Mensagem muito longa.","OK","",16)
		CASE lcCod==31
			uf_perguntalt_chama("Respostas de valor acrescentado n�o permitidas.","OK","",16)
		CASE lcCod==32
			uf_perguntalt_chama("Respostas de valor normal n�o permitidas.","OK","",16)
		CASE lcCod==33
			uf_perguntalt_chama("Per�odo corrente encontra-se barrado.","OK","",16)
		CASE lcCod==34
			uf_perguntalt_chama("Per�odo seguinte encontra-se barrado.","OK","",16)
		CASE lcCod==35
			uf_perguntalt_chama("C�digo de mensagem j� existente.","OK","",16)
		CASE lcCod==36
			uf_perguntalt_chama("CAP limite excedido.","OK","",16)
		CASE lcCod==37
			uf_perguntalt_chama("Cliente inactivo.","OK","",16)
		CASE lcCod==38
			uf_perguntalt_chama("Interface Web Services n�o dispon�vel para este cliente.","OK","",16)
		CASE lcCod==39
			uf_perguntalt_chama("Utilizador j� existente.","OK","",16)
		CASE lcCod==40
			uf_perguntalt_chama("Password vazia.","OK","",16)
		CASE lcCod==41
			uf_perguntalt_chama("N�o existem logs de mensagens dispon�veis.","OK","",16)
		CASE lcCod==42
			uf_perguntalt_chama("Identificador enviado n�o faz parte da lista de identificadores.","OK","",16)
		CASE lcCod==43
			uf_perguntalt_chama("Cliente com plano pr�-pago do tipo OnNet n�o pode enviar mensagens para n�meros OffNet.","OK","",16)
		CASE lcCod==44
			uf_perguntalt_chama("Este plano tarif�rio pr�-pago n�o permite enviar mensagens para n�meros internacionais.","OK","",16)
		CASE lcCod==45
			uf_perguntalt_chama("N� de telefone cont�m caracteres inv�lidos.","OK","",16)
		CASE lcCod==46
			uf_perguntalt_chama("Ocorreu um erro a eliminar a mensagem.","OK","",16)
		CASE lcCod==47
			uf_perguntalt_chama("A mensagem n�o pode ser eliminada porque n�o se encontra no estado Cancelada ou Terminada.","OK","",16)
		CASE lcCod==48
			uf_perguntalt_chama("O texto da mensagem n�o pode ser vazio.","OK","",16)
		CASE lcCod==49
			uf_perguntalt_chama("Este m�todo n�o suporta n�meros estrangeiros. Ter� de usar um outro m�todo equivalente de vers�o mais recente que j� os suporta. ","OK","",16)
		CASE lcCod==50
			uf_perguntalt_chama("A lista de contactos encontra-se vazia.","OK","",16)
		CASE lcCod==51
			uf_perguntalt_chama("A COMUNICA��O ad aeternum tem de aceitar pelo menos um tipo de respostas: gr�tis ou pagas.","OK","",16)
		CASE lcCod==52
			uf_perguntalt_chama("Valor inv�lido no tipo de activa��o da COMUNICA��O ad aeternum.","OK","",16)
		CASE lcCod==53
			uf_perguntalt_chama("O c�digo enviado j� est� associado a uma COMUNICA��O.","OK","",16)
		CASE lcCod==54
			uf_perguntalt_chama("O c�digo n�o est� associado � COMUNICA��O.","OK","",16)
		CASE lcCod==55
			uf_perguntalt_chama("O nome default � palavra reservada nas COMUNICA��Os ad aeternum, n�o pode ser usado como nome de uma COMUNICA��O.","OK","",16)
		CASE lcCod==56
			uf_perguntalt_chama("Encontra-se barrado no servi�o SMS Pro.","OK","",16)
		CASE lcCod==57
			uf_perguntalt_chama("O envio da mensagem foi bloqueado, porque n�o foi poss�vel cobrar a mensagem.","OK","",16)
		CASE lcCod==58
			uf_perguntalt_chama("O c�digo de COMUNICA��O n�o pode ser vazio.","OK","",16)
		CASE lcCod==59
			uf_perguntalt_chama("O nome da COMUNICA��O ad aeternum n�o pode ser vazio.","OK","",16)
		CASE lcCod==60
			uf_perguntalt_chama("Uma COMUNICA��O ad aeternum (excepto a default) tem de ter sempre um c�digo associado.","OK","",16)
		CASE lcCod==61
			uf_perguntalt_chama("N�o � poss�vel activar uma COMUNICA��O ad aeternum sem c�digo definido.","OK","",16)
		CASE lcCod==62
			uf_perguntalt_chama("O c�digo de COMUNICA��O n�o pode conter espa�os.","OK","",16)
		CASE lcCod==63
			uf_perguntalt_chama("O nome da COMUNICA��O n�o pode ser vazio.","OK","",16)
		CASE lcCod==64
			uf_perguntalt_chama("Existem contactos na lista com o Nome muito longo (m�ximo 30 caracteres).","OK","",16)
		CASE lcCod==65
			uf_perguntalt_chama("Existem contactos na lista com a Vari�vel de Envio muito longa (m�ximo 140 caracteres).","OK","",16)
		CASE lcCod==66
			uf_perguntalt_chama("Reservado.","OK","",16)
		CASE lcCod==67
			uf_perguntalt_chama("Reservado.","OK","",16)
		CASE lcCod==68
			uf_perguntalt_chama("Reservado.","OK","",16)
		CASE lcCod==69
			uf_perguntalt_chama("Reservado.","OK","",16)
		CASE lcCod==70
			uf_perguntalt_chama("O nome da lista n�o pode ser vazio.","OK","",16)
		CASE lcCod==71
			uf_perguntalt_chama("O nome da lista � muito longo (m�ximo 30 caracteres).","OK","",16)
		CASE lcCod==72
			uf_perguntalt_chama("A lista encontra-se em processo de importa��o de contactos e n�o poder� ser usada ou alterada.","OK","",16)
		CASE lcCod==73
			uf_perguntalt_chama("A COMUNICA��O n�o se encontra activa.","OK","",16)
		CASE lcCod==74
			uf_perguntalt_chama("O nome da COMUNICA��O n�o pode ser vazio.","OK","",16)
		CASE lcCod==75
			uf_perguntalt_chama("N�o foi submetido o bin�rio da prompt.","OK","",16)
		CASE lcCod==76
			uf_perguntalt_chama("O bin�rio da prompt submetido � demasiado grande.","OK","",16)
		CASE lcCod==77
			uf_perguntalt_chama("MSISDN tem de pertencer a uma rede nacional (2, 3, 91, 92, 93, 95, 96).","OK","",16)
		OTHERWISE
			*******
	ENDCASE 
	
	IF pEnvCamp AND lcCod != 0
		SELECT uCrsSMS
		*uf_gerais_actGrelha("","","update b_sms set emitido=0 where smsstamp='"+uCrsSMS.smsstamp+"'")
		pEnvCamp = .f.
		SELECT uCrsSMS
		*replace uCrsSMS.emitido WITH .f.
		CAMPANHAS_SMS.refresh
	ENDIF 
ENDFUNC


**
FUNCTION uf_SMS_ContaCaracteres
	LOCAL lcCont
	STORE 0 TO lcCont
	
	lcCont = LEN(ALLTRIM(CAMPANHAS_SMS.pageframe1.page2.txtMensagem.value))
	DO CASE 
		CASE lcCont <= 160
			CAMPANHAS_SMS.pageframe1.page2.lblCont.caption = STR(lcCont) + ' / 160 [ 1 SMS ]'
			RETURN 
		CASE lcCont <= 306
			CAMPANHAS_SMS.pageframe1.page2.lblCont.caption = STR(lcCont) + ' / 306 [ 2 SMS ]'
			RETURN 
		CASE lcCont <= 459
			CAMPANHAS_SMS.pageframe1.page2.lblCont.caption = STR(lcCont) + ' / 459 [ 3 SMS ]'
			RETURN 
		OTHERWISE 
			*****
	ENDCASE 
ENDFUNC


**
FUNCTION uf_sms_AddAutoPalavraChave
	IF USED("uCrsProdutosCli")
		LOCAL lcSQL
		IF RECCOUNT("uCrsProdutosCli")>0
			SELECT uCrsProdutosCli
			GO TOP
			SCAN
				&& Ref
				uf_sms_preencheListasAuto("uCrsLstRef", "Ref")
				
				&& Design
				uf_sms_preencheListasAuto("uCrsLstDesign", "Design")
				
				&& Familia
				uf_sms_preencheListasAuto("uCrsLstFam", "Faminome")
	
				&& Laborat�rio e Marca
				lcSQL=''
				TEXT TO lcSQL TEXTMERGE NOSHOW 
					SELECT u_lab, usr1 
					FROM st (nolock) 
					WHERE ref='<<uCrsProdutosCli.Ref>>'
				ENDTEXT 
				uf_gerais_actGrelha("","uCrsSMSLabMar",lcSQL)
				
				SELECT uCrsSMSLabMar
				SELECT uCrsProdutosCli
				replace u_lab WITH uCrsSMSLabMar.u_lab
				replace usr1 WITH uCrsSMSLabMar.usr1
				
				uf_sms_preencheListasAuto("uCrsLstLab", "u_lab")
				uf_sms_preencheListasAuto("uCrsLstMArca", "usr1")
				
				fecha("uCrsSMSLabMar")
			ENDSCAN
		ENDIF
		campanhas_sms.pageframe1.page3.lstRef.rowsource=''
		campanhas_sms.pageframe1.page3.lstRef.rowsource='uCrsLstRef'
		campanhas_sms.pageframe1.page3.lstDesign.rowsource=''
		campanhas_sms.pageframe1.page3.lstDesign.rowsource='uCrsLstDesign'
		campanhas_sms.pageframe1.page3.lstFam.rowsource=''
		campanhas_sms.pageframe1.page3.lstFam.rowsource='uCrsLstFam'
		campanhas_sms.pageframe1.page3.lstLab.rowsource=''
		campanhas_sms.pageframe1.page3.lstLab.rowsource='uCrsLstLab'
		campanhas_sms.pageframe1.page3.lstMarca.rowsource=''
		campanhas_sms.pageframe1.page3.lstMarca.rowsource='uCrsLstMarca'
	ENDIF 
ENDFUNC 


**
FUNCTION uf_sms_preencheListasAuto
	LPARAMETERS lcCursor, lcDesign
	
	LOCAL lcValida
	lcValida = 0
	
	SELECT &lcCursor
	GO TOP
	SCAN
		IF UPPER(ALLTRIM(astr(&lcCursor..valor))) == UPPER(ALLTRIM(astr(uCrsProdutosCli.&lcDesign)))
		lcValida = 1
		ENDIF 
	ENDSCAN
	IF lcValida == 0 AND !EMPTY(ALLTRIM(astr(uCrsProdutosCli.&lcDesign)))
		SELECT &lcCursor
		APPEND BLANK
		replace &lcCursor..valor WITH ALLTRIM(astr(uCrsProdutosCli.&lcDesign))
	ELSE
		lcValida = 0
	ENDIF
ENDFUNC 


**
FUNCTION uf_sms_cancelarcampanha
	pEnvCamp = .f.
	
	IF !FILE(ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + "\ltsSMS\LtsSMS.jar")
		MESSAGEBOX("FICHEIRO SMS N�O ENCONTRADO.")
		RETURN .f.
	ENDIF
	
	IF CAMPANHAS_SMS.pageframe1.page4.chkEImediato.tag == "true"
		uf_perguntalt_chama("N�O PODE CANCELAR UMA COMUNICA��O QUE J� FOI ENVIADA.","OK","", 48)
		RETURN .f.
	ELSE 
		SELECT uCrsSMS
		IF !uCrsSMS.emitido
			uf_perguntalt_chama("N�O PODE CANCELAR UMA COMUNICA��O QUE AINDA N�O FOI EMITIDA.","OK","", 48)
			RETURN .f.
		ENDIF 
		IF uf_perguntalt_chama("TEM A CERTEZA QUE PRETENDE CANCELAR O ENVIO DA COMUNICA��O?","Sim","N�o")
			Local lcWsPath
		
			IF !(uf_sms_validaInternet())
				RETURN .f.
			ENDIF 
			    		
			IF DIRECTORY(ALLTRIM(myPath))
				&&limpar respostas que j� possam existir para esta campanha
				uf_sms_eliminaRegistoResposta("cancelar")
				
				uf_param_sms_atualiza()
				
				SELECT uCrsSMS
				lcWsPath = ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + "\ltsSMS\LtsSMS.jar cancelar " + ALLTRIM(uCrsSMS.smsstamp) + " " + ALLTRIM(sql_db)

				oWSShell = CREATEOBJECT("WScript.Shell")
				oWSShell.Run(lcWsPath,1,.f.)

				uf_SMS_setTimer(.t.,"cancelar")
			ENDIF
		ENDIF
	ENDIF  
ENDFUNC


**
FUNCTION uf_sms_TerminarCampanha
	pEnvCamp = .f.
	
	IF !FILE(ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + "\ltsSMS\LtsSMS.jar")
		MESSAGEBOX("FICHEIRO SMS N�O ENCONTRADO.")
		RETURN .f.
	ENDIF
	
	IF CAMPANHAS_SMS.pageframe1.page4.chkReply.tag == "false"
		uf_perguntalt_chama("N�O PODE TERMINAR UMA COMUNICA��O QUE N�O ACEITA RESPOSTAS.","OK","", 48)
		RETURN .f.
	ELSE 
		SELECT uCrsSMS
		IF uf_perguntalt_chama("TEM A CERTEZA QUE PRETENDE ELIMINAR A CAMPANHA?","Sim","N�o")
			Local lcWsPath
		
			IF !(uf_sms_validaInternet())
				RETURN .f.
			ENDIF 
			    		
			IF DIRECTORY(ALLTRIM(myPath))
				&&limpar respostas que j� possam existir para esta campanha
				uf_sms_eliminaRegistoResposta("terminar")
				
				uf_param_sms_atualiza()
				
				SELECT uCrsSMS
				lcWsPath = ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + "\ltsSMS\LtsSMS.jar eliminar " + ALLTRIM(uCrsSMS.smsstamp)

				oWSShell = CREATEOBJECT("WScript.Shell")
				oWSShell.Run(lcWsPath,1,.f.)
				uf_SMS_setTimer(.t.,"terminar")
			ENDIF
		ENDIF
	ENDIF
ENDFUNC 


**
FUNCTION uf_sms_eliminaRegistoResposta
	LPARAMETERS lcTipo
	
	LOCAL lcSQL
	STORE "" TO lcSQL
	
	SELECT uCrsSMS
	TEXT TO lcSQL TEXTMERGE NOSHOW
		DELETE 
		FROM b_sms_resposta
		where 
			smsstamp = '<<ALLTRIM(uCrsSMS.smsstamp)>>'
			and tipo = <<IIF(EMPTY(ALLTRIM(lcTipo)),"tipo","'"+ALLTRIM(lcTipo)+"'")>>
	ENDTEXT
	uf_gerais_actGrelha("","",lcSQL)
ENDFUNC 


FUNCTION uf_emails_enviar
	LOCAL lcCont2
	lcCont2 = 1
	IF 	mySendEmail = .f.
		uf_perguntalt_chama("ATEN��O! OS PAR�METROS DE CONFIGURA��O PARA O ENVIO DE EMAILS N�O EST�O PREENCHIDOS. POR FAVOR PREENCHA-OS.","OK","", 48)
	ELSE
		IF uf_perguntalt_chama("TEM A CERTEZA DESEJA ENVIAR OS EMAILS? (Pode ser muito demorado dependente da quantidade a enviar)","Sim","N�o")
			regua(0,RECCOUNT("UCRSCLSMS"),"A ENVIAR EMAIL(S)...")
			SELECT UCRSCLSMS
			GO TOP
			SCAN
				regua(1,lcCont2,"A Enviar email: " + astr(lcCont2))
				IF uCrsSMS.assinatura=.f.
					uf_startup_sendmail_emp("", ALLTRIM(UCRSCLSMS.email), ALLTRIM(CAMPANHAS_SMS.txtDesign.value), uCrsSMS.email_body , alltrim(CAMPANHAS_SMS.pageframe1.page4.txtAnexo.value), .f.)
				ELSE
					uf_startup_sendmail_emp("", ALLTRIM(UCRSCLSMS.email), ALLTRIM(CAMPANHAS_SMS.txtDesign.value), uCrsSMS.email_body + CHR(13) + ALLTRIM(ucrse1.mailsign) , ALLTRIM(CAMPANHAS_SMS.pageframe1.page4.txtAnexo.value), .f.)
				ENDIF 
				uf_user_log_ins('CL', ALLTRIM(UCRSCLSMS.utstamp), 'E', 'Envio de email ' + ALLTRIM(CAMPANHAS_SMS.txtDesign.value))
				lcCont2 = lcCont2 + 1
			ENDSCAN 
			uf_perguntalt_chama("ENVIO CONCLU�DO.","OK","", 48)
			regua(2)
			
		ELSE
			uf_perguntalt_chama("ENVIO CANCELADO.","OK","", 48)
			RETURN .f.
		ENDIF 
	ENDIF 
ENDFUNC 



FUNCTION uf_emails_callPesCall

	LOCAL lcMonth, lcLastMonth
	
	lcMonth = uf_gerais_getdate(date(),"SQL")
	lcLastMonth = uf_gerais_getdate(date()-30,"SQL")
	&&alterado -1 para '' no/estab 20201009
	uf_pesqgeraischamadas_Chama('',lcLastMonth ,lcMonth ,'','','','') 

ENDFUNC 