FUNCTION uf_notificacoesMarcChama
	LOCAL lcSQL

	**
	TEXT TO lcSQL TEXTMERGE NOSHOW
		Select TOP 1 *,left(texto,250) m,SUBSTRING(texto,251,250) m1 from b_cli_notificacoes (nolock)
	ENDTEXT
	IF !uf_gerais_actGrelha("","ucrsDadosNotificacoes",lcSQL)
		uf_perguntalt_chama("N�O FOI POSSIVEL DETERMINAR OS DADOS EXISTENTES PARA AS NOTIFICA��ES. POR FAVOR CONTACTE O SUPORTE","OK","",16)
		RETURN .f.
	ENDIF	
	
	**
	IF RECCOUNT("ucrsDadosNotificacoes")==0
		SELECT ucrsDadosNotificacoes
		APPEND BLANK
		Replace ucrsDadosNotificacoes.dias 		WITH 0
		Replace ucrsDadosNotificacoes.texto		WITH ""
		Replace ucrsDadosNotificacoes.activo	WITH .f.
		Replace ucrsDadosNotificacoes.horaini	WITH "12:00"
		Replace ucrsDadosNotificacoes.horafinal	WITH "20:00"
		Replace ucrsDadosNotificacoes.m			WITH ""
		Replace ucrsDadosNotificacoes.m1		WITH ""
	ENDIF
	
	**
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_cli_NotificacoesTextoAutomatico
	ENDTEXT
	IF !uf_gerais_actGrelha("","ucrsNotificacoesTextoAutomatico",lcSQL)
		uf_perguntalt_chama("N�O FOI POSSIVEL DETERIMNAR OS DADOS EXISTENTES PARA AS NOTIFICA��ES. POR FAVOR CONTACTE O SUPORTE","OK","",16)
		RETURN .f.
	ENDIF	
	
	DO FORM NOTIFICACOESMARC 
		
	&&Centra
	NOTIFICACOESMARC.autocenter = .t.
	
	&&menu
	NOTIFICACOESMARC.menu1.estado("","","Gravar",.t.)
	
	&& Actualiza texto da mensagem
	NOTIFICACOESMARC.txtObservacoes.value = ucrsDadosNotificacoes.m + ucrsDadosNotificacoes.m1
	NOTIFICACOESMARC.txtObservacoes.value = ALLTRIM(NOTIFICACOESMARC.txtObservacoes.value)
	
	uf_NOTIFICACOESMARC_ContaCaracteres()
ENDFUNC 


**
FUNCTION uf_notificacoesmarc_gravar
	LOCAL lcSQL
	
	uf_gerais_actGrelha("","","BEGIN TRANSACTION")	
	SELECT ucrsDadosNotificacoes
	
	LOCAL lcStamp
	lcStamp = uf_gerais_stamp()
	
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		DELETE FROM b_cli_notificacoes
		INSERT INTO b_cli_notificacoes 
		values(
			'<<ALLTRIM(lcStamp)>>'
			,<<ucrsDadosNotificacoes.dias>>
			,'<<ucrsDadosNotificacoes.texto>>'
			,'<<m_chinis>>'
			,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
			,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8)
			,0
			,<<IIF(ucrsDadosNotificacoes.activo,1,0)>>
			,'<<ucrsDadosNotificacoes.horaini>>'
			,'<<ucrsDadosNotificacoes.horafinal>>') 
	ENDTEXT
	IF !uf_gerais_actGrelha("","",lcSQL)
		uf_gerais_actGrelha("","",[ROLLBACK])
		uf_perguntalt_chama("N�O FOI POSSIVEL ACTUALIZAR O REGISTO DE NOTIFICA��ES!","OK","",64)
	ELSE
		uf_gerais_actGrelha("","",[COMMIT TRANSACTION])
		uf_perguntalt_chama("REGISTO GRAVADO COM SUCESSO!","OK","",64)
	ENDIF
ENDFUNC


**
FUNCTION uf_notificacoesmarc_insereTexto
	SELECT ucrsNotificacoesTextoAutomatico
	
	IF !EMPTY(ALLTRIM(NOTIFICACOESMARC.tautomatico.Value))
		
		select ucrsNotificacoesTextoAutomatico
		LOCATE for ALLTRIM(ucrsNotificacoesTextoAutomatico.desc) == ALLTRIM(NOTIFICACOESMARC.tautomatico.Value)
		
		SELECT ucrsDadosNotificacoes
		REPLACE ucrsDadosNotificacoes.texto WITH ALLTRIM(ucrsDadosNotificacoes.texto) + ucrsNotificacoesTextoAutomatico.cod

		NOTIFICACOESMARC.txtObservacoes.refresh
	ENDIF	
ENDFUNC


**
FUNCTION uf_notificacoesmarc_sair
	NOTIFICACOESMARC.hide
	NOTIFICACOESMARC.release
ENDFUNC


**
FUNCTION uf_notificacoesmarc_ContaCaracteres
	LOCAL lcCont
	STORE 0 TO lcCont
	
	lcCont = LEN(NOTIFICACOESMARC.txtObservacoes.value)
	DO CASE 
		CASE lcCont <= 160
			NOTIFICACOESMARC.lblCont.caption = STR(lcCont) + ' / 160 [ 1 SMS ]'
			RETURN
		CASE lcCont <= 306
			NOTIFICACOESMARC.lblCont.caption = STR(lcCont) + ' / 306 [ 2 SMS ]'
			RETURN
		CASE lcCont <= 459
			NOTIFICACOESMARC.lblCont.caption = STR(lcCont) + ' / 459 [ 3 SMS ]'
			RETURN 
		OTHERWISE 
			*****
	ENDCASE 
ENDFUNC