**
FUNCTION uf_ListaSMS_chama
	LPARAMETERS lcData, lnSite
	
	if(EMPTY(lcData))
		lcData = ''
	endif
	
	if(EMPTY(lnSite))
		lnSite= mysite_nr
	ENDIF



	uf_param_sms_atualiza()
	IF FILE(ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + "\ltsSMS\LtsSMS.jar")
		lcWsPath = "javaw -jar " + ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + "\ltsSMS\LtsSMS.jar facturarGeral 0 " + ALLTRIM(sql_db)  + " " + lcData + " " +  ALLTRIM(STR(lnSite)) 
		
		oWSShell = CREATEOBJECT("WScript.Shell")
		oWSShell.Run(lcWsPath,1,.t.)
	ENDIF
	
ENDFUNC


**
FUNCTION uf_ListaSMS_chamaRapido

	uf_param_sms_atualiza()
	IF FILE(ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + "\ltsSMS\LtsSMS.jar")
		lcWsPath = "javaw -jar " + ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + "\ltsSMS\LtsSMS.jar facturar 0 " + ALLTRIM(sql_db)
		oWSShell = CREATEOBJECT("WScript.Shell")
		oWSShell.Run(lcWsPath,1,.t.)
	ENDIF
	
ENDFUNC

