**
FUNCTION uf_MVR_chama
	IF TYPE("MVR") != "U"
		MVR.show
	ELSE
		DO FORM MVR
	ENDIF
ENDFUNC


**
FUNCTION uf_MVR_gravar
	uf_MVR_calculaCompartALtIr()
ENDFUNC 


**
FUNCTION uf_MVR_init
	mvr.menu1.estado("","","Alterar",.t.)

	** cabe�alho
	Select ft2
	MVR.txtcod1.value			= ft2.u_codigo
	MVR.txtentidade1.value		= ft2.u_design
	MVR.txtcod2.value			= ft2.u_codigo2
	MVR.txtentidade2.value		= ft2.u_design2
	Select fi
	MVR.txtnmdoc.value		= Alltrim(fi.nmdoc)
	MVR.txtfno.value		= fi.fno
		
	** linhas
	MVR.txtref.value		= fi.ref
	MVR.txtdesign.value		= fi.design
	
	** valores originais
	MVR.txtopvp.value			= round(fi.u_epvp,2)
	IF !Empty(fi.u_epref)
		MVR.txtopref.value		= round(fi.u_epref,2)
	ELSE 
		MVR.txtopref.value		= 0.00
	ENDIF
	MVR.txtoepv.value			= round(fi.epv,2)
	MVR.txtoettent1.value		= round(fi.u_ettent1,2)
	MVR.txtoettent2.value		= round(fi.u_ettent2,2)
	MVR.txtoQt.value			= fi.qtt
	MVR.txtodiploma.value		= fi.u_diploma
	MVR.txtodiploma.enabled 	= .f.
	
	** valores para alterar
	MVR.txtpvp.value			= round(fi.u_epvp,2)
	MVR.txtepv.value			= round(fi.epv,2)
	MVR.txtpref.value			= round(fi.u_epref,2)
	MVR.txtcomp.value 			= 0
	MVR.txtcomp2.value 			= 0
	MVR.txtocomp.value 			= 0
	MVR.txtocomp2.value 		= 0
	Select ft2
	IF Empty(ft2.u_codigo2)
		MVR.txtcomp2.readonly 	= .t.
	ENDIF 
	MVR.txtettent1.value	= round(fi.u_ettent1,2)
	MVR.txtettent2.value	= round(fi.u_ettent2,2)
	MVR.txtQt.value			= fi.qtt
	MVR.txtdiploma.value	= fi.u_diploma
	MVR.txtdiploma.enabled 	= .f.
	
	MVR.txtotop5.value	= round(fi.pvpmaxre,2)
	MVR.txttop5.value	= round(fi.pvpmaxre,2)
	
	uf_MVR_calculaPercentCompIr()
ENDFUNC 


**
FUNCTION uf_MVR_actpvpepv

	IF MVR.txtcomp.value == 0 AND MVR.txtcomp2.value == 0
		uf_perguntalt_chama("Ambas as taxas n�o podem ser 0%.","OK","",48)
		MVR.txtcomp.value = MVR.txtocomp.value
		MVR.txtcomp2.value = MVR.txtocomp2.value
		uf_MVR_calculaCompartALtIr(.t.)
		Return
	ENDIF
	
	IF (MVR.txtcomp.value < 0) Or (MVR.txtcomp2.value < 0)
		uf_perguntalt_chama("A taxa de comparticipa��o n�o pode ser negativa.","OK","",48)
		MVR.txtcomp.value = MVR.txtocomp.value
		MVR.txtcomp2.value = MVR.txtocomp2.value
		*uf_unbindmis_actpvpepv()
		uf_MVR_calculaCompartALtIr(.t.)
		Return
	ENDIF 
	
	IF  (MVR.txtcomp.value > 100) Or (MVR.txtcomp2.value > 100)
		uf_perguntalt_chama("A taxa de comparticipa��o n�o pode ser superior a 100.","OK","",48)
		MVR.txtcomp.value = MVR.txtocomp.value
		MVR.txtcomp2.value = MVR.txtocomp2.value
		uf_MVR_calculaCompartALtIr(.t.)
		Return
	ENDIF 
	
	IF  MVR.txtpref.value < 0
		uf_perguntalt_chama("O pre�o de refer�ncia n�o pode ser negativo.","OK","",48)
		MVR.txtpref.value	= MVR.txtopref.value
		uf_MVR_calculaCompartALtIr(.t.)
		Return
	ENDIF 
	
	uf_MVR_calculaCompartALtIr(.t.)
ENDFUNC 


**
Function uf_MVR_calculaPercentCompIr
	Local lcSql, tcCodigo, tcCodigo2, tcRef, tcDiploma, lcChoosePvp
	Store '' To lcSql, tcCodigo, tcCodigo2, tcRef, tcDiploma
	LOCAL lcComp1, lcComp2
	STORE .f. to lcTemVal1, lcTemVal2 && Indica se � comparticipado pela entidade 1 e/ou 2
		
	tcCodigo	= Alltrim(MVR.txtcod1.value)
	tcCodigo2	= Alltrim(MVR.txtcod2.value)
	tcRef		= Alltrim(MVR.txtref.value)
	tcDiploma	= Alltrim(MVR.txtdiploma.value)
	lcChoosePvp	= MVR.txtPVP.value
	
	If Empty(tcDiploma)
		tcDiploma = 'xxx'
	Endif
	
	** 
	** Criar cursor - valores para plano 1
	**
	Text To lcSql Noshow textmerge
		exec up_receituario_CalcCompart '<<tcDiploma>>', <<lcChoosePvp>>, '<<tcRef>>', '<<tcCodigo>>',<<mysite_nr>>
	ENDTEXT
		
	If !uf_gerais_actGrelha("","uCrsCptVal",lcSql)
		*MSG(lcSql)
	Else
		If Reccount("uCrsCptVal")>0
			
			lcTemVal1 = .t.
			
			If !Empty(uCrsCptVal.percentagem)
				MVR.txtcomp.value	= Round(uCrsCptVal.percentagem,0)
				MVR.txtocomp.value	= Round(uCrsCptVal.percentagem,0)
			Endif
		Endif
	Endif
	
	**
	** Verificar se existe complementariedade
	**
	uf_gerais_actgrelha("", [uCrsCptCompl], [select compl from cptpla (nolock) where codigo='] + ALLTRIM(tcCodigo) + ['])
	IF USED("uCrsCptCompl")
		SELECT uCrsCptCompl
		IF RECCOUNT([uCrsCptCompl])>0
			
			**
			** Criar cursor - valores para plano 2
			**
			TEXT TO lcSql NOSHOW TEXTMERGE
				exec up_receituario_CalcCompart '<<ALLTRIM(tcDiploma)>>', <<lcChoosePvp>>, '<<ALLTRIM(tcRef)>>', '<<ALLTRIM(uCrsCptCompl.compl)>>',<<mysite_nr>>
			EndText
			uf_gerais_actgrelha("", [uCrsCptVal2], lcSql)
			SELECT uCrsCptVal2
			If Reccount([uCrsCptVal2])>0
				lcTemVal2 = .t.

				If !Empty(uCrsCptVal2.percentagem)
					MVR.txtcomp2.value	= Round(uCrsCptVal2.percentagem,0)
					MVR.txtocomp2.value	= Round(uCrsCptVal2.percentagem,0)
				ENDIF
			ENDIF
		ENDIF
		fecha([uCrsCptCompl])
	ENDIF
		
	If Used("uCrsCptVal")
		Fecha("uCrsCptVal")
	Endif 
	If used("uCrsCptVal2")
		Fecha("uCrsCptVal2")
	Endif
Endfunc


**
**
FUNCTION uf_MVR_calculaCompartALtIr
	LPARAMETERS tcBool
	
	** valida��es
	IF EMPTY(MVR.txtcomp.value) AND EMPTY(MVR.txtcomp2.value)
		uf_perguntalt_chama("N�o pode comparticipar com ambas a 0%.","OK","",48)
		RETURN .f.
	ENDIF
	
	LOCAL tcCodigo, tcCodigo2, tcRef, tcDiploma
	STORE '' To tcCodigo, tcCodigo2, tcRef, tcDiploma
	
	tcCodigo	= Alltrim(MVR.txtcod1.value)
	tcCodigo2	= Alltrim(MVR.txtcod2.value)
	tcRef		= Alltrim(MVR.txtref.value)
	tcDiploma	= Alltrim(MVR.txtdiploma.value)
	
	** chama fun��o calculo de comparticipa��o
	uf_receituario_MakePrecos(tcCodigo, tcRef, tcRef, "fi", tcDiploma, .f., .t.)

	IF !tcBool

		Select fi && Nota: os restantes campos s�o substitu�dos na fun��o makePrecos
		replace fi.u_comp		WITH .t.
		replace fi.pvpmaxre		WITH ROUND(MVR.txtTop5.value,2)
		
		** Recalcula Opcao **
		IF !(FI.opcao == 'I')
			replace fi.opcao	WITH IIF(fi.u_epvp > fi.pvpmaxre,'S', 'N')
		ENDIF
		
		** registar ocorr�ncia **
		Local lcOrigem
		Select ft
		lcOrigem = 'Doc.: ' + astr(ft.fno) + ' Ref.: '
		Select fi
		lcOrigem = lcOrigem + Alltrim(fi.ref)
		Select ft2
		lcOrigem = lcOrigem + ' Plano: ' + Alltrim(tcCodigo)
		If !Empty(tcCodigo2)
			lcOrigem = lcOrigem + ' | ' + Alltrim(tcCodigo2)
		ENDIF 
		
		SELECT fi
		uf_gerais_registaOcorrencia('Lotes', 'Manipula��o de Comparticipa��o', 1, Alltrim(lcOrigem), '', Alltrim(fi.fistamp))						
	
		** actualizar totais linha
		uf_atendimento_fiiliq(.t.)
	
		*** Calcular percentagem de custo para o utente
		Select fi
		IF fi.epv!=0
			IF fi.u_epvp=0
				replace fi.u_epvp WITH fi.epv
			ENDIF 
			IF fi.qtt>0
				Select fi
				Replace fi.u_txcomp WITH Round( (fi.etiliquido*100)/(fi.u_epvp*fi.qtt) ,2)
			ENDIF 
		ELSE 
			Replace fi.u_txcomp    with    0
		ENDIF 
		
		** actualizar totais
		uf_atendimento_actTotaisFt()
		
		** sair do ecra
		uf_MVR_sair()
	ENDIF
ENDFUNC 


**
FUNCTION uf_MVR_sair
	MVR.hide
	MVR.release
ENDFUNC