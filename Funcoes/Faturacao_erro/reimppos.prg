**
FUNCTION uf_REIMPPOS_Chama
	LPARAMETERS lcPainel
	
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		 select nmdoc
		 from td (nolock)
		 where u_tipodoc!=1 and u_tipodoc!=5
		 
		 union all 
		 
		 SELECT 'Recibo Normal' as nmdoc
		 
		 order by nmdoc
	ENDTEXT
	
	If !uf_gerais_actGrelha("","ucrsComboDocPOS",lcSQL)
		uf_perguntalt_chama("N�O FOI POSS�VEL ENCONTRAR DOCUMENTOS.","OK","",16)
		RETURN .f.
	ENDIF

	uf_REIMPPOS_CarregaDados(.t., lcPainel)

	IF TYPE("REIMPPOS")=="U"
		DO FORM REIMPPOS
	ELSE
		REIMPPOS.show
	ENDIF
	
	IF type("reimppos.menu1.actualizar") == "U"
		REIMPPOS.menu1.adicionaOpcao("opcoes", "Op��es", myPath + "\imagens\icons\opcoes_w.png", "","")
		REIMPPOS.menu1.adicionaOpcao("actualizar","Actualizar",myPath + "\imagens\icons\actualizar_w.png","uf_REIMPPOS_CarregaDados","")
		REIMPPOS.menu1.adicionaOpcao("talao","Tal�o",myPath + "\imagens\icons\imprimir_w.png","uf_imprimirPOS_ValImpTalao","")
		**REIMPPOS.menu1.adicionaOpcao("prevTalao","Prev. Tal�o",myPath + "\imagens\icons\prever_imp_w.png","uf_imprimirPOS_ValPrevTalao","")
		REIMPPOS.menu1.adicionaOpcao("verso1","Verso 1",myPath + "\imagens\icons\imprimir_w.png","uf_imprimirpos_ValimpRec with 1","")
		REIMPPOS.menu1.adicionaOpcao("verso2", "Verso 2", myPath + "\imagens\icons\imprimir_w.png", "uf_imprimirpos_ValimpRec with 2","")
		REIMPPOS.menu1.adicionaOpcao("atendimento","Atendimento",myPath + "\imagens\icons\imprimir_w.png","uf_imprimirPOS_reImpAtend","")
		REIMPPOS.menu1.adicionaOpcao("navegar","Navegar",myPath + "\imagens\icons\doc_seta_w.png","uf_reimppos_Navegar","F7")
		
		REIMPPOS.menu_opcoes.adicionaOpcao("prevVerso1","Prev. Verso 1","","uf_imprimirpos_ValPrevRec WITH 1")
		REIMPPOS.menu_opcoes.adicionaOpcao("prevVerso2", "Prev. Verso 2", "", "uf_imprimirpos_ValPrevRec WITH 2")
		REIMPPOS.menu_opcoes.adicionaOpcao("psico", "Psico", "", "uf_imprimirpos_ValImpPrevPsico with .f.")
		REIMPPOS.menu_opcoes.adicionaOpcao("prevPsico", "Prev. Psico", "", "uf_imprimirpos_ValImpPrevPsico with .t.")
        REIMPPOS.menu_opcoes.adicionaOpcao("prevTalao", "Prev. Tal�o", "", "uf_imprimirPOS_ValPrevTalao")
		IF(vartype(myTpaStamp)!="U") 
			REIMPPOS.menu_opcoes.adicionaOpcao("ultimoReciboTpa", "Reimp. Tal�o TPA", "", "uf_gestaocaixas_mensagemMenu with 1")	
		ENDIF

	ENDIF
	
	REIMPPOS.txtDataIni.value = uf_gerais_getDate((datetime()+((difhoraria-24)*3600)))
	REIMPPOS.txtDataFim.value = uf_gerais_getDate(datetime()+(difhoraria*3600))
	REIMPPOS.lblRegistos.caption = ALLTRIM(STR(RECCOUNT("ucrsPesqVendas"))) + " Resultados"
	If UPPER(ALLTRIM(lcPainel)) == "FT"
		Select ft
		If !Empty(ft.fno)
			REIMPPOS.txtNVenda.value = astr(ft.fno)
		Endif
		If !Empty(ft.no)
			REIMPPOS.txtNo.value = astr(ft.no)
		Endif
		If !Empty(ft.estab)
			REIMPPOS.txtEstab.value = astr(ft.estab)
		Endif
		If !Empty(ft.nmdoc)
			REIMPPOS.cmbDocumento.value = Alltrim(ft.nmdoc)
		Endif
		If !Empty(ft2.u_receita)
			REIMPPOS.txtReceita.value = Alltrim(ft2.u_receita)
		ENDIF
		If !Empty(ft.fdata)
			REIMPPOS.txtDataIni.value = uf_gerais_getDate(ft.fdata)
			REIMPPOS.txtDataFim.value = uf_gerais_getDate(ft.fdata)
		EndIf
	ENDIF
	REIMPPOS.grdVendas.setfocus
	REIMPPOS.txtNAtend.setfocus
	REIMPPOS.refresh
	
	uf_REIMPPOS_escondeColunas()
ENDFUNC


&& Hide colummn

FUNCTION uf_REIMPPOS_escondeColunas
	
	IF(uf_gerais_getParameter('ADM0000000281','BOOL'))
		
		IF(VARTYPE(REIMPPOS.grdVendas)!="U")
		
			IF(VARTYPE(REIMPPOS.grdVendas.vendnm)!="U")
				REIMPPOS.grdVendas.vendnm.visible=.f.
			ENDIF
			
		ENDIF
		
		
	ENDIF
	
	
	
ENDFUNC


**
FUNCTION uf_REIMPPOS_CarregaDados
	LPARAMETERS tcBool, lcPainel &&caso venha a .t. cria apenas o cursor

	LOCAL lcSQL, lcNVenda, lcNo, lcEstab, lcNmDoc, lcRec, lcDataIni, lcDataFim, lcNAtend
	STORE 0 TO lcNVenda, lcNo, lcNAtend
	STORE -1 TO lcEstab
	STORE '' TO lcSQL, lcNmDoc, lcRec
	lcDataIni = uf_gerais_getDate(DATE()-1,"SQL")
	lcDataFim = uf_gerais_getDate(DATE(),"SQL")
	
	regua(0,5,"A pesquisar as vendas...")
	regua(1,1,"A verificar par�metros...")
	
	IF tcBool == .t.
		If UPPER(ALLTRIM(lcPainel)) == "FT"
			Select ft
			If !Empty(ft.fno)
				lcNVenda = astr(ft.fno)
			Endif
			If !Empty(ft.no)
				lcNo = astr(ft.no)
			Endif
			If !Empty(ft.estab)
				lcEstab = astr(ft.estab)
			Endif
			If !Empty(ft.nmdoc)
				lcNmDoc = Alltrim(ft.nmdoc)
			Endif
			If !Empty(ft2.u_receita)
				lcRec = Alltrim(ft2.u_receita)
			ENDIF
			If !Empty(ft.fdata)
				lcDataIni = uf_gerais_getDate(ft.fdata,"SQL")
				lcDataFim = uf_gerais_getDate(ft.fdata,"SQL")
			EndIf
		ENDIF
		lcSQL = ""
		TEXT TO lcSql NOSHOW TEXTMERGE
			exec up_touch_reImprimir '',<<lcNVenda>>,'',<<lcNo>>,<<lcEstab>>,'<<uf_gerais_getDate(lcDataIni,"SQL")>>','<<uf_gerais_getDate(lcDataFim,"SQL")>>','<<lcNmDoc>>','<<lcRec>>', '', '<<mySite>>'
		ENDTEXT

		if type("REIMPPOS")=="U"
			regua(1,2,"A pesquisar os dados...")
			IF !uf_gerais_actGrelha("","ucrsPesqVendas",lcSql)
				uf_perguntalt_chama("N�O FOI POSSIVEL ACTUALIZAR A LISTA DE VENDAS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				RETURN .f.
			ENDIF
		ELSE
			regua(1,3,"A pesquisar os dados...")
			uf_gerais_actGrelha("REIMPPOS.grdVendas","ucrsPesqVendas",lcSQL)
		ENDIF
		
	ELSE
		**Trata parametros
		IF !EMPTY(REIMPPOS.txtNVenda.value)
			lcNVenda = REIMPPOS.txtNVenda.value
		ENDIF
		IF !EMPTY(REIMPPOS.txtNAtend.value)
			lcNAtend = REIMPPOS.txtNAtend.value
		ENDIF
		IF !EMPTY(REIMPPOS.txtNo.value)
			lcNo = REIMPPOS.txtNo.value
		ENDIF
		IF !EMPTY(REIMPPOS.txtEstab.value)
			lcEstab = REIMPPOS.txtEstab.value
		ENDIF
		lcDataIni = uf_gerais_getDate(REIMPPOS.txtDataIni.value,"SQL")
		lcDataFim = uf_gerais_getDate(REIMPPOS.txtDataFim.value,"SQL")
		lcSQL = ""
		TEXT TO lcSql NOSHOW TEXTMERGE 
			exec up_touch_reImprimir '<<ALLTRIM(REIMPPOS.txtProduto.value)>>', '<<lcNVenda>>', '<<ALLTRIM(REIMPPOS.txtNome.value)>>',<<lcNo>>,<<lcEstab>>,'<<uf_gerais_getDate(lcDataIni,"SQL")>>','<<uf_gerais_getDate(lcDataFim,"SQL")>>','<<ALLTRIM(REIMPPOS.cmbDocumento.value)>>','<<ALLTRIM(REIMPPOS.txtReceita.value)>>', '<<lcNAtend>>', '<<mySite>>'
		ENDTEXT

		regua(1,4,"A pesquisar os dados...")
		uf_gerais_actGrelha("REIMPPOS.grdVendas","ucrsPesqVendas",lcSQL)
		REIMPPOS.lblRegistos.caption = ALLTRIM(STR(RECCOUNT("ucrsPesqVendas"))) + " Resultados"
		REIMPPOS.Refresh
	ENDIF
	regua(1,5,"Pesquisa Terminada")
	regua(2)
ENDFUNC

FUNCTION uf_REIMPPOS_actualizaDadosLinha
	SELECT uCrsPesqVendas
	REIMPPOS.txtPlano.value = uCrsPesqVendas.plano
	REIMPPOS.txtL.value = uCrsPesqVendas.u_lote
	REIMPPOS.txtNR.value = uCrsPesqVendas.u_nslote
	REIMPPOS.txtL2.value = uCrsPesqVendas.u_lote2
	REIMPPOS.txtNR2.value = uCrsPesqVendas.u_nslote2
ENDFUNC

FUNCTION uf_reimppos_Navegar
	Local lcValida
	Store .f. To lcValida
	Public myFtIntroducao, myFtAlteracao
	
	For Each mf In _screen.forms
		If Upper(Alltrim(mf.name))=="ATENDIMENTO"
			lcValida=.t.
			EXIT 
		Endif
	Endfor
	If !lcValida
		If myFtIntroducao == .t. OR myFtAlteracao == .t.
			uf_perguntalt_chama("O ECR� DE FACTURA��O ENCONTRA-SE EM MODO DE INTRODU��O OU EDI��O.","OK","",48)
		Else
			SELECT uCrsPesqVendas
			If !Empty(uCrsPesqVendas.ftstamp)
				uf_facturacao_chama(Alltrim(uCrsPesqVendas.ftstamp))
			Endif
		Endif
	Else
		uf_perguntalt_chama("N�O PODE ACEDER AO ECRA DE FACTURA��O COM O ECRA DE ATENDIMENTO ABERTO.","OK","",48)
	Endif
ENDFUNC 

**
FUNCTION uf_REIMPPOS_sair
	**Fecha Cursores
	IF USED("uCrsPesqVendas")
		fecha("uCrsPesqVendas")
	ENDIF 
	
	IF USED("uCrsFt")
		fecha("uCrsFt")
	ENDIF
	IF USED("uCrsFt2")
		fecha("uCrsFt2")
	ENDIF
	IF USED("uCrsFi")
		fecha("uCrsFi")
	ENDIF
	
	REIMPPOS.hide
	REIMPPOS.release
	RELEASE REIMPPOS
ENDFUNC