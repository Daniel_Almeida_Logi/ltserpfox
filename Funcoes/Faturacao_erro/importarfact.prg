**
PUBLIC myEntidadeFact

FUNCTION uf_importarFact_Chama
	LPARAMETERS lcNo, lcTipoDoc
	
	LOCAL lcSQL, lcEfarmacia 
	STORE .f. TO lcEfarmacia 
	
	IF USED("ucrsImportDocsEntidades")
		FECHA("ucrsImportDocsEntidades")
	ENDIF

	myEntidadeFact=.f.
	** Carrega lista de documentos com bases nas permissoes
	LcSql = ""

	SELECT ft

	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_perfis_validaAcessoDocs_Importados '<<ch_userno>>', '<<ALLTRIM(ch_grupo)>>', 'Visualizar', 'BO,FT,FO', 0, '<<ALLTRIM(mySite)>>', '<<ALLTRIM(ft.nmdoc)>>', 'FT'
	ENDTEXT


	IF !uf_gerais_actGrelha("","uCrsListaDocs",lcSQL)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA NA VERIFICA��O DA LISTAGEM DE DOCUMENTOS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		return .f.
	ENDIF

    SELECT uCrsListaDocs
    
    IF RECCOUNT("uCrsListaDocs") = 0

		Text To lcSQL Textmerge noshow
			exec up_perfis_validaAcessoDocs <<ch_userno>>,'<<ch_grupo>>','visualizar','BO,FT,FO', 0, '<<ALLTRIM(mysite)>>'
		ENDTEXT

		IF !uf_gerais_actGrelha("","uCrsListaDocs",lcSQL)
			uf_perguntalt_chama("OCORREU UMA ANOMALIA NA VERIFICA��O DA LISTAGEM DE DOCUMENTOS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			return .f.
		ENDIF

	ENDIF
	
	If !USED("uCrsListaDocs")
		uf_perguntalt_chama("OCORREU UMA ANOMALIA NA VERIFICA��O DA LISTAGEM DE DOCUMENTOS. POR FAVOR CAONTACTE O SUPORTE.","OK","",16)
		return .f.
	ELSE
		IF uf_gerais_getParameter("ADM0000000142","BOOL")
			SELECT uCrsListaDocs
			APPEND BLANK 
			replace nomedoc WITH 'Reserva de Cliente'
		ENDIF 
		
		LcSql = ""
**		Text To lcSQL Textmerge noshow
**			select nmdos, ndos, bdempresas, tabela= 'BO', u_copref from ts where nmdos != 'Reserva de Cliente'
**		ENDTEXT
**		If !uf_gerais_actGrelha("","uCrsListaDocsTS",lcSQL)
**			uf_perguntalt_chama("OCORREU UMA ANOMALIA NA VERIFICA��O DA LISTAGEM DE DOCUMENTOS (DOSSIERS INTERNOS). POR FAVOR CAONTACTE O SUPORTE.","OK","",16)
**			return .f.
**		ENDIF 
**		
**		Select uCrsListaDocsTS
**		GO TOP 
**		SCAN 
**			SELECT uCrsListaDocs
**			APPEND BLANK 
**			replace uCrsListaDocs.nomedoc 		WITH uCrsListaDocsTS.nmdos
**			replace uCrsListaDocs.ndoc			WITH uCrsListaDocsTS.ndos
**			replace uCrsListaDocs.bdempresas 	WITH uCrsListaDocsTS.bdempresas 
**			replace uCrsListaDocs.tabela 		WITH uCrsListaDocsTS.tabela 
**			replace uCrsListaDocs.u_integra 	WITH .f.
**			replace uCrsListaDocs.u_copref 		WITH uCrsListaDocsTS.u_copref
**		ENDSCAN	
			
	ENDIF

	&& Valida se � farmacia
	Select uCrsE1
	IF UPPER(ALLTRIM(uCrsE1.tipoEmpresa)) == 'FARMACIA'
		lcEfarmacia = .t.
	ENDIF 
	
	
	** Exepc��o notas de Credito e Debito ARS na farmacia -> lcNo <199 AND (lcTipoDoc == 3 OR lcTipoDoc == 2) 
	IF lcNo < 199 AND (lcTipoDoc == 3 OR lcTipoDoc==2) AND lcEfarmacia == .t. && Mostra apenas documentos de entidades disponiveis para importa��o, se for farmacia
		myEntidadeFact=.t.
		Select uCrsLojas
		LOCATE FOR ALLTRIM(UPPER(ucrsLojas.site)) == ALLTRIM(UPPER(mysite))
		IF FOUND()
			select uCrsListaDocs 
			GO TOP 
			SCAN FOR uCrsListaDocs.ndoc != uCrsLojas.serie_ent AND uCrsListaDocs.ndoc != ucrsLojas.serie_entSNS 
				DELETE 
			ENDSCAN 
		ENDIF  
	ENDIF 	



	IF !USED("ucrsImportDocs")
		Create Cursor ucrsImportDocs (QTTOT n(9),Operador c(70),HORA c(20),TIPO c(254), ESTADO c(20), CABSTAMP c(25),LINSTAMP c(25), COPIAR n(1), TIPODOC c(254), DOCUMENTO c(254), data d,VALOR n(9,2))	
	ENDIF
	IF !USED("ucrsImportDocsEntidades")
		Create Cursor ucrsImportDocsEntidades (QTTOT n(9),Operador c(70),HORA c(20),TIPO c(254), ESTADO c(20), CABSTAMP c(25),LINSTAMP c(25),  COPIAR n(1), TIPODOC c(254), DOCUMENTO c(254), data d,VALOR n(9,2), descricaoCred c(60),valorCred n(9,2), tabiva n(5,0))	
	ENDIF
	
	IF !USED("ucrsImportLinhasDocs")
		Create Cursor ucrsImportLinhasDocs (stamp c(32), COPIAR n(1), ref c(18), design c(60), epv n(9,2), qtt n(9,0), etiliquido n(9,2), qttmov n(9,0), cabStamp c(25), tipo c(254), documento c(254))
	ENDIF
	
	Select uCrsListaDocs
	GO Top
		
	**	
	** Exepc��o notas de Credito e Debito ARS  na farmacia -> lcNo <199 AND (lcTipoDoc == 3 OR lcTipoDoc == 2) 
	DO FORM IMPORTARFACT WITH IIF(lcNo <199 AND (lcTipoDoc == 3 OR lcTipoDoc == 2) AND lcEfarmacia == .t.,.t.,.f.)

ENDFUNC


**
FUNCTION uf_importarFact_FiltraDocs
	SELECT uCrsListaDocs	
	IF !EMPTY(ALLTRIM(IMPORTARFACT.text1.value))
		SET FILTER TO LIKE ('*'+ALLTRIM(UPPER(IMPORTARFACT.text1.value))+'*',ALLTRIM(UPPER(uCrsListaDocs.nomeDoc)))
	ELSE
		SET FILTER TO 
	ENDIF

	IMPORTARFACT.gridListaDocs.refresh
ENDFUNC


**
FUNCTION uf_ImportarFact_ActualizaDocs
	LOCAL lcDataIni, lcDataFim, lcNo, lcSerie, lcSite, lcProd  
	STORE 0 TO lcNo
	
	IF USED("FT")
		SELECT FT
		lcNo = Ft.no
	ENDIF
		
	lcDataIni	= uf_gerais_getdate(IMPORTARFACT.dataInicio.value,"SQL")
	lcDataFim	= uf_gerais_getdate(IMPORTARFACT.DataFim.value,"SQL")
	lcSite		= IMPORTARFACT.local.value
	lcProd  = ''
	
	**pesquisa de productos
	IF VARTYPE(IMPORTARFACT.pageframe1)!='U' AND VARTYPE(IMPORTARFACT.pageframe1.page1)!='U'
		lcProd = ALLTRIM(IMPORTARFACT.pageframe1.page1.txtProd.value)
	ENDIF
	


	Select uCrsListaDocs
	
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_facturacao_ImportActualizaDocs
			<<lcNo>>
			,'<<Alltrim(importarfact.txtDocs.value)>>'
			,<<IMPORTARFACT.Forn>>
			,'<<Alltrim(lcDataIni)>>'
			,'<<Alltrim(lcDataFim)>>'
			,<<IMPORTARFACT.Aberto>>
			,'<<lcSite>>'
			,'<<Alltrim(lcProd)>>'
	ENDTEXT
	uf_gerais_actGrelha("IMPORTARFACT.pageframe1.page1.gridPesq","ucrsImportDocs",lcSQL)
		
	**Actualiza Linhas
	IF IMPORTARFACT.Pageframe1.activepage == 3 OR IMPORTARFACT.Pageframe1.activepage == 4
		uf_ImportarFact_ActualizaLinhasDocs()
	ENDIF 
	
	**Coloca Activos as defini��es do documento escolhido (Pag2)
	IF !USED("ucrsImportDocs")
		RETURN .f.
	ENDIF

	SELECT uCrsListaDocs
	IMPORTARFACT.Pageframe1.Page1.refresh
ENDFUNC


&& fun��o que extrai tipo de lote ou Renumera��o especifica do nome do documento
&& retorna vazio sen�o existir

FUNCTION uf_importarImportarFact_formataNomeRectificacao
	LPARAMETERS lcNome
	
	LOCAL lcPos, lcTexto
	STORE 0 TO lcPos
	STORE '' TO lcTexto
	
	
	lcNome=ALLTRIM(lcNome)
	
	lcpos = AT("Tipo",lcNome)
	
	IF(lcPos>0)
		lcTexto=" " + SUBSTR(lcNome, lcPos, lcpos + 6)
		RETURN lcTexto
	ENDIF
	
	lcpos = AT("Remunera��o",lcNome)
	IF(lcPos>0)
		lcTexto=" R.E."
	ENDIF
	
	RETURN lcTexto
		
ENDFUNC




**
FUNCTION uf_ImportarFact_ActualizaLinhasDocs
	Local lcSQL, lcSite
	LOCAL lcSite,lcOperador,lcOp,lcHora,lcTipo,lcEstado,lcCabStamp,lcEstado,lcDocumento,lcTipoDoc, lcData, lcTipoDocAbrev

	uv_cabs = ''

	SELECT cabStamp FROM ucrsImportDocs WITH (BUFFERING = .T.) WHERE copiar = 1 INTO CURSOR uc_cabSel

	SELECT uc_cabSel
	GO TOP
	SCAN

		uv_cabs = uv_cabs + IIF(!EMPTY(uv_cabs), ';', '') + uc_cabSel.cabStamp

		SELECT uc_cabSel
	ENDSCAN
	

	lcSQL = ""
	lcSite = IMPORTARFACT.local.value
		
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_facturacao_ImportActualizaLinhas <<IMPORTARFACT.qttMaiorZero>>, <<IMPORTARFACT.qttMovMenorQtt>>, '<<Alltrim(uv_cabs)>>', '<<lcSite>>'
	ENDTEXT


	uf_gerais_actGrelha("IMPORTARFACT.pageframe1.page1.grid1","ucrsImportLinhasDocs",lcSQL)
	

	select ucrsImportDocsEntidades
	GO top
	SCAN 
		DELETE
	ENDSCAN 
	Select uCrsListaDocs
	lcSerie = uCrsListaDocs.ndoc
	
	SELECT * FROM ucrsImportDocs WITH (BUFFERING = .T.) INTO CURSOR uc_cabSel READWRITE
	
	Select ucrsImportLinhasDocs
	GO Top
	SCAN 
		lcTipoDocAbrev =  uf_importarImportarFact_formataNomeRectificacao(ucrsImportLinhasDocs.design)
	
		SELECT uc_cabSel
		LOCATE FOR ALLTRIM(uc_cabSel.cabStamp) == ALLTRIM(ucrsImportLinhasDocs.cabStamp)

		Select ucrsImportDocsEntidades 
		APPEND BLANK
		Replace ucrsImportDocsEntidades.QTTOT 			WITH ucrsImportLinhasDocs.qtt 
		Replace ucrsImportDocsEntidades.Operador		WITH uc_cabSel.Operador 
		Replace ucrsImportDocsEntidades.HORA 			WITH uc_cabSel.HORA
		Replace ucrsImportDocsEntidades.TIPO 			WITH uc_cabSel.TIPO 
		Replace ucrsImportDocsEntidades.ESTADO 			WITH uc_cabSel.ESTADO
		Replace ucrsImportDocsEntidades.LINSTAMP 		WITH ucrsImportLinhasDocs.fistamp 
		Replace ucrsImportDocsEntidades.COPIAR 			WITH ucrsImportLinhasDocs.COPIAR 
		Replace ucrsImportDocsEntidades.TIPODOC 		WITH ucrsImportLinhasDocs.design
		Replace ucrsImportDocsEntidades.DOCUMENTO 		WITH ucrsImportLinhasDocs.documento 
		Replace ucrsImportDocsEntidades.data 			WITH uc_cabSel.data
		Replace ucrsImportDocsEntidades.VALOR 			WITH ucrsImportLinhasDocs.etiliquido
		Replace ucrsImportDocsEntidades.descricaoCred   WITH "Retifica��o � Fatura N� " +  ALLTRIM(uc_cabSel.DOCUMENTO) +" S�rie " + ASTR(lcSerie) + lcTipoDocAbrev  +" (" + uf_gerais_getdate(uc_cabSel.data ) +")"
		Replace ucrsImportDocsEntidades.valorCred 	 	WITH 0
		Replace ucrsImportDocsEntidades.tabiva 	 		WITH ucrsImportLinhasDocs.tabiva
		Replace ucrsImportDocsEntidades.cabStamp 		WITH ucrsImportLinhasDocs.cabStamp

	ENDSCAN 
	Select ucrsImportDocsEntidades
	GO TOP
	IMPORTARFACT.pageframe1.page1.gridEnt.refresh

	IF USED("uc_linSelFac")
		SELECT uc_linSelFac
		GO TOP
		SCAN

			SELECT ucrsImportLinhasDocs
			LOCATE FOR ALLTRIM(ucrsImportLinhasDocs.stamp) = ALLTRIM(uc_linSelFac.stamp)

			IF FOUND()
				REPLACE ucrsImportLinhasDocs.copiar WITH 1 
			ELSE
				SELECT uc_linSelFac
				DELETE 
			ENDIF

			SELECT uc_linSelFac
		ENDSCAN
	ENDIF

	IF USED("uc_linSelEnt")
		SELECT uc_linSelEnt
		GO TOP
		SCAN

			SELECT ucrsImportDocsEntidades
			LOCATE FOR ALLTRIM(ucrsImportDocsEntidades.linStamp) = ALLTRIM(uc_linSelEnt.stamp)

			IF FOUND()
				REPLACE ucrsImportDocsEntidades.copiar WITH 1 
			ELSE
				SELECT uc_linSelEnt
				DELETE 
			ENDIF

			SELECT uc_linSelEnt
		ENDSCAN
	ENDIF

	**Actualiza combo
	SELECT ucrsImportDocs
	**IMPORTARFACT.Pageframe1.Page3.documento.value = ALLTRIM(ucrsImportDocs.tipoDoc) + ":" + ALLTRIM(ucrsImportDocs.Documento)
ENDFUNC



**
FUNCTION uf_ImportarFact_ImportarLinhas
	IF !USED("ucrsImportDocs") OR !USED("ucrsImportLinhasDocs")
		RETURN .f.
	ENDIF
	
	regua(0,1,"A importar linhas....")	
	
	Local lcValida
	Store .f. To lcValida
	
	importarfact.lordem = uf_importarFact_maxLordem()
	
	Local lcValida
	Store .f. To lcValida
	
	uv_mudouCab = .T.
	uv_cabStamp = ""

	Select ucrsImportLinhasDocs
	Scan FOR ucrsImportLinhasDocs.copiar == 1


		IF EMPTY(uv_cabStamp) OR (ALLTRIM(uv_cabStamp) <> ALLTRIM(ucrsImportLinhasDocs.cabStamp))

			uv_mudouCab = .T.
			uv_cabStamp = ucrsImportLinhasDocs.cabStamp

		ELSE
			uv_mudouCab = .F.
		ENDIF

		If IMPORTARFACT.incluinome == 1 AND uv_mudouCab

			select * FROM ucrsImportDocs WITH (BUFFERING = .T.) WHERE ALLTRIM(ucrsImportDocs.cabstamp) = ALLTRIM(ucrsImportLinhasDocs.cabStamp) INTO CURSOR uc_cabSel READWRITE

			Select FI
			Append Blank
			REPLACE Fi.LORDEM	With	importarfact.lordem

			IF ALLTRIM(uc_cabSel.tipodoc) == 'Venda a Dinheiro'
				REPLACE Fi.design	WITH 'Fatura' + ' Nr. ' + ALLTRIM(uc_cabSel.documento) + '/3 de ' + uf_gerais_getdate(uc_cabSel.DATA)
			ELSE
				REPLACE Fi.design	WITH ALLTRIM(uc_cabSel.tipodoc) + ' Nr. ' + ALLTRIM(uc_cabSel.documento) + ' de ' + uf_gerais_getdate(uc_cabSel.DATA)
			ENDIF


			*VALORES POR DEFEITO
			** FISTAMP
			lcStamp = uf_gerais_stamp()

			SELECT FI
			
			REPLACE FI.FISTAMP 	With	lcStamp
			REPLACE FI.NMDOC 	With	Alltrim(Ft.Nmdoc)
			Replace Fi.ndoc		With	Ft.ndoc
			REPLACE FI.FNO 		With	Ft.Fno

			**IVA
			REPLACE FI.IVA With uf_gerais_getTabelaIVA(1, "TAXA")
			REPLACE FI.TABIVA With 1
			
			**ARMAZEM (Variavel publica criada no startup)
			REPLACE FI.ARMAZEM With myarmazem
		ENDIF

		* CHAMA FUN�AO CRIA LINHAS
		If ALLTRIM(ucrsImportLinhasDocs.tipo) = "FT" 
			uf_importarFact_crialinhas_individuaisFTFT(ucrsImportLinhasDocs.STAMP)
		Endif
		
		If ALLTRIM(ucrsImportLinhasDocs.tipo) = "BO" 
			uf_importarFact_crialinhas_individuaisBOFT(ucrsImportLinhasDocs.STAMP)
		Endif

		If ALLTRIM(ucrsImportLinhasDocs.tipo) = "FO" 
			uf_importarFact_crialinhas_individuaisFOFT(ucrsImportLinhasDocs.STAMP)
		Endif

		IF ucrsImportDocs.tipo = "TARV"
			uf_importarFact_crialinhas_individuaisTARVFT(ALLTRIM(ucrsImportLinhasDocs.STAMP), ALLTRIM(ucrsImportDocs.cabstamp))
		ENDIF
									
		lcValida=.t.
		
		SELECT ucrsImportLinhasDocs
	Endscan
	
	regua(2)
	
	If lcValida

		uf_atendimento_actValoresFi()
		uf_atendimento_actTotaisFt()

		Facturacao.refresh
		
		uf_importarFact_sair()
	Endif

ENDFUNC


**
FUNCTION uf_ImportarFact_SelTodos
	LPARAMETERS lcBool
	
	DO CASE
		CASE importarfact.pageframe1.activepage = 1
			SELECT ucrsImportDocs
			GO top 
			SCAN
				IF lcBool
					replace ucrsImportDocs.copiar WITH 1
				ELSE
					replace ucrsImportDocs.copiar WITH 0
				ENDIF 
			ENDSCAN 
			GO TOP 
		CASE importarfact.pageframe1.activepage = 3
			SELECT ucrsImportLinhasDocs
			GO TOP 
			SCAN
				IF lcBool
					replace ucrsImportLinhasDocs.copiar WITH 1
				ELSE
					replace ucrsImportLinhasDocs.copiar WITH 0
				ENDIF 
			ENDSCAN
			GO TOP 
		OTHERWISE
			RETURN .f.
	ENDCASE
	
	IF lcBool
		importarfact.menu1.selTodos.config("Des. Todos", myPath + "\imagens\icons\checked_w.png", "uf_ImportarFact_SelTodos with .f.","T")
	ELSE
		importarfact.menu1.selTodos.config("Sel. Todos", myPath + "\imagens\icons\unchecked_w.png", "uf_ImportarFact_SelTodos with .t.","T")
	ENDIF 
ENDFUNC


**
Function uf_ImportarFact_Importar
	Local lcOrigem, lcDestino, lcvalida

	LOCAL lcStamp
	
	** se tentar importar uma factura a entidades
	IF(myEntidadeFact)
		uf_perguntalt_chama("Por favor escolha uma linha da fatura","OK","",16)
		RETURN .f.
	ENDIF
	
	** Se s� tiver uma linha vazia elimina-a
	Select Fi
	COUNT TO lcNrRegistosFi
	IF lcNrRegistosFi == 1 AND EMPTY(Fi.ref) AND EMPTY(Fi.design) AND EMPTY(Fi.qtt)
		DELETE FROM Fi WHERE EMPTY(Fi.ref) AND EMPTY(Fi.design) AND EMPTY(Fi.qtt)
	ENDIF


	Store .f. To lcValida
	Select ucrsImportDocs
	Go top
	Scan
		If ucrsImportDocs.copiar == 1
			importarfact.lordem = uf_importarFact_maxLordem()

			** CONTROLA NOME DO FICHEIRO DE ORIGEM
			If IMPORTARFACT.incluinome == 1
				Select FI
				Append Blank
				REPLACE Fi.LORDEM	With	importarfact.lordem

				IF ALLTRIM(ucrsImportDocs.tipodoc) == 'Venda a Dinheiro'
					REPLACE Fi.design	WITH 'Fatura' + ' Nr. ' + ALLTRIM(ucrsImportDocs.documento) + '/3 de ' + uf_gerais_getdate(ucrsImportDocs.DATA)
				ELSE
					REPLACE Fi.design	WITH ALLTRIM(ucrsImportDocs.tipodoc) + ' Nr. ' + ALLTRIM(ucrsImportDocs.documento) + ' de ' + uf_gerais_getdate(ucrsImportDocs.DATA)
				ENDIF


				*VALORES POR DEFEITO
				** FISTAMP
				lcStamp = uf_gerais_stamp()

				SELECT FI
				
				REPLACE FI.FISTAMP 	With	lcStamp
				REPLACE FI.NMDOC 	With	Alltrim(Ft.Nmdoc)
				Replace Fi.ndoc		With	Ft.ndoc
				REPLACE FI.FNO 		With	Ft.Fno

				**IVA
				REPLACE FI.IVA With uf_gerais_getTabelaIVA(1, "TAXA")
				REPLACE FI.TABIVA With 1
				
				**ARMAZEM (Variavel publica criada no startup)
				REPLACE FI.ARMAZEM With myarmazem
			ENDIF
				
			* CHAMA FUN�AO CRIA LINHAS
			DO CASE
				CASE ucrsImportDocs.tipo = "FT" 
					uf_importarFact_crialinhas_compFTFT(ucrsImportDocs.CABSTAMP)
					lcValida=.t.
					
				CASE ucrsImportDocs.tipo == "BO" 
					uf_importarFact_crialinhas_compBOFT(ucrsImportDocs.CABSTAMP)
					lcValida=.t.
					
				CASE ucrsImportDocs.tipo == "TARV" 
					uf_importarFact_crialinhas_compTARVFT(ucrsImportDocs.CABSTAMP)
					lcValida=.t.				
			ENDCASE 
		Endif
	ENDSCAN
	

	If lcValida == .t.

		SELECT fi
		GO TOP
		SCAN

			uf_atendimento_fiiliq(.t.)
			
			IF uf_gerais_getParameter("ADM0000000211","BOOL") == .f.
				uf_atendimento_actValoresFi()
			ENDIF
		
			SELECT fi
		ENDSCAN


		**					
		IF uf_gerais_getParameter("ADM0000000211","BOOL") == .t. && Atribui automaticamente os Lotes
			UF_FACTURACAO_AtribuiLotesDocumento()
		ENDIF
		uf_atendimento_actValoresFi()
		uf_atendimento_actTotaisFt()
		uf_importarFact_sair()

	Endif

ENDFUNC 


FUNCTION uf_ImportarFact_validaDadosEntidadeImportar

	
	IF(!USED("ucrsImportDocsEntidades"))
		RETURN .f.
	ENDIF
	
	&&valida se alguma linha esta marcada para copiar
	SELECT  ucrsImportDocsEntidades
	GO TOP
	LOCATE FOR ucrsImportDocsEntidades.copiar == 1
	IF !FOUND()
		RETURN .f.
	ENDIF
	

	&&valida se as linhas amrcadas para copiar teem valor a creditar=0
	SELECT  ucrsImportDocsEntidades
	GO TOP
	LOCATE FOR ucrsImportDocsEntidades.copiar == 1 AND ucrsImportDocsEntidades.valorCred = 0	
	IF FOUND()
		RETURN .f.
	ENDIF
	
	RETURN .t.
		
ENDFUNC


**
FUNCTION uf_ImportarFact_ImportarFacturasEntidades
	Local lcOrigem, lcDestino, lcvalida
	Store .f. To lcValida
	
	LOCAL lcStamp
	
	** Se s� tiver uma linha vazia elimina-a
	Select Fi
	COUNT TO lcNrRegistosFi
	IF lcNrRegistosFi == 1 AND EMPTY(Fi.ref) AND EMPTY(Fi.design) AND EMPTY(Fi.qtt)
		DELETE FROM Fi WHERE EMPTY(Fi.ref) AND EMPTY(Fi.design) AND EMPTY(Fi.qtt)
	ENDIF
	
	&&valida se os dados est�o prontos a importar
	IF(!uf_ImportarFact_validaDadosEntidadeImportar())
		uf_perguntalt_chama("Selecione as linhas a importar e preencha o respetivo valor a creditar/debitar na coluna 'Valor C'","OK","",16)
		RETURN .f.
	ENDIF

	uv_credToDeb = .F.

	IF INLIST(ALLTRIM(ucrsImportDocs.tipodoc), 'Nota de Cr�dito', 'N.Cr�dito SNS Comp', 'N.Cr�dito SNS Fee') AND INLIST(ALLTRIM(ft.nmdoc), 'Nota de D�bito', 'N.D�bito SNS Fee', 'N.D�bito SNS Comp')
		uv_credToDeb = .T.
	ENDIF

	IF uv_credToDeb

		Select FI
		Append Blank
		REPLACE Fi.LORDEM	With	importarfact.lordem

		REPLACE Fi.design	WITH ALLTRIM(ucrsImportDocs.tipodoc) + ' Nr. ' + ALLTRIM(ucrsImportDocs.documento) + ' de ' + uf_gerais_getdate(ucrsImportDocs.DATA)

		*VALORES POR DEFEITO
		** FISTAMP
		lcStamp = uf_gerais_stamp()

		SELECT FI
		
		REPLACE FI.FISTAMP 	With	lcStamp
		REPLACE FI.NMDOC 	With	Alltrim(Ft.Nmdoc)
		Replace Fi.ndoc		With	Ft.ndoc
		REPLACE FI.FNO 		With	Ft.Fno

		**IVA
		REPLACE FI.IVA With uf_gerais_getTabelaIVA(1, "TAXA")
		REPLACE FI.TABIVA With 1
		
		**ARMAZEM (Variavel publica criada no startup)
		REPLACE FI.ARMAZEM With myarmazem
	ENDIF
	
	Select ucrsImportDocsEntidades
	Go top
	SCAN FOR ucrsImportDocsEntidades.copiar == 1 AND ucrsImportDocsEntidades.valorCred <> 0
		importarfact.lordem = uf_importarFact_maxLordem()
		
		** CONTROLA NOME DO FICHEIRO DE ORIGEM
		Select FI
		APPEND BLANK
		REPLACE Fi.LORDEM	WITH importarfact.lordem

		*VALORES POR DEFEITO
		** FISTAMP
		lcStamp = uf_gerais_stamp()

		SELECT FI
		REPLACE FI.FISTAMP 	WITH lcStamp
		REPLACE FI.FTSTAMP 	WITH Ft.ftstamp
		REPLACE FI.NMDOC 	WITH Alltrim(Ft.Nmdoc)
		Replace Fi.ndoc		WITH Ft.ndoc
		REPLACE FI.FNO 		WITH Ft.Fno
		REPLACE Fi.ref 		WITH "9999998"
		REPLACE Fi.qtt 		WITH 1
		REPLACE Fi.design 	WITH IIF(!uv_credToDeb, ALLTRIM(ucrsImportDocsEntidades.descricaoCred), ALLTRIM(ucrsImportDocsEntidades.tipoDoc))
		REPLACE Fi.etiliquido WITH IIF(!uv_credToDeb, ucrsImportDocsEntidades.valorCred, ABS(ucrsImportDocsEntidades.valorCred))
		REPLACE Fi.tiliquido WITH IIF(!uv_credToDeb, ucrsImportDocsEntidades.valorCred, ABS(ucrsImportDocsEntidades.valorCred)) * 200.482
		REPLACE	FI.EPV WITH IIF(!uv_credToDeb, ucrsImportDocsEntidades.valorCred, ABS(ucrsImportDocsEntidades.valorCred))
		REPLACE	fi.u_epvp WITH IIF(!uv_credToDeb, ucrsImportDocsEntidades.valorCred, ABS(ucrsImportDocsEntidades.valorCred))
		&& Liga��o a uma linha do documento
		REPLACE	FI.ofistamp WITH ALLTRIM(ucrsImportDocsEntidades.linstamp)

		**IVA
		REPLACE FI.IVA With uf_gerais_getTabelaIVA(ucrsImportDocsEntidades.tabiva, "TAXA")
		REPLACE FI.TABIVA With ucrsImportDocsEntidades.tabiva
		REPLACE FI.ivaincl With .t.
		&& N�o atualiza Informa��o da Linha com dados actuais da referencia
		Replace Fi.amostra 		With .t.
		**
		REPLACE FI.ARMAZEM With myarmazem

		If Ft.tipoDoc == 3 And Fi.etiliquido > 0
			Replace Fi.etiliquido With Fi.etiliquido * -1
			Replace Fi.tiliquido With Fi.tiliquido * -1
		ENDIF
		
		REPLACE FI.ivaincl With .t.
		
		uf_atendimento_fiiliq(.t.)
		uf_atendimento_actValoresFi()
		uf_atendimento_actTotaisFt()
	
		Select ucrsImportDocsEntidades
	ENDSCAN 
	
	uf_importarFact_sair()
	
ENDFUNC 


**
FUNCTION uf_importarfact_validaValorCredito
	Select ucrsImportDocsEntidades
	IF ucrsImportDocsEntidades.valorCred > ucrsImportDocsEntidades.valor
		Select ucrsImportDocsEntidades
		Replace ucrsImportDocsEntidades.valorCred WITH 0
		
		uf_perguntalt_chama("O valor a creditar/debitar n�o pode ser superior ao valor da Fatura.","OK","",32)
	ENDIF 
ENDFUNC


**
FUNCTION uf_importarFact_crialinhas_compFTFT
	LPARAMETERS cabstamp
	
	Local lcSQL
	
	** 	Variavel Pbruto usada para guardar a Quantidade Movimentada, actualizada em eventos ap�s grava��o dos Documentos
	
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		Select	
			qttmov = fi.Pbruto
			,fi.fistamp
		From 	
			FI (nolock) 
		Where 
			FTSTAMP = '<<Alltrim(cabstamp)>>'
		Order by
			lordem	
	Endtext
	If uf_gerais_actGrelha("","uc_LinhasFt",lcSQL) And Reccount("uc_LinhasFt") > 0
		Select uc_LinhasFt
		Go top
		Scan
			uf_importarFact_crialinhas_individuaisFTFT(uc_LinhasFt.fistamp)
		Endscan
	Endif
ENDFUNC


**
Function uf_importarFact_crialinhas_compBOFT
	LPARAMETERS cabstamp
	
	Local lcSQL
	
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		Select 
			bistamp
		From 
			BI (nolock) 
		Where 
			BOSTAMP = '<<Alltrim(cabstamp)>>'
		order by
			lordem
	Endtext
	If !uf_gerais_actGrelha("","uc_ImportDossier",lcSQL) 
		uf_perguntalt_chama("DESCULPE, N�O FOI POSS�VEL IDENTIFICAR AS DEFNI��ES DAS LINHAS A IMPORTAR. POR FAVOR CONTACTE A ASSIST�NCIA T�CNICA.","OK","",16)
		RETURN .f.
	Endif
	Select uc_ImportDossier
	Go top
	Scan
		uf_importarFact_crialinhas_individuaisBOFT(uc_ImportDossier.bistamp)
	Endscan	
	
ENDFUNC 


**
FUNCTION uf_importarFact_crialinhas_compTARVFT
	LPARAMETERS cabstamp
	Local lcSQL
	
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		Select 
			id
		From 
			ext_tarv_prescricao (nolock) 
		Where 
			id_ext_tarv = '<<Alltrim(cabstamp)>>'
		order by
			id
	Endtext
	If !uf_gerais_actGrelha("","uc_ImportTarv",lcSQL) 
		uf_perguntalt_chama("DESCULPE, N�O FOI POSS�VEL IDENTIFICAR AS DEFNI��ES DAS LINHAS A IMPORTAR. POR FAVOR CONTACTE A ASSIST�NCIA T�CNICA.","OK","",16)
		RETURN .f.
	else
		Select uc_ImportTarv
		Go top
		Scan
			uf_importarFact_crialinhas_individuaisTARVFT(ALLTRIM(uc_ImportTarv.id), Alltrim(cabstamp))
		ENDSCAN
		
		fecha("uc_ImportTarv")
	ENDIF
ENDFUNC


**
Function uf_importarFact_crialinhas_individuaisFTFT
	lparameters linstamp

	Local novaqtt
	** 	Variavel Pbruto usada para guardar a Quantidade Movimentada, actualizada em eventos ap�s grava��o dos Documentos
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#DadosProduto'))
		    DROP TABLE #DadosProduto
			
		select 
			st.ref
			,st.usalote
			,pic = isnull(fprod.pvporig,0)
		into
			#DadosProduto
		from
			st (nolock)
			left join fprod (nolock) on fprod.cnp = st.ref
		where
			 st.site_nr = <<mysite_nr>>

		SELECT
			usalote = isnull(#DadosProduto.usalote,CONVERT(bit,0))
			,pic = isnull(#DadosProduto.pic,0)
			,fi.Pbruto as qttmov
			,fi.*
		From
			FI (nolock) 
			left join #DadosProduto on fi.ref = #DadosProduto.ref
		Where 	
			fi.fistamp = '<<Alltrim(linstamp)>>'
		
		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#DadosProduto'))
		DROP TABLE #DadosProduto
	ENDTEXT

	If uf_gerais_actGrelha("","tempSQL_FNCMP",lcSQL) And Reccount("tempSQL_FNCMP") > 0

		LOCAL lcStamp
		Select tempSQL_FNCMP
		Scan
		
			**CONTROLA QUANTIDADE MOVIMENTADA > QUANTIDADE - COPIA APENAS A DIFERENCA
			novaqtt = tempSQL_FNCMP.qtt - tempSQL_FNCMP.qttmov 

            Select FI
            Append Blank
    
            *VALORES POR DEFEITO
            ** FNSTAMP
            lcStamp = uf_gerais_stamp()
            SELECT FI
            REPLACE FI.FISTAMP 		With 	lcStamp
            REPLACE fi.design 		WITH  	tempSQL_FNCMP.design
            REPLACE FI.NMDOC 		With 	Alltrim(Ft.nmdoc)
            REPLACE FI.NDOC 		With 	Ft.ndoc
            REPLACE FI.FNO	 		With 	Ft.Fno
            REPLACE FI.lordem		With 	importarfact.lordem
            
            * Centro Analitico
            REPLACE FI.ficcusto		With 	tempSQL_FNCMP.ficcusto
            
            * Controla Movimenta Stock
            Select uCrsListaDocs
            If IMPORTARFACT.MOVSTOCK = 1
                SELECT FI
                REPLACE Fi.ref		With 	IIF(EMPTY(ALLTRIM(tempSQL_FNCMP.ref)),ALLTRIM(tempSQL_FNCMP.oref),ALLTRIM(tempSQL_FNCMP.ref))
            Endif					
                
            If novaqtt > 0
                SELECT FI
                REPLACE	FI.qtt	With 	novaqtt
                REPLACE	FI.EPV	With  	tempSQL_FNCMP.EPV
                                            
                If tempSQL_FNCMP.qtt !=  tempSQL_FNCMP.qttmov
                    SELECT FI
                    REPLACE	FI.ETILIQUIDO	WITH  tempSQL_FNCMP.EPV * novaqtt 
                    REPLACE	FI.TILIQUIDO	WITH  tempSQL_FNCMP.EPV * novaqtt * 200.248
                Else
                    SELECT FI
                    REPLACE	FI.ETILIQUIDO	WITH  tempSQL_FNCMP.ETILIQUIDO
                    REPLACE	FI.TILIQUIDO	WITH  tempSQL_FNCMP.ETILIQUIDO * 200.248
                Endif
            Else
                SELECT FI
                REPLACE	FI.qtt 		WITH 	0
                REPLACE	FI.ETILIQUIDO	WITH 	0
            ENDIF
            
            SELECT FI
            REPLACE fi.iva 		WITH  tempSQL_FNCMP.iva
            REPLACE fi.tabiva 	WITH  IIF(tempSQL_FNCMP.tabiva = 0, uf_gerais_getUmValor("taxasIva", "codigo", "taxa = 0", "codigo asc"), tempSQL_FNCMP.tabiva)
            REPLACE fi.armazem 	WITH  tempSQL_FNCMP.armazem
            
            ** Campos FI ***********************************
            
            SELECT FI
            Replace Fi.unidade 	With tempSQL_FNCMP.unidade
            Replace Fi.unidad2 	With tempSQL_FNCMP.unidad2
            Replace Fi.iva 		With tempSQL_FNCMP.iva
            Replace Fi.ivaincl 	With tempSQL_FNCMP.ivaincl
            Replace Fi.codigo 	With tempSQL_FNCMP.codigo
            **Replace Fi.tabiva 	With tempSQL_FNCMP.tabiva
            Replace Fi.fnoft 	With tempSQL_FNCMP.fnoft
            Replace Fi.ndocft 	With tempSQL_FNCMP.ndocft
            Replace Fi.ftanoft 	With tempSQL_FNCMP.ftanoft
            Replace Fi.lobs2 	With tempSQL_FNCMP.lobs2
            Replace Fi.litem2 	With tempSQL_FNCMP.litem2
            Replace Fi.litem 	With tempSQL_FNCMP.litem
            Replace Fi.lobs3 	With tempSQL_FNCMP.lobs3
            Replace Fi.cpoc 	With tempSQL_FNCMP.cpoc
            Replace Fi.composto With tempSQL_FNCMP.composto
            Replace Fi.lrecno 	With tempSQL_FNCMP.lrecno
            Replace Fi.partes 	With tempSQL_FNCMP.partes
            Replace Fi.altura 	With tempSQL_FNCMP.altura
            Replace Fi.largura 	With tempSQL_FNCMP.largura
            Replace Fi.oref With IIF(EMPTY(ALLTRIM(tempSQL_FNCMP.ref)),ALLTRIM(tempSQL_FNCMP.oref),ALLTRIM(tempSQL_FNCMP.ref))
            Replace Fi.lote With tempSQL_FNCMP.lote
            Replace Fi.usr1 With tempSQL_FNCMP.usr1
            Replace Fi.usr2 With tempSQL_FNCMP.usr2
            Replace Fi.usr3 With tempSQL_FNCMP.usr3
            Replace Fi.usr4 With tempSQL_FNCMP.usr4
            Replace Fi.usr5 With tempSQL_FNCMP.usr5
            Replace Fi.cor 	With tempSQL_FNCMP.cor
            Replace Fi.tam 	With tempSQL_FNCMP.tam
            Replace Fi.stipo With tempSQL_FNCMP.stipo
            Replace Fi.fifref With tempSQL_FNCMP.fifref
            Replace Fi.familia With tempSQL_FNCMP.familia
            Replace Fi.pbruto With tempSQL_FNCMP.pbruto
            Replace Fi.bistamp With tempSQL_FNCMP.bistamp
            Replace Fi.stns With tempSQL_FNCMP.stns
            Replace Fi.fincusto With tempSQL_FNCMP.fincusto
            Replace Fi.ofistamp With tempSQL_FNCMP.fistamp
            Replace Fi.pv With tempSQL_FNCMP.pv
            Replace Fi.pvmoeda With tempSQL_FNCMP.pvmoeda
            Replace Fi.epv With tempSQL_FNCMP.epv
            Replace Fi.tmoeda With tempSQL_FNCMP.tmoeda
            Replace Fi.eaquisicao With tempSQL_FNCMP.eaquisicao
            
            ** Custo - Actualiza��o com Valores de Custo Actuais
            lcSQL = ""
            TEXT TO lcSQL TEXTMERGE NOSHOW
                Select PCPOND,EPCPOND, epcult From ST (nolock) Where (Ref = '<<Alltrim(tempSQL_FNCMP.ref)>>' Or ref = '<<Alltrim(tempSQL_FNCMP.oref)>>') and site_nr=<<mysite_nr>>
            ENDTEXT 
            If uf_gerais_actGrelha("","uc_PRECOCUSTO",lcSQL) 
                SELECT FI
                Replace Fi.custo With uc_PRECOCUSTO.epcult * 200.482
                Replace Fi.ecusto With uc_PRECOCUSTO.epcult 
            Endif

            
            SELECT FI
            Replace Fi.slvu With tempSQL_FNCMP.slvu
            Replace Fi.eslvu With tempSQL_FNCMP.eslvu
            Replace Fi.sltt With tempSQL_FNCMP.sltt
            Replace Fi.esltt With tempSQL_FNCMP.esltt
            Replace Fi.slvumoeda With tempSQL_FNCMP.slvumoeda
            Replace Fi.slttmoeda	With tempSQL_FNCMP.slttmoeda
            Replace Fi.nccod 		With tempSQL_FNCMP.nccod
            Replace Fi.ncinteg 		With tempSQL_FNCMP.ncinteg
            Replace Fi.epromo 		With tempSQL_FNCMP.epromo
            Replace Fi.desconto 	With tempSQL_FNCMP.desconto
            Replace Fi.desc2 		With tempSQL_FNCMP.desc2
            Replace Fi.desc3 		With tempSQL_FNCMP.desc3
            Replace Fi.desc4 		With tempSQL_FNCMP.desc4
            Replace Fi.desc5 		With tempSQL_FNCMP.desc5
            Replace Fi.desc6 		With tempSQL_FNCMP.desc6
            Replace Fi.iectin 		With tempSQL_FNCMP.iectin
            Replace Fi.eiectin 		With tempSQL_FNCMP.eiectin
            Replace Fi.vbase		With tempSQL_FNCMP.vbase
            Replace Fi.evbase 		With tempSQL_FNCMP.evbase
            Replace Fi.tliquido 	With tempSQL_FNCMP.tliquido
            Replace Fi.num1			With tempSQL_FNCMP.num1
            Replace Fi.pcp 			With tempSQL_FNCMP.pcp
            Replace Fi.epcp 		With tempSQL_FNCMP.epcp
            Replace Fi.pvori 		With tempSQL_FNCMP.pvori
            Replace Fi.epvori 		With tempSQL_FNCMP.epvori
            Replace Fi.szzstamp		With tempSQL_FNCMP.szzstamp
            Replace Fi.zona 		With tempSQL_FNCMP.zona
            Replace Fi.morada 		With tempSQL_FNCMP.morada
            Replace Fi.local 		With tempSQL_FNCMP.local
            Replace Fi.codpost 		With tempSQL_FNCMP.codpost
            Replace Fi.telefone 	With tempSQL_FNCMP.telefone
            Replace Fi.email 		With tempSQL_FNCMP.email
            Replace Fi.tkhposlstamp With tempSQL_FNCMP.tkhposlstamp
            Replace Fi.marcada 		With tempSQL_FNCMP.marcada
            Replace Fi.amostra 		With .t.
            Replace Fi.u_epref 		With tempSQL_FNCMP.u_epref
            Replace Fi.pic 			With tempSQL_FNCMP.pic
            Replace Fi.u_stock 		With tempSQL_FNCMP.u_stock
            Replace Fi.u_ip 		With tempSQL_FNCMP.u_ip
            Replace Fi.u_epvp 		With tempSQL_FNCMP.u_epvp
            Replace Fi.u_genalt 	With tempSQL_FNCMP.u_genalt
            Replace Fi.u_generico 	With tempSQL_FNCMP.u_generico
            Replace Fi.u_ettent1 	With tempSQL_FNCMP.u_ettent1
            Replace Fi.u_ettent2 	With tempSQL_FNCMP.u_ettent2
            Replace Fi.u_psicont 	With tempSQL_FNCMP.u_psicont
            Replace Fi.u_bencont 	With tempSQL_FNCMP.u_bencont
            Replace Fi.u_psico 		With tempSQL_FNCMP.u_psico
            Replace Fi.u_benzo 		With tempSQL_FNCMP.u_benzo
            Replace Fi.u_txcomp 	With tempSQL_FNCMP.u_txcomp
            Replace Fi.u_cnp 		With tempSQL_FNCMP.u_cnp
            Replace Fi.u_comp 		With tempSQL_FNCMP.u_comp
            Replace Fi.u_nbenef 	With tempSQL_FNCMP.u_nbenef
            Replace Fi.u_nbenef2 	With tempSQL_FNCMP.u_nbenef2
            Replace Fi.u_diploma 	With tempSQL_FNCMP.u_diploma
            Replace Fi.u_refvale 	With tempSQL_FNCMP.u_refvale
            Replace Fi.u_robot 		With tempSQL_FNCMP.u_robot
            Replace Fi.u_descval 	With tempSQL_FNCMP.u_descval 
            REPLACE Fi.usalote		WITH tempSQL_FNCMP.usalote
            REPLACE Fi.pic			WITH tempSQL_FNCMP.pic
            
            ** Verifica se o Documento � do Tipo Nota de Credito 3 e Coloca o sinal negativo
            Select Ft
            If Ft.tipoDoc == 3 And Fi.etiliquido > 0
                SELECT FI
                Replace Fi.etiliquido With Fi.etiliquido * -1
                Replace Fi.tiliquido With Fi.tiliquido * -1
            Endif
            
            If Ft.tipoDoc == 1 And Fi.etiliquido < 0
                SELECT FI
                Replace Fi.etiliquido With Fi.etiliquido * -1
                Replace Fi.tiliquido With Fi.tiliquido * -1
            Endif
            Select tempSQL_FNCMP
            ***********************************************
            importarfact.lordem = importarfact.lordem + 1
		Endscan
	Endif
ENDFUNC


**
Function uf_importarFact_crialinhas_individuaisBOFT
	LPARAMETERS linstamp
	
	Local novaqtt, lcArmazem, lcStamp 
	lcArmazem = 1
	
	** 	Variavel Pbruto usada para guardar a Quantidade Movimentada, actualizada em eventos ap�s grava��o dos Documentos
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		Select 	
			bi.*
			,bi.Pbruto as qttmov
			,usalote = isnull(st.usalote,CONVERT(bit,0))
			,epv1 = isnull(st.epv1,0)
			,pic = isnull(fprod.pvporig,0)
			,psico = ISNULL(psico, convert(BIT,0))
			,benzo = ISNULL(benzo, convert(BIT,0))
		From 
			BI (nolock)		
			LEFT JOIN st (nolock) ON bi.ref = st.ref
			left join fprod (nolock) on fprod.cnp = st.ref
		Where
			Bi.bistamp = '<<Alltrim(linstamp)>>'
			and st.site_nr = <<mysite_nr>>
	Endtext
			
	If !uf_gerais_actGrelha("","tempSQL_BICMP",lcSQL) 
		uf_perguntalt_chama("N�O FOI POSS�VEL IDENTIFICAR AS DEFINI��ES DAS LINHAS A IMPORTAR. POR FAVOR CONTACTE A ASSIST�NCIA T�CNICA.","OK","",16)	
		RETURN .f.
	ENDIF
			
	Select tempSQL_BICMP
	Scan
		** CONTROLA QUANTIDADE MOVIMENTADA > QUANTIDADE - COPIA APENAS A DIFERENCA
		
		IF tempSQL_BICMP.ndos != 43 
			novaqtt = tempSQL_BICMP.qtt - tempSQL_BICMP.qttmov 
		ELSE && Se for confirma��o de encomenda de cliente, considera a quantidade recebida - qtmov
			novaqtt = tempSQL_BICMP.qtrec - tempSQL_BICMP.qtt2
		ENDIF
		
        Select FI
        Append Blank
        *VALORES POR DEFEITO
        ** FNSTAMP
        lcStamp = uf_gerais_stamp()
        SELECT FI
        REPLACE FI.FISTAMP 		With 	lcStamp
        REPLACE FI.NMDOC 		With 	Alltrim(Ft.nmdoc)
        REPLACE FI.NDOC 		With 	Ft.ndoc
        REPLACE FI.FNO	 		With 	Ft.Fno
        * Centro Analitico
        REPLACE FI.fiCCusto		WITH	tempSQL_BICMP.Ccusto
        * Controla Movimenta Stock
        IF type("IMPORTARFACT")!='U'
	        If importarfact.movStock = 1
	            Select uCrsListaDocs
	            SELECT FI
	            REPLACE FI.ref	WITH ALLTRIM(tempSQL_BICMP.ref)
	        ENDIF
	    ELSE
	    	Select uCrsListaDocs
	        SELECT FI
	        REPLACE FI.ref	WITH ALLTRIM(tempSQL_BICMP.ref)
	    ENDIF 
                    
        If novaqtt > 0
            SELECT FI
            REPLACE	FI.qtt	WITH  novaqtt
            REPLACE	FI.EPV	WITH  tempSQL_BICMP.edebito
            Replace Fi.u_epvp With tempSQL_BICMP.edebito
        Else
            SELECT FI
            REPLACE	FI.qtt 				WITH 	0
            REPLACE	FI.ETILIQUIDO		WITH 	0
            REPLACE	FI.TILIQUIDO		WITH 	0
        Endif

        SELECT FI
        REPLACE Fi.design 	WITH  tempSQL_BICMP.design
        REPLACE Fi.iva 		WITH  tempSQL_BICMP.iva
        REPLACE Fi.tabiva 	WITH  IIF(tempSQL_BICMP.tabiva = 0, uf_gerais_getUmValor("taxasIva", "codigo", "taxa = 0", "codigo asc"), tempSQL_BICMP.tabiva)
        
        lcArmazem = tempSQL_BICMP.armazem
            
        SELECT FI
        REPLACE Fi.armazem 	WITH  lcArmazem 
        REPLACE Fi.pv 		WITH  tempSQL_BICMP.debito
        REPLACE Fi.fno 		WITH  Ft.fno
        REPLACE Fi.nmdoc 	WITH  Alltrim(Ft.nmdoc)
        REPLACE Fi.ndoc 	WITH  Ft.ndoc
        REPLACE Fi.rdata 	WITH  Date()
        REPLACE Fi.bistamp	WITH  tempSQL_BICMP.bistamp

        ***********************************
        SELECT FI
        Replace Fi.unidade 	With tempSQL_BICMP.unidade
        Replace Fi.ivaincl 	With tempSQL_BICMP.ivaincl
        Replace Fi.codigo 	With tempSQL_BICMP.codigo
        Replace Fi.cpoc 	With tempSQL_BICMP.cpoc
        Replace Fi.composto With tempSQL_BICMP.composto
        Replace Fi.lrecno 	With tempSQL_BICMP.lrecno
        Replace Fi.partes 	With tempSQL_BICMP.partes
        Replace Fi.oref 	With tempSQL_BICMP.ref
        Replace Fi.lote 	With tempSQL_BICMP.lote
        Replace Fi.usr1 	With tempSQL_BICMP.usr1
        Replace Fi.usr2 	With tempSQL_BICMP.usr2
        Replace Fi.usr3 	With tempSQL_BICMP.usr3
        Replace Fi.usr4 	With tempSQL_BICMP.usr4
        Replace Fi.usr5 	With tempSQL_BICMP.usr5
        Replace Fi.stipo 	With tempSQL_BICMP.stipo
        Replace Fi.familia 	With tempSQL_BICMP.familia
        Replace Fi.amostra 	With .t. 

        *Campo de Liga��o - Bistamp
        SELECT FI
        Replace Fi.bistamp With tempSQL_BICMP.bistamp
        Replace Fi.stns With tempSQL_BICMP.stns

        *Pre�os
        SELECT FI
        Replace Fi.pv 	With tempSQL_BICMP.edebito* 200.248
        Replace Fi.epv 	With tempSQL_BICMP.edebito
        Replace Fi.u_epvp With tempSQL_BICMP.edebito

        ** Custo - Actualiza��o com Valores de Custo Actuais
        If !Empty(Alltrim(tempSQL_BICMP.ref))
            lcSQL = ""
            TEXT TO lcSQL TEXTMERGE NOSHOW
                Select PCPOND,EPCPOND From ST (nolock) Where (Ref = '<<Alltrim(tempSQL_BICMP.ref)>>' ) 
            ENDTEXT 
            If uf_gerais_actGrelha("","uc_PRECOCUSTO",lcSQL) 
                SELECT FI
                Replace Fi.custo 	With uc_PRECOCUSTO.PCPOND 
                Replace Fi.ecusto 	With uc_PRECOCUSTO.EPCPOND 
            Endif
        Endif
        
        SELECT FI
        Replace Fi.slvu 		With tempSQL_BICMP.slvu
        Replace Fi.eslvu 		With tempSQL_BICMP.eslvu
        Replace Fi.sltt 		With tempSQL_BICMP.sltt
        Replace Fi.esltt 		With tempSQL_BICMP.esltt
        Replace Fi.slvumoeda 	With tempSQL_BICMP.slvumoeda
        Replace Fi.slttmoeda	With tempSQL_BICMP.slttmoeda
        ***
        SELECT FI
        Replace Fi.desconto 	With tempSQL_BICMP.desconto
        Replace Fi.desc2 		With tempSQL_BICMP.desc2
        Replace Fi.desc3 		With tempSQL_BICMP.desc3
        Replace Fi.desc4 		With tempSQL_BICMP.desc4
        Replace Fi.num1			With tempSQL_BICMP.num1
        Replace Fi.morada 		With tempSQL_BICMP.morada
        Replace Fi.local 		With tempSQL_BICMP.local
        Replace Fi.codpost 		With tempSQL_BICMP.codpost
    

        Select tempSQL_BICMP
        ***********************************************
        IF type("IMPORTARFACT")!='U'
            SELECT FI   
            REPLACE Fi.lordem		WITH importarfact.lordem
        ELSE
            SELECT FI  
            REPLACE Fi.lordem		WITH tempSQL_BICMP.lordem
        ENDIF 

        SELECT FI
        REPLACE Fi.usalote		WITH tempSQL_BICMP.usalote
        REPLACE Fi.pic			WITH tempSQL_BICMP.pic
        Replace Fi.u_psico 		With tempSQL_BICMP.psico
        Replace Fi.u_benzo 		With tempSQL_BICMP.benzo
        replace fi.campanhas	WITH tempSQL_BICMP.lobs
        
        IF type("IMPORTARFACT")!='U'
            importarfact.lordem = importarfact.lordem + 1
        ENDIF 
    
	ENDSCAN
	 
	Select fi 
	GO Top
	
ENDFUNC 


** 
* Nota: nas tarv n�o permitir importar o que j� foi dispensado
Function uf_importarFact_crialinhas_individuaisTARVFT
	lparameters linstamp, cabstamp

	Local novaqtt

	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		SELECT
			t.id
			,st.ref
			,st.usalote
			,pic = isnull(fprod.pvporig,0)
			,qttmov = t.qt_dispensada
			,qtt = t.qt
			,st.epcusto
			,epv = st.epv1
			,design = st.design
			,tabiva = st.tabiva
			,iva = (select taxa from taxasiva (nolock) where codigo=st.tabiva)
			,st.ivaincl
			,st.cpoc
			,st.usr1
			,st.familia
			,st.stns
			,st.EPCPOND
			,psico = ISNULL(fprod.psico,CONVERT(bit,0))
			,benzo = ISNULL(fprod.benzo,CONVERT(bit,0))
			,pref = ISNULL(fprod.pref,0)
			,st.stock
			,generico = ISNULL(fprod.generico,CONVERT(bit,0))
			,st.usalote
			,cnpem = ISNULL(fprod.cnpem,'')
		From
			ext_tarv_prescricao t (nolock)
			inner join st (nolock) on t.ref = st.ref
			left join fprod (nolock) on fprod.cnp = st.ref
		Where
			t.id_ext_tarv = '<<Alltrim(cabstamp)>>'
			and t.id = '<<Alltrim(linstamp)>>'
			and st.site_nr = <<mysite_nr>>
			and t.qt > t.qt_dispensada
	ENDTEXT

	If uf_gerais_actGrelha("","tempSQL_tarv_lin",lcSQL) And Reccount("tempSQL_tarv_lin") > 0
		LOCAL lcStamp
		Select tempSQL_tarv_lin
		SCAN
		
			** CONTROLA QUANTIDADE MOVIMENTADA > QUANTIDADE - COPIA APENAS A DIFERENCA
			novaqtt = tempSQL_tarv_lin.qtt - tempSQL_tarv_lin.qttmov
			
			** CONTROLA QUANTIDADE > 0
			If IMPORTARFACT.qttmaior == 0 And novaqtt <= 0
				*NAO ESCREVE
			Else
				Select FI
				Append Blank
		
				*VALORES POR DEFEITO
				** FNSTAMP
				lcStamp = uf_gerais_stamp()
				SELECT FI
				REPLACE FI.FISTAMP 		With 	lcStamp
				REPLACE FI.NMDOC 		With 	Alltrim(Ft.nmdoc)
				REPLACE FI.NDOC 		With 	Ft.ndoc
				REPLACE FI.FNO	 		With 	Ft.Fno
				REPLACE FI.lordem		With 	importarfact.lordem
				replace fi.id_ext_tarv_prescricao WITH ALLTRIM(tempSQL_tarv_lin.id) && campo de Liga��o
				
				* Centro Analitico
                SELECT FI
				REPLACE FI.ficcusto		With 	ft.ccusto

				** Custo - Actualiza��o com Valores de Custo Actuais
				&&Replace Fi.custo With uc_PRECOCUSTO.PCPOND
                SELECT FI
				Replace Fi.ecusto With tempSQL_tarv_lin.epcusto

                SELECT FI
				Replace Fi.eslvu With tempSQL_tarv_lin.epcusto                
				Replace Fi.esltt With tempSQL_tarv_lin.epcusto * novaqtt
				Replace Fi.slvumoeda With tempSQL_tarv_lin.epcusto
				Replace Fi.slttmoeda With tempSQL_tarv_lin.epcusto * novaqtt
				Replace Fi.pvmoeda With tempSQL_tarv_lin.epv

				* Controla Movimenta Stock
				Select uCrsListaDocs
                SELECT FI
				REPLACE Fi.ref WITH ALLTRIM(tempSQL_tarv_lin.ref)
				Replace Fi.codigo With ALLTRIM(tempSQL_tarv_lin.ref)
				replace fi.cnpem WITH ALLTRIM(tempSQL_tarv_lin.cnpem)

				If novaqtt > 0
                    SELECT FI
					REPLACE	FI.qtt	With 	novaqtt
					REPLACE	FI.EPV	With  	tempSQL_tarv_lin.epv
					REPLACE	FI.ETILIQUIDO	WITH  tempSQL_tarv_lin.EPV * novaqtt 
				Else
                    SELECT FI
					REPLACE	FI.qtt WITH 0
					REPLACE	FI.ETILIQUIDO WITH 0
				ENDIF

                SELECT FI
				REPLACE fi.design WITH tempSQL_tarv_lin.design
				REPLACE fi.iva WITH tempSQL_tarv_lin.iva
				REPLACE fi.tabiva WITH tempSQL_tarv_lin.tabiva
				Replace Fi.ivaincl With tempSQL_tarv_lin.ivaincl
				REPLACE fi.armazem WITH myArmazem

				** Campos FI ***********************************

                SELECT FI
				Replace Fi.ftanoft 	With ft.ftano
				Replace Fi.cpoc 	With tempSQL_tarv_lin.cpoc
				Replace Fi.lrecno 	With lcStamp
				Replace Fi.usr1 With tempSQL_tarv_lin.usr1
				Replace Fi.stipo With 1
				Replace Fi.familia With tempSQL_tarv_lin.familia
				Replace Fi.stns With tempSQL_tarv_lin.stns
				
				***
                SELECT FI
				Replace Fi.epcp 		With tempSQL_tarv_lin.EPCPOND
				Replace Fi.epvori 		With FI.ETILIQUIDO

				
				**** Campos de Utilizador - Verificar
                SELECT FI
				Replace Fi.u_epref 		With tempSQL_tarv_lin.pref
				Replace Fi.pic 			With tempSQL_tarv_lin.pic
				Replace Fi.u_stock 		With tempSQL_tarv_lin.stock
				Replace Fi.u_epvp 		With tempSQL_tarv_lin.epv
				Replace Fi.u_generico 	With tempSQL_tarv_lin.generico
				Replace Fi.u_psico 		With tempSQL_tarv_lin.psico
				Replace Fi.u_benzo 		With tempSQL_tarv_lin.benzo
				REPLACE Fi.usalote		WITH tempSQL_tarv_lin.usalote
				
				** Verifica se o Documento � do Tipo Nota de Credito 3 e Coloca o sinal negativo
				Select Ft
				If Ft.tipoDoc == 3 And Fi.etiliquido > 0
                    SELECT FI
					Replace Fi.etiliquido With Fi.etiliquido * -1
					Replace Fi.tiliquido With Fi.tiliquido * -1
				Endif
				
				If Ft.tipoDoc == 1 And Fi.etiliquido < 0
                    SELECT FI
					Replace Fi.etiliquido With Fi.etiliquido * -1
					Replace Fi.tiliquido With Fi.tiliquido * -1
				Endif
				Select tempSQL_tarv_lin
				***********************************************
				importarfact.lordem = importarfact.lordem + 1
			Endif
		ENDSCAN
		
		IF USED("tempSQL_tarv_lin")
			fecha("tempSQL_tarv_lin")
		ENDIF
	Endif
ENDFUNC

**
Function uf_importarFact_maxLordem
	Local lcCont
	lcCont= 0
	
	Select FI
	Scan
		If FI.LORDEM > lcCont
			lcCont = FI.LORDEM	
		Endif
	Endscan
		
	Return lcCont +1
Endfunc


**
FUNCTION uf_importarFact_ActualizaDefinicaoDoc
	Local lcSQL
	IF !USED("ucrsImportDocs")
		RETURN .f.
	ENDIF 
	
	Select uCrsListaDocs
	
	lcSQL = ""
	IF ALLTRIM(UPPER(uCrsListaDocs.Tabela)) == "FT"
		TEXT TO lcSQL TEXTMERGE NOSHOW
			UPDATE TD Set u_copref = <<IIF(uCrsListaDocs.u_copref,1,0)>> WHERE nmdoc = '<<Alltrim(uCrsListaDocs.nomedoc)>>'
		ENDTEXT
	ENDIF
	
	IF ALLTRIM(UPPER(uCrsListaDocs.Tabela)) == "BO"
		TEXT TO lcSQL TEXTMERGE NOSHOW
			UPDATE TS Set u_copref = <<IIF(uCrsListaDocs.u_copref,1,0)>> WHERE nmdos ='<<Alltrim(uCrsListaDocs.nomedoc)>>'
		ENDTEXT
	ENDIF
	
	IF !uf_gerais_actGrelha("","",lcSQL)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA A ACTUALIZAR AS DEFINI��ES DO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		return .f.
	ENDIF
ENDFUNC


**
FUNCTION uf_importarfact_gravar

	IF !up_isEntidade
			
			uf_ImportarFact_ImportarLinhas()

	ELSE

			importarfact.text1.setfocus
			uf_ImportarFact_ImportarFacturasEntidades()

	ENDIF

ENDFUNC 


**
FUNCTION uf_importarFact_sair
	IF USED("ucrsImportDocs")
		fecha("ucrsImportDocs")
	ENDIF 
	
	IF USED("ucrsImportLinhasDocs")
		fecha("ucrsImportLinhasDocs")
	ENDIF 
	
	IF USED("fi")
		SELECT fi
		GO TOP
	ENDIF
	
	RELEASE myEntidadeFact
	
	IMPORTARFACT.hide
	IMPORTARFACT.release
ENDFUNC

PROCEDURE uf_importarFact_redrawGrids

   WITH IMPORTARFACT.pageframe1

	  IF up_isEntidade 

		.page1.gridPesq.width = (.width/3) + 40

		.page1.gridEnt.width = .width - .page1.gridPesq.width
		.page1.gridEnt.left = .page1.gridPesq.left + .page1.gridPesq.width + 5
		.page1.gridEnt.top = .page1.gridPesq.top
		.page1.gridEnt.height = .page1.gridPesq.height
		.page1.check6.left = .page1.gridPesq.left + .page1.gridPesq.width + 17

	  ELSE

		.page1.grid1.width = (.width/2) -10
		.page1.gridPesq.width = (.width/2) -10

		.page1.grid1.left = .page1.gridPesq.left + .page1.gridPesq.width + 5
		.page1.check6.left = .page1.grid1.left + 15

	  ENDIF

   ENDWITH

ENDPROC

PROCEDURE uf_importarFact_selDocs

	uf_importarFact_ActualizaLinhasDocs()

	SELECT ucrsImportDocs

	IF ucrsImportDocs.copiar = 1
		IF up_isEntidade

			uv_curCabStamp = ucrsImportDocs.cabStamp
					
			IF !USED("uc_linSelEnt")
				CREATE CURSOR uc_linSelEnt(stamp c(254))
			ENDIF
			
			SELECT linStamp as stamp FROM ucrsImportDocsEntidades WITH (BUFFERING = .T.) WHERE ALLTRIM(ucrsImportDocsEntidades.cabStamp) = ALLTRIM(uv_curCabStamp) INTO CURSOR uc_cabSel
			
			SELECT uc_cabSel
			GO TOP
			SCAN
			
				SELECT uc_linSelEnt
				LOCATE FOR ALLTRIM(uc_cabSel.stamp) == ALLTRIM(uc_linSelEnt.stamp)
				
				IF !FOUND()
				
					SELECT uc_linSelEnt
					APPEND BLANK
					REPLACE uc_linSelEnt.stamp WITH uc_cabSel.stamp
					
				ENDIF
			
				SELECT uc_linSelEnt
			ENDSCAN

			UPDATE ucrsImportDocsEntidades SET copiar = 1 WHERE ALLTRIM(ucrsImportDocsEntidades.cabStamp) == ALLTRIM(uv_curCabStamp)

			SELECT ucrsImportDocsEntidades
			COUNT TO uv_count FOR !DELETED()

			SELECT uc_linSelEnt
			COUNT TO uv_countSEL FOR !DELETED()


			IF uv_countSEL == uv_count

				IF !IMPORTARFACT.selTodos
					IMPORTARFACT.pageframe1.page1.check6.click(.T.)
				ENDIF

			ELSE

				IF IMPORTARFACT.selTodos
					IMPORTARFACT.pageframe1.page1.check6.click(.T.)
				ENDIF

			ENDIF

		ELSE

			uv_curCabStamp = ucrsImportDocs.cabStamp
					
			IF !USED("uc_linSelFac")
				CREATE CURSOR uc_linSelFac(stamp c(254))
			ENDIF
			
			SELECT stamp FROM ucrsImportLinhasDocs WITH (BUFFERING = .T.) WHERE ALLTRIM(ucrsImportLinhasDocs.cabStamp) = ALLTRIM(uv_curCabStamp) INTO CURSOR uc_cabSel
			
			SELECT uc_cabSel
			GO TOP
			SCAN
			
				SELECT uc_linSelFac
				LOCATE FOR ALLTRIM(uc_cabSel.stamp) == ALLTRIM(uc_linSelFac.stamp)
				
				IF !FOUND()
				
					SELECT uc_linSelFac
					APPEND BLANK
					REPLACE uc_linSelFac.stamp WITH uc_cabSel.stamp
					
				ENDIF
			
				SELECT uc_linSelFac
			ENDSCAN

			UPDATE ucrsImportLinhasDocs SET copiar = 1 WHERE ALLTRIM(ucrsImportLinhasDocs.cabStamp) == ALLTRIM(uv_curCabStamp)

			SELECT ucrsImportLinhasDocs
			COUNT TO uv_count FOR !DELETED()

			SELECT uc_linSelFac
			COUNT TO uv_countSEL FOR !DELETED()


			IF uv_countSEL == uv_count

				IF !IMPORTARFACT.selTodos
					IMPORTARFACT.pageframe1.page1.check6.click(.T.)
				ENDIF

			ELSE

				IF IMPORTARFACT.selTodos
					IMPORTARFACT.pageframe1.page1.check6.click(.T.)
				ENDIF

			ENDIF

		ENDIF

	ENDIF

	

	IF up_isEntidade

		SELECT ucrsImportDocsEntidades
		COUNT TO uv_count FOR !DELETED()

		IF EMPTY(uv_count)

			IF IMPORTARFACT.selTodos
				IMPORTARFACT.pageframe1.page1.check6.click(.T.)
			ENDIF

		ENDIF

		IMPORTARFACT.pageframe1.page1.gridEnt.refresh()

	ELSE

		SELECT ucrsImportLinhasDocs
		COUNT TO uv_count FOR !DELETED()

		IF EMPTY(uv_count)

			IF IMPORTARFACT.selTodos
				IMPORTARFACT.pageframe1.page1.check6.click(.T.)
			ENDIF

		ENDIF

		IMPORTARFACT.pageframe1.page1.grid1.refresh()

	ENDIF

	

ENDPROC

PROCEDURE uf_IMPORTARFACT_selLin
	

	IF up_isEntidade

		IF !USED("uc_linSelEnt")
			CREATE CURSOR uc_linSelEnt(stamp c(254))
		ENDIF

		IF ucrsImportDocsEntidades.copiar = 1

			SELECT uc_linSelEnt
			APPEND BLANK
			REPLACE uc_linSelEnt.stamp WITH ucrsImportDocsEntidades.linStamp
			
			IF !IMPORTARFACT.seltodos
			
				SELECT * FROM ucrsImportDocsEntidades WITH (BUFFERING = .T.) INTO CURSOR uc_sels

				SELECT uc_sels
				COUNT TO uv_count FOR !DELETED() AND uc_sels.copiar = 0
				
				IF uv_count = 0

					IMPORTARFACT.pageframe1.page1.check6.click(.T.)

				ENDIF
		
			ENDIF
			
		ELSE

			SELECT uc_linSelEnt
			DELETE FOR ALLTRIM(uc_linSelEnt.stamp) == ALLTRIM(ucrsImportDocsEntidades.linStamp)
			
			IF IMPORTARFACT.seltodos
			
				IMPORTARFACT.pageframe1.page1.check6.click(.T.)
			
			ENDIF
			
		ENDIF

	ELSE

		IF !USED("uc_linSelFac")
			CREATE CURSOR uc_linSelFac(stamp c(254))
		ENDIF

		IF ucrsImportLinhasDocs.copiar = 1

			SELECT uc_linSelFac
			APPEND BLANK
			REPLACE uc_linSelFac.stamp WITH ucrsImportLinhasDocs.stamp
			
			IF !IMPORTARFACT.seltodos

				SELECT * FROM ucrsImportLinhasDocs WITH (BUFFERING = .T.) INTO CURSOR uc_sels
			
				SELECT uc_sels
				COUNT TO uv_count FOR !DELETED() AND uc_sels.copiar = 0
				
				IF uv_count = 0

					IMPORTARFACT.pageframe1.page1.check6.click(.T.)

				ENDIF
		
			ENDIF
			
		ELSE

			SELECT uc_linSelFac
			DELETE FOR ALLTRIM(uc_linSelFac.stamp) == ALLTRIM(ucrsImportLinhasDocs.stamp)
			
			IF IMPORTARFACT.seltodos
			
				IMPORTARFACT.pageframe1.page1.check6.click(.T.)
			
			ENDIF
			
		ENDIF

	ENDIF

ENDPROC

PROCEDURE uf_IMPORTARFACT_chkSelLin
	LPARAMETERS uv_progClick

    IF !USED("uc_linSelFac")
        CREATE CURSOR uc_linSelFac(stamp c(254))
    ENDIF

	IF IMPORTARFACT.seltodos
		IMPORTARFACT.seltodos = .F.
		IF !uv_progClick

			IF up_isEntidade

				DELETE FROM uc_linSelEnt

				SELECT ucrsImportDocsEntidades
				GO TOP

				REPLACE ucrsImportDocsEntidades.copiar with 0 FOR 1=1

				SELECT ucrsImportDocsEntidades
				GO TOP

			ELSE
		
				DELETE FROM uc_linSelFac

				SELECT ucrsImportLinhasDocs
				GO TOP

				REPLACE ucrsImportLinhasDocs.copiar with 0 FOR 1=1

				SELECT ucrsImportLinhasDocs
				GO TOP

			ENDIF

		ENDIF
	ELSE
		IMPORTARFACT.selTodos = .T.
		IF !uv_progClick

			IF up_isEntidade

				IMPORTARFACT.pageframe1.page1.gridPesq.setFocus()

				SELECT ucrsImportDocsEntidades
				GO TOP
			
				SCAN

					REPLACE ucrsImportDocsEntidades.copiar WITH 1

					SELECT ucrsImportDocsEntidades
				ENDSCAN

				INSERT INTO uc_linSelEnt SELECT linStamp FROM ucrsImportDocsEntidades WHERE copiar = 1 AND !DELETED()

				SELECT ucrsImportDocsEntidades
				GO TOP

			ELSE

				IMPORTARFACT.pageframe1.page1.gridPesq.setFocus()

				SELECT ucrsImportLinhasDocs
				GO TOP
			
				SCAN

					REPLACE ucrsImportLinhasDocs.copiar WITH 1

					SELECT ucrsImportLinhasDocs
				ENDSCAN

				INSERT INTO uc_linSelFac SELECT stamp FROM ucrsImportLinhasDocs WHERE copiar = 1 AND !DELETED()

				SELECT ucrsImportLinhasDocs
				GO TOP

			ENDIF

		ENDIF
	ENDIF

	IMPORTARFACT.pageframe1.page1.grid1.refresh()

ENDPROC


Function uf_importarFact_crialinhas_individuaisFOFT
	lparameters linstamp

	Local novaqtt
	** 	Variavel Pbruto usada para guardar a Quantidade Movimentada, actualizada em eventos ap�s grava��o dos Documentos
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#DadosProduto'))
		DROP TABLE #DadosProduto
			
		select 
			st.ref
			,st.usalote
			,pic = isnull(fprod.pvporig,0)
			,u_psico = fprod.psico
			,u_benzo = fprod.benzo
			,u_cnp = fprod.cnp
		into
			#DadosProduto
		from
			st (nolock)
			left join fprod (nolock) on fprod.cnp = st.ref
		where
			 st.site_nr = <<mysite_nr>>

		SELECT
			usalote = isnull(#DadosProduto.usalote,CONVERT(bit,0))
			,pic = isnull(#DadosProduto.pic,0)
			,fn.Pbruto as qttmov
			,ISNULL(#DadosProduto.u_psico,0) as u_psico
			,ISNULL(#DadosProduto.u_benzo,0) as u_benzo
			,ISNULL(#DadosProduto.u_cnp, '') as u_cnp
			,fn.*
		From
			FN (nolock) 
			left join #DadosProduto on fn.ref = #DadosProduto.ref
		Where 	
			fn.fnstamp = '<<Alltrim(linstamp)>>'
		
		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#DadosProduto'))
		DROP TABLE #DadosProduto
	ENDTEXT

	If uf_gerais_actGrelha("","tempSQL_FNCMP",lcSQL) And Reccount("tempSQL_FNCMP") > 0

		LOCAL lcStamp
		Select tempSQL_FNCMP
		Scan
		
			**CONTROLA QUANTIDADE MOVIMENTADA > QUANTIDADE - COPIA APENAS A DIFERENCA
			novaqtt = tempSQL_FNCMP.qtt - tempSQL_FNCMP.qttmov 

            Select FI
            Append Blank
    
            *VALORES POR DEFEITO
            ** FNSTAMP
            lcStamp = uf_gerais_stamp()
            SELECT FI
            REPLACE FI.FISTAMP 		With 	lcStamp
            REPLACE fi.design 		WITH  	tempSQL_FNCMP.design
            REPLACE FI.NMDOC 		With 	Alltrim(Ft.nmdoc)
            REPLACE FI.NDOC 		With 	Ft.ndoc
            REPLACE FI.FNO	 		With 	Ft.Fno
            REPLACE FI.lordem		With 	importarfact.lordem
            
            * Centro Analitico
            REPLACE FI.ficcusto		With 	tempSQL_FNCMP.fnccusto
            
            * Controla Movimenta Stock
            Select uCrsListaDocs
            If IMPORTARFACT.MOVSTOCK = 1
                SELECT FI
                REPLACE Fi.ref		With 	IIF(EMPTY(ALLTRIM(tempSQL_FNCMP.ref)),ALLTRIM(tempSQL_FNCMP.oref),ALLTRIM(tempSQL_FNCMP.ref))
            Endif					
                
            If novaqtt > 0
                SELECT FI
                REPLACE	FI.qtt	With 	novaqtt
                REPLACE	FI.EPV	With  	tempSQL_FNCMP.EPV
                                            
                If tempSQL_FNCMP.qtt !=  tempSQL_FNCMP.qttmov
                    SELECT FI
                    REPLACE	FI.ETILIQUIDO	WITH  tempSQL_FNCMP.EPV * novaqtt 
                    REPLACE	FI.TILIQUIDO	WITH  tempSQL_FNCMP.EPV * novaqtt * 200.248
                Else
                    SELECT FI
                    REPLACE	FI.ETILIQUIDO	WITH  tempSQL_FNCMP.ETILIQUIDO
                    REPLACE	FI.TILIQUIDO	WITH  tempSQL_FNCMP.ETILIQUIDO * 200.248
                Endif
            Else
                SELECT FI
                REPLACE	FI.qtt 		WITH 	0
                REPLACE	FI.ETILIQUIDO	WITH 	0
            ENDIF
            
            SELECT FI
            REPLACE fi.iva 		WITH  tempSQL_FNCMP.iva
            REPLACE fi.tabiva 	WITH  IIF(tempSQL_FNCMP.tabiva = 0, uf_gerais_getUmValor("taxasIva", "codigo", "taxa = 0", "codigo asc"), tempSQL_FNCMP.tabiva)
            REPLACE fi.armazem 	WITH  tempSQL_FNCMP.armazem
            
            ** Campos FI ***********************************
            
            SELECT FI
            Replace Fi.unidade 	With tempSQL_FNCMP.unidade
            **Replace Fi.unidad2 	With tempSQL_FNCMP.unidad2
            Replace Fi.iva 		With tempSQL_FNCMP.iva
            Replace Fi.ivaincl 	With tempSQL_FNCMP.ivaincl
            Replace Fi.codigo 	With tempSQL_FNCMP.codigo
            **Replace Fi.tabiva 	With tempSQL_FNCMP.tabiva
            **Replace Fi.fnoft 	With tempSQL_FNCMP.fnoft
            **Replace Fi.ndocft 	With tempSQL_FNCMP.ndocft
            **Replace Fi.ftanoft 	With tempSQL_FNCMP.ftanoft
            **Replace Fi.lobs2 	With tempSQL_FNCMP.lobs2
            **Replace Fi.litem2 	With tempSQL_FNCMP.litem2
            **Replace Fi.litem 	With tempSQL_FNCMP.litem
            **Replace Fi.lobs3 	With tempSQL_FNCMP.lobs3
            Replace Fi.cpoc 	With tempSQL_FNCMP.cpoc
            Replace Fi.composto With tempSQL_FNCMP.composto
            **Replace Fi.lrecno 	With tempSQL_FNCMP.lrecno
            **Replace Fi.partes 	With tempSQL_FNCMP.partes
            **Replace Fi.altura 	With tempSQL_FNCMP.altura
            **Replace Fi.largura 	With tempSQL_FNCMP.largura
            Replace Fi.oref With IIF(EMPTY(ALLTRIM(tempSQL_FNCMP.ref)),ALLTRIM(tempSQL_FNCMP.oref),ALLTRIM(tempSQL_FNCMP.ref))
            Replace Fi.lote With tempSQL_FNCMP.lote
            Replace Fi.usr1 With tempSQL_FNCMP.usr1
            Replace Fi.usr2 With tempSQL_FNCMP.usr2
            Replace Fi.usr3 With tempSQL_FNCMP.usr3
            Replace Fi.usr4 With tempSQL_FNCMP.usr4
            Replace Fi.usr5 With tempSQL_FNCMP.usr5
            Replace Fi.cor 	With tempSQL_FNCMP.cor
            Replace Fi.tam 	With tempSQL_FNCMP.tam
            **Replace Fi.stipo With tempSQL_FNCMP.stipo
            Replace Fi.fifref With tempSQL_FNCMP.fnfref
            Replace Fi.familia With tempSQL_FNCMP.familia
            Replace Fi.pbruto With tempSQL_FNCMP.pbruto
            Replace Fi.stns With tempSQL_FNCMP.stns
            **Replace Fi.fincusto With tempSQL_FNCMP.fincusto
            Replace Fi.pv With tempSQL_FNCMP.pv
            **Replace Fi.pvmoeda With tempSQL_FNCMP.pvmoeda
            Replace Fi.epv With tempSQL_FNCMP.epv
            Replace Fi.tmoeda With tempSQL_FNCMP.tmoeda
            **Replace Fi.eaquisicao With tempSQL_FNCMP.eaquisicao
            
            ** Custo - Actualiza��o com Valores de Custo Actuais
            lcSQL = ""
            TEXT TO lcSQL TEXTMERGE NOSHOW
                Select PCPOND,EPCPOND, epcult From ST (nolock) Where (Ref = '<<Alltrim(tempSQL_FNCMP.ref)>>' Or ref = '<<Alltrim(tempSQL_FNCMP.oref)>>') and site_nr=<<mysite_nr>>
            ENDTEXT 
            If uf_gerais_actGrelha("","uc_PRECOCUSTO",lcSQL) 
                SELECT FI
                Replace Fi.custo With uc_PRECOCUSTO.epcult * 200.482
                Replace Fi.ecusto With uc_PRECOCUSTO.epcult 
            Endif
            
            SELECT FI
            Replace Fi.slvu With tempSQL_FNCMP.slvu
            Replace Fi.eslvu With tempSQL_FNCMP.eslvu
            Replace Fi.sltt With tempSQL_FNCMP.sltt
            Replace Fi.esltt With tempSQL_FNCMP.esltt
            Replace Fi.slvumoeda With tempSQL_FNCMP.slvumoeda
            **Replace Fi.slttmoeda	With tempSQL_FNCMP.slttmoeda
            **Replace Fi.nccod 		With tempSQL_FNCMP.nccod
            **Replace Fi.ncinteg 		With tempSQL_FNCMP.ncinteg
            **Replace Fi.epromo 		With tempSQL_FNCMP.epromo
            Replace Fi.desconto 	With tempSQL_FNCMP.desconto
            Replace Fi.desc2 		With tempSQL_FNCMP.desc2
            Replace Fi.desc3 		With tempSQL_FNCMP.desc3
            Replace Fi.desc4 		With tempSQL_FNCMP.desc4
            **Replace Fi.iectin 		With tempSQL_FNCMP.iectin
            **Replace Fi.eiectin 		With tempSQL_FNCMP.eiectin
            **Replace Fi.vbase		With tempSQL_FNCMP.vbase
            **Replace Fi.evbase 		With tempSQL_FNCMP.evbase
            **Replace Fi.tliquido 	With tempSQL_FNCMP.tliquido
            Replace Fi.num1			With tempSQL_FNCMP.num1
            **Replace Fi.pcp 			With tempSQL_FNCMP.pcp
            **Replace Fi.epcp 		With tempSQL_FNCMP.epcp
            **Replace Fi.pvori 		With tempSQL_FNCMP.pvori
            **Replace Fi.epvori 		With tempSQL_FNCMP.epvori
            **Replace Fi.szzstamp		With tempSQL_FNCMP.szzstamp
            **Replace Fi.zona 		With tempSQL_FNCMP.zona
            **Replace Fi.morada 		With tempSQL_FNCMP.morada
            **Replace Fi.local 		With tempSQL_FNCMP.local
            **Replace Fi.codpost 		With tempSQL_FNCMP.codpost
            **Replace Fi.telefone 	With tempSQL_FNCMP.telefone
            **Replace Fi.email 		With tempSQL_FNCMP.email
            **Replace Fi.tkhposlstamp With tempSQL_FNCMP.tkhposlstamp
            Replace Fi.marcada 		With tempSQL_FNCMP.marcada
            Replace Fi.amostra 		With .t.
            **Replace Fi.u_epref 		With tempSQL_FNCMP.u_epref
            Replace Fi.pic 			With tempSQL_FNCMP.pic
            **Replace Fi.u_stock 		With tempSQL_FNCMP.u_stock
            **Replace Fi.u_ip 		With tempSQL_FNCMP.u_ip
            **Replace Fi.u_epvp 		With tempSQL_FNCMP.u_epvp
            **Replace Fi.u_genalt 	With tempSQL_FNCMP.u_genalt
            **Replace Fi.u_generico 	With tempSQL_FNCMP.u_generico
            **Replace Fi.u_ettent1 	With tempSQL_FNCMP.u_ettent1
            **Replace Fi.u_ettent2 	With tempSQL_FNCMP.u_ettent2
            **Replace Fi.u_psicont 	With tempSQL_FNCMP.u_psicont
            **Replace Fi.u_bencont 	With tempSQL_FNCMP.u_bencont
            Replace Fi.u_psico 		With tempSQL_FNCMP.u_psico
            Replace Fi.u_benzo 		With tempSQL_FNCMP.u_benzo
            **Replace Fi.u_txcomp 	With tempSQL_FNCMP.u_txcomp
            Replace Fi.u_cnp 		With tempSQL_FNCMP.u_cnp
            **Replace Fi.u_comp 		With tempSQL_FNCMP.u_comp
            **Replace Fi.u_nbenef 	With tempSQL_FNCMP.u_nbenef
            **Replace Fi.u_nbenef2 	With tempSQL_FNCMP.u_nbenef2
            **Replace Fi.u_diploma 	With tempSQL_FNCMP.u_diploma
            **Replace Fi.u_refvale 	With tempSQL_FNCMP.u_refvale
            **Replace Fi.u_robot 		With tempSQL_FNCMP.u_robot
            **Replace Fi.u_descval 	With tempSQL_FNCMP.u_descval 
            REPLACE Fi.usalote		WITH tempSQL_FNCMP.usalote
            REPLACE Fi.pic			WITH tempSQL_FNCMP.pic
            
            ** Verifica se o Documento � do Tipo Nota de Credito 3 e Coloca o sinal negativo
            Select Ft
            If Ft.tipoDoc == 3 And Fi.etiliquido > 0
                SELECT FI
                Replace Fi.etiliquido With Fi.etiliquido * -1
                Replace Fi.tiliquido With Fi.tiliquido * -1
            Endif
            
            If Ft.tipoDoc == 1 And Fi.etiliquido < 0
                SELECT FI
                Replace Fi.etiliquido With Fi.etiliquido * -1
                Replace Fi.tiliquido With Fi.tiliquido * -1
            Endif

			SELECT FI2 
			APPEND BLANK

			replace fi2.fistamp with lcStamp
			replace fi2.fnstamp with tempSQL_FNCMP.fnstamp

            Select tempSQL_FNCMP
            ***********************************************
            importarfact.lordem = importarfact.lordem + 1
		Endscan
	Endif
ENDFUNC

PROCEDURE uf_importarFact_setMovStock

	IMPORTARFACT.movStock = 1

ENDPROC

PROCEDURE uf_importarFact_chkSel
	LPARAMETERS uv_progClick

	IF !USED("uc_docSel")
		CREATE CURSOR uc_docSel(stamp c(254))
	ENDIF

	IF IMPORTARFACT.seltodosDocs
		IMPORTARFACT.seltodosDocs = .F.
		IF !uv_progClick
		
			DELETE FROM uc_docSel

			SELECT ucrsImportDocs
			GO TOP

			REPLACE ucrsImportDocs.copiar with 0 FOR 1=1

			IF IMPORTARFACT.selTodos
				IMPORTARFACT.pageframe1.page1.check6.click(.T.)
			ENDIF

			SELECT ucrsImportDocs
			GO TOP

		ENDIF
	ELSE
		IMPORTARFACT.selTodosDocs = .T.
		IF !uv_progClick

			IMPORTARFACT.pageframe1.page1.grid1.setFocus()

			SELECT ucrsImportDocs
			
			REPLACE ucrsImportDocs.copiar WITH 1 FOR 1=1

			INSERT INTO uc_docSel SELECT cabstamp FROM ucrsImportDocs WHERE copiar = 1 AND !DELETED()

			SELECT ucrsImportDocs
			GO TOP			

		ENDIF
	ENDIF

	uf_Importarfact_ActualizaLinhasDocs()

	IF IMPORTARFACT.selTodosDocs AND !IMPORTARFACT.selTodos
		IMPORTARFACT.pageframe1.page1.check6.click(.F.)
	ENDIF

	IMPORTARFACT.pageframe1.page1.gridPesq.refresh()

ENDPROC