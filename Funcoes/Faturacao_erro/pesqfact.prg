**
FUNCTION uf_pesqFact_chama
	LPARAMETERS lcEcra 
	
	
	&& CONTROLA PERFIS DE ACESSO
	IF !uf_gerais_validaPermUser(ch_userno , ch_grupo, 'Gest�o de Factura��o')
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ACEDER AO PAINEL GEST�O DE FACTURA��O.","OK","",48)
		RETURN .f.
	ENDIF
	
	IF TYPE("PESQFACT") != "U"
		PESQFACT.show
		
		UPDATE ucrsPesqFact SET ucrsPesqFact.escolha = .f.
		SELECT ucrsPesqFact
		GO TOP
	ELSE

		lcSQL = ""
		TEXT TO lcSQL TEXTMERGE NOSHOW
			exec up_perfis_validaAcessoDocs '<<ch_userno>>', '<<ALLTRIM(ch_grupo)>>', 'Visualizar', 'FT', 0, '<<mysite>>'
		ENDTEXT
		
		If !uf_gerais_actGrelha("","ucrsComboFactPesq",lcSQL)
			uf_perguntalt_chama("N�O FOI POSS�VEL ENCONTRAR DOCUMENTOS.","OK","",16)
			RETURN .f.
		ENDIF

		
		IF !USED("ucrsPesqFact")
			uf_PESQFACT_CarregaDados(.t.)
			
		ELSE
			SELECT ucrsPesqFact
			GO TOP
			SCAN
				REPLACE ucrsPesqFact.escolha WITH .f.
			ENDSCAN
			SELECT ucrsPesqFact
			GO TOP
		ENDIF

		DO FORM PESQFACT

		
		IF TYPE("lcEcra") == "C"
			pesqfact.ecra = UPPER(ALLTRIM(lcEcra))
		ENDIF 
	ENDIF 
ENDFUNC 


**
FUNCTION uf_PESQFACT_limparDados
	PESQFACT.numdoc.Value = ''
	PESQFACT.produto.Value = ''
	PESQFACT.dataIni.Value = ''
	PESQFACT.dataFim.Value = ''
	PESQFACT.nome.Value = ''
	PESQFACT.no.Value = ''
	PESQFACT.estab.Value = ''
	PESQFACT.ultimos.Value = ''
	PESQFACT.documento.Value = ''
	PESQFACT.txtNAtend.value = ''
	PESQFACT.txtNRec.value = ''

	uf_pesqFact_CarregaDados(.t.)
ENDFUNC 


**
FUNCTION uf_PESQFACT_CarregaDados
	LPARAMETERS tcBool, tcReport &&tcBool caso venha a .t. cria apenas o cursor
	LOCAL lcSQL

	
	IF tcBool == .t.
		lcSQL = ""
		TEXT TO lcSql NOSHOW TEXTMERGE 
			exec up_facturacao_pesquisarDocumentos 0, '', 0, 0, -1,  '30000101', '30000101', '', 0, '', '', '', '', '', ''
		ENDTEXT
		
		IF TYPE("PESQFACT")=="U"
			IF !uf_gerais_actGrelha("","ucrsPesqFact",lcSql)
				uf_perguntalt_chama("N�O FOI POSSIVEL ACTUALIZAR A LISTAGEM DE DOCUMENTOS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				RETURN .f.
			ENDIF
		ENDIF
		
	ELSE &&Refresca valores da Grid
		
		regua(0,0,"A processar as Listagem de Documentos...")
		
		**Trata parametro
		lcTop = IIF(INT(VAL(PESQFACT.ultimos.value))== 0,100000,INT(VAL(PESQFACT.ultimos.value)))
			
		If !empty(PESQFACT.DataIni.value)
			lcDataIni = uf_gerais_getdate(PESQFACT.dataIni.value,"SQL")
		Else
			lcDataIni ='19000101'
		EndIf
		If !empty(PESQFACT.DataFim.value)
			lcDataFim = uf_gerais_getdate(PESQFACT.dataFim.value,"SQL")
		Else
			If !empty(PESQFACT.DataIni.value)
				lcDataFim = lcDataIni
			Else
				lcDataFim='30001231'
			EndIf
		ENDIF
		
		IF EMPTY(tcReport)
	
			lcSQL = ""
			TEXT TO lcSql NOSHOW TEXTMERGE 
				exec up_facturacao_pesquisarDocumentos <<lcTop>>, '<<Alltrim(PESQFACT.NOME.VALUE)>>', <<IIF(EMPTY(Alltrim(PESQFACT.NUMDOC.VALUE)),0,Alltrim(LEFT(PESQFACT.NUMDOC.VALUE,10)))>>,
						<<IIF(EMPTY(Alltrim(PESQFACT.NO.VALUE)),0,Alltrim(PESQFACT.NO.VALUE))>>, <<IIF(EMPTY(Alltrim(PESQFACT.ESTAB.VALUE)),-1,Alltrim(LEFT(PESQFACT.ESTAB.VALUE,3)))>>,  '<<lcDataIni>>',  '<<lcDataFim>>',
						'<<Alltrim(PESQFACT.documento.Value)>>', <<ch_userno>>,  '<<Alltrim(ch_grupo)>>', '<<Alltrim(PESQFACT.produto.VALUE)>>', '<<Alltrim(PESQFACT.txtNAtend.VALUE)>>', '<<Alltrim(PESQFACT.txtNRec.VALUE)>>', 
						'<<Alltrim(PESQFACT.loja.Value)>>', '<<ASTR(PESQFACT.nif.value)>>', -1 ,'<<ALLTRIM(PESQFACT.op.value)>>'
			ENDTEXT
		
			uf_gerais_actGrelha("PESQFACT.GridPesq","ucrsPesqFact",lcSQL)
			regua(1,3,"A PESQUISAR...",.F.)
			
			PESQFACT.GridPesq.Refresh
			
			PESQFACT.lblRegistos.caption = ALLTRIM(STR(RECCOUNT("ucrsPesqFact"))) + " Resultados"
			
		ELSE
		
			TEXT TO lcReport TEXTMERGE NOSHOW 
				&topo=<<lcTop>>&entidade=<<Alltrim(PESQFACT.NOME.VALUE)>>&numdoc=<<IIF(EMPTY(Alltrim(PESQFACT.NUMDOC.VALUE)),0,Alltrim(PESQFACT.NUMDOC.VALUE))>>&no=<<IIF(EMPTY(Alltrim(PESQFACT.NO.VALUE)),0,Alltrim(PESQFACT.NO.VALUE))>>&estab= <<IIF(EMPTY(Alltrim(PESQFACT.ESTAB.VALUE)),-1,Alltrim(PESQFACT.ESTAB.VALUE))>>&dataIni=<<uf_gerais_getdate(lcDataIni)>>&dataFim=<<uf_gerais_getdate(lcDataFim)>>&doc=<<Alltrim(PESQFACT.documento.Value)>>&user=<<ch_userno>>&group=<<Alltrim(ch_grupo)>>&design=<<Alltrim(PESQFACT.produto.VALUE)>>&atend=<<Alltrim(PESQFACT.txtNAtend.VALUE)>>&receita=<<Alltrim(PESQFACT.txtNRec.VALUE)>>&site=<<Alltrim(PESQFACT.loja.Value)>>&ncont=<<ASTR(PESQFACT.nif.value)>>&exportado=-1
			ENDTEXT
				SELECT ucrsPesqFact
				GO TOP
				
				IF RECCOUNT("ucrsPesqFact") > 0
					uf_gerais_chamaReport("listagem_pesquisa_facturacao", ALLTRIM(lcReport))
				ELSE
					uf_perguntalt_chama("N�o existem dados para Impress�o. Por favor verifique. Obrigado.","OK","",16)
				ENDIF
				
		ENDIF
	regua(2)				
	ENDIF
	
ENDFUNC


**
*!*	FUNCTION uf_PESQFACT_imprimir
*!*		IF RECCOUNT("ucrsPesqFact") > 0
*!*			
*!*			**Trata parametros
*!*			If Empty(PESQFACT.ultimos.Value)
*!*				PESQFACT.ultimos.Value = 30
*!*			ENDIF

*!*			lcDataIni = PESQFACT.dataIni.value

*!*			lcDataFim = PESQFACT.DataFim.value
*!*			
*!*			
*!*			If !empty(PESQFACT.DataIni.value)
*!*				lcDataIni = PESQFACT.dataIni.value
*!*			Else
*!*				lcDataIni ='1900.01.01'
*!*			EndIf
*!*			If !empty(PESQFACT.DataFim.value)
*!*				lcDataFim = PESQFACT.DataFim.value
*!*			Else
*!*				If !empty(PESQFACT.DataIni.value)
*!*					lcDataFim = lcDataIni
*!*				Else
*!*					lcDataFim='3000.12.31'
*!*				EndIf
*!*			ENDIF

*!*			parametros  = ''
*!*			TEXT TO parametros TEXTMERGE NOSHOW
*!*				&topo=<<PESQFACT.ultimos.Value>>&entidade=<<uf_gerais_trataCarateresReservadosURL(Alltrim(PESQFACT.NOME.VALUE))>>&numdoc=<<IIF(EMPTY(Alltrim(PESQFACT.NUMDOC.VALUE)),0,Alltrim(PESQFACT.NUMDOC.VALUE))>>&no=<<IIF(EMPTY(Alltrim(PESQFACT.NO.VALUE)),0,Alltrim(PESQFACT.NO.VALUE))>>&estab=<<IIF(EMPTY(Alltrim(PESQFACT.ESTAB.VALUE)),-1,Alltrim(PESQFACT.ESTAB.VALUE))>>&dataIni=<<uf_gerais_getdate(lcDataIni, "DATA")>>&dataFim=<<uf_gerais_getdate(lcDataFim, "DATA")>>&doc=<<Alltrim(PESQFACT.documento.Value)>>&user=<<ch_userno>>&group=<<Alltrim(ch_grupo)>>&design=<<Alltrim(PESQFACT.produto.VALUE)>>&atend=<<Alltrim(PESQFACT.txtNAtend.VALUE)>>&receita=<<Alltrim(PESQFACT.txtNRec.VALUE)>>&site=<<Alltrim(PESQFACT.loja.Value)>>&ncont=<<astr(PESQFACT.nif.value)>>
*!*			ENDTEXT 

*!*			uf_gerais_chamaReport("listagem_pesquisa_facturacao", parametros )
*!*		ENDIF 
*!*	ENDFUNC 


**
FUNCTION uf_PESQFACT_sel

	IF EMPTY(PESQFACT.ecra)
		uf_PESQFACT_sair()
	ENDIF 
	IF USED("ucrsPesqFact")
		SELECT ucrsPesqFact
		DO CASE 
			CASE PESQFACT.ecra == "FACTURACAO"
				
				uf_facturacao_Chama(ALLTRIM(ucrsPesqFact.cabstamp))
		
			CASE PESQFACT.ecra == "VACINACAO"
				IF USED("uCrsVacinacao")
					Select uCrsVacinacao
					replace nrVenda WITH Alltrim(ucrsPesqFact.DOCUMENTO) + " - " + astr(ucrsPesqFact.NUMDOC)
					vacinacao.refresh
				ENDIF 
		ENDCASE 
		uf_PESQFACT_sair()
	ENDIF
	
ENDFUNC


**
FUNCTION uf_PESQFACT_novo
	uf_facturacao_chama('')
	uf_facturacao_novo()
	uf_PESQFACT_sair()
ENDFUNC

FUNCTION uf_PESQFACT_novo_escolhe
	LPARAMETERS lnDocumento
	LOCAL lcDocumento
	lcDocumento = ALLTRIM(lnDocumento)
	uf_facturacao_chama('')
	uf_facturacao_novo_escolhe(lcDocumento)
	uf_PESQFACT_sair()
ENDFUNC

**
FUNCTION uf_PESQFACT_selTodos
	LOCAL lcPos
	
	SELECT ucrsPesqFact
	lcPos = RECNO("ucrsPesqFact")
	
	IF PESQFACT.menu1.selTodos.lbl.caption == "Sel. Todos"
		SELECT ucrsPesqFact
		replace ALL ucrsPesqFact.escolha WITH .t.

		PESQFACT.menu1.selTodos.config("De-Sel. Todos", myPath + "\imagens\icons\checked_w.png", "uf_PESQFACT_selTodos")
	ELSE	
		SELECT ucrsPesqFact
		replace ALL ucrsPesqFact.escolha WITH .f.

		PESQFACT.menu1.selTodos.config("Sel. Todos", myPath + "\imagens\icons\unchecked_w.png", "uf_PESQFACT_selTodos")
	ENDIF
	
	SELECT ucrsPesqFact
	TRY
		GO lcPos
	CATCH
	ENDTRY
ENDFUNC


**
FUNCTION uf_PESQFACT_imprimirSeq 
		
		SELECT ucrsPesqFact
		GO TOP 
		SCAN 
			lcSQL = ''
			TEXT TO lcSQL NOSHOW TEXTMERGE 
				exec up_touch_ft '<<ALLTRIM(ucrsPesqFact.cabstamp)>>'
			ENDTEXT 
			uf_gerais_actGrelha("","FT",lcSQL)
			
			lcSQL = ''
			TEXT TO lcSQL NOSHOW TEXTMERGE 
				SELECT respostaDt = isnull((select top 1 DescrResposta from b_dt_resposta WHERE stampDoc = '<<ALLTRIM(ucrsPesqFact.cabstamp)>>' order by DataEnvio),''),* FROM FT2 (nolock) WHERE ft2stamp='<<ALLTRIM(ucrsPesqFact.cabstamp)>>'
			ENDTEXT 
			uf_gerais_actGrelha("","FT2",lcSQL)
			
			
			lcSQL = ''
			TEXT TO lcSQL NOSHOW TEXTMERGE 
				exec up_facturacao_linhas 0, <<mysite_nr>>, '<<ALLTRIM(ucrsPesqFact.cabstamp)>>'
			ENDTEXT 
			uf_gerais_actGrelha("","FI",lcSQL)	
			
			SELECT Ft
			SELECT Ft2
			SELECT Fi
			GO TOP 
			
			IF USED("TD")
				fecha("TD")
			ENDIF
		
			SELECT * FROM uCrsGeralTD WHERE uCrsGeralTD.ndoc == ft.ndoc AND ALLTRIM(uCrsGeralTD.site) == ALLTRIM(mySite) INTO CURSOR TD READWRITE
			
			SELECT ft
			SELECT ft2
			SELECT fi
			
			uf_imprimirgerais_chama('FACTURACAO', .t., .f., .f.)
			
		ENDSCAN 

ENDFUNC 

FUNCTION uf_PESQFACT_gerarSeq 
		public lcexportsyntstamp
		LOCAL lcCont2, lcCharSep, LcVar, lcNrLin
		lcCont2 = 1 
		lcNrLin = 0
		STORE "" TO lcexportsyntstamp, lcCharSep, LcVar
		
		SELECT ucrsPesqFact
		GO TOP 
		SCAN 
			IF ucrsPesqFact.escolha=.t.
				lcNrLin = lcNrLin + 1
			ENDIF 
		ENDSCAN 
		
		IF lcNrLin = 0
			uf_perguntalt_chama("N�O SELECIONOU NENHUM DOCUMENTO PARA EXPORTAR." + CHR(13) + "ATEN��O QUE TEM DE SELECIONAR 'PARA EXPORTA��O' NO TOPO DIREITO DA GRELHA DOS DOCUMENTOS.","OK","",48)
			RETURN .f.
		ENDIF 
		
		IF uf_perguntalt_chama("Deseja criar uma configura��o nova ou utilizar uma existente?" + CHR(13) + "Escolha a op��o pretendida?","Nova","Existente")
		
			lcSQL = ""
			TEXT TO lcSql NOSHOW TEXTMERGE 
				select 
					sel = CAST(0 as Bit)
					,design = 'Data'
					,campo='fdata'
					,ordem = 0
				union
				select 
					sel = CAST(0 as Bit)
					,design = 'Documento'
					,campo='nmdoc'
					,ordem = 0
				union
				select 
					sel = CAST(0 as Bit)
					,design = 'N� Documento'
					,campo='fno'
					,ordem = 0
				union
				select 
					sel = CAST(0 as Bit)
					,design = 'Nome Cliente'
					,campo='nome'
					,ordem = 0
				union
				select 
					sel = CAST(0 as Bit)
					,design = 'Nr. Cliente'
					,campo='no'
					,ordem = 0
				union
				select 
					sel = CAST(0 as Bit)
					,design = 'Estab. Cliente'
					,campo='estab'
					,ordem = 0
			ENDTEXT
			uf_gerais_actGrelha("","ucrsEXPORTSYNT",lcSQL)
			IF uf_exportsynt_chama()
			
			ELSE
				RETURN .f.
			ENDIF 
		ELSE
			TEXT TO lcSQL NOSHOW TEXTMERGE 
				select codigo = nome, exportsyntstamp from exportsynt (nolock) where [table]='pesqfact'
			ENDTEXT 
			uf_gerais_actGrelha("","curexportsynt",lcSQL)
			select curexportsynt 
			IF uf_valorescombo_chama("lcexportsyntstamp", 0, "curexportsynt ", 2, "exportsyntstamp ", "codigo")
				TEXT TO lcSQL TEXTMERGE noshow
					SELECT * from exportsynt WHERE exportsyntstamp='<<ALLTRIM(lcexportsyntstamp)>>'
				ENDTEXT
				uf_gerais_actGrelha("","ucrsEXPORTSYNT",lcSQL)
				TEXT TO lcSQL TEXTMERGE noshow
					SELECT * from exportsynt_linhas WHERE exportsyntstamp='<<ALLTRIM(lcexportsyntstamp)>>'
				ENDTEXT
				uf_gerais_actGrelha("","ucrsEXPORTSYNT_linhas",lcSQL)
			ELSE
				return(.f.)
			ENDIF 
			
		ENDIF 
		
		IF !USED("ucrsEXPORTSYNT")
			uf_perguntalt_chama("N�O SELECIONOU NENHUMA CONFIGURA��O.","OK","",48)
			RETURN .f.	
		ENDIF 
		IF RECCOUNT("ucrsEXPORTSYNT_linhas")=0
			uf_perguntalt_chama("N�O SELECIONOU NENHUMA CONFIGURA��O.","OK","",48)
			RETURN .f.			
		ENDIF 
		
		SELECT ucrsEXPORTSYNT
		lcCharSep=ALLTRIM(ucrsEXPORTSYNT.sepchar)
		
		** Selecionar pasta de destino para os ficheiros
		**oShell = CREATEOBJECT("Shell.Application")
		**oFolder = oShell.Application.BrowseForFolder(_screen.HWnd, "SELECCIONE A PASTA DE DESTINO:", 1)
		**if isnull(oFolder)
		**	RETURN .f.
		**ELSE
		**	? oFolder.self.Path
		**ENDIF
		**myFilePathSeq = oFolder.self.path +"\"

        **uv_path = ALLTRIM(uf_gerais_getUmValor("B_Terminal", "LTRIM(RTRIM(pathExport))", "terminal = '" + alltrim(myTerm) + "' and site = '" + alltrim(mySite) + "'"))
        uv_path = ALLTRIM(uf_gerais_getUmValor("B_Terminal", "LTRIM(RTRIM(pathExport))", "terminal = '" + alltrim(myTerm) + "'"))

        IF EMPTY(uv_path)

            uf_perguntalt_chama('Aten��o!' + chr(13) + 'N�o preencheu o par�metro "Caminho Export.".' + chr(13) +  'Ter� de preencher o mesmo para continuar.', "Ok", "", 48)
            RETURN .f.

        ENDIF

        IF !uf_gerais_createDir(uv_path)
            RETURN .F.
        ENDIF

        myFilePathSeq = uv_path + '\'

		SELECT * FROM ucrsPesqFact WHERE ucrsPesqFact.escolha=.t. INTO CURSOR ucrsPesqFactAux READWRITE
		
		SELECT ucrsPesqFactAux
		regua(0,RECCOUNT("ucrsPesqFactAux"),"A Exportar Documentos...")
					
		SELECT ucrsPesqFactAux
		GO TOP 
		SCAN 
			
			regua(1,lcCont2,"A Exportar Documento: " + astr(lcCont2))
			lcSQL = ''
			TEXT TO lcSQL NOSHOW TEXTMERGE 
				exec up_touch_ft '<<ALLTRIM(ucrsPesqFactAux.cabstamp)>>'
			ENDTEXT 
			uf_gerais_actGrelha("","FT",lcSQL)
			
			lcSQL = ''
			TEXT TO lcSQL NOSHOW TEXTMERGE 
				SELECT respostaDt = isnull((select top 1 DescrResposta from b_dt_resposta WHERE stampDoc = '<<ALLTRIM(ucrsPesqFactAux.cabstamp)>>' order by DataEnvio),''),* FROM FT2 (nolock) WHERE ft2stamp='<<ALLTRIM(ucrsPesqFactAux.cabstamp)>>'
			ENDTEXT 
			uf_gerais_actGrelha("","FT2",lcSQL)
			
			
			lcSQL = ''
			TEXT TO lcSQL NOSHOW TEXTMERGE 
				exec up_facturacao_linhas 0, <<mysite_nr>>, '<<ALLTRIM(ucrsPesqFactAux.cabstamp)>>'
			ENDTEXT 
			uf_gerais_actGrelha("","FI",lcSQL)	
			
			SELECT Ft
			SELECT Ft2
			SELECT Fi
			GO TOP 
			
			IF USED("TD")
				fecha("TD")
			ENDIF
		
			SELECT * FROM uCrsGeralTD WHERE uCrsGeralTD.ndoc == ft.ndoc AND ALLTRIM(uCrsGeralTD.site) == ALLTRIM(mySite) INTO CURSOR TD READWRITE
			myFileNameSeq = ""
			SELECT * FROM ucrsEXPORTSYNT_linhas ORDER BY ordem INTO CURSOR ucrsEXPORTSYNT_linhas_aux readwrite
			SELECT ucrsEXPORTSYNT_linhas_aux 
			GO TOP 	
			SCAN 
				LcVar = ALLTRIM(ucrsEXPORTSYNT_linhas_aux.campo)
				select ft
				FOR lnCnt = 1 TO FCOUNT()
					lcField = FIELD( lnCnt )
					**MESSAGEBOX(ALLTRIM(LcVar)+" "+ALLTRIM(lcField))
					IF UPPER(ALLTRIM(LcVar))==UPPER(ALLTRIM(lcField))
						luFVal = &lcField
						lcType = VARTYPE( luFVal )
	**					MESSAGEBOX(ALLTRIM(LcVar)+" "+ALLTRIM(lcField)+ " " + VARTYPE( luFVal ) )
						IF VARTYPE(luFVal)='C'
							luFVal = &lcField
						ENDIF 
						IF VARTYPE(luFVal)='N'
							luFVal = STR(&lcField)
						ENDIF 
						IF VARTYPE(luFVal)='D'
							luFVal = dtos(&lcField)
						ENDIF 
						IF VARTYPE(luFVal)='T'
							luFVal = LEFT(ttoc(&lcField),8)
						ENDIF 
						myFileNameSeq = myFileNameSeq + ALLTRIM(luFVal) + lcCharSep
					ENDIF 
				NEXT
				SELECT ucrsEXPORTSYNT_linhas_aux 
			ENDSCAN 
			myFileNameSeq = LEFT(ALLTRIM(myFileNameSeq), LEN(ALLTRIM(myFileNameSeq))-1 )
			myFileNameSeq = STRTRAN(ALLTRIM(myFileNameSeq),'.','')
			SELECT ft
			SELECT ft2
			SELECT fi
			**MESSAGEBOX(STRTRAN(STRTRAN(ALLTRIM(myFileNameSeq),'/',''),' ','')+".PDF")
			**myFileNameSeq = "fatura"
			**myFilePathSeq = "C:\temp\PDF\"
			myFileTableSeq = "FI"
			
			uf_imprimirgerais_chama('FACTURACAO', .t., .f., .f.)
			lcCont2 = lcCont2 + 1
		ENDSCAN 
		uf_perguntalt_chama("PROCESSO CONCLU�DO COM SUCESSO. POR FAVOR VERIFIQUE OS FICHEIROS NA DIRECTORIA DEFINIDA."+CHR(10)+CHR(10)+"NOTA: A GERA��O DOS FICHEIROS PODER� AINDA ESTAR EM CURSO.","OK","",64)
		regua(2)
		fecha("ucrsPesqFactAux")
ENDFUNC 


FUNCTION uf_PESQFACT_gerarSeq_AFP
		public lcexportsyntstamp
		LOCAL lcCont2, lcCharSep, LcVar, lcNrLin
		lcCont2 = 1 
		lcNrLin = 0
		STORE "" TO lcexportsyntstamp, lcCharSep, LcVar
		
		SELECT ucrsPesqFact
		GO TOP 
		SCAN 
			IF ucrsPesqFact.escolha=.t.
				lcNrLin = lcNrLin + 1
			ENDIF 
		ENDSCAN 
		
		IF lcNrLin = 0
			uf_perguntalt_chama("N�O SELECIONOU NENHUM DOCUMENTO PARA EXPORTAR." + CHR(13) + "ATEN��O QUE TEM DE SELECIONAR 'PARA EXPORTA��O' NO TOPO DIREITO DA GRELHA DOS DOCUMENTOS.","OK","",48)
			RETURN .f.
		ENDIF 
		
		IF uf_perguntalt_chama("Deseja criar uma configura��o nova ou utilizar uma existente?" + CHR(13) + "Escolha a op��o pretendida?","Nova","Existente")
		
			lcSQL = ""
			TEXT TO lcSql NOSHOW TEXTMERGE 
				select 
					sel = CAST(0 as Bit)
					,design = 'Data'
					,campo='fdata'
					,ordem = 0
				union
				select 
					sel = CAST(0 as Bit)
					,design = 'Documento'
					,campo='nmdoc'
					,ordem = 0
				union
				select 
					sel = CAST(0 as Bit)
					,design = 'N� Documento'
					,campo='fno'
					,ordem = 0
				union
				select 
					sel = CAST(0 as Bit)
					,design = 'Nome Cliente'
					,campo='nome'
					,ordem = 0
				union
				select 
					sel = CAST(0 as Bit)
					,design = 'Nr. Cliente'
					,campo='no'
					,ordem = 0
				union
				select 
					sel = CAST(0 as Bit)
					,design = 'Estab. Cliente'
					,campo='estab'
					,ordem = 0
			ENDTEXT
			uf_gerais_actGrelha("","ucrsEXPORTSYNT",lcSQL)
			IF uf_exportsynt_chama()
			
			ELSE
				RETURN .f.
			ENDIF 
		ELSE
			TEXT TO lcSQL NOSHOW TEXTMERGE 
				select codigo = nome, exportsyntstamp from exportsynt (nolock) where [table]='pesqfact'
			ENDTEXT 
			uf_gerais_actGrelha("","curexportsynt",lcSQL)
			select curexportsynt 
			IF uf_valorescombo_chama("lcexportsyntstamp", 0, "curexportsynt ", 2, "exportsyntstamp ", "codigo")
				TEXT TO lcSQL TEXTMERGE noshow
					SELECT * from exportsynt WHERE exportsyntstamp='<<ALLTRIM(lcexportsyntstamp)>>'
				ENDTEXT
				uf_gerais_actGrelha("","ucrsEXPORTSYNT",lcSQL)
				TEXT TO lcSQL TEXTMERGE noshow
					SELECT * from exportsynt_linhas WHERE exportsyntstamp='<<ALLTRIM(lcexportsyntstamp)>>'
				ENDTEXT
				uf_gerais_actGrelha("","ucrsEXPORTSYNT_linhas",lcSQL)
			ELSE
				return(.f.)
			ENDIF 
			
		ENDIF 
		
		IF !USED("ucrsEXPORTSYNT")
			uf_perguntalt_chama("N�O SELECIONOU NENHUMA CONFIGURA��O.","OK","",48)
			RETURN .f.	
		ENDIF 
		IF RECCOUNT("ucrsEXPORTSYNT_linhas")=0
			uf_perguntalt_chama("N�O SELECIONOU NENHUMA CONFIGURA��O.","OK","",48)
			RETURN .f.			
		ENDIF 
		
		SELECT ucrsEXPORTSYNT
		lcCharSep=ALLTRIM(ucrsEXPORTSYNT.sepchar)
		
		** Selecionar pasta de destino para os ficheiros
		**oShell = CREATEOBJECT("Shell.Application")
		**oFolder = oShell.Application.BrowseForFolder(_screen.HWnd, "SELECCIONE A PASTA DE DESTINO:", 1)
		**if isnull(oFolder)
		**	RETURN .f.
		**ELSE
		**	? oFolder.self.Path
		**ENDIF
		**myFilePathSeq = oFolder.self.path +"\"

        **uv_path = ALLTRIM(uf_gerais_getUmValor("B_Terminal", "LTRIM(RTRIM(pathExport))", "terminal = '" + alltrim(myTerm) + "' and site = '" + alltrim(mySite) + "'"))
        uv_path = ALLTRIM(uf_gerais_getUmValor("B_Terminal", "LTRIM(RTRIM(pathExport))", "terminal = '" + alltrim(myTerm) + "'"))

        IF EMPTY(uv_path)

            uf_perguntalt_chama('Aten��o!' + chr(13) + 'N�o preencheu o par�metro "Caminho Export.".' + chr(13) +  'Ter� de preencher o mesmo para continuar.', "Ok", "", 48)
            RETURN .f.

        ENDIF

        IF !uf_gerais_createDir(uv_path)
            RETURN .F.
        ENDIF

        myFilePathSeq = uv_path + "\"

		SELECT * FROM ucrsPesqFact WHERE ucrsPesqFact.escolha=.t. INTO CURSOR ucrsPesqFactAux READWRITE
		
		SELECT ucrsPesqFactAux
		regua(0,RECCOUNT("ucrsPesqFactAux"),"A Exportar Documentos...")
					
		SELECT ucrsPesqFactAux
		GO TOP 
		SCAN 
			
			regua(1,lcCont2,"A Exportar Documento: " + astr(lcCont2))
			lcSQL = ''
			TEXT TO lcSQL NOSHOW TEXTMERGE 
				exec up_touch_ft '<<ALLTRIM(ucrsPesqFactAux.cabstamp)>>'
			ENDTEXT 
			uf_gerais_actGrelha("","FT",lcSQL)
			
			lcSQL = ''
			TEXT TO lcSQL NOSHOW TEXTMERGE 
				SELECT respostaDt = isnull((select top 1 DescrResposta from b_dt_resposta WHERE stampDoc = '<<ALLTRIM(ucrsPesqFactAux.cabstamp)>>' order by DataEnvio),''),* FROM FT2 (nolock) WHERE ft2stamp='<<ALLTRIM(ucrsPesqFactAux.cabstamp)>>'
			ENDTEXT 
			uf_gerais_actGrelha("","FT2",lcSQL)
			
			
			lcSQL = ''
			TEXT TO lcSQL NOSHOW TEXTMERGE 
				exec up_facturacao_linhas 0, <<mysite_nr>>, '<<ALLTRIM(ucrsPesqFactAux.cabstamp)>>'
			ENDTEXT 
			uf_gerais_actGrelha("","FI",lcSQL)	
			
			SELECT Ft
			SELECT Ft2
			SELECT Fi
			GO TOP 
			
			IF USED("TD")
				fecha("TD")
			ENDIF
		
			SELECT * FROM uCrsGeralTD WHERE uCrsGeralTD.ndoc == ft.ndoc AND ALLTRIM(uCrsGeralTD.site) == ALLTRIM(mySite) INTO CURSOR TD READWRITE
			myFileNameSeq = ""
			SELECT * FROM ucrsEXPORTSYNT_linhas ORDER BY ordem INTO CURSOR ucrsEXPORTSYNT_linhas_aux readwrite
			SELECT ucrsEXPORTSYNT_linhas_aux 
			GO TOP 	
			SCAN 
				LcVar = ALLTRIM(ucrsEXPORTSYNT_linhas_aux.campo)
				select ft
				FOR lnCnt = 1 TO FCOUNT()
					lcField = FIELD( lnCnt )
					**MESSAGEBOX(ALLTRIM(LcVar)+" "+ALLTRIM(lcField))
					IF UPPER(ALLTRIM(LcVar))==UPPER(ALLTRIM(lcField))
						luFVal = &lcField
						lcType = VARTYPE( luFVal )
	**					MESSAGEBOX(ALLTRIM(LcVar)+" "+ALLTRIM(lcField)+ " " + VARTYPE( luFVal ) )
						IF VARTYPE(luFVal)='C'
							luFVal = &lcField
						ENDIF 
						IF VARTYPE(luFVal)='N'
							luFVal = STR(&lcField)
						ENDIF 
						IF VARTYPE(luFVal)='D'
							luFVal = dtos(&lcField)
						ENDIF 
						IF VARTYPE(luFVal)='T'
							luFVal = LEFT(ttoc(&lcField),8)
						ENDIF 
						myFileNameSeq = myFileNameSeq + ALLTRIM(luFVal) + lcCharSep
					ENDIF 
				NEXT
				SELECT ucrsEXPORTSYNT_linhas_aux 
			ENDSCAN 
			myFileNameSeq = LEFT(ALLTRIM(myFileNameSeq), LEN(ALLTRIM(myFileNameSeq))-1 )
			myFileNameSeq = STRTRAN(ALLTRIM(myFileNameSeq),'.','')

			SELECT ft
			SELECT ft2
			SELECT fi
**			MESSAGEBOX(myFileNameSeq)
			**myFileNameSeq = "fatura"
			**myFilePathSeq = "C:\temp\PDF\"
			myFileTableSeq = "FI"
			
			uf_imprimirgerais_chama('FACTURACAO', .t., .f., .f.)
			lcCont2 = lcCont2 + 1

			regua(1,lcCont2,"A enviar documento para FTP: " + astr(lcCont2))	
			uf_send_file_ftp("94.46.165.207", "afp", "yfV9xKSRn$im", "public_html/assets/media/docs/original/", myFilePathSeq+STRTRAN(STRTRAN(ALLTRIM(myFileNameSeq),'/',''),' ','')+".PDF", STRTRAN(STRTRAN(ALLTRIM(myFileNameSeq),'/',''),' ','')+".PDF")
			**uf_send_file_ftp("ftp.afp.com.pt", "afpcompt", "p0f181lOjL", "public_html/assets/media/docs/original/", myFilePathSeq+STRTRAN(STRTRAN(ALLTRIM(myFileNameSeq),'/',''),' ','')+".PDF", STRTRAN(STRTRAN(ALLTRIM(myFileNameSeq),'/',''),' ','')+".PDF")
			
			regua(1,lcCont2,"A registar o documento na BD: " + astr(lcCont2))	
			&& registar ficheiro na BD MySQL
			PUBLIC server,port,db,uid,pw, stringsend
			server="94.46.165.207"
			port="3306" && port mysql is open on
			db="afp_ws_db" && db name
			uid="afp_sa" && user anme
			pw="csgmNh)ueC[D" && password
			
*!*				server="cps6.webserver.pt"
*!*				port="3306" && port mysql is open on
*!*				db="afpcompt_mad2017" && db name
*!*				uid="afpcompt_mad" && user anme
*!*				pw="9Dq6Cm6Jf7Kl" && password

			mysql = SQLSTRINGCONNECT('Driver={MySQL ODBC 5.3 Unicode Driver};Server=' + server + ';Port=' + port + ';Database=' + db + ';uid=' + uid + ';Pwd=' + pw + ';',.T.)
			IF VARTYPE(mysql) == 'L' OR m.mysql < 0
			    MESSAGEBOX('MySQL Connection Failed. Logging will be disabled for this session.',16,'MySQL Error',3500)
			ENDIF
			
			TEXT TO lcSQL TEXTMERGE NOSHOW
				call  afp_ws_db.importInvoices(<<ALLTRIM(STR(ft.no))>>,'<<ALLTRIM(ft.nmdoc)>> nr <<ALLTRIM(STR(ft.fno))>> de <<ALLTRIM(STR(MONTH(ft.fdata))))>>-<<ALLTRIM(STR(YEAR(ft.fdata)))>>','<<STRTRAN(STRTRAN(ALLTRIM(myFileNameSeq),'/',''),' ','')+".PDF">>',@out);
			ENDTEXT 	

			IF VARTYPE(mysql) != 'L' AND m.mysql >= 0
			    SQLEXEC(mysql ,lcSQL, "")
			ENDIF

			TEXT TO lcSQL TEXTMERGE NOSHOW
				select * from afp_ws_db.afp_docs where name='<<ALLTRIM(ft.nmdoc)>> nr <<ALLTRIM(STR(ft.fno))>> de <<ALLTRIM(STR(MONTH(ft.fdata))))>>-<<ALLTRIM(STR(YEAR(ft.fdata)))>>'
			ENDTEXT 	

			IF VARTYPE(mysql) != 'L' AND m.mysql >= 0
			    SQLEXEC(mysql ,lcSQL, "UCRSRETURNVAL")
			ENDIF
			IF !USED("UCRSRETURNVAL")
				uf_perguntalt_chama("O SEGUINTE FICHEIRO N�O FICOU REGISTADO NA BD MYSQL - " + STRTRAN(STRTRAN(ALLTRIM(myFileNameSeq),'/',''),' ','') + ".PDF","OK","",48)
			ENDIF 	
			fecha("UCRSRETURNVAL")
			
			**sqldisconnect(mysql)		
		ENDSCAN 
		uf_perguntalt_chama("PROCESSO CONCLU�DO COM SUCESSO. POR FAVOR VERIFIQUE OS FICHEIROS NA DIRECTORIA DEFINIDA."+CHR(10)+CHR(10)+"NOTA: A GERA��O DOS FICHEIROS PODER� AINDA ESTAR EM CURSO.","OK","",64)
		regua(2)
		fecha("ucrsPesqFactAux")
ENDFUNC 



**
FUNCTION uf_PESQFACT_sair
	IF TYPE("PESQFACT") != "U"
		PESQFACT.hide
		PESQFACT.release
	ENDIF 
ENDFUNC

FUNCTION uf_PESQFACT_seltodos
	SELECT ucrsPesqFact
	GO TOP 
	SCAN
		replace ucrsPesqFact.escolha WITH .t.
	ENDSCAN 

	SELECT ucrsPesqFact
	GO TOP 
	PESQFACT.GridPesq.Refresh
	
ENDFUNC 

FUNCTION uf_PESQFACT_ftporreceber
	lcSQL = ''
			TEXT TO lcSQL NOSHOW TEXTMERGE 
				select CONVERT(varchar,fdata, 102) as Data,nmdoc as Documento,fno as DocNR,no as NrCliente,nome as Nome,etotal as Total from ft (nolock) -- where ftstamp not in (select regstamp from table_sync_status (nolock))
				where nmdoc='Factura'
				and (ft.ftstamp in (select distinct ftstamp from fi (nolock) where bistamp<>'' and nmdoc='Factura' and fistamp not in (select ofistamp from fi where nmdoc in ('Nota de Cr�dito','Reg. a Cliente')))
				and ftstamp in (select ftstamp from cc (nolock) where ftstamp<>'' and cmdesc='N/Factura' and edebf=0)
				)
				order by ft.fdata
			ENDTEXT 
			uf_gerais_actGrelha("","ucrsftporrec",lcSQL)
			select ucrsftporrec
			brow
ENDFUNC 