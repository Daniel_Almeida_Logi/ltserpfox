**Carregar dependencias
DO pesqfact
DO importarfact
DO reimppos
DO dadoscliente
DO mvr
DO pesqlote




** Define class para os eventos **
DEFINE CLASS pbordofact As Custom

	PROCEDURE proc_clickLOTE
		IF myFTIntroducao == .t. OR myFTAlteracao == .t.
			SELECT ft
			SELECT FI
			IF fi.usalote == .t.
				uf_PESQLOTE_chama(fi.lote, fi.ref, "FACTURACAO", ft.tipoDoc)
			ENDIF
		ENDIF
	ENDPROC
	
	**
	PROCEDURE proc_gotFocusLOTE
		WITH FACTURACAO.PageFrame1.Page1.gridDoc
			FOR i=1 TO .columnCount
				**EVENTO Lote
				If Upper(.Columns(i).ControlSource)=="FI.LOTE"
					*!*	&& Sempre readonly, porque n�o � suposto na factura��o a cria��o de novos Lotes
				ENDIF
			ENDFOR
		ENDWITH	
	ENDPROC

	** Escolhe Diploma nas Linhas 
	PROCEDURE proc_EscolheDiploma
		Local lcValida
		Public myFtIntroducao, myFtAlteracao 

		If myFtIntroducao == .t. OR myFtAlteracao == .t.
		
			Store .t. To lcValida
			
			Select fi
			If !Empty(Fi.u_diploma)
				Replace Fi.u_diploma With "" In fi
				uf_atendimento_actValoresFi()
				uf_atendimento_actTotaisFt()
				
				*Set Focus na Coluna Seguinte
				FOR i=1 TO  FACTURACAO.PageFrame1.Page1.gridDoc.columnCount
					If Upper( FACTURACAO.PageFrame1.Page1.gridDoc.Columns(i).ControlSource)=="FI.U_COMP"
						 FACTURACAO.PageFrame1.Page1.gridDoc.Columns(i).setfocus
						EXIT
					ENDIF
				ENDFOR
			ELSE
				Select Ft2
				IF !Empty(Alltrim(Ft2.u_codigo))
					
				     ** Identificar o plano associado ao documento **
					IF uf_gerais_actGrelha("","uCrsPlaDip",[select diploma from cptpla (nolock) where codigo=']+Alltrim(Ft2.u_codigo)+['])
						IF RECCOUNT("uCrsPlaDip")=0
						 	Store .f. To lcValida
						ENDIF
						   
						IF uCrsPlaDip.diploma!="1"
							Store .f. To lcValida
						ENDIF
						   
						fecha("uCrsPlaDip")
						IF lcValida == .t.
							myDiplomaAuto = .f.
							uf_diplomas_chama()
						ENDIF
					ENDIF
				ENDIF
				uf_atendimento_actValoresFi()
				uf_atendimento_actTotaisFt()
	     	ENDIF
	     ENDIF
	ENDPROC
		
	*Activa Eventos nas linhas
	Procedure proc_ActivaEventosLinhasFact
		uf_facturacao_EventosGridDoc()
	Endproc
	
	**Valida se existem vales de Desconto ao alterar descontos em valor
	Procedure proc_validaExistenciaValesDesconto
		
		PUBLIC myValeDescVal 
		LOCAL lcPos, lcValida, lcOldDescVal, ti
		STORE .t. To lcValida
		STORE 0 To lcOldDescVal, ti
		
		** Existem Vales de Descontos
		Select Fi	
		lcOldDescVal = Fi.u_descval

		If myValeDescVal == .t.
		
			WITH FACTURACAO.PageFrame1.Page1.gridDoc
				
				FOR i=1 TO .columnCount
					If Upper(.Columns(i).ControlSource)=="FI.U_DESCVAL"
						.columns(i).text1.value = Round(lcOldDescVal,2)
					Endif
				ENDFOR
			ENDWITH	
			uf_perguntalt_chama("DESCULPE, N�O � POSS�VEL APLICAR DESCONTOS EM VALOR MANUALMENTE, SE EXISTIREM VALES DE DESCONTO. POR FAVOR VERIFIQUE.","OK","",16)
		Endif
	ENDPROC 
	
	** Chama painel para altera��o de PVP's
	Procedure proc_PainelPVPS		

		DODEFAULT()

		IF myFtIntroducao == .t. Or myFtAlteracao == .t.

			SELECT FI
			uf_atendimento_alteraPvp(fi.fistamp)
		ENDIF 
	ENDPROC 

	** Nunca � permitido em caso algum alterar o Total na Linha
	Procedure proc_validaAlteracaoTotalLinha		
		Local lcPrecoUni
		Store 0 To lcPrecoUni
		
		Select fi
		If fi.qtt == 0
			replace fi.qtt	With 1
		Endif
		
		Select fi
		If fi.qtt != 0
			lcPrecoUni		= fi.etiliquido
			
			If fi.desconto>0 And fi.desconto<100
				lcPrecoUni	= lcPrecoUni / ((100-fi.desconto)/100)
			Endif
			If fi.desc2>0 And fi.desc2<100
				lcPrecoUni	= lcPrecoUni / ((100-fi.desc2)/100)
			Endif
			If fi.desc3>0 And fi.desc3<100
				lcPrecoUni	= lcPrecoUni / ((100-fi.desc3)/100)
			Endif
			If fi.desc4>0 And fi.desc4<100
				lcPrecoUni	= lcPrecoUni / ((100-fi.desc4)/100)
			Endif
			
			lcPrecoUni		= lcPrecoUni / fi.qtt
				
			If !lcPrecoUni=0
				replace fi.epv with ROUND(lcPrecoUni,2)
			ENDIF
			
			Select td
			If td.tipodoc=3
				If fi.etiliquido>0
					replace fi.etiliquido with fi.etiliquido* (-1)
				Endif
			ENDIF
			
			IF !EMPTY(ALLTRIM(fi.ref))
				uf_atendimento_actRef()
			ENDIF 
		Endif

		IF !EMPTY(ALLTRIM(fi.ref))
			uf_atendimento_actValoresFi()
		ENDIF
		uf_atendimento_actTotaisFt()
		uf_facturacao_DesactivaEventosLinhasFact()
	Endproc

	**
	Procedure proc_ApagarAutocompleteRefFi
		lparameters nKeyCode, nShiftAltCtrl
		mykeypressed = nKeyCode
	Endproc

	**
	Procedure proc_autocompleteRefFi
		lparameters nKeyCode		
		
		facturacao.eventoRef = .t.
		uf_facturacao_autocompleteRefFi(nKeyCode)
	Endproc
	
	**Right click procura Ref
	Procedure proc_pesquisarArtigosRefFi		
		Local lcSql, lcRef
		Store '' To lcSQL, lcRef
		**
		
		uf_pesqstocks_chama("FACTURACAO", facturacao.valorparapesquisa)
		uf_facturacao_DesactivaEventosLinhasFact()
	Endproc

	**Autocomplete - Designa��o
	Procedure proc_ApagarAutocompleteDesignFi
		lparameters nKeyCode, nShiftAltCtrl
		mykeypressed = nKeyCode
	Endproc

	**
	Procedure proc_autocompleteDesignFi
		lparameters nKeyCode		
		uf_facturacao_autocompleteDesign(nKeyCode)
	Endproc

	**Pesquisar Artigos
	Procedure proc_pesquisarArtigosDesignFi		
		uf_facturacao_pesquisarArtigosDesignFi()
		uf_facturacao_DesactivaEventosLinhasFact()
	Endproc
	
	* Corre na Altera��o da Coluna de Refer�ncia	
	Procedure proc_eventoRefFi
		uf_facturacao_eventoRefFi()
		facturacao.eventoRef = .f.
		uf_facturacao_DesactivaEventosLinhasFact()
	Endproc

	* ActualizaTotais das Linhas
	PROCEDURE proc_actualizaTotaisFI
		LOCAL lcFistamp

		** Se tiver o sistema de Lotes activo, verifica necessidade altera��o Lote
		**
		IF uf_gerais_getParameter("ADM0000000211","BOOL")
			uf_facturacao_AtribuiLoteAlteraQtt()
		ENDIF
		uf_atendimento_fiiliq(.t.)	
		uf_atendimento_actValoresFi()
		uf_atendimento_actTotaisFt()
		uf_facturacao_DesactivaEventosLinhasFact()
		
	ENDPROC
	
	**
	PROCEDURE proc_actualizaTotaisFIIvaIncl

		uf_atendimento_fiiliq(.t.)	
		uf_atendimento_actValoresFi()
		uf_atendimento_actTotaisFt()
		uf_facturacao_DesactivaEventosLinhasFact()
		
	ENDPROC
	
	**
	PROCEDURE proc_clickCCUSTO
		IF myFTIntroducao == .t. OR myFTAlteracao == .t.
			uf_valorescombo_chama("FI.FICCUSTO", 2, "ucrsCCusto", 2, "DESCRICAO", "DESCRICAO")
		ENDIF
	ENDPROC
	
	* Corre com duplo click na Referencia
	Procedure proc_navegast	
		Select Fi
		If !Empty(Fi.REF)
			* Verifica Existencia da Referencia
			TEXT TO lcSQL TEXTMERGE NOSHOW
				Select ref FROM ST (nolock) WHERE REF = '<<Alltrim(FI.REF)>>'
			ENDTEXT
			IF !uf_gerais_actGrelha("","ucrsExisteRef",lcSQL)
				uf_perguntalt_chama("OCORREU UMA ANOMALIA NA VERIFICA��O DOS DADOS DA REFER�NCIA, POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				RETURN .f.
			ENDIF
			 
			If Reccount("ucrsExisteRef") > 0
				&& VERIFICA SE ESTAVA ALGUM REGISTO EM EDI��O / INSER��O
				IF !(TYPE("gestao_stocks") == "U")
					IF !(gestao_stocks.Caption) == "Stocks e Servi�os"
						uf_perguntalt_chama("O ECR� DE STOCKS ENCONTRA-SE EM MODO DE ALTERA��O / INTRODU��O! TERMINE ESTA OPERA��O E TENTE NOVAMENTE.","OK","",64)
						RETURN .f.
					Endif
				Endif 	
				uf_stocks_chama(Alltrim(FI.REF))
			Endif
			If Used("ucrsExisteRef")
				Fecha("ucrsExisteRef")
			Endif
		Endif
	ENDPROC
	
	**
	PROCEDURE proc_clickIva
		public mylcCodIva
				
		LOCAL lcTaxaIva, lcTabIva
		
		IF myFtAlteracao == .t. OR myFtIntroducao == .t.

			uf_valorescombo_chama("mylcCodIva", 0, "select sel = convert(bit,0), codigo = CONVERT(int,codigo), taxa from taxasiva (nolock)", 1, "codigo", "codigo, taxa")
		
			IF !EMPTY(mylcCodIva)
						
				lcTaxaIva = uf_gerais_getTabelaIva(mylcCodIva,"TAXA")
				lcTabIva = mylcCodIva
		
				Select FI
				Replace FI.tabiva WITH lcTabIva  
				REPLACE FI.IVA With lcTaxaIva
	
				uf_atendimento_fiiliq(.t.)
				uf_atendimento_actTotaisFt()
			ENDIF
		ENDIF
	ENDPROC
	
	PROCEDURE proc_codmotiseimp
	
		SELECT fi 
		IF fi.iva=0 AND !EMPTY(fi.ref)
			LOCAL lcSQL
			lcSQL = ""
			TEXT TO lcSQL TEXTMERGE NOSHOW
				select code_motive, campo from b_multidata (nolock) where tipo = 'motiseimp' AND pais='<<ALLTRIM(ucrse1.pais)>>' ORDER BY campo
			ENDTEXT
			If !uf_gerais_actGrelha("", "ucrsTempMotiseimp", lcSql)
				uf_perguntalt_chama("N�o foi possivel encontrar regitos.", "", "OK", 16)
				RETURN .f.
			ENDIF

			public codmotiseimplin		
			codmotiseimplin = ''
			uf_valorescombo_chama("codmotiseimplin", 0, "ucrsTempMotiseimp", 2,  "campo", "code_motive, campo ", .F., .F., .T.)
			fecha("ucrsTempMotiseimp")
			
			IF len(codmotiseimplin) != 0
				lcSQL = ""
				TEXT TO lcSQL TEXTMERGE NOSHOW
					select code_motive, campo from b_multidata (nolock) where tipo = 'motiseimp' AND campo='<<ALLTRIM(codmotiseimplin)>>' AND pais='<<ALLTRIM(ucrse1.pais)>>' ORDER BY campo
				ENDTEXT
				If !uf_gerais_actGrelha("", "ucrsTempMotiseimp", lcSql)
					uf_perguntalt_chama("N�o foi possivel encontrar regitos.", "", "OK", 16)
					RETURN .f.
				ENDIF
				LOCAL lcMot, lcCodMot
				STORE '' TO lcMot, lcCodMot
				SELECT ucrsTempMotiseimp
				lcMot = ALLTRIM(ucrsTempMotiseimp.campo)
				lcCodMot = ALLTRIM(ucrsTempMotiseimp.code_motive)
				SELECT fi 
				replace fi.codmotiseimp WITH lcCodMot 
				replace fi.motiseimp WITH lcMot 
			ELSE
				**uf_perguntalt_chama("N�o selecionou nenhum motivo de isen��o.", "", "OK", 16)
				**RETURN .f.
				SELECT fi 
				replace fi.codmotiseimp WITH '' 
				replace fi.motiseimp WITH '' 
			ENDIF 
		ENDIF 

	ENDPROC 
	
	** verifica campo armazem
	PROCEDURE proc_validaAlteracaoArmazem
		Select Fi

		IF Fi.armazem == 0
			uf_perguntalt_chama("ARMAZ�M INVALIDO.","OK","",16)
			RETURN .f.
		ENDIF

		SELECT FI
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			exec up_gerais_armazensDisponiveis '<<ALLTRIM(mysite)>>', '<<fi.ref>>'
		ENDTEXT 		
		
		IF !uf_gerais_actGrelha("", "uCrsArmazensDisp",lcSql)
			uf_perguntalt_chama("N�O FOI POSSIVEL VERIFICAR OS ARMAZ�NS DISPONIVEIS. POR FAVOR CONTACTE O SUPORTE","OK","",16)
			RETURN .f.
		ENDIF
		
		SELECT uCrsArmazensDisp
		LOCATE FOR uCrsArmazensDisp.armazem == FI.armazem
		IF !FOUND()
			uf_perguntalt_chama("Armaz�m inv�lido para Empresa Atual. Por favor verifique.","OK","",48)
			SELECT FI
			Replace FI.armazem WITH myarmazem
			
			IF fi.usalote == .t.
				Replace Fi.lote with ""
			ENDIF
		ELSE
			IF fi.usalote == .t.
				uf_facturacao_AtribuiLote()
			ENDIF
		ENDIF 
			
		IF USED("uCrsArmazensDisp")
			fecha("uCrsArmazensDisp")
		ENDIF 
	ENDPROC
	
	** Ordena linha
	PROCEDURE proc_OrdenaPorDesignDoc
		uf_facturacao_OrdenaPorDesignDoc()
	ENDPROC
	
ENDDEFINE

**
DEFINE CLASS MYCOMBOFICCUSTO as comboBox
	Procedure LostFocus
		**
	EndProc
ENDDEFINE   

**
DEFINE CLASS commandButtonDestinos AS CommandButton
	Procedure Click
		IF myFtAlteracao == .f. AND myFtIntroducao == .f.
			uf_OrigensDestinos_Chama('FTDESTINOS')
		ENDIF
	Endproc
ENDDEFINE

**
DEFINE CLASS commandButtonOrigens AS CommandButton
	Procedure Click
		IF myFtAlteracao == .f. AND myFtIntroducao == .f.
			uf_OrigensDestinos_Chama('FTORIGENS')
		ENDIF
	Endproc
ENDDEFINE

**
DEFINE CLASS commandButtonMV AS CommandButton
	PROCEDURE Click
		IF myFtAlteracao == .t. OR myFtIntroducao == .t.
			***
			select ft
			IF !(ALLTRIM(ft.nmdoc) == 'Inser��o de Receita')
				uf_perguntalt_chama("S� � POSS�VEL UTILIZAR ESTA FUNCIONALIDADE NAS 'INSER��ES DE RECEITA'.","OK","",48)
				RETURN .f.
			ENDIF 
			Select ft2
			IF Empty(ft2.u_codigo)
				uf_perguntalt_chama("S� � POSS�VEL UTILIZAR ESTA FUNCIONALIDADE EM DOCUMENTOS COM RECEITA.","OK","",48)
				RETURN .f.
			ENDIF 
			Select fi
			If Empty(fi.ref)
				uf_perguntalt_chama("DEVE SELECIONAR PRIMEIRO A LINHA DA RECEITA QUE PRETENDE ALTERAR. DEVE EXISTIR REFER�NCIA.","OK","",48)
				RETURN .f.
			ENDIF 
			
			Select fi
			lcSQL = ""
			TEXT TO lcSQL NOSHOW TEXTMERGE
				exec up_stocks_pesquisaNoDic '<<Alltrim(fi.ref)>>', '', '',0
			ENDTEXT 
			
			IF uf_gerais_actGrelha("","uCrsExFprod",lcSQL)
				IF !Reccount("uCrsExFprod")>0
					uf_perguntalt_chama("S� PODE MANIPULAR O VALOR DE COMPARTICIPA��O EM PRODUTOS EXISTENTES NO DICION�RIO.","OK","",48)
					Fecha("uCrsExFprod")
					RETURN 
				ENDIF 
				Fecha("uCrsExFprod")
			ENDIF 
			uf_MVR_chama()
		ENDIF
	ENDPROC 
ENDDEFINE


**
FUNCTION uf_facturacao_chama
	Lparameters tcStamp
	
	** Verificar se existe nova vers�o para reiniciar o software
	uf_chk_nova_versao()	
	
	**Valida Licenciamento	
	IF (uf_ligacoes_addConnection('FACT') == .f.)
		RETURN .F.
	ENDIF
	
	
	release myInsereReceitaCorrigida
		
	
	&& CONTROLA PERFIS DE ACESSO
	IF uf_gerais_validaPermUser(ch_userno,ch_grupo,'Gest�o de Factura��o')
		IF !(TYPE('ATENDIMENTO') == "U") AND !FactCompartSeg
			uf_perguntalt_chama("N�O PODE ABRIR O ECR� DE FACTURA��O ENQUANTO O ECR� DE ATENDIMENTO ESTIVER ABERTO.","OK","",48)
			RETURN .f.
		ENDIF
*!*			IF !(TYPE('IDENTIFICACAO') == "U")
*!*				uf_perguntalt_chama("N�O PODE ABRIR O ECR� DE FACTURA��O ENQUANTO O ECR� DE IDENTIFICA��O PARA O ATENDIMENTO ESTIVER ABERTO.","OK","", 48)
*!*				RETURN .f.
*!*			ENDIF
		
		IF !(TYPE('FACTURACAO_ENTIDADES') == "U")
			uf_perguntalt_chama("N�O PODE ABRIR O ECR� DE FACTURA��O ENQUANTO O ECR� DE FACTURA��O �S ENTIDADES ESTIVER ABERTO.","OK","", 48)
			RETURN .f.
		ENDIF
	ELSE
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ACEDER AO PAINEL GEST�O DE FACTURA��O.","OK","",48)
		RETURN .f.
	ENDIF
	

	
	**verificar se o ecr� de atendimento se encontra aberto
	IF TYPE("ATENDIMENTO")!="U" AND !FactCompartSeg
		RETURN .f.
	ENDIF
	
	**fecha ecr� de pesquisa de utentes
	IF TYPE("PESQUTENTES") != "U"
		uf_pesqutentes_sair()
	ENDIF
	
	

	regua(0,8,"A carregar painel...")
	regua(1,1,"A carregar painel...")
	
	** Se o stamp vier vazio coloca stamp do ultimo documento, o parametro � irrelevante neste passo 
	LOCAL lcNAt
	IF EMPTY(tcStamp)
		lcNAt = ALLTRIM(uf_gerais_devolveUltRegisto(ALLTRIM(STR(myTermNo))))
		uf_gerais_actGrelha("","uCrsTempStamp","select top 1 ftstamp from ft (nolock) where u_nratend='"+lcNAt + IIF(myTermNo<10,'0'+alltrim(STR(myTermNo)),alltrim(STR(myTermNo)))+"' order by ftano desc")
		SELECT uCrsTempStamp
		tcStamp = alltrim(uCrsTempStamp.ftstamp)
		IF USED("uCrsTempStamp")
			fecha("uCrsTempStamp")
		ENDIF 
	ENDIF

	** Cria cursor com meios de pagamento
	IF !USED("UcrsConfigModPag")
		TEXT TO lcSQL NOSHOW TEXTMERGE
			select * from B_modoPag
		ENDTEXT
		
		If !uf_gerais_actGrelha("", "UcrsConfigModPag", lcSql)
			uf_perguntalt_chama("N�o foi poss�vel verificar as configura��es de Modo de Pagamento. Contacte o suporte.", "", "OK", 32)
			RETURN .f.
		ENDIF
	ENDIF	
	
	
	***
	PUBLIC myFtIntroducao, myFtAlteracao, myResizeFacturacao, myFactAGravar
		
	IF myFtIntroducao == .t. OR myFtAlteracao == .t.
		uf_perguntalt_chama("O ECR� DE FACTURA��O ENCONTRA-SE EM MODO DE INTRODU��O OU EDI��O.","OK","",48)
		regua(2)
		IF !(TYPE("FACTURACAO") == "U")
			FACTURACAO.show
		ENDIF
		RETURN .f.
	ENDIF
	
	regua(1,2,"A carregar painel...")	
	** VALIDA PERFIL **
		** levanta nome
	TEXT TO lcsql NOSHOW TEXTMERGE
		SELECT TOP 1 nmdoc as nomedoc FROM ft (nolock) WHERE ftstamp='<<ALLTRIM(tcStamp)>>'
	ENDTEXT
	
	

	
	IF uf_gerais_actGrelha("","uCrsNomeDoc",lcSql)
		IF RECCOUNT()>0
			IF !(uf_gerais_validaPermUser(ch_userno, ch_grupo, ALLTRIM(uCrsNomeDoc.nomedoc) + ' - Visualizar'))
				IF TYPE("FACTURACAO") != "U"
					uf_perguntalt_chama("O SEU PERFIL N�O PERMITE VISUALIZAR DOCUMENTOS DO TIPO: "+UPPER(ALLTRIM(uCrsNomeDoc.nomedoc))+".","OK","",48)
				ENDIF
				fecha("uCrsNomeDoc")
				tcStamp=""
				*Return .f.
			ENDIF
		ENDIF
		fecha("uCrsNomeDoc")
	ENDIF
	

	
	IF uf_gerais_getParameter_site('ADM0000000073', 'BOOL', mySite)
		IF USED("fimancompart")
			fecha("fimancompart")
		ENDIF
			
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			select 
				fistamp, ref, design, cast(00000000.00 as numeric(10,2)) as valor, cast(000.00 as numeric(5,2)) as perc, cast(00000000.00 as numeric(10,2)) as valortot	 
			from 
				fi 
			where 
				0=1
		ENDTEXT 
		uf_gerais_actGrelha("","fimancompart",lcSQL)
	ENDIF 
	
	**
		
		regua(1,3,"A carregar painel...")
		uf_Facturacao_CriaCursores(tcStamp)
		
		
		
		regua(1,4,"A carregar painel...")
		uf_Facturacao_carregaDocumentos()
	
	IF TYPE("FACTURACAO")=="U"

		**Valida Licenciamento	
		IF (uf_gerais_addConnection('FACT') == .f.)
			regua(2)
			RETURN .F.
		ENDIF
		
		regua(1,5,"A carregar painel...")	
		DO FORM FACTURACAO
		
		&& Configura menu principal
		**FACTURACAO.menu1.adicionaOpcao("aplicacoes", "Aplica��es", myPath + "\imagens\icons\aplicacoes_w.png", "uf_gerais_menuAplicacoes With 'FACTURACAO','Aplica��es'","")
		FACTURACAO.menu1.adicionaOpcao("opcoes","Op��es", myPath + "\imagens\icons\opcoes_w.png","","")
		FACTURACAO.menu1.adicionaOpcao("pesquisar","Pesquisar",myPath + "\imagens\icons\lupa_w.png","uf_pesqFact_chama with 'FACTURACAO'","")
		FACTURACAO.menu1.adicionaOpcao("novo","Novo",myPath + "\imagens\icons\mais_w.png","uf_Facturacao_novo","")
		FACTURACAO.menu1.adicionaOpcao("editar","Editar",myPath + "\imagens\icons\lapis_w.png","uf_facturacao_alterarDoc","")
		FACTURACAO.menu1.adicionaOpcao("anterior","Anterior",myPath + "\imagens\icons\ponta_seta_left_w.png","uf_facturacao_registoanterior","")
		FACTURACAO.menu1.adicionaOpcao("seguinte","Seguinte",myPath + "\imagens\icons\ponta_seta_rigth_w.png","uf_facturacao_registoseguinte","")
		FACTURACAO.menu1.adicionaOpcao("impDoc","Importar",myPath + "\imagens\icons\importar.png","uf_facturacao_importDocChama","")
		FACTURACAO.menu1.adicionaOpcao("dem","DEM",myPath + "\imagens\icons\detalhe_micro.png","uf_facturacao_DEM","")
		**FACTURACAO.menu1.adicionaOpcao("fichaUtente","Utente",myPath + "\imagens\icons\detalhe.png","uf_facturacao_fichaUtente","")
		FACTURACAO.menu1.adicionaopcao("novaLinha","Nova Linha",myPath + "\imagens\icons\mais_w.png","uf_facturacao_addLinha","")
		FACTURACAO.menu1.adicionaopcao("eliminarLinha","Eliminar Linha",myPath + "\imagens\icons\eliminar_w.png","uf_facturacao_remLinha","")
		FACTURACAO.menu1.adicionaopcao("pesquisarStock","Pesquisar",myPath + "\imagens\icons\lupa_w.png","uf_pesqstocks_chama with 'FACTURACAO'","")
		FACTURACAO.menu1.adicionaopcao("enviarncSNS","Enviar Doc.",myPath + "\imagens\icons\pasta_w.png","uf_facturacaoentidades_envioNCSNS","")
		**FACTURACAO.menu1.adicionaOpcao("anexos", "Anexos", myPath + "\imagens\icons\anexar_w.png", "uf_anexardoc_chama with 'faturacao', ALLTRIM(ft.ftstamp), ALLTRIM(ft.nmdoc) + ' '+ ALLTRIM(STR(ft.fno)), ALLTRIM(STR(ft.fno))", "")		

		

		&& Configura menu de Aplica��es
		**FACTURACAO.menu_aplicacoes.adicionaOpcao("impResumo","Impress�o Resumos","","uf_resumos_chama")
		**FACTURACAO.menu_aplicacoes.adicionaOpcao("regVacinacao","Registo Vacina��o","","uf_facturacao_OP_vacinacao")
		
		** Valida Perfil
**		IF !(uf_gerais_validaPermUser(ch_userno, ch_grupo, 'Importa��o Hist�rico de Vendas - Fatura��o'))
**			FACTURACAO.menu_aplicacoes.adicionaOpcao("impHistVendas","Imp. Hist. Vendas","","uf_facturacao_importHistVendas")
**		ENDIF
	
		*!* Op��o apenas dispon�vel para Armazenistas que n�o utilizam de momento
		*!*	IF uf_gerais_getParameter("ADM0000000211","BOOL")
		*!*		FACTURACAO.menu_aplicacoes.adicionaOpcao("confAviamentos","Conf. Aviamentos","","uf_facturacao_ConfEncomendasAviamentos")
		*!*	ENDIF
		
		&& Configura menu de op��es
		FACTURACAO.menu_opcoes.adicionaOpcao("imprimir","Imprimir","","uf_imprimirgerais_Chama with 'FACTURACAO'")
		FACTURACAO.menu_opcoes.adicionaOpcao("pos","Imprimir POS","","uf_reimppos_chama with 'FT'")
		FACTURACAO.menu_opcoes.adicionaOpcao("corrigirReceita","Corrigir Receita","","uf_facturacao_corrigirReceitaAuto")
		FACTURACAO.menu_opcoes.adicionaOpcao("dadosReceita","Dados Receita","","uf_facturacao_corrigirReceita")
		FACTURACAO.menu_opcoes.adicionaOpcao("consultarReceita","Consultar Receita","","uf_facturacao_consultarReceita")
		FACTURACAO.menu_opcoes.adicionaOpcao("reinserirReceita","Reinserir Receita","","uf_facturacao_reinserirReceita")
		FACTURACAO.menu_opcoes.adicionaOpcao("cliente","Dados Cliente","","uf_dadoscliente_Chama")
		FACTURACAO.menu_opcoes.adicionaOpcao("enviaDT","Enviar Doc. Transp.","","uf_facturacao_enviaDT")				
		FACTURACAO.menu_opcoes.adicionaOpcao("eliminar","Eliminar","","uf_facturacao_eliminarDoc")
		FACTURACAO.menu_opcoes.adicionaOpcao("ultimo","�ltimo","","uf_Facturacao_ultimoRegisto")
		FACTURACAO.menu_opcoes.adicionaOpcao("talaonnordisk","Reimp. Tal�o Controlo","","uf_talao_novonordisk")
        FACTURACAO.menu_opcoes.adicionaOpcao("fichaUtente","Utente","","uf_facturacao_fichaUtente")

		FACTURACAO.menu_opcoes.adicionaOpcao("alterameiospagamento","Meios Pag.","","uf_Facturacao_alterameiospagamento")
		FACTURACAO.menu_opcoes.adicionaOpcao("anexos", "Anexos", "", "uf_anexardoc_chama with 'FT', ALLTRIM(ft.ftstamp), ALLTRIM(ft.nmdoc) + ' '+ ALLTRIM(STR(ft.fno)), ALLTRIM(STR(ft.fno))")		
		FACTURACAO.menu_opcoes.adicionaOpcao("exportFTEntidadeADSE","Ft.ADSE","","uf_FACTENTIDADESCLINICA_EXPORT_FT WITH 'ADSE'")
		FACTURACAO.menu_opcoes.adicionaOpcao("exportFTEntidadeADM","Ft.ADM","","uf_FACTENTIDADESCLINICA_EXPORT_FT WITH 'ADM'")

        FACTURACAO.menu_opcoes.adicionaOpcao("impResumo","Impress�o Resumos","","uf_resumos_chama")
        FACTURACAO.menu_opcoes.adicionaOpcao("regVacinacao","Registo Vacina��o","","uf_facturacao_OP_vacinacao")

        IF !(uf_gerais_validaPermUser(ch_userno, ch_grupo, 'Importa��o Hist�rico de Vendas - Fatura��o'))
            FACTURACAO.menu_opcoes.adicionaOpcao("impHistVendas","Imp. Hist. Vendas","","uf_facturacao_importHistVendas")
        ENDIF
		
		FACTURACAO.Menu1.gravar.img1.Picture = myPath + "\imagens\icons\guardar_w.png"
		
		FACTURACAO.refresh
	ELSE
		regua(1,5,"A carregar painel...")
		FACTURACAO.show
	ENDIF 

	*** Cria classe para permitir os eventos dinamicos **
	If Type("Facturacao.oCust") # "O"
		Facturacao.AddObject("oCust","pbordofact")
		Facturacao.KeyPreview = .T.
	ENDIF
	******************************************************
	
	regua(1,6,"A carregar painel...")
	**Calcula apenas se vier com stamp Preechido
	IF !EMPTY(ALLTRIM(tcStamp))
		IF USED("TD")
			fecha("TD")
		ENDIF
		
		SELECT * FROM uCrsGeralTD WHERE uCrsGeralTD.ndoc == ft.ndoc AND ALLTRIM(uCrsGeralTD.site) == ALLTRIM(mySite) INTO CURSOR TD READWRITE

		Select Ft
		myFtstamp = Ft.Ftstamp

		SELECT fi
		uf_Facturacao_controlaWkLinhas()
		
		**Controla Campos de Lote e Receitas Riscado
		SELECT Ft
		IF EMPTY(Ft.u_ltstamp)
			IF FT.U_LOTE != 0
				FACTURACAO.ContainerCab.lote.FontStrikethru 	= .t.
			ELSE
				FACTURACAO.ContainerCab.lote.FontStrikethru = .f.
			ENDIF
			IF FT.U_nslote != 0
				FACTURACAO.ContainerCab.nslote.FontStrikethru = .t.
			ELSE
				FACTURACAO.ContainerCab.nslote.FontStrikethru = .f.
			ENDIF
		ELSE
			FACTURACAO.ContainerCab.lote.FontStrikethru 	= .f.
			FACTURACAO.ContainerCab.nslote.FontStrikethru = .f.
		ENDIF
		
		SELECT Ft
		IF EMPTY(Ft.u_ltstamp2)
			IF FT.U_LOTE2 != 0
				FACTURACAO.ContainerCab.lote2.FontStrikethru 	= .t.
			ELSE
				FACTURACAO.ContainerCab.lote2.FontStrikethru 	= .f.
			ENDIF
			IF FT.U_nslote2 != 0
				FACTURACAO.ContainerCab.nslote2.FontStrikethru = .t.
			ELSE
				FACTURACAO.ContainerCab.nslote2.FontStrikethru = .f.			
			ENDIF
		ELSE
			FACTURACAO.ContainerCab.lote2.FontStrikethru 	= .f.
			FACTURACAO.ContainerCab.nslote2.FontStrikethru = .f.
		ENDIF
		

		** Definir Ordena��o **
		Select fi
		index on lordem tag lordem
		
		** Controla Check Mov Caixa
		IF !EMPTY(ucrsPagCentral.evdinheiro);
			OR !EMPTY(ucrsPagCentral.epaga1);
			OR !EMPTY(ucrsPagCentral.epaga2);
			OR !EMPTY(ucrsPagCentral.echtotal);
			OR !EMPTY(ucrsPagCentral.epaga3);
			OR !EMPTY(ucrsPagCentral.epaga4);
			OR !EMPTY(ucrsPagCentral.epaga5);
			OR !EMPTY(ucrsPagCentral.epaga6)

			facturacao.pageframe1.page2.movcaixa.Tag = "true"
			facturacao.pageframe1.page2.movcaixa.Picture = myPath + "\imagens\icons\checked_b_24.bmp"
		ELSE
			facturacao.pageframe1.page2.movcaixa.Tag = "false"
			facturacao.pageframe1.page2.movcaixa.Picture = myPath + "\imagens\icons\unchecked_b_24.bmp"
		ENDIF
		
		
		&& cursor com os dados psico
		*uf_gerais_actGrelha("","DADOSPSICO","select top 1 * from B_dadosPsico (nolock) where ftstamp='" + ALLTRIM(tcStamp) + "'")
	ENDIF
	
	**
	uf_facturacao_AlertaPIC()
	uf_facturacao_DemCor()
	
	regua(1,7,"A carregar painel...")
	IF myResizeFacturacao== .f.
		uf_facturacao_expandeGridLinhas()
	ENDIF 

	regua(1,8,"A carregar painel...")
	uf_Facturacao_controlaObjectos()
	regua(2)
ENDFUNC


**
FUNCTION uf_facturacao_moverLinhas
	LPARAMETERS lcBool
	
	uf_atendimento_moverLinhas(lcBool)
	
	facturacao.pageframe1.page1.griddoc.refresh
ENDFUNC


******************************************
** Op��o de chamar Registo de vacina��o **
******************************************
FUNCTION uf_facturacao_OP_vacinacao
	
	&& VERIFICA SE ESTAVA ALGUM REGISTO EM EDI��O / INSER��O
	IF !(TYPE("VACINACAO")=="U")
		IF myVacIntroducao OR myVacAlteracao
			uf_perguntalt_chama("O ECR� DE REGISTO DE VACINA��ES ENCONTRA-SE EM MODO DE ALTERA��O / INTRODU��O! TERMINE ESTA OPERA��O E TENTE NOVAMENTE.","OK","",64)
			vacinacao.show
		Else
			uf_Vacinacao_chama()
		Endif
	Else
		uf_Vacinacao_chama()	
	Endif
ENDFUNC


**
FUNCTION uf_Facturacao_CriaCursores
	LPARAMETERS lcStamp

	IF EMPTY(ALLTRIM(lcStamp))
		uf_gerais_actGrelha("","FT","set fmtonly on exec up_touch_ft '' set fmtonly off")
		uf_gerais_actGrelha("","FT2","set fmtonly on SELECT respostaDt = convert(varchar(254),''),* FROM FT2 (nolock) WHERE 1=0 set fmtonly off")
		uf_gerais_actGrelha("","ucrsPagCentral","Select top 0 * from B_pagCentral (nolock)")
		
		&&:

		lcSQL = ""
		TEXT TO lcSQL NOSHOW TEXTMERGE
			exec up_facturacao_linhas 1, 0, ''
		ENDTEXT 
		IF TYPE("FACTURACAO")=="U"
			uf_gerais_actGrelha("","FI",lcSQL)
		ELSE
			uf_gerais_actGrelha("FACTURACAO.Pageframe1.Page1.GridDoc","FI",lcSQL)
		ENDIF 
		uf_gerais_actGrelha("","DADOSPSICO","set fmtonly on select top 1 *,cabvendasordem=0 from B_dadosPsico(nolock) set fmtonly off")
		
		TEXT TO lcSQL NOSHOW TEXTMERGE 
		select 
			* 
		from 
			FI2 (nolock) 
		WHERE 
			0=1
		ENDTEXT
		uf_gerais_actGrelha("","FI2",lcSQL) 
		

	ELSE
		&& cria cursores com informa��o da Venda - alterado para n�o mostrar o c�digo de acesso da receita e pin direito opcao 20161028
		uf_gerais_actGrelha("","FT","exec up_touch_ft '"+ALLTRIM(lcStamp)+"'")
		
		uf_gerais_actGrelha("","FTQRCODE","exec up_print_qrcode_a4 '"+ALLTRIM(lcStamp)+"'")
		
		uf_gerais_actGrelha("","fi_nnordisk","select * from fi_nnordisk where fistamp in (select fistamp from fi where ftstamp='"+ALLTRIM(lcStamp)+"')")
		
		&& mostra caso seja admin LTS obras
		IF ch_userno = 1
			uf_gerais_actGrelha("","FT2","SELECT respostaDt = isnull((select top 1 DescrResposta from b_dt_resposta WHERE stampDoc = '"+ALLTRIM(lcStamp)+"' order by DataEnvio),''),* FROM FT2 (nolock) WHERE ft2stamp='"+ALLTRIM(lcStamp)+"'")
		ELSE
			&& n�o mostra para restantes utilizadores
			uf_gerais_actGrelha("","FT2","exec up_facturacao_ft2 '"+ALLTRIM(lcStamp)+"'")		
		ENDIF
		
		SELECT ft2
		IF !EMPTY(ft2.stamporigproc)
			uf_gerais_actGrelha("","curFTSeg","select ft.nome, ft.no, b_utentes.tlmvl , ft.telefone from ft inner join b_utentes on ft.no=b_utentes.no and ft.estab=b_utentes.estab where ftstamp = '"+ALLTRIM(ft2.stamporigproc)+"'")
		ENDIF 
		
		SELECT ft2 
		IF !empty(ALLTRIM(ft2.nrelegibilidade))
			TEXT TO lcSQL NOSHOW TEXTMERGE 
				SELECT( case when (select count(token) from ft_compart (nolock) where returnContributionCorrelationId='<<ALLTRIM(ft2.nrelegibilidade)>>' and type=2)>0 
					then 'Elegibilidade eletr�nica'
					else 'Elegibilidade manual'
				end ) as texto
			ENDTEXT 
		ELSE
			TEXT TO lcSQL NOSHOW TEXTMERGE 
				SELECT( case when (select count(token) from ft_compart (nolock) where returnContributionCorrelationId='<<ALLTRIM(ft2.nrelegibilidade)>>' and type=9999)>0 
					then 'Elegibilidade eletr�nica'
					else 'Elegibilidade manual'
				end ) as texto
			ENDTEXT 
		ENDIF 
		uf_gerais_actGrelha("","ucrsElegib",lcSQL)
	
		SELECT ft 
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			Select * from B_pagCentral (nolock) WHERE nrAtend = '<<ALLTRIM(Ft.u_nratend)>>'
		ENDTEXT 
		uf_gerais_actGrelha("","ucrsPagCentral",lcSQL)
		
		lcSQL = ""
		TEXT TO lcSQL NOSHOW TEXTMERGE
			exec up_facturacao_linhas 0, <<mysite_nr>>, '<<ALLTRIM(lcStamp)>>'
		ENDTEXT 
		
		IF TYPE("FACTURACAO")=="U"
			uf_gerais_actGrelha("","FI",lcSQL)
		ELSE
			uf_gerais_actGrelha("FACTURACAO.Pageframe1.Page1.GridDoc","FI",lcSQL)
		ENDIF 
		
		uf_gerais_actGrelha("","DADOSPSICO","select top 1 *,cabvendasordem=0 from B_dadosPsico(nolock) where ftstamp='"+ALLTRIM(lcStamp)+"'")
		
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			select 
				*			
			from 
				FI2 (nolock) 
			WHERE 
				FI2.ftstamp =  '<<ALLTRIM(lcStamp)>>'
		ENDTEXT 
		uf_gerais_actGrelha("","FI2",lcSQL)
	
		** cursores para impress�o
*!*			SELECT ft 
*!*			uf_gerais_actGrelha("","FTprint","exec up_print_ft '"+ALLTRIM(ft.ftstamp)+"', '"+ALLTRIM(ft.u_nratend)+"', ''")
*!*			uf_gerais_actGrelha("","FIprint","exec up_print_fi '"+ALLTRIM(ft.ftstamp)+"', '"+ALLTRIM(ft.u_nratend)+"', ''")
*!*			uf_gerais_actGrelha("","DocListprint","exec up_print_docs_atendimento '"+ALLTRIM(ft.ftstamp)+"', '"+ALLTRIM(ft.u_nratend)+"', ''")
*!*			uf_gerais_actGrelha("","DocPagprint","exec up_print_pagamentos '"+ALLTRIM(ft.u_nratend)+"' ")

	ENDIF 
	
	uf_gerais_actGrelha("","uCrsCmbCCusto","Select RTRIM(LTRIM(CCUSTO)) as CCUSTO FROM CCT (NOLOCK) WHERE INACTIVO = 0 ORDER BY RTRIM(LTRIM(CCUSTO))")
	
	&& cursor centro de custos
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		Select 
			'' descricao
		union all
		Select 
			descricao 
		from 
			cu (nolock) 
		order by 
			descricao	
	ENDTEXT
	
				

		
	

	
	If !uf_gerais_actGrelha("", "ucrsCCusto", lcSql)
		uf_perguntalt_chama("N�O FOI POSS�VEL VERIFICAR A LISTAGEM DE CENTROS DE CUSTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	ENDIF 
	*********************

	
	lcSQL = ""
	IF RIGHT(ALLTRIM(ch_pack),4) == "2010"
		TEXT TO lcSQL TEXTMERGE NOSHOW
			SELECT 'PTE ou EURO' as MOEDA UNION ALL SELECT MOEDA FROM CB (nolock)
		ENDTEXT
	ELSE
		TEXT TO lcSQL TEXTMERGE NOSHOW
			SELECT 'EURO' as MOEDA UNION ALL SELECT MOEDA FROM CB (nolock)
		ENDTEXT
	ENDIF
	
		
	If !uf_gerais_actGrelha("","ucrsMoedaDoc",lcSQL)
		uf_perguntalt_chama("N�O FOI POSS�VEL DEFINIR A MOEDA DO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	ENDIF 
	TEXT TO lcSQL TEXTMERGE NOSHOW
		SELECT 
			RTRIM(DESCRICAO) descricao
		FROM 
			TP (nolock) 
		WHERE 
			tipo = 1 
		ORDER BY 
			DESCRICAO
	ENDTEXT
	
	
	If !uf_gerais_actGrelha("","ucrsCondPag",lcSQL)
		uf_perguntalt_chama("N�O FOI POSS�VEL DEFINIR A MOEDA DO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	ENDIF	
	
	TEXT TO lcSQL TEXTMERGE NOSHOW
		SELECT 
			modofact
		From
			b_utentes (nolock)
			inner join ft (nolock) on ft.no = b_utentes.no and ft.estab = b_utentes.estab
		WHERE 
			ftstamp='<<ALLTRIM(lcStamp)>>' 
	ENDTEXT
	
	If !uf_gerais_actGrelha("","ucrsModoFact",lcSQL)
		uf_perguntalt_chama("N�O FOI POSS�VEL DEFINIR O MODO DE FATURACAO DO CLIENTE. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	ENDIF
	
	&& Cursor de Dados do Produto (semelhante ao atendimento)
	IF !(uf_gerais_actGrelha("","UCRSATENDST",[exec up_touch_ActRef 'xxxxxxxxxxx',0,0]))
		uf_perguntalt_chama("OCORREU UM ERRO A CRIAR O CURSOR [UCRSATENDST]-fact.","OK","",16)
		RETURN .F.
	ENDIF

	
	&& Historico de PIC
	SELECT ucrsE1
	IF ALLTRIM(UPPER(ucrsE1.TipoEmpresa)) == "FARMACIA" 
		IF !USED("ucrsHistPic")
			TEXT TO lcSQL TEXTMERGE NOSHOW
				exec up_facturacao_picData 
			ENDTEXT
			If !uf_gerais_actGrelha("","ucrsHistPic",lcSQL)
				uf_perguntalt_chama("N�O FOI POSS�VEL DEFINIR INFORMA��O DO PIC DO DOCUMENTO.","OK","",16)
				RETURN .f.
			ENDIF
		ENDIF 
	ENDIF 

	
	IF USED("ucrsDadosDemPresc")
		fecha("ucrsDadosDemPresc")
	ENDIF
	
	
		
	&& Dados Dem: M�dico e Utente
	lcSQL = ""
	SELECT ft2
	TEXT TO lcSQL NOSHOW TEXTMERGE
		exec up_dem_informacaoMedico '<<ALLTRIM(ft2.token)>>'
	ENDTEXT 
	If !uf_gerais_actGrelha("","ucrsDadosDemPresc",lcSQL)
		uf_perguntalt_chama("N�O FOI POSS�VEL ENCONTRAR DOCUMENTOS DE FACTURA��O.","OK","",16)
		RETURN .f.
	ENDIF 
	
		&& Cursor fi_trans_info
	IF !USED("fi_trans_info")
		lcSQL  = ''
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			set fmtonly on 
			select 
				*
				,CAST('' as varchar(30)) as verify_i
				,CAST('' as varchar(100)) as verify_s
				, CAST('' as varchar(100)) as tcode 
				, CAST('' as varchar(200)) as verifymsg 
				, CAST('' as varchar(100)) as verif_reason
				,CAST('' as varchar(30)) as dispense_i
				, CAST('' as varchar(100)) as dispense_s 
				, CAST('' as varchar(200)) as dispensemsg 
				, CAST('' as varchar(100)) as disp_reason 
				,CAST('' as varchar(30)) as undo_i
				, CAST('' as varchar(100)) as undo_s 
				, CAST('' as varchar(200)) as undodispensemsg 
				, CAST('' as varchar(100)) as undo_reason 
				, CAST('' as varchar(10)) as tipo
				, CAST(0 as bit) as conferido
				, CAST(0 as bit) as deleted
			from 
				fi_trans_info (nolock) 
			set fmtonly off
		ENDTEXT 
	
		IF !uf_gerais_actGrelha("","fi_trans_info",lcSQL)
		**IF !uf_gerais_actGrelha("",[fi_trans_info],[set fmtonly on select *,CAST('' as varchar(30)) as verify_i,CAST('' as varchar(100)) as verify_s, CAST('' as varchar(100)) as tcode ,CAST('' as varchar(30)) as dispense_i, CAST('' as varchar(100)) as dispense_s from fi_trans_info (nolock) set fmtonly off])
			uf_perguntalt_chama("Ocorreu uma anomalia ao criar o cursor [fi_trans_info]. Por favor reinicie o Software.","OK","",16)
			RETURN .F.
		ENDIF	
	ELSE
		DELETE FROM fi_trans_info
	ENDIF
	
		&& Cursor fi_trans_info
	IF !USED("mixed_bulk_pend")
		lcSQL  = ''
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			set fmtonly on 
			select 
				*
			from 
				mixed_bulk_pend (nolock) 
			set fmtonly off
		ENDTEXT 
	
		IF !uf_gerais_actGrelha("","mixed_bulk_pend",lcSQL)
		**IF !uf_gerais_actGrelha("",[fi_trans_info],[set fmtonly on select *,CAST('' as varchar(30)) as verify_i,CAST('' as varchar(100)) as verify_s, CAST('' as varchar(100)) as tcode ,CAST('' as varchar(30)) as dispense_i, CAST('' as varchar(100)) as dispense_s from fi_trans_info (nolock) set fmtonly off])
			uf_perguntalt_chama("Ocorreu uma anomalia ao criar o cursor [mixed_bulk_pend]. Por favor reinicie o Software.","OK","",16)
			RETURN .F.
		ENDIF	
	ELSE
		DELETE FROM mixed_bulk_pend
	ENDIF	
	

		
	&& Cursor nrserie_com_results
	IF !USED("nrserie_com_results")
		IF !uf_gerais_actGrelha("",[nrserie_com_results],[set fmtonly on select * from nrserie_com_results (nolock) set fmtonly off])
			uf_perguntalt_chama("Ocorreu uma anomalia ao criar o cursor [nrserie_com_results]. Por favor reinicie o Software.","OK","",16)
			RETURN .F.
		ENDIF	
	ELSE
		DELETE FROM nrserie_com_results
	ENDIF
	
	
	TEXT TO lcSQL TEXTMERGE NOSHOW 
		select  codigo from  cptpla(nolock) WHERE combinado  = 1
	ENDTEXT 
	IF !uf_gerais_actGrelha("", "uCrsPlanosCombinados", lcSQL)
		uf_perguntalt_chama("Ocorreu uma anomalia ao criar o cursor [uCrsPlanosCombinados]. Por favor reinicie o Software.","OK","",16)
	ENDIF
	


	

ENDFUNC


** Carrega Documento com permiss�es na Combobox
** Verifica Permiss�es <NomeDocumento - Introduzir>s
FUNCTION uf_Facturacao_carregaDocumentos
	LPARAMETERS tcBool
	
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_perfis_validaAcessoDocs_utilizados '<<ch_userno>>', '<<ALLTRIM(ch_grupo)>>', 'Introduzir', 'FT', 0, '<<ALLTRIM(mysite)>>'
	ENDTEXT
	If !uf_gerais_actGrelha("","ucrsComboDocFac",lcSQL)
		uf_perguntalt_chama("N�O FOI POSS�VEL ENCONTRAR DOCUMENTOS DE FACTURA��O.","OK","",16)
		RETURN .f.
	ENDIF 
	

	

ENDFUNC


**
FUNCTION uf_Facturacao_controlaObjectos

	**Controla Caption
	DO CASE 
		CASE myFtIntroducao == .t.
			FACTURACAO.caption = "Fatura��o - INTRODU��O"
		CASE myFtAlteracao == .t.
			FACTURACAO.caption = "Fatura��o - ALTERA��O"
		OTHERWISE
			FACTURACAO.caption = "Fatura��o"
	ENDCASE
	
	** regras gerais para quando n�o existe nenhum documento selecionado
	Select Ft
	If Empty(Ft.nmdoc)
		** coloca inactivo campo de cliente ate escolher documento
		FACTURACAO.ContainerCab.no.readonly		= .t.
		FACTURACAO.ContainerCab.nome.readonly	= .t.

		** bot�es de grelha
		FACTURACAO.PageFrame1.Page1.ContainerLinhas.btnAddLinha.enable(.f.)
		FACTURACAO.PageFrame1.Page1.ContainerLinhas.btnDelLinha.enable(.f.)
		FACTURACAO.PageFrame1.Page1.ContainerLinhas.btnDescCom.enable(.f.)
		FACTURACAO.menu1.estado("impDoc, dem","HIDE")
		FACTURACAO.menu_opcoes.estado("exportFTEntidadeADSE, exportFTEntidadeADM, fichaUtente","HIDE")
	Else
		** Nome
		FACTURACAO.ContainerCab.nome.readOnly 		= .f.
		
		** Data da receita
*!*			IF USED("td")
*!*				SELECT td
*!*				IF RECCOUNT("TD")>0
			FACTURACAO.containerCab.datareceita.visible = .t.
			FACTURACAO.containerCab.ldatareceita.visible = .t.
*!*				ENDIF
*!*			ENDIF
		
		IF ALLTRIM(UPPER(FACTURACAO.ContainerCab.nome.value)) == "ADSE" && Mostra mesmo sem o documento estar garvar porque � necess�rio submeter o fichero antes da grava��o
			FACTURACAO.menu_opcoes.estado("exportFTEntidadeADSE","SHOW")
		ELSE
			FACTURACAO.menu_opcoes.estado("exportFTEntidadeADSE","HIDE")
		ENDIF
		
		IF used("fi_nnordisk")
			IF RECCOUNT("fi_nnordisk")>0 then
				FACTURACAO.menu_opcoes.estado("talaonnordisk","SHOW")
			ELSE
				FACTURACAO.menu_opcoes.estado("talaonnordisk","HIDE")
			ENDIF
		ELSE
			FACTURACAO.menu_opcoes.estado("talaonnordisk","HIDE")
		ENDIF 
		
		IF LIKE('ADM*', ALLTRIM(UPPER(FACTURACAO.ContainerCab.nome.value))) && Mostra mesmo sem o documento estar garvar porque � necess�rio submeter o fichero antes da grava��o
			FACTURACAO.menu_opcoes.estado("exportFTEntidadeADM","SHOW")
		ELSE
			FACTURACAO.menu_opcoes.estado("exportFTEntidadeADM","HIDE")
		ENDIF
	Endif
	
	***********************************
	** Modo de inser��o ou altera��o **
	***********************************
	IF myFtIntroducao == .t. Or myFtAlteracao == .t.
		
		** Outros Dados
		FACTURACAO.pageframe1.page2.txtu_nbenef.readonly = .f.
		FACTURACAO.pageframe1.page2.txtu_nbenef2.readonly = .f.
		FACTURACAO.pageframe1.page2.txtNCartaoCompart.readonly = .f.
		FACTURACAO.pageframe1.page2.txtCodEmb.readonly = .f.
		FACTURACAO.pageframe1.page2.cmbCCusto.readonly = .f.
		
		** Permitir escolher tipo documento
		FACTURACAO.containerCab.nmdoc.enabled 			= .t.
		
		** DATAS 
		FACTURACAO.ContainerCab.DOCDATA.enabled 		= .t.
		
		** N� Receita
		FACTURACAO.ContainerCab.receita.readonly 		= .f.
		
		** Set focus na Combo de documento
		FACTURACAO.ContainerCab.nmdoc.setfocus
					
		** bot�es de grelha
		FACTURACAO.PageFrame1.Page1.ContainerLinhas.btnAddLinha.enable(.t.)
		FACTURACAO.PageFrame1.Page1.ContainerLinhas.btnDelLinha.enable(.t.)
		FACTURACAO.PageFrame1.Page1.ContainerLinhas.btnDescCom.enable(.t.)
		FACTURACAO.menu1.estado("impDoc, dem","SHOW")
        FACTURACAO.menu_opcoes.estado("fichaUtente","SHOW")
	
		** Combobox
		FACTURACAO.ContainerCab.tpdesc.enabled	= .t.
		FACTURACAO.ContainerCab.moeda.enabled	= .t.
		
		** Desconto Finaceiro
		FACTURACAO.PageFrame1.Page1.ContainerTotais.DescFin.ReadOnly 		= .f.
		FACTURACAO.PageFrame1.Page1.ContainerTotais.ValorDescFin.ReadOnly	= .f.

		*Documento Original
		IF ALLTRIM(UPPER(FACTURACAO.ContainerCab.nmdoc.value))=="VENDA MANUAL"
			FACTURACAO.PageFrame1.Page1.ContainerTotais.docorig.ReadOnly 	= .f.
		ELSE
			FACTURACAO.PageFrame1.Page1.ContainerTotais.docorig.ReadOnly 	= .t.
		ENDIF
		
		*Bot�o de Suspender Venda
		IF USED("TD")
			SELECT TD
			IF TD.u_tipodoc == 9 OR TD.u_tipodoc == 8 OR TD.u_tipodoc == 3
				FACTURACAO.ContainerCab.chkSuspensa.enable(.t.)
				FACTURACAO.ContainerCab.chkSuspensa.visible = .t.
			ELSE
				FACTURACAO.ContainerCab.chkSuspensa.visible = .f.
			ENDIF
		ENDIF
		
		*Venda ao domic�lio
		facturacao.pageframe1.page3.chkVdDom.enable(.t.)
		
		*Observa��es
		facturacao.pageframe1.page4.EditObs.readonly = .f.
		
		*Excep��o
		facturacao.pageframe1.page2.txtExcecaoLinha.readonly= .t.
		facturacao.pageframe1.page2.txtExcecaoLinha.enabled= .t.
		
		facturacao.pageframe1.page2.btnExcecao.enable(.t.)

		
		*Psico
		facturacao.pageframe1.page5.txtReceita.readonly= .f.
		facturacao.pageframe1.page5.txtMedico.readonly= .f.
		facturacao.pageframe1.page5.txtNomeD.readonly= .f.
		facturacao.pageframe1.page5.txtMoradaD.readonly= .f.
		facturacao.pageframe1.page5.txtCodPostD.readonly= .f.
		facturacao.pageframe1.page5.txtNomeA.readonly= .f.
		facturacao.pageframe1.page5.txtMoradaA.readonly= .f.
		facturacao.pageframe1.page5.txtCodPostA.readonly= .f.
		facturacao.pageframe1.page5.txtBI.readonly= .f.
		facturacao.pageframe1.page5.txtData.readonly= .f.
		facturacao.pageframe1.page5.txtIdade.readonly= .f.
		facturacao.pageframe1.page5.btnImportDadosCl.enabled = .t.

		*Barra Lateral de op��es 
		FACTURACAO.menu1.estado("pesquisar,novo,editar,anterior,seguinte,enviarncSNS","HIDE","GRAVAR",.t.,"CANCELAR",.t.) &&opcoes
		FACTURACAO.menu1.estado("impDoc, dem,novaLinha, eliminarLinha, pesquisarStock","SHOW") && ,subir,descer
		FACTURACAO.menu_opcoes.estado("cliente, fichaUtente", "SHOW")
		FACTURACAO.menu_opcoes.estado("imprimir, pos, dadosReceita, corrigirReceita, ultimo, eliminar,alterameiospagamento, enviaDT" , "HIDE")
		
	ENDIF 
	
	***********************
	** Modo de Altera��o **
	***********************
	IF myFtAlteracao == .t.
		FACTURACAO.ContainerCab.nmdoc.enabled	= .f.
	ENDIF
	***********************
	
	******************************************************
	** Modo de Visualiza��o N�o Introdu��o e N�o Edi��o **
	******************************************************
	If myFtIntroducao == .f. And myFtAlteracao == .f.
		
		** Outros Dados
		FACTURACAO.pageframe1.page2.txtu_nbenef.readonly = .t.
		FACTURACAO.pageframe1.page2.txtu_nbenef2.readonly = .t.
		FACTURACAO.pageframe1.page2.txtNCartaoCompart.readonly = .t.
		FACTURACAO.pageframe1.page2.txtCodEmb.readonly = .t.
		FACTURACAO.pageframe1.page2.cmbCCusto.readonly = .t.
		
		** Nome
		FACTURACAO.ContainerCab.nome.readOnly 		= .t.
		
		** permitir escolher tipo documento
		FACTURACAO.ContainerCab.nmdoc.enabled 		= .f.
*		FACTURACAO.ContainerCab.nmdoc.readOnly		= .f.
				
		** DATAS 
		FACTURACAO.ContainerCab.DOCDATA.enabled = .f.
		
		** N� Receita
		FACTURACAO.ContainerCab.receita.readonly = .t.
			
		** bot�es de grelha
		FACTURACAO.PageFrame1.Page1.ContainerLinhas.btnAddLinha.enable(.f.)
		FACTURACAO.PageFrame1.Page1.ContainerLinhas.btnDelLinha.enable(.f.)
		FACTURACAO.PageFrame1.Page1.ContainerLinhas.btnDescCom.enable(.f.)
		
		** Combobox
		FACTURACAO.ContainerCab.tpdesc.enabled		= .f.
		FACTURACAO.ContainerCab.moeda.enabled		= .f.
		
		** Desconto Finaceiro
		FACTURACAO.PageFrame1.Page1.ContainerTotais.DescFin.ReadOnly = .t.
		FACTURACAO.PageFrame1.Page1.ContainerTotais.ValorDescFin.ReadOnly = .t.

		*Documento Original
		IF ALLTRIM(UPPER(FACTURACAO.ContainerCab.nmdoc.value))=="VENDA MANUAL"
			FACTURACAO.PageFrame1.Page1.ContainerTotais.docorig.ReadOnly = .t.
		ELSE
			FACTURACAO.PageFrame1.Page1.ContainerTotais.docorig.ReadOnly = .t.
		ENDIF
		
		*Bot�o de Suspender Venda
		IF USED("TD")
			SELECT TD
			IF TD.u_tipodoc == 9 OR TD.u_tipodoc == 8 OR TD.u_tipodoc == 3
				FACTURACAO.ContainerCab.chkSuspensa.enable(.f.)
				FACTURACAO.ContainerCab.chkSuspensa.visible = .t.
			ELSE
				FACTURACAO.ContainerCab.chkSuspensa.visible = .f.
			ENDIF
		ENDIF
		
		*Venda ao domic�lio
		facturacao.pageframe1.page3.chkVdDom.enable(.f.)
		
		*Observa��es
		facturacao.pageframe1.page4.EditObs.readonly = .t.
		
		*Excep��o
		facturacao.pageframe1.page2.txtExcecaoLinha.readonly= .t.
		facturacao.pageframe1.page2.txtExcecaoLinha.enabled= .f.
		
		facturacao.pageframe1.page2.btnExcecao.enable(.f.)
		
		*Psico
		facturacao.pageframe1.page5.txtReceita.readonly= .t.
		facturacao.pageframe1.page5.txtMedico.readonly= .t.
		facturacao.pageframe1.page5.txtNomeD.readonly= .t.
		facturacao.pageframe1.page5.txtMoradaD.readonly= .t.
		facturacao.pageframe1.page5.txtCodPostD.readonly= .t.
		facturacao.pageframe1.page5.txtNomeA.readonly= .t.
		facturacao.pageframe1.page5.txtMoradaA.readonly= .t.
		facturacao.pageframe1.page5.txtCodPostA.readonly= .t.
		facturacao.pageframe1.page5.txtBI.readonly= .t.
		facturacao.pageframe1.page5.txtData.readonly= .t.
		facturacao.pageframe1.page5.txtIdade.readonly= .t.
		facturacao.pageframe1.page5.btnImportDadosCl.enabled = .f.
		
		
		*Menu
		FACTURACAO.menu1.estado("opcoes,pesquisar,novo,editar,anterior,seguinte","SHOW","GRAVAR",.f.,"SAIR",.t.)
		FACTURACAO.menu1.estado("impDoc,dem, novaLinha, eliminarLinha, pesquisarStock","HIDE") 
		FACTURACAO.menu_opcoes.estado("cliente, enviaDT, fichaUtente", "HIDE")
		FACTURACAO.menu_opcoes.estado("imprimir, pos, dadosReceita, corrigirReceita, ultimo, eliminar, alterameiospagamento" , "SHOW")
		
		IF uf_Facturacao_deveEnviarSNS()
			FACTURACAO.menu1.estado("enviarncSNS" , "SHOW")
		else
			FACTURACAO.menu1.estado("enviarncSNS" , "HIDE")
		ENDIF 


	ENDIF 
	
	
	uf_excecaoMed_preencheCamposLivres()
	
	IF USED("ucrsWkLinhas")
		** Controla Edi��o da grid
		WITH FACTURACAO.PageFrame1.Page1.griddoc
			IF myFtIntroducao == .t. Or myFtAlteracao == .t.
				.Readonly 	= .f.
				SELECT ucrsWkLinhas
				GO TOP
				SCAN
					FOR i=1 TO .columnCount
						If Upper(Alltrim(ucrsWkLinhas.wkfield)) == Upper(Alltrim(.Columns(i).ControlSource))		
							If ucrsWkLinhas.WKREADONLY == .t. && Campos Readonly
								.Columns(i).Readonly = .t.
							ENDIF
							
							If Upper(Alltrim(ucrsWkLinhas.wkfield)) == "FI.TABIVA"
								.Columns(i).enabled = .t.
							ENDIF
						ENDIF
					ENDFOR
				ENDSCAN
			ELSE
				.Readonly 	= .t.
			ENDIF
		ENDWITH
	ENDIF
	
	IF UPPER(myPaisConfSoftw) == 'ANGOLA'
		FACTURACAO.ContainerCab.lcondpag.visible=.f.
		FACTURACAO.ContainerCab.lote.visible=.f.
		FACTURACAO.ContainerCab.label10.visible=.f.
		FACTURACAO.ContainerCab.nslote.visible=.f.
		FACTURACAO.ContainerCab.label5.visible=.f.
		FACTURACAO.ContainerCab.lote2.visible=.f.
		FACTURACAO.ContainerCab.label12.visible=.f.
		FACTURACAO.ContainerCab.nslote2.visible=.f.
		FACTURACAO.ContainerCab.label11.visible=.f.
	ENDIF 
		
	FACTURACAO.refresh
ENDFUNC



FUNCTION uf_facturacao_validaEntidadeNotaCreditoDebitooEletronica()
	LPARAMETERS lcNo
	
	local lcTemFacturacaoEletronica
	lcTemFacturacaoEletronica = .f.
	
	
	IF USED("uCrsTempEntidadeFactEl")
		fecha("uCrsTempEntidadeFactEl")
	ENDIF

	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		select 'electronica' = count(*) from cptorg(nolock) where e_ncnd = 1 and u_no = <<lcNo>>
	ENDTEXT
	If !uf_gerais_actGrelha("","uCrsTempEntidadeFactEl",lcSQL)
		uf_perguntalt_chama("N�O FOI POSS�VEL INFORMA��O SOBRE A ENTIDADE","OK","",16)
		RETURN .f.
	ENDIF 
	
	IF(uCrsTempEntidadeFactEl.electronica>0)
		lcTemFacturacaoEletronica = .t.
	ENDIF
	
		
	IF USED("uCrsTempEntidadeFactEl")
		fecha("uCrsTempEntidadeFactEl")
	ENDIF
	
	RETURN lcTemFacturacaoEletronica 

ENDFUNC



** valida se � nota de nota de credito ou debito

FUNCTION uf_Facturacao_deveEnviarSNS  
	SELECT FT
	IF( (LEFT(UPPER(ALLTRIM(ft.nmdoc)),15)='NOTA DE CR�DITO' OR LEFT(UPPER(ALLTRIM(ft.nmdoc)),12)='NOTA CR�DITO';
	  OR LEFT(UPPER(ALLTRIM(ft.nmdoc)),14)='NOTA DE D�BITO';
	  OR LEFT(UPPER(ALLTRIM(ft.nmdoc)),11)='NOTA D�BITO';
 	  OR LEFT(UPPER(ALLTRIM(ft.nmdoc)),13)='N.CR�DITO SNS';
   	  OR LEFT(UPPER(ALLTRIM(ft.nmdoc)),12)='N.D�BITO SNS';
	  OR LEFT(UPPER(ALLTRIM(ft.nmdoc)),14)='NOTA DE D�BITO') AND uf_facturacao_validaEntidadeNotaCreditoDebitooEletronica(Ft.no))
		RETURN .t.
	ELSE
		RETURN .f.	
	ENDIF

ENDFUNC


**
FUNCTION uf_Facturacao_ultimoRegisto
	LOCAL lcNAt
	lcNAt = ALLTRIM(uf_gerais_devolveUltRegisto(ALLTRIM(STR(myTermNo))))
	uf_gerais_actGrelha("","uCrsTempStamp","select top 1 ftstamp from ft (nolock) where site = '<<ALLTRIM(mysite)>>' and u_nratend='"+lcNAt + IIF(myTermNo<10,'0'+alltrim(STR(myTermNo)),alltrim(STR(myTermNo)))+"'")
	SELECT uCrsTempStamp
	uf_facturacao_chama(alltrim(uCrsTempStamp.ftstamp))
	
	IF USED("uCrsTempStamp")
		fecha("uCrsTempStamp")
	ENDIF 
	
ENDFUNC


**
FUNCTION uf_Facturacao_novo	
	LPARAMETERS lcNaoPedeTipoDoc
	LOCAL lcSQL 
	STORE '' TO lcSQL
	
	** Verificar se existe nova vers�o para reiniciar o software
	uf_chk_nova_versao()	
	
	myFtIntroducao = .t.
	myFtAlteracao = .f.
	
	release myInsereReceitaCorrigida
	
	IF Used("Ft")
		Select Ft
		DELETE ALL
	ENDIF 
	Select FT
	Append Blank 
	
	&& Controlo Moeda
	LOCAL lcMoedaDefault
	STORE 'EURO' TO lcMoedaDefault
	
	lcMoedaDefault = uf_gerais_getParameter("ADM0000000260","text")
	
	&& Se parametro n�o estiver preenchido assume � por default
	IF EMPTY(lcMoedaDefault)
		lcMoedaDefault = 'EURO'
	ENDIF
	
	Select FT
	Replace Ft.moeda WITH lcMoedaDefault
	
	If Used("Ft2")
		Select Ft2
		DELETE ALL
	ENDIF
	Select FT2
	Append Blank
	
	If Used("ucrsPagCentral")
		Select ucrsPagCentral
		DELETE ALL
	ENDIF
	Select ucrsPagCentral
	Append Blank
	
	**Elimina as Linhas
	If Used("FI")
		Select FI
		DELETE ALL
	ENDIF
	
		**Elimina as Linhas
	If Used("FI2")
		Select FI2
		DELETE ALL
	ENDIF
	
	**Elimina dadosPsico
	IF USED("dadosPsico")
		SELECT dadosPsico
		DELETE ALL
	ENDIF 
	
	uf_Facturacao_controlaObjectos()

	uf_facturacao_DemCor()

	facturacao.refresh
	
	**
	SELECT ucrsE1
	IF UPPER(ALLTRIM(ucrsE1.tipoEmpresa)) == "CLINICA"
		facturacao.pageframe1.page2.movcaixa.Tag = "true"
		facturacao.pageframe1.page2.movcaixa.Picture = myPath + "\imagens\icons\checked_b_24.bmp"
	ELSE
		facturacao.pageframe1.page2.movcaixa.Tag = "false"
		facturacao.pageframe1.page2.movcaixa.Picture = myPath + "\imagens\icons\unchecked_b_24.bmp"
	ENDIF		
	
	IF EMPTY(lcNaoPedeTipoDoc)
		Facturacao.ContainerCab.nmdoc.click
	ENDIF 
	
ENDFUNC


FUNCTION uf_Facturacao_novo_escolhe
	LPARAMETERS lnDocumento
	LOCAL lcSQL , lcDocumento
	STORE '' TO lcSQL
	lcDocumento = ALLTRIM(lnDocumento)
	
	** Verificar se existe nova vers�o para reiniciar o software
	uf_chk_nova_versao()	
	
	myFtIntroducao = .t.
	myFtAlteracao = .f.
	release myInsereReceitaCorrigida
	
	IF Used("Ft")
		Select Ft
		DELETE ALL
	ENDIF 
	Select FT
	Append Blank 
	
	&& Controlo Moeda
	LOCAL lcMoedaDefault
	STORE 'EURO' TO lcMoedaDefault
	
	lcMoedaDefault = uf_gerais_getParameter("ADM0000000260","text")
	
	&& Se parametro n�o estiver preenchido assume � por default
	IF EMPTY(lcMoedaDefault)
		lcMoedaDefault = 'EURO'
	ENDIF
	
	Select FT
	Replace Ft.moeda WITH lcMoedaDefault
	
	If Used("Ft2")
		Select Ft2
		DELETE ALL
	ENDIF
	Select FT2
	Append Blank
	
	If Used("ucrsPagCentral")
		Select ucrsPagCentral
		DELETE ALL
	ENDIF
	Select ucrsPagCentral
	Append Blank
	
	**Elimina as Linhas
	If Used("FI")
		Select FI
		DELETE ALL
	ENDIF
	
	
	**Elimina as Linhas
	If Used("FI2")
		Select FI2
		DELETE ALL
	ENDIF
	
	**Elimina dadosPsico
	IF USED("dadosPsico")
		SELECT dadosPsico
		DELETE ALL
	ENDIF 
	
	uf_Facturacao_controlaObjectos()

	uf_facturacao_DemCor()
	
	**
	SELECT ucrsE1
	IF UPPER(ALLTRIM(ucrsE1.tipoEmpresa)) == "CLINICA"
		facturacao.pageframe1.page2.movcaixa.Tag = "true"
		facturacao.pageframe1.page2.movcaixa.Picture = myPath + "\imagens\icons\checked_b_24.bmp"
	ELSE
		facturacao.pageframe1.page2.movcaixa.Tag = "false"
		facturacao.pageframe1.page2.movcaixa.Picture = myPath + "\imagens\icons\unchecked_b_24.bmp"
	ENDIF		
	SELECT ft 	
	replace ft.nmdoc WITH ALLTRIM(lcDocumento)
	facturacao.refresh
	uf_facturacao_valoresDefeitoFact()
	uf_facturacao_controlaNumeroFact(year(datetime()+(difhoraria*3600)))
	uf_atendimento_fiiliq() &&actualiza totais das linhas
	uf_atendimento_actTotaisFt() &&actualiza totais do cabe�alho
	uf_Facturacao_controlaWkLinhas()
	uf_Facturacao_controlaObjectos()
	uf_facturacao_criaLinhaDefault()
	
	facturacao.refresh
	
ENDFUNC


**
FUNCTION uf_facturacao_alterarDoc

	** Verificar se existe nova vers�o para reiniciar o software
	uf_chk_nova_versao()
	
	IF !USED("TD")
		uf_perguntalt_chama("O CURSOR TD N�O EST� CARREGADO.","OK","",48)
		RETURN .F.
	ELSE	
		SELECT TD
		IF td.u_integra == .T.
			uf_perguntalt_chama("ESTA S�RIE DE DOCUMENTO EST� CONFIGURADA COMO SENDO DE INTEGRA��O. N�O � POSS�VEL ALTERAR DOCUMENTOS DESTE TIPO.","OK","",48)
			RETURN .F.
		ENDIF
	ENDIF
	
	&& Verificar data fechada para altera��es
	IF uf_gerais_getParameter_site('ADM0000000006','BOOL')
		IF !EMPTY(ALLTRIM(uf_gerais_getParameter_site('ADM0000000007', 'text'))) AND DTOC(ft.fdata,1)<uf_gerais_getParameter_site('ADM0000000007', 'text') 
			uf_perguntalt_chama("N�o pode alterar/eliminar um documento com data inferior � data fechada para altera��es/elimina��es.","OK","",64)
			RETURN .f.
		ENDIF
	ENDIF 

	Select Ft
	IF !EMPTY(Ft.Nmdoc)

		** Valida Perfil de Altera��o <nomeDoc - Alterar>
		SELECT ft
		IF !(uf_gerais_validaPermUser(ch_userno, ch_grupo, ALLTRIM(ft.nmdoc) + ' - Alterar'))
			uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ALTERAR ESTE TIPO DE DOCUMENTO.","OK","",48)
			Return .f.
		ENDIF
		**
		
		&&verifica se o doc j� foi exportado
		select Ft
		IF !EMPTY(Ft.exportado)
			IF uf_gerais_validaPermUser(ch_userno, ch_grupo, 'Altera��o registos exportados') == .t.
				uf_perguntalt_chama("O seu perfil n�o permite editar registos exportados para a contabilidade.","OK","",32)
				Return .f.
			ENDIF
		
			IF uf_perguntalt_chama("Documento j� exportado para a Contabilidade. Pretende anular a exporta��o?","Sim","N�o",48)
				
				TEXT TO lcSQL NOSHOW TEXTMERGE
					UPDATE Ft SET exportado = 0 WHERE ftstamp = '<<ALLTRIM(ft.ftstamp)>>'
				ENDTEXT 
				
				If !uf_gerais_actGrelha("", "", lcSQL)
					uf_perguntalt_chama("N�o foi possivel anular a exporta��o do documento","OK","",16)
					RETURN .f.
				ELSE

					SELECT Ft 
					Replace Ft.exportado WITH .f.
				
					uf_perguntalt_chama("Exporta��o anulada com sucesso.","OK","",64)
				Endif
				
			ELSE 
				RETURN .f.			
			ENDIF 
		ENDIF 
		
		nrAtendimento = ft.u_nratend
		
		myFtIntroducao = .f.
		myFtAlteracao = .t.
		
		**
		uf_Facturacao_controlaWkLinhas()
		
		
		*************************************************************************************************************************************
		** Regras de certifica��o: 
		** Desactiva todos os eventos da grelha para n�o permitir a edi��o de valores - excepto se o documento for uma Inser��o de receita **
		*************************************************************************************************************************************
		SELECT TD
*		IF !(td.u_tipodoc == 3)
		IF LEN(ALLTRIM(td.tiposaft))>1
			&& Desactiva todos os eventos e coloca as colunas como readonly
			FOR EACH lcColuna IN FACTURACAO.PageFrame1.Page1.griddoc.columns
				FOR EACH lcObj in lcColuna.CONTROLS
				    UNBINDEVENTS(lcObj)
					TRY 
						lcColuna.enabled = .F.
					CATCH
						****
					ENDTRY
				ENDFOR
			ENDFOR
			
			&& Desactiva bot�es
			FACTURACAO.PageFrame1.Page1.ContainerLinhas.btnAddLinha.enable(.f.)
			FACTURACAO.PageFrame1.Page1.ContainerLinhas.btnDelLinha.enable(.f.)
			FACTURACAO.PageFrame1.Page1.ContainerLinhas.btnDescCom.enable(.f.)
			FACTURACAO.menu1.estado("impDoc, dem","HIDE")
            FACTURACAO.menu_opcoes.estado("fichaUtente","HIDE")
		ENDIF
		
		** for�ar eventos de active das pages no pageframe
		facturacao.pageframe1.activepage = facturacao.pageframe1.activepage
		
		** criar cursor para valida��es de grava��o
		fecha("uCrsFtOrig")
		SELECT ft.fno, ft.etotal, ft.fdata, ft.totqtt, edescc, ft2.motiseimp, ft.ncont, ft.nome, ft.no, fi.epv, fi.etiliquido, fi.iva,  fi.tabiva, fi.desconto, fi.qtt, fi.fistamp from ft inner join fi on fi.ftstamp=ft.ftstamp inner join ft2 on ft2.ft2stamp=ft.ftstamp into cursor uCrsFtOrig readwrite
		
	ENDIF
	
	myResizeFacturacao = .f.
	
	uf_facturacao_expandeGridLinhas()
	
	uf_Facturacao_controlaObjectos()
ENDFUNC


**
FUNCTION uf_facturacao_eliminarDoc
	Local lcValida, lcSQL
	
	** Apenas eliminar as inser��es de receita 
	IF !USED("TD")
		uf_perguntalt_Chama("N�o foi encontrada informa��o relativa ao tipo do documento." + CHR(13) + CHR(13) + "Por favor atualize o painel de fatura��o e tente novamente.","OK","",64)
		RETURN .f.
	ENDIF

	&& Verificar data fechada para altera��es
	IF uf_gerais_getParameter_site('ADM0000000006','BOOL')
		IF !EMPTY(ALLTRIM(uf_gerais_getParameter_site('ADM0000000007', 'text'))) AND DTOC(ft.fdata,1)<uf_gerais_getParameter_site('ADM0000000007', 'text') 
			uf_perguntalt_chama("N�o pode alterar/eliminar um documento com data inferior � data fechada para altera��es/elimina��es.","OK","",64)
			RETURN .f.
		ENDIF 
	ENDIF 
	
	SELECT TD
	*IF !(td.u_tipodoc == 3)
	IF !empty(alltrim(td.tiposaft))	
		uf_perguntalt_chama("N�O PODE APAGAR UM DOCUMENTO QUE FOI ASSINADO DIGITALMENTE DE ACORDO COM AS REGRAS DE CERTIFICA��O DE SOFTWARE.","OK","",64)
		RETURN .f.
	ELSE
		Select ft
		If !Empty(Ft.Nmdoc)
			** Valida Perfil de Elimina��o <nomeDoc - Eliminar>
			IF !(uf_gerais_validaPermUser(ch_userno, ch_grupo, ALLTRIM(ft.nmdoc) + ' - Eliminar'))
				uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ELIMINAR ESTE TIPO DE DOCUMENTO.","OK","",48)
				RETURN .f.
			ENDIF
			***********************
			
			* Tem permiss�es para eliminar
			If uf_perguntalt_chama('Quer mesmo Eliminar o documento?'+CHR(13)+CHR(13)+'N�o ser� possivel recuperar os Dados.',"Sim","N�o")

				** Se o documento tiver receita associada, elimina receita **
				lcSql=""
				If !Empty(ft.u_ltstamp)
					** Anular Receita DEM
					SELECT Ft2
					IF !EMPTY(Ft2.token) AND EMPTY(ft2.u_docorig)
						uf_atendimento_receitasAnular(Ft2.u_receita,Ft2.token)
					ENDIF 

					SELECT FT
					TEXT TO lcSql NOSHOW TEXTMERGE
						DELETE FROM ctltrct WHERE ctltrctstamp='<<Alltrim(Ft.u_ltstamp)>>'
					ENDTEXT
					If !uf_gerais_actGrelha("","",lcSql)
						uf_perguntalt_chama("OCORREU UM ERRO A ELIMINAR A RECEITA. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
						RETURN .f.
					ENDIF
				EndIf

				lcSql=""
				If !Empty(ft.u_ltstamp2)
					TEXT TO lcSql NOSHOW TEXTMERGE
						DELETE FROM ctltrct WHERE ctltrctstamp='<<Alltrim(ft.u_ltstamp2)>>'
					ENDTEXT
					If !uf_gerais_actGrelha("","",lcSql)
						uf_perguntalt_chama("OCORREU UM ERRO A ELIMINAR A RECEITA. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
						RETURN .f.
					ENDIF
				EndIf
				
				** ELIMINAR DOC FACT. **
				Select Ft
				lcSQL = ""
				TEXT TO lcSQL NOSHOW textmerge		
					exec up_facturacao_EliminaDoc '<<Alltrim(Ft.Ftstamp)>>'
				Endtext
				If !uf_gerais_actGrelha("","",lcSQL)
					uf_perguntalt_chama("N�O FOI POSS�VEL ELIMINAR O DOCUMENTO. POR FAVOR CONTACTE O SUPORTE. COD. DELETEFT","OK","",16)
					RETURN .f.
				Endif
				************************
				
				** limpar cursor de pesquisa
				IF USED("uCrsPesqFact")
					SELECT uCrsPesqFact
					LOCATE FOR ALLTRIM(uCrsPesqFact.cabstamp) == ALLTRIM(Ft.Ftstamp)
					DELETE
					GO TOP
				ENDIF 
				****************************************
				
				** chama ultimo doc **
				uf_Facturacao_ultimoRegisto()
				********************
			Endif
		ENDIF
	ENDIF
ENDFUNC


**
FUNCTION uf_facturacao_actualiza
	IF !USED("Ft")
		RETURN .f.
	ENDIF
	
	SELECT Ft
	GO TOP
	IF !EMPTY(Ft.ftstamp)
		uf_facturacao_Chama(Ft.ftstamp)
	ENDIF 
ENDFUNC


**
FUNCTION uf_facturacao_registoanterior
	Select FT
*!*		lcnumdoc = FT.FNO -1 
*!*		If !uf_gerais_actGrelha("","ucrsRegSeguinte","SELECT ftstamp as cabstamp FROM ft (nolock) WHERE YEAR(ft.fdata)="+astr(YEAR(ft.fdata))+" and nmdoc = '" + Alltrim(Ft.nmdoc) + "' AND FNO = "+ Str(lcnumdoc))
*!*			return
*!*		Endif
*!*		lcnovostamp= ucrsRegSeguinte.cabstamp
*!*		IF USED("ucrsRegSeguinte")
*!*			fecha("ucrsRegSeguinte")
*!*		ENDIF 
	LOCAL lcstampftactual, lcnovostamp
	STORE '' to lcnovostamp
	lcstampftactual = ALLTRIM(ft.ftstamp)
		
	LOCAL lcnext
	STORE .f. TO lcnext
	
	if(USED("ucrsPesqFact"))
		SELECT ucrsPesqFact
		GO TOP 
		SCAN 
			IF ALLTRIM(lcstampftactual) == ALLTRIM(ucrsPesqFact.cabstamp)
				lcnext = .t.
			ENDIF 
			IF lcnext = .f.
				lcnovostamp= ALLTRIM(ucrsPesqFact.cabstamp)
				lcnext = .f.
			ENDIF 
		ENDSCAN 
		IF EMPTY(lcnovostamp)
			SELECT ucrsPesqFact
			GO BOTTOM  
			lcnovostamp= ucrsPesqFact.cabstamp
		ENDIF 
	Endif
	If !Empty(Alltrim(lcnovostamp))
		uf_facturacao_Chama(lcnovostamp)
	Endif	
ENDFUNC


**
FUNCTION uf_facturacao_registoseguinte
	Select FT
	**lcnumdoc = FT.FNO + 1 
	**If !uf_gerais_actGrelha("","ucrsRegSeguinte","SELECT ftstamp as cabstamp FROM ft (nolock) WHERE YEAR(ft.fdata)="+astr(YEAR(ft.fdata))+" and nmdoc = '" + Alltrim(Ft.nmdoc) + "' AND FNO = "+ Str(lcnumdoc))
	**	return
	**ENDIF
	
	LOCAL lcstampftactual, lcnovostamp
	STORE '' to lcnovostamp
	lcstampftactual = ALLTRIM(ft.ftstamp)
		
	LOCAL lcnext
	STORE .f. TO lcnext
	
	if(USED("ucrsPesqFact"))
		SELECT ucrsPesqFact
		GO TOP 
		SCAN 
			IF lcnext = .t.
				lcnovostamp= ALLTRIM(ucrsPesqFact.cabstamp)
				lcnext = .f.
			ENDIF 
			IF ALLTRIM(lcstampftactual) == ALLTRIM(ucrsPesqFact.cabstamp)
				lcnext = .t.
			ENDIF 
		ENDSCAN 
		IF EMPTY(lcnovostamp)
			SELECT ucrsPesqFact
			GO TOP 
			lcnovostamp= ucrsPesqFact.cabstamp
		ENDIF 
	ENDIF 
	
	**lcnovostamp= ucrsRegSeguinte.cabstamp
	**IF USED("ucrsRegSeguinte")
	**	fecha("ucrsRegSeguinte")
	**ENDIF 
	If !Empty(Alltrim(lcnovostamp))
		uf_facturacao_Chama(lcnovostamp)
	Endif	
ENDFUNC

FUNCTION  uf_facturacao_painel_excecao


	** validar o lote
	LOCAL lcDem, lcLote

	lcDem= .f.
	lcLote = "0"
			
	if(USED("ft"))
		SELECT ft
			lcLote = ft.u_tlote
			if(EMPTY(lcLote))
				lcLote="0"
			ENDIF			
		
		if(VAL(lcLote)>90)
			lcDem= .t.
		endif
	ENDIF
	
	uf_atendimento_painel_excecao(lcDem)
	
ENDFUNC



** Elimina Linha Selecionada
FUNCTION uf_facturacao_apagaLinhaDoc
	LOCAL lcPromo, lcFiPos, lcLordem 
	STORE 0 TO lcFiPos
	
	Select Fi
	IF fi.epromo
		lcPromo = .t.
	ENDIF
		
	Facturacao.lockscreen = .t.

	SELECT FI
	lcLordem = FI.lordem
	SELECT TOP 1 fistamp,lordem FROM FI WHERE FI.lordem > lcLordem ORDER BY FI.lordem ASC INTO CURSOR ucrsNovoStamp READWRITE 
	
	LOCAL lcFistampAnt
	
	SELECT ucrsNovoStamp 
	lcFistampAnt = ALLTRIM(ucrsNovoStamp.fistamp)
	
	
		**Elimina as Linhas
	If Used("FI2")
		DELETE FROM FI2 WHERE FISTAMP = lcFistampAnt
	ENDIF
	
	
	
	&&lcFiPos = RECNO("FI")
	
	SELECT FI
	DELETE
	

	IF lcPromo && se for um vale limpa os descontos
		** recalcular descontos se existirem outros vales **
		uf_atendimento_transformaValeDC(.t., .t.)
	ELSE
		uf_atendimento_transformaValeDC(.t., .t., .t.)
	ENDIF
		
	uf_atendimento_actTotaisFt()
	
	IF RECCOUNT("ucrsNovoStamp")>0
		SELECT ucrsNovoStamp 
		lcStampNovaPosicao = ucrsNovoStamp.fistamp
		
		SELECT fi
		LOCATE FOR fi.fistamp= lcStampNovaPosicao 
		IF FOUND()
			**	
		ELSE
			SELECT fi
			GO Top
		ENDIF 
	ELSE
		SELECT fi
		GO bottom
	ENDIF
	 
	Facturacao.PageFrame1.Page1.gridDoc.refresh
	Facturacao.PageFrame1.Page1.GridDoc.setfocus
	Facturacao.lockscreen = .f.
	
ENDFUNC


**
FUNCTION uf_facturacao_importDocChama
	SELECT FT
	IF EMPTY(ALLTRIM(ft.nmdoc))
		uf_perguntalt_chama("Para poder efectuar a importa��o seleccione primeiro o tipo de documento que pretende criar.","OK","",64)
		RETURN .f.
	ENDIF
	IF EMPTY(ft.no)
		uf_perguntalt_chama("Para poder efectuar a importa��o seleccione primeiro o utente.","OK","",64)
		RETURN .f.
	ENDIF
	uf_importarFact_Chama(ft.no, ft.tipoDoc)
ENDFUNC


** Descontos Comerciais nas Linhas
FUNCTION uf_facturacao_aplicaDescComercial
	IF myFtIntroducao OR myFtAlteracao
		Local lcPos2, lcDesc, lcValida
		Store 0 to lcPos2, lcDesc
		Store .f. to lcValida

		SELECT Fi
		lcPos2 = recno("FI")
		GO TOP
		SCAN
			IF !empty(Fi.desconto)
				lcValida = .t.
			ENDIF
		ENDSCAN

		IF lcValida == .t.
			IF !uf_perguntalt_chama("ATEN��O: ESTE DOCUMENTO J� TEM DESCONTOS PREENCHIDOS, PRETENDE ALTERAR?","Sim","N�o")
				select Fi
				If lcPos2>0
					TRY
						go lcPos2
					CATCH
					ENDTRY
				Endif
				Return
			Endif
		Endif

		lcDesc = VAL(INPUTBOX('Qual o Desconto Pretendido:','','0.00'))

		Select Fi
		Go Top
		SCAN
			Replace Fi.desconto with ROUND(lcDesc,2)
			uf_atendimento_fiiliq(.t.)
			uf_atendimento_actTotaisFt()
		ENDSCAN
			
		Select fi
		If lcPos2>0
			TRY
				Go lcPos2
			CATCH
				*****
			ENDTRY
		ENDIF
	ENDIF 
ENDFUNC


**
FUNCTION uf_facturacao_expandeGridLinhas
	PUBLIC myResizeFacturacao

	If myResizeFacturacao == .t.
		Facturacao.pageframe1.page1.GridDoc.height = Facturacao.pageframe1.page1.GridDoc.height - 60
		Facturacao.pageframe1.page1.containerTotais.visible = .t.
		Facturacao.pageframe1.page1.containerLinhas.chkMostraTotais.config(myPath + "\imagens\icons\pagedown_B.png", "")
		myResizeFacturacao= .f.
		
	ELSE
		IF Facturacao.Pageframe1.height > Facturacao.pageframe1.page1.GridDoc.height + 60
			Facturacao.pageframe1.page1.GridDoc.height = Facturacao.pageframe1.page1.GridDoc.height + 60
			Facturacao.pageframe1.page1.containerTotais.visible = .f.
			Facturacao.pageframe1.page1.containerLinhas.chkMostraTotais.config(myPath + "\imagens\icons\pageup_B.png", "")
			myResizeFacturacao= .t.
		Endif
	Endif
	Facturacao.pageframe1.page1.containerLinhas.top = Facturacao.pageframe1.page1.GridDoc.height + 30
ENDFUNC


**
FUNCTION uf_facturacao_sair
	* Verifica se o Documento esta em Modo de Edicao ou Introducao
	If myFtAlteracao == .t. OR myFtIntroducao == .t.
		If uf_perguntalt_chama('Quer mesmo cancelar o documento de Factura��o?'+CHR(13)+CHR(13)+'Ir� perder as �ltimas altera��es feitas',"Sim","N�o")
			myFtIntroducao	=.f.
			myFtAlteracao	= .f.
							
			uf_Facturacao_ultimoRegisto()
			
			**Controla Objectos Visiveis e Enabled
			uf_Facturacao_controlaObjectos()
			
				
			if(USED("mixed_bulk_pend"))
				DELETE FROM  mixed_bulk_pend
			ENDIF
			if(USED("fi_trans_info"))
				DELETE FROM  fi_trans_info
			ENDIF
			
			release myInsereReceitaCorrigida

			
			
		ENDIF 
	ELSE 		
		uf_facturacao_exit()
		
		
	
	ENDIF 
ENDFUNC


** Apaga Registos do Cursor de Cabe�alho
Function uf_Facturacao_apagaRegistosFt
	Select Ft
	Scan
		Delete
	Endscan
Endfunc


** Apaga Registos do Curso das Linhas
Function uf_Facturacao_apagaRegistosLinhasFi
	Select FI
	Scan
		Delete
	Endscan
ENDFUNC


**
FUNCTION uf_facturacao_wkCamposEspecificos
	
	IF USED("ucrsWkLinhasAux")

		
		&& Farmacia Acrescenta CNPEM, PIC
		SELECT ucrsE1
		IF ALLTRIM(UPPER(ucrsE1.TipoEmpresa)) == "FARMACIA" 
			SELECT ft
			IF !EMPTY(ft.u_ltstamp) OR !EMPTY(ft.u_ltstamp2);
				OR (td.u_tipodoc == 3 AND (myFtIntroducao == .t. OR myFtAlteracao == .t.));
				OR (td.u_tipodoc == 9 AND (myFtIntroducao == .t. OR myFtAlteracao == .t.));
				OR (td.u_tipodoc == 8 AND (myFtIntroducao == .t. OR myFtAlteracao == .t.));
				OR (td.u_tipodoc == 7 AND (myFtIntroducao == .t. OR myFtAlteracao == .t.));
				OR (td.u_tipodoc == 2 AND (myFtIntroducao == .t. OR myFtAlteracao == .t.))
				
				SELECT ucrsWkLinhasAux
				GO Top
				SELECT ucrsWkLinhasAux
				LOCATE FOR UPPER(ALLTRIM(ucrsWkLinhasAux.wkfield)) == "FI.REF"
				IF FOUND()
				
					lcOrdem = ucrsWkLinhasAux.ordem
					
					UPDATE ucrsWkLinhasAux SET ordem = ordem + 1 WHERE ordem > lcOrdem
					
					SELECT ucrsWkLinhasAux
					APPEND BLANK
					Replace ucrsWkLinhasAux.wkreadonly WITH .t.
					Replace ucrsWkLinhasAux.wkfield WITH "FI.CNPEM"
					Replace ucrsWkLinhasAux.wktitle WITH "CNPEM"
					Replace ucrsWkLinhasAux.ordem WITH lcOrdem + 1
					Replace ucrsWkLinhasAux.largura WITH 90
					
				ENDIF 
				
*!*					
*!*					SELECT ucrsWkLinhasAux
*!*					GO Top
*!*					SELECT ucrsWkLinhasAux
*!*					LOCATE FOR UPPER(ALLTRIM(ucrsWkLinhasAux.wkfield)) == "FI.U_EPVP"
*!*					IF FOUND()
*!*									
*!*									
*!*						SELECT ucrsWkLinhasAux			
*!*						lcOrdem = ucrsWkLinhasAux.ordem
*!*						
*!*						UPDATE ucrsWkLinhasAux SET ordem = ordem + 1 WHERE ordem > lcOrdem
*!*						
*!*						SELECT ucrsWkLinhasAux
*!*						APPEND BLANK
*!*						Replace ucrsWkLinhasAux.wkreadonly WITH .t.
*!*						Replace ucrsWkLinhasAux.wkfield WITH "FI.PIC"
*!*						Replace ucrsWkLinhasAux.wktitle WITH "PIC"
*!*						Replace ucrsWkLinhasAux.ordem WITH lcOrdem + 1
*!*						Replace ucrsWkLinhasAux.largura WITH 70
*!*						Replace ucrsWkLinhasAux.mascara WITH "999999.99"
*!*						
*!*					ENDIF 

			ENDIF 
		ENDIF 
	ENDIF 

ENDFUNC 


** Controla Workflow do Documento Escolhido
FUNCTION uf_Facturacao_controlaWkLinhas
	Local ti, i, lcValorQuabraOld, lcValorCCAntes     
	
	STORE '' TO lcValorQuabraOld, lcValorCCAntes
		
	SELECT Ft
	GO TOP
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_facturacao_wk '<<Alltrim(Ft.nmdoc)>>'
	ENDTEXT 
	
	**Apaga colunas documento anterior se existirem
	FACTURACAO.PageFrame1.Page1.griddoc.columncount = 0

	SELECT FI
	GO TOP
	**Atribui recourdSource � Grid mediante o Tipo de documento
	FACTURACAO.PageFrame1.Page1.griddoc.recordsource="FI"
	
	If !uf_gerais_actGrelha("","ucrsWkLinhasAux",lcSQL)
		uf_perguntalt_chama("N�O FOI POSSIVEL IDENTIFICAR A CONFIGURA��O WORKFLOW ASSOCIADO AO DOCUMENTO.","OK","",16)
		RETURN .f.
	ENDIF
	
	uf_facturacao_wkCamposEspecificos()
	SELECT ucrsWkLinhasAux 
	GO Top
	SELECT * FROM ucrsWkLinhasAux ORDER BY ordem INTO CURSOR ucrsWkLinhas READWRITE 
	
	IF USED("ucrsWkLinhasAux")
		fecha("ucrsWkLinhasAux")
	ENDIF 
	
	LOCAL lcTabIvaPLinha
	
	i=1
			
	* VALIDA AS COLUNAS A MOSTRAR
	Select ucrsWkLinhas
	GO TOP 
	SCAN 
		IF LEFT(UPPER(Alltrim(ucrsWkLinhas.wkfield)),2) == "FI" AND UPPER(Alltrim(ucrsWkLinhas.wkfield)) != 'FI.U_ROBOT' &&AND UPPER(Alltrim(ucrsWkLinhas.wkfield)) != 'FI.FICCUSTO'
						
			IF TYPE(ucrsWkLinhas.wkfield) == 'C' OR  TYPE(ucrsWkLinhas.wkfield) == 'N' OR  TYPE(ucrsWkLinhas.wkfield) == 'L'
			
				WITH FACTURACAO.PageFrame1.Page1.griddoc

					.AddColumn
					.RowHeight = 30
					.columns(i).ControlSource = Alltrim(ucrsWkLinhas.wkfield)
					.columns(i).FontName = "Verdana"
					.columns(i).Fontsize = 9
					.columns(i).header1.FontName = "Verdana"
					.columns(i).ReadOnly = ucrsWkLinhas.WKREADONLY
					.columns(i).text1.ReadOnly = ucrsWkLinhas.WKREADONLY
					
					IF UPPER(Alltrim(ucrsWkLinhas.wkfield)) == "FI.LOTE" && Aletra de varios Pre�os para o lote Lotes
						.columns(i).dynamicBackColor = "IIF(fi.usalote and EMPTY(fi.lote),rgb[232,76,61],IIF(fi.alertaLote,rgb[232,76,61],IIF(recno()%2!=0,Rgb[255,255,255],rgb[240,240,240])))"
					ELSE
						.Columns(i).dynamicBackColor = "IIF(recno()%2!=0,Rgb[255,255,255],rgb[240,240,240])"
					ENDIF
					
					IF UPPER(Alltrim(ucrsWkLinhas.wkfield)) == "FI.U_EPVP" 
						.columns(i).dynamicBackColor = "IIF(fi.AlertaPic, rgb[232,76,61],IIF(recno()%2!=0,Rgb[255,255,255],rgb[240,240,240]))"
						.columns(i).dynamicForeColor = "IIF(fi.AlertaPic, rgb[255,255,255],rgb[0,0,0])"
					ELSE
						.Columns(i).dynamicBackColor = "IIF(recno()%2!=0,Rgb[255,255,255],rgb[240,240,240])"
					ENDIF

					

					.Columns(i).DynamicFontBold = "FI.PESQUISA"
										
					If ucrsWkLinhas.largura != 0
						.Columns(i).width = ucrsWkLinhas.largura
					ENDIF

					If 	!Empty(ucrsWkLinhas.mascara)
						.Columns(i).InputMask=Alltrim(ucrsWkLinhas.mascara)
						.Columns(i).text1.InputMask=Alltrim(ucrsWkLinhas.mascara)
					Endif

					.columns(i).header1.caption = Alltrim(ucrsWkLinhas.wktitle)
					.columns(i).header1.Alignment = 2
					&&.columns(i).header1.FontBold = .t.
					.columns(i).text1.BorderStyle =  0
					.columns(i).text1.ForeColor = RGB(0,0,0)
					.columns(i).text1.SelectedForeColor = RGB(0,0,0)
						.columns(i).text1.ControlSource = Alltrim(ucrsWkLinhas.wkfield)
					.Columns(i).Format="RTK"
					.Columns(i).text1.Format="RTK"
					.Columns(i).text1.hideSelection=.t.	
					.Columns(i).columnorder = ucrsWkLinhas.Ordem
					
					IF TYPE(ucrsWkLinhas.wkfield) == 'L'
						 .columns(i).RemoveObject("Text1")
						 .columns(i).AddObject("Check1","Checkbox")
						 .columns(i).Check1.Caption = ""
						 .columns(i).Alignment = 2
						 .columns(i).Check1.Alignment = 2
						 .columns(i).Sparse = .f.
						 .columns(i).Visible = .t.
						 .Columns(i).Check1.SpecialEffect = 1
						 .Columns(i).Check1.ColorSource = 0
                         .Columns(i).Check1.Style = 1
                         .columns(i).Check1.Picture = myPath + "\imagens\icons\unchecked_b_24.bmp"
                         .columns(i).Check1.DownPicture = myPath + "\imagens\icons\checked_b_24.bmp"
                         .columns(i).Check1.specialEffect = 1
                         .columns(i).Check1.themes = .f.
					ENDIF
				
				ENDWITH
			ELSE
				uf_perguntalt_chama("N�O FOI POSSIVEL APRESENTAR O CAMPO: " + ALLTRIM(UCRSWKLINHAS.WKFIELD) + ".","OK","",48)
			ENDIF
		ELSE
		
			&&Botao Origens
			If Upper(Alltrim(ucrsWkLinhas.wkfield)) == "WKORIGENS" 
			
				WITH FACTURACAO.PageFrame1.Page1.griddoc
					.AddColumn 
					If ucrsWkLinhas.largura != 0
						.Columns(i).width = ucrsWkLinhas.largura
					ENDIF
					.columns(i).header1.caption = Alltrim(ucrsWkLinhas.wktitle)
					.columns(i).header1.Alignment = 2
					&&.columns(i).header1.FontBold = .t.
					.Columns(i).AddObject('ORIGENS','CommandButtonOrigens')
					.Columns(i).ORIGENS.visible = .t.
					.Columns(i).Sparse = .f.
					.Columns(i).bound= .f.
					.Columns(i).ORIGENS.enabled= .t.
					.Columns(i).CurrentControl = 'ORIGENS'
					.Columns(i).ORIGENS.SpecialEffect = 1
                    .Columns(i).ORIGENS.Picture = myPath + "\imagens\icons\left_b_24.bmp"
                    .Columns(i).ORIGENS.PicturePosition = 14
					* DEFINE ORDEM
					.Columns(i).columnorder=ucrsWkLinhas.Ordem
				ENDWITH
			ENDIF	
		
			&&Botao Destinos
			If Upper(Alltrim(ucrsWkLinhas.wkfield)) == "WKDESTINOS" 
			
				WITH FACTURACAO.PageFrame1.Page1.griddoc
					.AddColumn 
					If ucrsWkLinhas.largura != 0
						.Columns(i).width = ucrsWkLinhas.largura
					ENDIF
					.columns(i).header1.caption = Alltrim(ucrsWkLinhas.wktitle)
					.columns(i).header1.Alignment = 2
					&&.columns(i).header1.FontBold = .t.
					.Columns(i).AddObject('DESTINOS','CommandButtonDestinos')
					.Columns(i).DESTINOS.visible = .t.
					.Columns(i).Sparse = .f.
					.Columns(i).bound= .f.
					.Columns(i).DESTINOS.enabled= .t.
					.Columns(i).CurrentControl = 'DESTINOS'
					.Columns(i).DESTINOS.SpecialEffect = 1
                    .Columns(i).DESTINOS.Picture = myPath + "\imagens\icons\right_b_24.bmp"
                    .Columns(i).DESTINOS.PicturePosition = 14
					* DEFINE ORDEM
					.Columns(i).columnorder=ucrsWkLinhas.Ordem
				ENDWITH
			ENDIF

			&&Trata botao Manipular Valores Comparticipa��o
			If Upper(Alltrim(ucrsWkLinhas.wkfield)) == "FI.U_ROBOT" 
				WITH FACTURACAO.PageFrame1.Page1.griddoc
					.AddColumn
					If ucrsWkLinhas.largura != 0
						.Columns(i).width = ucrsWkLinhas.largura
					ENDIF
					.columns(i).header1.caption = Alltrim(ucrsWkLinhas.wktitle)
					.columns(i).header1.Alignment = 2
					&&.columns(i).header1.FontBold = .t.
					.columns(i).header1.FontName = "Verdana"
					.columns(i).FontName = "Verdana"
					.Columns(i).AddObject('MANIPULAVALORES','commandButtonMV')
					.Columns(i).MANIPULAVALORES.visible = .t.
					.Columns(i).Sparse = .f.
					.Columns(i).bound= .f.
					.Columns(i).MANIPULAVALORES.enabled= .t.
					.Columns(i).CurrentControl = 'MANIPULAVALORES'
					.Columns(i).MANIPULAVALORES.CAPTION = 'M.V.'
					* DEFINE ORDEM
					.Columns(i).columnorder=ucrsWkLinhas.Ordem
				ENDWITH
			ENDIF
			
		ENDIF
		
		WITH FACTURACAO.PageFrame1.Page1.griddoc
			&&ocultar coluna lote
			If Upper(Alltrim(ucrsWkLinhas.wkfield)) == "FI.LOTE" 
				IF !uf_gerais_getParameter("ADM0000000211","BOOL")
					.columns(i).visible = .f.
				ENDIF 
			ENDIF
		ENDWITH
		
		i=i+1
	ENDSCAN 

	**Coloca Eventos
	uf_facturacao_ControlaEventosLinhas()

	*FACTURACAO.PageFrame1.Page1.griddoc.refresh
	FACTURACAO.refresh
ENDFUNC




FUNCTION uf_facturacao_guardaDadosPsico


	LOCAL lcPosFi
	LOCAL lcValPsiGrava 
	
	lcValPsiGrava =.f.
	
	lcPosFi= recno("FI")
	
	&&valida se tem algum psico
	SELECT fi
	GO TOP
	SCAN
		IF fi.u_psico	
			lcValPsiGrava = .t.
			EXIT
		ENDIF
	ENDSCAN
	

	
	IF(lcValPsiGrava==.t.)
	

	
		&& cria / atualiza registos na tabela dispensa_eletronica_cc [webservice usa esta tabela para envio dos dados]
		lcSQL = ''
		TEXT TO lcSQL NOSHOW TEXTMERGE
			SELECT * FROM dispensa_eletronica_cc (nolock) WHERE doc_id = '<<ALLTRIM(dadosPsico.u_ndutavi)>>'
		ENDTEXT
		IF	!uf_gerais_actGrelha("","uCrsAuxUtente",lcSQL)
			uf_perguntalt_chama("Ocorreu uma anomalia ao verificar a informa��o do utentes. Por favor contacte o Suporte.","OK","",16)
			RETURN .f.
		ELSE
			
			&&actualizar a sp com os dados completos
			&& insere registo na tabela para consulta do webservice
			IF RECCOUNT("uCrsAuxUtente") == 0
				lcSQL = ''
				TEXT TO lcSQL NOSHOW TEXTMERGE
					insert into dispensa_eletronica_cc (id, id_emp, nome, morada, nif, nrSNS, sexo, nascimento, validade_fim, publicKey, site, machine, doc_id, tipoDoc) 
               		values (LEFT(NEWID(),20), '<<ALLTRIM(uCrsE1.id_lt)>>', '<<ALLTRIM(ft.nome)>>', '<<ALLTRIM(ft.morada)>>', '<<ALLTRIM(ft.ncont)>>', '<<ALLTRIM(ft.nbenef)>>', '', '<<uf_gerais_getdate(ft.nascimento,"SQL")>>', '<<uf_gerais_getdate(dadosPsico.u_ddutavi, "SQL")>>', '','<<ALLTRIM(mySite)>>', '','<<ALLTRIM(dadosPsico.u_ndutavi)>>', '<<ALLTRIM(dadosPsico.codigoDocSPMS)>>')
               	ENDTEXT
               	IF	!uf_gerais_actGrelha("","uCrsAuxInsertUtente",lcSQL)
					uf_perguntalt_chama("Ocorreu uma anomalia ao inserir a informa��o detalhada do utente. Por favor contacte o Suporte.","OK","",16)
					RETURN .f.
				ENDIF
			ELSE
			
			
				lcSQL = ''
				TEXT TO lcSQL NOSHOW TEXTMERGE
				
					update dispensa_eletronica_cc 
					SET validade_fim = '<<uf_gerais_getdate(dadosPsico.u_ddutavi, "SQL")>>',nome = '<<ALLTRIM(dadosPsico.u_nmutavi)>>', morada = '<<ALLTRIM(dadosPsico.u_moutavi)>>'
					,doc_id = '<<ALLTRIM(dadosPsico.u_ndutavi)>>', nascimento ='<<uf_gerais_getdate(ft.nascimento,"SQL")>>', tipoDoc = '<<ALLTRIM(dadosPsico.codigoDocSPMS)>>'
					where doc_id = '<<ALLTRIM(dadosPsico.u_ndutavi)>>'

               	ENDTEXT
               	

               	
               	IF	!uf_gerais_actGrelha("","uCrsAuxInsertUtente",lcSQL)
					uf_perguntalt_chama("Ocorreu uma anomalia ao atualizar a informa��o detalhada do utente. Por favor contacte o Suporte.","OK","",16)
					RETURN .f.
				ENDIF
			ENDIF
		ENDIF

	ENDIF

	
	select Fi
	If lcPosFi>0
		TRY
			Go lcPosFi
		CATCH
			*****
		ENDTRY
	ENDIF
	
	RETURN .t.
	
ENDFUNC


**
FUNCTION uf_facturacao_gravar
	LPARAMETERS llAtend
	


	** Verificar se existe nova vers�o para reiniciar o software
	uf_chk_nova_versao()
	
	LOCAL lcValida, lcSqlFt2,lcSqlFt,lcSqlFi, lcSQLInsertDoc, lcValidaInsercaoDocumento, lcSQLUpdateDoc, lcSqlCert , lcTotprod, lcTotserv, lcTpempr, lcSqlCompartExterna
	STORE .f. TO lcValida, lcValidaInsercaoDocumento
	STORE "" TO lcSqlFt2, lcSqlFt, lcSqlFi, lcSqlFi2, lcSQLInsertDoc, lcSQLUpdateDoc, lcSqlCert, lcSqlCompartExterna
	
	lcTotprod=0
	lcTotserv=0
	lcTpempr=''
	
	TEXT TO lcSQL TEXTMERGE NOSHOW 
		select top 1 tipoempresa from empresa (nolock)
	ENDTEXT 
	uf_gerais_actGrelha("", "uCrstpempr", lcSQL)
	lcTpempr=ALLTRIM(uCrstpempr.tipoempresa)
	
	
	SELECT ucrse1
	 IF LEN(ALLTRIM(ucrse1.motivo_isencao_iva))=0
	 	uf_perguntalt_chama("N�O EXISTE MOTIVO DE ISEN��O DE IVA POR DEFEITO PREENCHIDO NA FICHA DA EMPRESA. POR FAVOR ATUALIZE PARA PODER FINALIZAR O ATENDIMENTO", "OK", "", 48)
	    RETURN .F.
	 ENDIF 
 
	&& Verificar data fechada para altera��es
	IF uf_gerais_getParameter_site('ADM0000000006','BOOL')
		IF !EMPTY(ALLTRIM(uf_gerais_getParameter_site('ADM0000000007', 'text'))) AND DTOC(ft.fdata,1)<uf_gerais_getParameter_site('ADM0000000007', 'text') 
			uf_perguntalt_chama("N�o pode inserir um documento com data inferior � data fechada para altera��es/elimina��es.","OK","",64)
			RETURN .f.
		ENDIF 
	ENDIF 
	
	&&Remove linhas a branco
	uf_documentos_removeLinhasVazias()
	
	** VERIFICAR SE OBRIGA SUPERVISOR NA ALTERA��O PARA DOCUMENTO A CR�DITO
	IF uf_gerais_getParameter_site('ADM0000000054', 'BOOL', mySite) = .t. AND ALLTRIM(ch_grupo) <> 'Administrador' AND ALLTRIM(ch_grupo) <> 'Supervisores' AND (ALLTRIM(td.tiposaft)='FT' OR ALLTRIM(td.tiposaft)='NC' OR ALLTRIM(td.tiposaft)='ND')
		IF !uf_gerais_valida_password_admin('FATURA��O', 'ALTERA��O DOC. PARA CR�DITO')
			RETURN .f.
		ENDIF 
	ENDIF 
	
	SELECT td 
	&& Valida��o artigos sem stock
	IF td.lancasl=.t. AND !uf_faturacao_valida_artigo_sem_stock() 
		RETURN .f. 
	ENDIF
	
	&& Validar valores m�nimos de fatura��o sem NIF
	&& Validar valores m�nimos de fatura��o sem NIF
	lcMoedaDefault = uf_gerais_getParameter("ADM0000000260","text")
	IF EMPTY(lcMoedaDefault)
		IF (ALLTRIM(ft.ncont)="999999990" OR EMPTY(ft.ncont)) AND (ALLTRIM(ft.nmdoc)=='Factura' OR ALLTRIM(ft.nmdoc)='Venda a Dinheiro' OR ALLTRIM(ft.nmdoc)='Fatura Recibo') AND UPPER(ALLTRIM(ucrse1.pais)) == 'PORTUGAL'	
			SELECT fi
			GO TOP 
			SCAN 
				IF !EMPTY(fi.ref) AND fi.stns=.t. 
					lcTotserv=lcTotserv+fi.etiliquido
				ENDIF 
				IF !EMPTY(fi.ref) AND fi.stns=.f. 
					lcTotprod=lcTotprod+fi.etiliquido
				ENDIF 
			ENDSCAN 
			IF lcTpempr='FARMACIA'
				IF lcTotserv>100 then
					uf_perguntalt_chama("N�o pode efetuar documentos com valor de servi�os superior a 100� sem preencher o NIF do cliente. Por favor corrija.","OK","",16)
					RETURN .f.
				ENDIF 
				IF lcTotprod>1000 then
					uf_perguntalt_chama("N�o pode efetuar documentos com valor de artigos superior a 1000� sem preencher o NIF do cliente. Por favor corrija.","OK","",16)
					RETURN .f.
				ENDIF 
			ELSE
				IF lcTotserv+lcTotprod>100 then
					uf_perguntalt_chama("N�o pode efetuar documentos com valor de superior a 100� sem preencher o NIF do cliente. Por favor corrija.","OK","",16)
					RETURN .f.
				ENDIF 
			ENDIF 
		ENDIF 
	ENDIF 
	
	IF !llAtend
		facturacao.containercab.lote.setfocus
	ENDIF 
	
	** valida��o dos dados do documento de origem para Angola NC X3
	IF UPPER(myPaisConfSoftw) == 'ANGOLA'
		LOCAL valmanintro
		store .f. to valmanintro
		IF ALLTRIM(ft.nmdoc)='Nt. Cr�d. Segur.' OR ALLTRIM(ft.nmdoc)='Nota de Cr�dito'
			SELECT fi
			GO TOP 
			SCAN 
				IF !EMPTY(fi.ref) AND EMPTY(fi.ofistamp)
					valmanintro = .t.
				ENDIF 
			ENDSCAN 
		ENDIF 
		
		IF valmanintro = .t.
			PUBLIC cvalnrdoc
			STORE '' TO cvalnrdoc
			uf_tecladoalpha_chama("cvalnrdoc", "Introduza o n� do documento origem:", .f., .f., 0)
			IF EMPTY(cvalnrdoc)
				uf_perguntalt_chama("TEM DE INTRODUZIR OBRIGAT�RIAMENTE O N� DE DOCUMENTO DE ORIGEM! VOLTE A EFETUAR A DEVOLU��O.","OK","",64)	
				return(.f.)
			ENDIF 
			SELECT ft2 
			replace ft2.u_docorig WITH ALLTRIM(cvalnrdoc)
			uf_atendimento_AdicionaLinhaFi(.t.,.t.)
			SELECT fi
			replace fi.lordem WITH 100
			replace fi.design WITH 'Anula��o Ft. Seguradoras Nr. '+ALLTRIM(cvalnrdoc)+' '
			
			
			IF ALLTRIM(ft.nmdoc)='Nt. Cr�d. Segur.'
				PUBLIC cvalnrdoc
				STORE '' TO cvalnrdoc
				uf_tecladoalpha_chama("cvalnrdoc", "Introduza o Nome do Segurado", .f., .f., 0)
				IF EMPTY(cvalnrdoc)
					uf_perguntalt_chama("TEM DE INTRODUZIR OBRIGAT�RIAMENTE O NOME DO SEGURADO! VOLTE A GRAVAR A DEVOLU��O.","OK","",64)	
					return(.f.)
				ENDIF 
				SELECT ft2 
				replace ft2.contacto WITH ALLTRIM(cvalnrdoc)
				
				uf_tecladoalpha_chama("cvalnrdoc", "Introduza o Nr. do Cart�o da Seguradora", .f., .f., 0)
				IF EMPTY(cvalnrdoc)
					uf_perguntalt_chama("TEM DE INTRODUZIR OBRIGAT�RIAMENTE O N� DO CART�O! VOLTE A GRAVAR A DEVOLU��O.","OK","",64)	
					return(.f.)
				ENDIF 
				SELECT ft2 
				replace ft2.c2codpost WITH ALLTRIM(cvalnrdoc)
				
				uf_tecladoalpha_chama("cvalnrdoc", "Introduza o contacto do segurado", .f., .f., 0)
				SELECT ft2 
				replace ft2.telefone WITH ALLTRIM(cvalnrdoc)
			
			ENDIF 
		ENDIF 
	
	ENDIF 
	
	myFactAGravar = .t.
	
	** Grava Dados Psico
	LOCAL lcValPsiGrava
	STORE .f. TO lcValPsiGrava
	
	** Carrega TD novamente
	IF !USED("TD")
		Select Ft
		SELECT * FROM uCrsGeralTD WHERE UPPER(ALLTRIM(uCrsGeralTD.nmdoc)) == UPPER(ALLTRIM(Facturacao.containercab.nmdoc.VALUE)) AND ALLTRIM(uCrsGeralTD.site) == ALLTRIM(mySite) INTO CURSOR TD READWRITE
	ENDIF 
		
	&& Avalia se � um documento integrado
	IF USED("td")
		SELECT TD
		IF td.u_integra == .T.
			uf_perguntalt_chama("ESTA S�RIE DE DOCUMENTO EST� CONFIGURADA COMO SENDO DE INTEGRA��O. N�O � POSS�VEL CRIAR DOCUMENTOS DESTE TIPO.","OK","",48)
			RETURN .F.
		ENDIF
	ELSE
		uf_perguntalt_chama("O CURSOR TD N�O EST� CARREGADO.","OK","",48)
		RETURN .F.
	ENDIF	
	
	if(uf_facturacao_guardaDadosPsico()  == .f.)
		RETURN .f.	
	endif	
	&& verifica campos obrigat�rios na gravacao 
	IF uf_facturacao_validaCamposObrigatorios() == .f.
		RETURN .f.	
	ENDIF

	IF myFtIntroducao = .t. && Introdu��o
		IF !(TYPE("FACTURACAO") == "U")
			&& Valida Mov Caixa
			IF facturacao.pageframe1.page2.movcaixa.Tag == "true"
				SELECT td
				IF td.u_tipodoc == 8
					**PUBLIC myCxStamp
					IF EMPTY(myCxStamp)
						uf_GestaoCaixas_chama(.t.)
						RETURN .f.
					ENDIF
					uf_mrpagamento_chama("FACTURACAO")
					PUBLIC myMrPagamentoCancelou, myMrPagamentoPagou
					IF EMPTY(myMrPagamentoPagou)
						RETURN .f.
					ENDIF 
				ENDIF 
			ENDIF 
		ENDIF
		&& Regras
		lcValida = uf_facturacao_RegrasGravacao(llAtend)
		
		&& Valida Isen��o de Iva
		SELECT fi
		GO TOP
		SCAN
			IF !empty(alltrim(td.tiposaft)) and fi.iva==0 AND EMPTY(ft2.motiseimp)
				DO CASE
					CASE Ft.pais == 3
						UPDATE ft2 SET motiseimp = "Isento Artigo 14. do CIVA (ou similar)"
					CASE Ft.pais == 2
						UPDATE ft2 SET motiseimp = "Isento Artigo 14. do RITI (ou similar)"
					OTHERWISE
                        IF EMPTY(ucrse1.motivo_isencao_iva)
                            UPDATE ft2 SET motiseimp = "Isento Artigo 9. do CIVA (ou similar)" 
                        ELSE 
                            UPDATE ft2 SET motiseimp = ALLTRIM(ucrse1.motivo_isencao_iva)
                        ENDIF 
				ENDCASE
			ENDIF
		ENDSCAN

		&& gravar registo nas ocorr�ncias caso tenham alterado PVP's
		select fi
		SCAN FOR fi.amostra == .t.
			uf_gerais_registaOcorrencia('Vendas','Altera��o de PVP na Venda (Back)',2,alltrim(fi.ref) + ' PVP: ' + astr(fi.epvori,8,2), alltrim(fi.ref) + ' PVP: ' + astr(fi.u_epvp,8,2), alltrim(fi.fistamp),'','','')
		ENDSCAN
		**
		
		

		If lcValida == .t.

			regua(0,100,"A PROCESSAR A GRAVA��O DO DOCUMENTO...",.f.)

			SELECT Ft

			&& Variaveis publicas para insercao igual em todas as tabelas
			SET HOURS TO 24
			Atime=ttoc(datetime()+(difhoraria*3600),2)
			Astr=left(Atime,8)
			myHora = Astr
			**myHora	= Time() && � necess�rio remover os segundos durante a inser��o na tabela
			myInvoiceData = uf_gerais_getDate(ft.fdata,"SQL")
			**
			
			&& cursor para enviar par�metros
			IF !USED("uCrsValIva")
				CREATE CURSOR uCrsValIva ;
				(lcEivaIn1 numeric(19,2), lcEivaIn2 numeric(19,2), lcEivaIn3 numeric(19,2), lcEivaIn4 numeric(19,2), lcEivaIn5 numeric(19,2), lcEivaIn6 numeric(19,2), lcEivaIn7 numeric(19,2), lcEivaIn8 numeric(19,2), lcEivaIn9 numeric(19,2), lcEivaIn10 numeric(19,2), lcEivaIn11 numeric(19,2), lcEivaIn12 numeric(19,2), lcEivaIn13 numeric(19,2), ;
				lcEivaV1 numeric(19,2), lcEivaV2 numeric(19,2), lcEivaV3 numeric(19,2), lcEivaV4 numeric(19,2), lcEivaV5 numeric(19,2), lcEivaV6 numeric(19,2), lcEivaV7 numeric(19,2), lcEivaV8 numeric(19,2), lcEivaV9 numeric(19,2), lcEivaV10 numeric(19,2), lcEivaV11 numeric(19,2), lcEivaV12 numeric(19,2), lcEivaV13 numeric(19,2))
			ELSE
				**SELECT uCrsValIva
				**DELETE ALL 
				FECHA("uCrsValIva")
				CREATE CURSOR uCrsValIva ;
				(lcEivaIn1 numeric(19,2), lcEivaIn2 numeric(19,2), lcEivaIn3 numeric(19,2), lcEivaIn4 numeric(19,2), lcEivaIn5 numeric(19,2), lcEivaIn6 numeric(19,2), lcEivaIn7 numeric(19,2), lcEivaIn8 numeric(19,2), lcEivaIn9 numeric(19,2), lcEivaIn10 numeric(19,2), lcEivaIn11 numeric(19,2), lcEivaIn12 numeric(19,2), lcEivaIn13 numeric(19,2), ;
				lcEivaV1 numeric(19,2), lcEivaV2 numeric(19,2), lcEivaV3 numeric(19,2), lcEivaV4 numeric(19,2), lcEivaV5 numeric(19,2), lcEivaV6 numeric(19,2), lcEivaV7 numeric(19,2), lcEivaV8 numeric(19,2), lcEivaV9 numeric(19,2), lcEivaV10 numeric(19,2), lcEivaV11 numeric(19,2), lcEivaV12 numeric(19,2), lcEivaV13 numeric(19,2))
			ENDIF
			SELECT uCrsValIva
			APPEND BLANK
				replace ;
					uCrsValIva.lcEivaIn1 WITH ft.EivaIn1 ;
					uCrsValIva.lcEivaIn2 WITH ft.EivaIn2 ;
					uCrsValIva.lcEivaIn3 WITH ft.EivaIn3 ;
					uCrsValIva.lcEivaIn4 WITH ft.EivaIn4 ;
					uCrsValIva.lcEivaIn5 WITH ft.EivaIn5 ;
					uCrsValIva.lcEivaIn6 WITH ft.EivaIn6 ;
					uCrsValIva.lcEivaIn7 WITH ft.EivaIn7 ;
					uCrsValIva.lcEivaIn8 WITH ft.EivaIn8 ;
					uCrsValIva.lcEivaIn9 WITH ft.EivaIn9 ;
					uCrsValIva.lcEivaIn10 WITH ft.EivaIn10 ;
					uCrsValIva.lcEivaIn11 WITH ft.EivaIn11 ;
					uCrsValIva.lcEivaIn12 WITH ft.EivaIn12 ;
					uCrsValIva.lcEivaIn13 WITH ft.EivaIn13 ;
					uCrsValIva.lcEivaV1 WITH ft.EivaV1 ;
					uCrsValIva.lcEivaV2 WITH ft.EivaV2 ;
					uCrsValIva.lcEivaV3 WITH ft.EivaV3 ;
					uCrsValIva.lcEivaV4 WITH ft.EivaV4 ;
					uCrsValIva.lcEivaV5 WITH ft.EivaV5 ;
					uCrsValIva.lcEivaV6 WITH ft.EivaV6 ;
					uCrsValIva.lcEivaV7 WITH ft.EivaV7 ;
					uCrsValIva.lcEivaV8 WITH ft.EivaV8 ;
					uCrsValIva.lcEivaV9 WITH ft.EivaV9 ;
					uCrsValIva.lcEivaV10 WITH ft.EivaV10 ;
					uCrsValIva.lcEivaV11 WITH ft.EivaV11 ;
					uCrsValIva.lcEivaV12 WITH ft.EivaV12 ;
					uCrsValIva.lcEivaV13 WITH ft.EivaV13

			lcSqlFt2 = uf_PAGAMENTO_GravarVendaFt2(ft.ftstamp, ft.ndoc, ft2.u_receita, ft2.u_codigo, ft2.u_codigo2, ft2.u_design, ft2.u_design2, ft2.u_abrev, ft2.u_abrev2, myHora, ft2.codAcesso, ft2.codDirOpcao, ft2.token)
			lcSqlFt2 = uf_gerais_trataPlicasSQL(lcSqlFt2) 
			
			TEXT TO lcSQLFtDeclare NOSHOW TEXTMERGE 
				DECLARE @fno as numeric(9,0)
			ENDTEXT 
			
			lcSQLFt = uf_PAGAMENTO_GravarVendaFt(ft.ftstamp, ft.nmdoc, ft.ndoc, ft.tipodoc, ft.no, ft.nome, ft.estab, Ft.TotQtt, ft.qtt1, ft.etotal, Ft.EttIliq, ft.custo, Ft.EttIva, Ft.edescc, ft.pdata, myhora, ft.cobrado, uf_gerais_getDate(ft.cdata,"SQL"), ft.ncont, uf_gerais_getDate(ft.bidata,"SQL"))
			lcSqlFt = uf_gerais_trataPlicasSQL(lcSQLFtDeclare + lcSqlFt) 
			
			lcSqlFi	= uf_PAGAMENTO_gravarVendaFi(Ft.Ftstamp, ft.nmdoc, ft.ndoc, ft.tipodoc, ft.fno, myHora, .t.) && ultimo parametro controla que � chamado do backoffice
			lcSqlFi = uf_gerais_trataPlicasSQL(lcSqlFi)
			
			
			&& Cria certifica��o para depois gravar tudo na mesma transa��o - alterado para corrigir BUG de cen�rio em que gravavam vendas simultaneamente e partiam certifica��o.
*!*				IF !empty(alltrim(td.tiposaft))	
*!*					lcSqlCert = uf_pagamento_gravarCert(ft.ftstamp, myInvoiceData, myHora, ft.numinternodoc, ROUND(Ft.Etotal+FT.efinv,2))
*!*					lcSqlCert = uf_gerais_trataPlicasSQL(lcSqlCert)	
*!*				
*!*					lcSQLInsertDoc = lcSqlFt2 + lcSqlFt + lcSqlFi + lcSqlCert 
*!*				ELSE
*!*					lcSQLInsertDoc = lcSqlFt2 + lcSqlFt + lcSqlFi 
*!*				ENDIF
			
			
			&& valida o numero interno do documento
			LOCAL lcNdoc
			lcNdoc=-1
			
			IF(!TYPE("ft.numinternodoc")=="U")
				lcNdoc=ft.numinternodoc
			ENDIF
			
			IF(!TYPE("ft.ndoc")=="U")
				lcNdoc=ft.ndoc
			ENDIF
			
			
		
			lcSqlCompartExterna = uf_facturacao_actualizacaCompartExterna()
			lcSqlCompartExterna = uf_gerais_trataPlicasSQL(lcSqlCompartExterna )		
	
	
			IF TYPE("FACTURACAO")!="U"
				&& Gravar certifica��o para as vendas manuais nao podemos gravar TD-49649
				IF ALLTRIM(UPPER(FACTURACAO.ContainerCab.nmdoc.value))!="VENDA MANUAL"
					lcSqlCert = uf_pagamento_gravarCert(ft.ftstamp, myInvoiceData, myHora, lcNdoc, ROUND(Ft.Etotal+FT.efinv,2))
					lcSqlCert = uf_gerais_trataPlicasSQL(lcSqlCert)		
				ENDIF 
			ENDIF 

			lcSQLInsertDoc = lcSqlFt2 + lcSqlFt + lcSqlFi + lcSqlCert + lcSqlCompartExterna
			
			IF !EMPTY(lcSQLInsertDoc) 

				lcSQL = ''
				TEXT TO lcSQL NOSHOW TEXTMERGE
					exec up_gerais_execSql 'Documentos - Insert', 1,'<<lcSQLInsertDoc>>', '', '', '' , '', ''
				ENDTEXT 
												
				IF !uf_gerais_actGrelha("","",lcSQL)
					uf_perguntalt_chama("OCORREU UMA ANOMALIA A GRAVAR O DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
					**RETURN .f.
					lcValidaInsercaoDocumento = .f.
				ELSE
					&& Tudo Ok
					lcValidaInsercaoDocumento = .t.
				ENDIF

			ENDIF 		
			
			IF lcValidaInsercaoDocumento == .t. 
				
				&& inserir linhas na fi2 em falta
				SELECT ft 
				lcSQL = ''
				TEXT TO lcSQL NOSHOW TEXTMERGE
					insert into fi2 (fistamp, ftstamp, motiseimp, codmotiseimp)
					select 
						fistamp, ftstamp
						,(case when iva=0 then (select motivo_isencao_iva from empresa where no=<<mysite_nr>>) else '' end)
						,(case when iva=0 then (select codmotiseimp from empresa where no=<<mysite_nr>>) else '' end)
					from fi (nolock)
					where ftstamp='<<ALLTRIM(ft.ftstamp)>>' and fistamp not in (select fistamp from fi2 (nolock) where ftstamp='<<ALLTRIM(ft.ftstamp)>>')
				ENDTEXT 
				uf_gerais_actGrelha("","",lcSQL)
				
				
					

            IF USED("FI2")

               uv_update = ''

               SELECT FI2
               GO TOP

               SCAN FOR !EMPTY(fi2.fnstamp)

                     TEXT TO lcSQL TEXTMERGE NOSHOW
                        UPDATE fi2 SET fnstamp = '<<ALLTRIM(fi2.fnstamp)>>' WHERE fi2.fistamp = '<<ALLTRIM(fi2.fistamp)>>' 
                     ENDTEXT

                     uv_update = uv_update + IIF(!EMPTY(uv_update), CHR(13) + CHR(9), '') + lcSQL
                  
                  SELECT FI2
               ENDSCAN

               IF !EMPTY(uv_update)

                  uf_gerais_actGrelha("", "", uv_update)

               ENDIF

            ENDIF
				
				
				&& Caso se trate de uma importa��o de reserva fecha o documento importado (linhas e cabe�alho) 
				IF uf_gerais_getParameter("ADM0000000142","BOOL")
					uf_reservas_abater()
				ENDIF 	
				
				&&fechar vales e cativa��es
				SELECT fi 
				GO TOP 
				SCAN
					IF ALLTRIM(fi.ref)=='V999999' AND !EMPTY(fi.bistamp)
						LOCAL lcnrvale
						STORE '' TO lcnrvale
						LcSql = ""
						Text To lcSQL Textmerge noshow
							select valeNr from bi2 where bi2stamp='<<ALLTRIM(fi.bistamp)>>'
						ENDTEXT
						uf_gerais_actGrelha("","uCrsnrvale",lcSQL)
						IF !EMPTY(uCrsnrvale.valenr)
							lcnrvale = ALLTRIM(uCrsnrvale.valenr)
							LcSql = ""
							Text To lcSQL Textmerge noshow
								update B_fidelvale set abatido=1 where ref='<<ALLTRIM(lcnrvale)>>'
							ENDTEXT
							uf_gerais_actGrelha("","uCrsnrvale",lcSQL)
						ENDIF 				
						fecha("uCrsnrvale")
					ENDIF 
					IF !EMPTY(fi.bistamp)
						LcSql = ""
						Text To lcSQL Textmerge noshow
							select nmdos from bi where bistamp='<<ALLTRIM(fi.bistamp)>>'
						ENDTEXT
						uf_gerais_actGrelha("","uCrsnmdocbo",lcSQL)
						IF ALLTRIM(uCrsnmdocbo.nmdos)=='Encomenda de Cliente'
							SELECT fi 
							LcSql = ""
							Text To lcSQL Textmerge noshow
								update st set cativado=	isnull((select sum(bi.qtt) from bi (nolock)
												INNER JOIN ts (nolock) ON bi.ndos = ts.ndos
												inner join bo (nolock) on bo.bostamp=bi.bostamp
												where ts.cativast=1 and bi.fechada=0 and bo.fechada=0 and bi.armazem=st.site_nr and bi.ref=st.ref),0)
								from st (nolock) where ref = '<<ALLTRIM(fi.ref)>>' and site_nr = <<mysite_nr>>
							ENDTEXT
							uf_gerais_actGrelha("","uCrsnmdocbo",lcSQL)							
						ENDIF 
						fecha("uCrsnmdocbo")
					ENDIF 
					
				ENDSCAN 
	
						
				&& verifica se tem psicotr�picos
				lcValPsiGrava = .f.
				
				SELECT FT
				SELECT fi
				GO TOP
				SCAN
					IF fi.u_psico	
						lcValPsiGrava = .t.
						EXIT
					ENDIF
				ENDSCAN
				
		
				&&guarda dadosPsico
				IF lcValPsiGrava
					SELECT ft
					uf_PAGAMENTO_GravarDadosPsico(ft.ftstamp, 0)
				ENDIF

				&& Grava Receita nos lotes
				Select ft
				IF !ft.cobrado && se nao for suspensa
					SELECT td
					IF !td.tipodoc==3 AND  !td.u_tipodoc==13  AND !td.tipodoc==2 AND !td.u_tipodoc==5 AND !td.u_tipodoc==1 AND !EMPTY(Alltrim(ft2.u_codigo)) && se n�o for factura resumo, nota de cr�dito/debito ou facturas entidades
						Select ft2

							FOR i=1 TO 3
							IF uf_receituario_VerificaLote("BACK", ALLTRIM(ft2.u_codigo), ALLTRIM(ft2.u_codigo2), ALLTRIM(ft.ftstamp), .f., ALLTRIM(FT2.U_RECEITA), ALLTRIM(ft2.token))
								i=3
							ELSE
								IF i == 3
									uf_perguntalt_chama("Ocorreu um problema a registar a venda nos lotes." + CHR(13) + "Por favor contacte o suporte.","OK","",16)
								ENDIF
							ENDIF
						ENDFOR
					ENDIF
				ENDIF 
			
				&& Actualiza Data da �ltima Venda do Cliente 
				SELECT td
				IF !td.tipodoc == 3 AND !td.u_tipodoc == 1 && se n�o for nota de cr�dito ou facturas entidades
					uf_PAGAMENTO_actDataUltVendaCl()
				ENDIF
			
				&& Actualiza ultimo registo alterado 
				SELECT ft
				uf_gerais_gravaUltRegisto(str(myTermNo), LEFT(nrAtendimento,8))
			
				&&
				IF td.tipodoc==1 && Factura Entidade
					uf_FACTENTIDADESCLINICA_actualizaMarcacoes()
				ENDIF 
				
				&& gravar arquivo digital
				IF myFTIntroducao
					* TABELA
					IF myArquivoDigital
						SELECT ft
						uf_arquivoDigital_TaloesLt(ft.ftstamp)
					ENDIF
					*PDF
					IF myArquivoDigitalPDF
						SELECT ft
						uf_arquivodigital_EmitirPdfsTalao('FACTURACAO','')	
					ENDIF
				ENDIF
				**
				regua(2)
			ELSE 				
				regua(2)
				
				** Regista Problema na Tabela de Logs
				Local lcmensagem, lcorigem 
				lcmensagem 	= "Erro de Inser��o na tabela FT; Nmdoc: " + Alltrim(Ft.nmdoc) + "; Fno: " + Alltrim(Str(Ft.fno)) + "; Ftstamp: " + Alltrim(Ft.Ftstamp) + "; User:" + Alltrim(Str(ch_userno))
				lcorigem 	= "Painel Factura��o: uf_gravarFactura"
				
				lcSQL = ""
				TEXT TO lcSQL TEXTMERGE NOSHOW
					Insert Into B_elog (tipo, status, mensagem, origem)
					Values 	('E','A','<<Alltrim(lcmensagem)>>', '<<Alltrim(lcorigem)>>')
				
				ENDTEXT 
				uf_gerais_actGrelha("","",lcSQL)
				
				RETURN .f.
			ENDIF 

		ELSE 
			RETURN .f.
		ENDIF 
	ENDIF 
	
	IF myFtAlteracao = .t. && Altera��o
		
		** Regras (validacao DEM feita aqui)
		lcValida = uf_facturacao_RegrasGravacao()
		If lcValida == .t.
			
			regua(0,100,"A PROCESSAR A GRAVA��O DO DOCUMENTO...",.f.)
		
			lcSqlFt2 = uf_facturacao_actualizaFt2()
			lcSqlFt2 = uf_gerais_trataPlicasSQL(lcSqlFt2) 
			lcSQLFt  = uf_facturacao_actualizaFt()
			lcSqlFt  = uf_gerais_trataPlicasSQL(lcSqlFt) 
			
			lcSqlFi	= uf_facturacao_actualizaFi()
			lcSqlFi = uf_gerais_trataPlicasSQL(lcSqlFi)
			
			lcSQLUpdateDoc = lcSqlFt2 + lcSqlFt + lcSqlFi
			
			IF !EMPTY(lcSQLUpdateDoc) 

				lcSql = ''
				TEXT TO lcSQL NOSHOW TEXTMERGE
					exec up_gerais_execSql 'Backoffice FT - Update', 1,'<<lcSQLUpdateDoc>>', '', '', '' , '', ''
				ENDTEXT 
			
				If !uf_gerais_actGrelha("","",lcSQL)
					uf_perguntalt_chama("OCORREU UM ERRO A GRAVAR O DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
					Return .f.
				ELSE
					&& Tudo Ok
					lcValidaInsercaoDocumento = .t.
				ENDIF

			ENDIF 		
			
			IF lcValidaInsercaoDocumento == .t. 
								
				&& Caso se trate de uma importa��o de reserva fecha o documento importado (linhas e cabe�alho) 
				IF uf_gerais_getParameter("ADM0000000142","BOOL")
					uf_reservas_abater()
				ENDIF 

				&& Grava Dados Psico 
				lcValPsiGrava = .f.
				SELECT fi
				GO TOP
				SCAN
					IF fi.u_psico	
						lcValPsiGrava = .t.
						EXIT
					ENDIF
				ENDSCAN
				IF lcValPsiGrava
					uf_facturacao_actualizaVendaPsico()
				ENDIF

				&& Grava Receita 
				Select ft
				IF !ft.cobrado
					SELECT td
					IF !td.tipodoc==3 AND !td.u_tipodoc==13 AND  !td.u_tipodoc==5 AND !td.u_tipodoc==1 AND !EMPTY(Alltrim(ft2.u_codigo)) && se n�o for factura resumo, nota de cr�dito ou facturas entidades, Factura Profroma
						FOR i=1 TO 3
							IF uf_receituario_VerificaLote("BACK", ft2.u_codigo, ft2.u_codigo2, ft.ftstamp, .f., Alltrim(FT2.U_RECEITA), Alltrim(ft2.token))
								i=3
							ELSE
								IF i == 3
									uf_perguntalt_chama("Ocorreu um problema a registar a venda nos lotes." + CHR(13) + "Por favor contacte o suporte.","OK","",16)
								ENDIF
							ENDIF
						ENDFOR
					ENDIF
				ENDIF
							
				regua(2)
			ELSE 
				regua(2)
				
				&& Regista Problema na Tabela de Logs
				Local lcmensagem, lcorigem
				lcmensagem = "Erro na Actualiza��o da tabela FT; Nmdoc: " + Alltrim(Ft.nmdoc) + "; Fno: " + Alltrim(Str(Ft.fno)) + "; Ftstamp: " + Alltrim(Ft.Ftstamp)  + "; User:" + Alltrim(Str(ch_userno))
				lcorigem = "Painel Factura��o: uf_gravarFactura"
				lcSQL = ""
				TEXT TO lcSQL TEXTMERGE NOSHOW
					Insert Into B_elog (tipo, status, mensagem, origem)
					Values 	('E','A','<<Alltrim(lcmensagem)>>', '<<Alltrim(lcorigem)>>')
				ENDTEXT
				uf_gerais_actGrelha("","",lcSQL)
				
				RETURN .f.
			ENDIF 

		ELSE 
			RETURN .f.
		ENDIF 
	ENDIF 


	&&&& TODO TEMP &&&&
	SELECT fi
	GO TOP
	LOCATE FOR !EMPTY(id_ext_tarv_prescricao) 
	IF FOUND()
		uf_perguntalt_chama("A dispensa TARV foi comunicada com sucesso.","OK","",32)
	ENDIF
	&&&&&&&&&&&&&&&&&&&
		
	
	&& Funcionalidade para imprimir documentos automaticamente
	IF myFtIntroducao = .t. && Introdu��o
		SELECT td
		IF td.imp_automatica == .T.
			uf_imprimirgerais_Chama('FACTURACAO', .t.)
		ENDIF
	ENDIF

	&& Atruibui valores a variaveis publicas
	STORE .f. TO myFtIntroducao, myFtAlteracao 
	
	SELECT TD
	IF TD.u_tipodoc != 3 && inser��o de receita
		uf_facturacao_eventosFtAposGravar() && Eventos Ap�s a Grava��o
	ENDIF
	
	&& Valida��o para envio autom�tico Documentos Transporte
	uf_facturacao_enviaDT()
	
	&& Gravar documento em PDF para site
	IF !empty(uf_gerais_getParameter_site('ADM0000000100', 'text'))
		LOCAL lcexporta
		lcexporta = .f.
		SELECT fi 
		GO TOP 
		SCAN 
			IF !EMPTY(fi.bistamp)	
				TEXT TO lcSQL TEXTMERGE NOSHOW
					select nmdos from bi (nolock) where bistamp='<<ALLTRIM(fi.bistamp)>>'
				ENDTEXT
				uf_gerais_actGrelha("","ucrsbifi",lcSQL)
				SELECT ucrsbifi
				IF ALLTRIM(ucrsbifi.nmdos)=='Encomenda de Cliente'
					lcexporta = .t.
				ENDIF 
				fecha("ucrsbifi")
			ENDIF 
		ENDSCAN 
		IF lcexporta = .t.
			public FTSeguradora 
			FTSeguradora = 'HIST'
			uf_imprimirgerais_Chama('FACTURACAO')
			IMPRIMIRGERAIS.hide
			IMPRIMIRGERAIS.pageframe1.page1.chkParaPDF.value = 1
			uf_imprimirGerais_gravar()
			uf_imprimirGerais_sair()
			lcNomeFicheiroPDF = ALLTRIM(STR(ft.fno))+'_'+ALLTRIM(STR(ft.ndoc))+'_'+ALLTRIM(str(year(ft.fdata)))+ALLTRIM(str(month(ft.fdata)))+ALLTRIM(str(day(ft.fdata)))+'.pdf'
			TEXT TO lcSQL TEXTMERGE NOSHOW
				insert into anexos (anexosstamp, regstamp, tabela, filename, descr, keyword, protected, ousrdata, ousrinis, usrdata, usrinis, tipo, validade)
				select left(newid(),21), '<<ALLTRIM(ft.ftstamp)>>', 'FT', '<<ALLTRIM(lcNomeFicheiroPDF)>>', 'Fatura <<ALLTRIM(lcNomeFicheiroPDF)>>', 'Fatura <<ALLTRIM(lcNomeFicheiroPDF)>>', 0, GETDATE(), 'ousrinis', getdate(), '', 'Fatura', '19000101'
			ENDTEXT
			uf_gerais_actGrelha("","",lcSQL)
		ENDIF 
	ENDIF 
	
	IF !llAtend
		uf_facturacao_chama(Alltrim(Ft.Ftstamp))
	ENDIF
	

	
	&& Impressao do Verso de Receita 
	IF td.u_tipodoc == 3
		IF !EMPTY(ALLTRIM(ft.u_ltstamp)) OR !EMPTY(ALLTRIM(ft.u_ltstamp2))
			IF uf_perguntalt_chama("Pretende imprimir a receita?", "Sim", "N�o")
				IF uCrsE1.ordemImpressaoreceita == .f.
					IF !EMPTY(ALLTRIM(ft.u_ltstamp))
						IF uf_gerais_getParameter_site('ADM0000000136', 'BOOL', mySite)
							uf_imprimirpos_impRecTalaoComum_talaoVerso(Alltrim(Ft.Ftstamp), 1, .F.)
						ELSE
							uf_imprimirpos_imprec(Alltrim(Ft.Ftstamp),1,"REIMP")
						ENDIF 
					ENDIF
					IF !EMPTY(ALLTRIM(ft.u_ltstamp2))
						IF uf_gerais_getParameter_site('ADM0000000136', 'BOOL', mySite)
							uf_imprimirpos_impRecTalaoComum_talaoVerso(Alltrim(Ft.Ftstamp), 2, .F.)
						ELSE 
							uf_imprimirpos_imprec(Alltrim(Ft.Ftstamp),2,"REIMP")
						ENDIF 
					ENDIF
				ELSE
					IF !EMPTY(ALLTRIM(ft.u_ltstamp2))
						IF uf_gerais_getParameter_site('ADM0000000136', 'BOOL', mySite)
							uf_imprimirpos_impRecTalaoComum_talaoVerso(Alltrim(Ft.Ftstamp), 2, .F.)
						ELSE
							uf_imprimirpos_imprec(Alltrim(Ft.Ftstamp),2,"REIMP")
						ENDIF 
					ENDIF
					IF !EMPTY(ALLTRIM(ft.u_ltstamp))
						IF uf_gerais_getParameter_site('ADM0000000136', 'BOOL', mySite)
							uf_imprimirpos_impRecTalaoComum_talaoVerso(Alltrim(Ft.Ftstamp), 1, .F.)
						ELSE
							uf_imprimirpos_imprec(Alltrim(Ft.Ftstamp),1,"REIMP")
						ENDIF 
					ENDIF
				ENDIF
			ENDIF
		ENDIF 
	ENDIF
	
	 IF USED("uCrsVerificaRespostaDEM")
        SELECT uCrsVerificaRespostaDEM
        DELETE ALL
    ENDIF 
    
    IF USED("uCrsVerificaRespostaDEMTotal") 
        SELECT ft2
        DELETE FROM uCrsVerificaRespostaDEMTotal WHERE ALLTRIM(receita_nr)=ALLTRIM(ft2.u_receita) AND !EMPTY(ALLTRIM(ft2.u_receita))
    ENDIF 
    
    IF USED("ucrsDemEfetivada")
        SELECT ucrsDemEfetivada
        DELETE ALL
    ENDIF 
    IF USED("uCrsVerificaRespostaDEMTotalAux")
        SELECT uCrsVerificaRespostaDEMTotalAux
        DELETE ALL
    ENDIF 
    IF USED("uCrsVerificaRespostaDEMAuxiliar")
        SELECT uCrsVerificaRespostaDEMAuxiliar
        DELETE ALL
    ENDIF 
    IF USED("uCrsVerificaRespostaDEM_nd")
        SELECT uCrsVerificaRespostaDEM_nd
        DELETE ALL
    ENDIF 
    IF USED("uCrsDEMndisp")
        SELECT uCrsDEMndisp
        DELETE ALL 
    ENDIF 
    IF USED("uCrsVerificaRespostaDEMAux")
        SELECT uCrsVerificaRespostaDEMAux
        DELETE ALL
    ENDIF 
    IF USED("uCrsVerificaRespostaDEM_tot")
        SELECT uCrsVerificaRespostaDEM_tot
        DELETE ALL
    ENDIF 
    

    
    
    IF USED("ucrsReceitaEfetivar")
        SELECT ucrsReceitaEfetivar
        DELETE ALL
    ENDIF 
    IF USED("ucrsDadosValidacaoEfetivarDEM")
        SELECT ucrsDadosValidacaoEfetivarDEM
        DELETE ALL
    ENDIF 
    IF USED("ucrsReceitaEfetivarAp")
        SELECT ucrsReceitaEfetivarAp
        DELETE ALL
    ENDIF 
    IF USED("ucrsReceitaValidar")
        SELECT ucrsReceitaValidar
        DELETE ALL
    ENDIF 
    IF USED("ucrsReceitaValidarAux")
        SELECT ucrsReceitaValidarAux
        DELETE ALL
    ENDIF 
    IF USED("ucrsReceitaValidarLinhas")
        SELECT ucrsReceitaValidarLinhas
        DELETE ALL
    ENDIF 
    IF USED("ucrsDadosValidacaoDEM")
        SELECT ucrsDadosValidacaoDEM
        DELETE ALL
    ENDIF 
    IF USED("ucrsDadosValidacaoDEMLinhas")
        SELECT ucrsDadosValidacaoDEMLinhas
        DELETE ALL
    ENDIF 
    IF USED("uCrsreceitas")
        SELECT uCrsreceitas
        DELETE ALL
    ENDIF 
    IF USED("ucrsReceitaValidar")
        SELECT ucrsReceitaValidar
        DELETE ALL
    ENDIF 
    IF USED("uCrsReceitaValidarAp")
        SELECT uCrsReceitaValidarAp
        DELETE ALL
     ENDIF 

	
	RETURN .t.

ENDFUNC


**
FUNCTION uf_facturacao_enviaDT
	IF !USED("TD")
		RETURN .f.
	ENDIF 

	&&Se for Guia de Transporte tentar enviar via webservice
	SELECT TD
	IF UPPER(ALLTRIM(TD.tiposaft)) == "GT"
		
		IF !uf_perguntalt_chama("Pretende fazer a comunica��o de trasporte � Autoridade Tribut�ria e Aduaneira (AT)?","Sim","N�o",64)
 			RETURN .f.
		ENDIF
		
		IF EMPTY(uf_gerais_getParameter("ADM0000000205","TEXT")) OR  EMPTY(uf_gerais_getParameter("ADM0000000206","TEXT"))
			uf_perguntalt_chama("Dados de configura��o para envio de Guias de Transporte n�o definidas. Contacte o suporte.","OK","",32)
			RETURN .f.
		ENDIF
	
		SELECT FT2
		IF EMPTY(ALLTRIM(ft2.subproc))
			regua(0,15,"A enviar Documento para a AT...")
			regua(1,1,"A enviar Documento para a AT...")
			*internet
			IF !uf_gerais_verifyInternet()
				uf_perguntalt_chama("ESTA FUNCIONALIDADE REQUER LIGA��O � INTERNET."+CHR(13)+"Por favor valide.","OK","",64)
				regua(2)
				RETURN .f.
			ENDIF
			*****************
			regua(1,2,"A enviar Documento para a AT...")
			facturacao.stampTmrDT = uf_gerais_stamp()
			
			SELECT FT
			lcSQL = ''
			TEXT TO lcSQL TEXTMERGE NOSHOW
				INSERT INTO b_DT_resposta
				(
					stamp
					,stampDoc
				)
				values
				(
					'<<ALLTRIM(facturacao.stampTmrDT)>>'
					,'<<ALLTRIM(ft.ftstamp)>>'
				)
			ENDTEXT 
			IF !uf_gerais_actGrelha("","",lcSQL)
				regua(2)
			ENDIF 
			
			regua(1,3,"A enviar Documento para a AT...")
			IF DIRECTORY(ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\DT')
				SELECT FT
			
				**lcWsPath = ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\DT\DT "' + ALLTRIM(facturacao.stampTmrDT) + '" "' + ALLTRIM(sql_db) + '" "' + ALLTRIM(ft.ftstamp) + '"'
				lcWsPath = ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\DT\ATDocTransporte.jar "' + ALLTRIM(facturacao.stampTmrDT) + '" "' + ALLTRIM(sql_db) + '" "' + ALLTRIM(ft.ftstamp) + '" "' + ALLTRIM(mySite) + '"'
				
				**_cliptext = lcWsPath
				oWSShell = CREATEOBJECT("WScript.Shell")
				oWSShell.Run(lcWsPath,1,.f.)
				regua(1,4,"A enviar Documento para a AT...")
				facturacao.tmrDT.enabled = .t.
			ENDIF
		ENDIF 
	ENDIF
ENDFUNC


**
FUNCTION uf_facturacao_tmrDT
	facturacao.ntmrDT = facturacao.ntmrDT + 1
	IF facturacao.ntmrDT > 10 && 10 tentativas de 3 segundos
		regua(2)
		uf_perguntalt_chama("N�o foi poss�vel obter resposta do WebService. Por favor volte a tentar mais tarde.","OK","",16)
		facturacao.tmrDT.enabled = .f.
		facturacao.ntmrDT = 0
		facturacao.stampTmrDT = ''
		RETURN .f.
	ENDIF
	regua(1,facturacao.ntmrDT + 2,"A enviar Documento para a AT...")
	TEXT TO lcSql NOSHOW textmerge
		SELECT 
			codResposta
			,descrResposta
			,ATDocCode
		from 
			b_DT_resposta (nolock)
		where
			stamp = '<<ALLTRIM(facturacao.stampTmrDT)>>'
	ENDTEXT
	IF !uf_gerais_actGrelha("","uCrsVerificaResposta",lcSql)
		regua(2)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA A VERIFICAR A RESPOSTA DO WEB SERVICE. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		facturacao.tmrDT.enabled = .f.
		facturacao.ntmrDT = 0
		facturacao.stampTmrDT = ''
		RETURN .f.
	ELSE
	
		SELECT uCrsVerificaResposta
		IF RECCOUNT("uCrsVerificaResposta") > 0 AND !EMPTY(ALLTRIM(uCrsVerificaResposta.CodResposta))
			SELECT uCrsVerificaResposta
			GO TOP
			
			*Verifica se n�o existe codigo de anomalia
			IF ALLTRIM(uCrsVerificaResposta.CodResposta) != "0" AND ALLTRIM(uCrsVerificaResposta.CodResposta) != "-100"
				regua(2)
				uf_perguntalt_chama("N�o foi possivel enviar o documento." + CHR(13) + "Motivo: "+ UPPER(ALLTRIM(uCrsVerificaResposta.DescrResposta)),"OK","",64)

				facturacao.tmrDT.enabled = .f.
				facturacao.ntmrDT = 0
				facturacao.stampTmrDT = ''
				RETURN .f.
			ENDIF
			
			IF ALLTRIM(uCrsVerificaResposta.CodResposta) == "-100"
				regua(2)
				uf_perguntalt_chama(UPPER(ALLTRIM(uCrsVerificaResposta.DescrResposta)),"OK","",64)
				
				SELECT ft2
				lcSQL=''
				TEXT TO lcSQL TEXTMERGE NOSHOW
					UPDATE ft2 
					SET subproc = '-100' 
					WHERE ft2.ft2stamp = '<<ALLTRIM(ft2.ft2stamp)>>'
				ENDTEXT 
				uf_gerais_actGrelha("","",lcSQL)
				
				select ft2
				replace ft2.subproc WITH '-100'
				
				facturacao.tmrDT.enabled = .f.
				facturacao.ntmrDT = 0
				facturacao.stampTmrDT = ''
				RETURN .f.
			ENDIF
			
			SELECT uCrsVerificaResposta
			SELECT ft2
			lcSQL=''
			TEXT TO lcSQL TEXTMERGE NOSHOW
				UPDATE ft2 
				SET subproc = '<<ALLTRIM(uCrsVerificaResposta.ATDocCode)>>' 
				WHERE ft2.ft2stamp = '<<ALLTRIM(ft2.ft2stamp)>>'
			ENDTEXT 
			uf_gerais_actGrelha("","",lcSQL)
			
			select ft2
			replace ft2.subproc WITH ALLTRIM(uCrsVerificaResposta.ATDocCode)
			
			regua(2)
			uf_perguntalt_chama("DOCUMENTO ENVIADO COM SUCESSO.","OK","",64)
			facturacao.tmrDT.enabled = .f.
			facturacao.ntmrDT = 0
			facturacao.stampTmrDT = ''
		ENDIF
	ENDIF 
ENDFUNC


**Eventos apos Grava��o
FUNCTION uf_facturacao_eventosFtAposGravar
	uf_facturacao_actualizaQuantidadeMovimentadaFact()
	
	** Fecho do documento de origem
	SELECT Fi
	LOCATE FOR !EMPTY(fi.bistamp)
	IF FOUND()
		uf_fechoautomaticodoc_chama("FT")
	ENDIF 	
	
ENDFUNC 


** Actualiza Quantidade Movimentada - isto tem de passar para trigger Lu�s Leal 
FUNCTION uf_facturacao_actualizaQuantidadeMovimentadaFact
	LOCAL lcSql
	
	SELECT Fi
	GO TOP 
	SCAN
		&& Cen�rio armazem - import confim. entrega cliente - tem de passar para trigger
*!*			IF !Empty(Fi.bistamp) 
*!*				lcSQL = ""
*!*				Text to lcSql noshow textmerge
*!*					Update 	
*!*						Bi 
*!*					Set 	
*!*						Pbruto = <<Fi.qtt>>
*!*					Where 	
*!*						bistamp = '<<Alltrim(Fi.bistamp)>>'
*!*				ENDTEXT 
*!*				If !uf_gerais_actGrelha("","",lcSQL)
*!*					uf_perguntalt_chama("N�O FOI POSSIVEL ACTUALIZAR A QUANTIDADE MOVIMENTADA DO DOCUMENTO DE ORIGEM.","OK","",16)
*!*					RETURN .f.
*!*				Endif
*!*			ENDIF 
		IF !Empty(Fi.ofistamp)
			lcSQL = ""
			Text to lcSql noshow textmerge
				UPDATE
					Fi
				SET
					Pbruto = <<Fi.qtt>>
				Where
					Fistamp = '<<Alltrim(Fi.ofistamp)>>'
			ENDTEXT
			IF !uf_gerais_actGrelha("","",lcSQL)
				uf_perguntalt_chama("N�O FOI POSS�VEL ACTUALIZAR A QUANTIDADE MOVIMENTADA DO DOCUMENTO DE ORIGEM.","OK","",16)
				RETURN .f.
			ENDIF
		ENDIF
		
		IF !EMPTY(fi.id_ext_tarv_prescricao)
			lcSQL = ""
			Text to lcSql noshow textmerge
				UPDATE
					ext_tarv_prescricao
				SET
					qt_dispensada = qt_dispensada + <<Fi.qtt>>
				Where
					id = '<<Alltrim(fi.id_ext_tarv_prescricao)>>'
			ENDTEXT
			IF !uf_gerais_actGrelha("","",lcSQL)
				uf_perguntalt_chama("N�O FOI POSS�VEL ACTUALIZAR A QUANTIDADE MOVIMENTADA DO DOCUMENTO DE ORIGEM.","OK","",16)
				RETURN .f.
			ENDIF
		ENDIF
	ENDSCAN

	&& Caso de trate de uma Nota de Cr�dito feita com Faturas Acordo, atualiza o campo pbruto na fi de forma a permitir que a fatura acordo seja importada novamente para uma fatura Resumo - Lu�s Leal 2015-10-05
	SELECT td
	IF td.u_tipodoc == 7
	
		if(USED("uCrsAtendCl"))
			SELECT uCrsAtendCl
			IF ALLTRIM(uCrsAtendCl.modofact) == ("Factura��o Acordo")
				SELECT fi
				GO TOP
				SCAN
					IF !EMPTY(fi.ofistamp)
						lcSQL = ""
						TEXT TO lcSQL NOSHOW TEXTMERGE 
							UPDATE
								fi
							set
								fi.pbruto = 0
							where
								fistamp = (select FRFI.ofistamp from fi FRFI inner join fi NCFI on FRFI.fistamp = NCFI.fistamp where NCFI.fistamp = '<<Alltrim(Fi.ofistamp)>>')
							AND
								fi.nmdoc = 'Factura Acordo'
						ENDTEXT
						IF !uf_gerais_actGrelha("","",lcSQL)
							uf_perguntalt_chama("N�O FOI POSS�VEL ACTUALIZAR A QUANTIDADE MOVIMENTADA DO DOCUMENTO DE ORIGEM. [F.A]","OK","",16)
							RETURN .f.
						ENDIF
				
					ENDIF
				ENDSCAN
			ENDIF
		ENDIF
		
	ENDIF

ENDFUNC 


** Altera��o da Combo que controla o Documento a Editar/Criar
FUNCTION u_facturacao_seltipodocFact
	PUBLIC myFtAlteracao, myFtIntroducao
	
	** VERIFICAR SE J� EXISTEM LINHAS **
	Local lcCntLin
	Store 0 To lcCntLin
	
	Select fi
	Calculate Count(fi.qtt) To lcCntLin For (fi.qtt!=0 Or !Empty(fi.ref) Or !Empty(fi.design) Or fi.etiliquido!=0)
	
	If lcCntLin>0
		If uf_perguntalt_chama("ATEN��O: SE ALTERAR O TIPO DE DOCUMENTO VAI PERDER TODAS AS ALTERA��ES �S LINHAS. PRETENDE CONTINUAR?","Sim","N�o")
			Select fi
			GO Top
			SCAN 
				DELETE
			ENDSCAN 
		Else
			Select FI
			GO TOP
			Facturacao.containercab.nmdoc.VALUE = Alltrim(FI.NMDOC)
			RETURN .f.
		Endif
	Endif

	******************************
	uf_facturacao_limpaNomeFact()
	uf_facturacao_valoresDefeitoFact()
	uf_facturacao_controlaNumeroFact(year(datetime()+(difhoraria*3600)))
	uf_atendimento_fiiliq() &&actualiza totais das linhas
	uf_atendimento_actTotaisFt() &&actualiza totais do cabe�alho
	uf_Facturacao_controlaWkLinhas()
	uf_Facturacao_controlaObjectos()
	uf_facturacao_criaLinhaDefault()
	
ENDPROC


**Verifica se existe linhas, caso n�o exista cria
Function uf_facturacao_criaLinhaDefault
	Local lcNumLinhas
	Store 0 To lcNumLinhas
	Select Fi
	Scan
		lcNumLinhas = lcNumLinhas +1
	Endscan
	
	If lcNumLinhas == 0
		uf_atendimento_AdicionaLinhaFi(.t.,.t.)
	Endif
ENDFUNC 


** Coloca campos limpos Cliente devido � mudan�a de Documento
FUNCTION uf_facturacao_limpaNomeFact
	IF !USED("Ft")
		RETURN .f.
	ENDIF
	
	Select Ft
	Replace	;
		Ft.Nome	With '' ;
		Ft.nome2	With '' ;
		Ft.No 		With 0 ;
		Ft.estab 	With 0 ;
		Ft.Morada	With '' ;
		Ft.Local 	With ''	;
		Ft.codpost	With '' ;
		Ft.ncont 	With '' ;
		Ft.tipo		With '' ;
		Ft.telefone	With '' 
Endfunc	


** Valores por Defeito cabe�alho
Function uf_facturacao_valoresDefeitoFact
	Local lcSQL
	
	LOCAL lcStamp
	lcStamp = uf_gerais_stamp()
	 
	Select Ft
	replace ;
		Ft.Ftstamp 	With 	lcStamp ;
		Ft.fdata 	With 	datetime()+(difhoraria*3600) ;
		Ft.bidata 	With 	datetime()+(difhoraria*3600) ;
		Ft.Ftano	With	Year(datetime()+(difhoraria*3600)) ;
		Ft.Memissao	With	uf_gerais_getParameter("ADM0000000260","text") ;
		Ft.pdata 	With 	datetime()+(difhoraria*3600) ; 
		Ft.cdata	With 	datetime()+(difhoraria*3600) ;
		Ft.nmdoc 	WITH 	ALLTRIM(Facturacao.containercab.nmdoc.VALUE)
		
	*Referencia Interna nas Linhas
	*Cambio fixo
	Replace	Ft.cambiofixo With .f.

	IF USED("TD")
		fecha("TD")
	ENDIF 
	SELECT FT
	SELECT * FROM uCrsGeralTD WHERE UPPER(ALLTRIM(uCrsGeralTD.nmdoc)) == UPPER(ALLTRIM(Facturacao.containercab.nmdoc.VALUE)) AND ALLTRIM(uCrsGeralTD.site) == ALLTRIM(mySite) INTO CURSOR TD READWRITE
	
	Select TD 
	Replace	;
		Ft.nmdoc 	With 	Alltrim(Td.nmdoc) ;
		Ft.ndoc		With	Td.ndoc ;
		Ft.tipodoc	With	Td.tipodoc

*!*		SELECT td
*!*		SELECT Ft
*!*		IF td.u_tipodoc != 8 
*!*			Replace ft.localTesouraria WITH ""
*!*			Replace ft.id_tesouraria_conta WITH 0
*!*		ELSE && Venda a Dinheiro
*!*			Replace ft.localTesouraria WITH "Conta Geral Recebimento"
*!*			Replace ft.id_tesouraria_conta WITH 99
*!*		ENDIF 
	
	&& comentado para n�o serem criadas Inconsist�ncias na exporta�ao para cont	
	IF td.u_tipodoc == 1 OR td.u_tipodoc == 2 OR td.u_tipodoc == 5 OR td.u_tipodoc == 6 OR td.u_tipodoc == 7 OR td.u_tipodoc ==8 OR td.u_tipodoc == 9 OR td.u_tipodoc == 11 OR td.u_tipodoc == 12
		Replace ft.localTesouraria WITH "Conta Geral Recebimento"
		Replace ft.id_tesouraria_conta WITH 99
	ELSE
		Replace ft.localTesouraria WITH ""
		Replace ft.id_tesouraria_conta WITH 0
	ENDIF
	
	* Taxas de Iva
	Select Ft
	Replace ;
		Ft.Ivatx1 	With	uf_gerais_getTabelaIVA(1,"TAXA") ;
		Ft.Ivatx2 	With	uf_gerais_getTabelaIVA(2,"TAXA") ;
		Ft.Ivatx3 	With	uf_gerais_getTabelaIVA(3,"TAXA") ;
		Ft.Ivatx4 	With	uf_gerais_getTabelaIVA(4,"TAXA") ;
		Ft.Ivatx5 	With	uf_gerais_getTabelaIVA(5,"TAXA") ;
		Ft.Ivatx6 	With	uf_gerais_getTabelaIVA(6,"TAXA") ;
		Ft.Ivatx7 	With	uf_gerais_getTabelaIVA(7,"TAXA") ;
		Ft.Ivatx8 	With	uf_gerais_getTabelaIVA(8,"TAXA") ;
		Ft.Ivatx9 	With	uf_gerais_getTabelaIVA(9,"TAXA") ;
		Ft.Ivatx10 	With	uf_gerais_getTabelaIVA(10,"TAXA") ;
		Ft.Ivatx11 	With	uf_gerais_getTabelaIVA(11,"TAXA") ;
		Ft.Ivatx12 	With	uf_gerais_getTabelaIVA(12,"TAXA") ;
		Ft.Ivatx13 	With	uf_gerais_getTabelaIVA(13,"TAXA") 
	
	Select Ft2
	Replace Ft2.Ft2Stamp 	With 	Ft.Ftstamp
	
	StampLinDEM=''
	
ENDFUNC 


** Coloca N�mero de Documento
Function uf_facturacao_controlaNumeroFact
	LPARAMETERS lcAno
	
	Local lcSQL
	
	**Verifica Tipo Documento
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_facturacao_numDoc <<Ft.ndoc>>, <<lcAno>>, '<<ALLTRIM(mySite)>>'
	Endtext
	If !uf_gerais_actGrelha("","uc_numdocFact",lcSQL)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA NO C�LCULO DO N�MERO DO DOCUMENTO.","OK","",16)
		Return
	Endif
	
	Select FT
	Replace Ft.fno With uc_numdocFact.numdoc 
	IF USED("uc_numdocFact")
		fecha("uc_numdocFact")
	ENDIF
ENDFUNC


**
FUNCTION uf_facturacao_NomeInteractiveChange()
	PUBLIC mykeypressed 

	Store 0 To lcTamanhoAntigo,lcNovoTamanho

	lcValorAntigo = Facturacao.ContainerCab.nome.value
	lcTamanhoAntigo =Len(Alltrim(Facturacao.ContainerCab.nome.value))

	IF ALLTRIM(Facturacao.ContainerCab.nome.value)==''
		lcsql = "SELECT TOP 1 convert(nvarchar(250),NOME) as NOME FROM b_utentes (nolock) WHERE NOME LIKE '" + Alltrim(lcValorAntigo) + "' and b_utentes.inactivo = 0 ORDER BY NOME ASC"
	ELSE
		lcsql = "SELECT TOP 1 convert(nvarchar(250),NOME) as NOME FROM b_utentes (nolock) WHERE NOME LIKE '" + Alltrim(lcValorAntigo) + "%' and b_utentes.inactivo = 0 ORDER BY NOME ASC"
	ENDIF
	If uf_gerais_actGrelha("","uCrsTempSQLAutoComp",lcSql)
		If Reccount("uCrsTempSQLAutoComp") > 0
			Select uCrsTempSQLAutoComp
			lcNovoValor =Alltrim(uCrsTempSQLAutoComp.NOME)
			lcNovoTamanho = Len(Alltrim(uCrsTempSQLAutoComp.NOME))

			If mykeypressed != 127 AND mykeypressed != 7 AND mykeypressed != 32
				Facturacao.ContainerCab.nome.value = Alltrim(uCrsTempSQLAutoComp.NOME)
				lc_diftamanhos = ABS(lcNovoTamanho - lcTamanhoAntigo)

				If  !EMPTY(lcNovoValor) 
					Facturacao.ContainerCab.nome.selstart = lcTamanhoAntigo 
					Facturacao.ContainerCab.nome.sellength = lc_diftamanhos 
				Endif
			Endif
		Endif
	ENDIF
ENDFUNC		


**
FUNCTION uf_facturacao_SugereCriarCliente
	Local lcNome, lcSql, lcMoeda, lcValidaCl
	
	STORE .f. TO lcValidaCl
	
	IF !USED("FT")
		RETURN .f.
	ENDIF
	
	SELECT FT
	lcNome = Upper(Alltrim(FT.nome))
	If Empty(lcnome) 
		uf_facturacao_limpaNomeFact()
	ELSE
		lcSQL = ""
		TEXT TO lcSQL TEXTMERGE NOSHOW
			exec up_facturacao_existeCl '<<Alltrim(lcNome)>>'
		ENDTEXT 
		If uf_gerais_actGrelha("","uCrsAtendCL",lcSQL) 
			IF Reccount("uCrsAtendCL")=0 
				If uf_perguntalt_chama('O CLIENTE N�O EXISTE... QUER INTRODUZIR CRIAR UM NOVO CLIENTE?',"Sim","N�o")						
					
					&& Valida se o ecra esta em modo de consulta
					IF !type("UTENTES")=="U" AND (myClIntroducao OR myClAlteracao)
						uf_perguntalt_chama("N�O � POSS�VEL REALIZAR ESTA A��O PORQUE O ECR� DE CLIENTES EST� EM MODO DE EDI��O. POR FAVOR TERMINE ESTA OPERA��O E TENTE NOVAMENTE","OK","",48)		
					ELSE
				
						uf_utentes_novo()
						utentes.AlwaysOnTop = .t.
						
						SELECT cl
						Replace CL.NOME With lcNome
						utentes.refresh	

					ENDIF
					uf_facturacao_limpaNomeFact()
				ELSE
					uf_facturacao_limpaNomeFact()
				ENDIF
			ENDIF 
		ENDIF
	ENDIF
	*Facturacao.ContainerCab.refresh
ENDFUNC


* Corre na Escolha do Fornecedor/Cliente/Entidade 
FUNCTION uf_facturacao_escolheCliente
	**Valida estado de Altera��io ou Introdu��o
	IF myFtIntroducao Or myFtAlteracao
		uf_pesqUtentes_Chama("FACTURACAO")
	ENDIF
ENDFUNC 


**
FUNCTION uf_facturacao_ControlaEventosLinhas
	FOR i=1 TO Facturacao.PageFrame1.Page1.gridDoc.columnCount
		
		WITH Facturacao.PageFrame1.Page1.gridDoc
			*Eventos nas Linhas		
			If Upper(.Columns(i).ControlSource)=="FI.REF"
				BindEvent(.columns(i).text1,"InteractiveChange",Facturacao.oCust,"proc_ActivaEventosLinhasFact")
				BindEvent(.columns(i).text1,"dblClick",Facturacao.oCust,"proc_navegast")
				*AutoComplete
				BindEvent(.columns(i).text1,"KeyPress",Facturacao.oCust,"proc_ApagarAutocompleteRefFi")
				BindEvent(.columns(i).text1,"InteractiveChange",Facturacao.oCust,"proc_autocompleteRefFi")
				BindEvent(.columns(i).text1,"RightClick",Facturacao.oCust,"proc_pesquisarArtigosRefFi")
			Endif
			If Upper(.Columns(i).ControlSource)=="FI.DESIGN"
				BindEvent(.columns(i).text1,"KeyPress",Facturacao.oCust,"proc_ApagarAutocompleteDesignFi")
				BindEvent(.columns(i).text1,"InteractiveChange",Facturacao.oCust,"proc_autocompleteDesignFi")
				BindEvent(.columns(i).text1,"InteractiveChange",Facturacao.oCust,"proc_ActivaEventosLinhasFact")
				BindEvent(.columns(i).header1,"Click",Facturacao.oCust,"proc_OrdenaPorDesignDoc")
			Endif
			If Upper(.Columns(i).ControlSource)=="FI.U_DIPLOMA"
				BindEvent(.columns(i).text1,"RightClick",Facturacao.oCust,"proc_EscolheDiploma")
				BindEvent(.columns(i).text1,"DblClick",Facturacao.oCust,"proc_EscolheDiploma")
			ENDIF
			If Upper(.Columns(i).ControlSource)=="FI.LOTE"
				BindEvent(.columns(i).text1,"Click",Facturacao.oCust,"proc_clickLOTE")
				BindEvent(.columns(i).text1,"gotFocus",Facturacao.oCust,"proc_gotFocusLOTE")
			ENDIF		
			If Upper(.Columns(i).ControlSource)=="FI.IVA"
				BindEvent(.columns(i).text1,"Click",Facturacao.oCust,"proc_clickIVA")
			ENDIF		
			If Upper(.Columns(i).ControlSource)=="FI.QTT"
				BindEvent(.columns(i).text1,"InteractiveChange",Facturacao.oCust,"proc_ActivaEventosLinhasFact")
			Endif
			If Upper(.Columns(i).ControlSource)=="FI.DESCONTO"
				BindEvent(.columns(i).text1,"InteractiveChange",Facturacao.oCust,"proc_ActivaEventosLinhasFact")
			ENDIF
			If Upper(.Columns(i).ControlSource)=="FI.U_DESCVAL"
				BindEvent(.columns(i).text1,"InteractiveChange",Facturacao.oCust,"proc_ActivaEventosLinhasFact")
			Endif			
			If Upper(.Columns(i).ControlSource)=="FI.DESC2"
				BindEvent(.columns(i).text1,"InteractiveChange",Facturacao.oCust,"proc_ActivaEventosLinhasFact")
			Endif
			If Upper(.Columns(i).ControlSource)=="FI.DESC3"
				BindEvent(.columns(i).text1,"InteractiveChange",Facturacao.oCust,"proc_ActivaEventosLinhasFact")
			Endif
			If Upper(.Columns(i).ControlSource)=="FI.DESC4"
				BindEvent(.columns(i).text1,"InteractiveChange",Facturacao.oCust,"proc_ActivaEventosLinhasFact")
			Endif
			*Evento Valor unit�rio
			If Upper(.Columns(i).ControlSource)=="FI.EPV"
				BindEvent(.columns(i).text1,"InteractiveChange",Facturacao.oCust,"proc_ActivaEventosLinhasFact")
			Endif
			*Evento Total da Linha 
			If Upper(.Columns(i).ControlSource)=="FI.ETILIQUIDO"
				BindEvent(.columns(i).text1,"InteractiveChange",Facturacao.oCust,"proc_ActivaEventosLinhasFact")
			ENDIF
			If Upper(.Columns(i).ControlSource)=="FI.ARMAZEM"
				BindEvent(.columns(i).text1,"InteractiveChange",Facturacao.oCust,"proc_ActivaEventosLinhasFact")
			ENDIF
			*Evento Painel e PVPS
			If Upper(.Columns(i).ControlSource)=="FI.U_EPVP"
				BindEvent(.columns(i).text1,"DblClick",Facturacao.oCust,"proc_PainelPVPS")
				BindEvent(.columns(i).text1,"RightClick",Facturacao.oCust,"proc_PainelPVPS")
			ENDIF
			If Upper(.Columns(i).ControlSource)=="FI.IVAINCL"
				BindEvent(.columns(i).check1,"Click",Facturacao.oCust,"proc_actualizaTotaisFIIvaIncl")
			ENDIF
			**EVENTO CCUSTO
			If Upper(.Columns(i).ControlSource)=="FI.FICCUSTO" 
				BindEvent(.columns(i).text1,"Click",Facturacao.oCust,"proc_clickCCUSTO")
			ENDIF	
			
			**Motivo de isen��o de IVA
			If Upper(.Columns(i).ControlSource)=="FI.CODMOTISEIMP"
				BindEvent(.columns(i).text1,"Click",Facturacao.oCust,"proc_codmotiseimp")
			ENDIF
			
		ENDWITH
	ENDFOR
ENDFUNC


**Activa / Desactiva Eventos Linhas
FUNCTION uf_facturacao_EventosGridDoc
	WITH Facturacao.PageFrame1.Page1.gridDoc
		FOR i=1 TO .columnCount
			If Upper(.Columns(i).ControlSource)=="FI.REF"
				BindEvent(.columns(i).text1,"LostFocus",Facturacao.oCust,"proc_eventoRefFi")
			Endif
			If Upper(.Columns(i).ControlSource)=="FI.DESIGN"
				BindEvent(.columns(i).text1,"LostFocus",Facturacao.oCust,"proc_pesquisarArtigosDesignFi")
			Endif			
			If Upper(.Columns(i).ControlSource)=="FI.QTT"
				BindEvent(.columns(i).text1,"LostFocus",Facturacao.oCust,"proc_actualizaTotaisFI")
			Endif
			If Upper(.Columns(i).ControlSource)=="FI.U_DESCVAL"
				BindEvent(.columns(i).text1,"LostFocus",Facturacao.oCust,"proc_actualizaTotaisFI")
			Endif	
			If Upper(.Columns(i).ControlSource)=="FI.U_DESCVAL"
				BindEvent(.columns(i).text1,"InteractiveChange",Facturacao.oCust,"proc_validaExistenciaValesDesconto")
			Endif	
			If Upper(.Columns(i).ControlSource)=="FI.DESCONTO"
				BindEvent(.columns(i).text1,"LostFocus",Facturacao.oCust,"proc_actualizaTotaisFI")
			Endif
			If Upper(.Columns(i).ControlSource)=="FI.DESC2"
				BindEvent(.columns(i).text1,"LostFocus",Facturacao.oCust,"proc_actualizaTotaisFI")
			Endif
			If Upper(.Columns(i).ControlSource)=="FI.DESC3"
				BindEvent(.columns(i).text1,"LostFocus",Facturacao.oCust,"proc_actualizaTotaisFI")
			Endif
			If Upper(.Columns(i).ControlSource)=="FI.DESC4"
				BindEvent(.columns(i).text1,"LostFocus",Facturacao.oCust,"proc_actualizaTotaisFI")
			Endif
			If Upper(.Columns(i).ControlSource)=="FI.EPV"
				BindEvent(.columns(i).text1,"LostFocus",Facturacao.oCust,"proc_actualizaTotaisFI")
			ENDIF
			If Upper(.Columns(i).ControlSource)=="FI.U_EPVP"
				BindEvent(.columns(i).text1,"LostFocus",Facturacao.oCust,"proc_actualizaTotaisFI")
			ENDIF
			If Upper(.Columns(i).ControlSource)=="FI.IVAINCL"
				BindEvent(.columns(i).check1,"Click",Facturacao.oCust,"proc_actualizaTotaisFIIvaIncl")
			Endif	
			If Upper(.Columns(i).ControlSource)=="FI.ETILIQUIDO"
				BindEvent(.columns(i).text1,"LostFocus",Facturacao.oCust,"proc_validaAlteracaoTotalLinha")
			ENDIF
			If Upper(.Columns(i).ControlSource)=="FI.ARMAZEM"
				BindEvent(.columns(i).text1,"LostFocus",Facturacao.oCust,"proc_validaAlteracaoArmazem")
			Endif
		ENDFOR
	ENDWITH 
ENDFUNC 


**
FUNCTION uf_facturacao_DesactivaEventosLinhasFact

	WITH Facturacao.PageFrame1.Page1.gridDoc
		FOR i=1 TO .columnCount
			If Upper(.Columns(i).ControlSource)=="FI.REF"
				unBindEvent(.columns(i).text1,"LostFocus",Facturacao.oCust,"proc_eventoRefFi")
			Endif
			If Upper(.Columns(i).ControlSource)=="FI.DESIGN"
				unBindEvent(.columns(i).text1,"LostFocus",Facturacao.oCust,"proc_pesquisarArtigosDesignFi")
			Endif			
			If Upper(.Columns(i).ControlSource)=="FI.QTT"
				unBindEvent(.columns(i).text1,"LostFocus",Facturacao.oCust,"proc_actualizaTotaisFI")
			Endif
			If Upper(.Columns(i).ControlSource)=="FI.U_DESCVAL"
				unBindEvent(.columns(i).text1,"LostFocus",Facturacao.oCust,"proc_actualizaTotaisFI")
			Endif	
			If Upper(.Columns(i).ControlSource)=="FI.U_DESCVAL"
				unBindEvent(.columns(i).text1,"InteractiveChange",Facturacao.oCust,"proc_validaExistenciaValesDesconto")
			Endif	
			If Upper(.Columns(i).ControlSource)=="FI.DESCONTO"
				unBindEvent(.columns(i).text1,"LostFocus",Facturacao.oCust,"proc_actualizaTotaisFI")
			Endif
			If Upper(.Columns(i).ControlSource)=="FI.DESC2"
				unBindEvent(.columns(i).text1,"LostFocus",Facturacao.oCust,"proc_actualizaTotaisFI")
			Endif
			If Upper(.Columns(i).ControlSource)=="FI.DESC3"
				unBindEvent(.columns(i).text1,"LostFocus",Facturacao.oCust,"proc_actualizaTotaisFI")
			Endif
			If Upper(.Columns(i).ControlSource)=="FI.DESC4"
				unBindEvent(.columns(i).text1,"LostFocus",Facturacao.oCust,"proc_actualizaTotaisFI")
			Endif
			If Upper(.Columns(i).ControlSource)=="FI.EPV"
				unBindEvent(.columns(i).text1,"LostFocus",Facturacao.oCust,"proc_actualizaTotaisFI")
			Endif
			If Upper(.Columns(i).ControlSource)=="FI.ETILIQUIDO"
				unBindEvent(.columns(i).text1,"LostFocus",Facturacao.oCust,"proc_validaAlteracaoTotalLinha")
			ENDIF
			If Upper(.Columns(i).ControlSource)=="FI.ARMAZEM"
				BindEvent(.columns(i).text1,"LostFocus",Facturacao.oCust,"proc_validaAlteracaoArmazem")
			Endif
*!*				If Upper(.Columns(i).ControlSource)=="FI.IVAINCL"
*!*					unBindEvent(.columns(i).check1,"Click",Facturacao.oCust,"proc_actualizaTotaisFIIvaIncl")
*!*				Endif	
		ENDFOR
	ENDWITH
ENDFUNC



FUNCTION uf_facturacao_dados
	
		**valida datamatrix
	IF EMPTY(uf_gerais_retorna_campos_datamatrix(ALLTRIM(lcautorizationcode)))
    	uf_perguntalt_chama("Codigo de autoriza��o / �nico da embalagem inv�lido, por favor, coloque manualmente.", "OK", "", 16)
    	INSERT INTO ucrsTempAuthCodeEmb VALUES (1, "")
    RETURN .T.
	 ELSE
	    LOCAL lcpacksn
	    SELECT ucrdatamatrixtemp
	    GOTO TOP
	    lcpacksn = ALLTRIM(ucrdatamatrixtemp.sn)
	    fecha("ucrDataMatrixTemp")
	    INSERT INTO ucrsTempAuthCodeEmb VALUES (2, lcpacksn)
	    RETURN .T.
	 ENDIF
		
ENDFUNC



**
FUNCTION uf_facturacao_eventoRefFi
	LPARAMETERS lcLote

	** Trata plicas
	Select fi
	Replace Fi.ref With STRTRAN(Fi.ref, Chr(39), ' ')
	If empty(fi.ref)
		uf_facturacao_ControlaEventosLinhas()
		RETURN .f.
	ENDIF
	

	** Verifica Codigo Alternativo
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_stocks_procuraRef '<<Alltrim(FI.REF)>>',<<mySite_nr>>
	ENDTEXT 
	IF !uf_gerais_actGrelha("","ucrsCodAlternativo",lcSQL)		
		uf_perguntalt_chama("N�O FOI POSS�VEL VERIFICAR O C�DIGO ALTERNATIVO DO ARTIGO. POR FAVOR VERIFIQUE.","OK","",16)
		RETURN .f.
	ENDIF
	

	IF !EMPTY(ucrsCodAlternativo.rateamento) AND uf_gerais_getParameter("ADM0000000211","BOOL") == .t.
		uf_perguntalt_chama("O PRODUTO: ["+ ALLTRIM(Fi.ref) +"]. Est� marcado como de rateamento!","OK","",64)			
	ENDIF

	IF Alltrim(Upper(Fi.ref)) != Alltrim(Upper(ucrsCodAlternativo.Ref))
		Replace Fi.ref With ucrsCodAlternativo.Ref
	ENDIF 	
	**
	
	


	LOCAL lcPos
	STORE 0 TO lcPos
	SELECT fi
	lcPos = RECNO("fi")
	IF USED("uCrsAtendST")
		SELECT uCrsAtendST
		DELETE FOR ALLTRIM(uCrsAtendST.ststamp)==ALLTRIM(fi.fistamp)
	ENDIF 

	uf_atendimento_actRef()

	SELECT fi
	TRY 
		GO lcPos
	CATCH
		***
	ENDTRY
	
	IF !EMPTY(ucrsCodAlternativo.cnpem)
		SELECT fi	
		Replace fi.cnpem WITH ALLTRIM(ucrsCodAlternativo.cnpem)
	ENDIF 
	
	IF USED("ucrsCodAlternativo")
		Fecha("ucrsCodAlternativo")
	ENDIF
	
	IF fi.usalote == .t.
		uf_facturacao_AtribuiLote()
	ENDIF
	
	uf_atendimento_actValoresFi()
	uf_atendimento_actTotaisFt()
	
	** valida��o c�digo embalagem 21092015 - Lu�s Leal
	LOCAL lcObriga, lcCod
	STORE .f. TO lcObriga
	
	select ft2		
	lcCod = UPPER(ALLTRIM(ft2.u_codigo))
			
	&& Tem de ser revisto para ser dinamico baseado no campo da cptpla CodUniEmb 21092015
	IF lcCod == "AS" OR lcCod == "WG" OR lcCod == "HP" OR lcCod == "WJ" 
		lcObriga = .t.
	ENDIF
			
	IF lcObriga == .t.
		&& No caso de ser Omega pede sempre
		IF UPPER(ALLTRIM(lcCod)) == "WJ"
			uf_tecladoalpha_chama("FI.U_CODEMB", "Introduza o C�digo da Embalagem:", .f., .f., 2)
			uf_atendimento_preenche_codUnicoEmbalagemFi()	
		ELSE
			SELECT fi 
			SELECT uCrsAtendST
			LOCATE FOR uCrsAtendST.ststamp = fi.fistamp
			IF FOUND()
				IF uCrsAtendST.CodUniEmb == .t. AND EMPTY(fi.u_codemb)
					uf_tecladoalpha_chama("FI.U_CODEMB", "Introduza o C�digo da Embalagem:", .f., .f., 2)
					uf_atendimento_preenche_codUnicoEmbalagemFi()	
					
				ENDIF		
			ENDIF
		ENDIF
	ENDIF
	
	uf_facturacao_ControlaEventosLinhas()
ENDFUNC


**
FUNCTION uf_Facturacao_autocompleteRefFi
	LPARAMETERS nKeyCode
		
	Store 0 To lcTamanhoAntigo,lcNovoTamanho
			
	********
	WITH FACTURACAO.PageFrame1.Page1.gridDoc
		FOR i=1 TO .columnCount
			If Upper(.Columns(i).ControlSource)=="FI.REF"
		
				** trata plicas
				.Columns(i).text1.value = STRTRAN(.Columns(i).text1.value, Chr(39), ' ')
				
				facturacao.valorparapesquisa = Alltrim(.Columns(i).text1.value)
				lcTamanhoAntigo = Len(Alltrim(.Columns(i).text1.value))

				lcsql = ""		
				TEXT TO lcSQL NOSHOW TEXTMERGE 
					SELECT TOP 1 convert(nvarchar,REF) as REF 
					FROM 
						ST (nolock) 
					WHERE 
						st.site_nr = <<mySite_nr>>
						AND ST.REF LIKE '<<Alltrim(facturacao.valorparapesquisa)>>%' 
					ORDER BY 
						REF ASC
				ENDTEXT 
				uf_gerais_actGrelha("","tempSQLAutoComp",lcSql)
				
				If Reccount("tempSQLAutoComp") > 0
				
					Select tempSQLAutoComp
					lcNovoValor =Alltrim(tempSQLAutoComp.Ref)
					lcNovoTamanho = Len(Alltrim(tempSQLAutoComp.Ref))
		
					If mykeypressed != 127 
	
						.Columns(i).text1.value = Alltrim(tempSQLAutoComp.REF)

						lc_diftamanhos = ABS(lcNovoTamanho - lcTamanhoAntigo)
	
						If  !EMPTY(lcNovoValor) 
							.Columns(i).text1.selstart = lcTamanhoAntigo 
							.Columns(i).text1.sellength = lc_diftamanhos 
						Endif
					Endif
				Endif
			Endif
		Endfor
	ENDWITH	
ENDFUNC


**
FUNCTION uf_facturacao_autocompleteDesign
	LPARAMETERS nKeyCode
	
	Store 0 To lcTamanhoAntigo, lcNovoTamanho
	
	********
	WITH FACTURACAO.PageFrame1.Page1.gridDoc
	
		FOR i = 1 TO .columnCount
			IF Upper(.Columns(i).ControlSource) == "FI.DESIGN"
				
				** tratamento plica
				IF mykeypressed = 39
					.Columns(i).text1.value = strtran(.Columns(i).text1.value,chr(39),'')
				ENDIF 
				*ESPA�O
				IF mykeypressed == 32
					facturacao.valorparapesquisa = .Columns(i).text1.value
					lcTamanhoAntigo =Len(ALLTRIM(.Columns(i).text1.value)) +1
					lcsql = ""		
					&&lcsql = "SELECT TOP 1 convert(nvarchar,DESIGN) as DESIGN FROM ST (nolock) WHERE DESIGN LIKE '"+ Alltrim(facturacao.valorparapesquisa) + " %' ORDER BY DESIGN ASC"
					
					TEXT TO lcSQL NOSHOW TEXTMERGE 
						SELECT TOP 1 convert(nvarchar,DESIGN) as DESIGN
						FROM
							ST (nolock) 
						WHERE 
							st.site_nr = <<mySite_nr>>
							AND ST.REF LIKE '<<Alltrim(facturacao.valorparapesquisa)>> %' 
						ORDER BY 
							REF ASC
					ENDTEXT 

					
				ELSE 
					facturacao.valorparapesquisa = .Columns(i).text1.value
					lcTamanhoAntigo =Len(ALLTRIM(.Columns(i).text1.value))
					
					lcsql = ""
					&&lcsql = "SELECT TOP 1 convert(nvarchar,DESIGN) as DESIGN FROM ST (nolock) WHERE DESIGN LIKE '"+ Alltrim(facturacao.valorparapesquisa) + "%' ORDER BY DESIGN ASC"
					TEXT TO lcSQL NOSHOW TEXTMERGE 
					
						SELECT 
							TOP 1 convert(nvarchar,DESIGN) as DESIGN 
						FROM 
							ST (nolock) 
						WHERE 
							st.site_nr = <<mySite_nr>>
							AND st.DESIGN LIKE '<<Alltrim(facturacao.valorparapesquisa)>>%' 
						ORDER BY 
							st.DESIGN ASC
					ENDTEXT 
					
					
				Endif
				
				uf_gerais_actGrelha("","tempSQLAutoComp",lcSql)
				
				IF Reccount("tempSQLAutoComp") > 0
				
					Select tempSQLAutoComp
					lcNovoValor =Alltrim(tempSQLAutoComp.DESIGN)
					lcNovoTamanho = Len(Alltrim(tempSQLAutoComp.DESIGN))
		
					If mykeypressed != 127 AND mykeypressed != 7 
	
						.Columns(i).text1.value = Alltrim(tempSQLAutoComp.DESIGN)

						lc_diftamanhos = ABS(lcNovoTamanho - lcTamanhoAntigo)
	
						If  !EMPTY(lcNovoValor) 
							.Columns(i).text1.selstart = lcTamanhoAntigo 
							.Columns(i).text1.sellength = lc_diftamanhos 
						ENDIF 
					ENDIF 
				ENDIF 
			ENDIF 
		ENDFOR 
	ENDWITH
ENDFUNC


**
FUNCTION uf_facturacao_pesquisarArtigosDesignFi
	IF myFactAGravar
		myFactAGravar = .f.
		RETURN .f.
	ENDIF
	** desactiva lostfocus
	ti=facturacao.pageframe1.page1.griddoc.columnCount
	FOR i=1 TO ti
		If Upper(facturacao.pageframe1.page1.griddoc.Columns(i).ControlSource)=="FI.DESIGN"
			UnBindEvent(facturacao.pageframe1.page1.griddoc.columns(i).text1,"LostFocus",facturacao.oCust,"proc_pesquisarArtigosDesignFi")
		Endif
	ENDFOR
	
	TEXT TO lcSQL TEXTMERGE NOSHOW
		Select 
			Count(ref) as valor
			,REF 
		From 
			ST (nolock) 
		Where 
			st.site_nr = <<mySite_nr>>
			and st.DESIGN LIKE '<<uf_gerais_trataPlicasSQL(Alltrim(facturacao.valorparapesquisa))>>%' 
		Group By 
			REF
   	ENDTEXT 

       

   If !uf_gerais_actGrelha("", "ucrsArtigos", lcSQL)
	   uf_perguntalt_chama("OCORREU UMA ANOMALIA NA PROCURA DO ARTIGO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
	   RETURN .f.
   Endif
   
	If  RECCOUNT("ucrsArtigos") > 0
		uf_pesqstocks_chama("FACTURACAO",facturacao.valorparapesquisa)
		fecha("ucrsArtigos")
	ENDIF
ENDFUNC


**
FUNCTION uf_facturacao_validaCamposObrigatorios
	Local lc_numregistos, lcMotIsenvaoIva
	STORE .f. TO lcMotIsenvaoIva
	
	&& Verifica se deve imprimir motivo de isencao de Iva
	lcMotIsencaoIva = uf_gerais_getParameter("ADM0000000261","Bool")
	
	Select Ft
	Go Top
	If Empty(Ft.nmdoc)
		uf_perguntalt_chama("EXISTEM CAMPOS OBRIGAT�RIOS POR PREENCHER. POR FAVOR VERIFIQUE. CAMPO: DOCUMENTO.","OK","",48)
		Return .f.
	Endif
	If Empty(Ft.nome)
		uf_perguntalt_chama("EXISTEM CAMPOS OBRIGAT�RIOS POR PREENCHER. POR FAVOR VERIFIQUE. CAMPO: NOME.","OK","",48)
		Return .f.
	ENDIF
	If Empty(Ft.no) OR Ft.no == 0
		uf_perguntalt_chama("EXISTEM CAMPOS OBRIGAT�RIOS POR PREENCHER. POR FAVOR VERIFIQUE. CAMPO: N� Cliente.","OK","",48)
		Return .f.
	Endif
	If Empty(Ft.fno) 
		uf_perguntalt_chama("EXISTEM CAMPOS OBRIGAT�RIOS POR PREENCHER. POR FAVOR VERIFIQUE. CAMPO: N�MERO DOCUMENTO.","OK","",48)
		Return .f.
	Endif
	If Empty(Ft.Fdata)
		uf_perguntalt_chama("EXISTEM CAMPOS OBRIGAT�RIOS POR PREENCHER. POR FAVOR VERIFIQUE. CAMPO: DATA DOCUMENTO.","OK","",48)
		Return .f.
	ENDIF

	IF EMPTY(ft.moeda)	
		LOCAL lcMoedaDefault
		STORE 'EURO' TO lcMoedaDefault
		lcMoedaDefault = uf_gerais_getParameter("ADM0000000260","text")
		&& Se parametro n�o estiver preenchido assume � por default
		IF EMPTY(lcMoedaDefault)
			lcMoedaDefault = 'EURO'
		ENDIF
		Select FT
		Replace Ft.moeda WITH lcMoedaDefault
	ENDIF 
	
	IF EMPTY(ft.moeda)
		uf_perguntalt_chama("EXISTEM CAMPOS OBRIGAT�RIOS POR PREENCHER. POR FAVOR VERIFIQUE. CAMPO: MOEDA","OK","",48)
		RETURN .f.
	ENDIF
	If Reccount("Fi") == 0
		uf_perguntalt_chama("N�O � POSSIVEL GRAVAR DOCUMENTOS SEM LINHAS. POR FAVOR VERIFIQUE.","OK","",48)
		Return .f.
	ENDIF

	** verifica Isencao Iva na venda
	IF lcMotIsencaoIva  == .t. AND UPPER(myPaisConfSoftw) <> 'ANGOLA'
		IF !empty(alltrim(td.tiposaft)) AND EMPTY(ft2.motiseimp) AND EMPTY(uCrsE1.motivo_isencao_iva) 	
			SELECT fi 
			GO TOP 
			SCAN 
				IF fi.iva == 0
					uf_perguntalt_chama("Existem produtos com Iva a 0% mas n�o definiu o Motivo de Isen��o. Por favor verifique.","OK","",48)
					RETURN .f.
				ENDIF
			ENDSCAN
		ENDIF
	ENDIF
	
	** Gravar Linhas sem refer�ncia
	SELECT fi
	GO TOP 
	SCAN 
		IF EMPTY(fi.ref) AND EMPTY(oref) AND !EMPTY(fi.design) AND fi.qtt != 0
			uf_perguntalt_chama("Existem linhas sem refer�ncia preenchida. Por favor verifique.","OK","",48)
			return.f.
		ENDIF
	ENDSCAN

	*Documento Original
	IF ALLTRIM(UPPER(ALLTRIM(Ft.nmdoc)))=="VENDA MANUAL"
		SELECT Ft2
		&& N� preenchido
		IF EMPTY(ALLTRIM(ft2.u_docorig))
			uf_perguntalt_chama("EXISTEM CAMPOS OBRIGAT�RIOS POR PREENCHER. POR FAVOR VERIFIQUE. CAMPO: DOCUMENTO DE ORIGEM PARA VENDAS MANUAIS.","OK","",48)
			Return .f.
		ENDIF
		
		&& Formato do N� correcto
		IF !(LIKE('*/*',ALLTRIM(ft2.u_docorig))) OR (EMPTY(STREXTRACT(ALLTRIM(ft2.u_docorig),'/'))) OR (OCCURS('/', ALLTRIM(ft2.u_docorig)) > 1)
			uf_perguntalt_chama("FORMATO DO N� DO DOCUMENTO ORIGINAL INV�LIDO. POR FAVOR PREENCHA A S�RIE/N� DOCUMENTO." + CHR(13) + "EX: ABC/00001","OK","",48)
			RETURN .f.
		ENDIF
	ENDIF 
	
	** Lotes
	IF uf_gerais_getParameter("ADM0000000211","BOOL") AND uf_gerais_getParameter("ADM0000000215","BOOL") == .f.
		IF td.lancaSl == .t. && Valida se mexe em stock
			select Fi
			GO TOP 
			SCAN 
				IF !EMPTY(fi.usalote) AND EMPTY(ALLTRIM(fi.lote)) AND !EMPTY(fi.ref)

					select Fi
					facturacao.Pageframe1.Page1.TextPesq.value = ALLTRIM(fi.ref)
					uf_facturacao_criaCursorProcura()
					uf_perguntalt_chama("Existem linhas em que o Lote n�o foi identificado." + CHR(13) + "Por favor verifique.","OK","",48)
				
					RETURN .f.
				ENDIF 
			ENDSCAN 
			select Fi
			GO Top
		ENDIF 
	ENDIF
	
	*Data de Receita n�o pode ser vazia no Documento Inser��o de Receita caso o Plano aplicado seja SNS
	Select TD
	IF td.u_tipodoc == 3
		SELECT Ft2
		&& ENTIDADE
		IF UPPER(ALLTRIM(ft2.u_abrev)) == "SNS" OR UPPER(ALLTRIM(ft2.u_abrev2)) == "SNS"	
			SELECT Ft
			IF YEAR(Ft.cdata) == 1900
				uf_perguntalt_chama("O campo Dt. Rec. � de preenchimento obrigat�rio para planos do SNS.","OK","",48)
				Return .f.
			ENDIF 
		ENDIF
	ENDIF 


	** Verifica existencia de produtos inativos
	IF USED("ucrsProdutosInactivosDoc")
		fecha("ucrsProdutosInactivosDoc")
	ENDIF 

	TEXT TO lcSQL NOSHOW TEXTMERGE 
		select ref as refinativa from st where inactivo = 1 AND site_nr = <<mysite_nr>>
	ENDTEXT 
	IF !uf_gerais_actGrelha("", "ucrsRefsInativas", lcSQL)
		uf_perguntalt_chama("N�O FOI POSSIVEL VERIFICAR A LISTA DE REFER�NCIAS INATIVAS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	ENDIF
	
	SELECT distinct refinativa FROM FI LEFT JOIN ucrsRefsInativas ON FI.ref = ucrsRefsInativas.refinativa WHERE refinativa IS NOT NULL INTO CURSOR ucrsProdutosInactivosDoc READWRITE 

	LOCAL lcRefs 
	
	IF RECCOUNT("ucrsProdutosInactivosDoc") != 0
		lcRefs = ""
		Select ucrsProdutosInactivosDoc 
		GO Top
		SCAN
			IF EMPTY(lcRefs)
				lcRefs  = ALLTRIM(ucrsProdutosInactivosDoc.refinativa)
			ELSE
				lcRefs  = lcRefs + ", " + ALLTRIM(ucrsProdutosInactivosDoc.refinativa)
			ENDIF 
			
		ENDSCAN 
				
		uf_perguntalt_chama("Existem produtos inativos no documento: Ex. " + lcRefs + CHR(13) + CHR(13) + "N�o � possivel gravar." ,"OK","",16)
		RETURN .f.
	ENDIF 		
	
	Return .t.
ENDFUNC


** Regras
FUNCTION uf_facturacao_RegrasGravacao
	LPARAMETERS llAtend
	
		
	Local lcSql, lcTab2, lcRef, lnMaxEmb, lnMaxUni, lnAlias, lcCodigo, lnLin, lcTab, validado, lcCntLin, lcCntLin2
	Store "" TO lcSql, lcRef
	Store 0 TO lnMaxEmb,lnMaxUni,lnLin,lcCntLin,lcCntLin2
	STORE .f. TO validado
	

	
	** Obriga contribuinte
	IF uf_gerais_getParameter("ADM0000000255","BOOL") AND EMPTY(ft.ncont)
		uf_perguntalt_chama("O N�mero de Contribuinte � obrigat�rio.","OK","",64)
		RETURN .f.
	ENDIF
	**	

	** N�o permite a Introdu��o de Regulariza��o a Cliente manualmente no backoffice Aki
	SELECT uCrsE1
	IF !emdesenvolvimento AND ALLTRIM(UPPER(uCrsE1.tipoEmpresa)) != "CLINICA"
		SELECT td
		IF td.u_tipodoc = 2 OR td.u_tipodoc = 1
			uf_perguntalt_chama("N�O � POSS�VEL A INTRODU��O DESTE TIPO DE DOCUMENTO MANUALMENTE NO BACKOFFICE. POR FAVOR VERIFIQUE.","OK","",64)
			RETURN .f.
		ENDIF 
	ENDIF
	**
	
	** Verificar se o documento j� foi exportado **
	SELECT ft
	IF !EMPTY(ft.exportado) AND ALLTRIM(UPPER(ft.nmdoc)) != 'INSER��O DE RECEITA'
		uf_perguntalt_chama("O Documento j� foi exportado. As altera��es podem n�o ser refletidas no destino.","OK","",64)
	ENDIF
	**
	
	IF ALLTRIM(ft2.u_codigo)=='RS'
		uf_perguntalt_chama("O PLANO DE COMPARTICIPA��O S� PODE SER APLICADO NO ATENDIMENTO.","OK","",48)
		RETURN .f.
	ENDIF 
	
	** Verifica que n�o tem planos de comparticipa��o
	IF !EMPTY(ft2.u_codigo) OR !EMPTY(ft2.u_codigo)
		TEXT TO lcSql NOSHOW textmerge
			select e_compart from cptpla where codigo='<<ALLTRIM(ft2.u_codigo)>>' or codigo='<<ALLTRIM(ft2.u_codigo2)>>'
		ENDTEXT
		IF !uf_gerais_actGrelha("","uCrsCompart",lcSql)
			uf_perguntalt_chama("N�o foi poss�vel verificar o plano do documento. Por favor contacte o Suporte.","OK","",16)
			RETURN .f.
		ENDIF
		
		LOCAL lcCorrigeCompartExterna
		lcCorrigeCompartExterna = .f.
		
		if(VARTYPE(myInsereReceitaCorrigida)!='U')
			lcCorrigeCompartExterna  = myInsereReceitaCorrigida			
		ENDIF
		
		
		IF RECCOUNT("uCrsCompart")>0
			SELECT uCrsCompart
			GO TOP 
			SCAN 
				IF uCrsCompart.e_compart = .t. AND type("ATENDIMENTO") == "U" AND EMPTY(lcCorrigeCompartExterna) 				
					uf_perguntalt_chama("OS PLANOS DE COMPARTICIPA��O S� PODEM SER APLICADOS NO ATENDIMENTO.","OK","",48)
					RETURN .f.						
				ENDIF 
			ENDSCAN 
		ENDIF 
	ENDIF 
	**
	
	** Verificar Se o sistema permite vendas ao cliente 200 **
	SELECT ft
	IF ft.no == 200
		IF uf_gerais_getParameter("ADM0000000122","BOOL")
			uf_perguntalt_chama("O SISTEMA APENAS PERMITE VENDER A CLIENTES COM FICHA. DEVE ESCOLHER UM CLIENTE ANTES DE PODER CONTINUAR.","OK","",64)
			RETURN .f.
		ENDIF
	ENDIF
	**
	
	** Verificar Se o Cliente tem a Factura��o Cancelada **
	IF !llAtend
		SELECT ft
		IF !(ft.no==200)
			IF uf_gerais_getParameter("ADM0000000128","BOOL")
				IF facturacao.noCreditCL
					uf_perguntalt_chama("O CLIENTE SELECCIONADO TEM A FACTURA��O CANCELADA. ASSIM N�O PODE CONTINUAR.","OK","",64)
					RETURN .f.
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	**
	** Controlar Plafond de Cliente
	** verificar plafond
	LOCAL lcficombi
	STORE .f. TO lcficombi
	IF UPPER(ALLTRIM(ucrse1.pais)) != 'PORTUGAL'
		select * from fi where !empty(fi.bistamp) into cursor ucrsfibi readwrite
		IF reccount("ucrsfibi")>0
			lcficombi = .t.
		ENDIF 
		fecha("ucrsfibi")
	ENDIF 
	
	lcSQL = ""	
	TEXT TO lcSql NOSHOW textmerge
		select eplafond, esaldo, no_ext from b_utentes where no = <<ft.no>> and estab = <<ft.estab>>
	ENDTEXT
	IF !uf_gerais_actGrelha("","uCrsClplafond",lcSql)
		uf_perguntalt_chama("N�o foi poss�vel verificar o plafond do cliente. Por favor contacte o Suporte.","OK","",16)
		RETURN .f.
	ENDIF
	
	LOCAL lcPlafondCl, lcSaldoCl, lcno_ext 
	STORE 0 TO lcPlafondCl, lcSaldoCl
	STORE '' TO lcno_ext 
	
	SELECT uCrsClplafond
	GO TOP 
	lcPlafondCl = uCrsClplafond.eplafond
	lcSaldoCl = uCrsClplafond.esaldo
	lcno_ext = uCrsClplafond.no_ext
	
	IF UPPER(ALLTRIM(ucrse1.pais)) != 'PORTUGAL' AND EMPTY(lcno_ext) AND td.u_tipodoc == 9 AND lcficombi = .f.
		uf_perguntalt_chama("N�o pode emitir documentos a cr�dito a clientes que n�o tenham n�mero externo!","OK","",64)
		RETURN .F.
	ENDIF 
	
	SELECT td
	GO TOP 
	
	&& Novo Controle Plafond Entidade de Fatura��o - 20150515 - Lu�s Leal
	IF uCrsE1.controla_plafond == .t.
		IF td.u_tipodoc == 9
			
			SELECT ft	
			IF ft.no != 200 AND uCrse1.plafond_cliente > 0 
				IF lcPlafondCl == 0
					IF (ft.etotal + uCrsClplafond.esaldo) > uCrse1.plafond_cliente	
						IF UPPER(ALLTRIM(ucrse1.pais)) == 'PORTUGAL'		
							IF uf_perguntalt_chama("O plafond do cliente foi ultrapassado." + CHR(13) + "Pretende continuar com o atendimento?","Sim","N�o",64)
									PUBLIC cval
									STORE '' TO cval
									uf_tecladoalpha_chama("cval", "INTRODUZA PASSWORD PARA PLAFOND:", .t., .f., 0)
									
									IF EMPTY(cval)
										uf_perguntalt_chama("Deve introduzir a password de Supervisor para permitir ultrapassar o plafond.","OK","",64)
										RETURN .f.
									ELSE
										IF !(UPPER(ALLTRIM(cval))==UPPER(ALLTRIM(uf_gerais_getParameter('ADM0000000187', 'TEXT'))))
											uf_perguntalt_chama("A password de Supervisor n�o est� correcta.","OK","",64)		
										RETURN .F.
										ENDIF 
									ENDIF
							ELSE
								RETURN .f.
							ENDIF
						ELSE
							IF !EMPTY(lcno_ext)
								uf_perguntalt_chama("O plafond do cliente foi ultrapassado." + CHR(13) + "N�o pode concluir o documento.","OK","",64)
								RETURN .F.
							ENDIF 
						ENDIF 
					ENDIF 
				ELSE
					IF (ft.etotal + uCrsClplafond.esaldo) > lcPlafondCl	
						IF UPPER(ALLTRIM(ucrse1.pais)) == 'PORTUGAL'	
							IF uf_perguntalt_chama("O plafond do cliente foi ultrapassado." + CHR(13) + "Pretende continuar com o atendimento?","Sim","N�o",64)
									PUBLIC cval
									STORE '' TO cval
									uf_tecladoalpha_chama("cval", "INTRODUZA PASSWORD PARA PLAFOND:", .t., .f., 0)
									
									IF EMPTY(cval)
										uf_perguntalt_chama("Deve introduzir a password de Supervisor para permitir ultrapassar o plafond.","OK","",64)
										RETURN .f.
									ELSE
										IF !(UPPER(ALLTRIM(cval))==UPPER(ALLTRIM(uf_gerais_getParameter('ADM0000000187', 'TEXT'))))
											uf_perguntalt_chama("A password de Supervisor n�o est� correcta.","OK","",64)		
										RETURN .F.
										ENDIF 
									ENDIF
							ELSE
								RETURN .f.
							ENDIF
						ELSE
							IF !EMPTY(lcno_ext) 
								uf_perguntalt_chama("O plafond do cliente foi ultrapassado." + CHR(13) + "N�o pode concluir o documento.","OK","",64)
								RETURN .F.
							ENDIF 
						ENDIF 
					ENDIF
				ENDIF
			ELSE
				SELECT ft
				GO TOP 
	     		IF ft.no != 200 
					IF (ft.etotal + uCrsClplafond.esaldo) > lcPlafondCl 
						IF UPPER(ALLTRIM(ucrse1.pais)) == 'PORTUGAL'
							IF uf_perguntalt_chama("O plafond do cliente foi ultrapassado." + CHR(13) + "Pretende continuar com o atendimento?","Sim","N�o",64)
								PUBLIC cval
								STORE '' TO cval
								uf_tecladoalpha_chama("cval", "INTRODUZA PASSWORD PARA PLAFOND:", .t., .f., 0)
									
								If EMPTY(cval)
									uf_perguntalt_chama("Deve introduzir a password de Supervisor para permitir ultrapassar o plafond.","OK","",64)
									RETURN .f.
								ELSE
									IF !(UPPER(ALLTRIM(cval))==UPPER(ALLTRIM(uf_gerais_getParameter('ADM0000000187', 'TEXT'))))
										uf_perguntalt_chama("A password de Supervisor n�o est� correcta.","OK","",64)		
										RETURN .F.
									ENDIF 
								ENDIF
							ELSE
								RETURN .f.
							ENDIF
						ELSE
							IF !EMPTY(lcno_ext)
								uf_perguntalt_chama("O plafond do cliente foi ultrapassado." + CHR(13) + "N�o pode concluir o documento.","OK","",64)
								RETURN .F.
							ENDIF 
						ENDIF 
					ENDIF 
				ENDIF
			ENDIF 
		ENDIF
	ENDIF
	**
	

	
	** Valida Registo de Documento com numera��o Superior e Data Inferior ao ultimo Documento
	** Diferente nas Inser��es de Receita
	IF myFtIntroducao
		SELECT td
		IF td.u_tipodoc != 3 
			SELECT Ft
			lcSQL = ""	
			TEXT TO lcSql NOSHOW textmerge
				SELECT	case when max(fdata) > '<<uf_gerais_getDate(ft.fdata,"SQL")>>' then 1 else 0 end as valor
				FROM	ft (nolock)
				WHERE	ndoc = <<Ft.Ndoc>> And YEAR(fdata) = YEAR('<<uf_gerais_getDate(Ft.fdata,"SQL")>>')
			ENDTEXT

			IF !uf_gerais_actGrelha("","uCrsMaxNoValida",lcSql)
				uf_perguntalt_chama("N�O FOI POSSIVEL VALIDAR A NUMERA��O DO �LTIMO DOCUMENTO.","OK","",16)
				RETURN .f.
			ELSE
				IF reccount()>0
					IF uCrsMaxNoValida.valor == 1
						uf_perguntalt_chama("J� EXISTE UM DOCUMENTO DESTE TIPO COM DATA SUPERIOR. POR FAVOR VERIFIQUE.","OK","",64)
						fecha("uCrsMaxNoValida")
						RETURN .f.
					ENDIF
				ENDIF
				fecha("uCrsMaxNoValida")
			ENDIF
		Endif
	ENDIF

	** Verificar se existem linhas com tabIva = 0	
	select fi
	Calculate cnt() to lcCntLin for fi.tabIva=0
	If lcCntLin>0
		uf_perguntalt_chama("Existem linhas que n�o t�m a Tabela de Iva definida. Por favor verifique.","OK","",64)
		Return .F.
	ENDIF 
	lcCntLin=0

	** valida se a sp de certifica��o est� no sistema **/
	IF !uf_gerais_actGrelha("","","SELECT dbo.up_GerarHash('')")
		uf_perguntalt_chama("FUN��O DE GEST�O DE ASSINATURAS DIGITAIS N�O DISPON�VEL, POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	ENDIF
	

	
	** valida certifica��o do documento anterior da mesma serie de factura��o **/
	IF myFtIntroducao == .t.
		SELECT Ft
		IF !empty(alltrim(td.tiposaft)) && N�o valida os documentos que excluem saft
			lcSQL = ""	
			TEXT TO lcSql NOSHOW textmerge
				exec up_cert_verificaDocAnterior <<ft.fno>>,<<ft.ndoc>>,'<<uf_gerais_getDate(ft.fdata,"SQL")>>','<<ALLTRIM(td.tiposaft)>>'
			ENDTEXT
			IF !uf_gerais_actGrelha("","uCrstemp",lcSql)
				uf_perguntalt_chama("O DOCUMENTO ANTERIOR N�O EST� ASSINADO DIGITALMENTE. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				RETURN .f.
			ELSE
				SELECT uCrstemp
				IF uCrstemp.count == 0
					uf_perguntalt_chama("O DOCUMENTO ANTERIOR N�O EST� ASSINADO DIGITALMENTE. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
					fecha("uCrstemp")
					RETURN .f.
				ENDIF
			ENDIF
			
			IF USED("uCrstemp")
				fecha("uCrstemp")
			ENDIF
		ENDIF	
	ENDIF
	
	** Valida Altera��es com base nas Regras de Certifica��o de Software 
	IF myFtAlteracao
		*IF !(td.u_tipodoc == 3) && N�o valida o documento Inser��o de Receita
		IF !empty(alltrim(td.tiposaft))	
			IF !USED("uCrsFtOrig")
				uf_perguntalt_chama("OCORREU UM ERRO A VALIDAR AS ALTERA��ES AO DOCUMENTO.","OK","",16)
				RETURN .f.
			ENDIF
			
			IF RECCOUNT("uCrsFtOrig")>0
				** Comparar com os valores a alterar **
					** totais
				SELECT ft
				GO TOP
				SELECT uCrsFtOrig
				GO TOP
				IF (ft.etotal != uCrsFtOrig.etotal) OR (ft.fno != uCrsFtOrig.fno) OR !(uf_gerais_getdate(ft.fdata,"SQL") == uf_gerais_getDate(uCrsFtOrig.fdata,"SQL")) OR (ft.totqtt != uCrsFtOrig.totqtt) OR !(ALLTRIM(ft.ncont) == ALLTRIM(uCrsFtOrig.ncont)) OR !(ALLTRIM(ft.nome) == ALLTRIM(uCrsFtOrig.nome)) OR (ft.no != uCrsFtOrig.no)
					uf_perguntalt_chama("DE ACORDO COM AS REGRAS DE CERTIFICA��O DE SOFTWARES DE FACTURA��O N�O PODE GRAVAR AS ALTERA��ES.","OK","",64)
					*fecha("uCrsFtOrig")
					RETURN .f.
				ENDIF
					** outros dados
				SELECT ft2
				GO TOP
				SELECT uCrsFtOrig
				GO TOP
				IF !(ALLTRIM(ft2.motiseimp)==ALLTRIM(uCrsFtOrig.motiseimp))
					uf_perguntalt_chama("DE ACORDO COM AS REGRAS DE CERTIFICA��O DE SOFTWARES DE FACTURA��O N�O PODE GRAVAR AS ALTERA��ES.","OK","",64)
					*fecha("uCrsFtOrig")
					RETURN .f.
				ENDIF
					** linhas
				SELECT fi
				GO TOP
				SCAN
					SELECT uCrsFtOrig
					GO TOP
					SCAN FOR ALLTRIM(uCrsFtOrig.fistamp)==ALLTRIM(fi.fistamp)
						IF (fi.epv!=uCrsFtOrig.epv) OR (fi.etiliquido!=uCrsFtOrig.etiliquido) OR (fi.iva!=uCrsFtOrig.iva) OR (fi.tabiva!=uCrsFtOrig.tabiva) ;
							OR (fi.desconto!=uCrsFtOrig.desconto) OR (fi.qtt!=uCrsFtOrig.qtt)
							lcValidaAltFi = .t.
							uf_perguntalt_chama("DE ACORDO COM AS REGRAS DE CERTIFICA��O DE SOFTWARES DE FACTURA��O N�O PODE GRAVAR AS ALTERA��ES.","OK","",64)
							*fecha("uCrsFtOrig")
							RETURN .f.
						ENDIF
						SELECT uCrsFtOrig
					ENDSCAN
					
					SELECT fi
				ENDSCAN
				**
			ENDIF
			
			*FECHA("uCrsFtOrig")
		ENDIF
	ENDIF	
	**
	
	
	&& valida se a venda � superior a 1000�
	lcMoedaDefault = uf_gerais_getParameter("ADM0000000260","text")
	IF EMPTY(lcMoedaDefault)
		IF !empty(alltrim(td.tiposaft))	
			IF ft.etotal >= 1000
				IF EMPTY(ALLTRIM(ft.ncont)) OR ALLTRIM(ft.ncont) == '999999990' OR EMPTY(ALLTRIM(ft.morada)) OR EMPTY(ALLTRIM(ft.nome)) AND UPPER(ALLTRIM(ucrse1.pais)) == 'PORTUGAL'
					uf_perguntalt_chama("Em documentos de fatura��o superiores a 1000� � obrigat�rio indicar o NIF, Nome e Morada do Utente.","OK","",48)
					RETURN .f.
				ENDIF
			ENDIF
		ENDIF
	ENDIF 
	SELECT ft2
	lcCodigo = ft2.u_codigo



	
	************************************************
	** VERIFICAR SE EXISTEM QUANTIDADE A NEGATIVO **
	************************************************
	
	select fi
	Calculate cnt() to lcCntLin for (!empty(fi.ref) or !empty(fi.design)) and fi.qtt<0
	If lcCntLin>0
		uf_perguntalt_chama("N�o pode colocar quantidades a negativo.","OK","",64)
		Return .F.
	ENDIF
	
	
		**************************************
	** VERIFICAR SE A VENDA ESTA VAZIA	**
	**************************************
	select fi
	GO top
	Calculate cnt() to lcCntLin for (!empty(fi.ref) or !empty(fi.design)) and fi.qtt>0
	If lcCntLin=0
		uf_perguntalt_chama("N�O PODE GRAVAR UMA VENDA SEM PRODUTOS."+CHR(10)+CHR(10)+"PELO MENOS UMA LINHA COM VALOR DEVE EXISTIR [QUANTIDADE>0].","OK","",64)
		Return .F.
	EndIf
	lcCntLin=0
	**
	
	

	** VALIDAR CLIENTE 
	SELECT Td
	If TD.lancacc==.t.
		If ft.no == 200
			uf_perguntalt_chama("N�O PODE VENDER A CR�DITO SEM PRIMEIRO ESCOLHER UM CLIENTE COM FICHA.","OK","",64)
			Return .F.
		ENDIF
		
		IF td.u_tipodoc==4
			IF UPPER(ALLTRIM(ucrsModoFact.modofact)) == 'FACTURA��O ACORDO'
				facturacao.modoFactCL = ucrsModoFact.modofact
			ENDIF
			IF UPPER(ALLTRIM(facturacao.modoFactCL))!='FACTURA��O ACORDO'
				uf_perguntalt_chama("S� PODE CRIAR DOCUMENTOS DESTE TIPO PARA CLIENTES COM O MODO DE FACTURA��O IGUAL A 'FACTURA��O ACORDO'.","OK","",64)
				RETURN .F.
			ENDIF 
		ENDIF 
	ENDIF 
	**
	

	
	**Verificar se existem linhas com Total = 0	
	select fi
	Calculate cnt() to lcCntLin for fi.etiliquido=0
	If lcCntLin>0 AND !llAtend
		IF !uf_perguntalt_chama("Existem linhas com total a Zero. Pretende continuar com a grava��o?","SIM","N�O",32)
			RETURN .f.
		ENDIF 
	ENDIF 
	
	lcCntLin=0
	**
	



	** validar n� atendimento
	SELECT ft
	IF EMPTY(ALLTRIM(ft.u_nratend))	AND TYPE("ATENDIMENTO") == "U" 
		uf_atendimento_geraNrAtendimento()
	ELSE
		IF VARTYPE(nratendimento) == "U"
			uf_atendimento_geraNrAtendimento()
		ELSE
			replace ft.u_nratend WITH ALLTRIM(nratendimento)
		ENDIF 
	ENDIF 
	**
		
	** Validar preenchimento da Loja e do terminal **
	SELECT ft
	IF EMPTY(ft.site)
		replace site	WITH ALLTRIM(mySite) IN ft
	ENDIF
	
	IF EMPTY(ft.pnome)
		replace pnome	WITH ALLTRIM(myTerm) IN ft
		replace pno		WITH myTermNo IN ft
	ENDIF
	**

	
	** Validar preenchimento dados do operador **
	IF EMPTY(ft.vendedor)
		replace vendedor	WITH  ch_vendedor
		replace vendnm		WITH  ALLTRIM(ch_vendnm)
	ENDIF
	**
	


	
	** Validar se � necess�rio correr o resto das valida��es
	Select Td
	If Td.u_tipodoc == 5 OR  Td.u_tipodoc == 13&& n�o correr c�digo de valida��es para a factura resumo, nem para proformas
		Return .t.
	ENDIF
	If td.tipodoc == 3 && n�o correr c�digo de valida��es para a notas de cr�dito
		
		&& Verifica se � uma nota de credito a entidades, n�o pemite linhas com valores e sem liga��o
		select ucrsLojas
		lcInfarmed = ucrsLojas.infarmed && Valida��o apenas para farm�cias
		Select ft
		IF Ft.no < 199 AND !EMPTY(lcInfarmed) AND UPPER(ALLTRIM(ucrse1.pais)) == 'PORTUGAL'

			Select Fi
			LOCATE FOR EMPTY(ALLTRIM(fi.ofistamp)) AND (!EMPTY(ALLTRIM(fi.ref)) OR !EMPTY(fi.etiliquido))
			IF FOUND()
				uf_perguntalt_chama("Existem linhas sem liga��o ao documento de origem. Verifique pf.","OK","",64)
				RETURN .f.
			ENDIF 		
		ENDIF 
		
		Return .t.
	ENDIF
	
	

	
		
	
	If Td.u_tipodoc == 12 && n�o correr c�digo de valida��es para as Guias de Transporte
		Return .t.
	ENDIF

	IF uf_gerais_getParameter("ADM0000000211","BOOL") AND ALLTRIM(ucrse1.tipoempresa)!='FARMACIA'&& n�o correr c�digo de valida��es para clientes Tipo Armaz�m
		** Lotes
		IF  uf_gerais_getParameter("ADM0000000215","BOOL") == .f.
			&& Verifica Informa��o da venda (contra-indica��es, validades, interac��es)
			IF !uf_facturacao_validaStockLotes()
				RETURN .F.
			ENDIF	
		ENDIF
		

		
		** INCREMENTAR CONTADOR PSICO/BENZO 	
		SELECT fi
		GO Top
		LOCATE FOR (!EMPTY(fi.u_psico) OR !EMPTY(fi.u_benzo)) AND !EMPTY(ALLTRIM(fi.ref))
			IF FOUND()
				SELECT Td
				IF td.lancasl && so valida psico se o documento mexer em stock
					uf_PAGAMENTO_ContadorPsicoBenzo()
				ENDIF
			ENDIF
			
		Return .t.
		
	ENDIF
	


	
	** CRIAR CURSOR COM DADOS DE COMPARTICIPA��O DA VENDA **
	IF !Empty(lcCodigo) && se existir comparticipa��o
		TEXT TO lcSql NOSHOW TEXTMERGE
			exec up_receituario_dadosDoPlano '<<lcCodigo>>'
		ENDTEXT
		uf_gerais_actGrelha("","uCrsCodPla",lcSql)
		If Reccount([uCrsCodPla])=0
			uf_perguntalt_chama("ESTE C�DIGO N�O EXISTE NA TABELA DE PLANOS DE COMPARTICIPA��O. RECTIFIQUE O MESMO.","OK","",64)			
			Return .F.
		ENDIF
	ENDIF 
	
		
	LOCAL lcTipoReceita
	lcTipoReceita = "RM"
	
	
	**

	*******************************************************************************
	*** VERIFICAR SE O DOCUMENTO TEM PLANO SEM PRODUTOS MARCADOS OU VICE-VERSA	***
	*** VALIDAR APLICA��O DOS DIPOLOMAS    										***
	*** VALIDAR VENDA DE PRODUTOS PROTOCOLO										***
	*******************************************************************************


	*** VERIFICAR SE A VENDA CONTEM PRODUTOS PROTOCOLO E N�O PROTOCOLO ***

	
	IF myFtAlteracao
	

	
		SELECT fi 
		GO TOP
		SCAN
			lcRef=fi.ref
			
			
			if(ALLTRIM(Fi.tipoR) == "RS" OR  ALLTRIM(Fi.tipoR) == "RSP") &&OR ALLTRIM(Fi.DEM) == .T.)
				lcTipoReceita  = "RSP"
			endif
			
			
			
			IF fi.u_comp
				TEXT TO lcSql NOSHOW TEXTMERGE
					Select 
						protocolo 
					FROM 
						fprod (nolock) 
					WHERE 
						cnp='<<ALLTRIM(lcRef)>>'
				ENDTEXT
				IF uf_gerais_actGrelha("","uCrsFprod",lcSql)
					If Reccount("uCrsFprod")>0
						IF uCrsFprod.protocolo==.t.
							lcCntLin=lcCntLin + 1
						Else
							lcCntLin2=lcCntLin2 + 1
						EndIf
					ENDIF
					FECHA([uCrsFProd])
				ELSE
					uf_perguntalt_chama("ATEN��O, OCORREU UM ERRO A VALIDAR CARACTERISTICAS DO PRODUTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
					RETURN .F.
				ENDIF
			EndIf
		ENDSCAN
		
	
		
		** no caso das insercoes de receita no recreserva usa o cursor auxiliar
		** :: ::TODO Planos Combinados
		IF(USED("FIinsercaoReceita"))	
			
			IF(RECCOUNT("FIinsercaoReceita")>0)	
			
			
				SELECT FIinsercaoReceita
				GO TOP
				SCAN
					lcRef=FIinsercaoReceita.ref
					
					
					if(ALLTRIM(FIinsercaoReceita.tipoR) == "RS" OR  ALLTRIM(FIinsercaoReceita.tipoR) == "RSP")
						lcTipoReceita  = "RSP"
					endif
					
					
					
					IF FIinsercaoReceita.u_comp
						TEXT TO lcSql NOSHOW TEXTMERGE
							Select 
								protocolo 
							FROM 
								fprod (nolock) 
							WHERE 
								cnp='<<ALLTRIM(lcRef)>>'
						ENDTEXT
						IF uf_gerais_actGrelha("","uCrsFprod",lcSql)
							If Reccount("uCrsFprod")>0
								IF uCrsFprod.protocolo==.t.
									lcCntLin=lcCntLin + 1
								Else
									lcCntLin2=lcCntLin2 + 1
								EndIf
							ENDIF
							FECHA([uCrsFProd])
						ELSE
							uf_perguntalt_chama("ATEN��O, OCORREU UM ERRO A VALIDAR CARACTERISTICAS DO PRODUTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
							RETURN .F.
						ENDIF
					EndIf
				ENDSCAN

				
			ENDIF
			
			
		ENDIF
		
		
		
		**validar no cursor auxiliar caso das das inser��es de receita 
		

		
		
	ENDIF 
	IF myFtIntroducao
		SELECT fi
		GO TOP
		SCAN
			SELECT uCrsAtendST
			LOCATE FOR UPPER(ALLTRIM(uCrsAtendST.ref)) == UPPER(ALLTRIM(fi.ref))  
			
			if(ALLTRIM(Fi.tipoR) == "RS" OR ALLTRIM(Fi.tipoR) == "RSP")
				lcTipoReceita  = "RSP"
			endif
			IF(!EMPTY(uCrsAtendST.ref))
				IF uCrsAtendST.protocolo = .T.
					lcCntLin = lcCntLin + 1
				ELSE 
					lcCntLin2 = lcCntLin2 + 1
				ENDIF
			ENDIF
		ENDSCAN
		
		
		
		
		** no caso das insercoes de receita no recreserva usa o cursor auxiliar
		** ::Planos Combinados
		IF(USED("FIinsercaoReceita"))	
			
			IF(RECCOUNT("FIinsercaoReceita")>0)	
			
				SELECT FIinsercaoReceita
				GO TOP
				SCAN
					SELECT uCrsAtendST
					LOCATE FOR UPPER(ALLTRIM(uCrsAtendST.ref)) == UPPER(ALLTRIM(FIinsercaoReceita.ref))   
					
					if(ALLTRIM(FIinsercaoReceita.tipoR) == "RS" OR ALLTRIM(FIinsercaoReceita.tipoR) == "RSP")
						lcTipoReceita  = "RSP"
					endif
					
					IF(!EMPTY(uCrsAtendST.ref))
						IF uCrsAtendST.protocolo = .T.
							lcCntLin = lcCntLin + 1
						ELSE 
							lcCntLin2 = lcCntLin2 + 1
						ENDIF
					ENDIF	
				ENDSCAN
			
			
			ENDIF
			
		ENDIF
		
	ENDIF
	
	
	
		
	** ::TODO Planos Combinados
	IF (lcCntLin>0 and lcCntLin2>0) AND ALLTRIM(lcTipoReceita)!= "RSP" 
		uf_perguntalt_chama("ATEN��O: NA MESMA VENDA N�O PODE VENDER PRODUTOS PROTOCOLO E N�O PROTOCOLO."+chr(10)+chr(10)+"POR FAVOR RECTIFIQUE A VENDA.","OK","",48)
		RETURN .F.
	ENDIF 
	**


	** validar diplomas e produtos marcados para comparticipa��o **
	IF !EMPTY(lcCodigo)
		SELECT fi
		GO TOP
		SCAN
			** fix p n�o guardar lixo qd o produto � lyrica e CFT 2.10 -  a informa��o vem como diploma da SPMS, usamos como diploma para calculo de comparticipa��es, mas n�o gravamos como diploma
			IF UPPER(ALLTRIM(fi.u_diploma)) == "CFT 2.10"
				replace fi.u_diploma WITH ""
			ENDIF
						
			IF fi.u_comp
				validado=.T.
			ENDIF

			** VERIFICA SE O PLANO PERMITE DIPLOMA 
			IF uCrsCodPla.diploma="0" AND !EMPTY(fi.u_diploma)
				uf_perguntalt_chama("ATEN��O: O PLANO '["+UPPER(alltrim(lcCodigo))+"] "+UPPER(alltrim(uCrsCodPla.design))+"' N�O PERMITE DIPLOMAS ASSOCIADOS."+chr(10)+"RECTIFIQUE A VENDAS ANTES DE CONTINUAR.","OK","",64)
				Return .F.
			ENDIF
			
			**
		ENDSCAN 

		IF validado=.f.
			uf_perguntalt_chama("ATEN��O: TEM UM PLANO ASSOCIADO � VENDA, MAS N�O TEM PRODUTOS MARCADOS PARA COPARTICIPA��O."+chr(10)+chr(10)+"POR FAVOR RECTIFIQUE ANTES DE CONTINUAR.","OK","",64)
			Return .F.
		ENDIF

	ELSE
		SELECT FI
		GO TOP
		SCAN
			IF fi.u_comp=.t.
				uf_perguntalt_chama("ATEN��O: ESTA VENDA N�O TEM PLANO, MAS TEM PRODUTOS MARCADOS PARA COMPARTICIPA��O."+chr(10)+chr(10)+"POR FAVOR RECTIFIQUE ANTES DE CONTINUAR.","OK","",64)
				Return .F.
			ENDIF
		ENDSCAN
	ENDIF
	******
	

	

	** validar plano com produtos protocolo **
	If lcCntLin>0 and lcCntLin2=0
		IF USED("uCrsCodPla")
			&&::TODO Planos combinados
			IF !uCrsCodPla.protocolo AND lcTipoReceita!='RSP'
				uf_perguntalt_chama("ATEN��O: SE A VENDA CONTEM PRODUTOS PROTOCOLO, O PLANO TAMB�M TEM DE SER PARA PROTOCOLO."+chr(10)+chr(10)+"POR FAVOR RECTIFIQUE ANTES DE CONTINUAR.","OK","",64)
				Return .F.
			Else
				** MARCAR O DOCUMENTO COMO VENDA DE PRODUTOS PROTOCOLO **
				Select ft && rever este ponto (esta a ser aplicado no front office??)
				replace ft.segmento	with	"protocolo"
			ENDIF
		ENDIF
	ENDIF 

	Store 0 to lcCntLin, lcCntLin2


	*** VERIFICA��O DE DADOS DO PLANO, N�s DE BENEFICI�RIO, N� DE RECEITA 
	*** VERIFICA��O DO N� M�XIMO DE PRODUTOS/EMBALAGENS/PRODUTOS UNIDOSE POR PLANO DE COMPARTICIPA��O 
	IF !Empty(lcCodigo)
		lnMaxEmb = Iif(Reccount([uCrsCodPla])>0,uCrsCodPla.MaxEmbMed,0)
		lnMaxUni = Iif(Reccount([uCrsCodPla])>0,uCrsCodPla.MaxembUni,0)
		
		Create Cursor uCrsFi (ref C(18), qtt N(12,3), u_ettent1 N(15,3), u_ettent2 N(15,3))
		
		Select fi
		Go top
		Scan
			lcRef = Iif(!Empty(fi.ref),fi.ref,fi.oref)
			If !Empty(lcRef) AND fi.partes == 0 AND EMPTY(fi.id_validacaodem)
				Insert INTO uCrsFi (ref,qtt,u_ettent1,u_ettent2) VALUES (lcRef,fi.qtt,fi.u_ettent1,fi.u_ettent2)
			EndIf
		ENDSCAN
		
		*** O Cursor uCrsGrpFi tem as Refer�ncias e as Somas das quantidades Agrupadas ***
		*** Apenas medicamentos com comparticipa��o, ie, u_ettent1 > 0				   ***
		
		IF Reccount([uCrsFi])>0
			Select ref, Sum(qtt) ct FROM uCrsFi WHERE (u_ettent1+u_ettent2)>0 GROUP BY ref INTO CURSOR uCrsGrpFi READWRITE
			Select uCrsGrpfi
			Calculate Sum(ct) TO lnLin
		Else
			lnLin = 0
		ENDIF
		
		fecha([uCrsFi])
	
		**
		IF USED("uCrsVerificaRespostaDEM")
			IF ALLTRIM(uCrsVerificaRespostaDEM.receita_tipo) != 'RSP'
				lcAux = .t.
			ELSE
				lcAux = .f.
			ENDIF
		ELSE
			lcAux = .t.
		ENDIF
	
		IF lnLin > uCrsCodPla.maxembrct AND !EMPTY(lcAux) 
			IF !uf_perguntalt_chama("O n� m�ximo de embalagens do plano foi excedido. " + Chr(13)+CHR(10)+Chr(13)+CHR(10)+;
						"N.� de Embalagens: "+astr(lnLin)+"; M�ximo do Plano: "+astr(uCrsCodPla.maxembrct) + "." + chr(13)+CHR(10)+Chr(13)+CHR(10)+;
						"Se continuar o verso de receita ser� impresso em tal�o.","Continuar","Corrigir",64)
				RETURN .F.
			ENDIF 
		ENDIF 
		
		IF USED("uCrsGrpFi")
			Select uCrsGrpFi
			SCAN
				IF myFtAlteracao
					** Verificar se o produto � unidose ou n�o
					uf_gerais_actGrelha("","uCrstipemb",[select tipembdescr, psico from fprod (nolock) where cnp=']+Alltrim(uCrsGrpFi.ref)+['])
					If Reccount("uCrstipemb")>0
						SELECT uCrstipemb
						SCAN
							** Se o produto for N�o Unidose...
							IF uCrstipemb.tipembdescr='Embalagem N�o Unit�ria' AND uCrstipemb.psico=.f. AND uCrsGrpFi.ct > lnMaxemb AND lnMaxEmb>0
								uf_perguntalt_chama("O PLANO DE COMPARTICIPA��O '"+UPPER(lcCodigo)+"' N�O PERMITE PARA O ARTIGO '"+Alltrim(uCrsgrpFi.ref)+"' UM N�MERO DE EMBALAGENS SUPERIOR A "+astr(lnMaxemb)+". NO ENTANTO NESTA VENDA EXISTEM "+Astr(uCrsGrpFi.ct)+" REFERENTES A ESTE PRODUTO.","OK","",64)
								fecha([uCrsGrpFi])
								SELECT fi
								Return .F.
							ENDIF
							** Se o produto for Unidose...
							IF (uCrstipemb.tipembdescr='Embalagem Unit�ria' OR uCrstipemb.psico=.t.) AND uCrsGrpFi.ct > lnMaxUni AND lnMaxEmb>0
								uf_perguntalt_chama("O PLANO DE COMPARTICIPA��O '"+lcCodigo+"' N�O PERMITE PARA O ARTIGO '"+Alltrim(uCrsgrpFi.ref)+"' UM N�MERO DE EMBALAGENS SUPERIOR A "+astr(lnMaxUni)+". NO ENTANTO NESTA VENDA EXISTEM "+Astr(uCrsGrpFi.ct)+" REFERENTES A ESTE PRODUTO.","OK","",64)
								fecha([uCrsGrpFi])
								SELECT fi
								Return .F.
							ENDIF
						ENDSCAN
					ENDIF
					
					If Used("uCrstipemb")
						fecha("uCrstipemb")
					EndIf
				ENDIF 
				
				IF myFtIntroducao
					** Verificar se o produto � unidose ou n�o
					SELECT uCrsAtendST
					LOCATE FOR ALLTRIM(UPPER(uCrsAtendST.ref)) == UPPER(Alltrim(uCrsGrpFi.ref))
					
					** Se o produto for N�o Unidose...
					IF uCrsAtendST.tipembdescr='Embalagem N�o Unit�ria' AND uCrsAtendST.psico=.f. AND uCrsGrpFi.ct > lnMaxemb AND lnMaxEmb>0
						uf_perguntalt_chama("O PLANO DE COMPARTICIPA��O '"+UPPER(lcCodigo)+"' N�O PERMITE PARA O ARTIGO '"+Alltrim(uCrsgrpFi.ref)+"' UM N�MERO DE EMBALAGENS SUPERIOR A "+astr(lnMaxemb)+". NO ENTANTO NESTA VENDA EXISTEM "+Astr(uCrsGrpFi.ct)+" REFERENTES A ESTE PRODUTO.","OK","",64)
						fecha([uCrsGrpFi])
						SELECT fi
						Return .F.
					ENDIF
					** Se o produto for Unidose...
					IF (uCrsAtendST.tipembdescr='Embalagem Unit�ria' OR uCrsAtendST.psico=.t.) AND uCrsGrpFi.ct > lnMaxUni AND lnMaxEmb>0
						uf_perguntalt_chama("O PLANO DE COMPARTICIPA��O '"+lcCodigo+"' N�O PERMITE PARA O ARTIGO '"+Alltrim(uCrsgrpFi.ref)+"' UM N�MERO DE EMBALAGENS SUPERIOR A "+astr(lnMaxUni)+". NO ENTANTO NESTA VENDA EXISTEM "+Astr(uCrsGrpFi.ct)+" REFERENTES A ESTE PRODUTO.","OK","",64)
						fecha([uCrsGrpFi])
						SELECT fi
						Return .F.
					ENDIF
				ENDIF
			ENDSCAN
		
			fecha([uCrsGrpFi])
		ENDIF
	ENDIF
	

	
	** VALIDAR N� DE RECEITA, DE BENEFICI�RIO, LOCAL DE PRESCRI��O E PRESCRITOR 
	LOCAL lcNb, lcNb2
	STORE '' TO lcNb, lcNb2
	IF type("recreserva")=='U' &&Adi��o verifca��o vendas suspensas 29-01-2018 JC
		IF !EMPTY(lcCodigo) AND (!ft.cobrado OR (ft.cobrado AND ft2.u_vdsusprg)) && So para Vendas com receita e n�o suspensas
			*** Verificar se Obriga a N.� Beneficiario ***
			If uCrsCodPla.beneficiario AND Empty(ft2.U_NBENEF2) &&Adi��o verifca��o vendas suspensas 29-01-2018 JC
				lcNb=INPUTBOX("QUAL O N� DE BENEFICI�RIO PARA ENTIDADE " +ALLTRIM(uCrsCodPla.cptorgabrev),"Falta N� de Benefici�rio.","")
				IF Empty(lcNb)
					uf_perguntalt_chama("TEM QUE INTRODUZIR O N� DE BENEFICI�RIO.","OK","",64)
					Return .F.
				ELSE

					select ft2
					REPLACE ft2.u_nbenef2 WITH lcNb
					
					lcNb = ""
				ENDIF
			ENDIF
			
			**If uCrsCodPla.beneficiario2 AND Empty(ft2.U_NBENEF2)
			**	lcNb2=INPUTBOX("QUAL O N� DE BENEFICI�RIO PARA A ENTIDADE " +ALLTRIM(uCrsCodPla.cptorgabrev2),"Falta N� de Benefici�rio.","")
			**	IF Empty(lcNb2)
			**		uf_perguntalt_chama("TEM QUE INTRODUZIR O N� DE BENEFICI�RIO.","OK","",64)
			**		Return .F.
			**	ELSE
			**		SElect ft2
			**		REPLACE ft2.u_nbenef2 WITH lcNb2
			**		
			**		lNb2 = ""
			**	ENDIF
			**ENDIF
		ENDIF 

		*** Controlo de Receita ***
		IF EMPTY(ft2.u_receita) OR (LEN(ALLTRIM(ft2.u_receita)) != 13 AND LEN(ALLTRIM(ft2.u_receita)) != 19)
		&&IF EMPTY(ft2.u_receita)
			*LOCAL lcValidaNrReceita, lcObrigaNrReceita, lcObrigaNrReceitaADSE

			TEXT TO lcSql NOSHOW TEXTMERGE
				exec up_receituario_dadosDoPlano '<<lcCodigo>>'
			ENDTEXT
			uf_gerais_actGrelha("","uCrsCodPla",lcSql)

			** SE OBRIGA N� DE RECEITA
				IF !empty(uCrsCodPla.obriga_nrReceita)
					SELECT ft2
					lcNb = ''
					
					lcNb = inputbox("Qual o n�mero de receita?", "O n�mero de receita tem de ter 13 ou 19 caracteres", "")
					IF EMPTY(lcNb) OR (LEN(ALLTRIM(lcNb)) != 13 AND LEN(ALLTRIM(lcNb)) != 19)
					&&IF EMPTY(lcNb)
						IF uCrsCodPla.obriga_nrReceita == 2
							uf_perguntalt_chama("Tem que introduzir um n�mero de receita v�lido(13 ou 19 caracteres).", "OK", "", 48)
							RETURN .F.
						ENDIF
					ELSE
						select ft2
						REPLACE ft2.u_receita WITH lcNb
					
						lcNb = ""
					
						IF !llAtend 
							Facturacao.ContainerCab.receita.refresh
						ENDIF 
					ENDIF			
				ENDIF			
			**ENDIF 
		ENDIF
		
		
		** Controlo do c�digo de Local de prescri��o e do prescritor ***
			** codigo local de prescri��o
		IF EMPTY(ft2.u_entpresc)
			LOCAL lcObrigaLocalPresc &&, lcObrigaLocalPrescADSE
			
			IF uf_gerais_getParameter("ADM0000000001","BOOL")
				lcObrigaLocalPresc = .t.

			ENDIF
			
			IF lcObrigaLocalPresc
				SELECT ft2
				*IF (lcObrigaLocalPrescAdse == .t. AND ( UPPER(ALLTRIM(ft2.u_abrev))=='ADSE' OR UPPER(ALLTRIM(ft2.u_abrev2))=='ADSE' )) OR lcObrigaLocalPrescAdse == .f. 
					lcNb = ''
					lcNb = inputbox("Qual o c�digo do local de prescri��o?","Falta C�digo do Local de Prescri��o.", "")
					IF EMPTY(lcNb)
						uf_perguntalt_chama("Tem que introduzir o c�digo do local de prescri��o.", "OK", "", 48)
						RETURN .F.
					ELSE
						select ft2
						REPLACE ft2.u_entpresc WITH lcNb
						
						lcNb = ""
					ENDIF
				*ENDIF
			ENDIF
		ENDIF
		
		** c�digo prescritor		
		IF EMPTY(ft2.u_nopresc)
			LOCAL lcObrigaCodPresc &&, lcObrigaCodPrescADSE
			
			IF uf_gerais_getParameter("ADM0000000002","BOOL")
				lcObrigaCodPresc= .t.				
			ENDIF
			
			IF lcObrigaCodPresc && caso seja obrigatorio o preenchimento do c�digo de prescritor
				SELECT ft2
				*IF (lcObrigaCodPrescADSE == .t. AND ( UPPER(ALLTRIM(ft2.u_abrev))=='ADSE' OR UPPER(ALLTRIM(ft2.u_abrev2))=='ADSE' )) OR lcObrigaCodPrescADSE == .f. 
					lcNb = ''
					lcNb = inputbox("Qual o c�digo do prescritor?", "Falta C�digo do Prescritor.", "")
					IF EMPTY(lcNb)
						uf_perguntalt_chama("Tem que introduzir o c�digo do prescritor.", "OK", "", 48)
						RETURN .F.
					ELSE
						select ft2
						REPLACE ft2.u_nopresc WITH lcNb
						
						lnNb = ""
					ENDIF
				*ENDIF
			ENDIF
		ENDIF
	ENDIF 
	


	** CORRER AS LINHAS E VALIDAR :
	** GEN�RICOS DE MEDICAMENTOS, CONTRA-INDICA��ES,
	** ACTUALIZAR DADOS DE FARM�CIA NAS LINHAS
	


	IF Used("uCrsCodPla")
		fecha([uCrsCodPla])
	ENDIF 

	** 2� FASE DAS VALIDA��ES 
	LOCAL llRetVal, lnPsi, lnTot, lnBen
	llRetVal=.T.		&& Variavel para Retorno
	lnPsi=0				&& Contador Psicotr�picos da Venda
	lnTot=0				&& Totalizador Linhas com Refer�ncia
	lnBen=0				&& Contador Benzodizepinas da Venda

	** VERIFICAR SE EXISTEM PSICO (REFs.) DIFERENTES NA MESMA VENDA 
	lnCntLin = 0
	SELECT fi
	GO TOP
	SCAN
		IF FI.u_psico
			** validar limites de quantidades **
			IF (fi.qtt > 4)
				uf_perguntalt_chama("N�O PODE TER UMA QUANTIDADE NA LINHA SUPERIOR A 4 PARA O PRODUTO: " + ALLTRIM(fi.design),"OK","",64)
				RETURN .F.
			ENDIF

			lnCntLin = lnCntLin + 1
			lnPsi = lnPsi+1	&& Incremento do Contador de Psicotr�picos da Venda
		ENDIF					
		
		IF fi.u_benzo
			lnBen = lnBen + 1
		ENDIF 
		
		IF !fi.epromo
			lnTot = lnTot+1 && Incremento do Contador de produtos da Venda
		ENDIF
	ENDSCAN
	
	lnCntLin = 0

	** VALIDAR SE A VENDA N�O CONTEM PRODUTOS PSICO E N�O PSICO - apenas aplic�vel em receitas manuais
	llRetVal = !(lnPsi > 0 AND lnPsi <> lnTot) 
	
	SELECT ft2
	IF EMPTY(ft2.token)
		IF !llRetVal
			uf_perguntalt_chama("N�O PODEM EXISTIR PSICOTR�PICOS E N�O PSICOTR�PICOS NA MESMA VENDA.","OK","",64)
			Return .f.
		ENDIF
	ENDIF
	

	** INCREMENTAR CONTADOR PSICO/BENZO 
	IF lnPsi > 0 OR lnBen > 0
		SELECT Td
		IF td.lancasl && so valida psico se o documento mexer em stock
			uf_PAGAMENTO_ContadorPsicoBenzo()
		ENDIF
	ENDIF

	** OBRIGAR O PREENCHIMENTOS DOS DADOS PSICOTR�PICO 
	IF td.lancasl && so valida psico se o documento mexer em stock
		IF lnPsi>0
			SELECT dadosPsico
			GO TOP
			
			SELECT ft2
			IF !EMPTY(ALLTRIM(ft2.u_codigo))
				IF uf_gerais_getDate(dadosPsico.u_recdata,"SQL") == "19000101"
					uf_perguntalt_chama("O CAMPO: DATA DA RECEITA, NOS DADOS PSICOTR�PICOS � DE PREENCHIMENTO OBRIGAT�RIO PARA VENDAS COM RECEITA.","OK","",64)
					facturacao.lbl5.click()
					RETURN .F.
				ENDIF 
			ENDIF 

         IF EMPTY(ft2.u_nbenef2)
            uf_perguntalt_chama("O CAMPO N� BENEFICI�RIO 2 � DE PREENCHIMENTO OBRIGAT�RIO PARA VENDAS COM RECEITA.","OK","",64)
            facturacao.lbl2.click()
            RETURN .F.
         ENDIF
			
			IF EMPTY(ALLTRIM(dadosPsico.u_cputavi)) OR EMPTY(ALLTRIM(dadosPsico.u_cputdisp)) OR uf_gerais_getDate(dadosPsico.u_ddutavi,"SQL") == "19000101";
				OR (dadosPsico.u_idutavi>130 OR dadosPsico.u_idutavi<1) OR EMPTY(ALLTRIM(dadosPsico.u_medico)) OR EMPTY(ALLTRIM(dadosPsico.u_moutavi));
				OR EMPTY(ALLTRIM(dadosPsico.u_moutdisp)) OR EMPTY(ALLTRIM(dadosPsico.u_ndutavi)) OR EMPTY(ALLTRIM(dadosPsico.u_nmutavi));
				OR EMPTY(ALLTRIM(dadosPsico.u_nmutdisp))
				
				uf_perguntalt_chama("N�O PREENCHEU TODOS OS CAMPOS RELATIVOS AOS DADOS DE PSICOTR�PICOS.","OK","",64)
				facturacao.lbl5.click()
				RETURN .F.
			ENDIF 
		ENDIF
	ENDIF
	**
	


	IF !USED("uCrsAtendCLPato")
		SELECT ft
		
		lcSQL = ''
		TEXT TO lcSQL TEXTMERGE NOSHOW
			declare @clstamp char(25)
			select @clstamp=utstamp from b_utentes where no=<<ft.no>> and estab=<<ft.estab>>
			exec up_clientes_patologias @clstamp
		ENDTEXT 
		uf_gerais_actGrelha("","uCrsAtendCLPato",lcSQL)
	ENDIF 
	
	&& Limpar Cursor uCrsAtendST
	local lcValRefExiste
	STORE .f. TO lcValRefExiste
	
	SELECT uCrsAtendST
	GO TOP 
	SCAN 
		lcValRefExiste = .f.
		SELECT fi
		GO TOP 
		SCAN
			IF ALLTRIM(uCrsAtendST.Ststamp) == ALLTRIM(fi.fistamp)
				lcValRefExiste = .t.
			ENDIF
		ENDSCAN
		IF !lcValRefExiste
			SELECT uCrsAtendST
			DELETE
		ENDIF
	ENDSCAN
	SELECT uCrsAtendST
	GO TOP
	
	Select fi
	GO TOP 
	LOCATE FOR !EMPTY(fi.dem) 
	IF FOUND()
		SELECT Ft2
		IF EMPTY(FT2.U_CODIGO)
			uf_perguntalt_chama("O preenchimento do Plano � obrigat�rio para linhas de Dispensa Eletr�nica de Medicamentos.","OK","",32)
			RETURN .f.
		ENDIF 
	ENDIF 
	
	



	&& Verifica Informa��o da venda (contra-indica��es, validades, interac��es)
	IF !llAtend

		IF !EMPTY(ft2.token)
			&&Valida��o receitas DEM
			uf_atendimento_validacaoReceitasDEM(.t.,ft2.u_receita,ft2.codAcesso, ft2.codDirOpcao, ft2.token)

		ENDIF
	
		IF !uf_infoVenda_valida()
			RETURN .F.
		ENDIF	
	ENDIF	
	

	** Valida Efetiva��o de Receita ** 
	Select fi
	GO TOP 
	LOCATE FOR !EMPTY(fi.dem) 
	IF FOUND()
		LOCAL lcValidaEfetivacao
		STORE .f. TO  lcValidaEfetivacao
		
		** Efectiva��o Receitas DEM		
		** valida se  o cursor ucrsDemEfetivadaexiste e valida se deve efectivar
		** n�o efectiva se j� tiver sido efectivada com sucesso
		
		IF(USED("ucrsDemEfetivada"))
		
			SELECT ucrsDemEfetivada
			GO TOP
			LOCATE FOR ALLTRIM(ucrsDemEfetivada.token)==ALLTRIM(fi.token)
			IF FOUND()
				lcValidaEfetivacao=.t.
			ELSE
				lcValidaEfetivacao=.f.		
			ENDIF				
		ELSE	
			lcValidaEfetivacao = .f.							
		ENDIF

		
		
		if(lcValidaEfetivacao==.f.)
			
			IF EMPTY(uf_atendimento_receitasEfetivar(.t.,ft2.u_receita, ft2.codAcesso, ft2.codDirOpcao, ft2.token))
				RETURN .f.
			ENDIF 
			
		ENDIF

	ENDIF 
	
	

	
	Return .T.
ENDFUNC


**
FUNCTION uf_facturacao_importarDadosPsicoCL
	
	SELECT dadosPsico
	GO TOP 
	replace dadosPsico.u_cputavi  WITH ALLTRIM(ft.codpost)
	replace dadosPsico.u_cputdisp WITH ft.codpost 
	replace dadosPsico.u_ddutavi  WITH ft.bidata
	replace dadosPsico.u_moutavi  WITH ALLTRIM(ft.morada)
	replace dadosPsico.u_moutdisp WITH ALLTRIM(ft.morada)
	replace dadosPsico.u_ndutavi  WITH ft.bino
	replace dadosPsico.u_nmutavi  WITH ALLTRIM(ft.nome)
	replace dadosPsico.u_nmutdisp WITH ALLTRIM(ft.nome)
    replace dadosPsico.codigoDocSPMS WITH uf_gerais_getUmValor("b_utentes", "codigoDocSPMS", "no = " + ASTR(ft.no) + " AND estab = " + ASTR(ft.estab))
	
	IF USED("uCrsVerificaRespostaDem")
		SELECT uCrsVerificaRespostaDem
		replace dadosPsico.u_recdata WITH uCrsVerificaRespostaDem.receita_data
		replace dadosPsico.u_medico  WITH ALLTRIM(uCrsVerificaRespostaDEM.medico_codigo)
	ENDIF

	facturacao.refresh
ENDFUNC


** Update Ft
Function uf_facturacao_actualizaFt
	LOCAL lcSql, lcExecuteSQL
	STORE "" TO lcExecuteSQL, lcSql
	
	SELECT Ft
	SET HOURS TO 24
	Atime=ttoc(datetime()+(difhoraria*3600),2)
	Astr=left(Atime,8)
	myHora = Astr
	**myHora	= Time() && � necess�rio remover os segundos durante a inser��o na tabela
	myInvoiceData = uf_gerais_getDate(ft.fdata,"SQL")
	
	
	** NOTAS:
	** Bilhete de identidade : ft.bidata, ft.bino, ft.bilocal
	** o campo ftid � incrementado automaticamente pela BD	
	Text to lcSql noshow textmerge
		Update	Ft
		Set		pais = <<Ft.pais>>,nmdoc = '<<Alltrim(Ft.NmDoc)>>', fno =<<Ft.fno>>, [no] = <<Ft.no>>, 
				nome = '<<Alltrim(Ft.nome)>>' , morada = '<<Alltrim(ft.morada)>>',
				[local] = '<<Alltrim(ft.local)>>', codpost = '<<Alltrim(ft.codpost)>>', ncont = '<<Alltrim(ft.ncont)>>',
				bino = '<<Alltrim(ft.bino)>>', bidata = '<<IIF(empty(ft.bidata),'19000101',uf_gerais_getDate(ft.bidata,"SQL"))>>',
				bilocal = '<<Alltrim(ft.bilocal)>>', telefone = '<<Alltrim(ft.telefone)>>',
				zona = '<<Alltrim(ft.zona)>>', vendedor =<<ch_vendedor>>, vendnm = '<<Alltrim(ch_vendnm)>>', fdata = '<<myInvoiceData>>', 
				ftano = year('<<myInvoiceData>>'),
				pdata = '<<Iif(td.u_tipodoc=9,uf_gerais_getDate(Ft.pdata,"SQL"),uf_gerais_getDate(datetime(),"SQL"))>>',
				carga = '<<ft.carga>>', descar = '<<ft.descar>>', saida = Left('<<Ft.saida>>',5),
				ivatx1 = <<ft.ivatx1>>, ivatx2 = <<ft.ivatx2>>, ivatx3 = <<ft.ivatx3>>,
				fin = <<ft.fin>>, ndoc = <<Ft.ndoc>>,
				moeda = '<<ft.moeda>>',fref = '<<ft.fref>>',
				ccusto = '<<ft.ccusto>>', ncusto = '<<ft.ncusto>>',
				facturada = <<IIF(ft.facturada,1,0)>>, fnoft = <<ft.fnoft>>, 
				nmdocft = '<<ft.nmdocft>>',estab = <<Ft.estab>>, cdata = '<<uf_gerais_getDate(ft.cdata,"SQL")>>',
				ivatx4 = <<ft.ivatx4>>,
				segmento = '<<ft.segmento>>',totqtt = <<Ft.TotQtt>>, qtt1 = <<Ft.Qtt1>>, qtt2 = <<ft.qtt2>>, 
				tipo = '<<ft.tipo>>',
				cobrado = <<IIF(Ft.cobrado,1,0)>>,cobranca = '<<ft.cobranca>>', 
				tipodoc = <<Ft.TipoDoc>>, chora = Left('<<Ft.chora>>',4),
				ivatx5 =  <<ft.ivatx5>>, ivatx6 = <<ft.ivatx6>>, ivatx7 = <<ft.ivatx7>>, 
				ivatx8 = <<ft.ivatx8>>, ivatx9 = <<ft.ivatx9>>,
				ivatx10 = <<ft.ivatx10>>, ivatx11 = <<ft.ivatx11>>,
				ivatx12 = <<ft.ivatx12>>, ivatx13 = <<ft.ivatx13>>, 
				cambiofixo = <<IIF(ft.cambiofixo,1,0)>>, memissao = '<<ft.memissao>>',
				cobrador = '<<ft.cobrador>>', rota = '<<ft.rota>>', multi = <<IIF(ft.multi,1,0)>>,
				cheque = <<IIF(ft.cheque,1,0)>>,clbanco = '<<ft.clbanco>>', clcheque = '<<ft.clcheque>>',
				chtotal = <<IIF(td.u_tipodoc!=9,ft.chtotal,0)>>,echtotal = <<IIF(td.u_tipodoc!=9,ft.echtotal,0)>>,
				custo = <<Ft.ecusto*200.482>>, 
				eivain1 = <<Ft.EivaIn1>>, eivain2 = <<Ft.EivaIn2>>,
				eivain3 = <<Ft.EivaIn3>>, eivav1 = <<Ft.EivaV1>>, eivav2 = <<Ft.EivaV2>>, eivav3 = <<Ft.EivaV3>>,
				ettiliq  =<<Ft.EttIliq>>, edescc = <<Ft.edescc>>, ettiva = <<Ft.EttIva>>, etotal = <<Ft.Etotal>>,
				eivain4 = <<Ft.EivaIn4>>, eivav4 = <<Ft.EivaV4>>, efinv = <<ft.efinv>>,ecusto = <<Ft.ecusto>>,
				eivain5 = <<Ft.EivaIn5>>, eivav5 = <<Ft.EivaV5>>, edebreg = <<ft.edebreg>>,
				eivain6 = <<ft.eivain6>>, eivav6 = <<ft.eivav6>>,eivain7 = <<ft.eivain7>>,eivav7 = <<ft.eivav7>>,
				eivain8 = <<ft.eivain8>>,eivav8 = <<ft.eivav8>>, eivain9 = <<ft.eivain9>>,eivav9 = <<ft.eivav9>>,
				eivain10 = <<ft.eivain10>>,eivav10 = <<ft.eivav10>>, eivain11 = <<ft.eivain11>>,eivav11 = <<ft.eivav11>>,
				eivain12 = <<ft.eivain12>>,eivav12 = <<ft.eivav12>>, eivain13 = <<ft.eivain13>>,eivav13 = <<ft.eivav13>>,
				total = <<Ft.Etotal*200.482>>,totalmoeda = <<ft.totalmoeda>>,ivain1 = <<Ft.EivaIn1*200.482>>,
				ivain2 = <<Ft.EivaIn2*200.482>>, ivain3 = <<Ft.EivaIn3*200.482>>, 
				ivain4 = <<Ft.EivaIn4*200.482>>, ivain5 = <<Ft.EivaIn5*200.482>>,
				ivain6 = <<ft.ivain6>>,
				ivain7 = <<ft.ivain7>>,ivain8 = <<ft.ivain8>>,
				ivain9 = <<ft.ivain9>>,ivav1 = <<Ft.EivaV1*200.482>>,
				ivav2 = <<Ft.EivaV2*200.482>>,ivav3 = <<Ft.EivaV3*200.482>>,
				ivav4 = <<Ft.EivaV4*200.482>>,ivav5 = <<Ft.EivaV5*200.482>>,ivav6 = <<ft.ivav6>>,
				ivav7 = <<ft.ivav7>>,ivav8 =  <<ft.ivav8>>,
				ivav9 = <<ft.ivav9>>, ttiliq = <<Ft.EttIliq*200.482>>,
				ttiva = <<Ft.EttIva*200.482>>,descc = <<ft.descc>>, debreg = <<ft.debreg>>,
				debregm = <<ft.debregm>>, intid = '<<ft.intid>>', nome2 = '<<ft.nome2>>',
				tpstamp = '<<ft.tpstamp>>', tpdesc = '<<ft.tpdesc>>', erdtotal = <<ft.erdtotal>>, rdtotal = <<ft.rdtotal>>,
				rdtotalm = <<ft.rdtotalm>>,
				cambio = <<IIF(td.u_tipodoc=9,1,0)>>, [site] = '<<ft.site>>', pnome = '<<ft.pnome>>',
				pno = <<ft.pno>>, cxstamp = '<<ft.cxstamp>>', cxusername = '<<ft.cxusername>>', ssstamp = '<<ft.ssstamp>>', ssusername = '<<ft.ssusername>>',
				anulado = <<IIF(ft.anulado,1,0)>>,
				virs = <<ft.virs>>, evirs = <<ft.evirs>>, valorm2 = <<ft.valorm2>>,
				--u_lote = '<<ft.u_lote>>', u_tlote = '<<ft.u_tlote>>', u_tlote2 = '<<ft.u_tlote2>>', u_ltstamp = '<<ft.u_ltstamp>>', u_slote = <<ft.u_slote>>, 
				--u_slote2 = <<ft.u_slote2>>, u_nslote = <<ft.u_nslote>>, u_nslote2 = <<ft.u_nslote2>>, u_ltstamp2 = '<<ft.u_ltstamp2>>',	u_lote2 = <<ft.u_lote2>>,
				usrinis = '<<m_chinis>>', usrdata = CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), usrhora = '<<myHora>>', u_nratend = '<<ft.u_nratend>>'
				,ft.datatransporte = '<<uf_gerais_getDate(ft.datatransporte,"SQL")>>'
				,ft.localcarga = '<<ALLTRIM(ft.localcarga)>>'
				,ft.id_tesouraria_conta = <<ft.id_tesouraria_conta>>
			Where 
				Ftstamp = '<<Alltrim(Ft.Ftstamp)>>'
	Endtext

	lcExecuteSQL  = lcExecuteSQL + CHR(13) + CHR(13) + lcSQL

	RETURN lcExecuteSQL  

ENDFUNC 


* Update Ft2
Function uf_facturacao_actualizaFt2
	LOCAL lcSql, lcExecuteSQL
	STORE "" TO lcExecuteSQL, lcSql
	
	** Notas: vdollocal = "Caixa"; vdlocal = "C"
	Text to lcSql noshow textmerge
		Update	Ft2
		Set		
				contacto = '<<ft2.contacto>>'
				,u_viatura  = '<<ALLTRIM(ft2.u_viatura)>>'
				,morada = '<<ft2.morada>>'
				,codpost = '<<ft2.codpost>>'
				,local = '<<ft2.local>>'
				,telefone = '<<ft2.telefone>>'
				,email = '<<ft2.email>>'
				,horaentrega = '<<ft2.horaentrega>>'
				,u_vddom = <<IIF(ft2.u_vddom,1,0)>>
				,obsdoc = '<<ft2.obsdoc>>'
				,u_receita = '<<Alltrim(FT2.u_receita)>>'
				,u_entpresc = '<<Alltrim(ft2.u_entpresc)>>'
				,u_nopresc = '<<Alltrim(ft2.u_nopresc)>>'
				,u_codigo = '<<Alltrim(Ft2.u_codigo)>>'
				,u_codigo2 = '<<Alltrim(Ft2.u_codigo2)>>'
				,u_nbenef = '<<Alltrim(ft2.u_nbenef)>>'
				,u_nbenef2 = '<<Alltrim(ft2.u_nbenef2)>>'
				,u_abrev = '<<Alltrim(Ft2.u_abrev)>>'
				,u_abrev2 = '<<Alltrim(Ft2.u_abrev2)>>'
				,c2no = <<ft2.c2no>>
				,c2estab = <<ft2.c2estab>>
				,subproc = '<<ft2.subproc>>'
				,u_vdsusprg = <<IIF(ft2.u_vdsusprg,1,0)>>
				,u_design = '<<Alltrim(Ft2.u_design)>>'
				,u_design2 = '<<Alltrim(Ft2.u_design2)>>'
				,c2nome = '<<ft2.c2nome>>'
				,c2pais = <<ft2.c2pais>>
				,evdinheiro = <<Iif(td.u_tipodoc!=9,ft2.evdinheiro,0)>>
				,epaga1 = <<Iif(td.u_tipodoc!=9,ft2.epaga1,0)>>
				,epaga2 = <<Iif(td.u_tipodoc!=9,ft2.epaga2,0)>>
				,u_docOrig = '<<ALLTRIM(ft2.u_docorig)>>'
				,etroco = <<IIF(td.u_tipodoc!=9,ft2.etroco,0)>>
				,motiseimp = '<<ft2.motiseimp>>'
				,u_acertovs = <<ft2.u_acertovs>>
				,c2codpost = '<<ft2.c2codpost>>'
				,eacerto = <<ft2.eacerto>>
				,vdollocal = 'Caixa'
				,vdlocal = 'C'
				,REEXGIVA = <<IIF(ft2.REEXGIVA,1,0)>>
				,formapag = '<<ft2.formapag>>'
				,modop1 = '<<ft2.modop1>>'
				,modop2 = '<<ft2.modop2>>'
				,usrinis =  '<<m_chinis>>'
				,usrdata =  CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
				,usrhora = '<<myHora>>' 
		Where	Ft2.Ft2Stamp = '<<Alltrim(Ft.Ftstamp)>>'
	EndText

	lcExecuteSQL  = lcExecuteSQL + CHR(13) + CHR(13) + lcSQL

	RETURN lcExecuteSQL  
ENDFUNC 


** Update � tabela de Linhas Fi
Function uf_facturacao_ActualizaFi
	LOCAL lcSql, lcExecuteSQL
	STORE "" TO lcExecuteSQL, lcSql
	

	** APAGA LINHAS QUE J� N�O EXISTEM, existem na BD Mas n�o existem no cursor proposto para grava��o
	lcSQL = ""
	TEXT TO lcSQL NOSHOW textmerge
		SELECT	FISTAMP
		FROM	FI (nolock)
		WHERE	FTSTAMP = '<<ALLTRIM(Ft.Ftstamp)>>'	
	ENDTEXT

	IF !uf_gerais_actGrelha("","uc_StampsAnterioresFi",lcSQL)
		uf_perguntalt_chama("OCORREU UM ERRO A ACTUALIZAR AS LINHAS DO DOCUMENTO.","OK","",16)
		RETURN
	Endif

	Select fistamp from uc_StampsAnterioresFi where fistamp not in (select fistamp from FI) into cursor lcCursorDeleteFI
	
	Select lcCursorDeleteFI
	Go top
	SCAN 
		lcSQL = ""
		TEXT TO lcSQL NOSHOW textmerge
			DELETE
			FROM	FI
			WHERE	FISTAMP = '<<ALLTRIM(lcCursorDeleteFI.fistamp)>>'	

			DELETE FROM fi_comp WHERE id_fi = '<<ALLTRIM(lcCursorDeleteFI.fistamp)>>'
				
		ENDTEXT
		lcExecuteSQL  = lcExecuteSQL + CHR(13) + CHR(13) + lcSQL
	ENDSCAN 
	
	**UPDATE LINHAS FI
	Select FI
	Go Top 
	SCAN
		lcSQL = ""
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			IF exists(Select FISTAMP From FI (nolock) Where Rtrim(Ltrim(FISTAMP)) = '<<FI.FISTAMP>>')
			BEGIN
				Update Fi
				Set	nmdoc =  '<<Alltrim(Fi.nmdoc)>>',fno =  <<Fi.Fno>>,ref =  '<<Alltrim(fi.ref)>>',design = '<<alltrim(fi.design)>>'
					,qtt =  <<fi.qtt>>,tiliquido = <<fi.tiliquido>>,etiliquido = <<fi.etiliquido>>,unidade = '<<fi.unidade>>'
					,unidad2 = '<<fi.unidad2>>',iva = <<fi.iva>>,ivaincl = <<IIF(fi.ivaincl,1,0)>>,codigo = '<<Alltrim(fi.codigo)>>'
					,tabiva = <<fi.tabiva>>,ndoc = <<Fi.ndoc>>,armazem = <<fi.armazem>>,fnoft = <<fi.fnoft>>,ndocft = <<fi.ndocft>>
					,ftanoft = <<fi.ftanoft>>,ftregstamp = '<<Alltrim(fi.ftregstamp)>>',lobs2 = '<<Alltrim(fi.lobs2)>>'
					,litem2 = '<<Alltrim(fi.litem2)>>',litem = '<<Alltrim(fi.litem)>>',lobs3 = '<<Alltrim(fi.lobs3)>>',rdata = '<<myInvoiceData>>'
					,cpoc = <<fi.cpoc>>,composto = <<IIF(fi.composto,1,0)>>,lrecno = '<<fi.lrecno>>',lordem = <<fi.lordem>>,fmarcada = <<IIF(fi.fmarcada,1,0)>>
					,partes = <<fi.partes>>,altura = <<fi.altura>>,largura = <<fi.largura>>,oref = '<<Alltrim(fi.oref)>>',lote = '<<Alltrim(fi.lote)>>'
					,usr1 = '<<Alltrim(fi.usr1)>>',usr2 = '<<Alltrim(fi.usr2)>>',usr3 = '<<Alltrim(fi.usr3)>>',usr4 = '<<Alltrim(fi.usr4)>>'
					,usr5 = '<<Alltrim(fi.usr5)>>',ftstamp = '<<Alltrim(Ft.Ftstamp)>>', cor = '<<Alltrim(fi.cor)>>',tam = '<<Alltrim(fi.tam)>>', stipo = 1, fifref = '<<Alltrim(fi.fifref)>>'
					,tipodoc = <<Fi.TipoDoc>>, familia = '<<Alltrim(fi.familia)>>', pbruto = <<fi.pbruto>>, bistamp = '<<Alltrim(fi.bistamp)>>',stns = <<IIF(fi.stns,1,0)>>, ficcusto = '<<fi.ficcusto>>'
					,fincusto = '<<Alltrim(fi.fincusto)>>', ofistamp = '<<Alltrim(fi.ofistamp)>>',pv = <<fi.pv>>, pvmoeda = <<fi.pvmoeda>>, epv = <<fi.epv>>, tmoeda = <<fi.tmoeda>>
					,eaquisicao = <<fi.eaquisicao>>, custo = <<fi.custo>>, ecusto = <<fi.ecusto>>, slvu = <<round(fi.pv/(fi.iva/100+1),2)>>,
					eslvu = <<Round(fi.epv/(fi.iva/100+1),2)>>, sltt = ABS(<<fi.tiliquido/(fi.iva/100+1)>>),  esltt = ABS(<<fi.etiliquido/(fi.iva/100+1)>>), slvumoeda = <<fi.slvumoeda>>, 	slttmoeda = <<fi.slttmoeda>>, 
					nccod = '<<Alltrim(fi.nccod)>>', epromo = <<IIF(fi.epromo,1,0)>>, fivendedor = <<ch_vendedor>>,  fivendnm = '<<alltrim(ch_vendnm)>>', 
					desconto = <<fi.desconto>>, desc2 = <<fi.desc2>>,  desc3 = <<fi.desc3>>, desc4 = <<fi.desc4>>, desc5 = <<fi.desc5>>, desc6 = <<fi.desc6>>,  
					iectin = <<fi.iectin>>, eiectin = <<fi.eiectin>>, vbase = <<fi.vbase>>, evbase = <<fi.evbase>>,	tliquido = <<fi.tliquido>>, num1 = <<fi.num1>>
					,pcp = <<fi.pcp>>, epcp = <<fi.epcp>>, rvpstamp = '<<Alltrim(fi.rvpstamp)>>', pvori = <<fi.pv>>, 	epvori = <<fi.epv>>, szzstamp = '<<Alltrim(fi.szzstamp)>>'
					,zona = '<<Alltrim(fi.zona)>>', morada = '<<Alltrim(fi.morada)>>', [local] = '<<Alltrim(fi.local)>>', codpost = '<<Alltrim(fi.codpost)>>',
					telefone = '<<Alltrim(fi.telefone)>>', email = '<<Alltrim(fi.email)>>', tkhposlstamp = '<<Alltrim(fi.tkhposlstamp)>>',u_generico = <<IIF(fi.u_generico,1,0)>>
					,u_cnp = '<<Alltrim(fi.u_cnp)>>', u_psicont = <<fi.u_psicont>>, u_bencont = <<fi.u_bencont>>, u_psico = <<IIF(fi.u_psico,1,0)>>
					,u_benzo = <<IIF(fi.u_benzo,1,0)>>, u_ettent1 = <<fi.u_ettent1>>, u_ettent2 = <<fi.u_ettent2>>
					,u_epref = <<fi.u_epref>>,pic = <<fi.pic>>,pvpmaxre = <<fi.pvpmaxre>>,  u_stock = <<fi.u_stock>>, u_epvp = <<fi.u_epvp>>, u_ip = <<IIF(fi.u_ip,1,0)>>,
					u_comp = <<IIF(fi.u_comp,1,0)>>, 	u_diploma = '<<Alltrim(fi.u_diploma)>>', usrinis = '<<m_chinis>>', usrdata = CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), 
					usrhora = '<<myHora>>', marcada =  <<IIF(fi.marcada,1,0)>>,  u_genalt =  <<IIF(fi.u_genalt,1,0)>>,  u_txcomp =  <<fi.u_txcomp>>, amostra = <<IIF(fi.amostra, 1, 0)>>,
					u_descval = <<fi.u_descval>>,u_codemb = '<<fi.u_codemb>>'			
				Where 
					Fistamp = '<<Alltrim(Fi.Fistamp)>>'
					
				update fi_comp SET comp_tipo = <<fi.comp_tipo>>,comp = <<fi.comp>>,comp_diploma = <<fi.comp_diploma>>,comp_tipo_2 = <<fi.comp_tipo_2>>,comp_2 = <<fi.comp_2>>,comp_diploma_2 = <<fi.comp_diploma_2>> where id_fi = '<<Alltrim(Fi.Fistamp)>>' 
				
			END
			ELSE
			BEGIN
				insert into fi (
						fistamp, nmdoc, fno, ref, design, qtt, tiliquido, etiliquido, unidade, unidad2, iva, ivaincl, codigo, tabiva, ndoc, armazem, fnoft, ndocft, ftanoft, ftregstamp
						,lobs2,litem2,litem,lobs3,rdata,cpoc,composto,lrecno,lordem,fmarcada,partes,altura,largura,oref,lote,usr1,usr2,usr3,usr4,usr5,ftstamp,cor,tam,stipo,fifref
						,tipodoc,familia,pbruto,bistamp,stns,ficcusto,fincusto,ofistamp,pv,pvmoeda,epv,tmoeda,eaquisicao,custo,ecusto,slvu,eslvu,sltt,esltt,slvumoeda,slttmoeda,nccod,epromo,
						fivendedor,fivendnm,desconto,desc2,desc3,desc4,desc5,desc6,iectin,eiectin,vbase,evbase,	tliquido,num1,pcp,epcp,rvpstamp,pvori,epvori,szzstamp,zona,morada,[local],
						codpost,telefone,email,tkhposlstamp, u_generico, u_cnp, u_psicont, u_bencont, u_psico, u_benzo, u_ettent1,u_ettent2,u_epref,pic,pvpmaxre, u_stock, u_epvp, u_ip,
						u_comp, u_diploma, ousrinis, ousrdata,ousrhora, usrinis, usrdata, usrhora,marcada, u_genalt, u_txcomp, amostra, u_descval,EVALDESC,VALDESC,u_codemb
					)Values (
						'<<Fi.Fistamp>>','<<Fi.nmdoc>>', <<Fi.Fno>>, '<<fi.ref>>', '<<alltrim(fi.design)>>', <<fi.qtt>>, <<fi.tiliquido>>,
						<<fi.etiliquido>>, '<<fi.unidade>>', '<<fi.unidad2>>', <<fi.iva>>, <<IIF(fi.ivaincl,1,0)>>,	'<<fi.codigo>>', <<fi.tabiva>>, <<Fi.ndoc>>, <<fi.armazem>>, <<fi.fnoft>>, <<fi.ndocft>>,
						<<fi.ftanoft>>, '<<fi.ftregstamp>>','<<fi.lobs2>>', '<<fi.litem2>>','<<fi.litem>>', '<<fi.lobs3>>', CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102),
						<<fi.cpoc>>, <<IIF(fi.composto,1,0)>>,'<<fi.lrecno>>',<<fi.lordem>>	,<<IIF(fi.fmarcada,1,0)>>, <<fi.partes>>, <<fi.altura>>, <<fi.largura>>,
						'<<fi.oref>>', '<<fi.lote>>', '<<fi.usr1>>', '<<fi.usr2>>', '<<fi.usr3>>','<<fi.usr4>>', '<<fi.usr5>>','<<Ft.Ftstamp>>','<<fi.cor>>','<<fi.tam>>',1
						,'<<fi.fifref>>',<<Fi.TipoDoc>>	,'<<fi.familia>>',<<fi.pbruto>>,'<<fi.bistamp>>',<<IIF(fi.stns,1,0)>>, '<<fi.ficcusto>>', '<<fi.fincusto>>',
						'<<fi.ofistamp>>', <<fi.pv>>, <<fi.pvmoeda>>, <<fi.epv>>,<<fi.tmoeda>>,<<fi.eaquisicao>>, <<fi.custo>>, <<fi.ecusto>>, <<round(fi.pv/(fi.iva/100+1),2)>>, <<Round(fi.epv/(fi.iva/100+1),2)>>,
						<<fi.tiliquido/(fi.iva/100+1)>>, <<fi.etiliquido/(fi.iva/100+1)>>, <<fi.slvumoeda>>, <<fi.slttmoeda>>,'<<fi.nccod>>',<<IIF(fi.epromo,1,0)>>,<<ch_vendedor>>, '<<alltrim(ch_vendnm)>>', <<fi.desconto>>,
						<<fi.desc2>>, <<fi.desc3>>, <<fi.desc4>>, <<fi.desc5>>, <<fi.desc6>>, <<fi.iectin>>,<<fi.eiectin>>,<<fi.vbase>>, <<fi.evbase>>,<<fi.tliquido>>, <<fi.num1>>,<<fi.pcp>>, <<fi.epcp>>,'<<fi.rvpstamp>>',
						<<fi.pv>>, <<fi.epv>>,'<<fi.szzstamp>>', '<<fi.zona>>', '<<fi.morada>>', '<<fi.local>>', '<<fi.codpost>>','<<fi.telefone>>', '<<fi.email>>','<<fi.tkhposlstamp>>',
						<<IIF(fi.u_generico,1,0)>>, '<<fi.u_cnp>>',<<fi.u_psicont>>, <<fi.u_bencont>>, <<IIF(fi.u_psico,1,0)>>, <<IIF(fi.u_benzo,1,0)>>, <<fi.u_ettent1>>,<<fi.u_ettent2>>,<<fi.u_epref>>
						,<<fi.pic>>,<<fi.pvpmaxre>>,<<fi.u_stock>>, <<fi.u_epvp>>, <<IIF(fi.u_ip,1,0)>>,<<IIF(fi.u_comp,1,0)>>, '<<fi.u_diploma>>', '<<m_chinis>>', CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102),
						'<<fi.ousrhora>>', '<<m_chinis>>', CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), '<<myHora>>',0, <<IIF(fi.u_genalt,1,0)>>, <<fi.u_txcomp>>, <<IIF(fi.amostra, 1, 0)>>, <<fi.u_descval>>
						,<<IIF(fi.qtt == 0,0,(fi.u_descval / fi.qtt))>>,<<IIF(fi.qtt == 0,0,(fi.u_descval / fi.qtt) * 200.482)>>,'<<fi.u_codemb>>'
				)
				IF <<fi.comp_tipo>> != 0 OR <<fi.comp_tipo_2>> != 0
				BEGIN
					INSERT INTO fi_comp (id_fi,comp_tipo,comp,comp_diploma,comp_tipo_2,comp_2,comp_diploma_2) VALUES ('<<Fi.Fistamp>>',<<fi.comp_tipo>>,<<fi.comp>>,<<fi.comp_diploma>>,<<fi.comp_tipo_2>>,<<fi.comp_2>>,<<fi.comp_diploma_2>>)
				END
			END
		ENDTEXT

		lcExecuteSQL  = lcExecuteSQL + CHR(13) + CHR(13) + lcSQL
					
	ENDSCAN 
	
	RETURN lcExecuteSQL
	
ENDFUNC 

**
FUNCTION uf_facturacao_actualizaVendaPsico
	
	IF !USED("dadosPsico")
		RETURN .t.
	ENDIF
	
	Set Point To "."
	
	SELECT dadosPsico
	
	IF uf_gerais_actGrelha("","checkExistDp",[select ftstamp from B_dadospsico dadospsico (nolock) where dadospsico.ftstamp=?dadosPsico.ftstamp])
		IF RECCOUNT()>0
			TEXT TO lcSql NOSHOW textmerge
				UPDATE B_dadospsico
				SET u_recdata	= '<<IIF(empty(dadosPsico.u_recdata),'19000101',uf_gerais_getDate(dadosPsico.u_recdata,"SQL"))>>',
					u_medico	= '<<ALLTRIM(dadosPsico.u_medico)>>',
					u_nmutdisp	= '<<ALLTRIM(dadospsico.u_nmutdisp)>>',
					u_moutdisp	= '<<ALLTRIM(dadospsico.u_moutdisp)>>',
					u_cputdisp	= '<<ALLTRIM(dadospsico.u_cputdisp)>>',
					u_nmutavi	= '<<ALLTRIM(dadospsico.u_nmutavi)>>',
					u_moutavi	= '<<ALLTRIM(dadospsico.u_moutavi)>>',
					u_cputavi	= '<<dadosPsico.u_cputavi>>',
					u_ndutavi	= '<<ALLTRIM(dadospsico.u_ndutavi)>>',
					u_ddutavi	= '<<IIF(empty(dadosPsico.u_ddutavi),'19000101',uf_gerais_getDate(dadosPsico.u_ddutavi,"SQL"))>>',
					u_idutavi	= <<dadospsico.u_idutavi>>,
					u_dirtec	= '<<uCrsE1.u_dirtec>>',
                    codigoDocSPMS = '<<ALLTRIM(dadospsico.codigoDocSPMS)>>'
				where ftstamp	= '<<ALLTRIM(ft.ftstamp)>>'
			ENDTEXT
			if !uf_gerais_actGrelha("","",lcSql)
				uf_perguntalt_chama("N�O FOI POSS�VEL ACTUALIZAR OS DADOS DE PSICOTR�PICOS.","OK","",48)
			ENDIF
		ENDIF
		fecha("checkExistDp")
	ELSE
		uf_perguntalt_chama("OCORREU UM ERRO A PROCURAR REGISTO DOS DADOS DE PSICOTR�PICOS.","OK","",16)
	ENDIF
ENDFUNC

**
FUNCTION uf_facturacao_criaCursorProcura
	SELECT Fi 
	go top
	
	SELECT fistamp ; 
	FROM fi ;
	WHERE (LIKE ('*'+ALLTRIM(UPPER(Facturacao.pageframe1.page1.textPesq.value))+'*',ALLTRIM(UPPER(fi.ref))) OR ;
			LIKE ('*'+ALLTRIM(UPPER(Facturacao.pageframe1.page1.textPesq.value))+'*',ALLTRIM(UPPER(fi.design))) OR;
			LIKE ('*'+ALLTRIM(UPPER(Facturacao.pageframe1.page1.textPesq.value))+'*',ALLTRIM(UPPER(fi.lote)))) AND ;
			!EMPTY(ALLTRIM(UPPER(Facturacao.pageframe1.page1.textPesq.value))) INTO CURSOR ucrsTempPesq
	SELECT ucrsTempPesq

	SELECT fi		
	GO Top
	SCAN
		IF (LIKE ('*'+ALLTRIM(UPPER(Facturacao.pageframe1.page1.textPesq.value))+'*',ALLTRIM(UPPER(fi.ref))) OR ;
			LIKE ('*'+ALLTRIM(UPPER(Facturacao.pageframe1.page1.textPesq.value))+'*',ALLTRIM(UPPER(fi.design))) OR;
			LIKE ('*'+ALLTRIM(UPPER(Facturacao.pageframe1.page1.textPesq.value))+'*',ALLTRIM(UPPER(fi.lote)))) AND ;
			!EMPTY(ALLTRIM(UPPER(Facturacao.pageframe1.page1.textPesq.value)))
			
			replace fi.PESQUISA WITH .t.
		ELSE
			replace fi.PESQUISA WITH .f.
		ENDIF
	ENDSCAN
	
	uf_facturacao_procuraLinhaSeguinte()
ENDFUNC


**
FUNCTION uf_facturacao_procuraLinhaSeguinte
	
	IF !USED("ucrsTempPesq")
		RETURN .f.
	ENDIF
	
	lcFistamp = ucrsTempPesq.fistamp
	select ucrsTempPesq
	try
		goto recno() +1
	catch
	 	go top
	endtry
	
	SELECT ucrsTempPesq
	SELECT fi
	SCAN
		IF fi.fistamp == ucrsTempPesq.fistamp
			EXIT
		Endif
	ENDSCAN

	Facturacao.pageframe1.page1.GridDoc.setfocus
ENDFUNC 

** Corre no LostFocus da combo de Condi�oes de Pagamento de Cliente
Function uf_facturacao_dataVencimentoCliente
	IF TYPE("GETDATE") != "U"
		RETURN .f.
	ENDIF
		 
	If !Empty(ft.fdata)
		Select Ft
		GO TOP 
		
		lcSQL=''
		TEXT TO lcSQL TEXTMERGE NOSHOW
			exec up_facturacao_CondPagCliente2 '<<Ft.no)>>', '<<Ft.estab)>>', '<<uf_gerais_getDate(Ft.fdata,"SQL")>>'
		ENDTEXT 

		If uf_gerais_actGrelha("","uc_DataVencimento",lcSQL)
			If Reccount("uc_DataVencimento") > 0
				SELECT ft
				GO TOP
				Replace	;
					Ft.tpstamp With uc_DataVencimento.tpstamp ;
					ft.tpdesc WITH uc_DataVencimento.condpag ;
					Ft.Pdata With uc_DataVencimento.DATAVENCIMENTO
			ELSE
				SELECT ft
				GO TOP
				Replace ;
					Ft.pdata with ft.fdata ;
					ft.tpstamp WITH '' ;
					ft.tpdesc WITH ''
			Endif
		ELSE
			SELECT ft
			GO TOP
			Replace ;
				Ft.pdata with ft.fdata ;
				ft.tpstamp WITH '' ;
				ft.tpdesc WITH ''
		Endif	
	ENDIF
	
	Facturacao.refresh
ENDFUNC


** funcao aplicar plano (click btn P.C)
FUNCTION uf_facturacao_AplicaPlanoComp
	LPARAMETERS lnDemPlano

	IF myFtIntroducao == .t. OR myFtAlteracao == .t.
		
		&& fix p alterar cData (data utilizada na receituario_makePrecos) - Lu�s Leal 26/07/2016
		SELECT ft
		replace ft.cdata WITH ft.bidata
		**
			
		LOCAL lcInclInativos 
		lcInclInativos = .f.
		
		Select TD
		IF td.u_tipodoc == 3
			lcInclInativos = .t.
		ELSE
			lcInclInativos = .f.
		ENDIF 

		** abre painel de sele��o de planos de comparticipa��o
		IF EMPTY(lnDemPlano)
			uf_selplanocomp_chama("ft2","u_codigo|u_design|u_abrev|u_codigo2|u_design2|u_abrev2","codigo|design|cptorgabrev|codigo2|design2|cptorgabrev2", lcInclInativos)
		ENDIF
		
		&&covid comentario inicio
		if(!EMPTY(ft2.u_codigo))
			IF(uf_atendimento_validaTipoSnsExtPorPlano(ALLTRIM(ft2.u_codigo)))
				uf_perguntalt_chama("Este plano s� pode ser aplicado no atendimento","OK","",48)	
				RETURN  .t.
			ENDIF			
		ENDIF
		&&covid comentario fim
		
			
		** selecciona vazio	
		IF EMPTY(ft2.u_codigo)
			SELECT fi
		  	GO TOP
		  	SCAN
		  		replace fi.u_comp WITH .f.
		  		uf_atendimento_actValoresFi()
		  	ENDSCAN
		  	SELECT fi
		  	GO TOP
		ELSE
			** seleccinou plano: Marca todos os produtos para Comparticipa��o
			uf_atendimento_comparticipaProd()
		ENDIF 

        select ft2
        uv_cod = ft2.u_codigo

        uv_compartManual = .F.
				
		** Actualizar comparticipa��o nas Linhas
		SELECT fi
		GO TOP
		SCAN
		    uf_atendimento_actValoresFi()
		ENDSCAN
		Select fi
		GO TOP

		** Actualizar Totais
		uf_atendimento_actTotaisFt()
		
		
		** calcula comp com base em pre�os em vigor � data da receita
		IF td.u_tipodoc == 3
			uf_facturacao_calcCompData()
		ENDIF	
		
		SELECT fi
		GO TOP 
		SCAN 
			** Verificar c�digo de embalagem obrigatorio Lu�s Leal 21-09-2015
			LOCAL lcObriga, lcCod
			STORE .f. TO lcObriga
			
			lcCod = UPPER(ALLTRIM(ft2.u_codigo))
			
			&& Tem de ser revisto para ser dinamico baseado no campo da cptpla CodUniEmb 21092015
			IF lcCod == "AS" OR lcCod == "WG" OR lcCod == "HP" OR lcCod == "WJ" 
				lcObriga = .t.
			ENDIF
			
			IF lcObriga == .t.
				&& No caso de ser Omega pede sempre
				IF UPPER(ALLTRIM(lcCod)) == "WJ"
					uf_perguntalt_chama("Este documento tem produtos que obrigam � coloca��o do c�digo de embalagem. Por favor verifique.","OK","",48)
				ELSE
					SELECT fi 
					SELECT uCrsAtendST
					LOCATE FOR uCrsAtendST.ststamp = fi.fistamp
					IF FOUND()
						IF uCrsAtendST.CodUniEmb == .t. AND EMPTY(fi.u_codemb)
							uf_perguntalt_chama("Este documento tem produtos que obrigam � coloca��o do c�digo de embalagem. Por favor verifique.","OK","",48)	
						ENDIF		
					ENDIF
				ENDIF
			ENDIF
		ENDSCAN
		
		facturacao.ContainerCab.refresh()
	ELSE
		RETURN .f.
	ENDIF
ENDFUNC

**
FUNCTION uf_facturacao_actCCusto
	IF USED("fi") AND USED("ft") AND (myFtIntroducao OR myFtAlteracao)
		SELECT ft
		replace ALL ficcusto	WITH	FT.CCUSTO	IN	FI
		GO TOP 
		uf_perguntalt_chama("LINHAS ACTUALIZADAS COM SUCESSO.","OK","",64)
	ENDIF
ENDFUNC

**
FUNCTION uf_facturacao_importClVD

	IF !USED("ft2")
		RETURN .f.
	ELSE
		SELECT ft2
		IF EMPTY(ALLTRIM(ft2.ft2stamp))
			RETURN .f.
		ENDIF
	ENDIF

	IF !USED("TD")
		RETURN .f.
	ELSE
		IF td.u_tipodoc == 3
			RETURN .f.
		ENDIF
	ENDIF 

	IF !myftalteracao AND !myftintroducao
		RETURN .f.
	ENDIF
	
	SELECT ft2
	IF !Ft2.u_vddom
		RETURN .f.
	ENDIF 
	
	SELECT ft
	
	SELECT ft2
	replace ;
		ft2.morada		WITH	ft.morada ;
		ft2.codpost		WITH	ft.codpost ;
		ft2.local		WITH	ft.local ;
		ft2.telefone	WITH	ft.telefone
	
	facturacao.refresh
	
	uf_perguntalt_chama("DADOS IMPORTADOS COM SUCESSO.","OK","",64)
	
ENDFUNC	

**
FUNCTION uf_facturacao_confCamposVD
	SELECT ft2
	IF ft2.u_vddom
		facturacao.pageframe1.page3.txtContacto.readonly	=	.F.
		facturacao.pageframe1.page3.txtMorada.readonly		=	.F.
		facturacao.pageframe1.page3.txtCodpost.readonly		=	.F.
		facturacao.pageframe1.page3.txtLocal.readonly		=	.F.
		facturacao.pageframe1.page3.txtTelefone.readonly	=	.F.
		facturacao.pageframe1.page3.txtEmail.readonly		=	.F.
		facturacao.pageframe1.page3.txtU_viatura.readonly	=	.F.
		facturacao.pageframe1.page3.txtHoraEntrega.readonly	=	.F.
	ELSE
		facturacao.pageframe1.page3.txtContacto.readonly	=	.T.
		facturacao.pageframe1.page3.txtMorada.readonly		=	.T.
		facturacao.pageframe1.page3.txtCodpost.readonly		=	.T.
		facturacao.pageframe1.page3.txtLocal.readonly		=	.T.
		facturacao.pageframe1.page3.txtTelefone.readonly	=	.T.
		facturacao.pageframe1.page3.txtEmail.readonly		=	.T.
		facturacao.pageframe1.page3.txtU_viatura.readonly	=	.T.
		facturacao.pageframe1.page3.txtHoraEntrega.readonly	=	.T.
	ENDIF

	facturacao.refresh
ENDFUNC 

**
FUNCTION uf_facturacao_exit
	
	


	IF USED("uCrsAtendST")
		fecha("uCrsAtendST")
	ENDIF 
	IF USED("uCrsAtendCLPato")
		fecha("uCrsAtendCLPato")
	ENDIF 
	IF USED("uCrsAtendCL")
		fecha("uCrsAtendCL")
	ENDIF 
	
	IF USED("dadosPsico")
		fecha("dadosPsico")
	ENDIF
	
	IF USED("uCrsFt")
		fecha("uCrsFt")
	ENDIF
		
	IF USED("ucrsDadosDemPresc")
		fecha("ucrsDadosDemPresc")
	ENDIF
	
	IF USED("uCrsFt2")
		fecha("uCrsFt2")
	ENDIF
	IF USED("uCrsFi")
		fecha("uCrsFi")
	ENDIF
	IF USED("uCrsVerificaRespostaDEM")
		fecha("uCrsVerificaRespostaDEM")
	ENDIF	
	IF USED("uCrsVerificaRespostaDEMTotal")
		fecha("uCrsVerificaRespostaDEMTotal")
	ENDIF

	IF USED("ucrsvaliva")
		fecha("ucrsvaliva")
	ENDIF	
			
	&&fecha cursor 	uCrsPlanosCombinados
	IF USED("uCrsPlanosCombinados")
		fecha("uCrsPlanosCombinados")
	ENDIF 	
	
	IF USED("fi2")
		fecha("fi2")
	ENDIF	
		
	IF USED("ft")
		fecha("ft")
	ENDIF	
		
	IF USED("ft2")
		fecha("ft2")
	ENDIF
	

	IF USED("fi")
		fecha("fi")
	ENDIF
	
	
	IF USED("uCrsExcecaoPanel")
		fecha("uCrsExcecaoPanel")
	ENDIF
	
	IF !(TYPE("excecaoMed") == "U")
		uf_excecaoMed_sair()
	ENDIF 
	
	if(USED("mixed_bulk_pend"))
		 fecha("mixed_bulk_pend")
	ENDIF
	
	if(USED("fi_trans_info"))
		fecha("fi_trans_info")
	ENDIF
	
	
	if(USED("ucrsComboDocFac"))
		fecha("ucrsComboDocFac")
	ENDIF
	

	myFtIntroducao	= .f.
	myFtAlteracao	= .f.
	release myInsereReceitaCorrigida	
		
	IF TYPE("FACTURACAO") != "U"       
		FACTURACAO.hide
		FACTURACAO.release
    ENDIF
    
    	
    
    


ENDFUNC

**
FUNCTION uf_facturacao_corrigirReceita
	
	IF !USED("FT")
		RETURN .f.
	ENDIF
	
	SELECT ft
	IF !EMPTY(ALLTRIM(ft.u_ltstamp)) OR !EMPTY(ALLTRIM(ft.u_ltstamp2))
	
		uf_corrigirreceita_chama(ALLTRIM(ft.u_ltstamp), ALLTRIM(ft.u_ltstamp2), ft.fdata)
	ELSE
		uf_perguntalt_chama("ESTE DOCUMENTO N�O CONT�M RECEITA. POR FAVOR VERIFIQUE","OK","",64)
	ENDIF
ENDFUNC 

&&Valida se Dem, n�o pode ser corrigida

FUNCTION uf_facturacao_validaSePermiteCorrigirReceita
		LPARAMETERS lcFtStamp
		
		LOCAL lcCorrigeDem
		STORE .t. TO lcCorrigeDem
		
		IF(EMPTY(lcFtStamp) or !USED("FT") or !USED("FT2") )
			RETURN .f.
		ENDIF

		SELECT u_tlote,u_tlote2, ft2.u_abrev,ft2.u_abrev2;
		FROM ft  ;
		LEFT  JOIN ft2 ON ft .ftstamp=ft2.ft2stamp INTO CURSOR ucrsFtJoinTemp
		
		
		&& valida se existe dem 
		SELECT ucrsFtJoinTemp
		GO TOP
		SCAN
			IF((ALLTRIM(u_tlote)=="96" OR  ALLTRIM(u_tlote)=="97" OR ALLTRIM(u_tlote)=="98" OR ALLTRIM(u_tlote)=="99";
			OR ALLTRIM(u_tlote2)=="96" OR ALLTRIM(u_tlote2)=="97" OR ALLTRIM(u_tlote2)=="98" OR ALLTRIM(u_tlote2)=="99");
			AND (ALLTRIM(u_abrev)=="SNS" OR ALLTRIM(u_abrev2)=="SNS"))
				
				lcCorrigeDem = .f.
				
			ENDIF
				
		ENDSCAN
		
		IF(USED("ucrsFtJoinTemp"))
			fecha("ucrsFtJoinTemp")
		ENDIF
		
		
		
		RETURN lcCorrigeDem 
ENDFUNC



** funcao para correcao automatica de receitas
FUNCTION uf_facturacao_corrigirReceitaAuto
	LOCAL lcQttOriginal, lcTokenOriginal
	
	IF !USED("FT")
		RETURN .f.
	ENDIF
	

	
	SELECT ft
	
	IF (!uf_facturacao_validaSePermiteCorrigirReceita(ALLTRIM(ft.ftstamp)))
		uf_perguntalt_chama("Funcionalidade n�o aplic�vel a receitas DEM. " +CHR(13)+"Se pretendido anule e consulte novamente a receita.","OK","",64)
		RETURN .f.
	ENDIF
	

	
	IF !EMPTY(ALLTRIM(ft.u_ltstamp)) OR !EMPTY(ALLTRIM(ft.u_ltstamp2))
		IF uf_gerais_validaPermUser(ch_userno, ch_grupo, 'Inser��o de Receita - Introduzir')
			IF uf_perguntalt_chama("Vai eliminar a receita do documento actual e introduzir uma nova. Depois de continuar n�o ser� poss�vel recuperar esta receita." + CHR(13) + CHR(13) + "Tem a certeza que pretende continuar?", "Sim", "N�o")
				
				
				
				** eliminar receitas
				
				If !Empty(ft.u_ltstamp)
					IF !uf_corrigirReceita_EliminarContadoresPar(1, .t.)
						RETURN .f.
					ENDIF
				ENDIF

				If !Empty(ft.u_ltstamp2)
					IF !uf_corrigirReceita_EliminarContadoresPar(2, .t.)
						RETURN .f.
					ENDIF 
				ENDIF
				**
				
				
				select * from ft into cursor uCrsFtCorrigeReceita readwrite 
				select * from ft2 into cursor uCrsFt2CorrigeReceita readwrite 
				select * from fi into cursor uCrsFiCorrigeReceita readwrite 
				
				SELECT uCrsFt2CorrigeReceita 
				lcTokenOriginal = ALLTRIM(uCrsFt2CorrigeReceita.token)
				
				uf_Facturacao_novo(.t.)

				facturacao.containercab.nmdoc.value='Inser��o de Receita'
				facturacao.containercab.nmdoc.lostfocus()

				select * from uCrsFiCorrigeReceita into cursor fi readwrite 

				uf_Facturacao_controlaWkLinhas()

				select ft
				
				LOCAL lcOfistamp, lcFtStamp
				STORE '' TO lcOfistamp, lcFtStamp
				
				lcFtSamp = ft.ftstamp
				
				LOCAL lcStamp		
				
				SELECT fi
				GO TOP
				SCAN
				
					lcQttOriginal = fi.qtt
				
					** atualizar para dados actuais	- apenas para linha com refer�ncia
					IF !EMPTY(fi.ref)
						uf_atendimento_actRef()
					ENDIF
					** 
	
					lcStamp = uf_gerais_stamp()
					
					SELECT FI
					lcOfistamp = fi.fistamp
					replace fi.fistamp 		WITH lcStamp 
					replace fi.ofistamp 	WITH lcOfistamp 
					replace fi.nmdoc 		WITH ft.nmdoc
					replace fi.fno 			WITH ft.fno
					replace fi.ndoc 		WITH ft.ndoc
					replace	fi.u_psicont 	WITH 0
					replace	fi.u_bencont 	WITH 0
					replace fi.qtt 			WITH lcQttOriginal
			
				ENDSCAN
				
				SELECT fi
				GO TOP
				
				select * from uCrsFTCorrigeReceita into cursor ft readwrite 
				
				SELECT ft
				replace ft.nmdoc WITH fi.nmdoc
				replace ft.tipodoc WITH 4
				replace ft.fno WITH fi.fno 
				replace ft.ftstamp WITH lcFtSamp 
				replace ft.ndoc WITH fi.ndoc
				replace ft.site WITH mySite
				replace ft.pnome WITH myTerm
				replace ft.pno WITH myTermNo
				replace ft.cxstamp WITH ''
				replace ft.cxusername WITH ''
				replace ft.sSstamp WITH '' 
				replace ft.sSusername WITH '' 
				replace ft.u_nratend WITH '' 
				replace ft.ftid WITH 0 
				replace ft.u_lote WITH 0 
				replace ft.u_lote2 WITH 0 
				replace ft.u_ltstamp WITH '' 
				replace ft.u_ltstamp2 WITH ''
				replace ft.u_nslote WITH 0 
				replace ft.u_nslote2 WITH 0
				replace ft.u_slote WITH 0 
				replace ft.u_slote2 WITH 0
				replace ft.u_tlote WITH ''
				replace ft.u_tlote2 WITH '' 
				
				select * from uCrsFT2CorrigeReceita into cursor ft2 readwrite 
				SELECT ft2
				replace ft2.ft2stamp WITH lcFtSamp
				
				select fi && necessario para recalculo das comparticipa��es
				GO TOP
				SCAN
					** atualizar para dados actuais	
					uf_atendimento_actValoresFi()
					** 
				ENDSCAN
					
				** Actualizar Totais
				uf_atendimento_actTotaisFt()
				
				IF USED("uCrsFtCorrigeReceita")
					fecha("uCrsFtCorrigeReceita")
				ENDIF 
				IF USED("uCrsFt2CorrigeReceita")
					fecha("uCrsFt2CorrigeReceita")
				ENDIF
				IF USED("uCrsFiCorrigeReceita")
					fecha("uCrsFiCorrigeReceita")
				ENDIF
			
				SELECT Ft2		
				IF !EMPTY(ft2.token)
				
					TEXT TO lcSQL NOSHOW TEXTMERGE 
						select 
							Dispensa_Eletronica_D.medicamento_cod
							,Dispensa_Eletronica_D.medicamento_cnpem
							,Dispensa_Eletronica_DD.ref
						From
							Dispensa_Eletronica_D (nolock)
							inner join Dispensa_Eletronica_DD (nolock) on Dispensa_Eletronica_D.id = Dispensa_Eletronica_DD.id
						Where
							Dispensa_Eletronica_D.token = '<<ALLTRIM(lcTokenOriginal)>>'
							and Dispensa_Eletronica_DD.token = '<<ALLTRIM(lcTokenOriginal)>>'
					ENDTEXT 

					uf_gerais_actGrelha("","uCrsDadosRelacionarCNPEM",lcSql)	
				
					uf_atendimento_DEM(.t.,Ft2.u_receita,Ft2.u_nbenef,Ft2.codAcesso,Ft2.codDirOpcao,.t.)
					
					LOCAL lcNrTentativas
					lcNrTentativas= 0
					DO WHILE .t.

						Wait Window "" timeout 3
						
						lcNrTentativas = lcNrTentativas +1
						TEXT TO lcSql NOSHOW TEXTMERGE
							exec up_dem_dadosReceita '<<ALLTRIM(facturacao.stamptmrConsultaDEM)>>'
						ENDTEXT

						uf_gerais_actGrelha("","uCrsVerificaRespostaDEM",lcSql)
						IF RECCOUNT("uCrsVerificaRespostaDEM") > 0
									
							IF ALLTRIM(uCrsVerificaRespostaDEM.resultado_consulta_cod) != "100003010001"
								regua(2)
								facturacao.stamptmrConsultaDEM = ''
								uf_perguntalt_chama("Resposta do Sistema Central de Prescri��es: " + ALLTRIM(uCrsVerificaRespostaDEM.resultado_consulta_descr),"OK","",16)

								RETURN .f.
							ENDIF 
						
							** Actualiza Token com novo pedido
							SELECT uCrsVerificaRespostaDEM
							GO Top
							UPDATE FT2 SET token = uCrsVerificaRespostaDEM.token
							UPDATE FI SET token = uCrsVerificaRespostaDEM.token, dem = .t.  &&WHERE fi.lordem >= uCrsCabVendas.lordem AND fi.lordem < uCrsCabVendas.lordem + 100000
								
							IF USED("ucrsFiAuxTemp")
								fecha("ucrsFiAuxTemp")
							ENDIF 
							SELECT * FROM fi INTO CURSOR ucrsFiAuxTemp READWRITE 
							SELECT * FROM uCrsVerificaRespostaDEM INTO CURSOR uCrsVerificaRespostaDEM_Aux READWRITE 

							SELECT uCrsVerificaRespostaDEM_Aux 
							GO TOP
							SCAN FOR !EMPTY(ALLTRIM(uCrsVerificaRespostaDEM_Aux.medicamento_cod)) OR !EMPTY(ALLTRIM(ASTR(uCrsVerificaRespostaDEM_Aux.medicamento_cnpem)))
							
								&& CNPEM
								IF EMPTY(ALLTRIM(uCrsVerificaRespostaDEM_Aux.medicamento_cod))

									SELECT ucrsFiAuxTemp
									GO TOP
									SCAN FOR EMPTY(ALLTRIM(ucrsFiAuxTemp.id_validacaoDem));
										and !empty(ALLTRIM(ucrsFiAuxTemp.ref))
									
										SELECT uCrsDadosRelacionarCNPEM
										GO Top
										LOCATE FOR ALLTRIM(uCrsVerificaRespostaDEM_Aux.medicamento_cnpem) == ALLTRIM(uCrsDadosRelacionarCNPEM.medicamento_cnpem);
											AND ALLTRIM(ucrsFiAuxTemp.ref) == ALLTRIM(uCrsDadosRelacionarCNPEM.ref)
										IF FOUND()	

											SELECT uCrsVerificaRespostaDEM_Aux
											SELECT ucrsFiAuxTemp
											replace ucrsFiAuxTemp.id_validacaoDem WITH uCrsVerificaRespostaDEM_Aux.id
										ENDIF
									ENDSCAN 

									UPDATE FI SET fi.id_validacaoDem = ucrsFiAuxTemp.id_validacaoDem FROM fi inner join ucrsFiAuxTemp on fi.fistamp = ucrsFiAuxTemp.fistamp WHERE fi.token = ALLTRIM(uCrsVerificaRespostaDEM_Aux.token) AND !EMPTY(ALLTRIM(ucrsFiAuxTemp.id_validacaoDem))							

								ELSE

									SELECT ucrsFiAuxTemp
									GO TOP 
									LOCATE FOR ALLTRIM(ucrsFiAuxTemp.ref) = ALLTRIM(uCrsVerificaRespostaDEM_Aux.medicamento_cod);
										AND !empty(ALLTRIM(ucrsFiAuxTemp.ref));
										AND EMPTY(ALLTRIM(ucrsFiAuxTemp.id_validacaoDem))&&;
										&&AND ALLTRIM(uCrsVerificaRespostaDEM_Aux.token) == ALLTRIM(ucrsFiAuxTemp.token) 
									IF FOUND()	

										SELECT ucrsFiAuxTemp
										replace ucrsFiAuxTemp.id_validacaoDem WITH uCrsVerificaRespostaDEM_Aux.id
									ENDIF
									
									UPDATE FI SET fi.id_validacaoDem = ucrsFiAuxTemp.id_validacaoDem FROM fi inner join ucrsFiAuxTemp on fi.fistamp = ucrsFiAuxTemp.fistamp WHERE fi.token = ALLTRIM(uCrsVerificaRespostaDEM_Aux.token) AND !EMPTY(ALLTRIM(ucrsFiAuxTemp.id_validacaoDem))
								ENDIF
							ENDSCAN
							
							EXIT && sai do ciclo
						ENDIF
						
						IF lcNrTentativas > 10
							exit
						ENDIF 
					ENDDO

					regua(2)
				ENDIF 
				
			ENDIF 		
		ELSE
			uf_perguntalt_chama("O SEU PERFIL N�O PERMITE INTRODUZIR RECEITAS.","OK","",48)
			RETURN .f.
		ENDIF 
	ELSE
		uf_perguntalt_chama("ESTE DOCUMENTO N�O CONT�M RECEITA. POR FAVOR VERIFIQUE","OK","",64)
	ENDIF
ENDFUNC

** funcao utilizada para consultar receita j� dispensadas DEM
FUNCTION uf_facturacao_consultarReceita
	LOCAL lcCodFarm, lcDbServer, lcDataIni, lcDataFim, lcReceita, lcToken, lcNomeJar, lcTestJar 
	STORE '' TO lcCodFarm, lcDbServer, lcDataIni, lcDataFim, lcReceita, lcToken, lcNomeJar, lcTestJar 

	&& valida se � farm�cia
	SELECT ucrsE1
	IF ALLTRIM(ucrsE1.tipoEmpresa) != "FARMACIA"
		uf_perguntalt_chama("FUNCIONALIDADE DISPON�VEL APENAS PARA CLIENTES DO TIPO FARM�CIA.","OK","",64)
		RETURN .t.
	ENDIF 
	
	&& Valida a liga��o � Internet 
	IF uf_gerais_verifyInternet() == .f.
		uf_perguntalt_chama("ESTA FUNCIONALIDADE REQUER LIGA��O � INTERNET."+CHR(13)+"POR FAVOR VERIFIQUE A SUA LIGA��O.","OK","",64)
		RETURN .f.
	ENDIF
	
	&& verifica a configura��o do c�digo de farm�cia
	SELECT ucrsE1
	IF EMPTY(ALLTRIM(ucrsE1.u_codfarm))
		uf_perguntalt_chama("C�digo de Farm�cia n�o configurado. Contacte o suporte.","OK","", 32)
		RETURN .f.
	ENDIF 
	
	&& C�digo Farm�cia 
	SELECT ucrsE1
	lcCodFarm = ALLTRIM(ucrsE1.u_codfarm)
	
	&& N� Receita 
	SELECT ft2
	IF !EMPTY(ft2.u_receita)
		lcReceita = ALLTRIM(ft2.u_receita)
	ELSE
		uf_perguntalt_chama("Para utilizar este funcionalidade, o campo n� de receita deve estar preenchido.","OK","", 32)
		RETURN .f.
	ENDIF
	
	&& Token receita
*!*		lcSQL = ""
*!*		TEXT TO lcSql NOSHOW TEXTMERGE 
*!*			select token from dispensa_eletronica where receita_nr = '<<ALLTRIM(ft2.u_receita)>>' AND resultado_efetivacao_cod = '100003040001'
*!*		ENDTEXT	
*!*		IF !uf_gerais_actGrelha("","uCrsAuxToken",lcSQL)
*!*			uf_perguntalt_chama("N�o foi poss�vel verificar o Token da receita para consulta.","OK","", 32)
*!*			RETURN .f.
*!*		ELSE
*!*			IF RECCOUNT("uCrsAuxToken") > 0
*!*				lcToken = ALLTRIM(uCrsAuxToken.token)
*!*			ELSE
*!*				uf_perguntalt_chama("N�o � poss�vel efetuar a consulta da receita seleccionada. Por favor contace o Suporte.","OK","", 32)
*!*				RETURN .f.
*!*			ENDIF	
*!*		ENDIF

	IF EMPTY(ft2.token)
		uf_perguntalt_chama("N�o � poss�vel efetuar a opera��o para a receita seleccionada. N�o existe token de verifica��o. Por favor contacte o Suporte.","OK","", 32)
		RETURN .f.
	ELSE 
		lcToken = ALLTRIM(ft2.token)
	ENDIF 

	
	&& Datas - Neste cen�rios as datas v�o a vazio porque a pesquisa � feita pelo n� de receita
	lcDataIni = ''
	lcDataFim = ''
	
	regua(0,5,"A obter dados do Sistema Central de Prescri��es...")
	regua(1,1,"A obter dados do Sistema Central de Prescri��es...")

	&& verifica a exist�ncia do software DEM
	IF DIRECTORY(ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\DEM')

		&& verifica se utiliza webservice de testes
		lcNomeJar = "LtsDispensaEletronica.jar"
		IF uf_gerais_getParameter("ADM0000000227","BOOL") == .f.
			lcTestJar = "0"	
		ELSE
			lcTestJar = "1"
		ENDIF 

		IF !FILE(ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\DEM\' + ALLTRIM(lcNomeJar))
			uf_perguntalt_chama("Ficheiro configura��o n�o encontrado. Por favor contacte o Suporte","OK","",64)
			RETURN .f.
		ENDIF 
		
*!*		Consulta:   consultaDispensas  CodFarmacia dataInicial dataFinal nrReceita dbServer dbName site
*!*		ex:  consultaDispensas  
*!*	         --CodFarmacia = 4600
*!*	         --dataInicial = ''
*!*	         --dataFinal = ''
*!*	         --nrReceita = zzzzzzzzzzzzz
*!*	         --dbServer = 172.20.5.4
*!*	         --dbName = ltdev29
*!*	         --Site = Loja 1
		
		&& verifica o servidor de BD
		IF !USED("uCrsServerName")
			TEXT TO lcSQL NOSHOW TEXTMERGE 
				Select @@SERVERNAME as dbServer
			ENDTEXT 
			IF !uf_gerais_actGrelha("", "ucrsServerName", lcSQL)
				uf_perguntalt_chama("N�o foi possivel proceder ao servi�o de Efetiva��o da Dispensa. Contacte o suporte." ,"OK","",32)
				regua(2)
				RETURN  .f.
			ENDIF
		ENDIF
				
		SELECT ucrsServerName
		lcDbServer = ALLTRIM(ucrsServerName.dbServer)
		
		&& efetua consulta da receita	
		lcWsPath = ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\DEM\' + ALLTRIM(lcNomeJar) + ' consultaDispensas ';
			+ ' --codFarmacia=' + ALLTRIM(lcCodFarm);
			+ ' --dataIni=' + lcDataIni;
			+ ' --nrReceita=' + ALLTRIM(lcReceita);
			+ ' --dbServer=' + ALLTRIM(lcDbServer);
			+ ' --dbName=' +UPPER(ALLTRIM(sql_db));
			+ ' --site=' +UPPER(ALLTRIM(STRTRAN(mySite,' ','_')));
			+ ' "--token=' + ALLTRIM(lcToken) + '"';
			+ ' --test=' + lcTestJar
			&&+ ' --dataFim=' + lcDataFim;
			

		&& Informa que est� a usar webservice de testes
		IF uf_gerais_getParameter("ADM0000000227","BOOL") == .t.		
			uf_perguntalt_chama("Webservice de teste..." ,"OK","",64)
		ENDIF 

		&& Envia comando Java
		lcWsPath = "javaw -jar " + lcWsPath 
			
		oWSShell = CREATEOBJECT("WScript.Shell")
		oWSShell.Run(lcWsPath,1,.t.)
		
		regua(2)
		
		&& Mostra a informa��o ao utilizador do Lote
		lcSQL = ""
		TEXT TO lcSql NOSHOW TEXTMERGE 
			exec up_dem_consultaDispensa '<<lcToken>>'
		ENDTEXT	
		IF !uf_gerais_actGrelha("","uCrsAuxConsulta",lcSQL)
			uf_perguntalt_chama("N�o foi poss�vel verificar a informa��o retornada pela SPMS. Por favor contacte o Suporte.","OK","", 32)
			RETURN .f.
		ELSE
			&& chama painel de consulta de dispensa que mostra informa��o da dispensa
			uf_consultadispensa_chama()
		ENDIF		
	ENDIF
	
	IF USED("uCrsAuxToken")
		fecha("uCrsAuxToken")
	ENDIF
	
	IF USED("uCrsAuxConsulta")
		fecha("uCrsAuxConsulta")
	ENDIF
	
ENDFUNC 


**
FUNCTION uf_facturacao_AtribuiLote
	LOCAL lcRef
	
	&& Se for um documento de credito nao valida stock, porque d� entrada de stock e essa informa��o deve ser importada do documento anterior 
	IF USED("TD")
		Select td
		IF td.tipodoc == 3 OR td.tipodoc == 4
			RETURN .f.
		ENDIF
	ENDIF
		
	** N�o est� ativo o parametro dos lotes
	IF !uf_gerais_getParameter("ADM0000000211","BOOL")
		RETURN .f.
	ENDIF
	
	SELECT Fi
	** Produto n�o est� configurado para usar lotes
	IF fi.usalote == .f.
		RETURN .f.
	ENDIF

	uf_FACTURACAO_divideLote(fi.ref, fi.qtt, fi.fistamp)
	
	SELECT ft 
	
	** Trata pre�os
	lcSQL = ""
	TEXT TO lcSql NOSHOW TEXTMERGE 
		exec up_ga_loteDetalhePrecos '<<ALLTRIM(fi.lote)>>','<<ALLTRIM(fi.ref)>>', <<IIF(EMPTY(ft.tipoDoc),0,ft.tipoDoc)>>
	ENDTEXT	
	IF !uf_gerais_actGrelha("","ucrsPesqLote",lcSQL)
		MESSAGEBOX("N�o foi possivel verificar os pre�os do Lote. Por favor contacte o suporte.",16,"Logitools Software")
		RETURN .f.
	ENDIF

	IF RECCOUNT("ucrsPesqLote")> 0
		select ucrsPesqLote
		GO TOP
		select fi
		replace fi.amostra WITH .t. && Na altera�ao considera o valor alterado
		Replace fi.epv     WITH ucrsPesqLote.epv1
		replace fi.u_epvp  WITH ucrsPesqLote.epv1
		Replace fi.ecusto  WITH ucrsPesqLote.pct 
		replace fi.lote    WITH ucrsPesqLote.lote
	ENDIF
	
	IF RECCOUNT("ucrsPesqLote")> 1 && Tem mais que um preco
		Select Fi 
		replace fi.alertaLote WITH .t.
	ELSE
		Select Fi 
		replace fi.alertaLote WITH .f.
	ENDIF

ENDFUNC

**
FUNCTION uf_facturacao_BeforRowColumnChange


	IF myFTIntroducao == .f. and myFTAlteracao == .f.
		return .f.
	ENDIF
	
	FACTURACAO.oldqtt = fi.qtt
	

	
ENDFUNC

**
FUNCTION UF_FACTURACAO_ATRIBUILOTEALTERAQTT
	LPARAMETERS lcLote
	
	LOCAL j,i,k,lcRef,lcStock
	STORE 0 TO lcStock 
	
	** N�o est� ativo o parametro dos lotes
	IF !uf_gerais_getParameter("ADM0000000211","BOOL")
		RETURN .f.
	ENDIF
	
	IF myFTIntroducao == .f. and myFTAlteracao == .f.
		return .f.
	ENDIF

	IF EMPTY(lcLote)
		lcLote = fi.lote
	ENDIF
	
	Select fi
	IF fi.usalote == .f. OR ft.tipodoc == 3 OR ft.tipodoc == 4
		RETURN .f.
	ENDIF

	uf_FACTURACAO_divideLote(fi.ref, fi.qtt, fi.fistamp)
	
ENDFUNC 

**
FUNCTION UF_FACTURACAO_DesdobraQTTEmLotes
	LOCAL i,j,k, lcRef, lclordem, lcEpv, lcbistamp, lcQttO, lcQttAplicadaLote
	
	STORE 0 TO k
	
	** Precorre linha a linha,
	** Caso o produto use lotes, e n�o tenha lote atribuido
	** Calcula quantidades em stocks ordenadas por validade
	** Distribui a quantiddae pelos lotes
	** Quando se esgotar a quantidade num lote � necess�rio cria nova linha
	** Se o stock terminar, cria nova linha com o restante stock

	Select Fi
	GO top
	SCAN FOR !empty(Fi.ref)
		
		lcRef = Fi.ref
		lclordem = Fi.lordem
		IF j == .f.
			lcQttO= fi.qtt 
		ENDIF
		lcbistamp = fi.bistamp 
		
		** Verifica se o produto tem Lote
		TEXT TO lcSQL NOSHOW TEXTMERGE
			Select usalote from st (nolock) WHERE ref ='<<Fi.ref>>'
		ENDTEXT 

		If !uf_gerais_actGrelha("","ucrsDadosRefLote",lcSQL)
			MESSAGEBOX("N�o foi poss�vel verificar defini��es da refer�ncia a importar. Por favor contacte o suporte.",16,"Logitools Software")
			RETURN .f.
		ENDIF
		
		IF ucrsDadosRefLote.usaLote == .t. AND EMPTY(Fi.Lote)
			
			** Verifica se o produto tem Lote
			TEXT TO lcSQL NOSHOW TEXTMERGE
				Select 
					0 as qttnova
					,ref
					,lote
					,stock = isnull(sum(case when cm < 50 then qtt else -qtt end),0)
					,ousrdata = MIN(ousrdata)
					,ousrhora = MIN(ousrhora)
				from 
					sl (nolock) 
				where 
					ref = '<<ALLTRIM(Fi.ref)>>' 
					and armazem = <<Fi.armazem>>
				Group by
					ref
					,lote
				Having
					isnull(sum(case when cm < 50 then qtt else -qtt end),0) > 0
				Order by 
					MIN(ousrdata)
					,MIN(ousrhora)	
				
			ENDTEXT 
**
*!*					Select 
*!*						0 as novaqtt
*!*						, * 
*!*					from 
*!*						st_lotes (nolock)
*!*					where 
*!*						ref = '<<ALLTRIM(Fi.ref)>>' 
*!*						and stock > 0
*!*						and armazem = <<Fi.armazem>>
*!*					order by 
*!*						ousrdata
*!*						, ousrhora		
	
			If !uf_gerais_actGrelha("","ucrsDadosRefLote",lcSQL)
				MESSAGEBOX("N�o foi poss�vel verificar defini��es dos lotes a atribuir. Por favor contacte o suporte.",16,"Logitools Software")
				RETURN .f.
			ENDIF

			k = 0				
			**
			FOR i = 1 TO Fi.qtt
				Select ucrsDadosRefLote
				GO top
				SCAN
					IF ucrsDadosRefLote.novaqtt < ucrsDadosRefLote.stock AND ucrsDadosRefLote.stock > 0
						k = k+1
						Replace ucrsDadosRefLote.novaqtt WITH  ucrsDadosRefLote.novaqtt + 1
						exit
					Endif
				ENDSCAN
			ENDFOR
			
			j = .f.
			Select ucrsDadosRefLote
			GO Top
			SCAN FOR ucrsDadosRefLote.novaqtt != 0
			
				** Verifica o ultimo pre�o do Lote
				TEXT TO lcSQL NOSHOW TEXTMERGE
					exec up_ga_loteUltimoPreco '<<ALLTRIM(ucrsDadosRefLote.lote)>>' 
				ENDTEXT 
				If !uf_gerais_actGrelha("","ucrsUltimoPrecoLote",lcSQL)
					MESSAGEBOX("N�o foi poss�vel verificar defini��es dos lotes a atribuir. Por favor contacte o suporte.",16,"Logitools Software")
					RETURN .f.
				ENDIF
			
				** 1 vez substitui qtt e atribui lote
				IF j == .f.
					Select Fi
					Replace fi.qtt WITH ucrsDadosRefLote.novaqtt
					Replace fi.lote WITH ucrsDadosRefLote.lote
					** Ultimo Pre�o do Lote
					REPLACE	FI.U_EPVP	WITH  ucrsUltimoPrecoLote.epv1
					REPLACE	FI.EPV	WITH  ucrsUltimoPrecoLote.epv1
					REPLACE	FI.ETILIQUIDO	WITH  ucrsUltimoPrecoLote.epv1 * ucrsDadosRefLote.novaqtt
					REPLACE	FI.TILIQUIDO	WITH  ucrsUltimoPrecoLote.epv1 * ucrsDadosRefLote.novaqtt * 200.482

					j = .t.
				ELSE
					uf_atendimento_AdicionaLinhaFi(.t.,.t.)
					REPLACE fi.ref WITH lcRef 
					uf_facturacao_eventoRefFi(.t.)
					
					Replace fi.qtt WITH ucrsDadosRefLote.novaqtt
					Replace fi.lordem WITH lclordem + 1
					Replace fi.lote WITH ucrsDadosRefLote.lote
					Replace Fi.bistamp 	With lcbistamp 
					** Ultimo Pre�o do Lote
					REPLACE	FI.U_EPVP	WITH  ucrsUltimoPrecoLote.epv1
					REPLACE	FI.EPV	WITH  ucrsUltimoPrecoLote.epv1
					REPLACE	FI.ETILIQUIDO	WITH  ucrsUltimoPrecoLote.epv1 * ucrsDadosRefLote.novaqtt
					REPLACE	FI.TILIQUIDO	WITH  ucrsUltimoPrecoLote.epv1 * ucrsDadosRefLote.novaqtt * 200.482
				ENDIF
				uf_atendimento_fiiliq(.t.)
				
			ENDSCAN
		
		ENDIF
		
		IF k < lcQttO

			IF j == .f.
				** N�o encontrou stock em nenhum Lote
				Replace fi.lote WITH ""
			ELSE
				uf_atendimento_AdicionaLinhaFi(.t.,.t.)
				REPLACE fi.ref WITH lcRef 
				uf_facturacao_eventoRefFi(.t.)
				Replace fi.qtt WITH lcQttO - k 
				Replace fi.lordem WITH lclordem + 1
				Replace fi.lote WITH ""
				Replace Fi.bistamp 	With lcbistamp 
			ENDIF
			
			uf_atendimento_fiiliq(.t.)
		ENDIF
		
		**	
		uf_atendimento_actTotaisFt()
	ENDSCAN

ENDFUNC

**
FUNCTION UF_FACTURACAO_AtribuiLotesDocumento

	Facturacao.lockscreen = .t.

	** necess�rio criar cursor auxiliar porque a fun��o dentro do ciclo cria novos registo na Fi e n�o � possivel fazer o scan
	Select * FROM fi INTO CURSOR ucrsFiaux2 READWRITE 

	SELECT ucrsFiaux2 
	GO Top
	SCAN FOR ucrsFiaux2.usalote == .t. AND !EMPTY(ucrsFiaux2.ref)
		Select Fi
		LOCATE FOR ALLTRIM(Fi.fistamp) == ALLTRIM(ucrsFiaux2.fistamp)
		IF FOUND()
			** Se tiver o sistema de Lotes activo, verifica necessidade altera��o Lote
			**
			IF uf_gerais_getParameter("ADM0000000211","BOOL")
				uf_facturacao_AtribuiLoteAlteraQtt()
			ENDIF
			
			uf_atendimento_fiiliq(.t.)	
			uf_atendimento_actValoresFi()
			uf_atendimento_actTotaisFt()
		ENDIF 
	ENDSCAN
	
	Facturacao.lockscreen = .f.
ENDFUNC 

**
FUNCTION uf_FACTURACAO_divideLote
	LPARAMETERS lcRef, lcQtt, lcFistamp

	LOCAL lcSQL, i, j ,lcNovoStamp, lcArmazem 
	j = 0
	lcNovoStamp = ""
	
	lcArmazem = IIF(EMPTY(facturacao.containerCab.armazem.value),myarmazem,facturacao.containerCab.armazem.value)
	
	SELECT * FROM fi WHERE fi.fistamp = lcFistamp INTO CURSOR ucrsFiAux READWRITE 

	** Verificar Lotes Disponiveis para Ref && 
	lcSQL=''
	TEXT TO lcSQL TEXTMERGE NOSHOW
		Select 
			ref
			,lote
			,armazem
			,stock = isnull(sum(case when cm < 50 then qtt else -qtt end),0)
			,ousrdata = MIN(ousrdata)
			,ousrhora = MIN(ousrhora)
			,qttAtribuir = 0
		from 
			sl (nolock) 
		where 
			ref = '<<ALLTRIM(lcRef)>>'
			and sl.armazem = <<lcArmazem>>
		Group by
			ref
			,lote
			,armazem
		Having
			isnull(sum(case when cm < 50 then qtt else -qtt end),0) > 0
		Order by 
			MIN(ousrdata)
			,MIN(ousrhora)
	ENDTEXT 
	If !uf_gerais_actGrelha("","ucrsDivideLote",lcSQL)
		MESSAGEBOX("N�o foi possivel verificar o Lote a atribuir. Por favor contacte o suporte.",16,"Logitools Software")
		RETURN .f.
	ENDIF	

	** Calcula quantidade j� aplicada da referencia por lote
	select SUM(qtt) as qtt,ref,lote from fi where ref = lcRef AND !EMPTY(lote) AND fi.fistamp != lcFistamp group by ref, lote INTO CURSOR ucrsQtAplicadaLoteAtual READWRITE
	** Acerta as quantidades com as quantidades j� introduzidas no documento
	IF RECCOUNT("ucrsDivideLote")>0
		Select ucrsDivideLote
		GO TOP 
		SCAN
			SELECT ucrsQtAplicadaLoteAtual 
			GO Top
			SCAN FOR ucrsQtAplicadaLoteAtual.lote == ucrsDivideLote.lote;
				AND ucrsQtAplicadaLoteAtual.ref == ucrsDivideLote.ref
				Replace ucrsDivideLote.stock WITH ucrsDivideLote.stock - ucrsQtAplicadaLoteAtual.qtt
			ENDSCAN
		ENDSCAN
	ENDIF
	
	FOR i = 1 TO lcQtt
		SELECT ucrsDivideLote
		GO TOP 
		SCAN FOR ucrsDivideLote.qttAtribuir < ucrsDivideLote.stock 
			Replace ucrsDivideLote.qttAtribuir with ucrsDivideLote.qttAtribuir + 1 
			EXIT 
		ENDSCAN  
	ENDFOR

	** Adiciona Nova Linha com a quantidade que n�o existe nos lotes
	SELECT ucrsDivideLote
	CALCULATE SUM(qttAtribuir) TO lcQttAplicada
	lcQtNaoAplicada = lcQtt-lcQttAplicada
	
	IF lcQtNaoAplicada > 0
		SELECT ucrsDivideLote
		APPEND BLANK
		Replace ucrsDivideLote.ref 			WITH lcRef
		Replace ucrsDivideLote.qttAtribuir  WITH lcQtNaoAplicada
		Replace ucrsDivideLote.lote 		WITH ''
	ENDIF 
	**

	SELECT ucrsDivideLote
	GO TOP 
	SCAN FOR ucrsDivideLote.qttAtribuir > 0 
		j=j+1
		
		IF j=1 && Primeira Linha faz replace
			Replace fi.qtt 		WITH ucrsDivideLote.qttAtribuir
			Replace fi.lote 	WITH ucrsDivideLote.lote
			Replace fi.armazem 	WITH ucrsDivideLote.armazem
		ELSE
			lcNovoStamp = uf_gerais_stamp()
			SELECT FI
			APPEND FROM DBF('ucrsFiAux')

			SELECT FI
			Replace fi.fistamp  WITH lcNovoStamp
			Replace fi.qtt 		WITH ucrsDivideLote.qttAtribuir
			Replace fi.lote 	WITH ucrsDivideLote.lote
			Replace fi.armazem  WITH ucrsDivideLote.armazem 
			Replace fi.lordem 	WITH fi.lordem &&+ j
		ENDIF 	
		
		uf_atendimento_fiiliq(.t.)
	ENDSCAN  
	
ENDFUNC   

**
FUNCTION uf_facturacao_validaStockLotes
	LOCAL lcSQL, lcRef 
		
		
	IF td.lancaSl == .f. && Valida se mexe em stock
		RETURN .t.
	ENDIF 
		
	select Fi
	GO Top
	SCAN FOR fi.usalote == .t. AND !EMPTY(fi.ref)
		
		lcRef = fi.ref
		lcLote = fi.lote
		
		** Calcula quantidade j� aplicada da referencia por lote
		select SUM(qtt) as qtt,ref,lote from fi where ref = lcRef AND !EMPTY(lote) group by ref, lote INTO CURSOR ucrsQtAplicadaLoteAtual READWRITE
		
		** Verifica stock actual do lotes para a referencia
		lcSQL = ""
		TEXT TO lcSQL NOSHOW TEXTMERGE
			Select 
				0 as qttnova
				,ref
				,lote
				,stock = isnull(sum(case when cm < 50 then qtt else -qtt end),0)
				,ousrdata = MIN(ousrdata)
				,ousrhora = MIN(ousrhora)
			from 
				sl (nolock) 
			where 
				ref = '<<ALLTRIM(fi.ref)>>'
				AND lote = '<<ALLTRIM(lcLote)>>'
			Group by
				ref
				,lote
			Order by 
				MIN(ousrdata)
				,MIN(ousrhora)	
		ENDTEXT
		
		If !uf_gerais_actGrelha("","ucrsStocksLote",lcSQL)
			MESSAGEBOX("N�o foi possivel verificar o Lote a atribuir. Por favor contacte o suporte.",16,"Logitools Software")
			RETURN .f.
		ENDIF
		
		Select ucrsStocksLote
		GO Top
		lcStock = ucrsStocksLote.stock
		
		** Caso n�o exista stock, n�o deixa fazer atribui��o
		IF  lcStock < fi.qtt
			uf_perguntalt_chama("N�o existe stock suficiente no Lote [" + ALLTRIM(lcLote) +"]."+CHR(13)+CHR(13)+"Stock: " + ALLTRIM(STR(lcStock,9,0)) + CHR(13)+ CHR(13)+"Selecione lote diferente.","OK","",32)
			RETURN .f.
		ENDIF
		
		** Acerta as quantidades com as quantidades j� introduzidas no documento
		IF RECCOUNT("ucrsStocksLote")>0
			Select ucrsStocksLote
			GO top
			SCAN
				SELECT ucrsQtAplicadaLoteAtual 
				GO Top
				SCAN FOR ucrsQtAplicadaLoteAtual.lote == ucrsStocksLote.lote and ucrsQtAplicadaLoteAtual.ref == ucrsStocksLote.ref
					Replace ucrsStocksLote.qttnova WITH ucrsStocksLote.qttnova + ucrsQtAplicadaLoteAtual.qtt
				ENDSCAN
			ENDSCAN
		ENDIF
			
		**
		Select ucrsStocksLote
		CALCULATE SUM(qttnova) to lcSomaQtt
		Select ucrsStocksLote
		CALCULATE SUM(stock) to lcSomaStock
		IF lcSomaQtt > lcSomaStock
			uf_perguntalt_chama("N�o existe stock suficiente no Lote [" + ALLTRIM(lcLote) +"]."+CHR(13)+CHR(13)+"Stock: " + ALLTRIM(STR(lcStock,9,0)) + CHR(13)+ CHR(13)+"Selecione lote diferente.","OK","",32)
			RETURN .f.
		ENDIF
		
	ENDSCAN
	
	RETURN .t.

ENDFUNC


**
FUNCTION uf_facturacao_ConfEncomendasAviamentos
	IF EMPTY(ALLTRIM(ft.nmdoc))
		uf_perguntalt_chama("DEVE SELECIONAR UM DOCUMENTO PARA PROSSEGUIR COM A CONFER�NCIA. POR FAVOR VERIFIQUE!","OK","",64)	
	ELSE
		uf_confencomendas_chama(.t.) && Parametro indica que � chamado para conferencia de aviamentos (Fatura��o) 		
	ENDIF
ENDFUNC


**
FUNCTION uf_facturacao_TabelaFixaIvaClientes
	LPARAMETERS lcTabIva
	LOCAL lcTaxaIva, lcSql
	lcTaxaIva = 0
	
	
	Select FT
	replace FT.tabIva WITH lcTabIva
	
	Select Fi
	GO Top
	SCAN
		uf_atendimento_fiiliq(.t.)
	ENDSCAN
	
	uf_atendimento_actTotaisFt()
ENDFUNC


**
Function uf_facturacao_importHistVendas
	LOCAL lcFileName, lcFileExt
	lcFileName = getfile()

	LOCAL lcSql
	STORE '' TO lcSql
	LOCAL lcMes, lcAno
	LOCAL lcRowData, lcRowAno, lcRowMesX, lcRowDia, lcRowHora
	
	IF EMPTY(lcFileName)
		uf_perguntalt_chama("Deve especificar o ficheiro de importa��o. Por favor verifique.", "OK", "", 64)
		return .f.
	ENDIF

	** Valida ficheiro
	lcLocal = ["] + ALLTRIM(lcFileName) + ["]
	IF !FILE(lcLocal)
		uf_perguntalt_chama("A localiza��o especificada para importa��o do ficheiro � inv�lida. Por favor verifique.", "OK", "", 64)
		return .f.
	ENDIF

	IF used("uCrsImportHistVendas")
		fecha("uCrsImportHistVendas")
	ENDIF
	IF used("uCrsTempFile")
		fecha("uCrsTempFile")
	ENDIF
	IF USED("ucrsTrocaIdLt")
		fecha("ucrsTrocaIdLt")
	ENDIF

	** guardar m�s de importa��o para o caso do ficheiro do sifarma
	lcAno = year(datetime()+(difhoraria*3600))
	lcMes = month(datetime()+(difhoraria*3600))

	** guardar local de importa��o para o caso do fx do sifarma
	PUBLIC myLocalLt

	** Valida extensoes dos ficheiros
	lcFileExt = UPPER(RIGHT(ALLTRIM(lcFileName),3))
	DO CASE
	
		&& Logitools
		CASE lcFileExt == "XLS"
			** Ler o xls para um cursor
			uf_gerais_xls2Cursor(lcFileName, "SellOut", "uCrsImportHistVendas")

			** Criar cursor e popular com conte�do do xls
			create cursor uCrsTempFile (ano I, mes I, dia I, hora c(8), ref c(18), pvp n(11,3), qt I, site c(20);
										,design c(60), lab c(150), familia c(18), faminome c(60), marca c(20);
										,tabiva n(2,0), taxaiva n(5,2), site_nr I, pvporig n(11,3) default 0, stock I)
			
			** validate xls columns
			columnData = (GetWordNum(uCrsImportHistVendas.data_hora_venda,1," "))
			columnHora = (GetWordNum(uCrsImportHistVendas.data_hora_venda,2," "))
			columnAno = (GetWordNum(columnData ,1,"/"))
			columnMes = (GetWordNum(columnData ,2,"/"))
			columnDia = (GetWordNum(columnData ,3,"/"))
			
			** check if all fields ok
			SELECT uCrsImportHistVendas
			IF TYPE('columnAno') == "U" OR TYPE('columnMes') == "U" OR TYPE('columnDia') == "U" OR TYPE('columnHora') == "U" OR TYPE('uCrsImportHistVendas.cnp') == "U" OR TYPE('uCrsImportHistVendas.pvp') == "U" OR TYPE('uCrsImportHistVendas.qtd_venda') == "U" OR TYPE('uCrsImportHistVendas.cod_farmacia') == "U"
				uf_perguntalt_chama("O nome das colunas no ficheiro n�o � o correto. Por favor valide.", "OK", "", 64)
				return .f.
			ENDIF

			SELECT uCrsImportHistVendas
			GO TOP 
			SCAN FOR !empty(uCrsImportHistVendas.cnp) and !isnull(uCrsImportHistVendas.cnp) and !empty(uCrsImportHistVendas.qtd_venda) and !isnull(uCrsImportHistVendas.qtd_venda)
				SELECT uCrsTempFile
				APPEND blank 
				
				lcRowData = (GetWordNum(uCrsImportHistVendas.data_hora_venda,1," "))
				lcRowHora = (GetWordNum(uCrsImportHistVendas.data_hora_venda,2," "))
				lcRowAno = (GetWordNum(lcRowData ,1,"/"))
				lcRowMes = (GetWordNum(lcRowData ,2,"/"))
				lcRowDia = (GetWordNum(lcRowData ,3,"/"))
				
				if type('lcRowAno') == "N"
					replace uCrsTempFile.ano with lcRowAno
				else
					replace uCrsTempFile.ano with val(lcRowAno)
				ENDIF
				
				**IF TYPE('mes') != "U"
				if type('lcRowMes') == "N"
					replace uCrsTempFile.mes with lcRowMes
				else
					replace uCrsTempFile.mes with val(lcRowMes)
				ENDIF
				
				**else
				**	IF type('uCrsImportHistVendas.m�s') = "N"
				**		replace uCrsTempFile.mes with uCrsImportHistVendas.m�s
				**	ELSE
				**		replace uCrsTempFile.mes with val(uCrsImportHistVendas.m�s)
				**	ENDIF
				**ENDIF
				
				if type('lcRowDia') == "N"
					replace uCrsTempFile.dia with IIF(isnull(lcRowDia), 0, lcRowDia)
				else
					replace uCrsTempFile.dia with val(IIF(isnull(lcRowDia), '0', lcRowDia))
				ENDIF
				
				**IF TYPE('uCrsImportHistVendas.hora') == "C"
				** time allways will be a string. cant be a number
				replace uCrsTempFile.hora with lcRowHora
				
				if type('uCrsImportHistVendas.cnp') == "N"
					replace uCrsTempFile.ref with astr(uCrsImportHistVendas.cnp)
				else
					replace uCrsTempFile.ref with uCrsImportHistVendas.cnp
				ENDIF
				
				if type('uCrsImportHistVendas.pvp') == "N"
					replace uCrsTempFile.pvp with IIF(isnull(uCrsImportHistVendas.pvp), 0, uCrsImportHistVendas.pvp)
				else
					replace uCrsTempFile.pvp with val(IIF(isnull(uCrsImportHistVendas.pvp), '0', uCrsImportHistVendas.pvp))
				ENDIF
				
				if type('uCrsImportHistVendas.qtd_venda') == "N"
					replace uCrsTempFile.qt with uCrsImportHistVendas.qtd_venda
				else
					replace uCrsTempFile.qt with val(uCrsImportHistVendas.qtd_venda)
				ENDIF
				
				** ver coluna no excel. neste momento esta cod_laboratorio
				replace uCrsTempFile.site with uCrsImportHistVendas.cod_farmacia
				
				IF TYPE('uCrsImportHistVendas.stock_data') == "N"
					replace uCrsTempFile.stock with uCrsImportHistVendas.stock_data
				else
					replace uCrsTempFile.stock with val(uCrsImportHistVendas.stock_data)
				endif

				select uCrsimportHistVendas
				
			ENDSCAN
	

		&& Sifarma 2000
		CASE lcFileExt == "TXT"
			IF !uf_perguntalt_chama("Aten��o: O ficheiro a importar deve, obrigat�riamente, ter sido gerado no m�s atual. Pretende continuar?", "SIM", "N�O", 64)
				RETURN .f.
			ENDIF
			
			TEXT TO lcSQL TEXTMERGE NOSHOW
				select distinct Local=site from empresa
			ENDTEXT
			If !uf_gerais_actGrelha("", "ucrsTrocaIdLt", lcSql)
				uf_perguntalt_chama("N�o foi poss�vel listar os ID's dos Locais. Por favor contacte o suporte.", "", "OK", 16)
				RETURN .f.
			ENDIF
			uf_valorescombo_chama("myLocalLt", 0, "ucrsTrocaIdLt", 2, "Local", "Local",.f.,.f.,.t.,.t.)
			IF EMPTY(myLocalLt)
				uf_perguntalt_chama("N�o pode continuar sem escolher o ID do local.", "", "OK", 48)
				RETURN .f.
			ENDIF

			** Criar cursor e popular com conte�do do txt
			** Nota os campos de valor t�m de ser texto, cao contr�rio perde-se o valor decimal na convers�o
			CREATE cursor uCrsImportHistVendas (ref c(18),NOM c(120),FAP c(120),LOCALIZACAO c(100),SAC c(10);
			,STM c(10),SMI c(10),QTE c(10),DUV c(20),DUC c(20),PVP c(13),PCU c(20),IVA c(20),CAT c(100),CT1 c(120),GEN c(10);
			,GPR c(10),CLA c(10),TPR c(10),CAR c(20),GAM c(10),V_0 c(20),V_1 c(20),V_2 c(20),V_3 c(20),V_4 c(20),V_5 c(20);
			,V_6 c(20),V_7	c(20),V_8 c(20),V_9 c(20),V_10 c(20),V_11 c(20),V_12 c(20);
			,V_13 c(20), V_14 c(20), V_15 c(20), V_16 c(20), V_17 c(20), V_18 c(20) ,V_19 c(20);
			,V_20 c(20),V_21 c(20),V_22 c(20),V_23 c(20),DTVAL c(20),FPD c(120),LAD c(120), PRATELEIRA c(60);
			,GAMA c(20),GRUPOHOMOGENEO c(120),INACTIVO c(10),PVP5 c(20);
			,design c(60), lab c(150), familia c(18), faminome c(60), marca c(20), tabiva n(2,0), taxaiva n(5,2), site_nr I, site c(60), pvporig n(11,3) default 0)

			SELECT uCrsImportHistVendas
			APPEND FROM &lcLocal TYPE DELIMITED WITH TAB

			Select * from uCrsImportHistVendas WHERE ALLTRIM(ref) != 'CPR' AND LOCALIZACAO != "ARMAZ�M" AND ALLTRIM(LOCALIZACAO) != "Reserva de Produtos" ORDER BY ref into CURSOR ucrsTempFile READWRITE

		OTHERWISE
			uf_perguntalt_chama("Tipo de Ficheiro inv�lido. Deve indicar um ficheiro xls ou txt.", "OK", "", 32)
			return .f.
	ENDCASE

	LOCAL lcSql2, nrRegistos, lcCount, lcOk
	STORE '' to lcSql2
	STORE 0 to nrRegistos, lcCount
	STORE .f. TO lcOk
	
	** vari�veis para o ficheiro do sifarma
	LOCAL txtAno, txtMes, lcVarQtt, lcQt, lcPvp
	STORE 0 TO lcQt, lcPvp
	txtAno = lcAno
	txtMes = lcMes

	IF USED("uCrsTempFile")
		nrRegistos = reccount("uCrsTempFile")
		
		IF nrRegistos  > 0
			regua(0,nrRegistos,"A processar importa��o do ficheiro...")

			** Preencher cursor com informa��o adicional e criar registo na st se n�o existir
			IF !uf_facturacao_importHistVendasCriaSt(lcFileExt)
				uf_perguntalt_chama("N�o foi poss�vel criar as fichas dos artigos. Por favor contacte o suporte", "OK", "", 16)
				regua(2)
				RETURN .f.
			ENDIF

			regua(1,1,"A Eliminar registos duplicados...")
			
			&& n�o adiciona refs que estejam duplicadas no cursor - deve ser melhorada para considerar s� primeira ref.
			IF lcFileExt == "TXT"
				SELECT ref, count(ref), design FROM uCrsTempFile GROUP BY ref, design HAVING count(ref) > 1 INTO CURSOR uCrsTempFileAux READWRITE
						
				SELECT * FROM uCrsTempFile WHERE ref not in (select ref from uCrsTempFileAux) INTO CURSOR uCrsTempFileAux1 READWRITE 
						
				IF USED("uCrsTempFile")					
					fecha("uCrsTempFile")
				ENDIF
				
				SELECT * FROM uCrsTempFileAux1 INTO CURSOR uCrsTempFile READWRITE			
				
				IF USED("uCrsTempFileAux1")					
					fecha("uCrsTempFileAux1")
				ENDIF
			ENDIF 
			
			&& Apagar registos BD
			SELECT uCrsTempFile
			GO TOP
			SCAN FOR !EMPTY(uCrsTempFile.ref)

				regua(1, RECNO("uCrsTempFile"), "A Eliminar registos duplicados: " + uCrsTempFile.ref)
				txtAno = lcAno
				txtMes = lcMes
				
				DO CASE
					&& ref com 12 meses por linha
					CASE lcFileExt == "TXT"
						FOR i=1 TO 12
							lcCount = lcCount + 1
							
							TEXT TO lcSql NOSHOW TEXTMERGE
								DELETE hist_vendas where ano = <<txtAno>> and mes = <<txtMes>> and dia = 0 and ref = '<<alltrim(uCrsTempFile.ref)>>' AND site = '<<ALLTRIM(myLocalLt)>>'
							ENDTEXT
							IF i<12 && at� ao pen�ltimo registo
								lcSql2 = lcSql2 + lcSql + chr(10)+chr(13)
							ENDIF
							
							IF txtMes==1
								txtMes=12
								txtAno = lcAno - 1
							ELSE
								txtMes = txtMes - 1
							ENDIF
						ENDFOR
					
					&& ref por dia/mes por linha
					CASE lcFileExt == "XLS" 
						**lcCount = lcCount + 1
						
*!*							TEXT TO lcSql NOSHOW TEXTMERGE
*!*								DELETE hist_vendas where ano = <<uCrsTempFile.ano>> and mes = <<uCrsTempFile.mes>> and dia = <<uCrsTempFile.dia>> and ref = '<<alltrim(uCrsTempFile.ref)>>' and site = '<<alltrim(uCrsTempFile.site)>>'
*!*							ENDTEXT
				ENDCASE

				IF lcCount >= 100
					lcCount = 0
					lcSql2 = lcSql2 + lcSql
					IF !uf_gerais_actGrelha("", "", lcSql2)
						uf_perguntalt_chama("N�o foi poss�vel importar o ficheiro corretamente. Por favor valide a integridade do mesmo.", "OK", "", 16)
						regua(2)
						RETURN .f.
					ENDIF
					lcSql2 = ""
				ELSE
					IF !EMPTY(lcSql)
						lcSql2 = lcSql2 + lcSql + chr(10)+chr(13)
					ENDIF
				ENDIF

				SELECT uCrsTempFile
			ENDSCAN
			
			IF !EMPTY(lcSql2)
				IF !uf_gerais_actGrelha("", "", lcSql2)
					uf_perguntalt_chama("N�o foi poss�vel importar o ficheiro corretamente. Por favor valide a integridade do mesmo.", "OK", "", 16)
					regua(2)
					RETURN .f.
				ENDIF
				lcSql2 = ""
			ENDIF

			regua(1, 1, "A inserir registos...")

			** Inserir registos
			lcSQL= ''
			SELECT uCrsTempFile
			GO TOP
			SCAN FOR !EMPTY(uCrsTempFile.ref)

				regua(1, RECNO("uCrsTempFile"), "A inserir registos: " + uCrsTempFile.ref)
				txtAno = lcAno
				txtMes = lcMes
				
				DO CASE
					&& ref com 12 meses por linha
					CASE lcFileExt == "TXT"

						FOR i=1 TO 12
							lcVarQtt = "ucrsTempFile.V_" + ALLTRIM(STR(i-1))
							lcQt = VAL(&lcVarQtt)
							lcPvp = VAL(strtran(ucrsTempFile.pvp,',','.'))

							IF lcQt > 0 && se n�o existir vendas n�o faz sentido guardar registo
								lcCount = lcCount + 1
								
								IF lcSql2 == ""
									TEXT TO lcSql NOSHOW TEXTMERGE 
										insert into hist_vendas (ano,mes,dia,ref,pvp,qt,site)
										values
											(<<txtAno>>, <<txtMes>>, 0, '<<alltrim(uCrsTempFile.ref)>>', <<lcPvp>>, <<lcQt>>, '<<ALLTRIM(myLocalLt)>>')
									ENDTEXT
								ELSE
									TEXT TO lcSql NOSHOW TEXTMERGE
										(<<txtAno>>, <<txtMes>>, 0, '<<alltrim(uCrsTempFile.ref)>>', <<lcPvp>>, <<lcQt>>, '<<ALLTRIM(myLocalLt)>>')
									ENDTEXT
								ENDIF

								IF !EMPTY(lcSql2)
									lcSql2 = lcSql2 + + "," + lcSql + chr(10)+chr(13)
								ELSE
									lcSql2 = lcSql2 + lcSql + chr(10)+chr(13)
								ENDIF
							ENDIF

							IF txtMes==1
								txtMes=12
								txtAno = lcAno - 1
							ELSE
								txtMes = txtMes - 1
							ENDIF
						ENDFOR

					&& ref por dia/mes por linha
					CASE lcFileExt == "XLS"
						&&IF uCrsTempFile.qt > 0
							lcCount = lcCount + 1

							TEXT TO lcSql NOSHOW TEXTMERGE 
														
								IF exists (Select ref From hist_vendas (nolock) where ano = <<uCrsTempFile.ano>> and mes = <<uCrsTempFile.mes>> and dia = <<uCrsTempFile.dia>> and hora = '<<alltrim(uCrsTempFile.hora)>>' and ref = '<<alltrim(uCrsTempFile.ref)>>' and site = '<<alltrim(uCrsTempFile.site)>>')
								BEGIN
									UPDATE hist_vendas SET hist_vendas.qt = hist_vendas.qt + <<uCrsTempFile.qt>>, pvp = <<uCrsTempFile.pvp>>, hist_vendas.stock = <<uCrsTempFile.stock>> where ano = <<uCrsTempFile.ano>> and mes = <<uCrsTempFile.mes>> and dia = <<uCrsTempFile.dia>> and hora = '<<alltrim(uCrsTempFile.hora)>>' and ref = '<<alltrim(uCrsTempFile.ref)>>' and site = '<<alltrim(uCrsTempFile.site)>>'
								END else begin
								
									insert into hist_vendas (ano,mes,dia,hora,ref,pvp,qt,site,stock)
									values
										(<<uCrsTempFile.ano>>, <<uCrsTempFile.mes>>, <<uCrsTempFile.dia>>, '<<alltrim(uCrsTempFile.hora)>>', '<<alltrim(uCrsTempFile.ref)>>', <<uCrsTempFile.pvp>>, <<uCrsTempFile.qt>>, '<<alltrim(uCrsTempFile.site)>>', <<uCrsTempFile.stock>>)
								END
								
							ENDTEXT
							
							lcSql2 = lcSql2 + lcSql + chr(10)+chr(13)
						&&ENDIF
				ENDCASE

				IF lcCount >= 50
					IF !uf_gerais_actGrelha("", "", lcSql2)
						uf_perguntalt_chama("N�o foi poss�vel importar o ficheiro corretamente. Por favor valide a integridade do mesmo.", "OK", "", 16)
						regua(2)
						RETURN .f.
					ELSE
						lcOk = .t.
					ENDIF
					lcCount = 0
					lcSql2 = ""
				ENDIF

				SELECT uCrsTempFile
			ENDSCAN
			
			IF !empty(lcSql2)
				IF !uf_gerais_actGrelha("", "", lcSql2)
					uf_perguntalt_chama("N�o foi poss�vel importar o ficheiro corretamente. Por favor valide a integridade do mesmo e tente novamente.", "OK", "", 16)
					regua(2)
					RETURN .f.
				ELSE
					lcOk = .t.
				ENDIF
			ENDIF
			
**			lcSQL = ''
**			TEXT TO lcSQL NOSHOW TEXTMERGE 
**				DELETE FROM hist_vendas WHERE hist_vendas.qt <= 0
**			ENDTEXT 
**			IF !uf_gerais_actGrelha("", "", lcSql)
**				uf_perguntalt_chama("N�o foi poss�vel eliminar os produtos com quantidade de venda <= 0.", "OK", "", 16)
**				regua(2)
**				RETURN .f.
**			ENDIF 
				
			regua(2)
		ENDIF
	ENDIF

	IF used("uCrsImportHistVendas")
		fecha("uCrsImportHistVendas")
	ENDIF
	
	IF used("uCrsTempFile")
		fecha("uCrsTempFile")
	ENDIF

	IF USED("uCrsTempFileAux")
		fecha("uCrsTempFileAux")
	ENDIF 

	IF lcOk
		uf_perguntalt_chama("Hist�rico importado com sucesso.", "OK", "", 64)
	ENDIF

	RELEASE myLocalLt
ENDFUNC


**
** Esta fun��o atualiza o cursor uCrsTempFile proveniente das importa��es dos ficheiros
** xls (logitools) e sifarma (txt) para importa��es de historico de vendas
**
FUNCTION uf_facturacao_importHistVendasCriaSt
	LPARAMETERS tcFileExt
	
	IF !USED("ucrsTempFile")
		RETURN .f.
	ENDIF

	LOCAL lcTotalRegistos, lcContador, lcSqlFinal
	STORE 0 TO lcContador
	STORE "" TO lcSqlFinal

	lcTotalRegistos = reccount("ucrsTempFile")
	regua(0, lcTotalRegistos, "A verificar dados do dicionario...")

	IF !USED("ucrsDadosFprod")
		** Carrega Informa��o da Fprod
		TEXT TO lcSQLx NOSHOW TEXTMERGE
			select
				cnp
				,design
				,titaimdescr
				,u_marca
				,u_familia
				,u_tabiva
				,faminome = ISNULL(stfami.nome,'')
				,taxaiva = ISNULL(taxasiva.taxa,0)
				,pvporig
			from
				fprod (nolock)
				left join stfami (nolock) on fprod.u_familia = stfami.ref
				left join taxasiva (nolock) on taxasiva.codigo = fprod.u_tabiva
		ENDTEXT

		IF !uf_gerais_actGrelha("","ucrsDadosFprod",lcSqlx)
			uf_perguntalt_chama("N�o foi poss�vel verificar dados adicionais do produto no dicion�rio", "OK", "", 16)
			regua(2)
			RETURN .f.
		ENDIF
	ENDIF



	regua(1,2,"A verificar dados do dicionario...")

	UPDATE ucrsTempFile;
    SET ;
    	ucrsTempFile.design = ALLTRIM(ucrsDadosFprod.design);
   	 	,ucrsTempFile.lab = ALLTRIM(ucrsDadosFprod.titaimdescr);
   	 	,ucrsTempFile.familia = ALLTRIM(ucrsDadosFprod.u_familia);
		,ucrsTempFile.faminome = ALLTRIM(ucrsDadosFprod.faminome);
		,ucrsTempFile.marca = ALLTRIM(ucrsDadosFprod.u_marca);
		,ucrsTempFile.tabiva = ucrsDadosFprod.u_tabiva;
		,ucrsTempFile.taxaiva = ucrsDadosFprod.taxaiva;
		,ucrsTempFile.pvporig = ucrsDadosFprod.pvporig;
    FROM ucrsTempFile;
    inner join ucrsDadosFprod on ALLTRIM(ucrsDadosFprod.cnp) == ALLTRIM(ucrsTempFile.ref)

	
	If (tcFileExt == "XLS")
		UPDATE ucrsTempFile;
	    SET ;
	    	ucrsTempFile.site_nr = ucrsLojas.no;
	    FROM ucrsTempFile;
	    inner join ucrsLojas on ALLTRIM(ucrsLojas.site) == ALLTRIM(ucrsTempFile.site)
	ELSE
		UPDATE ucrsTempFile;
	    SET ;
	    	ucrsTempFile.site_nr = ucrsLojas.no;
	    FROM ucrsTempFile;
   	    inner join ucrsLojas on ALLTRIM(ucrsLojas.site) == ALLTRIM(myLocalLt)
	ENDIF


	regua(1,3,"A verificar dados do dicionario...")

	&& Criar ficha do produto
	Select ucrsTempFile
	Go TOP
	Scan for !empty(ucrsTempFile.ref)

		lcContador = lcContador + 1

		lcRef = strtran(ALLTRIM(ucrsTempFile.ref),chr(39),'')
		lcDesign = strtran(ALLTRIM(ucrsTempFile.design),chr(39),'')
		lcTabIva = IIF(EMPTY(ucrsTempFile.tabiva), 4, ucrsTempFile.tabiva)
		lcIvaTaxa = IIF(EMPTY(ucrsTempFile.taxaiva), 0, ucrsTempFile.taxaiva)
		IF TYPE("ucrsTempFile.pvp")=='N'
			lcPvp = IIF(EMPTY(ucrsTempFile.pvp), ucrsTempFile.pvporig, ucrsTempFile.pvp)
		ELSE
			lcPvp = IIF(EMPTY(ucrsTempFile.pvp), ucrsTempFile.pvporig, VAL(strtran(uCrsTempFile.pvp,',','.')))
		ENDIF
		lcPCT = ROUND(IIF(EMPTY(lcPvp), 0, lcPvp - (lcPvp * lcIvaTaxa/100)),2)
		lcStamp = uf_gerais_stamp()

		TEXT TO lcSqlx NOSHOW TEXTMERGE
			if not exists (select ref from st (nolock) where st.ref = '<<lcRef>>' AND st.site_nr = <<ucrsTempFile.site_nr>>)
			BEGIN
				INSERT INTO st (
					ststamp,ref,design,epv1,epcusto,epcpond,epcult
					,ivaincl,st.iva1incl,st.iva2incl,st.iva3incl,st.iva4incl,st.iva5incl
					,st.tabiva,stmax,ptoenc,familia,faminome,usr1
					,ousrdata,ousrhora,ousrinis,usrdata,usrhora,usrinis
					,marg1,marg2,marg3,marg4,u_lab, site_nr, u_tipoetiq
				)
				Select
					ststamp = '<<ALLTRIM(lcStamp)>>'
					,ref = '<<lcRef>>'
					,design = '<<lcDesign>>'
					,epv1 = <<lcPvp>>
					,epcusto = <<lcPCT>>
					,epcpond = 0
					,epcult = <<lcPCT>>
					,ivaincl = 1
					,iva1incl = 1
					,iva2incl = 1 
					,iva3incl = 1
					,iva4incl = 1
					,iva5incl = 1
					,tabiva = <<lcTabIva>>
					,stmax = 0
					,ptoenc = 0
					,familia = case when '<<ALLTRIM(ucrsTempFile.familia)>>' = '' then '99' else '<<ALLTRIM(ucrsTempFile.familia)>>' end
					,faminome = case when '<<ALLTRIM(ucrsTempFile.faminome)>>' = '' then 'Outros' else '<<ALLTRIM(ucrsTempFile.faminome)>>' end
					,usr1 = '<<ALLTRIM(ucrsTempFile.marca)>>'
					,ousrdata = convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
					,ousrhora = convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108)
					,ousrinis = '<<m_chinis>>'
					,usrdata = convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
					,usrhora = convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108)
					,usrinis = '<<m_chinis>>'
					-- Margem Comercial
					,marg1	= case when <<lcPvp>> > 0 AND <<lcPCT>> >0 then ROUND( ((<<lcPvp>> / (<<lcIvaTaxa>>/100+1) / <<lcPCT>>) -1) * 100 ,2)
									when <<lcPvp>> >0 AND <<lcPCT>>=0 then 100
									ELSE 0 end
					-- Desconto Comercial
					,marg2	= case when <<lcPCT>> >0 and <<lcPCT>> >0
									then ROUND( ((<<lcPCT>> / <<lcPCT>>) -1) * 100 ,2)
									ELSE 0 end
					-- Margem Bruta
					,marg3	= case when <<lcPvp>> >0 and <<lcPCT>> >0
									then ROUND( ((<<lcPvp>> / (<<lcIvaTaxa>>/100+1) / <<lcPCT>>) -1) * 100 ,2)
									ELSE 0 end
					-- Benificio Bruto
					,marg4	= case when <<lcPvp>> >0 and <<lcPCT>> >0 then ROUND( (<<lcPvp>> / (<<lcIvaTaxa>>/100+1)) - <<lcPCT>> ,2)
									ELSE 0 end
			        ,u_lab = '<<ALLTRIM(ucrsTempFile.lab)>>'
			        ,site_nr = <<ucrsTempFile.site_nr>>
			        ,u_tipoetiq = 1
			END
		ENDTEXT

		lcSqlFinal  = lcSqlFinal  + CHR(10) + CHR(13) + lcSQLx

		IF lcContador > 200
			regua(1,RECNO("ucrsTempFile") ,"A atualizar/criar dados dos produtos: " + ASTR(RECNO("ucrsTempFile")) + " de " + ASTR(lcTotalRegistos))

			IF !EMPTY(lcSqlFinal)
				IF !uf_gerais_actgrelha("", "", lcSqlFinal)
					uf_perguntalt_chama("Ocorreu um erro a inserir o artigo. Por favor contacte o suporte","OK","",16)
					regua(2)
					return .f.
				ENDIF
			ENDIF
			lcContador = 0
			lcSqlFinal  = ""
		ENDIF
	ENDSCAN

	IF !EMPTY(lcSqlFinal)
		IF !uf_gerais_actgrelha("", "", lcSqlFinal)
			uf_perguntalt_chama("Ocorreu um erro a inserir o artigo. Por favor contacte o suporte","OK","",16)
			regua(2)
			return .f.
		ENDIF
	ENDIF

	SELECT ucrsTempFile
ENDFUNC


**
FUNCTION uf_Facturacao_alterameiospagamento

	SELECT Ft
	uf_ALTERAMEIOSPAG_chama(Ft.u_nratend,Ft.ftano)

ENDFUNC 


**
FUNCTION uf_facturacao_fichaUtente
	SELECT ft
	IF !EMPTY(ft.no)
		uf_utentes_chama(ft.no)
	ENDIF 
ENDFUNC 


** 
FUNCTION uf_facturacao_DEM

	** Verificar se existe nova vers�o para reiniciar o software
	uf_chk_nova_versao()
			
	uf_DADOSDEM_chama(.t.)
	
ENDFUNC 


**
FUNCTION uf_facturacao_addLinha

	** Verificar se existe nova vers�o para reiniciar o software
	uf_chk_nova_versao()

	IF myFtAlteracao AND !empty(alltrim(td.tiposaft))
		RETURN .f. 
	ELSE
		IF facturacao.eventoRef == .f.
			uf_atendimento_AdicionaLinhaFi(.t.,.t.)
			SELECT fi
			GO BOTTOM
			facturacao.pageframe1.page1.gridDoc.SetFocus
		ENDIF
	ENDIF 
ENDFUNC

**
FUNCTION uf_facturacao_remLinha

	IF myFtAlteracao AND !empty(alltrim(td.tiposaft))
		RETURN .f.
	ELSE
		facturacao.pageframe1.page1.gridDoc.SetFocus
		uf_facturacao_apagaLinhaDoc()

		SELECT fi
		lnRecNo = RECNO("FI")
		COUNT FOR !DELETED() TO lnReccount
		TRY
			GO lnRecNo
		CATCH
			GO BOTTOM
		ENDTRY
		IF lnReccount <= 0
			facturacao.pageframe1.page1.containerLinhas.btnAddLinha.Click()
		ENDIF 
	ENDIF 

ENDFUNC

**
FUNCTION uf_facturacao_AlertaPIC
	LPARAMETERS lcFistamp

	IF !USED("uCrsHistPic")
		RETURN .f.
	ENDIF 
	
	SELECT Ft 
	IF EMPTY(lcFistamp)    

		SELECT fi.fistamp FROM fi inner join ucrsHistPic on ALLTRIM(fi.ref) == ALLTRIM(ucrsHistPic.ref) WHERE fi.u_epvp = ucrsHistPic.preco AND ft.cdata >= ucrsHistPic.data INTO CURSOR uCrsLinhasPICsValidos READWRITE
		
		&&AND ft.fdata <= ucrsHistPic.data_fim;
		 
		UPDATE fi SET alertaPIC = IIF(ISNULL(ucrsLinhasPICsValidos.fistamp),.t.,.f.) from fi left join ucrsLinhasPICsValidos on fi.fistamp = ucrsLinhasPICsValidos.fistamp;
		Where ALLTRIM(fi.familia) == '1' OR ALLTRIM(fi.familia) == '2'

	ELSE
		
		SELECT fi
		IF ALLTRIM(fi.familia) == '1' OR ALLTRIM(fi.familia) == '2'
			
			SELECT fi.fistamp;
			FROM fi;
			inner join ucrsHistPic on ALLTRIM(fi.ref) = ALLTRIM(ucrsHistPic.ref);
			WHERE ;
				ALLTRIM(fi.fistamp) = ALLTRIM(lcFistamp);
				AND fi.u_epvp = ucrsHistPic.preco;
				AND ft.fdata >= ucrsHistPic.data;
			INTO CURSOR ucrsLinhasPICsValidos READWRITE
			
			
			SELECT ucrsLinhasPICsValidos 
			COUNT TO lcRegistos
			IF lcRegistos == 0
				UPDATE fi SET alertaPIC = .t. WHERE ALLTRIM(fi.fistamp) = ALLTRIM(lcFistamp) 
			ELSE
				UPDATE fi SET alertaPIC = .f. WHERE ALLTRIM(fi.fistamp) = ALLTRIM(lcFistamp)
			ENDIF 
		
			SELECT fi
			LOCATE FOR ALLTRIM(lcFistamp) == ALLTRIM(fi.fistamp)
			
		ENDIF 
		
	ENDIF 
ENDFUNC 


** 
FUNCTION uf_facturacao_DemCor
	
	SELECT Ft
	SELECT Ft2
	IF !empty(ALLTRIM(Ft2.token))
		IF !EMPTY(ALLTRIM(ft.u_tlote2))
			FACTURACAO.containerCab.lote2.disabledbackcolor = RGB(243,181,24)
			FACTURACAO.containerCab.lote2.disabledforecolor = RGB(255,255,255)
		ELSE
			IF !EMPTY(ALLTRIM(ft.u_tlote))
				FACTURACAO.containerCab.lote.disabledbackcolor = RGB(243,181,24)
				FACTURACAO.containerCab.lote.disabledforecolor = RGB(255,255,255)			
			ENDIF 
		ENDIF 
	ELSE
		FACTURACAO.containerCab.lote.disabledbackcolor = RGB(255,255,255)
		FACTURACAO.containerCab.lote.disabledforecolor = RGB(0,0,0)			
		FACTURACAO.containerCab.lote2.disabledbackcolor = RGB(255,255,255)
		FACTURACAO.containerCab.lote2.disabledforecolor = RGB(0,0,0)			
	ENDIF

ENDFUNC 


**
FUNCTION uf_facturacao_OrdenaPorDesignDoc

	IF myFtIntroducao == .t. OR myFtAlteracao == .t.

		IF facturacao.OrderDesign == .f.

			SELECT fi
			index on Upper(design) tag design 
			
			facturacao.OrderDesign = .t.
		ELSE
			
			SELECT fi
			index on Upper(design) tag design desc
							
			facturacao.OrderDesign = .f.
		ENDIF
		
		lcOrdemDasLinhas = 10000
		select fi
		GO TOP 
		SCAN
			replace fi.lordem with lcOrdemDasLinhas 
			lcOrdemDasLinhas = lcOrdemDasLinhas + 100
		ENDSCAN 	
		Select fi
		Go TOP 

		facturacao.refresh
	ENDIF
ENDFUNC 


** Caso data receita != data dia hoje, pergunta ao utilizador se pretende usar pre�os � data ou pre�os de hoje
FUNCTION uf_facturacao_calcCompData

		&& caso queira atualiza atualiza valores na fi � data
		&& ID_PRECO = 1   => pvp => [fi.u_epvp] 
		&& ID_PRECO = 101 => pref => [fi.u_epref]
		&& ID_PRECO = 301 => pvpMAX => [fi.pvpmaxre]
		&& ID_PRECO = 401 => pvpTOP5 

		&& verifica��es
		IF RECCOUNT("fi") = 0
			RETURN .f.
		ENDIF
		
		&& verifica se a data da receita � diferente do m�s atual -  luisleal 20160510
		IF uf_gerais_getdate(.f., "MES") != uf_gerais_getdate(ft.cdata,"MES")		
			IF uf_perguntalt_chama("A data da Receita refere-se a um m�s diferente do m�s atual. Pretende usar os pre�os em vigor � data seleccionada ?", "Sim", "N�o")
				SELECT fi 
				GO TOP 
				SCAN 			
					lcSQL = ''
					TEXT TO lcSQL TEXTMERGE NOSHOW
						exec up_facturacao_calcCompData '<<ALLTRIM(fi.ref)>>', '<<uf_gerais_getDate(Ft.cdata,"SQL")>>'
					ENDTEXT 
					
					uf_gerais_actGrelha("","uCrsAuxPrecos",lcSQL)
				
					IF RECCOUNT("uCrsAuxPrecos") > 0				
						
						&& atualiza valores fi
						SELECT uCrsAuxPrecos						
						replace fi.u_epvp   WITH IIF(uCrsAuxPrecos.PRECO_ID1 > 0 ,uCrsAuxPrecos.PRECO_ID1, fi.u_epvp)
						replace fi.u_epref  WITH IIF(uCrsAuxPrecos.PRECO_ID101 > 0 ,uCrsAuxPrecos.PRECO_ID101, fi.u_epref)
						replace fi.pvpmaxre WITH IIF(uCrsAuxPrecos.PRECO_ID301 > 0 ,uCrsAuxPrecos.PRECO_ID301, fi.pvpmaxre)	

						&& Calculo Comparticipa��es
						LOCAL tcCodigo, tcRef, tcDiploma
						STORE '' To tcCodigo, tcCodigo2, tcRef, tcDiploma
	
						tcCodigo	= UPPER(ALLTRIM(ft2.u_codigo))
						tcRef		= ALLTRIM(FI.REF)
						tcDiploma	= ALLTRIM(fi.u_diploma)
	
						** chama fun��o calculo de comparticipa��o
						uf_receituario_MakePrecos(tcCodigo, tcRef, tcRef, "fi", tcDiploma, .f., .t., .t.)

						Select fi && Nota: os restantes campos s�o substitu�dos na fun��o makePrecos
						replace fi.u_comp		WITH .t.
												
						** Recalcula Opcao **
						IF !(FI.opcao == 'I')
							replace fi.opcao	WITH IIF(fi.u_epvp > fi.pvpmaxre, 'S', 'N')
						ENDIF
						
						&& registar ocorr�ncia 
						LOCAL lcOrigem
						STORE '' TO lcOrigem
						
						Select ft
						lcOrigem = 'Doc.: ' + astr(ft.fno) + ' Ref.: '
						
						Select fi
						lcOrigem = lcOrigem + Alltrim(fi.ref)
						
						Select ft2
						lcOrigem = lcOrigem + ' Plano: ' + Alltrim(tcCodigo)
													
						SELECT fi
						uf_gerais_registaOcorrencia('RECEITU�RIO', 'Manipula��o de Comparticipa��o Autom�tica', 1, ALLTRIM(lcOrigem), '', ALLTRIM(fi.fistamp))
					
						&& actualizar totais linha
						uf_atendimento_fiiliq(.t.)
					
						&& Calcular percentagem de custo para o utente
*!*							Select fi
*!*							IF fi.epv!=0
*!*								IF fi.u_epvp=0
*!*									replace fi.u_epvp WITH fi.epv
*!*								ENDIF 
*!*								IF fi.qtt>0
*!*									Select fi
*!*									Replace fi.u_txcomp WITH Round((fi.etiliquido*100)/(fi.u_epvp*fi.qtt) ,2)
*!*								ENDIF 
*!*							ELSE 
*!*								Replace fi.u_txcomp WITH 0
*!*							ENDIF 
						
						&& actualizar totais
						uf_atendimento_actTotaisFt()

					ELSE
						uf_perguntalt_chama("N�o existem valores para a data seleccionada. Por favor contacte o Suporte. Obrigado.","OK","",48)
					ENDIF

					SELECT fi
				ENDSCAN 
			ELSE
				** n�o faz nada [para j�]
			ENDIF
		ENDIF
		
		IF USED("uCrsAuxPrecos")
			fecha("uCrsAuxPrecos")
		ENDIF			
ENDFUNC


**
FUNCTION uf_facturacao_reinserirReceita
	LOCAL lcQttOriginal, lcTokenOriginal
	
	IF !USED("FT")
		RETURN .f.
	ENDIF
	
	SELECT ft
	IF !EMPTY(ALLTRIM(ft.u_ltstamp)) OR !EMPTY(ALLTRIM(ft.u_ltstamp2))
		IF uf_gerais_validaPermUser(ch_userno, ch_grupo, 'Inser��o de Receita - Introduzir')
			IF uf_perguntalt_chama("ATEN��O: Vai reinserir uma receita j� faturada num m�s anterior." + CHR(13) + CHR(13) + "Tem a certeza que pretende continuar?", "Sim", "N�o") 
							
				select * from ft into cursor uCrsFtCorrigeReceita readwrite 
				select * from ft2 into cursor uCrsFt2CorrigeReceita readwrite 
				select * from fi into cursor uCrsFiCorrigeReceita readwrite 
				

				
				uf_Facturacao_novo(.t.)

				facturacao.containercab.nmdoc.value='Inser��o de Receita'
				facturacao.containercab.nmdoc.lostfocus()

				select * from uCrsFiCorrigeReceita into cursor fi readwrite

				uf_Facturacao_controlaWkLinhas()

				select ft
				
				LOCAL lcOfistamp, lcFtStamp
				STORE '' TO lcOfistamp, lcFtStamp
				
				lcFtSamp = ft.ftstamp
				
				LOCAL lcStamp
				
				SELECT fi
				GO TOP
				SCAN
				
					lcQttOriginal = fi.qtt
					
					lcStamp = uf_gerais_stamp()
					
					SELECT FI
					lcOfistamp = fi.fistamp
					replace fi.fistamp 		WITH lcStamp
					replace fi.ofistamp 	WITH lcOfistamp
					replace fi.nmdoc 		WITH ft.nmdoc
					replace fi.fno 			WITH ft.fno
					replace fi.ndoc 		WITH ft.ndoc
					replace	fi.u_psicont 	WITH 0
					replace	fi.u_bencont 	WITH 0
					replace fi.qtt 			WITH lcQttOriginal
			
				ENDSCAN
				
				SELECT fi
				GO TOP
				
				select * from uCrsFTCorrigeReceita into cursor ft readwrite 
				
				SELECT ft
				replace ft.nmdoc 		WITH fi.nmdoc
				replace ft.tipodoc 		WITH 4
				replace ft.fno 			WITH fi.fno
				replace ft.ftstamp 		WITH lcFtSamp
				replace ft.ndoc		 	WITH fi.ndoc
				replace ft.site 		WITH mySite
				replace ft.pnome 		WITH myTerm
				replace ft.pno 			WITH myTermNo
				replace ft.cxstamp 		WITH ''
				replace ft.cxusername 	WITH ''
				replace ft.sSstamp 		WITH ''
				replace ft.sSusername 	WITH ''
				replace ft.u_nratend 	WITH ''
				replace ft.ftid 		WITH 0 
				replace ft.u_lote 		WITH 0 
				replace ft.u_lote2 		WITH 0 
				replace ft.u_ltstamp 	WITH '' 
				replace ft.u_ltstamp2 	WITH ''
				replace ft.u_nslote 	WITH 0 
				replace ft.u_nslote2 	WITH 0
				replace ft.u_slote 		WITH 0 
				replace ft.u_slote2 	WITH 0
				replace ft.u_tlote 		WITH ''
				replace ft.u_tlote2 	WITH '' 
				replace ft.fdata		WITH datetime()+(difhoraria*3600)
				
				select * from uCrsFT2CorrigeReceita into cursor ft2 readwrite 
				SELECT ft2
				replace ft2.ft2stamp 	WITH lcFtSamp
				SELECT ft2
				UPDATE ft2 SET ft2.u_docorig = 'reinsercao receita' && para permitir eliminar estes docs sem eliminar o registo das receitas na BDNP	
				
				Public myInsereReceitaCorrigida
				myInsereReceitaCorrigida = .t.	
				
*!*					select fi && necessario para recalculo das comparticipa��es
*!*					GO TOP
*!*					SCAN
*!*						** atualizar para dados actuais	
*!*						uf_atendimento_actValoresFi()
*!*						** 
*!*					ENDSCAN
					
				** Actualizar Totais
				uf_atendimento_actTotaisFt()
				
				IF USED("uCrsFtCorrigeReceita")
					fecha("uCrsFtCorrigeReceita")
				ENDIF 
				IF USED("uCrsFt2CorrigeReceita")
					fecha("uCrsFt2CorrigeReceita")
				ENDIF
				IF USED("uCrsFiCorrigeReceita")
					fecha("uCrsFiCorrigeReceita")
				ENDIF				
			ENDIF
		ELSE
			uf_perguntalt_chama("O SEU PERFIL N�O PERMITE INTRODUZIR RECEITAS.","OK","",48)
			RETURN .f.
		ENDIF 
	ELSE
		uf_perguntalt_chama("ESTE DOCUMENTO N�O CONT�M RECEITA. POR FAVOR VERIFIQUE","OK","",64)
	ENDIF
ENDFUNC 


** funcao para calcular idade na faturacao
FUNCTION uf_faturacao_calcIdade
	LOCAL lcIdade
	STORE 0 TO lcIdade
	
	**
	IF uf_gerais_getdate(ft.nascimento,"SQL") != '19000101'
		lcIdade = DATE() - ctod(uf_gerais_getdate(ft.nascimento))
		lcIdade = transform(lcIdade/365,'99')	
	ELSE 
		lcIdade = str(lcIdade)
	ENDIF

	&& fix
	SELECT ft
	GO TOP 
		
	**
	IF VAL(lcIdade) <= 1 AND uf_gerais_getdate(ft.nascimento,"SQL") != '19000101'
	    SELECT nascimento, date(), ;
    	IIF(YEAR(nascimento)=YEAR(date()),MONTH(date())-MONTH(nascimento)+1, ;
        +(12*(YEAR(date())-YEAR(nascimento)-1)); 
        +MONTH(date())) As meses;
		 FROM ft INTO CURSOR uCrstempIdadeMeses READWRITE 
	
		facturacao.Pageframe1.page5.txtIDade.value = ASTR(uCrstempIdadeMeses.meses) + " Meses"
		
		fecha("uCrstempidademeses")
	ELSE 
		facturacao.Pageframe1.page5.txtIDade.value = ALLTRIM(lcIDade) + " Anos"
	ENDIF 
	
	facturacao.refresh
	
ENDFUNC 


FUNCTION uf_facturacao_imprimeDocPdf
	
	RELEASE FTSeguradora
	
	public FTSeguradora
    FTSeguradora= 'DOC'
    uf_imprimirgerais_Chama('FACTURACAO')
    IMPRIMIRGERAIS.hide
    IMPRIMIRGERAIS.pageframe1.page1.chkParaPDF.value = 1
    uf_imprimirGerais_gravar()
    uf_imprimirGerais_sair()

	
	RELEASE FTSeguradora
	
	
ENDFUNC


** Envio de NC e DEBITO para ao SNS
FUNCTION uf_facturacaoentidades_envioNCSNS
	LOCAL lcNomeJar, lcStampLigacao, lcStampFaturaSNS, lcIDCliente, lcAno, lcMes, lcResultadoFatura, lcDataAux, lcNrEntidade
	STORE '' TO lcNomeJar, lcStampLigacao, lcStampFaturaSNS, lcIDCliente, lcResultadoFatura 
	STORE 0 TO lcAno, lcMes
	


	&& Valida��es gerais software
	&& Verificar entidade [s� permite para SNS]
**	IF !UPPER(ALLTRIM(facturacaoentidades.lstEntidade.value)) == "SNS" AND !UPPER(ALLTRIM(facturacaoentidades.lstEntidade.value)) == "SICAD"
**		uf_perguntalt_chama("Funcionalidade dispon�vel apenas para o SNS e/ou SICAD.", "Ok", "", 16)			
**		RETURN .f.
**	ENDIF
	
	&& Tipo de Cliente
	IF ALLTRIM(ucrsE1.tipoEmpresa) != "FARMACIA"
		uf_perguntalt_chama("FUNCIONALIDADE APENAS DISPON�VEL PARA CLIENTES DO TIPO FARM�CIA. OBRIGADO.", "Ok", "", 16)
		RETURN .f.
	ENDIF 

	&& Valida a liga��o � Internet 
	IF uf_gerais_verifyInternet() == .f.
		uf_perguntalt_chama("ESTA FUNCIONALIDADE REQUER LIGA��O � INTERNET."+CHR(13)+"POR FAVOR VERIFIQUE A SUA LIGA��O.","OK","",16)
		RETURN .f.
	ENDIF
		
	&& N� de Contribuinte
	SELECT uCrsE1
	IF RECCOUNT("uCrsE1")>0
		IF EMPTY(uCrsE1.ncont)
			uf_perguntalt_chama("O NIF DA FARM�CIA N�O EST� CORRETAMENTE PREENCHIDO. POR FAVOR VERIFIQUE.", "Ok", "", 16)
			RETURN .f.
		ENDIF
	ENDIF

	&& Verifica a configura��o do c�digo de farm�cia
	SELECT ucrsE1
	IF EMPTY(ALLTRIM(ucrsE1.u_codfarm))
		uf_perguntalt_chama("O C�DIGO INFARMED DA FARM�CIA N�O EST� CORRETAMENTE PREENCHIDO. POR FAVOR VERIFIQUE.","OK","", 16)
		RETURN .f.
	ENDIF 
	
	&& Verifica a configura��o do ID do Cliente
	SELECT uCrsE1
	IF EMPTY(ALLTRIM(uCrsE1.id_lt))
		uf_perguntalt_chama("O C�DIGO DE CLIENTE LOGITOOLS N�O EST� CORRETAMENTE PREENCHIDO. POR FAVOR CONTACTE O SUPORTE.","OK","", 16)
		RETURN .f.
	ENDIF 
	lcIDCliente = ALLTRIM(uCrsE1.id_lt)
	**

	&& buscar ftstamp da fatura sns referente ao periodo definido no painel para verificar se envio j� foi aceite
	**lcDataAux = ALLTRIM(str(myfacturacaoentidadesAno)) + Substr("00",1,Len("00")-Len(ALLTRIM(str(myfacturacaoentidadesMes)))) + ALLTRIM(str(myfacturacaoentidadesMes)) + '01'
	**lcNrEntidade = IIF(UPPER(ALLTRIM(facturacaoentidades.lstEntidade.value)) == "SNS", 1, 66)
	**_cliptext=lcDataAux 
	
	lcSQL = ''
*!*		TEXT TO lcSQL TEXTMERGE NOSHOW 
*!*			select TOP 1 ftstamp 
*!*			FROM ft (nolock) 
*!*			INNER JOIN td (nolock) ON td.ndoc = ft.ndoc 
*!*			WHERE ft.no = 1 AND td.u_tipodoc = 1 AND ft.cdata = DATEADD(DAY, -1, DATEADD(MONTH, DATEDIFF(MONTH, 0, '<<dtosql(ft.fdata)>>')+1, 0)) AND ft.site = '<<mySite>>' 
*!*			order by ft.fno desc
*!*		ENDTEXT 

*!*		IF !uf_gerais_actGrelha("","uCrsAuxFT",lcSQL)
*!*			uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VERIFICAR A INFORMA��O DA FACTURA. POR FAVOR CONTACTE O SUPORTE.", "Ok", "", 16)
*!*			regua(2)
*!*			RETURN .f.
*!*		ELSE
*!*			IF RECCOUNT("uCrsAuxFT") < 1
*!*				uf_perguntalt_chama("AINDA N�O FOI EMITIDA FACTURA PARA O PERIODO SELECCIONADO. POR FAVOR VERIFIQUE.", "Ok", "", 16)
*!*				regua(2)
*!*				RETURN .f.
*!*			ELSE
*!*				select uCrsAuxFT
*!*				lcStampFaturaSNS = ALLTRIM(uCrsAuxFT.ftstamp)	
*!*			ENDIF
*!*		ENDIF 
	
	&& verificar se fatura j� foi enviada e envia novamente caso n�o tenha sido
	lcSQL = ''
	TEXT TO lcSQL TEXTMERGE NOSHOW 
		select TOP 1 ftstamp FROM [faturacao_eletronica_med] WHERE aceite = 1 AND ftstamp = '<<ALLTRIM(ft.ftstamp)>>' 
	ENDTEXT 
	IF !uf_gerais_actGrelha("","uCrsAuxFTStAMP",lcSQL)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VERIFICAR A INFORMA��O DO DOCUMENTO . POR FAVOR CONTACTE O SUPORTE.", "Ok", "", 16)
		regua(2)
		RETURN .f.
	ELSE
		IF RECCOUNT("uCrsAuxFTStAMP") > 0
			uf_perguntalt_chama("DOCUMENTO J� ENVIADO. POR FAVOR VERIFIQUE.", "Ok", "", 16)
			regua(2)
			RETURN .f.
		ELSE
			&& Procede ao envio da fatura
			regua(0,4,"A enviar Fatura Eletronicamente...")
			&&
			regua(1,1,"A configurar software para envio...")
			
			&& Emitirar Fatura em PDF e guardar na pasta cache - deve guardar IDCliente + MesFatura + DataEnvio
			SELECT uCrsE1
			

			uf_facturacao_imprimeDocPdf()					
			
	
			&& Verifica se software existe
			lcNomeJar = 'FaturacaoEletronicaMedicamentos.jar'
			
			IF !FILE(ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\ltsFaturacaoEletronica\' + ALLTRIM(lcNomeJar))
				uf_perguntalt_chama("FICHEIRO DE CONFIGURA��O N�O ENCONTRADO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				regua(2)
				RETURN .f.
			ENDIF 
			
			&& Verifica o servidor de BD
			IF !USED("uCrsServerName")
				lcSQL  = ''
				TEXT TO lcSQL NOSHOW TEXTMERGE 
					Select @@SERVERNAME as dbServer
				ENDTEXT 
				IF !uf_gerais_actGrelha("", "ucrsServerName", lcSQL)
					uf_perguntalt_chama("OCORREU UMA ANOMALIA. POR FAVOR CONTACTE O SUPORTE." ,"OK","",16)
					regua(2)
					RETURN  .f.
				ENDIF
			ENDIF
			
			SELECT ucrsServerName
			lcDbServer = ALLTRIM(ucrsServerName.dbServer)
			
			&&
			regua(1,2,"A recolher informa��o para envio...")
			&& recolher informa��o daqui
			&& campo cdata na ft tem a data do ultimo dia ao mes que corresponde o documento
			lcStampLigacao = uf_gerais_stamp()
			lcAno = ALLTRIM(STR(YEAR(ft.fdata)))
			lcMes = ALLTRIM(STR(MONTH(ft.fdata)))
			lcResultadoFatura = ALLTRIM(STR(0))
			
			&&
			regua(1,3,"A enviar informa��o...")
			&& efetua envio da fatura
			lcWsPath = ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\ltsFaturacaoEletronica\' + ALLTRIM(lcNomeJar);
					+ " " + ALLTRIM(lcStampLigacao);	
					+ " " + ALLTRIM(ft.ftstamp);
					+ " " + lcIDCliente;
					+ " " + lcAno;
					+ " " + lcMes;
					+ " " + ["] + ALLTRIM(mySite) + ["];
					+ " " + lcResultadoFatura;
					+ " 0"
			**_cliptext=lcWsPath
			&& Envia comando Java
			lcWsPath = "javaw -jar " + lcWsPath 	
							
			&& aguarda que software termine o envio da informa��o 3� PARAMETRO = .T.
			oWSShell = CREATEOBJECT("WScript.Shell")
			oWSShell.Run(lcWsPath, 1, .t.)
			
			&& verifica resposta 
			regua(1,4,"A verificar resposta dos Servi�os Partilhados...")

			lcSQL = ''
			TEXT TO lcSQL TEXTMERGE NOSHOW 
				select TOP 1 * FROM faturacao_eletronica_med (nolock) WHERE ftstamp = '<<ALLTRIM(ft.ftstamp)>>'  ORDER BY data desc
			ENDTEXT 
			IF !uf_gerais_actGrelha("","uCrsAuxResposta",lcSQL)
				uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VERIFICAR A RESPOSTA DOS SERVI�OS PARTILHADOS. POR FAVOR CONTACTE O SUPORTE.", "Ok", "", 16)
				regua(2)
				RETURN .f.
			ELSE
				IF RECCOUNT("uCrsAuxResposta") > 0 
					IF uCrsAuxResposta.aceite = .t.
						regua(2)
						uf_perguntalt_chama("ENVIO DO DOCUMENTO EFETUADO COM SUCESSO.", "Ok", "", 16)
						RETURN .t.
					ELSE
						lcSQL = ''
						TEXT TO lcSQL TEXTMERGE NOSHOW 
							select * FROM faturacao_eletronica_med_d (nolock) WHERE id_faturacao_eletronica_med = '<<uCrsAuxResposta.id>>' 
						ENDTEXT 	
						IF !uf_gerais_actGrelha("facturacaoentidades.gridResposta","uCrsAuxRespostaDet",lcSQL)
							uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VERIFICAR A RESPOSTA DOS SERVI�OS PARTILHADOS. POR FAVOR CONTACTE O SUPORTE.", "Ok", "", 16)
							regua(2)
							RETURN .f.
						ELSE
							&& colocar informa��o detalhada do erro aqui - 
							uf_perguntalt_chama("O DOCUMENTO ENVIDADO CONT�M ERROS. POR FAVOR CONTACTE O SUPORTE T�CNICO PARA AN�LISE.", "Ok", "", 16)
							facturacaoentidades.refresh
							regua(2)
							RETURN .f.
						ENDIF
					ENDIF
				ELSE
					regua(2)
					uf_perguntalt_chama("OCORREU UMA ANOMALIA AO ENVIAR O DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.", "Ok", "", 16)
					RETURN .t.
				ENDIF
			ENDIF

			regua(2)
		ENDIF
	ENDIF
	
	IF USED("uCrsAuxFT")
		fecha("uCrsAuxFT")
	ENDIF
	
	IF USED("uCrsAuxFTStAMP")
		fecha("uCrsAuxFTStAMP")
	ENDIF
	
	IF USED("uCrsAuxResposta")
		fecha("uCrsAuxResposta")
	ENDIF
		
	IF USED("uCrsAuxRespostaDet")
		fecha("uCrsAuxRespostaDet")
	ENDIF
				
ENDFUNC

*!*	Valida nos casos em que o numero do documento � editavel se esse numero ja existe para o ano em questao ou nao.
FUNCTION uf_factura��o_ValidaNumDoc

	LOCAL uCrsVerificaFno
	IF (EMPTY(FACTURACAO.ContainerCab.nmdoc.value) or  FACTURACAO.ContainerCab.numdoc.value == 0 )
		uf_facturacao_controlaNumeroFact(year(datetime()+(difhoraria*3600)))
		RETURN .f.
	ENDIF 
	
	lcSQL = ''
	TEXT TO lcSQL TEXTMERGE NOSHOW 
		SELECT  * FROM ft (nolock) WHERE ft.ftano = '<<YEAR(FACTURACAO.ContainerCab.docdata.value)>>'  AND  ft.nmdoc='<<FACTURACAO.ContainerCab.nmdoc.value>>' and  ft.fno=<<FACTURACAO.ContainerCab.numdoc.value>>
	ENDTEXT 
	IF !uf_gerais_actGrelha("","uCrsVerificaFno",lcSQL)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VERIFICAR O N�MERO DO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.", "Ok", "", 16)
		regua(2)
		RETURN .f.
	ELSE
		IF RECCOUNT("uCrsVerificaFno") > 0 
			uf_facturacao_controlaNumeroFact(year(datetime()+(difhoraria*3600)))
			uf_perguntalt_chama("Esse N�mero de Documento j� esta registado por favor verifique o N�mero ou edite o documento j� guardado. ", "Ok", "", 16)
			regua(2)
			RETURN .f.
		ENDIF 	
	ENDIF 
	
ENDFUNC 



** funcao para gravar certificacao no atendimento
FUNCTION uf_facturacao_MudaFNO	
	PARAMETERS lcFno
	LOCAL lcExecuteSQL , lcUpdateFt, lcUpdateFt  , lcUpdateFi 
	STORE "" TO lcExecuteSQL , lcUpdateFt , lcUpdateFt2 , lcUpdateFi 

	
	lcExecuteSQL = ''

		
	TEXT TO lcUpdateFt NOSHOW TEXTMERGE 
		UPDATE ft SET fno =<<lcFno>> WHERE ftstamp = '<<FT.ftstamp>>'
	ENDTEXT

	
	TEXT TO lcUpdateFi NOSHOW TEXTMERGE 
		UPDATE fi SET fno =<<lcFno>> WHERE ftstamp= '<<FT.ftstamp>>'
	ENDTEXT

	

	lcExecuteSQL  = lcUpdateFt + CHR(13) + lcUpdateFi 
	


	RETURN lcExecuteSQL  
	
ENDFUNC 



FUNCTION uf_faturacao_valida_artigo_sem_stock
	LOCAL lcStkLin, lcPassaValidacao
	STORE 0 TO lcStkLin, lcStkST, lcSTNS
	STORE .t. TO lcPassaValidacao
	STORE .f. TO lcSTNS
	IF !uf_gerais_getParameter_site('ADM0000000086', 'BOOL', mySite) AND myFtIntroducao = .T.
		SELECT fi
		GO TOP 
		select ref, sum(qtt) as qtt from fi where !empty(ref) AND EMPTY(ofistamp) AND partes=0 group by ref into cursor ucrstotqtts readwrite
		SELECT ucrstotqtts
		GO TOP 
		SCAN  
			lcStkLin = ucrstotqtts.qtt
			
			LOCAL lcIntBit, lcDecBit, lcConversao
			TEXT TO lcSQL NOSHOW textmerge
				select stock, stns from st (nolock) where ref='<<ALLTRIM(ucrstotqtts.ref)>>' and site_nr=<<mysite_nr>>
			ENDTEXT
			IF !uf_gerais_actgrelha("", "uCrsQttST", lcSql)
				uf_perguntalt_chama("OCORREU UMA ANOMALIA A GUARDAR O NOVO N�MERO DE RECEITA", "OK",16)
				RETURN .f.
			ENDIF
			SELECT uCrsQttST
			lcStkST = uCrsQttST.stock
			lcSTNS = uCrsQttST.stns
			fecha("uCrsQttST")
			
			SELECT ucrstotqtts
			IF lcStkLin > lcStkST AND lcSTNS = .f.
				uf_perguntalt_chama("O ARTIGO " + ALLTRIM(ucrstotqtts.ref) + " N�O TEM STOCK SUFICIENTE PARA FINALIZAR A VENDA. POR FAVOR RETIFIQUE!","OK","",48)
				lcPassaValidacao = .f.
			ENDIF 
			
		ENDSCAN 
		FECHA("ucrstotqtts")
	ENDIF 
	IF lcPassaValidacao
		RETURN .t. 
	ELSE
		SELECT fi
		GO TOP
		RETURN .f. 
	ENDIF 
ENDFUNC 

FUNCTION uf_faturacao_export_pdf_auto
*!*		public lcexportsyntstamp
*!*		LOCAL lcCont2, lcCharSep, LcVar, lcNrLin
*!*		lcCont2 = 1 
*!*		lcNrLin = 0
*!*		STORE "" TO lcexportsyntstamp, lcCharSep, LcVar
*!*		
*!*		** Selecionar pasta de destino para os ficheiros
*!*		oShell = CREATEOBJECT("Shell.Application")
*!*		oFolder = oShell.Application.BrowseForFolder(_screen.HWnd, "SELECCIONE A PASTA DE DESTINO:", 1)
*!*		if isnull(oFolder)
*!*			RETURN .f.
*!*		ELSE
*!*			? oFolder.self.Path
*!*		ENDIF
*!*		myFilePathSeq = oFolder.self.path +"\"

*!*		lcSQL = ''
*!*		TEXT TO lcSQL NOSHOW TEXTMERGE 
*!*			exec up_touch_ft '<<ALLTRIM(ft.ftstamp)>>'
*!*		ENDTEXT 
*!*		uf_gerais_actGrelha("","FT",lcSQL)
*!*		
*!*		lcSQL = ''
*!*		TEXT TO lcSQL NOSHOW TEXTMERGE 
*!*			SELECT respostaDt = isnull((select top 1 DescrResposta from b_dt_resposta WHERE stampDoc = '<<ALLTRIM(ft.ftstamp)>>' order by DataEnvio),''),* FROM FT2 (nolock) WHERE ft2stamp='<<ALLTRIM(ft.ftstamp)>>'
*!*		ENDTEXT 
*!*		uf_gerais_actGrelha("","FT2",lcSQL)
*!*		
*!*		
*!*		lcSQL = ''
*!*		TEXT TO lcSQL NOSHOW TEXTMERGE 
*!*			exec up_facturacao_linhas 0, <<mysite_nr>>, '<<ALLTRIM(ft.ftstamp)>>'
*!*		ENDTEXT 
*!*		uf_gerais_actGrelha("","FI",lcSQL)	
*!*		
*!*		SELECT Ft
*!*		SELECT Ft2
*!*		SELECT Fi
*!*		GO TOP 
*!*		
*!*		IF USED("TD")
*!*			fecha("TD")
*!*		ENDIF

*!*		SELECT * FROM uCrsGeralTD WHERE uCrsGeralTD.ndoc == ft.ndoc AND ALLTRIM(uCrsGeralTD.site) == ALLTRIM(mySite) INTO CURSOR TD READWRITE
*!*		myFileNameSeq = ""
*!*		myFileNameSeq = ALLTRIM(STR(ft.fno))+'_'+ALLTRIM(STR(ft.ndoc))+'_'+ALLTRIM(str(year(ft.fdata)))+ALLTRIM(str(month(ft.fdata)))+ALLTRIM(str(day(ft.fdata)))+'_'+alltrim(ft.nmdoc)+'.pdf'
*!*		myFileNameSeq = STRTRAN(ALLTRIM(myFileNameSeq),'.','')
*!*		SELECT ft
*!*		SELECT ft2
*!*		SELECT fi
*!*		
*!*		myFileTableSeq = "FI"
	
	**uf_imprimirgerais_chama('FACTURACAO', .t., .f., .f.)

	
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		Select Top 1 nomeficheiro, tabela, temduplicados, numduplicados
		From b_impressoes (nolock)
		Where tabela='FT' and documento='<<alltrim(ft.nmdoc)>>' and idupordefeito=1
	ENDTEXT
	If !uf_gerais_actGrelha("","templcSQL",lcSQL)
		uf_perguntalt_chama("N�O FOI POSS�VEL ENCONTRAR A CONFIGURA��O DE IMPRESS�ES. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		Return
	ENDIF
	If Reccount("templcSQL") > 0
		lcnomeFicheiro = Alltrim(templcSQL.nomeficheiro)
	ELSE
		RETURN .f.
	ENDIF 
	lcreport = Alltrim(myPath)+"\analises\" + Alltrim(lcnomeFicheiro)
					
	SELECT ft 
	lcNomeFicheiroPDF = ALLTRIM(STR(ft.fno))+'_'+ALLTRIM(STR(ft.ndoc))+'_'+ALLTRIM(str(year(ft.fdata)))+ALLTRIM(str(month(ft.fdata)))+ALLTRIM(str(day(ft.fdata)))+'_'+alltrim(ft.nmdoc)+'.pdf'
	uf_guarda_pdf_folder(ALLTRIM(lcreport), STRTRAN(STRTRAN(ALLTRIM(lcNomeFicheiroPDF),'/',''),' ',''), 'FI','c:\temp')
ENDFUNC 


FUNCTION uf_facturacao_info_emb
	
	RETURN .t.
ENDFUNC

**
*******************************
** Fun��o Nova
*******************************
FUNCTION uf_facturacao_scanner_lin
    LPARAMETERS uv_scanned

	uv_retval = .F.

    IF !myftalteracao AND !myftintroducao
        RETURN uv_retval
    ENDIF

    IF EMPTY(uv_scanned)
        RETURN uv_retval
    ENDIF

	IF uf_gerais_getUmValor("st", "count(*)", "ref = '" + ALLTRIM(uv_scanned) + "' and site_nr = " + ASTR(mySite_nr)) = 0 

		uv_altRef = uf_gerais_getUmValor("bc", "ref", "codigo = '" + ALLTRIM(uv_scanned) + "' and site_nr = " + ASTR(mySite_nr))

		IF !EMPTY(uv_altRef)
			uv_scanned = ALLTRIM(uv_altRef)
		ENDIF

	ENDIF
    

    SELECT FI
    GO TOP
    LOCATE FOR ALLTRIM(fi.ref) == ALLTRIM(uv_scanned)
    IF FOUND()

        SELECT FI
        REPLACE fi.qtt WITH (fi.qtt + 1)

        uv_retval = .T.

    ELSE
        
		SELECT FI

		IF EMPTY(FI.REF) AND EMPTY(FI.DESIGN)
        	uf_facturacao_addLinha()    
		ENDIF

        SELECT fi

        replace fi.ref with uv_scanned

        uf_facturacao_eventoRefFi()    

        uv_retval = .T.

        SELECT fi

        IF EMPTY(fi.ref) AND EMPTY(fi.design) AND fi.qtt = 0

            uf_facturacao_remLinha()

            uv_retval = .F.

        ENDIF

    ENDIF

	uf_atendimento_actValoresFi()
	uf_atendimento_actTotaisFt()

    FACTURACAO.refresh

    RETURN uv_retval
ENDFUNC

**
*******************************
** Fun��o Original
*******************************
FUNCTION uf_faturacao_scanner
	&& valida preenchimento
	
	
	If Empty(alltrim(facturacao.txtscanner.value))
		RETURN .F.
	ENDIF
	
	Local lcRef
	Store "" To lcRef
	
	lcRef = UPPER(alltrim(facturacao.txtscanner.value))
	DO CASE 
		CASE LEN(lcRef) >= 13 && n� receita
			
			TEXT TO lcSQL NOSHOW TEXTMERGE 
				select TOP 1 ft2stamp from ft2 where ft2.u_receita = '<<ALLTRIM(lcRef)>>' ORDER BY ft2.ousrdata DESC, ft2.ousrhora desc
			ENDTEXT 
			IF !uf_gerais_actgrelha("", "ucrsProcuraReceita", lcSQL)
				uf_perguntalt_chama("Ocorreu um erro na procura da receita introduzida. Por favor contacte o suporte","OK","",16)
				return .f.
			ENDIF
			IF RECCOUNT("ucrsProcuraReceita") == 0
				facturacao.txtscanner.value = ""
				RETURN .f.
			ELSE
				SELECT ucrsProcuraReceita
				IF !EMPTY(ucrsProcuraReceita.ft2stamp)
					uf_facturacao_chama(ucrsProcuraReceita.ft2stamp)
				ENDIF 
			ENDIF 	
	ENDCASE 
ENDFUNC 


FUNCTION uf_facturacao_retornaFtStampOriginal

	LOCAL lcSQl
	lcSql = ""
	LOCAL  lcPosFi, lcFtStamp, lcOFiStamp
	
	STORE  "" TO lcOFiStamp, lcFtStamp
	
	if(!USED("fi"))	
		RETURN ""
	ENDIF
	
	
	SELECT FI
	lcPosFi= RECNO("FI")
	
	
	SELECT fi
	GO TOP
	SCAN FOR !EMPTY(ALLTRIM(fi.ofistamp))
		lcOFiStamp = ALLTRIM(fi.ofistamp)
	ENDSCAN
	
	fecha("ucrsFtStampOrginal")
	
	if(!EMPTY( ALLTRIM(lcOFiStamp)))
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			select TOP 1 ftstamp as ftstamp  from fi(nolock) where fi.fistamp= '<<ALLTRIM(lcOFiStamp)>>' ORDER BY fi.rdata desc
		ENDTEXT 
		IF !uf_gerais_actgrelha("", "ucrsFtStampOrginal", lcSQL)
			uf_perguntalt_chama("Ocorreu um erro na procura da de venda original. Por favor contacte o suporte","OK","",16)
			return ""
		ENDIF
	ENDIF
	
	if(USED("ucrsFtStampOrginal"))
		lcFtStamp = ALLTRIM(ucrsFtStampOrginal.ftstamp)
	ENDIF
	
	
	fecha("ucrsFtStampOrginal")
	
	
		
	SELECT FI
	TRY
		GO lcPosFi
	CATCH
		GO TOP
	ENDTRY
	
	RETURN lcFtStamp
	

ENDFUNC 




****************************************************
** retorna SQL para alterar comparticipa��o externa
****************************************************
FUNCTION uf_facturacao_actualizacaCompartExterna
	
	LOCAL lcSQl
	lcSql = ""
	LOCAL lnPosFt2, lcPosFi, lcOFtStamp, lcOFiStamp, lcTokenExterno, lcFtStamp  
	
	STORE "" TO  lcOFtStamp, lcOFiStamp, lcTokenExterno, lcFtStamp  
	
	SELECT FT2
	lnPosFt2 = RECNO("FT2")
	
	SELECT FI
	lcPosFi= RECNO("FI")

	
	IF(VARTYPE(myInsereReceitaCorrigida)=='U')
		RETURN ""
	ENDIF	
	
	IF(EMPTY(myInsereReceitaCorrigida))
		RETURN ""
	ENDIF
	
		
	IF(!USED("FT2"))
		RETURN ""
	ENDIF	
	
	IF(!USED("fi"))
		RETURN ""
	ENDIF	
	
	
	SELECT ft2	
	IF TYPE("ft2.token_efectivacao_compl")=='U'
		RETURN ""
	ENDIF
	

	
	SELECT FT2
	GO TOP
	IF(EMPTY(ALLTRIM(ft2.token_efectivacao_compl)))
		RETURN ""
	ENDIF
	

	
	
	SELECT FT2
	GO TOP
	IF(!EMPTY(ALLTRIM(ft2.token_efectivacao_compl)))
		lcTokenExterno = ALLTRIM(ft2.token_efectivacao_compl)
		lcFtStamp  = ALLTRIM(ft2.ft2stamp)
	ENDIF
	

	
	lcOFtStamp = uf_facturacao_retornaFtStampOriginal()
	IF(EMPTY(ALLTRIM(lcOFtStamp)))
		RETURN ""
	ENDIF

	lcSql  = uf_PAGAMENTO_ActualizaCompartExterna(lcFtStamp  ,lcOFtStamp, lcTokenExterno)
	
	SELECT FI
	TRY
		GO lcPosFi
	CATCH
		GO TOP
	ENDTRY
	
	
		
	SELECT ft2
	TRY
		GO lnPosFt2
	CATCH
		GO TOP
	ENDTRY

		
	RETURN 	lcSql 

ENDFUNC 