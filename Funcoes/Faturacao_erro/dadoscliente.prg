FUNCTION uf_dadoscliente_Chama
	If Used("FT")
		Local lcSQL
		Select Ft
		Go Top
		CREATE CURSOR uc_dadosCliente(nome varchar(55), morada varchar(55), local varchar(43), codpost varchar(45), ncont varchar(20), telefone varchar(60), ccusto varchar(20))
		SELECT uc_dadosCliente
		APPEND BLANK
		replace ;
			nome with ft.nome;
			morada with ft.morada; 
			local with ft.local;
			codpost with ft.codpost;
			ncont with ft.ncont;
			telefone with ft.telefone;
			ccusto with ft.ccusto
	ELSE
		RETURN .f.
	ENDIF
	
	IF TYPE("dadoscliente")=="U"
		DO FORM dadoscliente
	ELSE
		dadoscliente.show
	ENDIF
ENDFUNC

FUNCTION uf_dadoscliente_init
	IF type("dadoscliente.menu1.rechama") == "U"
		dadoscliente.menu1.adicionaOpcao("rechama","Rechamar Dados",myPath + "\imagens\icons\actualizar_w.png","uf_dadoscliente_rechamarDadosCliente","F1")
	ENDIF
	
	dadoscliente.menu1.estado("","","ACTUALIZAR",.t.)
	
	dadoscliente.txtnomedocumento.value	= Ft.nmdoc
	dadoscliente.txtnumdocumento.value	= Ft.fno
ENDFUNC 

Function uf_dadoscliente_rechamarDadosCliente
	Local lcSQL
	
	lcSQL = "Select nome, morada, local, codpost, ncont, telefone,ccusto From b_utentes (nolock) Where NO = " + Str(Ft.no) + " AND ESTAB = " + Str(Ft.estab)
	If !uf_gerais_actGrelha("","uc_dadosCliente",lcSQL)
		uf_perguntalt_chama("N�O FOI POSS�VEL VERIFICAR OS DADOS DO CLIENTE. POR FAVOR CONTACTE A ASSIST�NCIA T�CNICA","OK","",16)
		RETURN .f.
	Endif

	dadoscliente.refresh
Endfunc

Function uf_dadoscliente_gravar
	Select uc_dadosCliente
	Select Ft
	REPLACE Ft.Nome	WITH	uc_dadosCliente.Nome
	REPLACE Ft.Morada	WITH	uc_dadosCliente.Morada
	REPLACE Ft.Local	WITH	uc_dadosCliente.Local
	REPLACE Ft.codpost	WITH	uc_dadosCliente.codpost
	REPLACE Ft.ncont	WITH	uc_dadosCliente.ncont
	REPLACE Ft.telefone	WITH	uc_dadosCliente.telefone
	REPLACE ft.ccusto	WITH	uc_dadosCliente.ccusto
	
	uf_perguntalt_chama("DADOS ACTUALIZADOS COM SUCESSO.","OK","",64)
	
	facturacao.refresh
	uf_dadoscliente_sair()
Endfunc

**
FUNCTION uf_dadoscliente_sair
	If Used("uc_dadosCliente")
		Fecha("uc_dadosCliente")
	ENDIF
	
	dadoscliente.hide
	dadoscliente.release
ENDFUNC