FUNCTION uf_PESQLOTE_chama
	LPARAMETERS lcLote, lcRef, lcEcra, lcTipoDoc
	
	IF TYPE("PESQLOTE") != "U"
		PESQLOTE.show
	ELSE
		
		IF !USED("ucrsPesqLote")
			lcSQL = ""
			TEXT TO lcSql NOSHOW TEXTMERGE 
				exec up_ga_loteDetalhePrecos '<<lcLote>>', '<<lcRef>>', <<IIF(EMPTY(lcTipoDoc),0,lcTipoDoc)>>
			ENDTEXT
		
			uf_gerais_actGrelha("","ucrsPesqLote",lcSQL)
		ENDIF
		
		DO FORM PESQLOTE
	ENDIF 
	
	PESQLOTE.local = lcEcra
	PESQLOTE.lote.value = lcLote
	PESQLOTE.ref = lcRef
	PESQLOTE.tipoDoc = IIF(EMPTY(lcTipoDoc),0,lcTipoDoc)

	uf_PESQLOTE_CarregaDados()
ENDFUNC 


** 
FUNCTION uf_PESQLOTE_CarregaDados

	lcSQL = ""
	TEXT TO lcSql NOSHOW TEXTMERGE 
	 	exec up_ga_loteDetalhePrecos '<<ALLTRIM(PESQLOTE.lote.value)>>','<<PESQLOTE.ref>>',0
	ENDTEXT
	uf_gerais_actGrelha("PESQLOTE.gridPesq","ucrsPesqLote",lcSQL)
	
	
	IF PESQLOTE.local == "FACTURACAO"
		
		IF USED("ucrsQtAplicadaLoteAtual")
			fecha("ucrsQtAplicadaLoteAtual")
		ENDIF 
		
		** Calcula qtt j� aplicada da referencia por lote
		select SUM(qtt) as qtt,ref,lote from fi where !EMPTY(lote) group by ref, lote INTO CURSOR ucrsQtAplicadaLoteAtual READWRITE
		
		UPDATE ucrsPesqLote;
		SET ucrsPesqLote.stock = ucrsPesqLote.stock - ucrsQtAplicadaLoteAtual.qtt;
		From ucrsPesqLote inner join ucrsQtAplicadaLoteAtual on ucrsPesqLote.ref = ucrsQtAplicadaLoteAtual.ref AND ucrsPesqLote.lote = ucrsQtAplicadaLoteAtual.lote

		
	ENDIF
ENDFUNC
	
	
**
FUNCTION uf_pesqLote_escolha

	**	
	DO CASE 
		CASE pesqLote.local == "FACTURACAO" 
						
			IF PESQLOTE.tipoDoc != 3
				select ucrsPesqLote
				IF !UF_FACTURACAO_ATRIBUILOTEALTERAQTT(ucrsPesqLote.lote)
					RETURN .f.
				ENDIF
			ENDIF 
			
			IF ucrsPesqLote.stock <= 0
				uf_perguntalt_chama("N�o existe stock suficiente no Lote.","OK","",32)
				RETURN .f.
			ENDIF 
			
			select ucrsPesqLote
			select fi
			replace fi.amostra with .t. && Na altera�ao considera o valor alterado
			replace fi.lote    with ucrsPesqLote.lote

			uf_FACTURACAO_divideLote(fi.ref, fi.qtt, fi.fistamp)

			uf_atendimento_fiiliq(.t.)	
			uf_atendimento_actTotaisFt()
		
		
		CASE pesqLote.local == "CONFENCOMENDAS" 	
	
			Select ucrsPesqLote
			Select ucrsFn
			
			select ucrsFn
			Replace ucrsFn.LOTE WITH ucrsPesqLote.lote
			
		CASE pesqLote.local == "CONFAVIAMENTOS" 	
	
			Select ucrsPesqLote
			Select ucrsFn
			
			** Caso n�o exista stock, n�o deixa fazer atribui��o
			IF  ucrsPesqLote.stock < ucrsfn.qttrec
				uf_perguntalt_chama("N�o existe stock suficiente no Lote.","OK","",32)
			ELSE
				select ucrsFn
				Replace ucrsFn.LOTE WITH ucrsPesqLote.lote
			ENDIF
						
		OTHERWISE
			**
		
	ENDCASE	
	
	uf_PesqLote_sair()
ENDFUNC


**
FUNCTION uf_PESQLOTE_sair
	IF USED("ucrsPesqLote")
		fecha("ucrsPesqLote")
	ENDIF

	PESQLOTE.hide
	PESQLOTE.release
ENDFUNC

