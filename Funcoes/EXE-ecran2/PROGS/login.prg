**
PROCEDURE uf_login_chama
 PUBLIC lcUtilPortal
 STORE '' TO lcUtilPortal

 SELECT uCrsDirectorios
	IF RECCOUNT("uCrsDirectorios")=1 
 	lcTipoEntrada = 'EMPRESA'
 ELSE
	lcTipoEntrada = 'PORTAL'
	myusername = 'LTSPORTAL'
 ENDIF 
 
 FOR EACH mf IN painelcentral.objects
    IF LIKE("*PAGINA*", UPPER(ALLTRIM(mf.name)))==.T.
       painelcentral.removeobject(mf.name)
    ENDIF
 ENDFOR
 LOCAL lcval
 STORE .F. TO lcval
 CREATE CURSOR uCrsEmpresas (nome C (60), odbc C (200), nomebd C (200), cs C (254), marcado L, act M, default 'C' (1), lastuser C (30), 'TitleBar' 'C' (1))
 LOCAL lcficheiroini
 PUBLIC lcficheiroinipath
 SELECT ucrsdirectorios
 GOTO TOP
 SCAN
    IF FILE(ALLTRIM(ucrsdirectorios.directorio)+'\'+myusername+'\config.ini')
       lcficheiroini = UPPER(FILETOSTR(ALLTRIM(ucrsdirectorios.directorio)+'\'+myusername+'\config.ini'))
       EXIT
    ENDIF
 ENDSCAN
 IF EMPTY(lcficheiroini)
    MESSAGEBOX('O ficheiro de configura��o do cliente n�o existe! Por favor contacte o Suporte. Obrigado.', 16, 'Logitools Software')
    QUIT
 ENDIF
 lcficheiroinipath = ALLTRIM(ucrsdirectorios.directorio)+'\'+myusername+'\config.ini'
 FOR lclinha = 1 TO (MEMLINES(lcficheiroini))
    lcstringlinha = MLINE(lcficheiroini, lclinha)
    DO CASE
       CASE UPPER('Empresa=')$lcstringlinha
          lcnomeempresa = ALLTRIM(STREXTRACT(lcstringlinha, UPPER('Empresa=')))
          SELECT ucrsempresas
          APPEND BLANK
          REPLACE ucrsempresas.nome WITH ALLTRIM(lcnomeempresa)
       CASE UPPER('BaseDados=')$lcstringlinha
          SELECT ucrsempresas
          REPLACE ucrsempresas.nomebd WITH ALLTRIM(STREXTRACT(lcstringlinha, UPPER('BaseDados=')))
       CASE UPPER('ODBC=')$lcstringlinha
          SELECT ucrsempresas
          REPLACE ucrsempresas.odbc WITH ALLTRIM(STREXTRACT(lcstringlinha, UPPER('ODBC=')))
       CASE UPPER('Default=')$lcstringlinha
          SELECT ucrsempresas
          REPLACE ucrsempresas.default WITH ALLTRIM(STREXTRACT(lcstringlinha, UPPER('Default=')))
       CASE UPPER('LastUser=')$lcstringlinha
          SELECT ucrsempresas
          REPLACE ucrsempresas.lastuser WITH ALLTRIM(STREXTRACT(lcstringlinha, UPPER('LastUser=')))
       CASE UPPER('TitleBar=')$lcstringlinha
          SELECT ucrsempresas
          REPLACE ucrsempresas.titlebar WITH ALLTRIM(STREXTRACT(lcstringlinha, UPPER('TitleBar=')))
    ENDCASE
    UPDATE uCrsEmpresas SET marcado = .F.
 ENDFOR
 SELECT ucrsempresas
 DELETE FOR ALLTRIM(ucrsempresas.nomebd)=="" .OR. ALLTRIM(ucrsempresas.nome)==""
 IF RECCOUNT("uCrsEmpresas")==0
    lcval = .T.
 ENDIF
 SELECT ucrsempresas
 IF painelcentral.lcloginauto
    LOCATE FOR UPPER(ALLTRIM(ucrsempresas.nomebd))==UPPER(ALLTRIM(painelcentral.lcbd))
 ELSE
    SELECT ucrsempresas
    GOTO TOP
    LOCATE FOR ALLTRIM(ucrsempresas.default)<>"0"
    IF FOUND()
       SELECT ucrsempresas
    ELSE
       LOCATE FOR ucrsempresas.marcado==.T.
       IF  .NOT. FOUND()
          SELECT ucrsempresas
          GOTO TOP
       ENDIF
    ENDIF
 ENDIF
 IF TYPE("LOGIN")=="U"
    DO FORM login
 ELSE
    login.show
 ENDIF
 login.ctndados.tecladovirtual1.caps()
 IF lcval
    DO CASE
       CASE ALLTRIM(login.idioma)=="ptPT"
          MESSAGEBOX("N�o existe nenhuma empresa configurada!"+CHR(13)+"Por favor contacte o suporte.", 48, "Logitools Software")
       CASE ALLTRIM(login.idioma)=="enEN"
          MESSAGEBOX("Can't find any company!"+CHR(13)+"Please contact the support.", 48, "Logitools Software")
       OTHERWISE
          MESSAGEBOX("N�o existe nenhuma empresa configurada!"+CHR(13)+"Por favor contacte o suporte.", 48, "Logitools Software")
    ENDCASE
    login.ctndados.txtuser.enabled = .F.
    login.ctndados.txtpass.enabled = .F.
 ELSE
    IF painelcentral.lcloginauto
       login.ctndados.txtuser.value = painelcentral.lcuser
       login.ctndados.txtpass.value = painelcentral.lcpass
       uf_login_entrar()
    ELSE
       login.ctndados.txtuser.setfocus
    ENDIF
 ENDIF
ENDPROC
**
PROCEDURE uf_login_resize
 painelcentral.lockscreen = .T.
 login.width = painelcentral.width-36
 login.height = painelcentral.height-60
 login.left = 18
 login.top = 20
 LOCAL lctop
 STORE 0 TO lctop
 login.ctndados.left = (login.width-login.ctndados.width)/2
 lctop = (login.height-login.ctndados.height-45)/2
 IF lctop>100
    login.ctndados.top = lctop
 ELSE
    login.ctndados.top = 100
 ENDIF
 painelcentral.lockscreen = .F.
ENDPROC
**
PROCEDURE uf_login_movergrelha
 LPARAMETERS lctipo
 login.ctndados.ctnempresa.grdempresa.setfocus
 IF  .NOT. lctipo
    KEYBOARD '{PGDN}'
 ELSE
    KEYBOARD '{PGUP}'
 ENDIF
ENDPROC
**


FUNCTION uf_login_entrar
	
 IF EMPTY(ALLTRIM(login.ctndados.txtuser.value)) .OR. EMPTY(ALLTRIM(login.ctndados.txtpass.value)) .OR. EMPTY(ALLTRIM(login.ctndados.txtempresa.value))
    DO CASE
       CASE ALLTRIM(login.idioma)=="ptPT"
          MESSAGEBOX("N�o preencheu todos os dados necess�rios para a autentica��o!", 48, "Logitools Software")
       CASE ALLTRIM(login.idioma)=="enEN"
          MESSAGEBOX("Please fill all authentication fields!", 48, "Logitools Software")
       OTHERWISE
          MESSAGEBOX("N�o preencheu todos os dados necess�rios para a autentica��o!", 48, "Logitools Software")
    ENDCASE
    RETURN .F.
 ENDIF
 LOCAL lcconstr, lcsql
 STORE "" TO lcconstr, lcsql
 
 IF ALLTRIM(lcTipoEntrada) == 'EMPRESA'
 
	 SELECT ucrsempresas
	 LOCATE FOR ALLTRIM(ucrsempresas.nome)==ALLTRIM(login.ctndados.txtempresa.value)
	 IF FOUND()
	    IF EMPTY(ALLTRIM(ucrsempresas.cs))
	       lcconstr = "DSN="+ALLTRIM(ucrsempresas.odbc)+";UID=logiserv;PWD=ls2k12...;DATABASE="+ALLTRIM(ucrsempresas.nomebd)+";App=LT - "+myusername+" - "+myclientname
	    ELSE
	       lcconstr = ALLTRIM(ucrsempresas.cs)
	    ENDIF
	 ELSE
	    RETURN .F.
	 ENDIF
	 IF uf_login_connectdb(lcconstr)
	    PUBLIC sql_db
	    sql_db = ALLTRIM(ucrsempresas.nomebd)
	 ELSE
	    RETURN .F.
	 ENDIF

	 && Verifica se utiliza o certificado da BD Master
	 TEXT TO lcsql TEXTMERGE NOSHOW
		select bool from b_parameters where stamp='ADM0000000303'
	 ENDTEXT
	 SQLEXEC(gnconnhandle, lcsql, "uCrsCertif")
	 IF RECCOUNT("uCrsCertif")=0
		 TEXT TO lcsql TEXTMERGE NOSHOW
			exec up_login_autenticaUser '<<ALLTRIM(login.ctnDados.txtPass.value)>>','<<ALLTRIM(login.ctnDados.txtUser.value)>>',''
		 ENDTEXT
		 SQLEXEC(gnconnhandle, lcsql, "uCrsUser")
	 ELSE
	 	IF uCrsCertif.bool = .f.
			 TEXT TO lcsql TEXTMERGE NOSHOW
				exec up_login_autenticaUser '<<ALLTRIM(login.ctnDados.txtPass.value)>>','<<ALLTRIM(login.ctnDados.txtUser.value)>>',''
			 ENDTEXT
			 SQLEXEC(gnconnhandle, lcsql, "uCrsUser")
	 	ELSE
	 		LOCAL lcdecryptedpassw
			TEXT TO lcsql TEXTMERGE NOSHOW
				exec up_login_autenticaUser_local '<<ALLTRIM(login.ctnDados.txtPass.value)>>','<<ALLTRIM(login.ctnDados.txtUser.value)>>',''
			ENDTEXT
			SQLEXEC(gnconnhandle, lcsql, "uCrsUser")
			lcdecryptedpassw = uf_login_decrypt(uCrsUser.pwpos)
			IF ALLTRIM(lcdecryptedpassw) == ALLTRIM(login.ctnDados.txtPass.value)
				replace uCrsUser.pwpos WITH ALLTRIM(lcdecryptedpassw ) 
			ELSE
				SELECT uCrsUser
				DELETE ALL 
			ENDIF 
	 	ENDIF 
	 ENDIF 
 ELSE
 	 lcUtilPortal = ALLTRIM(login.ctndados.txtuser.value)
	 lcconstr = "DSN=LTSSQL01;UID=logiserv;PWD=ls2k12...;DATABASE=msb-auth;App=LT - josecosta - JOSECOSTA"
	 IF uf_login_connectdb(lcconstr)
	    PUBLIC sql_db
	    sql_db = 'msb-auth'
	 ELSE
	    RETURN .F.
	 ENDIF
	 TEXT TO lcsql TEXTMERGE NOSHOW
			exec up_login_autenticaUser_central '<<ALLTRIM(login.ctnDados.txtPass.value)>>','<<ALLTRIM(login.ctnDados.txtUser.value)>>'
	 ENDTEXT
	 SQLEXEC(gnconnhandle, lcsql, "uCrsUser")
 ENDIF 
 
 LOCAL lnReccount
 SELECT uCrsUser 

 COUNT FOR !DELETED() TO lnReccount
 **IF RECCOUNT("uCrsUser")==0
 IF lnReccount==0
    DO CASE
       CASE ALLTRIM(login.idioma)=="ptPT"
          MESSAGEBOX("Utilizador ou password errados.", 64, "Logitools Software")
       CASE ALLTRIM(login.idioma)=="enEN"
          MESSAGEBOX("User or password invalid.", 64, "Logitools Software")
       OTHERWISE
          MESSAGEBOX("Utilizador ou password errados.", 64, "Logitools Software")
    ENDCASE
    login.ctndados.txtpass.value = ''
    login.ctndados.txtpass.setfocus()
    RETURN .F.
 ENDIF
 
 IF ALLTRIM(lcTipoEntrada) == 'PORTAL'
 	 lcconstr = "DSN=LTSSQL01;UID=logiserv;PWD=ls2k12...;DATABASE=ltsportal;App=LT - ltsportal - LTSPORTAL"
	 IF uf_login_connectdb(lcconstr)
	    PUBLIC sql_db
	    sql_db = 'ltsportal'
	 ELSE
	    RETURN .F.
	 ENDIF
	 login.ctndados.txtempresa.value='ltsportal'
 ENDIF 
 
 SELECT ucrsempresas
 IF ALLTRIM(ucrsempresas.titlebar)=="0"
    _SCREEN.titlebar = 0
    _SCREEN.windowstate = 2
 ELSE
    _SCREEN.titlebar = 1
    _SCREEN.windowstate = 2
 ENDIF
 
 **IF ALLTRIM(lcTipoEntrada) != 'PORTAL'
	 IF FILE(lcficheiroinipath)
	    STRTOFILE(" ", lcficheiroinipath, .F.)
	    SELECT * FROM uCrsEmpresas INTO CURSOR uCrsEmpresasInsert READWRITE
	    SELECT ucrsempresasinsert
	    GOTO TOP
	    SCAN
	       IF ALLTRIM(login.ctndados.txtempresa.value)<>ALLTRIM(ucrsempresasinsert.nome) .OR. LEN(ALLTRIM(login.ctndados.txtempresa.value))<>LEN(ALLTRIM(ucrsempresasinsert.nome))
	          REPLACE ucrsempresasinsert.default WITH "0"
	       ELSE
	          REPLACE ucrsempresasinsert.default WITH "1"
	       ENDIF
	    ENDSCAN
	    SELECT ucrsempresasinsert
	    GOTO TOP
	    SCAN
	       STRTOFILE(CHR(13)+CHR(10)+CHR(13)+CHR(10), lcficheiroinipath, .T.)
	       lcempresa = "Empresa="+ucrsempresasinsert.nome
	       STRTOFILE(CHR(13)+CHR(10)+lcempresa, lcficheiroinipath, .T.)
	       lcbasedados = "BaseDados="+ALLTRIM(ucrsempresasinsert.nomebd)
	       STRTOFILE(CHR(13)+CHR(10)+lcbasedados, lcficheiroinipath, .T.)
	       lcodbc = "ODBC="+ALLTRIM(ucrsempresasinsert.odbc)
	       STRTOFILE(CHR(13)+CHR(10)+lcodbc, lcficheiroinipath, .T.)
	       lcdefault = "Default="+ALLTRIM(ucrsempresasinsert.default)
	       STRTOFILE(CHR(13)+CHR(10)+lcdefault, lcficheiroinipath, .T.)
	       lclastuser = "LastUser="+ALLTRIM(ucrsempresasinsert.lastuser)
	       STRTOFILE(CHR(13)+CHR(10)+lclastuser, lcficheiroinipath, .T.)
	       lctitlebar = "TitleBar="+ALLTRIM(ucrsempresasinsert.titlebar)
	       STRTOFILE(CHR(13)+CHR(10)+lctitlebar, lcficheiroinipath, .T.)
	    ENDSCAN
	 ENDIF
 **ELSE
 
 **ENDIF 

 SQLEXEC(gnconnhandle, "select textValue from b_parameters where stamp='ADM0000000015'", "uCrsPath")
 IF RECCOUNT("uCrsPath")>0

    mypath = ALLTRIM(ucrspath.textvalue)
    SELECT ucrspath
    USE

    DO (mypath+"\startup\startup.app")

    uf_startup_chama()

 ELSE
    MESSAGEBOX("N�O FOI POSSIVEL OBTER O CAMINHO DE RECURSOS PARTILHADOS!", 16, "LOGITOOLS SOFTWARE")
    QUIT
 ENDIF
ENDFUNC

**
PROCEDURE uf_login_sair
 login.release
 RELEASE login
ENDPROC
**
FUNCTION uf_login_connectdb
 LPARAMETERS lcconnection_string
 PUBLIC gnconnhandle
 gnconnhandle = SQLSTRINGCONNECT(lcconnection_string)
 IF gnconnhandle>0
    RETURN .T.
 ELSE
    MESSAGEBOX("Connection Failed", 16, "ODBC Problem")
    RETURN .F.
 ENDIF
ENDFUNC
**

FUNCTION uf_login_encrypt
	LPARAMETERS lcString
	LOCAL loCrypt, lcTxt1, loSbEncrypted, lcDecryptedText, lcRetString

	loCrypt = CreateObject('Chilkat_9_5_0.Crypt2')

	lnSuccess = loCrypt .UnlockComponent("LGTLSP.CB1052019_eQLZAyAR9RkB")
		IF (lnSuccess <> 1) THEN
		  	? loMailman.LastErrorText
		    RELEASE loMailman
		   CANCEL
		ENDIF

	loCrypt.CryptAlgorithm = "aes"
	loCrypt.CipherMode = "cbc"
	loCrypt.KeyLength = 128

	loCrypt.SetEncodedKey("L0g1T00lsFarm@L0g1Lts2k16..LTS","hex")
	loCrypt.SetEncodedIV("L0g1T00lsFarm@L0g1Lts2k16..LTS","hex")

	loCrypt.EncodingMode = "hex"
	lcTxt1 = ALLTRIM(lcString)
	**lcTxt2 = "-" + CHR(13) + CHR(10)
	**lcTxt3 = "Done." + CHR(13) + CHR(10)


	loSbEncrypted = CreateObject('Chilkat_9_5_0.StringBuilder')


	loSbEncrypted.Append(loCrypt.EncryptStringENC(lcTxt1))

	lcRetString=loSbEncrypted.GetAsString()

	RELEASE loCrypt
	RELEASE loSbEncrypted
	
	RETURN lcRetString
ENDFUNC 

FUNCTION uf_login_decrypt
	LPARAMETERS lcString
	LOCAL loCrypt, lcTxt1, loSbEncrypted, lcDecryptedText, lcRetString

	loCrypt = CreateObject('Chilkat_9_5_0.Crypt2')

	lnSuccess = loCrypt .UnlockComponent("LGTLSP.CB1052019_eQLZAyAR9RkB")
		IF (lnSuccess <> 1) THEN
		  	? loMailman.LastErrorText
		    RELEASE loMailman
		   CANCEL
		ENDIF

	loCrypt.CryptAlgorithm = "aes"
	loCrypt.CipherMode = "cbc"
	loCrypt.KeyLength = 128

	loCrypt.SetEncodedKey("L0g1T00lsFarm@L0g1Lts2k16..LTS","hex")
	loCrypt.SetEncodedIV("L0g1T00lsFarm@L0g1Lts2k16..LTS","hex")

	loCrypt.EncodingMode = "hex"
	lcTxt1 = ALLTRIM(lcString)

	lcDecryptedText = loCrypt.DecryptStringENC(lcTxt1)

	lcRetString = lcDecryptedText

	RELEASE loCrypt
	RELEASE loSbEncrypted
	
	RETURN lcRetString
ENDFUNC 