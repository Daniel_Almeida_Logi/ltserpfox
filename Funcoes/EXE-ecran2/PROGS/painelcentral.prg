 DECLARE LONG FindWindow IN Win32API STRING, STRING
 
**
PROCEDURE uf_painelcentral_chama
 LPARAMETERS lcuser, lcpass, lccomando, lcloginauto
 IF TYPE("painelcentral")=="U"
    DO FORM painelcentral
    _SCREEN.visible = .T.
    painelcentral.lcuser = ALLTRIM(lcuser)
    painelcentral.lcpass = ALLTRIM(lcpass)
    painelcentral.lccomando = ALLTRIM(lccomando)
    painelcentral.lcloginauto = lcloginauto
    uf_login_chama()
 ELSE
 ENDIF
ENDPROC
**
PROCEDURE uf_painelcentral_quit
 painelcentral.lcloginauto = .F.
 FOR EACH mf IN painelcentral.objects
    IF LIKE("*PAGINA*", UPPER(ALLTRIM(mf.name)))==.T.
       painelcentral.removeobject(mf.name)
    ENDIF
 ENDFOR
 painelcentral.menu1.visible = .F.
 LOCAL lcpainel
 CREATE CURSOR uCrsPaineis (nome C (100))
 FOR i = 1 TO _SCREEN.formcount
    SELECT ucrspaineis
    APPEND BLANK
    REPLACE ucrspaineis.nome WITH _SCREEN.forms(i).name
 ENDFOR
 SELECT DISTINCT nome FROM uCrsPaineis INTO CURSOR uCrsPaineisFechar READWRITE
 SELECT ucrspaineisfechar
 SCAN
    IF  .NOT. (UPPER(ALLTRIM(ucrspaineisfechar.nome))=="PAINELCENTRAL")
       lcpainel = ucrspaineisfechar.nome
       TRY
          &lcpainel..RELEASE()
       CATCH
       ENDTRY
    ENDIF
 ENDSCAN
 painelcentral.timerintegracaophc.enabled = .F.
 TRY
    SQLDISCONNECT(gnconnhandle)
 CATCH
 ENDTRY
 uf_login_chama()
ENDPROC
**
