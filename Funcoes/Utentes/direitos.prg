FUNCTION uf_direitos_chama
	LPARAMETERS lcStamp, lcSource, lcTipo
	
	PUBLIC cval
	STORE '' TO cval
						
	uf_tecladoalpha_chama("cval", "INTRODUZA PASSWORD PARA ACEDER �S OP��ES DO RGPD", .t., .f., 0)
						
	IF EMPTY(cval)
		uf_perguntalt_chama("DEVE INTRODUZIR A PASSWORD.","OK","",64)
		RELEASE cval
		RETURN .f.
	ELSE
		IF !((ALLTRIM(cval))==(ALLTRIM(uf_gerais_getParameter('ADM0000000297', 'TEXT'))))
			uf_perguntalt_chama("A PASSWORD N�O EST� CORRECTA.","OK","",48)	
			RELEASE cval
				RETURN .F.
		ELSE
			RELEASE cval
		ENDIF 
	ENDIF
		
	** cria cursor vazio
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		select no, estab, nome, email, autoriza_emails, tlmvl, autoriza_sms from b_utentes WHERE utstamp='<<ALLTRIM(lcStamp)>>'
	ENDTEXT 
	IF !uf_gerais_actgrelha("", "uCrsdireitos", lcSql)
		uf_perguntalt_chama("ERRO AO LER A TABELA DE Utentes!","OK","",48)
		RETURN .F.
	ENDIF 
	
	&& Controla Abertura do Painel			
	if type("direitos")=="U"
		DO FORM direitos
	ELSE
		direitos.show
	ENDIF
	DO CASE 
		CASE lcTipo='esquecimento'
			direitos.txtTipoDireito.caption='EXERCER DIREITO AO ESQUECIMENTO'
			direitos.Label16.visible=.f.
			direitos.cmbTipo.visible=.f.
		CASE lcTipo='oposicao'
			direitos.txtTipoDireito.caption='EXERCER DIREITO � OPOSI��O'
			direitos.Label16.visible=.t.
			direitos.cmbTipo.visible=.t.
		CASE lcTipo='portabilidade'
			direitos.txtTipoDireito.caption='EXERCER DIREITO � PORTABILIDADE'		
			direitos.Label16.visible=.f.
			direitos.cmbTipo.visible=.f.
		OTHERWISE
		
	ENDCASE 
	
ENDFUNC


**
FUNCTION uf_direitos_carregamenu
	**direitos.menu1.adicionaopcao("atualizar", "Atualizar", myPath + "\imagens\icons\actualizar_w.png", "uf_LOGCLIENTE_Pesquisa", "F2")
ENDFUNC


**
FUNCTION uf_direitos_sair
	**Fecha Cursores
*!*		IF USED("uCrsLogCl")
*!*			fecha("uCrsLogCl")
*!*		ENDIF 
	
	direitos.hide
	direitos.release
ENDFUNC

FUNCTION uf_direitos_envia_SMS
	LPARAMETERS lcSource, lcDestino, lcNo, lcEstab
	LOCAL lcTexto, lcWsPath, lcCodEnvio , lcToken
	STORE '' TO lcWsPath , lcTexto
	**STORE LEFT(m_chinis+dtoc(date())+SYS(2015),25) TO lcCodEnvio 

	lcToken = RIGHT('0000'+STR(INT((999999 - 1 + 1) * Rand(-1) + 1)),6)
	STORE LEFT(ALLTRIM(m_chinis)+dtoc(date())+SYS(2015),25) TO lcCodEnvio
	
	** VAI BUSCAR A MENSAGEM PREDEFINIDA PARA O ENVIO DO SMS
	DO CASE
		CASE direitos.txtTipoDireito.caption='EXERCER DIREITO AO ESQUECIMENTO'
			TEXT TO lcSQL TEXTMERGE NOSHOW
				select top 1 email_body as MENSAGEM from b_sms where codminuta='TOKEN-ESQUECIMENTO-SMS' AND site = '<<ALLTRIM(mySite)>>' and convert(varchar(1), email_body) is not null order by ousrdata desc
			ENDTEXT 
		CASE direitos.txtTipoDireito.caption='EXERCER DIREITO � OPOSI��O'
			TEXT TO lcSQL TEXTMERGE NOSHOW
				select top 1 email_body as MENSAGEM from b_sms where codminuta='TOKEN-OPOSI��O-SMS' AND site = '<<ALLTRIM(mySite)>>' and convert(varchar(1), email_body) is not null order by ousrdata desc
			ENDTEXT 
		CASE direitos.txtTipoDireito.caption='EXERCER DIREITO � PORTABILIDADE'	
			TEXT TO lcSQL TEXTMERGE NOSHOW
				select top 1 email_body as MENSAGEM from b_sms where codminuta='TOKEN-PORTABILIDADE-SMS' AND site = '<<ALLTRIM(mySite)>>' and convert(varchar(1), email_body) is not null order by ousrdata desc
			ENDTEXT 
		OTHERWISE
	ENDCASE 
	IF !uf_gerais_actgrelha("", "ucrsMsg", lcSql)
		uf_perguntalt_chama("ERRO AO CONSULTAR A MENSAGEM DE ENVIO!","OK","",48)
		RETURN .F.
	ENDIF 
	IF RECCOUNT("ucrsMsg")=0
		uf_perguntalt_chama("N�O FOI CONFIGURADA NENHUMA MENSAGEM PARA O ENVIO DE TOKEN."+ CHR(13) +" POR FAVOR CONFIGURE A MESMA NO PAINEL DE COMUNICA��ES!","OK","",16)
		RETURN .F.
	ELSE
		lcTexto = ALLTRIM(ucrsMsg.MENSAGEM) + ' - ' + ALLTRIM(lcToken) 
	ENDIF
	
	TEXT TO lcSQL TEXTMERGE NOSHOW
		SELECT utstamp FROM b_utentes where no=<<lcNo>> and estab=<<lcEstab>>
	ENDTEXT 
	IF !uf_gerais_actgrelha("", "ucrsUtStamp", lcSql)
		uf_perguntalt_chama("ERRO AO CONSULTAR A TABELA DE UTENTES!","OK","",48)
		RETURN .F.
	ENDIF 
	lcUtStamp = ALLTRIM(ucrsUtStamp.utstamp)
	
	DO CASE
		CASE direitos.txtTipoDireito.caption='EXERCER DIREITO AO ESQUECIMENTO'
			uf_send_sms(lcCodEnvio, 'RGPD', ALLTRIM(lcSource), ALLTRIM(lcTexto), ALLTRIM(lcDestino), 'CL', lcUtStamp, 'Envio de token de direito ao esquecimento por SMS para o nr ' + ALLTRIM(lcDestino) + CHR(13) + ALLTRIM(lcTexto))
		CASE direitos.txtTipoDireito.caption='EXERCER DIREITO � OPOSI��O'
			uf_send_sms(lcCodEnvio, 'RGPD', ALLTRIM(lcSource), ALLTRIM(lcTexto), ALLTRIM(lcDestino), 'CL', lcUtStamp, 'Envio de token de direito de oposi��o por SMS para o nr ' + ALLTRIM(lcDestino) + CHR(13) + ALLTRIM(lcTexto))
		CASE direitos.txtTipoDireito.caption='EXERCER DIREITO � PORTABILIDADE'	
			uf_send_sms(lcCodEnvio, 'RGPD', ALLTRIM(lcSource), ALLTRIM(lcTexto), ALLTRIM(lcDestino), 'CL', lcUtStamp, 'Envio de token de direito � portabilidade por SMS para o nr ' + ALLTRIM(lcDestino) + CHR(13) + ALLTRIM(lcTexto))
		OTHERWISE
	ENDCASE 
	
	IF direitos.txtTipoDireito.caption='EXERCER DIREITO � OPOSI��O'
		DO CASE 
			CASE ALLTRIM(direitos.cmbtipo.value) = 'SMS'
				lcToken = lcToken+'1'
			CASE ALLTRIM(direitos.cmbtipo.value) = 'EMAIL'
				lcToken = lcToken+'2'
			OTHERWISE 
				lcToken = lcToken+'3'
		ENDCASE 
	ENDIF 

	TEXT TO lcSQL TEXTMERGE NOSHOW
		UPDATE b_utentes SET tokenaprovacao=<<ALLTRIM(lcToken)>> where no=<<lcNo>> and estab=<<lcEstab>>
	ENDTEXT 
	IF !uf_gerais_actgrelha("", "", lcSql)
		uf_perguntalt_chama("ERRO AO ATUALIZAR A TABELA DE UTENTES!","OK","",48)
		RETURN .F.
	ENDIF 
	
ENDFUNC

FUNCTION uf_direitos_envia_EMAIL
	LPARAMETERS lcDestino, lcNo, lcEstab
	
	LOCAL lcTo, lcToEmail, lcSubject, lcBody, lcAtatchment, lcToken, lcTexto 
	STORE '' TO lcTexto 

	lcToken = RIGHT('0000'+STR(INT((9999999 - 1 + 1) * Rand(-1) + 1)),6)
	
	DO CASE
		CASE direitos.txtTipoDireito.caption='EXERCER DIREITO AO ESQUECIMENTO'
			TEXT TO lcSQL TEXTMERGE NOSHOW
				select top 1 email_body as MENSAGEM from b_sms where codminuta='TOKEN-ESQUECIMENTO-EMAIL' AND site = '<<ALLTRIM(mySite)>>' and convert(varchar(1), email_body) is not null order by ousrdata desc
			ENDTEXT 
		CASE direitos.txtTipoDireito.caption='EXERCER DIREITO � OPOSI��O'
			TEXT TO lcSQL TEXTMERGE NOSHOW
				select top 1 email_body as MENSAGEM from b_sms where codminuta='TOKEN-OPOSI��O-EMAIL' AND site = '<<ALLTRIM(mySite)>>' and convert(varchar(1), email_body) is not null order by ousrdata desc
			ENDTEXT 
		CASE direitos.txtTipoDireito.caption='EXERCER DIREITO � PORTABILIDADE'	
			TEXT TO lcSQL TEXTMERGE NOSHOW
				select top 1 email_body as MENSAGEM from b_sms where codminuta='TOKEN-PORTABILIDADE-EMAIL' AND site = '<<ALLTRIM(mySite)>>' and convert(varchar(1), email_body) is not null order by ousrdata desc
			ENDTEXT 
		OTHERWISE
	ENDCASE 
	IF !uf_gerais_actgrelha("", "ucrsMsg", lcSql)
		uf_perguntalt_chama("ERRO AO CONSULTAR A MENSAGEM DE ENVIO!","OK","",48)
		RETURN .F.
	ENDIF 
	IF RECCOUNT("ucrsMsg")=0
		uf_perguntalt_chama("N�O FOI CONFIGURADA NENHUMA MENSAGEM PARA O ENVIO DE TOKEN."+ CHR(13) +" POR FAVOR CONFIGURE A MESMA NO PAINEL DE COMUNICA��ES!","OK","",16)
		RETURN .F.
	ELSE
		lcTexto = ALLTRIM(ucrsMsg.mensagem) + ' - ' + ALLTRIM(lcToken) 
	ENDIF
	
	lcTo = ""
	lcToEmail = ALLTRIM(lcDestino)
	lcSubject = 'Envio de c�digo de aprova��o'
	lcBody = lcTexto + CHR(13) + ALLTRIM(ucrse1.mailsign)
	lcAtatchment = ""
	regua(0,6,"A enviar email ...")
	uf_startup_sendmail_emp(lcTo, lcToEmail, lcSubject, lcBody , lcAtatchment, .t.)
	regua(2)
	
	IF direitos.txtTipoDireito.caption='EXERCER DIREITO � OPOSI��O'
		DO CASE 
			CASE ALLTRIM(direitos.cmbtipo.value) = 'SMS'
				lcToken = lcToken+'1'
			CASE ALLTRIM(direitos.cmbtipo.value) = 'EMAIL'
				lcToken = lcToken+'2'
			OTHERWISE 
				lcToken = lcToken+'3'
		ENDCASE 
	ENDIF 
	
	TEXT TO lcSQL TEXTMERGE NOSHOW
		UPDATE b_utentes SET tokenaprovacao=<<lcToken>> where no=<<lcNo>> and estab=<<lcEstab>>
	ENDTEXT 
	IF !uf_gerais_actgrelha("", "", lcSql)
		uf_perguntalt_chama("ERRO AO ATUALIZAR A TABELA DE UTENTES!","OK","",48)
		RETURN .F.
	ENDIF 
	
	TEXT TO lcSQL TEXTMERGE NOSHOW
		SELECT utstamp FROM b_utentes where no=<<lcNo>> and estab=<<lcEstab>>
	ENDTEXT 
	IF !uf_gerais_actgrelha("", "ucrsUtStamp", lcSql)
		uf_perguntalt_chama("ERRO AO CONSULTAR A TABELA DE UTENTES!","OK","",48)
		RETURN .F.
	ENDIF 
	
	DO CASE
		CASE direitos.txtTipoDireito.caption='EXERCER DIREITO AO ESQUECIMENTO'
			uf_user_log_ins('CL', ALLTRIM(ucrsUtStamp.utstamp), 'EMAIL', 'Envio de token de aprova��o por Email - direito ao esquecimento - para o email' + ALLTRIM(lcToEmail))
		CASE direitos.txtTipoDireito.caption='EXERCER DIREITO � OPOSI��O'
			uf_user_log_ins('CL', ALLTRIM(ucrsUtStamp.utstamp), 'EMAIL', 'Envio de token de aprova��o por Email - direito de oposi��o - para o email' + ALLTRIM(lcToEmail))
		CASE direitos.txtTipoDireito.caption='EXERCER DIREITO � PORTABILIDADE'	
			uf_user_log_ins('CL', ALLTRIM(ucrsUtStamp.utstamp), 'EMAIL', 'Envio de token de aprova��o por Email - direito � portabilidade - para o email' + ALLTRIM(lcToEmail))
		OTHERWISE
	ENDCASE 
	
ENDFUNC 

FUNCTION uf_direitos_confirma
	LPARAMETERS lcToken, lcNo, lcEstab

		LOCAL lcTipo

		TEXT TO lcSQL TEXTMERGE NOSHOW
			select tokenaprovacao from b_utentes where no=<<lcNo>> and estab=<<lcEstab>>
		ENDTEXT 
		IF !uf_gerais_actgrelha("", "ucrsToken", lcSql)
			uf_perguntalt_chama("ERRO AO CONSULTAR O TOKEN DO UTENTES!","OK","",48)
			RETURN .F.
		ENDIF 
		IF ucrsToken.tokenaprovacao = 0 then
			uf_perguntalt_chama("AINDA N�O FOI EMITIDO UM C�DIGO DE CONFIRMA��O PARA ESTE UTENTE!","OK","",16)
			RETURN .F.
		ELSE
			DO CASE
				CASE direitos.txtTipoDireito.caption='EXERCER DIREITO AO ESQUECIMENTO'
	
					IF uf_perguntalt_chama("Confirma o pedido de esquecimento do utente?" + CHR(13) + "ESTE PEDIDO � IRREVERS�VEL!","Sim","N�o")
					
						IF ucrsToken.tokenaprovacao = lcToken then
					
							TEXT TO lcSQL TEXTMERGE NOSHOW
								UPDATE b_utentes SET removido=1, data_removido=dateadd(HOUR, <<difhoraria>>, getdate()), inactivo=1 WHERE utstamp='<<ALLTRIM(cl.Utstamp)>>'
							ENDTEXT
							If !uf_gerais_actGrelha("", "", lcSql)
								uf_perguntalt_chama("N�o foi possivel exercer a op��o de esquecimento. Por favor contacte o suporte.", "", "OK", 32)
								RETURN .f.
							ENDIF
							uf_user_log_ins('CL', ALLTRIM(cl.Utstamp), 'ESQUECIMENTO', 'Exercida a op��o de esquecimento')	
							IF !EMPTY(cl.email) AND ucrse1.usacoms=.t. 
								LOCAL lcTo, lcToEmail, lcSubject, lcBody
								lcTo = ""
								lcToEmail = ALLTRIM(cl.email)
								lcSubject = 'Exercimento da Op��o de Esquecimento'
								lcBody = "Exmo(a). Sr(a). " + CHR(13) + CHR(13) + "Vimos por este meio confirmar o seu pedido de esquecimento dos dados feito na nossa farm�cia.";
										 + CHR(13) + 'A sua informa��o pessoal foi removida do nosso sistema com sucesso.' + CHR(13);
										 + CHR(13) + ALLTRIM(ucrse1.mailsign)
								lcAtatchment = ""
								regua(0,6,"A enviar email para o utente com a confirma��o...")
								uf_startup_sendmail_emp(lcTo, lcToEmail, lcSubject, lcBody , lcAtatchment, .t.)
								regua(2)
							ENDIF 
							
							uf_perguntalt_chama("Dados do utente removidos do sistema.","OK","",48)	

							&& Actualiza Ecr�
							IF TYPE("UTENTES")!= "U" 
								TEXT TO lcSql NOSHOW textmerge
									SELECT Top 1 
										b_utentes.* 
										,pontos = isnull(b_fidel.pontosAtrib,0) - isnull(b_fidel.pontosUsa,0)
										,binoValidade = convert(datetime,isnull(cc.validade_fim, '19000101'))
										,compfixa=0
                                        ,valcartao = 0
                                        ,percadic = 0
										,templano = convert(bit,0)
									FROM 
										b_utentes (nolock) 
										left join b_fidel (nolock)  on b_utentes.no = b_fidel.clno and b_utentes.estab = b_fidel.clestab
										left join dispensa_eletronica_cc cc (nolock) on b_utentes.utstamp = cc.utstamp
									where 
										b_utentes.no = <<lcNo>> 
										and b_utentes.estab = <<lcEstab>>
								ENDTEXT
								IF !uf_gerais_actgrelha("", "cl", lcSql)
									RETURN .f.
								ENDIF
								SELECT cl
								utentes.refresh
							ENDIF 
							uf_direitos_sair()
							uf_PESQUTENTES_chama("UTENTES")
							
						ELSE
							uf_perguntalt_chama("C�DIGO INV�LIDO" + CHR(13) + "N�O CORRESPONDE AO ENVIADO PARA O UTENTE!","OK","",48)
						ENDIF 
					ELSE
						uf_perguntalt_chama("PEDIDO CANCELADO!","OK","",48)
					ENDIF 

				CASE direitos.txtTipoDireito.caption='EXERCER DIREITO � OPOSI��O'
				
					IF 	EMPTY(direitos.cmbtipo.value)
						uf_perguntalt_chama("TEM DE ESCOLHER O TIPO DE OPOSI��O!","OK","",48)
						RETURN .F.
					ELSE

						lctipo = RIGHT(ALLTRIM(STR(ucrsToken.tokenaprovacao)),1)
						replace ucrsToken.tokenaprovacao with (ucrsToken.tokenaprovacao-VAL(lctipo))/10
						
						DO CASE 
							CASE ALLTRIM(lctipo) == '1'
								TEXT TO lcSQL TEXTMERGE NOSHOW
									UPDATE b_utentes SET autoriza_sms = 0 where no=<<lcNo>> and estab=<<lcEstab>>
								ENDTEXT 
							CASE ALLTRIM(lctipo) == '2'
								TEXT TO lcSQL TEXTMERGE NOSHOW
										UPDATE b_utentes SET autoriza_emails = 0 where no=<<lcNo>> and estab=<<lcEstab>>
									ENDTEXT 
							OTHERWISE 
								TEXT TO lcSQL TEXTMERGE NOSHOW
									UPDATE b_utentes SET autoriza_sms = 0, autoriza_emails = 0 where no=<<lcNo>> and estab=<<lcEstab>>
								ENDTEXT 
						ENDCASE 
						
						IF ucrsToken.tokenaprovacao = lcToken then
							
							IF !uf_gerais_actgrelha("", "", lcSql)
								uf_perguntalt_chama("ERRO AO ATUALIZAR A TABELA DE UTENTES!","OK","",48)
								RETURN .F.
							ENDIF 
						
							uf_perguntalt_chama("DIREITO � OPOSI��O CONFIRMADO COM SUCESSO!","OK","",64)
								
							TEXT TO lcSQL TEXTMERGE NOSHOW
								SELECT utstamp FROM b_utentes where no=<<lcNo>> and estab=<<lcEstab>>
							ENDTEXT 
							IF !uf_gerais_actgrelha("", "ucrsUtStamp", lcSql)
								uf_perguntalt_chama("ERRO AO CONSULTAR A TABELA DE UTENTES!","OK","",48)
								RETURN .F.
							ENDIF 
							uf_user_log_ins('CL', ALLTRIM(ucrsUtStamp.utstamp), 'CONFIRMA��O', 'Confirma��o do token de aprova��o - Direito de oposi��o')
							
							&& Actualiza Ecr�
							IF TYPE("UTENTES")!= "U" 
								TEXT TO lcSql NOSHOW textmerge
									SELECT Top 1 
										b_utentes.* 
										,pontos = isnull(b_fidel.pontosAtrib,0) - isnull(b_fidel.pontosUsa,0)
										,binoValidade = convert(datetime,isnull(cc.validade_fim, '19000101'))
										,compfixa=0
                                        ,valcartao = 0
                                        ,percadic = 0
										,templano = convert(bit,0)
									FROM 
										b_utentes (nolock) 
										left join b_fidel (nolock)  on b_utentes.no = b_fidel.clno and b_utentes.estab = b_fidel.clestab
										left join dispensa_eletronica_cc cc (nolock) on b_utentes.utstamp = cc.utstamp
									where 
										b_utentes.no = <<lcNo>> 
										and b_utentes.estab = <<lcEstab>>
								ENDTEXT
								IF !uf_gerais_actgrelha("", "cl", lcSql)
									RETURN .f.
								ENDIF
								SELECT cl
								utentes.refresh
							ENDIF 
							uf_direitos_sair()

						ELSE
							uf_perguntalt_chama("C�DIGO INV�LIDO" + CHR(13) + "N�O CORRESPONDE AO ENVIADO PARA O UTENTE!","OK","",48)
						ENDIF  
					ENDIF 
				CASE direitos.txtTipoDireito.caption='EXERCER DIREITO � PORTABILIDADE'	

					IF ucrsToken.tokenaprovacao = lcToken then
					
						LOCAL mDir, lcTo, lcToEmail, lcSubject, lcBody, lcAtatchment, lcNomeFicheiroXLS
						
						IF !EMPTY(cl.email)
							IF uf_perguntalt_chama("CONFIRMA O ENVIO DO FICHEIRO PARA O UTENTE?","Sim","N�o")
								mdir = ALLTRIM(uf_gerais_getParameter('ADM0000000015','TEXT')) + "\Cache\XLS"
								lcNomeFicheiroXLS= 'Utente'+ALLTRIM(STR(cl.no)) + ".xls"
								lcTo = ""
								lcToEmail = ALLTRIM(cl.email)
								lcSubject = 'Envio dos dados pessoais'
								lcBody = "Exmo(a). Sr(a). " + CHR(13) + CHR(13) + "No seguimento do seu pedido, vimos por este meio enviar-lhe o documento em excel com os seus dados pessoais." + CHR(13) + ALLTRIM(ucrse1.mailsign)
								lcAtatchment = mdir + "\"+ALLTRIM(lcNomeFicheiroXLS)

								TEXT TO lcSQL TEXTMERGE NOSHOW
									up_utentes_exporta '<<ALLTRIM(cl.Utstamp)>>'
								ENDTEXT 
								IF !uf_gerais_actgrelha("", "ucrsDadosUt", lcSql)
									uf_perguntalt_chama("ERRO AO CONSULTAR A TABELA DE UTENTES!","OK","",48)
									RETURN .F.
								ENDIF 

								SELECT ucrsDadosUt
								export to ALLTRIM(lcAtatchment) type xl5
								SELECT cl
								
								regua(0,6,"A enviar email ...")
								uf_startup_sendmail_emp(lcTo, lcToEmail, lcSubject, lcBody , lcAtatchment, .t.)
								regua(2)

								uf_user_log_ins('CL', ALLTRIM(cl.Utstamp), 'EMAIL', 'Envio de dados pessoais por email para ' + ALLTRIM(cl.email) + ' - ' + ALLTRIM(lcNomeFicheiroXLS))
							ELSE
								uf_perguntalt_chama("ENVIO CANCELADO!","OK","",48)
							ENDIF 

						ELSE
							uf_perguntalt_chama("N�o existe um email para envio preenchido." + CHR(13) + "Atualize a ficha respetiva.","OK","",48)
							RETURN .f.
						ENDIF 
					
					ELSE
						uf_perguntalt_chama("C�DIGO INV�LIDO" + CHR(13) + "N�O CORRESPONDE AO ENVIADO PARA O UTENTE!","OK","",48)
					ENDIF  

				OTHERWISE
			ENDCASE 
		ENDIF 
	
ENDFUNC 

FUNCTION uf_oposicao_confirma_doc

	LPARAMETERS lcNo, lcEstab

	DO CASE
		CASE direitos.txtTipoDireito.caption='EXERCER DIREITO AO ESQUECIMENTO'
		
			IF uf_perguntalt_chama("Confirma o pedido de esquecimento do utente?" + CHR(13) + "ESTE PEDIDO � IRREVERS�VEL!","Sim","N�o")
		
				TEXT TO lcSQL TEXTMERGE NOSHOW
					UPDATE b_utentes SET removido=1, data_removido=dateadd(HOUR, <<difhoraria>>, getdate()), inactivo=1 WHERE utstamp='<<ALLTRIM(cl.Utstamp)>>'
				ENDTEXT
				If !uf_gerais_actGrelha("", "", lcSql)
					uf_perguntalt_chama("N�o foi possivel exercer a op��o de esquecimento. Por favor contacte o suporte.", "", "OK", 48)
					RETURN .f.
				ENDIF
				
				uf_perguntalt_chama("DIREITO AO ESQUECIMENTO CONFIRMADO COM SUCESSO!" + CHR(13) + "N�O SE ESQUE�A DE ARQUIVAR O DOCUMENTO ASSINADO PELO CLIENTE.","OK","",48)
						
				TEXT TO lcSQL TEXTMERGE NOSHOW
					SELECT utstamp FROM b_utentes where no=<<lcNo>> and estab=<<lcEstab>>
				ENDTEXT 
				IF !uf_gerais_actgrelha("", "ucrsUtStamp", lcSql)
					uf_perguntalt_chama("ERRO AO CONSULTAR A TABELA DE UTENTES!","OK","",48)
					RETURN .F.
				ENDIF 
				uf_user_log_ins('CL', ALLTRIM(ucrsUtStamp.utstamp), 'CONFIRMA��O', 'Confirma��o de oposi��o via documento assinado - Direito ao esquecimento')
					
				&& Actualiza Ecr�
				IF TYPE("UTENTES")!= "U" 
					TEXT TO lcSql NOSHOW textmerge
						SELECT Top 1 
							b_utentes.* 
							,pontos = isnull(b_fidel.pontosAtrib,0) - isnull(b_fidel.pontosUsa,0)
							,binoValidade = convert(datetime,isnull(cc.validade_fim, '19000101'))
							,compfixa=0
                            ,valcartao = 0
                            ,percadic = 0
							,templano = convert(bit,0)
						FROM 
							b_utentes (nolock) 
							left join b_fidel (nolock)  on b_utentes.no = b_fidel.clno and b_utentes.estab = b_fidel.clestab
							left join dispensa_eletronica_cc cc (nolock) on b_utentes.utstamp = cc.utstamp
						where 
							b_utentes.no = <<lcNo>> 
							and b_utentes.estab = <<lcEstab>>
					ENDTEXT
					IF !uf_gerais_actgrelha("", "cl", lcSql)
						RETURN .f.
					ENDIF
					SELECT cl
					utentes.refresh
				ENDIF 
					
				uf_direitos_sair()
				uf_PESQUTENTES_chama("UTENTES")
			ELSE
				uf_perguntalt_chama("PEDIDO CANCELADO!","OK","",48)
			ENDIF 
			
		CASE direitos.txtTipoDireito.caption='EXERCER DIREITO � OPOSI��O'

			IF 	EMPTY(direitos.cmbtipo.value)
				uf_perguntalt_chama("TEM DE ESCOLHER O TIPO DE OPOSI��O!","OK","",48)
				RETURN .F.
			ELSE
				LOCAL lcTipo

				DO CASE 
					CASE ALLTRIM(direitos.cmbtipo.value) = 'SMS'
						TEXT TO lcSQL TEXTMERGE NOSHOW
							UPDATE b_utentes SET autoriza_sms = 0 where no=<<lcNo>> and estab=<<lcEstab>>
						ENDTEXT 
					CASE ALLTRIM(direitos.cmbtipo.value) = 'EMAIL'
						TEXT TO lcSQL TEXTMERGE NOSHOW
							UPDATE b_utentes SET autoriza_emails = 0 where no=<<lcNo>> and estab=<<lcEstab>>
						ENDTEXT 
					OTHERWISE 
						TEXT TO lcSQL TEXTMERGE NOSHOW
							UPDATE b_utentes SET autoriza_sms = 0, autoriza_emails = 0 where no=<<lcNo>> and estab=<<lcEstab>>
						ENDTEXT 
				ENDCASE 

				IF !uf_gerais_actgrelha("", "", lcSql)
					uf_perguntalt_chama("ERRO AO ATUALIZAR A TABELA DE UTENTES!","OK","",48)
					RETURN .F.
				ENDIF 
				
				uf_perguntalt_chama("DIREITO � OPOSI��O CONFIRMADO COM SUCESSO!" + CHR(13) + "N�O SE ESQUE�A DE ARQUIVAR O DOCUMENTO ASSINADO PELO CLIENTE.","OK","",64)
					
				TEXT TO lcSQL TEXTMERGE NOSHOW
					SELECT utstamp FROM b_utentes where no=<<lcNo>> and estab=<<lcEstab>>
				ENDTEXT 
				IF !uf_gerais_actgrelha("", "ucrsUtStamp", lcSql)
					uf_perguntalt_chama("ERRO AO CONSULTAR A TABELA DE UTENTES!","OK","",48)
					RETURN .F.
				ENDIF 
				uf_user_log_ins('CL', ALLTRIM(ucrsUtStamp.utstamp), 'CONFIRMA��O', 'Confirma��o de oposi��o via documento assinado - Direito de oposi��o')
				
				&& Actualiza Ecr�
				IF TYPE("UTENTES")!= "U" 
					TEXT TO lcSql NOSHOW textmerge
						SELECT Top 1 
							b_utentes.* 
							,pontos = isnull(b_fidel.pontosAtrib,0) - isnull(b_fidel.pontosUsa,0)
							,binoValidade = convert(datetime,isnull(cc.validade_fim, '19000101'))
							,compfixa=0
                            ,valcartao = 0
                            ,percadic = 0
							,templano = convert(bit,0)
						FROM 
							b_utentes (nolock) 
							left join b_fidel (nolock)  on b_utentes.no = b_fidel.clno and b_utentes.estab = b_fidel.clestab
							left join dispensa_eletronica_cc cc (nolock) on b_utentes.utstamp = cc.utstamp
						where 
							b_utentes.no = <<lcNo>> 
							and b_utentes.estab = <<lcEstab>>
					ENDTEXT
					IF !uf_gerais_actgrelha("", "cl", lcSql)
						RETURN .f.
					ENDIF
					SELECT cl
					utentes.refresh
				ENDIF 

				uf_direitos_sair()
				 
			ENDIF 

		CASE direitos.txtTipoDireito.caption='EXERCER DIREITO � PORTABILIDADE'	
		
		
			LOCAL mDir, lcTo, lcToEmail, lcSubject, lcBody, lcAtatchment, lcNomeFicheiroXLS
						
			IF !EMPTY(cl.email)
				IF uf_perguntalt_chama("CONFIRMA O ENVIO DO FICHEIRO PARA O UTENTE?","Sim","N�o")
					mdir = ALLTRIM(uf_gerais_getParameter('ADM0000000015','TEXT')) + "\Cache\XLS"
					lcNomeFicheiroXLS= 'Utente'+ALLTRIM(STR(cl.no)) + ".xls"
					lcTo = ""
					lcToEmail = ALLTRIM(cl.email)
					lcSubject = 'Envio dos dados pessoais'
					lcBody = "Exmo(a). Sr(a). " + CHR(13) + CHR(13) + "No seguimento do seu pedido, vimos por este meio enviar-lhe o documento em excel com os seus dados pessoais." + CHR(13) + ALLTRIM(ucrse1.mailsign)
					lcAtatchment = mdir + "\"+ALLTRIM(lcNomeFicheiroXLS)

					TEXT TO lcSQL TEXTMERGE NOSHOW
						up_utentes_exporta '<<ALLTRIM(cl.Utstamp)>>'
					ENDTEXT 
					IF !uf_gerais_actgrelha("", "ucrsDadosUt", lcSql)
						uf_perguntalt_chama("ERRO AO CONSULTAR A TABELA DE UTENTES!","OK","",48)
						RETURN .F.
					ENDIF 

					SELECT ucrsDadosUt
					export to ALLTRIM(lcAtatchment) type xl5
					SELECT cl
					
					regua(0,6,"A enviar email ...")
					uf_startup_sendmail_emp(lcTo, lcToEmail, lcSubject, lcBody , lcAtatchment, .t.)
					regua(2)

					uf_user_log_ins('CL', ALLTRIM(cl.Utstamp), 'EMAIL', 'Envio de dados pessoais por email para ' + ALLTRIM(cl.email) + ' - ' + ALLTRIM(lcNomeFicheiroXLS))
					
					
					uf_perguntalt_chama("FICHEIRO ENVIADO COM SUCESSO!" + CHR(13) + "N�O SE ESQUE�A DE ARQUIVAR O DOCUMENTO ASSINADO PELO CLIENTE.","OK","",64)
					
					TEXT TO lcSQL TEXTMERGE NOSHOW
						SELECT utstamp FROM b_utentes where no=<<lcNo>> and estab=<<lcEstab>>
					ENDTEXT 
					IF !uf_gerais_actgrelha("", "ucrsUtStamp", lcSql)
						uf_perguntalt_chama("ERRO AO CONSULTAR A TABELA DE UTENTES!","OK","",48)
						RETURN .F.
					ENDIF 
					uf_user_log_ins('CL', ALLTRIM(cl.utstamp), 'CONFIRMA��O', 'Confirma��o via documento assinado - Portabilidade, ficheiro enviado para ' + ALLTRIM(lcToEmail))
					
					&& Actualiza Ecr�
					IF TYPE("UTENTES")!= "U" 
						TEXT TO lcSql NOSHOW textmerge
							SELECT Top 1 
								b_utentes.* 
								,pontos = isnull(b_fidel.pontosAtrib,0) - isnull(b_fidel.pontosUsa,0)
								,binoValidade = convert(datetime,isnull(cc.validade_fim, '19000101'))
								,compfixa=0
                                ,valcartao = 0
                                ,percadic = 0
								,templano = convert(bit,0)
							FROM 
								b_utentes (nolock) 
								left join b_fidel (nolock)  on b_utentes.no = b_fidel.clno and b_utentes.estab = b_fidel.clestab
								left join dispensa_eletronica_cc cc (nolock) on b_utentes.utstamp = cc.utstamp
							where 
								b_utentes.no = <<lcNo>> 
								and b_utentes.estab = <<lcEstab>>
						ENDTEXT
						IF !uf_gerais_actgrelha("", "cl", lcSql)
							RETURN .f.
						ENDIF
						SELECT cl
						utentes.refresh
					ENDIF 

					
					uf_direitos_sair()
					
				ELSE
					uf_perguntalt_chama("ENVIO CANCELADO!","OK","",48)
				ENDIF 

			ELSE
				uf_perguntalt_chama("N�o existe um email para envio preenchido." + CHR(13) + "Atualize a ficha respetiva.","OK","",48)
				RETURN .f.
			ENDIF 

		OTHERWISE
	ENDCASE 


ENDFUNC 