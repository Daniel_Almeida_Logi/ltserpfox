FUNCTION uf_SincClientes_chama
	&& Controla abertura do Painel
	if type("SINCCLIENTES")=="U"
		DO FORM SINCCLIENTES
	ELSE
		SINCCLIENTES.show
	ENDIF
	
	*menu
	IF TYPE("sincclientes.menu1.sincTodos") == "U"
		sincclientes.menu1.adicionaOpcao("sincSel", "Sinc. Sel.", myPath + "\imagens\icons\doc_seta_w.png", "uf_SincClientes_Seleccionados", "F1")
		sincclientes.menu1.adicionaOpcao("sincTodos", "Sinc. Todos", myPath + "\imagens\icons\actualizar_w.png", "uf_SincClientes_Todos","F2")
	ENDIF 
ENDFUNC


**
FUNCTION uf_SincClientes_Todos
	LOCAL lcSQL
	STORE '' TO lcSQL	
	
	If !uf_perguntalt_chama("Aten��o, vai correr a sincroniza��o de todos os clientes."+CHR(13)+"Tem a certeza que pretende continuar?"+CHR(13)+"Poder� demorar alguns minutos.","Sim","N�o")  
		RETURN  
	ELSE
		** Preparar Regua **
		regua(0,3,"A SINCRONIZAR CLIENTES...",.f.)
		regua[1,1,"A SINCRONIZAR CLIENTES...",.f.]
		****************
		TEXT TO lcSQL TEXTMERGE noshow
			Set ansi_nulls on
			Set ansi_warnings on
			exec up_fanse_sync_cl_parametro -1
		ENDTEXT 
		regua[1,2,"A SINCRONIZAR CLIENTES...",.f.]
		IF !uf_gerais_actGrelha("","",lcSQL)
			uf_perguntalt_chama("OCORREU UM ERRO AO SINCRONIZAR OS CLIENTES.","OK","",16)
			REGUA(2)
			RETURN .f.
		ENDIF 
		uf_perguntalt_chama("CLIENTES SINCRONIZADOS COM SUCESSO.","OK","",64)
		regua[1,3,"A SINCRONIZAR CLIENTES...",.f.]
		REGUA(2)
		uf_SincClientes_sair()
	ENDIF 
		
ENDFUNC 


**
FUNCTION uf_SincClientes_Seleccionados		
	LOCAL lcSQL, lcNumero, lcNumero2
	STORE '' TO lcSQL
	STORE 0 TO lcNumero, lcNumero2
	
	IF EMPTY(ALLTRIM(astr(SINCCLIENTES.txtDO.value)))
		uf_perguntalt_chama("N�O INDICOU NENHUM N�MERO DE CLIENTE PARA SINCRONIZAR.","OK","",48)
		RETURN .f.
	ELSE
		IF EMPTY(ALLTRIM(astr(SINCCLIENTES.txtAO.value)))
			regua(0,3,"A SINCRONIZAR CLIENTES...",.f.)
			regua[0,1,"A SINCRONIZAR CLIENTES...",.f.]
			lcSQL=''
			TEXT TO lcSQL TEXTMERGE noshow
				Set ansi_nulls on
				Set ansi_warnings on
				exec up_fanse_sync_cl_parametro <<ALLTRIM(astr(SINCCLIENTES.txtDO.value))>>
			ENDTEXT 
			regua[0,2,"A SINCRONIZAR CLIENTES...",.f.]
			IF !uf_gerais_actGrelha("","",lcSQL)
				uf_perguntalt_chama("OCORREU UM ERRO AO SINCRONIZAR O CLIENTE: " + ALLTRIM(astr(SINCCLIENTES.txtDO.value)),"OK","",16)
				regua(2)
				RETURN .f.
			ELSE
				uf_perguntalt_chama("CLIENTES SINCRONIZADOS COM SUCESSO.","OK","",64)
				regua[0,3,"A SINCRONIZAR CLIENTES...",.f.]
				regua(2)
				uf_SincClientes_sair()
			ENDIF 
		ELSE
			IF VAL(SINCCLIENTES.txtDO.value) < VAL(SINCCLIENTES.txtAO.value)
				** Preparar Regua **
				mntotal=VAL(SINCCLIENTES.txtAO.value) - VAL(SINCCLIENTES.txtDO.value)+1
				regua(0,mntotal,"A SINCRONIZAR CLIENTES...",.f.)
				regua[0,1,"A SINCRONIZAR CLIENTES...",.f.]
				****************
				FOR i = VAL(SINCCLIENTES.txtDO.value) TO VAL(SINCCLIENTES.txtAO.value)+1
					lcNumero2 = VAL(SINCCLIENTES.txtDO.value) + lcNumero
					lcSQL=''
					TEXT TO lcSQL TEXTMERGE noshow
						Set ansi_nulls on
						Set ansi_warnings on
						exec up_fanse_sync_cl_parametro <<round(lcNumero2,0)>>
					ENDTEXT 
					IF !uf_gerais_actGrelha("","",lcSQL)
						uf_perguntalt_chama("OCORREU UM ERRO AO SINCRONIZAR O CLIENTE: " + astr(lcNumero2),"OK","",16)
					ENDIF
					lcNumero = lcNumero + 1
					i=i+1
					regua[0,lcNumero+1,"A SINCRONIZAR CLIENTES...",.f.]
				ENDFOR 
				regua(2)
				uf_perguntalt_chama("CLIENTES SINCRONIZADOS COM SUCESSO.","OK","",64)
				uf_SincClientes_sair()
			ELSE
				uf_perguntalt_chama("DADOS FORNECIDOS INV�LIDOS.","OK","",48)
				RETURN .f.
			ENDIF 
		ENDIF 
	ENDIF 
ENDFUNC 

**
FUNCTION uf_SincClientes_sair
	SINCCLIENTES.hide
		
	RELEASE SINCCLIENTES
ENDFUNC 