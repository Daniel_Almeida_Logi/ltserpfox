**
FUNCTION uf_FATASSOCIADOS_chama

	**Valida Licenciamento	
	IF (uf_gerais_addConnection('FATASS') == .f.)
		RETURN .F.
	ENDIF

	uf_FATASSOCIADOS_cursoresIniciais()

	IF TYPE("FATASSOCIADOS") == "U"
		DO FORM FATASSOCIADOS
	ELSE
		FATASSOCIADOS.show
	ENDIF
	
	RETURN .T.
ENDFUNC

**
FUNCTION uf_FATASSOCIADOS_carregaMenu
	FATASSOCIADOS.menu1.adicionaOpcao("opcoes", "Op��es", myPath + "\imagens\icons\opcoes_w.png", "","F1")
	FATASSOCIADOS.menu1.adicionaOpcao("actualizar", "Actualizar", myPath + "\imagens\icons\actualizar_w.png", "uf_FATASSOCIADOS_atualiza","F2")
	FATASSOCIADOS.menu1.adicionaOpcao("selTodos", "Sel. Todos", myPath + "\imagens\icons\unchecked_w.png", "uf_FATASSOCIADOS_selTodos","F3")
	FATASSOCIADOS.menu1.estado("", "", "Emitir", .t., "Sair", .t.)
ENDFUNC 


**
FUNCTION uf_FATASSOCIADOS_cursoresIniciais
	LOCAL lcSQL
	lcSQL = ""	

	IF !USED("ucrsDadosFacturacaoEntidades")
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			exec up_pesqdocassociados_faturacao 1900, 1900, 1, 1, 'xxxx', 0, 0, 'xxxx','', 0
		ENDTEXT 
		
		If !uf_gerais_actGrelha("", "ucrsDadosFacturacaoAssociados", lcSql)
			uf_perguntalt_chama("N�O FOI POSS�VEL VERIFICAR DADOS DE FATURACAO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			RETURN .f.
		ENDIF 
	ENDIF 
ENDFUNC 


**
FUNCTION uf_FATASSOCIADOS_selTodos

	IF EMPTY(FATASSOCIADOS.menu1.seltodos.tag) OR FATASSOCIADOS.menu1.seltodos.tag == "false"
		&& aplica sele��o
		REPLACE ALL sel WITH .t. FOR EMPTY(ucrsDadosFacturacaoAssociados.faturado) IN ucrsDadosFacturacaoAssociados
		
		&& altera o botao
		FATASSOCIADOS.menu1.selTodos.tag = "true"
		FATASSOCIADOS.menu1.selTodos.config("Sel. Todos", myPath + "\imagens\icons\checked_w.png", "uf_FATASSOCIADOS_selTodos","F3")

	ELSE
		&& aplica sele��o
		REPLACE ALL sel WITH .f. FOR EMPTY(ucrsDadosFacturacaoAssociados.faturado) IN ucrsDadosFacturacaoAssociados
		
		&& altera o botao
		FATASSOCIADOS.menu1.selTodos.tag = "false"
		FATASSOCIADOS.menu1.selTodos.config("Sel. Todos", myPath + "\imagens\icons\unchecked_w.png", "uf_FATASSOCIADOS_selTodos","F3")
	ENDIF
	
	SELECT ucrsDadosFacturacaoAssociados
	GO TOP 

ENDFUNC 

**
FUNCTION uf_FATASSOCIADOS_atualiza
	LOCAL lcSQL
	
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_pesqdocassociados_faturacao 
			<<FATASSOCIADOS.anoIni.Value>>
			,<<FATASSOCIADOS.anoFim.Value>>
			,<<FATASSOCIADOS.mesIni.Value>>
			,<<FATASSOCIADOS.mesFim.Value>>
			,'<<ALLTRIM(FATASSOCIADOS.utente.value)>>'
			,0
			,0
			,'<<ALLTRIM(FATASSOCIADOS.servico.value)>>'
			,'<<ALLTRIM(FATASSOCIADOS.tipo.value)>>'
			,'<<mysite_nr>>'
	ENDTEXT 	
			
	IF !uf_gerais_actGrelha("FATASSOCIADOS.gridPesq", "ucrsDadosFacturacaoAssociados", lcSql)
		uf_perguntalt_chama("N�O FOI POSS�VEL VERIFICAR DADOS DE FATURACAO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	ENDIF 
	
ENDFUNC 


**
FUNCTION uf_fatassociados_afterrowcolchangeentidade
	
ENDFUNC 


**
FUNCTION uf_fatassociados_validacao
	
		lcSQL = ""	
		TEXT TO lcSql NOSHOW textmerge
				SELECT	case when max(fdata) > '<<uf_gerais_getDate(FATASSOCIADOS.data.value,"SQL")>>' then 1 else 0 end as valor
				FROM	ft (nolock)
				WHERE 	ft.ftano = YEAR('<<uf_gerais_getDate(FATASSOCIADOS.data.value,"SQL")>>')
				AND     ft.nmdoc = 'Factura'
				AND 	ft.site = '<<mysite>>'
			ENDTEXT

			IF !uf_gerais_actGrelha("","uCrsMaxNoValida",lcSql)
				uf_perguntalt_chama("N�o foi poss�vel validar a numera��o do �ltimo documento.","OK","",16)
				RETURN .f.
			ELSE
				IF reccount()>0
					IF uCrsMaxNoValida.valor == 1
						uf_perguntalt_chama("J� existem documentos com data superior � seleccionada. Por favor verifique.","OK","",64)
						fecha("uCrsMaxNoValida")
						RETURN .f.
					ENDIF
				ENDIF
				fecha("uCrsMaxNoValida")
			ENDIF

		RETURN .t.
ENDFUNC


**
FUNCTION uf_FATASSOCIADOS_gravar
	LOCAL lcValida
	lcValida = .f.
	
	SELECT 	ucrsDadosFacturacaoAssociados 
	GO TOP
	
	SELECT No, Estab, Ano, Mes, Ref, COUNT(*) as TotalDuplicatas ;
	FROM ucrsDadosFacturacaoAssociados ;
	WHERE ucrsDadosFacturacaoAssociados.sel = .T.;
	GROUP BY No, Estab, Ano, Mes, Ref ;
	HAVING COUNT(*) > 1 ;
	INTO CURSOR ucrsCheckDuplicates readWrite
	
	SELECT ucrsCheckDuplicates
	GO TOP
	IF (ucrsCheckDuplicates.TotalDuplicatas > 1)
		uf_perguntalt_chama("Selecionou a mesma referencia para o mesmo utente." + CHR(13) + "Por favor desmarque uma das faturas.","OK","",48)
		fecha("ucrsCheckDuplicates")
		RETURN .f.
	ENDIF
	
	fecha("ucrsCheckDuplicates")
	
	
	IF !uf_perguntalt_chama("Vai emitir Documentos de Fatura��o para os servi�os selecionados. Pretende continuar?", "Sim", "N�o")
		RETURN .f.
	ENDIF 


	&& Valida Data Para Emiss�o da Fatura
	lcValida = uf_fatassociados_validacao()
	IF lcValida != .t.
		RETURN .f.
	ENDIF

	&& Cria cursor para posteriormente permitir a impress�o de seguida **
	IF Used("uCrsImpFatClServ")
		fecha("uCrsImpFatClServ")
	ENDIF 
	create cursor uCrsImpFatClServ (stamp c(26), nmdoc c(45), nome c(80))
	
	&& Criar cursor com um cliente por linha - usa campoFT porque corresponde aos dados a quem se deve faturar - no caso de ser AFP ordena o cursor por ordem alfabeticao a pedido deles
	IF UPPER(ALLTRIM(uCrse1.id_lt)) != 'E01322A'
		Select ucrsDadosFacturacaoAssociados
		GO TOP
		SELECT distinct noFT, estabFT, nome FROM ucrsDadosFacturacaoAssociados WHERE !EMPTY(ucrsDadosFacturacaoAssociados.sel) Into Cursor uCrsDistinctCl READWRITE 
		IF Reccount("uCrsDistinctCl") == 0
			uf_perguntalt_chama("N�o existem servi�os marcados para Faturar.", "OK", "", 48)
			RETURN
		ENDIF
	ELSE
		SELECT ucrsDadosFacturacaoAssociados
		GO TOP
		SELECT distinct noFT, estabFT, nome FROM ucrsDadosFacturacaoAssociados WHERE !EMPTY(ucrsDadosFacturacaoAssociados.sel) order by nome INTO CURSOR uCrsDistinctCl READWRITE 
		IF Reccount("uCrsDistinctCl") == 0
			uf_perguntalt_chama("N�o existem servi�os marcados para Faturar.", "OK", "", 48)
			RETURN
		ENDIF
	ENDIF
	

	&& Preparar Regua
	SELECT uCrsDistinctCl
	GO TOP
	mntotal = reccount()
	regua(0, mntotal, "A PROCESSAR FACTURAS...", .f.)

	SELECT uCrsDistinctCl
	GO TOP
	SCAN
		regua[1,recno("uCrsDistinctCl"),"A processar fatura para o cliente o Cliente N�: " + Astr(uCrsDistinctCl.noFT), .f.]
		
		**
		IF UPPER(ALLTRIM(uCrse1.id_lt)) == 'E01322A'
			SELECT uCrsImpFatClServ
			replace uCrsImpFatClServ.nome  WITH ALLTRIM(uCrsDistinctCl.nome)
		ENDIF
		
		&& variaveis para cabe�alho
		Local lcNome, lcEstab, lcNo, lcNrVenda, lcFtStamp, lcObs
		Store '' To lcNome, lcNmdoc, lcFtStamp, lcObs
		Store 0 To lcNo, lcEstab, lcNrVenda

		Local lcTotQtt, lcQtt1, lcEtotal, lcEttIliq, lcEcusto, lcEtotalLinha
		Local lcEivaIn1, lcEivaIn2, lcEivaIn3, lcEivaIn4, lcEivaIn5, lcEivaIn6, lcEivaIn7, lcEivaIn8, lcEivaIn9
		Local lcEivaV1, lcEivaV2, lcEivaV3, lcEivaV4, lcEivaV5, lcEivaV6, lcEivaV7, lcEivaV8, lcEivaV9
		Store 0 To lcTotQtt, lcQtt1, lcEtotal, lcEttIliq, lcEcusto, lcEtotalLinha
		Store 0 To lcEivaIn1, lcEivaIn2, lcEivaIn3, lcEivaIn4, lcEivaIn5, lcEivaIn6, lcEivaIn7, lcEivaIn8, lcEivaIn9
		Store 0 To lcEivaV1, lcEivaV2, lcEivaV3, lcEivaV4, lcEivaV5, lcEivaV6, lcEivaV7, lcEivaV8, lcEivaV9
		
		SELECT uCrsDadosFacturacaoAssociados
		GO TOP
		SCAN FOR uCrsDadosFacturacaoAssociados.sel AND uCrsDadosFacturacaoAssociados.noFT == uCrsDistinctCl.noFT AND uCrsDadosFacturacaoAssociados.estabFT== uCrsDistinctCl.estabFT
						
			&& Calcular Totais por Venda
			lcTotQtt		= lcTotQtt	+ 1
			lcQtt1			= lcQtt1	+ 1
			lcEtotal		= lcEtotal	+ ucrsDadosFacturacaoAssociados.ValorCalcTotais
			lcEtotalLinha	= lcEtotalLinha	+ ucrsDadosFacturacaoAssociados.totalLinha
			lcEttIliq		= lcEttIliq	+ ucrsDadosFacturacaoAssociados.totalLinhaSemIva
			lcEcusto		= lcEcusto	+ ucrsDadosFacturacaoAssociados.epcusto
		
			IF ucrsDadosFacturacaoAssociados.tabiva == 1
				lcEivaIn1	= lcEivaIn1 + ucrsDadosFacturacaoAssociados.totalLinhaSemIva
				lcEivaV1	= lcEivaV1 + ucrsDadosFacturacaoAssociados.totalLinhaIva
			ELSE 
				IF ucrsDadosFacturacaoAssociados.tabiva == 2
					lcEivaIn2	= lcEivaIn2 + ucrsDadosFacturacaoAssociados.totalLinhaSemIva
					lcEivaV2	= lcEivaV2 + ucrsDadosFacturacaoAssociados.totalLinhaIva
				ELSE 
					IF ucrsDadosFacturacaoAssociados.tabiva == 3
						lcEivaIn3	= lcEivaIn3 + ucrsDadosFacturacaoAssociados.totalLinhaSemIva
						lcEivaV3	= lcEivaV3 + ucrsDadosFacturacaoAssociados.totalLinhaIva
					ELSE 
						IF ucrsDadosFacturacaoAssociados.tabiva == 4
							lcEivaIn4	= lcEivaIn4 + ucrsDadosFacturacaoAssociados.totalLinhaSemIva
							lcEivaV4	= lcEivaV4 + ucrsDadosFacturacaoAssociados.totalLinhaIva
						ELSE 
							IF ucrsDadosFacturacaoAssociados.tabiva == 5
								lcEivaIn5	= lcEivaIn5 + ucrsDadosFacturacaoAssociados.totalLinhaSemIva
								lcEivaV5	= lcEivaV5 + ucrsDadosFacturacaoAssociados.totalLinhaIva
							ELSE 
								IF ucrsDadosFacturacaoAssociados.tabiva == 6
								lcEivaIn6	= lcEivaIn6 + ucrsDadosFacturacaoAssociados.totalLinhaSemIva
								lcEivaV6	= lcEivaV6 + ucrsDadosFacturacaoAssociados.totalLinhaIva
								ELSE 
									IF ucrsDadosFacturacaoAssociados.tabiva == 7
										lcEivaIn7	= lcEivaIn7 + ucrsDadosFacturacaoAssociados.totalLinhaSemIva
										lcEivaV7	= lcEivaV7 + ucrsDadosFacturacaoAssociados.totalLinhaIva
									ELSE 
										IF ucrsDadosFacturacaoAssociados.tabiva == 8
											lcEivaIn8	= lcEivaIn8 + ucrsDadosFacturacaoAssociados.totalLinhaSemIva
											lcEivaV8	= lcEivaV8 + ucrsDadosFacturacaoAssociados.totalLinhaIva
										ELSE 
											IF ucrsDadosFacturacaoAssociados.tabiva == 9
												lcEivaIn9	= lcEivaIn4 + ucrsDadosFacturacaoAssociados.totalLinhaSemIva
												lcEivaV9	= lcEivaV4 + ucrsDadosFacturacaoAssociados.totalLinhaIva
											ENDIF 
										ENDIF 
									ENDIF 
								ENDIF 
							ENDIF 
						ENDIF 
					ENDIF 
				ENDIF 
			ENDIF 

			Select ucrsDadosFacturacaoAssociados
		ENDSCAN

		&& Guardar stamps das Vendas
		lcFtStamp = uf_gerais_stamp()

		&& Guardar data e Hora
		Local lcHora, lcData, lcPdata &&, lcDataUltimoDiaMes
		SET HOURS TO 24
		Atime=ttoc(datetime()+(difhoraria*3600),2)
		Astr=left(Atime,8)
		lcHora = Astr
		**lcHora = Time()
		lcData = uf_gerais_getDate(FATASSOCIADOS.data.value,"SQL")
		lcPdata = lcData

		&& Determinar n�mero de venda
		LOCAL lcFno
		IF !uf_gerais_actgrelha("", "ucrsDadosFacturacaoAssociadosFno", [up_sales_getNextSaleNr'] + lcData + [',] + astr(myFact))
			uf_perguntalt_chama("Ocorreu um erro a calcular n�mero da venda. Por favor contate o suporte.", "OK", "", 16)
			RETURN .f.
		ELSE 
			IF reccount("ucrsDadosFacturacaoAssociadosFno") > 0
				select ucrsDadosFacturacaoAssociadosFno
				GO TOP 
				lcFno = ucrsDadosFacturacaoAssociadosFno.fno
			ELSE 
				lcFno = 1
			ENDIF 
		ENDIF 
			
		&& Observa��es Documento
		lcObs = 'Documento Gerado via Factura��o Autom�tica'
		
		&& Gravar Fatura Cabe�alho
		lcSQL = ''
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			exec up_sales_createHeader 
					'<<lcFtStamp>>'
					,<<lcFno>>
					,<<uCrsDistinctCl.noFT>>
					,<<uCrsDistinctCl.estabFT>>
					,<<myFact>>
					,'<<lcData>>' ,'<<lcHora>>',
					<<ch_vendedor>>
					,0
					,<<ROUND(lcEtotal,2)>>
					,<<lcTotQtt>>
					,<<lcEcusto>>
					,0
					,<<ROUND(lcEivaIn1,2)>>, <<ROUND(lcEivaIn2,2)>>, <<ROUND(lcEivaIn3,2)>>, <<ROUND(lcEivaIn4,2)>>, <<ROUND(lcEivaIn5,2)>>, <<ROUND(lcEivaIn6,2)>>, <<ROUND(lcEivaIn7,2)>>, <<ROUND(lcEivaIn8,2)>>, <<ROUND(lcEivaIn9,2)>>
					,<<ROUND(lcEivaV1,2)>>, <<ROUND(lcEivaV2,2)>>, <<ROUND(lcEivaV3,2)>>, <<ROUND(lcEivaV4,2)>>, <<ROUND(lcEivaV5,2)>>, <<ROUND(lcEivaV6,2)>>, <<ROUND(lcEivaV7,2)>>, <<ROUND(lcEivaV8,2)>>, <<ROUND(lcEivaV9,2)>>
					,'<<mySite>>'
					,<<myTermNo>>
					,'', ''
					,'<<ALLTRIM(lcObs)>>', ''
		ENDTEXT
	
		IF !uf_gerais_actgrelha("","",lcSQL)
			uf_perguntalt_chama("Ocorreu uma anomalia ao gravar o documento. Por favor contate o suporte.", "OK", "", 16)
			RETURN .f.
		ENDIF 
		
		&& Gravar Detalhe Fatura
		LOCAL lordem, lcDesign, lcClienteFT, lcAddLinha, lcDesignAddLinha, Lordem1  
		lordem = 11000
		STORE '' TO lcDesign, lcDesignAddLinha 
		STORE 0 TO lcClienteFT, Lordem1 
		STORE .f. TO lcAddLinha

 		SELECT ucrsDadosFacturacaoAssociados
		GO TOP
		SCAN FOR ucrsDadosFacturacaoAssociados.sel;
				 AND ucrsDadosFacturacaoAssociados.noFT == uCrsDistinctCl.noFT;
				 AND ucrsDadosFacturacaoAssociados.estabFT == uCrsDistinctCl.estabFT
			
			&& valida��o para adicionar linha com informa��o cliente + info
			IF lcClienteFT != ucrsDadosFacturacaoAssociados.estab
				lcClienteFT = ucrsDadosFacturacaoAssociados.estab
				lcAddLinha = .t.
			ENDIF
			
			&& Informa��o referente ao periodo de faturacao para usar design + info ou linha com cliente + info
			LOCAL lcMes, lcAno
			STORE 0 TO lcMes, lcAno
					
			IF (ucrsDadosFacturacaoAssociados.mes+ucrsDadosFacturacaoAssociados.desvio_mes_fat) == 0 
				lcMes = 12
				lcAno = (ucrsDadosFacturacaoAssociados.ano-1)
				IF (ucrsDadosFacturacaoAssociados.mes+ucrsDadosFacturacaoAssociados.desvio_mes_fat) == 13
					lcMes = 1
					lcAno = (ucrsDadosFacturacaoAssociados.ano+1)
				ENDIF
			ELSE
				lcMes = ASTR(ucrsDadosFacturacaoAssociados.mes+ucrsDadosFacturacaoAssociados.desvio_mes_fat)
				lcAno = ASTR(ucrsDadosFacturacaoAssociados.ano)
			ENDIF
			
			&& adicionar linha com a informa��o (nome do cliente + data) que corresponde ao set de produtos/servicos no documento por cliente ou adicionar informa��o � frente da design do produto
			IF ucrsDadosFacturacaoAssociados.no == ucrsDadosFacturacaoAssociados.noFT AND ucrsDadosFacturacaoAssociados.estab != ucrsDadosFacturacaoAssociados.estabFT
				IF 	lcAddLinha == .t.
					lcDesignAddLinha = ALLTRIM(ucrsDadosFacturacaoAssociados.nome) + " - Ref. a " + ASTR(lcMes) + "/" + ASTR(lcAno)		

					&& Gerar stamp para detalhe
					lcFiStamp = uf_gerais_stamp()
					
					&& Gerar Lordem add Linha
					Lordem1 = Lordem - 500
					
					lcSQL = ''
					TEXT TO  lcSQL NOSHOW TEXTMERGE 
						exec up_sales_createDetail
							 '<<lcFtStamp>>', '<<lcFiStamp>>', ''
							,'<<lcDesignAddLinha>>'
							,0 ,0, 23, 0
							,<<myArmazem>>, <<Lordem1>>, 0, 0, 0
					ENDTEXT
					
					IF !uf_gerais_actgrelha("", "", lcSQL)
						uf_perguntalt_chama("Ocorreu uma anomalia ao gravar uma linha do documento. Por favor contate o suporte.", "OK", "", 16)
						RETURN .f.
					ENDIF 

					lcAddLinha = .f.
				ENDIF
			ELSE 
				lcDesign = ALLTRIM(ucrsDadosFacturacaoAssociados.design) + " - Ref. a " + ASTR(lcMes) + "/" + ASTR(lcAno)			
			ENDIF

			&& Gerar stamp para detalhe
			lcFiStamp = uf_gerais_stamp()

			&&
			lcSQL = ''
			TEXT TO  lcSQL NOSHOW TEXTMERGE 
				exec up_sales_createDetail
					 '<<lcFtStamp>>', '<<lcFiStamp>>', '<<ucrsDadosFacturacaoAssociados.ref>>'
					,'<<lcdesign>>'
					,<<ucrsDadosFacturacaoAssociados.qtt>>, <<ucrsDadosFacturacaoAssociados.totalLinha>>, <<ucrsDadosFacturacaoAssociados.iva>>, <<IIF(ucrsDadosFacturacaoAssociados.ivaincl,1,0)>>
					,<<myArmazem>>, <<lordem>>, <<ucrsDadosFacturacaoAssociados.preco>>, <<ucrsDadosFacturacaoAssociados.desconto>>, <<ucrsDadosFacturacaoAssociados.desconto_valor>>
			ENDTEXT
			
			IF !uf_gerais_actgrelha("", "", lcSQL)
				uf_perguntalt_chama("Ocorreu uma anomalia ao gravar uma linha do documento. Por favor contate o suporte.", "OK", "", 16)
				RETURN .f.
			ENDIF

			&&
			lcSQL = ''
			TEXT TO  lcSql NOSHOW TEXTMERGE 
				INSERT INTO cl_servicos_fat (
					ano
					,mes
					,ref
					,nr_cl
					,dep_cl
					,fistamp
				)VALUES(
					<<ucrsDadosFacturacaoAssociados.ano>>
					,<<ucrsDadosFacturacaoAssociados.mes>>
					,'<<ALLTRIM(ucrsDadosFacturacaoAssociados.ref)>>'
					,<<ucrsDadosFacturacaoAssociados.no>>
					,<<ucrsDadosFacturacaoAssociados.estab>>
					,'<<ALLTRIM(lcFiStamp)>>'
				)				
			ENDTEXT
				
			IF !uf_gerais_actgrelha("", "", lcSQL)
				uf_perguntalt_chama("Ocorreu uma anomalia ao gravar uma linha do documento. Por favor contate o suporte.", "OK", "", 16)
				RETURN .f.
			ENDIF 


			lordem = lordem + 1000

			select ucrsDadosFacturacaoAssociados
		ENDSCAN

		** gravar certifica��o **
		uf_gravarCert(lcFtStamp, lcFno, myFact, lcData, lcData, lcHora, lcEtotal, 'FT')
		
		** arquivos pdf **
		myVersaoDoc=uf_gerais_versaodocumento(1)
		
*!*				IF myArquivoDigital
*!*					uf_arquivoDigital_TaloesLt(Alltrim(lcFtStamp))
*!*				Endif
	
*!*				If myArquivoDigitalPDF
*!*					uf_arquivodigital_ExpFactAutEntidadesA4(Alltrim(lcFtStamp))
*!*				Endif
		
		Select uCrsImpFatClServ
		Append Blank
		replace uCrsImpFatClServ.stamp with Alltrim(lcFtStamp)
		replace uCrsImpFatClServ.nmdoc With Alltrim(myFactNm)
	
		** reset �s vari�veis
		Store 0 To lcTotQtt, lcQtt1, lcEtotal, lcEttIliq, lcEcusto
		Store 0 To mlcEivaIn1, mlcEivaIn2, mlcEivaIn3, mlcEivaIn4, mlcEivaIn5, mlcEivaIn6, mlcEivaIn7, mlcEivaIn8, mlcEivaIn9
		Store 0 To mlcEivaV1, mlcEivaV2, mlcEivaV3, mlcEivaV4, mlcEivaV5, mlcEivaV6, mlcEivaV7, mlcEivaV8, mlcEivaV9
		
		Select uCrsDistinctCl
	ENDSCAN 
	
	** fecha a r�gua e cursores
	regua(2)
	If Used("uCrsDistinctCl")
		Fecha("uCrsDistinctCl")
	Endif
	
	&& Actualiza os resultados	
	uf_FATASSOCIADOS_atualiza()
	
	&& Imprimir as facturas
	IF USED("uCrsImpFatClServ")
		IF Reccount("uCrsImpFatClServ")>0
			IF uf_perguntalt_chama("Deseja imprimir os documentos?", "Sim", "N�o")
				LOCAL lcPrinter, lcDefaultPrinter	
				
				&& coloca o cursor por ordem alfabetica para impirmir por ordem alfab�tica caso seja a AFP - Luis Leal 20160920
				IF UPPER(ALLTRIM(uCrse1.id_lt)) == 'E01322A'

					SELECT * FROM uCrsImpFatClServ ORDER BY nome INTO CURSOR uCrsImpFatClServAux READWRITE 
					
					fecha("uCrsImpFatClServ")
					
					UPDATE uCrsImpFatClServAux SET nome = UPPER(nome)
					
					SELECT * FROM uCrsImpFatClServAux ORDER BY nome INTO CURSOR uCrsImpFatClServ READWRITE 
					
					IF USED("uCrsImpFatClServAux")
						fecha("uCrsImpFatClServAux")
					ENDIF
				ENDIF		
				
				lcPrinter = Getprinter()
				
				IF !EMPTY(lcPrinter)

					SELECT uCrsImpFatClServ
					GO TOP
					SCAN
						**				
						uf_FATASSOCIADOS_imprimeSeguida(Alltrim(uCrsImpFatClServ.stamp), Alltrim(uCrsImpFatClServ.nmdoc), Alltrim(lcPrinter))
						Select uCrsImpFatClServ
					ENDSCAN

				ELSE 
					uf_perguntalt_chama("N�O � POSS�VEL IMPRIMIR SEM ESCOLHER UMA IMPRESSORA. DEVE AGORA IMPRIMIR VIA O PAINEL GEST�O DE FACTURA��O.","OK","", 48)
				ENDIF 
			ENDIF 
		ENDIF 
		
		fecha("uCrsImpFatClServ")
	ENDIF 
ENDFUNC


**
FUNCTION uf_FATASSOCIADOS_imprimeSeguida
	Lparameters tcStamp, tcNmDoc, tcPrinter
	PUBLIC myMotIsencaoIva
	Local lcimpressao, lcNomeFicheiro, lcTabela, lcTemDuplicados, lcNumDuplicados, lcReport
	STORE '' TO myMotIsencaoIva
	
	** Obtem Nome do Ficheiro Tabela de Impressoes **
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		Select Top 1
			nomeficheiro
			,tabela
			,temduplicados
			,numduplicados
		From
			B_impressoes (nolock)
		Where
			documento = '<<Alltrim(tcNmDoc)>>'
		order by
			idupordefeito desc
	Endtext
	If !uf_gerais_actgrelha("", "templcSQL", lcSql)
		uf_perguntalt_chama("N�O FOI POSS�VEL ENCONTRAR A CONFIGURA��O DE IMPRESS�ES. POR FAVOR CONTACTE A ASSISTENCIA T�CNICA.","OK","", 16)
		RETURN .f.
	ELSE
		If RECCOUNT("templcSQL") > 0
			lcNomeFicheiro		= Alltrim(templcSQL.nomeficheiro)
			lctabela			= templcSQL.tabela
			lcTemDuplicados 	= templcSQL.temduplicados
			lcNumDuplicados 	= templcSQL.numduplicados
		ELSE
			uf_perguntalt_chama("N�O FOI POSS�VEL ENCONTRAR A CONFIGURA��O DE IMPRESS�ES. POR FAVOR CONTACTE A ASSISTENCIA T�CNICA.","OK","",48)
			RETURN .f.
		ENDIF
		Fecha("templcSQL")
	ENDIF 
	**
	
	** PREPARAR CURSORES PARA IMPRESS�ES **
	** Obtem dados Gerais da Empresa / Loja **
	SELECT uCrsE1
	
	** Guardar report a imprimir - ficheiro **
	lcReport = Alltrim(myPath)+"\analises\" + Alltrim(lcnomeFicheiro)
	IF !FILE(alltrim(lcReport))
		uf_perguntalt_chama("A IMPRESS�O N�O SE ENCONTRA DEFINIDA. POR FAVOR CONTACTE O SUPORTE","OK","",48)
		RETURN .f.
	ENDIF
	**
	
	** criar cursores para factura��o
	If Alltrim(lctabela) == "FT"
		** Trata BASES INC. IVA
		lcSQL = ""
		TEXT TO lcSQL TEXTMERGE NOSHOW
			exec up_gerais_iduCert '<<Alltrim(tcStamp)>>'
 		Endtext
 		IF !uf_gerais_actgrelha("", "ucrsHash", lcSql)
			uf_perguntalt_chama("OCORREU UM ERRO A CALCULAR INFORMACAO PARA A IMPRESSAO. POR FAVOR CONTACTE O SUPORTE.","OK","", 16)
 			RETURN .f.
 		Endif
		If !Reccount("ucrsHash")>0
			Select ucrsHash
			Append Blank
			replace ucrsHash.temcert	with .f.
			replace uCrsHash.parte1		With ''
			replace uCrsHash.parte1		With ''
		Endif
		
		** carregar cursores de factura��o **
		If uf_gerais_actgrelha("", "ft", [select * from ft (nolock) where ftstamp=']+Alltrim(tcStamp)+['])
			Select ft
			uf_gerais_actgrelha("", "td", [select * from td (nolock) where ndoc=]+ALLTRIM(str(ft.ndoc)))
			Select td
			uf_gerais_actgrelha("", "ft2", [select * from ft2 (nolock) where ft2stamp=']+Alltrim(tcStamp)+['])
			Select ft2
			uf_gerais_actgrelha("", "fi", [select * from fi (nolock) where ftstamp=']+Alltrim(tcStamp)+['])
			Select fi
			GO TOP
		ELSE
			uf_perguntalt_chama("OCORREU UM PROBLEMA A LEVANTAR OS DADOS DO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.","OK","", 16)
			RETURN .f.
		Endif
		**

	ENDIF 
	
	** cria cursor texto rodap�
	uf_gerais_TextoRodape('FT')

	SELECT td
	GO TOP

	Select ft
	GO TOP

	Select fi
	GO TOP 	
	
	PUBLIC  myPaisNacional
	STORE .f. TO myPaisNacional

	SELECT fi
	SELECT ft2
	LOCATE FOR fi.iva == 0
	IF FOUND()
		myMotIsencaoIva = ft2.motiseimp
	ENDIF 
	
	** Texto de Isen��o no IDU (calculado com base na nacionalidade do utente)
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		Select codigoP, noConvencao, designConvencao FROM b_utentes (nolock) WHERE b_utentes.no = <<ft.no>> and b_utentes.estab = <<ft.estab>>
 	ENDTEXT
 	
 	IF !uf_gerais_actGrelha("","uCrsDadosNacionalidadeUtente",lcSQL)
		uf_perguntalt_chama("Ocorreu um erro a verificar Nacionalidade do Utente. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
 		RETURN .f.
 	Endif	
 	
 	Select uCrsDadosNacionalidadeUtente
 	IF UPPER(ALLTRIM(uCrsDadosNacionalidadeUtente.codigoP)) != 'PT'
		myPaisNacional = .f.
	ELSE
		myPaisNacional = .t.
	ENDIF 
	
	** Calcula TxCambio para Impressoes a cliente estrangeiros
	PUBLIC myTxCambio			
	myTxCambio = 0
	
	SELECT ft
	IF ALLTRIM(ft.moeda) != 'EURO'
		lcSQL = ""
		TEXT TO lcSQL TEXTMERGE NOSHOW 
			select top 1 cambio as taxa from cb where moeda = '<<ALLTRIM(ft.moeda)>>' order by data + ousrhora desc
		ENDTEXT 
 		IF !uf_gerais_actGrelha("","uCrsTxCambio",lcSQL)
			uf_perguntalt_chama("Ocorreu uma anomalia a verificar Taxa de Cambio. Por favor contacte o suporte.","OK","",16)
 			RETURN .f.
		ENDIF 
		SELECT uCrsTxCambio
		GO TOP 
		myTxCambio = uCrsTxCambio.taxa
	ELSE
		myTxCambio = 1
	ENDIF

	SELECT td
	GO TOP

	Select ft
	GO TOP
	
	Select fi
	GO TOP 	
	
	** define impressora **
	IF !(alltrim(tcPrinter) == "Impressora por defeito do Windows")
		Set Printer To Name ''+alltrim(tcPrinter)+''
	ENDIF 
	
	** Trata Duplicados **
	IF lcTemDuplicados == .t. And lcNumDuplicados > 0
		FOR i=0 To lcNumDuplicados
			** IMPRIMIR
			myVersaoDoc = uf_gerais_versaodocumento(i+1)
			Report Form Alltrim(lcReport) To Printer Noconsole
		ENDFOR 
	ELSE
		** IMPRIMIR
		myVersaoDoc=uf_gerais_versaodocumento(1)
		Report Form Alltrim(lcReport) To Printer Noconsole
	ENDIF 
	
	SET PRINTER TO DEFAULT
	
	RELEASE myMotIsencaoIva, myPaisNacional, myTxCambio 

ENDFUNC


**
FUNCTION uf_FATASSOCIADOS_destino
	
	SELECT ucrsDadosFacturacaoAssociados
	IF EMPTY(ucrsDadosFacturacaoAssociados.faturado)
		uf_perguntalt_chama("Ainda n�o emitiu Documentos de Fatura��o para o servi�o selecionado.","OK","",16)
	ELSE
		uf_OrigensDestinos_Chama('FTASSOCIADOS')
	ENDIF
ENDFUNC


**
FUNCTION uf_FATASSOCIADOS_sair
	FATASSOCIADOS.hide
	FATASSOCIADOS.release
ENDFUNC

