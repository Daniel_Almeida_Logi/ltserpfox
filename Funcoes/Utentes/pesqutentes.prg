**
FUNCTION uf_PESQUTENTES_Chama
	LPARAMETERS tcOrigem, tcNome, tnContrib

	PUBLIC TokenPlafCentralx3, contadorPLFCT

	TokenPlafCentralx3=''
	contadorPLFCT = 10

	IF !uf_gerais_validaPermUser(ch_userno, ch_grupo, 'Gest�o de Clientes')
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ACEDER AO PAINEL GEST�O DE CLIENTES.","OK","",48)
		RETURN .f.
	ENDIF
	
	IF EMPTY(tcOrigem) OR !TYPE("tcOrigem")=="C"
		uf_perguntalt_chama("FUN��O: uf_PESQUTENTES_chama. PAR�METRO N�O V�LIDO","OK","", 16)
		RETURN .f.
	ELSE
		PUBLIC myOrigemPPCl, myOrderPesquisarCl
		myOrigemPPCl = tcOrigem
	ENDIF
	
	
	
	regua(0,3,"A carregar painel...")
	regua(1,1,"A carregar painel...")
	
	** valida permiss�es
	IF !(uf_gerais_validaPermUser(ch_userno, ch_grupo, 'Gest�o de Clientes - Visualizar'))
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE CONSULTAR CLIENTES.","OK","", 48)
		regua(2)
		RETURN .f.
	ENDIF
	
	regua(1,2,"A carregar painel...")	
		
	** cursor de pesquisa
	IF !USED("uCrsPesquisarCL")
		uf_PESQUTENTES_carregaDados(.t.)
	ELSE
				
		** coloca todos como n�o seleccionados
		SELECT uCrsPesquisarCl
		replace ALL uCrsPesquisarCL.sel WITH .F.
		
		IF tcOrigem == "ATENDIMENTO" OR tcOrigem == "PAGAMENTO" OR tcOrigem == "DOCASSOCIADOSCLIENTE" OR tcOrigem == "DOCASSOCIADOSCLIENTEFILTRO" OR tcOrigem == "ATENDIMENTOPSICO"  OR tcOrigem ="FATURACAOPSICO"
			SELECT uCrsPesquisarCl
			SET FILTER TO uCrsPesquisarCl.no > 199
		ENDIF 
		
		IF tcOrigem == "ENTIDADE"
			SELECT uCrsPesquisarCl
			SET FILTER TO uCrsPesquisarCl.clinica = .t.
		ENDIF
		
		SELECT uCrsPesquisarCl
		GO TOP
		
		IF tcOrigem == "PRESCRICAO"  && Evita erros com cursors diferentes
			SELECT uCrsPesquisarCl
			SET FILTER TO uCrsPesquisarCl.no < 0
		ENDIF 	
		
		IF myOrigemPPCl == "SMS" 
			select uCrsPesquisarCL
			set filter TO !EMPTY(uCrsPesquisarCL.tlmvl) AND uCrsPesquisarCL.autoriza_sms == .T. AND uCrsPesquisarCL.autorizado == .T.
		ENDIF 
		IF myOrigemPPCl == "EMAIL"	
			select uCrsPesquisarCL
			set filter to !EMPTY(uCrsPesquisarCL.email) AND uCrsPesquisarCL.autoriza_emails == .T. AND uCrsPesquisarCL.autorizado == .T.
		ENDIF 	
		
	ENDIF
	
	regua(1,3,"A carregar painel...")		
	
	IF TYPE("PESQUTENTES") == "U"
		DO FORM PESQUTENTES WITH tcNome, tnContrib
	ELSE
		IF TYPE("ATENDIMENTO") != "U"
			PESQUTENTES.show
		ENDIF 
	ENDIF
	
	PESQUTENTES.lockscreen = .t.
	
	** pagamento		
	IF tcOrigem == "PAGAMENTO"
	
		IF PesqUtentes.pageframe1.activepage == 1
			WITH PESQUTENTES.PageFrame1.page1
				.nome.value = ''
				.nome.setfocus
			ENDWITH 
		ELSE
			WITH PESQUTENTES.PageFrame1.page2
				.nome.value = ''
				.nome.setfocus
			ENDWITH 
		ENDIF
			
	ENDIF
	


	** Cartao Cidadao
	IF tcOrigem == "ATENDIMENTO"
		lcSQL = ''
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			exec up_utentes_dadosCC '<<ALLTRIM(myClientName)>>','<<ALLTRIM(mysite)>>', 1
		ENDTEXT 
		uf_gerais_actGrelha("", "uCrsDadosCartaoCliente", lcSQL)
		
		**
		IF RECCOUNT("uCrsDadosCartaoCliente") > 0

			SELECT uCrsPesquisarCl
			GO TOP
			SCAN
				DELETE 
			ENDSCAN
			
			SELECT uCrsDadosCartaoCliente
			IF PesqUtentes.pageframe1.activepage == 1
				WITH PESQUTENTES.PageFrame1.page1
**					.nome.value = STRTRAN(ALLTRIM(uCrsDadosCartaoCliente.nome), ' ' , '%')
					.nome.value = ALLTRIM(uCrsDadosCartaoCliente.nome)
					.ncont.value = ALLTRIM(uCrsDadosCartaoCliente.nif)
					.utenteno.value = ALLTRIM(uCrsDadosCartaoCliente.nrSNS)
					.txtNascimento.value = uf_gerais_getdate(uCrsDadosCartaoCliente.nascimento,"Date")
				ENDWITH 
			ELSE
				WITH PESQUTENTES.PageFrame1.page2
**					.nome.value = STRTRAN(ALLTRIM(uCrsDadosCartaoCliente.nome), ' ' , '%')
					.nome.value = ALLTRIM(uCrsDadosCartaoCliente.nome)
					.ncont.value = ALLTRIM(uCrsDadosCartaoCliente.nif)
					.utenteno.value = ASTR(uCrsDadosCartaoCliente.nrSNS)
					.txtNascimento.value = uf_gerais_getDate(uCrsDadosCartaoCliente.nascimento,"Date")
				ENDWITH 
			ENDIF

			uf_PESQUTENTES_carregaDados()	
			
		ENDIF 
		
		IF USED("uCrsDadosCartaoCliente")
			fecha("uCrsDadosCartaoCliente")
		ENDIF 
		
	ENDIF 
	
	IF uf_gerais_getParameter_site('ADM0000000126', 'BOOL', mySite) = .t.
		UPDATE uCrsPesquisarCL SET nrcartao = '******' WHERE len(alltrim(nrcartao))>0
	ENDIF 
	
	
	PESQUTENTES.lockscreen = .f.
	regua(2)

ENDFUNC


**
FUNCTION uf_PESQUTENTES_carregaMenu
	WITH PESQUTENTES.menu1
		**.adicionaOpcao("aplicacoes", "Aplica��es", myPath + "\imagens\icons\aplicacoes_w.png", "uf_gerais_menuAplicacoes With 'PESQUTENTES'","")
		.adicionaOpcao("opcoes", "Op��es", myPath + "\imagens\icons\opcoes_w.png", "","F1")
		.adicionaOpcao("actualizar","Pesquisar",myPath + "\imagens\icons\lupa_w.png","uf_PESQUTENTES_carregaDados with .f.", "F2")
		.adicionaOpcao("novo", "Novo", myPath + "\imagens\icons\mais_w.png", "uf_PESQUTENTES_criarFicha","F3")			
		.adicionaOpcao("alteraNome","N/ Criar Fich.",myPath + "\imagens\icons\ut_lapis_w.png","uf_PESQUTENTES_alteraNome","F4")				
		
		.adicionaOpcao("detalhe","Detalhe",myPath + "\imagens\icons\detalhe.png","uf_PESQUTENTES_detalhe","F5")
		**.adicionaOpcao("subir", "Subir", myPath + "\imagens\icons\pg_up_b.png", "uf_gerais_MovePage with .t.,'PESQUTENTES.menu1','uCrsPesquisarCL'", "F6")
		**.adicionaOpcao("descer", "Descer", myPath + "\imagens\icons\pg_down_b.png", "uf_gerais_MovePage with .f.,'PESQUTENTES.menu1','uCrsPesquisarCL'", "F7")
		.adicionaOpcao("selTodos","Sel. Todos",myPath + "\imagens\icons\unchecked_w.png","uf_PESQUTENTES_selTodos", "F8")
		.adicionaOpcao("limpar","Limpar",myPath + "\imagens\icons\limpar_b2.png","uf_PESQUTENTES_limpar", "F9")

		.adicionaOpcao("criarFicha","Criar Ficha",myPath + "\imagens\icons\ut_mais_w.png","uf_PESQUTENTES_criarFicha","F7")
		.adicionaOpcao("tecladoVirtual","Teclado",myPath + "\imagens\icons\teclado_w.png","uf_PESQUTENTES_tecladoVirtual","E")	

        IF uf_gerais_getParameter_site('ADM0000000143', 'BOOL', mySite)

            IF PESQUTENTES.pageframe1.activepage = 1
                .adicionaOpcao("sinc_RNU","Sinc RNU",myPath + "\imagens\icons\SincRNU_W.png","uf_utentes_sincronizarRNU WITH PESQUTENTES.pageframe1.page1.utenteno.value, PESQUTENTES.pageframe1.page2.ncont.value, 'PESQUTENTES'", "F10")
            ELSE
                .adicionaOpcao("sinc_RNU","Sinc RNU",myPath + "\imagens\icons\sincRNU_W.png","uf_utentes_sincronizarRNU WITH PESQUTENTES.pageframe1.page2.utenteno.value, PESQUTENTES.pageframe1.page2.ncont.value,'PESQUTENTES'", "F10")
            ENDIF

        ENDIF
		
		IF uf_gerais_getParameter("ADM0000000120","BOOL") == .t.
			.adicionaOpcao("sinc","Sinc. Ut.",myPath + "\imagens\icons\ut_w.png","uf_SincClientes_chama","U")
		ENDIF
	ENDWITH
	
	&& configura menu de Aplica��es
	**WITH PESQUTENTES.menu_aplicacoes
		**.adicionaOpcao("faturacao", "Documentos Fatura��o", "", "uf_facturacao_chama with ''")
		**.adicionaOpcao("recebimentos", "Recebimentos", "", "uf_utentes_chamaRecebimentos with .t.")
		**.adicionaOpcao("cartaoCliente", "CRM", "", "uf_cartaocliente_chama with '',''")
		**.adicionaOpcao("sms", "Comunica��es", "", "uf_SMS_chama with ''")
		**.adicionaOpcao("arquivodigital", "Arquivo Digital", "", "uf_arquivodigital_chama")
		&&.adicionaOpcao("promocoes", "Promo��es", "", "uf_perguntalt_chama with 'Brevemente dispon�vel.','OK','',64")
		**.adicionaOpcao("prescricao", "Prescri��o", "", "uf_utentes_prescMed with .t.")
		**.adicionaOpcao("marcacoes", "Marca��es", "", "uf_marcacoes_chama")
		**.adicionaOpcao("ftassociados", "Ft. Autom�tica", "", "uf_PESQUTENTES_FATASSOCIADOS_chama")
		**IF uf_gerais_getParameter('ADM0000000082', 'TEXT') == "CLINICA"
		**	.adicionaOpcao("convencoes", "Conven��es", "", "uf_pesqconvencoes_chama")
		**ENDIF
		
		&&.adicionaOpcao("calculadora", "Calculadora", "", "uf_calculadora_chama")
*!*			IF uf_gerais_getParameter("ADM0000000224","BOOL") == .t. && Clinica
*!*				.adicionaOpcao("facturacaoEntidadesClinica","Emiss�o Fatura��o","","uf_FACTENTIDADESCLINICA_chama with 'PESQUTENTES'")
*!*				.adicionaOpcao("honorarios","Emiss�o Honorarios","","uf_HONORARIOS_chama with 'PESQUTENTES'")
*!*			ENDIF 
	**ENDWITH

	&& configura menu de op��es
	WITH PESQUTENTES.menu_opcoes
		.adicionaOpcao("pesqAvancada","Pesquisa Avan�ada", "", "uf_PESQUTENTES_pesqAvancada")
		.adicionaOpcao("Imprimir","Imprimir Dados","","uf_PESQUTENTES_carregaDados WITH .f.,.t.")
		.adicionaOpcao("ImpCart","Imprimir Cart�o","","uf_PESQUTENTES_imprimirCartao")
		
		.adicionaOpcao("faturacao", "Documentos Fatura��o", "", "uf_facturacao_chama with ''")
		.adicionaOpcao("recebimentos", "Recebimentos", "", "uf_utentes_chamaRecebimentos with .t.")
		.adicionaOpcao("cartaoCliente", "CRM", "", "uf_cartaocliente_chama with '',''")
		.adicionaOpcao("sms", "Comunica��es", "", "uf_SMS_chama with ''")
		.adicionaOpcao("arquivodigital", "Arquivo Digital", "", "uf_arquivodigital_chama")
		
		.adicionaOpcao("prescricao", "Prescri��o", "", "uf_utentes_prescMed with .t.")
		.adicionaOpcao("marcacoes", "Marca��es", "", "uf_marcacoes_chama")
		.adicionaOpcao("ftassociados", "Ft. Autom�tica", "", "uf_PESQUTENTES_FATASSOCIADOS_chama")
		IF uf_gerais_getParameter('ADM0000000082', 'TEXT') == "CLINICA"
			.adicionaOpcao("convencoes", "Conven��es", "", "uf_pesqconvencoes_chama")
		ENDIF
	ENDWITH
	
	uf_PESQUTENTES_alternaMenu()
ENDFUNC


**
FUNCTION uf_PESQUTENTES_detalhe
	IF !myClIntroducao AND !myClAlteracao
		SELECT uCrsPesquisarCL
		uf_utentes_chama(uCrsPesquisarCL.no,uCrsPesquisarCL.estab)
		uf_PESQUTENTES_sair()
		utentes.show
	ELSE
		uf_perguntalt_chama("O ECR� DE UTENTES ENCONTRA-SE EM MODO DE INTRODU��O OU EDI��O","OK","", 48)
	ENDIF
ENDFUNC


**
FUNCTION uf_PESQUTENTES_alternaMenu
	DO CASE
		CASE myOrigemPPCl == "ATENDIMENTO" OR myOrigemPPCl == "ATENDIMENTOPSICO"
            
			PESQUTENTES.menu1.estado("alteraNome, Novo", "SHOW")
            IF uf_gerais_getParameter_site('ADM0000000143', 'BOOL', mySite)
                PESQUTENTES.menu1.estado("sinc_RNU", "SHOW")
            ENDIF
			PESQUTENTES.menu1.estado("selTodos, opcoes, tecladoVirtual, criarFicha", "HIDE")
		CASE myOrigemPPCl == "PAGAMENTO"
			**PESQUTENTES.pageframe1.page2.numero.value = ALLTRIM(STR(pagamento.pgFRVDP.page1.txtNoEF.value))
			PESQUTENTES.menu1.actualizar.click()				
			PESQUTENTES.menu1.estado("selTodos, opcoes, tecladoVirtual, Novo, alteraNome, criarFicha, detalhe, limpar", "HIDE")
            IF uf_gerais_getParameter_site('ADM0000000143', 'BOOL', mySite)
                PESQUTENTES.menu1.estado("sinc_RNU", "HIDE")
            ENDIF
			select uCrsPesquisarCL
			set filter to uCrsPesquisarCL.entFact == .T.		
		CASE myOrigemPPCl == "CRIARCLATEND"
			IF USED("uCrsPesquisarCL")
				SELECT uCrsPesquisarCL
				DELETE ALL
			ENDIF 
			PESQUTENTES.pageframe1.page2.estab.value = '0'
			PESQUTENTES.pageframe1.page2.estab.readonly = .t.

			PESQUTENTES.menu1.estado("selTodos, criarFicha, alteraNome, Novo", "HIDE")
            IF uf_gerais_getParameter_site('ADM0000000143', 'BOOL', mySite)
			    PESQUTENTES.menu1.estado("sinc_RNU", "HIDE")
            ENDIF
		CASE myOrigemPPCl == "SMS" OR myOrigemPPCl == "EMAIL" OR myOrigemPPCl == "PROMOCOES"
			PESQUTENTES.menu1.estado("alteraNome ,criarFicha , tecladovirtual ,Novo", "HIDE", "OK", .t.)
            IF uf_gerais_getParameter_site('ADM0000000143', 'BOOL', mySite)
            	PESQUTENTES.menu1.estado("sinc_RNU", "HIDE", "OK", .t.)
            ENDIF
		CASE myOrigemPPCl == "PRESCRICAO";
			OR myOrigemPPCl == "MARCACOES_1";
			OR myOrigemPPCl == "MARCACOES_2";
			OR myOrigemPPCl == "HONORARIOS";
			OR myOrigemPPCl == "HONORARIOS_ENTIDADE";
			OR myOrigemPPCl == "MARCACOES_COMP"
			
			PESQUTENTES.menu1.estado("selTodos, alteraNome, tecladoVirtual, opcoes, Novo", "HIDE")
            IF uf_gerais_getParameter_site('ADM0000000143', 'BOOL', mySite)
                PESQUTENTES.menu1.estado("sinc_RNU", "HIDE")
            ENDIF
			
		CASE myOrigemPPCl = "CARTAO_CLIENTE"
			PESQUTENTES.menu1.estado("selTodos, alteraNome, tecladoVirtual, opcoes, Novo", "HIDE")
            IF uf_gerais_getParameter_site('ADM0000000143', 'BOOL', mySite)
                PESQUTENTES.menu1.estado("sinc_RNU", "HIDE")
            ENDIF
		
		CASE myOrigemPPCl == "CAMPANHAS" OR myOrigemPPCl == "REBATIMENTOS" OR myOrigemPPCl == "MORADA"
			PESQUTENTES.menu1.estado("alteraNome, criarFicha, detalhe, tecladoVirtual, selTodos, Novo, limpar", "HIDE", "Aplicar", .t.,"Sair",.t.)
            IF uf_gerais_getParameter_site('ADM0000000143', 'BOOL', mySite)
			    PESQUTENTES.menu1.estado("sinc_RNU", "HIDE", "Aplicar", .t.,"Sair",.t.)
            ENDIF
		CASE  myOrigemPPCl == "FACTURACAO" OR myOrigemPPCl == "FACTURACAOPSICO" 
			PESQUTENTES.menu1.estado("criarFicha, detalhe, tecladoVirtual, selTodos, limpar", "HIDE")
            IF uf_gerais_getParameter_site('ADM0000000143', 'BOOL', mySite)
                PESQUTENTES.menu1.estado("sinc_RNU", "HIDE")
            ENDIF
		OTHERWISE
			PESQUTENTES.menu1.estado("alteraNome, criarFicha, detalhe, tecladoVirtual, selTodos, limpar", "HIDE")
            IF uf_gerais_getParameter_site('ADM0000000143', 'BOOL', mySite)
                PESQUTENTES.menu1.estado("sinc_RNU", "HIDE")
            ENDIF
	ENDCASE 
	
	IF myOrigemPPCl == "pcentral"
		PESQUTENTES.menu1.estado("detalhe", "HIDE")
	ENDIF
	
	**IF UPPER(ALLTRIM(uCrsE1.tipoempresa)) != "CLINICA"
		**PESQUTENTES.menu_aplicacoes.estado("prescricao", "HIDE")
	**ENDIF 
	
	**IF uf_gerais_getParameter_site('ADM0000000067', 'BOOL', mySite) = .t. AND ALLTRIM(ch_grupo) <> 'Administrador' AND ALLTRIM(ch_grupo) <> 'Supervisores'
	**	PESQUTENTES.menu1.estado("opcoes, aplicacoes", "HIDE")
	**ENDIF 
ENDFUNC


** Funcao pesquisa Utentes
FUNCTION uf_PESQUTENTES_carregaDados
	LPARAMETERS tcBool, tcReport
	
	** para limpar o cursor para evitar que sem clicar no pesquisar estejam dados 
	IF USED("uCrsPesquisarCl")
		SELECT uCrsPesquisarCl
		GO TOP
		SCAN
			DELETE 
		ENDSCAN
	ENDIF
	
	IF tcBool
		IF !USED("uCrsPesquisarCL")	
			LOCAL lcSql
			lcSql = ""
	
			DO CASE 
				CASE myOrigemPPCl == "PRESCRICAO" 
					TEXT TO lcSQL TEXTMERGE noshow
						exec up_utentes_pesquisa '','','','',''
					ENDTEXT
				CASE myOrigemPPCl == "MARCACOES_COMP" 
					TEXT TO lcSQL TEXTMERGE noshow
						exec up_clientes_pesquisa 0, '', 0, -1, '', '', '', '', '', '', 0, '=' , -999999, '', 0, '', '', '', '19000101', '19000101', '19000101', 0, '', '', '', 1,1, '', '', '', '', '',0,0
					ENDTEXT
				OTHERWISE
					TEXT TO lcSQL TEXTMERGE noshow
						exec up_clientes_pesquisa 0, '', 0, -1, '', '', '', '', '', '', 0, '=' , -999999, '', 0, '', '', '', '19000101', '19000101', '19000101', 0, '', '', '', 0,0, '', '', '', '', '',0,0
					ENDTEXT
			ENDCASE 
			

			uf_gerais_actGrelha("", "uCrsPesquisarCL", lcSQL)
		ENDIF

	ELSE
		regua(0,3,"A PESQUISAR...",.F.)
			
			DO CASE
				CASE myOrigemPPCl == "PRESCRICAO"  
				
					WITH PESQUTENTES.pageframe1.page3
							TEXT TO lcSQL TEXTMERGE noshow
								exec up_utentes_pesquisa '','<<ALLTRIM(.nome.value)>>','<<ALLTRIM(.nutente.value)>>','<<ALLTRIM(.nbenef.value)>>','<<ALLTRIM(.ncont.value)>>'
							ENDTEXT
						uf_gerais_actGrelha("PESQUTENTES.grdPesq", "uCrsPesquisarCL", lcSQL)		
					ENDWITH
							
					PESQUTENTES.lblRegistos.caption = ALLTRIM(STR(RECCOUNT("uCrsPesquisarCL"))) + ' Registos'
					
					&& posic�o do cursor no form caso hajam resultados ou n�o
					SELECT uCrsPesquisarCL
					IF RECCOUNT("uCrsPesquisarCL") > 0
						PesqUtentes.grdPesq.sel.setfocus
					ELSE
						PesqUtentes.pageframe1.page3.nome.setfocus							
					ENDIF	
					
				OTHERWISE
					LOCAL lcSQL, lcTop, lcNome, lcNo, lcEstab, lcMorada, lcNcont, lcNCartao, lcTipo, lcProfissao, lcSexo, lcNascimento, lcMarca
					LOCAL lcTlmvl, lcOperador, lcIdade, lcObs, lcMail, lcEFR, lcPatologia, lcEntre, lcE, lcRef, lcNId
					LOCAL lcInactivo, lctlf, lcutenteno, lcnbenef, lcValTouch, lcValEntidadeClinica, lcID, lcPim,lcPem
					
					** var defaults
					STORE '' TO lcSQL, lcNome, lcMorada, lcNcont, lcNCartao, lcTipo, lcProfissao, lcSexo, lcMarca
					STORE '' TO lcObs, lcEFR, lcPatologia, lcRef, lctlf, lcutenteno, lcnbenef,lcID, lcNId
					STORE 0 TO lcNo, lcTlmvl, lcMail, lcInactivo, lcValTouch, lcValEntidadeClinica, lcPim, lcPem
					STORE '19000101' TO lcNascimento, lcEntre

					lcEstab 		= -1
					lcOperador 		= "="
					lcIdade			= -999999
					lcE				= '30000101'
					
					regua(1,1,"A PESQUISAR...",.F.)
					
					** validar se front-office ou back-oficce
					LOCAL lcObj
					
					IF uf_gerais_getEstadoTouch() == .t. 
						IF ALLTRIM(myOrigemPPCl) == "CARTAO_CLIENTE_MANIPULACAO" OR ALLTRIM(myOrigemPPCl) == "CARTAO_CLIENTE" OR ALLTRIM(myOrigemPPCl) == "VACINACAO" OR ALLTRIM(myOrigemPPCl) == "REGVENDAS"  OR ALLTRIM(myOrigemPPCl) == "MEDICAMENTOHOSPITALAR" OR TYPE("avisochamada") != "U" ;
                            OR ALLTRIM(myOrigemPPCl) == "SINAVE"
							lcObj = "PESQUTENTES.pageframe1.page1"
							lcId = ALLTRIM(&lcObj..id.value)
						ELSE
							lcObj = "PESQUTENTES.pageframe1.page2"
						ENDIF
					ELSE
						lcObj = "PESQUTENTES.pageframe1.page1"
						lcId = ALLTRIM(&lcObj..id.value)
					ENDIF
					
                    

					** Atribui parametros de pesquisa				
					** campos dispon�veis em front-office e back-office
					lcTop 		= IIF(INT(VAL(&lcObj..topo.value))== 0,100000,INT(VAL(&lcObj..topo.value)))
					lcNome		= STRTRAN(ALLTRIM(&lcObj..nome.value), ' ', '%')
					lcNo		= IIF(EMPTY(&lcObj..numero.value), 0, STRTRAN(&lcObj..numero.value,' ',''))
					lcEstab		= IIF(EMPTY(&lcObj..estab.value), -1, &lcObj..estab.value)
					lcMorada	= ALLTRIM(&lcObj..morada.value)
					lcNcont		= ALLTRIM(&lcObj..ncont.value)
					lcNCartao	= ALLTRIM(&lcObj..nCartao.value)
					lctlf		= ALLTRIM(&lcObj..tlf.value)
					IF EMPTY(lctlf)
						lctlf = ALLTRIM(PESQUTENTES.Pageframe1.page1.tlf.value)
					ENDIF 
					lcutenteno	= ALLTRIM(&lcObj..utenteno.value)
					lcnbenef	= ALLTRIM(PESQUTENTES.pageframe1.page1.nbenef.value)
					lcTipo		= ALLTRIM(&lcObj..cmbTipo.value)
					lcProfissao	= ALLTRIM(&lcObj..profissao.value)
					lcSexo		= IIF(&lcObj..sexo.value == 'M/F', '', &lcObj..sexo.value)
					lcOperador	= ALLTRIM(&lcObj..OperadorLogico.value)
					lcInactivo	= IIF(&lcObj..image4.tag == "true", 1, 0)
					lcNId		= ALLTRIM(&lcObj..NId.value)

					IF ALLTRIM(UPPER(lcObj)) = "PESQUTENTES.PAGEFRAME1.PAGE2"
						lcCodPost = ALLTRIM(&lcObj..codPost.value)
					ELSE
						lcCodPost = ''
					ENDIF

					IF &lcObj..label6.caption == 'Idade'
						IF EMPTY(&lcObj..idade.value)
							lcIdade	= -999999
							lcOperador = '>'
						ELSE
							lcIdade	= astr(&lcObj..idade.value)
							lcNascimento = '19000101'
						ENDIF
					ENDIF

					IF &lcObj..label6.caption == 'Dt. Nasc.'
						lcNascimento = IIF(EMPTY(ALLTRIM(&lcObj..txtnascimento.value)), '19000101', uf_gerais_getDate(&lcObj..txtnascimento.value, "SQL"))
						lcNascimentoReport = IIF(EMPTY(ALLTRIM(&lcObj..txtnascimento.value)), '1900.01.01', uf_gerais_getDate(&lcObj..txtnascimento.value))
						lcIdade	= -999999
						lcOperador = '>'
					ELSE
						lcNascimento = '19000101'
						lcNascimentoReport = '1900.01.01'
					ENDIF 

					** Campos n�o dispon�veis em front-office
					IF uf_gerais_getEstadoTouch() == .f.
						lcMail	 	= IIF(&lcObj..image2.tag == "true", 1, 0)
						lcTlmvl 	= IIF(&lcObj..image3.tag == "true", 1, 0)
						lcPim	 	= IIF(&lcObj..image5.tag == "true", 1, 0)
						lcPem 		= IIF(&lcObj..image6.tag == "true", 1, 0)
						lcObs		= ALLTRIM(&lcObj..observacoes.value)
						lcEFR		= ALLTRIM(&lcObj..efr.value)
						
						** patologias
						lcPatologia	= ALLTRIM(&lcObj..patologias.value)

						** produto
*!*							IF USED("uCrsProdutosCLI")
*!*								SELECT uCrsProdutosCLI
*!*								GO TOP
*!*								SCAN
*!*									lcRef = lcRef + astr(uCrsProdutosCLI.ref) + '|'
*!*								ENDSCAN
*!*							ENDIF 
						
						lcRef =''
						
						IF USED("uCrsProdutosCLI")
							IF RECCOUNT("uCrsProdutosCLI") >0
						
								LOCAL lctokenTempFilter, lcValueText, lcFieldToAply, lcSite, lcSplit, lcMaxInsert    
								lctokenTempFilter 	= uf_gerais_gerarIdentifiers(36)
								lcValueText 		= 0
								lcFieldToAply		= "REFERENCIA"
								lcSite 				= mysite
								lcSplit 			= ','
								lcMaxInsert 		= 200 
								
								SELECT uCrsProdutosCLI
								GO TOP
								SCAN
									uf_gerais_insert_tempFilters(lctokenTempFilter ,uCrsProdutosCLI.ref, 0, lcFieldToAply, lcSite, lcSplit, lcMaxInsert)
								ENDSCAN
								
								lcref = lctokenTempFilter
							ENDIF
						ENDIF
					
						** marca 
						IF TYPE("MyMarcaAux") = "U"
							**
						ELSE
						   lcMarca = ALLTRIM(MyMarcaAux)
						ENDIF 
					
						IF !EMPTY(lcRef) OR !EMPTY(lcMarca)
							&& retirar pipe no final
							lcRef=substr(lcRef, 1, len(lcRef)-1)
								
							IF &lcObj..image1.tag == "false" AND (&lcObj..txtEntre.value=='' OR &lcObj..txtE.value == '')
								regua(2)
								uf_perguntalt_chama('TEM QUE SELECCIONAR UM PER�ODO NO QUAL OS PRODUTOS FORAM ADQUIRIDOS.',"OK","",48)
								RETURN .f.
							ENDIF
						ENDIF 
								
						** Periodo de tempo
						lcEntre		= IIF(EMPTY(ALLTRIM(&lcObj..txtEntre.value)), '19000101', uf_gerais_getDate(&lcObj..txtEntre.value, "SQL"))
						lcE			= IIF(EMPTY(ALLTRIM(&lcObj..txtE.value)), '30000101', uf_gerais_getDate(&lcObj..txtE.value, "SQL"))
						
						lcEntreReport	= IIF(EMPTY(ALLTRIM(&lcObj..txtEntre.value)), '1900.01.01', uf_gerais_getDate(&lcObj..txtEntre.value))
						lcEReport		= IIF(EMPTY(ALLTRIM(&lcObj..txtE.value)), '3000.01.01', uf_gerais_getDate(&lcObj..txtE.value))
	
					ENDIF
					
					IF (myOrigemPPCl == "ATENDIMENTO") OR myOrigemPPCl == "ATENDIMENTOPSICO" OR myOrigemPPCl == "PAGAMENTO";
						OR  myOrigemPPCl == "RESUMOASSOCIADOSCLIENTE" OR myOrigemPPCl == "DOCASSOCIADOSCLIENTE" OR myOrigemPPCl == "DOCASSOCIADOSCLIENTEFILTRO" OR ALLTRIM(myOrigemPPCl) == "VACINACAO" OR ALLTRIM(myOrigemPPCl) == "REGVENDAS" OR ALLTRIM(myOrigemPPCl) == "MEDICAMENTOHOSPITALAR";
						OR  myOrigemPPCl == "SINAVE"
						lcValTouch = 1
					ENDIF 
					
				
							
					
					IF myOrigemPPCl == "ENTIDADE";
						OR myOrigemPPCl == "FACTURACAOENTIDADES_ENTIDADE";
						OR myOrigemPPCl == "HONORARIOS";
						OR myOrigemPPCl == "HONORARIOS_ENTIDADE";
						OR myOrigemPPCl == "MARCACOES_COMP"
												
						lcValEntidadeClinica = 1
					ENDIF 
							
					
					LOCAL lcAutorizaSms, lcAutorizaEmail
					STORE 0 TO lcAutorizaSms, lcAutorizaEmail
					** Se for origin�rio do envio de comunica��es s� aparecem quem tem os dados preenchidos
					IF myOrigemPPCl == "SMS" 
						lcTlmvl = 1	
						lcAutorizaSms = 1
					ENDIF 
					IF myOrigemPPCl == "EMAIL" 
						lcMail= 1	
						lcAutorizaEmail = 1
					ENDIF 

					uv_eMail = ''
	
					IF PESQUTENTES.PageFrame1.activepage = 2
						uv_eMail = Alltrim(PESQUTENTES.PageFrame1.page2.email.value)
					ENDIF

					regua(1,2,"A PESQUISAR...",.F.)
					IF EMPTY(tcReport)
						lctlf		= ALLTRIM(&lcObj..tlf.value)
						
						lcSQL = ''
						IF &lcObj..chkContido.tag == "true"
							TEXT TO lcSQL TEXTMERGE NOSHOW	
								exec up_clientes_pesquisa <<lcTop>>, '<<lcNome>>', <<lcNo>>, <<lcEstab>>, '<<lcMorada>>', '<<lcNcont>>'
														,'<<lcNCartao>>', '<<lcTipo>>', '<<lcProfissao>>'
														,'<<lcSexo>>', <<lcTlmvl>>, '<<lcOperador>>', <<lcIdade>>, '<<lcObs>>', <<lcMail>>, '<<lcEFR>>'
														,'<<lcPatologia>>', '<<lcRef>>', '<<lcEntre>>', '<<lcE>>', '<<lcNascimento>>', <<lcInactivo>>
														,'<<lctlf>>', '<<lcutenteno>>', '<<lcnbenef>>', <<lcValTouch>>,<<lcValEntidadeClinica>>, '<<lcId>>', '<<lcMarca>>', '<<lcNid>>', '<<uv_eMail>>', '<<lcCodPost>>', <<lcAutorizaEmail>>, <<lcAutorizaSms>>, <<lcPim>>, <<lcPem>>
							ENDTEXT			
						ELSE
							TEXT TO lcSQL TEXTMERGE NOSHOW	
								exec up_clientes_pesquisa_comecado <<lcTop>>, '<<lcNome>>', <<lcNo>>, <<lcEstab>>, '<<lcMorada>>', '<<lcNcont>>'
														,'<<lcNCartao>>', '<<lcTipo>>', '<<lcProfissao>>'
														,'<<lcSexo>>', <<lcTlmvl>>, '<<lcOperador>>', <<lcIdade>>, '<<lcObs>>', <<lcMail>>, '<<lcEFR>>'
														,'<<lcPatologia>>', '<<lcRef>>', '<<lcEntre>>', '<<lcE>>', '<<lcNascimento>>', <<lcInactivo>>
														,'<<lctlf>>', '<<lcutenteno>>', '<<lcnbenef>>', <<lcValTouch>>,<<lcValEntidadeClinica>>, '<<lcId>>', '<<lcMarca>>', '<<lcNid>>', '<<uv_eMail>>', '<<lcCodPost>>', <<lcPim>>, <<lcPem>>
							ENDTEXT						
					
						ENDIF  
						uf_gerais_actGrelha("PESQUTENTES.grdPesq", "uCrsPesquisarCL", lcSQL)		
										
						regua(1,3,"A PESQUISAR...",.F.)
			
						LOCAL lcCountResultutentes
						** Devido ao filtros aplicados ao cursor n�o fazia a contagem correta devido aos eleminados 
						SELECT uCrsPesquisarCL  
						COUNT TO lcCountResultutentes FOR !DELETED()
						PESQUTENTES.lblRegistos.caption =   ALLTRIM(STR(lcCountResultutentes))+ ' Registos'

						** Coloca o Cursor na grelha ou objecto de pesquisa caso hajam resultados, ou n�o.
						SELECT uCrsPesquisarCL
						IF RECCOUNT("uCrsPesquisarCL") > 0 AND  myOrigemPPCl!='PAGAMENTO'
							PesqUtentes.grdPesq.sel.setfocus
						ELSE
							IF PesqUtentes.pageframe1.activepage = 1
								PesqUtentes.pageframe1.page1.nome.setfocus
							ELSE
								PesqUtentes.pageframe1.page2.nome.setfocus
							ENDIF
						ENDIF
					ELSE && imprime dados da pesquisa
						IF UPPER(ALLTRIM(uCrsE1.id_lt)) == 'E01322A'   
							lcReport = ''
							TEXT TO lcReport TEXTMERGE NOSHOW	
								&top=<<lcTop>>&nome=<<lcNome>>&no=<<lcNo>>&estab=<<lcEstab>>&morada=<<lcMorada>>&ncont=<<lcNcont>>&tipo=<<lcTipo>>&tlmvl=<<IIF(lcTlmvl==1,"true","false")>>&obs=<<lcObs>>&mail=<<IIF(lcMail==1,"true","false")>>&inactivo=<<IIF(lcInactivo==1,"true","false")>>&tlf=<<lctlf>>&site=<<mysite>>&id=<<lcId>>&pim=<<IIF(lcPim==1,"true","false")>>&pem=<<IIF(lcPem==1,"true","false")>>
							ENDTEXT		
							
							SELECT uCrsPesquisarCL
							GO TOP
								
							IF RECCOUNT("uCrsPesquisarCL") > 0
								uf_gerais_chamaReport("listagem_farmacias_afp", ALLTRIM(lcReport))
							ELSE
								uf_perguntalt_chama("N�o existem dados para Impress�o. Por favor verifique. Obrigado.","OK","",16)						
							ENDIF
						ELSE
							lcReport = ''
							TEXT TO lcReport TEXTMERGE NOSHOW	
								&top=<<lcTop>>&nome=<<lcNome>>&no=<<lcNo>>&estab=<<lcEstab>>&morada=<<lcMorada>>&ncont=<<lcNcont>>&ncartao=<<lcNCartao>>&tipo=<<lcTipo>>&Profissao=<<lcProfissao>>&sexo=<<lcSexo>>&tlmvl=<<IIF(lcTlmvl==1,"true","false")>>&operador=<<lcOperador>>&idade=<<lcIdade>>&obs=<<lcObs>>&mail=<<IIF(lcMail==1,"true","false")>>&entPla=<<lcEFR>>&patologia=<<lcPatologia>>&ref=<<lcRef>>&Entre=<<lcEntreReport>>&E=<<lcEReport>>&dtnasc=<<lcNascimentoReport>>&inactivo=<<IIF(lcInactivo==1,"true","false")>>&tlf=<<lctlf>>&nbenef=<<lcutenteno>>&nbenef2=<<lcnbenef>>&touch=false&entCompart=false&site=<<mysite>>&id=<<lcId>>&marca=<<lcMarca>>&pim=<<IIF(lcPim==1,"true","false")>>&pemh=<<IIF(lcPem==1,"true","false")>>
							ENDTEXT
								
							SELECT uCrsPesquisarCL
							GO TOP
								
							IF RECCOUNT("uCrsPesquisarCL") > 0
								uf_gerais_chamaReport("listagem_clientes_avancada", ALLTRIM(lcReport))
							ELSE
								uf_perguntalt_chama("N�o existem dados para Impress�o. Por favor verifique. Obrigado.","OK","",16)						
							ENDIF
						ENDIF
							
					ENDIF
			ENDCASE
		regua(2)
	ENDIF	
	
	IF uf_gerais_getParameter_site('ADM0000000126', 'BOOL', mySite) = .t.
		UPDATE uCrsPesquisarCL SET nrcartao = '******' WHERE len(alltrim(nrcartao))>0
	ENDIF 
	
	SELECT uCrsPesquisarCL 
	GO TOP 
	
ENDFUNC


**
FUNCTION uf_PESQUTENTES_selTodos
	LOCAL lcPos
	
	SELECT uCrsPesquisarCL
	lcPos = RECNO("uCrsPesquisarCL")
	
	IF PESQUTENTES.menu1.selTodos.lbl.caption == "Sel. Todos"
		SELECT uCrsPesquisarCL
		replace ALL uCrsPesquisarCL.sel WITH .t.

		PESQUTENTES.menu1.selTodos.config("De-Sel. Todos", myPath + "\imagens\icons\checked_w.png", "uf_PESQUTENTES_selTodos")
	ELSE	
		SELECT uCrsPesquisarCL
		replace ALL uCrsPesquisarCL.sel WITH .f.

		PESQUTENTES.menu1.selTodos.config("Sel. Todos", myPath + "\imagens\icons\unchecked_w.png", "uf_PESQUTENTES_selTodos")
	ENDIF
	
	SELECT uCrsPesquisarCL
	TRY
		GO lcPos
	CATCH
	ENDTRY
ENDFUNC


**
FUNCTION uf_PESQUTENTES_pesqAvancada
	LOCAL lcOp
	
	IF 	EMPTY(PESQUTENTES.pesqAvancada)
		PESQUTENTES.pesqAvancada 						= .t.
		PESQUTENTES.pageframe1.page1.efr.enabled 		= .f.
		PESQUTENTES.pageframe1.page1.patologias.enabled = .f.
		PESQUTENTES.pageframe1.page1.txtEntre.enabled 	= .t.
		PESQUTENTES.pageframe1.page1.txtE.enabled 		= .t.
		PESQUTENTES.shape18.top 						= PESQUTENTES.shape18.top + 106
		PESQUTENTES.grdPesq.top 						= PESQUTENTES.grdPesq.top + 106
		PESQUTENTES.grdPesq.height						= PESQUTENTES.grdPesq.height - 106
		PESQUTENTES.label3.top 							= PESQUTENTES.label3.top + 106
		PESQUTENTES.lblRegistos.top 					= PESQUTENTES.lblRegistos.top + 106
	ELSE
		PESQUTENTES.pesqAvancada 						= .f.
		PESQUTENTES.pageframe1.page1.efr.enabled 		= .t.
		PESQUTENTES.pageframe1.page1.patologias.enabled = .t.
		PESQUTENTES.pageframe1.page1.txtEntre.enabled 	= .t.
		PESQUTENTES.pageframe1.page1.txtE.enabled 		= .t.
		PESQUTENTES.shape18.top 						= PESQUTENTES.shape18.top - 106
		PESQUTENTES.grdPesq.top 						= PESQUTENTES.grdPesq.top - 106
		PESQUTENTES.grdPesq.height 						= PESQUTENTES.grdPesq.height + 106
		PESQUTENTES.label3.top 							= PESQUTENTES.label3.top - 106
		PESQUTENTES.lblRegistos.top 					= PESQUTENTES.lblRegistos.top - 106
	ENDIF
	
ENDFUNC


**
FUNCTION uf_PESQUTENTES_criarFicha
	IF myOrigemPPCl != "ATENDIMENTO" AND myOrigemPPCl != "ATENDIMENTOPSICO" 
		&&OR myOrigemPPCl == "MARCACOES_1" OR myOrigemPPCl == "MARCACOES_2" 
		uf_PESQUTENTES_sair()
		uf_utentes_chama(0,0)
		uf_utentes_novo()
	ELSE
		&& Verificar Se o Sistema Permite Criar Clientes
		IF uf_gerais_getParameter('ADM0000000120', 'BOOL')
			uf_perguntalt_chama("A CRIA��O/ALTERA��O/ELIMINA��O DE CLIENTES EST� BLOQUEADA NESTE SISTEMA.","OK","", 64)
			RETURN .f.
		ENDIF
		
		&& Valida permiss�o do utilizador
		IF !(uf_gerais_validaPermUser(m.ch_userno, m.ch_grupo, 'Gest�o de Clientes - Introduzir'))
			uf_perguntalt_chama("O SEU PERFIL N�O PERMITE CRIAR NOVOS CLIENTES.","OK","", 48)
			RETURN .f.
		ENDIF
		
		&& 
		lcCriouCliente = uf_PESQUTENTES_criaCliente()
		
		IF EMPTY(lcCriouCliente)
			RETURN .f.
		ENDIF 				
		uf_PESQUTENTES_sair()

		IF LEFT(ALLTRIM(ft.ncont),1) <> '5' AND LEFT(ALLTRIM(ft.ncont),1) <> '9' AND LEFT(ALLTRIM(ft.ncont),1) <> '6'
		
			** Chama painel de emiss�o de token
			lcSql = ""
			TEXT TO lcSQL NOSHOW TEXTMERGE 
				SELECT utstamp FROM b_utentes WHERE no=<<ft.no>> and estab=<<ft.estab>>
			ENDTEXT 			
			IF !uf_gerais_actGrelha("", "utstamp",lcSQL)
				MESSAGEBOX("DESCULPE, MAS OCORREU UM ERRO A VERIFICAR DADOS DOS SERVI�OS. POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
				RETURN .f.
			ENDIF
			
			IF RECCOUNT("utstamp")>0
				uf_enviotoken_chama(ALLTRIM(utstamp.utstamp), 'b_utentes')
			ENDIF 
		ENDIF
	
	
		IF  .NOT. USED("uCrsAtendCL")
        	IF  .NOT. uf_gerais_actgrelha("", 'uCrsAtendCL', 'set fmtonly on Exec up_touch_dadosCliente 200,0 set fmtonly off')
  				uf_perguntalt_chama("Ocorreu uma anomalia ao criar o cursor [UCRSATENDCL]. Por favor reinicie o Software.", "OK", "", 16)
 			ENDIF
 		ENDIF

	
		IF(USED("uCrsAtendCL"))
			SELECT uCrsAtendCL
			uf_utentes_autoriza_email_empresa(ALLTRIM(uCrsAtendCL.ncont),uCrsAtendCL.no,uCrsAtendCL.estab)
	
		ENDIF
					      
	ENDIF 
ENDFUNC


**
FUNCTION uf_PESQUTENTES_alteraNome	
	IF uf_gerais_getParameter("ADM0000000123","BOOL")
		uf_perguntalt_chama("N�O � PERMITIDO ALTERAR O NOME DO CLIENTE GEN�RICO.","OK","",64)
		RETURN .T.
	ENDIF
	

	&&valida se o nome � campo obrigatorio
	IF EMPTY(ALLTRIM(PESQUTENTES.pageframe1.page2.nome.value)) AND  EMPTY(uf_gerais_getParameter("ADM0000000276","BOOL"))
		uf_perguntalt_chama("Indique o nome a aplicar.","OK","",64)
		PESQUTENTES.pageframe1.page2.nome.setfocus
		RETURN .f.
	ENDIF


	IF  !EMPTY(PESQUTENTES.pageframe1.page2.ncont.value)
		IF (uf_gerais_NIF_chkdigit(ALLTRIM(PESQUTENTES.pageframe1.page2.ncont.value)) == .f. AND ALLTRIM(PESQUTENTES.pageframe1.page2.ncont.value) != ALLTRIM(uf_gerais_getParameter_site('ADM0000000075', 'text', mySite)))
			
			IF !uf_perguntalt_chama("N�mero de contribuinte inv�lido.","OK","Cancel",48)
				RETURN .f.
			ENDIF
					
		ENDIF		
	ENDIF

	IF uf_gerais_compStr(myOrigemPPCl , "FACTURACAOPSICO") 	
		IF (!USED("dadosPsico"))
			uf_perguntalt_chama("Ocorreu ao selecionar o cliente. Por favor contacte o suporte.","OK","",16)
			RETURN .f.	
		ENDIF
		uf_faturacao_selclientepsico(200, 0)
		uf_PESQUTENTES_sair()
		RETURN .T.
			*uf_atendimento_calcIdade()
	ENDIF


	IF myOrigemPPCl == "FACTURACAO"	
	
		LOCAL lcCOntribuinte
		IF(!USED("ft"))
			uf_perguntalt_chama("Ocorreu ao selecionar o cliente. Por favor contacte o suporte.","OK","",16)
			RETURN .f.	
		ENDIF
		
		lcCOntribuinte = uf_gerais_getparameter_site("ADM0000000075", "text", mysite)
		
		IF(EMPTY(ALLTRIM(PESQUTENTES.pageframe1.page1.nome.value)) AND EMPTY(ALLTRIM(PESQUTENTES.pageframe1.page1.ncont.value)))	
			SELECT ft
			REPLACE ft.nome WITH "CONSUMIDOR FINAL"			
			REPLACE ft.ncont WITH lcCOntribuinte 
		ELSE
	    	
	    	IF (!EMPTY(PESQUTENTES.pageframe1.page1.nome.value) AND EMPTY(ALLTRIM(PESQUTENTES.pageframe1.page1.ncont.value)) )
	    		SELECT ft
	    		REPLACE ft.nome WITH ALLTRIM(PESQUTENTES.pageframe1.page1.nome.value)
				REPLACE ft.ncont WITH lcCOntribuinte
			ELSE
				IF (EMPTY(PESQUTENTES.pageframe1.page1.nome.value) AND !EMPTY(ALLTRIM(PESQUTENTES.pageframe1.page1.ncont.value)) )
					SELECT ft
	    			REPLACE ft.nome WITH "CONSUMIDOR FINAL"	
					REPLACE ft.ncont WITH PESQUTENTES.pageframe1.page1.ncont.value
				ELSE
					SELECT ft
					REPLACE ft.nome WITH ALLTRIM(PESQUTENTES.pageframe1.page1.nome.value)
					REPLACE ft.ncont WITH PESQUTENTES.pageframe1.page1.ncont.value
				ENDIF
			ENDIF
	    	
		ENDIF
		
		SELECT ft
	    REPLACE ft.no WITH 200
				
		uf_PESQUTENTES_sair()
		RETURN .T.
	ENDIF

	*uf_atendimento_selCliente(200, 0, "ATENDIMENTO")
    IF !(TYPE("ATENDIMENTO") == "U")
        IF myOrigemPPCl == "ATENDIMENTO"
            uf_atendimento_selCliente(200, 0, "ATENDIMENTO")
        ELSE
            IF myOrigemPPCl == "ATENDIMENTOPSICO"
                uf_atendimento_selClientePsico(200, 0)
            ENDIF
        ENDIF
    ENDIF




	&&preenche consumidor final se vazio	
	IF(EMPTY(ALLTRIM(PESQUTENTES.pageframe1.page2.nome.value)))	
		PESQUTENTES.pageframe1.page2.nome.value = "CONSUMIDOR FINAL"
	ENDIF
	
    IF myOrigemPPCl == "ATENDIMENTO"
    	SELECT uCrsAtendCL
	    replace uCrsAtendCL.nome WITH ALLTRIM(PESQUTENTES.pageframe1.page2.nome.value)
    ELSE
        IF myOrigemPPCl == "ATENDIMENTOPSICO"
			SELECT uCrsAtendCL
            replace uCrsAtendCl.u_nmutavi WITH ALLTRIM(PESQUTENTES.pageframe1.page2.nome.value)
			replace uCrsAtendCl.u_moutavi WITH ALLTRIM(PESQUTENTES.pageframe1.page2.morada.value)
			IF !EMPTY(ALLTRIM(PESQUTENTES.pageframe1.page2.txtNascimento.value))
				replace uCrsAtendCl.u_dcutavi WITH CTOD(ALLTRIM(PESQUTENTES.pageframe1.page2.txtNascimento.value))
			ENDIF
			SELECT uCrsAtendCL
			replace uCrsAtendCl.u_ndutavi WITH ALLTRIM(PESQUTENTES.pageframe1.page2.nid.value)

			uf_atendimento_calcIdade()
        ENDIF
    ENDIF
	IF !EMPTY(PESQUTENTES.pageframe1.page2.ncont.value) AND myOrigemPPCl == "ATENDIMENTO"
		SELECT uCrsAtendCL
		replace uCrsAtendCL.ncont WITH PESQUTENTES.pageframe1.page2.ncont.value
	ENDIF

    IF myOrigemPPCl == "ATENDIMENTO"

        SELECT FT
        replace FT.nome WITH ALLTRIM(PESQUTENTES.pageframe1.page2.nome.value)
        IF !EMPTY(PESQUTENTES.pageframe1.page2.ncont.value)
            replace FT.ncont WITH PESQUTENTES.pageframe1.page2.ncont.value
        ENDIF

        WITH Atendimento.pgfDados.page1
            && cliente generico com nome diferente (sem ficha criada)
            if ft.no == 200 AND UPPER(ALLTRIM(ft.nome)) != "CONSUMIDOR FINAL"
                .txtNome.disabledbackcolor	= RGB(241,196,15)
            ELSE
                .txtNome.disabledbackcolor	= RGB(255,255,255)
            ENDIF
        ENDWITH	
        
        atendimento.refresh()
        
    ELSE

        WITH Atendimento.pgfDados.page1
            && cliente generico com nome diferente (sem ficha criada)
            if ft.no == 200 AND UPPER(ALLTRIM(ft.nome)) != "CONSUMIDOR FINAL"
                .txtNomeAdquirente.disabledbackcolor	= RGB(241,196,15)
            ELSE
                .txtNomeAdquirente.disabledbackcolor	= RGB(255,255,255)
            ENDIF
        ENDWITH	
        
		atendimento.refresh()
		
    ENDIF
		
	uf_PESQUTENTES_sair()
ENDFUNC


**
FUNCTION uf_PESQUTENTES_tecladoVirtual
	**PESQUTENTES.tecladoVirtual1.show("PESQUTENTES.grdPesq", 161, "PESQUTENTES.shape18", 161)
ENDFUNC


*************************************
*		Fechar Ecra de Pesquisa		*
*************************************
FUNCTION uf_PESQUTENTES_sair
	** fecha cursores
	IF USED("UCRSPRODUTOSCLI")
		fecha("UCRSPRODUTOSCLI")
	ENDIF 
	
	&& variavel publica usada para a pesquisa avan�ada de clientes
	IF TYPE("MyMarcaAux") = "U"
		**
	ELSE
	    RELEASE MyMarcaAux
	ENDIF 
	
	** fecha o painel
	IF TYPE("PESQUTENTES") <> "U"
		PESQUTENTES.hide
		PESQUTENTES.release
	ENDIF 
ENDFUNC


** 
function uf_PESQUTENTES_gravar
	DO case
		CASE myOrigemPPCl == "SMS" OR myOrigemPPCl == "EMAIL"
			uf_PESQUTENTES_selCienteSMS()
		CASE myOrigemPPCl == "PROMOCOES"
			uf_PESQUTENTES_selCientePROMOCOES()
		CASE myOrigemPPCl == "CAMPANHAS" 
			uf_CAMPANHAS_selPesquisaUt()			
		CASE myOrigemPPCl == "REBATIMENTOS"
			uf_REBATIMENTOS_selPesquisaUt()			
		OTHERWISE
			***
	ENDCASE 
ENDFUNC


**
FUNCTION uf_PESQUTENTES_selCientePROMOCOES
	LOCAL lcValida
	lcValida = 0
	
	SELECT uCrsPesquisarCL
	GO TOP
	SCAN
		IF uCrsPesquisarCL.sel == .t.
			** verifica se j� existe na lista
			SELECT UcrsCLPromo
			GO TOP
			SCAN
				IF UcrsCLPromo.clstamp == uCrsPesquisarCL.utstamp
					lcValida=1
				ENDIF
			ENDSCAN
			
			** se n�o existir acrescenta
			IF lcValida == 0
				SELECT UcrsCLPromo
				APPEND BLANK
				replace UcrsCLPromo.nome	WITH	ALLTRIM(uCrsPesquisarCL.nome)
				replace UcrsCLPromo.no		WITH	uCrsPesquisarCL.no
				replace UcrsCLPromo.estab 	WITH	uCrsPesquisarCL.estab
				replace UcrsCLPromo.tipo	WITH	uCrsPesquisarCL.tipo
				replace UcrsCLPromo.clstamp	WITH	uCrsPesquisarCL.utstamp
			ENDIF 
			
			lcValida=0
		ENDIF
	ENDSCAN
	
	PROMOCOES.refresh()
	
	uf_PESQUTENTES_sair()
ENDFUNC 


**
FUNCTION uf_PESQUTENTES_selCienteSMS
	LOCAL lcValida
	lcValida = 0

	SELECT uCrsPesquisarCL
	GO TOP
	SCAN
		IF uCrsPesquisarCL.sel == .t.
			** verifica se j� existe na lista
			SELECT UcrsCLSMS
			GO TOP
			SCAN
				IF UcrsCLSMS.utstamp == uCrsPesquisarCL.utstamp
					lcValida=1
				ENDIF
			ENDSCAN
			
			** se n�o existir acrescenta
			IF lcValida == 0
				SELECT UcrsCLSMS
				APPEND BLANK
				replace UcrsCLSMS.nome		WITH	ALLTRIM(uCrsPesquisarCL.nome)
				replace UcrsCLSMS.no		WITH	uCrsPesquisarCL.no
				replace UcrsCLSMS.estab 	WITH	uCrsPesquisarCL.estab
				replace UcrsCLSMS.tlmvl 	WITH	uCrsPesquisarCL.tlmvl
				replace UcrsCLSMS.utstamp 	WITH	uCrsPesquisarCL.utstamp
				replace UcrsCLSMS.email 	WITH	uCrsPesquisarCL.email
			ENDIF 
			
			lcValida=0
		ENDIF
	ENDSCAN

	uf_sms_AddAutoPalavraChave()

	CAMPANHAS_SMS.refresh
	
	uf_PESQUTENTES_sair()
ENDFUNC 


**
FUNCTION uf_PESQUTENTES_Sel
	LOCAL lcValida
	STORE 0 TO lcValida

	DO CASE
		CASE ALLTRIM(UPPER(myOrigemPPCl)) == "PCENTRAL"
			IF !empty(uCrsPesquisarCL.no_ext) AND ALLTRIM(uCrsPesquisarCL.no_ext)<>'0'
				uf_pesqutentes_actplafondcentral(ALLTRIM(uCrsPesquisarCL.no_ext),'PCENTRAL')
			ENDIF
			uf_utentes_chama(uCrsPesquisarCL.no, uCrsPesquisarCL.estab)
			
		CASE myOrigemPPCl == "UTENTES"
			IF !empty(uCrsPesquisarCL.no_ext) AND ALLTRIM(uCrsPesquisarCL.no_ext)<>'0'
				uf_pesqutentes_actplafondcentral(ALLTRIM(uCrsPesquisarCL.no_ext),'UTENTES')
			ENDIF 
			uf_utentes_chama(uCrsPesquisarCL.no, uCrsPesquisarCL.estab)
		
		CASE myOrigemPPCl == "SMS" OR myOrigemPPCl == "EMAIL"
			&& n�o faz nada
		
		CASE myOrigemPPCl == "PROMOCOES"
			&& n�o faz nada
		
		CASE myOrigemPPCl == "VACINACAO"
			If Used("uCrsVacinacao")
				Select uCrsVacinacao
				Go top
				replace uCrsVacinacao.nome		WITH uCrsPesquisarCL.nome
				replace uCrsVacinacao.contacto	WITH Iif(Empty(Alltrim(uCrsPesquisarCL.tlmvl)),Alltrim(uCrsPesquisarCL.telefone),Alltrim(uCrsPesquisarCL.tlmvl))
				replace uCrsVacinacao.dataNasc	WITH uCrsPesquisarCL.nascimento
				replace uCrsVacinacao.nrSns	WITH uCrsPesquisarCL.nBenef
				vacinacao.refresh
			ENDIF
		CASE myOrigemPPCl == "VACINACAO"
			If Used("uCrsVacinacao")
				Select uCrsVacinacao
				Go top
				replace uCrsVacinacao.nome		WITH uCrsPesquisarCL.nome
				replace uCrsVacinacao.contacto	WITH Iif(Empty(Alltrim(uCrsPesquisarCL.tlmvl)),Alltrim(uCrsPesquisarCL.telefone),Alltrim(uCrsPesquisarCL.tlmvl))
				replace uCrsVacinacao.dataNasc	WITH uCrsPesquisarCL.nascimento
				replace uCrsVacinacao.nrSns	WITH uCrsPesquisarCL.nBenef
				vacinacao.refresh
			ENDIF
			
		CASE myOrigemPPCl == "MEDICAMENTOHOSPITALAR"
			If Used("uCrsMedicamentosHospitalares")
				Select uCrsMedicamentosHospitalares
				Go top
				replace uCrsMedicamentosHospitalares.nomeUtente			WITH uCrsPesquisarCL.nome
				replace uCrsMedicamentosHospitalares.contactoUtente		WITH Iif(Empty(Alltrim(uCrsPesquisarCL.tlmvl)),Alltrim(uCrsPesquisarCL.telefone),Alltrim(uCrsPesquisarCL.tlmvl))
				replace uCrsMedicamentosHospitalares.dataNascimento		WITH uCrsPesquisarCL.nascimento
				replace uCrsMedicamentosHospitalares.identificacaoSNS	WITH uCrsPesquisarCL.nBenef
				replace uCrsMedicamentosHospitalares.sexo				WITH uCrsPesquisarCL.sexo
				medicamentoHospitalar.refresh
		ENDIF
		
		
		CASE myOrigemPPCl == "FACTURACAOPSICO"
			IF uCrsPesquisarCL.removido=.t. 
				MESSAGEBOX("DESCULPE, MAS ESTE UTENTE EXERCEU O DIREITO A ESQUECIMENTO E N�O PODE SER UTILIZADO.",16,"LOGITOOLS SOFTWARE")
			ELSE 
				IF !empty(uCrsPesquisarCL.no_ext) AND ALLTRIM(uCrsPesquisarCL.no_ext)<>'0'
					uf_pesqutentes_actplafondcentral(ALLTRIM(uCrsPesquisarCL.no_ext),'FACTURACAO')
				ENDIF 
				IF uCrsPesquisarCL.inactivo=.t. 
					MESSAGEBOX("DESCULPE, MAS ESTE UTENTE EST� INATIVO E N�O PODE SER UTILIZADO.",16,"LOGITOOLS SOFTWARE")
					uf_atendimento_selCliente(200, 0, "FACTURACAO")
				ELSE 

					uf_atendimento_selCliente(uCrsPesquisarCL.no, uCrsPesquisarCL.estab,"FACTURACAO")
					
					** desconto do cliente
					facturacao.descontoCL = uCrsPesquisarCL.desconto
					facturacao.noCreditCL = uCrsPesquisarCL.nocredit
					facturacao.modoFactCL = uCrsPesquisarCL.modofact
					
					uf_facturacao_dataVencimentoCliente()
					
					&& Tabala fixa de Iva para clientes estrangeiros
					uf_facturacao_TabelaFixaIvaClientes(uCrsPesquisarCL.tabIva)
					
					IF LEFT(ALLTRIM(ft.ncont),1) <> '5' AND LEFT(ALLTRIM(ft.ncont),1) <> '9' AND LEFT(ALLTRIM(ft.ncont),1) <> '6'
						TEXT TO lcSQL NOSHOW TEXTMERGE 
							SELECT utstamp, autorizado FROM b_utentes WHERE no=<<ft.no>> and estab=<<ft.estab>>
						ENDTEXT 			
						IF !uf_gerais_actGrelha("", "utstamp",lcSQL)
							MESSAGEBOX("DESCULPE, MAS OCORREU UM ERRO A VERIFICAR DADOS DOS SERVI�OS. POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
							RETURN .f.
						ENDIF
						IF utstamp.autorizado = .f.
							uf_enviotoken_chama(ALLTRIM(utstamp.utstamp), 'b_utentes')
						ENDIF 
					ENDIF 
				ENDIF 
			ENDIF 
	
			FACTURACAO.refresh
		
		
		
			
		CASE myOrigemPPCl == "FACTURACAO"
			IF uCrsPesquisarCL.removido=.t. 
				MESSAGEBOX("DESCULPE, MAS ESTE UTENTE EXERCEU O DIREITO A ESQUECIMENTO E N�O PODE SER UTILIZADO.",16,"LOGITOOLS SOFTWARE")
			ELSE 
				IF !empty(uCrsPesquisarCL.no_ext) AND ALLTRIM(uCrsPesquisarCL.no_ext)<>'0'
					uf_pesqutentes_actplafondcentral(ALLTRIM(uCrsPesquisarCL.no_ext),'FACTURACAO')
				ENDIF 
				IF uCrsPesquisarCL.inactivo=.t. 
					MESSAGEBOX("DESCULPE, MAS ESTE UTENTE EST� INATIVO E N�O PODE SER UTILIZADO.",16,"LOGITOOLS SOFTWARE")
					uf_atendimento_selCliente(200, 0, "FACTURACAO")
				ELSE 

					uf_atendimento_selCliente(uCrsPesquisarCL.no, uCrsPesquisarCL.estab,"FACTURACAO")
					
					** desconto do cliente
					facturacao.descontoCL = uCrsPesquisarCL.desconto
					facturacao.noCreditCL = uCrsPesquisarCL.nocredit
					facturacao.modoFactCL = uCrsPesquisarCL.modofact
					
					uf_facturacao_dataVencimentoCliente()
					
					&& Tabala fixa de Iva para clientes estrangeiros
					uf_facturacao_TabelaFixaIvaClientes(uCrsPesquisarCL.tabIva)
					
					IF LEFT(ALLTRIM(ft.ncont),1) <> '5' AND LEFT(ALLTRIM(ft.ncont),1) <> '9' AND LEFT(ALLTRIM(ft.ncont),1) <> '6'
						TEXT TO lcSQL NOSHOW TEXTMERGE 
							SELECT utstamp, autorizado FROM b_utentes WHERE no=<<ft.no>> and estab=<<ft.estab>>
						ENDTEXT 			
						IF !uf_gerais_actGrelha("", "utstamp",lcSQL)
							MESSAGEBOX("DESCULPE, MAS OCORREU UM ERRO A VERIFICAR DADOS DOS SERVI�OS. POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
							RETURN .f.
						ENDIF
						IF utstamp.autorizado = .f.
							uf_enviotoken_chama(ALLTRIM(utstamp.utstamp), 'b_utentes')
						ENDIF 
					ENDIF 
				ENDIF 
			ENDIF 
	
			FACTURACAO.refresh
			
		CASE myOrigemPPCl == "DOCUMENTOS"
			IF !empty(uCrsPesquisarCL.no_ext) AND ALLTRIM(uCrsPesquisarCL.no_ext)<>'0'
				uf_pesqutentes_actplafondcentral(ALLTRIM(uCrsPesquisarCL.no_ext), 'DOCUMENTOS')
			ENDIF 
			
			Select cabdoc
			replace cabdoc.nome		WITH uCrsPesquisarCL.nome
			replace cabdoc.no		WITH uCrsPesquisarCL.no
			replace cabdoc.estab	WITH uCrsPesquisarCL.estab
			
			
			&& Tabala fixa de Iva para fornecedores estrangeiros
			uf_documentos_TabelaFixaIvaForn(uCrsPesquisarCL.tabIva)
			
			DOCUMENTOS.refresh
		
		Case myOrigemPPCl == "CARTAO_CLIENTE_MANIPULACAO"
			Select uCrsPesquisarCL
			IF uf_gerais_actgrelha("", [uCrsDadosFidelSel], [exec up_cartao_pontosFidel ] + ALLTRIM(str(uCrsPesquisarCL.no)) + [, ] + ALLTRIM(str(uCrsPesquisarCL.estab)))
				If Reccount()>0
					cartaocliente.ctnDadosCartao.pageframe1.page2.txtNome.value	= Alltrim(uCrsDadosFidelSel.nome)
					cartaocliente.ctnDadosCartao.pageframe1.page2.txtNo.value	= uCrsDadosFidelSel.no
					cartaocliente.ctnDadosCartao.pageframe1.page2.txtEstab.value = uCrsDadosFidelSel.estab
					cartaocliente.ctnDadosCartao.pageframe1.page2.txtCartao.value = Alltrim(uCrsDadosFidelSel.nrcartao)
					cartaocliente.ctnDadosCartao.pageframe1.page2.txtPTotal.value = uCrsDadosFidelSel.pontostotal
					cartaocliente.ctnDadosCartao.pageframe1.page2.txtPLivres.value = uCrsDadosFidelSel.pontoslivres
					cartaocliente.ctnDadosCartao.pageframe1.page2.txtTipo.value = Alltrim(uCrsDadosFidelSel.tipo)
					cartaocliente.ctnDadosCartao.pageframe1.page2.chkInactivo.tag=IIF(uCrsDadosFidelSel.inactivo,"false","true")
					cartaocliente.ctnDadosCartao.pageframe1.page2.chkInactivo.click()
				Endif
				Fecha("uCrsDadosFidelSel")
			Endif	
		
		Case uf_gerais_compstr(myOrigemPPCl, "REGVENDAS")
			Select uCrsPesquisarCL
			if Type("REGVENDAS")<>"U"
				regvendas.txtCliente.Value 	= alltrim(uCrsPesquisarCL.nome)
				regvendas.txtNo.Value 		= alltrim(str(uCrsPesquisarCL.no))
				regvendas.txtEstab.Value 	= alltrim(str(uCrsPesquisarCL.estab))
			endif 
		Case myOrigemPPCl == "RECIBOS"
			IF !empty(uCrsPesquisarCL.no_ext) AND ALLTRIM(uCrsPesquisarCL.no_ext)<>'0'
				uf_pesqutentes_actplafondcentral(ALLTRIM(uCrsPesquisarCL.no_ext), 'RECIBOS')
			ENDIF 
			
			IF USED("RE")
				IF uCrsPesquisarCL.removido=.t. 
					MESSAGEBOX("DESCULPE, MAS ESTE UTENTE EXERCEU O DIREITO A ESQUECIMENTO E N�O PODE SER UTILIZADO.",16,"LOGITOOLS SOFTWARE")
				ELSE 

					SELECT uCrsPesquisarCL
					SELECT RE
					REPLACE	RE.NOME 	WITH ALLTRIM(UPPER(uCrsPesquisarCL.NOME))
					REPLACE	RE.NO		WITH uCrsPesquisarCL.NO
					REPLACE	RE.ESTAB 	WITH uCrsPesquisarCL.ESTAB
					REPLACE	RE.MORADA	WITH uCrsPesquisarCL.MORADA
					REPLACE	RE.LOCAL	WITH uCrsPesquisarCL.LOCAL
					REPLACE	RE.CODPOST 	WITH uCrsPesquisarCL.CODPOST
					REPLACE	RE.NCONT 	WITH uCrsPesquisarCL.NCONT
					REPLACE RE.CCUSTO	WITH uCrsPesquisarCL.CCUSTO
					REPLACE re.ZONA		WITH uCrsPesquisarCL.ZONA
					
					IF LEFT(ALLTRIM(re.ncont),1) <> '5' AND LEFT(ALLTRIM(re.ncont),1) <> '9' AND LEFT(ALLTRIM(re.ncont),1) <> '6'
						TEXT TO lcSQL NOSHOW TEXTMERGE 
							SELECT utstamp, autorizado FROM b_utentes WHERE no=<<re.no>> and estab=<<re.estab>>
						ENDTEXT 			
						IF !uf_gerais_actGrelha("", "utstamp",lcSQL)
							MESSAGEBOX("DESCULPE, MAS OCORREU UM ERRO A VERIFICAR DADOS DOS SERVI�OS. POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
							RETURN .f.
						ENDIF
						IF utstamp.autorizado = .f.
							uf_enviotoken_chama(ALLTRIM(utstamp.utstamp), 'b_utentes')
						ENDIF 
					ENDIF 
				ENDIF 
		
				SRE.REFRESH
			ENDIF
			
		Case myOrigemPPCl == "CARTAO_CLIENTE"
			uf_cartaocliente_SelPesquisa()
		
		Case myOrigemPPCl == "MARCACOES_SMX"
	
			IF USED("MX")
				SELECT uCrsPesquisarCL
				SELECT MX
				REPLACE	MX.CLNOME 	WITH ALLTRIM(UPPER(uCrsPesquisarCL.NOME))
				REPLACE	MX.CLNO		WITH uCrsPesquisarCL.NO
				REPLACE	MX.CLESTAB 	WITH uCrsPesquisarCL.ESTAB
				REPLACE	MX.MORADA	WITH uCrsPesquisarCL.MORADA
				REPLACE	MX.ORIGEM 	WITH "CL"
				REPLACE	MX.LOCAL	WITH uCrsPesquisarCL.LOCAL
				REPLACE	MX.CODPOST 	WITH uCrsPesquisarCL.CODPOST 
				SMX.REFRESH
			ENDIF
			
		CASE myOrigemPPCl == "ATENDIMENTO"
			IF USED("fi") AND uCrsPesquisarCL.no = 200
				SELECT * FROM fi WHERE ALLTRIM(fi.tipor)=='RES' INTO CURSOR ucrsfireceirasres2 READWRITE 
				IF RECCOUNT("ucrsfireceirasres2") > 0
					uf_perguntalt_chama("DESCULPE, MAS N�O PODE UTILIZAR O CLIENTE 200 PARA UM ATENDIMENTO J� COM RESERVAS." ,"OK","",48)
					fecha("ucrsfireceirasres2")
					RETURN .F.
				ENDIF 
				fecha("ucrsfireceirasres2")
			ENDIF 
			
			IF !empty(uCrsPesquisarCL.no_ext) AND ALLTRIM(uCrsPesquisarCL.no_ext)<>'0'
				uf_pesqutentes_actplafondcentral(ALLTRIM(uCrsPesquisarCL.no_ext), 'ATENDIMENTO')
			ENDIF 
			
			IF uCrsPesquisarCL.removido=.t. 
				MESSAGEBOX("DESCULPE, MAS ESTE UTENTE EXERCEU O DIREITO A ESQUECIMENTO E N�O PODE SER UTILIZADO.",16,"LOGITOOLS SOFTWARE")
				uf_atendimento_selCliente(200, 0, "ATENDIMENTO")
			ELSE 
				IF uCrsPesquisarCL.inactivo=.t. 
					MESSAGEBOX("DESCULPE, MAS ESTE UTENTE EST� INATIVO E N�O PODE SER UTILIZADO.",16,"LOGITOOLS SOFTWARE")
					uf_atendimento_selCliente(200, 0, "ATENDIMENTO")
				ELSE 
					uf_atendimento_selCliente(uCrsPesquisarCL.no, uCrsPesquisarCL.estab, "ATENDIMENTO")
					uf_atendimento_verificaInfoUtente()
					IF uCrsPesquisarCL.no > 200
						**uf_reservas_cria('LISTA',.f.)
						uf_atendimento_reservas_cliente(uCrsPesquisarCL.no, uCrsPesquisarCL.estab)
					ENDIF 
				ENDIF 
			ENDIF 
			
		CASE myOrigemPPCl == "FATURACAOPSICO"
            
			uf_faturacao_selClientePsico(uCrsPesquisarCL.no, uCrsPesquisarCL.estab)
			
		CASE myOrigemPPCl == "ATENDIMENTOPSICO"
            
			uf_atendimento_selClientePsico(uCrsPesquisarCL.no, uCrsPesquisarCL.estab)
		
		CASE myOrigemPPCl == "PAGAMENTO"
			SELECT uCrsCabVendas
			IF uCrsCabVendas.modoPag=.t. AND uCrsPesquisarCL.NO=200 
				IF uCrsPesquisarCL.sel=.t.
					**select uCrsPesquisarCL
					**replace uCrsPesquisarCL.sel with .f.
					uf_perguntalt_chama("N�o pode alterar para o cliente 200 quando o documento est� definido a cr�dito.","OK","",64)
					RETURN .f.
				ELSE 
					RETURN .f.
				ENDIF 
			ENDIF 
			
			SELECT uCrsPesquisarCL
			IF !empty(uCrsPesquisarCL.no_ext) AND ALLTRIM(uCrsPesquisarCL.no_ext)<>'0'
				uf_pesqutentes_actplafondcentral(ALLTRIM(uCrsPesquisarCL.no_ext), 'PAGAMENTO')
			ENDIF 
			IF uCrsPesquisarCL.entFact
				uf_pagamento_pesqEntFact(uCrsPesquisarCL.NO, uCrsPesquisarCL.ESTAB)
			ELSE
				select uCrsPesquisarCL
				replace uCrsPesquisarCL.sel with .f.
				
				uf_perguntalt_chama("N�o pode seleccionar um Utente que n�o seja Entidade de Factura��o.","OK","",64)
				RETURN .f.
			ENDIF 
		
		CASE myOrigemPPCl == "CRIARCLATEND"
			uf_criarclatend_pesqSede(uCrsPesquisarCL.NO)
		
		CASE myOrigemPPCl == "PRESCRICAO" 
			uf_pesqutentes_escolhePresc()
		
		CASE myOrigemPPCl == "MARCACOES_1" OR myOrigemPPCl == "MARCACOES_2" 
			uf_pesqutentes_escolheMarcacoes()
			
		CASE myOrigemPPCl == "MARCACOES_COMP"
			uf_pesqutentes_escolheMarcacoesComp()	
		
		CASE myOrigemPPCl == "SERIES" 
			uf_pesqutentes_escolheSerie()
			
		CASE myOrigemPPCl == "ENTIDADE" 
			uf_pesqutentes_escolheEntidade()
			
		CASE myOrigemPPCl == "FACTURACAOENTIDADES_UTENTES" 
			uf_pesqutentes_escolheFACTURACAOENTIDADES_UTENTES()

		CASE myOrigemPPCl == "FACTURACAOENTIDADES_ENTIDADE" 
			uf_pesqutentes_escolheFACTURACAOENTIDADES_ENTIDADE()
			
		CASE myOrigemPPCl == "HONORARIOS"	
			uf_pesqutentes_escolheHONORARIOS()
			
		CASE myOrigemPPCl == "HONORARIOS_ENTIDADE"		
			uf_pesqutentes_escolheHONORARIOS_ENTIDADE()
		
		CASE myOrigemPPCl == "DOCASSOCIADOSENTIDADE"
			uf_pesqutentes_escolheDOCASSOCIADOSENTIDADES(.f.)

		CASE myOrigemPPCl == "DOCASSOCIADOSENTIDADEFILTRO"
			uf_pesqutentes_escolheDOCASSOCIADOSENTIDADES(.t.)

			
		CASE myOrigemPPCl == "DOCASSOCIADOSCLIENTE"
			uf_pesqutentes_escolheDOCASSOCIADOSCLIENTES(.f.)
		
		CASE myOrigemPPCl == "DOCASSOCIADOSCLIENTEFILTRO"
			uf_pesqutentes_escolheDOCASSOCIADOSCLIENTES(.t.)
		
		CASE myOrigemPPCl == "RESUMOASSOCIADOSCLIENTE"
			uf_pesqutentes_escolheRESUMOASSOCIADOSCLIENTE()	
			
		CASE myOrigemPPCl == "FACTURACAOASSOCIADOS_UTENTES" 
			uf_pesqutentes_escolheFACTURACAOASSOCIADOS_UTENTES()

		CASE myOrigemPPCl == "MORADA"
			SELECT cabDoc
			Replace cabdoc.xpdmorada WITH "[" + ASTR(uCrsPesquisarCL.NO) + "]" +  ALLTRIM(uCrsPesquisarCL.morada) + " " + ALLTRIM(uCrsPesquisarCL.codPost) + " " + ALLTRIM(uCrsPesquisarCL.local)
		
		Case myOrigemPPCl == "SINAVE"
			SELECT uCrsComSinave
			replace uCrsComSinave.healthUsername WITH ALLTRIM(uCrsPesquisarCL.nome)
			replace uCrsComSinave.rnu WITH VAL(ALLTRIM(uCrsPesquisarCL.nbenef))
			replace uCrsComSinave.healthUserBirthDate WITH uCrsPesquisarCL.nascimento
			replace uCrsComSinave.healthUserAddress WITH ALLTRIM(uCrsPesquisarCL.morada)
			IF AT('-', uCrsPesquisarCL.codpost)<>0
				replace uCrsComSinave.healthUserCodPostalBase WITH LEFT(uCrsPesquisarCL.codpost, AT('-', uCrsPesquisarCL.codpost)-1 )
				replace uCrsComSinave.healthUserCodPostalSuffix WITH SUBSTR(uCrsPesquisarCL.codpost, AT('-', uCrsPesquisarCL.codpost)+1, 3)
			ENDIF 
			replace uCrsComSinave.healthUserCountryBorn WITH ALLTRIM(uCrsPesquisarCL.codigop)
			replace uCrsComSinave.healthUserId WITH ALLTRIM(uCrsPesquisarCL.bino)
			
			comunicasinave.refresh

		OTHERWISE
			***
	ENDCASE
	
	IF myOrigemPPCl != "SMS" AND myOrigemPPCl != "EMAIL" AND myOrigemPPCl != "PROMOCOES" AND myOrigemPPCl != "ATENDIMENTO"
		uf_PESQUTENTES_sair()
	ENDIF
ENDFUNC


** Evento Click no especialista escolhido
FUNCTION uf_pesqutentes_escolhePresc
	
	SELECT ucrsCabPresc
	GO TOP
	SELECT uCrsPesquisarCL
	Replace ucrsCabPresc.utstamp			WITH uCrsPesquisarCL.clstamp
	Replace ucrsCabPresc.nome 				WITH uCrsPesquisarCL.nome
	Replace ucrsCabPresc.no 				WITH uCrsPesquisarCL.no
	Replace ucrsCabPresc.morada 			WITH uCrsPesquisarCL.morada
	Replace ucrsCabPresc.local 				WITH uCrsPesquisarCL.local
	Replace ucrsCabPresc.codpost			WITH uCrsPesquisarCL.codpost
	Replace ucrsCabPresc.telefone			WITH uCrsPesquisarCL.telefone
	Replace ucrsCabPresc.telemovel			WITH uCrsPesquisarCL.tlmvl
	Replace ucrsCabPresc.email				WITH uCrsPesquisarCL.email
	Replace ucrsCabPresc.sexo				WITH uCrsPesquisarCL.sexo
	Replace ucrsCabPresc.nrutente			WITH uCrsPesquisarCL.nbenef
	Replace ucrsCabPresc.nrbenef			WITH IIF(uCrsPesquisarCL.cesd == .f. ,uCrsPesquisarCL.nbenef2, uCrsPesquisarCL.cesdidp)
	Replace ucrsCabPresc.dtnasc				WITH uCrsPesquisarCL.nascimento
	Replace ucrsCabPresc.nacionalidade		WITH uCrsPesquisarCL.codigop
	replace ucrsCabPresc.naturalidade		WITH uCrsPesquisarCL.codigopnat
	
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		Select 
			dplmsstamp
			,u_design
			,diploma
			,patologia
		from 
			b_cli_diplomasID (nolock) 
		where 
			idstamp = '<<uCrsPesquisarCL.clstamp>>'
		order by 	
			diploma
	ENDTEXT 
	IF !uf_gerais_actgrelha("Prescricao.pageframe1.page1.pageframeUtentes.page3.grdPresc", [uCrsDiplomasPresc], lcSQL)
		RETURN .f.
	ENDIF
	
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_prescricao_PatologiasUtente '<<ALLTRIM(uCrsPesquisarCL.clstamp)>>'
	ENDTEXT 
	IF !uf_gerais_actgrelha("Prescricao.pageframe1.page1.pageframeUtentes.page4.grdPresc", [uCrsPatologiasPresc], lcSQL)
		RETURN .f.
	ENDIF
	
	** Historico de MCDT's
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_prescricao_mcdtMenos90 <<uCrsPesquisarCL.no>>
	ENDTEXT 
	IF !uf_gerais_actgrelha("Prescricao.pageframe1.page1.pageframeUtentes.page8.gridPresq", [uCrsmcdtMenosNoventa], lcSQL)
		RETURN .f.
	ENDIF
	
	IF !EMPTY(uCrsPesquisarCL.clstamp)
		lcSQL = ""
		Text to lcSql noshow textmerge
			exec up_prescricao_HistoricoMedUtente '<<ALLTRIM(uCrsPesquisarCL.clstamp)>>'
		ENDTEXT
		IF !uf_gerais_actgrelha("Prescricao.pageframe1.page1.pageframeUtentes.page9.gridPresq", [uCrsHistMedUtente], lcSQL)
			MESSAGEBOX("Ocorreu um erro a procurar hist�rico de medicamentos. Contacte o suporte.",16,"LOGITOOLS SOFTWARE")
			RETURN .f.
		ENDIF
	ENDIF
	
	** Obt�m informa��o do Utente selecionado
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		 exec up_prescricao_utenteInfo '<<uCrsPesquisarCL.clstamp>>'
	ENDTEXT 

	IF !uf_gerais_actgrelha("", [uCrsInfoUtente], lcSQL)
		RETURN .f.
	ENDIF
	SELECT uCrsInfoUtente

	** Trata Entidade Independente
	DO CASE 
		CASE !EMPTY(uCrsInfoUtente.nprescsns)
			Select uCrsCabPresc
			Replace uCrsCabPresc.entidade		WITH "SEM COMPARTICIPACA��O P/ SNS"
			Replace uCrsCabPresc.entidadeDescr	WITH "Sem Comparticipa��o pelo SNS"
			replace uCrsCabPresc.entidadeCod	WITH "999998"
			Replace ucrsCabPresc.nrbenef WITH ""
		CASE (uCrsInfoUtente.despla == "Independente" OR EMPTY(uCrsInfoUtente.despla)) AND EMPTY(ALLTRIM(uCrsInfoUtente.nbenef))
			Select uCrsCabPresc
			Replace uCrsCabPresc.entidade		WITH "SEM COMPARTICIPACA��O P/ SNS"
			Replace uCrsCabPresc.entidadeDescr	WITH "Sem Comparticipa��o pelo SNS"
			replace uCrsCabPresc.entidadeCod	WITH "999998"
			Replace ucrsCabPresc.nrbenef WITH ""
		CASE (uCrsInfoUtente.despla == "Independente" OR EMPTY(uCrsInfoUtente.despla)) AND !EMPTY(ALLTRIM(uCrsInfoUtente.nbenef))
			Select uCrsCabPresc
			Replace uCrsCabPresc.entidade		WITH "SNS"
			Replace uCrsCabPresc.entidadeDescr	WITH "SNS"
			replace uCrsCabPresc.entidadeCod	WITH "935601"
			Replace ucrsCabPresc.nrbenef WITH ""
		OTHERWISE
			Select uCrsCabPresc
			Replace uCrsCabPresc.entidade		WITH uCrsInfoUtente.entpla	
			Replace uCrsCabPresc.entidadeDescr	WITH uCrsInfoUtente.despla
			Replace uCrsCabPresc.entidadeCod	WITH uCrsInfoUtente.codpla
	ENDCASE 
	
	** Substitui informa��o de Regime especial de comparticipa��o
	Select uCrsCabPresc
	Replace UcrsCabPresc.RECMtipo		WITH uCrsInfoUtente.recmtipo
	Replace UcrsCabPresc.RECMCodigo 	WITH uCrsInfoUtente.recmmotivo
	Replace UcrsCabPresc.RECMDescricao 	WITH uCrsInfoUtente.recmdescricao
	Replace UcrsCabPresc.RECMDatainicio	WITH uCrsInfoUtente.recmdatainicio
	Replace UcrsCabPresc.RECMDatafim 	WITH uCrsInfoUtente.recmdatafim

	**Substitui informa��o de beneficios (Isen��o de taxa)
	Replace UcrsCabPresc.BENEFCodigo 		WITH uCrsInfoUtente.itmotivo
	Replace UcrsCabPresc.BENEFDescricao 	WITH uCrsInfoUtente.itdescricao
	Replace UcrsCabPresc.BENEFDatainicio    WITH uCrsInfoUtente.itdatainicio
	Replace UcrsCabPresc.BENEFDatafim 		WITH uCrsInfoUtente.itdatafim

	IF !EMPTY(ALLTRIM(uCrsInfoUtente.itmotivo))
		Replace UcrsCabPresc.mcdt_isento WITH .t.
	ELSE
		Replace UcrsCabPresc.mcdt_isento WITH .f.	
	ENDIF
	
	uf_prescricao_ActualizaEncargos()

	Prescricao.ultimoutstamp = ucrsCabPresc.utstamp
	PRESCRICAO.pageframe1.page1.pageframeUtentes.Refresh
	PRESCRICAO.refresh
	
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		SELECT utstamp, autorizado FROM b_utentes WHERE no=<<ucrsCabPresc.no>>
	ENDTEXT 			
	IF !uf_gerais_actGrelha("", "utstamp",lcSQL)
		MESSAGEBOX("DESCULPE, MAS OCORREU UM ERRO A VERIFICAR DADOS DOS SERVI�OS. POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF
	IF utstamp.autorizado = .f.
		uf_enviotoken_chama(ALLTRIM(utstamp.utstamp), 'b_utentes')
	ENDIF 
	
ENDFUNC


**
FUNCTION uf_pesqutentes_escolheMarcacoes
	IF myOrigemPPCl == "MARCACOES_1" 
		SELECT uCrsPesquisarCL
		marcacoes.utente.Value = ALLTRIM(UPPER(uCrsPesquisarCL.nome))
	ELSE
		IF USED("UcrsMarcacoesDia")
			SELECT UcrsMarcacoesDia
			SELECT uCrsPesquisarCL
			REPLACE UcrsMarcacoesDia.nome		 WITH ALLTRIM(uCrsPesquisarCL.nome)
			REPLACE UcrsMarcacoesDia.no			 WITH uCrsPesquisarCL.no
			REPLACE UcrsMarcacoesDia.estab 		 WITH uCrsPesquisarCL.estab
			REPLACE UcrsMarcacoesDia.utstamp  	 WITH uCrsPesquisarCL.utstamp
		ENDIF
	ENDIF
	MARCACOES.REFRESH
ENDFUNC


**
FUNCTION uf_pesqutentes_escolheMarcacoesComp

	**
	IF USED("uCrsCompServMarcacao")
		SELECT uCrsPesquisarCL
		select uCrsCompServMarcacao
		REPLACE uCrsCompServMarcacao.entidade WITH ALLTRIM(uCrsPesquisarCL.nome)
		REPLACE uCrsCompServMarcacao.entidadeno WITH uCrsPesquisarCL.no
		REPLACE uCrsCompServMarcacao.entidadeestab WITH uCrsPesquisarCL.estab
		MARCACOES.Container1.ContainerDetalhes.Pageframe1.Page2.GridRecursos.refresh
	ENDIF 
	
ENDFUNC

**
FUNCTION uf_pesqutentes_escolheSerie
	
	IF USED("ucrsSeriesServ")
		SELECT ucrsSeriesServ
		SELECT uCrsPesquisarCL
		REPLACE ucrsSeriesServ.utstamp 	WITH ALLTRIM(uCrsPesquisarCL.utstamp)
		REPLACE ucrsSeriesServ.nome		WITH ALLTRIM(uCrsPesquisarCL.nome)
		REPLACE ucrsSeriesServ.no 		WITH uCrsPesquisarCL.no
		REPLACE ucrsSeriesServ.estab 	WITH uCrsPesquisarCL.estab
		
		uf_PLANEAMENTOMR_atualizaPVPs()
	ENDIF

	IF USED("ucrsPeriodosCalendarioMensal")
		SELECT ucrsPeriodosCalendarioMensal
		lcPosicao = ucrsPeriodosCalendarioMensal.posicao
		UPDATE ucrsPeriodosCalendarioMensal SET nome = ALLTRIM(ucrsSeriesServ.nome) WHERE !EMPTY(ucrsPeriodosCalendarioMensal.sel)
		SELECT ucrsPeriodosCalendarioMensal
		LOCATE FOR ucrsPeriodosCalendarioMensal.posicao == lcPosicao
	ENDIF 
	PLANEAMENTOMR.REFRESH
ENDFUNC


**
FUNCTION uf_pesqutentes_escolheEntidade
	
	IF USED("ucrsEntidadesUtentes")
		SELECT ucrsEntidadesUtentes
		APPEND BLANK
		SELECT uCrsPesquisarCL
		REPLACE ucrsEntidadesUtentes.eutstamp WITH ALLTRIM(uCrsPesquisarCL.utstamp)
		REPLACE ucrsEntidadesUtentes.enome    WITH ALLTRIM(uCrsPesquisarCL.nome)
		REPLACE ucrsEntidadesUtentes.eno      WITH uCrsPesquisarCL.no
		REPLACE ucrsEntidadesUtentes.eestab   WITH uCrsPesquisarCL.estab
		
		IF USED("CL")
			Select CL
			REPLACE ucrsEntidadesUtentes.utstamp WITH ALLTRIM(cl.utstamp)
			REPLACE ucrsEntidadesUtentes.nome 	 WITH ALLTRIM(cl.nome)
			REPLACE ucrsEntidadesUtentes.no 	 WITH cl.no
			REPLACE ucrsEntidadesUtentes.estab	 WITH cl.estab
		ENDIF 
	ENDIF

	UTENTES.REFRESH
ENDFUNC


**
FUNCTION uf_pesqutentes_escolheFACTURACAOENTIDADES_ENTIDADE
	SELECT  uCrsPesquisarCL
	FACTENTIDADESCLINICA.entidade.value = ALLTRIM(uCrsPesquisarCL.nome)
	FACTENTIDADESCLINICA.show && forca Focus
ENDFUNC 


**
FUNCTION uf_pesqutentes_escolheFACTURACAOENTIDADES_UTENTES
	SELECT  uCrsPesquisarCL
	FACTENTIDADESCLINICA.utente.value = ALLTRIM(uCrsPesquisarCL.nome)
ENDFUNC 

**
FUNCTION uf_pesqutentes_escolheFACTURACAOASSOCIADOS_UTENTES
	SELECT  uCrsPesquisarCL
	FATASSOCIADOS.utente.value = ALLTRIM(uCrsPesquisarCL.nome)
ENDFUNC 


**
FUNCTION uf_pesqutentes_escolheHONORARIOS
	SELECT  uCrsPesquisarCL
	IMPORTHONORARIOS.utente.value = ALLTRIM(uCrsPesquisarCL.nome)
ENDFUNC 


**
FUNCTION uf_pesqutentes_escolheHONORARIOS_ENTIDADE
	SELECT  uCrsPesquisarCL
	IMPORTHONORARIOS.entidade.value = ALLTRIM(uCrsPesquisarCL.nome)
ENDFUNC

**
FUNCTION uf_pesqutentes_escolheDOCASSOCIADOSENTIDADES
	LPARAMETERS tcFiltro

	IF EMPTY(tcFiltro)
		SELECT uCrsPesquisarCL
		SELECT uCrsDocAssociados
		REPLACE uCrsDocAssociados.nome_org		WITH ALLTRIM(uCrsPesquisarCL.nome)
		REPLACE uCrsDocAssociados.nr_org		WITH uCrsPesquisarCL.no

		PESQDOCASSOCIADOS.REFRESH
	ELSE
		PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.nome_org.value    = ALLTRIM(uCrsPesquisarCL.nome)
		PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.nr_org.value	 	 = STR(uCrsPesquisarCL.no)
		PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.id_org.value 	 = ALLTRIM(uCrsPesquisarCL.id)
		
		PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.nome_org.refresh
		PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.nr_org.refresh
		PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.id_org.refresh
	ENDIF
ENDFUNC



**
FUNCTION uf_pesqutentes_escolheDOCASSOCIADOSCLIENTES
	LPARAMETERS tcFiltro

	IF EMPTY(tcFiltro)
		SELECT uCrsPesquisarCL
		SELECT uCrsDocAssociados 
		REPLACE uCrsDocAssociados.nome_cl	WITH ALLTRIM(uCrsPesquisarCL.nome)
		REPLACE uCrsDocAssociados.id_cl		WITH uCrsPesquisarCL.id
		
		PESQDOCASSOCIADOS.REFRESH
	ELSE
		PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.nome_cl.value = ALLTRIM(uCrsPesquisarCL.nome)
		PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.nr_cl.value	 = STR(uCrsPesquisarCL.no)
		PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.id_cl.value 	 = ALLTRIM(uCrsPesquisarCL.id)
		
		PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.nome_cl.refresh
		PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.nr_cl.refresh
		PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.id_cl.refresh
	ENDIF 
	
ENDFUNC


**
FUNCTION uf_pesqutentes_escolheRESUMOASSOCIADOSCLIENTE
	
	SELECT uCrsPesquisarCL
	
	PESQDOCASSOCIADOS.Pageframe1.Page2.nome_cl.value = ALLTRIM(uCrsPesquisarCL.nome)
	PESQDOCASSOCIADOS.Pageframe1.Page2.nr_cl.value	 = STR(uCrsPesquisarCL.no)
	PESQDOCASSOCIADOS.Pageframe1.Page2.id_cl.value 	 = ALLTRIM(uCrsPesquisarCL.id)
	
	PESQDOCASSOCIADOS.REFRESH
	
ENDFUNC 

************************************************
** leitura de c�digos de barras com o scanner **
************************************************
FUNCTION uf_pesqutentes_scanner

	pesqutentes.txtscanner.Value = strtran(pesqutentes.txtscanner.Value, chr(39), '')
	
	&& valida preenchimento
	If Empty(alltrim(pesqutentes.txtscanner.value))
		RETURN .F.
	ENDIF
	
	Local lcRef, lcUtente
	Local lcValida, lcValidaVal, lcValidaValRef, lcValidaCL
	Store .f. To lcValida, lcValidaVal, lcValidaValRef, lcValidaCL
	Store 0 To lcRef, lcUtente
	
	lcRef = UPPER(alltrim(pesqutentes.txtscanner.value))
	
	DO CASE

		*********************** 
		** Cart�o de Cliente **
		***********************
		CASE UPPER(Left(lcRef,2)) == 'CL' AND (Len(lcRef) == 7)
			DO CASE 
				CASE PESQUTENTES.Pageframe1.activepage == 1
				     PESQUTENTES.Pageframe1.Page1.ncartao.value = ALLTRIM(lcRef)
					 PESQUTENTES.Pageframe1.Page1.ncartao.setfocus
				CASE PESQUTENTES.Pageframe1.activepage == 2
					 PESQUTENTES.Pageframe1.Page2.ncartao.value = ALLTRIM(lcRef)	
					 PESQUTENTES.Pageframe1.Page2.ncartao.setfocus
			ENDCASE 
			
			PESQUTENTES.menu1.actualizar.click
		
		*******************************************
		** Nr de Utente**			
		*******************************************
		CASE (Len(lcRef) > 8) And (Len(lcRef) < 11)
			DO CASE 
				CASE PESQUTENTES.Pageframe1.activepage == 1
					PESQUTENTES.Pageframe1.Page1.utenteno.value = ALLTRIM(lcRef)
					PESQUTENTES.Pageframe1.Page1.utenteno.setfocus
				CASE PESQUTENTES.Pageframe1.activepage == 2
					PESQUTENTES.Pageframe1.Page2.utenteno.value = ALLTRIM(lcRef)
					PESQUTENTES.Pageframe1.Page2.utenteno.setfocus
			ENDCASE 
			
			PESQUTENTES.menu1.actualizar.click
	ENDCASE
	
	&& limpa valor
	pesqutentes.txtscanner.value = ""
ENDFUNC 


**
FUNCTION uf_PESQUTENTES_FATASSOCIADOS_chama
	IF !uf_FATASSOCIADOS_chama()
		RETURN .F.
	ENDIF
	uf_PESQUTENTES_sair()
	FATASSOCIADOS.show
ENDFUNC


** 
FUNCTION uf_PESQUTENTES_criaCliente
	LOCAL lcValidaCamposObrigatorios 
	lcValidaCamposObrigatorios = .f.

	IF !uf_gerais_getParameter_site('ADM0000000088', 'BOOL', mySite)
		** Valida campos Obrigat�rios
		WITH PESQUTENTES.Pageframe1
			DO CASE 
				CASE .activepage == 2 && FrontOffice

					IF EMPTY(ALLTRIM(.page2.nome.value))
						uf_perguntalt_chama("O campo Nome do Utente � obrigat�rio.","OK","", 48)
						RETURN .f.
					ENDIF

					IF EMPTY(ALLTRIM(.page2.morada.value)) AND uf_gerais_getParameter("ADM0000000112","BOOL") == .t. && Morada Obrigatorio
						uf_perguntalt_chama("O campo Morada � obrigat�rio.","OK","", 48)
						RETURN .f.
					ENDIF 

					IF EMPTY(ALLTRIM(.page2.ncont.value)) AND uf_gerais_getParameter("ADM0000000046","BOOL") == .t. && NIF Obrigatorio
						uf_perguntalt_chama("O campo Contribuinte � obrigat�rio.","OK","", 48)
						RETURN .f.
					ENDIF
					
					IF EMPTY(ALLTRIM(.page2.tlf.value)) AND uf_gerais_getParameter("ADM0000000022","BOOL") == .t. && Telefone Obrigatorio
						uf_perguntalt_chama("O campo Telefone/Tlm � obrigat�rio.","OK","", 48)
						RETURN .f.
					ENDIF 
					
					IF EMPTY(ALLTRIM(.page2.txtNascimento.value)) AND uf_gerais_getParameter("ADM0000000023","BOOL") == .t. ;
					AND !uf_utentes_validaSeUtenteTipoEmpresa(ALLTRIM(.page2.ncont.value),"") && Data Nascimento Obrigatorio
						uf_perguntalt_chama("O campo Dt. Nascimento � obrigat�rio.","OK","", 48)
						RETURN .f.
					ENDIF
					
					IF EMPTY(ALLTRIM(.page2.Nid.value)) AND uf_gerais_getParameter("ADM0000000305","BOOL") == .t. && Nr Doc. ID obrigat�rio
						uf_perguntalt_chama("O campo NId (N� Doc. ID.) � obrigat�rio.","OK","", 48)
						RETURN .f.
					ENDIF 

               IF !uf_gerais_validaPattern("^[0-9]*$", ALLTRIM(.page2.Nid.value) ) AND UPPER(ALLTRIM(ucrse1.pais))=='PORTUGAL'
                  uf_perguntalt_chama("O campo NId (N� Doc. ID.) n�o pode conter letras nem caracteres especiais.", "OK", "", 48)                  
                  RETURN .f.
               ENDIF
				
			ENDCASE 
			
		ENDWITH 
		
		
		lcSQL = ""
		TEXT TO lcSQL TEXTMERGE NOSHOW
			SET fmtonly on select b_utentes.*, binovalidade = '19000101' from b_utentes (nolock) where 1=0 SET fmtonly off
		ENDTEXT
		
		IF !uf_gerais_actGrelha("","uCrsInCLAtend",lcSQL)
			uf_perguntalt_chama("N�O FOI POSS�VEL CRIAR CURSOR DE CLIENTES. POR FAVOR REINICIE O SOFTWARE.","OK","",16)
			RETURN .f.
		ENDIF
		
		SELECT uCrsInCLAtend
		APPEND BLANK
			
		uf_utentes_ValoresDefeito("uCrsInCLAtend","PESQUTENTES")
		
		SELECT uCrsInCLAtend
		Replace uCrsInCLAtend.nome WITH ALLTRIM(PESQUTENTES.Pageframe1.page2.nome.value)
		Replace uCrsInCLAtend.ncont WITH ALLTRIM(PESQUTENTES.Pageframe1.page2.ncont.value)
		Replace uCrsInCLAtend.morada WITH ALLTRIM(PESQUTENTES.Pageframe1.page2.morada.value)
		
		IF LEFT(ALLTRIM(PESQUTENTES.Pageframe1.page2.nome.value),1) != "2"
			Replace uCrsInCLAtend.tlmvl WITH ALLTRIM(PESQUTENTES.Pageframe1.page2.tlf.value)
		ELSE
			Replace uCrsInCLAtend.telefone WITH ALLTRIM(PESQUTENTES.Pageframe1.page2.tlf.value)
		ENDIF 
		
		Replace uCrsInCLAtend.nascimento WITH CTOD(uf_gerais_getdate(PESQUTENTES.Pageframe1.page2.txtnascimento.value))
		Replace uCrsInCLAtend.nbenef WITH ALLTRIM(PESQUTENTES.Pageframe1.page2.utenteno.value)
		Replace uCrsInCLAtend.email WITH ALLTRIM(PESQUTENTES.Pageframe1.page2.email.value)
		Replace uCrsInCLAtend.bino WITH ALLTRIM(PESQUTENTES.Pageframe1.page2.nid.value)
		Replace uCrsInCLAtend.codPost WITH ALLTRIM(PESQUTENTES.Pageframe1.page2.codPost.value)

		** Actualiza as contas de cliente para integra��o na contabilidade (para1)
		uf_utentes_contaSNC(.t.,"PESQUTENTES")

		** Funcao com validacoes antes da gravacao
		LOCAL lcValida
		STORE .f. TO lcValida

		lcValida = uf_utente_validacoes("uCrsInCLAtend")

		IF lcValida == .t.
			IF !uf_utentes_gravarBD("uCrsInCLAtend", "CRIARCLATEND", "uCrsPatCLAtend")
				RETURN .f.
			ENDIF 
		
		ELSE
			RETURN .f.
		ENDIF
		
		IF !(TYPE("ATENDIMENTO") == "U")
            IF myOrigemPPCl == "ATENDIMENTO"
			    uf_atendimento_selCliente(uCrsInCLAtend.no,uCrsInCLAtend.estab,"ATENDIMENTO")
            ELSE
                IF myOrigemPPCl == "ATENDIMENTOPSICO"
                    uf_atendimento_selClientePsico(uCrsInCLAtend.no, uCrsInCLAtend.estab)
                ENDIF
            ENDIF
		ENDIF
	ELSE
		LOCAL lcnoins, lcSQL
		
		** Verificar se o cliente existe localmente
		lcSQL =''
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			exec up_utentes_camposduplicados_at '<<ALLTRIM(PESQUTENTES.Pageframe1.page2.ncont.value)>>'
				,'<<ALLTRIM(PESQUTENTES.Pageframe1.page2.email.value)>>'
				,'<<ALLTRIM(PESQUTENTES.Pageframe1.page2.tlf.value)>>' 
				,'<<ALLTRIM(PESQUTENTES.Pageframe1.page2.nid.value)>>'
		ENDTEXT 

		IF !(uf_gerais_actGrelha("","uCrsCamposRepetidosAT",lcSQL))
			uf_perguntalt_chama("Ocorreu uma anomalida a verificar os dados do Utente. Por favor contacte o Suporte.","OK","",16)
			RETURN .f.
		ENDIF

		IF !EMPTY(ALLTRIM(uCrsCamposRepetidosAT.texto)) AND uCrsCamposRepetidosAT.aviso == .f.
			uf_perguntalt_chama(uCrsCamposRepetidosAT.texto,"OK","",16)
			RETURN .f.
		ELSE
			IF !EMPTY(ALLTRIM(uCrsCamposRepetidosAT.texto)) AND uCrsCamposRepetidosAT.aviso == .t.
				uf_perguntalt_chama(uCrsCamposRepetidosAT.texto,"OK","",64)
			ENDIF
		ENDIF	
		fecha("uCrsCamposRepetidosAT")
		
		** Verificar se o cliente existe no servidor central
			
		LOCAL StampUt 
		StampUt = uf_gerais_stamp()
		
		#DEFINE  FLAG_ICC_FORCE_CONNECTION 1
	 	DECLARE Long InternetCheckConnection IN Wininet.dll String Url, Long dwFlags, Long Reserved
	 	
		LOCAL lcValidaLigInternet, lcUrl, lcRemoteSQL, lcNoRemoto, lcRemoteDB, lcRemoteSRV
		STORE .t. TO lcvalidaLigInternet
		STORE '' TO lcUrl , lcRemoteSQL, lcRemoteDB, lcRemoteSRV
		STORE 0 TO lcNoRemoto
		
		lcUrl = uf_gerais_getParameter_site('ADM0000000089', 'TEXT', mySite)
		lcRemoteSQL = uf_gerais_getParameter_site('ADM0000000090', 'TEXT', mySite)
		lcRemoteDB = substr(uf_gerais_getParameter_site('ADM0000000090', 'TEXT', mySite),at('].[',uf_gerais_getParameter_site('ADM0000000090', 'TEXT', mySite))+2 ,99)
		lcRemoteSRV = substr(uf_gerais_getParameter_site('ADM0000000090', 'TEXT', mySite), 1,at('].[',uf_gerais_getParameter_site('ADM0000000090', 'TEXT', mySite)))
		IF InternetCheckConnection(lcUrl, FLAG_ICC_FORCE_CONNECTION, 0) <> 0
			*? "Connection is available"
			lcValidaLigInternet = .t.
			** verifica se o SQL central est� online
			lcSQL =''
			TEXT TO lcSQL NOSHOW TEXTMERGE 
				exec <<ALLTRIM(lcRemoteSQL)>>.[dbo].up_utentes_camposduplicados_at '','','',''
			ENDTEXT 
			IF !(uf_gerais_ActGrelha_semerro("","uCrsCamposRepetidosAT",lcSQL))
				lcvalidaLigInternet = .f.	
			ENDIF
		ELSE
			lcvalidaLigInternet = .f.
		ENDIF
		
		IF lcValidaLigInternet == .f.
			** n�o tem liga��o com o servidor
			lcNoRemoto = uf_utentes_insert_local_error(ALLTRIM(StampUt))
			uf_atendimento_selCliente(lcNoRemoto,0,"ATENDIMENTO")
			RETURN .t.
		ELSE
		
			lcSQL =''
			TEXT TO lcSQL NOSHOW TEXTMERGE 
				exec <<ALLTRIM(lcRemoteSQL)>>.[dbo].up_utentes_camposduplicados_at '<<ALLTRIM(PESQUTENTES.Pageframe1.page2.ncont.value)>>'
					,'<<ALLTRIM(PESQUTENTES.Pageframe1.page2.email.value)>>'
					,'<<ALLTRIM(PESQUTENTES.Pageframe1.page2.tlf.value)>>' 
					,'<<ALLTRIM(PESQUTENTES.Pageframe1.page2.nid.value)>>'
			ENDTEXT 

			IF !(uf_gerais_actGrelha("","uCrsCamposRepetidosAT",lcSQL))
				uf_perguntalt_chama("Ocorreu uma anomalida a verificar os dados do Utente. Por favor contacte o Suporte.","OK","",16)
				RETURN .f.
			ENDIF

			IF !EMPTY(ALLTRIM(uCrsCamposRepetidosAT.texto)) AND uCrsCamposRepetidosAT.aviso == .f.
				IF uCrsCamposRepetidosAT.no > 0
					If uf_perguntalt_chama("O UTENTE J� EST� CRIADO NA bd CENTRAL. DESEJA IMPORT�-LO?","Sim","N�o")
						IF uf_PESQUTENTES_import_utente_central(uCrsCamposRepetidosAT.no)
							lcnoins = uCrsCamposRepetidosAT.no
							IF !(TYPE("ATENDIMENTO") == "U")
								uf_atendimento_selCliente(lcnoins,0,"ATENDIMENTO")
								RETURN .t.
							ENDIF
						ELSE
							RETURN .f.
						ENDIF 
					ELSE 
						**uf_perguntalt_chama(uCrsCamposRepetidosAT.texto,"OK","",16)
						RETURN .f.
					ENDIF 
				ENDIF 
			ELSE
				IF !EMPTY(ALLTRIM(uCrsCamposRepetidosAT.texto)) AND uCrsCamposRepetidosAT.aviso == .t.
					uf_perguntalt_chama(uCrsCamposRepetidosAT.texto,"OK","",64)
				ENDIF
			ENDIF	
			fecha("uCrsCamposRepetidosAT")
			
			lcnoins = uf_utentes_insremoto_at()
			IF !(TYPE("ATENDIMENTO") == "U")
				uf_atendimento_selCliente(lcnoins,0,"ATENDIMENTO")
			ENDIF
		ENDIF 
	ENDIF 

	
	RETURN .t.
ENDFUNC 

FUNCTION uf_PESQUTENTES_import_utente_central
	LPARAMETERS lcNoUtente
	LOCAL lcRemoteSQL 
	lcRemoteSQL = uf_gerais_getParameter_site('ADM0000000090', 'TEXT', mySite)
	TEXT TO lcSQL TEXTMERGE NOSHOW
		insert into b_utentes (utstamp, no, estab, nome, morada, ncont, bino, nbenef, local, tlmvl, tipo, codpla, despla, nbenef2, ousrdata, ousrhora, ousrinis, usrdata, usrhora, usrinis, vencimento, moeda, entpla, notif,pais, email, descp, codigop, descpNat, codigopNat, no_ext)
		select utstamp, no, estab, nome, morada, ncont, bino, nbenef, local, tlmvl, tipo, codpla, despla, nbenef2, ousrdata, ousrhora, ousrinis, usrdata, usrhora, usrinis, vencimento, moeda, entpla, notif,pais, email, descp, codigop, descpNat, codigopNat, no_ext from  <<lcRemoteSQL>>.[dbo].[b_utentes] where no=<<lcNoUtente>>
	ENDTEXT
	If !uf_gerais_actGrelha("", "", lcSql)
		uf_perguntalt_chama("N�o foi possivel inserir o cliente na BD local. Por favor contacte o suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF
	RETURN .t.
ENDFUNC 

** funcao para imprimir etiquetas com o nr do cartao de cliente
FUNCTION uf_PESQUTENTES_imprimirCartao

	IF !uf_perguntalt_chama("Pretende imprimir dados do cart�o para todos os utentes da pesquisa?","Sim","N�o")
		RETURN .f.
	ENDIF
	
	Local lcCount, lcPrinter,lcOldPrinter
	Store 0 To lcCount
	lcOldPrinter= set("printer",2)
	

	
	TEXT TO lcSQL TEXTMERGE NOSHOW
		select documento,nomeficheiro from B_impressoes (nolock) where tabela = 'CARTAO_ETIQUETAS'
	ENDTEXT
	If !uf_gerais_actGrelha("", "ucrsEtiquetasFicheiro", lcSql)
		uf_perguntalt_chama("N�o foi possivel verificar a configura��o de etiquetas. Por favor contacte o suporte.", "", "OK", 32)
		RETURN .f.
	ENDIF
	lcReportCartao = ALLTRIM(ucrsEtiquetasFicheiro.nomeficheiro)
		
	&& Verifica se os reports de etiquetas existem
	If !File(Alltrim(mypath)+'\analises\' + ALLTRIM(lcReportCartao))
		uf_perguntalt_chama("Modelo  de impress�o n�o encontrado. Se a dificuldade persistir contacte o Suporte.","OK","",48)
		RETURN .f.
	ENDIF

	**
	lcPrinter = uf_etiquetas_calculaLabelPrinter("PRODUTO")			

	&& Verificar se a impressora existe
	IF EMPTY(lcPrinter)
		uf_perguntalt_chama("A impressora n�o est� a ser detectada. Se a dificuldade persistir contacte o Suporte.","OK","",48)
		RETURN .f.
	ELSE
		DIMENSION lcPrinters[1,1]
		APRINTERS(lcPrinters)
		LOCAL lcExistePrinter
		FOR lnIndex = 1 TO ALEN(lcPrinters,1)
			IF upper(alltrim(lcPrinters(lnIndex,1))) == upper(alltrim(lcPrinter))
				lcExistePrinter = .t.
			ENDIF
		ENDFOR
		IF !lcExistePrinter && se existir
			uf_perguntalt_chama("A impressora n�o est� a ser detectada. Se a dificuldade persistir contacte o Suporte.","OK","",48)
			RETURN
		ENDIF
	ENDIF 
		
	&& Define a Impressora	
	Set Printer To Name ''+lcPrinter+''
	Set Device To print
	Set Console off

	&& Cria cursor Aux que permite usar o mesmo IDU em diferentes paineis
	SELECT no, nome, '*'+alltrim(nrcartao)+'*'  as nrcartao FROM cl INTO CURSOR auxImpCl READWRITE 

	&& Dados para impress�o
	SELECT auxImpCl 
	GO TOP 
	SCAN FOR !EMPTY(ALLTRIM(auxImpCl.nrcartao))
		Report Form Alltrim(mypath)+'\analises\' + ALLTRIM(lcReportCartao) To Printer
	ENDSCAN
						
	&& Define a impressora no default
	Set Device To screen
	Set printer to Name ''+lcOldPrinter+''
	Set Console On

	IF USED("auxImpCl")
		fecha("auxImpCl")
	ENDIF
	
ENDFUNC 


**
FUNCTION uf_PESQUTENTES_limpar

	PESQUTENTES.pageframe1.page2.nome.value = ''
	PESQUTENTES.pageframe1.page2.numero.value = ''
	PESQUTENTES.pageframe1.page2.estab.value = ''
	PESQUTENTES.pageframe1.page2.morada.value = ''
	PESQUTENTES.pageframe1.page2.ncont.value = ''
	PESQUTENTES.pageframe1.page2.nCartao.value = ''
	PESQUTENTES.pageframe1.page2.tlf.value = ''
	PESQUTENTES.pageframe1.page2.utenteno.value = ''
	PESQUTENTES.pageframe1.page2.email.value = ''
	PESQUTENTES.pageframe1.page2.cmbTipo.value = ''
	PESQUTENTES.pageframe1.page2.profissao.value = ''
	PESQUTENTES.pageframe1.page2.sexo.value = 'M/F'
	PESQUTENTES.pageframe1.page2.OperadorLogico.value = '>'
	PESQUTENTES.pageframe1.page2.txtnascimento.value = '1900.01.01'	
	PESQUTENTES.pageframe1.page2.ncont.value = ''	

	PESQUTENTES.refresh()
	PESQUTENTES.pageframe1.page2.nome.setfocus()

ENDFUNC

FUNCTION uf_pesqutentes_actplafondcentral
	LPARAMETERS lcNoExt, lcOrigem, uv_espera
	
	IF uf_gerais_getparameter_site('ADM0000000079', 'BOOL', mySite)
		** colocar o plafond a 0 se for no atendimento
		IF  VARTYPE(lcOrigem) != 'L' AND (ALLTRIM(lcOrigem)=='ATENDIMENTO' OR ALLTRIM(lcOrigem)=='BO')
			TEXT TO lcSQL TEXTMERGE NOSHOW
				UPDATE b_utentes SET eplafond=0, esaldo=0 WHERE no_ext = '<<ALLTRIM(lcNoExt)>>'
			ENDTEXT
			If !uf_gerais_actGrelha("", "", lcSql)
				uf_perguntalt_chama("N�o foi possivel atualizar o plafond a 0 para este cliente. Por favor contacte o suporte.", "", "OK", 32)
				RETURN .f.
			ENDIF
		ENDIF 
	
		LOCAL lcAdd
		STORE '&' TO lcAdd
		lcWsDir    = ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\X3\ltsX3'
				
		lcWsPath = 'javaw -jar ltsx3.jar ' + '' + ' "--YCREDCCC==1"' + ' "' + '--IDCLIENT=' + ALLTRIM(lcNoExt)  + '" '
		lcWsPath = "cmd /c cd /d " + lcWsDir+ " "+  lcAdd + lcAdd + ' ' + lcWsPath 

	
		oWSShell = CREATEOBJECT("WScript.Shell")
		oWSShell.Run(lcWsPath,0,uv_espera)
	ENDIF 
ENDFUNC 