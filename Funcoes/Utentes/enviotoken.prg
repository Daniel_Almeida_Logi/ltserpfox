FUNCTION uf_ENVIOTOKEN_chama
	LPARAMETERS lcStamp, lcSource
	
    IF uf_gerais_getParameter_site("ADM0000000001", "BOOL", mySite) = .t.

		** cria cursor vazio
		lcSQL = ""
		TEXT TO lcSQL TEXTMERGE NOSHOW
			select no, estab, nome, email, autoriza_emails, tlmvl, autoriza_sms from b_utentes WHERE utstamp='<<ALLTRIM(lcStamp)>>'
		ENDTEXT 
		IF !uf_gerais_actgrelha("", "uCrsEnvioToken", lcSql)
			uf_perguntalt_chama("ERRO AO LER A TABELA DE Utentes!","OK","",48)
			RETURN .F.
		ENDIF 
		
		&& Controla Abertura do Painel			
		if type("ENVIOTOKEN")=="U"
			DO FORM ENVIOTOKEN
		ELSE
			ENVIOTOKEN.show
		ENDIF
	ENDIF 
	
ENDFUNC


**
FUNCTION uf_ENVIOTOKEN_carregamenu
	**ENVIOTOKEN.menu1.adicionaopcao("atualizar", "Atualizar", myPath + "\imagens\icons\actualizar_w.png", "uf_LOGCLIENTE_Pesquisa", "F2")
ENDFUNC


**
FUNCTION uf_ENVIOTOKEN_sair
	**Fecha Cursores
*!*		IF USED("uCrsLogCl")
*!*			fecha("uCrsLogCl")
*!*		ENDIF 
	
	ENVIOTOKEN.hide
	ENVIOTOKEN.release
ENDFUNC

FUNCTION uf_ENVIOTOKEN_envia_SMS
	LPARAMETERS lcSource, lcDestino, lcNo, lcEstab
	LOCAL lcTexto, lcWsPath, lcCodEnvio , lcToken, lcUtStamp 
	STORE '' TO lcWsPath , lcTexto
	STORE LEFT(ALLTRIM(m_chinis)+dtoc(date())+SYS(2015),25) TO lcCodEnvio 
	
	lcToken = RIGHT('0000'+alltrim(STR(INT((999999 - 1 + 1) * Rand(-1) + 1))),6)
	
	&& Verifica se ainda tem SMS dispon�veis - o controlo � feito por calcudo do valor definido no parametro ADM0000000188 vs n� de SMS enviados na tabela b_sms_fact 
*!*		lcSQL = ''
*!*		TEXT TO lcSQL TEXTMERGE NOSHOW
*!*			select ((select 
*!*				isnull(sum(total),0)
*!*			from b_sms_fact (nolock)
*!*			where 
*!*				username = '<<ALLTRIM(uf_gerais_getParameter("ADM0000000157","TEXT"))>>'
*!*				and 
*!*				status in (0,1,2))
*!*				+ (SELECT COUNT(STAMP) from logs_com where enviado=1)) as enviados
*!*		ENDTEXT

*!*		uf_gerais_actGrelha("","uCrsEnviados",lcSQL)
*!*		SELECT uCrsEnviados
*!*		IF uf_gerais_getParameter("ADM0000000188","NUM") <= (uCrsEnviados.enviados + 1)
*!*			uf_perguntalt_chama("N�O ADQUIRIU SMS SUFICIENTES PARA ENVIAR ESTA COMUNICA��O." + CHR(13) + "PARA ADQUIRIR MAIS SMS POR FAVOR CONTACTE A LOGITOOLS (OP��O - COMERCIAL).", "OK","",16)
*!*			RETURN .F.
*!*		ENDIF 

	** VAI BUSCAR A MENSAGEM PREDEFINIDA PARA O ENVIO DO SMS
	TEXT TO lcSQL TEXTMERGE NOSHOW
		select top 1 email_body as MENSAGEM from b_sms where codminuta='TOKEN-RGPD-SMS' AND site = '<<ALLTRIM(mySite)>>' and convert(varchar(1), email_body) is not null order by ousrdata desc
	ENDTEXT 
	IF !uf_gerais_actgrelha("", "ucrsMsg", lcSql)
		uf_perguntalt_chama("ERRO AO CONSULTAR A MENSAGEM DE ENVIO!","OK","",48)
		RETURN .F.
	ENDIF 
	IF RECCOUNT("ucrsMsg")=0
		uf_perguntalt_chama("N�O FOI CONFIGURADA NENHUMA MENSAGEM PARA O ENVIO DE TOKEN."+ CHR(13) +" POR FAVOR CONFIGURE A MESMA NO PAINEL DE COMUNICA��ES!","OK","",16)
		RETURN .F.
	ELSE
		lcTexto = ALLTRIM(ucrsMsg.MENSAGEM) + ' - ' + ALLTRIM(lcToken) 
	ENDIF
	
	TEXT TO lcSQL TEXTMERGE NOSHOW
		SELECT utstamp FROM b_utentes where no=<<lcNo>> and estab=<<lcEstab>>
	ENDTEXT 
	IF !uf_gerais_actgrelha("", "ucrsUtStamp", lcSql)
		uf_perguntalt_chama("ERRO AO CONSULTAR A TABELA DE UTENTES!","OK","",48)
		RETURN .F.
	ENDIF 
	lcUtStamp = ALLTRIM(ucrsUtStamp.utstamp)
	
	uf_send_sms(lcCodEnvio, 'RGPD', ALLTRIM(lcSource), ALLTRIM(lcTexto), ALLTRIM(lcDestino), 'CL', lcUtStamp, 'Envio de token de aprova��o por SMS para o nr ' + ALLTRIM(lcDestino) + CHR(13) )
	
	TEXT TO lcSQL TEXTMERGE NOSHOW
		UPDATE b_utentes SET tokenaprovacao=<<lcToken>> where no=<<lcNo>> and estab=<<lcEstab>>
	ENDTEXT 
	IF !uf_gerais_actgrelha("", "", lcSql)
		uf_perguntalt_chama("ERRO AO ATUALIZAR A TABELA DE UTENTES!","OK","",48)
		RETURN .F.
	ENDIF 

	
ENDFUNC

FUNCTION uf_ENVIOTOKEN_envia_EMAIL
	LPARAMETERS lcDestino, lcNo, lcEstab
	
	LOCAL lcTo, lcToEmail, lcSubject, lcBody, lcAtatchment, lcToken, lcTexto 
	STORE '' TO lcTexto 
	
	lcToken = RIGHT('0000'+alltrim(STR(INT((999999 - 1 + 1) * Rand(-1) + 1))),6)
	
	TEXT TO lcSQL TEXTMERGE NOSHOW
		select top 1 email_body from b_sms where codminuta='TOKEN-RGPD-EMAIL' AND site = '<<ALLTRIM(mySite)>>' and convert(varchar(1), email_body) is not null order by b_sms.ousrdata desc
	ENDTEXT 
	IF !uf_gerais_actgrelha("", "ucrsMsg", lcSql)
		uf_perguntalt_chama("ERRO AO CONSULTAR A MENSAGEM DE ENVIO!","OK","",48)
		RETURN .F.
	ENDIF 
	IF RECCOUNT("ucrsMsg")=0
		uf_perguntalt_chama("N�O FOI CONFIGURADA NENHUMA MENSAGEM PARA O ENVIO DE TOKEN."+ CHR(13) +" POR FAVOR CONFIGURE A MESMA NO PAINEL DE COMUNICA��ES!","OK","",16)
		RETURN .F.
	ELSE
		lcTexto = ALLTRIM(ucrsMsg.email_body) + ' - ' + ALLTRIM(lcToken) 
	ENDIF
	
	lcTo = ""
	lcToEmail = ALLTRIM(lcDestino)
	lcSubject = 'Envio de c�digo de aprova��o'
	lcBody = lcTexto + CHR(13) + ALLTRIM(ucrse1.mailsign)
	lcAtatchment = ""
	regua(0,6,"A enviar email ...")
	uf_startup_sendmail(lcTo, lcToEmail, lcSubject, lcBody , lcAtatchment, .t.)
	regua(2)
	
	TEXT TO lcSQL TEXTMERGE NOSHOW
		UPDATE b_utentes SET tokenaprovacao=<<lcToken>> where no=<<lcNo>> and estab=<<lcEstab>>
	ENDTEXT 
	IF !uf_gerais_actgrelha("", "", lcSql)
		uf_perguntalt_chama("ERRO AO ATUALIZAR A TABELA DE UTENTES!","OK","",48)
		RETURN .F.
	ENDIF 
	
	TEXT TO lcSQL TEXTMERGE NOSHOW
		SELECT utstamp FROM b_utentes where no=<<lcNo>> and estab=<<lcEstab>>
	ENDTEXT 
	IF !uf_gerais_actgrelha("", "ucrsUtStamp", lcSql)
		uf_perguntalt_chama("ERRO AO CONSULTAR A TABELA DE UTENTES!","OK","",48)
		RETURN .F.
	ENDIF 
	uf_user_log_ins('CL', ALLTRIM(ucrsUtStamp.utstamp), 'EMAIL', 'Envio de token de aprova��o por Email para '+ALLTRIM(lcDestino) + CHR(13) )
	
ENDFUNC 

FUNCTION uf_ENVIOTOKEN_confirma
	LPARAMETERS lcToken ,lcNo, lcEstab
	
	TEXT TO lcSQL TEXTMERGE NOSHOW
		SELECT utstamp FROM b_utentes where no=<<lcNo>> and estab=<<lcEstab>>
	ENDTEXT 
	IF !uf_gerais_actgrelha("", "ucrsUtStamp", lcSql)
		uf_perguntalt_chama("ERRO AO CONSULTAR A TABELA DE UTENTES!","OK","",48)
		RETURN .F.
	ENDIF 
	
	TEXT TO lcSQL TEXTMERGE NOSHOW
		select tokenaprovacao from b_utentes where no=<<lcNo>> and estab=<<lcEstab>>
	ENDTEXT 
	IF !uf_gerais_actgrelha("", "ucrsToken", lcSql)
		uf_perguntalt_chama("ERRO AO CONSULTAR O TOKEN DO UTENTES!","OK","",48)
		RETURN .F.
	ENDIF 
	IF ucrsToken.tokenaprovacao = 0 then
		uf_perguntalt_chama("AINDA N�O FOI EMITIDO UM C�DIGO DE CONFIRMA��O PARA ESTE UTENTE!","OK","",48)
		RETURN .F.
	ELSE
				
		IF ucrsToken.tokenaprovacao = lcToken then
			lcSQL1 = ''
			
			IF enviotoken.chktelemovel.tag == "true" AND enviotoken.chkemail.tag == "true"
				TEXT TO lcSQL1 TEXTMERGE NOSHOW
					update b_utentes SET autorizado=1, autoriza_emails=1, autoriza_sms=1 where no=<<lcNo>> and estab=<<lcEstab>>
				ENDTEXT 
				uf_user_log_ins('CL', ALLTRIM(ucrsUtStamp.utstamp), 'CONFIRMA��O', 'Confirma��o do token de aprova��o - autoriza��o comunica��es via email e telem�vel')
			ENDIF 
			IF enviotoken.chktelemovel.tag == "false" AND enviotoken.chkemail.tag == "true"
				TEXT TO lcSQL1 TEXTMERGE NOSHOW
					update b_utentes SET autorizado=1, autoriza_emails=1, autoriza_sms=0 where no=<<lcNo>> and estab=<<lcEstab>>
				ENDTEXT 
				uf_user_log_ins('CL', ALLTRIM(ucrsUtStamp.utstamp), 'CONFIRMA��O', 'Confirma��o do token de aprova��o - autoriza��o comunica��es via email')
			ENDIF 
			IF enviotoken.chktelemovel.tag == "true" AND enviotoken.chkemail.tag == "false"
				TEXT TO lcSQL1 TEXTMERGE NOSHOW
					update b_utentes SET autorizado=1, autoriza_emails=0, autoriza_sms=1 where no=<<lcNo>> and estab=<<lcEstab>>
				ENDTEXT 
				uf_user_log_ins('CL', ALLTRIM(ucrsUtStamp.utstamp), 'CONFIRMA��O', 'Confirma��o do token de aprova��o - autoriza��o comunica��es via telem�vel')
			ENDIF 
			IF enviotoken.chktelemovel.tag == "false" AND enviotoken.chkemail.tag == "false"
				TEXT TO lcSQL1 TEXTMERGE NOSHOW
					update b_utentes SET autorizado=1 where no=<<lcNo>> and estab=<<lcEstab>>
				ENDTEXT 
				uf_user_log_ins('CL', ALLTRIM(ucrsUtStamp.utstamp), 'CONFIRMA��O', 'Confirma��o do token de aprova��o - sem autoriza��o de comunica��es')
			ENDIF 
			IF !uf_gerais_actgrelha("", "", lcSql1)
				uf_perguntalt_chama("ERRO AO ATUALIZAR A TABELA DE UTENTES!","OK","",48)
				RETURN .F.
			ENDIF 
			IF enviotoken.chkact.tag == "true"
				IF !empty(enviotoken.txtTlmvl.value) AND !empty(enviotoken.txtEmail.value)
					TEXT TO lcSQL1 TEXTMERGE NOSHOW
						update b_utentes SET email='<<ALLTRIM(enviotoken.txtemail.value)>>', tlmvl='<<ALLTRIM(enviotoken.txtTlmvl.value)>>' where no=<<lcNo>> and estab=<<lcEstab>>
					ENDTEXT 
					uf_user_log_ins('CL', ALLTRIM(ucrsUtStamp.utstamp), 'CONFIRMA��O', 'Confirma��o do token de aprova��o - atualiza��o de email e telem�vel')
				ENDIF 
				IF empty(enviotoken.txtTlmvl.value) AND !empty(enviotoken.txtEmail.value)
					TEXT TO lcSQL1 TEXTMERGE NOSHOW
						update b_utentes SET email='<<ALLTRIM(enviotoken.txtemail.value)>>' where no=<<lcNo>> and estab=<<lcEstab>>
					ENDTEXT 
					uf_user_log_ins('CL', ALLTRIM(ucrsUtStamp.utstamp), 'CONFIRMA��O', 'Confirma��o do token de aprova��o - atualiza��o de email')
				ENDIF 
				IF !empty(enviotoken.txtTlmvl.value) AND empty(enviotoken.txtEmail.value)
					TEXT TO lcSQL1 TEXTMERGE NOSHOW
						update b_utentes SET tlmvl='<<ALLTRIM(enviotoken.txtTlmvl.value)>>' where no=<<lcNo>> and estab=<<lcEstab>>
					ENDTEXT 
					uf_user_log_ins('CL', ALLTRIM(ucrsUtStamp.utstamp), 'CONFIRMA��O', 'Confirma��o do token de aprova��o - atualiza��o de telem�vel')
				ENDIF 
				IF !uf_gerais_actgrelha("", "", lcSql1)
					uf_perguntalt_chama("ERRO AO ATUALIZAR A TABELA DE UTENTES!","OK","",48)
				RETURN .F.
			ENDIF 
			ENDIF 
		
			uf_perguntalt_chama("C�DIGO CONFIRMADO COM SUCESSO!","OK","",64)
			
			TEXT TO lcSQL TEXTMERGE NOSHOW
				SELECT utstamp FROM b_utentes where no=<<lcNo>> and estab=<<lcEstab>>
			ENDTEXT 
			IF !uf_gerais_actgrelha("", "ucrsUtStamp", lcSql)
				uf_perguntalt_chama("ERRO AO CONSULTAR A TABELA DE UTENTES!","OK","",48)
				RETURN .F.
			ENDIF 
						
			&& Actualiza Ecr�
			IF TYPE("UTENTES")!= "U" 
				TEXT TO lcSql NOSHOW textmerge
					SELECT Top 1 
						b_utentes.* 
						,pontos = isnull(b_fidel.pontosAtrib,0) - isnull(b_fidel.pontosUsa,0)
						,binoValidade = convert(datetime,isnull(cc.validade_fim, '19000101'))
						,compfixa=0
						,valcartao = 0
						,percadic = 0
						,templano = convert(bit,0)
					FROM 
						b_utentes (nolock) 
						left join b_fidel (nolock)  on b_utentes.no = b_fidel.clno and b_utentes.estab = b_fidel.clestab
						left join dispensa_eletronica_cc cc (nolock) on b_utentes.utstamp = cc.utstamp
					where 
						b_utentes.no = <<lcNo>> 
						and b_utentes.estab = <<lcEstab>>
				ENDTEXT
				IF !uf_gerais_actgrelha("", "cl", lcSql)
					RETURN .f.
				ENDIF
	
				SELECT cl
				utentes.refresh
			ENDIF 
			
			uf_ENVIOTOKEN_sair()
	
		ELSE
			uf_perguntalt_chama("C�DIGO INV�LIDO" + CHR(13) + "N�O CORRESPONDE AO ENVIADO PARA O UTENTE!","OK","",48)
		ENDIF 
	ENDIF 
	
ENDFUNC 


FUNCTION uf_ENVIOTOKEN_confirma_doc
	LPARAMETERS lcNo, lcEstab
	LOCAL lcUtstamp
	
	IF uf_perguntalt_chama("CONFIRMA QUE O UTENTE ASSINOU A MINUTA COM O CONSENTIMENTO?","Sim","N�o")
		lcSQL = ''
			TEXT TO lcSQL TEXTMERGE NOSHOW
				SELECT utstamp FROM b_utentes(nolock) where no=<<lcNo>> and estab=<<lcEstab>>
			ENDTEXT 
			IF !uf_gerais_actgrelha("", "ucrsUtStamp", lcSql)
				uf_perguntalt_chama("ERRO AO CONSULTAR A TABELA DE UTENTES!","OK","",48)
				RETURN .F.
			ENDIF 
			lcUtstamp=ucrsUtStamp.utstamp
			lcSQL1 = ''
			
			IF enviotoken.chktelemovel.tag == "true" AND enviotoken.chkemail.tag == "true"
				TEXT TO lcSQL1 TEXTMERGE NOSHOW
					update b_utentes SET autorizado=1, autoriza_emails=1, autoriza_sms=1 where no=<<lcNo>> and estab=<<lcEstab>>
				ENDTEXT 
				uf_user_log_ins('CL', ALLTRIM(lcUtstamp), 'CONFIRMA��O', 'Confirma��o de aprova��o por documento - autoriza��o comunica��es via email e telem�vel')
			ENDIF 
			IF enviotoken.chktelemovel.tag == "false" AND enviotoken.chkemail.tag == "true"
				TEXT TO lcSQL1 TEXTMERGE NOSHOW
					update b_utentes SET autorizado=1, autoriza_emails=1, autoriza_sms=0 where no=<<lcNo>> and estab=<<lcEstab>>
				ENDTEXT 
				uf_user_log_ins('CL', ALLTRIM(lcUtstamp), 'CONFIRMA��O', 'Confirma��o de aprova��o por documento - autoriza��o comunica��es via email')
			ENDIF 
			IF enviotoken.chktelemovel.tag == "true" AND enviotoken.chkemail.tag == "false"
				TEXT TO lcSQL1 TEXTMERGE NOSHOW
					update b_utentes SET autorizado=1, autoriza_emails=0, autoriza_sms=1 where no=<<lcNo>> and estab=<<lcEstab>>
				ENDTEXT 
				uf_user_log_ins('CL', ALLTRIM(lcUtstamp), 'CONFIRMA��O', 'Confirma��o de aprova��o por documento - autoriza��o comunica��es via telem�vel')
			ENDIF 
			IF enviotoken.chktelemovel.tag == "false" AND enviotoken.chkemail.tag == "false"
				TEXT TO lcSQL1 TEXTMERGE NOSHOW
					update b_utentes SET autorizado=1 where no=<<lcNo>> and estab=<<lcEstab>>
				ENDTEXT 
				uf_user_log_ins('CL', ALLTRIM(lcUtstamp), 'CONFIRMA��O', 'Confirma��o de aprova��o por documento - sem autoriza��o de comunica��es')
			ENDIF 
			IF !uf_gerais_actgrelha("", "", lcSql1)
				uf_perguntalt_chama("ERRO AO ATUALIZAR A TABELA DE UTENTES!","OK","",48)
				RETURN .F.
			ENDIF 
			IF enviotoken.chkact.tag == "true"
				IF !empty(enviotoken.txtTlmvl.value) AND !empty(enviotoken.txtEmail.value)
					TEXT TO lcSQL1 TEXTMERGE NOSHOW
						update b_utentes SET email='<<ALLTRIM(enviotoken.txtemail.value)>>', tlmvl='<<ALLTRIM(enviotoken.txtTlmvl.value)>>' where no=<<lcNo>> and estab=<<lcEstab>>
					ENDTEXT 
					uf_user_log_ins('CL', ALLTRIM(lcUtstamp), 'CONFIRMA��O', 'Confirma��o de aprova��o por documento - atualiza��o de email e telem�vel')
				ENDIF 
				IF empty(enviotoken.txtTlmvl.value) AND !empty(enviotoken.txtEmail.value)
					TEXT TO lcSQL1 TEXTMERGE NOSHOW
						update b_utentes SET email='<<ALLTRIM(enviotoken.txtemail.value)>>' where no=<<lcNo>> and estab=<<lcEstab>>
					ENDTEXT 
					uf_user_log_ins('CL', ALLTRIM(lcUtstamp), 'CONFIRMA��O', 'Confirma��o de aprova��o por documento - atualiza��o de email')
				ENDIF 
				IF !empty(enviotoken.txtTlmvl.value) AND empty(enviotoken.txtEmail.value)
					TEXT TO lcSQL1 TEXTMERGE NOSHOW
						update b_utentes SET tlmvl='<<ALLTRIM(enviotoken.txtTlmvl.value)>>' where no=<<lcNo>> and estab=<<lcEstab>>
					ENDTEXT 
					uf_user_log_ins('CL', ALLTRIM(lcUtstamp), 'CONFIRMA��O', 'Confirma��o de aprova��o por documento - atualiza��o de telem�vel')
				ENDIF 
				IF !uf_gerais_actgrelha("", "", lcSql1)
					uf_perguntalt_chama("ERRO AO ATUALIZAR A TABELA DE UTENTES!","OK","",48)
				RETURN .F.
			ENDIF 
			ENDIF 
	
		uf_perguntalt_chama("CONFIRMA��O EFETUADA!","OK","",64)

		&& Actualiza Ecr�
		IF TYPE("UTENTES")!= "U" 
			
			TEXT TO lcSql NOSHOW textmerge
				SELECT Top 1 
					b_utentes.* 
					,pontos = isnull(b_fidel.pontosAtrib,0) - isnull(b_fidel.pontosUsa,0)
					,binoValidade = convert(datetime,isnull(cc.validade_fim, '19000101'))
					,compfixa=0
					,valcartao = 0
					,percadic = 0
					,templano = convert(bit,0)
				FROM 
					b_utentes (nolock) 
					left join b_fidel (nolock)  on b_utentes.no = b_fidel.clno and b_utentes.estab = b_fidel.clestab
					left join dispensa_eletronica_cc cc (nolock) on b_utentes.utstamp = cc.utstamp
				where 
					b_utentes.no = <<lcNo>> 
					and b_utentes.estab = <<lcEstab>>
			ENDTEXT
			IF !uf_gerais_actgrelha("", "cl", lcSql)
				RETURN .f.
			ENDIF

			SELECT cl
			utentes.refresh
		ENDIF  
		
		uf_ENVIOTOKEN_sair()
	ELSE
		uf_perguntalt_chama("PEDIDO CANCELADO.","OK","",48)	
	ENDIF 
	
	
ENDFUNC 