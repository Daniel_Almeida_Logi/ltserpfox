**
FUNCTION uf_wdgfiltroreport_alteraAnalise
	LOCAL lcStrObj, lcVdefault, lcPag, lcTxtRel, lcWdg, lcNPagina

	IF TYPE("relatorio") == "U"
		lcTxtRel = "painelCentral.pagina" + astr(painelCentral.pagina) + ".container1.wdgselreport.txtAnalise"
		lcWdg = "painelCentral.pagina" + astr(painelCentral.pagina) + ".container1.wdgselreport"
		lcPag = "painelCentral.pagina" + astr(painelCentral.pagina) + ".container1.wdgfiltroreport.containerfiltros"
		lcNPagina = astr(painelCentral.pagina)
	ELSE
		lcTxtRel = "relatorio.pagina2.container1.wdgselreport.txtAnalise"
		lcWdg = "relatorio.pagina2.container1.wdgselreport"
		lcPag = "relatorio.pagina2.container1.wdgfiltroreport.containerfiltros"
		lcNPagina = "2"
	ENDIF 
	
	IF EMPTY(ALLTRIM(&lcTxtRel..value))
		RETURN .f.
	ENDIF
	
	&lcPag..posultobj = 6
	&lcPag..espacamento = 2
	
	lcStrObj = ""
	
	*FOR EACH mf IN &lcPag..objects
	FOR EACH mf IN (IIF(TYPE("relatorio")=="U","painelCentral.pagina","relatorio.pagina") + lcNPagina + ".container1.wdgfiltroreport.containerfiltros.objects")
		lcStrObj = lcStrObj + mf.name + ";"
	ENDFOR
	
	ALINES(aArrayEliminaCampos,lcStrObj, ";")
		
	IF ALEN(aArrayEliminaCampos) > 0
		TRY 
			FOR i=1 to ALEN(aArrayEliminaCampos)
				&lcPag..RemoveObject(aArrayEliminaCampos[i])
			ENDFOR
		CATCH
			** controla elimina��o exporadiaca de obj que nao existe			
		ENDTRY
	ENDIF
	
	** Obtem Defini��es da Analise
	lcSQL = ""
	lcValor = &lcWdg..lcOrdem
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_analises_Lista_Definicoes <<lcValor>>
	ENDTEXT

	IF !uf_gerais_actGrelha("","ucrsAnalisesDef",lcSQL)
		return .f.
	ENDIF

	SELECT ucrsAnalisesDef
	GO Top
	SCAN
	
		** Trata Valor Default
		** Controla se o Valor por defeito � expressao sql
		IF ucrsAnalisesDef.vdefaultsql == .t.
			TRY 
				lcSQL = ''
				Alines(arrayDefault,ucrsAnalisesDef.vdefault,"&")
				For i=1 To Alen(arrayDefault)
					If i == 1
						lcSQL = arrayDefault[i]
					Else
						lcObj = Strtran(arrayDefault[i],['],[])
						lcSQL = lcSQL + &lcObj + [']
					Endif
				ENDFOR
				IF !uf_gerais_actGrelha("","ucrsAnalisesvDefault",lcSQL)
					MESSAGEBOX("N�O FOI POSSIVEL OBTER O VALOR POR DEFEITO PARA O PARAMETRO: " + ALLTRIM(UCRSANALISESDEF.LABEL),48,"LOGITOOLS SOFTWARE")
				ELSE
					IF !EMPTY(ucrsAnalisesvDefault.valor)
						SELECT ucrsAnalisesDef
				
						lcVdefault = ''
						DO CASE 
							CASE ALLTRIM(UPPER(ucrsAnalisesDef.tipo)) == 'D'
								lcVdefault = uf_gerais_getdate(ucrsAnalisesvDefault.valor)
							CASE type('ucrsAnalisesvDefault.valor') == 'N'	
								lcVdefault = STR(ucrsAnalisesvDefault.valor)
							OTHERWISE
								lcVdefault = ucrsAnalisesvDefault.valor
						ENDCASE
						Replace ucrsAnalisesDef.vdefault WITH lcVdefault
					ENDIF 
				ENDIF 
			CATCH
				&&Caso a expressao T-sql n�o seja valida
				Replace ucrsAnalisesDef.vdefault WITH ""
			ENDTRY
		ENDIF
		** Controla se o Valor por defeito � expressao fox
		IF ucrsAnalisesDef.vdefaultfox == .t.
			lcVdefault = ALLTRIM(ucrsAnalisesDef.vdefault)
			TRY
				DO CASE 
					CASE type(lcVdefault) == 'D'
						lcVdefault = uf_gerais_getdate(&lcVdefault)
					CASE type(lcVdefault) == 'N'	
						lcVdefault = STR(&lcVdefault)
					OTHERWISE
						lcVdefault = &lcVdefault
				ENDCASE
				
				Replace ucrsAnalisesDef.vdefault WITH lcVdefault 
			
			CATCH
				MESSAGEBOX("N�o foi possivel atribuir valor por defeito para um ou mais par�metros.",32,"Logitools Software")
			ENDTRY
		ENDIF
		
		*********************************************************		
		
		**Adiciona
		lcComado = Left(ucrsAnalisesDef.comando, 16777184)
		 
		&lcPag..adicionaOpcao(ALLTRIM(ucrsAnalisesDef.nome), ALLTRIM(ucrsAnalisesDef.label), ALLTRIM(ucrsAnalisesDef.tipo), ALLTRIM(lcComado),;
		ucrsAnalisesDef.vdefault, ucrsAnalisesDef.colunas, ucrsAnalisesDef.colunasWidth, ucrsAnalisesDef.firstempty, ucrsAnalisesDef.formato, ucrsAnalisesDef.mascara, ucrsAnalisesDef.multiselecao, ucrsAnalisesDef.comandoesql)
		
		lcObj = lcPag + "." + ALLTRIM(ucrsAnalisesDef.nome)
		
		**Trata obrigat�rios
		IF !EMPTY(ucrsAnalisesDef.obrigatorio)
			TRY 
				&lcObj..text1.DisabledBackColor = RGB(191,223,223)&&RGB(230,242,255)		
				
			CATCH
				** No caso de n�o existir o objecto, n�o faz nada
			ENDTRY 
		ENDIF 
		
		**Trata visiveis
		IF EMPTY(ucrsAnalisesDef.visivel)
			&lcObj..visible = .f.
		ENDIF
		
		
		**Cloca Valor por defeito caso seja uma expressao fox, caso nao seja este valor � atibuido no config do obj
		IF ucrsAnalisesDef.vdefaultfox == .t. AND !EMPTY(ALLTRIM(lcVdefault))
			TRY
				&lcObj..text1.value = ALLTRIM(lcVdefault)
			CATCH
				MESSAGEBOX("N�o foi possivel atribuir valor por defeito para um ou mais par�metros.",32,"Logitools Software")
			ENDTRY 
		ENDIF
		
	ENDSCAN
ENDFUNC


**
FUNCTION uf_wdgfiltroreport_renomeia
	IF USED("uCrsListaBrowser")
		LOCAL lcObjAlt, lcCont
		
		SELECT uCrsListaBrowser
		GO TOP
		SCAN
			lcObjAlt = ALLTRIM(STRTRAN(uCrsListaBrowser.nome, "container1.container1", "container1"))

			&lcObjAlt..name = &lcObjAlt..name + "_temp"
		ENDSCAN
		
		lcCont = 1
		SELECT uCrsListaBrowser
		GO TOP
		SCAN
			lcObjAlt = ALLTRIM(STRTRAN(uCrsListaBrowser.nome, "container1.container1", "container1")) + "_temp"
			IF lcCont == 1 
				&lcObjAlt..name = "wdgBrowser"
			ELSE
				&lcObjAlt..name = "wdgBrowser" + astr(lcCont)
			ENDIF
			lcCont = lcCont + 1
		ENDSCAN
	ENDIF 
ENDFUNC