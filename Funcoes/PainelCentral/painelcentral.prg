DECLARE Long FindWindow In Win32API String, String

DO wdgbrowser
DO wdgfiltroreport
DO wdgmensagens
DO wdgtitulo
DO detalhemensagem


**
FUNCTION uf_painelcentral_chama
	LPARAMETERS lcBD, lcUser, lcPass, lcComando, lcLoginAuto &&, lcAcivaTimerIntegracaoPHC
	
	PUBLIC myPainelCentralEdicao
	STORE .F. TO  myPainelCentralEdicao
	
	IF TYPE("painelcentral") == "U"		
		DO FORM painelcentral
		_screen.Visible = .T. && ignora a configura��o do ficheiro config.fpw
		
		painelcentral.lcBD = ALLTRIM(lcBD)
		painelcentral.lcUser = ALLTRIM(lcUser)
		painelcentral.lcPass = ALLTRIM(lcPass)
		painelcentral.lcComando = ALLTRIM(lcComando)
		painelcentral.lcLoginAuto = lcLoginAuto

		uf_login_chama()
		**		
	ELSE
		*painelcentral.show
	ENDIF
ENDFUNC 

** ACEITA��O DOS TERMOS DE UTILIZA��O PELO UTILIZADOR
FUNCTION uf_painelcentral_user_aceita
	lcSQL = ''
	TEXT TO lcSQL Noshow Textmerge
		UPDATE B_US SET aceita_termos = 1, dt_aceita_termos=dateadd(HOUR, <<difhoraria>>, getdate()) where iniciais='<<ALLTRIM(m_chinis)>>'
	ENDTEXT
	IF !uf_gerais_actGrelha("","curb_us",lcSql)			
		uf_perguntalt_chama("Aten��o: Ocorreu uma anomalia ao atualizar o acesso do utilizador. Por favor contacte o Suporte.","OK","",16)
		RETURN .f.
	ENDIF
	**painelcentral.menu1.estado("inicio, trocaEmpresa, trocaUser, relatorios, mensagens, suporte, sistema", "SHOW")
	*painelcentral.menu1.estado("inicio, relatorios, notificacoes, sistema", "SHOW")
	uf_painelcentral_alternaPagina(1)
ENDFUNC 


FUNCTION uf_painelcentral_user_recusa
	lcSQL = ''
	TEXT TO lcSQL Noshow Textmerge
		UPDATE B_US SET aceita_termos = 0, dt_aceita_termos=dateadd(HOUR, <<difhoraria>>, getdate()) where iniciais='<<ALLTRIM(m_chinis)>>'
	ENDTEXT
	IF !uf_gerais_actGrelha("","curb_us",lcSql)			
		uf_perguntalt_chama("Aten��o: Ocorreu uma anomalia ao atualizar o acesso do utilizador. Por favor contacte o Suporte.","OK","",16)
		RETURN .f.
	ENDIF
	uf_Logitools_sair()
ENDFUNC 

FUNCTION uf_painelcentral_manuais
	ShellExecute(0,"open","https://drive.google.com/drive/folders/1eNT0HlIb7fQe9BL7oQGBAHAo74e9qz_Q?usp=sharing","","",1)
ENDFUNC 

FUNCTION uf_painelcentral_circulares
	ShellExecute(0,"open","https://drive.google.com/drive/folders/1eNT0HlIb7fQe9BL7oQGBAHAo74e9qz_Q?usp=sharing","","",1)
ENDFUNC 


**
FUNCTION uf_painelcentral_carregamenu
	LPARAMETERS lcPag, lcBool, lcRecarrega
	
	LOCAL lcObj
	STORE "" TO lcObj

	*�painelcentral.menu1.visible = .t.
	*painelcentral.menu1.estado("","","gravar",.f.,"Terminar",.t.)
*!*		
*!*		IF TYPE("painelcentral.menu1.ctnUser") = "U"
*!*	      
*!*			painelcentral.menu1.newObject("ctnUser", "dadosUser", "dadosUser.vcx")

*!*			IF TYPE("painelcentral.menu1.ctnUser") <> "U"

*!*				painelcentral.menu1.ctnUser.visible = .t.
*!*				painelcentral.menu1.ctnUser.left = 0
*!*				painelcentral.menu1.ctnUser.top = 0
*!*				painelcentral.menu1.ctnUser.mousePointer = 15
*!*				painelcentral.menu1.ctnUser.lblLoja.mousePointer = 15
*!*				painelcentral.menu1.ctnUser.lblUser.mousePointer = 15

*!*			ENDIF

*!*			painelcentral.menu1.posultobj = 65
*!*			painelcentral.newobject("menu_empresaUser", "menu_opcoes", "menu.vcx")

*!*			WITH painelcentral.menu_empresaUser

*!*				.adicionaOpcao("mudarEmpresa","Mudar Loja","","uf_painelCentral_alteraLoja")	
*!*				.adicionaOpcao("mudarUtilizador","Mudar Utilizador","","uf_painelCentral_alteraUser")	
*!*				.left = painelcentral.menu1.left - (.width + 5)
*!*				.top = 15
*!*				.label1.visible = .F.
*!*				.line1.visible = .F.
*!*				.mudarEmpresa.top = 0
*!*				.mudarUtilizador.top = .mudarEmpresa.height
*!*				.height = .mudarEmpresa.height + .mudarUtilizador.height
*!*				
*!*			ENDWITH

*!*	    ELSE
*!*	        WITH painelcentral.menu1.ctnUser
*!*	            SELECT ucrsE1
*!*	            .lblLoja.caption = IIF(!EMPTY(ucrsE1.abrev), ALLTRIM(ucrsE1.abrev), ALLTRIM(mySite))
*!*	            IF USED("ucrsUser")
*!*	                SELECT ucrsUser
*!*	                .lblUser.caption = ALLTRIM(ucrsUser.vendnm)
*!*	            ELSE

*!*	                IF TYPE("m_chusernm") = 'C'
*!*	                    .lblUser.caption = ALLTRIM(m_chusernm)
*!*	                ELSE
*!*	                    .lblUser.caption = ''
*!*	                ENDIF

*!*	            ENDIF
*!*	        ENDWITH
*!*		ENDIF

	&& Inicio
*!*		IF TYPE("painelcentral.menu1.inicio") == "U" AND ALLTRIM(ucrse1.tipoempresa) != 'PORTAL'
*!*			painelcentral.menu1.adicionaOpcao("inicio","Inicio",myPath + "\imagens\icons\new\inicio.png","uf_painelcentral_alternaPagina with 1")
*!*		ENDIF
	
	&& Relatorios
*!*		IF TYPE("painelcentral.menu1.relatorios") == "U" AND ALLTRIM(ucrse1.tipoempresa) != 'PORTAL'
*!*			painelcentral.menu1.adicionaOpcao("relatorios","Relat�rios",myPath + "\imagens\icons\new\relatorios.png","uf_analises_chama")
*!*		ENDIF

	&& Mensagens e Suporte
	**IF TYPE("painelcentral.menu1.mensagens") == "U"
	**	painelcentral.menu1.adicionaOpcao("mensagens","Mensagens",myPath + "\imagens\icons\new\suporte.png","")
	**ENDIF
*!*		IF TYPE("painelcentral.menu1.notificacoes") == "U" AND ALLTRIM(ucrse1.tipoempresa) != 'PORTAL'
*!*			painelcentral.menu1.adicionaOpcao("notificacoes","Notifica��es",myPath + "\imagens\icons\detalhe.png","uf_notificacoes_chama")
*!*		ENDIF


	&& Sistema
*!*		IF TYPE("painelcentral.menu1.sistema") == "U" AND ALLTRIM(ucrse1.tipoempresa) != 'PORTAL'
*!*			painelcentral.menu1.adicionaOpcao("sistema","Sistema",myPath + "\imagens\icons\new\sistema.png","uf_painelcentral_alternaPagina with 3")
*!*		ENDIF


	&& Alterar Empresa
*!*		IF ALLTRIM(ucrse1.tipoempresa) != 'PORTAL'
*!*			IF TYPE("painelcentral.menu1.trocaEmpresa") == "U" 
*!*				painelcentral.menu1.adicionaOpcao("trocaEmpresa", Proper(mySite) ,myPath + "\imagens\icons\new\empresa.png","uf_gerais_alterarEmpresa")
*!*			ELSE
*!*				painelcentral.menu1.trocaEmpresa.config(Proper(mySite) ,myPath + "\imagens\icons\new\empresa.png","uf_gerais_alterarEmpresa")
*!*			ENDIF
*!*		ENDIF 
	
*!*		painelcentral.menu1.trocaEmpresa.visible = .f.

	&& Alterar Utilizador
*!*		IF ALLTRIM(ucrse1.tipoempresa) != 'PORTAL'
*!*			IF TYPE("painelcentral.menu1.trocaUser") == "U" 
*!*				painelcentral.menu1.adicionaOpcao("trocaUser",ch_vendnm, myPath + "\imagens\icons\new\utilizador.png","uf_gerais_trocaUtilizador")
*!*			ELSE
*!*				painelcentral.menu1.trocaUser.config(ch_vendnm, myPath + "\imagens\icons\new\utilizador.png","uf_gerais_trocaUtilizador")
*!*			ENDIF	
*!*		ENDIF 

*!*		painelcentral.menu1.trocaUser.visible = .f.


	fecha("uCrsPCentral")
	
	uf_startup_sqlExec("select * from b_pcentral where terminal = '"+astr(myTermNo)+"'", "uCrsPCentral")
	
	fecha("uCrsNPag")
	SELECT distinct pagina FROM uCrsPCentral INTO CURSOR uCrsNPag READWRITE
	
	SELECT uCrsNPag

	LOCATE  FOR uCrsNPag.pagina == lcPag
	IF FOUND()
		
		painelcentral.pagina = uCrsNPag.pagina
		
		lcObj = "pagina"+astr(uCrsNPag.pagina) 

		IF TYPE("painelcentral."+lcObj) == "U" OR lcRecarrega
			
			IF !lcRecarrega
				painelcentral.newobject(lcObj, "pagina", "pcentral.vcx")
				painelcentral.&lcObj..top = 25
				painelcentral.&lcObj..width = painelcentral.width
				painelcentral.&lcObj..height = painelcentral.height
				painelcentral.&lcObj..visible = .f.
				painelcentral.&lcObj..anchor = 67 &&195


			ENDIF

			IF lcRecarrega AND lcPag = 1

				DO WHILE PAINELCENTRAL.PAGINA1.Container1.ControlCount > 0

					FOR EACH uv_obj IN PAINELCENTRAL.PAGINA1.Container1.objects

						PAINELCENTRAL.PAGINA1.Container1.REMOVEOBJECT(uv_obj.name)

					NEXT

				ENDDO

			ENDIF
			
			SELECT uCrsPCentral
			SCAN FOR uCrsPCentral.pagina == uCrsNPag.pagina
			
				IF uCrsPCentral.inactivo == .f.

					IF lcRecarrega

						IF TYPE("painelcentral."+ALLTRIM(lcObj)+"."+ALLTRIM(uCrsPCentral.nome)) <> "U"
							uv_curObject = "painelcentral."+ALLTRIM(lcObj)
							&uv_curObject..removeObject(ALLTRIM(uCrsPCentral.nome))
						ENDIF

					ENDIF
			
					painelcentral.&lcObj..adicionaopcao(ALLTRIM(uCrsPCentral.nome),ALLTRIM(uCrsPCentral.tipo),uCrsPCentral.colIni,uCrsPCentral.colFin,uCrsPCentral.linIni,;
														uCrsPCentral.linFin,ALLTRIM(uCrsPCentral.imagem),ALLTRIM(uCrsPCentral.imagem2),ALLTRIM(uCrsPCentral.texto),;
														ALLTRIM(uCrsPCentral.comando),uCrsPCentral.backStyle,ALLTRIM(uCrsPCentral.backColor),ALLTRIM(uCrsPCentral.borderColor),;
														uCrsPCentral.borderWidth,uCrsPCentral.comandosql,uCrsPCentral.imagemSimbolo,ALLTRIM(uCrsPCentral.foreColor))
				ENDIF
			ENDSCAN
			painelcentral.&lcObj..visible = .t.
		ENDIF
	ENDIF
	
	&& hack para carregar p�gina sem ter que actualizar painel
	lcObj = "painelcentral.pagina" + astr(lcPag)
	IF &lcObj..contembrowser == 1
		regua(0,2,"A Carregar Browser")
		regua(1,1,"A Carregar Browser")
		regua(1,2,"A Carregar Browser")
		regua(2)
	ENDIF 

	**
	uf_gerais_atualizaNomeEmpresaPainelCentral()
	
	
	**regista utilizador ao abrir o painel central
	LOCAL lcMsgLog
	lcMsgLog="Registo Painel Central"
	IF vartype(m_chinis)!='U' and !EMPTY(ALLTRIM(m_chinis))
		lcMsgLog = lcMsgLog + " -> " + ALLTRIM(m_chinis)
	ENDIF
	
	uf_gerais_registaerrolog(lcMsgLog, "Login")
ENDFUNC

**********************************
**		Logout do utilizador	**
**********************************
FUNCTION uf_painelcentral_logout
	LPARAMETERS lcObj

	m.ch_vendedor		= 0
	m.ch_vendnm			= ''
	m.m_chnome			= ''
	m.m_chinis			= ''
	m.ch_userno			= 0
	m.ch_grupo			= ''
	myLingua			= 'ptPT'
	emDesenvolvimento 	= .f.
	m_chusernm			= ''
		 
	&& ALTERA O BOTAO DE AUTENTICA��O   
    &lcObj..lblVendnm.caption	=	UPPER(alltrim(m.ch_vendnm))
    &lcObj..lblVendedor.caption	=	UPPER(astr(m.ch_vendedor))
    	
    && autenticacao �nica
	uf_painelcentral_LoginUnica(lcObj)  		
ENDFUNC

******************************************
**		Controla autentica��o �nica		**
******************************************
FUNCTION uf_painelcentral_LoginUnica
	LPARAMETERS lcObj
	&& valida login unica
	IF (myLoginUnica AND UPPER(ALLTRIM(myuserloginunica)) == UPPER(ALLTRIM(m.ch_vendnm))) OR m.m_chnome == ''
		DO WHILE (UPPER(ALLTRIM(myuserloginunica)) == UPPER(ALLTRIM(m.ch_vendnm))) OR m.m_chnome == ''
			uf_painelcentral_login(lcObj)
		ENDDO
	ENDIF
ENDFUNC


** Fun��o de login de utilizador 
FUNCTION uf_painelcentral_login
	LPARAMETERS lcObj
	
	LOCAL cval, lcValidaPwOp
	STORE '' TO cval
	STORE 0 TO lcValidaPwOp
	
	PUBLIC m_chusernm
	m_chusernm = ''

	IF myDocIntroducao == .t. or myDocAlteracao == .t. or myFtIntroducao == .t. or myFtAlteracao == .t.
		uf_perguntalt_chama("N�o � poss�vel trocar de utilizador durante a inser��o / edi��o de documentos.","OK","", 48)
	ELSE
		PUBLIC myBackOpPass
		STORE '' TO myBackOpPass
		uf_tecladoalpha_Chama('myBackOpPass', 'INTRODUZA PASSWORD DE OPERADOR:', .t., .f., 0)
		
		cval = myBackOpPass

		IF EMPTY(mySite)
			mySite = ''
		ENDIF

		&& VALIDAR E GUARDAR DADOS DO OPERADOR
		IF !EMPTY(ALLTRIM(cval))
			IF uf_gerais_getParameter('ADM0000000303', 'BOOL') = .f.
			    lcSQL = [exec up_login_autenticaUser ']+alltrim(cval)+[','',']+ALLTRIM(mySite)+[']
			    If !uf_gerais_actgrelha("", "uCrsUserTMP", lcSQL)
			        uf_perguntalt_chama("Ocorreu uma anomalia a verificar os dados do utilizador."+Chr(13)+Chr(10)+"Se o problema persistir por favor contacte o Suporte.","OK","",16)
					RETURN .F.
			    ENDIF 
				SELECT * FROM uCrsUserTMP WITH (BUFFERING = .T.) INTO CURSOR uCrsUserTMP_safe
		   ELSE 
		   		LOCAL lcencryptedpassw
		   		lcencryptedpassw = uf_gerais_encrypt(cval) 
		   		lcSQL = [exec up_login_autenticaUser_altera ']+alltrim(lcencryptedpassw)+[','',']+ALLTRIM(mySite)+[']
		   	
		   		If !uf_gerais_actgrelha("", "uCrsUserTMP", lcSQL)
		       	 	uf_perguntalt_chama("Ocorreu uma anomalia a verificar os dados do utilizador."+Chr(13)+Chr(10)+"Se o problema persistir por favor contacte o Suporte.","OK","",16)
					RETURN .F.
			    ELSE
			   		LOCAL lcdecryptedpassw
					lcdecryptedpassw = uf_gerais_decrypt(uCrsUserTMP.pwpos)
					IF ALLTRIM(lcdecryptedpassw) != alltrim(cval)
						SELECT uCrsUserTMP
						DELETE ALL 
					ELSE
						replace uCrsUserTMP.pwpos WITH ALLTRIM(lcdecryptedpassw )
					ENDIF 
				ENDIF 

				SELECT * FROM uCrsUserTMP WITH (BUFFERING = .T.) INTO CURSOR uCrsUserTMP_safe
		   ENDIF 

            IF !EMPTY(mysite)
                SELECT uCrsUserTMP_safe
                GO TOP 
                lcSQL = ""
                TEXT TO lcSQL TEXTMERGE NOSHOW
                    exec up_gerais_trocaEmpresa <<IIF(EMPTY(uCrsUserTMP_safe.userno), '-1', uCrsUserTMP_safe.userno)>>,'<<ALLTRIM(uCrsUserTMP_safe.grupo)>>'
                ENDTEXT
                If !uf_gerais_actGrelha("", "ucrsTrocaEmpresaTemp", lcSql)
                    uf_perguntalt_chama("N�o foi possivel verificar as Empresas dispon�veis.", "", "OK", 16)
                    RETURN .f.
                ENDIF
                
                SELECT ucrsTrocaEmpresaTemp
                GO TOP 
				COUNT FOR !DELETED() TO uv_count 

				IF uv_count = 0
			        uf_perguntalt_chama("A PASSWORD N�O EST� CORRECTA."+Chr(13)+Chr(10)+"VERIFIQUE SE ESCREVEU A PASSWORD CORRECTAMENTE.","OK","",48)
			        uf_gerais_trocaUtilizador()
				ENDIF

                LOCATE FOR ALLTRIM(mySite) == ALLTRIM(ucrsTrocaEmpresaTemp.local)
                IF !FOUND()	
                    uf_perguntalt_chama("O utilizador n�o pertence � loja atual. N�o � possivel trocar de utilizador.", "", "OK", 16)
					IF USED("uCrsUserTMP_safe")
						FECHA("uCrsUserTMP_safe")
					ENDIF
                    RETURN 2
					**uf_gerais_trocaUtilizador()
                ENDIF 		   
			
            ENDIF 

			SELECT uCrsUserTMP_safe
			GO TOP  
			COUNT FOR !DELETED() TO uv_count 
			IF uv_count > 0

				IF USED("uCrsUser")
					FECHA("uCrsUser")
				ENDIF

				SELECT * FROM uCrsUserTMP_safe WITH (BUFFERING=.T.) INTO CURSOR uCrsUser READWRITE
			ENDIF 


*!*			    If !uf_gerais_actgrelha("", "uCrsUser", lcSQL)
*!*			        uf_perguntalt_chama("Ocorreu uma anomalia a verificar os dados do utilizador."+Chr(13)+Chr(10)+"Se o problema persistir por favor contacte o Suporte.","OK","",16)
*!*			    ELSE
		    
			    **IF Reccount()>0 && se encontrou operador
			     LOCAL lnReccount
				 SELECT uCrsUser 

				COUNT FOR !DELETED() TO lnReccount
				IF lnReccount>0

					
					
					SELECT uCrsUser
					GO TOP 
					
			        && ALTERAR VARI�VEIS DE SISTEMA
		    		ch_vendedor		= uCrsUser.userno
			        ch_vendnm    	= LEFT(ALLTRIM(uCrsUser.nome),20)
			        m_chnome     	= uCrsUser.nome
			        m_chinis     	= uCrsUser.iniciais
			        ch_userno    	= uCrsUser.userno
			        ch_grupo     	= uCrsUser.grupo
		    		ch_especialista = uCrsUser.userno
		    		m_chusernm 		= ALLTRIM(uCrsUser.vendnm)
		    		myLingua		= 'ptPT'

                    FOR EACH uo_obj IN _SCREEN.Forms
                        IF TYPE("uo_obj.menu1.ctnUser") <> "U"
                        	IF uo_obj.menu1.ctnUser.visible
	                            uo_obj.menu1.ctnUser.lblUser.init()
	                        ENDIF
                        ENDIF
                    NEXT
		    		
		    		&& Atualizar Perfis para o novo utilizador
		    		IF USED("uCrsPfUtilizador")
		    			fecha("uCrsPfUtilizador")
		    		ENDIF

					TEXT TO lcSQL NOSHOW TEXTMERGE
						exec up_perfis_validaAcessoPaineis '<<mySite>>'
					ENDTEXT 

					IF !uf_gerais_actgrelha("", "uCrsPfUtilizador", lcSQL)
						MESSAGEBOX("OCORREU UMA ANOMALIA AO VERIFICAR OS PERFIS DE UTILIZADOR.", 16, "LOGITOOLS SOFTWARE")
						RETURN .f.
					ENDIF 
			        		
			        && ALTERA O BOTAO DE AUTENTICA��O
			        && widget
			        IF TYPE("lcObj") == "C"
				        IF !EMPTY(ALLTRIM(lcObj))
				        	&lcObj..init
				        ENDIF
				    ENDIF
			        
			        &&uf_atendimento_configOperador()
			        	
			        && atendimento
			        LOCAL lcResult

			        
		        	IF !TYPE("ATENDIMENTO") == "U"
		        		lcResult  = uf_atendimento_configOperador()		        		
		        	ELSE 
			        	&& SsStamp - atualiza qd se troca de utilizador no PCentral
			        	IF uCrsE1.gestao_cx_operador = .t.
			        		lcSQL = ''
				        	TEXT TO lcSQL NOSHOW TEXTMERGE 
				        		exec up_caixa_verificaCaixaAberta '<<ALLTRIM(mysite)>>', '<<ALLTRIM(ch_vendnm)>>', 1
				        	ENDTEXT
				        	
				        	
				        	IF !uf_gerais_actgrelha("", "uCrsValidaSsStamp", lcSQL)
								MESSAGEBOX("OCORREU UMA ANOMALIA AO CONFIGURAR A CAIXA DO UTILIZADOR", 16, "LOGITOOLS SOFTWARE")
								fecha("uCrsValidaSsStamp")
								RETURN .f.
							ELSE
								IF RECCOUNT("uCrsValidaSsStamp") > 0
									mySsStamp = ALLTRIM(uCrsValidaSsStamp.SsStamp)
								ENDIF
								fecha("uCrsValidaSsStamp")					
							ENDIF 
						ENDIF
		        	ENDIF
        			
        			
        			lcValidaPwOp = 1
        			if(!EMPTY(lcResult) AND VARTYPE(lcResult)=='N')
        				lcValidaPwOp = lcResult	
		        	ENDIF
		        	
		    		
			    ELSE
			        uf_perguntalt_chama("A PASSWORD N�O EST� CORRECTA."+Chr(13)+Chr(10)+"VERIFIQUE SE ESCREVEU A PASSWORD CORRECTAMENTE.","OK","",48)
			        uf_gerais_trocaUtilizador()
			        **uf_tecladoalpha_Chama('myBackOpPass', 'INTRODUZA PASSWORD DE OPERADOR:', .t., .f., 0)
			    ENDIF 
			    
		        IF USED("uCrsUserTMP")
					FECHA("uCrsUserTMP")
				ENDIF
*!*			   ENDIF 
		ELSE
	        uf_perguntalt_chama("A PASSWORD EST� VAZIA."+Chr(13)+Chr(10)+"TEM DE INSERIR UMA PASSWORD V�LIDA.","OK","",48)
	        uf_gerais_trocaUtilizador()
	**		lcValidaPwOp = 2
		ENDIF
	ENDIF 
	
	myBackOpPass = ''
	
	RETURN lcValidaPwOp

ENDFUNC


**
FUNCTION uf_painelcentral_quit

	&& Caso seja fechado a partir da aplica��o deve pedir autentica��o
	painelcentral.lcLoginAuto = .f.

	FOR EACH mf IN painelcentral.objects
		IF LIKE("*PAGINA*",UPPER(ALLTRIM(mf.name))) == .t.
			painelcentral.removeObject(mf.Name)
		ENDIF
	ENDFOR
*!*		painelcentral.menu1.visible = .f.
*!*		
	&&fechar paineis
	create cursor uCrsPaineis (nome c(100))
	for i=1 to _SCREEN.FormCount
		select uCrsPaineis
		append blank
		replace uCrsPaineis.nome with _SCREEN.Forms(i).name
	ENDfor
	local lcPainel
	select distinct nome from uCrsPaineis into cursor uCrsPaineisFechar readwrite
	select uCrsPaineisFechar
	scan
		if !(upper(alltrim(uCrsPaineisFechar.nome)) == "PAINELCENTRAL") &&and !(upper(alltrim(uCrsPaineisFechar.nome)) == "APTEC")
			lcPainel = uCrsPaineisFechar.nome
			TRY
				&lcPainel..release()
			CATCH
			ENDTRY
		ENDIF
	ENDSCAN

 	** Desactiva Timer de verifica��o Tabela Integracao
   	Painelcentral.timerIntegracaoPhc.enabled = .f.

	** Limpa cursores
	lcCursor = ""
	FOR x = 1 TO AUSED(aTableAliases,1)
		lcCursor = aTableAliases(x)
		IF x%2!=0
			IF used(lcCursor)
				fecha(lcCursor)
			ENDIF
		ENDIF
	NEXT

	**Terminar liga��o � BD
	TRY
		SQLDISCONN(gnConnHandle)
	CATCH
	ENDTRY
	IF !USED("ucrse1")
		uf_login_chama()
	ELSE
		IF ALLTRIM(ucrse1.tipoempresa) != 'PORTAL'
			uf_login_chama()
		ELSE
			uf_logitools_sair()
		ENDIF 
	ENDIF 
ENDFUNC


**
FUNCTION uf_painelcentral_alternaPagina
	LPARAMETERS lcPag

	IF lcPag == painelcentral.pagina
		RETURN .t.
	ENDIF 
	
	SELECT uCrsPCentral
	LOCATE FOR uCrsPCentral.pagina == lcPag
	IF !FOUND()
		RETURN .f.
	ENDIF 
	
	regua(0,5,"A carregar Painel Central...")
	regua(1,1,"A carregar Painel Central...")
	LOCAL lcObj

	&&configura bot�o Voltar
*!*		IF lcPag > 10
*!*			painelcentral.menu1.voltar.config("Voltar","voltar_w.png","uf_painelcentral_alternaPagina with " + astr(painelcentral.pagina))
*!*		ENDIF 
	
	regua(1,2,"A carregar Painel Central...")
	
	painelcentral.removeObject("PAGINA"+astr(painelcentral.pagina))
	
	regua(1,3,"A carregar Painel Central...")
	
	&&
	uf_painelcentral_carregamenu(lcPag, .f.)
	
	regua(1,4,"A carregar Painel Central...")
	
	SELECT uCrsNPag
	LOCATE FOR uCrsNPag.pagina == lcPag
	IF !FOUND()
		regua(2)
		RETURN .f.
	ENDIF 
	
	regua(1,5,"A carregar Painel Central...")
	
	*lcObj = "painelcentral.pagina" + astr(lcPag)
	*&lcObj..refresh
	
	regua(2)
ENDFUNC


**
FUNCTION uf_painelcentral_VerificaComandoPhc
	
	LOCAL lcComando, lcSql 
	STORE '' TO lcComando, lcSql  

	lcClientName = GETENV("CLIENTNAME")
	lcCurDir = CURDIR()

	TEXT TO lcSQL TEXTMERGE noshow
		exec up_integracaophc_verificaComandosPhc '<<lcClientName>>',<<ch_userno>>
	ENDTEXT
	

	IF !uf_gerais_actGrelha("", "uCrsComandosIntegracaoPhc", lcSQL)
		RETURN .f.
	ENDIF 
	IF RECCOUNT("uCrsComandosIntegracaoPhc")>0
		uf_integracaoPhc_bringToFront()
	ENDIF 
	SELECT uCrsComandosIntegracaoPhc
	GO Top
	SCAN
		IF !EMPTY(ALLTRIM(uCrsComandosIntegracaoPhc.comando))
		
			TEXT TO lcSQL TEXTMERGE noshow
				exec up_integracaophc_UpdateComandosPhc '<<ALLTRIM(uCrsComandosIntegracaoPhc.id)>>'
			ENDTEXT
			IF !uf_gerais_actGrelha("", "uCrsUpdateIntegracaoPhc", lcSQL)
				RETURN .f.
			ENDIF 
			
			lcComando = ALLTRIM(uCrsComandosIntegracaoPhc.comando)
			&lcComando 
		ENDIF 
	ENDSCAN	
	
	IF USED("uCrsComandosIntegracaoPhc")
		fecha("uCrsComandosIntegracaoPhc")		
	ENDIF 
	IF USED("uCrsUpdateIntegracaoPhc")
		fecha("uCrsUpdateIntegracaoPhc")		
	ENDIF 
ENDFUNC 


**
FUNCTION uf_integracaoPhc_bringToFront
	lcscreenCaption = [Logitools Software]
	nHWND = FindWindow(Null,lcscreenCaption)

	If nHWND >0
	    =uf_integracaoPhc_ForceForegroundWindow(nHWND)
	ENDIF
ENDFUNC


**
FUNCTION uf_integracaoPhc_ForceForegroundWindow(lnHWND)

    LOCAL nForeThread, nAppThread
    
    =uf_integracaoPhc_Decl()
    
    nForeThread = GetWindowThreadProcessId(GetForegroundWindow(), 0)
    nAppThread = GetCurrentThreadId()

    IF nForeThread != nAppThread
        AttachThreadInput(nForeThread, nAppThread, .T.)
        BringWindowToTop(lnHWND)
        ShowWindow(lnHWND,3)
        AttachThreadInput(nForeThread, nAppThread, .F.)
    ELSE
        BringWindowToTop(lnHWND)
        ShowWindow(lnHWND,3)
    ENDIF
    
ENDFUNC


**
FUNCTION uf_integracaoPhc_Decl
*!* DECLARE INTEGER SetForegroundWindow IN user32 INTEGER hwnd 
 
    DECLARE Long BringWindowToTop In Win32API Long

    DECLARE Long ShowWindow In Win32API Long, Long

    DECLARE INTEGER GetCurrentThreadId; 
        IN kernel32
     
    DECLARE INTEGER GetWindowThreadProcessId IN user32; 
        INTEGER   hWnd,; 
        INTEGER @ lpdwProcId  
     
    DECLARE INTEGER GetCurrentThreadId; 
        IN kernel32  
        
    DECLARE INTEGER AttachThreadInput IN user32 ;
        INTEGER idAttach, ;
        INTEGER idAttachTo, ;
        INTEGER fAttach

    DECLARE INTEGER GetForegroundWindow IN user32  
    
ENDFUNC

FUNCTION uf_painelcentral_start_suporte
	DECLARE INTEGER ShellExecute IN shell32.dll ; 
  	INTEGER hndWin, ; 
  	STRING cAction, ; 
  	STRING cFileName, ; 
  	STRING cParams, ;  
  	STRING cDir, ; 
	INTEGER nShowWin
	ShellExecute(0,"open","c:\Logitools\lauchsuporte.bat","","",1)
	ShellExecute(0,"open","d:\Logitools\lauchsuporte.bat","","",1)
ENDFUNC 

FUNCTION uf_painelcentral_restart_robot
	DECLARE INTEGER ShellExecute IN shell32.dll ; 
  	INTEGER hndWin, ; 
  	STRING cAction, ; 
  	STRING cFileName, ; 
  	STRING cParams, ;  
  	STRING cDir, ; 
	INTEGER nShowWin
	ShellExecute(0,"runas","c:\Logitools\restartRobot.bat","","",1)
	ShellExecute(0,"runas","d:\Logitools\restartRobot.bat","","",1)
ENDFUNC 

PROCEDURE uf_painelcentral_mostraOpcoes

    IF TYPE("PAINELCENTRAL.MENU_EMPRESAUSER") <> "U"

        PAINELCENTRAL.MENU_EMPRESAUSER.visible = .t.
        PAINELCENTRAL.MENU_EMPRESAUSER.mudarEmpresa.top = 0
		PAINELCENTRAL.MENU_EMPRESAUSER.mudarUtilizador.top = PAINELCENTRAL.MENU_EMPRESAUSER.mudarEmpresa.height
		PAINELCENTRAL.MENU_EMPRESAUSER.height = PAINELCENTRAL.MENU_EMPRESAUSER.mudarEmpresa.height + PAINELCENTRAL.MENU_EMPRESAUSER.mudarUtilizador.height

        PAINELCENTRAL.MENU_EMPRESAUSER.zorder(0)

    ENDIF

ENDPROC

PROCEDURE uf_painelCentral_alteraLoja

    uf_gerais_alterarEmpresa()

*!*		painelCentral.menu1.ctnUser.lblLoja.init()

	PAINELCENTRAL.LOCKSCREEN = .T.


	CREATE CURSOR uCrsPaineis (nome c(100))
	FOR i=1 TO _SCREEN.FormCount
		SELECT uCrsPaineis
		APPEND BLANK
		REPLACE uCrsPaineis.nome with _SCREEN.Forms(i).name
	ENDFOR


	SELECT DISTINCT nome FROM uCrsPaineis INTO CURSOR uCrsPaineisFechar READWRITE
	SELECT uCrsPaineisFechar
	GO TOP

	SCAN FOR !uf_gerais_compStr(uCrsPaineisFechar.nome, "PAINELCENTRAL")
		lcPainel = uCrsPaineisFechar.nome
		TRY
			&lcPainel..release()
		CATCH
		ENDTRY
	ENDSCAN

	FECHA("uCrsPaineis")
	FECHA("uCrsPaineisFechar")
	
	IF !uf_gerais_getparameter_site('ADM0000000084', 'BOOL', mySite)
	
		TEXT TO lcSQL1 NOSHOW TEXTMERGE
        	delete from b_pcentral
        ENDTEXT
        
        uf_gerais_actgrelha("", "", lcSQL1)
	
		TEXT TO lcSQL NOSHOW TEXTMERGE
			exec up_manutencao_ActualizaPainelCentralPelosTerminais 0, '<<mySite>>', <<IIF(TYPE("myTermNo") == "U", 0, myTermNo)>>, <<ch_userno>>
		ENDTEXT 
	ELSE
		TEXT TO lcSQL1 NOSHOW TEXTMERGE
        	delete from b_pcentral
        ENDTEXT
        uf_gerais_actgrelha("", "", lcSQL1)
            
		IF ALLTRIM(ch_grupo) = 'Operadores'
			TEXT TO lcSQL NOSHOW TEXTMERGE
				exec up_manutencao_ActualizaPainelCentralPelosTerminais_soatendimento 0, '<<mySite>>'
			ENDTEXT
		ELSE
			TEXT TO lcSQL NOSHOW TEXTMERGE
				exec up_manutencao_ActualizaPainelCentralPelosTerminais 0, '<<mySite>>', <<IIF(TYPE("myTermNo") == "U", 0, myTermNo)>>, <<ch_userno>>
			ENDTEXT
		ENDIF 
	ENDIF 
	IF !uf_gerais_actgrelha("", "", lcSQL)
		MESSAGEBOX("OCORREU UMA ANOMALIA A CONFIGURAR O PAINEL CENTRAL.", 16, "LOGITOOLS SOFTWARE")
		PAINELCENTRAL.LOCKSCREEN = .F.
		RETURN .f.
	ENDIF 


	uf_painelcentral_carregamenu(1, .F., .T.)

	uf_painelcentral_alternaPagina(1)

	PAINELCENTRAL.LOCKSCREEN = .F.

ENDPROC

PROCEDURE uf_painelCentral_alteraUser

    uv_curUser = m_chinis

    uf_gerais_trocaUtilizador()
    IF(UPPER(ALLTRIM(uv_curUser)) != UPPER(ALLTRIM(m_chinis)))
*!*	        painelCentral.menu1.ctnUser.lblUser.init()

		PAINELCENTRAL.LOCKSCREEN = .T.
		uf_painelcentral_alternaPagina(1)

		IF !uf_gerais_getparameter_site('ADM0000000084', 'BOOL', mySite)
		
			TEXT TO lcSQL1 NOSHOW TEXTMERGE
				delete from b_pcentral  WHERE terminal = <<IIF(TYPE("myTermNo") == "U", 0, myTermNo)>>
			ENDTEXT
			
			uf_gerais_actgrelha("", "", lcSQL1)
		
			TEXT TO lcSQL NOSHOW TEXTMERGE
				exec up_manutencao_ActualizaPainelCentralPelosTerminais 0, '<<mySite>>', <<IIF(TYPE("myTermNo") == "U", 0, myTermNo)>>, <<ch_userno>>
			ENDTEXT 
		ELSE
			TEXT TO lcSQL1 NOSHOW TEXTMERGE
				delete from b_pcentral where terminal = <<myTermNo>>
			ENDTEXT
			uf_gerais_actgrelha("", "", lcSQL1)
				
			IF ALLTRIM(ch_grupo) = 'Operadores'
				TEXT TO lcSQL NOSHOW TEXTMERGE
					exec up_manutencao_ActualizaPainelCentralPelosTerminais_soatendimento 0, '<<mySite>>'
				ENDTEXT
			ELSE
				TEXT TO lcSQL NOSHOW TEXTMERGE
					exec up_manutencao_ActualizaPainelCentralPelosTerminais 0, '<<mySite>>', <<IIF(TYPE("myTermNo") == "U", 0, myTermNo)>>, <<ch_userno>>
				ENDTEXT
			ENDIF 
		ENDIF 
		IF !uf_gerais_actgrelha("", "", lcSQL)
			MESSAGEBOX("OCORREU UMA ANOMALIA A CONFIGURAR O PAINEL CENTRAL.", 16, "LOGITOOLS SOFTWARE")
			PAINELCENTRAL.LOCKSCREEN = .F.
			RETURN .f.
		ENDIF 

		uf_painelcentral_carregamenu(1, .F., .T.)
		uf_painelcentral_alternaPagina(1)
		PAINELCENTRAL.LOCKSCREEN = .F.
    ENDIF

ENDPROC


FUNCTION uf_painelcentral_editar
	
	IF myPainelCentralEdicao 
		myPainelCentralEdicao = .F.
	ELSE 
		myPainelCentralEdicao = .T.
	ENDIF 
	
	
	lcSQL = ""
    TEXT TO lcSQL TEXTMERGE NOSHOW
        select 
        	nome, comando 
        from b_pcentral (nolock) 
        where 
        	nome like '%adiciona%' 
        	and terminal = <<mytermno>> 
        	and pagina = 1
    ENDTEXT
    If !uf_gerais_actGrelha("", "ucrsAtalhosPc", lcSql)
        uf_perguntalt_chama("N�o foi possivel verificar as op��es do Painel Central.", "", "OK", 16)
        RETURN .f.
    ENDIF
	
	
	lcSQL = ""
    TEXT TO lcSQL TEXTMERGE NOSHOW
        select 
        	nome 
        from b_pcentral (nolock) 
        where 
        	nome not like '%adiciona%' 
        	and terminal = <<mytermno>> 
        	and pagina = 1
        	and tipo = 'A'
    ENDTEXT
    If !uf_gerais_actGrelha("", "ucrsFixosPc", lcSql)
        uf_perguntalt_chama("N�o foi possivel verificar as Empresas dispon�veis.", "", "OK", 16)
        RETURN .f.
    ENDIF
	IF myPainelCentralEdicao 

			SELECT ucrsFixosPc
			GO TOP 
			SCAN 
				lcObj = ALLTRIM(ucrsFixosPc.nome)
				PainelCentral.PAGINA1.CONTAINER1.&lcObj..Enabled = .F.			
				PainelCentral.PAGINA1.CONTAINER1.&lcObj..Image1.Enabled = .F.
				PainelCentral.PAGINA1.CONTAINER1.&lcObj..Label1.Enabled = .F.
				
			ENDSCAN  
			
			SELECT ucrsAtalhosPc
			GO TOP 
			SCAN 
			
				lcObj = ALLTRIM(ucrsAtalhosPc.nome)
				PainelCentral.PAGINA1.CONTAINER1.&lcObj..comando = "uf_painelcentral_escolheatalho with '" + lcObj + "'"
				PainelCentral.PAGINA1.CONTAINER1.&lcObj..BorderColor = RGB(255,94,91)
			ENDSCAN 
			
			PainelCentral.PAGINA1.CONTAINER1.editar.image1.picture = mypath + "\imagens\painelcentral\editar_menu_c.png"
			
		
			
		
	
	ELSE  
	
		
		
		SELECT ucrsFixosPc
		GO TOP 
		SCAN 
			lcObj = ALLTRIM(ucrsFixosPc.nome)
			PainelCentral.PAGINA1.CONTAINER1.&lcObj..Enabled = .T.
			PainelCentral.PAGINA1.CONTAINER1.&lcObj..Image1.Enabled = .T.
			PainelCentral.PAGINA1.CONTAINER1.&lcObj..Label1.Enabled = .T.

		ENDSCAN
		
			PAINELCENTRAL.LOCKSCREEN = .T.
		uf_painelcentral_alternaPagina(1)

		IF !uf_gerais_getparameter_site('ADM0000000084', 'BOOL', mySite)
		
			TEXT TO lcSQL1 NOSHOW TEXTMERGE
				delete from b_pcentral  WHERE terminal = <<IIF(TYPE("myTermNo") == "U", 0, myTermNo)>>
			ENDTEXT
			
			uf_gerais_actgrelha("", "", lcSQL1)
		
			TEXT TO lcSQL NOSHOW TEXTMERGE
				exec up_manutencao_ActualizaPainelCentralPelosTerminais 0, '<<mySite>>', <<IIF(TYPE("myTermNo") == "U", 0, myTermNo)>>, <<ch_userno>>
			ENDTEXT 
		ELSE
			TEXT TO lcSQL1 NOSHOW TEXTMERGE
				delete from b_pcentral where terminal = <<myTermNo>>
			ENDTEXT
			uf_gerais_actgrelha("", "", lcSQL1)
				
			IF ALLTRIM(ch_grupo) = 'Operadores'
				TEXT TO lcSQL NOSHOW TEXTMERGE
					exec up_manutencao_ActualizaPainelCentralPelosTerminais_soatendimento 0, '<<mySite>>'
				ENDTEXT
			ELSE
				TEXT TO lcSQL NOSHOW TEXTMERGE
					exec up_manutencao_ActualizaPainelCentralPelosTerminais 0, '<<mySite>>', <<IIF(TYPE("myTermNo") == "U", 0, myTermNo)>>, <<ch_userno>>
				ENDTEXT
			ENDIF 
		ENDIF 
		IF !uf_gerais_actgrelha("", "", lcSQL)
			MESSAGEBOX("OCORREU UMA ANOMALIA A CONFIGURAR O PAINEL CENTRAL.", 16, "LOGITOOLS SOFTWARE")
			PAINELCENTRAL.LOCKSCREEN = .F.
			RETURN .f.
		ENDIF 
		uf_painelcentral_carregamenu(1, .F., .T.)
		uf_painelcentral_alternaPagina(1)
	    PAINELCENTRAL.LOCKSCREEN = .F.
			myPainelCentralEdicao = .F.
		
	ENDIF  
	
	fecha("ucrsAtalhosPc")
	
	fecha("ucrsFixosPc")
	
ENDFUNC 



FUNCTION uf_painelcentral_escolheatalho
	LPARAMETERS lcObjAtalho
	
	PUBLIC  lcOpcaoEscolha
	STORE "" TO lcOpcaoEscolha	
		
	lcSQL = ""
    TEXT TO lcSQL TEXTMERGE NOSHOW
        select 	
				 imagem 
				, texto
				, comando	
		from b_pcentral (nolock)
		where 1 = 1
				and pagina between  2 and 5
				and tipo = 'A'
				and terminal = <<mytermno>>
		group by imagem, texto, comando 

    ENDTEXT
    If !uf_gerais_actGrelha("", "ucrsAtalhosPesq", lcSql)
        uf_perguntalt_chama("N�o foi possivel verificar as op��es do Painel Central.", "", "OK", 16)
        RETURN .f.
    ENDIF
		
	IF uf_valorescombo_chama("lcOpcaoEscolha", 0, "ucrsAtalhosPesq", 2, "imagem", "texto")
		IF EMPTY(lcOpcaoEscolha)
		
			uf_painelCentral_eliminaatalhos(lcObjAtalho)
		ELSE
		
			SELECT ucrsAtalhosPesq
			Locate for ALLTRIM(ucrsAtalhosPesq.imagem) = ALLTRIM(lcOpcaoEscolha)
			
			uf_painelCentral_gravaatalhos(lcObjAtalho)
			
			
		ENDIF 
	ELSE
		RETURN .F.
	ENDIF 
	
	
	
	RELEASE lcOpcaoEscolha
	

	fecha("ucrsAtalhosPesq")
ENDFUNC



FUNCTION uf_painelCentral_gravaatalhos
	LPARAMETERS lcObjAtalho
	
	
	LOCAL lcPosAtalho
	STORE "" TO lcPosAtalho
	lcPosAtalho =STRTRAN(lcObjAtalho, "adiciona", "")
	
	SELECT ucrsAtalhosPesq
	lcSQL = ""
    TEXT TO lcSQL TEXTMERGE NOSHOW
	    	Declare @usstamp varchar(25) = (select usstamp from b_us where userno = <<ch_userno>>)
			if exists ( select * from b_painelCentralAtalhos where posicao = <<lcPosAtalho>> and usstamp = @usstamp)
			begin
				update b_painelCentralAtalhos 
				SET 	nome  = '<<ucrsAtalhosPesq.texto>>' 
					,comando  = '<<STRTRAN(ucrsAtalhosPesq.comando, "'", "''")>>'	
					,iconNome = '<<ucrsAtalhosPesq.imagem>>'
					, posicao = <<lcPosAtalho >>
				where posicao = <<lcPosAtalho >> and usstamp = @usstamp
				
			end
			else
			begin
				INSERT INTO [dbo].[b_painelCentralAtalhos]
				           ([painelCentralAtalhosStamp]
				           ,[nome]
				           ,[comando]
				           ,[iconNome]
				           ,[posicao]
				           ,[usstamp]
				           ,[ousrdata])
				     VALUES
				           (LEFT(newid(),25)
				           ,'<<ucrsAtalhosPesq.texto>>'
				           ,'<<STRTRAN(ucrsAtalhosPesq.comando, "'", "''")>>'
				           ,'<<ucrsAtalhosPesq.imagem>>'
				           ,<<lcPosAtalho>>
				           ,@usstamp
				           ,GETDATE())
			end 
	     
    ENDTEXT
    If !uf_gerais_actGrelha("", "", lcSql)
        uf_perguntalt_chama("N�o foi possivel verificar as op��es do Painel Central.", "", "OK", 16)
        RETURN .f.
    ENDIF
    
    
    
    	PAINELCENTRAL.LOCKSCREEN = .T.
		uf_painelcentral_alternaPagina(1)

		IF !uf_gerais_getparameter_site('ADM0000000084', 'BOOL', mySite)
		
			TEXT TO lcSQL1 NOSHOW TEXTMERGE
				delete from b_pcentral  WHERE terminal = <<IIF(TYPE("myTermNo") == "U", 0, myTermNo)>>
			ENDTEXT
			
			uf_gerais_actgrelha("", "", lcSQL1)
		
			TEXT TO lcSQL NOSHOW TEXTMERGE
				exec up_manutencao_ActualizaPainelCentralPelosTerminais 0, '<<mySite>>', <<IIF(TYPE("myTermNo") == "U", 0, myTermNo)>>, <<ch_userno>>
			ENDTEXT 
		ELSE
			TEXT TO lcSQL1 NOSHOW TEXTMERGE
				delete from b_pcentral where terminal = <<myTermNo>>
			ENDTEXT
			uf_gerais_actgrelha("", "", lcSQL1)
				
			IF ALLTRIM(ch_grupo) = 'Operadores'
				TEXT TO lcSQL NOSHOW TEXTMERGE
					exec up_manutencao_ActualizaPainelCentralPelosTerminais_soatendimento 0, '<<mySite>>'
				ENDTEXT
			ELSE
				TEXT TO lcSQL NOSHOW TEXTMERGE
					exec up_manutencao_ActualizaPainelCentralPelosTerminais 0, '<<mySite>>', <<IIF(TYPE("myTermNo") == "U", 0, myTermNo)>>, <<ch_userno>>
				ENDTEXT
			ENDIF 
		ENDIF 
		IF !uf_gerais_actgrelha("", "", lcSQL)
			MESSAGEBOX("OCORREU UMA ANOMALIA A CONFIGURAR O PAINEL CENTRAL.", 16, "LOGITOOLS SOFTWARE")
			PAINELCENTRAL.LOCKSCREEN = .F.
			RETURN .f.
		ENDIF 
	uf_painelcentral_carregamenu(1, .F., .T.)
	uf_painelcentral_alternaPagina(1)
    PAINELCENTRAL.LOCKSCREEN = .F.
    myPainelCentralEdicao = .F.
    RETURN .T.
ENDFUNC 



FUNCTION uf_painelCentral_eliminaatalhos
	LPARAMETERS lcObjAtalho
	
	
	LOCAL lcPosAtalho
	STORE "" TO lcPosAtalho
	lcPosAtalho =STRTRAN(lcObjAtalho, "adiciona", "")
	
	 TEXT TO lcSQL TEXTMERGE NOSHOW
	    	Declare @usstamp varchar(25) = (select usstamp from b_us where userno = <<ch_userno>>)
			delete from b_painelCentralAtalhos where posicao = <<lcPosAtalho>> and usstamp = @usstamp     
    ENDTEXT
    If !uf_gerais_actGrelha("", "", lcSql)
        uf_perguntalt_chama("N�o foi possivel verificar as op��es do Painel Central.", "", "OK", 16)
        RETURN .f.
    ENDIF
    
		PAINELCENTRAL.LOCKSCREEN = .T.
		uf_painelcentral_alternaPagina(1)

		IF !uf_gerais_getparameter_site('ADM0000000084', 'BOOL', mySite)
		
			TEXT TO lcSQL1 NOSHOW TEXTMERGE
				delete from b_pcentral  WHERE terminal = <<IIF(TYPE("myTermNo") == "U", 0, myTermNo)>>
			ENDTEXT
			
			uf_gerais_actgrelha("", "", lcSQL1)
		
			TEXT TO lcSQL NOSHOW TEXTMERGE
				exec up_manutencao_ActualizaPainelCentralPelosTerminais 0, '<<mySite>>', <<IIF(TYPE("myTermNo") == "U", 0, myTermNo)>>, <<ch_userno>>
			ENDTEXT 
		ELSE
			TEXT TO lcSQL1 NOSHOW TEXTMERGE
				delete from b_pcentral where terminal = <<myTermNo>>
			ENDTEXT
			uf_gerais_actgrelha("", "", lcSQL1)
				
			IF ALLTRIM(ch_grupo) = 'Operadores'
				TEXT TO lcSQL NOSHOW TEXTMERGE
					exec up_manutencao_ActualizaPainelCentralPelosTerminais_soatendimento 0, '<<mySite>>'
				ENDTEXT
			ELSE
				TEXT TO lcSQL NOSHOW TEXTMERGE
					exec up_manutencao_ActualizaPainelCentralPelosTerminais 0, '<<mySite>>', <<IIF(TYPE("myTermNo") == "U", 0, myTermNo)>>, <<ch_userno>>
				ENDTEXT
			ENDIF 
		ENDIF 
		IF !uf_gerais_actgrelha("", "", lcSQL)
			MESSAGEBOX("OCORREU UMA ANOMALIA A CONFIGURAR O PAINEL CENTRAL.", 16, "LOGITOOLS SOFTWARE")
			PAINELCENTRAL.LOCKSCREEN = .F.
			RETURN .f.
		ENDIF 
	uf_painelcentral_carregamenu(1, .F., .T.)
	uf_painelcentral_alternaPagina(1)
    PAINELCENTRAL.LOCKSCREEN = .F.
    myPainelCentralEdicao = .F.
    RETURN .T.
	
ENDFUNC 