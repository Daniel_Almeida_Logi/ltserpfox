** Cria classe para permitir os eventos dinamicos **
DEFINE CLASS pMessagens As Custom
	PROCEDURE proc_wdgmensagens_dblClick
		uf_wdgmensagens_dblClick()
	ENDPROC
ENDDEFINE


**
FUNCTION uf_wdgmensagens_RefrescaMsg
	LOCAL lcSQL, lcOcj, lcObj2, lcObj3
	STORE "" TO lcSQL, lcObj, lcObj2, lcObj3
	
	lcObj2 = "painelcentral.pagina" + astr(painelcentral.pagina) &&+ ".wdgmensagens"
	lcObj3 = lcObj2 + ".oCust"

	If Type(lcObj3) # "O"

		&lcObj2..AddObject("oCust","pMessagens")
	ENDIF 
	IF !uf_gerais_actgrelha("", "uCrsLstMsg", [exec up_mensagens_pesquisa ']+m_chinis+['])
		uf_perguntalt_chama("OCORREU UM ERRO AO ACEDER AS MENSAGENS DO UTILIZADOR!","OK","",16)
		RETURN .f.
	ENDIF

	&& Atribiu recordsource a grelha
	lcObj = "painelcentral.pagina" + astr(painelcentral.pagina) + ".wdgmensagens.grid1"
	IF TYPE(lcObj) != "U"
		&lcObj..recordsource = "uCrsLstMsg"

		&& Configura colunas
		FOR i=1 TO &lcObj..columncount
			&lcObj..columns(i).visible= .F.

		 	DO CASE
		 		CASE ALLTRIM(UPPER(&lcObj..columns(i).controlsource)) == ALLTRIM(Upper("uCrsLstMsg.resumo"))

		 			&lcObj..columns(i).columnorder 	=	1
		 			&lcObj..columns(i).WIDTH 		=	&lcObj..parent.width - 200
		 			&lcObj..columns(i).visible 		=	.t.
		 			&lcObj..columns(i).readonly 	=	.t.
		
					BindEvent(&lcObj..columns(i).text1,"dblClick",&lcObj2..oCust,"proc_wdgmensagens_dblClick")
	 			
		 		CASE ALLTRIM(UPPER(&lcObj..columns(i).controlsource)) == ALLTRIM(Upper("uCrsLstMsg.data"))
		 			&lcObj..columns(i).columnorder 	=	2
		 			&lcObj..columns(i).WIDTH 		=	&lcObj..parent.width - 100
		 			&lcObj..columns(i).visible 		=	.t.
		 			&lcObj..columns(i).readonly 	=	.t.
		 			
				OTHERWISE
					***
			ENDCASE

			&& Cores das Linhas
			&lcObj..columns(i).dynamicBackColor="IIF(uCrsLstMsg.visto, Rgb[132,132,132], rgb[231,231,231])"
		ENDFOR 
		&lcObj..refresh
	ENDIF
ENDFUNC


**
FUNCTION uf_wdgmensagens_dblClick()

	DO FORM detalhemensagem

ENDFUNC