FUNCTION uf_wdgbrowser_abreURL
	LPARAMETERS lcURL, lcObj, lcCaption

	IF TYPE("lcCaption") == "C"
		IF !EMPTY(ALLTRIM(lcCaption))
			&lcObj..Label1.caption = ALLTRIM(lcCaption)
		ENDIF 
	ENDIF 
	
	lcObj = lcObj + "._webbrowser41"
	
	&lcObj..setfocus
	
	IF TYPE(lcObj) != "U"
		*&lcObj..visible = .t.
		&lcObj..navigate(ALLTRIM(lcURL),"","","","")
	ENDIF 
	
	lcObj = IIF(TYPE("relatorio") == "U","painelCentral.pagina" + astr(painelCentral.pagina),"relatorio.pagina2")
	&lcObj..contembrowser = 1
ENDFUNC 


**
FUNCTION uf_wdgbrowser_MaxMin
	LPARAMETERS lcObj, lcAccao

	IF lcAccao == 'max'
		TRY
			&lcObj..parent.parent.wdgselreport.zorder(1)
			&lcObj..parent.parent.wdgfiltroreport.zorder(1)
		CATCH
			**
		ENDTRY
		IF &lcObj..parent._webBrowser41.width > 0
			&lcObj..parent.browserVisible = 1
		ELSE
			&lcObj..parent.browserVisible = 0
		ENDIF 
		
		&&Max
		&lcObj..config(mypath+"\imagens\icons\setaMin.png")
		&lcObj..parent.lcaccao = 'min'
		
		&lcObj..parent.oTop = &lcObj..parent.top
		&lcObj..parent.oLeft = &lcObj..parent.left
		&lcObj..parent.oHeight = &lcObj..parent.height
		&lcObj..parent.oWidth = &lcObj..parent.width
		
		&lcObj..parent.Image3.picture = 'fundo_cab_atalho.jpg'
		&lcObj..parent.top = 0
		&lcObj..parent.left = 0
		&lcObj..parent.height = &lcObj..parent.parent.height
		&lcObj..parent.width = &lcObj..parent.parent.width
		
		IF USED("uCrsListaBrowser")
			SELECT uCrsListaBrowser
			GO top
			SCAN
				IF !(UPPER(alltrim(lcObj)) == UPPER(ALLTRIM(uCrsListaBrowser.nome)))
					lcObjV = uCrsListaBrowser.nome
					*&lcObjV.._webBrowser41.visible = .f.
					&lcObjV.._webBrowser41.width = 0
				ENDIF
			ENDSCAN
		ENDIF
		*&lcObj..parent._webBrowser41.visible = .t.
		&lcObj..parent._webBrowser41.width = &lcObj..parent.width
	ELSE
		&&Min
		&lcObj..config(mypath+"\imagens\icons\setaMax.png")
		&lcObj..parent.lcaccao = 'max'
		
		&lcObj..parent.Image3.picture = 'fundo_cab_atalho.png'
		&lcObj..parent.Top = &lcObj..parent.oTop
		&lcObj..parent.Left = &lcObj..parent.oLeft
		&lcObj..parent.Height = &lcObj..parent.oHeight
		&lcObj..parent.Width = &lcObj..parent.oWidth
		
		IF &lcObj..parent.browserVisible == 1
			*&lcObj..parent._webBrowser41.visible = .t.
			&lcObj..parent._webBrowser41.width = &lcObj..parent.width
			&lcObj..parent.browserVisible = 1
		ELSE
			*&lcObj..parent._webBrowser41.visible = .f.
			&lcObj..parent._webBrowser41.width = 0
			&lcObj..parent.browserVisible = 0
		ENDIF 
		
		LOCAL lcRestaurar
		lcRestaurar = &lcObj..parent.restaurabrowser
		*&lcRestaurar.._webBrowser41.visible = .t.
		&lcRestaurar.._webBrowser41.width = &lcRestaurar..width
	ENDIF 
ENDFUNC

FUNCTION uf_wdgBrowser_ordena
	IF USED("uCrsListaBrowser")
		LOCAL lcTop, lcHeight, lcObjAlt, lcTopC
		
		SELECT uCrsListaBrowser
		CALCULATE MAX(uCrsListaBrowser.top) TO lcTop
		CALCULATE MIN(uCrsListaBrowser.height) TO lcHeight

		SELECT uCrsListaBrowser
		LOCATE FOR uCrsListaBrowser.marcado == .t.
		IF FOUND()
			lcObjAlt = uCrsListaBrowser.nome
			lcTopC = &lcObjAlt..top
			&lcObjAlt..top = lcTop
			&lcObjAlt..height = lcHeight
			
			SELECT uCrsListaBrowser
			SCAN FOR uCrsListaBrowser.marcado == .f.
				IF uCrsListaBrowser.top > lcTopC
					lcObjAlt = uCrsListaBrowser.nome
					&lcObjAlt..top = uCrsListaBrowser.top - 25
					&lcObjAlt..height = uCrsListaBrowser.height + 25
				ENDIF
			ENDSCAN
			
			&&zorder
			SELECT uCrsListaBrowser
			GO top
			SCAN FOR uCrsListaBrowser.marcado == .f.
				lcObjAlt = uCrsListaBrowser.nome
				*&lcObjAlt.._webbrowser41.visible = .f.
				&lcObjAlt..zorder(1)
				&lcObjAlt.._webbrowser41.width = 0
			ENDSCAN
			SELECT uCrsListaBrowser
			LOCATE FOR uCrsListaBrowser.marcado == .t.
			lcObjAlt = uCrsListaBrowser.nome
			*&lcObjAlt.._webbrowser41.visible = .t.			
			&lcObjAlt..zorder(0)
			&lcObjAlt.._webbrowser41.width = &lcObjAlt..width
		ENDIF
	ENDIF
ENDFUNC 


**
FUNCTION uf_wdgBrowser_ordenaEliminar
	IF USED("uCrsListaBrowser")
		LOCAL lcTop, lcHeight, lcObjAlt, lcTopC
		
		SELECT uCrsListaBrowser
		CALCULATE MAX(uCrsListaBrowser.top) TO lcTop
		CALCULATE MIN(uCrsListaBrowser.height) TO lcHeight

		SELECT uCrsListaBrowser
		LOCATE FOR uCrsListaBrowser.marcado == .t.
		IF FOUND()
			lcObjAlt = uCrsListaBrowser.nome
			lcTopC = &lcObjAlt..top
			&lcObjAlt..top = lcTop
			&lcObjAlt..height = lcHeight
			
			SELECT uCrsListaBrowser
			SCAN FOR uCrsListaBrowser.marcado == .f.
				IF uCrsListaBrowser.top > lcTopC
					lcObjAlt = uCrsListaBrowser.nome
					&lcObjAlt..top = uCrsListaBrowser.top - 25
					&lcObjAlt..height = uCrsListaBrowser.height + 25
				ENDIF
			ENDSCAN
			
			SELECT uCrsListaBrowser
			LOCATE FOR uCrsListaBrowser.top == lcTop AND !(uCrsListaBrowser.marcado)
			IF FOUND()
				lcObjAlt = uCrsListaBrowser.nome
				*&lcObjAlt.._webBrowser41.visible = .t.
				&lcObjAlt.._webBrowser41.width = &lcObjAlt..width
			ELSE
				SELECT uCrsListaBrowser
				LOCATE FOR uCrsListaBrowser.top == lcTop - 25
				IF FOUND()
					lcObjAlt = uCrsListaBrowser.nome
					*&lcObjAlt.._webBrowser41.visible = .t.
					&lcObjAlt.._webBrowser41.width = &lcObjAlt..width
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDFUNC 


**
FUNCTION uf_wdgBrowser_AbreUrlFornecedor
	LPARAMETERS lcPag, lcObj
	IF !USED("uCrsPCentral")
		uf_logitools_sqlExec("select * from b_pcentral (nolock) where terminal = '"+astr(myTermNo)+"'", "uCrsPCentral")
	ENDIF

	SELECT uCrsPCentral
	LOCATE FOR uCrsPCentral.pagina == lcPag AND UPPER(ALLTRIM(uCrsPCentral.nome)) == lcObj
	IF FOUND()
	
		lcObj2 = IIF(TYPE("relatorio") == "U","painelcentral.pagina1","relatorio.pagina2")
		&lcObj2..adicionaopcao(ALLTRIM(uCrsPCentral.nome),ALLTRIM(uCrsPCentral.tipo),uCrsPCentral.colIni,uCrsPCentral.colFin,uCrsPCentral.linIni,uCrsPCentral.linFin,ALLTRIM(uCrsPCentral.imagem),ALLTRIM(uCrsPCentral.imagem2),ALLTRIM(uCrsPCentral.texto),ALLTRIM(uCrsPCentral.comando),uCrsPCentral.backStyle,ALLTRIM(uCrsPCentral.backColor),ALLTRIM(uCrsPCentral.borderColor),uCrsPCentral.borderWidth,uCrsPCentral.comandosql,uCrsPCentral.imagemSimbolo)
	ENDIF
ENDFUNC