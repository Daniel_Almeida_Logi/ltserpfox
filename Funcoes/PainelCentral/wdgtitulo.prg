**
FUNCTION uf_wdgTitulo_alteraCor
	LPARAMETERS lcCor
	
	IF TYPE("lcCor") == "C" AND !EMPTY(ALLTRIM(lcCor))
		LOCAL lcObjLbl
		
		lcObjLbl = "painelcentral.pagina" + astr(painelcentral.pagina) + ".wdgTitulo"
		
		ALINES(cores,lcCor,",")
		&lcObjLbl..label1.foreColor = RGB(VAL(cores[1]),VAL(cores[2]),VAL(cores[3]))
	ENDIF
ENDFUNC 