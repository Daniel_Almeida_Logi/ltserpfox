**
FUNCTION uf_detalhemensagem_chama
	IF TYPE("detalhemensagem") == "U"
		DO FORM detalhemensagem
	ELSE
		detalhemensagem.show()
	ENDIF
ENDFUNC 


**
FUNCTION uf_detalhemensagem_sair
	detalhemensagem.hide
	detalhemensagem.release	
ENDFUNC


**
FUNCTION uf_detalhemensagem_FiltraCursor

	SELECT uCrsLstMsg
	GO TOP
	IF !EMPTY(detalhemensagem.valor.value)
		SET FILTER TO LIKE('*'+UPPER(ALLTRIM(detalhemensagem.valor.value))+'*',UPPER(uCrsLstMsg.resumo))
	ELSE
		SET FILTER TO
	ENDIF

	detalhemensagem.GRID1.refresh
ENDFUNC

**
FUNCTION uf_detalhemensagem_navega

	SELECT uCrsLstMsg
	IF !EMPTY(ALLTRIM(uCrsLstMsg.stamp))
		uf_documentos_chama(uCrsLstMsg.stamp)
		uf_detalhemensagem_sair()
	ENDIF
		
ENDFUNC