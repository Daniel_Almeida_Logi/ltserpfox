FUNCTION uf_psicobenzo_chama
	&& CONTROLA PERFIS DE ACESSO
	IF uf_gerais_validaPermUser(ch_userno,ch_grupo,'Gest�o Psicotr�picos - Gest�o')
		IF !(TYPE('ATENDIMENTO') == "U")
			uf_perguntalt_chama("N�O PODE ABRIR O ECR� DE GEST�O DE PSICOTR�PICOS/BENZODIAZEPINAS ENQUANTO O ECR� DE ATENDIMENTO ESTIVER ABERTO.","OK","",48)
			RETURN .f.
		ENDIF
		IF !(TYPE('IDENTIFICACAO') == "U")
			uf_perguntalt_chama("N�O PODE ABRIR O ECR� DE GEST�O DE PSICOTR�PICOS/BENZODIAZEPINAS ENQUANTO O ECR� DE IDENTIFICA��O PARA O ATENDIMENTO ESTIVER ABERTO.","OK","",48)
			RETURN .f.
		ENDIF
	ELSE
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ACEDER AO PAINEL GEST�O DE PSICOTR�PICOS.","OK","",48)
		RETURN .f.
	ENDIF
	
	regua(0,3,"A carregar painel...")
	regua(1,1,"A carregar painel...")
	*** Abre o ecr� caso n�o esteja aberto
	IF TYPE("PSICOBENZO")=="U"
		
		**Valida Licenciamento	
		IF (uf_gerais_addConnection('PSICOBENZO') == .f.)
			regua(2)
			RETURN .F.
		ENDIF
		
		regua(1,2,"A carregar painel...")
		uf_psicobenzo_cursoresDefault()
		regua(1,3,"A carregar painel...")
		DO FORM PSICOBENZO
		
		&&Centra
		PSICOBENZO.autocenter = .t.
		PSICOBENZO.windowstate = 2
	ELSE
		regua(1,3,"A carregar painel...")
		PSICOBENZO.show
	ENDIF
	
	regua(2)
ENDFUNC


**
FUNCTION uf_psicobenzo_cursoresDefault

	*
	Text To lcSql Noshow textmerge
		Set FMTONLY ON
		exec dbo.up_psico_entradasPsico '20120101', '20120101', '<<mySite>>'
		Set FMTONLY OFF
	Endtext
	If !uf_gerais_actGrelha("","uCrsPsiEnt",lcSql)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA NA VERIFICA��O DE ENTRADAS DE PSICOTR�PICOS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	Endif
	
	**
	Text To lcSql Noshow textmerge
		Set FMTONLY ON
		exec dbo.up_psico_saidasPsico '20120101', '20120101', '<<mySite>>'
		Set FMTONLY OFF
	Endtext
	If !uf_gerais_actGrelha("","uCrsPsiSai",lcSql)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA NA VERIFICA��O DE SA�DAS DE PSICOTR�PICOS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	ENDIF
	
	**
	Text To lcSql Noshow textmerge
		Set FMTONLY ON
		exec dbo.up_psico_entradasBenzo '20120101', '20120101', '<<mySite>>'
		Set FMTONLY OFF
	Endtext
	If !uf_gerais_actGrelha("","uCrsBenEnt",lcSql)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA NA VERIFICA��O DE ENTRADAS DE BENZODIAZEPINAS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	ENDIF
	
	**
	Text To lcSql Noshow textmerge
		Set FMTONLY ON
		exec dbo.up_psico_saidasBenzo '20120101', '20120101', '<<mySite>>'
		Set FMTONLY OFF
	Endtext
	If !uf_gerais_actGrelha("","uCrsBenSai",lcSql)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA NA VERIFICA��O DE SA�DAS DE BENZODIAZEPINAS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	Endif
ENDFUNC


**
FUNCTION uf_psicobenzo_actualizar
	DO CASE
		CASE psicobenzo.pageframe1.activepage == 1 && psicotr�picos entradas
			uf_psicobenzo_actualizaPsicoEnt()
		CASE psicobenzo.pageframe1.activepage == 2 && psicotr�picos saidas
			uf_psicobenzo_actualizaPsicoSai()
		CASE psicobenzo.pageframe1.activepage == 3 && benzodiazepinas entradas
			uf_psicobenzo_actualizaBenzoEnt()
		CASE psicobenzo.pageframe1.activepage == 4 && benzodiazepinas saidas
			uf_psicobenzo_actualizaBenzoSai()
		OTHERWISE
			uf_perguntalt_chama("PAGEFRAME N�O ENCONTRADO. POR FAVOR CONTACTE O SUPORTE","OK","",16)
	ENDCASE
ENDFUNC


**
FUNCTION uf_psicobenzo_sair
	PSICOBENZO.hide
	PSICOBENZO.release
ENDFUNC


**
FUNCTION uf_psicobenzo_gravar
ENDFUNC

********************
** PSICOTR�PICOS  **
********************

FUNCTION uf_psicobenzo_actualizaPsicoEnt
	**
	Text To lcSql Noshow textmerge
		exec dbo.up_psico_entradasPsico '<<uf_gerais_getdate(psicobenzo.txtDataIni.value, "SQL")>>', '<<uf_gerais_getdate(psicobenzo.txtDataFim.value, "SQL")>>', '<<psicobenzo.cbLoja.displayvalue>>'
	ENDTEXT
	
	uf_gerais_actGrelha("PSICOBENZO.pageframe1.page1.grdPsiEnt", "uCrsPsiEnt", lcSQL)
ENDFUNC


**
FUNCTION uf_psicobenzo_actualizaPsicoSai
	**
	Text To lcSql Noshow textmerge
		exec dbo.up_psico_saidasPsico '<<uf_gerais_getdate(psicobenzo.txtDataIni.value, "SQL")>>', '<<uf_gerais_getdate(psicobenzo.txtDataFim.value, "SQL")>>', '<<psicobenzo.cbLoja.displayvalue>>'
	ENDTEXT
	
	uf_gerais_actGrelha("PSICOBENZO.pageframe1.page2.grdPsiSai", "uCrsPsiSai", lcSQL)
ENDFUNC


**
FUNCTION uf_psicobenzo_mapaPsicoSai
	** Construir string com parametros	
	lcSql = "&dataIni=" + uf_gerais_getdate(psicobenzo.txtDataIni.value, "DATA") + "&dataFim=" + uf_gerais_getdate(psicobenzo.txtDataFim.value, "DATA") + "&site=" + psicobenzo.cbLoja.displayvalue
	
	** chamar report pelo nome dele
	uf_gerais_chamaReport("relatorio_psicobenzo_saidasPsico", lcSql)
ENDFUNC


**
FUNCTION uf_psicobenzo_mapaPsicoEnt	
	** Construir string com parametros	
	lcSql = "&dataIni=" + uf_gerais_getdate(psicobenzo.txtDataIni.value, "DATA") + "&dataFim=" + uf_gerais_getdate(psicobenzo.txtDataFim.value, "DATA") + "&site=" + psicobenzo.cbLoja.displayvalue
	
	uf_gerais_chamaReport("relatorio_psicobenzo_entradasPsico", lcSql)
ENDFUNC


**
FUNCTION uf_psicobenzo_mapaPsicoBal
	** Construir string com parametros	
	lcSql = "&dataIni=" + uf_gerais_getdate(psicobenzo.txtDataIni.value, "DATA") + "&dataFim=" + uf_gerais_getdate(psicobenzo.txtDataFim.value, "DATA") + "&site=" + psicobenzo.cbLoja.displayvalue
	
	uf_gerais_chamaReport("relatorio_psicobenzo_BalancoPsico", lcSql)
ENDFUNC


*********************
** BENZODIAZEPINAS **
*********************

**
FUNCTION uf_psicobenzo_actualizaBenzoEnt
	**
	Text To lcSql Noshow textmerge
		exec dbo.up_psico_entradasBenzo '<<uf_gerais_getdate(psicobenzo.txtDataIni.value, "SQL")>>', '<<uf_gerais_getdate(psicobenzo.txtDataFim.value, "SQL")>>', '<<psicobenzo.cbLoja.displayvalue>>'
	ENDTEXT
	
	uf_gerais_actGrelha("PSICOBENZO.pageframe1.page3.grdBenEnt","uCrsBenEnt",lcSQL)
ENDFUNC


**
FUNCTION uf_psicobenzo_actualizaBenzoSai
	**
	Text To lcSql Noshow textmerge
		exec dbo.up_psico_saidasBenzo '<<uf_gerais_getdate(psicobenzo.txtDataIni.value, "SQL")>>', '<<uf_gerais_getdate(psicobenzo.txtDataFim.value, "SQL")>>', '<<psicobenzo.cbLoja.displayvalue>>'
	ENDTEXT
	
	uf_gerais_actGrelha("PSICOBENZO.pageframe1.page4.grdBenSai","uCrsBenSai",lcSQL)
ENDFUNC


**
FUNCTION uf_psicobenzo_mapaBenzoEnt
	** Construir string com parametros	
	lcSql = "&dataIni=" + uf_gerais_getdate(psicobenzo.txtDataIni.value, "DATA") + "&dataFim=" + uf_gerais_getdate(psicobenzo.txtDataFim.value, "DATA") + "&site=" + psicobenzo.cbLoja.displayvalue
	
	uf_gerais_chamaReport("relatorio_psicobenzo_EntradasBenzo", lcSql)
ENDFUNC


**
FUNCTION uf_psicobenzo_mapaBenzoBal
	** Construir string com parametros	
	lcSql = "&dataIni=" + uf_gerais_getdate(psicobenzo.txtDataIni.value, "DATA") + "&dataFim=" + uf_gerais_getdate(psicobenzo.txtDataFim.value, "DATA") + "&site=" + psicobenzo.cbLoja.displayvalue
	
	uf_gerais_chamaReport("relatorio_psicobenzo_BalancoBenzo", lcSql)
ENDFUNC

**
FUNCTION uf_psicobenzo_contador_psico_benzo_entradasSaidas
	LPARAMETERS lcEntrSaid, lcPsicoBenzo, lcSiteVerificar
	
	LOCAL lnContador, lcSqlBenzo, lcSiteContar 
	STORE 0 TO lnContador
	STORE '' TO lcSqlBenzo, lcSiteContar   
		
	IF(EMPTY(lcSiteVerificar))
		lcSiteContar = mySite
	ELSE 
		lcSiteContar = lcSiteVerificar
	ENDIF
	
	IF lnContador = 0
		IF lcPsicoBenzo = 1	
			TEXT TO lcSqlBenzo NOSHOW TEXTMERGE
				EXEC up_gerais_psico <<lcEntrSaid>>, '<<lcSiteContar>>'
			ENDTEXT
		ELSE
			TEXT TO lcSqlBenzo NOSHOW TEXTMERGE
				EXEC up_gerais_benzo <<lcEntrSaid>>, '<<lcSiteContar>>'
			ENDTEXT
		ENDIF

		IF !uf_gerais_actGrelha("", "uCrsContador", lcSqlBenzo)
    		uf_perguntalt_chama("N�o foi possivel obter a contagem dos psicotr�picos. Por favor contacte o Suporte.","OK","",16)
    		RETURN lnContador
		ENDIF	

		lnContador = uCrsContador.ct +1
		
		fecha("uCrsContador")
	ELSE
		lnContador = lnContador	+ 1
	ENDIF	
		
	RETURN lnContador 
ENDFUNC

