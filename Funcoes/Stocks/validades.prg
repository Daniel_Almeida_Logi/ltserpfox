**
FUNCTION uf_VALIDADES_chama
	LPARAMETERS lcPainel, lcRef
	
	PUBLIC myOrigemPainelPPV, myOrigemDocPPV, myOrigemRefPPV, myValidadesAlteracao
			
	&& valida parameters
	IF EMPTY(lcPainel)
		myOrigemPainelPPV = ''
	ELSE
		myOrigemPainelPPV = UPPER(ALLTRIM(lcPainel))
	ENDIF

	 DO CASE
		CASE myOrigemPainelPPV == "PESQSTOCKS"
			IF !uf_gerais_validaPermUser(ch_userno, ch_grupo,'Gest�o de Stock - Gest�o de Validades')
				uf_perguntalt_chama("O SEU PERFIL N�O TEM PERMISS�ES PARA ACEDER A ESTA FUNCIONALIDADE.","OK","",48)
				RETURN .f.
			ENDIF
		CASE myOrigemPainelPPV == "ST"
			IF !uf_gerais_validaPermUser(ch_userno, ch_grupo,'Gest�o de Stock - Gest�o de Validades')
				uf_perguntalt_chama("O SEU PERFIL N�O TEM PERMISS�ES PARA ACEDER A ESTA FUNCIONALIDADE.","OK","",48)
				RETURN .f.
			ENDIF
	ENDCASE

	
	IF EMPTY(ALLTRIM(lcRef))
		myOrigemRefPPV = ''
	ELSE
		myOrigemRefPPV = lcRef
	ENDIF
	**
	
	DO CASE
		CASE myOrigemPainelPPV == 'DOCUMENTOS'
			If Used("cabdoc")
				Select cabdoc
				myOrigemDocPPV = Alltrim(cabdoc.cabstamp)
			Else
				myOrigemDocPPV = "0"
			ENDIF
			
		CASE myOrigemPainelPPV == 'ST'
			myOrigemDocPPV = ""
			
		OTHERWISE
			***
	ENDCASE
	
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_stocks_pesquisaValidades '', '30000101', 0, '', 99999, 'ST', '', '30000101', <<mysite_nr>>,''	
	ENDTEXT

	&& Controla Abertura do Painel
	IF type("VALIDADES")=="U"
	
		IF !uf_gerais_actgrelha("", "uCrsPesqVal", lcSql)
			RETURN .F.
		ENDIF
	
		DO FORM VALIDADES
	ELSE
		VALIDADES.show
	ENDIF
ENDFUNC


**
FUNCTION uf_validades_carregamenu
	WITH VALIDADES.menu1
			.adicionaOpcao("opcoes", "Op��es", myPath + "\imagens\icons\opcoes_w.png", "uf_gerais_menuOpcoes With 'validade'","F1")
			.adicionaOpcao("pesquisar", "Pesquisar", myPath + "\imagens\icons\actualizar_w.png", "uf_VALIDADES_pesquisa","F2")
			**.adicionaOpcao("apagarPesq", "Limpar", myPath + "\imagens\icons\limpar_w.png", "uf_VALIDADES_limpaPesq","F3")
			.adicionaOpcao("navegar", "Ver Ficha", myPath + "\imagens\icons\stocks_lupa_w.png", "uf_VALIDADES_Navegar","F3")
			.adicionaOpcao("pesqSVal", "Inc. s/ Val.", myPath + "\imagens\icons\unchecked_w.png", "uf_Validades_pesqSVal","F4")
	ENDWITH
	
	** Configura Menu Op��es
	WITH VALIDADES.menu_opcoes
		.adicionaOpcao("imprimir", "Imprimir Dados", myPath + "\imagens\icons\imprimir_w.png", "uf_VALIDADES_Imprimir")
	ENDWITH 

	validades.Menu1.gravar.img1.Picture = myPath + "\imagens\icons\guardar_w.png"

	validades.menu1.estado("", "", "Gravar", .t., "Sair", .t.)
ENDFUNC


**
FUNCTION uf_VALIDADES_limpaPesq
	VALIDADES.txtProd.value = ''
	VALIDADES.spnStock.value = 0
	VALIDADES.txtVal.value = ''
	VALIDADES.cmbFamilia.displayValue = ''
	VALIDADES.cmbFamilia.Value = ''
    VALIDADES.txtLocalizacao.Value = ''
	**uf_VALIDADES_pesquisa(.f.)
ENDFUNC 


**
FUNCTION uf_VALIDADES_pesquisa
	
	LOCAL lcValidade, lcFamilia, lcSvalidade, lcValidade2, lcLocalizacao
	
	&& Valida par�metros da SP
	IF !empty(VALIDADES.txtVal.value)
		If VALIDADES.txtVal.value == '1900.01'
			lcValidade = '30001201'
		ELSE
			lcValidade = alltrim(VALIDADES.txtVal.value) + "." + astr(day(gomonth(ctod(uf_gerais_getDate(alltrim(VALIDADES.txtVal.value) + ".28")), 1) - day(gomonth(ctod(uf_gerais_getDate(alltrim(VALIDADES.txtVal.value) + ".28")), 1))))
		ENDIF
	ELSE
		lcValidade = '30001201'
	ENDIF
	
	IF !empty(VALIDADES.txtVal2.value)
		If VALIDADES.txtVal2.value == '1900.01'
			lcValidade2 = '19000102'
		ELSE
			lcValidade2 = alltrim(VALIDADES.txtVal2.value) + "." + astr(day(gomonth(ctod(uf_gerais_getDate(alltrim(VALIDADES.txtVal2.value) + ".28")), 1) - day(gomonth(ctod(uf_gerais_getDate(alltrim(VALIDADES.txtVal2.value) + ".28")), 1))))
		ENDIF
	ELSE
		lcValidade2 = '19000102'
	ENDIF
	
	IF !empty(VALIDADES.cmbFamilia.value)
		lcFamilia = VALIDADES.cmbFamilia.value
	ELSE
		lcFamilia = ""
	ENDIF
	
	IF EMPTY(validades.menu1.pesqSVal.tag) OR validades.menu1.pesqSVal.tag == "false"
		lcSvalidade=0
	ELSE
		lcSvalidade=1
	ENDIF

    IF !EMPTY(VALIDADES.txtLocalizacao.value)
		lcLocalizacao = ALLTRIM(VALIDADES.txtLocalizacao.value)
	ELSE
		lcLocalizacao = ''
	ENDIF

	**
	lcSQL = ""
	IF UPPER(ALLTRIM(uCrsE1.tipoempresa)) = "ARMAZEM"
		Text To lcSQL Noshow textmerge
			exec up_stocks_pesquisaValidadesLotes '<<alltrim(VALIDADES.txtProd.value)>>', '<<lcValidade>>', <<lcSvalidade>>, '<<Alltrim(lcFamilia)>>', <<VALIDADES.spnStock.value>>, <<myOrigemPainelPPV>>, '<<Alltrim(myOrigemDocPPV)>>', '<<lcValidade2>>', '<<alltrim(VALIDADES.lote.value)>>',<<mysite_nr>>,'<<ALLTRIM(lcLocalizacao)>>'	
		ENDTEXT
	ELSE
		Text To lcSQL Noshow textmerge
			exec up_stocks_pesquisaValidades '<<alltrim(VALIDADES.txtProd.value)>>', '<<lcValidade>>', <<lcSvalidade>>, '<<Alltrim(lcFamilia)>>', <<VALIDADES.spnStock.value>>, <<myOrigemPainelPPV>>, '<<Alltrim(myOrigemDocPPV)>>', '<<lcValidade2>>',<<mysite_nr>>,'<<ALLTRIM(lcLocalizacao)>>' 
		ENDTEXT
	ENDIF

	uf_gerais_actGrelha("VALIDADES.GridPesq", "uCrsPesqVal", lcSQL)
	VALIDADES.lblRegistos.caption = ALLTRIM(STR(RECCOUNT("uCrsPesqVal"))) + " Resultados"

	** actualiza rodap�
	uf_validades_InfoST()
	
	myValidadesAlteracao = .F.
ENDFUNC


**
FUNCTION uf_Validades_pesqSVal
	**
	IF EMPTY(validades.menu1.pesqSVal.tag) OR validades.menu1.pesqSVal.tag == "false"
		validades.menu1.pesqSVal.config("Inc. s/ Val.", myPath + "\imagens\icons\checked_w.png", "uf_Validades_pesqSVal")
		validades.menu1.pesqSVal.tag = "true"
	ELSE
		validades.menu1.pesqSVal.config("Inc. s/ Val.", myPath + "\imagens\icons\unchecked_w.png", "uf_Validades_pesqSVal")
		validades.menu1.pesqSVal.tag = "false"
	ENDIF
	
	uf_VALIDADES_pesquisa()
ENDFUNC


**
FUNCTION uf_VALIDADES_Imprimir
	LOCAL lcValidade, lcFamilia, lcSvalidade, lcValidade2, lcLocalizacao
	
	DO CASE
		CASE !TYPE("VALIDADES") == "U" 
			IF !uf_gerais_validaPermUser(ch_userno, ch_grupo,'Relat�rios - Painel')
				uf_perguntalt_chama("O SEU PERFIL N�O TEM PERMISS�ES PARA ACEDER A ESTA FUNCIONALIDADE.","OK","",48)
				RETURN .f.
			ENDIF
	ENDCASE
	
	&& Valida par�metros da SP
	IF !empty(VALIDADES.txtVal.value)
		If VALIDADES.txtVal.value == '1900.01'
			lcValidade = '31.12.3000'
		ELSE
			lcValidade = alltrim(VALIDADES.txtVal.value) + "." + astr(day(gomonth(ctod(uf_gerais_getDate(alltrim(VALIDADES.txtVal.value) + ".28")), 1) - day(gomonth(ctod(uf_gerais_getDate(alltrim(VALIDADES.txtVal.value) + ".28")), 1))))
		ENDIF
	ELSE
		lcValidade = '31.12.3000'
	ENDIF
	
	IF !empty(VALIDADES.txtVal2.value)
		If VALIDADES.txtVal2.value == '1900.01'
			lcValidade2 = '02.01.1900'
		ELSE
			lcValidade2 = alltrim(VALIDADES.txtVal2.value) + "." + astr(day(gomonth(ctod(uf_gerais_getDate(alltrim(VALIDADES.txtVal2.value) + ".28")), 1) - day(gomonth(ctod(uf_gerais_getDate(alltrim(VALIDADES.txtVal2.value) + ".28")), 1))))
		ENDIF
	ELSE
		lcValidade2 = '02.01.1900'
	ENDIF
	
	IF !empty(VALIDADES.cmbFamilia.value)
		lcFamilia = VALIDADES.cmbFamilia.value
	ELSE
		lcFamilia = ""
	ENDIF

	IF EMPTY(validades.menu1.pesqSVal.tag) OR validades.menu1.pesqSVal.tag == "false"
		lcSvalidade=0
	ELSE
		lcSvalidade=1
	ENDIF
	
	IF !empty(VALIDADES.txtLocalizacao.value)
		lcLocalizacao= VALIDADES.txtLocalizacao.value
	ELSE
		lcLocalizacao= ""
	ENDIF
	******************************
	lcProd = alltrim(VALIDADES.txtProd.value)
	lcSvalidade = IIF(lcSvalidade==0,"false","true") 
	lcFamilia = Alltrim(VALIDADES.cmbFamilia.value)
	lcStock = Alltrim(Str(VALIDADES.spnStock.value))
	lcLote = ALLTRIM(validades.lote.value)

	IF UPPER(ALLTRIM(uCrsE1.tipoEmpresa)) == 'ARMAZEM'
		lcSQL = ""
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			&prod=<<lcProd>>&validade=<<lcValidade>>&svalidade=<<lcSvalidade>>&familia=<<lcFamilia>>&stock=<<lcStock>>&painel=<<myOrigemPainelPPV>>&doc=<<Alltrim(myOrigemDocPPV)>>&site=<<mysite>>&validade2=<<lcValidade2>>&site_nr=<<mySite_nr>>&lote=<<ALLTRIM(lcLote)>>&localizacao=<<lcLocalizacao>>
		ENDTEXT
	ELSE
		lcSQL = ""
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			&prod=<<lcProd>>&validade=<<lcValidade>>&svalidade=<<lcSvalidade>>&familia=<<lcFamilia>>&stock=<<lcStock>>&painel=<<myOrigemPainelPPV>>&doc=<<Alltrim(myOrigemDocPPV)>>&site=<<mysite>>&validade2=<<lcValidade2>>&site_nr=<<mySite_nr>>&lote=<<ALLTRIM(lcLote)>>&localizacao=<<lcLocalizacao>>
		ENDTEXT
	ENDIF
	
	SELECT uCrsPesqVal
	GO TOP 
		
		IF RECCOUNT("uCrsPesqVal") > 0
			uf_gerais_chamaReport("relatorio_stocks_validades", lcSql)
		ELSE
			uf_perguntalt_chama("N�o existem dados para Impress�o. Por favor verifique. Obrigado.","OK","",16)
		ENDIF
	
ENDFUNC


**
FUNCTION uf_VALIDADES_Navegar
	If Used("uCrsPesqVal")
		Select uCrsPesqVal
		uf_stocks_chama(ALLTRIM(uCrsPesqVal.ref))
	ENDIF
ENDFUNC


**
FUNCTION uf_validades_InfoST
	VALIDADES.txtStMax.refresh
	VALIDADES.txtPEnc.refresh
	VALIDADES.txtL1.refresh
	VALIDADES.txtL2.refresh
	VALIDADES.txtL3.refresh
	VALIDADES.chkPsico.refresh
	VALIDADES.txtDCI.refresh
ENDFUNC


**
FUNCTION uf_VALIDADES_gravar
	Local lcValida, lcSQL, lcSiteNr
	Store .f. To lcValida
	Store '' To lcSQL
	STORE 1 TO lcSiteNr
	
	IF(!EMPTY(uCrsE1.siteno))
		lcSiteNr = uCrsE1.siteno
	ENDIF
	
	Select uCrsPesqVal
	GO TOP
	SCAN
	
		IF EMPTY(uCrsPesqVal.lote)
			IF uCrsPesqVal.validade != uCrsPesqVal.validade2 
				lcSQL=''
				TEXT TO lcSQL TEXTMERGE NOSHOW
					UPDATE
						st
					Set
						validade = <<IIF(EMPTY(uf_gerais_getdate(uCrsPesqVal.validade2, "SQL")) OR ALLTRIM(uf_gerais_getdate(uCrsPesqVal.validade2, "SQL")) == "190001","'19000101'"," CONVERT(VARCHAR,EOMONTH('" + ALLTRIM(uf_gerais_getdate(uCrsPesqVal.validade2, "SQL"))+"01'))")>> 
					Where
						ref = '<<Alltrim(uCrsPesqVal.ref)>>'
						and st.site_nr = <<lcSiteNr>>
				ENDTEXT
				If !uf_gerais_actGrelha("", "", lcSQL)
					lcValida = .t.
				ENDIF
			ENDIF
		ELSE
			IF uCrsPesqVal.validade != uCrsPesqVal.validade2
				lcSQL=''
				TEXT TO lcSQL TEXTMERGE NOSHOW
					UPDATE
						st_lotes
					Set
						validade = <<IIF(EMPTY(uf_gerais_getdate(uCrsPesqVal.validade2, "SQL")) OR ALLTRIM(uf_gerais_getdate(uCrsPesqVal.validade2, "SQL")) == "190001","'19000101'"," CONVERT(VARCHAR,EOMONTH('" + ALLTRIM(uf_gerais_getdate(uCrsPesqVal.validade2, "SQL"))+"01'))")>> 
					Where
						ref = '<<Alltrim(uCrsPesqVal.ref)>>'
						and lote  = '<<Alltrim(uCrsPesqVal.lote)>>'
						and armazem = <<lcSiteNr>>
				ENDTEXT
				If !uf_gerais_actGrelha("", "", lcSQL)
					lcValida = .t.
				ENDIF
			ENDIF
		ENDIF
	ENDSCAN
	
	If lcValida == .f.
		uf_perguntalt_chama("VALIDADES ACTUALIZADAS COM SUCESSO","OK","", 64)
	ENDIF
	
	myValidadesAlteracao = .F.
	
	&& Alterado para encerrar painel quando se gravam altera��es; Aten��o ao alterar deve manter-se coer�ncia com os paineis de Conf PVP e altera��es em grupo.
	uf_VALIDADES_sair()
ENDFUNC


**
FUNCTION uf_VALIDADES_sair
	IF myValidadesAlteracao == .t.
		IF !uf_perguntalt_chama("PRETENDE CANCELAR AS ALTERA��ES EFECTUADAS?","Sim","N�o")
			RETURN .F.
		ELSE
			**Fecha Cursores
			IF USED("uCrsPesqVal")
				fecha("uCrsPesqVal")
			EndIf
			
			Release myOrigemPainelPPV
			Release myOrigemDocPPV
			Release myOrigemRefPPV
			RELEASE myValidadesAlteracao

			VALIDADES.hide
			VALIDADES.release
		ENDIF
	ELSE
		**Fecha Cursores
		IF USED("uCrsPesqVal")
			fecha("uCrsPesqVal")
		EndIf
		
		Release myOrigemPainelPPV
		Release myOrigemDocPPV
		Release myOrigemRefPPV
		RELEASE myValidadesAlteracao

		VALIDADES.hide
		VALIDADES.release
	ENDIF
ENDFUNC


FUNCTION uf_validades_scanner

    uv_scan = ALLTRIM(VALIDADES.txtScanner.value)

    IF EMPTY(uv_scan)
        VALIDADES.txtProd.setFocus()
        RETURN .F.
    ENDIF

    IF !EMPTY(uv_scan) AND AT(" ", ALLTRIM(uv_scan)) = 0 AND LEN(ALLTRIM(uv_scan)) > 20
        uv_ref = uf_gerais_getRefFromQR(ALLTRIM(uv_scan))

        IF !EMPTY(uv_ref)

            VALIDADES.txtProd.setFocus()
            VALIDADES.txtProd.value = ALLTRIM(uv_ref)
            VALIDADES.menu1.pesquisar.click			

        ENDIF
    ENDIF

ENDFUNC

PROCEDURE uf_validades_fichaprod
lcRef = uCrsPesqVal.ref
 IF  .NOT. EMPTY(lcRef)
    uf_stocks_chama(ALLTRIM(lcRef))
 ENDIF
ENDPROC