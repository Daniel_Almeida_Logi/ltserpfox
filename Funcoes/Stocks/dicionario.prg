Function uf_verNoDicionarioSt
	LOCAL lcRefFprod 
	STORE "" TO lcRefFprod 
	
	Select St
	If uf_gerais_actGrelha("",[uCrsFpRef],[SELECT cnp FROM fprod (nolock) WHERE cnp=']+Alltrim(st.ref)+['])
		If Reccount()>0
			lcRefFprod = uCrsFpRef.ref
		Endif
		FECHA([uCrsFpRef])
	Endif
	
	uf_chamaDicionario(lcRefFprod)
EndFunc


**
FUNCTION uf_criaProdFprod
	LOCAL lcStamp, lcvalida
	lcStamp = uf_gerais_stamp()
	lcValida = .f.
	
	SELECT st
		
	IF EMPTY(st.ref)
		uf_perguntalt_chama("A FICHA DO PRODUTO TEM DE ESTAR ABERTA COM O PRODUTO QUE PRETENDE CRIAR NO DICION�RIO.","OK","",64)
		RETURN
	ENDIF
	
	IF !uf_perguntalt_chama("ATEN��O: VAI CRIAR O PRODUTO ["+ALLTRIM(ST.DESIGN)+"] NO DICION�RIO. TEM A CERTEZA QUE PRETENDE CONTINUAR?","Sim","N�o")
		RETURN
	ENDIF
	
	IF uf_gerais_actGrelha("",[uCrsFprodVerify],[select cnp from fprod (nolock) where fprod.cnp=?st.ref])
		IF RECCOUNT()>0
			uf_perguntalt_chama("ESSE PRODUTO J� EXISTE NO DICION�RIO.","OK","",64)
			FECHA("uCrsFprodVerify")
			RETURN
		ENDIF
		fecha("uCrsFprodVerify")
	ENDIF
		
	TEXT TO lcSql NOSHOW TEXTMERGE
		INSERT INTO fprod
			(fprodstamp, ref, cnp, design, grupo)
		Values
			('<<ALLTRIM(lcStamp)>>', '<<ALLTRIM(st.ref)>>', '<<ALLTRIM(st.ref)>>', '<<ALLTRIM(st.design)>>', 'ST')
	ENDTEXT
	IF !uf_gerais_actGrelha("","",lcSql)
		uf_perguntalt_chama("OCORREU UM PROBLEMA A REGISTAR PRODUTO NO DICION�RIO.","OK","",16)
		RETURN
	ELSE
		uf_perguntalt_chama("PRODUTO CRIADO COM SUCESSO.","OK","",64)
	ENDIF
ENDFUNC
