**
FUNCTION uf_inventario_chama
	LPARAMETERS lcStamp
	
	PUBLIC myInventarioAlteracao, myInventarioIntroducao, myControlBeginInventarioRobo, myCodInter, myLastRefRobo
	
	&& CONTROLA PERFIS DE ACESSO
	IF uf_gerais_validaPermUser(ch_userno,ch_grupo,'Invent�rio F�sico')
		IF !myInventario
			uf_perguntalt_chama("PARA ADQUIRIR ESTE M�DULO POR FAVOR ENTRE EM CONTACTO COM SUPORTE! PE�A J� A SUA DEMONSTRA��O.","OK","",64)
			RETURN .f.
		ENDIF

		IF !(TYPE("ATENDIMENTO") == "U")
			uf_perguntalt_chama("N�O PODE ABRIR O ECR� DE INVENT�RIO ENQUANTO O ECR� DE ATENDIMENTO ESTIVER ABERTO.","OK","",48)
			RETURN .f.
		ENDIF
		IF !(TYPE("FACTURACAO") == "U")
			uf_perguntalt_chama("N�O PODE ABRIR O ECR� DE INVENT�RIO ENQUANTO O ECR� DE FACTURA��O ESTIVER ABERTO.","OK","",48)
			RETURN .f.
		ENDIF
	ELSE
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ACEDER AO PAINEL INVENT�RIO F�SICO.","OK","",48)
		RETURN .f.
	ENDIF
	
	LOCAL lcSQL

	**Cria cursores
	If Used("ucrsStic")
		SELECT ucrsStic
		GO top
		SCAN
			Delete
		ENDSCAN
	Endif
		
	If Used("ucrsStil")
		SELECT ucrsStil
		GO top
		SCAN
			Delete
		ENDSCAN
	Endif
	
	IF EMPTY(ALLTRIM(lcStamp))
		TEXT TO lcSQL TEXTMERGE NOSHOW
			SET FMTONLY ON
				SELECT 
					TOP 1 armazem = 1, * 
				from
					stic (nolock)
			SET FMTONLY OFF
		ENDTEXT
		
		IF !uf_gerais_actGrelha("", "ucrsStic",lcSQL)
			uf_perguntalt_chama("OCORREU UMA ANOMALIA NA OBTEN��O DE DADOS DA TABELA DE INVENTARIO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			RETURN .f.
		ENDIF
		 
		&&Linhas do Invent�rio
		TEXT TO lcSQL TEXTMERGE NOSHOW
			SET FMTONLY ON
			SELECT 
				pesquisa = convert(bit,0)
				,ltValida = convert(bit,1)
				,*
				,psico
				,benzo
			FROM stil (NOLOCK)
			INNER JOIN fprod(NOLOCK) ON stil.ref = fprod.ref
			SET FMTONLY OFF
		ENDTEXT
		IF !uf_gerais_actGrelha(IIF(TYPE("inventario")=="U","","inventario.pageframe1.page2.grdInv"), "uCrsInvL",lcSQL)
			uf_perguntalt_chama("OCORREU UMA ANOMALIA NA OBTEN��O DE DADOS DA TABELA DE INVENTARIO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			RETURN .f.
		ENDIF
	ELSE
		TEXT TO lcSQL TEXTMERGE NOSHOW
			SELECT 
				Top 1 armazem = 1, * 
			From
				stic (nolock)
			Where 
				sticstamp = '<<ALLTRIM(lcStamp)>>'
		ENDTEXT
		IF !uf_gerais_actGrelha("", "ucrsStic",lcSQL)
			uf_perguntalt_chama("OCORREU UMA ANOMALIA NA OBTEN��O DE DADOS DA TABELA DE INVENTARIO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			RETURN .f.
		ENDIF
		 
		 
		 **
		 
		 
		&&Linhas do Invent�rio
		TEXT TO lcSQL TEXTMERGE NOSHOW
			SELECT 
				pesquisa = convert(bit,0)
				,ltValida = convert(bit,1)
				,*
				,psico
				,benzo
			FROM stil (NOLOCK)
			LEFT JOIN fprod(NOLOCK) ON stil.ref = fprod.ref
			Where 
				sticstamp = '<<ALLTRIM(lcStamp)>>'
		ENDTEXT 
		IF !uf_gerais_actGrelha(IIF(TYPE("inventario")=="U","","inventario.pageframe1.page2.grdInv"), "uCrsInvL",lcSQL)
			uf_perguntalt_chama("OCORREU UMA ANOMALIA NA OBTEN��O DE DADOS DA TABELA DE INVENTARIO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			RETURN .f.
		ENDIF
	ENDIF

	IF !used("ucrsStil")	
		TEXT TO lcSQL TEXTMERGE NOSHOW
			SET FMTONLY ON
				SELECT 
					TOP 1 
					pesquisa = convert(bit,0)
					,ltValida = CONVERT(bit,0)
					,* 
					,psico
				    ,benzo
				FROM stil (NOLOCK)
				Left JOIN fprod(NOLOCK) ON stil.ref = fprod.ref
			SET FMTONLY OFF
		ENDTEXT
		IF !uf_gerais_actGrelha("", "ucrsStil",lcSQL)
			uf_perguntalt_chama("OCORREU UMA ANOMALIA NA OBTEN��O DE DADOS DA TABELA DE INVENTARIO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			RETURN .f.
		ENDIF
	ENDIF 	
	
	SELECT ucrsStic
	Replace ucrsStic.armazem WITH myArmazem
	
	IF TYPE("INVENTARIO")=="U"
		myLastRefRobo = '0000000'
		
		**Valida Licenciamento	
		IF (uf_gerais_addConnection('INV') == .f.)
			RETURN .F.
		ENDIF
		
		DO FORM INVENTARIO
		INVENTARIO.refresh
	ELSE
		INVENTARIO.show
	ENDIF
	
	&& anular ou lan�ar
	SELECT uCrsStic
	IF uCrsStic.lanca
		inventario.menu1.estado("anular","SHOW")
		inventario.menu1.estado("editar,eliminar,lancar","HIDE")
	ELSE
		inventario.menu1.estado("anular","HIDE")
		inventario.menu1.estado("editar,eliminar,lancar","SHOW")
	ENDIF
	
	inventario.lancar.menu1.estado("", "", "Lan�ar", .t., "Sair", .t.)
	inventario.lancar.menu1.funcaoGravar = "uf_inventario_lancarMovimentos"
	inventario.lancar.menu1.funcaoSair = "uf_receituario_lancarFechar"
	
	inventario.Menu1.gravar.img1.Picture = myPath + "\imagens\icons\guardar_w.png"
	
	uf_inventario_controlaObj()
ENDFUNC


**
FUNCTION uf_inventario_selData
	Public myVirtualVarD 
	myVirtualVarD = 'INVENTARIO.datainv'
	uf_chamaGetDatePresc()
ENDFUNC


** Novo Invent�rio
FUNCTION uf_inventario_novo
	Public myInventarioIntroducao, myInventarioAlteracao 
	
	If myInventarioAlteracao == .t. Or myInventarioIntroducao == .t.
		RETURN .f.
	ENDIF
	**	
	Store .t. To myInventarioIntroducao
	
	**INVENTARIO.lbl1.click
	
	LOCAL lcStamp
	lcStamp = uf_gerais_stamp()
	
	SELECT ucrsStic
	DELETE ALL
	
	SELECT ucrsStil
	DELETE ALL
	
	SELECT uCrsInvL
	DELETE ALL
	
	SET HOURS TO 24
	Atime=ttoc(datetime()+(difhoraria*3600),2)
	Astr=left(Atime,8)
	Select ucrsStic
	Append Blank
	
	Replace ucrsStic.STICSTAMP With Alltrim(lcStamp)
	Replace ucrsStic.data With  datetime()+(difhoraria*3600)
	**Replace ucrsStic.data With DATE()
	replace uCrsStic.hora WITH left(Astr,5)
	**replace uCrsStic.hora WITH left(time(),5)
	
	INVENTARIO.Caption = "Inventario - Introdu��o"
	Inventario.armazem.value = myArmazem
	Inventario.lote.value = ""
	uf_inventario_controlaObj()
	inventario.descricao.setfocus
ENDFUNC 



**
FUNCTION uf_inventario_alterar	
	Public myInventarioIntroducao, myInventarioAlteracao 
	Select uCrsStic

	If !Empty(ucrsStic.STICSTAMP) AND myInventarioAlteracao == .f. AND myInventarioIntroducao == .f.
		
		IF uCrsStic.lanca
			uf_perguntalt_chama("N�o pode editar um invent�rio j� lan�ado.","OK","",32)
			RETURN .f.
		ENDIF 
		**
		Store .t. To myInventarioAlteracao
	
		uf_inventario_controlaObj()
	ENDIF
ENDFUNC 


**
FUNCTION uf_inventario_controlaObj
	Public myInventarioIntroducao, myInventarioAlteracao 

	**Edi��o 
	IF myInventarioIntroducao == .t. OR myInventarioAlteracao == .t.
		WITH INVENTARIO
			IF myInventarioIntroducao == .t.
				INVENTARIO.descricao.readonly = .f.
			ELSE
				INVENTARIO.descricao.readonly = .t.
			ENDIF
			
			.ref.readonly	= .f.
			.design.readonly = .t.
		 	.ref.value = ""
			.design.value = ""
			.datainv.enabled = .t.
			.armazem.enabled = .t.
			.txtHora.readonly	= .f.
			.lote.readonly	= .f.
		
					
			
			IF myInventarioIntroducao == .t.
				.Caption = "Inventario - Introdu��o"
			ENDIF
			IF myInventarioAlteracao == .t.
				.Caption = "Inventario - Altera��o"
			ENDIF
			
			.menu1.estado("contagem,novo,editar,pesquisar,eliminar,anular,lancar","HIDE")
			.menu1.estado("", "SHOW", "GRAVAR", .t., "SAIR", .t.)
			
			IF .pageframe1.activepage == 2
				.pageframe1.page2.grdInv.stock.readonly = .f.
				.pageframe1.page2.grdInv.zona.readonly = .f.
				.pageframe1.page2.grdInv.armazem.readonly = .f.
				.pageframe1.page2.grdInv.lote.readonly = .f.
			ENDIF 
		ENDWITH 
	ELSE
		WITH INVENTARIO
			.Caption = "Inventario"
			.descricao.readonly = .t.
			.ref.readonly	= .t.
			.design.readonly = .t.
			.ref.value = ""
			.design.value = ""
			.datainv.enabled = .f.
			.armazem.enabled = .f.
			.txtHora.readonly	= .t.
			.lote.readonly	= .t.
						
			.menu1.estado("", "SHOW", "GRAVAR", .f., "SAIR", .t.)
			.menu1.estado("contagem,novo,editar,pesquisar,eliminar","SHOW")
			
			SELECT uCrsStic
			IF uCrsStic.lanca
				.menu1.estado("anular","SHOW")
			ELSE
				.menu1.estado("lancar","SHOW")
			ENDIF
			
			.pageframe1.activePage = 2
			.lblDescricao.caption = "Produtos Inventariados"		
			.pageframe1.page2.grdInv.stock.readonly = .t.
			.pageframe1.page2.grdInv.zona.readonly = .t.
			.pageframe1.page2.grdInv.armazem.readonly = .t.
			.pageframe1.page2.grdInv.lote.readonly = .t.
		ENDWITH 
	ENDIF 

	WITH INVENTARIO
		IF Inventario.emcontagem == .f.
			.lblDescricao.caption = "Produtos Inventariados"				
		ELSE
			.menu1.estado("", "", "APLICAR", .t., "CANCELAR", .t.)
			.lblDescricao.caption = "Produtos em contagem ..."
		ENDIF
	ENDWITH 
	
	INVENTARIO.refresh
ENDFUNC


** 
FUNCTION uf_inventario_ultimo
	LOCAL lcSql 

	TEXT TO lcSQL TEXTMERGE NOSHOW
		Select 
			Top 1 sticstamp 
		from
			stic (nolock)
		order by 
			data desc
	ENDTEXT
	IF !uf_gerais_actGrelha("", "ucrsUltimoInv",lcSQL)
		uf_perguntalt_chama("N�O FOI POSS�VEL DETERMINAR O ULTIMO INVENT�RIO.","OK","",16)
		RETURN .f.
	ENDIF 
	IF !EMPTY(ucrsUltimoInv.sticstamp)
		uf_inventario_chama(ALLTRIM(ucrsUltimoInv.sticstamp))
	ELSE
		uf_inventario_chama('')
	ENDIF
	inventario.PageFrame1.ActivePage=2
	up_inventario_actTotaisInvGeral()
ENDFUNC


**
FUNCTION uf_inventario_lancaArtigo
	Lparameters lcRef, lcQtt, lcBool, lcArmazem, lcLote
	
	
	**IF uf_gerais_getParameter('ADM0000000211', 'BOOL')
	IF UPPER(uf_gerais_getParameter("ADM0000000082","TEXT")) == UPPER("ARMAZEM")
		uf_inventario_lancaArtigoComLote(lcRef, lcQtt, lcBool, lcArmazem, lcLote)
	ELSE
		uf_inventario_lancaArtigoSemLote(lcRef, lcQtt, lcBool, lcArmazem, lcLote)
	ENDIF
ENDFUNC

** Lan�a Novo artigo Inventario
FUNCTION uf_inventario_lancaArtigoComLote
	Lparameters lcRef, lcQtt, lcBool, lcArmazem, lcLote
	
	LOCAL lcStamp, lcCursor
	
	** Valida��es de Existencia de Ref�
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_stocks_procuraRef '<<Alltrim(lcRef)>>',<<mysite_nr>>
	ENDTEXT
	If !uf_gerais_actGrelha("","ucrsStInv",lcSQL)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA AO OBTER DADOS DA REFER�NCIA INTRODUZIDA, POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	ENDIF
	
	IF inventario.pageframe1.activepage == 1
		lcCursor = "uCrsStil"
	ELSE
		lcCursor = "uCrsInvL"
	ENDIF

	If Reccount("ucrsStInv") > 0
		SELECT ucrsStInv
		INVENTARIO.design.Value = Alltrim(ucrsStInv.design)
		INVENTARIO.ref.value = Alltrim(ucrsStInv.ref)
	
		Select &lcCursor
		Locate For Upper(Alltrim(&lcCursor..ref)) == Upper(Alltrim(ucrsStInv.ref)) AND &lcCursor..armazem == lcArmazem AND Upper(Alltrim(&lcCursor..lote)) == Upper(Alltrim(lcLote))
		If Found()
			Select &lcCursor
			Replace Ref 		WITH Alltrim(ucrsStInv.ref)
			Replace Design 		WITH Alltrim(ucrsStInv.design)
			Replace stock 		WITH &lcCursor..stock + lcQtt
			Replace lordem 		WITH Recno(lcCursor) * 1000 
			Replace ltValida 	WITH .t.
			Replace armazem 	WITH lcArmazem
			Replace lote 		WITH lcLote
		ELSE
			lcStamp = uf_gerais_stamp()
			
			Select &lcCursor
			Append Blank
			Replace stilstamp 	With lcStamp 
			Replace Ref 		With Alltrim(ucrsStInv.ref)
			Replace Design 		WITH Alltrim(ucrsStInv.design)
			Replace stock 		With IIF(EMPTY(lcQtt),0,lcQtt)
			Replace lordem 		WITH Recno(lcCursor) * 1000 
			Replace Zona 		WITH "P"
			Replace ltValida 	WITH .t.
			Replace armazem 	WITH lcArmazem
			Replace lote 		WITH lcLote
		ENDIF
		up_inventario_actRpInv()
	Else
		** Veio de importa��o de Ficheiro *********
		If lcBool
		
			Locate For Upper(Alltrim(&lcCursor..ref)) == Upper(Alltrim(lcRef)) AND &lcCursor..armazem == lcArmazem AND Upper(Alltrim(&lcCursor..lote)) == Upper(Alltrim(lcLote))
			If Found()
				Select &lcCursor
				Replace Ref 		With Alltrim(lcRef)
				Replace stock 		With &lcCursor..stock + lcQtt
				Replace lordem 		With Recno(lcCursor) * 1000 
				Replace ltValida 	With .f.
				Replace armazem 	WITH lcArmazem
				Replace lote 		WITH lcLote
			ELSE
				lcStamp = uf_gerais_stamp()
				
				Select &lcCursor
				Append Blank
				Replace stilstamp 	With lcStamp
				Replace Ref 		With Alltrim(lcRef)
				Replace stock		WITH IIF(EMPTY(lcQtt),0,lcQTT)
				Replace lordem 		WITH Recno(lcCursor) * 1000 
				Replace Zona		WITH "P"
				Replace ltValida 	WITH .f.
				Replace armazem 	WITH lcArmazem
				Replace lote 		WITH lcLote
			Endif
			**
		
		ELSE
			uf_perguntalt_chama("A REFER�NCIA INTRODUZIDA N�O EXISTE. POR FAVOR VERIFIQUE.","OK","",64)	
		ENdif
	ENDIF
	IF USED("ucrsStInv")
		Fecha("ucrsStInv")
	ENDIF 
	
	** Selecciona a posi��o Alterada
	INVENTARIO.Refresh
ENDFUNC 

** Lan�a Novo artigo Inventario
FUNCTION uf_inventario_lancaArtigoSemLote
	Lparameters lcRef, lcQtt, lcBool, lcArmazem, lcLote
	

	LOCAL lcStamp, lcCursor
	
	** Valida��es de Existencia de Ref�
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_stocks_procuraRef '<<Alltrim(lcRef)>>',<<mysite_nr>>
	ENDTEXT
	If !uf_gerais_actGrelha("","ucrsStInv",lcSQL)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA AO OBTER DADOS DA REFER�NCIA INTRODUZIDA, POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	ENDIF
	
	IF inventario.pageframe1.activepage == 1
		lcCursor = "uCrsStil"
	ELSE
		lcCursor = "uCrsInvL"
	ENDIF

	If Reccount("ucrsStInv") > 0
		SELECT ucrsStInv
		INVENTARIO.design.Value = Alltrim(ucrsStInv.design)
		INVENTARIO.ref.value = Alltrim(ucrsStInv.ref)
	
		Select &lcCursor
		Locate For Upper(Alltrim(&lcCursor..ref)) == Upper(Alltrim(ucrsStInv.ref)) AND &lcCursor..armazem == lcArmazem AND Upper(Alltrim(&lcCursor..lote)) == Upper(Alltrim(lcLote))
		If Found()
			Select &lcCursor
			Replace Ref 		WITH Alltrim(ucrsStInv.ref)
			Replace Design 		WITH Alltrim(ucrsStInv.design)
			Replace stock 		WITH &lcCursor..stock + lcQtt
			Replace lordem 		WITH Recno(lcCursor) * 1000 
			Replace ltValida 	WITH .t.
			Replace armazem 	WITH lcArmazem
			Replace lote 		WITH IIF(EMPTY(lcLote), '', lcLote)
		ELSE
			lcStamp = uf_gerais_stamp()
			
			Select &lcCursor
			Append Blank
			Replace stilstamp 	With lcStamp 
			Replace Ref 		With Alltrim(ucrsStInv.ref)
			Replace Design 		WITH Alltrim(ucrsStInv.design)
			Replace stock 		With lcQtt
			Replace lordem 		WITH Recno(lcCursor) * 1000 
			Replace Zona 		WITH "P"
			Replace ltValida 	WITH .t.
			Replace armazem 	WITH lcArmazem
			Replace lote 		WITH IIF(EMPTY(lcLote), '', lcLote)
		ENDIF
		up_inventario_actRpInv()
	ELSE
		** Veio de importa��o de Ficheiro *********
		If lcBool
		
			Locate For Upper(Alltrim(&lcCursor..ref)) == Upper(Alltrim(lcRef)) AND &lcCursor..armazem == lcArmazem AND Upper(Alltrim(&lcCursor..lote)) == Upper(Alltrim(lcLote))
			If Found()
				Select &lcCursor
				Replace Ref 		With Alltrim(lcRef)
				Replace stock 		With &lcCursor..stock + lcQtt
				Replace lordem 		With Recno(lcCursor) * 1000 
				Replace ltValida 	With .f.
				Replace armazem 	WITH lcArmazem
				Replace lote 		WITH IIF(EMPTY(lcLote), '', lcLote)
			ELSE
				lcStamp = uf_gerais_stamp()
				
				Select &lcCursor
				Append Blank
				Replace stilstamp 	With lcStamp
				Replace Ref 		With Alltrim(lcRef)
				Replace stock		WITH lcQtt
				Replace lordem 		WITH Recno(lcCursor) * 1000 
				Replace Zona		WITH "P"
				Replace ltValida 	WITH .f.
				Replace armazem 	WITH lcArmazem
				Replace lote 		WITH IIF(EMPTY(lcLote), '', lcLote)
			Endif
			*************************************
		
		ELSE
			uf_perguntalt_chama("A REFER�NCIA INTRODUZIDA N�O EXISTE. POR FAVOR VERIFIQUE.","OK","",64)	
		ENdif
	ENDIF
	IF USED("ucrsStInv")
		Fecha("ucrsStInv")
	ENDIF 
	
	** Selecciona a posi��o Alterada
	INVENTARIO.Refresh
ENDFUNC 



**
FUNCTION up_inventario_actRpInv
	Local lcPos, lcTot, lcTotRef
	Store 0 To lcPos, lcTot, lcTotRef
		
	Select ucrsStil
	lcPos = Recno()
	
	** quantidade total
	Select ucrsStil
	Calculate Sum(ucrsStil.stock) To lcTot
	INVENTARIO.pageframe1.page1.qttotal.value = lcTot
	
	** nr de referencias
	Select ucrsStil
	Count To lcTotRef
	INVENTARIO.pageframe1.page1.reftotal.value = lcTotRef
	
	Select ucrsStil
	If lcPos>0 And lcPos<=lcTotRef
		TRY
			GO lcPos
		CATCH
		ENDTRY
	Endif
ENDFUNC 


**
FUNCTION uf_inventario_apagaLinha
	If myInventarioIntroducao == .t. or  myInventarioAlteracao == .t.
		Select ucrsStil
		If uf_perguntalt_chama("TEM A CERTEZA QUE DESEJA APAGAR A REF: "+Alltrim(ucrsStil.REF)+" DA LISTAGEM?","Sim","N�o")
			Select ucrsStil
			Delete
		Endif
		Select ucrsStil
		GO Top
		
		INVENTARIO.pageframe1.page1.gridPesq.Refresh
		up_inventario_actRpInv()
		INVENTARIO.refresh
	ENDIF 
ENDFUNC


**
** Corrige a Linha
Function uf_inventario_CorrigeLinha
	Local lcRef
	Store '' To lcRef

	INVENTARIO.pageframe1.page1.gridPesq.setFocus
	
	If Used("ucrsStil")
		Select ucrsStil
		If !empty(ucrsStil.ref)
			lcRef = inputbox('Nova Refer�ncia','','')		
				
			If !Empty(lcRef)
				
				lcSQL = ""
				Text To lcSQL Textmerge  Noshow
					exec up_stocks_procuraRef '<<Alltrim(lcRef)>>',<<mysite_nr>>
				Endtext
				If uf_gerais_actGrelha("","uc_refExistInv",lcSQL)
					If Reccount("uc_refExistInv") > 0
						Select ucrsStil
						Delete
					Endif
				Endif	
				
				uf_inventario_lancaArtigo(lcRef,ucrsStil.stock,.f.,ucrsStil.armazem)
			Endif
	
		Endif
		up_inventario_actRpInv()
	Endif
Endfunc


**
Function uf_inventario_Gravar
	Public myInvDesvios
    
    inventario.descricao.setfocus()
    
	Local lcValidaInsercaoInventario, lcValidaActualizaInventario
	Store .f. To  lcValidaInsercaoInventario, lcValidaActualizaInventario,  lcvalidaCamposObrigatoriosInventario
	
	** Valida Campos Obrigatorios
	IF uf_inventario_CamposObrig() == .f.
		RETURN .f.
	ENDIF
	
	If myInventarioIntroducao == .t. && Introdu��o

		** Trata Transac��es para garantir a Integridade de Dados
		if uf_gerais_actGrelha ("","",[BEGIN TRANSACTION])
			If uf_inventario_insere() == .t. AND uf_inventario_insereLinhas() == .t.
				&& Tudo Ok, insere o Documento na BD
				uf_gerais_actGrelha("","",[COMMIT TRANSACTION])
				
				uf_perguntalt_chama("INVENT�RIO INTRODUZIDO COM SUCESSO!","OK","",64)
				Select ucrsStil
				Delete All
				
				INVENTARIO.ref.value		= ""
				INVENTARIO.design.value 	= ""
			Else
				uf_gerais_actGrelha("","",[ROLLBACK])		
				RETURN .f.
			Endif
		Else
			RETURN .f.
		Endif	
	Endif
	
	If myInventarioAlteracao == .t. && Altera��o
		if uf_gerais_actGrelha ("","",[BEGIN TRANSACTION])

			If uf_inventario_actualizaCab() == .t. AND uf_inventario_actualizaLinhas() == .t.
				&& Tudo Ok, insere o Documento na BD
				uf_gerais_actGrelha("","",[COMMIT TRANSACTION])	
				
				uf_perguntalt_chama("INVENTARIO ACTUALIZADO COM SUCESSO!","OK","",64)
				Select ucrsStil
				Delete All
				
				INVENTARIO.ref.value		= ""
				INVENTARIO.design.value 	= ""
			Else
				uf_gerais_actGrelha("","",[ROLLBACK])
				RETURN .f.
			Endif
		Else
			RETURN .f.
		Endif
	Endif
	
	myInvDesvios = .f.
	myInventarioIntroducao = .f.
	myInventarioAlteracao	= .f.
	Inventario.emcontagem = .f.
	uf_inventario_controlaObj()
	
	SELECT ucrsStic
	uf_inventario_chama(ALLTRIM(uCrsStic.sticstamp))
ENDFUNC 


**Update Cabecalho
Function uf_inventario_actualizaCab
	Local lcSQL
	
	SET HOURS TO 24
	Atime=ttoc(datetime()+(difhoraria*3600),2)
	Astr=left(Atime,8)
	lcHora	= Astr 
	**lcHora	= Time() 
	
	***
	Select ucrsStic
	Text to lcSql noshow textmerge
		Update	stic
		Set		descricao	= '<<Alltrim(ucrsStic.descricao)>>',
				usrinis		= '<<m_chinis>>', 
				usrdata		= convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),112), 
				usrhora		= '<<lcHora>>',
				data		= '<<uf_gerais_getDate(ucrsStic.data,"SQL")>>',
				hora		= '<<uCrsStic.hora>>',
				ccusto		= '<<uCrsStic.ccusto>>'
		From	stic
		Where	sticstamp	= '<<ucrsStic.sticstamp>>'	
	Endtext
	if !uf_gerais_actGrelha("","",lcSql)
		uf_perguntalt_chama("N�O FOI POSS�VEL GRAVAR O INVENT�RIO. [STIC]","OK","",48)
		RETURN .f.
	ENDIF
	Return .t.
Endfunc 


**
FUNCTION uf_inventario_actualizaLinhas
	Local lcSQL, lcStamp, lcAdiciona
	
	IF inventario.pageframe1.activepage == 1
		TRY
			SELECT uCrsStil
			SKIP -1
			SKIP 1
		CATCH
		ENDTRY
		SELECT * FROM uCrsStil INTO CURSOR uCrsUpLinhas READWRITE
		lcAdiciona = .t.
	ELSE
		TRY
			SELECT uCrsInvL
			SKIP -1
			SKIP 1
		CATCH
		ENDTRY
		SELECT * FROM uCrsInvL INTO CURSOR uCrsUpLinhas READWRITE
		lcAdiciona = .f.
	ENDIF
		
	* inicializa a r�gua apresentando um t�tulo e o n� total de registos
	regua(0,reccount("uCrsUpLinhas"),"A PROCESSAR...",.f.)
	
	SELECT uCrsUpLinhas
	GO TOP
	SCAN
		** actualizar regua **
		regua[1,recno("uCrsUpLinhas"),"A PROCESSAR PRODUTO: "+ALLTRIM(uCrsUpLinhas.design),.f.]
		
		SET HOURS TO 24
		Atime=ttoc(datetime()+(difhoraria*3600),2)
		Astr=left(Atime,8)
		lchora = Astr
		**lchora = Time()
		
		***VERIFICA SE A LINHA EXISTE
		lcSQL = ""
		TEXT TO lcSQL NOSHOW textmerge
			Select Ref From STIL (nolock) Where Rtrim(Ltrim(Ref)) = '<<Alltrim(uCrsUpLinhas.Ref)>>' AND Rtrim(Ltrim(Lote)) = '<<Alltrim(uCrsUpLinhas.Lote)>>' AND STICSTAMP = '<<Alltrim(ucrsStic.sticstamp)>>' AND armazem = <<uCrsUpLinhas.armazem>>
		ENDTEXT

*!*	_CLIPTEXT = lcSQL
*!*	MESSAGEBOX(lcSQL)


		IF !uf_gerais_actGrelha("","lcExisteSTIL",lcSQL)
			uf_perguntalt_chama("OCORREU UM ERRO A VERIFICAR EXISTENCIA DA LINHA DO INV., POR FAVOR CONTACTE O SUPORTE."+CHR(10)+"Cod: ACTSTIL-VERIFICAEXISTESTIL","OK","",16)
			REGUA(2)
			RETURN .f.
		Endif
			
		*EXISTE ACTUALIZA
		If Reccount("lcExisteSTIL")> 0
			lcSQL = ""
			TEXT TO lcSQL NOSHOW TEXTMERGE
				UPDATE	
					STIL
				SET		
					design	= '<<Alltrim(uCrsUpLinhas.design)>>',
					stock	= <<IIF(lcAdiciona,"(stock + "+astr(uCrsUpLinhas.stock)+")",uCrsUpLinhas.stock)>>,
					/*stock	= <<IIF(lcAdiciona,"case when (stock + "+astr(uCrsUpLinhas.stock)+") < 0 then 0 else (stock + "+astr(uCrsUpLinhas.stock)+") end",uCrsUpLinhas.stock)>>,*/
					zona	= case when zona='R' then zona else '<<Alltrim(uCrsUpLinhas.zona)>>' end,
					lote 	= '<<Alltrim(uCrsUpLinhas.lote)>>',
					usrinis = '<<m_chinis>>', 
					usrdata = CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),112), 
					usrhora = '<<lcHora>>'
					<<IIF(!myInvDesvios,"",IIF(lcAdiciona,",u_qtt = ("+astr(uCrsUpLinhas.stock)+")",",u_qtt = "+astr(uCrsUpLinhas.stock)))>>
				WHERE	
					REF = '<<Alltrim(uCrsUpLinhas.REF)>>'
					AND ARMAZEM = <<uCrsUpLinhas.ARMAZEM>>
					AND STICSTAMP = '<<Alltrim(ucrsStic.sticstamp)>>'	
					and Rtrim(Ltrim(Lote)) = '<<Alltrim(uCrsUpLinhas.Lote)>>'
			ENDTEXT
		
			IF !uf_gerais_actGrelha("","",lcSQL)
				uf_perguntalt_chama("OCORREU UM ERRO A ACTUALIZAR AS LINHAS DO INVENTARIO. POR FAVOR CONTACTE O SUPORTE."+CHR(10)+"Cod: ACTINV-STIL","OK","",16)
				REGUA(2)
				Return .f.
			Endif
		ELSE && A REF N�O EXISTE - INSERE
			Select 	ucrsStic
			Select 	uCrsUpLinhas
			
			lcSql = ""
			lcStamp = uf_gerais_stamp()
			Text to lcSql noshow textmerge
				INSERT INTO STIL (
					stilstamp
					,ref
					,design
					,data
					,stock
					,sticstamp
					,armazem
					,lordem
					,zona
					,lote
					,ousrinis
					,ousrdata
					,ousrhora
					,usrinis
					,usrdata
					,usrhora
					<<IIF(!myInvDesvios, "", ",u_qtt")>>
				)VALUES( 
					'<<Alltrim(lcStamp)>>'
					,'<<Alltrim(uCrsUpLinhas.ref)>>'
					,'<<Alltrim(uCrsUpLinhas.design)>>'
					,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),112)
					,<<uCrsUpLinhas.stock>>
					,'<<Alltrim(ucrsStic.sticstamp)>>'
					,<<uCrsUpLinhas.armazem>>
					,<<uCrsUpLinhas.lordem>>
					,'<<Alltrim(uCrsUpLinhas.zona)>>'
					,'<<Alltrim(uCrsUpLinhas.Lote)>>'
					,'<<m_chinis>>'
					,CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),112)
					,'<<lcHora>>'
					,'<<m_chinis>>'
					,CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),112)
					,'<<lcHora>>'
					<<IIF(!myInvDesvios, "", "," + astr(uCrsUpLinhas.stock))>> 
				)
			ENDTEXT
			
			If !uf_gerais_actGrelha("","",lcSql)
				uf_perguntalt_chama("OCORREU UM ERRO A ACTUALIZAR AS LINHAS DO INVENTARIO. POR FAVOR CONTACTE O SUPORTE."+CHR(10)+"Cod: ACTINV-STIL","OK","",16)
				Return .f.
			ENDIF 
		Endif
		
		SELECT uCrsUpLinhas	
	ENDSCAN

	** Apagar linhas que j� n�o existem 	
	IF inventario.pageframe1.activepage == 2
		IF USED("uCrsInvDel")

			local lcRef
			lcRef =''

			select uCrsInvDel 
			SCAN 
				lcRef = lcRef + "'"+alltrim(uCrsInvDel.stilstamp)+"',"
			ENDSCAN 

			IF !EMPTY(ALLTRIM(lcRef))
				TEXT TO lcSQL TEXTMERGE NOSHOW
					DELETE FROM stil
					WHERE 
						stilstamp in (<<LEFT(lcRef,LEN(lcRef)-1)>>)
						and sticstamp = '<<ALLTRIM(uCrsStic.sticstamp)>>'
				ENDTEXT
				uf_gerais_actGrelha("","",lcSQL)
			ENDIF
		
			fecha("uCrsInvDel")
		ENDIF
	ENDIF
	
	** fechar regua
	REGUA(2)
	
	Return .t.
ENDFUNC 


**
FUNCTION uf_inventario_insere
	Local lcSQL
	
	SET HOURS TO 24
	Atime=ttoc(datetime()+(difhoraria*3600),2)
	Astr=left(Atime,8)
	lcHora	= Astr
	**lcHora	= Time() 
	lcData 	= datetime()+(difhoraria*3600)
	**lcData 	= DATE()
	***
	Select ucrsStic
	Text to lcSql noshow textmerge
		INSERT INTO stic(
			sticstamp, data, descricao, lanca,
			stamp, ccusto, ousrinis, ousrdata, ousrhora,
			usrinis, usrdata, usrhora, hora
			)
		Values (
			'<<ucrsStic.sticstamp>>', '<<uf_gerais_getDate(ucrsStic.data,"SQL")>>','<<Alltrim(ucrsStic.descricao)>>',<<Iif(ucrsStic.lanca,1,0)>>,
			'<<ucrsStic.stamp>>','<<ucrsStic.ccusto>>', '<<m_chinis>>', 
			CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),112), '<<lcHora>>', '<<m_chinis>>', CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),112), '<<lcHora>>', '<<uCrsStic.hora>>'
			)
	ENDTEXT
		
	IF !uf_gerais_actGrelha("","",lcSql)
		uf_perguntalt_chama("N�O FOI POSS�VEL GRAVAR O INVENT�RIO. [STIC]","OK","",48)
		RETURN .f.
	ENDIF

	RETURN .t.
ENDFUNC 


**
FUNCTION uf_inventario_insereLinhas
	LOCAL lcSQL, lcStamp, lcHora
	
	SET HOURS TO 24
	Atime=ttoc(datetime()+(difhoraria*3600),2)
	Astr=left(Atime,8)
	lcHora	= Astr 
	**lcHora	= Time() 
	
	IF inventario.pageframe1.activepage == 1
		TRY
			SELECT uCrsStil
			SKIP -1
			SKIP 1
		CATCH
		ENDTRY
		SELECT * FROM uCrsStil INTO CURSOR uCrsInsLinhas READWRITE
	ELSE
		TRY
			SELECT uCrsInvL
			SKIP -1
			SKIP 1
		CATCH
		ENDTRY
		SELECT * FROM uCrsInvL INTO CURSOR uCrsInsLinhas READWRITE
	ENDIF
	
	SELECT uCrsInsLinhas
	GO TOP
	SCAN
		lcStamp = uf_gerais_stamp()
		Text to lcSql noshow textmerge
			INSERT INTO STIL(
				stilstamp
				,ref
				,design
				,data
				,stock
				,sticstamp
				,armazem
				,lordem
				,zona
				,lote
				,ousrinis
				,ousrdata
				,ousrhora
				,usrinis
				,usrdata
				,usrhora
			)VALUES( 
				'<<Alltrim(lcStamp)>>'
				,'<<Alltrim(uCrsInsLinhas.ref)>>'
				,'<<Alltrim(uCrsInsLinhas.design)>>'
				,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),112)
				,<<uCrsInsLinhas.stock>>
				,'<<Alltrim(ucrsStic.sticstamp)>>'
				,<<uCrsInsLinhas.armazem>>
				,<<uCrsInsLinhas.lordem>>
				,'<<Alltrim(uCrsInsLinhas.zona)>>'
				,'<<Alltrim(uCrsInsLinhas.lote)>>'
				,'<<m_chinis>>'
				,CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),112)
				,'<<lcHora>>'
				,'<<m_chinis>>'
				,CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),112)
				,'<<lcHora>>'
			)
		Endtext

		If !uf_gerais_actGrelha("","",lcSql)
			uf_perguntalt_chama("N�O FOI POSS�VEL GRAVAR O INVENT�RIO. [STIL]","OK","",48)
			Return .f.
		Endif
	
		Select uCrsInsLinhas
	ENDSCAN
	
	RETURN .t.
ENDFUNC 


** Valida Campos Obrigat�rios Inventario
FUNCTION uf_inventario_CamposObrig
	Local lcSQL
	
	Select ucrsStic
	GO TOP
	If Empty(ucrsStic.DESCRICAO)
		uf_perguntalt_chama("EXISTEM CAMPOS OBRIGAT�RIOS POR PREENCHER. POR FAVOR VERIFIQUE. CAMPO: DESCRI��O.","OK","",64)
		Return .f.
	Endif
	If !myInventarioAlteracao 
		TEXT TO lcSql NOSHOW TEXTMERGE
			SELECT
				COUNT(*) as contador 
			From
				stic (nolock) 
			Where
				Upper(Ltrim(Rtrim(descricao))) = '<<Upper(Alltrim(ucrsStic.descricao))>>'
		ENDTEXT
		if !uf_gerais_actGrelha("","uc_DescrInv",lcSql)
			uf_perguntalt_chama("N�O FOI POSS�VEL VERIFICAR AS DESIGNA��ES DE INVENT�RIO. [STIC]","OK","",48)
			Return .f.
		ENDIF
			
		If uc_DescrInv.contador >0
			uf_perguntalt_chama("J� EXISTE UM INVENT�RIO COM ESSA DESIGNA��O. POR FAVOR VERIFIQUE.","OK","",64)
			Return .f.
		Endif
	Endif
	
	** Valida Linhas Com Refer�ncias N�o V�lidas
	IF inventario.pageframe1.activepage == 1
		Select ucrsStil
		GO TOP
		SCAN FOR ucrsStil.ltValida == .f.
			uf_perguntalt_chama("EXISTEM LINHAS COM REFER�NCIA INV�LIDA. POR FAVOR VERIFIQUE.","OK","",64)
			RETURN .f.
		ENDSCAN
	ENDIF
	
	** Valida hora
	Select ucrsStic
	IF (ucrsStic.hora > '23:59')
		uf_perguntalt_chama("Hora inv�lida. Por favor verifique.","OK","",64)
		RETURN .f.
	ENDIF
			
	RETURN .t.
ENDFUNC


** Chamada do Menu
FUNCTION uf_inventario_sair
	uf_inventario_exit()
ENDFUNC


**
FUNCTION uf_inventario_exit
	Public myInventarioIntroducao, myInventarioAlteracao 
	
	IF !(TYPE("INVENTARIO") == "U")
		inventario.ref.setfocus
	ENDIF 
	
	* Verifica se o Documento esta em Modo de Edicao ou Introducao
	If myInventarioIntroducao == .t. OR myInventarioAlteracao == .t.
		If uf_perguntalt_chama('Quer mesmo cancelar o invent�rio?'+CHR(13)+CHR(13)+'Ir� perder as �ltimas altera��es feitas',"Sim","N�o")
	
			Inventario.emcontagem = .f.
			myInventarioIntroducao	= .f.
			myInventarioAlteracao = .f.
			uf_inventario_controlaObj()
			SELECT uCrsStic
			uf_inventario_chama(ALLTRIM(uCrsStic.sticstamp))
		ENDIF 
	ELSE 
		INVENTARIO.release

		&& Liberta variaveis publicas
		myInventarioIntroducao	= .f.
		myInventarioAlteracao = .f.
		
		Release myCodInter
 		Release myControlBeginInventarioRobo
		RELEASE INVENTARIO
	ENDIF 
ENDFUNC


**
FUNCTION uf_inventario_setTimerInv
	
	IF myInventarioAlteracao == .f. AND myInventarioIntroducao == .f.
		uf_perguntalt_chama("DEVE COLOCAR O INVENTARIO EM MODO DE ALTERA��O OU INTRODU��O PARA USAR ESTA FUNCIONALIDADE","OK","", 64)
		RETURN .f.
	ENDIF
	
	&& valida se usa robo
	If ((!myUsaRobot And !myUsaRobotApostore) and !myUsaFarmax) AND myUsaServicosRobot = .f.  
		uf_perguntalt_chama("ESTA OP��O APENAS EST� ACTIVA EM FARM�CIAS QUE USEM ROB�!","OK","",64)
		RETURN .f.
	Endif 

	** Controla se um Invent�rio j� chegou ao fim **
	If myControlBeginInventarioRobo
		uf_perguntalt_chama("J� EFECTUOU UM INVENT�RIO ROB�TICO COMPLETO, N�O DEVE REALIZAR OUTRO.","OK","",64)
		RETURN .f.
	Endif
	
	&& valida separador
	IF Inventario.emcontagem == .f.
		uf_inventario_contagem()
	ENDIF  
	
	IF myUsaServicosRobot = .f. 
	
		If INVENTARIO.pageframe1.page1.btnRobot.visible == .f.
		
			Select ucrsStil
			Count To lcNrRows For Alltrim(zona)=='P'
			If lcNrRows>0
				uf_perguntalt_chama("INVENT�RIO DO ROB� DEVE SER EXECUTADO ISOLADAMENTE.","OK","",64)
				Go Top
				RETURN .f.
			Endif
			
			If INVENTARIO.pageframe1.page1.tsc2.visible == .f.
			
				INVENTARIO.pageframe1.page1.Label9.visible = .t.
				INVENTARIO.pageframe1.page1.tsc2.visible	= .t.
				INVENTARIO.pageframe1.page1.Label12.visible	= .t.
				INVENTARIO.pageframe1.page1.tsc6.visible	= .t.
				INVENTARIO.pageframe1.page1.Label13.visible	= .t.
				INVENTARIO.pageframe1.page1.lastref.visible	= .t.
		
				INVENTARIO.pageframe1.page1.tsc2.value = 0
				INVENTARIO.pageframe1.page1.tsc6.value = 0
				
				INVENTARIO.ref.readonly = .t.

				uf_robot_tecnilabMsgK(myLastRefRobo)
			Endif
			
			If Type("INVENTARIO.MyTimerInv") # "O"
				INVENTARIO.AddObject('MyTimerInv','MyTimerInv')
			Else
				INVENTARIO.MyTimerInv.enabled = .t.
			ENDIF

			INVENTARIO.pageframe1.page1.btnRobot.visible = .t.

		ELSE

			INVENTARIO.MyTimerInv.enabled = .f.
			INVENTARIO.pageframe1.page1.btnRobot.visible = .f.
			
			If INVENTARIO.pageframe1.page1.tsc2.visible
				
				INVENTARIO.pageframe1.page1.Label9.visible = .f.
				INVENTARIO.pageframe1.page1.tsc2.visible	= .f.
				INVENTARIO.pageframe1.page1.Label12.visible = .f.
				INVENTARIO.pageframe1.page1.tsc6.visible	= .f.
				INVENTARIO.pageframe1.page1.Label13.visible = .f.
				INVENTARIO.pageframe1.page1.lastref.visible = .f.
			Endif
			
			uf_inventario_RoboInv()
		ENDIF 
	ELSE
		uf_inventario_RoboInv_servicos()
	ENDIF 
	
Endfunc


**
FUNCTION uf_inventario_RoboInv
	
	
	Local lcRefc, lcStamp
	Store '' To lcRefc
	
	TEXT TO lcSql NOSHOW TEXTMERGE
		exec up_robot_respostaK '<<astr(myTermNo)>>', <<Iif(myUsaRobotApostore, 1, 0)>>
	ENDTEXT 
	If !uf_gerais_actGrelha("","uCrsRoboInvf",lcSql)
		uf_perguntalt_chama("OCORREU UM ERRO A VALIDAR O INVENT�RIO DO ROBOT. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	Else
		
		If Reccount()>0
			select uCrsRoboInvf
			* a vari�vel guarda o total de registos do cursor tempcursor
			mntotal=reccount()
			* inicializa a r�gua apresentando um t�tulo e o n� total de registos
			regua(0,mntotal,"A PROCESSAR PRODUTOS LIDOS DO ROB�...",.f.)
			
			Select uCrsRoboInvf
			Go Top
			Scan For uCrsRoboInvf.conferido==0	 And !(Alltrim(uCrsRoboInvf.num_foll)=='00')
				** actualiza conferido na tabela
				uf_gerais_actGrelha("","",[update B_movtec_robot set conferido=1 where id=]+astr(uCrsRoboInvf.id))
			
				Try
					lcRefc = val(Alltrim(uCrsRoboInvf.ref))
				Catch
					lcRefc = Alltrim(uCrsRoboInvf.ref)
				Endtry
				
				Try
					lcRefc = Str(lcRefc)
				Catch
					***
				Endtry
				
				** Valida Refer�ncia **
				Text To lcSQL Textmerge  Noshow
					exec up_stocks_procuraRef '<<Alltrim(lcRefc)>>',<<mysite_nr>>
				Endtext
				If !uf_gerais_actGrelha("","uc_stInv",lcSQL)
					uf_perguntalt_chama("OCORREU UMA ANOMALIA AO OBTER DADOS DA REFER�NCIA INTRODUZIDA, POR FAVOR CONTACTE O SUPORTE.","OK","",16)
					RETURN .f.
				Else
					If Reccount()>0	
						** actualizar regua **
						regua[1,recno("uCrsRoboInvf"),"A PROCESSAR PRODUTO: "+ALLTRIM(uc_stInv.design), .f.]
						
						** adicionar produto ao invent�rio **
						Select ucrsStil
						Locate For Alltrim(ucrsStil.ref)==Alltrim(uc_stInv.ref)
						If !Found()
							lcStamp = uf_gerais_stamp()
							Select ucrsStil
							Append Blank
							replace ucrsStil.stilstamp	WITH lcStamp
							replace ucrsStil.ref		WITH Alltrim(uc_stInv.ref)
							replace ucrsStil.design		WITH Alltrim(uc_stInv.design)
							replace ucrsStil.stock		WITH uCrsRoboInvf.stock
							replace ucrsStil.ltValida	WITH .t.
							replace ucrsStil.zona		WITH 'R'
							replace ucrsStil.armazem 	WITH ucrsStic.armazem
						ENDIF
					ENDIF
					
					Fecha("uc_stinv")		
				ENDIF
				
				SELECT uCrsRoboInvf
			ENDSCAN
			
			* fecha a r�gua
			regua(2)
			
			uf_perguntalt_chama("OS PRODUTOS DO ROB� FORAM ADICIONADOS COM SUCESSO. DEVE GRAVAR O INVENT�RIO DE SEGUIDA.","OK","",64)
			
			myControlBeginInventarioRobo = .T. && N�o deixa correr o invent�rio depois de j� ter completado um
			
			INVENTARIO.pageframe1.page1.gridPesq.Refresh
			
			** actualizar totais **
			up_inventario_actRpInv()
			****************
		ENDIF
		
		Fecha("uCrsRoboInvf")
	Endif
ENDFUNC 


FUNCTION uf_INVENTARIO_importaFicheiroTxt
	
	
	IF uf_perguntalt_chama("Indique o formato do ficheiro txt a importar." + CHR(13) +;
		" 1 - " +;
		" <ref><tab><stock><tab><armazem><lote>" + CHR(13) +;
		" 2 -"+ CHR(13) +;
		" <ref>(13)<lote>(10)<stock>(4)","Formato 1","Formato 2",64)
		
		uf_INVENTARIO_importaFicheiroTxt1()
	ELSE
		uf_INVENTARIO_importaFicheiroTxt2()	
	ENDIF


ENDFUNC



** Formato obrigatorio: <ref><tab><stock><tab><armazem><lote>
** No caso de ficheiros excel dever�o ser convertidos para ficheiros txt (copy paste)
** N�o interessa o numero de espa�os
** Importa Ficheiro txt
FUNCTION uf_INVENTARIO_importaFicheiroTxt1
	PUBLIC myInventarioAlteracao, myInventarioIntroducao
	
	IF myInventarioAlteracao == .f. AND myInventarioIntroducao == .f.
		uf_perguntalt_chama("DEVE COLOCAR O INVENTARIO EM MODO DE ALTERA��O OU INTRODU��O PARA USAR ESTA FUNCIONALIDADE","OK","", 64)
		RETURN .f.
	ENDIF
	
	&& valida separador
	IF Inventario.emcontagem == .f.
		uf_inventario_contagem()
	ENDIF 

	IF USED("ucrsTempFile")
		fecha("ucrsTempFile")
	ENDIF

	LOCAL uv_file
	STORE '' TO uv_file

	uv_file = uf_gerais_getFile(ALLTRIM(uf_gerais_getUmValor("B_terminal", "pathReport", "terminal = '" + ALLTRIM(myTerm) + "'")), 'txt')

	IF EMPTY(uv_file)
		RETURN .F.
	ENDIF
		
	CREATE cursor ucrsTempFile (ref c(32), stock n(9), armazem n(3), lote c(60), ltValida l)
	try


		APPEND FROM (uv_file) TYPE DELIMITED WITH TAB
	catch
		**
	endtry

	Select ucrsTempFile 
	Go Top
	SCAN FOR !EMPTY(ucrsTempFile.ref)
			
		** Verifica se a ref existe, caso nao exista marca linha 
		lcSQL = ""
		Text To lcSQL Textmerge  Noshow
			exec up_stocks_procuraRef '<<Alltrim(ucrsTempFile.ref)>>',<<mysite_nr>>
		Endtext
		If uf_gerais_actGrelha("","uc_St2",lcSQL)
			If Reccount("uc_St2") > 0
				Select ucrsTempFile
				Replace ucrsTempFile.ltVALIDA 	With .t.
				Replace ucrsTempFile.ref 		With Alltrim(uc_St2.ref)
			Else
				Select ucrsTempFile
				Replace ucrsTempFile.ltVALIDA 	With .f.			
			Endif
		Endif
		Select ucrsTempFile
	ENDSCAN

	
	Set Point To "."
	**Tudo Ok Vou lan�ar as Referencias
	* a vari�vel guarda o total de registos do cursor tempcursor
	
	Select ucrsTempFile
	mntotal=reccount("ucrsTempFile")
	* inicializa a r�gua apresentando um t�tulo e o n� total de registos
	regua(0,mntotal,"A IMPORTAR LINHAS DO FICHEIRO...",.f.)
	
	Select ucrsTempFile
	Go top
	Scan
		** actualizar regua **
		regua[1,recno("ucrsTempFile"),"A PROCESSAR PRODUTO: "+ALLTRIM(ucrsTempFile.ref),.f.]
	
		lcRef = Alltrim(ucrsTempFile.ref)
		If !Empty(lcRef)
			uf_inventario_lancaArtigo(lcRef, ucrsTempFile.stock,.t., ucrsTempFile.armazem, ucrsTempFile.Lote)
		Endif
	Endscan
		
	* fecha a r�gua
	regua(2)
ENDFUNC           




** Formato obrigatorio: <ref>(13)<lote>(10)<stock>(4)
** Importa Ficheiro txt
FUNCTION uf_INVENTARIO_importaFicheiroTxt2
	PUBLIC myInventarioAlteracao, myInventarioIntroducao
	
	
	IF myInventarioAlteracao == .f. AND myInventarioIntroducao == .f.
		uf_perguntalt_chama("DEVE COLOCAR O INVENTARIO EM MODO DE ALTERA��O OU INTRODU��O PARA USAR ESTA FUNCIONALIDADE","OK","", 64)
		RETURN .f.
	ENDIF
	
	&& valida separador
	IF Inventario.emcontagem == .f.
		uf_inventario_contagem()
	ENDIF 

	IF USED("ucrsTempFile")
		fecha("ucrsTempFile")
	ENDIF
	IF USED("ucrsTempFileAux")
		fecha("ucrsTempFileAux")
	ENDIF

	LOCAL uv_file
	STORE '' TO uv_file

	uv_file = uf_gerais_getFile(ALLTRIM(uf_gerais_getUmValor("B_terminal", "pathReport", "terminal = '" + ALLTRIM(myTerm) + "'")), 'txt')

	IF EMPTY(uv_file)
		RETURN .F.
	ENDIF
	
	CREATE cursor ucrsTempFile(ref c(32), stock n(9,0), armazem n(3), lote c(60), ltValida l)
	CREATE cursor ucrsTempFileAux(txtinformacao c(254))
	try
		APPEND FROM (uv_file) TYPE DELIMITED WITH TAB
	catch
		**
	endtry
	
	select ucrsTempFileAux
	GO Top
	SCAN FOR !EMPTY(ALLTRIM(ucrsTempFileAux.txtinformacao)) 
	
		IF TYPE(RIGHT(ALLTRIM(ucrsTempFileAux.txtinformacao),4)) == "N"
			Select ucrsTempFile 
			APPEND BLANK
			Replace ucrsTempFile.ref WITH LEFT(ALLTRIM(ucrsTempFileAux.txtinformacao),13)
			Replace ucrsTempFile.stock WITH VAL(RIGHT(ALLTRIM(ucrsTempFileAux.txtinformacao),4))
			Replace ucrsTempFile.lote WITH RIGHT(LEFT(ALLTRIM(ucrsTempFileAux.txtinformacao),23),10)
			Replace ucrsTempFile.armazem WITH ucrsStic.armazem
		ENDIF
	ENDSCAN
	
	
	
	Select ucrsTempFile 
	Go Top
	SCAN FOR !EMPTY(ucrsTempFile.ref)
			
		** Verifica se a ref existe, caso nao exista marca linha 
		lcSQL = ""
		Text To lcSQL Textmerge  Noshow
			exec up_stocks_procuraRef '<<Alltrim(ucrsTempFile.ref)>>',<<mysite_nr>>
		Endtext
		If uf_gerais_actGrelha("","uc_St2",lcSQL)
			If Reccount("uc_St2") > 0
				Select ucrsTempFile
				Replace ucrsTempFile.ltVALIDA 	With .t.
				Replace ucrsTempFile.ref 		With Alltrim(uc_St2.ref)
			Else
				Select ucrsTempFile
				Replace ucrsTempFile.ltVALIDA 	With .f.			
			Endif
		Endif
		Select ucrsTempFile
	ENDSCAN

	
	Set Point To "."
	**Tudo Ok Vou lan�ar as Referencias
	* a vari�vel guarda o total de registos do cursor tempcursor
	
	Select ucrsTempFile
	mntotal=reccount("ucrsTempFile")
	* inicializa a r�gua apresentando um t�tulo e o n� total de registos
	regua(0,mntotal,"A IMPORTAR LINHAS DO FICHEIRO...",.f.)
	
	Select ucrsTempFile
	Go top
	Scan
		** actualizar regua **
		regua[1,recno("ucrsTempFile"),"A PROCESSAR PRODUTO: "+ALLTRIM(ucrsTempFile.ref),.f.]
	
		lcRef = Alltrim(ucrsTempFile.ref)
		If !Empty(lcRef)
			uf_inventario_lancaArtigo(lcRef, ucrsTempFile.stock,.t., ucrsTempFile.armazem, ucrsTempFile.Lote)
		Endif
	Endscan
		
	* fecha a r�gua
	regua(2)
ENDFUNC           


**
FUNCTION uf_inventario_adicionaStockNconf
	PUBLIC myPesqInventarioNConferidos 
	
	LOCAL lcStamp
		IF myInventarioAlteracao == .f. AND myInventarioIntroducao == .f.
		uf_perguntalt_chama("DEVE COLOCAR O INVENTARIO EM MODO DE ALTERA��O OU INTRODU��O PARA USAR ESTA FUNCIONALIDADE","OK","", 64)
		RETURN .f.
	ENDIF
	
	
	&& valida separador
	IF Inventario.emcontagem == .f.
		uf_inventario_contagem()
	ENDIF 
	

	If !uf_perguntalt_chama("ATEN��O: ESTA OP��O S� DEVE SER EXECUTADA NO FIM DO INVENT�RIO, DEPOIS DA CONFER�NCIA MANUAL E ROB�TICA. PRETENDE CONTINUAR?","Sim","N�o")
		RETURN .f.
	Endif
	
	If !uf_perguntalt_chama("VAI ADICIONAR AO INVENT�RIO TODOS OS PRODUTOS QUE N�O FORAM CONFERIDOS. ESTES PRODUTOS SER�O INVENTARIADOS A ZERO."+Chr(10)+Chr(10)+"PRETENDE CONTINUAR?","Sim","N�o")
		RETURN .f.
	Endif
	
	Select ucrsStil
	Count To lcNrRows
	If lcNrRows>0
		uf_perguntalt_chama("ESTA OP��O DEVE SER EXECUTADA ISOLADAMENTE.","OK","",64)
		RETURN .f.
	Endif
	
	**chama painel de selec��o de Inventarios
	myPesqInventarioNConferidos = .t.
	uf_pesqinventario_chama()
ENDFUNC


**
FUNCTION uf_inventario_adicionaStockNconfAfter
	myPesqInventarioNConferidos = .f.
	LOCAL lcPrimeiro
	lcLista = ""

	
	Select ucrsPesquisarInventarios
	GO TOP
	SCAN
		IF ucrsPesquisarInventarios.sel == .t.
			lcLista = lcLista + ALLTRIM(ucrsPesquisarInventarios.sticstamp) + [,]
		ENDIF
	ENDSCAN

	Select ucrsStic
	If !Empty(ucrsStic.sticstamp)
	
		If !Empty(Alltrim(lcLista))
			TEXT TO lcSql Noshow textmerge
				exec up_stocks_InventarioInserirNaoConferido '<<left(ALLTRIM(lcLista),len(ALLTRIM(lcLista))-1)>>',<<mysite_nr>>
			ENDTEXT
		ELSE
			TEXT To lcSql Noshow textmerge
				exec up_stocks_InventarioInserirNaoConferido '<<Alltrim(ucrsStic.sticstamp)>>',<<mysite_nr>>
			ENDTEXT 
		ENDIF

		If !uf_gerais_actGrelha("", "uCrsNaoConf",lcSql)
			uf_perguntalt_chama("OCORREU UM PROBLEMA A PESQUISAR OS PRODUTOS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			RETURN .f.
		ELSE
			
			*Verificar se � uma importa��o parcial
			IF USED("uCrsImpParcial")
				IF RECCOUNT("uCrsImpParcial") > 0
					SELECT uCrsImpParcial
					SELECT uCrsNaoConf
					DELETE from uCrsNaoConf where ref not in (select ref from uCrsImpParcial)
				ENDIF
			ENDIF
						
			* a vari�vel guarda o total de registos do cursor tempcursor
			Select uCrsNaoConf
			mntotal=RECCOUNT("uCrsNaoConf")
			
			IF mntotal == 0
				uf_perguntalt_chama("N�O EXISTEM PRODUTOS A SER ADICIONADOS...","OK","",64)
				RETURN .f.
			ENDIF
			
			* inicializa a r�gua apresentando um t�tulo e o n� total de registos
			regua(0,mntotal,"A PROCESSAR PRODUTOS N�O INVENTARIADOS...",.f.)
			
			Select uCrsNaoConf
			Go top
			Scan
				** actualizar regua **
				regua[1,recno("uCrsNaoConf"),"A PROCESSAR PRODUTO: "+ALLTRIM(uCrsNaoConf.design),.f.]
				
				lcStamp = uf_gerais_stamp()
				Select ucrsStil
				Append Blank
				replace ucrsStil.stilstamp	With lcStamp
				replace ucrsStil.ref		With Alltrim(uCrsNaoConf.ref)
				replace ucrsStil.design		With Alltrim(uCrsNaoConf.design)
				replace ucrsStil.stock		With 0
				replace ucrsStil.ltvalida	With .t.
				replace ucrsStil.zona		With 'P'
				replace ucrsStil.armazem 	WITH ucrsStic.armazem
				replace ucrsStil.lote 		WITH uCrsNaoConf.lote
				
				Select uCrsNaoConf
			Endscan
			
			* fecha a r�gua
			regua(2)
			
			Fecha("uCrsNaoConf")
		Endif
	ENDIF
	INVENTARIO.pageframe1.page1.gridPesq.REFRESH
	
	fecha("uCrsImpParcial")
ENDFUNC


**
** Altera Data, lan�a movimentos Correctivos de Stock
Function uf_inventario_AlterarDataInventarioLt	
	Public myInvDesvios 
	***********************		
	Local lcDataFimInventario, lcStamp
	
	
	Inventario.refresh
	
	If !(myInventarioAlteracao Or myInventarioIntroducao)
		uf_perguntalt_chama("O INVENT�RIO DEVE ESTAR EM MODO DE ALTERA��O. ASSIM N�O PODE CONTINUAR.","OK","",64)
		RETURN .f.
	Endif
	
	If !uf_perguntalt_chama("ATEN��O: ESTA OP��O S� DEVE SER EXECUTADA NO FIM DO INVENT�RIO, DEPOIS DA CONFER�NCIA MANUAL E ROB�TICA. PRETENDE CONTINUAR?","Sim","N�o")
		RETURN .f.
	Endif
	
	If !uf_perguntalt_chama("VAI ADICIONAR AO INVENT�RIO OS MOVIMENTOS DESVIANTES EM RELA��O � NOVA DATA INDICADA."+Chr(10)+Chr(10)+"PRETENDE CONTINUAR?","Sim","N�o")
		RETURN .f.
	Endif
	
	&& valida separador
	IF Inventario.emcontagem == .f.
		uf_inventario_contagem()
	ENDIF 
	
	Store .t. To myInvDesvios
			
	Select ucrsStil
	Count To lcNrRows
	If lcNrRows>0
		uf_perguntalt_chama("ESTA OP��O DEVE SER EXECUTADA ISOLADAMENTE.","OK","",64)
		RETURN .f.
	Endif
	
	Select ucrsStiC
	If Empty(ucrsStiC.sticstamp)
		uf_perguntalt_chama("N�o foi possivel identificar o invent�rio. Tente novamente.","OK","",64)
		RETURN .f.
	ENDIF 
			
	** Pede Data
	lcDataFimInventario  = INPUTBOX("Introduza a Data da Contagem:", '', uf_gerais_getDate(Date()))
	
	lcSQL = ""
	Text To lcSql Noshow textmerge
		Select	
			sl.armazem
			,sl.ref
			,sl.lote
			,(select design from st where st.ref = sl.ref and site_nr = <<mysite_nr>>) as design
			,Sum((Case When cm < 50 Then qtt Else -qtt end) *-1)  as qtt
		From	
			Sl 
			inner join stil on Sl.ref = stil.ref and sl.armazem = stil.armazem and sl.lote = stil.lote
		Where	
			(	(DATALC = '<<uf_gerais_getDate(ucrsStic.data,"SQL")>>' AND sl.ousrhora > '<<IIF(EMPTY(ucrsStic.hora),'00:00',ALLTRIM(ucrsStic.hora))>>')
				OR 
				DATALC > '<<uf_gerais_getDate(ucrsStic.data,"SQL")>>'	
			)
			AND DATALC <= '<<uf_gerais_getDate(lcDataFimInventario,"SQL")>>'
			and stil.sticstamp = '<<Alltrim(ucrsStic.sticstamp)>>'
			
		Group By 
			sl.armazem, sl.REF, sl.LOTE	
	ENDTEXT

*!*	_CLIPTEXT  =lcSQL
*!*	MESSAGEBOX(lcSQL)

	If !uf_gerais_actGrelha("", "uCrsDesvInvent",lcSql)
		uf_perguntalt_chama("OCORREU UM PROBLEMA A PESQUISAR OS PRODUTOS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	Else
		* a vari�vel guarda o total de registos do cursor tempcursor
		Select uCrsDesvInvent
		mntotal=reccount()
		If mntotal = 0
			uf_perguntalt_chama("N�O FORAM ENCONTRADOS RESULTADOS A ADICIONAR AO INVENT�RIO.","OK","",16)
			return
		Endif
		
		* inicializa a r�gua apresentando um t�tulo e o n� total de registos
		regua(0,mntotal,"A PROCESSAR PRODUTOS N�O INVENTARIADOS...",.f.)
	
		Select uCrsDesvInvent
		Go TOP 
		SCAN 
		
			** actualizar regua **
			regua[1,recno("uCrsDesvInvent"),"A PROCESSAR PRODUTO: "+ALLTRIM(uCrsDesvInvent.design),.f.]
			
			lcStamp = uf_gerais_stamp()
			
			Select ucrsStil
			Append Blank
			replace ucrsStil.stilstamp With lcStamp
			replace ucrsStil.ref With Alltrim(uCrsDesvInvent.ref)
			replace ucrsStil.design	With Alltrim(uCrsDesvInvent.design)
			replace ucrsStil.stock With uCrsDesvInvent.qtt
			replace ucrsStil.ltValida With .t.
			replace ucrsStil.zona With 'P'
			replace ucrsStil.armazem WITH uCrsDesvInvent.armazem
			replace ucrsStil.lote With uCrsDesvInvent.lote
			
			Select uCrsDesvInvent
		Endscan
		
		* fecha a r�gua
		regua(2)
		
		Fecha("uCrsDesvInvent")
	ENDIF

	Select ucrsStil
	GO TOP
	Inventario.pageFrame1.page1.refresh

	
ENDFUNC


**
FUNCTION uf_inventario_DesviosAntesLancar
	Select ucrsStic
	lcParametros="&descricao=" + ALLTRIM(ucrsStic.descricao) + "&site=" + ALLTRIM(mysite)
	
	uf_gerais_chamaReport('relatorio_conferencia_diferencasInventarioAntesLancamento',lcParametros,'',.f.)
ENDFUNC


** analise que mostra informa��o antes e ap�s invent�rio - pressuposto � analisar apenas um invvent�rio
FUNCTION uf_inventario_analise
	LOCAL lcDataIni, lcHoraIni, lcDataFim, lcHoraFim, lcSticStamp
	LOCAL lcHora, lcMinuto
	
	SELECT uCrsStic
	GO TOP  
	
	**lcHoraIni = '00:00:01'
	** tirar 1 minuto a hora inicial
	lcHora = left(ALLTRIM(uCrsStic.hora),2)
	lcMinuto = right(ALLTRIM(uCrsStic.hora),2)
	
	IF (lcMinuto == "00" AND lcHora=="00")
		lcDataIni = uf_gerais_getdate(uCrsStic.data - 1, "SQL")
		lcHoraIni = '23:59:59'
	ELSE
		lcDataIni = uf_gerais_getdate(uCrsStic.data, "SQL")
		
		if (lcMinuto == "00")
			if !(lcHora == "00")
				lcHora2 = val(lcHora)
				lcHoraIni = replicate('0', 2-len(alltrim(str(lcHora2)))) + alltrim(str((lcHora2-1))) + ':59:00'
			else
				lcHoraIni = lcHora + ':00:00'
			endif
		ELSE
			lcMinuto2 = val(lcMinuto)
			lcHoraIni = lcHora + ':' + replicate('0', 2-len(alltrim(str(lcMinuto2)))) + alltrim(str(lcMinuto2-1)) + ':00'
		endif
	ENDIF
	
	lcDataFim = uf_gerais_getdate(uCrsStic.data,"SQL")
	**lcHoraFim = '23:59:59'
	lcHoraFim = uCrsStic.hora + ':05'
		
	lcSticStamp = uCrsStic.sticstamp
	
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		&dataIni=<<lcDataIni>>&horaIni=<<lcHoraIni>>&dataFim=<<lcDataFim>>&horaFim=<<lcHoraFim>>&inventario1=<<lcSticStamp>>&pcl=<<'true'>>&pcp=<<'true'>>&pct=<<'true'>>&site=<<mySite>>
	ENDTEXT
	
	SELECT uCrsStic
	GO TOP 

	IF RECCOUNT("uCrsStic") > 0
		IF !EMPTY(uCrsStic.lanca)
			uf_gerais_chamaReport('relatorio_analiseDesvioInventario', lcSQL)
		ELSE
			uf_perguntalt_chama("Apenas pode efetuar esta consulta depois de lan�ar o Invent�rio seleccionado. Por favor verifique. Obrigado.", "OK", "", 64)
		ENDIF
	ELSE
		uf_perguntalt_chama("Deve seleccionar um invent�rio para efetuar a consulta. Por favor verifique. Obrigado.", "OK", "", 64)
	ENDIF
	
ENDFUNC


**
FUNCTION uf_inventario_criaCursorProcura

	SELECT stilstamp FROM ucrsStil WHERE (LIKE ('*'+ALLTRIM(UPPER(INVENTARIO.pageframe1.page1.textPesq.value))+'*',ALLTRIM(UPPER(ucrsStil.ref))) OR ;
			LIKE ('*'+ALLTRIM(UPPER(INVENTARIO.pageframe1.page1.textPesq.value))+'*',ALLTRIM(UPPER(ucrsStil.design)))) AND ;
			!EMPTY(ALLTRIM(UPPER(INVENTARIO.pageframe1.page1.textPesq.value))) INTO CURSOR ucrsTempPesq
	SELECT ucrsTempPesq

	SELECT ucrsStil		
	GO Top
	SCAN
		IF (LIKE ('*'+ALLTRIM(UPPER(INVENTARIO.pageframe1.page1.textPesq.value))+'*',ALLTRIM(UPPER(ucrsStil.ref))) OR ;
			LIKE ('*'+ALLTRIM(UPPER(INVENTARIO.pageframe1.page1.textPesq.value))+'*',ALLTRIM(UPPER(ucrsStil.design)))) AND ;
			!EMPTY(ALLTRIM(UPPER(INVENTARIO.pageframe1.page1.textPesq.value)))
			
			replace ucrsStil.PESQUISA WITH .t.
		ELSE
			replace ucrsStil.PESQUISA WITH .f.
		ENDIF
	ENDSCAN
	SELECT ucrsStil

	uf_inventario_procuraLinhaSeguinte()
ENDFUNC


**
FUNCTION uf_inventario_procuraLinhaSeguinte
	IF !USED("ucrsTempPesq")
		RETURN .f.
	ENDIF
		
	lcStilstamp = ucrsTempPesq.stilstamp
	select ucrsTempPesq
	try
		goto recno() +1
	catch
	 	go top
	endtry
		
	SELECT ucrsTempPesq
	SELECT ucrsStil
	SCAN
		IF ucrsStil.stilstamp == ucrsTempPesq.stilstamp

			EXIT
		Endif
	ENDSCAN
	INVENTARIO.pageframe1.page1.gridPesq.refresh
ENDFUNC


**
FUNCTION uf_inventario_KeyPess
	LPARAMETERS nKeyCode, nShiftAltCtrl

	Do Case
		Case nkeyCode == 1 && home
			INVENTARIO.ref.setfocus()
			INVENTARIO.ref.value=''
			Keyboard "{tab}"
		Case nKeyCode == 27 && esc
			INVENTARIO.ref.value=''
			INVENTARIO.ref.setfocus()
		CASE (nKeyCode = 39) && Plica
			INVENTARIO.ref.value=''
			INVENTARIO.ref.setfocus()
		Otherwise
			*****
	Endcase
ENDFUNC


*************************************************************
DEFINE CLASS myTimerInv as Timer
	interval = 3000
	
	Function Timer
		
		Text To lcSql Noshow textmerge
			exec up_robot_respostaK '<<astr(myTermNo)>>', <<Iif(myUsaRobotApostore, 1, 0)>>
		ENDTEXT
		If !uf_gerais_actGrelha("","uCrsRoboInv",lcSql)
			uf_perguntalt_chama("OCORREU UM ERRO A VALIDAR O INVENT�RIO DO ROBOT. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			uf_inventario_setTimerInv()
			RETURN .f.
		Else
			If Reccount("uCrsRoboInv")==0
				INVENTARIO.pageframe1.page1.tsc2.value = INVENTARIO.pageframe1.page1.tsc2.value + 3
			Else
				Local lcValInv, lcValFim, lcValCodInter
				Store .f. To lcValInv, lcValFim, lcValCodInter

				Select uCrsRoboInv
				Go Top
				Scan For Empty(Alltrim(uCrsRoboInv.order_state))
					
					** validar codigo de internacionalizacao
					** Condi��o adicionada para controlar erro de robo em que guarda mais do que um c�digo de internaciona��o e entra em loop no invent�rio (Apenas tecnilab)
					If Empty(myCodInter)
						myCodInter = uCrsRoboInv.codInter
					Else
						If !(myCodInter == uCrsRoboInv.codInter)
							lcValCodInter = .t.
						Endif
					Endif
					 
					** marcar linha como verificada **
					uf_gerais_actGrelha("","",[update b_movtec_robot set order_state='V' where id=]+astr(uCrsRoboInv.id))
					
					** Verificar se Chegamos ao Fim do Invent�rio **
					If Alltrim(uCrsRoboInv.num_foll) == "00"
						** fim da encomenda, vamos adicionar produtos ao painel
						uf_inventario_setTimerInv()
						lcValInv = .f.
						lcValFim = .t.
					Else
						lcValInv = .t.
						** Incrementar quandidade de referencias inventariadas **
						INVENTARIO.pageframe1.page1.tsc6.value = INVENTARIO.pageframe1.page1.tsc6.value + 1
						*******************************************
					Endif
					************************************************
				Endscan
				
				** verifrica��o de seguran�a se faltam menos de 10 produtos **
				Select uCrsRoboInv
				Go Bottom
				If !(Alltrim(uCrsRoboInv.num_foll) == "10")
					** fim da encomenda, vamos adicionar produtos ao painel
					uf_inventario_setTimerInv()
					lcValInv = .f.
					lcValFim = .t.
				Endif
				*****************************************************
				
				If lcValInv
					*myExisteConfRobot = .t.
					INVENTARIO.pageframe1.page1.tsc2.value = 0
					INVENTARIO.pageframe1.page1.lastref.value = myLastRefRobo
					
					SELECT uCrsRoboInv
					GO BOTTOM
					IF myUsaRobotApostore
						myLastRefRobo = Alltrim(uCrsRoboInv.product_id)
						uf_robot_tecnilabMsgK(Alltrim(uCrsRoboInv.product_id))
					ELSE
						IF !Empty(Alltrim(uCrsRoboInv.ref))
							IF lcValCodInter == .t.
								myLastRefRobo = Alltrim(uCrsRoboInv.oref)
								uf_robot_tecnilabMsgK(Alltrim(uCrsRoboInv.oref))
							ELSE
								myLastRefRobo = Alltrim(uCrsRoboInv.ref)
								uf_robot_tecnilabMsgK(Alltrim(uCrsRoboInv.ref))
							ENDIF
						ENDIF
					ENDIF
				ELSE
					If !lcValFim
						INVENTARIO.pageframe1.page1.tsc2.value = INVENTARIO.pageframe1.page1.tsc2.value + 3
					Else
						myLastRefRobo = '0000000'
					Endif
				Endif
			Endif
		Endif
	Endfunc
ENDDEFINE



FUNCTION uf_inventario_eliminar
	inventario.descricao.setfocus
	SELECT uCrsStic
	IF RECCOUNT("uCrsStic") > 0
		IF !myInventarioAlteracao AND !myInventarioIntroducao
			SELECT uCrsStic
			IF !uCrsStic.lanca
				IF uf_perguntalt_chama("Tem a certeza que prentende eliminar o invent�rio?","Sim","N�o")
					TEXT TO lcSQL TEXTMERGE NOSHOW
						DELETE FROM stic WHERE sticstamp = '<<ALLTRIM(uCrsStic.sticstamp)>>'
						DELETE FROM stil WHERE sticstamp = '<<ALLTRIM(uCrsStic.sticstamp)>>'
					ENDTEXT 
					IF uf_gerais_actGrelha("","",lcSQL)
						uf_perguntalt_chama("Invent�rio eliminado com sucesso.","OK","",64)
						uf_inventario_ultimo()
					ELSE
						uf_perguntalt_chama("Ocorreu um erro a eliminar o invent�rio.","OK","",16)
						RETURN .f.
					ENDIF
				ENDIF
			ELSE
				uf_perguntalt_chama("N�o pode eliminicar um invent�rio j� lan�ado." + CHR(13) + CHR(13) + "Verifique se pretende anular o invent�rio.","OK","",64)
				RETURN .f.
			ENDIF 
		ENDIF 
	ENDIF
ENDFUNC


FUNCTION uf_inventario_anular
	SELECT uCrsStic
	IF RECCOUNT("uCrsStic") > 0
		IF !myInventarioAlteracao AND !myInventarioIntroducao
			IF uf_gerais_getParameter_site('ADM0000000006','BOOL', mySite_nr) AND !EMPTY(ALLTRIM(uf_gerais_getParameter_site('ADM0000000007', 'text', mySite_nr))) AND DTOC(ucrsStic.data,1)>=uf_gerais_getParameter_site('ADM0000000007', 'text', mySite_nr) 
				SELECT uCrsStic
				IF uCrsStic.lanca
					IF uf_perguntalt_chama("Tem a certeza que prentende anular o invent�rio?","Sim","N�o")
						TEXT TO lcSQL TEXTMERGE NOSHOW
							UPDATE stic SET lanca = 0 WHERE sticstamp = '<<ALLTRIM(uCrsStic.sticstamp)>>'
							DELETE FROM sl WHERE sticstamp = '<<ALLTRIM(uCrsStic.sticstamp)>>'
						ENDTEXT
						IF uf_gerais_actGrelha("","",lcSQL)
					
							** Triggers Lotes
*!*								IF uf_gerais_getParameter("ADM0000000211","BOOL") 
*!*									Select uCrsInvL
*!*									GO Top
*!*									SCAN
*!*										* remover 
*!*										uf_gerais_triggerLote(ALLTRIM(uCrsInvL.lote), ALLTRIM(uCrsInvL.ref), 0, "DELETE", 0, 0, '19000101', uCrsInvL.armazem)
*!*									ENDSCAN		
*!*								ENDIF
							**
							TEXT TO lcSQL TEXTMERGE NOSHOW
								select top 1 data from stic where lanca=1 order by data desc
							ENDTEXT
							uf_gerais_actGrelha("","crsultinv",lcSQL)
							IF RECCOUNT("crsultinv")=0 then
								TEXT TO lcSQL TEXTMERGE NOSHOW
									UPDATE B_Parameters_site SET textvalue='' WHERE stamp='ADM0000000007' AND site='<<mySite>>' 
								ENDTEXT 
								uf_gerais_actGrelha("","",lcSQL)
							ELSE
								TEXT TO lcSQL TEXTMERGE NOSHOW
									UPDATE B_Parameters_site SET textvalue='<<uf_gerais_getDate(crsultinv.data,"SQL")>>' WHERE stamp='ADM0000000007' AND site='<<mySite>>' 
								ENDTEXT 
								uf_gerais_actGrelha("","",lcSQL)							
							ENDIF 
							
							
							uf_perguntalt_chama("Invent�rio anulado com sucesso.","OK","",64)
							uf_inventario_chama(ALLTRIM(uCrsStic.sticstamp))
						ELSE
							uf_perguntalt_chama("Ocorreu um erro a eliminar o invent�rio.","OK","",16)
							RETURN .f.
						ENDIF
					ENDIF
				ENDIF 
			ELSE
				uf_perguntalt_chama("N�o pode anular um invent�rio com data inferior ao �ltimo fechado.","OK","",64)
				RETURN .f.
			ENDIF 
		ENDIF 
	ENDIF
ENDFUNC 


FUNCTION uf_inventario_apagaLinhaBD
	IF myInventarioIntroducao OR myInventarioAlteracao
		IF !USED("uCrsInvDel")
			CREATE CURSOR uCrsInvDel (stilstamp c(25))
		ENDIF
		
		SELECT uCrsInvL
		SELECT uCrsInvDel
		APPEND BLANK
		replace uCrsInvDel.stilstamp WITH uCrsInvL.stilstamp 
		
		SELECT uCrsInvL
		DELETE
		
		TRY 
			SKIP -1
		CATCH
			SKIP 1
		FINALLY
			**
		ENDTRY
		
		inventario.pageframe1.page2.grdInv.refresh
	ENDIF
ENDFUNC

FUNCTION uf_inventario_lancar

	IF myInventarioAlteracao == .t. Or myInventarioIntroducao == .t.
		uf_perguntalt_chama("N�o pode lan�ar um invent�rio antes de gravar.","OK","",32)
		RETURN .f.
	ENDIF 


	SELECT ucrsStic
	IF !EMPTY(ALLTRIM(uCrsStic.sticstamp))
		** Colocar container visivel
		inventario.lancar.visible = .t.
		
		** Colocar valores por defeito
		uf_gerais_actGrelha("","uCrsTxtEntrada","select cmdesc from cm2(nolock) where cm=47")
		SELECT uCrsTxtEntrada
		inventario.lancar.txtEntrada.value = ALLTRIM(uCrsTxtEntrada.cmdesc)
		uf_gerais_actGrelha("","uCrsTxtSaida","select cmdesc from cm2(nolock) where cm=92")
		SELECT uCrsTxtSaida
		inventario.lancar.txtSaida.value = ALLTRIM(uCrsTxtSaida.cmdesc)
		
		fecha("uCrsTxtEntrada")
		fecha("uCrsTxtSaida")
		
		** Colocar sombra
		uf_inventario_sombra(.t.)
	ENDIF
ENDFUNC


**
FUNCTION uf_inventario_sombra
	LPARAMETERS tcBool
	
	IF tcBool
		** remover foco da grelha
		inventario.ref.setfocus
		
		** Colocar sombra
		inventario.sombra.width		= inventario.width
		inventario.sombra.height	= inventario.height
		inventario.sombra.left 		= 0
		inventario.sombra.top 		= 0
		
		inventario.sombra.visible 	= .t.
	ELSE
		** remover sombra
		inventario.sombra.visible 	= .f.
	ENDIF
ENDFUNC


FUNCTION uf_receituario_lancarFechar
	** Colocar container invisivel
	inventario.lancar.visible = .f.
	uf_inventario_sombra(.f.)
ENDFUNC


FUNCTION uf_inventario_lancarMovimentos
	IF uf_perguntalt_chama("Vai lan�ar os movimentos de acerto de stock." + chr(13)+CHR(13) + "Pretende continuar?","Sim","N�o")
		DO CASE 
			CASE inventario.lancar.optVal.value == 1
				uf_inventario_lancarSL('PCP')
			CASE inventario.lancar.optVal.value == 2
				uf_inventario_lancarSL('PCL')
		ENDCASE
	ENDIF
ENDFUNC


**
FUNCTION uf_inventario_lancarSL
	LPARAMETERS lcTipo
	
	LOCAL lcStamp, lcStockSA, lcCmDesc, lcQtt, lcCm, lcValida
	STORE '' TO lcCmDesc
	STORE 0 TO lcStockSA, lcQtt, lcCm
	STORE .f. TO lcValida

	IF uf_gerais_actGrelha("","","BEGIN TRANSACTION")
		IF uf_inventario_lancarUpdStic()
			&&actualizar linhas invent�rio
			regua(0,RECCOUNT("uCrsInvL"),"A lan�ar os movimentos...")
			SELECT uCrsInvL
			GO TOP
			SCAN
				regua(1,RECNO("uCrsInvL"),"A processar refer�ncia: " + ALLTRIM(uCrsInvL.ref))
				lcSQL = ''
				
				TEXT TO lcSQL TEXTMERGE NOSHOW 
					exec up_stocks_InventarioValorizacao '<<ALLTRIM(uCrsInvL.ref)>>' ,'<<uf_gerais_getDate(uCrsStic.data,"SQL")>>' ,<<uCrsInvL.armazem>>,'<<lcTipo>>','<<ALLTRIM(uCrsInvL.Lote)>>'
				ENDTEXT 
						
				IF uf_gerais_actGrelha("","uCrsPrecoTemp",lcSQL)
					SELECT uCrsInvL
					SELECT uCrsPrecoTemp
					DO CASE
						CASE lcTipo == 'PCP'
							lcSQL = ''
							TEXT TO lcSQL TEXTMERGE NOSHOW
								UPDATE
									stil
								SET 
									pcpond = <<uCrsPrecoTemp.preco>>
									,epcpond = <<uCrsPrecoTemp.epreco>>
								where
									stilstamp = '<<ALLTRIM(uCrsInvL.stilstamp)>>'
									and Lote = '<<ALLTRIM(uCrsInvL.Lote)>>'
							ENDTEXT 
							IF uf_gerais_actGrelha("","",lcSQL)
								lcValida = .t.
							ELSE
								lcValida = .f.
							ENDIF
						CASE lcTipo == 'PCL'
							lcValida = .t.
					ENDCASE 
					
					IF lcValida
						
						&&lan�ar movimentos na SL
						lcStamp = uf_gerais_stamp()
							
							* Obter dados da tabela SA
						SELECT uCrsInvL
						lcSQL = ''						
						TEXT TO lcSQL TEXTMERGE NOSHOW
							exec up_stocks_InventarioStockArmazem '<<uCrsInvL.ref>>', <<uCrsInvL.armazem>>, '<<ALLTRIM(uCrsInvL.Lote)>>','<<uf_gerais_getDate(uCrsStic.data,"SQL") + " " + ALLTRIM(uCrsStic.hora) + ":00">>' 
						ENDTEXT 
						uf_gerais_actGrelha("","uCrsTempSA",lcSQL)
						SELECT uCrsTempSA
						IF RECCOUNT("uCrsTempSA") > 0
							lcStockSA = uCrsTempSA.stock
						ELSE
							lcStockSA = 0
						ENDIF
						
						SELECT uCrsPrecoTemp
						SELECT uCrsStic
						SELECT uCrsInvL
			
						IF lcStockSA > uCrsInvL.stock
							lcCmDesc = ALLTRIM(inventario.lancar.txtSaida.value)
							lcQtt = lcStockSA - uCrsInvL.stock
							lcCm = 92				
							 IF (uCrsInvL.psico)			 
                                uf_psicobenzo_contador_psico_benzo_entradasSaidas(1,1)
                            ENDIF
                            IF (uCrsInvL.benzo)	
                                uf_psicobenzo_contador_psico_benzo_entradasSaidas(1,2)
                            ENDIF	
						ELSE
							IF lcStockSA == uCrsInvL.stock
								lcCmDesc = 'naoinsere'
								lcQtt = 0
								lcCm = 0
							ELSE
								lcCmDesc = ALLTRIM(inventario.lancar.txtEntrada.value)
								lcQtt = uCrsInvL.stock - lcStockSA
								lcCm = 47
								IF (uCrsInvL.psico)
                                    uf_psicobenzo_contador_psico_benzo_entradasSaidas(0,1)
                                ENDIF
                                IF (uCrsInvL.benzo)	
                                    uf_psicobenzo_contador_psico_benzo_entradasSaidas(0,2)
                                ENDIF
							ENDIF	
						ENDIF
						
						IF !(lcCmDesc == 'naoinsere')
							lcSQL = ''
							TEXT TO lcSQL TEXTMERGE NOSHOW
								INSERT INTO sl
									(
									slstamp
									,ref
									,design
									,datalc
									,cmdesc
									,qtt
									,evu
									,ett
									,nome
									,origem
									,cm
									,armazem
									,pcpond
									,epcpond
									,sticstamp
									,lote
									,ousrinis
									,ousrdata
									,ousrhora
									,usrinis
									,usrdata
									,usrhora
								)
								values
									(
									'<<ALLTRIM(lcStamp)>>'
									,'<<ALLTRIM(uCrsInvL.ref)>>'
									,'<<ALLTRIM(uCrsInvL.design)>>'
									,'<<uf_gerais_getDate(uCrsStic.data,"SQL")>>'
									,'<<lcCmDesc>>'
									,<<lcQtt>>
									,<<uCrsPrecoTemp.epreco>>
									,<<uCrsPrecoTemp.epreco * lcQtt>>
									,'<<ALLTRIM(uCrsStic.descricao)>>'
									,'IF'
									,<<lcCm>>
									,<<uCrsInvL.armazem>>
									,<<IIF(lcTipo == 'PCP',uCrsPrecoTemp.preco,0)>>
									,<<IIF(lcTipo == 'PCP',uCrsPrecoTemp.epreco,0)>>
									,'<<ALLTRIM(uCrsStic.sticstamp)>>'
									,'<<ALLTRIM(uCrsInvL.lote)>>'
									,'<<m_chinis>>'
									,'<<uf_gerais_getDate(uCrsStic.data,"SQL")>>'
									,'<<ALLTRIM(uCrsStic.hora)>>:00'
									,'<<m_chinis>>'
									,'<<uf_gerais_getDate(uCrsStic.data,"SQL")>>'
									,'<<ALLTRIM(uCrsStic.hora)>>:00'
									)
							ENDTEXT 
							
							IF !uf_gerais_actGrelha("","",lcSQL)
								uf_perguntalt_chama("Ocorreu um erro a gravar os movimentos para a refer�ncia: " + ALLTRIM(uCrsInvL.ref) + CHR(13)+CHR(13) + "Por favor verifique.","OK","",16)
								uf_gerais_actGrelha("","","ROLLBACK")
								regua(2)
								RETURN .f.
							ENDIF
						ENDIF
					ELSE
						uf_perguntalt_chama("Ocorreu um erro a actualizar os dados para a refer�ncia: " + ALLTRIM(uCrsInvL.ref) + CHR(13)+CHR(13) + "Por favor verifique.","OK","",16)
						uf_gerais_actGrelha("","","ROLLBACK")
						regua(2)
						RETURN .f.
					ENDIF 
					fecha("uCrsPrecoTemp")
					fecha("uCrstempSA")
				ELSE
					uf_perguntalt_chama("Ocorreu um erro a obter os dados para a refer�ncia: " + ALLTRIM(uCrsInvL.ref) + CHR(13)+CHR(13) + "Por favor verifique.","OK","",16)
					uf_gerais_actGrelha("","","ROLLBACK")
					regua(2)
					RETURN .f.
				ENDIF 
			ENDSCAN
		ELSE
			uf_perguntalt_chama("Ocorreu um erro a marcar o invent�rio como lan�ado.","OK","",16)
			uf_gerais_actGrelha("","","ROLLBACK")
			regua(2)
			RETURN .f.
		ENDIF 
	ELSE
		RETURN .f.
	ENDIF 
	
	IF uf_gerais_actGrelha("","",[COMMIT TRANSACTION])
		regua(2)
		
		TEXT TO lcSQL TEXTMERGE NOSHOW
			UPDATE B_Parameters_site SET textvalue='<<uf_gerais_getDate(uCrsStic.data,"SQL")>>' WHERE stamp='ADM0000000007' AND site='<<mySite>>' 
		ENDTEXT 

		IF !uf_gerais_actGrelha("","",lcSQL)
			uf_perguntalt_chama("Ocorreu um erro a preencher a data de fecho de movimentos nos par�metros. Por favor verifique.","OK","",16)
			RETURN .f.
		ENDIF

		uf_perguntalt_chama("Invent�rio lan�ado com sucesso","OK","",64)
		inventario.lancar.menu1.sair.click
		SELECT uCrsStic
		uf_inventario_chama(ALLTRIM(uCrsStic.sticstamp))
	ELSE
		regua(2)
		uf_perguntalt_chama("Ocorreu um erro a lan�ar o invent�rio.","OK","",16)
		RETURN .f.
	ENDIF
ENDFUNC


**
FUNCTION uf_inventario_lancarUpdStic
	&&marcar invent�rio como lan�ado
	SELECT uCrsStic
	lcSQL = ''
	TEXT TO lcSQL TEXTMERGE NOSHOW 
		UPDATE 
			stic 
		SET 
			lanca = 1 
		WHERE 
			sticstamp = '<<alltrim(uCrsStic.sticstamp)>>'
	ENDTEXT 
	IF uf_gerais_actGrelha("","",lcSQL)
		RETURN .t.
	ELSE
		RETURN .f.
	ENDIF
ENDFUNC

FUNCTION up_inventario_actTotaisInvGeral
	Local lcPos, lcTot, lcTotRef
	Store 0 To lcPos, lcTot, lcTotRef
		
	Select ucrsInvL
	lcPos = Recno("ucrsInvL")
	
	** quantidade total
	Select ucrsInvL
	Calculate Sum(ucrsInvL.stock) To lcTot
	INVENTARIO.pageframe1.page2.qttotal.value = lcTot
	
	** nr de referencias
	Select ucrsInvL
	Count To lcTotRef
	INVENTARIO.pageframe1.page2.reftotal.value = lcTotRef
	
	Select ucrsInvL
	If lcPos>0 And lcPos<=lcTotRef
		TRY
			Go lcPos
		CATCH 
		ENDTRY
	Endif
ENDFUNC


**
FUNCTION uf_inventario_contagem
	inventario.descricao.setfocus
	IF !USED("ucrsStic")
		RETURN .f.
	ENDIF
	IF RECCOUNT("ucrsStic") == 0
		RETURN .f.
	ENDIF 

	IF myInventarioAlteracao == .f. AND myInventarioIntroducao == .f.
		uf_inventario_alterar()
	ENDIF
	
	Inventario.PageFrame1.activePage = 1
	Inventario.emcontagem = .t.
	uf_inventario_controlaObj()
ENDFUNC


FUNCTION uf_inventario_RoboInv_servicos
	LOCAL lcRobotstatus, lcPedStampStRobot
	
	IF !(uf_perguntalt_chama("CONFIRMA A IMPORTA��O DO INVENT�RIO DO ROBOT?" + CHR(13) + "NOTA: ESTA OPERA��O PODE SER MUITO DEMORADA E N�O PODE UTILIZAR O TERMINAL ENQUANTO N�O FINALIZAR.","Sim","N�o"))
		RETURN .f.
	ENDIF
		
	lcPedStampStRobot = uf_gerais_stamp()
	lcRobotstatus =uf_robot_servico_inventory(ALLTRIM(lcPedStampStRobot))
	
	IF VAL(lcRobotstatus) = 200
		LOCAL lcFimInv, LclCounter
		STORE .f. TO lcFimInv
		LOCAL lcCont, lcCont2
		lcCont = myServRobotTOinv*2
		lcCont2 = 1
		regua(0,lcCont,"A IMPORTAR O INVENT�RIO...")
		LclCounter = myServRobotTOinv
		DO WHILE lcFimInv = .f.
			regua(1,lcCont2,"A IMPORTAR O INVENT�RIO DO ROBOT...")
			TEXT TO lcSQL NOSHOW TEXTMERGE 
				select * from robot_response_payload_stock
				where token='<<ALLTRIM(lcPedStampStRobot)>>' and lastrequest=1
			ENDTEXT

			IF !uf_gerais_actGrelha("","ucrsresposta",lcSQL)
				uf_perguntalt_chama("ERRO AO PESQUISAR RESULTADO DO FIM DE INVENT�RIO","OK","",48)
				RETURN .f.
			ENDIF
			
			IF RECCOUNT("ucrsresposta")>0
				lcFimInv = .t.
			ENDIF 
			LclCounter = LclCounter - 0.5
			
			IF LclCounter = 0 
				lcFimInv = .t.
			ENDIF 

			WAIT "" TIMEOUT 1
			lcCont2 = lcCont2 + 1
			fecha("ucrsresposta")
		ENDDO
		
		IF LclCounter = 0 
			uf_perguntalt_chama("O PEDIDO DE INVENT�RIO N�O FOI CONCLUIDO NO TEMPO PARAMETRIZADO. POR FAVOR VERIFIQUE!","OK","",48)
			regua(2)
			RETURN .f.
		ELSE
			lcCont2 = lcCont
			regua(1,lcCont2,"A LAN�AR O INVENT�RIO DO ROBOT...")
			TEXT TO lcSQL NOSHOW TEXTMERGE 
				SELECT distinct st.design ,st.epcpond ,rrps.quantity, rrps.productCode 
				from robot_response_payload_stock rrps (nolock)
				inner join st on st.ref=rrps.productcode and st.site_nr=<<ucrse1.siteno>>
				where token='<<ALLTRIM(lcPedStampStRobot)>>' and lastrequest<>1
			ENDTEXT

			IF !uf_gerais_actGrelha("","ucrsInvRobot",lcSQL)
				uf_perguntalt_chama("ERRO AO OBTER OS RESULTADOS DO INVENT�RIO DO ROBOT","OK","",48)
				regua(2)
				RETURN .f.
			ENDIF
			select ucrsInvRobot
			IF RECCOUNT("ucrsInvRobot")=0
				uf_perguntalt_chama("O PEDIDO DE INVENT�RIO DO ROBOT N�O RETORNOU RESULTADOS","OK","",48)
				regua(2)
			ELSE
				LOCAL lcSticStamp
				SELECT uCrsstic
				lcSticStamp=ALLTRIM(uCrsstic.sticstamp)
				LOCAL lcLordem
				STORE 1000 TO lcLordem
				SELECT ucrsInvRobot
				GO TOP 
				SCAN 
					local lcstilstamp 
					lcstilstamp =uf_gerais_stamp()
					SELECT ucrsStil
					go bottom
					APPEND BLANK 
					replace ucrsStil.pesquisa WITH .f.
					replace ucrsStil.ltvalida WITH .t.
					replace ucrsStil.stilstamp WITH alltrim(lcstilstamp )
					replace ucrsStil.ref WITH ucrsInvRobot.productcode
					replace ucrsStil.design WITH ucrsInvRobot.design
					replace ucrsStil.data WITH DATE()
					replace ucrsStil.stock WITH ucrsInvRobot.quantity
					replace ucrsStil.sticstamp WITH lcSticStamp
					replace ucrsStil.armazem WITH ucrse1.siteno
					replace ucrsStil.ousrinis WITH m_chinis
					replace ucrsStil.ousrdata WITH DATE()
					replace ucrsStil.ousrhora WITH TIME()
					replace ucrsStil.usrinis WITH m_chinis
					replace ucrsStil.usrdata WITH DATE()
					replace ucrsStil.usrhora WITH TIME()
					replace ucrsStil.pcpond WITH ucrsInvRobot.epcpond 
					replace ucrsStil.epcpond WITH ROUND(ucrsInvRobot.epcpond*200.482,2)
					replace ucrsStil.lordem WITH lcLordem
					replace ucrsStil.zona	WITH 'R'
					lcLordem = lcLordem + 100
				
				ENDSCAN 
			ENDIF 
			regua(2)
			up_inventario_actRpInv()
			uf_perguntalt_chama("INVENT�RIO IMPORTADO COM SUCESSO DO ROBOT!","OK","",48)
			INVENTARIO.pageframe1.page1.gridPesq.Refresh
			inventario.pageframe1.page2.grdInv.refresh
			inventario.refresh
			FECHA("ucrsInvRobot")
		ENDIF 
			
	ELSE
		uf_perguntalt_chama("O Logitools n�o obteve resposta do equipamento, por favor volte a tentar. Obrigado","OK","",16)
	ENDIF 
		
	
ENDFUNC 



