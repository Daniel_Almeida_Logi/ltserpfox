**
FUNCTION uf_stockPorArmazem_chama
	LPARAMETERS nRef, nDesign, nOrigem
		
	&& Valida Parametros
	PUBLIC myOrigemRefPStSa, myOrigemDesignPStSa, OrigemChamada, TokenStArmCentralx3, contadorSTAC

	TokenStArmCentralx3=''
	contadorSTAC = 10

	myOrigemRefPStSa = nRef
	myOrigemDesignPStSa = nDesign

	IF TYPE("nOrigem")<>'L'
		OrigemChamada = ALLTRIM(nOrigem)
	ELSE
		OrigemChamada = ''
	ENDIF 
	
	IF uf_gerais_getparameter_site('ADM0000000078', 'BOOL', mySite)
		LOCAL lcAdd
		STORE '&' TO lcAdd
		TokenStArmCentralx3 = uf_gerais_stamp()
		lcWsDir    = ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\X3\ltsX3'

		lcWsPath = 'javaw -jar ltsx3.jar ' + '' + ' "--X3INTSTO==1"' + ' "' + '--IDREF=' + ALLTRIM(nRef)  + '" "' + '--TOKEN= ' + ALLTRIM(TokenStArmCentralx3) + '"'
		lcWsPath = "cmd /c cd /d " + lcWsDir+ " "+  lcAdd + lcAdd + ' ' + lcWsPath 
	
		oWSShell = CREATEOBJECT("WScript.Shell")
		oWSShell.Run(lcWsPath,0,.f.)
	ENDIF 
	 
	IF TYPE("stockPorArmazem") == "U"
	
		IF !uf_gerais_getParameter('ADM0000000310', 'BOOL')
			&& Cria Cursor Vazio
			TEXT TO lcSQL TEXTMERGE NOSHOW 
				exec up_stocks_MovimentosSa '<<ALLTRIM(myOrigemRefPStSa)>>', '<<mysite>>', '<<ch_userno>>', '<<ch_grupo>>', 0
			ENDTEXT
		ELSE 
			#DEFINE  FLAG_ICC_FORCE_CONNECTION 1
		 	DECLARE Long InternetCheckConnection IN Wininet.dll String Url, Long dwFlags, Long Reserved
		 	
			LOCAL lcValidaLigInternet, lcUrl, lcRemoteSQL, lcNoRemoto, lcRemoteDB, lcRemoteSRV
			STORE .t. TO lcvalidaLigInternet
			STORE '' TO lcUrl , lcRemoteSQL, lcRemoteDB, lcRemoteSRV
			STORE 0 TO lcNoRemoto
			
			lcUrl = uf_gerais_getParameter_site('ADM0000000089', 'TEXT', mySite)
			lcRemoteSQL = uf_gerais_getParameter_site('ADM0000000090', 'TEXT', mySite)
			lcRemoteDB = substr(uf_gerais_getParameter_site('ADM0000000090', 'TEXT', mySite),at('].[',uf_gerais_getParameter_site('ADM0000000090', 'TEXT', mySite))+2 ,99)
			lcRemoteSRV = substr(uf_gerais_getParameter_site('ADM0000000090', 'TEXT', mySite), 1,at('].[',uf_gerais_getParameter_site('ADM0000000090', 'TEXT', mySite)))
			IF InternetCheckConnection(lcUrl, FLAG_ICC_FORCE_CONNECTION, 0) <> 0
				*? "Connection is available"
				lcValidaLigInternet = .t.
			ELSE
				lcvalidaLigInternet = .f.
			ENDIF

			IF lcValidaLigInternet == .t.
				LOCAL lcbd, lcRemoteSQL 
				lcbd = substr(	uf_gerais_getParameter_site('ADM0000000090', 'TEXT', mySite),at('.[',uf_gerais_getParameter_site('ADM0000000090', 'TEXT', mySite))+1,30)
				lcRemoteSQL = left(uf_gerais_getParameter_site('ADM0000000090', 'TEXT', mySite),at('.[',uf_gerais_getParameter_site('ADM0000000090', 'TEXT', mySite))-1)
				TEXT TO lcSQL TEXTMERGE NOSHOW 
					DECLARE @RunStoredProcSQL VARCHAR(1000);
					SET @RunStoredProcSQL = 'EXEC <<lcbd>>.[dbo].up_stocks_Sa_central ''<<ALLTRIM(myOrigemRefPStSa)>>''';
					EXEC (@RunStoredProcSQL) AT <<lcRemoteSQL >>;
				ENDTEXT
			ELSE
				&& Cria Cursor Vazio
				TEXT TO lcSQL TEXTMERGE NOSHOW 
					exec up_stocks_MovimentosSa '<<ALLTRIM(myOrigemRefPStSa)>>', '<<mysite>>', '<<ch_userno>>', '<<ch_grupo>>', 0
				ENDTEXT			
			
			ENDIF 
		ENDIF 
		IF !uf_gerais_actgrelha("", "uCrsStSa", lcSql)
			RETURN .F.
		ENDIF
			
		DO FORM stockPorArmazem
	ELSE

		stockPorArmazem.show
	ENDIF
	uf_stockporarmazem_configgrelha()
ENDFUNC


**
FUNCTION uf_stockPorArmazem_carregaMenu
	WITH stockPorArmazem.menu1
		**.adicionaOpcao("pesquisar", "Pesquisar", myPath + "\imagens\icons\lupa_w.png", "uf_stockPorArmazem_carregaDados", "P")
		.adicionaopcao("imprimir", "Imprimir", myPath + "\imagens\icons\imprimir_w.png", "uf_stockPorArmazem_report", "I")
	ENDWITH 
ENDFUNC


**
FUNCTION uf_stockPorArmazem_carregaDados
	LOCAL lcSQL
	lcSQL = ""
	
	IF !uf_gerais_getParameter('ADM0000000310', 'BOOL')
		TEXT TO lcSQL TEXTMERGE NOSHOW 
			exec up_stocks_MovimentosSa '<<ALLTRIM(myOrigemRefPStSa)>>', '<<mysite>>', '<<ch_userno>>', '<<ch_grupo>>',0
		ENDTEXT
		
	ELSE 
		#DEFINE  FLAG_ICC_FORCE_CONNECTION 1
	 	DECLARE Long InternetCheckConnection IN Wininet.dll String Url, Long dwFlags, Long Reserved
	 	
		LOCAL lcValidaLigInternet, lcUrl, lcRemoteSQL, lcNoRemoto, lcRemoteDB, lcRemoteSRV
		STORE .t. TO lcvalidaLigInternet
		STORE '' TO lcUrl , lcRemoteSQL, lcRemoteDB, lcRemoteSRV
		STORE 0 TO lcNoRemoto
		
		lcUrl = uf_gerais_getParameter_site('ADM0000000089', 'TEXT', mySite)
		lcRemoteSQL = uf_gerais_getParameter_site('ADM0000000090', 'TEXT', mySite)
		lcRemoteDB = substr(uf_gerais_getParameter_site('ADM0000000090', 'TEXT', mySite),at('].[',uf_gerais_getParameter_site('ADM0000000090', 'TEXT', mySite))+2 ,99)
		lcRemoteSRV = substr(uf_gerais_getParameter_site('ADM0000000090', 'TEXT', mySite), 1,at('].[',uf_gerais_getParameter_site('ADM0000000090', 'TEXT', mySite)))
		IF InternetCheckConnection(lcUrl, FLAG_ICC_FORCE_CONNECTION, 0) <> 0
			*? "Connection is available"
			lcValidaLigInternet = .t.
		ELSE
			lcvalidaLigInternet = .f.
		ENDIF

		IF lcValidaLigInternet == .t.
		
			LOCAL lcbd, lcRemoteSQL 
			lcbd = substr(	uf_gerais_getParameter_site('ADM0000000090', 'TEXT', mySite),at('.[',uf_gerais_getParameter_site('ADM0000000090', 'TEXT', mySite))+1,30)
			lcRemoteSQL = left(uf_gerais_getParameter_site('ADM0000000090', 'TEXT', mySite),at('.[',uf_gerais_getParameter_site('ADM0000000090', 'TEXT', mySite))-1)
			TEXT TO lcSQL TEXTMERGE NOSHOW 
				DECLARE @RunStoredProcSQL VARCHAR(1000);
				SET @RunStoredProcSQL = 'EXEC <<lcbd>>.[dbo].up_stocks_Sa_central ''<<ALLTRIM(myOrigemRefPStSa)>>''';
				EXEC (@RunStoredProcSQL) AT <<lcRemoteSQL >>;
			ENDTEXT
		ELSE
			TEXT TO lcSQL TEXTMERGE NOSHOW 
				exec up_stocks_MovimentosSa '<<ALLTRIM(myOrigemRefPStSa)>>', '<<mysite>>', '<<ch_userno>>', '<<ch_grupo>>',0
			ENDTEXT
	
		ENDIF 
	ENDIF 
	IF !uf_gerais_actgrelha("stockPorArmazem.grid1", "uCrsStSa", lcSql)
		RETURN .f.
	ENDIF
	
	&& Dados das BD, com liga��o
	TEXT TO lcSQL textmerge noshow
		select * from site_lnk(nolock)
	ENDTEXT 
	IF !uf_gerais_actgrelha("", "uCrsSiteLnk", lcSql)
		uf_perguntalt_chama("Configura��o SiteLink n�o encoentrada.","OK","", 16)
		RETURN .f.
	ENDIF
		
	IF RECCOUNT("uCrsSiteLnk") != 0 
		SELECT  uCrsSiteLnk
		GO TOP 
		SCAN
			lcSQL = ''
			TEXT TO lcSQL textmerge noshow
				exec <<IIF(EMPTY(uCrsSiteLnk.servidor),"",ALLTRIM(uCrsSiteLnk.servidor)+".")>><<ALLTRIM(uCrsSiteLnk.bd)>>.dbo.up_stocks_MovimentosSa '<<Alltrim(myOrigemRefPStSa)>>', '<<mysite>>', '<<ch_userno>>', '<<ch_grupo>>'
			ENDTEXT 
				
			IF !uf_gerais_actgrelha("", "uCrsStSaLnk", lcSQL)
				RETURN .f.
			ENDIF
		
			SELECT uCrsStSaLnk
			GO TOP 
			SCAN FOR uCrsSiteLnk.armazem == uCrsStSaLnk.armazem && apenas o armazem configurado
				
				Select uCrsStSa
				APPEND BLANK
				Replace uCrsStSa.armazem	 WITH uCrsStSaLnk.armazem
				Replace uCrsStSa.empresa	 WITH uCrsStSaLnk.empresa		
				Replace uCrsStSa.loja 		 WITH uCrsStSaLnk.loja			
				Replace uCrsStSa.stock 		 WITH uCrsStSaLnk.stock				
				Replace uCrsStSa.epcpond 	 WITH uCrsStSaLnk.epcpond			
				Replace uCrsStSa.epcpondtotal WITH uCrsStSaLnk.epcpondtotal 				
				Replace uCrsStSa.resfor 	 WITH uCrsStSaLnk.resfor				
				Replace uCrsStSa.rescli 	 WITH uCrsStSaLnk.rescli				
				
			ENDSCAN
		ENDSCAN 
	ENDIF 
	
	SELECT uCrsStSa
	GO TOP 
	**IF OrigemChamada = 'ATENDIMENTO'
		uf_stockporarmazem_configgrelha()
	**ENDIF 
ENDFUNC 

**
FUNCTION  uf_stockPorArmazem_report	

	IF !uf_gerais_validaPermUser(ch_userno, ch_grupo,'Relat�rio Extrato de Stock por Armaz�m')
		uf_perguntalt_chama("O SEU PERFIL N�O TEM PERMISS�ES PARA ACEDER A ESTA FUNCIONALIDADE.","OK","",48)
		RETURN .f.
	ENDIF
	IF !uf_gerais_validaPermUser(ch_userno, ch_grupo,'Gest�o de stock - Visualizar stock armaz�m')
		uf_perguntalt_chama("O SEU PERFIL N�O TEM PERMISS�ES PARA ACEDER A ESTA FUNCIONALIDADE.","OK","",48)
		RETURN .f.
	ENDIF
	** Construir string com parametros
	      lcSql = "&site="    + mysite+; 
				"&ref="		+ ALLTRIM(stockPorArmazem.txtRef.value)+;
				"&userno="  	+ALLTRIM(STR(ch_userno))+;
				"&grupo=" 		+ALLTRIM(ch_grupo)
	
	** chamar report
	uf_gerais_chamaReport("relatorio_stocks_porArmazem", lcSql)
ENDFUNC 


**
FUNCTION  uf_stockPorArmazem_sair
	IF  USED("uCrsStSa")
		Fecha("uCrsStSa")
	ENDIF 
	
	RELEASE myOrigemRefPStSa
	RELEASE myOrigemDesignPStSa
	
	stockPorArmazem.hide
	stockPorArmazem.release
ENDFUNC 

FUNCTION uf_stockporarmazem_configgrelha


	IF uf_gerais_getParameter_site('ADM0000000058', 'BOOL', mySite) = .t. AND !EMPTY(uf_gerais_getParameter_site('ADM0000000058', 'TEXT', mySite))
		IF TYPE("STOCKPORARMAZEM") != 'U'
			Local ti, i
			STORE 0 TO ti, i	
			
			PUBLIC lcQttVerm, lcQttAmarMax, lcQttAmarMin , lcQttVerde
			lcQttVerm = VAL(uf_gerais_getParameter_site('ADM0000000057', 'TEXT', mySite))
			lcQttAmarMin = val(substr(uf_gerais_getParameter_site('ADM0000000058', 'TEXT', mySite),1, at(" ", uf_gerais_getParameter_site('ADM0000000058', 'TEXT', mySite))))
			lcQttAmarMax = val(substr(uf_gerais_getParameter_site('ADM0000000058', 'TEXT', mySite),at(" ", uf_gerais_getParameter_site('ADM0000000058', 'TEXT', mySite)), 2))
			lcQttVerde = VAL(uf_gerais_getParameter_site('ADM0000000059', 'TEXT', mySite))
			ti = stockPorArmazem.grid1.columnCount
			FOR i=1 TO ti
				IF stockPorArmazem.grid1.Columns(i).name == "armazem" OR stockPorArmazem.grid1.Columns(i).name == "stock"
					**stockPorArmazem.grid1.columns(i).dynamicBackColor = "IIF(uCrsStSa.stock <= lcQttVerm , rgb[255,0,0], (IIF(uCrsStSa.stock >= lcQttVerde ,rgb[0,128,0], rgb[255,128,0])) )"
					stockPorArmazem.grid1.columns(i).dynamicBackColor = "IIF(uCrsStSa.stock < lcQttAmarMin , rgb[255,0,0], (IIF(uCrsStSa.stock > lcQttAmarMax , rgb[0,255,0], rgb[255,128,0])) )"
		*!*				If Upper(alltrim(stockPorArmazem.grid1.Columns(i).name))=="STOCK" AND uf_gerais_getParameter_site('ADM0000000060', 'BOOL', mySite) AND ALLTRIM(ch_grupo) <> 'Administrador'
		*!*					stockPorArmazem.grid1.columns(i).dynamicForeColor = "IIF(uCrsStSa.stock1 <= lcQttVerm , rgb[255,0,0], (IIF(uCrsStSa.stock1 >= lcQttVerde ,rgb[0,128,0], rgb[255,128,0])) )"
		*!*				ENDIF 	
				ENDIF 
			ENDFOR
		ENDIF 
	ENDIF 
	

	
	IF TYPE("STOCKPORARMAZEM") != 'U'
		stockPorArmazem.grid1.refresh
	ENDIF 
ENDFUNC 