**
FUNCTION uf_recursos_chama
	LPARAMETERS lcstamp
	PUBLIC myRecursosIntroducao, myRecursosAlteracao
	
	IF !uf_gerais_validaPermUser(ch_userno,ch_grupo,'Gest�o Recursos')
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE CONSULTAR AS CONFIGURA��ES DE SISTEMA.","OK","",48)
		RETURN .f.
	ENDIF
	
	LOCAL lcSQL
	STORE '' TO lcSQL

	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_Marcacoes_PequisaRecurso '<<ALLTRIM(lcstamp)>>'
	ENDTEXT	
	IF !uf_gerais_actgrelha("", "uCrsDadosRecurso", lcSQL)
		MESSAGEBOX("OCORREU UM ERRO A VERIFICAR DADOS DO RECURSO! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF

	IF type("RECURSOS")=="U"
		DO FORM RECURSOS
	ELSE
		RECURSOS.show
	ENDIF

	IF !EMPTY(uCrsDadosRecurso.no)
		RECURSOS.Pageframe1.Page2.Disponibilidades1.Atualizar("RECURSOS",uCrsDadosRecurso.no,0)
	ENDIF
	uf_recursos_alternaMenu()
ENDFUNC

**
FUNCTION uf_recursos_init
	WITH recursos
		
		&& Configura menu principal
		.menu1.adicionaOpcao("opcoes", "Op��es", myPath + "\imagens\icons\opcoes_w.png", "","O")
		.menu1.adicionaOpcao("pesquisar", "Pesquisar", myPath + "\imagens\icons\lupa_w.png", "uf_pesqrecurso_Chama","P")
		.menu1.adicionaOpcao("actualizar", "Atualizar", myPath + "\imagens\icons\actualizar_w.png", "uf_recursos_actualiza","A")
		.menu1.adicionaOpcao("novo", "Novo", myPath + "\imagens\icons\mais_w.png", "uf_recursos_novo","N")
		.menu1.adicionaOpcao("editar", "Editar", myPath + "\imagens\icons\lapis_w.png", "uf_recursos_editar","E")
		.menu1.adicionaOpcao("eliminar", "Eliminar", myPath + "\imagens\icons\cruz_w.png", "uf_recursos_apaga","X")
		.menu_opcoes.adicionaOpcao("disponibilidades", "Disponibilidades", "", "uf_recursos_disponibilidades")
		.menu_opcoes.adicionaOpcao("ultimo", "�ltimo", "", "uf_recursos_ultimo")
	ENDWITH 
ENDFUNC


**
FUNCTION uf_recursos_actualiza
	IF USED("uCrsDadosRecurso")
		SELECT uCrsDadosRecurso
		uf_recursos_chama(ALLTRIM(uCrsDadosRecurso.recursosstamp))
		Recursos.refresh
	ENDIF
ENDFUNC


**
FUNCTION uf_recursos_apaga
	LOCAL lcSQL
	
	IF USED("uCrsDadosRecurso") AND myRecursosIntroducao != .t. AND myRecursosAlteracao != .t.
		IF uf_perguntalt_chama("Vai eliminar o recurso. Pretende continuar.", "SIM", "N�O", 64) == .t.
			
			SELECT uCrsDadosRecurso
			lcSQL = ""
			TEXT TO lcSQL TEXTMERGE NOSHOW
				exec up_Marcacoes_EliminaRecurso '<<ALLTRIM(uCrsDadosRecurso.recursosstamp)>>'
			ENDTEXT
			
			IF !uf_gerais_actgrelha("", "", lcSQL)
				MESSAGEBOX("N�O FOI POSS�VEL ELIMINAR O RECURSO. POR FAVOR CONTACTE O SUPORTE",16,"LOGITOOLS SOFTWARE")
				RETURN .f.			
			ELSE
				MESSAGEBOX("RECURSO ELIMINADO COM SUCESSO.",64,"LOGITOOLS SOFTWARE")
				uf_recursos_ultimo()
				uf_recursos_alternaMenu()
			ENDIF
		ENDIF
	ENDIF
ENDFUNC 


**
FUNCTION uf_recursos_ultimo
	**
	LOCAL lcSQL
	
	TEXT TO lcSQL NOSHOW TEXTMERGE
		exec up_Marcacoes_UltimaAlteracaoRecurso
	ENDTEXT
	IF !uf_gerais_actgrelha("", "uCrsultimoRecurso", lcSQL)
		MESSAGEBOX("N�O FOI POSSIVEL VERIFICAR O �LTIMO RECURSO ALTERADO! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF
	IF RECCOUNT("uCrsultimoRecurso") > 0
		SELECT uCrsultimoRecurso	
		uf_recursos_chama(ALLTRIM(uCrsultimoRecurso.recursosstamp))
	ENDIF
	fecha("uCrsultimoRecurso")
ENDFUNC


**Gravar Recurso na BD
FUNCTION uf_recursos_gravar
	PUBLIC myRecursosIntroducao, myRecursosAlteracao
	LOCAL lcCamposObrigatoriosRecursos, lcValidainsertRecurso, lcValidainsertRecursoExcepcoes, lcValidaupdateRecursoExcepcoes  
	
	Store .f. To lcCamposObrigatoriosRecursos, lcValidainsertRecurso 
	STORE .t. TO lcValidainsertRecursoExcepcoes, lcValidaupdateRecursoExcepcoes  
	
	** Para chamar esta fun��o uma das variaveis tem que estar a .t.
	IF (myRecursosIntroducao == .f. AND myRecursosAlteracao == .f.) OR !USED("uCrsDadosRecurso")
		RETURN .f.
	ENDIF
		
	lcCamposObrigatoriosRecursos = uf_Recursos_camposObrigatorios()
	
	** Caso seja retornado .f. sai fora, j� deu aviso na fun��o anterior
	IF lcCamposObrigatoriosRecursos == .f.
		RETURN .f.
	ENDIF
	
	IF myRecursosIntroducao = .t.
				
		** Valida Numera��o do Recurso, para quando existe inser��es em simultaneo
		TEXT TO lcSQL NOSHOW TEXTMERGE
			exec up_Marcacoes_NumeroRecurso
		ENDTEXT
		IF !uf_gerais_actgrelha("", "uCrsNumeroRecurso", lcSQL)
			MESSAGEBOX("OCORREU UM ERRO A VERIFICAR NUMERA��O DO RECURSO! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
			RETURN .f.
		ENDIF

		SELECT uCrsNumeroRecurso
		SELECT uCrsDadosRecurso
		IF uCrsNumeroRecurso.numero != uCrsDadosRecurso.no
			SELECT uCrsDadosRecurso
			Replace uCrsDadosRecurso.no WITH uCrsNumeroRecurso.numero
		ENDIF
		*********************************************************
		
		**Faz a Inser��o
		lcSQL = ""
		TEXT TO lcSQL NOSHOW TEXTMERGE
			INSERT INTO recursos(
					recursosstamp
					,nome
					,no
					,obs
					,grupo
					,tipo
					,ousrinis
					,ousrdata
					,ousrhora
					,usrinis
					,usrdata
					,usrhora
			)VALUES	(
					'<<ALLTRIM(uCrsDadosRecurso.recursosstamp)>>'
					,'<<ALLTRIM(uCrsDadosRecurso.nome)>>'
					,<<uCrsDadosRecurso.no>>
					,'<<uCrsDadosRecurso.obs>>'
					,'<<ALLTRIM(uCrsDadosRecurso.grupo)>>'
					,'<<ALLTRIM(uCrsDadosRecurso.tipo)>>'
					,'<<m.m_chinis>>'
					,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
					,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8)
					,'<<m.m_chinis>>'
					,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
					,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8)
			)
		ENDTEXT
		IF !uf_gerais_actgrelha("", "", lcSQL)					
			MESSAGEBOX("OCORREU UM ERRO A INSERIR RECURSO! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
			return .f.
		ENDIF
	
	
	ELSE 
		
			
			lcSQL = ""
			TEXT TO lcSQL NOSHOW TEXTMERGE
				UPDATE	
					recursos 
				SET 	
					nome 			= '<<ALLTRIM(uCrsDadosRecurso.nome)>>',
					no				= <<uCrsDadosRecurso.no>>,
					obs				= '<<uCrsDadosRecurso.obs>>',
					grupo			= '<<ALLTRIM(uCrsDadosRecurso.grupo)>>',
					tipo 			= '<<ALLTRIM(uCrsDadosRecurso.tipo)>>',
					usrinis			= '<<m.m_chinis>>',
					usrdata			= convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102),
					usrhora			= convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8)
				WHERE 	
					recursosstamp 	= '<<ALLTRIM(uCrsDadosRecurso.recursosstamp)>>'
			ENDTEXT
			IF !uf_gerais_actgrelha("", "", lcSQL)
				MESSAGEBOX("OCORREU UM ERRO A ACTUALIZAR DADOS DO RECURSO! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
				return .f.
			ENDIF
			
			

	ENDIF
		
	myRecursosIntroducao	= .f.
	myRecursosAlteracao	= .f.
	recursos.refresh
	uf_recursos_alternaMenu()	
ENDFUNC




**Valida preenchimento de campos obrigat�rios ao gravar
FUNCTION uf_Recursos_camposObrigatorios
	
	SELECT uCrsDadosRecurso
	GO TOP 

	IF EMPTY(uCrsDadosRecurso.nome)
		MESSAGEBOX("EXISTEM CAMPOS OBRIGAT�RIOS POR PREECHER. POR FAVOR VERIFIQUE.",48,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF
	
	RETURN .t.
ENDFUNC


**
FUNCTION uf_recursos_editar
	PUBLIC myRecursosIntroducao, myRecursosAlteracao
	
	IF USED("uCrsDadosRecurso") 
		IF RECCOUNT("uCrsDadosRecurso")>0
			myRecursosIntroducao = .f.
			myRecursosAlteracao = .t.
			uf_recursos_alternaMenu()
		ENDIF
	ENDIF

ENDFUNC


**
FUNCTION uf_recursos_novo
	PUBLIC myRecursosIntroducao, myRecursosAlteracao
	
	myRecursosIntroducao = .t.
	myRecursosAlteracao = .f.
		
	**Numer��o
	TEXT TO lcSQL NOSHOW TEXTMERGE
		exec up_Marcacoes_NumeroRecurso
	ENDTEXT
	IF !uf_gerais_actgrelha("", "ucrsTempNumero", lcSQL)
		MESSAGEBOX("N�O FOI POSSIVEL DETERMINAR O N�MERO INTERNO DO RECURSO, POR FAVOR CONTACTE O SUPORTE!",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF
			
	** Elimina Dados do cursor
	SELECT uCrsDadosRecurso
	GO TOP 
	SCAN 
		Delete
	ENDSCAN

	lcStamp = uf_gerais_stamp()
	SELECT uCrsDadosRecurso
	APPEND BLANK
	SELECT ucrsTempNumero
	Replace uCrsDadosRecurso.recursosstamp  WITH lcStamp
	Replace uCrsDadosRecurso.no  			WITH ucrsTempNumero.numero 
	
	IF USED("ucrsTempNumero")
		fecha("ucrsTempNumero")
	ENDIF
		
	uf_recursos_alternaMenu()
	
ENDFUNC


**
FUNCTION uf_recursos_alternaMenu
	
	** Em modo de Leitura
	IF myRecursosIntroducao == .f. AND  myRecursosAlteracao == .f.
		recursos.menu1.estado("opcoes, pesquisar, actualizar, novo, editar, eliminar", "SHOW", "Gravar", .f., "Sair", .t.)
	ELSE
		** menu1
		recursos.menu1.estado("opcoes, pesquisar, actualizar, novo, editar, eliminar", "HIDE", "Gravar", .t., "Cancelar", .t.)
	ENDIF

	uf_recursos_obj()

	recursos.refresh
ENDFUNC


FUNCTION uf_recursos_obj
	WITH recursos
		**Edi��o
		IF myRecursosIntroducao == .t. OR myRecursosAlteracao == .t.
			.nome.readonly = .f.
			.no.readonly = .f.
			.pageframe1.page1.obs.readonly = .f.
		ELSE
			.nome.readonly = .t.
			.no.readonly = .t.
			.pageframe1.page1.obs.readonly = .t.
		ENDIF
	ENDWITH 
ENDFUNC


FUNCTION uf_recursos_disponibilidades

	uf_SERIESSERV_chama('','')

ENDFUNC

**
FUNCTION uf_recursos_sair
	
	IF myRecursosIntroducao == .t. OR myRecursosAlteracao == .t.
		IF uf_perguntalt_chama("Quer cancelar as altera��es efectuadas?", "SIM", "N�O", 64) == .t.
			STORE .f. TO myRecursosIntroducao , myRecursosAlteracao 			
			uf_recursos_alternaMenu()
			uf_recursos_ultimo()
		ENDIF
	ELSE
		**Fecha Cursores
		RECURSOS.hide
		RECURSOS.release
			
		**Liberta variaveis publicas
		RELEASE RECURSOS
	ENDIF
ENDFUNC 


