**
FUNCTION uf_pesqstocks_chama
	LPARAMETERS nOrigem, nRefDesign, lcModal, lcCNPEM, lcSoParafarmacia, lcDemRef, lcDemCNPEM, lcDemDescr, lcDemPosologia, lcDemTipoReceita, lcDemTipoLinha, lcDemDci

	&& CONTROLA PERFIS DE ACESSO
	IF !uf_gerais_validaPermUser(ch_userno,ch_grupo,'Gest�o de Stock')
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ACEDER AO PAINEL GEST�O DE STOCK.","OK","",48)
		RETURN .f.
	ENDIF
	
	IF ALLTRIM(nOrigem) == 'STOCKS_ALTERACOES'
		IF !uf_gerais_validaPermUser(ch_userno,ch_grupo,'Altera��o em grupo de stocks')
			uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ACEDER AO PAINEL ALTERA��ES DE STOCK EM GRUPO.","OK","",48)
			RETURN .f.
		ENDIF
	ENDIF 

	LOCAL lcMaximiza
	lcMaximiza = .f.
	
	&& Valida par�metro
	IF EMPTY(nOrigem) OR !(TYPE("nOrigem")=="C")
		uf_perguntalt_chama("O PAR�METRO: ORIGEM, N�O � V�LIDO.","OK","", 48)
		RETURN .F.
	ENDIF
	
	PUBLIC myOrderPesquisarSt, myOrigemPPST, myOrigemRefDesignPPST, myHideMultiSel, up_altDisp, up_altNov, up_altPort

    up_altDisp = .F. 
    up_altNov = .F. 
    up_altPort = .F.
	
	myOrderPesquisarSt = .t.
	
	myOrigemPPST = UPPER(ALLTRIM(nOrigem))
	
	IF !EMPTY(lcDemRef) OR !EMPTY(lcDemCNPEM)
		myHideMultiSel = .t.
	ELSE
		myHideMultiSel = .f.
	ENDIF 

	IF !EMPTY(nRefDesign) AND TYPE('nRefDesign') == "C"
		myOrigemRefDesignPPST = ALLTRIM(nRefDesign)
	ELSE
		myOrigemRefDesignPPST = ''
	ENDIF
	
	regua(0,3,"A carregar painel...")
	regua(1,1,"A verificar permiss�es...")
	&& Valida permiss�o dos stocks
	IF !(uf_gerais_validaPermUser(ch_userno, ch_grupo, 'Gest�o de Stock - Visualizar'))
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE CONSULTAR ARTIGOS.","OK","", 48)
		regua(2)
		RETURN .F.
	ENDIF
	
	regua(1,2,"A consultar a BD...")
	** criar cursor default
	LOCAL lcSql
	lcSql = ""
	
&&	fecha("uCrsPesqST")
	IF !USED("uCrsPesqST")
		CREATE CURSOR uCrsPesqST(sel l, ref c(18), design c(100), temFicha l, stock n(13,3), stockempresa n(13,3), stockgrupo n(13,3), epv1 n(19,6), validade d, faminome c(60), familia c(18), comp l, generico l, psico l, benzo l;
		,ststamp c(25), ptoenc n(10,3), stmax n(13,3), marg1 n(16,3), local c(20), u_local c(60), u_local2 c(60), cnpem c(8), marg3 n(16,3), marg4 n(16,3), t5 l, t45 c(20), textosel c(254), u_lab c(150);
		,usr1 c(20), stns l, inactivo l, dci c(254), site c(60), obs c(254), marcada l, hst l, diassmov n(9,0), pctf c(18),qttminvd n(10,2),qttembal n(10,2) ,epvqttminvd n(10,2),epvqttembal n(10,2), histDias c(10) , histDiasOrdem n(10), hist n(10), BB N(16,3), mfornec n(10,4), mfornec2 n(10,4))
	ENDIF 

	IF nOrigem == "SERIESSERV";
		OR nOrigem== "CONSUMO";
		OR nOrigem == "CONSUMO_MR";
		OR nOrigem == "CONSUMO_STOCKS";
		OR nOrigem == "FACTURACAOENTIDADES_UTENTES";
		OR nOrigem == "FACTURACAOASSOCIADOS_UTENTES";		
		OR nOrigem == "HONORARIOS";
		OR nOrigem == "SERVFACTUT";
		OR nOrigem == "ENTIDADECOMPARTICIPADORA";
		OR nOrigem == "CONVENCAO";
		OR nOrigem == "COMPONENTE_STOCKS";
		OR !EMPTY(lcSoParafarmacia)
		
		SELECT uCrsPesqST
		GO TOP 
		SCAN
			DELETE
		ENDSCAN
	ENDIF
	
	regua(1,3,"A carregar o painel...")
	&& abre o formulario
	IF TYPE("pesqstocks") == "U"
		regua(2)
		DO FORM pesqstocks WITH .t., lcModal, lcCNPEM, lcSoParafarmacia, lcDemRef, lcDemCNPEM, lcDemDescr, lcDemPosologia, lcDemTipoReceita, lcDemTipoLinha, lcDemDci
	ELSE
		regua(2)
		pesqstocks.show
	ENDIF	
	
	uf_pesqStocks_configgrelha()

ENDFUNC


**
FUNCTION uf_pesqstocks_CarregaMenu	
	** Confgura Barra Lateral
	WITH pesqstocks.menu1
		*.adicionaOpcao("aplicacoes", "Aplica��es", myPath + "\imagens\icons\aplicacoes_w.png", "uf_gerais_menuAplicacoes With 'pesqstocks'","")
		.adicionaOpcao("opcoes", "Op��es", myPath + "\imagens\icons\opcoes_w.png", "uf_gerais_menuOpcoes With 'pesqstocks'","")
		.adicionaOpcao("actualizar", "Pesquisar", myPath + "\imagens\icons\lupa_w.png", "uf_pesqstocks_carregaDados","")
		.adicionaOpcao("novavenda", "Nova venda", myPath + "\imagens\icons\mais_w.png", "uf_pesqstocks_novavenda","")
		.adicionaOpcao("novoProd", "Criar Ficha", myPath + "\imagens\icons\mais_w.png", "uf_pesqstocks_criarFicha","")
		.adicionaOpcao("novaLinha", "Nova Linha", myPath + "\imagens\icons\mais_w.png", "uf_pesqstocks_novaLinha","")
		IF UPPER(myPaisConfSoftw) == 'ANGOLA'
			.adicionaOpcao("InfoCom", "Stk. Arm.", myPath + "\imagens\icons\detalhe_micro.png", "uf_stockPorArmazem_chama with ALLTRIM(uCrsPesqST.ref), ALLTRIM(uCrsPesqST.design), 'ATENDIMENTO'","")
		ELSE
			.adicionaOpcao("InfoCom", "Inf. Comercial", myPath + "\imagens\icons\detalhe_micro.png", "uf_pesqstocks_verFicha","")
		ENDIF 
		.adicionaOpcao("InfoFar", "Inf. Farma.", myPath + "\imagens\icons\detalhe_micro.png", "uf_pesqStocks_verDic","")
		.adicionaOpcao("InfoFornec", "Consulta Forn.", myPath + "\imagens\icons\detalhe_micro.png", "uf_pesqStocks_verFornec","")
		.adicionaOpcao("multiSel", "Multi Sel.", myPath + "\imagens\icons\unchecked_w.png", "uf_pesqstocks_multiSel","")
		.adicionaOpcao("selTodos", "Sel. todos", myPath + "\imagens\icons\unchecked_w.png", "uf_pesqstocks_SelTodos with .t.","")
		.adicionaOpcao("regfalta", "Registar Falta", myPath + "\imagens\icons\inventario_w36.png", "uf_pesqstocks_regfalta with ALLTRIM(uCrsPesqST.ref)","")
		.adicionaOpcao("alternativos", "Alternativos", mypath+"\imagens\icons\checked_w.png",  "uf_pesqstocks_alternativos","")
	ENDWITH 

	** Configura Menu Aplica��es
**	WITH pesqstocks.menu_aplicacoes
**		.adicionaOpcao("validades", "Confer�ncia de Validades", "", "uf_PESQSTOCKS_validadeschama")
**		.adicionaOpcao("confPvps", "Gest�o de PVPs", "", "uf_PESQSTOCKS_confpvpchama")
**		.adicionaOpcao("altGrupo", "Alt. em Grupo", "", "uf_PESQSTOCKS_altprodchama")
**		.adicionaOpcao("inventario", "Inventario", "", "uf_PESQSTOCKS_inventariochama")
**		.adicionaOpcao("verInfoCom","Ver Inf. Comercial","","uf_pesqstocks_verFicha")
**		.adicionaOpcao("verInfoFar","Ver Inf. Farmac�utica","","uf_pesqStocks_verDic")
**		
**	ENDWITH

	** Configura Menu Op��es
	WITH pesqstocks.menu_opcoes
		.adicionaOpcao("prioritarios", "Imp. Priorit�rios", "", "uf_stocks_actualizaImportaPrioritarios")
		.adicionaOpcao("imprimir", "Imprimir Dados", "", "uf_pesqstocks_imprimir")
		.adicionaOpcao("Importxls", "Importar XLS", "", "uf_pesqStocks_importXls")
        .adicionaOpcao("validades", "Confer�ncia de Validades", "", "uf_PESQSTOCKS_validadeschama")
        .adicionaOpcao("confPvps", "Gest�o de PVPs", "", "uf_PESQSTOCKS_confpvpchama")
      IF ALLTRIM(UPPER(myOrigemPPST)) <> "STOCKS_ALTERACOES"
         .adicionaOpcao("altGrupo", "Alt. em Grupo", "", "uf_PESQSTOCKS_altprodchama")
      ENDIF
        .adicionaOpcao("inventario", "Inventario", "", "uf_PESQSTOCKS_inventariochama")
        *.adicionaOpcao("verInfoCom","Ver Inf. Comercial","","uf_pesqstocks_verFicha")
        .adicionaOpcao("verInfoFar","Ver Inf. Farmac�utica","","uf_pesqStocks_verDic")
	ENDWITH 
	
	pesqstocks.Menu1.gravar.img1.Picture = myPath + "\imagens\icons\guardar_w.png"
	
*!*		IF uf_gerais_getParameter_site('ADM0000000067', 'BOOL', mySite)
*!*			pesqstocks.menu1.estado("novavenda", "HIDE")	
*!*		ENDIF 
	IF !uf_gerais_getParameter_site('ADM0000000139', 'BOOL', mySite) OR myOrigemPPST == "STOCKS_ALTERACOES"
		pesqstocks.menu1.estado("regfalta", "HIDE")	
	ENDIF 
	
	uf_pesqstocks_alternaMenu()
		
ENDFUNC


**
FUNCTION uf_pesqStocks_verDic
	SELECT uCrsPesqST
	IF !EMPTY(uCrsPesqST.ref)
		uf_dicionario_chama(uCrsPesqST.ref)
		uf_pesqstocks_sair()
		dicionario.show
	ELSE 
		uf_dicionario_pesquisar()
		uf_pesqstocks_sair()
	ENDIF
ENDFUNC 


**
FUNCTION uf_pesqstocks_selLab
	IF pesqstocks.pageframe1.activePage == 1 && n�o touch
		lcObj = "pesqstocks.pageframe1.page1"
	ELSE
		lcObj = "pesqstocks.pageframe1.page2"
	ENDIF
	
	&lcObj..lab.value = ALLTRIM(uf_gerais_getParameter("ADM0000000204","TEXT"))
	pesqstocks.menu1.actualizar.click
ENDFUNC


**
FUNCTION uf_pesqstocks_novavenda
	uf_atendimento_novaVenda(.t.)
ENDFUNC 


**
FUNCTION uf_pesqstocks_alternaMenu

	DO CASE 
		CASE myOrigemPPST == "ATENDIMENTO"
			pesqstocks.menu1.estado(" opcoes, novalinha, selTodos, novoProd ", "HIDE")
			*pesqstocks.menu_aplicacoes.estado("validades, confPvps, altGrupo, inventario", "HIDE")
			*pesqstocks.menu_aplicacoes.estado("verInfoCom, verInfoFar", "SHOW")
			pesqstocks.menu_opcoes.estado("Importxls", "HIDE")
			IF !EMPTY(myHideMultiSel)
				pesqstocks.menu1.estado("MultiSel", "HIDE")			
			ENDIF 

		CASE myOrigemPPST == "STOCKS"
			pesqstocks.menu1.estado("novaLinha, novavenda, selTodos, InfoCom, InfoFar, MultiSel ", "HIDE")
			pesqstocks.menu_opcoes.estado("Importxls", "HIDE")					
	
		CASE myOrigemPPST == "STOCKS_ALTERACOES"
         pesqstocks.menu1.estado("novavenda", "HIDE")
			TEXT To lcSQL TEXTMERGE NOSHOW
				select site from empresa order by site
			ENDTEXT 
			uf_gerais_actGrelha("","ucrsEmpresas",lcSQL)
			IF RECCOUNT("ucrsEmpresas")>1 then
				pesqstocks.menu1.adicionaOpcao("empresas", "Empresas", myPath + "\imagens\icons\empresa_b_lateral.png", "uf_altgr_escolheEmpresas","")
			ENDIF 
			pesqstocks.menu1.estado(" novalinha, novoProd, InfoCom, InfoFar, InfoFornec", "HIDE", "Gravar", .T., "Sair", .T.)
			**pesqstocks.menu_aplicacoes.estado("validades, confPvps, altGrupo, inventario, verInfoCom, verInfoFar", "HIDE")
			pesqstocks.menu_opcoes.estado("Importxls", "SHOW")
			pesqstocks.menu_opcoes.estado("prioritarios, imprimir", "HIDE")
			pesqstocks.menu1.estado("selTodos", "SHOW")



		CASE myOrigemPPST == "DOCUMENTOS"
			pesqstocks.menu1.estado("opcoes, novavenda, InfoFar, InfoFornec, InfoCom, novaLinha", "HIDE", "", .T., "Sair", .T.)
			pesqstocks.menu_opcoes.estado("Importxls", "HIDE")

		CASE myOrigemPPST == "ENCAUTOMATICAS"
			pesqstocks.menu1.estado(" opcoes, novavenda, selTodos, InfoCom, InfoFar", "HIDE", "Gravar", .T., "Sair", .T.)
			pesqstocks.menu_opcoes.estado("Importxls", "HIDE")

		CASE myOrigemPPST == "INVENTARIO"
			pesqstocks.menu1.estado(" opcoes, novavenda, novalinha, novoProd, InfoCom, InfoFar, InfoFornec", "HIDE", "Gravar", .T., "Sair", .T.) &&, selTodos
			pesqstocks.menu_opcoes.estado("Importxls", "HIDE")

		CASE myOrigemPPST == "CONSOLIDACAOCOMPRAS"
			pesqstocks.menu1.estado(" opcoes, novavenda, novalinha, InfoCom, InfoFar, InfoFornec", "HIDE", "Aplicar", .F., "Sair", .T.)			
			pesqstocks.menu_opcoes.estado("Importxls", "HIDE")
		
		CASE myOrigemPPST == "CAMPANHAS"
			pesqstocks.menu1.estado(" opcoes, novavenda, novalinha, InfoCom, InfoFar, selTodos, InfoFornec", "HIDE", "Aplicar", .T., "Sair", .T.)
			pesqstocks.menu_opcoes.estado("Importxls", "HIDE")
		
		CASE myOrigemPPST == "FACTURACAO"		
			pesqstocks.menu1.estado(" opcoes, novavenda, InfoFar, InfoFornec", "HIDE", "", .T., "Sair", .T.)
			pesqstocks.menu_opcoes.estado("Importxls", "HIDE")
		
		CASE myOrigemPPST == "COMPONENTE_STOCKS"
			pesqstocks.menu1.estado(" opcoes, novavenda, novalinha, selTodos, InfoCom, InfoFar, novoProd, multiSel, InfoFornec", "HIDE", "", .F., "Sair", .T.)
			pesqstocks.menu_opcoes.estado("Importxls", "HIDE")
		OTHERWISE
			pesqstocks.menu1.estado(" opcoes, novavenda, novalinha, selTodos, InfoCom, InfoFar, InfoFornec", "HIDE", "", .F., "Sair", .T.)
	ENDCASE 
	
ENDFUNC


**
FUNCTION uf_pesqstocks_verFicha
	SELECT uCrsPesqST
	uf_stocks_chama(uCrsPesqST.ref, .t.)
	**uf_pesqstocks_sair()
	stocks.show
ENDFUNC 


**
FUNCTION uf_pesqstocks_carregaDados
	LOCAL lcSQL, lcTop, lcRef, lcFamilia, lcLab, lcMarca, lcDci, lcStock, lcGenerico, lcCNPEM, lcRecurso, lcAtributos, lcConvencao, lcClass2, lcTipoProduto
	LOCAL lcOperadorLogico, lcInactivos, lcServicos, lcSiteNr,lcSite, lcParafarmacia, lctxIva ,lcFornecedor, uv_fornStamp, lcLocalizacao
	STORE "" TO lcSQL, lcRef, lcFamilia, lcLab, lcMarca, lcDci, lcCNPEM, lcRecurso, lcAtributos, lcSite, lcConvencao, lctxIva, lcFornecedor, lcClass2, lcTipoProduto, uv_fornStamp, lcLocalizacao
	STORE 0 TO lcTop, lcStock, lcInactivos, lcGenerico, lcServicos, lcSiteNr, lcParafarmacia

	&& Guarda produtos selecionados
	IF myOrigemPPST == "ENCAUTOMATICAS"
		SELECT uCrsPesqST 
		GO TOP
		
		SELECT * FROM uCrsPesqST WHERE uCrsPesqST.sel == .t. INTO CURSOR uCrsPesqST_Sel READWRITE 
	ENDIF 		
	
	regua(0,3,"A atualizar dados...")
	regua(1,1,"A atualizar dados...")
	
	LOCAL lcObj
	DO CASE 
		CASE pesqstocks.pageframe1.activePage == 1 && n�o touch
			lcObj = "pesqstocks.pageframe1.page1"
		CASE pesqstocks.pageframe1.activePage == 2
			lcObj = "pesqstocks.pageframe1.page2"
	ENDCASE 

	&& Configura grelha de acordo com a informa��o que deve ser visivel
	pesqstocks.grdPesq.StockEmpresa.header1.caption = "STK. Emp."
	pesqstocks.grdPesq.StockEmpresa.dynamicBackColor = "IIF(Recno()%2!=0,Rgb[255,255,255],rgb[240,240,240])"

	&& Trata parametros
	lcTop = IIF(INT(VAL(&lcObj..topo.value))== 0,100000,INT(VAL(&lcObj..topo.value)))
	lcRef = STRTRAN(ALLTRIM(&lcObj..ref.value),' ','%')
	lcMarca = ALLTRIM(&lcObj..marca.value)
	lctxIva = ASTR(&lcObj..txIva.value)
 
	**	
	IF pesqstocks.pageframe1.activePage == 1
		lcFamilia 			= ALLTRIM(&lcObj..familia.value)
		lcLab				= ALLTRIM(&lcObj..lab.value)
		lcDci				= ALLTRIM(&lcObj..dci.value)
		lcGenerico			= IIF(ALLTRIM(&lcObj..genericos.value)=="", -1, IIF(ALLTRIM(&lcObj..genericos.value)=="Gen�ricos",1,0))
		lcLocalizacao       = ALLTRIM(&lcObj..txtLocalizacao.value)
		
		** Quantidade de stock
		lcOperadorLogico	= IIF(ALLTRIM(&lcObj..OperadorLogico.value)==">", "maior", IIF(ALLTRIM(&lcObj..OperadorLogico.value)=="<", "menor", "igual"))
				
		&& Valida Stock
		IF !EMPTY(&lcObj..stock.value)
			IF TYPE(&lcObj..stock.value) == "N"
				lcStock = &lcObj..stock.value
			ELSE
				lcStock = 0
			ENDIF
		ELSE
			lcStock = -999999
		ENDIF
		
		lcFornecedor  = ALLTRIM(&lcObj..fornecedor.value)
		uv_fornStamp  = ALLTRIM(PESQSTOCKS.PageFrame1.page1.fornecedor.tag)
		lcClass2 	  = ALLTRIM(&lcObj..Class2.value)
		lcTipoProduto = ALLTRIM(&lcObj..TipoProduto.value)
	ELSE
		lcRecurso = ALLTRIM(&lcObj..recurso.value)
		lcStock = -999999
		lcOperadorLogico = "maior"
	ENDIF 

	&& servicos
	lcServicos	= 0 
	IF UPPER(ALLTRIM(&lcObj..tipo.value)) == "SERVICOS"
		lcServicos	= 1 
	ENDIF 
	
	IF UPPER(ALLTRIM(&lcObj..tipo.value)) == "PRODUTOS"
		lcServicos	= 2		
	ENDIF 
	
	lcInactivos	= IIF(&lcObj..chkinactivos.tag == "true", 1, 0)
	
	IF pesqstocks.pageframe1.activePage == 2
		lcParafarmacia = 0
		lcComFicha	   = 1
		lcT5		   = 0
		lcT4		   = 0
		lcMovimentos   = 0
	ELSE
		lcParafarmacia 	= IIF (&lcObj..chkParafarmacia.tag == "true", 1,0)
		lcComFicha		= IIF(&lcObj..chkComFicha.tag == "true", 1, 0)
		lcT5			= IIF(&lcObj..chkT5.tag == "true", 1, 0)
		lcT4			= IIF(&lcObj..chkT4.tag == "true", 1, 0)
		lcMovimentos	= IIF(!EMPTY(&lcObj..SemMovimento.value), &lcObj..SemMovimento.value, 0)
	ENDIF 
	
	lcSQL = ''
	IF &lcObj..chkContido.tag == "true"
		TEXT TO lcSQL TEXTMERGE NOSHOW
			exec up_stocks_pesquisa 
				<<lcTop>>
				,'<<lcRef>>'
				,'<<lcFamilia>>'
				,'<<lcLab>>'
				,'<<lcMarca>>'
				,'<<lcDci>>'
				,<<lcServicos>>
				,'<<lcOperadorLogico>>'
				,<<lcStock>>
				,<<lcGenerico>>
				,<<lcInactivos>>
				,<<myArmazem>>
				,0
				,<<IIF(myOrigemPPST=="ATENDIMENTO" or myOrigemPPST=="RESERVAS",1,0)>>
				,'<<lcRecurso>>'
				,<<mySite_nr>>
				,<<lcParafarmacia>>
				,<<lcComFicha>>
				,<<lcT5>>
				,<<IIF(lctxIva=="",-1,lctxIva)>>
				,'<<ALLTRIM(lcFornecedor)>>'
				,<<lcMovimentos>>
				,'<<ALLTRIM(lcClass2)>>'
				,'<<ALLTRIM(lcTipoProduto)>>'
				,<<lcT4>>
				,'<<ALLTRIM(uv_fornStamp)>>'
                ,'<<ALLTRIM(lcLocalizacao)>>'
		ENDTEXT 
	ELSE
		TEXT TO lcSQL TEXTMERGE NOSHOW
			exec up_stocks_pesquisa_comecado 
				<<lcTop>>
				,'<<lcRef>>'
				,'<<lcFamilia>>'
				,'<<lcLab>>'
				,'<<lcMarca>>'
				,'<<lcDci>>'
				,<<lcServicos>>
				,'<<lcOperadorLogico>>'
				,<<lcStock>>
				,<<lcGenerico>>
				,<<lcInactivos>>
				,<<myArmazem>>
				,0
				,<<IIF(myOrigemPPST=="ATENDIMENTO" or myOrigemPPST=="RESERVAS",1,0)>>
				,'<<lcRecurso>>'
				,<<mySite_nr>>
				,<<lcParafarmacia>>
				,<<lcComFicha>>
				,<<lcT5>>
				,<<IIF(lctxIva=="",-1,lctxIva)>>
				,'<<ALLTRIM(lcFornecedor)>>'
				,<<lcMovimentos>>
				,'<<ALLTRIM(lcClass2)>>'
				,'<<ALLTRIM(lcTipoProduto)>>'
				,<<lcT4>>
				,'<<ALLTRIM(uv_fornStamp)>>'
                ,'<<ALLTRIM(lcLocalizacao)>>'
		ENDTEXT 
	ENDIF 

	regua(1,3,"A atualizar dados...")
	
	uf_gerais_actGrelha("pesqstocks.grdPesq", "uCrsPesqST", lcSQL)	
	
	IF USED("uCrsAtendComp")
		LOCAL lcPercAdicSeg
		lcPercAdicSeg=0
		SELECT uCrsAtendComp
		GO TOP 
		IF uCrsAtendComp.percadic > 0
			lcPercAdicSeg = uCrsAtendComp.percadic
		ENDIF 
		IF lcPercAdicSeg > 0
			SELECT uCrsPesqST
			GO TOP 
			SCAN 
				replace uCrsPesqST.epv1 WITH ROUND(uCrsPesqST.epv1*(1+(lcPercAdicSeg)),2)
				replace uCrsPesqST.epvqttminvd WITH ROUND(uCrsPesqST.epvqttminvd *(1+(lcPercAdicSeg)),2)
				replace uCrsPesqST.epvqttembal WITH ROUND(uCrsPesqST.epvqttembal *(1+(lcPercAdicSeg)),2)
			ENDSCAN
			SELECT uCrsPesqST
			GO TOP  
		ENDIF 
	ENDIF 
	
	&&
	IF myOrigemPPST == "ENCAUTOMATICAS"
		IF USED("uCrsPesqST_Sel")
			SELECT uCrsPesqST 
			GO TOP
			SELECT uCrsPesqST_Sel
			GO TOP
			
			SELECT uCrsPesqST.* FROM uCrsPesqST LEFT JOIN uCrsPesqST_Sel ON ALLTRIM(uCrsPesqST.ref) == ALLTRIM(uCrsPesqST_Sel.ref) WHERE uCrsPesqST_Sel.ref is null INTO CURSOR uCrsPesqSTNovaPesq READWRITE  

			SELECT uCrsPesqST 
			GO TOP
			SCAN
				DELETE
			ENDSCAN 

			SELECT uCrsPesqST
			APPEND FROM DBF("uCrsPesqST_Sel")
			
			SELECT uCrsPesqST
			APPEND FROM DBF("uCrsPesqSTNovaPesq")
			
			IF USED("uCrsPesqSTNovaPesq")
				fecha("uCrsPesqSTNovaPesq")
			ENDIF 
			
			SELECT uCrsPesqST 
			GO TOP
		ENDIF
	ENDIF 
	
	pesqstocks.lblRegistos.caption = ALLTRIM(STR(RECCOUNT("uCrsPesqST"))) + " registos"
	pesqstocks.grdPesq.autofit
	
	uf_pesqStocks_configgrelha()
	
	IF pesqstocks.Pageframe1.Page1.ChkHst.tag == "true"
		**uf_pesqstocks_atualizaHst()
		uf_pesqstocks_atualizaHstDias()
	ENDIF 
	
	IF  myOrigemPPST == "CONSOLIDACAOCOMPRAS"
		PESQSTOCKS.menu1.selTodos.config("Sel. Todos", myPath + "\imagens\icons\unchecked_w.png", "uf_pesqstocks_selTodos with .t.","")
	ENDIF
	
	regua(2)

	uf_pesqstocks_grid_setFocus()
ENDFUNC 


**
FUNCTION uf_pesqstocks_imprimir
	LOCAL lcSQL, lcTop, lcRef, lcFamilia, lcLab, lcMarca, lcDci, lcStock, lcGenerico,lcCNPEM, lcSiteNr, lcServMarc,lcSite,lcT4  
	LOCAL lcOperadorLogico, lcInactivos, lcServicos, lcRecurso,lcAtendimento, lcFornecedor, lcClass2, lcTipoProduto, lcLocalizacao, lcTxIva
	STORE "" TO lcSQL, lcRef, lcFamilia, lcLab, lcMarca, lcDci, lcCNPEM, lcRecurso,lcSite ,lcFornecedor, lcClass2, lcTipoProduto, lcLocalizacao
	STORE 0 TO lcTop, lcStock, lcInactivos, lcGenerico, lcServicos, lcSiteNr,lcT4
	STORE .f. TO lcAtendimento, lcServMarc 

	 DO CASE
		CASE !TYPE("PESQSTOCKS") == "U" 
			IF !uf_gerais_validaPermUser(ch_userno, ch_grupo,'Relat�rios - Painel')
				uf_perguntalt_chama("O SEU PERFIL N�O TEM PERMISS�ES PARA ACEDER A ESTA FUNCIONALIDADE.","OK","",48)
				RETURN .f.
			ENDIF
 	ENDCASE
	
	LOCAL lcObj
	IF pesqstocks.pageframe1.activePage == 1 && n�o touch
		lcObj = "pesqstocks.pageframe1.page1"
	ELSE
		lcObj = "pesqstocks.pageframe1.page2"
	ENDIF

	&& Parametros de pesquisa
	lcTop 			= &lcObj..topo.value
	lcRef 			= ALLTRIM(&lcObj..ref.value)
	lcFamilia 		= ALLTRIM(&lcObj..familia.value)
	lcLab			= ALLTRIM(&lcObj..lab.value)
	lcMarca			= ALLTRIM(&lcObj..marca.value)
	lcDci			= ALLTRIM(&lcObj..dci.value)
	lcFornecedor	= ALLTRIM(&lcObj..fornecedor.value)
	lcClass2		= ALLTRIM(&lcObj..class2.value)
	lcTipoProduto	= ALLTRIM(&lcObj..tipoProduto.value)
	lcDiasMov		= ALLTRIM(&lcObj..SemMovimento.value)
	lcServicos		= 0 
	lcLocalizacao	= ALLTRIM(&lcObj..txtLocalizacao.value)
	lcTxIva			= &lcObj..txiva.value
	lcTxIva = IIF(EMPTY(lcTxIva),-1,ROUND(lcTxIva,0))
		
	IF UPPER(ALLTRIM(&lcObj..tipo.value)) == "SERVICOS"
		lcServicos	= 1 
	ENDIF 
	IF UPPER(ALLTRIM(&lcObj..tipo.value)) == "PRODUTOS"
		lcServicos	= 2		
	ENDIF 
	lcAtendimento		= IIF(myOrigemPPST=="ATENDIMENTO" or myOrigemPPST=="RESERVAS",.t.,.f.)
	
	DO CASE 
		CASE myOrigemPPST == "SERIESSERV";
			OR myOrigemPPST == "MARCACOES_EDIT";
			OR myOrigemPPST == "FACTURACAOENTIDADES_UTENTES";
			OR myOrigemPPST == "FACTURACAOASSOCIADOS_UTENTES";
			OR myOrigemPPST == "ENTIDADECOMPARTICIPADORA";
			OR myOrigemPPST == "CONVENCAO";			
			OR myOrigemPPST == "MARCACOES";
			OR myOrigemPPST == "MARCACOES_COMP";
			OR myOrigemPPST == "HONORARIOS"
			
		lcServMarc = .t.	
	ENDCASE 
	
	&& quantidade de stock
	lcOperadorLogico	= IIF(ALLTRIM(&lcObj..OperadorLogico.value)==">", "maior", IIF(ALLTRIM(&lcObj..OperadorLogico.value)=="<", "menor", "igual"))
			
	&& Valida Stock
	If !empty(&lcObj..stock.value)
		IF TYPE(&lcObj..stock.value) == "N"
			lcStock = &lcObj..stock.value
		ELSE
			lcStock = 0
		ENDIF
	ELSE
		lcStock = -999999
	ENDIF
	
	
	&& Valida DiasMov
	If !empty(&lcObj..SemMovimento.value)
		IF TYPE(&lcObj..SemMovimento.value) == "N"
			lcDiasMov = &lcObj..SemMovimento.value
		ENDIF
	ELSE
		lcDiasMov = 0
	ENDIF
	
	lcInactivos		= IIF(&lcObj..chkinactivos.tag == "true", 1, 0)
	
	IF pesqstocks.pageframe1.activePage == 2
		lcGenerico		= -1
		lcParafarmacia  = 0
		lcComFicha		= 1
		lcT5			= 0
		lcT4			= 0
	ELSE
		lcGenerico		= IIF(ALLTRIM(&lcObj..genericos.value)=="", -1, IIF(ALLTRIM(&lcObj..genericos.value)=="Gen�ricos",1,0))
		lcParafarmacia 	= IIF (&lcObj..chkParafarmacia.tag == "true", 1,0)
		lcComFicha		= IIF(&lcObj..chkComFicha.tag == "true", 1, 0)
		lcT5			= IIF(&lcObj..chkT5.tag == "true", 1, 0)
		lcT4			= IIF(&lcObj..chkT4.tag == "true", 1, 0)
	ENDIF 
	
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		&top=<<lcTop>>&artigo=<<lcRef>>&familia=<<lcFamilia>>&lab=<<lcLab>>&marca=<<lcMarca>>&dci=<<lcDci>>&servicos=<<ASTR(lcServicos)>>&operadorLogico=<<lcOperadorLogico>>&stock=<<lcStock>>&generico=<<ALLTRIM(STR(lcGenerico))>>&inactivo=<<IIF(lcInactivos==0, 'false', 'true')>>&site=<<mysite>>&armazem=<<myArmazem>>&servMarc=<<IIF(lcServMarc,'true','false')>>&atendimento=<<IIF(lcAtendimento,'true','false')>>&recurso=<<lcRecurso>>&site_nr=<<mySite_nr>>&parafarmacia=<<IIF(lcParafarmacia==0, 'false', 'true')>>&comFicha=<<IIF(lcComFicha==0, 'false', 'true')>>&t5=<<IIF(lcT5==0,'false','true')>>&t4=<<IIF(lcT4==0,'false','true')>>&fornecedor=<<lcFornecedor>>&class2=<<lcClass2>>&tipoProduto=<<lcTipoProduto>>&diassmov=<<lcDiasMov>>&localizacao=<<lcLocalizacao>>&txIva=<<lcTxIva>>
	ENDTEXT

	SELECT uCrsPesqST
	GO TOP 

		IF RECCOUNT("uCrsPesqST") > 0
			uf_gerais_chamaReport("Listagem_stocks_pesquisa", lcSql)	
		ELSE
			uf_perguntalt_chama("N�o existem dados para Impress�o. Por favor verifique. Obrigado.","OK","",16)
		ENDIF
ENDFUNC 


**
FUNCTION uf_pesqstocks_sair
	LPARAMETERS nBool

	IF !nBool AND myOrigemPPST == "STOCKS_ALTERACOES" AND uf_pesqstocks_validaAlteracao()
		
		IF !uf_perguntalt_chama("VAI CANCELAR AS ALTERA��ES, TEM A CERTEZA QUE PRETENDE CONTINUAR?","Sim","N�o")
			RETURN .F.
		ENDIF
	ENDIF
			
	IF myOrigemPPST == "ATENDIMENTO"
		&& Aplicar comparticipa��o
		SELECT fi
		lcPosFi = RECNO("fi")
		SELECT uCrsAtendCL
		IF uCrsAtendCL.no != 200
			IF !EMPTY(ALLTRIM(uCrsAtendCL.codpla))
				IF uf_gerais_getParameter("ADM0000000192","BOOL")
					uf_atendimento_aplicaPlanoAuto()
				ENDIF 
			ENDIF
		ENDIF
		SELECT fi
		TRY 
			GO lcPosFi
		CATCH
			GO bottom
		ENDTRY
		uf_atendimento_aplicaTabCompartAddLine()
		atendimento.grdAtend.setfocus
	ENDIF
	
	IF myOrigemPPST == "CONSOLIDACAOCOMPRAS"
		uf_pesqstocks_sel_to_consolidacaoCompras()
	ENDIF
	
	IF USED("uCrsPesqST_Sel")
		fecha("uCrsPesqST_Sel")
	ENDIF 
	IF USED("ucrsEmpresasSel")
		fecha("ucrsEmpresasSel")
	ENDIF 

**	IF USED("ucrsConfEmpresas")
**		fecha("ucrsConfEmpresas")
**	ENDIF 
	
	IF USED("uCrsPesqMulti")
		fecha("uCrsPesqMulti")
	ENDIF 	
	IF USED("ucrsConfEmpresasAux")
		fecha("ucrsConfEmpresasAux")
	ENDIF 
	
	** release vari�veis publicas
	RELEASE myOrigemPPST
	RELEASE myOrigemRefDesignPPST
	RELEASE myOrderPesquisarSt
	RELEASE myHideMultiSel
	
	RELEASE MyclassNivel1
	RELEASE MyclassNivel2
	RELEASE MyclassNivel3
	RELEASE MyInformacao 
	
	&& fecha o ecra
	pesqstocks.hide
	pesqstocks.release
ENDFUNC

FUNCTION uf_pesqstocks_sair_semconf
	LPARAMETERS nBool
	
	IF myOrigemPPST == "ATENDIMENTO"
		&& Aplicar comparticipa��o
		SELECT fi
		lcPosFi = RECNO("fi")
		SELECT uCrsAtendCL
		IF uCrsAtendCL.no != 200
			IF !EMPTY(ALLTRIM(uCrsAtendCL.codpla))
				IF uf_gerais_getParameter("ADM0000000192","BOOL")
					uf_atendimento_aplicaPlanoAuto()
				ENDIF 
			ENDIF
		ENDIF
		SELECT fi
		TRY 
			GO lcPosFi
		CATCH
			GO bottom
		ENDTRY
		atendimento.grdAtend.setfocus
	ENDIF
	
	IF USED("ucrsConfEmpresas")
		fecha("ucrsConfEmpresas")
	ENDIF 
	
	IF USED("uCrsPesqMulti")
		fecha("uCrsPesqMulti")
	ENDIF 
	
	IF USED("ucrsConfEmpresasAux")
		fecha("ucrsConfEmpresasAux")
	ENDIF 


	IF USED("uCrsPesqST_Sel")
		fecha("uCrsPesqST_Sel")
	ENDIF 
	** release vari�veis publicas
	RELEASE myOrigemPPST
	RELEASE myOrigemRefDesignPPST
	RELEASE myOrderPesquisarSt
	RELEASE myHideMultiSel
	
	RELEASE MyclassNivel1
	RELEASE MyclassNivel2
	RELEASE MyclassNivel3
	RELEASE MyInformacao 
	
	&& fecha o ecra
	pesqstocks.hide
	pesqstocks.release
ENDFUNC


**
FUNCTION uf_pesqstocks_sel
	LOCAL lcRefs, lcId , lcPos, lcLordem
	STORE 0 TO lcRefs
	SELECT uCrspesqst
	DO CASE
	
		CASE myOrigemPPST == "SERVFACTUT"
			SELECT ucrsServicosFaturacao
			LOCATE FOR ucrsServicosFaturacao.ref = UCRSPESQST.ref
			IF FOUND()
				uf_perguntalt_chama("J� tem esta refer�ncia selecionada para fatura��o autom�tica." + CHR(13) + "Por favor elimine a j� existente ou edite a mesma.","OK","",48)
				RETURN .f.
			ENDIF
		

			SELECT ucrsServicosFaturacao
			go bottom
			APPEND BLANK
			Replace ucrsServicosFaturacao.ref 	 	WITH UCRSPESQST.ref
			Replace ucrsServicosFaturacao.design 	WITH UCRSPESQST.design
			Replace ucrsServicosFaturacao.fatura 	WITH .t.
			Replace ucrsServicosFaturacao.data 		WITH DATE()
			Replace ucrsServicosFaturacao.preco		WITH UCRSPESQST.epv1
			replace ucrsServicosFaturacao.qtt 		WITH 1

			
			Utentes.Pageframe1.page4.GridServFat.Refresh
		
		CASE myOrigemPPST == "ENTIDADECOMPARTICIPADORA"
			lcId = uf_gerais_getId()
			SELECT UCRSPESQST
			SELECT ucrsUtComparticipacoes
			APPEND BLANK
			Replace ucrsUtComparticipacoes.id 		WITH lcId
			Replace ucrsUtComparticipacoes.utstamp  WITH cl.utstamp
			Replace ucrsUtComparticipacoes.ref 		WITH UCRSPESQST.ref
			Replace ucrsUtComparticipacoes.design 	WITH UCRSPESQST.design

			**
			Replace ucrsUtComparticipacoes.site WITH mySite
			
			** Valida Se tem apenas uma Conven��o apara a entidade, caso tenha aplica
			TEXT TO lcSQL NOSHOW TEXTMERGE
				select 
					cpt_conv.id
					,cpt_conv.descr
					,cpt_conv.id_entidade
					,isnull(B_utentes.nome,'') as entidade
				from 
					cpt_conv (nolock)
					left join B_utentes (nolock) on cpt_conv.id_entidade = B_utentes.no and B_utentes.entCompart = 1
				Where
					B_utentes.no = <<cl.no>>
				Order by
					cpt_conv.descr
			ENDTEXT 
			IF uf_gerais_actGrelha("","ucrsTempConvencao",lcSQL)
				IF RECCOUNT("ucrsTempConvencao") == 1
					SELECT ucrsTempConvencao
					Replace ucrsUtComparticipacoes.convencao WITH ucrsTempConvencao.descr
					Replace ucrsUtComparticipacoes.id_cpt_conv WITH ucrsTempConvencao.id
				ENDIF 		
			ENDIF 
			**
			
			Utentes.Pageframe1.page7.Pageframe1.Page2.GridPesq.Refresh
		
		CASE myOrigemPPST == "CONVENCAO"
			
			lcId = uf_gerais_getId()
			SELECT UCRSPESQST
			SELECT uCrsConvencoesLin
			APPEND BLANK
			Replace uCrsConvencoesLin.id WITH lcId 
			Replace uCrsConvencoesLin.ref WITH UCRSPESQST.ref
			Replace uCrsConvencoesLin.design WITH UCRSPESQST.design
			Replace uCrsConvencoesLin.pvp WITH UCRSPESQST.epv1
			Replace uCrsConvencoesLin.especialidade WITH UCRSPESQST.especialidade 

			convencoes.PageFrame1.Page1.gridPesq.Refresh
			
		CASE myOrigemPPST == "CONSUMO"
			
			Select UCRSPESQST
			Select ucrsSeriesServ
			Select ucrsServicosSerie
			
			Select uCrsSeriesConsumo
			APPEND BLANK
				
			Replace uCrsSeriesConsumo.seriestamp WITH ucrsSeriesServ.seriestamp
			Replace uCrsSeriesConsumo.reforiginal WITH ucrsServicosSerie.ref
			Replace uCrsSeriesConsumo.ref WITH UCRSPESQST.ref
			Replace uCrsSeriesConsumo.design WITH UCRSPESQST.design 
			Replace uCrsSeriesConsumo.qtt WITH 1
			
						
			PLANEAMENTOMR.ContainerDetalhe.Pageframe1.Page3.GridConsumos.Refresh	
			
		CASE myOrigemPPST == "CONSUMO_MR"
		
			Select ucrsMarcacoesDia
			lcMrstamp = ALLTRIM(ucrsMarcacoesDia.mrstamp)
			lcconsumomrstamp = uf_gerais_getId(1)
			
			Select ucrsMrConsumos
			APPEND BLANK
						
			SELECT ucrsMrConsumos
			Replace ucrsMrConsumos.ref WITH UCRSPESQST.ref
			Replace ucrsMrConsumos.design WITH UCRSPESQST.design 
			Replace ucrsMrConsumos.duracao WITH UCRSPESQST.u_duracao
			Replace ucrsMrConsumos.qtt WITH 1
			Replace ucrsMrConsumos.consumomrstamp WITH lcconsumomrstamp
			Replace ucrsMrConsumos.mrstamp WITH lcMrstamp 

			marcacoes.pageFrame1.page1.container1.ContainerDetalhes.pageFrame1.Page4.Refresh	
			
		CASE myOrigemPPST == "CONSUMO_STOCKS"
					
			Select uCrsStSt
			APPEND BLANK
						
			SELECT uCrsStSt
			Replace uCrsStSt.ref WITH UCRSPESQST.ref
			Replace uCrsStSt.design WITH UCRSPESQST.design 
			Replace uCrsStSt.duracao WITH UCRSPESQST.u_duracao
			Replace uCrsStSt.qtt WITH 1
			Replace uCrsStSt.reforiginal WITH st.ref 
			
			Stocks.Pageframe1.Page5.GridServicos.Refresh

		CASE myOrigemPPST == "COMPONENTE_STOCKS"

			IF ALLTRIM(UCRSPESQST.ref) == ALLTRIM(st.ref)
				uf_perguntalt_chama("Um produto n�o pode ser composto e componente ao mesmo tempo.","OK","",32)
				RETURN .f.			
			ENDIF 
			
			Select uCrsComponentes
			APPEND BLANK
						
			SELECT uCrsComponentes
			Replace uCrsComponentes.ref WITH UCRSPESQST.ref
			Replace uCrsComponentes.qtt WITH 1
			Replace uCrsComponentes.ref_st WITH st.ref 
			
			stocks.pageframe1.page3.gridComponentes.Refresh
			
		CASE myOrigemPPST == "MARCACOES_EDIT"
			SELECT UCRSPESQST
			SELECT ucrsMarcacoesDia
			Replace ucrsMarcacoesDia.ref WITH UCRSPESQST.ref
			Replace ucrsMarcacoesDia.design WITH UCRSPESQST.design
			
			Marcacoes.refresh

		CASE myOrigemPPST == "MARCACOES"
		
			LOCAL lcservmrstamp, lcMaxOrdemServMarc
			
			lcservmrstamp  = uf_gerais_stamp()
				
			Select UcrsMarcacoesDia
			**
			Select uCrsServMarcacao
			CALCULATE MAX(ordem) TO lcMaxOrdemServMarc
			
			**
			Select uCrsServMarcacao
			APPEND BLANK
			Replace uCrsServMarcacao.ref WITH UCRSPESQST.ref
			Replace uCrsServMarcacao.design WITH UCRSPESQST.design
			Replace uCrsServMarcacao.ordem WITH IIF(ISNULL(lcMaxOrdemServMarc),1,lcMaxOrdemServMarc+1)
			
			Replace uCrsServMarcacao.servmrstamp WITH lcservmrstamp  
			Replace uCrsServMarcacao.mrstamp WITH UcrsMarcacoesDia.mrstamp
			Replace uCrsServMarcacao.duracao WITH UCRSPESQST.u_duracao
			Replace uCrsServMarcacao.qtt WITH 1
			Replace uCrsServMarcacao.dataInicio WITH UcrsMarcacoesDia.data
			Replace uCrsServMarcacao.dataFim WITH UcrsMarcacoesDia.dataFim
			Replace uCrsServMarcacao.hinicio WITH UcrsMarcacoesDia.hinicio
			Replace uCrsServMarcacao.hfim WITH UcrsMarcacoesDia.hfim 
			Replace uCrsServMarcacao.compart WITH 0
			Replace uCrsServMarcacao.pvp WITH UCRSPESQST.epv1
			Replace uCrsServMarcacao.total WITH UCRSPESQST.epv1
		
		CASE myOrigemPPST == "MARCACOES_COMP"
			
			LOCAL lcservmrstamp1 
			
			** Verifica se a referencia encolhida existe na lista de Servi�os
			Select uCrsServMarcacao
			GO TOP 
			Select uCrsServMarcacao
			LOCATE FOR ALLTRIM(uCrsServMarcacao.ref) == ALLTRIM(UCRSPESQST.ref)
			IF FOUND()	
				lcservmrstamp1 = uCrsServMarcacao.servmrstamp
			ELSE
				uf_perguntalt_chama("O servi�o selecionado n�o est� na lista de servi�os desta marca��o. Adicione primero o servi�o.","OK","", 48)
				RETURN .f.
			ENDIF 		
		
			LOCAL lcservcompmrstamp, lcMaxOrdemServMarc
			lcservcompmrstamp  = uf_gerais_stamp()
				
			Select UcrsMarcacoesDia
			**
			Select uCrsCompServMarcacao
			CALCULATE MAX(ordem) TO lcMaxOrdemServMarc
									
			**
			Select uCrsCompServMarcacao
			APPEND BLANK
			Replace uCrsCompServMarcacao.servcompmrstamp WITH lcservcompmrstamp  
			Replace uCrsCompServMarcacao.mrstamp WITH UcrsMarcacoesDia.mrstamp
			Replace uCrsCompServMarcacao.servmrstamp WITH lcservmrstamp1
			Replace uCrsCompServMarcacao.ref WITH UCRSPESQST.ref
			Replace uCrsCompServMarcacao.design  WITH UCRSPESQST.design
			Replace uCrsCompServMarcacao.pvp WITH UCRSPESQST.epv1
			Replace uCrsCompServMarcacao.Total WITH UCRSPESQST.epv1
			Replace uCrsCompServMarcacao.valor WITH 0
			Replace uCrsCompServMarcacao.ordem WITH IIF(ISNULL(lcMaxOrdemServMarc),1,lcMaxOrdemServMarc+1)

		CASE myOrigemPPST == "FACTURACAOENTIDADES_UTENTES"
			SELECT UCRSPESQST
			FACTENTIDADESCLINICA.servico.value = ALLTRIM(UCRSPESQST.ref)
			FACTENTIDADESCLINICA.servico.refresh
		
		CASE myOrigemPPST == "FACTURACAOASSOCIADOS_UTENTES"
			SELECT UCRSPESQST
			FATASSOCIADOS.servico.value = ALLTRIM(UCRSPESQST.ref)
			FATASSOCIADOS.servico.refresh
		
		CASE myOrigemPPST == "HONORARIOS"
			SELECT UCRSPESQST
			IMPORTHONORARIOS.servico.value = ALLTRIM(UCRSPESQST.ref)
			IMPORTHONORARIOS.servico.refresh
			
		CASE myOrigemPPST == "STOCKS_ALTERACOES"
			RETURN .t.
		
		CASE myOrigemPPST == "INVENTARIO"
			RETURN .t.
			
		CASE myOrigemPPST == "STOCKS"
			SELECT UCRSPESQST
			IF EMPTY(UCRSPESQST.temFicha)
				uf_dicionario_chama(uCrsPesqST.ref)
			ELSE 	
				uf_stocks_chama(UCRSPESQST.REF)
			ENDIF 

		CASE myOrigemPPST == "VACINACAO"
			IF USED("UCRSVACINACAO")
				SELECT UCRSVACINACAO
				REPLACE DESCRICAO WITH ALLTRIM(UCRSPESQST.DESIGN)
				REPLACE comercial_id WITH ALLTRIM(UCRSPESQST.ref)
				SET CENTURY ON
				REPLACE UCRSVACINACAO.validade WITH CTOT(UCRSPESQST.validade)

				IF TYPE("myCallVaccineService") <> "U"
					myCallVaccineService = .F.
				ENDIF
		
				VACINACAO.REFRESH	
			ENDIF	
		
		CASE myOrigemPPST == "ATENDIMENTO"
			
			PUBLIC myEscolheuProdutoDem
			myEscolheuProdutoDem = .t.
		
			LOCAL lcPosFi
			STORE 0 TO lcPosFI
			

			
			IF pesqstocks.grdPesq.columns(1).check1.value
				
				**
				IF !uf_pesqstocks_validaCriacaoFicha()
					RETURN .f.
				ENDIF 
				**
				SELECT uCrsPesqST
				IF uCrsPesqST.qttcli > 0
					IF (uCrsPesqST.stock - uCrsPesqST.qttcli) <= 0
						IF !uf_perguntalt_chama("Existe uma reserva aberta com quantidade " + ALLTRIM(STR(uCrsPesqST.qttcli)) + " para o produto que seleccionou." + CHR(13) + "Tem a certeza que pretende adicion�-lo ao atendimento?","Sim","N�o")						
							RETURN .f.
						ELSE
							If uf_gerais_getParameter('ADM0000000066', 'BOOL')
								** pedir password **
								PUBLIC cval
								STORE '' TO cval
								
								uf_tecladoalpha_chama("cval", "INTRODUZA PASSWORD PARA PERMITIR SELECCIONAR PRODUTO RESERVADO:", .t., .f., 0)
								
								If empty(cval)
									uf_perguntalt_chama("DEVE INTRODUZIR A PASSWORD.","OK","",64)
									RELEASE cval
									RETURN .f.		
								ELSE
									IF !(UPPER(ALLTRIM(cval))==UPPER(ALLTRIM(uf_gerais_getParameter('ADM0000000066', 'TEXT'))))
										uf_perguntalt_chama("A PASSWORD N�O EST� CORRECTA.","OK","",64)	
										RELEASE cval
										RETURN .F.
									ELSE
										RELEASE cval
									ENDIF 
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF 
				** DEM, encontrou referencia no atendimento para validar, n�o acrescenta linha - n�o sei o que isto faz temp comment
				SELECT Fi
				**IF !EMPTY(fi.dem) AND atendimento.tmrConsultaDEM.enabled == .f.	
				**valida se nao � Dem, nem vale

				IF !EMPTY(fi.dem) AND atendimento.tmrConsultaDEM.enabled == .f.  AND !EMPTY(fi.stns) AND !EMPTY(fi.epromo) 
				
					LOCAL lcDemConsultada
					STORE .t. TO lcDemConsultada			
					
					lcDemConsultada=uf_atendimento_validaSeDemConsultada()
					if(lcDemConsultada==.f.)
						uf_atendimento_novaVenda(.t.)
					ENDIF
	
				ENDIF 				
				
				uf_atendimento_AdicionaLinhaFi(.F.,.F.) &&(n�o funcionava com multiselec��o)
				**uf_atendimento_AdicionaLinhaFi(.F.,.T.)
								
				SELECT fi
				lcPosFi = RECNO("fi")
				
				replace fi.ref with UCRSPESQST.ref
				
				IF uf_gerais_getParameter_site('ADM0000000060', 'BOOL', mySite)
					replace fi.qtt WITH UCRSPESQST.qttembal
				ENDIF 
				
				uf_atendimento_actRef()
				&& Adiciona linhas para a comparticipa��o manual			
				SELECT fi 
				IF UsaManCompart 
					uf_atendimento_AdicionaLinhafimancompart()
				ENDIF 
				
				SELECT fi
				TRY 
					GO lcPosFi
				CATCH
					GO bottom
				ENDTRY

				uf_atendimento_eventoFi()
				SELECT fi		
				TRY 
					GO lcPosFi
				CATCH
					GO bottom
				ENDTRY
				
				&& actualiza o ecra
				uf_atendimento_dadosST()
				SELECT fi
				TRY 
					GO lcPosFi
				CATCH
					GO BOTTOM
				ENDTRY

				&& Adiciona registo � tabela de caixas para registar n� de s�rie
				SELECT UCRSPESQST
				IF UCRSPESQST.dispositivo_seguranca
					uf_insert_fi_trans_info_seminfo()
				ENDIF 
				
				&&verifica se produto pode ser comparticipado por programas de apoio especiais (ex: betmiga)
				LOCAL lcInfoPlano
				STORE '' TO lcInfoPlano
				
				lcInfoPlano = uf_atendimento_compApoioEspecial()
				
				IF !EMPTY(lcInfoPlano) AND (ALLTRIM(fi.ref)!='5681903' AND ALLTRIM(fi.ref)!='5771050')
					uf_perguntalt_chama("Aten��o, este produto pode ser comparticipado ao abrigo do programa de apoio especial: " + lcInfoPlano + ".","OK","",64)			
				ENDIF
							
				&& verifica descontos
				IF uf_gerais_getParameter("ADM0000000074","BOOL")
					SELECT UCRSPESQST
					IF UCRSPESQST.mfornec > 0 &&percentagem
						uf_Atendimento_alteraDesc(.t., UCRSPESQST.mfornec, UCRSPESQST.dataFimPromo, UCRSPESQST.dataIniPromo)
					ELSE
						IF UCRSPESQST.mfornec2 > 0 &&valor
							uf_atendimento_alteraDescVal(.t., UCRSPESQST.mfornec2, UCRSPESQST.dataFimPromo, UCRSPESQST.dataIniPromo)
						ENDIF 
					ENDIF
				ENDIF 

				SELECT fi
				TRY 
					GO lcPosFi
				CATCH
					GO BOTTOM
				ENDTRY

				IF TYPE("ATENDIMENTO") != "U"
					** Inser��o comparticipa��o Novo Nordisk
					IF InsNNordisk = .T. 
						uf_atendimento_compart_nnordisk()
						
						lcPos=RECNO("fi")
						lcLordem = fi.lordem
						lcLordem = VAL(LEFT(ALLTRIM(STR(lcLordem)),2)+'0000')
						SELECT fi
						GO TOP 
						SCAN
							IF fi.lordem = lcLordem 
								uf_atendimento_aplicaPlanoND(fi.fistamp)
								
								SELECT ucrsatendcomp
								SELECT fi
								REPLACE fi.design WITH '.[' + ALLTRIM(ucrsatendcomp.codigo) + '][' + ALLTRIM(ucrsatendcomp.cptorgabrev) + '] Receita:'+" -"
								replace fi.tipor WITH 'RM'
							ENDIF 
						ENDSCAN 
											
						SELECT fi
						TRY
							GO lcPos
						CATCH
						ENDTRY					
					ENDIF 
					InsNNordisk = .F.
				ENDIF 
				
				SELECT fi
				TRY 
					GO lcPosFi
				CATCH
					GO BOTTOM
				ENDTRY
				
				atendimento.txtTotalVendas.setfocus
				uf_atendimento_infoTotais()
				
				SELECT fi
				TRY 
					GO lcPosFi
				CATCH
					GO BOTTOM
				ENDTRY
				
				pesqStocks.show()
			ELSE
				RETURN .f.	
			ENDIF
		
		CASE myOrigemPPST == "FACTURACAO"
			IF pesqstocks.grdPesq.columns(1).check1.value
				
				PUBLIC myEscolheuProdutoDem
				myEscolheuProdutoDem = .t.

				**
				IF !uf_pesqstocks_validaCriacaoFicha()
					RETURN .f.
				ENDIF 

				SELECT fi
				IF !EMPTY(fi.ref)
					uf_atendimento_AdicionaLinhaFi(.T.,.F.)
				ENDIF 
				
				SELECT fi
				replace fi.ref with UCRSPESQST.ref			
				uf_facturacao_eventoRefFi()
			ENDIF
		
		CASE myOrigemPPST == "PESQCLIENTES"
		
			LOCAL lcvalidaExiste 
			lcvalidaExiste = .f.
	
			IF USED("UCRSPRODUTOSCLI")

				SELECT UCRSPESQST
				IF UCRSPESQST.SEL == .t.
				
					IF lcvalidaExiste == .f.
						SELECT UCRSPRODUTOSCLI
						GO BOTTOM
						APPEND BLANK
						REPLACE UCRSPRODUTOSCLI.REF			WITH ALLTRIM(UCRSPESQST.REF)
						REPLACE UCRSPRODUTOSCLI.DESIGN 		WITH ALLTRIM(UCRSPESQST.DESIGN)
						REPLACE UCRSPRODUTOSCLI.FAMINOME 	WITH ALLTRIM(UCRSPESQST.FAMINOME)
					ENDIF
				ELSE
					SELECT UCRSPRODUTOSCLI
					SCAN FOR ALLTRIM(UCRSPRODUTOSCLI.REF) == ALLTRIM(UCRSPESQST.REF)
						DELETE
					ENDSCAN
					SELECT UCRSPRODUTOSCLI
					GO TOP
				ENDIF

				PRODSELCLI.REFRESH
			
			ENDIF	

		CASE myOrigemPPST == "DOCUMENTOS"
			
			IF EMPTY(pesqstocks.grdPesq.columns(1).check1.value)
				RETURN .f.
			ENDIF
			
			IF myDocAlteracao == .f. AND myDocIntroducao == .f.
				uf_perguntalt_chama("PARA UTILIZAR ESTA OP��O O ECR� DE DOCUMENTOS DEVE ESTAR EM MODO DE EDI��O.","OK","", 48)
				RETURN .f.
			ENDIF
	
			**
			IF !uf_pesqstocks_validaCriacaoFicha()
				RETURN .f.
			ENDIF 

			**		
			documentos.lockscreen =  .t.
			documentos.PageFrame1.Page1.gridDoc.SetFocus
			
			DO CASE
				CASE mytipoDoc = "FO"
					
					SELECT fn
					IF !EMPTY(fn.ref)
						uf_documentos_criaNovaLinha(.t.,.t.)
					ELSE 
						uf_documentos_editarTemp(fn.fnstamp)
					ENDIF 

					** inserir refer�ncia
					SELECT fn
					Select UCRSPESQST
					Replace fn.ref WITH ALLTRIM(UCRSPESQST.ref)
					
					IF UCRSPESQST.qttcli > 0
						replace fn.u_reserva WITH .t.
					ENDIF 

					uf_documentos_eventoref()
					
					Select CabDoc
					IF !EMPTY(cabDoc.tabIva)
						lcTaxaIva = uf_gerais_getTabelaIva(cabDoc.tabIva,"TAXA")
						Replace fn.tabIva WITH cabDoc.tabIva
						Replace fn.Iva WITH lcTaxaIva
					ENDIF
					
					uf_documentos_recalculaTotaisFN()

				CASE mytipoDoc = "BO"

*!*						LOCAL lcValUnitCaixa
*!*						STORE .f. TO lcValUnitCaixa
*!*						SELECT cabdoc 
*!*						IF ALLTRIM(cabdoc.doc)='Imprimir Etiquetas'
*!*							lcValUnitCaixa = .t.
*!*						ENDIF 
						
					SELECT BI
					
					IF !EMPTY(BI.ref)
						uf_documentos_criaNovaLinha(.t.,.t.)
					ELSE 
						uf_documentos_editarTemp(bi.bistamp)
					ENDIF 

					** inserir refer�ncia
					Select UCRSPESQST
					SELECT BI
					Replace bi.ref WITH Alltrim(UCRSPESQST.ref)
					uf_documentos_eventoref()
					IF uf_gerais_getParameter_site('ADM0000000060', 'BOOL', mySite)
						SELECT UCRSPESQST
						SELECT BI
						replace Bi.qtt WITH UCRSPESQST.qttembal
					ENDIF
					
*!*						IF lcValUnitCaixa = .t.
*!*							SELECT bi
*!*							replace bi.edebito WITH ROUND(bi.edebito * bi.qtt,2)
*!*						ENDIF 
					
					Select CabDoc
					IF !EMPTY(cabDoc.tabIva)
						lcTaxaIva = uf_gerais_getTabelaIva(cabDoc.tabIva,"TAXA")
						Replace bi.tabIva WITH cabDoc.tabIva
						Replace bi.Iva WITH lcTaxaIva
					ENDIF
					
					uf_documentos_recalculaTotaisBI()
				OTHERWISE
					**
			ENDCASE
			
			uf_documentos_actualizatotaisCAB()
			documentos.lockscreen =  .f.
			
		CASE myOrigemPPST == "RESERVAS"
			Select UCRSPESQST
			If !(UCRSPESQST.sel)
				uf_reservas_eliminaLinha(UCRSPESQST.ref)
			Else
				uf_reservas_AddProd(UCRSPESQST.ref)
			Endif 	
			
		CASE myOrigemPPST == "ENCAUTOMATICAS"
		
			**
			IF !uf_pesqstocks_validaCriacaoFicha()
				RETURN .f.
			ENDIF 
			
			&& Multiselecao aplica todos os produtos no final
			IF pesqstocks.menu1.multiSel.tag != 'true'
			
				IF Used("crsenc")
					** procura produto **
					PUBLIC MyEncConjunta
					PUBLIC myencGrupo 
					
					**
					LOCAL lcIDSedeGrupo
					lcIDSedeGrupo = ""
					IF USED("uCrsConfiguracaoSedeGrupo")
						IF myencGrupo == .t.
							SELECT uCrsConfiguracaoSedeGrupo
							lcIDSedeGrupo = ALLTRIM(uCrsConfiguracaoSedeGrupo.bdname)
						ENDIF
					ENDIF 
									
					SELECT UCRSPESQST

					TEXT TO lcSQL NOSHOW TEXTMERGE
					 	exec up_enc_autoref <<IIF(MyEncConjunta,0,myArmazem)>>, '<<Alltrim(UCRSPESQST.ref)>>','<<ALLTRIM(lcIDSedeGrupo)>>', <<mysite_nr>>
					ENDTEXT 
				
					IF uf_gerais_actgrelha("", "uCrsAddProdPea",lcSQL)
						IF reccount("uCrsAddProdPea") == 0
							uf_perguntalt_chama("N�O � POSSIVEL ADICIONAR ESTE PRODUTO.","OK","",64)
							RETURN .f.
						ELSE

							** verifica se produto j� existe **
							Local lcCrsencPos, lcValidaAddPea
							lcCrsencPos = 0
							lcValidaAddPea = .f.

							SELECT crsenc
							GO TOP
							SCAN
								IF Alltrim(crsenc.ref)==Alltrim(uCrsAddProdPea.ref) 
									IF !uf_perguntalt_chama("O PRODUTO J� EXISTE NA ENCOMENDA AUTOM�TICA." + CHR(13) + "PRETENDE NAVEGAR PARA A LINHA DO PRODUTO?", "Sim","N�o")
										RETURN .f.
									ELSE
                              encautomaticas.gridPesq.setFocus()
										myOrigemPPST = "ENCAUTOMATICASSAIR"
										EXIT
									ENDIF
								ENDIF
							ENDSCAN
							***************************
							IF myOrigemPPST != "ENCAUTOMATICASSAIR" AND USED("crsenc")
								** adiciona produto **
								Select crsenc
								Append Blank
								replace crsenc.ststamp		With 	Alltrim(uCrsAddProdPea.ststamp)
								replace crsenc.u_fonte		With 	Alltrim(uCrsAddProdPea.u_fonte)
								replace crsenc.ref			With 	Alltrim(uCrsAddProdPea.ref)
								replace crsenc.refb			With 	Alltrim(uCrsAddProdPea.refb)
								replace crsenc.codigo		With 	Alltrim(uCrsAddProdPea.codigo)
								replace crsenc.design		With 	Alltrim(uCrsAddProdPea.design)
								replace crsenc.stock		With 	uCrsAddProdPea.stock
								replace crsenc.stmin		With 	uCrsAddProdPea.stmin
								replace crsenc.stmax		With 	uCrsAddProdPea.stmax
								replace crsenc.ptoenc		With 	uCrsAddProdPea.ptoenc
								replace crsenc.eoq			With 	uCrsAddProdPea.eoq
								replace crsenc.qtbonus		With 	uCrsAddProdPea.qtbonus
								replace crsenc.qttadic		With 	uCrsAddProdPea.qttadic
								replace crsenc.qttfor		With 	uCrsAddProdPea.qttfor
								replace crsenc.qttacin		With 	uCrsAddProdPea.qttacin
								replace crsenc.qttcli		With 	uCrsAddProdPea.qttcli
                                replace crsenc.qttres		With 	uCrsAddProdPea.qttcli
								replace crsenc.fornecedor	With 	Alltrim(uCrsAddProdPea.fornecedor)
								replace crsenc.fornec		With 	uCrsAddProdPea.fornec
                                replace crsenc.fornecabrev	With 	uCrsAddProdPea.fornecabrev
								replace crsenc.fornestab	With 	uCrsAddProdPea.fornestab
								replace crsenc.epcusto		with 	uCrsAddProdPea.epcusto
								replace crsenc.epcpond		With 	uCrsAddProdPea.epcpond
								replace crsenc.epcult		With 	uCrsAddProdPea.epcult
								replace crsenc.tabiva		With 	uCrsAddProdPea.tabiva
								replace crsenc.iva			With 	uCrsAddProdPea.iva
								replace crsenc.cpoc			With 	uCrsAddProdPea.cpoc
								replace crsenc.familia		With 	uCrsAddProdPea.familia
								replace crsenc.faminome		With 	alltrim(uCrsAddProdPea.faminome)
								replace crsenc.u_lab		With 	Alltrim(uCrsAddProdPea.u_lab)
								replace crsenc.conversao	With 	uCrsAddProdPea.conversao
								
								replace crsenc.stockgh		With 	uCrsAddProdPea.stockgh
								replace crsenc.bonusfornec	With 	Alltrim(uCrsAddProdPea.bonusfornec)
								Replace crsenc.pclfornec	WITH 	uCrsAddProdPea.pclfornec
								replace crsenc.esgotado		With 	Alltrim(uCrsAddProdPea.esgotado)

								Replace  crsenc.alertaPCL		WITH 	uCrsAddProdPea.alertaPCL
								Replace  crsenc.alertaBonus		WITH 	uCrsAddProdPea.alertaBonus
								Replace  crsenc.alertaStockGH	WITH 	uCrsAddProdPea.alertaStockGH
								Replace  crsenc.marg4			WITH 	uCrsAddProdPea.marg4
								replace crsenc.enc			WITH  	.f.
								replace crsenc.sel			WITH 	.f.
								TRY
									replace crsenc.marca		WITH uCrsAddProdPea.marca
								CATCH
								ENDTRY
								replace crsenc.prioritario WITH uCrsAddProdPea.prioritario
								replace crsenc.pvp WITH uCrsAddProdPea.pvp
								REPLACE crsenc.obs WITH uCrsAddProdPea.obs
								REPLACE crsenc.u_nota1 WITH uCrsAddProdPea.u_nota1 
								IF TYPE("crsenc.pcusto") <> "U"
									REPLACE crsenc.pcusto WITH uCrsAddProdPea.pclFicha
								ENDIF
								IF TYPE("crsenc.descontoForn") <> "U"
									REPLACE crsenc.descontoForn WITH uCrsAddProdPea.descontoForn
									REPLACE crsenc.descontoFornOri WITH uCrsAddProdPea.descontoForn
								ENDIF
							ENDIF
						ENDIF
						
						IF USED("uCrsAddProdPea")
							Fecha("uCrsAddProdPea")
						ENDIF
						
						if(USED("crsenc"))					
							SELECT crsenc
						endif	
						
						encautomaticas.gridPesq.refresh
					ENDIF
				ENDIF
			ENDIF
		**
		
		CASE myOrigemPPST == "CC_EXCEPCOES"				
			** inserir refer�ncia
			Select UCRSPESQST
			SELECT uCrsFidelExcpt
			Replace uCrsFidelExcpt.ref WITH Alltrim(UCRSPESQST.ref)
			
		CASE myOrigemPPST == "HISTORICO"
			
			** inserir refer�ncia
			Select uCrsHisTrat
			APPEND BLANK
			Replace uCrsHisTrat.ref		WITH UCRSPESQST.ref
			Replace uCrsHisTrat.design	WITH UCRSPESQST.design
			Replace uCrsHisTrat.tipo	WITH "I"
			Replace uCrsHisTrat.dataDoc	WITH date()
			Replace uCrsHisTrat.dtIni	WITH date()
			Replace uCrsHisTrat.dtp		WITH date()
			Replace uCrsHisTrat.qtt		WITH 1
			Replace uCrsHisTrat.dci		WITH UCRSPESQST.dci
			Replace uCrsHisTrat.alterado	WITH  .t.
			
			utentes.pageframe1.page6.containerDetalhes.posologia.setFocus
			utentes.pageframe1.page6.containerDetalhes.refresh
			utentes.refresh
			utentes.pageframe1.page6.grdHist.setfocus
			
			Index On dataDoc Tag dataDoc desc
			GO TOP
			
		CASE myOrigemPPST == "PROMOORIGEM"
			** inserir refer�ncia
			Select uCrsPromoO
			LOCATE FOR ALLTRIM(uCrsPromoO.ref) == Alltrim(UCRSPESQST.ref)
			IF !FOUND()
				Select uCrsPromoO
				APPEND BLANK
				Replace ;
					uCrsPromoO.ref		WITH Alltrim(UCRSPESQST.ref) ;
					uCrsPromoO.design	WITH Alltrim(UCRSPESQST.design) ;
					uCrsPromoO.qtt		WITH 0
				
				promocoes.pageframe1.page3.grdOrigem.refresh
			ENDIF
		
		CASE myOrigemPPST == "PROMODESTINO"
			** inserir refer�ncia
			Select uCrsPromoD
			LOCATE FOR ALLTRIM(uCrsPromoD.ref) == Alltrim(UCRSPESQST.ref)
			IF !FOUND()
				Select uCrsPromoD
				APPEND BLANK
				Replace ;
					uCrsPromoD.ref		WITH Alltrim(UCRSPESQST.ref) ;
					uCrsPromoD.design	WITH Alltrim(UCRSPESQST.design) ;
					uCrsPromoD.qtt		WITH 0
				
				promocoes.pageframe1.page3.grdDestino.refresh
			ENDIF
		
		CASE myOrigemPPST == "PROMOEXCECAO"
			** inserir refer�ncia
			Select uCrsPromoE
			LOCATE FOR ALLTRIM(uCrsPromoE.ref) == Alltrim(UCRSPESQST.ref)
			IF !FOUND()
				Select uCrsPromoE
				APPEND BLANK
				Replace ;
					uCrsPromoE.ref		WITH Alltrim(UCRSPESQST.ref) ;
					uCrsPromoE.design	WITH Alltrim(UCRSPESQST.design)
				
				promocoes.pageframe1.page4.grdExcecoes.refresh
			ENDIF

		CASE myOrigemPPST == "CONSOLIDACAOCOMPRAS"
			uf_pesqstocks_sel_to_consolidacaoCompras()
		
		OTHERWISE
			**	
	ENDCASE
	
	** funcionamento dos atalhos
	pesqstocks.show

	IF !(myOrigemPPST == "STOCKS_ALTERACOES");
		AND !(myOrigemPPST == "PESQCLIENTES") AND !(myOrigemPPST == "RESERVAS");
		AND !(myOrigemPPST == "PROMOORIGEM") AND !(myOrigemPPST == "PROMODESTINO");
		AND !(myOrigemPPST == "PROMOEXCECAO"); 
		AND !(myOrigemPPST == "ENTIDADECOMPARTICIPADORA") AND !(myOrigemPPST == "CONSUMO_MR");
		AND !(myOrigemPPST == "HISTORICO") AND !(myOrigemPPST == "CONSUMO_STOCKS");
		AND !(myOrigemPPST == "CONSUMO"); &&AND !(myOrigemPPST == "SERIESSERV")
		AND !(myOrigemPPST == "SERVFACTUT") AND !(myOrigemPPST == "CONVENCAO") AND !(myOrigemPPST == "CONSOLIDACAOCOMPRAS");
		&& AND	!(myOrigemPPST == "DOCUMENTOS") AND !(myOrigemPPST == "FACTURACAO") AND !(myOrigemPPST == "ENCAUTOMATICAS")
		IF pesqstocks.menu1.multiSel.tag = 'true'
			RETURN .f.
		ELSE
			**
		ENDIF
		
		uf_pesqstocks_sair()
	ELSE
		pesqstocks.show
	ENDIF
		
ENDFUNC


**
FUNCTION uf_pesqstocks_validaCriacaoFicha
	LOCAL lcSQL 
	lcSQL = ''
	
	LOCAL lcPosFi
	
	
	
	** mantem cursor fi se existir
	if(USED("fi"))
		SELECT fi
		lcPosFi = RECNO("fi")
	ENDIF
	

	SELECT uCrspesqst
	
	
	IF !EMPTY(UCRSPESQST.temficha)
		RETURN .t.
	ENDIF
	
	 
	 IF uf_stocks_temReferenciaBloqueada(.t., UCRSPESQST.ref)
        uf_perguntalt_chama("N�O � POSSIVEL CRIAR O PRODUTO COM ESTA REFER�NCIA","OK","",48)
        RETURN .f.
    ENDIF


	IF uf_perguntalt_chama("O Artigo ou Servi�o selecionado n�o tem ficha criada. Pretende criar ficha?","SIM","N�O", 32)

      IF !uf_gerais_checkNewRef(ALLTRIM(UCRSPESQST.ref))
         RETURN .F.
      ENDIF

      IF uf_gerais_getParameter("ADM0000000272","BOOL")
			
			LOCAL lcIdFornecedor
			
			lcIdFornecedor= INT(uf_gerais_getParameter("ADM0000000272","Num"))
			
			IF(lcIdFornecedor>0)
			
				TEXT TO lcSQL NOSHOW TEXTMERGE 
					select 
						nome,no,estab, inactivo
					from 
						fl (nolock) 
					where 
						fl.no = <<lcIdFornecedor>>
                  and fl.estab = 0
				ENDTEXT 

				IF !uf_gerais_actgrelha("", "uCrsTempForn", lcSQL ) 
					uf_perguntalt_chama("ATEN��O: Esse fornecedor preferencial n�o existe","OK","", 48)	

            ELSE
				
               IF(RECCOUNT("uCrsTempForn")>0)

                     IF uCrsTempForn.inactivo
                           uf_perguntalt_chama("ATEN��O: Esse fornecedor preferencial encontra-se inactivo.","OK","", 48)	
                     ENDIF
            
               ENDIF
            ENDIF
					
				fecha("uCrsTempForn")
				
			ENDIF
			
		ENDIF
	
		TEXT TO lcSQL NOSHOW TEXTMERGE
			exec up_stocks_criaSt '<<ALLTRIM(UCRSPESQST.ref)>>', <<mysite_nr>>
		ENDTEXT
		
		IF uf_gerais_actGrelha("", "", lcSQL)
			LOCAL lcRefActual
			lcRefActual = ''
			
			** Respondeu sim e criou a ficha
			SELECT UCRSPESQST
			lcRefActual = ALLTRIM(UCRSPESQST.ref)			
			Replace UCRSPESQST.temFicha WITH .t.
			
			uf_dicionario_guarda_imagem_anexos(lcRefActual ,mysite)	
			
			** mantem cursor fi se existir
			uf_gerais_recoloca_cursor("fi", lcPosFi)

			RETURN .t.
		ELSE 
			** mantem cursor fi se existir
			uf_gerais_recoloca_cursor("fi", lcPosFi)
			
			RETURN .f.
		ENDIF 		
	ENDIF 

	** Respondeu n�o
	SELECT UCRSPESQST
	Replace UCRSPESQST.sel WITH .f.
	
	** mantem cursor fi se existir
	uf_gerais_recoloca_cursor("fi", lcPosFi)			
			
	RETURN .f.		
ENDFUNC 

FUNCTION  uf_gerais_recoloca_cursor
	LPARAMETERS lcCursor, lcPosOrig
	
	if(USED(lcCursor))
		
		SELECT &lcCursor
	    TRY
	        GO lcPosOrig
	    CATCH
	        GO TOP
	    ENDTRY
	ENDIF
			
	
ENDFUNC
	



**
FUNCTION uf_pesqstocks_novaLinha

	DO CASE
		CASE myOrigemPPST == "DOCUMENTOS"
			uf_documentos_criaNovaLinha(.t.,.t.)
			IF  Alltrim(mytipoDoc) == "BO"
				SELECT BI 	
				uf_documentos_editarTemp(bi.bistamp)
			ELSE 
				SELECT FN
		 		uf_documentos_editarTemp(fn.fnstamp)
			ENDIF

		CASE myOrigemPPST == "FACTURACAO"
			IF myFtAlteracao AND LEN(ALLTRIM(td.tiposaft))>1
				RETURN .f. 
			ELSE
				IF facturacao.eventoRef == .f.
					uf_atendimento_AdicionaLinhaFi(.t.,.t.)
					SELECT fi
					GO BOTTOM
				ENDIF
			ENDIF 
		OTHERWISE
			**
	ENDCASE 	
ENDFUNC



FUNCTION  uf_pesqstocks_validaAlteracao
	

	if(VARTYPE(pesqstocks)=='U')
		RETURN .T.
	ENDIF
	
	

	if(VARTYPE(pesqstocks.container2)=='U')
		RETURN .T.
	ENDIF
	
	

	if(VARTYPE(pesqstocks.container2.PageFrame1)=='U')
		RETURN .T.
	ENDIF
	

	if(VARTYPE(pesqstocks.container2.PageFrame1.page1)=='U')
		RETURN .T.
	endif	
	

	WITH pesqstocks.container2.PageFrame1.page1	
			IF EMPTY(ALLTRIM(.txtFornecedor.value)) AND EMPTY(.txtFornec.value) AND EMPTY(ALLTRIM(.txtMarca.value)) ;
					AND EMPTY(ALLTRIM(.txtU_Lab.value)) AND EMPTY(ALLTRIM(.cmbFamilia.value)) ;
					AND EMPTY(.txtstmax.value) AND EMPTY(.txtptoenc.value) ;
					AND EMPTY(.txteoq.value) ;
					AND EMPTY(.txtClassIMS.value) ;
					AND EMPTY(.txtClassFarm.value) ;
					AND pAlterainativo=0 ;
					AND pAlteraimprime=0 ;
					AND EMPTY(.txtmarg1.value) ;
					AND EMPTY(.txtlocal.value) ;
					AND EMPTY(.txtu_local.value) ;
					AND EMPTY(.txtu_local2.value) ;
					AND EMPTY(.txtu_nota1.value) ;
					AND EMPTY(.txtobs.value) ;
                    AND EMPTY(.txtStockIndisp.value) ;                  
                    AND .chkDispWeb.tag = "false" AND .chkDispWebN.tag = "false" ;
                    AND .chkNov.tag = "false" AND .chkNovN.tag = "false" ;
                    AND .chkPortes.tag = "false" AND .chkPortesN.tag = "false" ;
					AND .chkPSCons.tag = "false" AND .chkPSConsN.tag = "false" ; 					
                    AND .chkObs.tag = "false" ;
                    AND .chkLab.tag = "false" ;
                    AND .chkMarca.tag = "false" ;
                    AND .chkIMS.tag = "false" ;
                    AND .chkFam.tag = "false" ;
                    AND .chkForn.tag = "false" ;
                    AND .chkLoc.tag = "false" ;
                    AND .chkLoc2.tag = "false" ;
                    AND .chkLoc3.tag = "false" ;
                    AND .chkNota.tag = "false" ;
                    AND .chkObs.tag = "false" ;
                    AND .chkPimAlter.tag = "false" AND .chkPimAlterN.tag = "false" ;
					AND .chkDescPerc.tag = "false" AND .chkDescVal.tag = "false";
					AND .chkDtIniPromo.tag = "false" AND .chkDtFimPromo.tag = "false"

				RETURN .f.
			ENDIF
	ENDWITH 		
	
	RETURN .t.
ENDFUNC

	
	



** Grava altera��es aos produtos seleccionados na pesquisa 
FUNCTION uf_pesqstocks_gravar
	LOCAL lcActualizaFornecedor, lcSelSites, lcCount, lcFamFamilia
	lcSelSites=''
	DO CASE
		CASE myOrigemPPST == "STOCKS_ALTERACOES"

			** Valida��es
			lcAlter1=IIF(pesqstocks.Container2.Pageframe1.Page1.chkInactivosAlter.tag == "true", 1, 0)
			lcAlter2=IIF(pesqstocks.Pageframe1.Page1.chkInactivos.tag == "true", 1, 0)
	
			IF pesqstocks.Container2.Pageframe1.Page1.chku_impetiqAlterS.tag=="true" AND pesqstocks.Container2.Pageframe1.Page1.chku_impetiqAlterN.tag=="true"
				uf_perguntalt_chama("N�o pode ter a op��o de alterar as etiquetas com SIM e N�O selecionados!","OK","",16)
				RETURN .f.
			ENDIF
			
			IF pesqstocks.Container2.Pageframe1.Page1.chku_impetiqAlterS.tag=="false" AND pesqstocks.Container2.Pageframe1.Page1.chku_impetiqAlterN.tag=="false"
				pAlteraimprime=0
			ENDIF
			
			if(!uf_pesqstocks_validaAlteracao())
				uf_perguntalt_chama("N�O EXISTE NADA A ALTERAR!","OK","",48)
				RETURN .f.
			ENDIF
					
			WITH pesqstocks.container2.PageFrame1.page1
				* apanhar stamps seleccionados
				lcStamps = "''"
				lcRefs = "''"
				SELECT ucrspesqst
				GO TOP
				SCAN
					IF ucrspesqst.sel == .t.
						lcStamps = lcStamps + ", '" + ALLTRIM(ucrspesqst.ststamp)+ "'"
					ENDIF
				ENDSCAN
				
				LOCAL lcFonecedor, lcFornec, lcFornestab, lcMarca, lcLab, lcFamilia, lcFaminome, lcSqlAltGrupo, lcStMax, lcPtoEnc, lcEOQ, lcFamStamp, lcU_impetiq, lcMarg1, lcLocal, lcU_local, lcU_local2, lcU_nota1, lcObs, lcMarg4, lcMarg3 
				LOCAL lcEpv1_final, lcMfornec, lcMfornec2, uv_altDescPerc, uv_altDescVal, lcDtIniPromo, lcDtFimPromo, lcQuestionario
				STORE 0 TO lcMfornec, lcMfornec2
				PUBLIC MyclassNivel1 ,MyclassNivel2, MyclassNivel3, MyclassNivel4, MyInformacao 

				lcActualizaFornecedor = .f.
                uv_altForn      = IIF(.chkForn.tag == "true", .T., .F.)
				IF ALLTRIM(.txtFornecedor.value) == 'Sem Fornecedor Preferencial' OR !EMPTY(ALLTRIM(.txtFornecedor.value)) OR uv_altForn
					lcActualizaFornecedor = .t.
				ENDIF 
										
				IF ALLTRIM(.txtFornecedor.value) == 'Sem Fornecedor Preferencial'
					lcFornecedor	= ''
				ELSE	
					lcFornecedor	= ALLTRIM(.txtFornecedor.value)
				ENDIF
				lcFornec		= IIF(EMPTY(ALLTRIM(.txtFornecedor.value)), "0", ALLTRIM(STR(.txtFornec.value)))
				lcFornestab		= IIF(EMPTY(ALLTRIM(.txtFornecedor.value)), "0", ALLTRIM(STR(.txtFornestab.value)))
               
                
                lcDtIniPromo = IIF(uf_gerais_compStr(.txtDtIni.value, "1900.01.01"), "", ALLTRIM(.txtDtIni.value)) 
                uv_dtIniPromo = IIF(.chkDtIniPromo.tag == "true", .T., .F.)
                                 
                lcDtFimPromo = IIF(uf_gerais_compStr(.txtDtFim.value, "1900.01.01"), "", ALLTRIM(.txtDtFim.value))
                uv_dtFimPromo = IIF(.chkDtFimPromo.tag == "true", .T., .F.)
                
                lcQuestionario = IIF(uf_gerais_compStr(.txtQuestionario.Tag, ""), "", ALLTRIM(.txtQuestionario.Tag))
                uv_Questionario = IIF(.chkQuestionario.tag == "true", .T., .F.)
 
				lcMarca			= ALLTRIM(.txtMarca.value)
                uv_altMarca     = IIF(.chkMarca.tag == "true", .T., .F.)

                uv_altClassIMS  = IIF(.chkIMS.tag == "true", .T., .F.)

				lcLab			= ALLTRIM(.txtU_Lab.value)
                uv_altLab       = IIF(.chkLab.tag == "true", .T., .F.)

				lcFamilia		= ALLTRIM(.cmbFamilia.value)
				lcFaminome		= ALLTRIM(.txtfaminome.value)
                uv_altFamilia   = IIF(.chkFam.tag == "true", .T., .F.)

				lcStMax			= .txtstmax.value
				lcPtoEnc		= .txtptoenc.value
				lcEOQ			= .txteoq.value
				lcInactivoalter	= IIF(.chkInactivosAlter.tag == "true", 1, 0)
                lcInactivoalterN= IIF(.chkInactivosAlterN.tag == "true", 1, 0)
	 	
                IF lcInactivoalter = 1

                    IF uf_gerais_getUmValor("st", "count(*)", "ststamp in (" + ALLTRIM(lcStamps) + ") and st.stock > 0 and st.stns = 0 ") <> 0

                        uf_perguntalt_chama("N�o � poss�vel inactivar pois selecionou artigos com stock." ,"OK","",48)
                        RETURN .F.

                    ENDIF

                ENDIF	 
	
				lcU_impetiq		= IIF(.chku_impetiqAlterS.tag == "true", "0", "1")
				lcMarg1			= .txtmarg1.value
				lcLocal			= ALLTRIM(.txtlocal.value)
                uv_altLoc       = IIF(.chkLoc.tag == "true", .T., .F.)

				lcU_local		= ALLTRIM(.txtu_local.value)
                uv_altLoc2      = IIF(.chkLoc2.tag == "true", .T., .F.)

				lcU_local2		= ALLTRIM(.txtu_local2.value)
                uv_altLoc3      = IIF(.chkLoc3.tag == "true", .T., .F.)

				lcU_nota1		= ALLTRIM(.txtu_nota1.value)
                uv_altNota      = IIF(.chkNota.tag == "true", .T., .F.)

				lcObs			= ALLTRIM(.txtobs.value)
                uv_altObs       = IIF(.chkObs.tag == "true", .T., .F.)
	
                DO CASE 
                    CASE .chkDispWeb.tag == "true"
                        uv_dispOnline = 1

                    CASE .chkDispWebN.tag == "true"
                        uv_dispOnline = 0
                    
                    OTHERWISE
                        uv_dispOnline = -1

                ENDCASE
	 
                DO CASE 
                    CASE .chkNov.tag == "true"
                        uv_novidade = 1

                    CASE .chkNovN.tag == "true"
                        uv_novidade = 0
                    
                    OTHERWISE
                        uv_novidade = -1

                ENDCASE
	 	
                DO CASE 
                    CASE .chkPortes.tag == "true"
                        uv_portesGratis = 1

                    CASE .chkPortesN.tag == "true"
                        uv_portesGratis = 0
                    
                    OTHERWISE
                        uv_portesGratis = -1

                ENDCASE

				DO CASE
					CASE .chkPSCons.tag == "true"
						uv_psobConsulta = 1

					CASE .chkPSConsN.tag == "true"
						uv_psobConsulta = 0

					OTHERWISE
						uv_psobConsulta = -1

				ENDCASE
	 
	            DO CASE
	               CASE .chkPimAlter.tag == "true"
	                  uv_PIM = 1
	               CASE .chkPimAlterN.tag == "true"
	                  uv_PIM = 0
	               OTHERWISE
	                  uv_pim = -1

	            ENDCASE

				DO CASE
					CASE uf_gerais_compStr(.chkDescPerc.tag, "true")
						lcMfornec = .txtDescPerc.value
						uv_altDescPerc = .T.
						lcEpv1_final = 'ROUND((st.epv1 - ((st.epv1 * ' + ASTR(lcMfornec, 6, 2) + ')/100)),2)'
					CASE uf_gerais_compStr(.chkDescVal.tag, "true")
						lcMfornec2 = .txtDescVal.value
						uv_altDescVal = .T.
						lcEpv1_final = 'ROUND((st.epv1 - ' + ASTR( lcMfornec2, 6, 2) + '), 2)'
					OTHERWISE
						lcEpv1_final = ''
				ENDCASE
 		
                uv_stockIndisp  = .txtStockIndisp.value

				IF !EMPTY(.txtClassFarm.value)			
					lcFamStamp		= ALLTRIM(.txtClassFarm.value)
					
					lcSQL = ''
					TEXT TO lcSQL TEXTMERGE NOSHOW 
						SELECT famstamp, design FROM b_famFamilias (nolock) WHERE UPPER(RTRIM(LTRIM(design))) = '<<lcFamStamp>>'
					ENDTEXT 			
						
					IF !uf_gerais_actgrelha("", "uCrsAuxFamilia", lcSQL)
						uf_perguntalt_chama("Ocorreu uma anomalia ao verificar a classifica��o do produto.","OK","",16)
						RETURN .f.
					ENDIF	

					IF RECCOUNT("uCrsAuxFamilia") > 0
						lcFamStamp = ALLTRIM(uCrsAuxFamilia.famstamp)
						lcFamFamilia = ALLTRIM(uCrsAuxFamilia.design)
					ELSE
						&& n�o � suposto acontecer. failsafe
						lcFamStamp = ''
					ENDIF

					IF USED("uCrsAuxFamilia")
						fecha("uCrsAuxFamilia")
					ENDIF
				ELSE
					lcFamStamp = ""
				ENDIF	
				
			
				* update tabela st
				lcSqlAltGrupo = ""
				** Caso nao selecione empresa			
				IF !USED("ucrsEmpresasSel")	
					uf_altgr_verificarEmpresas()
				ENDIF
										
				IF USED("ucrsEmpresasSel")then
					lcSelSites = ''
					lcSelLojas = ''
					
	
				
					SELECT ucrsEmpresasSel
					GO TOP
					SCAN  FOR ucrsEmpresasSel.sel = .T.
						REPLACE ucrsEmpresasSel.selempresa WITH .t.
					ENDSCAN
		
					
					LOCAL lcOndeTrocar
					lcOndeTrocar = ''

					SELECT ucrsEmpresasSel
					GO top
					SCAN
						IF ucrsEmpresasSel.selempresa == .t.
							lcOndeTrocar = lcOndeTrocar + IIF( EMPTY(lcOndeTrocar), '', ',') + ALLTRIM(ucrsEmpresasSel.local)
						ENDIF
					ENDSCAN 
					
				
																	
					IF !uf_perguntalt_chama("Confirma as altera��es nas seguintes empresas?" +CHR(13)+"(" + lcOndeTrocar+ ")" +CHR(13)+ "CONFIRMA?","Sim","N�o")
						RETURN .f.					
					ENDIF

					SELECT * FROM ucrspesqst WITH (BUFFERING = .T.) WHERE ucrspesqst.sel INTO CURSOR uc_altST READWRITE
					
				
					regua(1,1,"A Atualizar Produtos...")
					
					SELECT ucrsEmpresasSel
					GO TOP 
					SCAN FOR ucrsEmpresasSel.sel == .t.
						lcSelSites = ALLTRIM(STR(ucrsEmpresasSel.siteno))
										
						SELECT uc_altST 
						GO TOP
						SCAN
							

							CREATE CURSOR uc_updates(ststamp C(25), updates M)
							
							lcRefs = ALLTRIM(uc_altST.Ref)
				
							uv_altFields = 'Loja ' + ALLTRIM(lcSelSites) + '' + CHR(13)
							uv_loja = 'Loja ' + ALLTRIM(lcSelSites) + '' + CHR(13)

							TEXT TO lcSQL TEXTMERGE NOSHOW
								exec up_stocks_dadosRef '<<ALLTRIM(uc_altST.Ref)>>', <<lcSelSites>>
							ENDTEXT
							IF !uf_gerais_actgrelha("", "uc_tmpST", lcSql)
								regua(2)
								RETURN .F.
							ENDIF
		
							IF lcActualizaFornecedor
								IF ALLTRIM(uc_tmpST.fornecedor) <> ALLTRIM(lcFornecedor)
									uv_altFields = uv_altFields + 'Campo: FORNECEDOR - Valor original: '  + ALLTRIM(uc_tmpST.fornecedor) + ' - Valor alterado: ' + ALLTRIM(lcFornecedor) + CHR(13)
								ENDIF
							ENDIF

							IF lcActualizaFornecedor 
								IF uc_tmpST.fornec <> VAL(lcFornec)
									uv_altFields = uv_altFields + 'Campo: FORNEC - Valor original: '  + ASTR(uc_tmpST.fornec) + ' - Valor alterado: ' + ALLTRIM(lcFornec) + CHR(13)
								ENDIF
							ENDIF

							IF lcActualizaFornecedor 
								IF uc_tmpST.fornestab <> VAL(lcFornestab)
									uv_altFields = uv_altFields + 'Campo: FORNESTAB - Valor original: '  + ASTR(uc_tmpST.fornestab) + ' - Valor alterado: ' + ALLTRIM(lcFornestab) + CHR(13)
								ENDIF
							ENDIF
		
							IF uv_altMarca OR !EMPTY(lcMarca)
								IF ALLTRIM(uc_tmpST.usr1) <> ALLTRIM(lcMarca) OR (EMPTY(lcMarca) AND uv_altMarca)
									uv_altFields = uv_altFields + 'Campo: USR1 - Valor original: '  + ALLTRIM(uc_tmpST.usr1) + ' - Valor alterado: ' + ALLTRIM(lcMarca) + CHR(13)
								ENDIF
							ENDIF
						
							IF uv_dtIniPromo OR !EMPTY(lcDtIniPromo)
								uv_altFields = uv_altFields + 'Campo: DATAINIPROMO - Valor original: '  + ALLTRIM(uc_tmpST.dataIniPromo) + ' - Valor alterado: ' + ALLTRIM(lcDtIniPromo) + CHR(13)
							ENDIF
							
							IF uv_Questionario OR !EMPTY(lcQuestionario)
								uv_altFields = uv_altFields + 'Campo: Questionario - Valor original: '  + ALLTRIM(uc_tmpST.Questionario) + ' - Valor alterado: ' + ALLTRIM(lcQuestionario) + CHR(13)
							ENDIF
							
							
							IF uv_dtFimPromo OR !EMPTY(lcDtFimPromo)
								uv_altFields = uv_altFields + 'Campo: DATAFIMPROMO - Valor original: '  + ALLTRIM(uc_tmpST.dataFimPromo) + ' - Valor alterado: ' + ALLTRIM(lcDtFimPromo) + CHR(13)
							ENDIF
							
							
							IF uv_altLab OR !EMPTY(lcLab)
								IF ALLTRIM(uc_tmpST.u_lab) <> ALLTRIM(lcLab) OR (EMPTY(lcLab) AND uv_altLab)
									uv_altFields = uv_altFields + 'Campo: U_LAB - Valor original: '  + ALLTRIM(uc_tmpST.u_lab) + ' - Valor alterado: ' + ALLTRIM(lcLab) + CHR(13)
								ENDIF
							ENDIF

							IF ALLTRIM(uc_tmpST.familia) <> ALLTRIM(lcFamilia) AND !EMPTY(lcFamilia)
								uv_altFields = uv_altFields + 'Campo: FAMILIA - Valor original: '  + ALLTRIM(uc_tmpST.familia) + ' - Valor alterado: ' + ALLTRIM(lcFamilia) + CHR(13)
							ENDIF

							IF ALLTRIM(uc_tmpST.faminome) <> ALLTRIM(lcFaminome) AND !EMPTY(lcFaminome)
								uv_altFields = uv_altFields + 'Campo: FAMINOME - Valor original: '  + ALLTRIM(uc_tmpST.faminome) + ' - Valor alterado: ' + ALLTRIM(lcFaminome) + CHR(13)
							ENDIF

							IF uc_tmpST.stmax <> VAL(lcStMax) AND !EMPTY(lcStMax)
								uv_altFields = uv_altFields + 'Campo: STMAX - Valor original: '  + ASTR(uc_tmpST.stmax,13,3) + ' - Valor alterado: ' + ALLTRIM(lcStMax) + CHR(13)
							ENDIF

							IF uc_tmpST.ptoenc <> VAL(lcPtoEnc) AND !EMPTY(lcPtoEnc)
								uv_altFields = uv_altFields + 'Campo: PTOENC - Valor original: '  + ASTR(uc_tmpST.ptoenc,10,3) + ' - Valor alterado: ' + ALLTRIM(lcPtoEnc) + CHR(13)
							ENDIF
							

							IF uc_tmpST.eoq <> VAL(lcEOQ) AND !EMPTY(lcEOQ)
								uv_altFields = uv_altFields + 'Campo: EOQ - Valor original: '  + ASTR(uc_tmpST.eoq,13,3) + ' - Valor alterado: ' + ALLTRIM(lcEOQ) + CHR(13)
							ENDIF
			
							IF uc_tmpST.marg1 <> VAL(lcMarg1) AND !EMPTY(lcMarg1)
			
								uv_altFields = uv_altFields + 'Campo: MARG1 - Valor original: '  + ASTR(uc_tmpST.marg1,16,3) + ' - Valor alterado: ' + ALLTRIM(lcMarg1) + CHR(13)
								
								uv_newEpcusto = ROUND((uc_tmpST.epcusto*1+((VAL(lcMarg1)/100))*(uf_gerais_getUmValor("taxasIva", "taxa", "codigo = " + ASTR(uc_tmpST.tabIva))/100)+1),2)
							
								uv_altFields = uv_altFields + 'Campo: EPV1 - Valor original: '  + ASTR(uc_tmpST.epv1,19,6) + ' - Valor alterado: ' + ASTR(uv_newEpcusto,19,6) + CHR(13)
								
								SELECT uc_tmpST
								lcMarg4 = uc_tmpST.marg4
								lcMarg3 = uc_tmpST.marg3
					
							ENDIF

							IF uv_altLoc OR !EMPTY(lcLocal)
								IF ALLTRIM(uc_tmpST.local) <> ALLTRIM(lcLocal) OR (EMPTY(lcLocal) AND uv_altLoc)
									uv_altFields = uv_altFields + 'Campo: LOCAL - Valor original: '  + ALLTRIM(uc_tmpST.local) + ' - Valor alterado: ' + ALLTRIM(lcLocal) + CHR(13)
								ENDIF
							ENDIF

							IF uv_altLoc2 OR !EMPTY(lcU_local)
								IF ALLTRIM(uc_tmpST.u_local) <> ALLTRIM(lcU_local) OR (EMPTY(lcU_local) AND uv_altLoc2)
									uv_altFields = uv_altFields + 'Campo: U_LOCAL - Valor original: '  + ALLTRIM(uc_tmpST.u_local) + ' - Valor alterado: ' + ALLTRIM(lcU_local) + CHR(13)
								ENDIF
							ENDIF

							IF uv_altLoc3 OR !EMPTY(lcU_local2)
								IF ALLTRIM(uc_tmpST.u_local2) <> ALLTRIM(lcU_local2) OR (EMPTY(lcU_local2) AND uv_altLoc3)
									uv_altFields = uv_altFields + 'Campo: U_LOCAL2 - Valor original: '  + ALLTRIM(uc_tmpST.u_local2) + ' - Valor alterado: ' + ALLTRIM(lcU_local2) + CHR(13)
								ENDIF
							ENDIF
		
							IF uv_altNota OR !EMPTY(lcU_nota1)
								IF ALLTRIM(uc_tmpST.u_nota1) <> ALLTRIM(lcU_nota1) OR (EMPTY(lcU_nota1) AND uv_altNota)
									uv_altFields = uv_altFields + 'Campo: U_NOTA1 - Valor original: '  + ALLTRIM(uc_tmpST.u_nota1) + ' - Valor alterado: ' + ALLTRIM(lcU_nota1) + CHR(13)
								ENDIF
							ENDIF

							IF uv_altObs OR !EMPTY(lcObs)
								IF ALLTRIM(uc_tmpST.obs) <> ALLTRIM(lcObs) OR (EMPTY(lcObs) AND uv_altObs)
									uv_altFields = uv_altFields + 'Campo: OBS - Valor original: '  + ALLTRIM(uc_tmpST.obs) + ' - Valor alterado: ' + ALLTRIM(lcObs) + CHR(13)
								ENDIF
							ENDIF

							IF lcInactivoalter = 1 OR lcInactivoalterN = 1
								IF uc_tmpST.inactivo AND lcInactivoalterN = 1
									uv_altFields = uv_altFields + 'Campo: INACTIVO - Valor original: Verdadeiro - Valor alterado: Falso ' + CHR(13)
								ELSE
									IF !uc_tmpST.inactivo AND lcInactivoalter = 1
										uv_altFields = uv_altFields + 'Campo: INACTIVO - Valor original: Falso - Valor alterado: Verdadeiro ' + CHR(13)
									ENDIF
								ENDIF
							ENDIF

							IF pAlteraimprime <> 0

								IF uc_tmpST.u_impetiq <> IIF(lcU_impetiq = "1", .T., IIF(!EMPTY(lcU_impetiq), .F., uc_tmpST.u_impetiq)) 
									uv_altFields = uv_altFields + 'Campo: U_IMPETIQ - Valor original: ' + IIF(uc_tmpST.u_impetiq, 'Verdadeiro', 'Falso') + ' - Valor alterado: ' + IIF(lcU_impetiq = "1", 'Verdadeiro', 'Falso') + CHR(13)
								ENDIF

							ENDIF
	
							IF uv_altClassIMS &&OR !EMPTY(MyclassNivel1)
								IF ALLTRIM(uc_tmpST.u_depstamp) <> ALLTRIM(MyclassNivel1) OR (EMPTY(MyclassNivel1) AND uv_altClassIMS)
									uv_altFields = uv_altFields + 'Campo: U_DEPSTAMP - Valor original: '  + ALLTRIM(uc_tmpST.u_depstamp) + ' - Valor alterado: ' + ALLTRIM(MyclassNivel1) + CHR(13)
								ENDIF
							ENDIF

							IF uv_altClassIMS &&OR !EMPTY(MyclassNivel2)
								IF ALLTRIM(uc_tmpST.u_secstamp) <> ALLTRIM(MyclassNivel2) OR (EMPTY(MyclassNivel2) AND uv_altClassIMS)
									uv_altFields = uv_altFields + 'Campo: U_SECSTAMP - Valor original: '  + ALLTRIM(uc_tmpST.u_secstamp) + ' - Valor alterado: ' + ALLTRIM(MyclassNivel2) + CHR(13)
								ENDIF
							ENDIF
	
							IF uv_altClassIMS &&OR !EMPTY(MyclassNivel3)
								IF ALLTRIM(uc_tmpST.u_catstamp) <> ALLTRIM(MyclassNivel3) OR (EMPTY(MyclassNivel3) AND uv_altClassIMS)
									uv_altFields = uv_altFields + 'Campo: U_CATSTAMP - Valor original: '  + ALLTRIM(uc_tmpST.u_catstamp) + ' - Valor alterado: ' + ALLTRIM(MyclassNivel3) + CHR(13)
								ENDIF
							ENDIF

							IF uv_altClassIMS &&OR !EMPTY(MyclassNivel4)
								IF ALLTRIM(uc_tmpST.u_segstamp) <> ALLTRIM(MyclassNivel4) OR (EMPTY(MyclassNivel4) AND uv_altClassIMS)
									uv_altFields = uv_altFields + 'Campo: U_SEGSTAMP - Valor original: '  + ALLTRIM(uc_tmpST.u_segstamp) + ' - Valor alterado: ' + ALLTRIM(MyclassNivel4) + CHR(13)
								ENDIF
							ENDIF

							IF uv_altFamilia OR !EMPTY(lcFamStamp)
								IF ALLTRIM(uc_tmpST.u_famstamp) <> ALLTRIM(lcFamStamp) OR (EMPTY(lcFamStamp) AND uv_altClassIMS)
									uv_altFields = uv_altFields + 'Campo: U_FAMSTAMP - Valor original: '  + ALLTRIM(uc_tmpST.u_famstamp) + ' - Valor alterado: ' + ALLTRIM(lcFamStamp) + CHR(13)
									uv_altFields = uv_altFields + 'Campo: FAMFAMILIA - Valor original: '  + ALLTRIM(uc_tmpST.famfamilia) + ' - Valor alterado: ' + ALLTRIM(lcFamFamilia) + CHR(13)
								ENDIF
							ENDIF
	
							IF uv_dispOnline <> -1
								IF uc_tmpST.dispOnline <> IIF(uv_dispOnline = 1, .T., .F.)
									uv_altFields = uv_altFields + 'Campo: DISPONLINE - Valor original: '  + IIF(uc_tmpST.dispOnline, 'Verdadeiro', 'Falso') + ' - Valor alterado: ' + IIF(uv_dispOnline = 1, 'Verdadeiro', 'Falso') + CHR(13)
								ENDIF
							ENDIF

							IF uv_portesGratis <> -1
								IF uc_tmpST.portesGratis <> IIF(uv_portesGratis = 1, .T., .F.)
									uv_altFields = uv_altFields + 'Campo: PORTESGRATIS - Valor original: '  + IIF(uc_tmpST.portesGratis, 'Verdadeiro', 'Falso') + ' - Valor alterado: ' + IIF(uv_portesGratis = 1, 'Verdadeiro', 'Falso') + CHR(13)
								ENDIF
							ENDIF

							IF uv_novidade <> -1
								IF uc_tmpST.novidadeOnline <> IIF(uv_novidade = 1, .T., .F.)
									uv_altFields = uv_altFields + 'Campo: NOVIDADEONLINE - Valor original: '  + IIF(uc_tmpST.novidadeOnline, 'Verdadeiro', 'Falso') + ' - Valor alterado: ' + IIF(uv_novidade = 1, 'Verdadeiro', 'Falso') + CHR(13)
								ENDIF
							ENDIF

							IF !EMPTY(uv_stockIndisp)
								IF uc_tmpST.tempoIndiponibilidade <> VAL(uv_stockIndisp)
									uv_altFields = uv_altFields + 'Campo: TEMPOINDIPONIBILIDADE - Valor original: '  + ASTR(uc_tmpST.tempoIndiponibilidade, 8, 1) + ' - Valor alterado: ' + ALLTRIM(uv_stockIndisp) + CHR(13)
								ENDIF
							ENDIF
	
							IF uv_psobConsulta <> -1
								IF IIF(uc_tmpST.precoSobConsulta, .T., .F.) <> IIF(uv_psobConsulta = 1, .T., .F.)
									uv_altFields = uv_altFields + 'Campo: PRECOSOBCONSULTA - Valor original: '  + IIF(uc_tmpST.precoSobConsulta, 'Verdadeiro', 'Falso') + ' - Valor alterado: ' + IIF(uv_psobConsulta = 1, 'Verdadeiro', 'Falso') + CHR(13)
								ENDIF
							ENDIF

							IF uv_psobConsulta <> -1
								IF IIF(uc_tmpST.pim, .T., .F.) <> IIF(uv_pim = 1, .T., .F.)
									uv_altFields = uv_altFields + 'Campo: PIM - Valor original: '  + IIF(uc_tmpST.pim, 'Verdadeiro', 'Falso') + ' - Valor alterado: ' + IIF(uv_pim = 1, 'Verdadeiro', 'Falso') + CHR(13)
								ENDIF
							ENDIF

							IF uv_altDescPerc							
								IF uc_tmpST.mfornec <> lcMfornec								
									uv_altFields = uv_altFields + 'Campo: mfornec - Valor original: ' + ASTR(uc_tmpST.mfornec, 6, 2) + ' - Valor alterado: ' + ASTR(lcMfornec, 6, 2) + CHR(13)
									IF !EMPTY(uc_tmpST.mfornec2)
										uv_altFields = uv_altFields + 'Campo: mfornec2 - Valor original: ' + ASTR(uc_tmpST.mfornec2, 6, 2) + ' - Valor alterado: 0' + CHR(13)
									ENDIF
									uv_altFields = uv_altFields + 'Campo: epv1_final - Valor original: ' + ASTR(uc_tmpST.epv1_final, 19, 6) + ' - Valor alterado: ' + ASTR((uc_tmpST.epv1 - ((uc_tmpST.epv1 * lcMfornec)/100)), 19, 6) + CHR(13)
								ENDIF
							ENDIF
	
							IF uv_altDescVal

								IF lcMfornec2 > uc_tmpST.epv1
									uf_perguntalt_responsivo_chama("N�o pode aplicar um desconto em valor superior ao valor do produto." + CHR(13) ;
																	+ "Ref: " + ALLTRIM(uc_tmpST.ref) + CHR(13);
																	+ "Design: " + ALLTRIM(uc_tmpSt.design) + CHR(13);
																	+ "Pre�o: " + ASTR(uc_tmpST.epv1, 19, 6);
									, "OK", "", 48)
									RETURN .F.
								ENDIF

								IF uc_tmpST.mfornec2 <> lcMfornec2
									uv_altFields = uv_altFields + 'Campo: mfornec2 - Valor original: ' + ASTR(uc_tmpST.mfornec2, 6, 2) + ' - Valor alterado: ' + ASTR(lcMfornec2, 6, 2) + CHR(13)
									IF !EMPTY(uc_tmpST.mfornec)
										uv_altFields = uv_altFields + 'Campo: mfornec - Valor original: ' + ASTR(uc_tmpST.mfornec, 6, 2) + ' - Valor alterado: 0' + CHR(13)
									ENDIF
									uv_altFields = uv_altFields + 'Campo: epv1_final - Valor original: ' + ASTR(uc_tmpST.epv1_final, 19, 6) + ' - Valor alterado: ' + ASTR((uc_tmpST.epv1 - lcMfornec2), 19, 6) + CHR(13)
								ENDIF
							ENDIF


							IF LEN(ALLTRIM(uv_altFields)) > LEN(ALLTRIM(uv_loja))

								SELECT uc_updates
								APPEND BLANK
								REPLACE uc_updates.ststamp WITH uc_tmpST.ststamp,;
										uc_updates.updates WITH ALLTRIM(uv_altFields)

							ENDIF
					
								TEXT TO lcSqlAltGrupo TEXTMERGE NOSHOW
								UPDATE
									st
								SET
									<<IIF(!EMPTY(lcActualizaFornecedor),IIF(EMPTY(lcFornecedor), "Fornecedor = '',", "fornecedor='" + lcFornecedor + "',"),"")>>
									<<IIF(!EMPTY(lcActualizaFornecedor),IIF(EMPTY(lcFornecedor), "fornec = 0, ", "fornec=" + lcFornec + ","),"")>>
									<<IIF(!EMPTY(lcActualizaFornecedor),IIF(EMPTY(lcFornecedor), "fornestab = 0, ", "fornestab=" + lcFornestab + ","),"")>>
									<<IIF(EMPTY(lcMarca), IIF(!uv_altMarca, "", "usr1 = '',"), "usr1='" + lcMarca + "',")>>
									<<IIF(EMPTY(lcDtIniPromo), IIF(!uv_dtIniPromo, "", "dataIniPromo = '',"), "dataIniPromo='" + lcDtIniPromo + "',")>>
									<<IIF(EMPTY(lcDtFimPromo), IIF(!uv_dtFimPromo, "", "dataFimPromo = '',"), "dataFimPromo='" + lcDtFimPromo + "',")>>
									<<IIF(EMPTY(lcLab), IIF(!uv_altLab, "", "u_lab = '',"), "u_lab='" + lcLab + "',")>>
									<<IIF(EMPTY(lcFamilia), "", "familia='" + lcFamilia + "',")>>
									<<IIF(EMPTY(lcFaminome), "", "faminome='" + lcFaminome + "',")>>
									<<IIF(EMPTY(lcStMax), "", "stmax=" + lcStMax + ",")>>
									<<IIF(EMPTY(lcPtoEnc), "", "ptoenc=" + lcPtoEnc + ",")>>
									<<IIF(EMPTY(lcEOQ), "", "eoq=" + lcEOQ + ",")>>
									<<IIF(EMPTY(lcMarg1), "", "marg1=" + lcMarg1 + ", epv1=ROUND((epcusto*(1+(" + lcMarg1 + "/100.00)))*(((select taxasiva.taxa from taxasiva where taxasiva.codigo=st.tabiva)/100.00)+1),2),")>>
									<<IIF(EMPTY(lcLocal), IIF(!uv_altLoc, "", "local = '',"), "local='" + lcLocal + "',")>>
									<<IIF(EMPTY(lcU_local), IIF(!uv_altLoc2, "", "u_local = '',"), "u_local='" + lcU_local + "',")>>
									<<IIF(EMPTY(lcU_local2), IIF(!uv_altLoc3, "", "u_local2 = '',"), "u_local2='" + lcU_local2 + "',")>>
									<<IIF(EMPTY(lcU_nota1), IIF(!uv_altNota, "", "u_nota1 = '',"), "u_nota1='" + lcU_nota1 + "',")>> 
									<<IIF(EMPTY(lcObs), IIF(!uv_altObs, "", "obs = '',"), "obs='" + lcObs + "',")>>	
									usrdata		= convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
									,usrhora	= convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108)
									,usrinis	= '<<m_chinis>>'
									,inactivo	= <<IIF(lcInactivoalter = 1, '1', IIF(lcInactivoalterN = 1, '0', 'inactivo')) >>
									<<IIF(pAlteraimprime=0, "", ", u_impetiq	= " + lcU_impetiq + " ")>>
									,<<IIF(!EMPTY(MyclassNivel1), "u_depstamp = '" + MyclassNivel1 + "'", IIF(!uv_altClassIMS, "u_depstamp = u_depstamp", "u_depstamp = ''" ))>>
									,<<IIF(!EMPTY(MyclassNivel2), "u_secstamp = '" + MyclassNivel2 + "'", IIF(!uv_altClassIMS, "u_secstamp = u_secstamp", "u_secstamp = ''" ))>>
									,<<IIF(!EMPTY(MyclassNivel3), "u_catstamp = '" + MyclassNivel3 + "'", IIF(!uv_altClassIMS, "u_catstamp = u_catstamp", "u_catstamp = ''" ))>>
									,<<IIF(!EMPTY(MyclassNivel4), "u_segstamp = '" + MyclassNivel4 + "'", IIF(!uv_altClassIMS, "u_segstamp = u_segstamp", "u_segstamp = ''" ))>>								
									,<<IIF(!EMPTY(lcFamStamp), "u_famstamp = '" + lcFamStamp+ "'", IIF(!uv_altFamilia, "u_famstamp = u_famstamp", "u_famstamp = ''" ))>>
									,dispOnline = <<IIF(uv_dispOnline = -1, 'dispOnline', ASTR(uv_dispOnline))>>
									,portesGratis = <<IIF(uv_portesGratis = -1, 'portesGratis', ASTR(uv_portesGratis))>>
									,novidadeOnline = <<IIF(uv_novidade = -1, 'novidadeOnline', ASTR(uv_novidade))>>
									,tempoIndiponibilidade = <<IIF(EMPTY(uv_stockIndisp) , 'tempoIndiponibilidade', uv_stockIndisp)>>
									,precoSobConsulta = <<IIF(uv_psobConsulta = -1, 'precoSobConsulta', ASTR(uv_psobConsulta))>>
									,pim = <<IIF(uv_pim = -1, 'precoSobConsulta', ASTR(uv_pim))>>
									<<IIF(!EMPTY(lcEpv1_final), ',epv1_final = ' + ALLTRIM(lcEpv1_final), '')>>
									,mfornec = <<IIF(uv_altDescPerc, lcMfornec, IIF(uv_altDescVal, '0', 'mfornec'))>>
									,mfornec2 = <<IIF(uv_altDescVal, lcMfornec2, IIF(uv_altDescPerc, '0', 'mfornec2'))>>
									<<IIF(EMPTY(lcMarg1), "", ",marg4=" + TRANSFORM(lcMarg4, "9999999.99"))>>
									<<IIF(EMPTY(lcMarg1), "", ",marg3=" + TRANSFORM(lcMarg3, "9999999.99"))>>
								WHERE
									ref in ('<<lcRefs>>')
									and site_nr in (<<lcSelSites>>)
							ENDTEXT

							IF !uf_gerais_actGrelha("", "", lcSqlAltGrupo)
								RETURN .f.
							ELSE

								IF USED("uc_updates")
									
									SELECT uc_updates
									GO TOP
									
									SCAN
										uf_user_log_ins('ST', ALLTRIM(uc_updates.ststamp), 'ALTERA��O', ALLTRIM(uc_updates.updates))
									ENDSCAN

									FECHA("uc_updates")

								ENDIF
							
							ENDIF

							IF !EMPTY(lcMarg1)					
								uf_stocks_Corrige_mov_alteracaoGrupo(lcRefs, lcSelSites)
								uf_pesqstocks_bb()
								uf_pesqstocks_marg3(lcRefs,lcSelSites)
							ENDIF
							
						ENDSCAN					
					ENDSCAN
					regua(2)
				ELSE
						regua(2)
						uf_perguntalt_responsivo_chama("N�o selecionou a loja a atualizar.", "OK", "", 4)
						RETURN .F.	
				ENDIF 

			ENDWITH 
			
			
			uf_perguntalt_chama("ARTIGOS EDITADOS COM SUCESSO.","OK","",64)

			WITH pesqstocks.container2.PageFrame1.page1
				.txtFornecedor.value=''
				.txtFornestab.value=''
				.txtFornec.value=''
				.txtMarca.value=''
				.txtU_Lab.value=''
				.cmbFamilia.value=''
				.txtfaminome.value=''
				.txtstmax.value=''
				.txtptoenc.value=''
				.txteoq.value=''
				.txtClassIMS.value=''
				.txtClassFarm.value=''
				.chkInactivosAlter.tag = "false"
				.chkInactivosAlterN.tag = "false"
				.chku_impetiqAlterS.tag = "false"
				.chku_impetiqAlterN.tag = "false"
				.chkInactivosAlter.image1.Picture = myPath + "\imagens\icons\unchecked_b_24.bmp"
				.chkInactivosAlterN.image1.Picture = myPath + "\imagens\icons\unchecked_b_24.bmp"
				.chku_impetiqAlterN.image1.Picture = myPath + "\imagens\icons\unchecked_b_24.bmp"
				.chku_impetiqAlterS.image1.Picture = myPath + "\imagens\icons\unchecked_b_24.bmp"
				.txtmarg1.value=''
				.txtlocal.value=''
				.txtu_local.value=''
				.txtu_local2.value=''
				.txtu_nota1.value=''
				.txtobs.value=''
				MyclassNivel1=''
				MyclassNivel2=''
				MyclassNivel3=''
				MyclassNivel4=''
				pAlteraimprime=0
				up_altDisp = .F.
				up_altNov = .F.
				up_altPort = .F.
				.txtStockIndisp.value = ''
				.chkDispWeb.tag = "false"
				.chkNov.tag = "false"
				.chkPortes.tag = "false"
				.chkDispWeb.image1.Picture = myPath + "\imagens\icons\unchecked_b_24.bmp"
				.chkNov.image1.Picture = myPath + "\imagens\icons\unchecked_b_24.bmp"
				.chkPortes.image1.Picture = myPath + "\imagens\icons\unchecked_b_24.bmp"
				.chkDispWebN.tag = "false"
				.chkNovN.tag = "false"
				.chkPortesN.tag = "false"
				.chkDispWebN.image1.Picture = myPath + "\imagens\icons\unchecked_b_24.bmp"
				.chkNovN.image1.Picture = myPath + "\imagens\icons\unchecked_b_24.bmp"
				.chkPortesN.image1.Picture = myPath + "\imagens\icons\unchecked_b_24.bmp"
				.chkLab.tag = "false"
				.chkLab.image1.Picture = myPath + "\imagens\icons\unchecked_b_24.bmp"
				.chkMarca.tag = "false"
				.chkMarca.image1.Picture = myPath + "\imagens\icons\unchecked_b_24.bmp"
				.chkIMS.tag = "false"
				.chkIMS.image1.Picture = myPath + "\imagens\icons\unchecked_b_24.bmp"
				.chkFam.tag = "false"
				.chkFam.image1.Picture = myPath + "\imagens\icons\unchecked_b_24.bmp"
				.chkForn.tag = "false"
				.chkForn.image1.Picture = myPath + "\imagens\icons\unchecked_b_24.bmp"
				.chkNota.tag = "false"
				.chkNota.image1.Picture = myPath + "\imagens\icons\unchecked_b_24.bmp"
				.chkPSCons.tag = "false"
				.chkPSCons.image1.Picture = myPath + "\imagens\icons\unchecked_b_24.bmp"
				.chkPSConsN.tag = "false"
				.chkPSConsN.image1.Picture = myPath + "\imagens\icons\unchecked_b_24.bmp"
				.chkPimAlter.tag = "false"
				.chkPimAlter.image1.Picture = myPath + "\imagens\icons\unchecked_b_24.bmp"
				.chkDescPerc.tag = "false"
				.chkDescPerc.image1.Picture = myPath + "\imagens\icons\unchecked_b_24.bmp"
				.chkDescVal.tag = "false"
				.chkDescVal.image1.Picture = myPath + "\imagens\icons\unchecked_b_24.bmp"
				.chkDtIniPromo.tag = "false"
				.chkDtIniPromo.image1.Picture = myPath + "\imagens\icons\unchecked_b_24.bmp"
				.chkDtFimPromo.tag = "false"
				.chkDtFimPromo.image1.Picture = myPath + "\imagens\icons\unchecked_b_24.bmp"
				.txtDescPerc.value = ""
				.txtDescVal.value = ""
				.txtDtIni.value = "1900.01.01"
				.txtDtFim.value = "1900.01.01"
				PESQSTOCKS.descVal = 0.00
				PESQSTOCKS.descPerc = 0.00
				PESQSTOCKS.datafimpromo = ""
				PESQSTOCKS.datainipromo = ""
			ENDWITH 

			uf_pesqstocks_carregaDados()
			uf_pesqstocks_grid_setFocus()

		CASE myOrigemPPST == "SMS"
			IF USED("uCrsProdutosSMS")
				LOCAL lcValida
				lcValida = .t.
				SELECT uCrsPesqST
				GO TOP 
				SCAN
					IF uCrsPesqST.sel == .t.
						SELECT uCrsProdutosSMS
						GO TOP
						SCAN
							IF ALLTRIM(uCrsProdutosSMS.ref) == ALLTRIM(uCrsPesqST.ref)
								lcValida = .f.
							ENDIF 
						ENDSCAN 
						
						IF lcValida
							APPEND BLANK
							replace uCrsProdutosSMS.ref WITH ALLTRIM(uCrsPesqST.ref)
							replace uCrsProdutosSMS.design WITH ALLTRIM(uCrsPesqST.design)
						ENDIF 
					ENDIF 
				ENDSCAN
				uf_pesqstocks_grid_setFocus()
				uf_pesqstocks_sair()
			ENDIF 

		CASE myOrigemPPST == "INVENTARIO"
			IF USED("uCrsImpParcial")
				SELECT uCrsImpParcial
				DELETE ALL
			ELSE
				CREATE CURSOR uCrsImpParcial (ref c(18))
				SELECT uCrsImpParcial
				SELECT uCrsPesqST
				GO TOP
				SCAN FOR uCrsPesqST.sel == .t.
					SELECT uCrsImpParcial					
					APPEND BLANK
					replace uCrsImpParcial.ref WITH ALLTRIM(uCrsPesqST.ref)
				ENDSCAN
				
				SELECT uCrsImpParcial
				IF RECCOUNT("uCrsImpParcial") > 0
					IF TYPE("pesqinventario") != "U"
						pesqinventario.menu1.parcial.config("Parcial",myPath + "\imagens\icons\checked_w.png","uf_pesqinventario_pesqStock","P")
					ELSE
						pesqinventario.menu1.parcial.config("Parcial",myPath + "\imagens\icons\unchecked_w.png","uf_pesqinventario_pesqStock","P")
					ENDIF
				ENDIF
				uf_pesqstocks_grid_setFocus()
				uf_pesqstocks_sair()
			ENDIF

		CASE myOrigemPPST == "CONSOLIDACAOCOMPRAS"
			uf_pesqstocks_sel_to_consolidacaoCompras()
			uf_pesqstocks_grid_setFocus()
			&& Alterado para encerrar painel quando se gravam altera��es; Aten��o ao alterar deve manter-se coer�ncia com os paineis de Conf PVP e Conf Validade.
			uf_pesqstocks_sair()
		CASE myOrigemPPST == "CAMPANHAS"
	
			uf_CAMPANHAS_selPesquisaSt()

		CASE myOrigemPPST == "ENCAUTOMATICAS"
			PUBLIC MyEncConjunta
			PUBLIC myencGrupo 

			LOCAL lcListaRefs, lcIDSedeGrupo
			lcListaRefs = ""
			lcIDSedeGrupo = ""
			
			SELECT ucrspesqst
			GO TOP
			SCAN FOR !EMPTY(ucrspesqst.sel)
				lcListaRefs = lcListaRefs + IIF(!EMPTY(lcListaRefs),",","") + ALLTRIM(ucrspesqst.ref)
			ENDSCAN 

			IF EMPTY(lcListaRefs)		
				uf_perguntalt_chama("N�o selecionou produtos a adicionar � encomenda autom�tica.","OK","",64)
				RETURN .f.
			ENDIF 

			IF USED("uCrsConfiguracaoSedeGrupo")
				IF myencGrupo == .t.
					SELECT uCrsConfiguracaoSedeGrupo
					lcIDSedeGrupo = ALLTRIM(uCrsConfiguracaoSedeGrupo.bdname)
				ENDIF
			ENDIF 
		
			TEXT TO lcSQL NOSHOW TEXTMERGE
			 	exec up_enc_autorefEncAutomatica <<IIF(MyEncConjunta,0,myArmazem)>>, '<<Alltrim(lcListaRefs)>>','<<ALLTRIM(lcIDSedeGrupo)>>', <<mysite_nr>>
			ENDTEXT 

			IF uf_gerais_actgrelha("", "uCrsAddProdPea",lcSQL)
			
				SELECT uCrsAddProdPea
				BROWSE FIELDS marg4
				GO TOP
				SCAN 
				
					** verifica se produto j� existe **
					Local lcCrsencPos, lcValidaAddPea
					lcCrsencPos = 0
					lcValidaAddPea = .f.
					**
					


					IF myOrigemPPST != "ENCAUTOMATICASSAIR" AND  USED("crsenc")
						** adiciona produto **
						Select crsenc
						Append Blank
						replace crsenc.ststamp		With 	Alltrim(uCrsAddProdPea.ststamp)
						replace crsenc.u_fonte		With 	Alltrim(uCrsAddProdPea.u_fonte)
						replace crsenc.ref			With 	Alltrim(uCrsAddProdPea.ref)
						replace crsenc.refb			With 	Alltrim(uCrsAddProdPea.refb)
						replace crsenc.codigo		With 	Alltrim(uCrsAddProdPea.codigo)
						replace crsenc.design		With 	Alltrim(uCrsAddProdPea.design)
						replace crsenc.stock		With 	uCrsAddProdPea.stock
						replace crsenc.stmin		With 	uCrsAddProdPea.stmin
						replace crsenc.stmax		With 	uCrsAddProdPea.stmax
						replace crsenc.ptoenc		With 	uCrsAddProdPea.ptoenc
						replace crsenc.eoq			With 	uCrsAddProdPea.eoq
						replace crsenc.qtbonus		With 	uCrsAddProdPea.qtbonus
						replace crsenc.qttadic		With 	uCrsAddProdPea.qttadic
						replace crsenc.qttfor		With 	uCrsAddProdPea.qttfor
						replace crsenc.qttacin		With 	uCrsAddProdPea.qttacin
						replace crsenc.qttcli		With 	uCrsAddProdPea.qttcli
                        replace crsenc.qttres		With 	uCrsAddProdPea.qttcli
						replace crsenc.fornecedor	With 	Alltrim(uCrsAddProdPea.fornecedor)
						replace crsenc.fornec		With 	uCrsAddProdPea.fornec
						replace crsenc.fornestab	With 	uCrsAddProdPea.fornestab
						replace crsenc.epcusto		with 	uCrsAddProdPea.epcusto
						replace crsenc.epcpond		With 	uCrsAddProdPea.epcpond
						replace crsenc.epcult		With 	uCrsAddProdPea.epcult
						replace crsenc.tabiva		With 	uCrsAddProdPea.tabiva
						replace crsenc.iva			With 	uCrsAddProdPea.iva
						replace crsenc.cpoc			With 	uCrsAddProdPea.cpoc
						replace crsenc.familia		With 	uCrsAddProdPea.familia
						replace crsenc.faminome		With 	alltrim(uCrsAddProdPea.faminome)
						replace crsenc.u_lab		With 	Alltrim(uCrsAddProdPea.u_lab)
						replace crsenc.conversao	With 	uCrsAddProdPea.conversao
						
						replace crsenc.stockgh		With 	uCrsAddProdPea.stockgh
						replace crsenc.bonusfornec	With 	Alltrim(uCrsAddProdPea.bonusfornec)
						Replace crsenc.pclfornec	WITH 	uCrsAddProdPea.pclfornec
						replace crsenc.esgotado		With 	Alltrim(uCrsAddProdPea.esgotado)

						Replace crsenc.alertaPCL		WITH 	uCrsAddProdPea.alertaPCL
						Replace crsenc.alertaBonus		WITH 	uCrsAddProdPea.alertaBonus
						Replace crsenc.alertaStockGH	WITH 	uCrsAddProdPea.alertaStockGH
						REPLACE crsenc.obs				WITH	uCrsAddProdPea.obs
						REPLACE crsenc.u_nota1			WITH	uCrsAddProdPea.u_nota1
						replace crsenc.marg4			With 	uCrsAddProdPea.marg4
						replace crsenc.enc			WITH  	.f.
						replace crsenc.sel			WITH 	.f.
						TRY
							replace crsenc.marca		WITH uCrsAddProdPea.marca
						CATCH
						ENDTRY
						IF TYPE("crsenc.pcusto") <> "U"
							REPLACE crsenc.pcusto WITH uCrsAddProdPea.pclFicha
						ENDIF
						IF TYPE("crsenc.descontoForn") <> "U"
							REPLACE crsenc.descontoForn WITH uCrsAddProdPea.descontoForn
						ENDIF
					ENDIF
				ENDSCAN
				
				IF USED("uCrsAddProdPea")
					Fecha("uCrsAddProdPea")
				ENDIF
				
				SELECT crsenc
				
				encautomaticas.gridPesq.refresh
				uf_pesqstocks_grid_setFocus()

			ENDIF
			
			uf_perguntalt_chama("Produtos adicionados � encomenda com sucesso.","OK","",64)
			uf_pesqstocks_sair()
			
			
			
		OTHERWISE
			**
	ENDCASE
	
ENDFUNC


**
FUNCTION uf_pesqstocks_SelTodos
	LPARAMETERS lcBool
	
	**uf_pesqstocks_multiSel()
	
	IF lcBool
		&& aplica sele��o
		REPLACE ALL sel WITH .t. IN uCrspesqst
		
		&& altera o botao
		PESQSTOCKS.menu1.selTodos.config("De-Sel. Todos", myPath + "\imagens\icons\checked_w.png", "uf_pesqstocks_selTodos with .f.","")
		
		&& Selecciona os Produtos 
		DO CASE
			CASE myOrigemPPST == "DOCUMENTOS"
				
				**
				IF myDocAlteracao == .f. AND myDocIntroducao == .f.
					uf_perguntalt_chama("PARA UTILIZAR ESTA OP��O O ECR� DE DOCUMENTOS DEVE ESTAR EM MODO DE EDI��O.","OK","", 48)
					RETURN .f.
				ENDIF
			

				**		
				documentos.lockscreen =  .t.
				documentos.PageFrame1.Page1.gridDoc.SetFocus
				
				DO CASE
					CASE mytipoDoc = "FO"
						
						SELECT uCrsPesqST
						GO TOP 
						SCAN 
							IF uCrsPesqST.sel == .t.
								
								&& Verifica se a ficha est� criada
								IF uf_pesqstocks_validaCriacaoFicha() == .f.
									RETURN .f.
								ENDIF 
							
								SELECT fn
								IF !EMPTY(fn.ref)
									uf_documentos_criaNovaLinha(.t.,.t.)
								ELSE
									uf_documentos_editarTemp(fn.fnstamp)
								ENDIF 

								** inserir refer�ncia
								SELECT fn
								SELECT uCrspesqst
								REPLACE fn.ref WITH ALLTRIM(UCRSPESQST.ref)
								
								IF UCRSPESQST.qttcli > 0
									replace fn.u_reserva WITH .t.
								ENDIF 

								uf_documentos_eventoref()
								
								SELECT CabDoc
								IF !EMPTY(cabDoc.tabIva)
									lcTaxaIva = uf_gerais_getTabelaIva(cabDoc.tabIva,"TAXA")
									Replace fn.tabIva WITH cabDoc.tabIva
									Replace fn.Iva    WITH lcTaxaIva
								ENDIF
							ENDIF
							
						ENDSCAN
						
						uf_documentos_recalculaTotaisFN()
							

					CASE mytipoDoc = "BO"
						
						PUBLIC lcValapl
						STORE 0 TO lcValapl
*!*							LOCAL lcValUnitCaixa
*!*							STORE .f. TO lcValUnitCaixa
						IF ALLTRIM(cabdoc.doc)='Tab. Comparticipa��es'
							uf_tecladonumerico_chama("lcValapl", "Introduza o valor a comparticipar para esta selec��o.", 0, .T., .F., 5, 2)
						ENDIF 
*!*							IF ALLTRIM(cabdoc.doc)='Imprimir Etiquetas'
*!*								lcValUnitCaixa = .t.
*!*							ENDIF 
						
						SELECT uCrspesqst
						GO TOP 
						SCAN
							IF uCrspesqst.sel == .t.
							
								&& Verifica se a ficha est� criada
								IF uf_pesqstocks_validaCriacaoFicha() == .f.
									RETURN .f.
								ENDIF 
						
								SELECT BI
								
								IF !EMPTY(BI.ref)
									uf_documentos_criaNovaLinha(.t.,.t.)
								ELSE
									uf_documentos_editarTemp(bi.bistamp)
								ENDIF 

								** inserir refer�ncia
								Select UCRSPESQST
								SELECT BI
								Replace bi.ref WITH Alltrim(UCRSPESQST.ref)
								uf_documentos_eventoref()
								
								IF lcValapl>0
									SELECT bi 
									replace bi.edebito WITH lcValapl
									replace bi.ettdeb WITH lcValapl
								ENDIF 
*!*									IF lcValUnitCaixa = .t.
*!*										SELECT bi
*!*										replace bi.edebito WITH ROUND(bi.edebito * bi.qtt,2)
*!*									ENDIF 
																
								SELECT CabDoc
								IF !EMPTY(cabDoc.tabIva)
									lcTaxaIva = uf_gerais_getTabelaIva(cabDoc.tabIva,"TAXA")
									Replace bi.tabIva WITH cabDoc.tabIva
									Replace bi.Iva WITH lcTaxaIva
								ENDIF
								
								uf_documentos_recalculaTotaisBI()
							ENDIF
						ENDSCAN
					OTHERWISE
						**
				ENDCASE
				
				uf_documentos_actualizatotaisCAB()
				documentos.lockscreen =  .f.
			CASE myOrigemPPST == "FACTURACAO"

				SELECT uCrspesqst
				GO TOP 
				SCAN
					IF uCrspesqst.sel == .t.	
						&& Verifica se a ficha est� criada
						IF uf_pesqstocks_validaCriacaoFicha() == .f.
							RETURN .f.
						ENDIF 
					
						SELECT fi
						IF !EMPTY(fi.ref)
							uf_atendimento_AdicionaLinhaFi(.T.,.F.)
						ENDIF 
					
						SELECT fi
							replace fi.ref with UCRSPESQST.ref			
							uf_facturacao_eventoRefFi()
					ENDIF	
				ENDSCAN
			
			CASE myOrigemPPST == "CONSOLIDACAOCOMPRAS"
			
				rec = RECCOUNT("UCRSPESQST")
				i = 1
				regua(1,rec,"A Adicionar Refer�ncias...")
				SELECT UCRSPESQST
				GO TOP
				
				SCAN
					regua(1,i,"A Remover Refer�ncias...")
					uf_pesqstocks_sel_to_consolidacaoCompras()
					i=i+1
					SELECT UCRSPESQST
				ENDSCAN
				
				SELECT UCRSPESQST
				GO TOP
				regua(2)
			
			CASE myOrigemPPST == "PESQCLIENTES"
			
				LOCAL lcvalidaExiste 
				lcvalidaExiste = .f.
		
				IF USED("UCRSPRODUTOSCLI")

					SELECT UCRSPESQST
					GO TOP
					IF UCRSPESQST.SEL == .t.

						SELECT UCRSPRODUTOSCLI
						SCAN FOR ALLTRIM(UCRSPRODUTOSCLI.REF) == ALLTRIM(UCRSPESQST.REF)
							lcvalidaExiste = .t.
						ENDSCAN
					
						IF lcvalidaExiste == .f.

							SELECT UCRSPESQST
							GO TOP
							SCAN FOR UCRSPESQST.SEL == .t.
								SELECT UCRSPRODUTOSCLI
								GO BOTTOM
								APPEND BLANK
								REPLACE UCRSPRODUTOSCLI.REF			WITH ALLTRIM(UCRSPESQST.REF)
								REPLACE UCRSPRODUTOSCLI.DESIGN 		WITH ALLTRIM(UCRSPESQST.DESIGN)
								REPLACE UCRSPRODUTOSCLI.FAMINOME 	WITH ALLTRIM(UCRSPESQST.FAMINOME)
							ENDSCAN
						ENDIF
					ELSE

						SELECT UCRSPRODUTOSCLI
						SCAN FOR ALLTRIM(UCRSPRODUTOSCLI.REF) == ALLTRIM(UCRSPESQST.REF)
							DELETE
						ENDSCAN
						SELECT UCRSPRODUTOSCLI
						GO TOP
					ENDIF
					
					PRODSELCLI.REFRESH
				
				ENDIF
					
			OTHERWISE 
				**
		ENDCASE

	ELSE
		&& aplica sele��o
		REPLACE ALL sel WITH .f. IN ucrspesqst
		
*!*			IF  myOrigemPPST == "CONSOLIDACAOCOMPRAS"
*!*				rec = RECCOUNT("UCRSPESQST")
*!*				i = 1
*!*				regua(1,rec,"A Remover Refer�ncias...")
*!*					SELECT UCRSPESQST
*!*					GO TOP
*!*					
*!*					SCAN
*!*						regua(1,i,"A Remover Refer�ncias...")
*!*						uf_pesqstocks_sel_to_consolidacaoCompras()
*!*						i=i+1
*!*						SELECT UCRSPESQST
*!*					ENDSCAN
*!*					
*!*					SELECT UCRSPESQST
*!*					GO TOP
*!*					regua(2)
*!*			ENDIF
		
		&& altera o botao
		PESQSTOCKS.menu1.selTodos.config("Sel. Todos", myPath + "\imagens\icons\unchecked_w.png", "uf_pesqstocks_selTodos with .t.","")
	ENDIF
	
	SELECT uCrspesqst
	GO TOP
ENDFUNC


**
FUNCTION uf_pesqstocks_multiSel
	IF pesqstocks.menu1.multiSel.tag == 'false'
	   pesqstocks.menu1.multiSel.tag = 'true'
		
	   && altera o botao
	   pesqstocks.menu1.multiSel.config("Multi Sel.", myPath + "\imagens\icons\checked_w.png", " uf_pesqstocks_multiSel","")
	   pesqstocks.menu1.estado("seltodos", "SHOW")
	ELSE
	   && aplica sele��o
	   pesqstocks.menu1.multiSel.tag = 'false'
		
	   && altera o botao
       pesqstocks.menu1.multiSel.config("Multi Sel.", myPath + "\imagens\icons\unchecked_w.png", " uf_pesqstocks_multiSel","")
		pesqstocks.menu1.estado("seltodos", "HIDE")
		
	ENDIF
ENDFUNC 


** Move selecc��o para a linha anterior / seguinte 
FUNCTION uf_pesqstocks_moverlinha
	LPARAMETERS lcModo
	
	Select ucrspesqst
	
	IF lcModo
		pesqstocks.grdpesq.setfocus
		KEYBOARD '{PGUP}'
	ELSE
		pesqstocks.grdpesq.setfocus
		KEYBOARD '{PGDN}'
	ENDIF
	 
	&& coloca o foco na grelha
	pesqstocks.grdpesq.setfocus
ENDFUNC


**
FUNCTION uf_pesqStocks_topofundo
	LPARAMETERS tcBool
	
	IF USED("ucrspesqst")
		IF RECCOUNT("ucrspesqst")>0
			SELECT ucrspesqst
			
			IF tcBool
				GO top
			ELSE
				GO bottom
			ENDIF
		ENDIF
		
		pesqstocks.grdPesq.refresh
	ENDIF
	
ENDFUNC


**
FUNCTION uf_pesqstocks_configPainel
	
	lcTipoCliente = uf_gerais_getParameter("ADM0000000082","TEXT")

	IF lcTipoCliente == "CLINICA"
		pesqstocks.pageframe1.activepage = 2
	ELSE
		pesqstocks.pageframe1.activepage = 1
	ENDIF 	
			
	** Abre container FML, caso pedido
	IF myOrigemPPST == "STOCKS_ALTERACOES"
		pesqstocks.container2.Visible = .t.
	ENDIF

ENDFUNC


**
FUNCTION uf_pesqstocks_criarFicha
	IF myStocksIntroducao OR myStocksAlteracao
		*uf_perguntalt_chama("O painel de stocks encontra-se em modo de Introdu��o ou Edi��o." + CHR(13) + "Por favor verifique.","OK","",48)
		uf_pesqstocks_sair()
		stocks.show()
	ELSE
		uf_pesqstocks_sair()
		uf_stocks_chama('')
		uf_stocks_novo()
	ENDIF 
ENDFUNC 


** leitura de c�digos de barras com o scanner 
FUNCTION uf_pesqstocks_scanner
	
	pesqstocks.txtscanner.Value = strtran(pesqstocks.txtscanner.Value, chr(39), '')
	
	&& valida preenchimento
	If Empty(alltrim(pesqstocks.txtscanner.value))
		RETURN .F.
	ENDIF
		
	Local lcRef, lcUtente
	Local lcValida, lcValidaVal, lcValidaValRef, lcValidaCL
	Store .f. To lcValida, lcValidaVal, lcValidaValRef, lcValidaCL
	Store 0 To lcRef, lcUtente
	
	lcRef	= UPPER(alltrim(pesqstocks.txtscanner.value))
	
	DO CASE
	
*!*			&&Ref
*!*			CASE (Len(lcRef) == 7) OR (Len(lcRef) == 13) 
*!*			
*!*				
			
		&&CNPEM

		CASE !EMPTY(lcRef) AND AT(" ", ALLTRIM(lcRef)) = 0 AND LEN(ALLTRIM(lcRef)) > 20

			uv_ref = uf_gerais_getRefFromQR(ALLTRIM(lcRef))

            IF !EMPTY(uv_ref)

    			DO CASE 
	    			CASE pesqstocks.Pageframe1.ActivePage == 1
		    			pesqstocks.Pageframe1.Page1.ref.SetFocus()
			    		pesqstocks.Pageframe1.Page1.ref.value = uv_ref
			    ENDCASE 	
			
			    pesqstocks.menu1.actualizar.click			

            ENDIF

		CASE (Len(lcRef) == 8) And (Left(lcRef,1) == "5")

			DO CASE 
				CASE pesqstocks.Pageframe1.ActivePage == 1
					pesqstocks.Pageframe1.Page1.ref.SetFocus()
					pesqstocks.Pageframe1.Page1.ref.value = lcRef
			ENDCASE 	
			
			pesqstocks.menu1.actualizar.click
		OTHERWISE
			DO CASE 
				CASE pesqstocks.Pageframe1.ActivePage == 1
					pesqstocks.Pageframe1.Page1.ref.SetFocus()
					pesqstocks.Pageframe1.Page1.ref.value = lcRef
			ENDCASE 	
			
			pesqstocks.menu1.actualizar.click
	ENDCASE
		
ENDFUNC


**
FUNCTION uf_PESQSTOCKS_validadeschama
	uf_pesqstocks_sair()
	DO CASE
		CASE !TYPE("PESQSTOCKS") == "U" 
			IF !uf_gerais_validaPermUser(ch_userno, ch_grupo,'Gest�o de Stock - Gest�o de Validades')
				uf_perguntalt_chama("O SEU PERFIL N�O TEM PERMISS�ES PARA ACEDER A ESTA FUNCIONALIDADE.","OK","",48)
				RETURN .f.
			ENDIF
	ENDCASE
	uf_validades_chama('ST', '')
ENDFUNC 


**
FUNCTION uf_PESQSTOCKS_confpvpchama
	uf_confpvp_chama('PESQ_STOCKS')
	uf_pesqstocks_sair()
ENDFUNC 


**
FUNCTION uf_PESQSTOCKS_inventariochama
	uf_pesqstocks_sair()
	uf_inventario_chama('')
ENDFUNC


** funcao utilizada para chamar painel de altera��es em grupo
FUNCTION uf_PESQSTOCKS_altprodchama
	
	DO CASE
		CASE !TYPE("PESQSTOCKS") == "U" 
			IF !uf_gerais_validaPermUser(ch_userno, ch_grupo,'Altera��o em grupo de stocks ')
				uf_perguntalt_chama("O SEU PERFIL N�O TEM PERMISS�ES PARA ACEDER A ESTA FUNCIONALIDADE.","OK","",48)
				RETURN .f.
			ENDIF
	ENDCASE
	
	uf_pesqstocks_chama('STOCKS_ALTERACOES')
	
	myOrigemPPST = "STOCKS_ALTERACOES"
	
	pesqstocks.container2.Visible = .t.

	uf_pesqstocks_alternaMenu()

	IF  pesqstocks.container2.Visible == .t.
		pesqstocks.grdPesq.Height = pesqstocks.Height -270
	ELSE
		pesqstocks.grdPesq.Height = pesqstocks.Height -140
	ENDIF	

   pesqstocks.menu_opcoes.estado("altGrupo", "HIDE")

ENDFUNC


**
FUNCTION uf_pesqstocks_hst

	IF pesqstocks.Pageframe1.Page1.ChkHst.tag == "true"
		WITH pesqstocks.grdPesq
			FOR i=1 TO .columnCount
				IF .Columns(i).name == "histDias"
					.Columns(i).visible = .t.
					.Columns(i).width = 50
				ENDIF
			ENDFOR
		ENDWITH

		**uf_pesqstocks_atualizaHst()
		uf_pesqstocks_atualizaHstDias()
		SELECT uCrsPesqST
		GO TOP 	
	ELSE
		WITH pesqstocks.grdPesq
			FOR i=1 TO .columnCount
				IF .Columns(i).name == "histDias"
					.Columns(i).visible = .f.
				ENDIF
			ENDFOR
		ENDWITH
	ENDIF 
	
ENDFUNC 


**
FUNCTION uf_pesqstocks_atualizaHst
	LOCAL lcFtNo, lcFtEstab
	SELECT FT 
	lcFtNo = ft.no
	lcFtEstab = ft.estab
	
	IF EMPTY(lcFTno)
		RETURN .f.
	ENDIF
	IF lcFTno == 200
		RETURN .f.
	ENDIF 
	
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		SELECT distinct
			ref 
		from
			fi (nolock) 
			inner join ft (nolock) on fi.ftstamp = ft.ftstamp 
		where
			ft.no = <<lcFtNo>> and ft.estab = <<lcFtEstab>>
			and ftano > YEAR(dateadd(HOUR, <<difhoraria>>, getdate()))-3
			and fi.ref != ''
	ENDTEXT
	
	IF !uf_gerais_actGrelha("", "ucrsHstCli", lcSQL)
		uf_perguntalt_chama("N�O � POSSIVEL VERIFICAR HIST�RICO DE VENDAS.","OK","",64)
		RETURN .f.
	ENDIF
	
	UPDATE uCrsPesqST SET hst = .t. from uCrsPesqST inner join ucrsHstCli on ALLTRIM(ucrsHstCli.ref) == ALLTRIM(uCrsPesqST.ref)
	
	SELECT uCrsPesqST 
	GO Top
	
	IF USED("ucrsHstCli")
		fecha("ucrsHstCli")
	ENDIF 
ENDFUNC 


** funcao p importar produtos nas altera��es em grupo
FUNCTION uf_pesqStocks_importXls
	LOCAL lcFileName, lcFileExt, nrRegistos, lcSQL
	STORE 0 TO nrRegistos 
	STORE '' TO lcSQL

	uf_perguntalt_chama("O documento deve estar no formato .XLS e conter uma coluna com o nome REF, com as refer�ncias a importar. O nome do ficheiro n�o deve conter espa�os.","OK","",64)	
	
	lcFileName = uf_gerais_getFile(ALLTRIM(uf_gerais_getUmValor("B_terminal", "pathReport", "terminal = '" + ALLTRIM(myTerm) + "'")), 'xls')

	IF EMPTY(lcFileName)
		uf_perguntalt_chama("Deve especificar o ficheiro de importa��o. Por favor verifique.", "OK", "", 64)
		RETURN .f.
	ENDIF

	&& Valida ficheiro
	lcLocal = ["] + ALLTRIM(lcFileName) + ["]
	IF !FILE(lcLocal)
		uf_perguntalt_chama("A localiza��o especificada para importa��o do ficheiro � inv�lida. Por favor verifique.", "OK", "", 64)
		RETURN .f.
	ENDIF
	
	&& Informa��o do ficheiro 

	IF used("uCrsTempFile")
		fecha("uCrsTempFile")
	ENDIF

	&& guardar local de importa��o para o caso do fx do sifarma
	PUBLIC myLocalLt
	
	&& cursor auxiliar p convert ref
	create cursor uCrsTempFile (ref c(18), site c(20))
	
	&& cursor uCrsPesqST p garantir que ambos os cursores t�m o mesmo n� de campos
	lcSQL = ''
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_stocks_pesquisa 1, 'xxx999', '', '', '', '', 0, 'maior', -999999, -1
				, 0, <<myArmazem>>, 0, 0, '', <<mySite_nr>>, 0, 1, 0, -1, ''
				, 0, '', '',''
	ENDTEXT 
	IF !uf_gerais_actGrelha("pesqstocks.grdPesq", "uCrsPesqst", lcSql)
		uf_perguntalt_chama("N�O EFETUAR A OPERA��O. POR FAVOR CONTACTE O SUPORTE.","OK","", 16)
		RETURN .f.
	ENDIF
	pesqstocks.lblRegistos.caption = ALLTRIM(STR(RECCOUNT("uCrsPesqST"))) + " registos"
	
	&& definir pagina onde est�o ficheiros
		
	&& Valida extensoes dos ficheiros
	lcFileExt = UPPER(RIGHT(ALLTRIM(lcFileName),3))
	DO CASE
	
		&& ficheiro do tipo XLS
		CASE lcFileExt == "XLS"
			
			&& Ler o xls para um cursor
			IF !uf_gerais_xls2Cursor(lcFileName, "", "uCrsAuxProdutos", .T.)
				uf_perguntalt_chama("N�o foi poss�vel ler o ficheiro indicado. Por favor contacte o Suporte.","OK","",64)	
				RETURN .f.
			ENDIF
		
			&& verifica se todos os campos est�o ok
			SELECT uCrsAuxProdutos
			IF TYPE('uCrsAuxProdutos.ref') = "U" 
				uf_perguntalt_chama("O nome das colunas no ficheiro n�o � o correto. Por favor valide.", "OK", "", 64)
				RETURN  .f.
			ENDIF
			
			nrRegistos = reccount("uCrsAuxProdutos")
			
			regua(0, nrRegistos, "A processar importa��o do ficheiro...")

			SELECT uCrsAuxProdutos
			GO TOP 
			SCAN FOR !EMPTY(uCrsAuxProdutos.ref) AND !ISNULL(uCrsAuxProdutos.ref)
				
				SELECT uCrsTempFile 
				APPEND BLANK 
			
				IF TYPE('uCrsAuxProdutos.ref') == "N"
					replace uCrsTempFile.ref WITH astr(uCrsAuxProdutos.ref)
				ELSE 
					replace uCrsTempFile.ref WITH uCrsAuxProdutos.ref
				ENDIF 
				
				replace uCrsTempFile.site WITH mySite
			
			
				&& atualiza regua 
				regua(1, RECNO("uCrsAuxProdutos"), "A verificar produtos") 
			
				** Verifica se a referencia existe
				lcSQL = ''
				TEXT TO lcSQL TEXTMERGE NOSHOW
					exec up_stocks_pesquisa_import_bulk_ref 1, '<<ALLTRIM(uCrsTempFile.ref)>>', '', '', '', '', 0, 'maior', -999999, -1
							, 0, <<myArmazem>>, 0, 0, '', <<mySite_nr>>, 0, 1, 0, -1, ''
							, 0, '', '',''
				ENDTEXT 

				IF !uf_gerais_actGrelha("", "uCrsAuxRef", lcSql)
					uf_perguntalt_chama("N�O FOI POSSIVEL VERIFICAR A INFORMA��O DO ARTIGO:" + STR(uCrsDados.ref) + ". POR FAVOR CONTACTE O SUPORTE.","OK","", 16)
					RETURN .f.
				ENDIF

				&& insere linhas
				IF RECCOUNT("uCrsAuxRef") > 0

					SELECT uCrsPesqst
					INSERT INTO uCrsPesqst SELECT * FROM uCrsAuxRef
				
				ENDIF

				select uCrsAuxProdutos
			ENDSCAN
		OTHERWISE
		
			uf_perguntalt_chama("Tipo de Ficheiro inv�lido. Deve indicar um ficheiro do tipo .XLS.", "OK", "", 32)
			RETURN .f.
	ENDCASE

	&& 
	regua(2)
	
	uf_perguntalt_chama("Importa��o conclu�da com sucesso.","OK","",64)	

	&& no de registos e autofit
	SELECT uCrsPesqST
	GO TOP 
	
	pesqstocks.grdPesq.refresh
	
	pesqstocks.lblRegistos.caption = ALLTRIM(STR(RECCOUNT("uCrsPesqST"))) + " registos"
	pesqstocks.grdPesq.autofit
	
	&& fecha cursores utilizados
	IF USED("uCrsTempFile")
		fecha("uCrsTempFile")
	ENDIF

	IF USED("uCrsAuxProdutos")
		fecha("uCrsAuxProdutos")
	ENDIF	
	
	IF USED("uCrsAuxRef")
		fecha("uCrsAuxRef")
	ENDIF	

ENDFUNC 


** consultar informa��o do produto no fornecedor
FUNCTION uf_pesqStocks_verFornec	
	
	LOCAL lcObj
	DO CASE 
		CASE pesqstocks.pageframe1.activePage == 1 && n�o touch
			lcObj = "pesqstocks.pageframe1.page1"
		CASE pesqstocks.pageframe1.activePage == 2
			lcObj = "pesqstocks.pageframe1.page2"
	ENDCASE 

	&& Trata parametros
	lcRef = ALLTRIM(&lcObj..ref.value)

	&& n�o permite efetuar pedidos sem ref ou design preenchida
	IF EMPTY(lcRef)	
		uf_perguntalt_chama("DEVE PREENCHER O CAMPO ARTIGO/SERVI�O PARA EFETUAR O PEDIDO DE INFORMA��O AO FORNECEDOR. OBRIGADO.","OK","", 16)
		RETURN .f.			
	ENDIF

	&& Guardar caminho do software de envio
	LOCAL  lcWsPath, lcIDCliente, lcNomeJar, lcToken, lcInfForn, lcSearchType
	STORE '' TO lcWsPath, lcIDCliente, lcNomeJar, lcToken, lcInfForn, lcSearchType
	
	&& 
	lcToken = uf_gerais_stamp()
	
	&& id do cliente LTS
	SELECT uCrsE1
	IF EMPTY(ALLTRIM(uCrsE1.id_lt))
		uf_perguntalt_chama("O C�DIGO DE CLIENTE LOGITOOLS N�O EST� CORRETAMENTE PREENCHIDO. POR FAVOR CONTACTE O SUPORTE.","OK","", 16)
		RETURN .f.
	ENDIF 
	lcIDCliente = ALLTRIM(uCrsE1.id_lt)
	
	&& valida e configura software p envio encomenda
	lcNomeJar = 'LTSESBProductsClient.jar'
	IF !FILE(ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\LTSESBProductsClient\' + ALLTRIM(lcNomeJar))
		uf_perguntalt_chama("O SOFTWARE DE ENVIO LTS ESB N�O EST� CONFIGURADO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	ENDIF 
	
	regua(1,2,"A efetuar a pesquisa no fornecedor, por favor aguarde.")
	
	&& Define de usa testes ou n�o 
	&& verifica se utiliza webservice de testes
	LOCAL lcDefineWsTest
	STORE 0 TO lcDefineWsTest
	IF uf_gerais_getParameter("ADM0000000227","BOOL") == .f.
		lcDefineWsTest = 0
	ELSE
		lcDefineWsTest = 1
	ENDIF 
	
	&& define o tipo de consulta
	IF ISDIGIT(lcRef)
		 lcSearchType = 'Ref'
	ELSE 
		 lcSearchType = 'Descr'
	ENDIF 
			
	lcWsPath = ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\LTSESBProductsClient\' + ALLTRIM(lcNomeJar);
		+ ' "--searchEntity=' + 'OCP' + ["];
		+ ' "--searchType=' + lcSearchType  + ["];
		+ ' "--searchString=' + ALLTRIM(lcRef) + ["];
		+ ' "--idCl=' + lcIDCliente + ["];
		+ ' "--site=' + UPPER(ALLTRIM(mySite)) + ["];
		+ ' "--Token=' + ALLTRIM(lcToken) + ["]

	&& Envia comando Java		
	lcWsPath = "javaw -jar " + lcWsPath 		
	
	&& aguarda que software termine o envio da informa��o 3� PARAMETRO = .T.
	oWSShell = CREATEOBJECT("WScript.Shell")
	oWSShell.Run(lcWsPath, 1, .t.)
	
	regua(1,3,"A atualizar informa��o, por favor aguarde.")
			
	&& configura grelha de acordo com a informa��o que deve ser visivel
	pesqstocks.grdPesq.StockEmpresa.header1.caption = "Estado"
	pesqstocks.grdPesq.epv1.header1.caption = "PVPF"

	&& show pctf column
	pesqstocks.grdPesq.pctf.Visible = .t.					
	
	&& mostra informa��o retornada pelo fornecedor. 
	lcSQL = ''
	TEXT TO lcSQL TEXTMERGE NOSHOW 
		exec up_stocks_pesquisaESB
			<<250>>
			,'<<lcRef>>'
			,''
			,''
			,''
			,''
			,0
			,''
			,0
			,0
			,0
			,<<myArmazem>>
			,0
			,0
			,''
			,<<mySite_nr>>
			,0
			,0
			,0
			,-1
			,''
			,0
			,''
			,''
			,'<<ALLTRIM(lcToken)>>'
	ENDTEXT		
	IF !uf_gerais_actGrelha("pesqstocks.grdPesq", "uCrsPesqST", lcSQL)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VERIFICAR A RESPOSTA DO FORNECEDOR. POR FAVOR CONTACTE O SUPORTE.", "Ok", "", 16)
		regua(2)
		RETURN .f.
	ELSE		
		pesqstocks.lblRegistos.caption = ALLTRIM(STR(RECCOUNT("uCrsPesqST"))) + " registos"
		pesqstocks.grdPesq.autofit

		** Coloca Cursor na Coluna de selecc��o de Produto
		uf_pesqstocks_grid_setFocus()

		regua(2)
		uf_perguntalt_chama("PESQUISA EFETUADA COM SUCESSO.", "Ok", "", 64)
	ENDIF

ENDFUNC 


FUNCTION uf_altgr_escolheEmpresas
fecha("ucrsConfEmpresas")
	IF vartype(ucrsEmpresasSel)=="U" THEN 
		** Listam de Empresas
		IF USED("ucrsConfEmpresas")
			SELECT * FROM ucrsConfEmpresas INTO CURSOR ucrsConfEmpresasAux READWRITE 
		ENDIF 
		
		IF !USED("ucrsConfEmpresas")
			TEXT To lcSQL TEXTMERGE NOSHOW
				exec up_gerais_trocaEmpresa <<ch_userno>>,'<<ALLTRIM(ch_grupo)>>'
			ENDTEXT 
			uf_gerais_actGrelha("","ucrsListSiteAux",lcSQL)
			SELECT .f. as selEmpresa,* FROM ucrsListSiteAux INTO CURSOR ucrsConfEmpresas READWRITE 
		ENDIF 

		IF USED("ucrsConfEmpresasAux")	
			SELECT ucrsConfEmpresas
			GO TOP
			SCAN 
				SELECT ucrsConfEmpresasAux	
				LOCATE FOR ALLTRIM(UPPER(ucrsConfEmpresas.local)) == ALLTRIM(UPPER(ucrsConfEmpresasAux.local))
				IF FOUND()
					Replace ucrsConfEmpresas.selEmpresa WITH ucrsConfEmpresasAux.selEmpresa
				ENDIF 
			ENDSCAN 
			
		ENDIF 

		SELECT ucrsConfEmpresas
		GO top
		SCAN
			IF ucrsConfEmpresas.siteno=mySite_nr then
				replace ucrsConfEmpresas.selempresa WITH .t.
			ENDIF 
		ENDSCAN 
	
		uf_valorescombo_chama("ucrsConfEmpresas", 6, "ucrsConfEmpresas", 2, "LOCAL", "LOCAL",.t.,.f.,.t.,.t.)
	ELSE
		uf_valorescombo_chama("ucrsConfEmpresas", 6, "ucrsConfEmpresas", 3, "LOCAL", "LOCAL",.t.,.f.,.t.,.t.)
	ENDIF 

ENDFUNC 


FUNCTION uf_pesqStocks_configgrelha
	IF uf_gerais_getParameter_site('ADM0000000058', 'BOOL', mySite) = .t. AND !EMPTY(uf_gerais_getParameter_site('ADM0000000058', 'TEXT', mySite))
		
			Local ti, i
			STORE 0 TO ti, i	
			
			PUBLIC lcQttVerm, lcQttAmarMax, lcQttAmarMin , lcQttVerde
			lcQttVerm = VAL(uf_gerais_getParameter_site('ADM0000000057', 'TEXT', mySite))
			lcQttAmarMin = val(substr(uf_gerais_getParameter_site('ADM0000000058', 'TEXT', mySite),1, at(" ", uf_gerais_getParameter_site('ADM0000000058', 'TEXT', mySite))))
			lcQttAmarMax = val(substr(uf_gerais_getParameter_site('ADM0000000058', 'TEXT', mySite),at(" ", uf_gerais_getParameter_site('ADM0000000058', 'TEXT', mySite)), 2))
			lcQttVerde = VAL(uf_gerais_getParameter_site('ADM0000000059', 'TEXT', mySite))
			ti = pesqstocks.grdPesq.columnCount
			FOR i=1 TO ti
				If Upper(ALLTRIM((pesqstocks.grdPesq.columns(i).name)))=="STOCK"
					IF !EMPTY(uCrsPesqST.ref) 
						**pesqstocks.grdPesq.columns(i).dynamicBackColor = "IIF(uCrsPesqST.stock <= lcQttVerm , rgb[255,0,0], (IIF(uCrsPesqST.stock >= lcQttVerde ,rgb[0,255,0], rgb[255,128,0])) )"
						pesqstocks.grdPesq.columns(i).dynamicBackColor= "IIF(!ISNULL(uCrsPesqST.stock), IIF(uCrsPesqST.stock < lcQttAmarMin , rgb[255,0,0], (IIF(uCrsPesqST.stock > lcQttAmarMax ,rgb[0,255,0], rgb[255,128,0]))), rgb[255,255,255])"
						pesqstocks.grdPesq.columns(i).text1.backColor= IIF(uCrsPesqST.stock < lcQttAmarMin , rgb[255,0,0], (IIF(uCrsPesqST.stock > lcQttAmarMax ,rgb[0,255,0], rgb[255,128,0])) )
						
						IF uf_gerais_getParameter_site('ADM0000000060', 'BOOL', mySite) AND ALLTRIM(ch_grupo) <> 'Administrador' AND ALLTRIM(ch_grupo) <> 'Supervisores' AND ALLTRIM(ch_grupo) <> 'Administrativo' 
							pesqstocks.grdPesq.columns(i).foreColor= IIF(!ISNULL(uCrsPesqST.stock), IIF(uCrsPesqST.stock < lcQttAmarMin , rgb[255,0,0], (IIF(uCrsPesqST.stock > lcQttAmarMax ,rgb[0,255,0], rgb[255,128,0])) ), rgb[255,255,255])
							pesqstocks.grdPesq.columns(i).dynamicForeColor= "IIF(!ISNULL(uCrsPesqST.stock), IIF(uCrsPesqST.stock < lcQttAmarMin , rgb[255,0,0], (IIF(uCrsPesqST.stock > lcQttAmarMax ,rgb[0,255,0], rgb[255,128,0]))), rgb[255,255,255])"
							pesqstocks.grdPesq.columns(i).text1.inputmask = 'XXXXXXX'
						ENDIF 
					ENDIF 
				ENDIF 
**				stockPorArmazem.grid1.columns(i).dynamicBackColor = "IIF(uCrsStSa.stock <= lcQttVerm , rgb[255,0,0], (IIF(uCrsStSa.stock >= lcQttVerde ,rgb[0,255,0], rgb[255,128,0])) )"
	*!*				If Upper(alltrim(stockPorArmazem.grid1.Columns(i).name))=="STOCK" AND uf_gerais_getParameter_site('ADM0000000060', 'BOOL', mySite) AND ALLTRIM(ch_grupo) <> 'Administrador'
	*!*					stockPorArmazem.grid1.columns(i).dynamicForeColor = "IIF(uCrsStSa.stock1 <= lcQttVerm , rgb[255,0,0], (IIF(uCrsStSa.stock1 >= lcQttVerde ,rgb[0,128,0], rgb[255,128,0])) )"
	*!*				ENDIF 	
			ENDFOR

	ENDIF
	
	IF TYPE("pesqstocks") != "U" 
		pesqstocks.grdPesq.refresh
		IF pesqstocks.pageframe1.activepage = 1
			pesqstocks.pageframe1.page1.ref.setfocus
		ENDIF
	ENDIF 

ENDFUNC 

PROCEDURE uf_pesqstocks_atualizaHstDias

	LOCAL uv_FtNo, uv_FtEstab

   SELECT uCrsPesqST 

   UPDATE uCrsPesqST SET histDias = '', histDiasOrdem = 9999999999 WHERE 1=1 and !DELETED()

	SELECT uCrsPesqST 
	GO TOP
	
	SELECT FT 
	
	uv_FtNo = ft.no
	uv_FtEstab = ft.estab
	
	IF EMPTY(uv_FTno) OR uv_FTno == 200
		RETURN .F.
	ENDIF

	TEXT TO lcSQL NOSHOW TEXTMERGE 
		EXEC up_stocks_histDias <<ASTR(uv_ftNo)>>, <<ASTR(uv_ftEstab)>>, <<ASTR(difhoraria)>>
	ENDTEXT

	IF !uf_gerais_actGrelha("", "uc_HstCli", lcSQL)
	
		uf_perguntalt_chama("N�O � POSSIVEL VERIFICAR HIST�RICO DE VENDAS.","OK","",64)
			
		RETURN .F.
		
	ENDIF
	
	SELECT uc_HstCli

	IF RECCOUNT("uc_HstCli") <> 0
	
		UPDATE uCrsPesqST SET histDias  = uc_HstCli.lastBuy, histDiasOrdem = IIF(!EMPTY(uc_HstCli.lastBuy),VAL(uc_HstCli.lastBuy), 9999999999) from uCrsPesqST inner join uc_HstCli on ALLTRIM(uc_HstCli.ref) == ALLTRIM(uCrsPesqST.ref) WHERE !DELETED()
    	UPDATE uCrsPesqST SET hist = histDiasOrdem WHERE !DELETED()
		
	ENDIF

	IF USED("uc_HstCli")
		fecha("uc_HstCli")
	ENDIF 

	&&uf_pesqstocks_grid_setFocus()
ENDPROC


FUNCTION uf_pesqstocks_grid_setFocus
			
	regua(1,9,"A carregar produtos...")
	WAIT WINDOW "" TIMEOUT 1
	IF(USED("uCrsPesqST") and TYPE("PESQSTOCKS") != "U" )
		SELECT uCrsPesqST
		GO TOP 	
		IF RECCOUNT("uCrsPesqST") > 0
			pesqstocks.grdPesq.setfocus
			pesqstocks.grdpesq.activatecell(1,1)
			pesqstocks.grdPesq.refresh
			pesqstocks.grdPesq.sel.setfocus	
		ELSE
			IF pesqstocks.pageframe1.activepage = 1	
				pesqstocks.pageframe1.page1.ref.setfocus
			ENDIF
			WAIT WINDOW "" TIMEOUT 1
		ENDIF
	ENDIF

	regua(2)

ENDFUNC



PROCEDURE uf_pesqstocks_ordenargrelhaAtendimento
		IF USED("uCrsPesqST") AND RECCOUNT("uCrsPesqST")>0
	
		LOCAL loGrid ,lcCursor, lcOrdenacao
		STORE '' TO lcOrdenacao
		
		IF uf_gerais_getParameter_site('ADM0000000108', 'BOOL', mySite) = .T.
			lcOrdenacao = uf_gerais_getParameter_site('ADM0000000108', 'TEXT', mySite)
		ENDIF
		
		IF !EMPTY(lcOrdenacao)

			loGrid = PESQSTOCKS.grdPesq
		    lcCursor = "uCrsPesqST"

			lnColumns = loGrid.ColumnCount
			
			IF lnColumns > 0
			   DIMENSION laControlSource[lnColumns]
			   FOR lnColumn = 1 TO lnColumns
				   laControlSource[lnColumn] = loGrid.Columns[lnColumn].ControlSource
			   ENDFOR

		    ENDIF

		    loGrid.RecordSource = ""

            
		    
			 SELECT * FROM uCrsPesqST with (buffering=.t.)  ORDER BY uCrsPesqST.marcada desc, &lcOrdenacao. into cursor uCrsPesqST  readwrite
		 	
			 SELECT uCrsPesqST 
			 GO TOP
			 
			   loGrid.RecordSource = lcCursor
		
			   FOR lnColumn = 1 TO lnColumns
		   		loGrid.Columns[lnColumn].ControlSource = laControlSource[lnColumn]
			   ENDFOR
			
			PESQSTOCKS.grdPesq.refresh
			PESQSTOCKS.grdPesq.setfocus
			
			ENDIF 
		ENDIF

ENDPROC

FUNCTION uf_pesqstocks_regfalta
	LPARAMETERS lcRefFalta, lnQtt
	
	IF EMPTY(lcRefFalta)
		uf_perguntalt_chama("Tem que selecionar um artigo para registar","OK","",16)
		RETURN .f.
	ENDIF 
	
	
	IF(EMPTY(lnQtt))
		lnQtt = 1
	ENDIF
	
	LOCAL lcrefins
	lcrefins = ALLTRIM(lcRefFalta)
	
	lcSQL = ''
	TEXT TO lcSQL NOSHOW TEXTMERGE
		select TOP 1 bostamp, obrano from bo (nolock) where  site='<<ALLTRIM(mySite)>>' AND ndos=48 and CONVERT(varchar, dataobra, 112)=CONVERT(varchar, GETDATE(), 112) ORDER BY obrano desc
	ENDTEXT 
	IF !uf_gerais_actGrelha("","ucrsregfaltasdia",lcSQL)
		uf_perguntalt_chama("O software n�o conseguiu verificar o documento de registo de faltas para hoje. Por favor contacte o suporte.","OK","",16)
		RETURN .f.
	ELSE
		LOCAL lcSQLINSBO , lcSQLBI 
		lcSQL = ''
		lcSQLBI = ''
		lcSQLINSBO = ''
		SELECT ucrsregfaltasdia
		LOCAL lcbostampragfalta, lcobranoregfalta
		IF reccount("ucrsregfaltasdia")=0 &&OR EMPTY(ucrsregfaltasdia.bostamp)
			LOCAL rf_newStamp 
			rf_newStamp = uf_gerais_stamp()
			lcbostampragfalta = ALLTRIM(rf_newStamp)
			
			lcSQL = ''
			TEXT TO lcSQL NOSHOW TEXTMERGE
				select isnull(MAX(obrano),0)+1 as nextno from bo (nolock) where ndos=48 and boano=YEAR(GETDATE())
			ENDTEXT 
			uf_gerais_actGrelha("","ucrsnextno",lcSQL)
			SELECT ucrsnextno
			lcobranoregfalta = ucrsnextno.nextno
			fecha("ucrsnextno")
			
			SELECT ucrsregfaltasdia
			
			LOCAL lcmoedadoc
			lcmoedadoc = ALLTRIM(uf_gerais_getparameter("ADM0000000260", "text"))
			
			LOCAL lctaxa1, lctaxa2, lctaxa3, lctaxa4, lctaxa5, lctaxa6, lctaxa7, lctaxa8, lctaxa9, lctaxa10, lctaxa11, lctaxa12
			lctaxa1 = uf_gerais_getTabelaIVA(1,"TAXA")
			lctaxa2 = uf_gerais_getTabelaIVA(2,"TAXA")
			lctaxa3 = uf_gerais_getTabelaIVA(3,"TAXA")
			lctaxa4 = uf_gerais_getTabelaIVA(4,"TAXA")
			lctaxa5 = uf_gerais_getTabelaIVA(5,"TAXA")
			lctaxa6 = uf_gerais_getTabelaIVA(6,"TAXA")
			lctaxa7 = uf_gerais_getTabelaIVA(7,"TAXA")
			lctaxa8 = uf_gerais_getTabelaIVA(8,"TAXA")
			lctaxa9 = uf_gerais_getTabelaIVA(9,"TAXA")
			lctaxa10 = uf_gerais_getTabelaIVA(10,"TAXA")
			lctaxa11 = uf_gerais_getTabelaIVA(11,"TAXA")
			lctaxa12 = uf_gerais_getTabelaIVA(12,"TAXA")
			
			TEXT TO lcSQLINSBO TEXTMERGE NOSHOW
				insert into bo 
					(bostamp
					, nmdos
					, obrano
					, dataobra
					, nome
					, datafinal
					, vendedor
					, vendnm
					, no
					, boano
					, dataopen
					,ndos
					, moeda
					, estab
					, memissao
					, origem
					, site
					, pnome
					, pno
					, ousrinis
					, ousrdata
					, ousrhora
					, usrinis
					, usrdata
					, usrhora)
				select 
					'<<ALLTRIM(rf_newStamp)>>'
					, 'Registo Faltas'
					, <<lcobranoregfalta>>
					, CONVERT(varchar, GETDATE(), 112)
					, ISNULL((select nome from ag (nolock) where no=1),'')
					, CONVERT(varchar, GETDATE(), 112)
					, <<ch_vendedor>>
					, '<<ALLTRIM(ch_vendnm)>>'
					, 1, YEAR(GETDATE())
					, CONVERT(varchar, GETDATE(), 112)
					,48
					, '<<ALLTRIM(lcmoedadoc)>>'
					, 0
					, '<<ALLTRIM(lcmoedadoc)>>'
					, 'BO'
					, '<<ALLTRIM(mySite)>>'
					, '<<ALLTRIM(myTerm)>>'
					, <<myTermNo>>
					, '<<ALLTRIM(m_chinis)>>'
					,convert(varchar,getdate(),102)
					,convert(varchar,getdate(),8)
					, '<<ALLTRIM(m_chinis)>>'
					,convert(varchar,getdate(),102)
					,convert(varchar,getdate(),8)
					
				insert into bo2 
					(bo2stamp
					, ousrinis
					, ousrdata
					, ousrhora
					, usrinis
					, usrdata
					, usrhora
					, armazem
					, IVATX1
					, IVATX2
					, IVATX3
					, IVATX4
					, IVATX5
					, IVATX6
					, IVATX7
					, IVATX8
					, IVATX9
					, IVATX10
					, ivatx11
					, ivatx12)
				select '<<ALLTRIM(rf_newStamp)>>'
					, '<<ALLTRIM(m_chinis)>>'
					, convert(varchar,getdate(),102)
					, convert(varchar,getdate(),8)
					, '<<ALLTRIM(m_chinis)>>'
					, convert(varchar,getdate(),102)
					, convert(varchar,getdate(),8)
					, <<myArmazem>>
					, <<lctaxa1>>
					, <<lctaxa2>>
					, <<lctaxa3>>
					, <<lctaxa4>>
					, <<lctaxa5>>
					, <<lctaxa6>>
					, <<lctaxa7>>
					, <<lctaxa8>>
					, <<lctaxa9>>
					, <<lctaxa10>>
					, <<lctaxa11>>
					, <<lctaxa12>>
			ENDTEXT
			
			IF !uf_gerais_actGrelha("","",lcSQLINSBO)
				uf_perguntalt_chama("OCORREU UM ERRO A GRAVAR O CABE�ALHO DO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				RETURN
			ENDIF 
			
		ELSE
			SELECT ucrsregfaltasdia
			lcbostampragfalta = ALLTRIM(ucrsregfaltasdia.bostamp)
			lcobranoregfalta = ucrsregfaltasdia.obrano
		ENDIF 
		
		TEXT TO lcSQLBI TEXTMERGE NOSHOW
			insert into bi 
				(bistamp
				, nmdos
				, obrano
				, ref
				, design
				, qtt
				, iva
				, tabiva
				, armazem
				, no
				, ndos
				, rdata
				, datafinal
				, dataobra
				, dataopen
				, lordem
				, nome
				, vendedor
				, vendnm
				, codigo
				, familia
				, bostamp
				, ousrinis
				, ousrdata
				, ousrhora
				, usrinis
				, usrdata
				, usrhora
				, ivaincl)
			select 
				LEFT(NEWID(),21)
				, 'Registo Faltas'
				, <<lcobranoregfalta>>
				, ref
				, design
				, <<lnQtt>>
				, taxa
				, tabiva
				, <<myArmazem>>
				, 1
				, 48
				, CONVERT(varchar, GETDATE(), 112)
				, CONVERT(varchar, GETDATE(), 112)
				, CONVERT(varchar, GETDATE(), 112)
				, CONVERT(varchar, GETDATE(), 112)
				, 100
				, isnull((select nome from ag (nolock) where no = 1),'')
				, <<ch_vendedor>>
				, '<<ALLTRIM(ch_vendnm)>>'
				, ref
				, familia
				, '<<ALLTRIM(lcbostampragfalta)>>'
				, '<<ALLTRIM(m_chinis)>>'
				,convert(varchar,getdate(),102)
				,convert(varchar,getdate(),8)
				, '<<ALLTRIM(m_chinis)>>'
				,convert(varchar,getdate(),102)
				,convert(varchar,getdate(),8)
				, 1
			from st (nolock) 
			inner join taxasiva (nolock) on st.tabiva=taxasiva.codigo 
			where 
				ref='<<ALLTRIM(lcrefins)>>' 
				and site_nr=<<mysite_nr>>
		Endtext
		
		**lcSqlI = lcSqlBO + CHR(13) + lcSQLBI 
		 **_cliptext = lcSqlI 
		 
		IF !uf_gerais_actGrelha("","",lcSQLBI )
			uf_perguntalt_chama("OCORREU UM ERRO A GRAVAR AS LINHAS DO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			RETURN
		ELSE
			uf_perguntalt_chama("Produto adicionado ao registo de faltas","OK","",64)
		Endif
	ENDIF 
	
	IF USED("ucrsregfaltasdia")
		fecha("ucrsregfaltasdia")
	ENDIF 
ENDFUNC 
**
PROCEDURE uf_pesqstocks_alternativos
	
	IF(!USED("UCRSPESQST"))
		RETURN .F.
	ENDIF
	
	PUBLIC lccodcnpext
	lccodcnpext = uf_gerais_getUmValor("st","codCNP ","ref = '" + ALLTRIM(UCRSPESQST.ref) + "' AND SITE_NR =" + ASTR(mysite_nr))
	
	uf_principioactivo_chama(ALLTRIM(UCRSPESQST.ref), ALLTRIM(UCRSPESQST.design), ALLTRIM(UCRSPESQST.dci), "DCI", "PESQSTOCKS")
		
ENDPROC

FUNCTION uf_pesqstocks_sel_to_consolidacaoCompras
		
	IF(!USED("UCRSPESQST"))
		RETURN .F.
	ENDIF
	IF(!USED("ucrsConsolidacaoSt"))
		RETURN .F.
	ENDIF
	
	SELECT uCrsPesqST
				
	IF(uCrsPesqST.sel)
		SELECT ucrsConsolidacaoSt
		GO Top
		LOCATE FOR ALLTRIM(ucrsConsolidacaoSt.ref) == ALLTRIM(UCRSPESQST.ref) && N�o permite repetidos
		IF !FOUND()				
			SELECT ucrsConsolidacaoSt
			APPEND BLANK
			Replace ucrsConsolidacaoSt.ref WITH Alltrim(UCRSPESQST.ref)
			Replace ucrsConsolidacaoSt.design WITH Alltrim(UCRSPESQST.design)
			Replace ucrsConsolidacaoSt.lab WITH Alltrim(UCRSPESQST.u_lab)
			Replace ucrsConsolidacaoSt.validado WITH .t.
			
		ENDIF

	ENDIF
	uf_consolidacaocompras_limpar()
ENDFUNC
**
FUNCTION uf_pesqstocks_alteradata
	WITH pesqstocks.container2.PageFrame1.page1	
		.txtDtIni.value = thisform.dataIniPromo
		.txtDtFim.value = thisform.dataFimPromo
	ENDWITH  
ENDFUNC 

FUNCTION uf_altgr_verificarEmpresas
	fecha("ucrsConfEmpresas")
	IF !USED("ucrsConfEmpresas")

		IF !USED("ucrsConfEmpresas")
			fecha("ucrsListSiteAux")
			TEXT To lcSQL TEXTMERGE NOSHOW
				exec up_gerais_trocaEmpresa <<ch_userno>>,'<<ALLTRIM(ch_grupo)>>'
			ENDTEXT 
			uf_gerais_actGrelha("","ucrsListSiteAux",lcSQL)
			SELECT .f. as sel,.f. as selEmpresa,* FROM ucrsListSiteAux INTO CURSOR ucrsConfEmpresas READWRITE 
		ENDIF 

		IF USED("ucrsConfEmpresasAux")	
		SELECT ucrsConfEmpresas
			GO TOP
			SCAN 
				SELECT ucrsConfEmpresasAux	
				LOCATE FOR ALLTRIM(UPPER(ucrsConfEmpresas.local)) == ALLTRIM(UPPER(ucrsConfEmpresasAux.local))
				IF FOUND()
					Replace ucrsConfEmpresas.selEmpresa WITH ucrsConfEmpresasAux.selEmpresa
				ENDIF 
			ENDSCAN 
			
		ENDIF 

		SELECT ucrsConfEmpresas
		GO top
		SCAN
			IF ucrsConfEmpresas.siteno=mySite_nr then
				replace ucrsConfEmpresas.selempresa WITH .t.
				replace ucrsConfEmpresas.sel WITH .t.
			ENDIF 
		ENDSCAN 
	ENDIF
	
	SELECT sel,selempresa, local, designacao, siteno, armazem1 FROM ucrsConfEmpresas INTO CURSOR ucrsEmpresasSel READWRITE 
ENDFUNC


FUNCTION uf_stocks_Corrige_mov_alteracaoGrupo
	LPARAMETERS lcRef, lcSite

	LOCAL lcEPcpond, lcMovEntrada, lcStock, lcQttMov, lcCont, lcCont2, lcMesesAtua  
	STORE 0.00 TO lcEPcpond
	STORE .f. TO lcMovEntrada
	STORE 0 TO lcStock, lcQttMov, lcCont
	STORE 1 TO lcCont2 
	
		lcMesesAtua = uf_gerais_getparameter_site('ADM0000000130', 'NUM', mysite)
		IF lcMesesAtua <= 0
			lcMesesAtua = -1900
		ELSE
			lcMesesAtua = lcMesesAtua  * -1	
		ENDIF
		
		IF uf_gerais_getParameter("ADM0000000301","BOOL")
			TEXT To lcSQL TEXTMERGE NOSHOW
				SELECT sl.*, cm2.mudapcpond FROM sl (NOLOCK) 
				inner join cm2 (nolock) on sl.cm=cm2.cm 
				where ref= '<<ALLTRIM(lcRef)>>'
					and sl.ousrdata >= DATEADD(month, <<lcMesesAtua>> ,GETDATE())
				order by sl.datalc, sl.ousrdata, sl.ousrhora
			ENDTEXT 
		ELSE
			TEXT To lcSQL TEXTMERGE NOSHOW
				SELECT sl.*, cm2.mudapcpond FROM sl (NOLOCK) 
				inner join cm2 (nolock) on sl.cm=cm2.cm 
				where ref='<<ALLTRIM(lcRef)>>' and armazem=<<lcSite>> 
					and sl.ousrdata >= DATEADD(month, <<lcMesesAtua>> ,GETDATE())
				order by sl.datalc, sl.ousrdata, sl.ousrhora
			ENDTEXT 		
		ENDIF 
		uf_gerais_actGrelha("","ucrsListMov",lcSQL)

		IF reccount("ucrsListMov") > 0
			lcCont= reccount("ucrsListMov")
			
			** Fazer o rec�lculo e atualiza��o de valores
&&			regua(0,lcCont,"A Atualizar Documentos...")
			SELECT ucrsListMov
			GO TOP 
			SCAN 
				&&regua(1,lcCont2,"A Atualizar Documentos...")
				lcSQL = ""
				** atualiza��o de movimentos da sl
				TEXT To lcSQL TEXTMERGE NOSHOW
					UPDATE sl SET epcpond=<<lcEPcpond>>, pcpond=<<ROUND(lcEPcpond*200.482,2)>> where slstamp='<<ALLTRIM(ucrsListMov.slstamp)>>'
				ENDTEXT 
				uf_gerais_actGrelha("","",lcSQL)

				SELECT ucrsListMov
				** atualiza��o de movimentos da fi
				IF !EMPTY(ucrsListMov.fistamp)
					TEXT To lcSQL TEXTMERGE NOSHOW
						UPDATE fi SET epcp=<<lcEPcpond>>, pcp=<<ROUND(lcEPcpond*200.482,2)>> where fistamp='<<ALLTRIM(ucrsListMov.fistamp)>>'
					ENDTEXT 
					uf_gerais_actGrelha("","",lcSQL)
				ENDIF 
		
				SELECT ucrsListMov
				** atualiza��o de movimentos da bi
				IF !EMPTY(ucrsListMov.bistamp)
					TEXT To lcSQL TEXTMERGE NOSHOW
						UPDATE bi SET epcusto=<<lcEPcpond>>, pcusto=<<ROUND(lcEPcpond*200.482,2)>> where bistamp='<<ALLTRIM(ucrsListMov.bistamp)>>' and ar2mazem=<<ucrsListMov.armazem>>
					ENDTEXT 
				
					uf_gerais_actGrelha("","",lcSQL)
				ENDIF 
				
				SELECT ucrsListMov
				** atualiza��o de movimentos da fn
				IF !EMPTY(ucrsListMov.fnstamp)
					TEXT To lcSQL TEXTMERGE NOSHOW
						UPDATE fn SET qtt=qtt where fnstamp='<<ALLTRIM(ucrsListMov.fnstamp)>>'
					ENDTEXT 
					uf_gerais_actGrelha("","",lcSQL)
				ENDIF

				SELECT ucrsListMov				
				**IF ALLTRIM(ucrsListMov.cmdesc)='V/Factura' OR ALLTRIM(ucrsListMov.cmdesc)='V/Vd. Dinhei.' OR ALLTRIM(ucrsListMov.cmdesc)='V/Vd. Dinhei.' OR ALLTRIM(ucrsListMov.cmdesc)='V/Guia Transp.'
				IF ucrsListMov.mudapcpond = .t.
					REPLACE ucrsListMov.EPCPOND WITH lcEPcpond
					IF ucrsListMov.cm < 50
						lcQttMov = ucrsListMov.qtt
					ELSE 
						lcQttMov = ucrsListMov.qtt * (-1)
					ENDIF 

					DO CASE 
						CASE (lcStock + lcQttMov ) <= 0 
							lcEPcpond = 0
						case (lcStock + lcQttMov ) > 0
							IF lcStock > 0 then
								**lcEPcpond = ROUND(((lcStock * lcEPcpond)+(ucrsListMov.ett)) / (lcStock  + ucrsListMov.qtt),2)
								lcEPcpond = ROUND(((lcStock * lcEPcpond)+(ucrsListMov.evu * ucrsListMov.qtt)) / (lcStock  + ucrsListMov.qtt),2)
							ELSE
								**lcEPcpond = ROUND(((0 * lcEPcpond)+(ucrsListMov.ett)) / (0  + ucrsListMov.qtt),2)
								lcEPcpond = ROUND(((0 * lcEPcpond)+(ucrsListMov.evu * ucrsListMov.qtt)) / (0  + ucrsListMov.qtt),2)
							ENDIF 
						OTHERWISE
					ENDCASE 
					lcStock  = lcStock + lcQttMov
				ELSE
					REPLACE ucrsListMov.EPCPOND WITH lcEPcpond
					IF ucrsListMov.cm < 50 then
						lcStock = lcStock + ucrsListMov.qtt
					ELSE 
						lcStock = lcStock - ucrsListMov.qtt
					ENDIF 
				ENDIF 
				
				lcCont2 = lcCont2 + 1
				SELECT ucrsListMov
			ENDSCAN 
		
			SELECT uc_tmpST
			IF uf_gerais_getParameter("ADM0000000301","BOOL")
				TEXT To lcSQL TEXTMERGE NOSHOW
					UPDATE 
						st 
					SET 
						epcpond=<<lcEPcpond>>, pcpond=<<ROUND(lcEPcpond*200.482,2)>>
  					from st
  					left join taxasiva on taxasiva.codigo = st.tabiva
					where 
						ref='<<ALLTRIM(lcRef)>>'
				ENDTEXT 
			ELSE
				TEXT To lcSQL TEXTMERGE NOSHOW
					UPDATE 
						st 
					SET 
						epcpond=<<lcEPcpond>>
						, pcpond=<<ROUND(lcEPcpond*200.482,2)>>
  					from st
  					left join taxasiva on taxasiva.codigo = st.tabiva
  					 where 
  					 	ref='<<ALLTRIM(lcRef)>>'
  					 	and site_nr=<<lcSite>>
				ENDTEXT
			ENDIF 
		
			uf_gerais_actGrelha("","",lcSQL)
			
			&&uf_perguntalt_chama("PROCESSO CONCLU�DO COM SUCESSO.", "Ok", "", 64)
			&&regua(2)
			
		ENDIF 

		IF USED("ucrsListMov")
			fecha("ucrsListMov")
		ENDIF

ENDFUNC 


FUNCTION uf_pesqstocks_bb
	LOCAL lcEPcpond, lcMovEntrada, lcStock, lcQttMov, lcCont, lcCont2, lcMesesAtua  
	STORE 0.00 TO lcEPcpond
	STORE .f. TO lcMovEntrada
	STORE 0 TO lcStock, lcQttMov, lcCont
	STORE 1 TO lcCont2 

	lcMesesAtua = uf_gerais_getparameter_site('ADM0000000130', 'NUM', mysite)
	IF lcMesesAtua <= 0
		lcMesesAtua = -1900
	ELSE
		lcMesesAtua = lcMesesAtua  * -1	
	ENDIF
	IF uf_gerais_getParameter("ADM0000000301","BOOL")
		TEXT To lcSQL TEXTMERGE NOSHOW
			SELECT sl.*, cm2.mudapcpond FROM sl (NOLOCK) 
			inner join cm2 (nolock) on sl.cm=cm2.cm 
			where ref='<<ALLTRIM(uc_tmpST.ref)>>' 
				and sl.ousrdata >= DATEADD(month, <<lcMesesAtua>> ,GETDATE())
			order by sl.datalc, sl.ousrdata, sl.ousrhora
		ENDTEXT 
	ELSE
		TEXT To lcSQL TEXTMERGE NOSHOW
			SELECT sl.*, cm2.mudapcpond FROM sl (NOLOCK) 
			inner join cm2 (nolock) on sl.cm=cm2.cm 
			where ref='<<ALLTRIM(uc_tmpST.ref)>>' and armazem=<<uc_tmpST.site_nr>> 
				and sl.ousrdata >= DATEADD(month, <<lcMesesAtua>> ,GETDATE())
			order by sl.datalc, sl.ousrdata, sl.ousrhora
		ENDTEXT 		
	ENDIF 
	uf_gerais_actGrelha("","ucrsListMov",lcSQL)

	IF reccount("ucrsListMov") > 0
		lcCont= reccount("ucrsListMov")			
		** Fazer o rec�lculo e atualiza��o de valores
		regua(0,lcCont,"A Atualizar Documentos...")
		SELECT ucrsListMov
		GO TOP 
		SCAN 
			regua(1,lcCont2,"A Atualizar Documentos...")
			
			** atualiza��o de movimentos da sl
			TEXT To lcSQL TEXTMERGE NOSHOW
				UPDATE sl SET epcpond=<<lcEPcpond>>, pcpond=<<ROUND(lcEPcpond*200.482,2)>> where slstamp='<<ALLTRIM(ucrsListMov.slstamp)>>'
			ENDTEXT 
			uf_gerais_actGrelha("","",lcSQL)

			SELECT ucrsListMov
			** atualiza��o de movimentos da fi
			IF !EMPTY(ucrsListMov.fistamp)
				TEXT To lcSQL TEXTMERGE NOSHOW
					UPDATE fi SET epcp=<<lcEPcpond>>, pcp=<<ROUND(lcEPcpond*200.482,2)>> where fistamp='<<ALLTRIM(ucrsListMov.fistamp)>>'
				ENDTEXT 
				uf_gerais_actGrelha("","",lcSQL)
			ENDIF 
				
			SELECT ucrsListMov
			** atualiza��o de movimentos da bi
			IF !EMPTY(ucrsListMov.bistamp)
				TEXT To lcSQL TEXTMERGE NOSHOW
					UPDATE bi SET epcusto=<<lcEPcpond>>, pcusto=<<ROUND(lcEPcpond*200.482,2)>> where bistamp='<<ALLTRIM(ucrsListMov.bistamp)>>' and ar2mazem=<<uc_tmpST.site_nr>>
				ENDTEXT 
				uf_gerais_actGrelha("","",lcSQL)
			ENDIF 
			
			SELECT ucrsListMov
			** atualiza��o de movimentos da fn
			IF !EMPTY(ucrsListMov.fnstamp)
				TEXT To lcSQL TEXTMERGE NOSHOW
					UPDATE fn SET qtt=qtt where fnstamp='<<ALLTRIM(ucrsListMov.fnstamp)>>'
				ENDTEXT 
				uf_gerais_actGrelha("","",lcSQL)
			ENDIF

			SELECT ucrsListMov				
			IF ucrsListMov.mudapcpond = .t.
				REPLACE ucrsListMov.EPCPOND WITH lcEPcpond
				IF ucrsListMov.cm < 50
					lcQttMov = ucrsListMov.qtt
				ELSE 
					lcQttMov = ucrsListMov.qtt * (-1)
				ENDIF 

				DO CASE 
					CASE (lcStock + lcQttMov ) <= 0 
						lcEPcpond = 0
					case (lcStock + lcQttMov ) > 0
						IF lcStock > 0 then
							lcEPcpond = ROUND(((lcStock * lcEPcpond)+(ucrsListMov.evu * ucrsListMov.qtt)) / (lcStock  + ucrsListMov.qtt),2)
						ELSE
							lcEPcpond = ROUND(((0 * lcEPcpond)+(ucrsListMov.evu * ucrsListMov.qtt)) / (0  + ucrsListMov.qtt),2)
						ENDIF 
					OTHERWISE
				ENDCASE 
				lcStock  = lcStock + lcQttMov
			ELSE
				REPLACE ucrsListMov.EPCPOND WITH lcEPcpond
				IF ucrsListMov.cm < 50 then
					lcStock = lcStock + ucrsListMov.qtt
				ELSE 
					lcStock = lcStock - ucrsListMov.qtt
				ENDIF 
			ENDIF 
			
			lcCont2 = lcCont2 + 1
			SELECT ucrsListMov
		ENDSCAN 
		
		SELECT uc_tmpST 
		IF uf_gerais_getParameter("ADM0000000301","BOOL")
			TEXT To lcSQL TEXTMERGE NOSHOW
				UPDATE 
					st 
				SET 
					epcpond=<<lcEPcpond>>, pcpond=<<ROUND(lcEPcpond*200.482,2)>>
					, marg4 = (case when st.epv1>0
						then ROUND( (st.epv1 / (taxasiva.taxa/100+1)) - <<lcEPcpond>> ,2)
						ELSE 0 end)
				from st
				left join taxasiva on taxasiva.codigo = st.tabiva
				where 
					ref='<<ALLTRIM(uc_tmpST.ref)>>'
			ENDTEXT 
		ELSE
			TEXT To lcSQL TEXTMERGE NOSHOW
				UPDATE 
					st 
				SET 
					epcpond=<<lcEPcpond>>
					, pcpond=<<ROUND(lcEPcpond*200.482,2)>>
					, marg4 = (case when st.epv1>0
						then ROUND( (st.epv1 / (taxasiva.taxa/100+1)) - <<lcEPcpond>> ,2)
						ELSE 0 end)
				from st
				left join taxasiva on taxasiva.codigo = st.tabiva
				 where 
					ref='<<ALLTRIM(uc_tmpST.ref)>>' 
					and site_nr=<<uc_tmpST.site_nr>>
			ENDTEXT
		ENDIF 
		uf_gerais_actGrelha("","",lcSQL)
		
		regua(2)
		
	ENDIF 

	IF USED("ucrsListMov")
		fecha("ucrsListMov")
	ENDIF


ENDFUNC 

** criei isto pois depois da correcao do bb
** necess�rio fazer de novo select para obter
** os valores mais recentes e corretos
FUNCTION uf_pesqstocks_marg3
	
	LPARAMETERS lcRefAtualizar, lcSitesaAtualizar
	
	LOCAL lcsqlST, lcSqlStUpdate, lcValor 			
	TEXT To lcsqlST TEXTMERGE NOSHOW
			SELECT
				*
			FROM 
				st
			WHERE 
				ref='<<ALLTRIM(uc_tmpST.ref)>>' 
				and site_nr=<<uc_tmpST.site_nr>>
		ENDTEXT 
	uf_gerais_actGrelha("","ucrsAtualizaMarg",lcsqlST)

	lcValor = 0
	IF ucrsAtualizaMarg.epv1 > 0 And ROUND(ucrsAtualizaMarg.epcpond,2)>0 		

		SELECT 	ucrsAtualizaMarg
		lcValor = ((ucrsAtualizaMarg.marg4 * 100)/ ROUND(ucrsAtualizaMarg.epcpond,2))
		
		SELECT ucrsAtualizaMarg		
		IF lcValor <= 9999
			replace ucrsAtualizaMarg.marg3 With Round(lcvalor,2)
		ELSE
			replace ucrsAtualizaMarg.marg3 With 0
		ENDIF
	ELSE
		replace ucrsAtualizaMarg.marg3 With 0
	ENDIF
	**
	
	
	TEXT TO lcSqlStUpdate TEXTMERGE NOSHOW
		UPDATE
			st
		SET
			<<"marg3=" + TRANSFORM(ucrsAtualizaMarg.marg3, "9999999.99")>>
		WHERE
			ref in ('<<lcRefAtualizar>>')
			and site_nr in (<<lcSitesaAtualizar>>)
	ENDTEXT

	IF !uf_gerais_actGrelha("", "", lcSqlStUpdate)
		RETURN .f.
	ENDIF
	
	fecha("ucrsAtualizaMarg")
ENDFUNC