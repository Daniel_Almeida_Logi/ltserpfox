FUNCTION uf_movimentosst_chama
	LPARAMETERS nRef, nDesign, nLote
	
	&& Valida Parametros
	IF EMPTY(nRef)
		uf_perguntalt_chama("FUN��O: UF_CHAMAMOVIMENTOSST. O PAR�METRO REFEER�NCIA N�O � V�LIDO!","OK","", 16)
		RETURN .F.
	ENDIF
	
	IF !(TYPE("nLote") == "C")
		nLote = ''
	ENDIF
	
	PUBLIC myOrderMovimentosSt, myOrigemRefPMP, myOrigemDesignPMP

	&& CONTROLA PERFIS DE ACESSO
	IF !uf_gerais_validaPermUser(ch_userno, ch_grupo, 'Gest�o de Stock - Movimentos')
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ACEDER AO PAINEL DE MOVIMENTOS.","OK","", 48)
		RETURN .f.
	ENDIF
		 
	IF TYPE("MovimentosST") == "U"
		&& Cria Cursor Vazio
		Text to lcSQL textmerge noshow
			exec up_stocks_Movimentos '<<nRef>>', <<myArmazem>>, '20000101', '20000101', 0, ''
		ENDTEXT

		IF !uf_gerais_actgrelha("", "uCrsMST", lcSql)
			RETURN .F.
		ENDIF
		
		DO FORM MovimentosST WITH nRef, nDesign, nLote
	ELSE
		MovimentosST.show
	ENDIF
ENDFUNC


**
FUNCTION uf_movimentosst_carregaMenu
	movimentosst.menu1.adicionaopcao("atualizar", "Atualizar", myPath + "\imagens\icons\actualizar_w.png", "uf_MovimentosST_carregaDados","A")
	movimentosst.menu1.adicionaopcao("navega", "Ver Doc.", myPath + "\imagens\icons\doc_seta_w.png", "uf_MovimentosST_navega","V")
	movimentosst.menu1.adicionaopcao("stockArm", "Stock. Arm.", myPath + "\imagens\icons\armazem_w.png", "uf_movimentosst_stockArm","S")
	movimentosst.menu1.adicionaopcao("imprimir", "Imprimir", myPath + "\imagens\icons\imprimir_w.png", "uf_movimentosst_report","I")
	movimentosst.menu1.adicionaopcao("topo", "Topo", myPath + "\imagens\icons\topo_w.png", "uf_movimentosst_topo","05")
	movimentosst.menu1.adicionaopcao("fundo", "Fundo", myPath + "\imagens\icons\fundo_w.png", "uf_movimentosst_fundo","24")
ENDFUNC


**
FUNCTION uf_MovimentosST_carregaDados
	local lcSQL, lcDataIni, lcDataFim, lcTotEntradas, lcTotSaidas, lcSaldo, lcTotEntradas2, lcTotSaidas2, lcArmazem
	Store "" To lcSQL
	
	&& valida��es
	IF !empty(movimentosST.txtDataIni.value)
		lcDataIni = uf_gerais_getDate(movimentosST.txtdataIni.value, "SQL")
	ELSE
		uf_perguntalt_chama("DEFINA UMA DATA INICIAL V�LIDA.","OK","", 48)
		RETURN .F.
	ENDIF

	IF !empty(movimentosST.txtDataFim.value) OR (ctod(movimentosST.txtDataFim.value) < Ctod(movimentosST.txtDataIni.value))
		lcDataFim = uf_gerais_getDate(movimentosST.txtdataFim.value, "SQL")
	ELSE
		uf_perguntalt_chama("DEFINA UMA DATA FINAL V�LIDA.","OK","",48)
		RETURN .F.
	ENDIF

	IF !Empty(movimentosST.txtArmazem.value)
		lcArmazem = movimentosST.txtArmazem.value
	ELSE
		lcArmazem = myarmazem
	ENDIF
	****

	&& INICIALIZA CONTADORES
	lcSaldo2		= 0
	lcTotEntradas2	= 0
	lcTotSaidas2	= 0
	lcTotEntradas	= 0
	lcTotSaidas		= 0
	lcSaldo 		= 0

	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_stocks_Movimentos '<<Alltrim(movimentosST.txtRef.value)>>', <<lcArmazem>>, '<<lcDataIni>>', '<<lcDataFim>>', 0, '<<ALLTRIM(movimentosST.txtLote.value)>>'
	ENDTEXT
	
   

	IF !uf_gerais_actgrelha("", "uCrsMST2", lcSql)
		RETURN .F.
	ELSE
		SELECT uCrsMST
		DELETE ALL
		
		SELECT uCrsMST2
		If Reccount("uCrsMST2") > 0
			IF USED("ST")
				select ST
				IF st.stns == .t.
					select uCrsMST2
					replace ALL uCrsMST2.SALDOANTERIOR WITH 0
					replace ALL uCrsMST2.ENTRADASANTERIOR WITH 0
					replace ALL uCrsMST2.SAIDASANTERIOR WITH 0
					replace ALL uCrsMST2.SALDO WITH 0
					replace ALL uCrsMST2.ENTRADAS WITH 0
					replace ALL uCrsMST2.SAIDAS WITH 0
				ENDIF 
			ENDIF 
			
			*******************************************		
			Select uCrsMST2
			GO TOP
			SCAN
				* Calcula o Total das colunas *
				lcTotEntradas2	= lcTotEntradas2 + uCrsMST2.entradas
				lcTotSaidas2	= lcTotSaidas2 + uCrsMST2.saidas
			ENDSCAN
			*******************************************

			SELECT uCrsMST2
			GO TOP
			IF UCRSMST2.ENTRADASANTERIOR != 0 Or UCRSMST2.SAIDASANTERIOR != 0
				Select uCrsMST
				Append Blank
				replace	UCRSMST.DATALC		WITH "..."
				replace	UCRSMST.CMDESC		WITH "Stock Anterior"
				replace UCRSMST.ENTRADAS	WITH UCRSMST2.ENTRADASANTERIOR
				replace UCRSMST.SAIDAS		WITH UCRSMST2.SAIDASANTERIOR
				replace UCRSMST.SALDO		WITH UCRSMST2.SALDOANTERIOR
			Endif
			**************************
			
			SELECT uCrsMST
			APPEND FROM DBF("uCrsMST2")
			
			SELECT uCrsMST
			GO TOP
			SCAN
				* Calcular o Saldo de cada linha *
				lcSaldo 	= lcSaldo + uCrsMST.entradas - uCrsMST.saidas
				replace uCrsMST.saldo WITH lcSaldo
				
				* Calcula o Total das colunas *
				lcTotEntradas	=	lcTotEntradas + uCrsMST.Entradas
				lcTotSaidas		=	lcTotSaidas + uCrsMST.saidas
			ENDSCAN
				
			Append Blank
			Append Blank
			replace UCRSMST.CMDESC		WITH "Total do Per�odo"
			replace UCRSMST.ENTRADAS	WITH lcTotEntradas2
			replace UCRSMST.SAIDAS		WITH lcTotSaidas2
			replace UCRSMST.SALDO		WITH lcTotEntradas2 - lcTotSaidas2
		ENDIF
	ENDIF
		
	movimentosST.txtTotalEntradas.value	= Round(lcTotEntradas,2)
	movimentosST.txtTotalSaidas.value	= Round(lcTotSaidas,2)
	movimentosST.txtTotalSaldo.value	= Round(lcTotEntradas-lcTotSaidas,2)
    
    uf_MovimentosST_filtraDoc()
ENDFUNC



**
FUNCTION uf_MovimentosST_report
		
	LOCAL lcSql, lcArmazem
	STORE '' TO lcSQL
	
	** Valores por defeito
	If empty(movimentosST.txtDataIni.value)
		uf_perguntalt_chama("DEFINA UMA DATA INICIAL V�LIDA.","OK","",48)
		RETURN .F.
	EndIf
		
	If empty(movimentosST.txtDataFim.value)
		uf_perguntalt_chama("DEFINA UMA DATA FINAL V�LIDA.","OK","",48)
		RETURN .F.
	Endif

	IF !Empty(movimentosST.txtArmazem.value)
		lcArmazem = movimentosST.txtArmazem.value
	ELSE
		lcArmazem = ALLTRIM(STR(myarmazem))
	ENDIF

	** Construir string com parametros
	lcSql = "&armazem=" + ALLTRIM(lcArmazem) +;
			"&dataIni=" + uf_gerais_getdate(movimentosST.txtDataIni.value, "DATA") +;
			"&dataFim=" + uf_gerais_getdate(movimentosST.txtDataFim.value, "DATA") +;
			"&site=" 	+ mySite +;
			"&modo="	+ "true" +;
			"&lote="	+ ALLTRIM(movimentosst.txtLote.value) +;
			"&ref="		+ ALLTRIM(movimentosst.txtRef.value) +;
			"&movimento="		+ "todos" 

	uf_gerais_chamaReport("relatorio_stocks_Movimentos", lcSql)
ENDFUNC



**
FUNCTION uf_MovimentosST_filtraDoc
	IF Upper(Alltrim(movimentosST.cmbDocumento.value)) == ""
		SELECT UCRSMST
		SET FILTER TO
	ELSE
		SELECT UCRSMST
		SET FILTER TO UPPER(ALLTRIM(UCRSMST.CMDESC)) == UPPER(ALLTRIM(movimentosST.cmbDocumento.VALUE)) OR ALLTRIM(UCRSMST.CMDESC) == "TOTAL DO PER�ODO"
	ENDIF
		
	SELECT UCRSMST
	GO TOP
	
	movimentosST.grid1.refresh
ENDFUNC


**
FUNCTION uf_MovimentosST_navega
	
	&& Valida Cursor
	IF !USED("uCrsMST")
		RETURN .f.
	ENDIF
	
	Select uCrsMST
	
	DO CASE
		
		Case Upper(Alltrim(uCrsMST.ORIGEM)) == "BO"
			&& CONTROLA PERFIS DE ACESSO
			IF !uf_gerais_validaPermUser(ch_userno,ch_grupo,'Gest�o de Documentos')
				uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ACEDER AO PAINEL GEST�O DE DOCUMENTOS.","OK","", 48)
				RETURN .f.	
			ENDIF
			
			IF myDocAlteracao == .t. Or myDocIntroducao == .t.
				uf_perguntalt_chama("O ECR� ENCONTRA-SE EM MODO DE INTRODU��O OU EDI��O.","OK","", 48)
			ELSE
			
				** Obtem o stamp de cabecalho
				If uf_gerais_actgrelha("", "uc_navCab", [Select bostamp FROM bi (nolock) where bistamp = ']+ ALLTRIM(uCrsMST.bistamp) + ['])
					
					SELECT uc_navCab
					IF RECCOUNT("uc_navCab") > 0
						uf_documentos_chama(uc_navCab.bostamp)
					ENDIF
					
					fecha("uc_navCab")
				ENDIF
			ENDIF
	
		CASE UPPER(ALLTRIM(UCRSMST.ORIGEM)) == "FO"
			&& CONTROLA PERFIS DE ACESSO
			IF !uf_gerais_validaPermUser(ch_userno,ch_grupo,'Gest�o de Documentos')
				uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ACEDER AO PAINEL GEST�O DE DOCUMENTOS.","OK","", 48)
				RETURN .f.
			ENDIF
			
			IF myDocAlteracao == .T. OR myDocIntroducao == .T.
				uf_perguntalt_chama("O ECR� ENCONTRA-SE EM MODO DE INTRODU��O OU EDI��O.","OK","", 48)
			ELSE
				** Obtem o stamp de cabecalho
				IF uf_gerais_actgrelha("", "uc_navCab", [Select fostamp FROM fn (nolock) where fnstamp = '] + ALLTRIM(uCrsMST.fnstamp) + ['])			

					SELECT uc_navCab
					IF RECCOUNT("uc_navCab") > 0
						uf_documentos_chama(uc_navCab.fostamp)
					ENDIF
					
					fecha("uc_navCab")
				ENDIF
			ENDIF 
			
		Case Upper(Alltrim(uCrsMST.ORIGEM)) == "FT"
			&& CONTROLA PERFIS DE ACESSO
			IF !uf_gerais_validaPermUser(ch_userno,ch_grupo,'Gest�o de Factura��o')
				uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ACEDER AO PAINEL GEST�O DE DOCUMENTOS.","OK","", 48)
				RETURN .f.	
			ENDIF
				
			IF myFtIntroducao == .t. OR myFtAlteracao == .t.
				uf_perguntalt_chama("O ECR� ENCONTRA-SE EM MODO DE INTRODU��O OU EDI��O.","OK","", 48)
			ELSE
				** Obtem o stamp de cabecalho
				IF uf_gerais_actgrelha("", "uc_navCab", [Select ftstamp FROM fi (nolock) where fistamp = '] + ALLTRIM(uCrsMST.fistamp) + ['])

					Select uc_navCab
					If Reccount("uc_navCab") > 0
						uf_facturacao_chama(Alltrim(uc_navCab.ftstamp))
					ENDIF
					
					fecha("uc_navCab")
				ENDIF
			ENDIF
			
		Case Upper(Alltrim(uCrsMST.ORIGEM)) == "IF"			
			Select uCrsMST
			If !Empty(uCrsMST.STICSTAMP)
				IF myInventarioIntroducao == .t. OR myInventarioAlteracao == .t.
					uf_perguntalt_chama("O ECR� ENCONTRA-SE EM MODO DE INTRODU��O OU EDI��O.","OK","", 48)
				ELSE
					uf_inventario_chama(Alltrim(uCrsMST.STICSTAMP))
				ENDIF
			ENDIF
			
			
		OTHERWISE
			***
	ENDCASE

ENDFUNC



**
FUNCTION uf_movimentosst_stockArm
	IF USED("st")
		SELECT st
		uf_stockPorArmazem_chama(st.ref, st.design)
	ENDIF
ENDFUNC



**
FUNCTION uf_movimentosst_topo
	SELECT uCrsMST
	GO TOP
ENDFUNC


**
FUNCTION uf_movimentosst_fundo
	SELECT uCrsMST
	GO BOTTOM
ENDFUNC


** Fechar Ecra de Pesquisa de STOCKS
FUNCTION uf_movimentosST_sair
	&& liberta variaveis publicas
	RELEASE myOrderMovimentosSt
	*RELEASE myOrigemRefPMP
	*RELEASE myOrigemDesignPMP
	
	** fecha cursores
	IF USED("uCrsMST")
		fecha("uCrsMST")
	ENDIF
	IF USED("uCrsMST2")
		fecha("uCrsMST2")
	ENDIF
		
	&& fecha painel
	movimentosst.hide
	MovimentosST.release
ENDFUNC 