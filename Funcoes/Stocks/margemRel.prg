***
FUNCTION uf_margemRel_chama()

	IF !WEXIST("MARGEMREL")
		DO FORM MARGEMREL
	ELSE
		MARGEMREL.show()
	ENDIF

ENDFUNC

**
FUNCTION uf_margemRel_sair()

	MARGEMREL.hide()
	MARGEMREL.release()

ENDFUNC