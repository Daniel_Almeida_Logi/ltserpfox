**
FUNCTION uf_pesqinventario_chama
	PUBLIC myPesqInventarioNConferidos 
		
	IF !USED("ucrsPesquisarInventarios")
		**Carrega Cursores
		Local lcSQL
		lcSQL = ""
		TEXT TO lcSql NOSHOW TEXTMERGE 
			SET FMTONLY ON
				SELECT 
					sel = convert(bit,0),
					Descricao,
					convert(varchar,Data,102) data,
					hora,
					lanca,
					sticstamp 
				From 
					stic (nolock) 
			SET FMTONLY OFF
		ENDTEXT
		IF !uf_gerais_actGrelha("",[ucrsPesquisarInventarios],lcSql)
			uf_perguntalt_chama("OCORREU UMA ANOMALIA NA LISTAGEM DE INVENTARIOS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			RETURN .f.
		ENDIF 
	ENDIF
	
	IF TYPE("PESQINVENTARIO")=="U"
		DO FORM PESQINVENTARIO
	ELSE
		PESQINVENTARIO.show
	ENDIF
ENDFUNC


**
FUNCTION uf_pesqInventario_CarregaDados
	Local lcSQL
	lcSQL = ""
	TEXT TO lcSql NOSHOW TEXTMERGE 
		SELECT 
			sel = convert(bit,0),
			Descricao,
			convert(varchar,Data,102) data,
			hora,
			lanca,
			sticstamp 
		From 
			stic (nolock) 
		Where 
			Descricao like '%<<ALLTRIM(PESQINVENTARIO.nome.value)>>%'
	ENDTEXT
	IF !EMPTY(ALLTRIM(pesqInventario.dataInv.value)) AND pesqInventario.datainv.value != '1900.01.01'
		lcSQL = lcSQL + " and data = '"+uf_gerais_getDate(pesqInventario.datainv.value,"SQL")+"'"
	ENDIF
	lcSQL = lcSQL + " Order by data desc"
	uf_gerais_actGrelha("PESQINVENTARIO.gridPesq","ucrsPesquisarInventarios",lcSQL)
	
	pesqinventario.show
	
ENDFUNC


**
FUNCTION uf_pesqInventario_Escolhe
	SELECT ucrsPesquisarInventarios
	uf_inventario_chama(Alltrim(ucrsPesquisarInventarios.sticstamp))
	
	uf_pesqinventario_sair()
ENDFUNC


**
FUNCTION uf_pesqinventario_sair
	
	**
	PESQINVENTARIO.hide
	PESQINVENTARIO.release
ENDFUNC 


FUNCTION uf_pesqinventario_gravar
	IF myPesqInventarioNConferidos 
		uf_inventario_adicionaStockNconfAfter()
	ENDIF
	
	uf_pesqinventario_sair()
ENDFUNC


FUNCTION uf_pesqinventario_pesqStock
	uf_pesqstocks_chama("INVENTARIO")
ENDFUNC