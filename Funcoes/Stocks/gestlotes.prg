**
FUNCTION uf_GESTLOTES_chama
	LPARAMETERS lcLote, LcRef
	
	PUBLIC myGestLotesAlteracao
	
	IF !(TYPE("lcLote") == "C")
		lcLote = ''
	ENDIF 
	
	IF TYPE("GESTLOTES") == "U"
		&& Cria Cursor Vazio
		Text to lcSQL textmerge noshow
			SET fmtonly on
			exec up_ga_lotesPesquisa '', '', '', ''
			SET fmtonly off
		ENDTEXT
		IF !uf_gerais_actgrelha("", "uCrsGestLotes", lcSql)
			RETURN .F.
		ENDIF
		
		lcSQL = ''
		TEXT TO lcSQL TEXTMERGE NOSHOW
			SET fmtonly on
			exec up_ga_loteDetalheStock ''
			SET fmtonly off
		ENDTEXT
		
		IF !uf_gerais_actGrelha("","uCrsLoteStock",lcSQL)
			RETURN .F.
		ENDIF
		
		lcSQL = ''
		TEXT TO lcSQL TEXTMERGE NOSHOW
			SET fmtonly on
			exec up_ga_loteDetalhePrecos '','',''
			SET fmtonly off
		ENDTEXT
		
		IF !uf_gerais_actGrelha("","uCrsLotePrecos",lcSQL)
			RETURN .F.
		ENDIF
		
		lcSQL = ''
		TEXT TO lcSQL TEXTMERGE NOSHOW
			SET fmtonly on
			exec up_ga_loteDetalhe '<<lcLote>>'
			SET fmtonly off
		ENDTEXT
		
		IF !uf_gerais_actGrelha("","uCrsLoteDetalhe",lcSQL)
			RETURN .f.
		ENDIF
		
		DO FORM GESTLOTES
		
		IF !EMPTY(LcRef)
			GESTLOTES.PageFrame1.Page1.txtref.value = LcRef
		ENDIF
	ELSE
		GESTLOTES.show
	ENDIF
	
	uf_GESTLOTES_carregaMenu()
	
	IF !EMPTY(ALLTRIM(lcLote))
		uf_GESTLOTES_detalhe(lcLote,LcRef)
	ENDIF 
ENDFUNC


**
FUNCTION uf_GESTLOTES_carregaMenu
	IF TYPE("GESTLOTES.menu1.pesquisar") == "U"
		GESTLOTES.menu1.adicionaopcao("pesquisar", "Pesquisar", myPath + "\imagens\icons\actualizar_w.png", "uf_GESTLOTES_pesquisar","P")
		GESTLOTES.menu1.adicionaOpcao("editar", "Editar", myPath + "\imagens\icons\lapis_w.png", "uf_GESTLOTES_editar","E")
		GESTLOTES.menu1.adicionaopcao("detalhe", "Detalhe", myPath + "\imagens\icons\doc_seta_w.png", "uf_GESTLOTES_detalhe with ''","D")
		GESTLOTES.menu1.adicionaopcao("mov", "Movimentos", myPath + "\imagens\icons\armazem_w.png", "uf_GESTLOTES_movimentos","M")
	ENDIF
	
	gestlotes.menu1.estado("mov, editar","HIDE")
ENDFUNC



**
FUNCTION uf_GESTLOTES_alternaMenu

	IF GESTLOTES.pageframe1.activepage == 2
		
		IF myGestLotesAlteracao == .f.
			gestlotes.menu1.estado("detalhe","HIDE")
			gestlotes.menu1.estado("Pesquisar, mov, editar","SHOW")
			gestlotes.menu1.pesquisar.config("Pesquisar", myPath + "\imagens\icons\exportar_w.png", "uf_GESTLOTES_pesquisar", "P")
			gestlotes.menu1.estado("", "", "Gravar", .f., "Sair", .t.)
			
			WITH gestlotes.Pageframe1.page2.grdLotePreco	
				FOR i=1 TO .ColumnCount
					IF .Columns(i).Header1.Caption = "PCT" OR .Columns(i).Header1.Caption = "PVP"
						.Columns(i).readonly = .t.
						.Columns(i).Text1.readonly = .t.
					ENDIF 
				ENDFOR
			ENDWITH
		ELSE
			gestlotes.menu1.estado("Pesquisar,detalhe, mov, editar","HIDE")
			gestlotes.menu1.estado("", "", "Gravar", .t., "Cancelar", .t.)
			
			WITH gestlotes.Pageframe1.page2.grdLotePreco	
				FOR i=1 TO .ColumnCount
					IF .Columns(i).Header1.Caption = "PCT" OR .Columns(i).Header1.Caption = "PVP"
						.Columns(i).readonly = .f.
						.Columns(i).Text1.readonly = .f.
					ENDIF
				ENDFOR
			ENDWITH
			
		ENDIF
	ELSE
		gestlotes.menu1.estado("mov, editar","HIDE")
		gestlotes.menu1.estado("Pesquisar, detalhe","SHOW")
		gestlotes.menu1.pesquisar.config("Pesquisar", myPath + "\imagens\icons\actualizar_w.png", "uf_GESTLOTES_pesquisar", "P")
		gestlotes.menu1.estado("", "", "", .t., "Cancelar", .t.)
	ENDIF

	GESTLOTES.refresh
ENDFUNC


**
FUNCTION uf_GESTLOTES_movimentos
	SELECT uCrsLoteDetalhe
	uf_movimentosst_chama(ALLTRIM(uCrsLoteDetalhe.ref),ALLTRIM(uCrsLoteDetalhe.design),ALLTRIM(uCrsLoteDetalhe.lote))
ENDFUNC


**
FUNCTION uf_GESTLOTES_detalhe
	LPARAMETERS lcLote, lcRef
	
	IF EMPTY(ALLTRIM(lcLote))
		SELECT uCrsGestLotes
		lcLote = ALLTRIM(uCrsGestLotes.nomeLote)
	ENDIF
	
	IF !EMPTY(ALLTRIM(lcLote))
		uf_gestLotes_carregaLote(ALLTRIM(lcLote),ALLTRIM(lcRef))
		GESTLOTES.pageframe1.activepage = 2
		
		uf_GESTLOTES_alternaMenu()
	ENDIF
ENDFUNC


FUNCTION uf_gestLotes_carregaLote
	LPARAMETERS lcLote, lcRef
	
	*STOCK
	lcSQL = ''
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_ga_loteDetalheStock '<<lcLote>>'
	ENDTEXT
	uf_gerais_actGrelha("gestLotes.pageframe1.page2.grdLoteStock","uCrsLoteStock",lcSQL)
	
	*PRE�OS
	lcSQL = ''
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_ga_loteDetalhePrecos '<<lcLote>>', '<<IIF(EMPTY(lcRef),'',lcRef)>>',''
	ENDTEXT
	uf_gerais_actGrelha("gestLotes.pageframe1.page2.grdLotePreco","uCrsLotePrecos",lcSQL)
	
	*INFO
	lcSQL = ''
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_ga_loteDetalhe '<<lcLote>>'
	ENDTEXT
	uf_gerais_actGrelha("","uCrsLoteDetalhe",lcSQL)
ENDFUNC

**
Function uf_GESTLOTES_pesquisar
	IF gestlotes.pageframe1.activepage = 1 
		Local lcSQL
		lcSQL = ""
		
		TEXT TO lcSQL TEXTMERGE NOSHOW
			exec up_ga_lotesPesquisa '<<ALLTRIM(GESTLOTES.pageframe1.page1.txtLote.value)>>', '<<ALLTRIM(GESTLOTES.pageframe1.page1.txtRef.value)>>', '<<ALLTRIM(GESTLOTES.pageframe1.page1.txtDesign.value)>>', '<<mySite>>'
		ENDTEXT
		IF !uf_gerais_actgrelha("GESTLOTES.pageframe1.page1.grdLotes", "uCrsGestLotes", lcSQL)
			RETURN .f.
		ENDIF
		
		SELECT uCrsGestLotes
		GO TOP
	ELSE
		GESTLOTES.pageframe1.activepage = 1
		uf_GESTLOTES_alternaMenu()
	ENDIF
EndFunc


**
Function uf_GESTLOTES_editar

	myGestLotesAlteracao = .t.
	uf_GESTLOTES_alternaMenu()

Endfunc 


**
Function uf_GESTLOTES_gravar
	LOCAL lcSQL
		
	Select uCrsLoteDetalhe
	*Calcula Valores Originais, para escrever na ocorrencias alteracao validade
	*INFO
	lcSQL = ''
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_ga_loteDetalhe '<<uCrsLoteDetalhe.lote>>'
	ENDTEXT
	uf_gerais_actGrelha("","uCrsLoteDetalheOriginal",lcSQL)
	
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE noshow
		UPDATE 
			st_lotes
		SET 
			Validade = '<<STRTRAN(uf_gerais_getdate(uCrsLoteDetalhe.validade,"SQL"),'-','')+'01'>>'
		WHere
			stamp = '<<uCrsLoteDetalhe.stamp>>'
			
		IF '<<STRTRAN(uf_gerais_getdate(uCrsLoteDetalheOriginal.validade,"SQL"),'-','')+'01'>>' != '<<STRTRAN(uf_gerais_getdate(uCrsLoteDetalhe.validade,"SQL"),'-','')+'01'>>'
			BEGIN	
									
				INSERT into B_ocorrencias
					(stamp, linkstamp, tipo, grau, descr,
					ovalor, dvalor,
					usr, date, site, terminal)
				values
					(LEFT(newid(),25), '<<uCrsLoteDetalhe.stamp>>', 'Stocks', 2, 'Altera��o de Validade na Gest�o de Lotes',
					'<<STRTRAN(uf_gerais_getdate(uCrsLoteDetalheOriginal.validade,"SQL"),'-','')+'01'>>', '<<STRTRAN(uf_gerais_getdate(uCrsLoteDetalhe.validade,"SQL"),'-','')+'01'>>',
					<<ch_userno>>, dateadd(HOUR, <<difhoraria>>, getdate()), '<<mySite>>', '<<myTermNo>>')	
		END	
	ENDTEXT
	IF !uf_gerais_actGrelha("","",lcSQL)
		Messagebox("N�o foi possivel guardar as altera��es. Contacte o suporte.",16,"Logitools Software")
		RETURN .f.
	ENDIF
	
	
	*Calcula Valores Originais, para escrever na ocorrencias
	lcSQL = ''
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_ga_loteDetalhePrecos '<<ALLTRIM(uCrsLoteDetalhe.lote)>>',''
	ENDTEXT
	uf_gerais_actGrelha("","uCrsLotePrecosOriginal",lcSQL)
	
	** Atualiza Precos e Validade Lote
	Select uCrsLotePrecos
	GO TOP 
	SCAN
	
		Select uCrsLotePrecosOriginal
		LOCATE FOR ALLTRIM(uCrsLotePrecosOriginal.stampPrecos) == ALLTRIM(uCrsLotePrecos.stampPrecos)
		IF FOUND()
			lcSQL = ""
			TEXT TO lcSQL TEXTMERGE noshow
				UPDATE 
					st_lotes
				SET 
					Validade = '<<STRTRAN(uf_gerais_getdate(uCrsLoteDetalhe.validade,"SQL"),'-','')+'01'>>'
				WHere
					stamp = '<<uCrsLoteDetalhe.stamp>>'
				
				IF <<uCrsLotePrecosOriginal.PCT>> != <<uCrsLotePrecos.pct>> OR <<uCrsLotePrecosOriginal.EPV1>> != <<uCrsLotePrecos.EPV1>>
				BEGIN	
					UPDATE 
						st_lotes_precos
					SET 
						pct = <<uCrsLotePrecos.pct>>
						,epv1 = <<uCrsLotePrecos.epv1>>
						,usrdata = convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
						,usrhora = convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108)
						,usrinis = '<<m_chinis>>'
					WHere
						stampPrecos = '<<uCrsLotePrecos.stampPrecos>>'
				
				END 
				IF <<uCrsLotePrecosOriginal.PCT>> != <<uCrsLotePrecos.PCT>>
				BEGIN	
										
					INSERT into B_ocorrencias
							(stamp, linkstamp, tipo, grau, descr,
							ovalor, dvalor,
							usr, date, site, terminal)
						values
							(
							LEFT(newid(),25), '<<uCrsLoteDetalhe.stamp>>', 'Stocks', 2, 'Altera��o de PCT na Gest�o de Lotes',
							'<<round(uCrsLotePrecosOriginal.PCT,2)>>', '<<round(uCrsLotePrecos.PCT,2)>>',
							<<ch_userno>>, dateadd(HOUR, <<difhoraria>>, getdate()), '<<mySite>>', '<<myTermNo>>'
						)	
				END
			
				IF <<uCrsLotePrecosOriginal.EPV1>> != <<uCrsLotePrecos.EPV1>>
				BEGIN	
										
					INSERT into B_ocorrencias
							(stamp, linkstamp, tipo, grau, descr,
							ovalor, dvalor,
							usr, date, site, terminal)
						values
							(LEFT(newid(),25), '<<uCrsLoteDetalhe.stamp>>', 'Stocks', 2, 'Altera��o de PVP na Gest�o de Lotes',
							'<<round(uCrsLotePrecosOriginal.EPV1,2)>>', '<<round(uCrsLotePrecos.EPV1,2)>>',
							<<ch_userno>>, dateadd(HOUR, <<difhoraria>>, getdate()), '<<mySite>>', '<<myTermNo>>')	
				END	
			ENDTEXT 
		ENDIF
		
		Select uCrsLotePrecos
	
		IF !uf_gerais_actGrelha("","",lcSQL)
			Messagebox("N�o foi possivel guardar as altera��es. Contacte o suporte.",16,"Logitools Software")
			RETURN .f.
		ENDIF

	ENDSCAN
	
	myGestLotesAlteracao = .f.
	uf_GESTLOTES_alternaMenu()

	uf_GESTLOTES_chama(uCrsLoteDetalhe.lote)

Endfunc 

**
Function uf_GESTLOTES_ValidadeLote
	LPARAMETERS lcLote
	
	IF EMPTY(lcLote)
		RETURN ""
	ENDIF
	
	LOCAL lcDataValidade
	lcDataValidade = ""
	
	TEXT TO lcSQL NOSHOW TEXTMERGE
		select validade from st_lotes where lote = '<<ALLTRIM(lcLote)>>'
	ENDTEXT
	IF !uf_gerais_actGrelha("","ucrsValidadeLote",lcSQL)
		Messagebox("N�o foi possivel verificar a validade do Lote : "+ ALLTRIM(lcLote) + ". Contacte o suporte.",16,"Logitools Software")
		RETURN ""
	ENDIF
	
	IF RECCOUNT("ucrsValidadeLote")>0
		select ucrsValidadeLote
		lcDataValidade = uf_gerais_getDate(ucrsValidadeLote.validade,"DATA")
	ENDIF	
	
	RETURN lcDataValidade
	

ENDFUNC


**
Function uf_GESTLOTES_sair
	IF myGestLotesAlteracao == .t.
		myGestLotesAlteracao = .f.
		uf_GESTLOTES_alternaMenu()
	
	ELSE
		uf_GESTLOTES_exit()
	ENDIF

ENDFUNC


**
Function uf_GESTLOTES_exit
	Fecha("uCrsGestLotes")
	RELEASE myGestLotesAlteracao 
	
	GESTLOTES.hide
	GESTLOTES.release
ENDFUNC

