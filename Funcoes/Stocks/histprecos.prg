FUNCTION uf_histprecos_chama
	LPARAMETERS lcRef
		
	** cria cursor vazio
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		select * from histprecos_ref WHERE 0=1 ORDER BY ousrdata
	ENDTEXT 
	IF !uf_gerais_actgrelha("", "uCrshistprecos", lcSql)
		uf_perguntalt_chama("ERRO AO LER A TABELA DE LOGS!","OK","",16)
		RETURN .F.
	ENDIF 
	
	&& Controla Abertura do Painel			
	if type("histprecos")=="U"
		DO FORM HISTPRECOS
	ELSE
		HISTPRECOS.show
	ENDIF
	
	** carrega dados
	TEXT TO lcSQL TEXTMERGE NOSHOW
		select * from histprecos_ref WHERE ref='<<ALLTRIM(st.ref)>>' ORDER BY ousrdata
	ENDTEXT 	
	IF !uf_gerais_actgrelha("histprecos.GridPesq", "uCrshistprecos", lcSql)
		uf_perguntalt_chama("ERRO AO LER A TABELA DE LOGS!","OK","",16)
		RETURN .F.
	ENDIF 
	
ENDFUNC


**
FUNCTION uf_histprecos_carregamenu
	histprecos.menu1.adicionaopcao("atualizar", "Atualizar", myPath + "\imagens\icons\actualizar_w.png", "uf_histprecos_Pesquisa", "F2")
ENDFUNC


**
FUNCTION uf_histprecos_Pesquisa
	**LPARAMETERS lcRef
	LOCAL lcSQL
	
	regua(0,0,"A atualizar...")
	
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		select * from histprecos_ref WHERE ref='<<st.Ref>>' AND site_nr=<<st.site_nr>> ORDER BY ousrdata
	ENDTEXT

	uf_gerais_actGrelha("histprecos.GridPesq", "uCrshistprecos", lcSQL)

	regua(2)
	
	histprecos.Refresh
	
ENDFUNC

**
FUNCTION uf_histprecos_sair
	**Fecha Cursores
	IF USED("uCrshistprecos")
		fecha("uCrshistprecos")
	ENDIF 
	
	histprecos.hide
	histprecos.release
ENDFUNC