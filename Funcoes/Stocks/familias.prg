
***
FUNCTION uf_familias_chama
	LPARAMETERS lcLocal
		
	LOCAL lcSQL
	
	PUBLIC myStocksIntroducao, myStocksAlteracao

	IF myStocksIntroducao == .f. AND myStocksAlteracao == .f. AND lcLocal == "STOCKS"
		RETURN .f.
	ENDIF
	
	IF !USED("ucrsFamDepartamentos")
		lcSQL = ""		
		TEXT TO lcSQL TEXTMERGE NOSHOW			
			SELECT distinct convert(varchar(4),id) as depstamp, descr as design from grande_mercado_hmr order by descr 
		ENDTEXT
		If !uf_gerais_actGrelha("", "ucrsFamDepartamentos", lcSQL)
			return .f.
		ENDIF
	ENDIF
	
	IF !USED("ucrsFamSeccoes")	
		lcSQL = ""
		TEXT TO lcSQL TEXTMERGE NOSHOW			
			SELECT distinct convert(varchar(4),id) as secstamp, descr as design from mercado_hmr order by descr 
		ENDTEXT
		If !uf_gerais_actGrelha("", "ucrsFamSeccoes", lcSQL)
			return .f.
		ENDIF
	ENDIF
	
	IF !USED("ucrsFamCategorias")	
		lcSQL = ""
		TEXT TO lcSQL TEXTMERGE NOSHOW			
			SELECT distinct convert(varchar(4),id) as catstamp, descr as design from categoria_hmr order by descr 
		ENDTEXT
		If !uf_gerais_actGrelha("", "ucrsFamCategorias", lcSQL)
			return .f.
		ENDIF
	ENDIF	
	
	IF !USED("ucrsFamSegmentos")	
		lcSQL = ""
		TEXT TO lcSQL TEXTMERGE NOSHOW			
			SELECT distinct convert(varchar(4),id) as segstamp, descr as design from segmento_hmr order by descr  
		ENDTEXT
		If !uf_gerais_actGrelha("", "ucrsFamSegmentos", lcSQL)
			return .f.
		ENDIF
	ENDIF
		
	IF TYPE("FAMILIAS")=="U"
		DO FORM FAMILIAS WITH lcLocal
	ELSE
		FAMILIAS.show
	ENDIF
ENDFUNC 


**
FUNCTION uf_familias_actualizaDepartamento
	SELECT ucrsFamDepartamentos
	FAMILIAS.departamento.value = ucrsFamDepartamentos.design
	FAMILIAS.Refresh
ENDFUNC


**
FUNCTION uf_familias_alteraDepartamento
	IF EMPTY(ALLTRIM(FAMILIAS.departamento.value))
	ELSE
		
		SELECT ucrsFamDepartamentos
	ENDIF
	FAMILIAS.Refresh
ENDFUNC


**
FUNCTION uf_familias_actualizaSeccoes
	SELECT ucrsFamSeccoes
	FAMILIAS.seccao.value = ucrsFamSeccoes.design
	FAMILIAS.Refresh
ENDFUNC


**
FUNCTION uf_familias_alteraSeccoes

	IF EMPTY(ALLTRIM(FAMILIAS.seccao.value))
	ELSE		
		SELECT ucrsFamSeccoes
	ENDIF
		
	FAMILIAS.Refresh
ENDFUNC


**
FUNCTION uf_familias_actualizaCategorias
	SELECT ucrsFamCategorias
	FAMILIAS.categoria.value = ucrsFamCategorias.design
	FAMILIAS.Refresh
ENDFUNC


**
FUNCTION uf_familias_alteraCategorias
	
	IF EMPTY(ALLTRIM(FAMILIAS.categoria.value))
	ELSE
		SELECT ucrsFamCategorias
	ENDIF
	
	FAMILIAS.Refresh
ENDFUNC

FUNCTION uf_familias_actualizaSegmentos
	SELECT ucrsFamSegmentos
	FAMILIAS.segmento.value = ucrsFamSegmentos.design
	FAMILIAS.Refresh
ENDFUNC

FUNCTION uf_familias_alteraSegmentos
	
	IF EMPTY(ALLTRIM(FAMILIAS.segmento.value))
	ELSE
		SELECT ucrsFamSegmentos
	ENDIF
	
	FAMILIAS.Refresh
ENDFUNC

** 
FUNCTION uf_familias_gravar

	DO CASE 
		CASE FAMILIAS.local == "STOCKS"
			LOCAL lcRefStocks, lcDesignStocks
			STORE "" TO lcRefStocks, lcDesignStocks
			IF USED("ST")
				IF EMPTY(ALLTRIM(FAMILIAS.departamento.value))
					lcRefStocks = lcRefStocks 
					lcDesignStocks= lcDesignStocks
					
					REPLACE ST.u_depstamp WITH ""
					stocks.pageframe1.page2.departamento.value = ""
				ELSE
					SELECT St
					SELECT ucrsFamDepartamentos					
					lcRefStocks = lcRefStocks + ALLTRIM(ucrsFamDepartamentos.depstamp)															
					lcDesignStocks= lcDesignStocks + ALLTRIM(ucrsFamDepartamentos.design)
					
					REPLACE ST.u_depstamp WITH ALLTRIM(ucrsFamDepartamentos.depstamp)
					stocks.pageframe1.page2.departamento.value = ALLTRIM(ucrsFamDepartamentos.design)					
				ENDIF

				
				IF EMPTY(ALLTRIM(FAMILIAS.seccao.value))
					lcRefStocks = lcRefStocks + ";" 
					lcDesignStocks= lcDesignStocks + ";" 
					
					REPLACE ST.u_secstamp WITH ""
					stocks.pageframe1.page2.seccao.value = ""
				ELSE
					SELECT ucrsFamSeccoes
					lcRefStocks = lcRefStocks + ";" + ALLTRIM(ucrsFamSeccoes.secstamp)
					lcDesignStocks= lcDesignStocks + ";" + ALLTRIM(ucrsFamSeccoes.design)
					
					REPLACE ST.u_secstamp WITH ALLTRIM(ucrsFamSeccoes.secstamp)
					stocks.pageframe1.page2.seccao.value = ALLTRIM(ucrsFamSeccoes.design)
				ENDIF
					
				IF EMPTY(ALLTRIM(FAMILIAS.categoria.value))
					lcRefStocks = lcRefStocks + ";" 
					lcDesignStocks= lcDesignStocks + ";"
					
					REPLACE ST.u_catstamp WITH ""
					stocks.pageframe1.page2.categoria.value = ""
				ELSE
					SELECT ucrsFamCategorias
					lcRefStocks = lcRefStocks + ";" + ALLTRIM(ucrsFamCategorias.catstamp)
					lcDesignStocks= lcDesignStocks + ";" + ALLTRIM(ucrsFamCategorias.design)
					
					REPLACE ST.u_catstamp WITH ALLTRIM(ucrsFamCategorias.catstamp)
					stocks.pageframe1.page2.categoria.value = ALLTRIM(ucrsFamCategorias.design)
				ENDIF
				
				IF EMPTY(ALLTRIM(FAMILIAS.segmento.value))
					lcRefStocks = lcRefStocks + ";" 
					lcDesignStocks= lcDesignStocks + ";" 
					
					REPLACE ST.u_segstamp WITH ""
					stocks.pageframe1.page2.segmento.value = ""
				ELSE
					SELECT ucrsFamSegmentos
					lcRefStocks = lcRefStocks + ";" + ALLTRIM(ucrsFamSegmentos.segstamp)
					lcDesignStocks= lcDesignStocks + ";" + ALLTRIM(ucrsFamSegmentos.design)
					
					REPLACE ST.u_segstamp WITH ALLTRIM(ucrsFamSegmentos.segstamp)
					stocks.pageframe1.page2.segmento.value = ALLTRIM(ucrsFamSegmentos.design)
				ENDIF
						
				STOCKS.REFRESH
				uf_familias_sair()
			ENDIF
			
		CASE FAMILIAS.local == "PROMOO"
			LOCAL lcRef, lcDesign
			STORE "" TO lcRef, lcDesign
			
			IF !EMPTY(ALLTRIM(FAMILIAS.departamento.value))
				SELECT ucrsFamDepartamentos
				lcRef = ALLTRIM(ucrsFamDepartamentos.depstamp)
				lcDesign = ALLTRIM(FAMILIAS.departamento.value)
			ELSE
				lcRef = lcRef 
				lcDesign = lcDesign 
			ENDIF

			IF !EMPTY(ALLTRIM(FAMILIAS.seccao.value))
				SELECT ucrsFamSeccoes
				lcRef = lcRef + ";" + ALLTRIM(ucrsFamSeccoes.secstamp)
				lcDesign = lcDesign + ";" + ALLTRIM(FAMILIAS.seccao.value)
			ELSE
				lcRef = lcRef + ";"
				lcDesign = lcDesign + ";"
			ENDIF
				
			IF !EMPTY(ALLTRIM(FAMILIAS.categoria.value))
				SELECT ucrsFamCategorias
				lcRef = lcRef + ";" + ALLTRIM(ucrsFamCategorias.catstamp)
				lcDesign = lcDesign + ";" + ALLTRIM(FAMILIAS.categoria.value)
			ELSE
				lcRef = lcRef + ";"
				lcDesign = lcDesign + ";"
			ENDIF
			
			IF !EMPTY(ALLTRIM(FAMILIAS.segmento.value))
				SELECT ucrsFamSegmentos
				lcRef = lcRef + ";" + ALLTRIM(ucrsFamSegmentos.segstamp)
				lcDesign = lcDesign + ";" + ALLTRIM(FAMILIAS.segmento.value)
			ELSE
				lcRef = lcRef + ";"
				lcDesign = lcDesign + ";"
			ENDIF
				
			SELECT uCrsPromoOF
			LOCATE FOR ALLTRIM(uCrsPromoOF.ref) == Alltrim(lcRef)
			IF !FOUND()
				SELECT uCrsPromoOF
				APPEND BLANK
				Replace ;
					uCrsPromoOF.ref		WITH Alltrim(lcRef) ;
					uCrsPromoOF.design	WITH Alltrim(lcDesign) ;
					uCrsPromoOF.qtt		WITH 1
				
				promocoes.pageframe1.page6.grdOrigem.refresh
			ENDIF
			
			uf_familias_sair()
			
		CASE FAMILIAS.local == "PROMOD"
			LOCAL lcRef, lcDesign
			STORE "" TO lcRef, lcDesign
			IF !EMPTY(ALLTRIM(FAMILIAS.departamento.value))
				SELECT ucrsFamDepartamentos
				lcRef = ALLTRIM(ucrsFamDepartamentos.depstamp)
				lcDesign = ALLTRIM(FAMILIAS.departamento.value)
			ELSE
				lcRef = lcRef 
				lcDesign = lcDesign 
			ENDIF
			
			IF !EMPTY(ALLTRIM(FAMILIAS.seccao.value))
				SELECT ucrsFamSeccoes
				lcRef = lcRef + ";" + ALLTRIM(ucrsFamSeccoes.secstamp)
				lcDesign = lcDesign + ";" + ALLTRIM(FAMILIAS.seccao.value)
			ELSE
				lcRef = lcRef + ";"
				lcDesign = lcDesign + ";" 
			ENDIF
				
			IF !EMPTY(ALLTRIM(FAMILIAS.categoria.value))
				SELECT ucrsFamCategorias
				lcRef = lcRef + ";" + ALLTRIM(ucrsFamCategorias.catstamp)
				lcDesign = lcDesign + ";" + ALLTRIM(FAMILIAS.categoria.value)
			ELSE
				lcRef = lcRef + ";"
				lcDesign = lcDesign + ";"
			ENDIF
			
			IF !EMPTY(ALLTRIM(FAMILIAS.segmento.value))
				SELECT ucrsFamSegmentos
				lcRef = lcRef + ";" + ALLTRIM(ucrsFamSegmentos.segstamp)
				lcDesign = lcDesign + ";" + ALLTRIM(FAMILIAS.segmento.value)
			ELSE
				lcRef = lcRef + ";"
				lcDesign = lcDesign + ";"
			ENDIF
				
			SELECT uCrsPromoDF
			LOCATE FOR ALLTRIM(uCrsPromoDF.ref) == Alltrim(lcRef)
			IF !FOUND()
				SELECT uCrsPromoDF
				APPEND BLANK
				Replace ;
					uCrsPromoDF.ref		WITH Alltrim(lcRef) ;
					uCrsPromoDF.design	WITH Alltrim(lcDesign) ;
					uCrsPromoDF.qtt		WITH 1
				
				promocoes.pageframe1.page6.grdDestino.refresh
			ENDIF
			
			uf_familias_sair()
			
		CASE FAMILIAS.local == "STOCKS_ALTERACOES"
			PUBLIC MyclassNivel1, MyclassNivel2, MyclassNivel3, MyclassNivel4, MyInformacao
			STORE '' TO MyclassNivel1, MyclassNivel2, MyclassNivel3, MyclassNivel4, MyInformacao
		
				IF EMPTY(ALLTRIM(FAMILIAS.departamento.value))
					MyclassNivel1 = ""
					MyInformacao  = ""
				ELSE
					SELECT ucrsFamDepartamentos
					MyclassNivel1 = ALLTRIM(uCrsFamDepartamentos.depstamp)
					MyInformacao  = ALLTRIM(uCrsFamDepartamentos.design)
				ENDIF

				IF EMPTY(ALLTRIM(FAMILIAS.seccao.value))
					MyclassNivel2  = ""
					&& MyInformacao = ""
				ELSE
					SELECT ucrsFamSeccoes
					MyclassNivel2 = ALLTRIM(uCrsFamSeccoes.secstamp)
					MyInformacao  = MyInformacao + " - " + ALLTRIM(uCrsFamSeccoes.design)
				ENDIF
					
				IF EMPTY(ALLTRIM(FAMILIAS.categoria.value))
					MyclassNivel3 = ""
					&& MyInformacao = ""
				ELSE
					SELECT ucrsFamCategorias
					MyclassNivel3 = ALLTRIM(ucrsFamCategorias.catstamp)
					MyInformacao = MyInformacao + " - " + ALLTRIM(uCrsFamCategorias.design)
				ENDIF
			
				IF EMPTY(ALLTRIM(FAMILIAS.segmento.value))
					MyclassNivel4 = ""
					&& MyInformacao = ""
				ELSE
					SELECT ucrsFamSegmentos
					MyclassNivel4 = ALLTRIM(ucrsFamSegmentos.segstamp)
					MyInformacao = MyInformacao + " - " + ALLTRIM(ucrsFamSegmentos.design)
				ENDIF			
			
			pesqstocks.container2.PageFrame1.page1.txtClassIMS.value = MyInformacao 
			
			pesqstocks.refresh
			uf_familias_sair()	
			
		CASE FAMILIAS.local == "PESQSTOCKS"	
		
			lcValor = ""
			IF !EMPTY(ALLTRIM(FAMILIAS.departamento.value))
				lcValor = lcValor + ALLTRIM(FAMILIAS.departamento.value)
			ELSE
				lcValor = lcValor
			ENDIF
			
			IF !EMPTY(ALLTRIM(FAMILIAS.seccao.value))
				lcValor = lcValor + ";" + ALLTRIM(FAMILIAS.seccao.value)
			ELSE
				lcValor = lcValor + ";"
			ENDIF
			
			IF !EMPTY(ALLTRIM(FAMILIAS.categoria.value))
				lcValor = lcValor + ";" + ALLTRIM(FAMILIAS.categoria.value)
			ELSE
				lcValor = lcValor + ";"
			ENDIF
			
			IF !EMPTY(ALLTRIM(FAMILIAS.segmento.value))
				lcValor = lcValor + ";" + ALLTRIM(FAMILIAS.segmento.value)
			ELSE
				lcValor = lcValor + ";"
			ENDIF
					
			
			pesqstocks.PageFrame1.page1.Class2.value = ALLTRIM(lcValor)
			
			pesqstocks.refresh
			uf_familias_sair()	
			
		OTHERWISE 
		
			TRY 
				lcObj = FAMILIAS.local 
				
				lcValor = ""
				IF !EMPTY(ALLTRIM(FAMILIAS.departamento.value))
					lcValor = lcValor + ALLTRIM(FAMILIAS.departamento.value)
				ELSE
					lcValor = lcValor
				ENDIF
				
				IF !EMPTY(ALLTRIM(FAMILIAS.seccao.value))
					lcValor = lcValor + ";" + ALLTRIM(FAMILIAS.seccao.value)
				ELSE
					lcValor = lcValor + ";"
				ENDIF
				
				IF !EMPTY(ALLTRIM(FAMILIAS.categoria.value))
					lcValor = lcValor + ";" + ALLTRIM(FAMILIAS.categoria.value)
				ELSE
					lcValor = lcValor + ";"
				ENDIF
				
				IF !EMPTY(ALLTRIM(FAMILIAS.segmento.value))
					lcValor = lcValor + ";" + ALLTRIM(FAMILIAS.segmento.value)
				ELSE
					lcValor = lcValor + ";"
				ENDIF
								
				&lcObj..value = ALLTRIM(lcValor)
					
				uf_familias_sair()
			CATCH
				uf_perguntalt_chama("N�o foi possivel aplicar a familia selecionada!", "Ok", "", 16)
			ENDTRY
	ENDCASE 
ENDFUNC


**
FUNCTION uf_familias_vazio
	FAMILIAS.departamento.value = ""
	FAMILIAS.seccao.value = ""
	FAMILIAS.categoria.value = ""
	FAMILIAS.segmento.value = ""
** quando aplicado um filtro anteriormente,repor o filtro original para quando vazio sem ter de limpar a textbox
	uf_familias_filtra_grandeMercado()
	uf_familias_filtra_mercado()
	uf_familias_filtra_categoria()
	uf_familias_filtra_segmentos()
** coloca de volta na 1 posicao	
	SELECT ucrsFamDepartamentos
	GO TOP
	familias.GridDepartamento.refresh()
	
	SELECT ucrsFamSeccoes
	GO TOP
	familias.GridSeccao.refresh()
	
	SELECT ucrsFamCategorias
	GO TOP
	familias.GridCategoria.refresh()
	
	SELECT ucrsFamSegmentos
	GO TOP
	familias.GridSegmento.refresh()
	
ENDFUNC


**
FUNCTION uf_familias_sair
	FAMILIAS.hide
	FAMILIAS.release
ENDFUNC

PROCEDURE uf_familias_filtra_grandeMercado
	SELECT ucrsFamDepartamentos
	
	IF EMPTY(familias.departamento.Value)
		IF !USED("ucrsFamDepartamentos")
			lcSQL = ""		
			TEXT TO lcSQL TEXTMERGE NOSHOW			
				SELECT distinct convert(varchar(4),id) as depstamp, descr as design from grande_mercado_hmr order by descr 
			ENDTEXT
			If !uf_gerais_actGrelha("", "ucrsFamDepartamentos", lcSQL)
				return .f.
			ENDIF
		ENDIF
		
		SELECT ucrsFamDepartamentos
		GO TOP
		familias.GridDepartamento.refresh()
		
	ENDIF

	IF !EMPTY(familias.departamento.Value)
		SET FILTER TO LIKE( UPPER(ALLTRIM(familias.departamento.Value)) + "*", UPPER(ALLTRIM(ucrsFamDepartamentos.design)))
	ELSE
		SET FILTER TO
	ENDIF
	SELECT ucrsFamDepartamentos
		
	familias.GridDepartamento.refresh()
ENDPROC

PROCEDURE uf_familias_filtra_mercado
	SELECT ucrsFamSeccoes
	
	IF EMPTY(familias.seccao.Value)
		IF !USED("ucrsFamSeccoes")	
			lcSQL = ""
			TEXT TO lcSQL TEXTMERGE NOSHOW			
				SELECT distinct convert(varchar(4),id) as secstamp, descr as design from mercado_hmr order by descr 
			ENDTEXT
			If !uf_gerais_actGrelha("", "ucrsFamSeccoes", lcSQL)
				return .f.
			ENDIF
		ENDIF
		SELECT ucrsFamSeccoes
		GO TOP
		familias.GridSeccao.refresh()
	ENDIF
	
	IF !EMPTY(familias.seccao.Value)
		SET FILTER TO LIKE( UPPER(ALLTRIM(familias.seccao.Value)) + "*", UPPER(ALLTRIM(ucrsFamSeccoes.design)))
	ELSE
		SET FILTER TO
	ENDIF
	SELECT ucrsFamSeccoes

	
	familias.GridSeccao.refresh()
ENDPROC

PROCEDURE uf_familias_filtra_categoria
	SELECT ucrsFamCategorias
	
	IF EMPTY(familias.categoria.Value)
		IF !USED("ucrsFamCategorias")	
			lcSQL = ""
			TEXT TO lcSQL TEXTMERGE NOSHOW			
				SELECT distinct convert(varchar(4),id) as catstamp, descr as design from categoria_hmr order by descr 
			ENDTEXT
			If !uf_gerais_actGrelha("", "ucrsFamCategorias", lcSQL)
				return .f.
			ENDIF
		ENDIF	
		SELECT ucrsFamCategorias
		GO TOP
		familias.GridCategoria.refresh()
	ENDIF
	
	IF !EMPTY(familias.categoria.Value)
		SET FILTER TO LIKE( UPPER(ALLTRIM(familias.categoria.Value)) + "*", UPPER(ALLTRIM(ucrsFamCategorias.design)))
	ELSE
		SET FILTER TO
	ENDIF
	SELECT ucrsFamCategorias

	
	familias.GridCategoria.refresh()
ENDPROC

PROCEDURE uf_familias_filtra_segmentos
		
	SELECT ucrsFamSegmentos
	
	IF EMPTY(familias.segmento.Value)
		IF !USED("ucrsFamSegmentos")	
			lcSQL = ""
			TEXT TO lcSQL TEXTMERGE NOSHOW			
				SELECT distinct convert(varchar(4),id) as segstamp, descr as design from segmento_hmr order by descr  
			ENDTEXT
			If !uf_gerais_actGrelha("", "ucrsFamSegmentos", lcSQL)
				return .f.
			ENDIF
		ENDIF
		SELECT ucrsFamSegmentos
		GO TOP
		familias.GridSegmento.refresh()
	ENDIF
	
	IF !EMPTY(familias.segmento.Value)
		SET FILTER TO LIKE( UPPER(ALLTRIM(familias.segmento.Value)) + "*", UPPER(ALLTRIM(ucrsFamSegmentos.design)))
	ELSE
		SET FILTER TO
	ENDIF
	SELECT ucrsFamSegmentos

	
	familias.GridSegmento.refresh()
ENDPROC