&& INCLUIR PRGS SECUNDARIOS
DO pesqstocks
DO CodigosAdicionais
DO dicionario
DO confPVP
DO movimentosST
DO INVENTARIO
DO PESQINVENTARIO
DO stockPorArmazem
DO VALIDADES
DO Familias
DO dependencias
DO gestlotes
DO recursos
DO pesqrecurso
DO logstocks
DO ESCALOESPV
**DO actividades
**DO pesqactividades
DO histprecos
DO margemRel


*****************************
*		Chamar Artigo		*
*****************************
FUNCTION uf_stocks_chama
	LPARAMETERS lcRef, lcATop
	
	PUBLIC MyEncConjunta, myControlaAltPvp, myControlaAltMC, myControlaAltPCT

	** Verificar se existe nova vers�o para reiniciar o software
	uf_chk_nova_versao()

	regua(0,8,"A carregar painel...")
	regua(1,1,"A verificar perfil...")


	&& CONTROLA PERFIS DE ACESSO
	IF !uf_gerais_validaPermUser(ch_userno,ch_grupo,'Gest�o de Stock')
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ACEDER AO PAINEL GEST�O DE STOCK.","OK","",48)
		RETURN .f.
	ENDIF
	
	&& Valida Parametro
	IF EMPTY(lcRef)
		lcRef = ''
	ENDIF
	
	regua(1,2,"A carregar cursores...")

	** carrega cursores default
	uf_stocks_cursoresDefault(lcRef)
	
	*****************************************************
	*		ABERTURA DO PAINEL DE GEST�O DE STOCKS		*
	*****************************************************
	LOCAL lcArranque
	
	regua(1,3,"A abrir painel...")
	
	IF type("stocks")=="U"

		**Valida Licenciamento	
		IF (uf_gerais_addConnection('ST') == .f.)
			RETURN .F.
		ENDIF

		&& Cursor de Recursos do Artigo
		lcSQL = ""
		TEXT TO lcSQL TEXTMERGE NOSHOW
			exec up_Marcacoes_ListaRecursosSt '<<ALLTRIM(st.ref)>>', <<mysite_nr>>
		ENDTEXT
		IF !uf_gerais_actGrelha("","ucrsListaRecursosST",lcSQL)
			MESSAGEBOX("N�O FOI POSS�VEL OBTER LISTAGEM DE RECURSOS. POR FAVOR CONTACTE O SUPORTE.",48,"LOGITOOLS SOFTWARE")
			RETURN .f.
		ENDIF	

		&& Cursor de Servicos associados ao Artigo
		TEXT TO lcSQL TEXTMERGE NOSHOW
			exec up_marcacos_STST '<<ALLTRIM(st.ref)>>', <<mysite_nr>>
		ENDTEXT	
		IF !uf_gerais_actgrelha("", "uCrsStSt", lcSQL)
			MESSAGEBOX("OCORREU UM ERRO A VERIFICAR OS TECNICOS ASSOCIADOS � ACTIVIDADE! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
			RETURN .f.
		ENDIF

		&& componentes
		TEXT TO lcSQL TEXTMERGE NOSHOW
			exec up_stocks_componentes  '<<ALLTRIM(st.ref)>>', <<mysite_nr>>
		ENDTEXT	
		IF !uf_gerais_actgrelha("", "ucrsComponentes", lcSQL)
			MESSAGEBOX("OCORREU UM ERRO A VERIFICAR OS COMPONENTES DO PRODUTO! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
			RETURN .f.
		ENDIF

		DO FORM stocks WITH .t., lcATop

		uf_stocks_carregaMenu()
				
		lcArranque = .t.
	ELSE
		stocks.show
		
		
		
		&& Cursor de Recursos do Artigo
		lcSQL = ""
		TEXT TO lcSQL TEXTMERGE NOSHOW
			exec up_Marcacoes_ListaRecursosSt '<<ALLTRIM(st.ref)>>', <<mysite_nr>>
		ENDTEXT
		IF !uf_gerais_actGrelha("stocks.pageframe1.page5.gridEquipamentos","ucrsListaRecursosST",lcSQL)
			MESSAGEBOX("N�O FOI POSS�VEL OBTER LISTAGEM DE RECURSOS. POR FAVOR CONTACTE O SUPORTE.",48,"LOGITOOLS SOFTWARE")
			RETURN .f.
		ENDIF
		
		&& Cursor de Servicos associados ao Artigo
		TEXT TO lcSQL TEXTMERGE NOSHOW
			exec up_marcacos_STST '<<ALLTRIM(st.ref)>>', <<mysite_nr>>
		ENDTEXT	
		IF !uf_gerais_actgrelha("stocks.pageframe1.page5.gridServicos", "uCrsStSt", lcSQL)
			MESSAGEBOX("OCORREU UM ERRO A VERIFICAR OS TECNICOS ASSOCIADOS � ACTIVIDADE! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
			RETURN .f.
		ENDIF
		
		&& componentes
		TEXT TO lcSQL TEXTMERGE NOSHOW
			exec up_stocks_componentes  '<<ALLTRIM(st.ref)>>', <<mysite_nr>>
		ENDTEXT	
		IF !uf_gerais_actgrelha("stocks.pageframe1.page3.gridComponentes", "ucrsComponentes", lcSQL)
			MESSAGEBOX("OCORREU UM ERRO A VERIFICAR OS COMPONENTES DO PRODUTO! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
			RETURN .f.
		ENDIF
				
	ENDIF
	**
	&& controlar pct e margem
	SELECT st
	stocks.alterapct = st.epcusto
	stocks.alteramargem = st.marg1
		
	&& Calcula o Stock Previsto
	SELECT st
	stocks.pageframe1.page1.txtTotal.value = (ST.Stock + ST.qttfor - ST.qttcli)
	stocks.pageframe1.page3.txtTotal.value = (ST.Stock + ST.qttfor - ST.qttcli)
	
	&& calcula PVA e PVF
	DO CASE 
		CASE st.epv1 > 0 AND st.epv1 <= 6.68
			stocks.Pageframe1.Page1.txtpva.value = ((st.epv1-0.94)/1.1475)+((st.epv1/1.06)*0.004)
		CASE st.epv1 >= 6.69 AND st.epv1 <= 9.97
		 	stocks.Pageframe1.Page1.txtpva.value = ((st.epv1-1.95)/1.146)+((st.epv1/1.06)*0.004)
		CASE st.epv1 >= 9.98 AND st.epv1 <= 14.1 
			stocks.Pageframe1.Page1.txtpva.value = ((st.epv1-2.66)/1.1439)+((st.epv1/1.06)*0.004)
		CASE st.epv1 >= 14.2 AND st.epv1 <= 26.96
			stocks.Pageframe1.Page1.txtpva.value = ((st.epv1-4.17)/1.1393)+((st.epv1/1.06)*0.004)
		CASE st.epv1 >= 26.97 AND st.epv1 <= 64.58
			stocks.Pageframe1.Page1.txtpva.value = ((st.epv1-8)/1.1316)+((st.epv1/1.06)*0.004)
		OTHERWISE 
			stocks.Pageframe1.Page1.txtpva.value = ((st.epv1-12.73)/1.105)+((st.epv1/1.06)*0.004)
	ENDCASE 
	
	DO CASE 
		CASE st.epv1>0 AND st.epv1 <= 6.68
			stocks.Pageframe1.Page1.txtpvf.value = ((st.epv1*0.89098)-0.587522)+((st.epv1/1.06)*0.004)
		CASE st.epv1 >= 6.69 AND st.epv1 <= 9.97
			stocks.Pageframe1.Page1.txtpvf.value = ((st.epv1*0.8915)-1.21849)+((st.epv1/1.06)*0.004)
		CASE st.epv1 >= 9.98 AND st.epv1 <= 14.1 
			stocks.Pageframe1.Page1.txtpvf.value = ((st.epv1*0.8927)-1.6647)+((st.epv1/1.06)*0.004)
		CASE st.epv1 >= 14.2 AND st.epv1 <= 26.96 
			stocks.Pageframe1.Page1.txtpvf.value = ((st.epv1*0.8953)-2.6133)+((st.epv1/1.06)*0.004)
		CASE st.epv1 >= 26.97 AND st.epv1 <= 64.58
			stocks.Pageframe1.Page1.txtpvf.value = ((st.epv1*0.899965)-4.9997)+((st.epv1/1.06)*0.004)
		OTHERWISE 
			stocks.Pageframe1.Page1.txtpvf.value = ((st.epv1*0.915656)-7.9763)+((st.epv1/1.06)*0.004)
	ENDCASE 

	&& configurar o ecr� caso tenha aberto agora o painel
	regua(1,8,"A configurar ecr�...")
	uf_stocks_confEcra(1)

	uf_stocks_alternaMenu()

	&& reset ao container detalheSt
	stocks.pageframe1.page1.detalhest1.reset

	&& primeira imagem
	uf_stocks_trocaImg(0)

	
	&& campanhasOnline
	uf_stocks_validaCampanhasOnline()

	
	uf_stocks_ecraStRobot()
	
	uf_stocks_controlaSeVoltaChamarPrecosSpms()
			
	stocks.REFRESH
	regua(2)
ENDFUNC




**
FUNCTION uf_stocks_cursoresDefault
	LPARAMETERS lcRef
	

	LOCAL lcSql

	&& Cursor Principal
	lcSQL=''
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_stocks_dadosRef '<<ALLTRIM(lcRef)>>', <<mySite_nr>>
	ENDTEXT
	IF !uf_gerais_actGrelha("", "ST", lcSql)
		RETURN .f.
	ENDIF
	
	
	&& Cursor de CampnhasOnLine
	lcSQL=''
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_stocks_campanhaOnline '<<ALLTRIM(lcRef)>>', <<mySite_nr>>
	ENDTEXT
	IF !uf_gerais_actGrelha("", "ucrsCampanhaOnline", lcSql)
		RETURN .f.
	ENDIF
	
	IF RECCOUNT("ST") == 0
		SELECT st
		APPEND BLANK
		GO TOP
	ENDIF

	** Copia dados para gravra dados no historico de altere��es de PVP
	SELECT ref, epv1, marg1, marg2, marg3, marg4 FROM ST INTO CURSOR ucrsAlteracaoHistPVP READWRITE

	&& Cursor de Unidades
	IF !USED("uCrsUnidade")
		IF !uf_gerais_actGrelha("", "uCrsunidade", [select '' as campo union all Select left(campo,25) from b_multidata(nolock) where tipo='st_unidade' order by campo])
			RETURN
		ENDIF
	ENDIF
	
	&& Cursor com Indicadores Produto - Necess�rio rever formula de Calculo por est� a tornar o software muito lento
	lcSQL = ''
	TEXT TO lcSQL TEXTMERGE NOSHOW 
		exec up_stocks_calcindicadores '<<ALLTRIM(lcRef)>>', '<<mysite>>' , <<mySite_nr>>
	ENDTEXT 		
	IF !uf_gerais_actGrelha("", "uCrsindicadores", lcSQL)
		uf_perguntalt_chama("Ocorreu uma anomalia a verificar os indicadores do Produto.","OK","",16)
		RETURN .f.
	ENDIF

	&& Cursor para a ComboBox Tipo de Atributos
	IF !USED("uCrsAtrib")
		IF !uf_gerais_actGrelha("", "uCrsAtrib", "exec up_stocks_Atributos")
			RETURN
		ENDIF
	ENDIF

	&& Cursor para a ComboBox de Atributos
	IF !USED("uCrsTipoAtrib")
		IF !uf_gerais_actGrelha("", "uCrsTipoAtrib", [select distinct tipo from B_Atributos (nolock) order by tipo])
			RETURN
		ENDIF
	ENDIF
	
	&& Cursor de Lotes
	lcSQL = ''
	IF !uf_gerais_getParameter("ADM0000000211","BOOL")
		TEXT TO lcSQL TEXTMERGE NOSHOW
			SET fmtonly on
			exec up_ga_lotePorRef ''
			SET fmtonly off
		ENDTEXT 
	ELSE
		TEXT TO lcSQL TEXTMERGE NOSHOW
			exec up_ga_lotePorRef '<<lcRef>>'
		ENDTEXT 
	ENDIF
	
	IF !uf_gerais_actGrelha(IIF(TYPE("stocks")=="U","","stocks.pageframe1.page6.grdLotes"), "uCrsLotesProd", lcSQL)
		RETURN .f.
	ENDIF
	
	Local lcLote, lcLote2, lcAgrupa
	Store '' To lcLote, lcLote2
	SELECT uCrsLotesProd
	GO TOP
	lcLote = UPPER(ALLTRIM(uCrsLotesProd.lote))
	SELECT uCrsLotesProd
	SCAN
		IF VAL(uCrsLotesProd.id) > 1
			SELECT uCrsLotesProd
			replace ;
				uCrsLotesProd.stock WITH '' ;
				uCrsLotesProd.validade WITH '' ;
				uCrsLotesProd.ultmov WITH ''
		ENDIF
		
		lcLote2 = UPPER(ALLTRIM(uCrsLotesProd.lote))
		If !(lcLote == lcLote2 )
			If lcAgrupa
				lcAgrupa = .f.
			Else
				lcAgrupa = .t.
			Endif
		Endif
		replace uCrsLotesProd.agrupar WITH lcAgrupa
		lcLote = UPPER(ALLTRIM(uCrsLotesProd.lote))
	ENDSCAN
	SELECT uCrsLotesProd
	GO TOP
	IF !(TYPE("stocks") == "U")
		stocks.pageframe1.page6.grdLotes.refresh
	
	ENDIF 
	
	
	
	&& Cursor com as imagens de producto
	uf_stocks_criaCursorImagens()
	


ENDFUNC


FUNCTION uf_stocks_trocaImgCima
	LPARAMETERS lcOrdem
	
	SELECT ucrsImgProd
	TRY
      GOTO recno() + lcOrdem 
   CATCH
 	  GO TOP
   ENDTRY	 
   

   
   SELECT ucrsImgProd	                  
   return ucrsImgProd.imagePath

ENDFUNC


FUNCTION uf_stocks_trocaImgBaixo
	LPARAMETERS lcOrdem
	
	SELECT ucrsImgProd
	TRY
       GOTO recno() + lcOrdem 
    CATCH
 	   GO BOTTOM
 	ENDTRY
 	

 	
   SELECT ucrsImgProd	                  
   return ucrsImgProd.imagePath

ENDFUNC

FUNCTION uf_stocks_retornaValoresCampanhasOnline
	LPARAMETERS lcValue, lcPerc, lcOffer,lcBuy
	
	LOCAL lcOferta, lcCompra
	STORE 0 TO lcOferta,lcCompra
	
	

	DO CASE 
		CASE !EMPTY(lcValue)
			if(VARTYPE(lcValue)!='N')
				lcOferta= ROUND(VAL(lcValue),2)
			ELSE
				lcOferta = ROUND(lcValue,2)
			ENDIF			
		CASE !EMPTY(lcPerc) 
			if(VARTYPE(lcPerc)!='N')
				lcOferta= ROUND(VAL(lcPerc),2)
			ELSE
				lcOferta= ROUND(lcPerc,2)	
			ENDIF	
		CASE !EMPTY(lcOffer)
			if(VARTYPE(lcOffer)!='N')
				lcOferta= ROUND(VAL(lcOffer),2)
			ELSE
				lcOferta= ROUND(lcOffer,2)	
			ENDIF
			
		OTHERWISE 
			lcOferta=0	
	ENDCASE 		
	
	if(!EMPTY(lcBuy)) 
		if(VARTYPE(lcBuy)!='N') 
			lcCompra= ROUND(VAL(lcBuy),2)
		ELSE
			lcCompra= ROUND(lcBuy,2)	
		ENDIF	
	ENDIF
	
	

		
	select ucrsCampanhaOnline	
	GO TOP
	replace valorOferta WITH lcOferta
	replace valorCompra WITH lcCompra
	


ENDFUNC



FUNCTION uf_stocks_validaCampanhasOnline
		
		
	IF(USED("st"))
		if(VARTYPE(stocks.pageframe1.page7)!='U')
			if(USED("ucrsCampanhaOnline"))
				LOCAL lcCampId, lcValueBuy, lcValueOffer
				Store  0 TO lcCampId,lcValueBuy, lcValueOffer
				
			
				
				IF !myStocksIntroducao AND  !myStocksAlteracao
					SELECT ucrsCampanhaOnline
					GO TOP
					lcValueBuy = ucrsCampanhaOnline.valorCompra
					lcValueOffer= ucrsCampanhaOnline.valorOferta			
				ENDIF
				
		
		
				
				lcCampId  = stocks.Pageframe1.Page7.optu_campanhaOnline.value
	
				
				DO CASE 
					CASE lcCampId  = 0
						stocks.pageframe1.page7.txtCampanhaValue.value = ''
						stocks.pageframe1.page7.txtCampanhaPerc.value = ''
						stocks.pageframe1.page7.txtCampanhaOffer.value = ''
						stocks.pageframe1.page7.txtCampanhaBuy.value = ''
						uf_stocks_controlaObjCampanhasOnlineDataReset()
						uf_stocks_controlaObjCampanhasOnlineData(.f.)
						
						
					CASE lcCampId  = 1
						if(!myStocksIntroducao AND  !myStocksAlteracao)
					   		stocks.pageframe1.page7.txtCampanhaValue.value = lcValueOffer
					   	endif	
						stocks.pageframe1.page7.txtCampanhaPerc.value = ''
						stocks.pageframe1.page7.txtCampanhaOffer.value = ''
						stocks.pageframe1.page7.txtCampanhaBuy.value = ''
						uf_stocks_controlaObjCampanhasOnlineData(.t.)							
					CASE lcCampId  = 2				
						stocks.pageframe1.page7.txtCampanhaValue.value = ''
						if(!myStocksIntroducao AND  !myStocksAlteracao)
							stocks.pageframe1.page7.txtCampanhaPerc.value = lcValueOffer
						endif	
						stocks.pageframe1.page7.txtCampanhaOffer.value = ''
						stocks.pageframe1.page7.txtCampanhaBuy.value = ''
						uf_stocks_controlaObjCampanhasOnlineData(.t.)								
					CASE  lcCampId  = 3
						stocks.pageframe1.page7.txtCampanhaValue.value = ''
						stocks.pageframe1.page7.txtCampanhaPerc.value = ''
						if(!myStocksIntroducao AND  !myStocksAlteracao)
							stocks.pageframe1.page7.txtCampanhaOffer.value = lcValueOffer
							stocks.pageframe1.page7.txtCampanhaBuy.value = lcValueBuy 	
						ENDIF
						uf_stocks_controlaObjCampanhasOnlineData(.t.)	
						
					OTHERWISE 
						stocks.pageframe1.page7.txtCampanhaValue.value = ''
						stocks.pageframe1.page7.txtCampanhaPerc.value = ''
						stocks.pageframe1.page7.txtCampanhaOffer.value = ''
						stocks.pageframe1.page7.txtCampanhaBuy.value = ''	
						uf_stocks_controlaObjCampanhasOnlineDataReset()
						uf_stocks_controlaObjCampanhasOnlineData(.f.)
						
				ENDCASE 
				


				uf_stocks_retornaValoresCampanhasOnline(stocks.pageframe1.page7.txtCampanhaValue.value,;
				stocks.pageframe1.page7.txtCampanhaPerc.value,stocks.pageframe1.page7.txtCampanhaOffer.value,stocks.pageframe1.page7.txtCampanhaBuy.value)	
				

		
						
				
			ENDIF
			
			
		ENDIF
		

	
	ENDIF

ENDFUNC



&&  0 - topo 
&&  1 - cima 
&&  -1 - baixo



FUNCTION uf_stocks_trocaImg
	LPARAMETERS lcOrdem
	
	IF(USED("st"))
		
		SELECT st
		GO TOP 
			IF !EMPTY(st.ststamp)
				LOCAL lcImagePath
				lcImagePath=''
			

				if(USED("ucrsImgProd"))
				

					SELECT   ucrsImgProd
				
					if(lcOrdem == 0)
					
						SELECT   ucrsImgProd
						GO TOP
                        lcImagePath=ucrsImgProd.imagePath 
                    ENDIF
                        
					if(lcOrdem > 0)
					
					   lcImagePath = uf_stocks_trocaImgCima(lcOrdem)
                    ENDIF   
						
					if(lcOrdem <0)
					
                       lcImagePath = uf_stocks_trocaImgBaixo(lcOrdem)
					ENDIF   
					

					stocks.pageframe1.page7.imgProd.picture = ALLTRIM(lcImagePath)

					
					if(RECCOUNT("ucrsImgProd")>0)
						stocks.pageframe1.page7.lblcorrouselCaption.Caption = "Imagem: " + ALLTRIM(STR(RECNO("ucrsImgProd"))) + " de " +  ALLTRIM(STR(RECCOUNT("ucrsImgProd")))
					ELSE
						stocks.pageframe1.page7.lblcorrouselCaption.Caption = "Nenhuma imagem disponivel"
					endif
							
				ENDIF		
				
				
		 ENDIF
	
	ENDIF
	
	
	
	


ENDFUNC




**
FUNCTION uf_stocks_criaCursorImagens

	IF(USED("st"))
		
		SELECT st
		GO TOP 
			IF !EMPTY(st.ststamp)
				
				IF USED("ucrsImgProd")
					fecha("ucrsImgProd")
				ENDIF
			
				LOCAL  lcSQL 
				lcSQL = ''
				TEXT TO lcSQL TEXTMERGE NOSHOW 
					exec up_stocks_imagensProd '<<ALLTRIM(st.ststamp)>>',  <<mySite_nr>>, 'imagemProd'
				ENDTEXT 		
				IF !uf_gerais_actGrelha("", "ucrsImgProd", lcSQL)
					uf_perguntalt_chama("Ocorreu uma anomalia a retornar as imagens do producto.","OK","",16)
					RETURN .f.
				ENDIF			
				
			ENDIF
		
		
	
	ENDIF
	
	
	

ENDFUNC

**
FUNCTION uf_stocks_documentos
	IF USED("st")
		IF !EMPTY(st.ststamp)
			uf_listadocumentos_chama(st.ref, 0, st.design, "ST")
		ENDIF
	ENDIF
ENDFUNC


**
FUNCTION uf_stocks_carregaMenu
	&& Configura menu principal
	WITH stocks.menu1
		**.adicionaOpcao("aplicacoes", "Aplica��es", myPath + "\imagens\icons\aplicacoes_w.png", "uf_gerais_menuAplicacoes With 'stocks'","O")
		.adicionaOpcao("opcoes", "Op��es", myPath + "\imagens\icons\opcoes_w.png", "","F1")
		.adicionaOpcao("pesquisar", "Pesquisar", myPath + "\imagens\icons\lupa_w.png", [uf_pesqstocks_chama with "STOCKS"],"F2")
		.adicionaOpcao("importDic", "Imp. Dic.", myPath + "\imagens\icons\stocks_seta_w.png", "uf_stocks_importDicSt","F3")
		.adicionaOpcao("novo", "Novo", myPath + "\imagens\icons\mais_w.png", "uf_stocks_novo","F4")
		.adicionaOpcao("editar", "Editar", myPath + "\imagens\icons\lapis_w.png", "uf_stocks_editar","F5")
		.adicionaOpcao("anterior", "Anterior", myPath + "\imagens\icons\ponta_seta_left_w.png", "uf_stocks_anterior","F6")
		.adicionaOpcao("seguinte", "Seguinte", myPath + "\imagens\icons\ponta_seta_rigth_w.png", "uf_stocks_seguinte","F7")
		.adicionaOpcao("todasEmpresas","Empresas",myPath + "\imagens\icons\unchecked_w.png","uf_stocks_criaFichaEmpresas","F8")
		TEXT To lcSQL TEXTMERGE NOSHOW
			select site from empresa order by site
		ENDTEXT 
		uf_gerais_actGrelha("","ucrsEmpresasTmp",lcSQL)
		IF RECCOUNT("ucrsEmpresasTmp")>1 then
			stocks.menu1.adicionaOpcao("selempresas", "Sel. Empresas", myPath + "\imagens\icons\empresa_b_lateral.png", "uf_altgr_escolheEmpresasST","")
		ENDIF 

	ENDWITH 
	
	** Aplica��es
**	WITH stocks.menu_aplicacoes
**		.adicionaOpcao("validades", "Confer�ncia de Validades", "", "uf_stocks_chamaValidades")
**		.adicionaOpcao("confPvps", "Gest�o de PVPs", "", [uf_confpvp_chama WITH "STOCKS"])
**		.adicionaOpcao("altGrupo", "Alt. em Grupo", "", "uf_pesqstocks_chama with 'STOCKS_ALTERACOES' ")
**		.adicionaOpcao("inventario", "Inventario", "", "uf_inventario_chama with ''")
**		.adicionaOpcao("projetoViaVerde", "Enviar VVM.", "", "uf_stocks_enviarVVM")
**	ENDWITH 
	
	&& configura menu de op��es
	WITH stocks.menu_opcoes
		.adicionaOpcao("movimentos", "Movimentos", "", "uf_stocks_chamaMovimentos")
		.adicionaOpcao("etiquetas", "Etiquetas", "", "uf_stocks_etiquetas")
        .adicionaOpcao("encomendar", "Encomendar", "", [uf_encautomaticas_chama WITH  "STOCKS", .F., .F.])
		**.adicionaOpcao("stockArm", "Stock Armaz�m", "", "uf_stocks_chamaStockArm")
		.adicionaOpcao("multiloja", IIF(MyEncConjunta == .f.,"Ver detalhe Multi-Loja","Ver Detalhe Loja �nica"), "", "uf_stocks_MultiLoja")
		.adicionaOpcao("verDic", "Ver Inf. Farmac�utica", "", [uf_stocks_chamaDicionario])
		.adicionaOpcao("criarDic", "Criar Inf. Farmac�utica", "", "uf_stocks_criaProdFprod")
		.adicionaOpcao("consultarProd", "Consultar Produto", "", "uf_stocks_consultarProd with 'Stocks'")
		.adicionaOpcao("documentos", "Consulta Documentos","", "uf_stocks_chamaDocumentos")
		.adicionaOpcao("imprimir", "Imprimir", "", "uf_stocks_ImprimeArtigo")
		.adicionaOpcao("prioritarios", "Imp. Priorit�rios", "", "uf_stocks_actualizaImportaPrioritarios")
		.adicionaOpcao("atualizar", "Atualizar", myPath + "\imagens\icons\actualizar_w.png", "uf_stocks_refrescar")
		**.adicionaOpcao("eliminar", "Eliminar", myPath + "\imagens\icons\cruz_w.png", "uf_stocks_eliminar") ** Retirado TD-48086
		.adicionaOpcao("ultimo", "�ltimo", "", "uf_stocks_ultimo")
		.adicionaOpcao("corrige", "Corrigir stocks", "", "uf_stocks_CorrigeSA_global")
		.adicionaOpcao("corrigem", "Atualizar PCP Artigo", "", "uf_stocks_Corrige_mov")
		.adicionaOpcao("corrigeT", "Atualizar PCP Todos Art.", "", "uf_stocks_Corrige_mov_todos")
		.adicionaOpcao("log", "Log de altera��es", "", "uf_LOGstocks_chama with ALLTRIM(st.ststamp)")
		.adicionaOpcao("escaloespv", "Escal�es PV", "", "uf_ESCALOESPV_chama")
		.adicionaOpcao("anexos", "Anexos", "", "uf_anexardoc_chama with 'st', ALLTRIM(st.ststamp), 'Ref: '+ ALLTRIM(st.ref), ALLTRIM(st.ref), 'imagemProd'")
        .adicionaOpcao("validades", "Confer�ncia de Validades", "", "uf_stocks_chamaValidades")
        .adicionaOpcao("confPvps", "Gest�o de PVPs", "", [uf_confpvp_chama WITH "STOCKS"])
        .adicionaOpcao("altGrupo", "Alt. em Grupo", "", "uf_pesqstocks_chama with 'STOCKS_ALTERACOES' ")
        .adicionaOpcao("inventario", "Inventario", "", "uf_inventario_chama with ''")
        .adicionaOpcao("projetoViaVerde", "Enviar VVM.", "", "uf_stocks_enviarVVM")
	ENDWITH 
		
	IF uf_gerais_getParameter("ADM0000000221","BOOL")
		stocks.menu_opcoes.adicionaOpcao("mapaconsis", "CONSIS Mapa", "", "uf_MAPAROBOT_Chama")
	ENDIF 
	
	IF uf_gerais_getParameter_site('ADM0000000064', 'BOOL', mySite)
		stocks.menu_opcoes.adicionaOpcao("histprecos", "Hist. Pre�os", "", "uf_histprecos_chama")
	ENDIF 
	
	IF emdesenvolvimento == .t.
		stocks.menu_opcoes.adicionaOpcao("actSa", "Corrigir SA", "", "uf_stocks_CorrigeSA")
	ENDIF
	
	stocks.Menu1.gravar.img1.Picture = myPath + "\imagens\icons\guardar_w.png"
	
ENDFUNC


**
FUNCTION uf_stocks_alternaMenu
	IF myStocksIntroducao OR myStocksAlteracao
		stocks.menu1.estado("", "", "Gravar", .t., "Cancelar", .t.)
		stocks.menu1.estado("pesquisar, novo, editar, anterior, seguinte", "HIDE")
		** stocks.menu_opcoes.estado("ultimo,eliminar", "HIDE") ** Retirado TD-48086
		stocks.menu_opcoes.estado("ultimo", "HIDE")
		stocks.menu_opcoes.estado("imprimir, movimentos, etiquetas, documentos,  VerDic, criarDic, ultimo, encomendar", "HIDE") &&recursos
		IF uf_gerais_getParameter("ADM0000000221","BOOL")
			stocks.menu_opcoes.estado("mapaconsis", "HIDE") 
		ENDIF 			
		stocks.menu1.estado("importDic", "SHOW")
		TEXT To lcSQL TEXTMERGE NOSHOW
			select site from empresa order by site
		ENDTEXT 
		uf_gerais_actGrelha("","ucrsEmpresasTMP",lcSQL)
		IF RECCOUNT("ucrsEmpresasTMP")>1 then
			stocks.menu1.estado("todasEmpresas,selempresas", "SHOW")
		ENDIF 
	ELSE
		stocks.menu1.estado("", "", "Gravar", .f., "Sair", .t.)
		stocks.menu1.estado("pesquisar, novo, editar, anterior, seguinte", "SHOW")
		** stocks.menu_opcoes.estado("ultimo,eliminar", "SHOW") ** Retirado TD-48086
		stocks.menu_opcoes.estado("ultimo", "SHOW")
		stocks.menu1.estado("importDic", "HIDE")
		TEXT To lcSQL TEXTMERGE NOSHOW
			select site from empresa order by site
		ENDTEXT 
		uf_gerais_actGrelha("","ucrsEmpresasTMP",lcSQL)
		IF RECCOUNT("ucrsEmpresasTMP")>1 then
			stocks.menu1.estado("todasEmpresas, selempresas", "HIDE")
		ENDIF 
		SELECT st
		IF EMPTY(st.ref)
			stocks.menu_opcoes.estado("imprimir, movimentos, etiquetas, documentos, criarDic, encomendar", "HIDE")
		ELSE
			stocks.menu_opcoes.estado("imprimir, movimentos, etiquetas, documentos, VerDic, criarDic, ultimo, encomendar", "SHOW") &&recursos
		ENDIF
		IF uf_gerais_getParameter("ADM0000000221","BOOL")
			stocks.menu_opcoes.estado("mapaconsis", "SHOW")
		ENDIF 
	ENDIF
	IF myStocksIntroducao
		TEXT To lcSQL TEXTMERGE NOSHOW
			select site from empresa order by site
		ENDTEXT 
		uf_gerais_actGrelha("","ucrsEmpresasTMP",lcSQL)
		IF RECCOUNT("ucrsEmpresasTMP")>1 then
			stocks.menu1.estado("selempresas", "HIDE")
		ENDIF 
	ENDIF 
	IF alltrim(ucrse1.tipoempresa)= 'ARMAZEM'
		stocks.menu_opcoes.estado("escaloespv", "SHOW")
	ELSE
		stocks.menu_opcoes.estado("escaloespv", "HIDE")
	ENDIF 
		
	IF alltrim(ucrse1.tipoempresa)= 'ARMAZEM'
		IF ALLTRIM(St.familia)='1' OR ALLTRIM(St.familia)='2'
			stocks.Pageframe1.Page1.txtepv1.Top=20
			stocks.Pageframe1.Page1.txtPCT.Top=47
			stocks.Pageframe1.Page1.tabiva.Top=20
			stocks.Pageframe1.Page1.txtMC.Top=47
			stocks.Pageframe1.Page1.txtpva.visible=.t.
			stocks.Pageframe1.Page1.txtpvf.visible=.t.
			stocks.Pageframe1.Page1.Label19.visible=.t.
			stocks.Pageframe1.Page1.Label20.visible=.t.
			stocks.Pageframe1.Page1.Label6.Top=23
			stocks.Pageframe1.Page1.Label1.Top=50
			stocks.Pageframe1.Page1.Label3.Top=23
			stocks.Pageframe1.Page1.Label5.Top=50
		ELSE  
			stocks.Pageframe1.Page1.txtepv1.Top=35
			stocks.Pageframe1.Page1.txtPCT.Top=64
			stocks.Pageframe1.Page1.tabiva.Top=35
			stocks.Pageframe1.Page1.txtMC.Top=64
			stocks.Pageframe1.Page1.txtpva.visible=.f.
			stocks.Pageframe1.Page1.txtpvf.visible=.f.
			stocks.Pageframe1.Page1.Label19.visible=.f.
			stocks.Pageframe1.Page1.Label20.visible=.f.
			stocks.Pageframe1.Page1.Label6.Top=38
			stocks.Pageframe1.Page1.Label1.Top=67
			stocks.Pageframe1.Page1.Label3.Top=38
			stocks.Pageframe1.Page1.Label5.Top=67
		ENDIF
	ELSE
		stocks.Pageframe1.Page1.txtepv1.Top=35
		stocks.Pageframe1.Page1.txtPCT.Top=64
		stocks.Pageframe1.Page1.tabiva.Top=35
		stocks.Pageframe1.Page1.txtMC.Top=64
		stocks.Pageframe1.Page1.txtpva.visible=.f.
		stocks.Pageframe1.Page1.txtpvf.visible=.f.
		stocks.Pageframe1.Page1.Label19.visible=.f.
		stocks.Pageframe1.Page1.Label20.visible=.f.
		stocks.Pageframe1.Page1.Label6.Top=38
		stocks.Pageframe1.Page1.Label1.Top=67
		stocks.Pageframe1.Page1.Label3.Top=38
		stocks.Pageframe1.Page1.Label5.Top=67
	ENDIF 
	

ENDFUNC


**
FUNCTION uf_stocks_refrescar
	SELECT st
	uf_stocks_chama(ALLTRIM(st.ref))
ENDFUNC


**
FUNCTION uf_stocks_MultiLoja
	PUBLIC MyEncConjunta
	
	IF MyEncConjunta == .f.
		MyEncConjunta = .t.
		Stocks.menu_opcoes.multiloja.config("Ver detalhe Loja �nica","","uf_stocks_MultiLoja")
	ELSE
		MyEncConjunta = .f.
		Stocks.menu_opcoes.multiloja.config("Ver detalhe Multi-Loja","","uf_stocks_MultiLoja")
	ENDIF

	WITH Stocks.pageframe1.page1 
		.detalhest1.myActivateResumoRef = ""
		.detalhest1.myActivate5baratos = ""
		.detalhest1.myActivatebonusref = ""
		.detalhest1.myActivateesgotadosref = ""
		.detalhest1.myActivatehistprecoscompras = ""
		.detalhest1.myActivatesaidasref = ""
		.detalhest1.myActivatehistprecosref = ""
		.detalhest1.refresh
	ENDWITH 
		
	Stocks.refresh
ENDFUNC


**
FUNCTION uf_stocks_etiquetas
	IF USED("st")
		IF !EMPTY(st.ststamp)
            IF !uf_gerais_getParameter("ADM0000000343","BOOL")
		        uf_etiquetas_chama("ST")
            ELSE

*!*	                uv_validImp = .F.

*!*	                DIMENSION laPrinters[1,1] 
*!*	                = APRINTERS(laPrinters)
*!*	                    
*!*	                FOR lnIndex = 1 TO ALEN(laPrinters,1)

*!*	                    lcPrinterName=laPrinters(lnIndex,1)

*!*	                    IF LEFT(UPPER(alltrim(lcPrinterName)), 9) = "ETIQUETAS"
*!*	                        uv_validImp = .T.
*!*	                    ENDIF

*!*	                ENDFOR 

*!*	                IF uv_validImp
*!*	                    uf_imprimirgerais_Chama("ETIQUETAS_STOCKS")
*!*	                ELSE

*!*	                    uf_perguntalt_chama("N�o existe nenhuma impressora de Etiquetas." + chr(13) + "Por favor contacte o suporte.","OK","", 48)
*!*	                    RETURN .F.
*!*	                    
*!*	                ENDIF

				uf_imprimirgerais_Chama("ETIQUETAS_STOCKS")

            ENDIF
		ENDIF
	ENDIF
ENDFUNC


*************************************
*		Configura Ecra Stocks		*
*************************************
FUNCTION uf_stocks_confEcra
	LPARAMETERS tcEstado
		
	IF USED("st")
		
	
	
		LOCAL lcBool
		
		SELECT st
		IF st.stns && se for servi�o esconde os campos de stock
			lcBool = .f.
		ELSE
			lcBool = .t.
		ENDIF

		** Convers�o
*!*			stocks.pageframe1.page1.label21.visible			= lcBool
*!*			stocks.pageframe1.page1.shape1.visible			= lcBool
*!*			stocks.pageframe1.page1.label18.visible			= lcBool
*!*			stocks.pageframe1.page1.label17.visible			= lcBool
*!*			stocks.pageframe1.page1.label19.visible			= lcBool
*!*			stocks.pageframe1.page1.label20.visible			= lcBool
*!*			stocks.pageframe1.page1.cmbunidade.visible		= lcBool
*!*			stocks.pageframe1.page1.cmbuni2.visible			= lcBool
*!*			stocks.pageframe1.page1.txtConversao.visible	= lcBool
*!*			stocks.pageframe1.page1.txtlconversao.visible	= lcBool
		
		** Movimentos
		stocks.pageframe1.page1.label33.visible			= lcBool
		stocks.pageframe1.page1.label10.visible			= lcBool
		stocks.pageframe1.page1.txttxrotacao.visible	= lcBool
		stocks.pageframe1.page1.label28.visible		    = lcBool
		stocks.pageframe1.page1.txttxcobertura.visible	= lcBool
		stocks.pageframe1.page1.label9.visible		    = lcBool
		stocks.pageframe1.page1.txtvalcobertura.visible	= lcBool
		stocks.pageframe1.page1.label37.visible			= lcBool
		stocks.pageframe1.page1.label36.visible			= lcBool
		stocks.pageframe1.page1.txtUdata.visible		= lcBool
		stocks.pageframe1.page1.txtusaid.visible		= lcBool
		
		** Gest�o de Stock
		stocks.pageframe1.page1.label7.visible			= lcBool
		stocks.pageframe1.page1.label41.visible			= lcBool
		stocks.pageframe1.page1.txtstarm.visible		= lcBool
		stocks.pageframe1.page1.label8.visible			= lcBool
		stocks.pageframe1.page1.txtstock.visible		= lcBool
		stocks.pageframe1.page1.label11.visible			= lcBool
		stocks.pageframe1.page1.txtqttcli.visible		= lcBool
		stocks.pageframe1.page1.label12.visible			= lcBool
		stocks.pageframe1.page1.txtqttfor.visible		= lcBool
		stocks.pageframe1.page1.label13.visible			= lcBool
		stocks.pageframe1.page1.txttotal.visible		= lcBool
		stocks.pageframe1.page1.label2.visible			= lcBool
		stocks.pageframe1.page1.txtstmax.visible		= lcBool
		stocks.pageframe1.page1.label14.visible			= lcBool
		stocks.pageframe1.page1.txtptoenc.visible		= lcBool
		stocks.pageframe1.page1.label15.visible			= lcBool
		stocks.pageframe1.page1.txteoq.visible			= lcBool
		stocks.pageframe1.page1.label16.visible			= lcBool
		stocks.pageframe1.page1.cmbu_aprov.visible		= lcBool
		**stocks.pageframe1.page1.label25.visible			= lcBool
		**stocks.pageframe1.page1.txtstEmp.visible		= lcBool
		stocks.pageframe1.page1.btnEncCli.visible 		= lcBool
		stocks.pageframe1.page1.btnEncFor.visible 		= lcBool
		stocks.pageframe1.page1.btnstgrp.visible 		= lcBool
		stocks.pageframe1.page1.txtStRobot.visible 	    = lcBool
		stocks.pageframe1.page1.lblStRobot.visible 	    = lcBool	
		**separador stocks
		stocks.lbl3.enabled =  lcBool
	ENDIF
	
	stocks.pageframe1.tabs	=	.f.
	


	IF tcEstado == 1
		stocks.caption 						= "Artigos e Servi�os"
		myStocksIntroducao 					= .f.
		myStocksAlteracao 					= .f.
		
		stocks.pageframe1.enabled			= .f.
				
		&& Dados Artigo
		stocks.txtRef.readonly				= .t.
		stocks.txtDesign.readonly			= .t.
		stocks.cmbU_Fonte.enabled			= .f.
		stocks.btnRefAutomatica.enable(.f.)
		stocks.btnCodAdicionais.enable(.t.)
		stocks.chkstns.enable(.f.)
		stocks.chkInactivo.enable(.f.)
		stocks.chkqlook.enable(.f.)
		stocks.CheckMarc.enable(.f.)
		stocks.chkU_impetiq.enable(.f.)
		stocks.chkrateamento.enable(.f.)
		stocks.chkusalote.enable(.f.)
		stocks.chkusalote.enable(.f.)
		

		stocks.pageframe1.page1.chkivaIncl.enable(.f.)
		stocks.pageframe1.page1.chkArredonadamentoaut.enable(.f.)
		&&--
		stocks.pageframe1.page2.cmbfamilia.DisabledBackColor = RGB(255,255,255)	
		
		stocks.pageframe1.page1.tabiva.DisabledBackColor = RGB(255,255,255)	
		stocks.pageframe1.page1.txtEpv1.readonly = .t.
		stocks.pageframe1.page1.txtPCT.readonly = .t.
		stocks.pageframe1.page1.txtMC.readonly = .t.
		
		
		**
		stocks.txtvalidade.enabled			= .f.
		

		&& Configura campos obrigat�rios
		stocks.pageframe1.page2.cmbFamilia.enabled 			= .f.
		stocks.pageframe1.page2.txtFaminome.enabled	 		= .f.

		** Separador integra��o
		WITH stocks.pageframe1.page4
			.btncontainv.enable(.f.)
			.btncontacoe.enable(.f.)
			.btncontacev.enable(.f.)
			.btncontareo.enable(.f.)
			.btncontaieo.enable(.f.)
		ENDWITH
		
				** Separador Loja Online
		WITH  stocks.pageframe1.page7
			.chkNovidade.enable(.f.)
			.chkDispOnline.enable(.f.)
			.chkPortesGratis.enable(.f.)
			.chkPrecoSobConsulta.enable(.f.)
		
		ENDWITH
		
		stocks.pageframe1.page7.optu_campanhaOnline.enabled = .f.
		
		&& Separador Dependencias
		stocks.pageframe1.page5.SPRmin.Enabled 			= .f.
			
		**separador atributos
		stocks.pageframe1.page2.chkPrioridade.enable(.f.)
      stocks.pageframe1.page2.chkPim.enable(.f.)
		
		stocks.pageframe1.page1.txtqttminvd.readonly		= .t.
		stocks.pageframe1.page1.txtqttembal.readonly		= .t.
		
	
	ELSE
		
		IF tcEstado == 2
			myStocksIntroducao 	= .t.
			myStocksAlteracao 	= .f.

			stocks.caption		= "Artigos e Servi�os - Modo de Introdu��o"
			stocks.txtRef.readonly				=	.f. 
			stocks.txtDesign.readonly			= 	.f.
		ELSE
			myStocksIntroducao 	= .f.
			myStocksAlteracao 	= .t.
			
			stocks.caption		= "Artigos e Servi�os - Modo de Altera��o"
			stocks.txtRef.readonly				=	.t. 
			
			IF uf_gerais_getParameter("ADM0000000258","BOOL") == .f.
				
				&&valida se existem movimentos
				IF (uf_stocks_validaRefExisteTabMov('INICIO')) &&nao existem movimentos
					stocks.txtDesign.readonly			= 	.f.
				ELSE &&existem movimentos
					stocks.txtDesign.readonly			= 	.t.
				ENDIF
	
			ELSE
				stocks.txtDesign.readonly			= 	.f.
			ENDIF
**			TEXT To lcSQL TEXTMERGE NOSHOW
**				select site from empresa order by site
**			ENDTEXT 
**			uf_gerais_actGrelha("","ucrsEmpresas",lcSQL)
**			IF RECCOUNT("ucrsEmpresas")>1 then
**				stocks.menu1.adicionaOpcao("selempresas", "Sel. Empresas", myPath + "\imagens\icons\empresa_b_lateral.png", "uf_altgr_escolheEmpresas","")
**			ENDIF 
		ENDIF
		
		
		
		stocks.pageframe1.enabled			=	.T.
				
		&& Dados Artigo
		IF myStocksIntroducao 
			stocks.btnRefAutomatica.enable(.t.)
		ELSE
			TEXT TO lcSql NOSHOW TEXTMERGE PRETEXT 7
				select count(slstamp) as conta from sl (nolock) where ref='<<ALLTRIM(st.ref)>>'
			ENDTEXT
			uf_gerais_actgrelha("", [uCrsContMovSL], lcSql)
			SELECT uCrsContMovSL
			IF uCrsContMovSL.conta = 0
				stocks.btnRefAutomatica.enable(.t.)
			ELSE
				stocks.btnRefAutomatica.enable(.f.)
			ENDIF 
			fecha("uCrsContMovSL")
		ENDIF 
		stocks.btnCodAdicionais.enable(.f.)
		stocks.cmbU_Fonte.enabled			=	.t.
		stocks.chkstns.enable(.t.)
		stocks.chkInactivo.enable(.t.)
		stocks.chkqlook.enable(.t.)
		stocks.CheckMarc.enable(.t.)
		stocks.chkU_impetiq.enable(.t.)
		stocks.chkrateamento.enable(.t.)
		stocks.chkusalote.enable(.t.)

		
		**
		IF ALLTRIM(ucrse1.tipoempresa)<>'FARMACIA' AND ALLTRIM(ucrse1.tipoempresa)<>'PARAFARMACIA'
			stocks.pageframe1.page1.chkivaIncl.enable(.t.)
		ENDIF 
		stocks.pageframe1.page1.chkArredonadamentoaut.enable(.t.)
		&&--
		stocks.pageframe1.page2.cmbfamilia.DisabledBackColor = RGB(191,223,223)
		stocks.pageframe1.page1.tabiva.DisabledBackColor = RGB(191,223,223)
		stocks.pageframe1.page1.txtEpv1.readonly = .f.
		stocks.pageframe1.page1.txtPCT.readonly = .f.
		stocks.pageframe1.page1.txtMC.readonly = .f.
		
		stocks.txtvalidade.enabled			= .t.
			
		&& Configura campos obrigat�rios
		stocks.pageframe1.page2.cmbFamilia.enabled 			= .t.
		stocks.pageframe1.page2.txtFaminome.enabled	 		= .t.

		** Separador integra��o
		WITH stocks.pageframe1.page4
			.btncontainv.enable(.t.)
			.btncontacoe.enable(.t.)
			.btncontacev.enable(.t.)
			.btncontareo.enable(.t.)
			.btncontaieo.enable(.t.)
		ENDWITH
		
		
		** Separador Loja Online
		WITH stocks.pageframe1.page7
			.chkNovidade.enable(.t.)
			.chkDispOnline.enable(.t.)
			.chkPortesGratis.enable(.t.)
			.chkPrecoSobConsulta.enable(.t.)
			
		ENDWITH
		
		stocks.pageframe1.page7.optu_campanhaOnline.enabled = .t.

		&& Separador Dependencias
		stocks.pageframe1.page5.SPRmin.Enabled 				= .t.
		
		**separador atributos
		stocks.pageframe1.page2.chkPrioridade.enable(.t.)
      stocks.pageframe1.page2.chkPim.enable(.t.)
		
		IF uf_gerais_getParameter_site('ADM0000000137', 'BOOL', mySite) = .t.
			stocks.pageframe1.page1.txtqttminvd.readonly		= .f.
			stocks.pageframe1.page1.txtqttembal.readonly		= .f.
		ENDIF 
		
		
	Endif			
	
	&&configura pageframe lotes
	IF uf_gerais_getParameter("ADM0000000211","BOOL")
		stocks.lbl6.visible = .t.
	ELSE
		stocks.lbl6.visible = .f.
	ENDIF 
	

	
	
	
ENDFUNC


FUNCTION  uf_stocks_ecraStRobot

	if(vartype(stocks)=='U')
	  RETURN .f.
	ENDIF
	
	
	
	if(vartype(stocks.pageframe1)=='U')
	  RETURN .f.
	ENDIF
	
	
	if(vartype(stocks.pageframe1.page1)=='U')
	  RETURN .f.
	ENDIF
	
	
	&&stock do robot
	if(!EMPTY(myusaservicosrobot))

	    local lcValStock
		lcValStock = uf_stocks_getStockRobot()
		
		if(!empty(lcValStock) )
			if(VAL(lcValStock)>=-99)
				stocks.pageframe1.page1.txtStRobot.value = lcValStock
			ENDIF
				
		Endif
	ENDIF

ENDFUNC

******************************************
** Navega para o ultimo registo editado **
******************************************
FUNCTION uf_stocks_ultimo
	LOCAL lcUltStampSt
	
	lcUltStampSt = uf_gerais_devolveUltRegisto('ST')

	IF !EMPTY(lcUltStampSt)
		uf_stocks_chama(lcUltStampSt)
		
		uf_stocks_confecra(1)
		uf_stocks_alternaMenu()
	ENDIF
ENDFUNC


*********************************************
*		C�digos Adicionais do Artigo		*
*********************************************
FUNCTION uf_stocks_CodAdicionais
	
	IF USED("ST")
		SELECT ST
		IF EMPTY(st.ststamp)
			RETURN
		ELSE
			uf_CodigosAdicionais_chama()
		ENDIF 	
	endif	
ENDFUNC


*************************
*	INSERIR ARTIGO		*
*************************
FUNCTION uf_stocks_novo

	** Verificar se existe nova vers�o para reiniciar o software
	uf_chk_nova_versao()
	
	IF (uf_gerais_validaPermUser(ch_userno,ch_grupo,'Gest�o de Stock - Introduzir'))
		
		stocks.Caption 		= "Stocks e Servi�os - Modo de Introdu��o"
		myStocksIntroducao	= .f.
		
		&& Recria os cursores com valores vazios
		uf_stocks_chama('')
		
		** config ecra
		uf_stocks_confEcra(2)
		uf_stocks_alternaMenu()
		
		&& Preenche Valores por defeito
		uf_stocks_ValoresDefeito()
		
	ELSE
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE INTRODUZIR ARTIGOS.","OK","", 48)
	ENDIF 
ENDFUNC


*****************************
*	 Alterar Stock		*
*****************************
FUNCTION uf_stocks_editar

	** Verificar se existe nova vers�o para reiniciar o software
	uf_chk_nova_versao()
	
	IF (uf_gerais_validaPermUser(ch_userno,ch_grupo,'Gest�o de Stock - Alterar'))		
		
		&& Valida Cursor
		SELECT ST
		IF EMPTY(ST.ststamp)
			RETURN
		ENDIF
		
		&&verifica se a refer�ncia n�o est� protegida para elimina��o
		ALINES(refNedit,uf_gerais_getParameter("ADM0000000218","TEXT"),";")
		FOR i = 1 TO ALEN(refNedit)
			IF UPPER(ALLTRIM(refNedit[i])) == UPPER(ALLTRIM(st.ref))
				uf_perguntalt_chama("N�O � POSS�VEL EDITAR ESTA REFER�NCIA.","OK","",48)
				RETURN .f.
			ENDIF
		ENDFOR 
		********************
		
		myControlaAltRef = st.ref
		
		&& Configura ecra
		uf_stocks_confEcra(3)

		uf_stocks_alternaMenu()
	ELSE
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ALTERAR ARTIGOS.","OK","", 48)
	ENDIF	
ENDFUNC


***********************************************
* 			VALIDA REFRENCIAS BLOQUEADAS	  *
***********************************************

FUNCTION uf_stocks_temReferenciaBloqueada
	LPARAMETERS lcEmIntroducao, lcRef
	LOCAL lcValidaBloqueado, lcProdCodes
	STORE .t. TO lcValidaBloqueado
	
	lcProdCodes = uf_gerais_getParameter("ADM0000000277","bool")
	

	&&verifica se pode criar produto com esta referencia
	IF lcProdCodes 
		ALINES(lcRefDescBlocked,uf_gerais_getParameter("ADM0000000277","TEXT"),";")
		FOR i = 1 TO ALEN(lcRefDescBlocked)
			IF UPPER(ALLTRIM(lcRefDescBlocked[i])) == LEFT(UPPER(ALLTRIM(lcRef)),1)
				lcValidaBloqueado=.f.
			ENDIF
		ENDFOR 
	ELSE
		lcValidaBloqueado=.f.
	ENDIF
	

	** se n�o se encontra em cria��o
	IF  EMPTY(lcEmIntroducao)
		lcValidaBloqueado=.f.
	ENDIF
	
	
	
	&&Verifica se � um produto do dicionario, caso seja uma referencia bloqueada
	&&apenas � feito caso lcValidaBloqueado==.t para poupar queries ao sql
	IF(lcValidaBloqueado==.t.)
		IF !uf_gerais_actGrelha("",[uCrsDicDescBlocked],[exec up_dicionario_temRef ']+ALLTRIM(lcRef)+['])
			uf_perguntalt_chama("OCORREU UM ERRO A PESQUISAR O DICION�RIO DE MEDICAMENTOS.","OK","",16)
			lcValidaBloqueado=.f.
		ELSE
			&&valida de existe no dicionario
			IF(uCrsDicDescBlocked.contaProd>0)
				lcValidaBloqueado=.f.
			ELSE
				lcValidaBloqueado=.t.
			ENDIF
		ENDIF
	ENDIF
	
	
	&&limpa o cursor
	IF(USED("uCrsDicDescBlocked")) 
		fecha("uCrsDicDescBlocked")
	ENDIF
	

	RETURN lcValidaBloqueado
ENDFUNC



FUNCTION uf_stocks_getStockRobot

    LOCAL lcrespstkrobot, contador, lctimeout
	
	IF(!USED("st"))
		RETURN .f.
	ENDIF

	uf_gerais_meiaRegua("A consultar stock no robot")
	
	
	LOCAL lcpedstampstrobot
	LOCAL lcrefrobot, lcstockrobot
	lcpedstampstrobot = uf_gerais_stamp()
	
	lcstockrobot = .f.
	
	SELECT st
	GO TOP
	lcrefrobot = ALLTRIM(st.ref)
	
	IF !EMPTY(myusaservicosrobot)	
	
		lctimeout = uf_gerais_getparameter_site('ADM0000000035', 'NUM', mysite)
		contador = lctimeout/1000
		lcstockrobot = uf_robot_servico_getprondinfo(ALLTRIM(lcpedstampstrobot), ALLTRIM(lcrefrobot))
		IF (EMPTY(lcstockrobot))
		   lcstockrobot = .f.
		ENDIF
	ENDIF

	regua(2)

	return lcstockrobot	
	
ENDFUNC	

FUNCTION uf_stocks_controlaObjCampanhasOnlineData
	LPARAMETERS lcStatus
	

	
	IF(VARTYPE(stocks)!='U')	
		stocks.pageframe1.page7.txtCampanhaDataInicio.enabled = lcStatus
		stocks.pageframe1.page7.txtCampanhaDataFim.enabled = lcStatus
		stocks.pageframe1.page7.txtCampanhaDataInicio.refresh
		stocks.pageframe1.page7.txtCampanhaDataFim.refresh
	ENDIF
	
ENDFUNC

FUNCTION uf_stocks_controlaObjCampanhasOnlineDataReset


	
	IF(VARTYPE(stocks)!='U')	
		if(USED("ucrsCampanhaOnline"))
			SELECT ucrsCampanhaOnline
			GO TOP
			replace ucrsCampanhaOnline.dataInicio WITH   DATETIME(1900,01,01)	
			replace ucrsCampanhaOnline.dataFim    WITH   DATETIME(1900,01,01)	
			&&TABLEUPDATE()
		ENDIF
	
	ENDIF
	
ENDFUNC


FUNCTION uf_stocks_validacoesCampanhasOnline
	
	SELECT ucrsCampanhaOnline
	GO TOP
	LOCAL lcIdCampaign, lcValueOffer, lcValueBuy, lcDateInit, lcDateEnd
	STORE 0 TO lcIdCampaign,lcValueOffer,lcValueBuy


	SELECT ucrsCampanhaOnline
	GO TOP
	lcIdCampaign =ucrsCampanhaOnline.id
	lcValueOffer = ROUND(ucrsCampanhaOnline.valorOferta,2)
	lcValueBuy   = ROUND(ucrsCampanhaOnline.valorCompra,2)
	lcDateInit   = ucrsCampanhaOnline.dataInicio
	lcDateEnd    = ucrsCampanhaOnline.dataFim    
	
	
	LOCAL lcFamilia
	lcFamilia= "0"
	if(USED("st"))
		SELECT st
		GO TOP
		lcFamilia = LTRIM(RTRIM(st.familia))		
	endif
	
	
	lcIdCampaign= ucrsCampanhaOnline.id
	
	if(lcIdCampaign<=0 or lcIdCampaign>=4)
		RETURN .t.
	ENDIF
	

						
	if(lcDateInit>lcDateEnd AND  lcDateEnd!=DATETIME(1900,01,01))
		uf_perguntalt_chama("A data de inicio n�o pode ser maior que a data de fim","OK","", 48)
		RETURN .f.
	ENDIF

	
	DO CASE 	
			CASE lcIdCampaign= 1 OR lcIdCampaign  = 2
				if(lcValueOffer <= 0)
					uf_perguntalt_chama("Por favor escolha um valor de oferta positivo","OK","", 48)
					RETURN .f.
				ENDIF
											
			CASE lcIdCampaign= 3				
				if(lcValueBuy  <= 0 and  lcValueOffer <= 0)
					uf_perguntalt_chama("Por favor escolha quantidades de oferta positivo","OK","", 48)
					RETURN .f.
				ENDIF	
				
				if(lcValueBuy >= lcValueOffer)
					uf_perguntalt_chama("O valor de oferta tem de ser maior que o de compra","OK","", 48)
					RETURN .f.
				ENDIF
				
				if(lcFamilia == "1")
					uf_perguntalt_chama("Esta campanha n�o pode ser aplicada a MSRM","OK","", 48)
					RETURN .f.
				ENDIF						
						
				if(lcDateInit>lcDateEnd)
					uf_perguntalt_chama("A data de inicio n�o pode ser maior que a data de fim","OK","", 48)
					RETURN .f.
				ENDIF
				
			OTHERWISE 

				RETURN .t.
			
		ENDCASE 


	

ENDFUNC



***********************************************
* 			CONTROLA GRAVA��O DOS DADOS		  *
***********************************************
FUNCTION uf_stocks_gravar

    SELECT ST
    REPLACE st.design with ALLTRIM(strtran(st.design,chr(39),''))

	
	LOCAL lcValidaST,lcValidaSTatrib,lcValidaBC
	STORE .F. TO lcValidaST,lcValidaSTatrib,lcValidaBC, lcValidaCampaign
	
	** Verificar se existe nova vers�o para reiniciar o software
	uf_chk_nova_versao()
	
	** Valida��es

	&&verifica se pode criar produto com esta referencia
	IF uf_stocks_temReferenciaBloqueada(myStocksIntroducao, st.ref)
		uf_perguntalt_chama("N�O � POSS�VEL CRIAR O PRODUTO COM ESTA REFER�NCIA","OK","",48)
		RETURN .f.
	ENDIF
	
	&&valida campanhas online
	if(USED("ucrsCampanhaOnline"))
		uf_stocks_validaCampanhasOnline()
		if(!uf_stocks_validacoesCampanhasOnline())		
			RETURN .f.
		ENDIF
		
	ENDIF
		
	

	&& REFERENCIA
	IF EMPTY(ALLTRIM(st.ref))
		uf_perguntalt_chama("O CAMPO: REFER�NCIA, � DE PREENCHIMENTO OBRIGAT�RIO!","OK","",48)
		stocks.TXTREF.SETFOCUS
		RETURN .f.	
	ENDIF	

	&& DESIGNA��O	
	IF EMPTY(ALLTRIM(st.DESIGN))
		uf_perguntalt_chama("O CAMPO: DESIGNA��O, � DE PREENCHIMENTO OBRIGAT�RIO!","OK","",48)
		stocks.TXTDESIGN.SETFOCUS
		RETURN .f.
	ENDIF
	
	IF LEFT(ALLTRIM(st.DESIGN),1) == "." &&n�o permitir que design comece por ponto (conflito com atendimento)
		uf_perguntalt_chama("O CAMPO: DESIGNA��O, N�O PODE COME�AR POR '.'!","OK","",48)
		stocks.TXTDESIGN.SETFOCUS
		RETURN .f.
	ENDIF



	&& FAMILIA E FAMINOME	
	IF EMPTY(ST.FAMILIA) OR EMPTY(ALLTRIM(st.FAMINOME))
		uf_perguntalt_chama("O CAMPO: FAM�LIA, � DE PREENCHIMENTO OBRIGAT�RIO!","OK","",48)
		stocks.PAGEFRAME1.PAGE2.CMBFAMILIA.SETFOCUS
		RETURN
	ENDIF
	
	&& TABIVA
	IF EMPTY(ST.tabiva)
		uf_perguntalt_chama("O CAMPO: IVA, � DE PREENCHIMENTO OBRIGAT�RIO!","OK","",48)		
		stocks.PAGEFRAME1.PAGE1.tabiva.SETFOCUS
		RETURN
	ENDIF
	
	
	&& PONTO DE ENCOMENDA & STOCK MAX
	IF(!empty(st.ptoenc))
		if(st.ptoenc>st.stmax)
			uf_perguntalt_chama("O ponto de encomenda n�o pode ser superior ao stockmax.","OK","",48)
			stocks.PAGEFRAME1.PAGE1.txtptoenc.SETFOCUS
			RETURN .t.
		ENDIF	
	ENDIF
	
	


	&& C�digo Repetido na ST
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		select 
			top 1 ref 
		from 
			st (nolock) 
		where 
			st.ref = '<<uf_gerais_CleanAlphaNum(st.ref)>>' 
			and st.site_nr = <<mySite_nr>>
			and st.ststamp != '<<ALLTRIM(st.ststamp)>>'
	ENDTEXT 

	IF !uf_gerais_actgrelha("", "uCrsTempRef", lcSQL)
		uf_perguntalt_chama("ATEN��O: J� EXISTE UM ARTIGO COM A MESMA REFER�NCIA.","OK","", 48)	
		RETURN .f.
	ENDIF
	
	IF RECCOUNT("uCrsTempRef")>0
		uf_perguntalt_chama("ATEN��O: J� EXISTE UM ARTIGO COM A MESMA REFER�NCIA.","OK","", 48)	
		FECHA("uCrstempREF")
		RETURN .f.
	ENDIF
	FECHA("uCrstempREF")
	
	&& C�digo repetido na BC
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		select 
			top 1 codigo 
		from 
			bc (nolock) 
		where 
			bc.codigo = '<<ALLTRIM(st.ref)>>' 
			and bc.site_nr = <<mySite_nr>>
	ENDTEXT 

	IF !uf_gerais_actgrelha("", "uCrsTempRefBc", lcSQL)
		uf_perguntalt_chama("ATEN��O: J� EXISTE UM ARTIGO COM C�DIGO ADICIONAL IGUAL A ESTA REFER�NCIA.","OK","", 48)	
		RETURN .f.
	ENDIF
	
	IF RECCOUNT("uCrsTempRefBc")>0
		uf_perguntalt_chama("ATEN��O: J� EXISTE UM ARTIGO COM C�DIGO ADICIONAL IGUAL A ESTA REFER�NCIA.","OK","", 48)	
		FECHA("uCrstempREF")
		RETURN .f.
	ENDIF
	FECHA("uCrstempREFBc")
	
	&& DESIGNA��O REPETIDA
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		select 
			top 1 ref 
		from 
			st (nolock) 
		where 
			st.design = '<<ALLTRIM(st.design)>>' 
			and st.site_nr = <<mySite_nr>>
			and st.ststamp != '<<ALLTRIM(st.ststamp)>>'
	ENDTEXT 
	IF uf_gerais_actgrelha("", "uCrsTempDesign", lcSQL)
		IF RECCOUNT("uCrsTempDesign")>0
			uf_perguntalt_chama("ATEN��O: J� EXISTE UM ARTIGO COM A MESMA DESIGNA��O! REF:"+ uCrstempDesign.ref,"OK","",48)
			FECHA("uCrstempDesign")
			RETURN .f.
		ENDIF
		FECHA("uCrstempDesign")
	ENDIF
	
	&& SE A REFERENCIA ESTIVER MARCADA COMO MARCA��O DEVE SER INCLUIDO A DURA��O
	IF ST.u_SERVMARC == .t. AND ST.u_duracao == 0
		uf_perguntalt_chama("DEVE PREENCHER O CAMPO DURA��O (SEPERADOR DEPEND�NCIAS), PARA SERVI�OS DE MARCA��ES!","OK","",48)
		stocks.lbl5.click
		RETURN .f.
	ENDIF
	
	&& N�o permitir colocar inactivo produtos com stock
	IF st.inactivo AND st.stock > 0 AND st.stns == .f.
		uf_perguntalt_chama("N�o � poss�vel inactivar produtos com stock.","OK","",48)
		RETURN .f.
	ENDIF
	
	&& Avisar que vai colocar como inativo produtos que t�m reservas Abertas
	IF st.inactivo AND st.qttcli > 0
		IF !(uf_perguntalt_chama("Vai colocar como inativo um produto com reservas em aberto." +CHR(13)+CHR(13)+ "Pretende continuar ?","Sim","N�o"))
			RETURN .f.
		ENDIF
	ENDIF
	
	&& PVP
	IF EMPTY(ST.EPV1)
		IF !(uf_perguntalt_chama("Tem a certeza que deseja gravar o Artigo sem P.V.P.?","Sim","N�o"))
			RETURN .f.
		ENDIF
	ENDIF
	
	&& Validar Datas de Promo��o
	IF st.dataIniPromo > st.dataFimPromo
		uf_perguntalt_chama("A data de in�cio de promo��o, n�o pode ser superior � dat de fim.", "OK","",48)
		RETURN .F.
	ENDIF 	
	
	&& Validade
	SELECT st
	IF EMPTY(ST.Validade)
		replace st.validade WITH CTOD('1900.01.01')
	ENDIF
	
	&&validar motivo de isne��o para iva 0
	IF !uf_stocks_validarMotivoIsencao()
		RETURN .F.
	ENDIF
		
	&& atualiza tipo/class produto definidica pela farm�cia  - LL 27052016
	IF EMPTY(ALLTRIM(stocks.Pageframe1.Page2.familia.value))
		SELECT st
		Replace st.u_famstamp WITH ""
		Replace st.famfamilia WITH ""
	ELSE
		LOCAL lcAux
		STORE '' TO lcAux
		lcAux = UPPER(ALLTRIM(stocks.Pageframe1.Page2.familia.value))
				
		lcSQL = ''
		TEXT TO lcSQL TEXTMERGE NOSHOW 
			SELECT famstamp FROM b_famFamilias (nolock) WHERE UPPER(RTRIM(LTRIM(design))) = '<<lcAux>>'
		ENDTEXT 
			
		IF !uf_gerais_actgrelha("", "uCrsAuxFamilia", lcSQL)
			uf_perguntalt_chama("Ocorreu uma anomalia ao verificar a classifica��o do produto.","OK","",16)
			RETURN .f.
		ENDIF	
		
		IF RECCOUNT("uCrsAuxFamilia") > 0
			Replace st.u_famstamp WITH uCrsAuxFamilia.famstamp
			Replace st.famfamilia WITH lcAux
		ELSE
			&& n�o � suposto acontecer. failsafe
			Replace st.u_famstamp WITH ""
			Replace st.famfamilia WITH ""
		ENDIF
		
		IF USED("uCrsAuxFamilia")
			fecha("uCrsAuxFamilia")
		ENDIF
	ENDIF
	
	
	LOCAL lcRefActual
	SELECT st
	lcRefActual= ALLTRIM(st.ref)
						
	** come�a transaction									
	IF uf_gerais_actGrelha("","",[BEGIN TRANSACTION])	
		&& Controla PCT a Zero
		uf_stocks_controlaPCTzero()
		
		IF (myStocksIntroducao)
			&& Preenche contas
			uf_stocks_preencheContas(.f.)

			&& Gravar em todas as Empresas
			IF STOCKS.menu1.todasEmpresas.tag == "true"
				
				SELECT ucrsLojas
				GO TOP 
				SCAN FOR !empty(ucrsLojas.no)
					lcValidaST = uf_stocks_InsereArtigo(ucrsLojas.no,ucrsLojas.site)
					
					lcValidaSTdependencias	= uf_dependencias_gravar(ucrsLojas.no,ucrsLojas.site)
				
					lcValidaCampaign   = uf_stocks_InsereCampanhasOnline(ucrsLojas.no,ucrsLojas.site)
				
					**uf_dicionario_guarda_imagem_anexos(lcRefActual,ucrsLojas.site)   

					uf_dicionario_guarda_imagem_anexos_novoArtigo(lcRefActual,ucrsLojas.site)   
				
				ENDSCAN 
			ELSE
				lcValidaST 				= uf_stocks_InsereArtigo(mysite_nr,mysite)
				lcValidaSTdependencias	= uf_dependencias_gravar(mysite_nr,mysite)
				lcValidaCampaign        = uf_stocks_InsereCampanhasOnline(mysite_nr,mysite)
				**uf_dicionario_guarda_imagem_anexos(mysite)  
				uf_dicionario_guarda_imagem_anexos_novoArtigo(lcRefActual,mySite)   
			ENDIF 
		
			      
			IF lcValidaST == .t. AND lcValidaSTdependencias == .t. AND lcValidaCampaign   ==.t.
				&& confirma a transa��o
				uf_gerais_actGrelha("","",[COMMIT TRANSACTION])

				** Actualiza ultimo registo alterado
				uf_gerais_gravaUltRegisto('st', st.ref)
				uf_stocks_gravaHistPvp()
						
				&& Chama os dados do artigo inserido
				uf_stocks_chama(st.ref)
			
				&& Campanhas
				uf_campanhas_actualizaCampanhasProduto(st.ref)
				
				
				&& Configura o ecr�
				uf_stocks_confEcra(1)
				uf_stocks_alternaMenu()
			ELSE
				uf_gerais_actGrelha("","",[ROLLBACK])
			ENDIF
			
		ELSE
				
			&& Gravar em todas as Empresas
			IF STOCKS.menu1.todasEmpresas.tag == "true"
				SELECT uCrsLojas
				GO TOP 
				SCAN FOR !empty(ucrsLojas.no)
					lcValidaST 				= uf_stocks_InsereArtigo(ucrsLojas.no,ucrsLojas.site)
					lcValidaSTdependencias	= uf_dependencias_gravar(ucrsLojas.no,ucrsLojas.site)
					lcValidaCampaign   	= uf_stocks_InsereCampanhasOnline(ucrsLojas.no,ucrsLojas.site)
					uf_dicionario_guarda_imagem_anexos(lcRefActual,ucrsLojas.site) 
				ENDSCAN 
			ENDIF 
			
				
			lcValidaST 				= uf_stocks_UpdateArtigo()				
			lcValidaBC				= uf_stocks_updateBC()
			lcValidaSTdependencias	= uf_dependencias_gravar()
			lcValidaCampaign   	    = uf_stocks_InsereCampanhasOnline()
			uf_dicionario_guarda_imagem_anexos(lcRefActual,mysite)
			
			
			IF lcValidaST == .t. AND lcValidaBC == .t. AND lcValidaSTdependencias == .t. AND lcValidaCampaign== .t.
				&& confirma a transa��o
				uf_gerais_actGrelha("","",[COMMIT TRANSACTION])
			
				** Actualiza ultimo registo alterado
				uf_gerais_gravaUltRegisto('st',st.ref)					
				uf_stocks_gravaHistPvp()

				&& Chama os dados do artigo inserido	
				uf_stocks_chama(st.ref)

				&& Campanhas
				uf_campanhas_actualizaCampanhasProduto(st.ref)
				
				&& Configura o ecr�
				uf_stocks_confEcra(1)
				uf_stocks_alternaMenu()
				
				*stocks.refresh	
			ELSE
				uf_gerais_actGrelha("","",[ROLLBACK])
			endif
		ENDIF

	ELSE
		uf_gerais_actGrelha("","",[ROLLBACK])
	ENDIF

	IF STOCKS.windowType = 1 AND WEXIST("ENCAUTOMATICAS")
		STOCKS.hide()
		FECHA("ST")
		STOCKS.release()
	ENDIF
	
ENDFUNC

*************************************************
*				INSER��O DE CAMPANHAS ONLINE   *
*************************************************
FUNCTION uf_stocks_InsereCampanhasOnline
	LPARAMETERS lcsite_nr, lcSite
	
	LOCAL lcIdCampaign, lcValueOffer, lcValueBuy, lcSQL 
	STORE 0 TO lcIdCampaign,lcValueOffer,lcValueBuy

	
	lcSQL =''
	IF EMPTY(lcsite_nr)
		lcsite_nr  = mysite_nr
	ENDIF 
	IF EMPTY(lcSite)
		lcSite= mysite
	ENDIF 
	
	if(!USED("ucrsCampanhaOnline") OR !USED("st"))
		RETURN .t.
	ENDIF
	
	
	if(RECCOUNT("ucrsCampanhaOnline")==0)
		RETURN .t.
	ENDIF
	
	SELECT ucrsCampanhaOnline
	GO TOP
	lcIdCampaign =ucrsCampanhaOnline.id
	lcValueOffer = ROUND(ucrsCampanhaOnline.valorOferta,2)
	lcValueBuy   = ROUND(ucrsCampanhaOnline.valorCompra,2)
	lcDataInit   = ucrsCampanhaOnline.dataInicio
	lcDataEnd    = ucrsCampanhaOnline.dataFim
	
	if(EMPTY(lcDataInit))
		lcDataInit = "1900.01.01"
	ENDIF
	
	if(EMPTY(lcDataEnd ))
		lcDataEnd = "1900.01.01"
	ENDIF

	
	SELECT ST
	GO TOP	
	if(lcIdCampaign <=0 and lcIdCampaign >=4)
		TEXT TO lcSQL TEXTMERGE NOSHOW 
			delete from campanhas_online_st where ref = '<<ALLTRIM(st.ref)>>' and site_nr = <<lcsite_nr>>
		ENDTEXT
	
	ELSE
		TEXT TO lcSQL TEXTMERGE NOSHOW 
			delete from campanhas_online_st where ref = '<<ALLTRIM(st.ref)>>' and site_nr = <<lcsite_nr>>
		
			INSERT INTO [campanhas_online_st]
	           ([stamp]
	           ,[idTipoCampanha]
	           ,[valorOferta]
	           ,[valorCompra]
	           ,[ref]
	           ,[site_nr]
	           ,[activo]
	           ,[ousrinis]
	           ,[ousrdata]
	           ,[ousrhora]
	           ,[usrinis]
	           ,[usrdata]
	           ,[usrhora]
	           ,[dataInicio]
	           ,[dataFim]
	           
	           )
	         VALUES
	           (LEFT(NEWID(),34)
	           ,<<lcIdCampaign>>
	           ,<<lcValueOffer>>
	           ,<<lcValueBuy>>
	           ,'<<ALLTRIM(st.ref)>>'
	           ,<<lcsite_nr>>
	           ,0
	           ,'<<m_chinis>>'
	           ,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
	           ,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108)
	           ,'<<m_chinis>>'
	           ,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
	           ,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108)
	           ,'<<uf_gerais_getDate(lcDataInit,"SQL")>>'
	           ,'<<uf_gerais_getDate(lcDataEnd ,"SQL")>>'
	           )
			ENDTEXT
	
	ENDIF
	

	
	IF uf_gerais_actgrelha("","",lcSql)
	
		RETURN .t.
	ELSE
		RETURN .f.
	ENDIF
ENDFUNC



*************************************************
*				INSER��O DE ARTIGO				*
*************************************************
FUNCTION uf_stocks_InsereArtigo
	LPARAMETERS lcsite_nr, lcSite
	
	IF EMPTY(lcsite_nr)
		lcsite_nr  = mysite_nr
	ENDIF 
	IF EMPTY(lcSite)
		lcSite= mysite
	ENDIF 

	IF st.iva == 0
		IF EMPTY(st.codmotiseimp) OR   EMPTY(st.motiseimp)
			uf_stocks_codigoMotivoInsencao(lcsite_nr, lcSite)
		ENDIF
	ENDIF
		
	SELECT ST
	lcSQL=''
	TEXT TO lcSQL TEXTMERGE NOSHOW 
		if not exists (select ref, site_nr from st(nolock) where ref = '<<ALLTRIM(st.ref)>>' and site_nr = <<lcsite_nr>>)
		Begin
			INSERT INTO st
			(
				ststamp,ref,design,u_fonte,bloqueado,fobloq,qlook,stns,inactivo,
				epv1,tabiva,familia,faminome,u_lab,u_impetiq,u_tipoetiq,unidade,uni2,conversao,qttcat,stmin,stmax,ptoenc,eoq,u_aprov,local,u_local,u_local2,validade,
				usr1,qttesp,volume,peso,pbruto,url,imagem,u_nota1,obs,		
				fornecedor,fornec,fornestab,forref,pentrega,despimp,mfornec,mfornec2,
				marg1,pcusto,marg2,epcpond,epcult,marg3,marg4,
				baixr,nsujpp,compnovo,clinica,sujinv,ecomissao,cpoc,containv,contacoe,contacev,contareo,contaieo,
				epcusto,IVAINCL,ivapcincl,iva1incl,iva2incl,iva3incl,iva4incl,iva5incl,
				ousrdata,ousrhora,ousrinis,usrdata,usrhora,usrinis,
				u_servmarc, u_duracao,
				u_depstamp, u_secstamp, u_catstamp, u_famstamp, u_segstamp,
				marcada, usalote, rateamento, mrsimultaneo,
				site_nr, refentidade, designentidade, categoria,
				profundidade,altura,
				designOnline, caractOnline, apresentOnline,
				novidadeOnline, tempoIndiponibilidade,dispOnline, portesGratis, codmotiseimp, motiseimp, precoSobConsulta, pim, epv1_final, qttembenc,
				dataIniPromo, dataFimPromo
			)
			values
			(
				LEFT(NEWID(),25),
				'<<ALLTRIM(STRTRAN(st.ref,chr(39),''))>>',
				'<<ALLTRIM(strtran(st.design,chr(39),''))>>',
				'<<ALLTRIM(st.u_fonte)>>',
				<<IIF(st.bloqueado,1,0)>>,
				<<IIF(st.fobloq,1,0)>>,
				<<IIF(st.qlook,1,0)>>,
				<<IIF(st.stns,1,0)>>,
				<<IIF(st.inactivo,1,0)>>,
				<<st.epv1>>,
				<<st.tabiva>>,
				'<<ALLTRIM(st.familia)>>',
				'<<ALLTRIM(st.faminome)>>',
				'<<ALLTRIM(u_lab)>>',
				<<IIF(u_impetiq,1,0)>>,
				<<st.u_tipoetiq>>,
				'<<ALLTRIM(st.unidade)>>',
				'<<ALLTRIM(st.uni2)>>',
				<<st.conversao>>,
				<<st.qttcat>>,
				<<st.stmin>>,
				<<st.stmax>>,
				<<st.ptoenc>>,
				<<st.eoq>>,
				'<<st.u_aprov>>',
				'<<ALLTRIM(st.local)>>',
				'<<ALLTRIM(st.u_local)>>',
				'<<ALLTRIM(st.u_local2)>>',
				'<<uf_gerais_getDate(st.validade,"SQL")>>',
				'<<ALLTRIM(st.usr1)>>',
				<<st.qttesp>>,
				<<st.volume>>,
				<<st.peso>>,
				<<st.pbruto>>,
				'<<ALLTRIM(st.url)>>',
				'<<ALLTRIM(st.imagem)>>',
				'<<ALLTRIM(st.u_nota1)>>',
				'<<ALLTRIM(st.obs)>>',
				'<<ALLTRIM(st.fornecedor)>>',
				<<st.fornec)>>,
				<<st.fornestab)>>,
				'<<ALLTRIM(st.forref)>>',
				<<st.pentrega>>,
				<<st.despimp>>,
				<<st.mfornec>>,
				<<st.mfornec2>>,
				<<st.marg1>>,
				<<st.pcusto>>,
				<<st.marg2>>,
				<<st.epcpond>>,
				<<st.epcult>>,
				<<st.marg3>>,
				<<st.marg4>>,
				
				<<IIF(st.baixr,1,0)>>,
				<<IIF(st.nsujpp,1,0)>>,
				<<IIF(st.compnovo,1,0)>>,
				<<IIF(st.clinica,1,0)>>,
				<<IIF(st.sujinv,1,0)>>,
				<<st.ecomissao>>,
				<<st.cpoc>>,
				'<<ALLTRIM(st.containv)>>',
				'<<ALLTRIM(st.contacoe)>>',
				'<<ALLTRIM(st.contacev)>>',
				'<<ALLTRIM(st.contareo)>>',
				'<<ALLTRIM(st.contaieo)>>',
					
				<<ST.EPCUSTO>>,
				<<IIF(ST.IVAINCL,1,0)>>,
				<<IIF(ST.ivapcincl,1,0)>>,
				<<IIF(ST.iva1incl,1,0)>>,
				<<IIF(ST.iva2incl,1,0)>>,
				<<IIF(ST.iva3incl,1,0)>>,
				<<IIF(ST.iva4incl,1,0)>>,
				<<IIF(ST.iva5incl,1,0)>>,
							
				convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102),
				convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108),
				'<<m_chinis>>',
				convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102),
				convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108),
				'<<m_chinis>>',
				<<IIF(ST.u_servmarc,1,0)>>,
				<<ST.u_duracao>>,
				'<<u_depstamp>>',
				'<<u_secstamp>>',
				'<<u_catstamp>>',
				'<<u_famstamp>>',	
				'<<u_segstamp>>',	
				<<IIF(st.marcada,1,0)>>,
				<<IIF(st.usalote,1,0)>>,
				<<IIF(st.rateamento,1,0)>>,
				<<st.mrsimultaneo>>,
				<<lcsite_nr>>,
				'<<ALLTRIM(st.refentidade)>>',
                '<<ALLTRIM(st.designentidade)>>',
                '<<ALLTRIM(st.categoria)>>',
				<<st.profundidade)>>,
				<<st.altura)>>,
				'<<ALLTRIM(st.designOnline)>>',
				'<<ALLTRIM(st.caractOnline)>>',
				'<<ALLTRIM(st.apresentOnline)>>',
				<<IIF(ST.novidadeOnline,1,0)>>,
				<<st.tempoIndiponibilidade)>>,
				<<IIF(st.dispOnline,1,0)>>,
				<<IIF(st.portesGratis,1,0)>>,
				'<<ALLTRIM(st.codmotiseimp)>>',
				'<<ALLTRIM(st.motiseimp)>>',
				<<IIF(st.precoSobConsulta, 1, 0)>>,
            	<<IIF(st.pim,1,0)>>,
				<<st.epv1_final>>
				,<<st.qttembenc>>
				,'<<st.dataIniPromo>>'
				,'<<st.dataFimPromo>>'
			)
		End
	ENDTEXT
	
	


	IF uf_gerais_actgrelha("","",lcSql)
		LOCAL lcStamp
		
		** Guardar Registo da Altera��o de PCT e Margem**
		IF stocks.alterapct != st.epcusto
			lcStamp = uf_gerais_stamp()
			TEXT TO lcSql Noshow Textmerge
				INSERT into B_ocorrencias
					(stamp, linkstamp, tipo, grau, descr,
					ovalor, dvalor,
					usr, date, site, terminal)
				values
					('<<alltrim(lcStamp)>>', '<<alltrim(st.ststamp)>>', 'Stocks', 2, 'Altera��o de PCT na cria��o de ficha do produto:<<ALLTRIM(st.ref)>>',
					'<<round(stocks.alterapct,2)>>', '<<round(st.epcusto,2)>>',
					<<ch_userno>>, dateadd(HOUR, <<difhoraria>>, getdate()), '<<lcSite>>', '<<myTermNo>>')
			ENDTEXT
			uf_gerais_actGrelha("","",lcSql)

		ENDIF
		
		IF stocks.alteramargem != st.marg1
			lcStamp = uf_gerais_stamp()
			TEXT TO lcSql Noshow Textmerge
				INSERT into B_ocorrencias
					(stamp, linkstamp, tipo, grau, descr,
					ovalor, dvalor,
					usr, date, site, terminal)
				values
					('<<alltrim(lcStamp)>>', '<<alltrim(st.ststamp)>>', 'Stocks', 2, 'Altera��o de Margem na cria��o de ficha de produto:<<ALLTRIM(st.ref)>>',
					'<<round(stocks.alteramargem,2)>>', '<<round(st.marg1,2)>>',
					<<ch_userno>>, dateadd(HOUR, <<difhoraria>>, getdate()), '<<lcSite>>', '<<myTermNo>>')
			ENDTEXT
			uf_gerais_actGrelha("","",lcSql)

		ENDIF
		**
		
		RETURN .t.
	ELSE
		RETURN .f.
	ENDIF
ENDFUNC


*************************************************
*			UPDATE DOS DADOS DO ARTIGO			*
*************************************************
FUNCTION uf_stocks_updateArtigo
	
	SELECT ST
		lojalocal=0
		IF !USED("ucrsEmpresasSel")
			lojalocal=1
		ELSE
			SELECT ucrsEmpresasSel
			GO top
			SCAN
				IF ucrsEmpresasSel.siteno=mysite_nr THEN 
					lojalocal=1
				ENDIF 
			ENDSCAN 
		ENDIF
		
		
		** REGISTO DE ALTERA��ES NA TABELA DE LOGS DE UTILIZADOR
		local lcdados
		lcdados= 'Loja ' + ALLTRIM(STR(mySite_nr)) + '' + CHR(13)
		select st
		select * FROM st INTO CURSOR u_st READWRITE 
		select u_st
		
		TEXT TO lcSQL TEXTMERGE NOSHOW
			exec up_stocks_dadosRef '<<ALLTRIM(st.Ref)>>', <<mySite_nr>>
		ENDTEXT
		IF !uf_gerais_actgrelha("", "st_aux", lcSql)
			regua(2)
			RETURN .f.
		ENDIF
		
		SELECT u_st
		scan all
		SCATTER MEMO TO la_st
		IF RECCOUNT("st_aux")>0
			SELECT st_aux
			SCATTER MEMO TO la_st_aux
				FOR i = 1 TO ALEN(la_st)
				if vartype(la_st[i]) == vartype(la_st_aux[i])
					IF la_st[i] <> la_st_aux[i]
						if alltrim(vartype(la_st[i]))=='C'
							lcdados=lcdados + 'Campo: '+ alltrim(field(i)) + ' - Valor original: '  + alltrim(la_st_aux[i]) + ' - Valor alterado: ' + alltrim(la_st[i]) + CHR(13)
						endif
						if alltrim(vartype(la_st[i]))=='N'
							lcdados=lcdados + 'Campo: '+ alltrim(field(i)) + ' - Valor original: '  + ALLTRIM(str(la_st_aux[i])) + ' - Valor alterado: ' + ALLTRIM(str(la_st[i])) + CHR(13)
						endif
						if alltrim(vartype(la_st[i]))=='L'
							lcdados=lcdados + 'Campo: '+ alltrim(field(i)) + ' - Valor original: '  + alltrim(iif(la_st_aux[i]=.t.,'Verdadeiro','Falso')) + ' - Valor alterado: ' + alltrim(iif(la_st[i]=.t.,'Verdadeiro','Falso')) + CHR(13)
						endif
					endif 
				endif 
			NEXT
			SKIP IN st_aux
		ENDIF 
		SELECT u_st
		ENDSCAN
		**messagebox(lcdados)
		
		lcSQL=''
		lcSQL1=''
		
		Lcimpetiq=IIF(stocks.chkU_impetiq.tag == "true", "1", "0")
		
		IF !USED("ucrsEmpresasSel") OR RECCOUNT("ucrsEmpresasSel")=0 OR lojalocal=1 then
			&&Obter Stamp QUestionario
			LOCAL lcQuestStampTmp
			lcQuestStampTmp= uf_gerais_getUmValor("quest", "queststamp", "descr = '" +ALLTRIM(STOCKS.Pageframe1.page2.txtquest.value) + "'")
			lcSelLojas = ALLTRIM(mysite)
			TEXT TO lcSQL TEXTMERGE noshow
				update st 
				SET 
					ref			=	'<<ALLTRIM(st.ref)>>',
					design		=	'<<ALLTRIM(strtran(st.design,chr(39),''))>>',
					u_fonte		=	'<<ALLTRIM(st.u_fonte)>>',
					bloqueado	=	<<IIF(st.bloqueado,1,0)>>,
					fobloq		=	<<IIF(st.fobloq,1,0)>>,
					qlook		=	<<IIF(st.qlook,1,0)>>,
					stns		=	<<IIF(st.stns,1,0)>>,
					inactivo	=	<<IIF(st.inactivo,1,0)>>,
					
					epv1		=	<<st.epv1>>,
					tabiva		=	<<st.tabiva>>,
					familia		=	'<<ALLTRIM(st.familia)>>',
					faminome	=	'<<ALLTRIM(st.faminome)>>',
					u_lab		=	'<<ALLTRIM(st.u_lab)>>',
					U_impetiq		=<<Lcimpetiq>>,
					u_tipoetiq	=	<<st.u_tipoetiq>>,
					unidade		=	'<<ALLTRIM(st.unidade)>>',
					uni2		=	'<<ALLTRIM(st.uni2)>>',
					conversao	=	<<st.conversao>>,
					qttcat		=	<<st.qttcat>>,
					stmin		=	<<st.stmin>>,
					stmax		=	<<st.stmax>>,
					ptoenc		=	<<st.ptoenc>>,
					eoq			=	<<st.eoq>>,
					u_aprov		=	'<<st.u_aprov>>',
					local		=	'<<ALLTRIM(st.local)>>',
					u_local		=	'<<ALLTRIM(st.u_local)>>',
					u_local2	=	'<<ALLTRIM(st.u_local2)>>',
					validade	=	'<<uf_gerais_getDate(st.validade,"SQL")>>',
						
					usr1		=	'<<ALLTRIM(st.usr1)>>',
					qttesp		=	<<st.qttesp>>,
					volume		=	<<st.volume>>,
					peso		=	<<st.peso>>,
					pbruto		=	<<st.pbruto>>,
					url			=	'<<ALLTRIM(st.url)>>',
					imagem		=	'<<ALLTRIM(st.imagem)>>',
					u_nota1		=	'<<ALLTRIM(st.u_nota1)>>',
					obs			=	'<<ALLTRIM(st.obs)>>',
					
					fornecedor	=	'<<ALLTRIM(st.fornecedor)>>',
					fornec		=	<<st.fornec)>>,
					fornestab	=	<<st.fornestab)>>,
					forref		=	'<<ALLTRIM(st.forref)>>',
					pentrega	=	<<st.pentrega>>,
					despimp		=	<<st.despimp>>,
					mfornec		=	<<st.mfornec>>,
					mfornec2	=	<<st.mfornec2>>,
					
					marg1		=	<<st.marg1>>,
					pcusto		=	<<st.pcusto>>,
					marg2		=	<<st.marg2>>,
					epcpond		=	<<st.epcpond>>,
					epcult		=	<<st.epcult>>,
					marg3		=	<<st.marg3>>,
					marg4		=	<<st.marg4>>,
					
					baixr		=	<<IIF(st.baixr,1,0)>>,
					nsujpp		=	<<IIF(st.nsujpp,1,0)>>,
					compnovo	=	<<IIF(st.compnovo,1,0)>>,
					clinica		=	<<IIF(st.clinica,1,0)>>,
					sujinv		=	<<IIF(st.sujinv,1,0)>>,
					ecomissao	=	<<st.ecomissao>>,
					cpoc		=	<<st.cpoc>>,
					containv	=	'<<ALLTRIM(st.containv)>>',
					contacoe	=	'<<ALLTRIM(st.contacoe)>>',
					contacev	=	'<<ALLTRIM(st.contacev)>>',
					contareo	=	'<<ALLTRIM(st.contareo)>>',
					contaieo	=	'<<ALLTRIM(st.contaieo)>>',
					
					EPCUSTO		=	<<ST.EPCUSTO>>,
					IVAINCL		=	<<IIF(ST.IVAINCL,1,0)>>,
					ivapcincl	=	<<IIF(ST.ivapcincl,1,0)>>,
					iva1incl	=	<<IIF(ST.iva1incl,1,0)>>,
					iva2incl	=	<<IIF(ST.iva2incl,1,0)>>,
					iva3incl	=	<<IIF(ST.iva3incl,1,0)>>,
					iva4incl	=	<<IIF(ST.iva4incl,1,0)>>,
					iva5incl	=	<<IIF(ST.iva5incl,1,0)>>,
									
					usrdata		=	convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102),
					usrhora		=	convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108),
					usrinis		=	'<<m_chinis>>',
					
					u_servmarc	= 	<<IIF(ST.u_servmarc,1,0)>>,
					u_duracao	=	<<ST.u_duracao>>,
					u_depstamp	= '<<ST.u_depstamp>>', 
					u_secstamp	= '<<ST.u_secstamp>>',
					u_catstamp	= '<<ST.u_catstamp>>', 
					u_famstamp	= '<<ST.u_famstamp>>', 
										
					u_segstamp	= '<<ST.u_segstamp>>',	  				
					
					marcada		= <<IIF(st.marcada,1,0)>>,
               pim   		= <<IIF(st.pim,1,0)>>,
					usalote 	= <<IIF(st.usalote,1,0)>>,
					rateamento 	= <<IIF(st.rateamento,1,0)>>,
					mrsimultaneo = <<st.mrsimultaneo>>,
					site_nr = <<mySite_nr>>,
					exportado = 0,
					refentidade = '<<ALLTRIM(st.refentidade)>>',
					designentidade = '<<ALLTRIM(st.designentidade)>>',
					categoria = '<<ALLTRIM(st.categoria)>>',
					profundidade = <<st.profundidade)>>,
		            altura = <<st.altura)>>,
		            designOnline = '<<ALLTRIM(st.designOnline)>>',
		            caractOnline = '<<ALLTRIM(st.caractOnline)>>',
		            apresentOnline = '<<ALLTRIM(st.apresentOnline)>>',
		            novidadeOnline = <<IIF(ST.novidadeOnline,1,0)>>,
		            tempoIndiponibilidade =<<st.tempoIndiponibilidade)>>,
		            dispOnline = <<IIF(ST.dispOnline,1,0)>>,
		            portesGratis= <<IIF(ST.portesGratis,1,0)>>,
		            codmotiseimp='<<ALLTRIM(st.codmotiseimp)>>',
		            motiseimp='<<ALLTRIM(st.motiseimp)>>',
					precoSobConsulta = <<IIF(st.precoSobConsulta, 1, 0)>>,
					qttminvd = <<st.qttminvd>>,
					qttembal = <<st.qttembal>>,
					epv1_final = <<st.epv1_final>>,
					qttembenc = <<st.qttembenc>>,
					dataIniPromo = '<<st.dataIniPromo>>',
					dataFimPromo = '<<st.dataFimPromo>>',
					questionario = '<<lcQuestStampTmp>>' 
				WHERE 
					ststamp =	'<<ALLTRIM(st.ststamp)>>'
					and st.site_nr = <<mysite_nr>>
			ENDTEXT			
		ENDIF
		
		IF USED("ucrsEmpresasSel") AND RECCOUNT("ucrsEmpresasSel") <> 0
            lcCount=1
            SELECT ucrsEmpresasSel
            GO top
            SCAN
                IF lcCount=1 then
                    lcSelSites = ALLTRIM(STR(ucrsEmpresasSel.siteno))
                    lcSelLojas = ALLTRIM(ucrsEmpresasSel.local)
                ELSE
                    lcSelSites = lcSelSites + ', ' + ALLTRIM(STR(ucrsEmpresasSel.siteno))
                    lcSelLojas = lcSelLojas + ',' + ALLTRIM(ucrsEmpresasSel.local)
                ENDIF 
                lcCount=lcCount+1
            ENDSCAN 
            
            TEXT TO lcSQL1 TEXTMERGE noshow
                update st 
                SET 
                    tabiva		=	<<st.tabiva>>,
                    familia		=	'<<ALLTRIM(st.familia)>>',
                    faminome	=	'<<ALLTRIM(st.faminome)>>',
                    u_tipoetiq	=	<<st.u_tipoetiq>>,
                    unidade		=	'<<ALLTRIM(st.unidade)>>',
                    uni2		=	'<<ALLTRIM(st.uni2)>>',
                    conversao	=	<<st.conversao>>,
                    volume		=	<<st.volume>>,
                    peso		=	<<st.peso>>,
                    stns		=	<<IIF(st.stns,1,0)>>,
                    qlook		=	<<IIF(st.qlook,1,0)>>,
                    inactivo	=	<<IIF(st.inactivo,1,0)>>,
                    U_impetiq		=	<<Lcimpetiq>>,
                    marcada		= <<IIF(st.marcada,1,0)>>,
                    pim    		= <<IIF(st.pim,1,0)>>,
                    qttesp		=	<<st.qttesp>>,
                    usr1		=	'<<ALLTRIM(st.usr1)>>',					
                    u_lab		=	'<<ALLTRIM(st.u_lab)>>',
                    pbruto		=	<<st.pbruto>>,
                    categoria	=	'<<ALLTRIM(st.categoria)>>',
                    url			=	'<<ALLTRIM(st.url)>>',
                    imagem		=	'<<ALLTRIM(st.imagem)>>',
                    usrdata		=	convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102),
                    usrhora		=	convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108),
                    usrinis		=	'<<m_chinis>>',
                    u_depstamp	= '<<ST.u_depstamp>>', 
                    u_secstamp	= '<<ST.u_secstamp>>',
                    u_catstamp	= '<<ST.u_catstamp>>', 
                    u_famstamp	= '<<ST.u_famstamp>>',
                    
                    u_segstamp	= '<<ST.u_segstamp>>',
                    
                    profundidade = <<st.profundidade)>>,
                    altura      = <<st.altura)>>,
                    designOnline = '<<ALLTRIM(st.designOnline)>>',
                    caractOnline = '<<ALLTRIM(st.caractOnline)>>',
                    apresentOnline = '<<ALLTRIM(st.apresentOnline)>>',
                    novidadeOnline = <<IIF(ST.novidadeOnline,1,0)>>,
                    tempoIndiponibilidade =<<st.tempoIndiponibilidade)>>,
                    dispOnline = <<IIF(ST.dispOnline,1,0)>>,
                    portesGratis= <<IIF(ST.portesGratis,1,0)>>,
                    codmotiseimp='<<ALLTRIM(st.codmotiseimp)>>',
                    motiseimp='<<ALLTRIM(st.motiseimp)>>',
                    precoSobConsulta = <<IIF(st.precoSobConsulta, 1, 0)>>,
                    qttminvd = <<st.qttminvd>>,
                    qttembal = <<st.qttembal>>,
					epv1_final = <<st.epv1_final>>
                WHERE 
                    ref =	'<<ALLTRIM(st.ref)>>'
                    and site_nr in (<<lcSelSites>>)
            ENDTEXT

		ELSE
			lcSelSites = ''		
		ENDIF
		
		IF USED("ucrsEmpresasSel") then
			**lcMsgaviso="ATEN��O: Vai refletir TODOS os campos da ficha do produto da loja atual, exceto design.,"
			**lcMsgaviso1=" pre�os/margens, campos gest�o stk., validade, localiza��o e obs., nas lojas sel. "
			lcMsgaviso="Confirma a alter. de TODOS os campos da ficha do prod. da loja atual, exceto design.,"
			lcMsgaviso1=" pre�os/margens, campos gest�o stk., valid., local. e obs., nas lojas sel. "
		ELSE
			lcMsgaviso="ATEN��O: Vai alterar a ficha do produto em quest�o. "
			lcMsgaviso1=""
		ENDIF 
		
		IF !EMPTY(lcSelSites)
			lcSelSites = "(" + lcSelSites + ")"
		ENDIF 
		
		If !uf_perguntalt_chama(lcMsgaviso+lcMsgaviso1 + CHR(13) + lcSelSites + CHR(13) + "PRETENDE CONTINUAR?"+CHR(13)+" ","Sim","N�o")
			RETURN .f.
		ELSE
			IF USED("ucrsEmpresasSel")
				fecha("ucrsEmpresasSel")
			ENDIF 
			IF USED("ucrsConfEmpresas")
				fecha("ucrsConfEmpresas")
			ENDIF 
	
			IF USED("uCrsPesqMulti")
				fecha("uCrsPesqMulti")
			ENDIF 
	
			IF USED("ucrsConfEmpresasAux")
				fecha("ucrsConfEmpresasAux")
			ENDIF 
			
			uf_gerais_actGrelha("","",lcSQL1)
			
			IF uf_gerais_actGrelha("","",lcSQL)
				
				LOCAL lcStamp
				
				** Guardar Registo da Altera��o de PCT e Margem**
				IF stocks.alterapct != st.epcusto
					lcStamp = uf_gerais_stamp()
					TEXT TO lcSql Noshow Textmerge
						INSERT into B_ocorrencias
							(stamp, linkstamp, tipo, grau, descr,
							ovalor, dvalor,
							usr, date, site, terminal)
						values
							('<<alltrim(lcStamp)>>', '<<alltrim(st.ststamp)>>', 'Stocks', 2, 'Altera��o de PCT na edi��o de ficha do produto: <<ALLTRIM(st.ref)>>',
							'<<round(stocks.alterapct,2)>>', '<<round(st.epcusto,2)>>',
							<<ch_userno>>, dateadd(HOUR, <<difhoraria>>, getdate()), '<<mySite>>', '<<myTermNo>>')
					ENDTEXT
					uf_gerais_actGrelha("","",lcSql)
				ENDIF
				
				IF stocks.alteramargem != st.marg1
					lcStamp = uf_gerais_stamp()
					TEXT TO lcSql Noshow Textmerge
						INSERT into B_ocorrencias
							(stamp, linkstamp, tipo, grau, descr,
							ovalor, dvalor,
							usr, date, site, terminal)
						values
							('<<alltrim(lcStamp)>>', '<<alltrim(st.ststamp)>>', 'Stocks', 2, 'Altera��o de Margem na edi��o de ficha de produto: <<ALLTRIM(st.ref)>>',
							'<<round(stocks.alteramargem,2)>>', '<<round(st.marg1,2)>>',
							<<ch_userno>>, dateadd(HOUR, <<difhoraria>>, getdate()), '<<mySite>>', '<<myTermNo>>')
					ENDTEXT
					uf_gerais_actGrelha("","",lcSql)
				ENDIF
				
				** REGISTO CONSULTA NOS LOGS DE UTILIZADOR			
				uf_user_log_ins('ST', ALLTRIM(st.ststamp), 'ALTERA��O', lcdados)
		
				RETURN .t.
			ELSE
				uf_perguntalt_chama("OCORREU UM ERRO AO ACTUALIZAR OS DADOS DO ARTIGO!","OK","",16)
				RETURN .f.
			ENDIF

		ENDIF 
ENDFUNC


**
FUNCTION uf_stocks_updateBC
	SELECT st
	lcSQL=''
	TEXT TO lcSQL TEXTMERGE NOSHOW 
		UPDATE
			BC
		SET 
			REF 	= '<<st.ref>>',
			design  = '<<ALLTRIM(strtran(st.design,chr(39),''))>>'
		where 
			bc.ststamp = '<<st.ststamp>>'
			and bc.site_nr = <<mysite_nr>>
	ENDTEXT 

	IF uf_gerais_actGrelha("","",lcSQL)
		RETURN .t.
	ELSE
		uf_perguntalt_chama("OCORREU UM ERRO AO ACTUALIZAR OS C�DIGOS ADICIONAIS! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	endif
Endfunc


*****************************
* 		VALIDAR REF 		*
*****************************
FUNCTION uf_stocks_validaRefExisteTabMov
	LPARAMETERS lcTipo
	
	IF EMPTY(lcTipo)
		uf_perguntalt_chama("PARAMETRO TIPO N�O PODE SER VAZIO.","OK","", 16)
	ENDIF
	
	SELECT st
	
	IF lcTipo == "NOVO"
	
		TEXT TO lcSQL NOSHOW TEXTMERGE
			select 
				top 1 ststamp 
			from 
				st (nolock) 
			where 
				st.ref='<<ALLTRIM(st.ref)>>'
				and st.site_nr = <<mySite_nr>>
		ENDTEXT 
	
		IF uf_gerais_actgrelha("", "uCrstempST", lcSQL)
			IF RECCOUNT("uCrsTempST")>0
				uf_perguntalt_chama("ESSA REFER�NCIA J� EXISTE.","OK","", 48)
				RETURN .f.
			ENDIF
			FECHA("uCrstempST")
		ELSE
			RETURN .f.
		ENDIF
	ELSE
		&& Valida movimentos
		SELECT st
		IF uf_gerais_actgrelha("", "uCrstempSL", [select top 1 slstamp from sl (nolock) where sl.ref='] + st.ref + ['])
			IF RECCOUNT("uCrsTempSL")>0
				DO CASE
					CASE lcTipo == 'ELIMINAR'
						uf_perguntalt_chama("N�O PODE APAGAR REFER�NCIAS COM MOVIMENTOS.","OK","", 48)
					CASE lcTipo == 'EDITAR'
						uf_perguntalt_chama("N�O PODE ALTERAR REFER�NCIAS COM MOVIMENTOS.","OK","", 48)
				ENDCASE
				
				RETURN .f.
			ENDIF
			FECHA("uCrstempSL")
		ELSE
			RETURN .f.
		ENDIF
		
		&& Valida BI
		SELECT st
		IF uf_gerais_actgrelha("", "uCrstempBI", [select top 1 bistamp from bi (nolock) where bi.ref='] + st.ref + ['])
			IF RECCOUNT("uCrsTempBI")>0
				DO CASE
					CASE lcTipo=='ELIMINAR'
						uf_perguntalt_chama("N�O PODE APAGAR REFER�NCIAS COM MOVIMENTOS.","OK","",48)	
					CASE lcTipo=='EDITAR'
						uf_perguntalt_chama("N�O PODE ALTERAR REFER�NCIAS COM MOVIMENTOS.","OK","",48)	
				ENDCASE	
				
				RETURN .f.	
			ENDIF
			FECHA("uCrstempBI")
		ELSE
			RETURN .f.
		ENDIF
		
		&& Valida FI
		SELECT st
		IF uf_gerais_actgrelha("", "uCrstempFi", [select top 1 fistamp from fi (nolock) where fi.ref='] + st.ref + ['])
			IF RECCOUNT("uCrstempFi")>0
				DO CASE
					CASE lcTipo=='ELIMINAR'
						uf_perguntalt_chama("N�O PODE APAGAR REFER�NCIAS COM MOVIMENTOS.","OK","",48)
					CASE lcTipo=='EDITAR'
						uf_perguntalt_chama("N�O PODE ALTERAR REFER�NCIAS COM MOVIMENTOS.","OK","",48)
				ENDCASE	
				RETURN .f.
			ENDIF
			FECHA("uCrstempFi")
		ELSE
			RETURN .f.
		ENDIF
	ENDIF
	
	&& passou valida��es	
	RETURN .t.
ENDFUNC


*****************************
* 		ELIMINAR Artigo		*
*****************************
FUNCTION uf_stocks_eliminar
	LOCAL lcDelDependencias 
	STORE .F. TO lcDelDependencias 
	
	** Verificar se existe nova vers�o para reiniciar o software
	uf_chk_nova_versao()
	
	IF !(uf_gerais_validaPermUser(ch_userno,ch_grupo,'Gest�o de Stock - Eliminar'))
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ELIMINAR ARTIGOS.","OK","",48)
		RETURN .f.
	ENDIF	
	
	IF !(used("St"))
		return
	ENDIF
	
	SELECT st
	
	IF empty(st.ref)
		RETURN .f.
	ENDIF
	
	&&verifica se a refer�ncia n�o est� protegida para elimina��o
	ALINES(refNelim,uf_gerais_getParameter("ADM0000000217","TEXT"),";")
	FOR i = 1 TO ALEN(refNelim)
		IF UPPER(ALLTRIM(refNelim[i])) == UPPER(ALLTRIM(st.ref))
			uf_perguntalt_chama("N�O � POSS�VEL ELIMINAR ESTA REFER�NCIA.","OK","",48)
			RETURN .f.
		ENDIF
	ENDFOR 
	********************
	
	IF !(uf_stocks_validaRefExisteTabMov('ELIMINAR'))
		RETURN .f.
	ENDIF
		
	&& Passou valida��es - vai apagar
		IF uf_perguntalt_chama("Tem a certeza que deseja apagar o Artigo"+CHR(13)+CHR(13)+"Vai perder todos os dados para sempre","Sim","N�o")
			IF uf_gerais_actGrelha ("","",[BEGIN TRANSACTION])	
				SELECT ST
				&& Apaga Atributos
				IF uf_gerais_actGrelha("","",[delete from b_statrib where ref=']+st.ref+['])		
					&& Apaga Codigos alternativos
					IF uf_gerais_actGrelha("","",[delete from bc where bc.ref=']+ALLTRIM(st.ref)+[' and bc.site_nr=] + ASTR(st.site_nr))		
						&& Apaga dependencias
						lcDelDependencias = uf_dependencias_elimina()
						IF lcDelDependencias 
							IF uf_gerais_actGrelha("","",[delete from st where ref=']+ALLTRIM(st.ref)+[' and st.site_nr=] + ASTR(st.site_nr))
								uf_gerais_actGrelha("","",[COMMIT TRANSACTION])
								uf_perguntalt_chama("REGISTO APAGADO COM SUCESSO!","OK","",64)
								
								** limpar cursor de pesquisa
								IF USED("uCrsPesqST")
									SELECT uCrsPesqST
									LOCATE FOR ALLTRIM(uCrsPesqST.ststamp) == ALLTRIM(st.ststamp)
									DELETE
									GO TOP
								ENDIF 
								****************************************
								
								&& navega para ultimo registo
								uf_gerais_actGrelha ("",[ucrsTemp6],[select top 1 ref from st (nolock) where st.site_nr=] + ASTR(st.site_nr) + [ order by ousrdata desc ,ousrhora desc])
								IF RECCOUNT()>0
									uf_stocks_chama(ucrsTemp6.ref)
								ELSE
									uf_stocks_chama('')
								ENDIF
								IF USED("ucrsTemp6")
									fecha("ucrsTemp6")
								endif	
							ELSE
								uf_gerais_actGrelha("","",[ROLLBACK])
								uf_perguntalt_chama("OCORREU UM ERRO AO APAGAR O ARTIGO!","OK","",16)
							ENDIF 	
						ELSE
							uf_gerais_actGrelha("","",[ROLLBACK])
							uf_perguntalt_chama("OCORREU UM ERRO AO APAGAR O ARTIGO!","OK","",16)
						ENDIF
					ELSE
						uf_gerais_actGrelha("","",[ROLLBACK])
						uf_perguntalt_chama("OCORREU UM ERRO AO APAGAR O ARTIGO!","OK","",16)
					endif	
				ELSE
					uf_gerais_actGrelha("","",[ROLLBACK])
					uf_perguntalt_chama("OCORREU UM ERRO AO APAGAR O ARTIGO!","OK","",16)
				ENDIF
			ELSE
				return		
			endif
		endif			
ENDFUNC


**
FUNCTION uf_Stocks_sair
	IF myStocksIntroducao == .t. OR myStocksAlteracao == .t.
		If uf_perguntalt_chama("DESEJA CANCELAR AS ALTERA��ES?"+CHR(13)+CHR(13)+"Ir� perder as �ltimas altera��es feitas","Sim","N�o")
			IF STOCKS.windowType = 1 AND WEXIST("ENCAUTOMATICAS")
				STOCKS.hide()
				FECHA("ST")
				STOCKS.release()

				RETURN .T.
			ELSE
				IF myStocksIntroducao
					&& navega para ultimo registo
					uf_stocks_chama(uf_gerais_devolveUltRegisto("st"))
				ELSE
					SELECT st
					uf_stocks_chama(st.ref)
				ENDIF

			&& Actualizar Painel
				uf_stocks_confEcra(1)
				uf_stocks_alternaMenu()

			ENDIF
		ENDIF
	ELSE
*!*			&& Evita erros nos objectos que vao buscar dados a cursores ao sair
*!*			stocks.pageframe1.page1.cmbunidade.rowsource	=	''
*!*			stocks.pageframe1.page1.cmbuni2.rowsource		=	''
*!*					
*!*			stocks.pageframe1.page3.cmbunidade.rowsource	=	''
*!*			stocks.pageframe1.page3.cmbuni2.rowsource		=	''
*!*			
		
		stocks.txtref.enabled = .f.
		
		
		IF WEXIST("DOCUMENTOS") 

			IF myDocIntroducao .OR. myDocAlteracao
	
				SELECT ST 
		
				uv_curRef = ST.ref
	
				lcCursor = IIF(myTipoDoc=="FO","FN","BI")
		
				SELECT &lcCursor.
				LOCATE FOR &lcCursor..ref == uv_curRef .AND. EMPTY(&lcCursor..design) .AND. &lcCursor..qtt = 0
				IF FOUND()
		
					SELECT &lcCursor.
			
					uf_documentos_eventoref()
		
				ENDIF	
		
			ENDIF

		ENDIF
			
		&& fecha cursores
		IF USED("ST")
			Fecha("ST")
		ENDIF
		
		IF USED("ucrsCampanhaOnline")
			Fecha("ucrsCampanhaOnline")
		ENDIF
		
		
		
		
		IF USED("uCrsUnidade")
			fecha("uCrsUnidade")
		ENDIF
*!*			IF USED("uCrsPesqST")
*!*				fecha("uCrsPesqST")
*!*			ENDIF
*!*			IF USED("uCrsAtributosSt")
*!*				fecha("uCrsAtributosSt")
*!*			ENDIF	
		If Used("uCrs1")
			fecha("uCrs1")
		EndIf
		If Used("uCrsEsgotados")
			fecha("uCrsEsgotados")
		EndIf
		if Used("uCrsBonus")
			fecha("uCrsBonus")
		ENDIF
		if Used("uCrsUnidade")
			fecha("uCrsUnidade")
		ENDIF
		if Used("uCrsAtrib")
			fecha("uCrsAtrib")
		ENDIF
		if Used("uCrsTipoAtrib")
			fecha("uCrsTipoAtrib")
		ENDIF
		if Used("uCrsVendasSst")
			fecha("uCrsVendasSst")
		ENDIF
		IF USED("uCrsRefAuto")
			fecha("uCrsRefAuto")
		ENDIF
		IF USED("ucrsListaRecursos")
			fecha("ucrsListaRecursos")
		ENDIF
		IF USED("ucrsListaDr")
			fecha("ucrsListaDr")
		ENDIF
		IF USED("ucrsListaRecursosST")
			fecha("ucrsListaRecursosST")
		ENDIF
		IF USED("ucrsListaDrST")
			fecha("ucrsListaDrST")
		ENDIF
		IF USED("ucrsImgProd")
			fecha("ucrsImgProd")
		ENDIF
			

		myStocksIntroducao = .f.
		myStocksAlteracao = .f.
		RELEASE myImpressaoOrigem
		RELEASE myControlaExisteRef
		RELEASE myControlaAltRef
						
		stocks.hide
		stocks.Release
		
		&& Se o ecr� de pesquisa estiver aberto, fecha-o
*!*			IF !(type("pesquisarST")=="U")
*!*				uf_pesqstocks_sair()
*!*			ENDIF
		
		&& Se o ecr� de codigos Adicionais estiver aberto, fecha-o
		if !(type("Codigos_Adicionais")=="U")
			uf_codigosAdicionais_sair()
		ENDIF
	ENDIF		
Endfunc	



*********************************
*	 NAVEGA REGISTO ANTERIOR	*
*********************************
FUNCTION uf_stocks_anterior

	LOCAL lcPos, lcCount
	STORE 0 TO lcPos
	
	IF USED("uCrsPesqST")
		IF RECCOUNT("uCrsPesqST") > 0
			SELECT uCrsPesqST
			lcPOS = RECNO("uCrsPesqST")
			
			IF lcPos > 1
				SELECT uCrsPesqST
				TRY
					SKIP - 1
				CATCH
				ENDTRY
				
				&& Chama dados do Artigo
				uf_stocks_chama(uCrsPesqST.ref)
			ENDIF
		ENDIF
	ENDIF
ENDFUNC



*********************************
*	 NAVEGA PROXIMO REGISTO 	*
*********************************
FUNCTION uf_stocks_seguinte	
	LOCAL lcPos,lcCount
	STORE 0 TO lcPos,lcCount
	
	IF USED("uCrsPesqST")
		IF RECCOUNT("uCrsPesqST")>0
			SELECT uCrsPesqST
			lcPOS = RECNO("uCrsPesqST")

			*CALCULATE COUNT(VAL(uCrsPesqST.ref)) TO lcCount

			IF lcPos < RECCOUNT("uCrsPesqST")
				SELECT uCrsPesqST
				TRY
					SKIP + 1
				CATCH
				ENDTRY
				
				&& Chama dados do Artigo
				uf_stocks_chama(uCrsPesqST.ref)
			ENDIF
		ENDIF
	ENDIF
ENDFUNC

FUNCTION uf_stocks_retornaDadosFornecedorPreferencial
		LPARAMETERS lcArray
		
		LOCAL lcFornec, lcFornecNome, lcEstab
		
		STORE 0 TO lcFornec, lcEstab		
		lcFornecNome=''
	
		

			&&configura fornecedor preferencial
		IF uf_gerais_getParameter("ADM0000000272","BOOL")
			
			LOCAL lcIdFornecedor
			
			lcIdFornecedor= INT(uf_gerais_getParameter("ADM0000000272","Num"))
			
			if(lcIdFornecedor>0)
			
				TEXT TO lcSQL NOSHOW TEXTMERGE 
					select 
						nome,no,estab, inactivo
					from 
						fl (nolock) 
					where 
						fl.no = <<lcIdFornecedor>>
                        and fl.estab = 0
				ENDTEXT 

				IF !uf_gerais_actgrelha("", "uCrsTempForn", lcSQL ) 
					uf_perguntalt_chama("ATEN��O: Esse fornecedor preferencial n�o existe","OK","", 48)	
					RETURN .f.
				ENDIF
				
				IF(RECCOUNT("uCrsTempForn")>0)

                    IF uCrsTempForn.inactivo
                        uf_perguntalt_chama("ATEN��O: Esse fornecedor preferencial encontra-se inactivo.","OK","", 48)	
                    ELSE
				
    					lcFornecNome = ALLTRIM(uCrsTempForn.nome)
	    				lcFornec	 = uCrsTempForn.no
		    			lcEstab		 = uCrsTempForn.estab 

                    ENDIF
			
				ENDIF
				

				
				fecha("uCrsTempForn")
				
			ENDIF
			
			
		
		ENDIF
		
		&&guarda dados no array
		STORE lcFornec TO lcArray(1) 
		STORE lcFornecNome TO lcArray(2)
		STORE lcEstab TO lcArray(3)

		

ENDFUNC




*************************************
*		VALORES POR DEFEITO			*
*************************************
FUNCTION uf_stocks_ValoresDefeito
	LPARAMETERS tcBool
	
	
	** Informa��o do fornecedor **
	DIMENSION lcFornData(3)
	uf_stocks_retornaDadosFornecedorPreferencial(@lcFornData)
	
	
	IF !tcBool
		LOCAL lcStamp
		lcStamp = uf_gerais_stamp()
		
		SELECT ST
		
		&& STSTAMP
		replace st.ststamp	with	lcStamp
		replace st.familia  with "99"
		replace st.faminome with "Outros"
		
		&& Op��es do Topo
		replace		st.u_fonte		WITH	"I"
		replace		st.qlook		with	.F.
		replace		st.stns			with	.F.
		replace		st.inactivo		with	.F.
		
		&& Separador de Integra�ao
		replace		st.baixr		with	.F.	
		replace		st.nsujpp		with	.F.
		replace		st.compnovo		with	.F.
		replace		st.clinica		with	.F.
		replace		st.sujinv		with	.F.
		
		&& Iva inclu�do
		
		IF UPPER(uf_gerais_getParameter("ADM0000000082","TEXT")) == UPPER("Farmacia") OR EMPTY(uf_gerais_getParameter("ADM0000000082","TEXT"))
			replace		st.IVAINCL		with	.T.
			replace		st.ivapcincl	with	.F.
			replace		st.iva1incl		with	.T.
			replace		st.iva2incl		with	.T.
			replace		st.iva3incl		with	.T.
			replace		st.iva4incl		with	.T.
			replace		st.iva5incl		with	.T.
		ELSE
			replace		st.IVAINCL		with	.F.
			replace		st.ivapcincl	with	.F.
			replace		st.iva1incl		with	.F.
			replace		st.iva2incl		with	.F.
			replace		st.iva3incl		with	.F.
			replace		st.iva4incl		with	.F.
			replace		st.iva5incl		with	.F.
		ENDIF 

		&& Separador Financeiros
		replace		st.epv1			with	0
		replace		st.marg1		with	0
		replace		st.pcusto		with	0
		replace		st.marg2		with	0
		replace		st.epcpond		with	0
		replace		st.epcult		with	0
		replace		st.marg3		with	0
		replace		st.marg4		with	0

		&& Dados de Cria��o
		SELECT ST
		replace 	st.ousrdata		WITH	date()
		replace 	st.ousrinis		WITH	m_chinis

		** Aplicar factor de convers�o por default **
		SELECT St
		IF EMPTY(st.validade)
			SELECT st
			replace st.validade WITH CTOD('1900.01.01')
		ENDIF
		
		** Tipo Etiqueta
		replace st.u_tipoetiq WITH 1
				
		** aplicar gest�o de stocks por default **
		** removeu-sea hierarquia de fam�lias **
		IF EMPTY(st.ptoenc) AND EMPTY(st.stmax)
			IF !EMPTY(st.familia)
				TEXT TO lcSql NOSHOW TEXTMERGE
					SELECT
						ptoenc, stmax
					from
						B_stfamgest stfg (nolock)
					where
						stfg.u_fam3='<<ALLTRIM(st.familia)>>'
				ENDTEXT
				IF uf_gerais_actGrelha("", "uCrsStFamGest", lcSql)
					
					IF RECCOUNT("uCrsStFamGest") > 0
						SELECT st
						replace st.stmax	WITH uCrsStFamGest.stmax
						replace st.ptoenc	WITH uCrsStFamGest.ptoenc
					ENDIF
					
					fecha("uCrsStFamGest")
				ENDIF
			ENDIF
		ENDIF
		******************************************
		
		** Aplicar factor de convers�o por default **
		SELECT St
		IF EMPTY(st.conversao)
			SELECT st
			replace st.conversao WITH 1
		ENDIF
		*********************************************
		
		STOCKS.menu1.todasEmpresas.tag = 'true'
		STOCKS.menu1.todasEmpresas.config("Empresas", myPath + "\imagens\icons\checked_w.png", "uf_stocks_criaFichaEmpresas","E")
		
		stocks.refresh
		

	
	
		** Aplicar Fornecedor preferencial **
		
		SELECT st
		replace st.fornec 	  	  WITH INT(lcFornData(1))
		replace st.fornecedor 	  WITH ALLTRIM(lcFornData(2))
		replace st.fornestab	  WITH INT(lcFornData(3))
			
		*********************************************
		
		
		
		
		
		
	ELSE && chamado pelo ecra de dicion�rio ao criar produto
		
		
		SELECT fprod
		
		** Aplicar factor de convers�o por default **
		** n�o � preciso visto ja estar criado directamente da fun��o do dicion�rio
		*********************************************
		
		** aplicar gest�o de stocks por default por familia infarmed **
		IF !EMPTY(fprod.ref) AND !EMPTY(fprod.u_familia)
			TEXT TO lcSql NOSHOW TEXTMERGE
				SELECT
					ptoenc, stmax
				from
					B_stfamgest stfg (nolock)
				where
					stfg.u_fam3 = '<<fprod.u_familia)>>'
			ENDTEXT
			IF uf_gerais_actgrelha("", "uCrsStFamGest", lcSql)
				IF RECCOUNT("uCrsStFamGest")>0
					TEXT TO lcSql NOSHOW textmerge
						UPDATE
							st
						SET
							stmax 		= <<uCrsStFamGest.stmax>>,
							ptoenc		= <<uCrsStFamGest.ptoenc>>
						where
							ref='<<ALLTRIM(fprod.ref)>>'
					ENDTEXT
					uf_gerais_actgrelha("","",lcSql)
				ENDIF
				
				fecha("uCrsStFamGest")
			ENDIF
		ENDIF
	ENDIF
	*******************************************************
ENDFUNC 


**
Function uf_stocks_ImporExporStock
	&& Valida Acesso
	IF !(uf_gerais_validaPermUser(ch_userno,ch_grupo,'Gest�o de Stock - Import/Export'))
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ACEDER � FUN��O DE IMPORTAR / EXPORTAR.","OK","",48)
		RETURN
	ENDIF

	LOCAL lcRpath
			
	lcRpath = "javaw -jar " + ALLTRIM(myPath)+"\externos\imporexpor\imporexpor.jar"
	oWSShell = CREATEOBJECT("WScript.Shell")
	oWSShell.Run(lcRpath,1,.f.)	
ENDFUNC


**
FUNCTION uf_stocks_preencheContas
	LPARAMETERS tcBool, lcSite_nr
	LOCAL lcSite_nrUpd
	STORE 0 TO lcSite_nrUpd
	
	IF !EMPTY(lcSite_nr) 
		lcSite_nrUpd = lcSite_nr
	ELSE 
		lcSite_nrUpd = mysite_nr
	ENDIF
	
	LOCAL lcCsnc, lcContaInv, lcContaCEV, lcSql, lcTabIva, lcStCsnc, lcStContaInv, lcStContaCev
	STORE 0 TO lcCsnc, lcTabIva, lcStCsnc
	STORE '' TO lcContaInv, lcContaCEV, lcSql, lcStContaInv, lcStContaCev
	
	** guardar tabiva **
	IF !tcBool
		SELECT st
		GO TOP
	
		lcTabIva 		= st.tabiva
		lcStCsnc		= st.cpoc
		lcStContaInv	= st.containv
		lcStContaCev	= st.contacev
	ELSE
		SELECT fprod
		GO TOP
		lcTabIva = fprod.u_tabiva
	ENDIF 
	
	&& CPOC
	IF (EMPTY(ALLTRIM(astr(lcStCsnc))) OR lcStCsnc == 0)
		IF uf_gerais_getParameter('ADM0000000152', 'BOOL')
			lcCsnc = uf_gerais_getParameter('ADM0000000152', 'NUM')
		ENDIF
	ENDIF
	*********

	&& Containv
	IF EMPTY(ALLTRIM(astr(lcStContaInv)))
		IF uf_gerais_getParameter('ADM0000000150', 'BOOL')
			lcContaInv = uf_gerais_getParameter('ADM0000000150', 'TEXT') + astr(lcTabIva)
			IF lcTabIva != 4
				lcContaInv = lcContaInv + '1'
			ENDIF
		ENDIF
	ENDIF 
	***********

	&& Contacev
	IF EMPTY(ALLTRIM(astr(lcStContaCev)))

		IF uf_gerais_getParameter('ADM0000000151', 'BOOL')
			lcContaCEV = uf_gerais_getParameter('ADM0000000151', 'TEXT') + astr(lcTabIva)
			IF lcTabIva !=4
				lcContaCEV = lcContaCEV + '1'
			ENDIF
		ENDIF
	ENDIF 
	***********	

	IF !tcBool &&chamado do ecra de stocks

		IF !EMPTY(lcContaInv)
			SELECT st
			replace st.containv WITH lcContaInv
		ENDIF 
		IF !EMPTY(lcCsnc)
			SELECT st
			replace st.cpoc WITH lcCsnc
		ENDIF	
		IF !EMPTY(lcContaCEV)
			SELECT st	
			replace st.contacev WITH lcContaCEV
		ENDIF 	
		************	

	ELSE && chamado do ecra de dicion�rio
	
		TEXT TO lcSql NOSHOW textmerge
			UPDATE
				st
			SET 
				cpoc 		= <<lcCsnc>>
				,containv	= '<<ALLTRIM(lcContaInv)>>'
				,contacev	= '<<ALLTRIM(lcContaCEV)>>'
			where
				st.ref = '<<ALLTRIM(fprod.ref)>>'
				and site_nr = '<<lcSite_nrUpd>>'
		ENDTEXT
		uf_gerais_actGrelha("", "", lcSql)

	ENDIF
ENDFUNC 


**
Function uf_stocks_controlaPCTzero
	LPARAMETERS tcBool, lcSite_nr
	LOCAL lcSite_nrUpd
	STORE 0 TO lcSite_nrUpd
	
	IF !EMPTY(lcSite_nr) 
		lcSite_nrUpd = lcSite_nr
	ELSE 
		lcSite_nrUpd = mysite_nr
	ENDIF
	
	LOCAL lcDescCom, lcCrs, lcColunaIva, lcRef
	STORE 0 TO lcDescCom
	
	SET POINT TO "."
	
	IF uf_gerais_getParameter('ADM0000000044', 'BOOL') && se controla PCT a Zero

		lcDescCom = uf_gerais_getParameter('ADM0000000044', 'NUM') && guardar desconto comercial para os eticos	
		
		IF tcBool && se vier do dicion�rio
			lcCrs 		= "fprod"
			lcColunaIva = "u_tabiva"
		ELSE && se vier do ecr� de stocks
			SELECT st
			
			IF st.epcusto != 0
				RETURN .f.
			ENDIF
			
			lcCrs 		= "st"
			lcColunaIva = "tabiva"
		ENDIF
	
		** guarda ref
		SELECT &lcCrs
		lcRef = &lcCrs..ref
		
		SELECT &lcCrs
		IF uf_gerais_actgrelha("", "uCrsGetIva", [select Isnull(taxa,0) as iva from taxasiva (nolock) where codigo=] + ALLTRIM(STR(&lcCrs..&lcColunaIva))) && guardar iva
			IF uf_gerais_actgrelha("", "uCrsGrupoPvp", [select ISNULL(grupo,'') as grupo, epreco from fpreco (nolock) where cnp='] + ALLTRIM(lcRef) + [' and grupo='pvp']) && validar se o produto � etico
				IF RECCOUNT("uCrsGrupoPvp") > 0 && se o produto for �tico
					
					IF  tcBool && se vier do dicion�rio
						IF uCrsGrupoPvp.epreco > 0 && se o produto tiver pvp
							TEXT TO lcSql NOSHOW textmerge
								UPDATE st
								SET epcusto = <<ROUND( (uCrsGrupoPvp.epreco * 100 / (uCrsGetIva.iva+100)) * ((100-lcDescCom)/100) ,2)>>
								where st.ref = '<<ALLTRIM(lcRef)>>' and st.site_nr = '<<lcSite_nrUpd>>'
							ENDTEXT
							uf_gerais_actgrelha("", "", lcSql)
						ENDIF
						
					ELSE && se vier do ecra de stocks
						SELECT st
						replace st.epcusto WITH ROUND( (st.epv1 * 100 / (uCrsGetIva.iva + 100)) * ((100-lcDescCom)/100) ,2)
					ENDIF
					
				ELSE && se n�o for �tico
					IF !tcBool && se vier do ecra de stocks
						SELECT st
						IF st.marg1 > 0
							replace st.epcusto WITH ROUND( (st.epv1 * 100 / (uCrsGetIva.iva + 100)) * ((100-st.marg1)/100) ,2)
						ELSE
							replace st.epcusto WITH ROUND( (st.epv1 * 100 / (uCrsGetIva.iva + 100)) ,2)
						ENDIF
					ENDIF
				ENDIF
				
				fecha("uCrsGrupoPvp")
			ENDIF
			
			Fecha("uCrsGetIva")
		ENDIF
		
	ENDIF
ENDFUNC


*********************************************************
*		Verifica se o Plano Seleccionado existe		  	*
*********************************************************
FUNCTION uf_stocks_verificaContaSNCExiste
	LPARAMETERS lcValor
	
	IF TYPE(lcValor)=="N"
		IF uf_gerais_actGrelha("", "uCrsTempConta","select conta from pc (nolock) where conta = '" + lcValor + "'")
			IF Reccount("uCrsTempConta")==0
				uf_perguntalt_chama("A CONTA SELECCIONADA N�O EXISTE! VERIFIQUE SE PRETENDE CORRIGIR.","OK","",64)
				fecha("uCrsTempConta")
			ENDIF
		ELSE
			uf_perguntalt_chama("OCORREU UM ERRO A OBTER DADOS DA CONTA. POR FAVOR CONTACTE O SUPORTE.","OK","",64)
		ENDIF
	ENDIF

ENDFUNC


**
FUNCTION uf_stocks_ImprimeArtigo
	SELECT ST
	
	&& Valida��o
	IF EMPTY(alltrim(st.ststamp))
		RETURN .f.
	ENDIF 
	
	&& Verifica se o report de artigo existe
	If !File(Alltrim(mypath) + '\analises\artigo.frx')
		uf_perguntalt_chama("O REPORT DE ARTIGO N�O EST� A SER DETECTADO! POR FAVOR CONTACTE O SUPORTE.","OK","", 48)
		RETURN .f.
	ENDIF
	
	&& Prepara impressora
	lcReport = ALLTRIM(mypath)+'\analises\artigo.frx'
	lcPrinter = Getprinter()
	lcDefaultPrinter=set("printer",2)
	
	&& preparar impressora
	Set Printer To Name (alltrim(lcPrinter))
			
	Set Device To print
	Set Console off
	
	&& Imprime	
	IF !Empty(lcPrinter)
		SELECT st
		Report Form Alltrim(lcReport) To Printer
	ELSE
		**		
	ENDIF
	
	&& por impressora no default
	Set Device To screen
	Set printer to Name ''+lcDefaultPrinter+''
	Set Console On		

	&& crack para n�o partir design
	select st
	go top
	stocks.refresh
ENDFUNC 


**
FUNCTION uf_stocks_chamaMovimentos

	** Verificar se existe nova vers�o para reiniciar o software
	uf_chk_nova_versao()
	
	SELECT st
	IF !EMPTY(st.ref)
		uf_movimentosst_chama(st.ref, st.design)
	ENDIF
ENDFUNC


******************************
** Calcular Check-Digit CNP **
******************************
FUNCTION uf_stocks_CheckDigitoCNP
	LPARAMETERS tcRef
	
	IF EMPTY(tcRef)
		RETURN .f.
	ENDIF
	
	LOCAL i, lcNum, lcTotal, lcResult
	STORE 0 To i, lcNum, lcTotal, lcResult

	FOR i = 1 TO Len(tcRef)
		lcNum = val((Substr(tcRef, i, 1)))

		IF i = 1 && Altera��o em 16-02-2006
	    	IF (lcNum != 2) And (lcNum != 3)  And (lcNum != 4) And (lcNum != 5)
	      		lcNum = 0
	      	ENDIF
		ENDIF

	  	IF Mod(i, 2) = 0
	   		lcNum = lcNum * 2
	  	ENDIF

	  	IF lcNum > 9
	    	lcNum = val(substr(Alltrim(str(lcNum)), 1, 1 )) + val(Substr(alltrim(str(lcNum)), 2, 1))
	    ENDIF
	  	
	  	lcTotal = lcTotal + lcNum
	ENDFOR
	
	DO WHILE lcTotal > 10
		lcTotal = lcTotal - 10
	ENDDO
	
	IF lcTotal > 0
		lcResult = 10 - lcTotal
	ELSE
		lcResult = lcTotal
	ENDIF
	
	RETURN lcResult
ENDFUNC



**
FUNCTION uf_Stocks_RefAutomatica
	&& valida��es
	IF !USED("st")
		RETURN .f.
	ENDIF
	
	** valida se pode alterar a refer�ncia - modo de altera��o
	IF stocks.Caption == "Stocks e Servi�os - Modo de Altera��o"
		IF !uf_stocks_validaRefExisteTabMov("EDITAR")
			RETURN .f.
		ENDIF
	ENDIF
	
	** gera nova refer�ncia
	IF !uf_gerais_actgrelha("", "uCrsRefAuto", [EXEC up_stocks_refAutomatica])
		RETURN .f.
	ENDIF
	
	SELECT uCrsRefAuto
	
	SELECT st
	replace st.ref	WITH 	ALLTRIM(uCrsRefAuto.ref)
		
	fecha("uCrsRefAuto")
	
	stocks.txtRef.refresh()
ENDFUNC 



**
FUNCTION uf_stocks_importDicSt

	IF TYPE("stocks")=="U"
		RETURN
	ENDIF
	
	If myStocksIntroducao OR myStocksAlteracao
		
		STOCKS.txtdesign.lostfocus
		
		SELECT st
		
		TEXT TO lcSql TEXTMERGE NOSHOW
			SELECT TOP 1
				fprod.ref
				,fprod.design
				,fprod.pvporig
				,fprod.barcode
				,fprod.u_tabiva
				,fprod.u_marca
				,fprod.u_familia
				,nome = case when stfami.nome is null then ' ' else stfami.nome end
				,fprod.titaimdescr
				
				--,fprod.depstamp
				--,fprod.secstamp
				--,fprod.catstamp
				
				--converter o id para varchar pois na st os campos sao varchar
				,convert(varchar(4),fprod.id_grande_mercado_hmr) as depstamp
				,convert(varchar(4),fprod.id_mercado_hmr) as secstamp
				,convert(varchar(4),fprod.id_categoria_hmr) as catstamp
				,convert(varchar(4),fprod.id_segmento_hmr) as segstamp
				
				,fprod.famstamp
				,fprod.sfstamp
				
				--,departamento = isnull(b_famDepartamentos.design ,'')
				--,seccao = isnull(b_famSeccoes.design,'')
				--,categoria = case when st.u_catstamp='' then st.categoria else isnull(b_famCategorias.design,'') end
				
				--carregar classificacao do hmr - 20200916 JG		
				, ISNULL(A.descr,'') as departamento
				, ISNULL(B.descr,'') as seccao
				, ISNULL(C.descr,'') as categoria
				, ISNULL(D.descr,'') as segmento
				-- carregar info online
				
				, convert(varchar(100),LTRIM(RTRIM(ISNULL(fprod.designOnline,'')))) as designOnline
				, convert(varchar(4000),LTRIM(RTRIM(ISNULL(fprod.caractOnline,'')))) as caractOnline
				, convert(varchar(4000),LTRIM(RTRIM(ISNULL(fprod.apresentOnline,'')))) as apresentOnline
								
				
				,famfamilia =  isnull(b_famFamilias.design,'')
			from
				fprod (nolock)
				left join stfami (nolock) on stfami.ref = fprod.u_familia
				
				--left join b_famDepartamentos (nolock) on fprod.depstamp = b_famDepartamentos.depstamp
				--left join b_famSeccoes (nolock) on fprod.secstamp = b_famSeccoes.secstamp
				--left join b_famCategorias (nolock) on fprod.catstamp = b_famCategorias.catstamp
				
				--carregar classificacao do hmr - 20200916 JG		
				left join grande_mercado_hmr (nolock) A on A.id = fprod.id_grande_mercado_hmr
				left join mercado_hmr (nolock) B on B.id = fprod.id_mercado_hmr
				left join categoria_hmr (nolock) C on C.id = fprod.id_categoria_hmr
				left join segmento_hmr (nolock) D on D.id = fprod.id_segmento_hmr
				
				left join b_famFamilias (nolock) on fprod.famstamp = b_famFamilias.famstamp
				left join st (nolock) on st.ref=fprod.ref
			where
				fprod.cnp = '<<ALLTRIM(st.ref)>>'
		ENDTEXT
		
		
	

		If !uf_gerais_actgrelha("", "CRSfprod", lcSql)
			RETURN
		ELSE
	
			
			IF reccount("CRSfprod") > 0


				select st
				replace st.design		with CRSfprod.design
				replace st.tabiva		with CRSfprod.u_tabiva
				replace st.familia		with CRSfprod.u_familia
				replace st.faminome		with CRSfprod.nome
				replace st.usr1			with CRSfprod.u_marca
				
				replace st.u_depstamp 	with ALLTRIM(CRSfprod.depstamp)
				replace st.u_secstamp 	with ALLTRIM(CRSfprod.secstamp)
				replace st.u_catstamp 	with ALLTRIM(CRSfprod.catstamp)
				replace st.u_segstamp 	with ALLTRIM(CRSfprod.segstamp)
				
				replace st.u_famstamp 	with CRSfprod.famstamp
				
				replace st.departamento with CRSfprod.departamento 
				replace st.seccao 		with CRSfprod.seccao 
				replace st.categoria 	with CRSfprod.categoria 
				replace st.segmento 	with CRSfprod.segmento				
				**replace st.famfamilia 	with CRSfprod.famfamilia
				
				replace st.designOnline	    with ALLTRIM(CRSfprod.designOnline)	
				replace st.caractOnline	    with ALLTRIM(CRSfprod.caractOnline)	
				replace st.apresentOnline	with ALLTRIM(CRSfprod.apresentOnline)	
				
				IF UPPER(uf_gerais_getParameter("ADM0000000082","TEXT")) == UPPER("Farmacia") OR EMPTY(uf_gerais_getParameter("ADM0000000082","TEXT"))
					replace		st.IVAINCL		with	.T.
					replace		st.ivapcincl	with	.F.
					replace		st.iva1incl		with	.T.
					replace		st.iva2incl		with	.T.
					replace		st.iva3incl		with	.T.
					replace		st.iva4incl		with	.T.
					replace		st.iva5incl		with	.T.
				ELSE
					replace		st.IVAINCL		with	.F.
					replace		st.ivapcincl	with	.F.
					replace		st.iva1incl		with	.F.
					replace		st.iva2incl		with	.F.
					replace		st.iva3incl		with	.F.
					replace		st.iva4incl		with	.F.
					replace		st.iva5incl		with	.F.
				ENDIF 
				
				replace st.u_lab		WITH alltrim(CRSfprod.titaimdescr)
				replace st.u_fonte		WITH "D"
				replace st.u_impetiq	WITH IIF(CRSfprod.pvporig>0,.t.,.f.)
				
				** Atualiza PVP apenas se tiver pvpOrig > que 0
				IF CRSfprod.pvporig > 0	
					SELECT st		
					replace st.epv1		WITH CRSfprod.pvporig
				ENDIF
			ELSE 
				uf_perguntalt_chama("A REFER�NCIA N�O EXISTE NO DICION�RIO DE PRODUTOS.","OK","", 48)
			ENDIF
			
			fecha("CRSfprod")
		ENDIF 
		
		uf_stocks_actualizaIvaTabIva()
	
		stocks.txtvalidade.setfocus
		stocks.refresh
	ELSE
		uf_perguntalt_chama("PARA EXECUTAR ESTA FUNCIONALIDADE NECESSITA DE TER O ECR� EM MODO DE ALTERA��O E TER A REFER�NCIA DO PRODUTO J� PREENCHIDA.","OK","", 48)
	ENDIF
ENDFUNC



**
FUNCTION uf_stocks_chamaDocumentos
	LPARAMETERS nDoc

	&& So funciona se o ecra estiver em modo de consulta
	IF !myStocksAlteracao AND !myStocksIntroducao
		
		IF !(TYPE("LISTADOCUMENTOS") == "U")
			IF !(myOrigemTipoPLD == "ST")
				uf_perguntalt_chama("A LISTAGEM DE DOCUMENTOS ENCONTRA-SE ABERTA DE OUTRO PAINEL.","OK","", 48)
				RETURN .f.
			ENDIF
		ENDIF
		
		SELECT st
		IF !EMPTY(ST.ref)
			IF EMPTY(nDoc)
				uf_LISTADOCUMENTOS_chama(st.ref, 0, st.design, "ST")
			ELSE
				uf_LISTADOCUMENTOS_chama(st.ref, 0, st.design, "ST", nDoc)
			ENDIF
		ENDIF
	ENDIF
ENDFUNC


**
FUNCTION uf_stocks_chamaStockArm
	SELECT ST
	IF !EMPTY(st.ref) AND st.stns == .f.
		uf_stockPorArmazem_chama(st.ref, st.design, 'STOCKS')
	ENDIF
ENDFUNC


**
FUNCTION uf_stocks_chamaDocumentosProduto

	** Verificar se existe nova vers�o para reiniciar o software
	uf_chk_nova_versao()
	
	SELECT st
	IF !EMPTY(st.ref)
		uf_documentosproduto_chama(st.ref, "")
	ENDIF
ENDFUNC


**
FUNCTION uf_stocks_chamaValidades
	SELECT st
	uf_validades_chama('ST', ALLTRIM(st.ref))
ENDFUNC


**
FUNCTION uf_stocks_chamaDicionario
	SELECT st
	uf_dicionario_chama(ALLTRIM(st.ref))
ENDFUNC

**
FUNCTION uf_stocks_criaProdFprod
	LOCAL lcStamp, lcvalida
	lcStamp = uf_gerais_stamp()
	lcValida = .f.
	
	** Verificar se existe nova vers�o para reiniciar o software
	uf_chk_nova_versao()
	
	SELECT st
		
	IF EMPTY(st.ref)
		uf_perguntalt_chama("A FICHA DO PRODUTO TEM DE ESTAR ABERTA COM O PRODUTO QUE PRETENDE CRIAR NO DICION�RIO.","OK","",64)
		RETURN .f.
	ENDIF
	
	IF !uf_perguntalt_chama("Aten��o: vai criar o produto ["+ALLTRIM(ST.DESIGN)+"] no dicion�rio. Tem a certeza que pretende continuar?", "Sim", "N�o")
		RETURN .f.
	ENDIF
	
	TEXT TO lcSQL NOSHOW TEXTMERGE
		select 
			ref 
		from 
			fprod (nolock)
		where 
			fprod.ref = '<<ALLTRIM(st.ref)>>'
	ENDTEXT 
	
	IF uf_gerais_actGrelha("","uCrsFprodVerify",lcSQL)
		IF RECCOUNT()>0
			uf_perguntalt_chama("ESSE PRODUTO J� EXISTE NO DICION�RIO.","OK","",64)
			FECHA("uCrsFprodVerify")
			RETURN .f.
		ENDIF
		fecha("uCrsFprodVerify")
	ENDIF
		
	TEXT TO lcSql NOSHOW TEXTMERGE
		INSERT INTO 
			fprod (fprodstamp, ref, cnp, design, grupo, u_alterado, depstamp, secstamp, catstamp)
		Values
			('<<ALLTRIM(lcStamp)>>', '<<ALLTRIM(st.ref)>>', '<<ALLTRIM(st.ref)>>', '<<ALLTRIM(strtran(st.design,chr(39),''))>>', 'ST', 1, '<<ALLTRIM(st.u_depstamp)>>', '<<ALLTRIM(st.u_secstamp)>>', '<<ALLTRIM(st.u_catstamp)>>')
	ENDTEXT

	IF !uf_gerais_actGrelha("","",lcSql)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA AO CRIAR O PRODUTO NO DICION�RIO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN
	ELSE
		uf_perguntalt_chama("PRODUTO CRIADO COM SUCESSO.","OK","",64)
	ENDIF
ENDFUNC


**
FUNCTION uf_stocks_abrirGestaoRecursos
	IF uf_dependencias_controlaAcessoMarcacoes() == .t.
		uf_abrePainelRecursos()
	ELSE
		uf_perguntalt_chama("PARA ABRIR O PAINEL DE GEST�O DE RECURSOS TEM QUE POSSUIR O M�DULO DE MARCA��ES.","OK","",64)
		RETURN .F.
	ENDIF
ENDFUNC


**
FUNCTION uf_stocks_NovoRecurso

	IF myStocksIntroducao == .f. AND myStocksAlteracao== .f.
		RETURN .f.
	ENDIF 

*!*		IF !(uf_gerais_actGrelha("","ucrsTempListaRecursos",[exec up_marcacoes_ListaRecursos '', '', 0]))
*!*			uf_perguntalt_chama("OCORREU UM ERRO A CARREGAR OS RECURSOS!","OK","",16)
*!*			RETURN .f.
*!*		ELSE
*!*			uf_valorescombo_chama("ucrsListaRecursosST.nome", 2, "ucrsTempListaRecursos", 2, "nome", "sel,tipo,nome,no",.t.)
*!*		ENDIF

*!*		Stocks.pageframe1.page5.gridEquipamentos.REFRESH

	**
	uf_PESQSERIESRECURSOS_Chama('STOCKS')
ENDFUNC



**
FUNCTION uf_stocks_ApagaRecurso

	IF myStocksIntroducao == .f. AND myStocksAlteracao== .f.
		RETURN .f.
	ENDIF 
		
	select ucrsListaRecursosST
	DELETE 
	
	Select ucrsListaRecursosST
	lcPos = recno()
	select ucrsListaRecursosST
	TRY
		IF lcPos>1
			go lcPos-1
		ENDIF
	CATCH
		**
	ENDTRY

	Stocks.pageframe1.page5.gridEquipamentos.REFRESH
	
	select ucrsListaRecursosST
	GO BOTTOM 
		 
ENDFUNC



*!*	**
*!*	FUNCTION uf_stocks_NovoTecnico

*!*		IF myStocksIntroducao == .f. AND myStocksAlteracao== .f.
*!*			RETURN .f.
*!*		ENDIF 

*!*		IF !(uf_gerais_actGrelha("","ucrsTempListaTecnicos",[exec up_marcacoes_ListaTecnicos '']))
*!*			uf_perguntalt_chama("OCORREU UM ERRO A CARREGAR OS TECNICOS!","OK","",16)
*!*			RETURN .f.
*!*		ELSE
*!*		
*!*			uf_valorescombo_chama("uCrsStTecnicos.nome", 2, "ucrsTempListaTecnicos", 2, "nome", "nome")
*!*			
*!*			IF USED("ucrsTempListaTecnicos")
*!*				fecha("ucrsTempListaTecnicos")
*!*			ENDIF
*!*		ENDIF
*!*		
*!*		IF !(uf_gerais_actGrelha("","ucrsDadosTecnico",[exec up_marcacoes_DadosUS '] + ALLTRIM(uCrsStTecnicos.nome) + [']))
*!*			uf_perguntalt_chama("OCORREU UM ERRO A CARREGAR OS TECNICOS!","OK","",16)
*!*			RETURN .f.
*!*		ELSE
*!*			SELECT ucrsDadosTecnico
*!*			SELECT uCrsStTecnicos
*!*			Replace uCrsStTecnicos.usstamp WITH ucrsDadosTecnico.usstamp 
*!*			Replace uCrsStTecnicos.userno WITH ucrsDadosTecnico.userno
*!*			Replace uCrsStTecnicos.ref WITH st.ref
*!*			Replace uCrsStTecnicos.duracao WITH 0
*!*		ENDIF
*!*		
*!*		Stocks.pageframe1.page5.gridTecnicos.REFRESH
*!*	ENDFUNC



*!*	**
*!*	FUNCTION uf_stocks_ApagaTecnico

*!*		IF myStocksIntroducao == .f. AND myStocksAlteracao== .f.
*!*			RETURN .f.
*!*		ENDIF 
*!*			
*!*		select uCrsStTecnicos
*!*		DELETE 
*!*		
*!*		Select uCrsStTecnicos
*!*		lcPos = recno()
*!*		select uCrsStTecnicos
*!*		TRY
*!*			IF lcPos>1
*!*				go lcPos-1
*!*			ENDIF
*!*		CATCH
*!*			**
*!*		ENDTRY

*!*		Stocks.pageframe1.page5.gridTecnicos.REFRESH
*!*		
*!*		select uCrsStTecnicos
*!*		GO BOTTOM 
*!*			 
*!*	ENDFUNC


**
FUNCTION uf_stocks_NovoServico

	IF myStocksIntroducao == .f. AND myStocksAlteracao== .f.
		RETURN .f.
	ENDIF 

	IF !(uf_gerais_actGrelha("","ucrsTempListaServicos",[exec up_marcacoes_ListaServicos]))
		uf_perguntalt_chama("OCORREU UM ERRO A CARREGAR OS TECNICOS!","OK","",16)
		RETURN .f.
	ELSE
	
		uf_valorescombo_chama("uCrsStSt.ref", 2, "ucrsTempListaServicos", 2, "ref", "ref")

	ENDIF
	
	SELECT ucrsTempListaServicos
	SELECT uCrsStSt
	Replace uCrsStSt.reforiginal WITH st.ref
	Replace uCrsStSt.ref WITH ucrsTempListaServicos.ref
	Replace uCrsStSt.design WITH ucrsTempListaServicos.design 
	Replace uCrsStSt.duracao WITH ucrsTempListaServicos.duracao
	Replace uCrsStSt.qtt WITH 1
		
	Stocks.pageframe1.page5.gridServicos.REFRESH
ENDFUNC


**
FUNCTION uf_stocks_ApagaServico

	IF myStocksIntroducao == .f. AND myStocksAlteracao== .f.
		RETURN .f.
	ENDIF 
		
	select uCrsStSt
	DELETE 
	
	Select uCrsStSt
	lcPos = recno()
	select uCrsStSt
	TRY
		IF lcPos>1
			go lcPos-1
		ENDIF
	CATCH
		**
	ENDTRY

	Stocks.pageframe1.page5.gridServicos.REFRESH
	
	select uCrsStSt
	GO BOTTOM 
		 
ENDFUNC


**
FUNCTION uf_stocks_CorrigeSA
	LOCAL lcSQL
	
	IF !uf_perguntalt_chama("Pretende atualizar os stocks por armaz�m? Deve executar backup da Base de Dados antes.","Sim","N�o",32)
		RETURN .f.
	ENDIF
	
	TEXT TO lcSQL NOSHOW TEXTMERGE
		exec up_stocks_CorrigeSA <<mysite_nr>>
	ENDTEXT 
	IF !(uf_gerais_actGrelha("","",lcSQL))
		uf_perguntalt_chama("OCORREU UM ERRO A ATUALIZAR SA!","OK","",16)
		RETURN .f.
	ELSE
		uf_perguntalt_chama("Stock por Armaz�m atualizado com sucesso!","OK","",64)
	ENDIF
ENDFUNC


FUNCTION uf_stocks_CorrigeSA_global
	LOCAL lcSQL
	
	IF !uf_perguntalt_chama("Pretende verificar/corrigir as quantidades de todos os artigos/armaz�ns?","Sim","N�o",32)
		RETURN .f.
	ENDIF
	
	TEXT TO lcSQL NOSHOW TEXTMERGE
		exec up_stocks_CorrigeSA_global
	ENDTEXT 
	IF !(uf_gerais_actGrelha("","",lcSQL))
		uf_perguntalt_chama("OCORREU UM ERRO A ATUALIZAR!","OK","",16)
		RETURN .f.
	ELSE
		uf_perguntalt_chama("Stocks atualizados com sucesso!","OK","",64)
	ENDIF
ENDFUNC



**
FUNCTION uf_stocks_consumonovo
	uf_pesqstocks_chama('CONSUMO_STOCKS')
ENDFUNC 


**
FUNCTION uf_stocks_actualizaIvaTabIva
	Select st
	TEXT TO lcSQL NOSHOW TEXTMERGE
		select top 1 taxa from taxasiva where codigo = <<st.tabIva>>
	ENDTEXT
	IF !(uf_gerais_actGrelha("","ucrsIvaTab",lcSQL))
		uf_perguntalt_chama("Ocorreu um erro a verificar Taxa de Iva em fun��o da tabela!","OK","",16)
		RETURN .f.
	ELSE
		Select ucrsIvaTab
		Select St
		Replace st.iva WITH ucrsIvaTab.taxa 
	ENDIF
	stocks.Pageframe1.Page1.tabiva.Refresh
	
ENDFUNC 



**
FUNCTION uf_stocks_calcMargensConfPvp
	LPARAMETERS tcMargem, lcEscolheCursor
	
	Local lcValor, lcArredondaPVP, lcCursor
	Store 0 To lcValor

	IF !USED("st")
		lcCursor = "uc_tmpST"		
	ELSE
		lcCursor = "st"
	ENDIF

	SELECT &lcCursor
	lcArredondaPVP = &lcCursor..arredondaPVP
	
	IF USED("st")
		stocks.Gravahistpvp = .t.
	ENDIF
	
	IF tcMargem
		** calcula pvp com base na margem comercial
		SELECT &lcCursor
		IF uf_gerais_getparameter_site('ADM0000000129', 'bool')
*!*				IF st.ivaincl
*!*					lcValor = (st.marg1/100+1) * st.epv1 * (st.iva/100+1) && pvp final		
*!*				ELSE
*!*					lcValor = (st.marg1/100+1) * st.epv1 && pvp final		
*!*				ENDIF 
			IF &lcCursor..marg1=100
				lcValor = ROUND((&lcCursor..epcusto / (1-(99.99/100)))*(1+(&lcCursor..iva/100)),2)
			ELSE
				lcValor = ROUND((&lcCursor..epcusto / (1-(&lcCursor..marg1/100)))*(1+(&lcCursor..iva/100)),2)
			ENDIF 
		ELSE
			IF &lcCursor..ivaincl
				lcValor = (&lcCursor..marg1/100+1) * &lcCursor..epcusto * (&lcCursor..iva/100+1) && pvp final		
			ELSE
				lcValor = (&lcCursor..marg1/100+1) * &lcCursor..epcusto && pvp final		
			ENDIF 
		ENDIF 

		IF lcArredondaPVP
			lcCentimos = 0.05
			lcValor = round(lcValor/lcCentimos ,0)*lcCentimos 
		ENDIF 

		IF lcValor > 0
			replace &lcCursor..epv1 With Round(lcValor,2)
		ENDIF
	ELSE	
		Select &lcCursor
		** margem comercial
		If &lcCursor..epv1>0 And ROUND(&lcCursor..epcusto,2)>0 

			IF lcArredondaPVP
				SELECT &lcCursor
				lcValor = &lcCursor..epv1 
				lcCentimos = 0.05
				lcValor = round(lcValor/lcCentimos ,0)*lcCentimos 
				SELECT &lcCursor
				replace &lcCursor..epv1 With Round(lcValor,2)
			ENDIF 

			IF uf_gerais_getparameter_site('ADM0000000129', 'bool')
				lcValor = round((1-(&lcCursor..epcusto/(&lcCursor..epv1/((&lcCursor..iva/100)+1))))*100,2)
			ELSE 
				IF &lcCursor..ivaincl
					lcValor = ((&lcCursor..epv1 / (&lcCursor..iva/100+1) / ROUND(&lcCursor..epcusto,2)) -1) * 100
				ELSE
					lcValor = ((&lcCursor..epv1 / ROUND(&lcCursor..epcusto,2)) -1) * 100	
				ENDIF 
			ENDIF 
			
			SELECT &lcCursor
			IF lcValor <= 9999
				replace &lcCursor..marg1 With Round(lcvalor,2)
			Else
				replace &lcCursor..marg1 With 0
			Endif
		Else
			replace &lcCursor..marg1 With 0
		Endif
		lcValor=0
	ENDIF
	**
	
	** desconto comercial (marg2)
	If ROUND(&lcCursor..epcusto,2)>0 And ROUND(&lcCursor..epcpond,2)>0
		lcValor = ((ROUND(&lcCursor..epcusto,2) / ROUND(&lcCursor..epcpond,2)) -1) * 100
		SELECT &lcCursor
		IF lcValor <= 9999
			replace &lcCursor..marg2 With Round(lcvalor,2)
		Else
			replace &lcCursor..marg2 With 0
		Endif
	Else
		replace &lcCursor..marg2 With 0
	Endif
	lcValor=0
	**
		
	** beneficio bruto (marg4)
	IF &lcCursor..epv1 > 0 	
		IF &lcCursor..ivaincl
			lcValor = ( &lcCursor..epv1 / (&lcCursor..iva/100+1) ) - ROUND(&lcCursor..epcpond,2)
		ELSE
			lcValor = &lcCursor..epv1 - ROUND(&lcCursor..epcpond,2)
		ENDIF 

		SELECT &lcCursor		
		IF lcValor <= 9999
			replace &lcCursor..marg4 With Round(lcvalor,2)
		ELSE
			replace &lcCursor..marg4 With 0
		ENDIF
	ELSE
		replace &lcCursor..marg4 With 0
	ENDIF
	lcValor = 0
	**
** margem bruta (marg3)
	IF &lcCursor..epv1 > 0 And ROUND(&lcCursor..epcpond,2)>0 &&And uCrsConfPvp.iva>0		
		IF &lcCursor..ivaincl	
			lcValor = ((&lcCursor..epv1 / (&lcCursor..iva/100+1) / ROUND(&lcCursor..epcpond,2)) -1) * 100
		ELSE	
			lcValor = ((&lcCursor..epv1 / ROUND(&lcCursor..epcpond,2)) -1) * 100
		ENDIF 

		SELECT &lcCursor		
		IF lcValor <= 9999
			replace &lcCursor..marg3 With Round(lcvalor,2)
		ELSE
			replace &lcCursor..marg3 With 0
		ENDIF
	ELSE
		replace &lcCursor..marg3 With 0
	ENDIF
	**


	IF USED("st")
		stocks.refresh
	ENDIF

ENDFUNC


**
FUNCTION uf_stocks_gravaHistPvp
	**
	IF !USED("ucrsAlteracaoHistPVP")
		RETURN .f.
	ENDIF 	
	
	IF!EMPTY(stocks.Gravahistpvp)
	
		SELECT ST
		SELECT ucrsAlteracaoHistPVP
		LOCATE FOR st.epv1 != ucrsAlteracaoHistPVP.epv1;
			OR st.marg1 != ucrsAlteracaoHistPVP.marg1;
			OR st.marg2 != ucrsAlteracaoHistPVP.marg2;
			OR st.marg3 != ucrsAlteracaoHistPVP.marg3;
			OR st.marg4 != ucrsAlteracaoHistPVP.marg4
		IF FOUND()
			** Guardar historico pvp's**
			lcStampReg = uf_gerais_stamp()

			TEXT TO lcSql NOSHOW TEXTMERGE
				INSERT INTO B_historicopvp
					(stamp, ref, opvp, dpvp, oMc, dMc, oDc, dDc, oMb, dMb, oBb, dBb, data, local, usr, site, terminal)
				values (
					'<<ALLTRIM(lcStampReg)>>'
					,'<<ALLTRIM(st.ref)>>'
					,<<Round(ucrsAlteracaoHistPVP.epv1,2)>>
					,<<Round(st.epv1,2)>>
					,<<Round(ucrsAlteracaoHistPVP.marg1,2)>>
					,<<Round(st.marg1,2)>>
					,<<Round(ucrsAlteracaoHistPVP.marg2,2)>>
					,<<Round(st.marg2,2)>>
					,<<Round(ucrsAlteracaoHistPVP.marg3,2)>>
					,<<Round(st.marg3,2)>>
					,<<Round(ucrsAlteracaoHistPVP.marg4,2)>>
					,<<Round(st.marg4,2)>>
					,dateadd(HOUR, <<difhoraria>>, getdate())
					,'Painel de STOCKS'
					,<<ch_userno>>
					,'<<ALLTRIM(mySite)>>'
					,'<<myTermNo>>'
					)
			ENDTEXT
			If !uf_gerais_actgrelha("", "", lcSql)
				uf_perguntalt_chama("OCORREU UM ERRO A GUARDAR HIST�RICO DE PVP'S. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			Endif
			****************************
		ENDIF 
		stocks.Gravahistpvp = .f.
	ENDIF 
	
	
ENDFUNC 


**
FUNCTION uf_stocks_criaFichaEmpresas

	IF STOCKS.menu1.todasEmpresas.tag == 'false'
		STOCKS.menu1.todasEmpresas.tag = 'true'
		STOCKS.menu1.todasEmpresas.config("Empresas", myPath + "\imagens\icons\checked_w.png", "uf_stocks_criaFichaEmpresas","E")
	ELSE
		STOCKS.menu1.todasEmpresas.tag = 'false'
		STOCKS.menu1.todasEmpresas.config("Empresas", myPath + "\imagens\icons\unchecked_w.png", "uf_stocks_criaFichaEmpresas","E")
	ENDIF

ENDFUNC 


**
FUNCTION uf_stocks_actualizaImportaPrioritarios
	LOCAL lcExecuteSql, lcFilePath, uv_farm
	lcExecuteSql = ""
	lcFilePath = ""
	uv_farm = .F.

	IF uf_perguntalt_chama("Qual a origem do ficheiro que pretende importar ?","Farm�cia","Fornecedor",32)
		uv_farm = .T.
	ENDIF

	IF uv_farm
		uf_perguntalt_chama("O ficheiro tem de ser no formato CSV com as refer�ncias separadas por [TAB].","OK","",64)
	ELSE
		uf_perguntalt_chama("O ficheiro tem de ser no formato CSV com as refer�ncias separadas por ';'.","OK","",64)
	ENDIF
	
	lcFilePath = uf_gerais_getFile(ALLTRIM(uf_gerais_getUmValor("B_terminal", "pathReport", "terminal = '" + ALLTRIM(myTerm) + "'")), 'csv')
	IF EMPTY(lcFilePath)
		uf_perguntalt_chama("Ficheiro inv�lido.","OK","",64)
		RETURN .f.
	ENDIF 
	
	IF UPPER(RIGHT(ALLTRIM(lcFilePath),3))!= "CSV"
		uf_perguntalt_chama("Ficheiro inv�lido. Verifique o formato do ficheiro.","OK","",64)
		RETURN .f.
	ENDIF 
	
	CREATE cursor ucrsTempImportaPrioritarios(LINHA c(254))
	SELECT ucrsTempImportaPrioritarios
	APPEND FROM &lcFilePath DELIMITED WITH TAB 
	
	&& comentado temporarioamente - N�o sei qual � a estrutra do ficheiro original, com este comment aceita
	&& csv com uma ref por linha - L 20160420
	
	IF !uv_farm

		SELECT ucrsTempImportaPrioritarios

		uv_vals = ucrsTempImportaPrioritarios.linha

		SELECT ucrsTempImportaPrioritarios
		DELETE

		FOR i = 1 TO ALINES(ua_refs, uv_vals, ";")
			IF !EMPTY(ua_refs[i])
				SELECT ucrsTempImportaPrioritarios
				APPEND BLANK

				REPLACE ucrsTempImportaPrioritarios.linha WITH ALLTRIM(ua_refs[i])
			ENDIF
		NEXT

		
	ENDIF
	
	IF RECCOUNT("ucrsTempImportaPrioritarios") == 0
		uf_perguntalt_chama("N�o foram encontradas artigos a actualizar. Verifique o formato do ficheiro.","OK","",64)
		RETURN .f.
	ENDIF 
	
	lcExecuteSql = ""
	SELECT ucrsTempImportaPrioritarios
	GO TOP 
	SCAN 
		lcExecuteSql = lcExecuteSql + IIF(!EMPTY(lcExecuteSql),",","") + "'" + ALLTRIM(ucrsTempImportaPrioritarios.linha) + "'"
	ENDSCAN
	
	lcSQL = ''
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		UPDATE st SET marcada = 0 WHERE site_nr = <<mysite_nr>>
		UPDATE st SET marcada = 1 WHERE site_nr = <<mysite_nr>> and st.ref in (<<lcExecuteSql>>)
	ENDTEXT 
	
	IF uf_gerais_actgrelha("", "", lcSql)
		uf_perguntalt_chama("Artigos marcados como priorit�rios com sucesso. Apenas foram actualizados os produtos existentes na BD.","OK","",64)
	ELSE 
		uf_perguntalt_chama("N�o foi poss�vel atualizar os artigos como priorit�rios.","OK","",64)
	ENDIF 
	
ENDFUNC 


**
FUNCTION uf_stocks_componenteNovo

	IF myStocksIntroducao == .f. AND myStocksAlteracao== .f.
		RETURN .f.
	ENDIF 

	uf_pesqstocks_chama('COMPONENTE_STOCKS')
	
ENDFUNC 


**
FUNCTION uf_stocks_ApagaComponente


	IF myStocksIntroducao == .f. AND myStocksAlteracao== .f.
		RETURN .f.
	ENDIF 
		
	select uCrsComponentes
	DELETE 
	
	Select uCrsComponentes
	lcPos = recno()
	select uCrsComponentes
	TRY
		IF lcPos>1
			go lcPos-1
		ENDIF
	CATCH
		**
	ENDTRY

	stocks.pageframe1.page3.gridComponentes.REFRESH
	
	select uCrsComponentes
	GO BOTTOM 
ENDFUNC 


**

** temp func 
FUNCTION uf_stocks_enviarVVM

	IF UPPER(ALLTRIM(uCrsE1.u_assfarm)) != 'AFP'
		uf_perguntalt_chama("Funcionalidade dispon�vel apenas para associados AFP.","OK","",64)	
		RETURN .f.
	ENDIF
	
	lcSQL = ''
	TEXT TO lcSql NOSHOW TEXTMERGE
		SELECT textvalue FROM b_parameters WHERE stamp = 'ADM0000000205'
	ENDTEXT
	IF  !uf_gerais_actgrelha("", "uCrsAux", lcSql)
		uf_perguntalt_chama("Ocorreu uma anomalia a enviar a informa��o para o Infarmed. Por favor contacte o Suporte.","OK","",16)
		RETURN .f.
	ENDIF 
	
	IF UPPER(ALLTRIM(uCrsAux.textvalue)) == UPPER(ALLTRIM(uf_gerais_getdate(.f.,"MES")))
		uf_perguntalt_chama("Informa��o j� comunicada para o m�s atual.","OK","",64)
	ELSE
		
		regua(0,1,"A enviar informa��o...")

		lcSQL = ''
		TEXT TO lcSql NOSHOW TEXTMERGE		
			select top 10000 * from fprod (nolock)
		ENDTEXT
		IF  !uf_gerais_actgrelha("", "uCrsAuxTT", lcSql)
			uf_perguntalt_chama("Ocorreu uma anomalia a enviar a informa��o para o Infarmed. Por favor contacte o Suporte.","OK","",16)
			regua(2)
			RETURN .f.
		ENDIF 
		
		IF USED("uCrsAuxTT")
			fecha("uCrsAuxTT")
		ENDIF	
		
		lcSQL = ''
		TEXT TO lcSql NOSHOW TEXTMERGE
			UPDATE b_parameters SET textvalue = '<<UPPER(uf_gerais_getdate(.f.,"MES"))>>', mostrabool = 0 WHERE stamp = 'ADM0000000205'
		ENDTEXT
		IF  !uf_gerais_actgrelha("", "", lcSql)
			uf_perguntalt_chama("Ocorreu uma anomalia a enviar a informa��o para o Infarmed. Por favor contacte o Suporte.","OK","",16)
			regua(2)
			RETURN .f.
		ENDIF 
		
		
		regua(2)
		
		uf_perguntalt_chama("Envio da informa��o para o Infarmed, efetuado com sucesso.","OK","",64)	
	ENDIF

	IF USED("uCrsAux")
		fecha("uCrsAux")
	ENDIF
ENDFUNC 


FUNCTION  uf_stocks_getSqlUltAno()
	LPARAMETERS lcRef
	
	LOCAL lcEmpSel
	lcEmpSel = ''
	if(USED("ucrsConfEmpresas"))
		STORE '' TO lcEmpSel
		SELECT ucrsConfEmpresas
		GO TOP 
		SCAN 
			IF ucrsConfEmpresas.selempresa=.t.
				IF EMPTY(lcEmpSel)
					lcEmpSel = ALLTRIM(STR(ucrsConfEmpresas.armazem1))
				ELSE
					lcEmpSel = lcEmpSel + ',' + ALLTRIM(STR(ucrsConfEmpresas.siteno))
				ENDIF 
			ENDIF 
		ENDSCAN 
	ENDIF 
	



	if(EMPTY(lcEmpSel))
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			exec up_stocks_VendasUltAno <<IIF(MyEncConjunta==.t.,0,myarmazem)>>,'<<lcRef>>',<<mysite_nr>>
		ENDTEXT 
	ELSE
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			exec up_stocks_VendasUltAno_multiemp '<<lcEmpSel>>','<<lcRef>>',<<mysite_nr>>
		ENDTEXT 	
	ENDIF
	



	RETURN lcSQL 
		
ENDFUNC


FUNCTION  uf_stocks_getSqlQttvendas()
   LPARAMETERS lcRef
   
	
	LOCAL lcEmpSel
	lcEmpSel = ''
	if(USED("ucrsConfEmpresas")) and !USED("crsEnc")
		STORE '' TO lcEmpSel
		SELECT ucrsConfEmpresas
		GO TOP 
		SCAN 
			IF ucrsConfEmpresas.selempresa=.t.
				IF EMPTY(lcEmpSel)
					lcEmpSel = ALLTRIM(STR(ucrsConfEmpresas.armazem1))
				ELSE
					lcEmpSel = lcEmpSel + ',' + ALLTRIM(STR(ucrsConfEmpresas.siteno))
				ENDIF 
			ENDIF 
		ENDSCAN 
	ENDIF 
	


	if(EMPTY(lcEmpSel))			
		TEXT TO lcSQL TEXTMERGE noshow
			exec up_stocks_qttvendas <<IIF(MyEncConjunta==.t.,0,myarmazem)>>,'<<lcRef>>',<<IIF(MyEncConjunta==.t.,-1,mysite_nr)>>
		ENDTEXT
	ELSE
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			exec up_stocks_qttvendas_multiemp'<<lcEmpSel>>','<<lcRef>>',<<mysite_nr>>
		ENDTEXT 	
	ENDIF
	

	RETURN lcSQL 
		
ENDFUNC







** funcao para consultar informa��o do produto no produto 20160816 LL
FUNCTION uf_stocks_consultarProd
	LPARAMETERS lnOrigem
	&& Guardar caminho do software de envio
	LOCAL  lcWsPath, lcIDCliente, lcNomeJar, lcToken, lcInfForn, lcSearchString, lcEta
	STORE '' TO lcWsPath, lcIDCliente, lcNomeJar, lcToken, lcInfForn, lcSearchString, lcEta
	
	&& 
	lcToken = uf_gerais_stamp()
	
	&& definir searchsting 
	DO CASE
		CASE lnOrigem == "Atendimento"
			lcSearchString = ALLTRIM(fi.ref)
		CASE lnOrigem == "Stocks"
			lcSearchString = ALLTRIM(st.ref)	
		CASE lnOrigem == "Encomendas"
			if(USED("crsenc"))			
				lcSearchString = ALLTRIM(crsenc.ref)
			endif		
		CASE lnOrigem == "EncomendasAtendimento"
			
			SELECT ucrsEncomenda.ref FROM ucrsEncomenda WHERE ucrsEncomenda.sel == .t. INTO CURSOR uCrsPesqST_Sel READWRITE 
			
			IF (RECCOUNT("uCrsPesqST_Sel")== 0)
				uf_perguntalt_chama("Necessita selecionar um produto antes de pesquisar informa��o no fornecedor.","OK","", 16)
				RETURN .f.
			ELSE
				IF (RECCOUNT("uCrsPesqST_Sel")> 1)
				uf_perguntalt_chama("Apenas pode selecionar um producto para consulta de informa��o no fornecedor.","OK","", 16)
				RETURN .f.
				ENDIF
			ENDIF

			lcSearchString = ALLTRIM(uCrsPesqST_Sel.ref)

			IF USED("uCrsPesqST_Sel")
				fecha("uCrsPesqST_Sel")
			ENDIF

		OTHERWISE 
			lcSearchString = ALLTRIM(st.ref)				
	ENDCASE	
	

	&& id do cliente LTS
	SELECT uCrsE1
	IF EMPTY(ALLTRIM(uCrsE1.id_lt))
		uf_perguntalt_chama("O C�DIGO DE CLIENTE LOGITOOLS N�O EST� CORRETAMENTE PREENCHIDO. POR FAVOR CONTACTE O SUPORTE.","OK","", 16)
		RETURN .f.
	ENDIF 
	lcIDCliente = ALLTRIM(uCrsE1.id_lt)
	
	&& valida e configura software p envio encomenda
	lcNomeJar = 'LTSESBProductsClient.jar'
	IF !FILE(ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\LTSESBProductsClient\' + ALLTRIM(lcNomeJar))
		uf_perguntalt_chama("O SOFTWARE DE ENVIO LTS ESB N�O EST� CONFIGURADO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	ENDIF 
	
	regua(1,2,"A consultar informa��o do produto, por favor aguarde.")
	
	&& Define de usa testes ou n�o 
	&& verifica se utiliza webservice de testes
	LOCAL lcDefineWsTest
	STORE 0 TO lcDefineWsTest
	IF uf_gerais_getParameter("ADM0000000227","BOOL") == .f.
		lcDefineWsTest = 0
	ELSE
		lcDefineWsTest = 1
	ENDIF 
			
	lcWsPath = ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\LTSESBProductsClient\' + ALLTRIM(lcNomeJar);
		+ ' "--searchEntity=' + 'OCP' + ["];
		+ ' "--searchType=' + 'Ref' + ["];
		+ ' "--searchString=' + lcSearchString  + ["];
		+ ' "--idCl=' + lcIDCliente + ["];
		+ ' "--site=' + UPPER(ALLTRIM(mySite)) + ["];
		+ ' "--Token=' + ALLTRIM(lcToken) + ["]

	&& Envia comando Java		
	lcWsPath = "javaw -jar " + lcWsPath 		
	
	&& aguarda que software termine o envio da informa��o 3� PARAMETRO = .T.
	oWSShell = CREATEOBJECT("WScript.Shell")
	oWSShell.Run(lcWsPath, 1, .t.)
	
	regua(2)
				
	&& mostra informa��o retornada pelo fornecedor. && TEMP TOP 1 POR CAUSA DE BUG QUE HT AINDA N�O CONSEGUIU CORRIGIR
	lcSQL = ''
	TEXT TO lcSQL TEXTMERGE NOSHOW 
		SELECT TOP 1 ref, descr, status, stock, pct, pvp, tax, ISNULL(eta,'') as eta FROM ext_esb_products (nolock) WHERE token = '<<lcToken>>'
	ENDTEXT		
	IF !uf_gerais_actGrelha("","uCrsAuxRespostaDetalheESB",lcSQL)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VERIFICAR A RESPOSTA DO FORNECEDOR. POR FAVOR CONTACTE O SUPORTE.", "Ok", "", 16)
		RETURN .f.
	ELSE
		IF reccount("uCrsAuxRespostaDetalheESB") > 0
		
			lcStock = ALLTRIM(STR(uCrsAuxRespostaDetalheESB.STOCK))
			
		
			IF ISNULL(lcStock)
				lcStock = "-" 
			ENDIF
			
			IF  YEAR(uCrsAuxRespostaDetalheESB.eta) = 1900
				lcEta = "-" 
			ELSE
				lcEta =  ALLTRIM(TRANSFORM(uCrsAuxRespostaDetalheESB.eta,'99999999'))
			ENDIF
			

			lcInfForn = ALLTRIM(uCrsAuxRespostaDetalheESB.descr) + CHR(13) + "Fornecedor: OCP" + CHR(13) +"Estado: " + ALLTRIM(uCrsAuxRespostaDetalheESB.status) + "." + CHR(13) + "Stock: " + lcStock  + "." + CHR(13) +  "P. Custo: " + LTRIM(TRANSFORM(uCrsAuxRespostaDetalheESB.pct,'99999.99')) + "�" + CHR(13) + "PVP: " + LTRIM(TRANSFORM(uCrsAuxRespostaDetalheESB.pvp,'9999.99')) + "�" + CHR(13) + "Taxa Iva: " + LTRIM(TRANSFORM(uCrsAuxRespostaDetalheESB.tax,'9999.99')) + CHR(13) + "Data de entrega: " + lcEta
			uf_perguntalt_chama(lcInfForn,"OK","",64)
			**uf_perguntalt_chama(ALLTRIM(uCrsAuxRespostaDetalheESB.descr) + "." + CHR(13) + "Status: " + ALLTRIM(uCrsAuxRespostaDetalheESB.status) + "." + CHR(13) + "Stock: " + uCrsAuxRespostaDetalheESB.STOCK,"OK","",64)
		ELSE
			uf_perguntalt_chama("N�O FORAM ENCONTRADOS PRODUTOS COM A REFER�NCIA INTRODUZIDA. POR FAVOR CONTACTE O FORNECEDOR PARA MAIS INFORMA��O. OBRIGADO.", "Ok", "", 16)
		ENDIF		
	ENDIF

	IF USED("uCrsAuxRespostaDetalheESB")
		fecha("uCrsAuxRespostaDetalheESB")
	ENDIF
	
ENDFUNC 

FUNCTION uf_altgr_escolheEmpresasST
	
	IF vartype(ucrsEmpresasSel)=="U" THEN 
		** Listam de Empresas
		IF USED("ucrsConfEmpresas")
			SELECT * FROM ucrsConfEmpresas INTO CURSOR ucrsConfEmpresasAux READWRITE 
		ENDIF 
		
		IF !USED("ucrsConfEmpresas")
			TEXT To lcSQL TEXTMERGE NOSHOW
				exec up_gerais_trocaEmpresa <<ch_userno>>,'<<ALLTRIM(ch_grupo)>>'
			ENDTEXT 
			uf_gerais_actGrelha("","ucrsListSiteAux",lcSQL)
			SELECT .f. as selEmpresa,* FROM ucrsListSiteAux INTO CURSOR ucrsConfEmpresas READWRITE 
		ENDIF 
		
		IF USED("ucrsConfEmpresasAux")	
			SELECT ucrsConfEmpresas
			GO TOP
			SCAN 
				SELECT ucrsConfEmpresasAux	
				LOCATE FOR ALLTRIM(UPPER(ucrsConfEmpresas.local)) == ALLTRIM(UPPER(ucrsConfEmpresasAux.local))
				IF FOUND()
					Replace ucrsConfEmpresas.selEmpresa WITH ucrsConfEmpresasAux.selEmpresa
				ENDIF 
			ENDSCAN 
			
		ENDIF 
		
		SELECT ucrsConfEmpresas
		GO top
		SCAN
			IF ucrsConfEmpresas.siteno=mySite_nr then
				replace ucrsConfEmpresas.selempresa WITH .t.
			ENDIF 
		ENDSCAN 
		IF type("VALORESCOMBO")!="U"
			VALORESCOMBO.release
		ENDIF 
		uf_valorescombo_chama("ucrsConfEmpresas", 6, "ucrsConfEmpresas", 2, "LOCAL", "LOCAL",.t.,.f.,.t.,.t.)
		
	ELSE

		uf_valorescombo_chama("ucrsConfEmpresas", 6, "ucrsConfEmpresas", 3, "LOCAL", "LOCAL",.t.,.f.,.t.,.t.)
	ENDIF 
	
ENDFUNC 


FUNCTION uf_stocks_Corrige_mov
	LOCAL lcEPcpond, lcMovEntrada, lcStock, lcQttMov, lcCont, lcCont2, lcMesesAtua  
	STORE 0.00 TO lcEPcpond
	STORE .f. TO lcMovEntrada
	STORE 0 TO lcStock, lcQttMov, lcCont
	STORE 1 TO lcCont2 

	If uf_perguntalt_chama("CONFIRMA QUE QUER CORRIGIR O PRE�O DE CUSTO PONDERADO DOS MOVIMENTOS E DA FICHA DESTE ARTIGO?","Sim","N�o")
	
		lcMesesAtua = uf_gerais_getparameter_site('ADM0000000130', 'NUM', mysite)
		IF lcMesesAtua <= 0
			lcMesesAtua = -1900
		ELSE
			lcMesesAtua = lcMesesAtua  * -1	
		ENDIF
		IF uf_gerais_getParameter("ADM0000000301","BOOL")
			TEXT To lcSQL TEXTMERGE NOSHOW
				SELECT sl.*, cm2.mudapcpond FROM sl (NOLOCK) 
				inner join cm2 (nolock) on sl.cm=cm2.cm 
				where ref='<<ALLTRIM(st.ref)>>' 
					and sl.ousrdata >= DATEADD(month, <<lcMesesAtua>> ,GETDATE())
				order by sl.datalc, sl.ousrdata, sl.ousrhora
			ENDTEXT 
		ELSE
			TEXT To lcSQL TEXTMERGE NOSHOW
				SELECT sl.*, cm2.mudapcpond FROM sl (NOLOCK) 
				inner join cm2 (nolock) on sl.cm=cm2.cm 
				where ref='<<ALLTRIM(st.ref)>>' and armazem=<<st.site_nr>> 
					and sl.ousrdata >= DATEADD(month, <<lcMesesAtua>> ,GETDATE())
				order by sl.datalc, sl.ousrdata, sl.ousrhora
			ENDTEXT 		
		ENDIF 
		uf_gerais_actGrelha("","ucrsListMov",lcSQL)

		IF reccount("ucrsListMov") > 0
			lcCont= reccount("ucrsListMov")
			** Verificar se h� movimentos de entrada para recalcular
*!*				SELECT ucrsListMov
*!*				GO TOP 
*!*				SCAN 
*!*					**IF ALLTRIM(ucrsListMov.cmdesc)='V/Factura' OR ALLTRIM(ucrsListMov.cmdesc)='V/Vd. Dinhei.' OR ALLTRIM(ucrsListMov.cmdesc)='V/Vd. Dinhei.' OR ALLTRIM(ucrsListMov.cmdesc)='V/Guia Transp.'
*!*					IF ALLTRIM(ucrsListMov.cmdesc)='V/Factura' OR ALLTRIM(ucrsListMov.cmdesc)='V/Vd. Dinhei.' 
*!*						lcMovEntrada = .t.
*!*					ENDIF 
*!*				ENDSCAN 
*!*				IF lcMovEntrada=.f. then
*!*					uf_perguntalt_chama("N�O FORAM ENCONTRADOS MOVIMENTOS DE ENTRADA DO ARTIGO. N�O H� NADA PARA RECALCULAR.", "Ok", "", 16)
*!*					RETURN .F.
*!*				ENDIF 
*!*				lcMovEntrada = .f.
			
			** Fazer o rec�lculo e atualiza��o de valores
			regua(0,lcCont,"A Atualizar Documentos...")
			SELECT ucrsListMov
			GO TOP 
			SCAN 
				regua(1,lcCont2,"A Atualizar Documentos...")
				
				** atualiza��o de movimentos da sl
				TEXT To lcSQL TEXTMERGE NOSHOW
					UPDATE sl SET epcpond=<<lcEPcpond>>, pcpond=<<ROUND(lcEPcpond*200.482,2)>> where slstamp='<<ALLTRIM(ucrsListMov.slstamp)>>'
				ENDTEXT 
				uf_gerais_actGrelha("","",lcSQL)

				SELECT ucrsListMov
				** atualiza��o de movimentos da fi
				IF !EMPTY(ucrsListMov.fistamp)
					TEXT To lcSQL TEXTMERGE NOSHOW
						UPDATE fi SET epcp=<<lcEPcpond>>, pcp=<<ROUND(lcEPcpond*200.482,2)>> where fistamp='<<ALLTRIM(ucrsListMov.fistamp)>>'
					ENDTEXT 
					uf_gerais_actGrelha("","",lcSQL)
				ENDIF 
					
				SELECT ucrsListMov
				** atualiza��o de movimentos da bi
				IF !EMPTY(ucrsListMov.bistamp)
					TEXT To lcSQL TEXTMERGE NOSHOW
						UPDATE bi SET epcusto=<<lcEPcpond>>, pcusto=<<ROUND(lcEPcpond*200.482,2)>> where bistamp='<<ALLTRIM(ucrsListMov.bistamp)>>' and ar2mazem=<<st.site_nr>>
					ENDTEXT 
					uf_gerais_actGrelha("","",lcSQL)
				ENDIF 
				
				SELECT ucrsListMov
				** atualiza��o de movimentos da fn
				IF !EMPTY(ucrsListMov.fnstamp)
					TEXT To lcSQL TEXTMERGE NOSHOW
						UPDATE fn SET qtt=qtt where fnstamp='<<ALLTRIM(ucrsListMov.fnstamp)>>'
					ENDTEXT 
					uf_gerais_actGrelha("","",lcSQL)
				ENDIF

				SELECT ucrsListMov				
				**IF ALLTRIM(ucrsListMov.cmdesc)='V/Factura' OR ALLTRIM(ucrsListMov.cmdesc)='V/Vd. Dinhei.' OR ALLTRIM(ucrsListMov.cmdesc)='V/Vd. Dinhei.' OR ALLTRIM(ucrsListMov.cmdesc)='V/Guia Transp.'
				IF ucrsListMov.mudapcpond = .t.
					REPLACE ucrsListMov.EPCPOND WITH lcEPcpond
					IF ucrsListMov.cm < 50
						lcQttMov = ucrsListMov.qtt
					ELSE 
						lcQttMov = ucrsListMov.qtt * (-1)
					ENDIF 

					DO CASE 
						CASE (lcStock + lcQttMov ) <= 0 
							lcEPcpond = 0
						case (lcStock + lcQttMov ) > 0
							IF lcStock > 0 then
								**lcEPcpond = ROUND(((lcStock * lcEPcpond)+(ucrsListMov.ett)) / (lcStock  + ucrsListMov.qtt),2)
								lcEPcpond = ROUND(((lcStock * lcEPcpond)+(ucrsListMov.evu * ucrsListMov.qtt)) / (lcStock  + ucrsListMov.qtt),2)
							ELSE
								**lcEPcpond = ROUND(((0 * lcEPcpond)+(ucrsListMov.ett)) / (0  + ucrsListMov.qtt),2)
								lcEPcpond = ROUND(((0 * lcEPcpond)+(ucrsListMov.evu * ucrsListMov.qtt)) / (0  + ucrsListMov.qtt),2)
							ENDIF 
						OTHERWISE
					ENDCASE 
					lcStock  = lcStock + lcQttMov
				ELSE
					REPLACE ucrsListMov.EPCPOND WITH lcEPcpond
					IF ucrsListMov.cm < 50 then
						lcStock = lcStock + ucrsListMov.qtt
					ELSE 
						lcStock = lcStock - ucrsListMov.qtt
					ENDIF 
				ENDIF 
				
				lcCont2 = lcCont2 + 1
				SELECT ucrsListMov
			ENDSCAN 
			
			SELECT st 
			IF uf_gerais_getParameter("ADM0000000301","BOOL")
				TEXT To lcSQL TEXTMERGE NOSHOW
					UPDATE 
						st 
					SET 
						epcpond=<<lcEPcpond>>, pcpond=<<ROUND(lcEPcpond*200.482,2)>>
						, marg4 = (case when st.epv1>0
							then ROUND( (st.epv1 / (taxasiva.taxa/100+1)) - <<lcEPcpond>> ,2)
  							ELSE 0 end)
  					from st
  					left join taxasiva on taxasiva.codigo = st.tabiva
					where 
						ref='<<ALLTRIM(st.ref)>>'
				ENDTEXT 
			ELSE
				TEXT To lcSQL TEXTMERGE NOSHOW
					UPDATE 
						st 
					SET 
						epcpond=<<lcEPcpond>>
						, pcpond=<<ROUND(lcEPcpond*200.482,2)>>
						, marg4 = (case when st.epv1>0
							then ROUND( (st.epv1 / (taxasiva.taxa/100+1)) - <<lcEPcpond>> ,2)
  							ELSE 0 end)
  					from st
  					left join taxasiva on taxasiva.codigo = st.tabiva
  					 where 
  					 	ref='<<ALLTRIM(st.ref)>>' 
  					 	and site_nr=<<st.site_nr>>
				ENDTEXT
			ENDIF 
			uf_gerais_actGrelha("","",lcSQL)
			
			uf_perguntalt_chama("PROCESSO CONCLU�DO COM SUCESSO.", "Ok", "", 64)
			regua(2)
			
		ELSE
			uf_perguntalt_chama("N�O FORAM ENCONTRADOS MOVIMENTOS COM A REFER�NCIA INTRODUZIDA. N�O H� NADA PARA RECALCULAR.", "Ok", "", 16)
		ENDIF 

		IF USED("ucrsListMov")
			fecha("ucrsListMov")
		ENDIF
	ELSE
		uf_perguntalt_chama("PROCESSO CANCELADO.", "Ok", "", 16)
	ENDIF

ENDFUNC 

FUNCTION uf_stocks_Corrige_mov_todos
	LOCAL lcEPcpond, lcMovEntrada, lcStock, lcQttMov, lcCont, lcCont2 , lcRef, lcStampSt, lcMesesAtua 
	
	STORE 0.00 TO lcEPcpond
	STORE .f. TO lcMovEntrada
	STORE 0 TO lcStock, lcQttMov, lcCont
	STORE 1 TO lcCont2 
	STORE '' TO lcRef, lcStampSt

	If uf_perguntalt_chama("CONFIRMA QUE QUER CORRIGIR O PRE�O DE CUSTO PONDERADO DOS MOVIMENTOS E DA FICHA DE TODOS OS ARTIGO?" + CHR(13) + "ATEN��O - OP��O MUITO DEMORADA. N�O DEVE SER EXECUTADA DURANTE O FUNCIONAMENTO NORMAL DA FARM�CIA","Sim","N�o")
		
		lcMesesAtua = uf_gerais_getparameter_site('ADM0000000130', 'NUM', mysite)
		IF lcMesesAtua <= 0
			lcMesesAtua = -1900
		ELSE
			lcMesesAtua = lcMesesAtua  * -1	
		ENDIF
		
		IF uf_gerais_getParameter("ADM0000000301","BOOL")
			TEXT To lcSQL TEXTMERGE NOSHOW
				SELECT ref, ststamp FROM st (NOLOCK) where stns=0 order by st.ref
			ENDTEXT 
		ELSE
			TEXT To lcSQL TEXTMERGE NOSHOW
				SELECT ref, ststamp FROM st (NOLOCK) where site_nr=<<mySite_nr>> and stns=0 order by st.ref
			ENDTEXT 		
		ENDIF 
		uf_gerais_actGrelha("","ucrsListART",lcSQL)
		lcCont= reccount("ucrsListART")
		regua(0,lcCont,"A Atualizar artigos ... ")
		SELECT ucrsListART
		GO TOP 
		SCAN 
			regua(1,lcCont2,"A Atualizar Documentos... " + ALLTRIM(ucrsListART.ref))
			lcEPcpond = 0
			STORE .f. TO lcMovEntrada
			lcStock = 0
			lcQttMov = 0
			lcRef = ALLTRIM(ucrsListART.ref)
			lcStampSt = ALLTRIM(ucrsListART.ststamp)
			
			
			IF uf_gerais_getParameter("ADM0000000301","BOOL")
				TEXT To lcSQL TEXTMERGE NOSHOW
					SELECT sl.*, cm2.mudapcpond FROM sl (NOLOCK) 
					inner join cm2 (nolock) on sl.cm=cm2.cm 
					where ref='<<ALLTRIM(lcRef)>>'
						and sl.ousrdata >= DATEADD(month, <<lcMesesAtua>> ,GETDATE())
					order by sl.datalc, sl.ousrdata, sl.ousrhora
				ENDTEXT 
			ELSE
				TEXT To lcSQL TEXTMERGE NOSHOW
					SELECT sl.*, cm2.mudapcpond FROM sl (NOLOCK) 
					inner join cm2 (nolock) on sl.cm=cm2.cm 
					where ref='<<ALLTRIM(lcRef)>>' and armazem=<<mySite_nr>> 
						and sl.ousrdata >= DATEADD(month, <<lcMesesAtua>> ,GETDATE())
					order by sl.datalc, sl.ousrdata, sl.ousrhora
				ENDTEXT 		
			ENDIF 
*!*				IF uf_gerais_getParameter("ADM0000000301","BOOL")
*!*					TEXT To lcSQL TEXTMERGE NOSHOW
*!*						SELECT * FROM sl (NOLOCK) where ref='<<ALLTRIM(lcRef)>>' order by sl.datalc, sl.ousrdata, sl.ousrhora
*!*					ENDTEXT 
*!*				ELSE
*!*					TEXT To lcSQL TEXTMERGE NOSHOW
*!*						SELECT * FROM sl (NOLOCK) where ref='<<ALLTRIM(lcRef)>>' and armazem=<<mySite_nr>> order by sl.datalc, sl.ousrdata, sl.ousrhora
*!*					ENDTEXT 			
*!*				ENDIF 
			uf_gerais_actGrelha("","ucrsListMov",lcSQL)

			IF reccount("ucrsListMov") > 0
				** Fazer o rec�lculo e atualiza��o de valores
				SELECT ucrsListMov
				GO TOP 
				SCAN 
						
					** atualiza��o de movimentos da sl
					TEXT To lcSQL TEXTMERGE NOSHOW
						UPDATE sl SET epcpond=<<lcEPcpond>>, pcpond=<<ROUND(lcEPcpond*200.482,2)>> where slstamp='<<ALLTRIM(ucrsListMov.slstamp)>>'
					ENDTEXT 
					uf_gerais_actGrelha("","",lcSQL)

					SELECT ucrsListMov
					** atualiza��o de movimentos da fi
					IF !EMPTY(ucrsListMov.fistamp)
						TEXT To lcSQL TEXTMERGE NOSHOW
							UPDATE fi SET epcp=<<lcEPcpond>>, pcp=<<ROUND(lcEPcpond*200.482,2)>> where fistamp='<<ALLTRIM(ucrsListMov.fistamp)>>'
						ENDTEXT 
						uf_gerais_actGrelha("","",lcSQL)
					ENDIF 
					
					SELECT ucrsListMov
					** atualiza��o de movimentos da bi
					IF !EMPTY(ucrsListMov.bistamp)
						TEXT To lcSQL TEXTMERGE NOSHOW
							UPDATE bi SET epcusto=<<lcEPcpond>>, pcusto=<<ROUND(lcEPcpond*200.482,2)>> where bistamp='<<ALLTRIM(ucrsListMov.bistamp)>>' and ar2mazem=<<st.site_nr>>
						ENDTEXT 
						uf_gerais_actGrelha("","",lcSQL)
					ENDIF 
					
					SELECT ucrsListMov
					** atualiza��o de movimentos da fn
					IF !EMPTY(ucrsListMov.fnstamp)
						TEXT To lcSQL TEXTMERGE NOSHOW
							UPDATE fn SET qtt=qtt where fnstamp='<<ALLTRIM(ucrsListMov.fnstamp)>>'
						ENDTEXT 
						uf_gerais_actGrelha("","",lcSQL)
					ENDIF


					SELECT ucrsListMov
					
					**IF ALLTRIM(ucrsListMov.cmdesc)='V/Factura' OR ALLTRIM(ucrsListMov.cmdesc)='V/Vd. Dinhei.' OR ALLTRIM(ucrsListMov.cmdesc)='V/Vd. Dinhei.' OR ALLTRIM(ucrsListMov.cmdesc)='V/Guia Transp.'
					IF ucrsListMov.mudapcpond = .t.
						REPLACE ucrsListMov.EPCPOND WITH lcEPcpond
						IF ucrsListMov.cm < 50
							lcQttMov = ucrsListMov.qtt
						ELSE 
							lcQttMov = ucrsListMov.qtt * (-1)
						ENDIF 

						DO CASE 
							CASE (lcStock + lcQttMov ) <= 0 
								lcEPcpond = 0
							case (lcStock + lcQttMov ) > 0
								IF lcStock > 0 then
									lcEPcpond = ROUND(((lcStock * lcEPcpond)+(ucrsListMov.evu * ucrsListMov.qtt)) / (lcStock  + ucrsListMov.qtt),2)
								ELSE
									lcEPcpond = ROUND(((0 * lcEPcpond)+(ucrsListMov.evu * ucrsListMov.qtt)) / (0  + ucrsListMov.qtt),2)
								ENDIF 
							OTHERWISE
						ENDCASE 
						lcStock  = lcStock + lcQttMov
					ELSE
						REPLACE ucrsListMov.EPCPOND WITH lcEPcpond
						IF ucrsListMov.cm < 50 then
							lcStock = lcStock + ucrsListMov.qtt
						ELSE 
							lcStock = lcStock - ucrsListMov.qtt
						ENDIF 
					ENDIF 
					
					SELECT ucrsListMov
				ENDSCAN 
				IF uf_gerais_getParameter("ADM0000000301","BOOL")
					TEXT To lcSQL TEXTMERGE NOSHOW
						UPDATE 
							st 
						SET 
							epcpond=<<lcEPcpond>>
							, pcpond=<<ROUND(lcEPcpond*200.482,2)>>
							, marg4 = (case when st.epv1>0
								then ROUND( (st.epv1 / (taxasiva.taxa/100+1)) - <<lcEPcpond>> ,2)
  								ELSE 0 end )
  						from st
	  					left join taxasiva on taxasiva.codigo = st.tabiva
  						where 
  							ref='<<ALLTRIM(lcRef)>>'
					ENDTEXT 
				ELSE
					TEXT To lcSQL TEXTMERGE NOSHOW
						UPDATE 
							st 
						SET 
							epcpond=<<lcEPcpond>>
							, pcpond=<<ROUND(lcEPcpond*200.482,2)>>
							, marg4 = (case when st.epv1>0
								then ROUND( (st.epv1 / (taxasiva.taxa/100+1)) - <<lcEPcpond>> ,2)
  								ELSE 0 end)
  						from st
	  					left join taxasiva on taxasiva.codigo = st.tabiva
  						where
  							ref='<<ALLTRIM(lcRef)>>' 
  							and site_nr=<<mySite_nr>>
					ENDTEXT 				
				ENDIF 
				uf_gerais_actGrelha("","",lcSQL)
				
			ENDIF 

			IF USED("ucrsListMov")
				fecha("ucrsListMov")
			ENDIF
			lcCont2 = lcCont2 + 1			
			SELECT ucrsListART
		ENDSCAN 

		IF USED("ucrsListART")
			fecha("ucrsListART")
		ENDIF
		uf_perguntalt_chama("PROCESSO CONCLU�DO COM SUCESSO.", "Ok", "", 64)
		regua(2)

	ELSE
		uf_perguntalt_chama("PROCESSO CANCELADO.", "Ok", "", 16)
	ENDIF 
ENDFUNC 


FUNCTION uf_stocks_detalhest_verificaEcra_precosSPMS
	LPARAMETERS lcCursor
	
	LOCAL lcReferencia
	STORE '' TO lcReferencia	
	DO CASE
		CASE lcCursor = "ST"
			IF USED("ucrsMergeSearchStocks")
				SELECT ucrsMergeSearchStocks
				DELETE ALL
			ENDIF		
			
		    IF !USED("ST")
		      uf_perguntalt_chama("Ocorreu a obter os dados dos produtos. Por favor contacte o suporte.", "OK", "", 32)
		      RETURN .F.
		    ENDIF
		    SELECT ST
		    lcReferencia = ALLTRIM(st.ref)	    
		    IF EMPTY(lcReferencia)
		        uf_perguntalt_chama("Referencia nao encontrada. Por favor contacte o suporte", "Ok", "", 16)
		    	RETURN .F.
    		ENDIF 
    		   IF !uf_stocks_detalhest_precosSpms(lcReferencia) 		   
    		   		uf_servicos_pricesDem_closeCursors()    		   			   		
    		   		RETURN .F.
    		   ENDIF
		OTHERWISE
			RETURN .F.
	ENDCASE
	
ENDFUNC


FUNCTION uf_stocks_detalhest_precosSpms
	LPARAMETERS lcRef

	LOCAL lcStocksToken
	STORE '' TO lcStocksToken
	
	uf_servicos_pricesDem_closeCursors()
	
	IF EMPTY(lcRef)
		uf_perguntalt_chama("Referencia n�o encontrada. Por favor contacte o suporte", "Ok", "", 16)
		RETURN .F.
	ENDIF	
	
	lcStocksToken = uf_gerais_gerarIdentifiers(36)
	 
	IF uf_servicos_pricesDem_createSend(lcStocksToken,lcRef)
		IF !uf_servicos_pricesDem(lcStocksToken)
			RETURN .F.
		ENDIF
	ELSE
		uf_servicos_pricesDem_closeCursors()
		RETURN .F.
	ENDIF
	
	IF USED("ucrsMergeSearch") AND RECCOUNT("ucrsMergeSearch")>0
	
		LOCAL loGrid ,lcCursor

		loGrid = stocks.pageframe1.page1.detalhest1.pageframe1.page16.gridspms
	    lcCursor = "ucrsMergeSearchStocks"

		lnColumns = loGrid.ColumnCount
		
		IF lnColumns > 0
		   DIMENSION laControlSource[lnColumns]
		   FOR lnColumn = 1 TO lnColumns
			   laControlSource[lnColumn] = loGrid.Columns[lnColumn].ControlSource
		   ENDFOR

	    ENDIF

	    loGrid.RecordSource = ""

		 INSERT INTO ucrsMergeSearchStocks SELECT * FROM ucrsMergeSearch 
	 	
		 SELECT ucrsMergeSearchStocks 
		 GO TOP
		 
		   loGrid.RecordSource = lcCursor
	
		   FOR lnColumn = 1 TO lnColumns
	   		loGrid.Columns[lnColumn].ControlSource = laControlSource[lnColumn]
		   ENDFOR

		 	stocks.pageframe1.page1.detalhest1.refresh
		 
		ENDIF 
	
	RETURN .T.
ENDFUNC

FUNCTION uf_stocks_detalhest_cleanCursorsPrecosSPMS
	
	IF (uf_gerais_getParameter_site("ADM0000000214","BOOL") == .f.)
		RETURN .F.
	ENDIF
	
    IF !USED("ucrsMergeSearchStocks")
		LOCAL lcSqlResponse
	 	STORE '' TO lcSqlResponse
	 	
	 	TEXT TO lcSqlResponse NOSHOW textmerge
			exec up_get_responseSearchPrice ''
		ENDTEXT
		IF !uf_gerais_actgrelha("", "ucrsResponseSearchPrice", lcSqlResponse)
			uf_perguntalt_chama("Ocorreu uma anomalia a criar o cursor Response Search Price", "OK",16)
			RETURN .f.
		ENDIF
		
		LOCAL lcSqlResponsePvp
	 	STORE '' TO lcSqlResponsePvp
	 	
	 	TEXT TO lcSqlResponsePvp NOSHOW textmerge
			exec up_get_searchPricePvp ''
		ENDTEXT
		IF !uf_gerais_actgrelha("", "ucrsSearchPricePvp", lcSqlResponsePvp)
			uf_perguntalt_chama("Ocorreu uma anomalia a criar o cursor de PVP. Por favor contactar o suporte.", "OK",16)
			RETURN .f.
		ENDIF
		
		uf_servicos_pricesDem_mergeCursor()
	
		SELECT * FROM ucrsMergeSearch  INTO CURSOR ucrsMergeSearchStocks readwrite

    ELSE
        SELECT ucrsMergeSearchStocks
        DELETE ALL
    ENDIF 
ENDFUNC

FUNCTION uf_stocks_validarMotivoIsencao

	SELECT ST
	IF st.iva == 0
	
		IF EMPTY(st.codmotiseimp) OR EMPTY(st.motiseimp)

			IF uf_perguntalt_chama("N�o selecionou o motivo de isen��o. Pretende colocar por defeito o da empresa?","Sim","N�o")
		           
	           	fecha("ucrsCodMotivoInsencao")
				LOCAL lcSQLCodMotivo
				lcSQLCodMotivo = ""
				TEXT TO lcSQLCodMotivo TEXTMERGE NOSHOW
					SELECT code_motive, campo FROM b_multidata (NOLOCK) WHERE tipo = 'motiseimp' AND campo='<<ALLTRIM(uCrsE1.motivo_isencao_iva)>>'
				ENDTEXT
				IF !uf_gerais_actGrelha("", "ucrsCodMotivoInsencao", lcSQLCodMotivo)
					uf_perguntalt_chama("N�o foi possivel encontrar regitos do motivo de insen��o.", "", "OK", 16)
					RETURN .f.
				ENDIF
				
				IF RECCOUNT("ucrsCodMotivoInsencao") = 0
					uf_perguntalt_chama("O motivo de exen��o da empresa n�o se encontra preenchido. Por favor preencha para depois poder proceder a esta altera��o.", "", "OK", 16)
					RETURN .f.
				ENDIF
				
				SELECT ucrsCodMotivoInsencao
				REPLACE st.codmotiseimp WITH ALLTRIM(ucrsCodMotivoInsencao.code_motive)
				REPLACE st.motiseimp	WITH ALLTRIM(ucrsCodMotivoInsencao.campo)
				fecha("ucrsCodMotivoInsencao")
	           
			ELSE
				RETURN .F.
	        ENDIF
		ENDIF
	
	ENDIF		
	RETURN .T.
ENDFUNC

FUNCTION uf_stocks_codigoMotivoInsencao
	LPARAMETERS lcsitenr, lcSite

	fecha("ucrsCodMotivoInsencao")
	LOCAL lcSQLCodMotivo
	lcSQLCodMotivo = ""
	TEXT TO lcSQLCodMotivo TEXTMERGE NOSHOW
		select codmotiseimp,motivo_isencao_iva from empresa(nolock) WHERE (no = <<lcsitenr>> or site='<<lcSite>>') 
	ENDTEXT
	IF !uf_gerais_actGrelha("", "ucrsCodMotivoInsencao", lcSQLCodMotivo)
		uf_perguntalt_chama("N�o foi possivel encontrar regitos do motivo de insen��o.", "", "OK", 16)
		RETURN .f.
	ENDIF
	
	IF RECCOUNT("ucrsCodMotivoInsencao") = 0
		uf_perguntalt_chama("O motivo de exen��o da empresa n�o se encontra preenchido. Por favor preencha para depois poder proceder a esta altera��o.", "", "OK", 16)
		RETURN .f.
	ENDIF
	
	SELECT ST
	REPLACE st.codmotiseimp WITH ALLTRIM(ucrsCodMotivoInsencao.codmotiseimp)
	REPLACE st.motiseimp WITH ALLTRIM(ucrsCodMotivoInsencao.motivo_isencao_iva)
	fecha("ucrsCodMotivoInsencao")

ENDFUNC

FUNCTION uf_stocks_controlaSeVoltaChamarPrecosSpms
	IF TYPE("Stocks") = "O"
		IF TYPE("Stocks.PageFrame1.Page1.Detalhest1.Pageframe1.Page16") = "O"
			IF Stocks.PageFrame1.Page1.Detalhest1.Pageframe1.ActivePage = Stocks.PageFrame1.Page1.Detalhest1.Pageframe1.Page16.PageOrder
			   	uf_servicos_pricesDem_closeCursors()
				uf_stocks_detalhest_verificaEcra_precosSPMS('ST')
			ENDIF
		ENDIF
	ENDIF
ENDFUNC