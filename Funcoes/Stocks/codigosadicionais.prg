**
FUNCTION uf_CodigosAdicionais_chama
	
	&& Cursor de C�digos Adicionais
	IF !USED("st")
		RETURN .f.
	ENDIF
	SELECT st
		lcSQL = ""
		TEXT TO lcSQL TEXTMERGE NOSHOW
			SELECT * FROM bc WHERE ref = '<<ALLTRIM(st.ref)>>' AND site_nr = <<mysite_nr>>
		ENDTEXT
		IF !uf_gerais_actGrelha("","uCrsCodAdicionais",lcSQL)
			MESSAGEBOX("N�o foi poss�vel verificar os c�digos adicionais do produto. Por favor contacte o suporte.",48,"LOGITOOLS SOFTWARE")
			RETURN .f.
		ENDIF
	
	&& Controla abertura do Painel
	if type("Codigos_Adicionais")=="U"
		DO FORM Codigos_Adicionais
	ELSE
		Codigos_Adicionais.show
	Endif
ENDFUNC


**
FUNCTION uf_Codigos_Adicionais_sair
		
	IF USED("uCrsCodAdicionais")
		Fecha("uCrsCodAdicionais")
	ENDIF

	Codigos_Adicionais.hide
	Codigos_Adicionais.Release
		
	RELEASE Codigos_Adicionais
ENDFUNC


** Alterar C�digos Alternativos
FUNCTION uf_CodigosAdicionais_alterar
	Local lcCodigo,lcStamp,lcSql
	Store "" To lcCodigo,lcSQL,lcStamp
	
	lcStamp = Codigos_Adicionais.list1.value

	
	
	IF EMPTY(lcStamp)
		uf_perguntalt_chama("POR FAVOR SELECCIONE UMA REFER�NCIA.","OK","",48)
		RETURN .f.
	ENDIF

	uv_lojas = ALLTRIM(CODIGOS_ADICIONAIS.txtLocal.Value)

	IF EMPTY(uv_lojas)
		uf_perguntalt_chama("ATEN��O: TEM DE SELECIONAR A LOJA!","OK","",48)	
		RETURN .F.
	ENDIF
	
	**lcCodigo = INPUTBOX("Preencha a refer�ncia:", "", "")

	uv_cod = ""

	uf_tecladoalpha_chama("uv_cod", "INTRODUZA A REFER�NCIA DO PRODUTO:", .f., .f., 0)
	
	lcCodigo =  strtran(uv_cod, chr(39), '')
		
	&& Valida��es
	&& Ref vazia
	If Empty(lcCodigo)
		uf_perguntalt_chama("A REFER�NCIA N�O PODE SER VAZIA!","OK","",48)
		FECHA("uCrstempRef")
		return		
	ENDIF
	
	&& se cod alternativo � maior max
	IF LEN(lcCodigo)>18
		uf_perguntalt_chama("O limite m�ximo de caracteres admitido para cada um dos c�digos alternativos � de (18), por favor retifique.","OK", "", 48)
		RETURN .f.
	ENDIF
	
	IF uf_gerais_getParameter("ADM0000000350", "BOOL")
	
		uv_regras = uf_gerais_getParameter("ADM0000000350", "TEXT")
		
		ALINES(aWord, uv_regras, ';')

		FOR i=1 TO ALEN(aWord)

			IF ALLTRIM(lcCodigo) == ALLTRIM(aWord(i)) AND !EMPTY(ALLTRIM(aWord(i)))
			
				uf_perguntalt_chama("N�o pode utilizar esse c�digo como refer�ncia.", "OK", "", 48)
				RETURN .F.
			
			ENDIF

		NEXT
	
	ENDIF
	
	&& Ref repetida na ST
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE
		select top 1 * from ST (nolock) where st.ref='<<ALLTRIM(lcCodigo)>>' AND st.site_nr in (select no from empresa(nolock) where site in ('<<ALLTRIM(STRTRAN(uv_lojas, ',', "','"))>>'))
	ENDTEXT
	uf_gerais_actGrelha("","uCrstempRef",lcSQL)	
	IF RECCOUNT()>0
		uf_perguntalt_chama("ATEN��O: J� EXISTE UM ARTIGO COM A MESMA REFER�NCIA!","OK","",48)	
		FECHA("uCrstempRef")
		return
	ENDIF
	IF USED("uCrstempRef")
		FECHA("uCrstempRef")
	ENDIF	
		
	&& Ref repetida na BC
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE
		select top 1 * from bc (nolock) where codigo='<<ALLTRIM(lcCodigo)>>' AND bc.site_nr in (select no from empresa(nolock) where site in ('<<ALLTRIM(STRTRAN(uv_lojas, ',', "','"))>>'))
	ENDTEXT
	uf_gerais_actGrelha("","uCrsTempRefBc",lcSQL)	
	IF RECCOUNT() > 0
		uf_perguntalt_chama("ATEN��O: J� EXISTE UM ARTIGO COM A MESMA REFER�NCIA ALTERNATIVA!","OK","",48)	
		RETURN .f.
	ENDIF
	IF USED("uCrsTempRefBc")
		FECHA("uCrsTempRefBc")
	ENDIF
				
	If uf_perguntalt_chama("Vai alterar a refer�ncia para " + Alltrim(lcCodigo) + ". Pretende continuar?","Sim","N�o")
		Text To lcSQL Textmerge Noshow
			Update 
				BC
			Set 
				codigo	= '<<ALLTRIM(lcCodigo)>>',
				usrinis = '<<m_chinis>>',
				usrdata = convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102),
				usrhora	= convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108)
			Where 
				ref = (select ref from bc(nolock) where bcstamp = '<<Alltrim(lcStamp)>>')
				and codigo = (select codigo from bc(nolock) where bcstamp = '<<Alltrim(lcStamp)>>')
				and site_nr in (select no from empresa(nolock) where site in ('<<ALLTRIM(STRTRAN(uv_lojas, ',', "','"))>>'))
		Endtext
		
		If uf_gerais_actGrelha("","",lcSql)
			Select uCrsCodAdicionais
			GO top
			scan
				IF uCrsCodAdicionais.BCstamp = lcStamp
					replace uCrsCodAdicionais.codigo	With alltrim(lcCodigo)
				endif
			endscan
			uf_perguntalt_chama("ALTERA��O EFECTUADA COM SUCESSO.","OK","",64)
			Codigos_Adicionais.list1.clear
			Codigos_Adicionais.list1.refresh
		ELSE 
			uf_perguntalt_chama("ERRO AO REALIZAR A GRAVA��O. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			RETURN .f.
		ENDIF 
	ENDIF 
ENDFUNC 


** Introduzir C�digos Alternativos
FUNCTION uf_CodigosAdicionais_introduzir

	Local lcCodigo,lcSql,lcStamp
	Store "" To lcCodigo,lcSQL,lcStamp
	
	**lcCodigo = INPUTBOX('Preencha a refer�ncia:', "", "")

	uv_lojas = ALLTRIM(CODIGOS_ADICIONAIS.txtLocal.value)

	IF EMPTY(uv_lojas)
		uf_perguntalt_chama("ATEN��O: TEM DE SELECIONAR A LOJA!","OK","",48)	
		RETURN .F.
	ENDIF
	
	PUBLIC myCodAlt
	STORE '' TO myCodAlt
	
	&& Alterado para permitir utilizar Scanner
	uf_tecladoalpha_chama("myCodAlt", "INTRODUZA A REFER�NCIA DO PRODUTO:", .f., .f., 0)
	
	lcCodigo =  strtran(myCodAlt, chr(39), '')
	
	&& Ref Vazia	
	IF EMPTY(lcCodigo)
		uf_perguntalt_chama("A REFER�NCIA N�O PODE SER VAZIA!","OK", "", 48)
		RETURN .f.
	ENDIF
	
	&& se cod alternativo � maior max
	IF LEN(lcCodigo)>18
		uf_perguntalt_chama("O limite m�ximo de caracteres admitido para cada um dos c�digos alternativos � de (18), por favor retifique.","OK", "", 48)
		RETURN .f.
	ENDIF
	
	IF uf_gerais_getParameter("ADM0000000350", "BOOL")
	
		uv_regras = uf_gerais_getParameter("ADM0000000350", "TEXT")
		
		ALINES(aWord, uv_regras, ';')

		FOR i=1 TO ALEN(aWord)

			IF ALLTRIM(lcCodigo) == ALLTRIM(aWord(i)) AND !EMPTY(ALLTRIM(aWord(i)))
			
				uf_perguntalt_chama("N�o pode utilizar esse c�digo como refer�ncia.", "OK", "", 48)
				RETURN .F.
			
			ENDIF

		NEXT
	
	ENDIF
		
	&& Ref repetida na ST
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE
		select top 1 * from ST (nolock) where st.ref='<<ALLTRIM(lcCodigo)>>' AND st.site_nr in (select no from empresa(nolock) where site in ('<<ALLTRIM(STRTRAN(uv_lojas, ',', "','"))>>')) AND st.inactivo = 0
	ENDTEXT
	uf_gerais_actGrelha("","uCrstempRef",lcSQL)	
	IF RECCOUNT()>0
		uf_perguntalt_chama("ATEN��O: J� EXISTE UM ARTIGO COM A MESMA REFER�NCIA!","OK","",48)	
		FECHA("uCrstempRef")
		RETURN 
	ENDIF
	IF USED("uCrstempRef")
		FECHA("uCrstempRef")
	ENDIF	
	
	&& Ref repetida na BC	
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE
		select top 1 * from bc (nolock) where codigo='<<ALLTRIM(lcCodigo)>>' AND bc.site_nr in (select no from empresa(nolock) where site in ('<<ALLTRIM(STRTRAN(uv_lojas, ',', "','"))>>'))
	ENDTEXT
	uf_gerais_actGrelha("","uCrsTempRefBc",lcSQL)	
	IF RECCOUNT() > 0
		uf_perguntalt_chama("ATEN��O: J� EXISTE UM ARTIGO COM A MESMA REFER�NCIA ALTERNATIVA!","OK","",48)	
		RETURN .f.
	ENDIF
	IF USED("uCrsTempRefBc")
		FECHA("uCrsTempRefBc")
	ENDIF	

	ALINES(ua_lojas, uv_lojas, .t., ',')

	uv_erro = .T.

	FOR i = 1 to ALEN(ua_lojas)

		uf_gerais_actGrelha("","uCrsStamp",[Select 'ADM'+left(newid(),22) as stamp])
		SELECT 	uCrsStamp
		lcStamp= uCrsStamp.stamp
		Fecha("uCrsStamp")		
	
		TEXT TO lcSQL TEXTMERGE NOSHOW 
			INSERT INTO 
				BC (BcStamp,ref,design,codigo, ststamp, ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora, site_nr)
			Values 
				(
					'<<lcStamp>>'
					,'<<ALLTRIM(Codigos_Adicionais.txtref.value)>>'
					,'<<ALLTRIM(Codigos_Adicionais.txtDesign.value)>>'
					,'<<lcCodigo>>'
					,'<<ALLTRIM(st.ststamp)>>'
					,'<<m_chinis>>'
					,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
					,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108)
					,'<<m_chinis>>'
					,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
					,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108)
					,(select no from empresa(nolock) where site = '<<ALLTRIM(ua_lojas(i))>>')
				)
		ENDTEXT 

		IF  uf_gerais_actGrelha("","",lcSql)
			SELECT uCrsCodAdicionais
			LOCATE FOR uCrsCodAdicionais.codigo = ALLTRIM(lcCodigo)
			IF !FOUND()
				Select uCrsCodAdicionais
				APPEND BLANK 
				replace uCrsCodAdicionais.BCstamp	With 	alltrim(lcStamp)
				replace uCrsCodAdicionais.codigo	With 	alltrim(lcCodigo)
				replace uCrsCodAdicionais.ref		With 	alltrim(Codigos_Adicionais.txtref.value)
			ENDIF
			uv_erro = .F.
		ELSE 
			uf_perguntalt_chama("ERRO AO REALIZAR A GRAVA��O. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			RETURN .f.
		ENDIF 

		IF !uv_erro
			uf_perguntalt_chama("ALTERA��O EFECTUADA COM SUCESSO.","OK","",64)
		ENDIF

	NEXT
		
		Codigos_Adicionais.list1.clear
		Codigos_Adicionais.list1.refresh
	**Endif
ENDFUNC 


** Eliminar C�digo Adicional
FUNCTION uf_CodigosAdicionais_eliminar
	Local lcStamp,lcSql
	Store "" To lcSQL,lcStamp
	
	lcStamp = Codigos_Adicionais.list1.value
	SELECT uCrsCodAdicionais

	uv_ref = ALLTRIM(CODIGOS_ADICIONAIS.txtRef.value)
	uv_lojas = ALLTRIM(CODIGOS_ADICIONAIS.txtLocal.value)

	IF EMPTY(uv_lojas)
		uf_perguntalt_chama("ATEN��O: TEM DE SELECIONAR A LOJA!","OK","",48)	
		RETURN .F.
	ENDIF
	
	IF EMPTY(lcStamp)
		uf_perguntalt_chama("POR FAVOR SELECCIONE UM REGISTO.","OK","",48)
		RETURN .f.
	endif
	
	If uf_perguntalt_chama("Tem a certeza que deseja eliminar a refer�ncia adicional?","Sim","N�o")	
		Text To lcSQL Textmerge Noshow
			DELETE from BC
			Where 
				
				ref = '<<ALLTRIM(uv_ref)>>'
				and codigo = (select codigo from bc(nolock) where bcstamp = '<<ALLTRIM(lcstamp)>>')
				and site_nr in (select no from empresa(nolock) where site in ('<<ALLTRIM(STRTRAN(uv_lojas, ',', "','"))>>'))
		Endtext
		
		If uf_gerais_actGrelha("","",lcSql)
			Select uCrsCodAdicionais
			DELETE from uCrsCodAdicionais	where uCrsCodAdicionais.BCstamp = lcStamp
			uf_perguntalt_chama("REGISTO ELIMINADO COM SUCESSO.","OK","",64)
		Else
			uf_perguntalt_chama("ERRO AO ELIMINAR O REGISTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			RETURN .f.
		ENDIF
		
		Codigos_Adicionais.list1.clear
		Codigos_Adicionais.list1.refresh
	Endif
ENDFUNC 
