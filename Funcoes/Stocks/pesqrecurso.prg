**
FUNCTION uf_pesqrecurso_Chama
	LPARAMETERS lcOrigem
	
	&& CONTROLA PERFIS DE ACESSO
	IF !uf_gerais_validaPermUser(ch_userno,ch_grupo,'Gest�o de Recursos')
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ACEDER AO PAINEL GEST�O DE RECURSOS.","OK","",48)
		RETURN .f.
	ENDIF
	
	IF TYPE("PESQRECURSO")=="U"
		IF !USED("uCrsPesqRecursos")
			TEXT TO lcSQL TEXTMERGE NOSHOW
				SET FMTONLY ON
				exec up_stocks_pesquisaRecursos '',0
				SET FMTONLY OFF
			ENDTEXT 
			IF !uf_gerais_actgrelha("", "uCrsPesqRecursos", lcSql)
				MESSAGEBOX("N�o foi possivel aplicar defini��es pesquisa. Contacte o suporte.",16,"LOGITOOLS SOFTWARE")
				RETURN .f.
			ENDIF
		ENDIF
		
		DO FORM PESQRECURSO
	ELSE
		PESQRECURSO.show
	ENDIF
	
	PESQRECURSO.origem = lcOrigem

ENDFUNC 


**
FUNCTION uf_pesqrecurso_carregaMenu
	PESQRECURSO.menu1.adicionaOpcao("actualizar", "Actualizar", myPath + "\imagens\icons\actualizar_w.png", "uf_pesqrecurso_actPesquisa","A")
	PESQRECURSO.menu1.adicionaOpcao("limpar", "Limpar", myPath + "\imagens\icons\limpar_w.png", "uf_pesqrecurso_limparDados","L")
	PESQRECURSO.menu1.adicionaOpcao("subir", "Subir", myPath + "\imagens\icons\ponta_seta_up.png", "uf_gerais_MovePage with .t.,'pesqrecurso.gridpesq','uCrsPesqRecursos'")
	PESQRECURSO.menu1.adicionaOpcao("descer", "Descer", myPath + "\imagens\icons\ponta_seta_down.png", "uf_gerais_MovePage with .f.,'pesqrecurso.gridpesq','uCrsPesqRecursos'")
ENDFUNC


**
FUNCTION uf_pesqrecurso_actPesquisa

	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_stocks_pesquisaRecursos '<<ALLTRIM(pesqrecurso.nome.value)>>',<<IIF(EMPTY(ALLTRIM(pesqrecurso.no.value)),0,ALLTRIM(pesqrecurso.no.value))>>
	ENDTEXT 
	IF !uf_gerais_actgrelha("pesqrecurso.gridPesq", "uCrsPesqRecursos", lcSql)
		MESSAGEBOX("N�o foi possivel aplicar defini��es pesquisa. Contacte o suporte.",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF
	pesqrecurso.lblRegistos.caption = ALLTRIM(STR(RECCOUNT("uCrsPesqRecursos"))) + " Resultados"
	
	pesqrecurso.refresh
ENDFUNC


**
FUNCTION uf_pesqrecurso_limparDados
	pesqrecurso.nome.Value = ''
	uf_pesqrecurso_actPesquisa()
ENDFUNC 


**
FUNCTION uf_pesqrecurso_sel

	IF EMPTY(pesqrecurso.origem) && Chamado do painel de recursos
		SELECT uCrsPesqRecursos
		uf_recursos_chama(uCrsPesqRecursos.recursosstamp)
		RETURN .f.
	ENDIF	
	
	DO CASE
		CASE pesqrecurso.origem == "MARCACOES_EDIT"
		
			lcmrrecstamp = uf_gerais_getId(1)
			
			SELECT uCrsPesqRecursos
			SELECT ucrsMrRecursos
			APPEND BLANK
			Replace ucrsMrRecursos.nome WITH uCrsPesqRecursos.nome
			Replace ucrsMrRecursos.no WITH uCrsPesqRecursos.no
			Replace ucrsMrRecursos.recursosstamp WITH uCrsPesqRecursos.recursosstamp
			Replace ucrsMrRecursos.mrstamp WITH UcrsMarcacoesDia.mrstamp
			Replace ucrsMrRecursos.mrrecstamp WITH lcmrrecstamp
			
			marcacoes.refresh
		CASE pesqrecurso.origem == "SERIES"
			IF USED("ucrsSeriesServ")
				SELECT uCrsPesqRecursos
				SELECT ucrsSeriesServ
				REPLACE ucrsSeriesServ.nome WITH ALLTRIM(uCrsPesqRecursos.nome)
				REPLACE ucrsSeriesServ.no WITH uCrsPesqRecursos.no
				REPLACE ucrsSeriesServ.estab WITH 0
			ENDIF

			SERIESSERV.REFRESH
		
		OTHERWISE
			**
	ENDCASE
	
	uf_pesqrecurso_sair()
ENDFUNC


**
FUNCTION uf_pesqrecurso_sair
	fecha("uCrsPesqRecursos")
	
	pesqrecurso.hide
	pesqrecurso.release
ENDFUNC
