
**
FUNCTION uf_dependencias_gravar
	LPARAMETERS lcsite_nr, lcSite
	
	IF EMPTY(lcsite_nr)
		lcsite_nr  = mysite_nr
	ENDIF 
	IF EMPTY(lcSite)
		lcSite= mysite
	ENDIF 
		
	**
	uf_dependencias_insereRecursos(lcsite_nr, lcSite)
    
	uf_dependencias_insereServicos(lcsite_nr, lcSite)
    
	uf_dependencias_insereComponentes(lcsite_nr, lcSite)
    
	
ENDFUNC


&& Equipamentos
FUNCTION uf_dependencias_insereRecursos
	LPARAMETERS lcsite_nr, lcSite

	&& Recursos associados
	lcSQL = ""
	TEXT TO lcSQL NOSHOW textmerge
		DELETE FROM b_cli_stRecursos WHERE REF = '<<ALLTRIM(ST.REF)>>' AND site_nr = <<lcsite_nr>>
	ENDTEXT
	IF !uf_gerais_actGrelha("", "",lcSQL)
		MESSAGEBOX("DESCULPE, MAS OCORREU UM ERRO A ELIMINAR RECURSOS ASSOCIADOS. POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF		
	
	&& INSERE RECURSOS
	SELECT ucrsListaRecursosST
	GO TOP
	SCAN
	
		lcSQL = ""
		lcstamp = uf_gerais_getID(1)
		TEXT TO lcSQl TEXTMERGE NOSHOW
			INSERT INTO b_cli_stRecursos(
				strecstamp
				,ref
				,nome
				,no
				,stamp
				,duracao
				,ousrinis
				,ousrdata
				,ousrhora
				,tipo
				,site_nr
				,marcacao
				,pvp
				,mrsimultaneo
			)Values(
				'<<ALLTRIM(lcstamp)>>',
				'<<ALLTRIM(st.ref)>>',
				'<<ALLTRIM(ucrsListaRecursosST.nome)>>',
				<<ucrsListaRecursosST.no>>,
				'<<ALLTRIM(ucrsListaRecursosST.stamp)>>',
				<<ucrsListaRecursosST.duracao>>,
				'<<m.m_chinis>>',
				convert(nvarchar,dateadd(HOUR, <<difhoraria>>, getdate()),102),
				convert(nvarchar,dateadd(HOUR, <<difhoraria>>, getdate()),108),
				'<<ALLTRIM(ucrsListaRecursosST.tipo)>>'
				,<<lcsite_nr>>
				,<<IIF(ucrsListaRecursosST.marcacao,1,0)>>
				,<<ucrsListaRecursosST.pvp>>
				,<<ucrsListaRecursosST.mrsimultaneo>>
			)
		ENDTEXT
		IF !uf_gerais_actGrelha("", "",lcSQL)
			MESSAGEBOX("DESCULPE, MAS OCORREU UM ERRO A VERIFICAR DADOS DOS RECURSOS. POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
			RETURN .f.
		ENDIF
		
		SELECT ucrsListaRecursosST
	ENDSCAN
	
	RETURN .t.
ENDFUNC


** Servi�os
FUNCTION uf_dependencias_insereServicos
	LPARAMETERS lcsite_nr, lcSite

	**Trata Inser��o na tabela de US
	TEXT TO lcSQL NOSHOW textmerge
		DELETE FROM	b_stst WHERE reforiginal = '<<ALLTRIM(ST.REF)>>' AND site_nr = <<lcsite_nr>>
	ENDTEXT
	IF !uf_gerais_actgrelha("", "", lcSQL)
		MESSAGEBOX("DESCULPE, MAS OCORREU UM ERRO A ACTUALIZAR OS SUB-SERVICOS. POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		return .f.
	ENDIF 
	
	SELECT uCrsStSt
	IF RECCOUNT("uCrsStSt")>0
		SELECT uCrsStSt
		GO TOP
		SCAN
			lcStamp = uf_gerais_stamp()
			lcSQL = ""
			TEXT TO lcSQL NOSHOW TEXTMERGE
								
				INSERT INTO b_StSt
				(	stststamp
					,reforiginal
					,ref
					,qtt
					,duracao
					,site_nr 
				)
				VALUES	(		
					'<<ALLTRIM(lcStamp)>>'
					,'<<ALLTRIM(st.ref)>>'
					,'<<ALLTRIM(uCrsStSt.ref)>>'
					,<<uCrsStSt.QTT>>
					,<<uCrsStSt.duracao>>
					,<<lcsite_nr>>
				)
			ENDTEXT
			IF !uf_gerais_actgrelha("", "", lcSQL)
				MESSAGEBOX("OCORREU UM ERRO A ACTUALIZAR OS SUB-SERVICOS! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
				return .f.
			ENDIF
		ENDSCAN
	ENDIF
ENDFUNC


**Componentes
FUNCTION uf_dependencias_insereComponentes
	LPARAMETERS lcsite_nr, lcSite

	**Trata Inser��o na tabela de US
	TEXT TO lcSQL NOSHOW textmerge
		DELETE FROM	st_componentes WHERE ref_st = '<<ALLTRIM(ST.REF)>>' AND site_nr = <<lcsite_nr>>
	ENDTEXT
	IF !uf_gerais_actgrelha("", "", lcSQL)
		MESSAGEBOX("DESCULPE, MAS OCORREU UM ERRO A ACTUALIZAR OS COMPONENTES. POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		return .f.
	ENDIF 
	
	SELECT uCrsComponentes
	IF RECCOUNT("uCrsComponentes")>0
		SELECT uCrsComponentes
		GO TOP
		SCAN
			lcSQL = ""
			TEXT TO lcSQL NOSHOW TEXTMERGE
				INSERT INTO st_componentes(ref_st,ref,qtt,site_nr) VALUES ('<<ALLTRIM(uCrsComponentes.ref_st)>>','<<ALLTRIM(uCrsComponentes.ref)>>',<<uCrsComponentes.QTT>>,<<lcsite_nr>>)
			ENDTEXT
			IF !uf_gerais_actgrelha("", "", lcSQL)
				MESSAGEBOX("OCORREU UM ERRO A ACTUALIZAR OS COMPONENTES! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
				return .f.
			ENDIF
		ENDSCAN
	ENDIF
ENDFUNC



FUNCTION uf_dependencias_elimina
	**
	TEXT TO lcSQL NOSHOW textmerge
		DELETE FROM	b_stst WHERE ref = '<<ALLTRIM(ST.REF)>>' AND site_nr = <<mysite_nr>>
	ENDTEXT
	IF !uf_gerais_actgrelha("", "", lcSQL)
		MESSAGEBOX("DESCULPE, MAS OCORREU UM ERRO A ACTUALIZAR OS SUB-SERVICOS. POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		return .f.
	ENDIF 
	
	lcSQL = ""
	TEXT TO lcSQL NOSHOW textmerge
		DELETE FROM b_cli_stRecursos WHERE REF = '<<ALLTRIM(ST.REF)>>' AND site_nr = <<mysite_nr>>
	ENDTEXT
	IF !uf_gerais_actGrelha("", "",lcSQL)
		MESSAGEBOX("DESCULPE, MAS OCORREU UM ERRO A ELIMINAR RECURSOS ASSOCIADOS. POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF
	
	lcSQL = ""
	TEXT TO lcSQL NOSHOW textmerge
		DELETE FROM st_componentes WHERE ref_st = '<<ALLTRIM(ST.REF)>>' AND site_nr = <<mysite_nr>>
	ENDTEXT
	IF !uf_gerais_actGrelha("", "",lcSQL)
		MESSAGEBOX("DESCULPE, MAS OCORREU UM ERRO A ELIMINAR COMPONENTES. POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF
	
	
	**
*!*		TEXT TO lcSQL NOSHOW textmerge
*!*			DELETE from	b_StUs WHERE ref = '<<ALLTRIM(ST.REF)>>'
*!*		ENDTEXT
*!*		IF !uf_gerais_actgrelha("", "", lcSQL)
*!*			MESSAGEBOX("DESCULPE, MAS OCORREU UM ERRO A ACTUALIZAR OS EQUIPAMENTOS. POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
*!*			return .f.
*!*		ENDIF 
	&& 
	
	
	&& 
*!*		lcSQL = ""
*!*		TEXT TO lcSQL NOSHOW textmerge
*!*			DELETE FROM st_precos WHERE REF = '<<ALLTRIM(ST.REF)>>'
*!*		ENDTEXT
*!*		IF !uf_gerais_actGrelha("", "",lcSQL)
*!*			MESSAGEBOX("DESCULPE, MAS OCORREU UM ERRO A ELIMINAR PRE�OS ASSOCIADOS � REFERENCIA. POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
*!*			RETURN .f.
*!*		ENDIF
ENDFUNC
