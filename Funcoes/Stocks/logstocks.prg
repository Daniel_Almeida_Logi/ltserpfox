FUNCTION uf_LOGSTOCKS_chama
	LPARAMETERS lcStamp
		
	** cria cursor vazio
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		select * from user_logs WHERE 0=1 ORDER BY DATA
	ENDTEXT 
	IF !uf_gerais_actgrelha("", "uCrsLogST", lcSql)
		uf_perguntalt_chama("ERRO AO LER A TABELA DE LOGS!","OK","",16)
		RETURN .F.
	ENDIF 
	
	&& Controla Abertura do Painel			
	if type("LOGSTOCKS")=="U"
		DO FORM LOGSTOCKS
	ELSE
		LOGSTOCKS.show
	ENDIF
	
	** carrega dados
	TEXT TO lcSQL TEXTMERGE NOSHOW
		select * from user_logs WHERE REGSTAMP='<<lcStamp>>' AND [table]='ST' ORDER BY DATA
	ENDTEXT 	
	IF !uf_gerais_actgrelha("logSTOCKS.GridPesq", "uCrsLogST", lcSql)
		uf_perguntalt_chama("ERRO AO LER A TABELA DE LOGS!","OK","",16)
		RETURN .F.
	ENDIF 
	
ENDFUNC


**
FUNCTION uf_LOGSTOCKS_carregamenu
	logSTOCKS.menu1.adicionaopcao("atualizar", "Atualizar", myPath + "\imagens\icons\actualizar_w.png", "uf_LOGSTOCKS_Pesquisa", "F2")
ENDFUNC


**
FUNCTION uf_LOGSTOCKS_Pesquisa
	LPARAMETERS lcStamp
	LOCAL lcSQL
	
	regua(0,0,"A atualizar...")
	
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		select * from user_logs WHERE REGSTAMP='<<lcStamp>>' AND [table]='ST' ORDER BY DATA
	ENDTEXT

	uf_gerais_actGrelha("LOGSTOCKS.GridPesq", "ucrsLogST", lcSQL)

	regua(2)
	
	LOGSTOCKS.Refresh
	
ENDFUNC

**
FUNCTION uf_LOGSTOCKS_sair
	**Fecha Cursores
	IF USED("uCrsLogST")
		fecha("uCrsLogST")
	ENDIF 
	
	LOGSTOCKS.hide
	LOGSTOCKS.release
ENDFUNC