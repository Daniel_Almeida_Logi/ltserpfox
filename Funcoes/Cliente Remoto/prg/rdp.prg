LPARAMETERS lcModo, lcUtilizador, lcPassword

** Valida se o exe j� esta a correr
LOCAL lcEstado

IF uf_ExeRunning("LTS Software.exe")
	LOCAL lcVar 
	
	STORE '' TO lcVar
		
	TEXT TO lcVar NOSHOW
		Set WshShell = CreateObject("WScript.Shell")
		WshShell.AppActivate("Logitools Software")
		WshShell.SendKeys "{ENTER}"
		WshShell.SendKeys "(% )E"
	ENDTEXT 

	loScript = Createobject([MSScriptcontrol.scriptcontrol.1])
	loScript.Language = [VBScript]
	loScript.AddCode(lcVar)
	
	&& Fecha esta instancia do exe
	QUIT
ENDIF


** Valida se o computador tem internet
IF uf_ValidaLigacaoInternet() == 0
	MESSAGEBOX("Por favor verifique a sua liga��o � Internet.",48,"LOGITOOLS SOFTWARE")
	QUIT
ENDIF


*************************************
** Valida parametros
*************************************

*!*	DO CASE
*!*		CASE TYPE("lcMOdo")=="L" OR (TYPE("lcModo")=="C" AND (lcModo == "0"))
*!*			&& Verifica actualiza��es
*!*			Declare Integer ShellExecute In shell32;
*!*				INTEGER HWnd,;
*!*				STRING  lpOperation,;
*!*				STRING  lpFile,;
*!*				STRING  lpParameters,;
*!*				STRING  lpDirectory,;
*!*				INTEGER nShowCmd

*!*			lcparametros = ""
*!*			ShellExecute(0, "open", "update.exe", "", "",1)
*!*			
*!*			&& Fecha este EXE
*!*			QUIT
*!*		
*!*		CASE (TYPE("lcModo")=="C" AND (lcModo == "1"))			
*!*			***
*!*			
*!*		OTHERWISE
*!*			MESSAGEBOX("O par�metro n�o � valido!","Logitools Software",64)	
*!*			QUIT
*!*	ENDCASE


*******************************
** Define variaveis publicas
*******************************
PUBLIC ;
	myChavePrivada, ;	&& Chave para encripta��o
	myUtilizador, ;		&& Utilizador para conec��o RDP
	myPassword, ;		&& Password para conec��o RDP
	Altura, ;			&& Altura do ecra
	Comprimento, ;		&& Comprimento do ecra
	RDP, ;				&& Formulario de RDP
	Intro, ;			&& Formulario Intro
	Login,;				&& Formulario Login
	myAutoLogin			&& Auto Login RDP
	
myChavePrivada = "hYO&a.sTvs#@xXvwr7J2"	

	
*******************************
** Define Librarias
*******************************
SET CLASSLIB TO registry.vcx ADDITIVE
SET LIBRARY TO (LOCFILE("vfpencryption.fll")) && or vfpencryption71.fll



*!*	*************************************************
*!*	Configura��es gerais do Visual Fox na aplica��o
*!*	*************************************************

&& Configura��es gerais
SET SYSMENU TO DEFA
SET RESOURCE OFF 
SET DELETED ON
SET TALK OFF
SET NOTIFY OFF
SET SYSMENU OFF
SET ESCAPE OFF
SET SAFETY OFF
SET CONSOLE OFF
SET DATE TO YMD
SET POINT TO "."
HIDE MENU _MSYSMENU
SET STATUS BAR OFF
SET TALK OFF

&& Associa eventos
ON SHUTDOWN uf_sair()

&& Configura ecra
_screen.WindowState = 2
Comprimento = _screen.width

&& Configura ecra
&& Aparencia
_screen.caption = "Logitools Software"
_screen.Icon = 'logitools.ico'
_screen.TitleBar = 1
_screen.ControlBox = .F.
_screen.BorderStyle = 1
_screen.Visible = .T.
_screen.WindowState = 2
_screen.Refresh()

Altura = _screen.Height &&+ 25
Comprimento = _screen.width

&& Aparencia
_screen.height = 175
_screen.width = 270
_screen.Resize()
_screen.Refresh()
_screen.AutoCenter = .T.


*************************************
** Define tratamento de erros
*************************************
ON ERROR DO ErrorHandler WITH ERROR( ), MESSAGE( ), MESSAGE(1), PROGRAM( ), LINENO(1)


*******************************
** Carrega configura��es
*******************************
uf_Config()

************************
** Cria Formularios
************************
IF EMPTY(myUtilizador) OR EMPTY(myPassword) OR EMPTY(myAutoLogin)
	LOGIN = NEWOBJECT("LOGIN")
	LOGIN.SHOW
ELSE
	&& resize do ecra para intro
	_screen.TitleBar = 0
	_screen.Visible = .F.
	_screen.height = 250
	_screen.width = 400
	_screen.ControlBox = .T.
	_screen.refresh()
	_screen.Visible = .T.
ENDIF

&& Cria form de RDP e vai criando a liga��o RDP em BackGround
RDP = NEWOBJECT("RDP")

&& Cria form com anima��o de intro
_screen.Refresh()
INTRO = NEWOBJECT("INTRO")
INTRO.SHOW()
_screen.Visible = .T.


************************
** P�ra de processar
************************
READ EVENTS

** Cria Classe para o formulario de LOGIN
DEFINE CLASS LOGIN AS Form
Height = _screen.Height
Width = _screen.width
AutoCenter = .T.
AlwaysOnTop = .T.
BorderStyle = 0
ColorSource = 4
ControlBox = .F.
ShowWindow = 0
WindowState = 0
TitleBar = 0
BorderStyle = 0
Height = 175
Width = 270
Caption = "Logitools Software"
AllowOutput = .F.
icon = 'logitools.ico'
visible = .T.
WindowType = 1	
	
ADD OBJECT lblUtilizador AS Label WITH ;
	AutoSize = .T., ;
	visible = .T., ;
	BackStyle = 0, ;
	Caption = "Utilizador", ;
	Height = 17, ;
	Width = 53, ;
	Top = 16, ;
	Left = 17 

ADD OBJECT lblPassword AS Label WITH ;
	AutoSize = .T., ;
	visible = .T., ;
	BackStyle = 0, ;
	Caption = "Password", ;
	Height = 17, ;
	Width = 53, ;
	Top = 44, ;
	Left = 12

ADD OBJECT txtUtilizador AS TextBox WITH ;
	Enabled = .T., ;
	visible = .T., ;
	Height = 25, ;
	Width = 180, ;
	Top = 12, ;
	Left = 75, ;
	TabIndex = 1, ;
	TabStop = .T., ;
	Value = myUtilizador

ADD OBJECT txtPassword AS TextBox WITH ;
	Enabled = .T., ;
	visible = .T., ;
	Height = 25, ;
	Width = 180, ;
	Top = 41, ;
	Left = 75, ;
	PasswordChar = "*", ;
	TabIndex = 2, ;
	TabStop = .T., ;
	Value = myPassword
	
ADD OBJECT chkMemorizar AS CheckBox WITH ;
	enabled = .T., ;
	visible = .T., ;
	AutoSize = .T., ;
	BackStyle = 0, ;
	Caption = "Memorizar credenciais", ;
	Height = 17, ;
	Width = 142, ;
	Top = 72, ;
	Left = 75, ;
	TabIndex = 3, ;
	TabStop = .T., ;
	Value = 1

ADD OBJECT chkAutoLogin AS CheckBox WITH ;
	enabled = .T., ;
	visible = .T., ;
	AutoSize = .T., ;
	BackStyle = 0, ;
	Caption = "Login Autom�tico", ;
	Height = 17, ;
	Width = 142, ;
	Top = 90, ;
	Left = 75, ;
	TabIndex = 3, ;
	TabStop = .T., ;
	Value = 0

ADD OBJECT cmbConfirmar AS CommandButton WITH ;
	enabled = .T., ;
	visible = .T., ;
	Height = 36, ;
	Width = 100, ;
	Top = 110, ;
	Left = 27,;
	TabIndex = 4, ;
	TabStop = .T., ;
	Caption = "Confirmar"
	
ADD OBJECT cmbCancelar AS CommandButton WITH ;
	enabled = .T., ;
	visible = .T., ;
	Height = 36, ;
	Width = 100, ;
	Top = 110, ;
	Left = 135,;
	TabIndex = 5, ;
	TabStop = .T., ;
	Caption = "Cancelar"

&& Evento KeyPress no Formulario
PROCEDURE Keypress
	LPARAMETERS nKeyCode, nShiftAltCtrl
		
	IF nKeyCode == 13 && enter
		Thisform.cmbConfirmar.Click
	ENDIF 
ENDPROC

&& Evento Bot�o Confirmar
PROCEDURE cmbConfirmar.Click
		
	&& Valida Preenchimento
	IF LEN(ALLTRIM(THISFORM.txtUtilizador.value)) = 0
		MESSAGEBOX("O nome de utilizador n�o pode ser vazio.","LOGITOOLS SOFTWARE",32)
		RETURN
	ENDIF
	
	IF LEN(ALLTRIM(THISFORM.txtPassword.value)) = 0
		MESSAGEBOX("A password n�o pode ser vazia.","LOGITOOLS SOFTWARE",32)
		RETURN
	ENDIF
		
	&& Escreve no Ficheiro INI
	uf_FicheiroINI(ALLTRIM(THISFORM.txtUtilizador.value),ALLTRIM(THISFORM.txtPassword.value),THISFORM.chkMemorizar.value,THISFORM.chkAutoLogin.value)
	**uf_FicheiroINI(ALLTRIM(THISFORM.txtUtilizador.value),ALLTRIM(THISFORM.txtPassword.value),THISFORM.chkMemorizar.value)
	
	&& Atribui User e Pass as variaveis publicas
	myUtilizador = ALLTRIM(THISFORM.txtUtilizador.value)
	myPassword = ALLTRIM(THISFORM.txtPassword.value)
	
	&& resize do ecra para intro
	_screen.TitleBar = 0
	_screen.Visible = .F.
	_screen.height = 250
	_screen.width = 400
	_screen.ControlBox = .T.
	_screen.refresh()
	
	&& Fecha o formulario
	thisform.Hide()
	Thisform.Release()
	_screen.Visible = .T.
ENDPROC

&& Evento Bot�o Cancelar
PROCEDURE cmbCancelar.Click
	QUIT
ENDPROC
	
ENDDEFINE


*****************************************
** Cria Classe para o formulario de INTRO
*****************************************
DEFINE CLASS INTRO AS FORM
	Height = _screen.Height
	Width = _screen.width
	AutoCenter = .T.
	BorderStyle = 0
	ColorSource = 4
	ControlBox = .F.
	ShowWindow = 0
	WindowState = 0
	TitleBar = 0
	BorderStyle = 0
	Height = 250
	Width = 400
	Caption = "Logitools Software"
	AllowOutput = .T.
	rdpcount = 0
	rdpusernames = ""
	rdppasswords = ""
	rdpservers = ""
	Name = "Intro"
	icon = 'logitools.ico'
	visible = .T.
		
	ADD OBJECT image1 AS image WITH ;
		BackStyle = 0, ;
		top = 2, ;
		left = 0, ;
		height = 200, ;
		width = 400, ;
		picture = 'logitools_logo.png', ;
		stretch = 1
	
	ADD OBJECT barra AS Shape WITH ;
		Top = 210, ;
		Left = 8, ;
		Height = 36, ;
		Width = 1, ;
		BackColor = RGB(42,144,155), ;
		BorderStyle = 1, ;
		SpecialEffect = 2, ;
		Name = "Barra"
	
	&& Adiciona timer
	ADD OBJECT timer1 AS timer WITH ;
		enabled = .T., ;
		interval = 20000
	
	ADD OBJECT timer2 AS timer WITH ;
		enabled = .T., ;
		interval = 150
	
	PROCEDURE Destroy	
		&& Reconfigura o Ecra
		_screen.TitleBar = 1
		_screen.Visible = .F.
		_screen.height = altura
		_screen.width = comprimento
		_screen.BorderStyle = 3
		_screen.AutoCenter = .F.
		_screen.WindowState = 2
		_screen.Resize()
		_screen.Refresh()	
		_screen.AlwaysOnTop = .t.

		&& Coloca o painel de RDP Visivel
		RDP.visible = .T.
		_screen.AlwaysOnTop = .f.
		
		&& Fecha o formulario
		thisform.hide
		thisform.release
		_screen.Visible = .T.
	ENDPROC
	
	&& Evento Timer 1
	PROCEDURE Timer1.TIMER 
		THISFORM.DESTROY
	ENDPROC
	
	&& Evento Timer 2
	PROCEDURE Timer2.TIMER 
		IF THISFORM.Barra.width <= 389
			THISFORM.Barra.width = THISFORM.Barra.Width + 3
		ELSE
			This.Enabled = .F.
		ENDIF
	ENDPROC
	
ENDDEFINE


*****************************************
** Cria Classe para o formulario de RDP
*****************************************
DEFINE CLASS RDP AS FORM

	Height = _screen.Height
	Width = _screen.width
	*AutoCenter = .T.
	BorderStyle = 0
	icon = 'logitools.ico'
	Caption = "Logitools Software"
	TitleBar = 0
	
	height = Altura
	width = Comprimento
	rdpcount = 0
	rdpusernames = ""
	rdppasswords = ""
	rdpservers = ""
	Name = "RDP"
	visible = .F.
		
	&& Adiciona timer
	ADD OBJECT RDPtimer AS timer WITH ;
		enabled = .T., ;
		interval = 5000

	PROCEDURE init
		
		Local ;
		lFound, ;
		lobject, ;
		lcVersion, ;
		lnx

		m.lFound = .F.
		
		For lnx = 9 To 3 Step -1

			Try
				m.lobject = Createobject('MSTSCAX.MSTSCAX.' + Transform(m.lnx))
				m.lFound = .T.
			CATCH
			ENDTRY
			
			If m.lFound = .T. Then
				EXIT
			ENDIF

		ENDFOR
		
		m.lobject = Null
		m.lcVersion = Transform(m.lnx)
			
		&& Reconfigura o tamanho da janela (para o controlo de rdp funcionar correctamente)
		thisform.windowstate = 2
		thisform.resize()	

		&& Adiciona Olecontrol por codigo (no XP so funciona adicionando assim)
		thisform.AddObject ('oRDP','olecontrol','MSTSCAX.MSTSCAX.' + m.lcVersion)		
			
		With thisform.oRDP
			.left = 1
			.top = 1
			.width = thisform.width
			.height = thisform.height 
			.visible = .T.
		ENDWITH

		*********************************
		** Establece liga��o RDP
		*********************************
	
		WITH thisform.oRDP
			&& Dados de login
				.Server = 'LTSAPP.LOGITOOLS.PT'
				.colorDepth = 16
				.UserName = 'Logitools\' + myUtilizador
				.AdvancedSettings2.ClearTextPassword = myPassword
				.AdvancedSettings2.RDPPort = 33890

			&& Devices	
				.AdvancedSettings2.RedirectPrinters = 1
				.AdvancedSettings2.RedirectDrives = 1
				.AdvancedSettings2.RedirectPorts = 1
				.AdvancedSettings2.RedirectPosDevices = 1
				.AdvancedSettings2.RedirectSmartCards = 0
				** .AdvancedSettings2.RedirectDirectX = 0 &&Este parametro n�o � usado, ver: htt*s://msdn.microsoft.com/en-us/library/ee338625%28v=vs.85%29.aspx
			
			&& Performance
				.AdvancedSettings2.Compress = 1
				.AdvancedSettings2.AcceleratorPassthrough = 1
				.AdvancedSettings2.BitmapPersistence = 1
				.AdvancedSettings2.ShadowBitmap = 1
				.AdvancedSettings2.AudioRedirectionMode = 0
				
			&&Melhorias Performance
				.AdvancedSettings2.PerformanceFlags=256 &&h*tps://helpdesk.stone-ware.com/portal/helpcenter/articles/rdp-settings
			
			&& TsPrint
				.AdvancedSettings2.PluginDlls = "TsPrint.dll"
			
			&& Auto Reconnect
				.AdvancedSettings2.EnableAutoReconnect = .T.
				**.AdvancedSettings2.CanAutoReconnect = .T.		
				**.AdvancedSettings4.SmartSizing = .T. &&Testar parametro
				.AdvancedSettings4.SmartSizing = 1 &&Testar parametro
			
			&& Funcionamento
				.AdvancedSettings2.EnableWindowsKey = 0 &&Testar parametro
				.AdvancedSettings2.RedirectClipboard = 1
				
			&& Layout
				.DesktopWidth = thisform.Width
				.DesktopHeight = thisform.Height
			
			&& Establece liga��o
				.Connect()
		ENDWITH	
				
	ENDPROC

	PROCEDURE Destroy
		QUIT
	ENDPROC
	
*!*		PROCEDURE Minimize
*!*			thisform.maximize
*!*		ENDPROC
	
	PROCEDURE RDPtimer.TIMER 			
		IF thisform.oRDP.Connected = 0 Then
			QUIT
		Endif
	ENDPROC
		
ENDDEFINE


**************************************
** Verifica se o exe j� esta a correr
***************************************
FUNCTION uf_ExeRunning
	LPARAMETERS lcExe,lcAccao
	
	**********************************
	**		Valida Parametros		**
	**********************************
	IF EMPTY(lcExe) OR !TYPE("lcExe")=="C"
		MESSAGEBOX("O par�metro : EXE, n�o � v�lido!",48,"LOGITOOLS SOFTWARE")
		RETURN .F.
	ENDIF
	
	IF EMPTY(lcAccao)
		lcAccao = 0
	ENDIF
	
	******************************************
	**		Verifica se esta a correr		**
	******************************************
	LOCAL lcTSID,lcTS, lcCont
	STORE .F. TO lcTS
	STORE '' TO lcTSID
	STORE 0 TO lcCont

	LOCAL loLocator, loWMI, loProcesses, loProcess,lcEstado
	STORE .f. TO lcEstado
	loLocator 	=	CREATEOBJECT('WBEMScripting.SWBEMLocator')
	loWMI		=	loLocator.ConnectServer() 
	loWMI.Security_.ImpersonationLevel = 3  && Impersonate

	loProcesses	= loWMI.ExecQuery([SELECT * FROM Win32_Process WHERE Name = '] + ALLTRIM(lcExe) + ['])

	IF loProcesses.Count > 1
		lcEstado = .T.
	ENDIF
			
	RETURN lcEstado
ENDFUNC


******************************
** Fecha a aplica��o
******************************
PROCEDURE uf_sair
	QUIT
ENDPROC


**************************************
** Tratamento de erros
**************************************
PROCEDURE ErrorHandler
   PARAMETERS tnError, tcMessage, tcMessage1, tcProgram, tnLineno
   
   LOCAL lcErrorMessage
   lcErrorMessage = "Error number: " + TRANSFORM(tnError) + CHR(13) ;
      + "Error message: " + tcMessage + CHR(13) ;
      + "Line of code: " + tcMessage1 + CHR(13);
      + "Program: " + tcProgram + CHR(13);
      + "Line number: " + TRANSFORM(tnLineno)
   
   MESSAGEBOX(lcErrorMessage, 16, "Logitools Software")
   CLEAR EVENTS
ENDPROC


***********
**
***********
FUNCTION uf_config
	LOCAL lcUtilizador, lcPassword, lcAutoLogin
	STORE '' TO lcUtilizador, lcPassword, lcAutoLogin
			
	*!*	************************************
	*!*	Calcula Directorios (Ficheiro INI)
	*!*	************************************
	IF !FILE ('Config.ini')
		uf_FicheiroIni()
	ENDIF  
	
	*!*	************************************
	*!*	Le valores do ficheiro INI
	*!*	************************************
	lcFicheiroINI = Addbs(Justpath(Sys(16,0))) + 'Config.ini'
	
	lcOldIniReg = Createobject("oldinireg")
	lcOldIniReg.GetIniEntry(@lcUtilizador, "LOGIN", "Utilizador", lcFicheiroINI)
	lcOldIniReg.GetIniEntry(@lcPassword, "LOGIN", "Password", lcFicheiroINI)
	lcOldIniReg.GetIniEntry(@lcAutoLogin, "LOGIN", "AutoLogin", lcFicheiroINI)
	
	*!*	************************************
	*!*	Desencripta valores para fazer login
	*!*	************************************
	**myUtilizador = DECRYPT(ALLTRIM(lcUtilizador),myChavePrivada,1024)
	myUtilizador = ALLTRIM(lcUtilizador)
    myPassword = DECRYPT(ALLTRIM(lcPassword),myChavePrivada,1024)
    **myPassword = ALLTRIM(lcPassword)
    myAutoLogin = IIF(!EMPTY(lcAutoLogin), IIF(lcAutoLogin=="1",.t., .f.), "")
ENDFUNC


**************************************************
** Gere ficheiro ini com configura��es de liga��o
**************************************************
FUNCTION uf_FicheiroIni
	LPARAMETERS lcUtilizador, lcPassword, lcMemorizar, lcAutoLogin 

	LOCAL lcFicheiroINI
	
	lcFicheiroINI = Addbs(Justpath(Sys(16,0))) + 'Config.ini'
	
	*!*	************************************
	*!*	Valida parametros
	*!*	************************************	
	IF EMPTY(lcUtilizador)
		lcUtilizador = ''
	ENDIF 
	
	IF EMPTY(lcPassword)
		lcPassword = ''
	ENDIF 
	
	IF EMPTY(lcMemorizar)
		lcMemorizar = 0
	ENDIF 
	
	IF EMPTY(lcAutoLogin)
		lcAutoLogin = 0
	ENDIF
	
	*!*	************************************
	*!*	Apaga e cria ficheiro INI
	*!*	************************************
	IF FILE(lcFicheiroINI)
		ERASE(lcFicheiroINI)
	ENDIF
	
	*!*	************************************
	*!*	Cria ficheiro INI
	*!*	************************************
	lnFileHandle = FCREATE('Config.INI',0)
	
	&& Escreve no ficheiro
	FWRITE (lnFileHandle, '[Login]' + CHR(13)+CHR(10)) 
	**FWRITE (lnFileHandle, 'Utilizador=' + IIF(lcMemorizar = 1 , ENCRYPT(ALLTRIM(lcUtilizador),myChavePrivada,1024),"") + CHR(13)+CHR(10) )
	FWRITE (lnFileHandle, 'Utilizador=' + IIF(lcMemorizar = 1 , ALLTRIM(lcUtilizador),"") + CHR(13)+CHR(10) )
	FWRITE (lnFileHandle, 'Password=' + IIF(lcMemorizar = 1 , ENCRYPT(ALLTRIM(lcPassword),myChavePrivada,1024),"") + CHR(13)+CHR(10))	
	**FWRITE (lnFileHandle, 'Password=' + IIF(lcMemorizar = 1 , ALLTRIM(lcPassword),"") + CHR(13)+CHR(10))	
	FWRITE (lnFileHandle, 'AutoLogin=' + IIF(lcAutoLogin = 1 , "1", "0") + CHR(13)+CHR(10))
	
	&& Fecha o ficheiro
	FCLOSE (lnFileHandle)
	
ENDFUNC


*************************************************
** Verifica se a URL de update est� dispon�vel
*************************************************
FUNCTION uf_ValidaLigacaoInternet
	DECLARE INTEGER InternetGetConnectedState IN WinInet ;
	INTEGER @lpdwFlags, INTEGER dwReserved

	LOCAL lnFlags, lnReserved, lnValida

	lnFlags=0
	lnReserved=0
	
	lnValida = InternetGetConnectedState(@lnFlags,lnReserved)
	
	RETURN lnValida
ENDFUNC