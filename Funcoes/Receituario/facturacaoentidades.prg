** 
FUNCTION uf_facturacaoentidades_chama
	LPARAMETERS lcAno, lcMes

	** vari�veis publicas para a emiss�o das facturas
	Public myFacHora, myFacData, myNrVenda
	Store 0 To myNrVenda
	
	public mlcEivaIn1, mlcEivaIn2, mlcEivaIn3, mlcEivaIn4, mlcEivaIn5, mlcEivaIn6, mlcEivaIn7, mlcEivaIn8, mlcEivaIn9
	public mlcEivaV1, mlcEivaV2, mlcEivaV3, mlcEivaV4, mlcEivaV5, mlcEivaV6, mlcEivaV7, mlcEivaV8, mlcEivaV9
	Store 0 To mlcEivaIn1, mlcEivaIn2, mlcEivaIn3, mlcEivaIn4, mlcEivaIn5, mlcEivaIn6, mlcEivaIn7, mlcEivaIn8, mlcEivaIn9
	Store 0 To mlcEivaV1, mlcEivaV2, mlcEivaV3, mlcEivaV4, mlcEivaV5, mlcEivaV6, mlcEivaV7, mlcEivaV8, mlcEivaV9
	
	PUBLIC myfacturacaoentidadesAno, myfacturacaoentidadesMes
		
	&& Valida Parametros
	IF EMPTY(lcANO)
		myfacturacaoentidadesAno = YEAR(DATE())
	ELSE
		myfacturacaoentidadesAno = lcAno
	ENDIF

	IF EMPTY(lcMes)
		myfacturacaoentidadesMes = MONTH(DATE())
	ELSE
		myfacturacaoentidadesMes = lcMes
	ENDIF
	
	** carrega cursor vazio
	uf_facturacaoentidades_cursorEfl(.f.)
	uf_facturacaoentidades_criaCursores()

	&& Abre o painel
	IF TYPE("facturacaoentidades")=="U"
		DO FORM facturacaoentidades
	
		
	ELSE
		facturacaoentidades.show
	ENDIF
	
		

	
ENDFUNC


**
FUNCTION uf_facturacaoentidades_carregaMenu
	facturacaoentidades.menu1.adicionaopcao("atualiza", "Atualizar", myPath + "\imagens\icons\actualizar_w.png", "uf_facturacaoentidades_cursorEfl with .t.","A")
	**facturacaoentidades.menu1.adicionaopcao("selTodos", "Sel. Todos", myPath + "\imagens\icons\unchecked_w.png", "uf_facturacaoentidades_selTodos","T")
	facturacaoentidades.menu1.adicionaopcao("fxAnf", "Gerar Fx.", myPath + "\imagens\icons\pasta_w.png", "uf_facturacaoentidades_envioFactAnf","F")
	facturacaoentidades.menu1.adicionaopcao("enviarFactSNS", "Enviar Factura", myPath + "\imagens\icons\pasta_w.png", "uf_facturacaoentidades_envioFactSNS","E")
	**facturacaoentidades.menu1.adicionaopcao("validarBenef", "Validar Benef.", myPath + "\imagens\icons\informacao_w.png", "uf_facturacaoentidades_checkBenefAfp","V")
	facturacaoentidades.menu1.estado("", "SHOW", "Emitir", .t., "Sair", .t.)
ENDFUNC

FUNCTION uf_facturacaoentidades_criaCursores
		

	
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_receituario_resultado_envio_entidadeFe  '' ,-1,-1,1,'<<Alltrim(mySite)>>'
 	ENDTEXT
 		
 		
 	IF !uf_gerais_actGrelha("" ,"uCrsAuxRespostaDet",lcSQL)
 			regua(2)	
		uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VERIFICAR A RESPOSTA DOS SERVI�OS PARTILHADOS. POR FAVOR CONTACTE O SUPORTE.", "Ok", "", 16)
	ENDIF

ENDFUNC


**
FUNCTION uf_facturacaoentidades_alternaMenu
	SELECT uCrsE1
	
*!*		DO CASE 
*!*			CASE UPPER(ALLTRIM(uCrsE1.u_assfarm)) == "AFP"
*!*				facturacaoentidades.menu1.estado("fxAnf", "HIDE", "Emitir", .t., "Sair", .t.)
*!*		ENDCASE 
	
ENDFUNC


FUNCTION 	uf_facturacaoPreencheUltimoDiaMes()
	
		** Guardar �ltimo Dia do M�s **
		IF(!EMPTY(myfacturacaoentidadesMes))
			
			Local lcData
			lcData = ALLTRIM(str(myfacturacaoentidadesAno)) + Substr("00",1,Len("00")-Len(ALLTRIM(str(myfacturacaoentidadesMes)))) + ALLTRIM(str(myfacturacaoentidadesMes)) + '01'

			IF USED("uCrsUdm")
				fecha("uCrsUdm")
			ENDIF
			
			TEXT TO lcSql NOSHOW TEXTMERGE
			
				SELECT  
					convert(varchar, dateadd(HOUR, <<difhoraria>>, getdate()) ,104) as dataAtual
					,convert(varchar, DATEADD(DAY, -1, DATEADD(MONTH, DATEDIFF(MONTH, 0, '<<lcData>>')+1, 0)) ,104) as udm
					,DATEADD(DAY, -1, DATEADD(MONTH, DATEDIFF(MONTH, 0, '<<lcData>>')+1, 0)) as ultimoDia
			ENDTEXT
			If !uf_gerais_actgrelha("", [uCrsUdm], lcSql)
				uf_perguntalt_chama("OCORREU UM ERRO A VERIFICAR DATA ATUAL. POR FAVOR CONTACTE O SUPORTE.","OK","", 16)
				RETURN
			ENDIF
			

			Select uCrsUdm	
			FACTURACAOENTIDADES.chkDataFactura.label1.caption = "Factura SNS, Emitir � data: " + ALLTRIM(uCrsUdm.udm)
			
		
	ENDIF
	


ENDFUNC




FUNCTION uf_facturacaoentidades_imprimeDocPdf
	

ENDFUNC


**
FUNCTION uf_facturacaoentidades_cursorEfl
	LPARAMETERS tcBool

	**
*	Public myEflAno, myEflMes 
*	Set Point To "."
	IF !tcBool
		** Criar Cursor **
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			SET fmtonly on
				exec up_receituario_facturacaoEntidades 2020, 1, '<<ALLTRIM(uf_gerais_getSitePostos())>>', '<<ALLTRIM(mysite)>>'
			SET fmtonly off
		ENDTEXT 
		IF !uf_gerais_actgrelha("", [uCrsEFL], lcSql)
			uf_perguntalt_chama("OCORREU UM ERRO A PROCURAR LOTES A FACTURAR. POR FAVOR CONTACTE O SUPORTE.","OK","", 16)
			RETURN .f.
		ENDIF
		
		SELECT uCrsEFL
		GO top
		SCAN
			replace uCrsEFL.sel WITH .t.
		ENDSCAN
		
		uf_facturacaoentidades_resultado_envio()


	ELSE
		
		If EMPTY(facturacaoentidades.cmbAno.value) Or Empty(facturacaoentidades.cmbMes.value)
			RETURN .f.
		ENDIF
		
		myfacturacaoentidadesAno = VAL(ALLTRIM(facturacaoentidades.cmbAno.value))
		myfacturacaoentidadesMes = uf_gerais_getMonthComboBox("facturacaoentidades.cmbMes")


		** Criar Cursor **
		TEXT TO lcSql Noshow textmerge
			exec up_receituario_facturacaoEntidades <<myfacturacaoentidadesAno>>, <<myfacturacaoentidadesMes>>, '<<ALLTRIM(uf_gerais_getSitePostos())>>', '<<ALLTRIM(mysite)>>'
		ENDTEXT
		
		IF !uf_gerais_actgrelha("facturacaoentidades.grdEfl", [uCrsEFL], lcSql)
			uf_perguntalt_chama("OCORREU UM ERRO A PROCURAR LOTES A FACTURAR. POR FAVOR CONTACTE O SUPORTE.","OK","", 16)
			RETURN
		ELSE
			SELECT uCrsEFL
			GO TOP
			SCAN FOR !(ALLTRIM(uCrsEFL.fistamp) == "fee")
				replace uCrsEfl.utente	With uCrsEfl.pvp-uCrsEfl.comp
				SELECT uCrsEfl
			ENDSCAN
			
			
			SELECT uCrsEFL
			GO top
			SCAN
				replace uCrsEFL.sel WITH .t.
			ENDSCAN
			
			SELECT uCrsEfl
			GO TOP
		ENDIF
		**
		
		** aplica filtro
		uf_facturacaoentidades_filtraFacturacao()
	ENDIF
ENDFUNC

FUNCTION uf_facturacaoentidades_resultado_envio	

		LOCAL lcAbrev, lcMes, lcAno, lcGrid 
		lcAbrev = ''
		lcMes = -1
		lcAno = -1
		lcGrid = ""

		
		if(vartype(FACTURACAOENTIDADES)!='U')
			if(!EMPTY(facturacaoentidades.lstEntidade.value))
				lcAbrev=ALLTRIM(facturacaoentidades.lstEntidade.value)
			ENDIF
			
			if(!EMPTY(facturacaoentidades.cmbMes.value))
				lcMes=uf_gerais_getMonthExToMonth(ALLTRIM(facturacaoentidades.cmbMes.value))
			ENDIF
			
			
			if(!EMPTY(facturacaoentidades.cmbAno.value))
				lcAno=ALLTRIM(facturacaoentidades.cmbAno.value)
			ENDIF
			
			lcGrid  = "facturacaoentidades.gridResposta"	
			
			uf_gerais_meiaRegua("A consultar resultados")
		

			
			TEXT TO lcSQL TEXTMERGE NOSHOW
				exec up_receituario_resultado_envio_entidadeFe  '<<Alltrim(lcAbrev)>>' ,<<lcAno>>,<<lcMes>>,1,'<<Alltrim(mySite)>>'
	 		Endtext
	 		IF !uf_gerais_actGrelha(lcGrid   ,"uCrsAuxRespostaDet",lcSQL)
	 			regua(2)	
				uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VERIFICAR A RESPOSTA DOS SERVI�OS PARTILHADOS. POR FAVOR CONTACTE O SUPORTE.", "Ok", "", 16)
				RETURN .f.
			ENDIF
			
			regua(2)		
		ENDIF
		
	
		
ENDFUNC	

FUNCTION uf_gerais_geraUltimoDiaDoMes

	TEXT TO lcSql NOSHOW TEXTMERGE
			SELECT  
				convert(varchar, dateadd(HOUR, <<difhoraria>>, getdate()) ,104) as dataAtual
				,convert(varchar, DATEADD(DAY, -1, DATEADD(MONTH, DATEDIFF(MONTH, 0, '<<lcData>>')+1, 0)) ,104) as udm
				,DATEADD(DAY, -1, DATEADD(MONTH, DATEDIFF(MONTH, 0, '<<lcData>>')+1, 0)) as ultimoDia
		ENDTEXT
		If !uf_gerais_actgrelha("", [uCrsUdm], lcSql)
			uf_perguntalt_chama("OCORREU UM ERRO A VERIFICAR DATA ATUAL. POR FAVOR CONTACTE O SUPORTE.","OK","", 16)
			RETURN
		ENDIF

ENDFUNC


FUNCTION uf_facturacaoEntidadesValidaIdConv
 LPARAMETERS lcAbrev, lcSite
 
 	LOCAL lcSQL, lcResult, lcId
 	lcSQL = ''
 	lcId =  ''
 	
 	lcResult = .f.
 	

 	fecha("uCursAuxRespConvMcdt")

 
 	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_receituario_validaCodigoConvencao '<<Alltrim(lcAbrev)>>','<<Alltrim(lcSite)>>'
	Endtext
 	IF !uf_gerais_actGrelha("","uCursAuxRespConvMcdt",lcSQL)
 			regua(2)	
		uf_perguntalt_chama("OCORREU UMA ANOMALIA A RETORNAR O ID DE CONVEN��O. POR FAVOR CONTACTE O SUPORTE.", "Ok", "", 16)
		RETURN .f.
	ENDIF
	
	
	if(USED("uCursAuxRespConvMcdt"))
	
		if(RECCOUNT("uCursAuxRespConvMcdt")>0)
			SELECT uCursAuxRespConvMcdt
			GO TOP
			lcId  = ALLTRIM(uCursAuxRespConvMcdt.id)
		ENDIF
		
	ENDIF
	

	
	fecha("uCursAuxRespConvMcdt")
		
 	RETURN lcId

ENDFUNC

FUNCTION uf_facturacaoentidades_validaUltimaDataSerie
	LPARAM lcNdoc,myFacData
	
	LOCAL lcResult
	lcResult = .f.
	
	fecha("uCursAuxRespFnoMcdt")
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_receituario_validaFnoFacturacaoPosterior <<lcNdoc>>,'<<myFacData>>'
	Endtext
 	IF !uf_gerais_actGrelha("","uCursAuxRespFnoMcdt",lcSQL)
 		regua(2)	
		uf_perguntalt_chama("OCORREU UMA ANOMALIA A RETORNAR O ULTIMO DOCUMENTO DE FACTURA��O. POR FAVOR CONTACTE O SUPORTE.", "Ok", "", 16)
		RETURN .f.
	ENDIF
	
	
	SELECT uCursAuxRespFnoMcdt
	GO TOP
	lcResult  = uCursAuxRespFnoMcdt.result
	
	

	fecha("uCursAuxRespFnoMcdt")
	
	RETURN lcResult
	
ENDFUNC

FUNCTION uf_facturacaoentidades_validaSeSNS
	LPARAMETERS lcAbrev, lcNo
	
	LOCAL lnNo, lcAbrevAsStr
	lnNo  = 0

	
	IF(EMPTY(lcAbrev))
		lcAbrevAsStr = ""
	ELSE
		lcAbrevAsStr = ALLTRIM(lcAbrev)	
	ENDIF
	
	IF(EMPTY(lcNo))
		lnNo = 0
	ELSE
		lnNo = lcNo			
	ENDIF
	
	
	if(lnNo == 1 Or lnNo =142)
		RETURN .t.
	endif
	
	IF Upper(Alltrim(lcAbrevAsStr )) == 'SNS' OR  uf_gerais_compStr(ALLTRIM(lcAbrevAsStr),"VACINASSNS") 
		RETURN .t.
	ENDIF
	

	
	RETURN .f.
ENDFUNC


**
FUNCTION uf_facturacaoentidades_gravar
	LOCAL lcDataUltimoDiaMes
	
	
	If !Used("uCrsEFL1")
		SELECT * FROM uCrsEFL WITH (buffering =.t.) INTO CURSOR uCrsEFL1 READWRITE WHERE Upper(Alltrim(uCrsEFL.entidades)) == Upper(Alltrim(facturacaoentidades.lstEntidade.value))
	ENDIF
	
	If EMPTY(facturacaoentidades.cmbAno.value) Or Empty(facturacaoentidades.cmbMes.value)
		uf_perguntalt_chama("POR FAVOR CONFIRME O ANO E M�S DE FACTURA��O E REFRESQUE OS DADOS.","OK","", 48)
		RETURN
	ENDIF
	**
	

	
	**
	Local lcValidaInsertEntFt, lcValidaInsertEntFt2, lcValidaInsertEntFi
	
	LOCAL lcAbrevEntidade 
	lcAbrevEntidade  = 'XXX'
	
	**Set Point To "."
	
	** Valida��es **
	If !Used("uCrsEFL1")
		Return
	ENDIF
	
	
	
	
	** validar se o utilizador escolheu alguma coisa **
	**LOCAL lcvalida
	**SELECT uCrsEfl
	**GO TOP
	**SCAN FOR sel
**		lcValida = .t.
	**ENDSCAN
	**IF !lcValida
	**	uf_perguntalt_chama("NENHUM TIPO DE LOTE FOI SELECCIONADO PARA FACTURA��O.","OK","", 48)
	**	RETURN
	**ENDIF
	**
	
	** Guardar �ltimo Dia do M�s **
	Local lcData, lcPdata
	lcData = ALLTRIM(str(myfacturacaoentidadesAno)) + Substr("00",1,Len("00")-Len(ALLTRIM(str(myfacturacaoentidadesMes)))) + ALLTRIM(str(myfacturacaoentidadesMes)) + '01'

	

	IF !USED("uCrsUdm")
		TEXT TO lcSql NOSHOW TEXTMERGE
			SELECT  
				convert(varchar, dateadd(HOUR, <<difhoraria>>, getdate()) ,104) as dataAtual
				,convert(varchar, DATEADD(DAY, -1, DATEADD(MONTH, DATEDIFF(MONTH, 0, '<<lcData>>')+1, 0)) ,104) as udm
				,DATEADD(DAY, -1, DATEADD(MONTH, DATEDIFF(MONTH, 0, '<<lcData>>')+1, 0)) as ultimoDia
		ENDTEXT
		If !uf_gerais_actgrelha("", [uCrsUdm], lcSql)
			uf_perguntalt_chama("OCORREU UM ERRO A VERIFICAR DATA ATUAL. POR FAVOR CONTACTE O SUPORTE.","OK","", 16)
			RETURN
		ENDIF
	ENDIF
	Select uCrsUdm
	lcDataUltimoDiaMes = uCrsUdm.ultimoDia
	



	** Criar cursor com um cliente por linha **
	SELECT distinct no, estab, entidades FROM uCrsEFL1 WHERE uCrsEFL.sel == .t. Into Cursor uCrsEf


	IF !Used("uCrsEf")
		uf_perguntalt_chama("N�O FORAM ENCONTRADOS CLIENTES PARA FACTURAR.","OK","", 48)
		RETURN
	ENDIF
	
	

	**valida se lotes fechados
	uf_gerais_meiaRegua("A validar se lotes fechados...")
	SELECT uCrsEf
	GO TOP
	SCAN
		LOCAL lcNoEfEnt, lcNoEfEstab
		lcNoEfEnt   = uCrsEf.no
		lcNoEfEstab = uCrsEf.estab
		
	
		
		fecha("uCrsLotesAbertos")
			
		TEXT TO lcSQL TEXTMERGE NOSHOW
			exec up_receituario_validaSeLotesFechadosEntidade <<lcNoEfEnt>> ,<<lcNoEfEstab>>,<<myfacturacaoentidadesAno>>,<<myfacturacaoentidadesMes>>,'<<ALLTRIM(uf_gerais_getSitePostos())>>'
 		Endtext
 		IF !uf_gerais_actGrelha("" ,"uCrsLotesAbertos",lcSQL)
 			regua(2)	
			uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VALIDAR O ESTADO DOS LOTES. POR FAVOR CONTACTE O SUPORTE.", "Ok", "", 16)
			regua(2)
			RETURN .f.
		ENDIF
	
		if(USED("uCrsLotesAbertos"))
			LOCAL lcAbrev, lcTotal
			SELECT uCrsLotesAbertos
			GO TOP
			lcAbrev = ALLTRIM(uCrsLotesAbertos.abrev)
			lcTotal = uCrsLotesAbertos.total
			IF(!EMPTY(lcTotal) AND  !EMPTY(lcAbrev))
				uf_perguntalt_chama("POR FAVOR FECHE OS TODOS OS LOTES DA ENTIDADE: " + lcAbrev  , "Ok", "", 16)
				regua(2)
				RETURN .f. 
			ENDIF			
		ENDIF

		fecha("uCrsLotesAbertos")

		SELECT uCrsEf
	ENDSCAN
	
	**valida se entidade bem criada
	uf_gerais_meiaRegua("A validar entidades...")
	SELECT uCrsEf
	GO TOP
	SCAN
		LOCAL lcNoEfEnt, lcNoEfEstab
		lcNoEfEnt   = uCrsEf.no
		lcNoEfEstab = uCrsEf.estab
		
	
		
		fecha("uCrsLotesAbertos")
		LOCAL lcSQLEnt
		lcSQLEnt = ''
			
		TEXT TO lcSQLEnt TEXTMERGE NOSHOW
			exec up_receituario_validaEntidadeFacturar <<lcNoEfEnt>> ,<<lcNoEfEstab>> ,'<<Alltrim(mySite)>>'
 		Endtext
 		IF !uf_gerais_actGrelha("" ,"uCrsEntidadeValida",lcSQLEnt)
 			regua(2)	
			uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VALIDAR A ENTIDADE. POR FAVOR CONTACTE O SUPORTE.", "Ok", "", 16)
			regua(2)
			RETURN .f.
		ENDIF
		
		
		SELECT uCrsEntidadeValida
		GO TOP
		
		if(USED("uCrsEntidadeValida"))
			LOCAL lcAbrev, lcValida
	
			SELECT uCrsEntidadeValida
			GO TOP
			lcAbrev = ALLTRIM(uCrsEntidadeValida.entAbrev)
			lcValida = uCrsEntidadeValida.entvalida
		
			IF(EMPTY(lcValida))
				uf_perguntalt_chama("Erro na parametriza��o do(a) "+lcAbrev+", por favor contacte o suporte" , "Ok", "", 16)
				regua(2)
				RETURN .f. 
			ENDIF			
		ENDIF

		fecha("uCrsEntidadeValida")

		SELECT uCrsEf
	ENDSCAN
	
	
	
	

		
	regua(2)
	

	
	**

	** CALCULAR TOTAIS **
	Local lcNome, lcEstab, lcNo, lcNdoc, lcNmDoc, lcTipoDoc, lcNrVenda, lcFtStamp, lcEntidades
	Store '' To lcNome, lcNmdoc, lcFtStamp, lcEntidades
	Store 0 To lcNo, lcEstab, lcNdoc, lcNrVenda, lcTipoDoc
	Local lcTotQtt, lcQtt1, lcEtotal, lcEttIliq, lcEcusto
	Store 0 To lcTotQtt, lcQtt1, lcEtotal, lcEttIliq, lcEcusto

	** estas variaveis sao publicas porque a funcao de gravar FT excede o numero de parametros
	Store 0 To mlcEivaIn1, mlcEivaIn2, mlcEivaIn3, mlcEivaIn4, mlcEivaIn5, mlcEivaIn6, mlcEivaIn7, mlcEivaIn8, mlcEivaIn9
	Store 0 To mlcEivaV1, mlcEivaV2, mlcEivaV3, mlcEivaV4, mlcEivaV5, mlcEivaV6, mlcEivaV7, mlcEivaV8, mlcEivaV9
	** **

	** Preparar Regua **
	SELECT uCrsEf
	mntotal = reccount()
	regua(0, mntotal, "A PROCESSAR FACTURAS...", .f.)
	
	
	
	
	********************

	SELECT uCrsEf
	GO TOP
	SCAN
		regua[1,recno("uCrsEf"),"A PROCESSAR FACTURA PARA A ENTIDADE N�: "+Astr(uCrsEf.no),.f.]
		LOCAL lcSQLEnt 
		TEXT TO lcSQLEnt  TEXTMERGE NOSHOW
			exec up_gerais_dadosCliente <<uCrsEf.no>> ,<<uCrsEf.estab>>
 		Endtext
	
		** Dados do Cliente **
		IF uf_gerais_actgrelha("", "uCrsClEfl",lcSQLEnt)
			IF Reccount("uCrsClEfl") > 0
				SELECT uCrsClEfl
				lcNome 	= Alltrim(uCrsClEfl.nome)
				lcNo	= uCrsEf.no
				lcEstab	= uCrsEf.estab
				lcEntidades = ALLTRIM(uCrsEf.entidades)
			ELSE
				uf_perguntalt_chama("N�O FOI POSS�VEL ENCONTRAR O NOME DA ENTIDADE. POR FAVOR CONTACTE O SUPORTE.","OK","", 16)
				RETURN
			ENDIF
		ELSE
			uf_perguntalt_chama("OCORREU UMA ANOMALIA A VERIFICAR O NOME DA ENTIDADE. POR FAVOR CONTACTE O SUPORTE.","OK","", 16)
			RETURN
		ENDIF
		
		
	
	
		**********************

		** GUARDAR STAMPS DAS VENDAS **
		lcFtStamp = uf_gerais_stamp()
		*******************************

		** GUARDAR HORA DO SISTEMA *******
		Atime=ttoc(datetime()+(difhoraria*3600),2)
		Astr=left(Atime,8)
		myFacHora	= Astr && � necess�rio remover os segundos durante a inser��o na tabela
		**myFacHora	= Time() && � necess�rio remover os segundos durante a inser��o na tabela
		**********************************

**		SELECT * FROM uCrsEFL WITH (buffering =.t.) INTO CURSOR uCrsEFL READWRITE WHERE Upper(Alltrim(uCrsEFL.entidades)) == Upper(Alltrim(facturacaoentidades.lstEntidade.value))

		SELECT uCrsEFL1
		GO TOP
		SCAN FOR uCrsEFL1.no==uCrsEf.no AND uCrsEFL1.estab=uCrsEf.estab and  uf_gerais_compStr(uCrsEFL1.entidades,uCrsEf.entidades) 
			
	

			LOCAL lcDocMcdtName
			lcDocMcdtName = 'Factura SNS MCDT'
			

			
			** QUAL O TIPO DO DOCUMENTO A GRAVAR E N� DA VENDA **
			DO CASE
				&&SD-68009
				CASE uf_facturacaoentidades_validaSeSNS(Upper(Alltrim(uCrsEFL1.entidades)),uCrsEFL1.no)
				&&SD-68009 - uf_facturacaoentidades_validaSeSNS(Upper(Alltrim(uCrsEFL1.entidades)))			
					lcNdoc		= mySEntSNS
					lcNmDoc		= mySEntSNSNm
				CASE !EMPTY(uCrsEFL1.mcdt)
				    lcNdoc      = uf_gerais_getUmValor("td", "ndoc", "site = '" + ALLTRIM(mysite) + "' and nmdoc = '" + ALLTRIM(lcDocMcdtName) + "'")
				    lcNmDoc     = ALLTRIM(lcDocMcdtName)						
				OTHERWISE				
					lcNdoc		= mySEnt
					lcNmDoc		= mySEntNm	
			ENDCASE

			lcTipoDoc	= 1		
	
			
*!*				
*!*				SELECT uCrsEFL1
*!*				if uf_gerais_compStr(ALLTRIM(uCrsEFL1.entidades),'VACINASSNS') 
*!*					uf_perguntalt_chama("Entidade Bloqueada para factura��o","OK","",64)
*!*					regua(2)			
*!*					RETURN .f.
*!*				ENDIF

			
			** definir data factura e vencimento
				
			***********************************
			
			SELECT uCrsUdm
			&&SD-68009
			&&IF (FACTURACAOENTIDADES.chkDataFactura.tag=="false" AND (uf_facturacaoentidades_validaSeSNS(Upper(Alltrim(uCrsEFL1.entidades))) OR  !EMPTY(uCrsEFL1.mcdt)))
			&&SD-68009
			IF (FACTURACAOENTIDADES.chkDataFactura.tag=="false" AND (uf_facturacaoentidades_validaSeSNS(Upper(Alltrim(uCrsEFL1.entidades)),uCrsEFL1.no) OR  !EMPTY(uCrsEFL1.mcdt)))
				lcData = uCrsUdm.dataAtual		
			else
				lcData = uCrsUdm.udm
			ENDIF

			IF !uf_gerais_checkDocInsertDate(lcNdoc, lcData)
				regua(2)
				uf_perguntalt_chama("J� foi lan�ado um documento '" + ALLTRIM(lcNmDoc) + "' com data superior a " + uf_gerais_getDate(lcData) + ".","OK","",48)
				RETURN .F.
			ENDIF

			myFacData	= uf_gerais_getDate(lcData,"SQL")
			lcPdata		= uf_gerais_getDate(lcData,"SQL")
			

			
			

			IF(lcNdoc<=0)
				uf_perguntalt_chama("N�mero de documento de factura��o n�o dispon�vel! Por favor contacte o suporte!","OK","",64)
				regua(2)			
				RETURN .f.
			ENDIF
			
			
		
			
			SELECT uCrsEFL1
			IF(EMPTY(ALLTRIM(uf_facturacaoEntidadesValidaIdConv(ALLTRIM(uCrsEFL1.entidades),ALLTRIM(mysite)))) and !EMPTY(uCrsEFL1.mcdt))
				uf_perguntalt_chama("Por favor preencha o n�mero de conven��o para a entidade MCDT","OK","",64)
				regua(2)				
				RETURN .f.
			ENDIF
			
		
			
			
			if(!EMPTY(uCrsEFL1.mcdt) OR  !EMPTY(uCrsEFL1.prestacao))
				IF(!uf_facturacaoentidades_validaUltimaDataSerie(lcNdoc,myFacData))
					uf_perguntalt_chama("Foram criados documentos posteriores a essa data, Por favor verifique","OK","",64)
					regua(2)				
					RETURN .f.
				ENDIF				
			ENDIF
			
		
			

			&&SD-68009
			&&IF uf_facturacaoentidades_validaSeSNS(Upper(Alltrim(uCrsEFL1.entidades)))
			&&SD-68009			
			IF uf_facturacaoentidades_validaSeSNS(Upper(Alltrim(uCrsEFL1.entidades)),uCrsEFL1.no)
				lcAbrevEntidade  = 	Upper(Alltrim(uCrsEFL1.entidades))	
			ENDIF
		
		
			
			******************************************************
			
			SELECT uCrsEFL1
			
			** CALCULAR TOTAIS POR VENDA **
			lcTotQtt		= lcTotQtt	+ 1
			lcQtt1			= lcQtt1	+ 1
			lcEtotal		= lcEtotal	+ uCrsEFL1.comp
			lcEttIliq		= lcEttIliq	+ (uCrsEFL1.comp / (uCrsEFL1.iva/100+1))
			lcEcusto		= lcEcusto	+ 0
		
			IF uCrsEFL1.tabiva=1
				mlcEivaIn1	= mlcEivaIn1 + (uCrsEFL1.comp /(uCrsEFL1.iva/100+1))
				mlcEivaV1	= mlcEivaV1 + (uCrsEFL1.comp - (uCrsEFL1.comp / (uCrsEFL1.iva/100+1)))
			Else
				if uCrsEFL1.tabiva=2
					mlcEivaIn2	= mlcEivaIn2 + (uCrsEFL1.comp / (uCrsEFL1.iva/100+1))
					mlcEivaV2	= mlcEivaV2 + (uCrsEFL1.comp - (uCrsEFL1.comp / (uCrsEFL1.iva/100+1)))
				Else
					IF uCrsEFL1.tabiva=3
						mlcEivaIn3	= mlcEivaIn3 + (uCrsEFL1.comp / (uCrsEFL1.iva/100+1))
						mlcEivaV3	= mlcEivaV3 + (uCrsEFL1.comp - (uCrsEFL1.comp / (uCrsEFL1.iva/100+1)))
					Else
						IF uCrsEFL1.tabiva=4
							mlcEivaIn4	= mlcEivaIn4 + (uCrsEFL1.comp / (uCrsEFL1.iva/100+1))
							mlcEivaV4	= mlcEivaV4 + (uCrsEFL1.comp - (uCrsEFL1.comp /(uCrsEFL1.iva/100+1)))
						Else
							IF uCrsEFL1.tabiva=5
								mlcEivaIn5	= mlcEivaIn5 + (uCrsEFL1.comp / (uCrsEFL1.iva/100+1))
								mlcEivaV5	= mlcEivaV5 + (uCrsEFL1.comp - (uCrsEFL1.comp / (uCrsEFL1.iva/100+1)))
							Else
								If uCrsEFL1.tabiva==6
									mlcEivaIn6	= mlcEivaIn6 + (uCrsEFL1.comp / (uCrsEFL1.iva/100+1))
									mlcEivaV6	= mlcEivaV6 + (uCrsEFL1.comp - (uCrsEFL1.comp / (uCrsEFL1.iva/100+1)))
								Else
									If uCrsEFL1.tabiva==7
										mlcEivaIn7	= mlcEivaIn7 + (uCrsEFL1.comp / (uCrsEFL1.iva/100+1))
										mlcEivaV7	= mlcEivaV7 + (uCrsEFL1.comp - (uCrsEFL1.comp / (uCrsEFL1.iva/100+1)))
									Else
										If uCrsEFL1.tabiva==8
											mlcEivaIn8	= mlcEivaIn8 + (uCrsEFL1.comp / (uCrsEFL1.iva/100+1))
											mlcEivaV8	= mlcEivaV8 + (uCrsEFL1.comp - (uCrsEFL1.comp / (uCrsEFL1.iva/100+1)))
										Else
											If uCrsEFL1.tabiva==9
												mlcEivaIn9	= mlcEivaIn9 + (uCrsEFL1.comp / (uCrsEFL1.iva/100+1))
												mlcEivaV9	= mlcEivaV9 + (uCrsEFL1.comp - (uCrsEFL1.comp / (uCrsEFL1.iva/100+1)))								
											Endif
										Endif
									Endif
								Endif
							Endif
						Endif
					Endif
				Endif
			Endif
			******************************
			SELECT uCrsEFL1
		ENDSCAN
		
		
		If uf_gerais_actgrelha("", "", [BEGIN TRANSACTION])
			** GRAVAR FACTURA **

	

			SELECT uCrsEf
			lcValidaInsertEntFt		= uf_facturacaoentidades_GravarFt(lcFtStamp, lcNmDoc, lcNdoc, lcTipoDoc, lcNo, lcNome, lcEstab, lcTotQtt, lcQtt1, round(lcEtotal,2), round(lcEttIliq,2), round(mlcEivaV1+mlcEivaV2+mlcEivaV3+mlcEivaV4+mlcEivaV5+mlcEivaV6+mlcEivaV7+mlcEivaV8+mlcEivaV9,2), lcPdata, lcDataUltimoDiaMes)
			lcValidaInsertEntFt2	= uf_facturacaoentidades_GravarFt2(lcFtStamp,ALLTRIM(uCrsEf.entidades))
			lcValidaInsertEntFi		= uf_facturacaoentidades_GravarFi(lcFtStamp, lcNmDoc, lcNdoc, lcTipoDoc, lcNo, lcEstab, lcEntidades )		
			*******************
				
			If lcValidaInsertEntFt And lcValidaInsertEntFt2 And lcValidaInsertEntFi && Tudo Ok, insere o Documento na BD
				uf_gerais_actgrelha("", "", [COMMIT TRANSACTION])
						
				** for�ar triggers **
				uf_gerais_actgrelha("", "", [update ft set ft.ftstamp=ft.ftstamp where ftstamp=']+Alltrim(lcFtStamp)+['])
						
				** gravar certifica��o **
				uf_gravarCert(lcFtStamp, myNrVenda, lcNdoc, myFacData, myFacData, myFacHora, lcEtotal, 'FT')
				
				** Marcar Receitas como Facturadas **
				uf_facturacaoentidades_marcarReceitasComoFacturadas()
							
				** arquivos pdf **
				myVersaoDoc=uf_gerais_versaodocumento(1)
				
				IF myArquivoDigital
					uf_arquivoDigital_TaloesLt(Alltrim(lcFtStamp))
				Endif
		
				If myArquivoDigitalPDF
					uf_arquivodigital_ExpFactAutEntidadesA4(Alltrim(lcFtStamp))
				Endif
				***************
				
				** Cria cursor para posteriormente permitir a impress�o dos tal�es **
				If !Used("uCrsImpFacEnt")
					create cursor uCrsImpFacEnt (stamp c(26), nmdoc c(45))
				EndIf
				Select uCrsImpFacEnt
				Append Blank
				replace uCrsImpFacEnt.stamp		with Alltrim(lcFtStamp)
				replace uCrsImpFacEnt.nmdoc		With Alltrim(lcNmDoc)
				****************************************************
			Else
				uf_gerais_actgrelha("", "", [ROLLBACK])
				
				** Regista Problema na Tabela de Logs
				Local lcmensagem, lcorigem
				lcmensagem 	= "Erro de Inser��o na tabela FT; Nmdoc: " + Alltrim(lcNmDoc) + "; Fno: " + Alltrim(Str(myNrVenda)) + "; Ftstamp: " + Alltrim(lcFtStamp) + "; User:" + Alltrim(Str(ch_userno))
				lcorigem 	= "Factura��o �s Entidades: Gravar Factura"
				
				uf_gerais_registaErroLog(Alltrim(lcmensagem), Alltrim(lcorigem))
				
				** fecha a r�gua **
				regua(2)
				
				Return
			Endif
		Endif
		
		** reset �s vari�veis **
		Store 0 To lcTotQtt, lcQtt1, lcEtotal, lcEttIliq, lcEcusto
		Store 0 To mlcEivaIn1, mlcEivaIn2, mlcEivaIn3, mlcEivaIn4, mlcEivaIn5, mlcEivaIn6, mlcEivaIn7, mlcEivaIn8, mlcEivaIn9
		Store 0 To mlcEivaV1, mlcEivaV2, mlcEivaV3, mlcEivaV4, mlcEivaV5, mlcEivaV6, mlcEivaV7, mlcEivaV8, mlcEivaV9
		
		myNrVenda = 0
		


		
		If Used("uCrsClEfl")
			Fecha("uCrsClEfl")
		Endif
		************************
		
		Select uCrsEf
	Endscan
	
	** fecha a r�gua **
	regua(2)
	
	** fechar os cursores **
	If Used("uCrsEf")
		Fecha("uCrsEf")
	Endif
	******************
	
	uf_facturacaoentidades_cursorEfl(.t.)
	
	uf_perguntalt_chama("OPERA��O EFECTUADA COM SUCESSO.","OK","",64)
	
	** Imprimir as facturas **
	If Used("uCrsImpFacEnt")
		If Reccount("uCrsImpFacEnt")>0
			If uf_perguntalt_chama("DESEJA IMPRIMIR OS DOCUMENTOS?", "Sim", "N�o")
				**Local lcPrinter, lcDefaultPrinter
				**lcPrinter = Getprinter()
				
				**If !Empty(lcPrinter)
				Select uCrsImpFacEnt
				Go TOP 
				SCAN 
					LOCAL tcStamp
					Select uCrsImpFacEnt
					tcStamp = ALLTRIM(uCrsImpFacEnt.stamp)
					
					uf_gerais_actgrelha("", "ft", [select * from ft (nolock) where ftstamp=']+Alltrim(tcStamp)+['])
					uf_gerais_actgrelha("", "FTQRCODE", "exec up_print_qrcode_a4 '"+ALLTRIM(tcStamp)+"'")
					Select ft
					uf_gerais_actgrelha("", "td", [select * from td (nolock) where ndoc=]+ALLTRIM(str(ft.ndoc)))
					Select td
					uf_gerais_actgrelha("", "ft2", [select * from ft2 (nolock) where ft2stamp=']+Alltrim(tcStamp)+['])
					Select ft2
					uf_gerais_actgrelha("", "fi", [select * from fi (nolock) where ftstamp=']+Alltrim(tcStamp)+['])
					Select fi
					GO TOP
					
					uf_imprimirgerais_Chama('FACTURACAO',.f.,.t.)
				ENDSCAN 
					
*!*						** Preparar Regua **
*!*						Select uCrsImpFacEnt
*!*						mntotal=reccount()
*!*						regua(0,mntotal,"A IMPRIMIR FACTURAS...",.f.)
*!*						****************
*!*						
*!*						Select uCrsImpFacEnt
*!*						Go Top
*!*						Scan
*!*							regua[1,recno("uCrsImpFacEnt"),"A IMPRIMIR FACTURAS...:",.f.]
*!*					
*!*							uf_facturacaoentidades_imprimeSeguida(Alltrim(uCrsImpFacEnt.stamp), Alltrim(uCrsImpFacEnt.nmdoc), Alltrim(lcPrinter))
*!*						
*!*							Select uCrsImpFacEnt
*!*						Endscan
*!*						
*!*						** fecha a r�gua **
*!*						regua(2)
*!*					Else
*!*						uf_perguntalt_chama("N�O � POSS�VEL IMPRIMIR SEM ESCOLHER UMA IMPRESSORA. DEVE AGORA IMPRIMIR VIA O PAINEL GEST�O DE FACTURA��O.","OK","", 48)
*!*					Endif
			Endif
		Endif
	Endif
	********************
	
	If Used("uCrsImpFacEnt")
		Fecha("uCrsImpFacEnt")
	Endif
	If Used("td")
		Fecha("td")
	Endif
	If Used("ft")
		Fecha("ft")
	Endif
	If Used("ft2")
		Fecha("ft2")
	Endif
	If Used("fi")
		Fecha("fi")
	Endif
	If Used("tempfi")
		Fecha("tempfi")
	ENDIF
	
	
	
	IF Upper(Alltrim(lcAbrevEntidade)) == 'SNS'
		If uf_perguntalt_chama("Deseja enviar a factura eletronica do SNS", "Sim", "N�o")
			facturacaoentidades.lstEntidade.value = 'SNS'
			uf_facturacaoentidades_envioFactSNS()
		ENDIF		
	ENDIF

	
ENDFUNC






**
Function uf_facturacaoentidades_gravarFt
	Lparameters lnFtStamp, lnNmDoc, lnNdoc, lnTipoDoc, lnClNo, lnClNome, lnClEstab, lnTotQtt, lnQtt1, lnEtotal, lnEttIliq, lnEttIva, lnPdata, lnDataUltimoDiaMes


	******* N� DA VENDA **************************************
	&&If uf_gerais_actgrelha("", "uCrsMaxFno", [exec up_facturacao_numDoc ] + ALLTRIM(str(lnNDoc)) + [, ] + ALLTRIM(str((myfacturacaoentidadesAno))))
	**If uf_gerais_actgrelha("", "uCrsMaxFno", [exec up_facturacao_numDoc ] + ALLTRIM(str(lnNDoc)) + [, ] + LEFT(myFacData,4) + [, '] + ALLTRIM(mySite) + [' ])
	**	If Reccount("uCrsMaxFno") > 0
	**		myNrVenda = uCrsMaxFno.numdoc
	**	Else
	**		uf_perguntalt_chama("OCORREU UM PROBLEMA A DETERMINAR O N�MERO DO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.","OK","", 16)
	**		Fecha("uCrsMaxFno")
	**		Return
	**	Endif
	**	Fecha("uCrsMaxFno")
	**Else
	**	uf_perguntalt_chama("OCORREU UM PROBLEMA A DETERMINAR O N�MERO DO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.","OK","", 16)
	**	Return
	**Endif
	********************************************************
	
	** N� de Atendimento **
	Store '' To nrAtendimento
	uf_atendimento_geraNrAtendimento()
	********************
	
	** vari�veis est�ticas para j� **
	** campo ID tesouraria conta est�tico porque preciso de perceber como se preenche este valor - Lu�s Leal 20160412
	Local lcPais, lcId_tesouraria_conta
	STORE 1 TO lcPais
	STORE 99 TO lcId_tesouraria_conta
	*********************************
	
	Select uCrsClEfl
	Go TOP

	** NOTAS:
	** Bilhete de identidade : ft.bidata, ft.bino, ft.bilocal
	** o campo ftid � incrementado automaticamente pela BD
	Text to lcSql noshow textmerge

		DECLARE @fno NUMERIC(10)
		DECLARE @tabNewDocNr TABLE (newDocNr NUMERIC(10,0))

		INSERT INTO @tabNewDocNr (newDocNr)
		EXEC up_get_newDocNr 'FT', <<lnNdoc>>, '<<mySite>>', 1

		SET @fno = (SELECT newDocNr FROM @tabNewDocNr)

		insert into ft (
			ftstamp
			,pais
			,ndoc
			,nmdoc
			,fno
			,[no]
			,estab
			,nome
			,nome2
			,morada
			,[local]
			,codpost
			,ncont
			,telefone
			,tipo
			,bino
			,bidata
			,bilocal
			,zona
			,vendedor
			,vendnm
			,fdata
			,ftano
			,pdata
			,saida
			,ivatx1
			,ivatx2
			,ivatx3
			,ivatx4
			,ivatx5
			,ivatx6
			,ivatx7
			,ivatx8
			,ivatx9
			,moeda
			,facturada
			,fnoft
			,nmdocft
			,cdata
			,totqtt
			,qtt1
			,tipodoc,
			chora,
			memissao,
			eivain1, eivain2, eivain3,
			eivain4, eivain5,
			eivain6, eivain7, eivain8, eivain9,
			ivain1, ivain2, ivain3,
			ivain4, ivain5,
			ivain6, ivain7, ivain8, ivain9,
			eivav1, eivav2, eivav3,
			eivav4, eivav5,
			eivav6, eivav7, eivav8, eivav9,
			ivav1, ivav2, ivav3,
			ivav4, ivav5,
			ivav6, ivav7, ivav8, ivav9,
			ettiliq, ttiliq, ettiva, ttiva,
			etotal, total, ccusto,
			[site], pnome, pno, u_nratend,
			ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora, id_tesouraria_conta
		)
		Values (
			'<<lnFtStamp>>',
			<<lcPais>>, <<lnNdoc>>, '<<lnNmDoc>>', @fno,
			<<lnClNo>>, <<lnClEstab>>, '<<Alltrim(lnClNome)>>', '<<Alltrim(uCrsClEfl.nome2)>>', '<<Alltrim(uCrsClEfl.morada)>>', '<<Alltrim(uCrsClEfl.local)>>',
			 '<<Alltrim(uCrsClEfl.codpost)>>', '<<Alltrim(uCrsClEfl.ncont)>>', '<<Alltrim(uCrsClEfl.telefone)>>', '<<alltrim(uCrsClEfl.tipo)>>',
			'<<Alltrim(uCrsClEfl.bino)>>','19000101', '',
			'<<Alltrim(uCrsClEfl.zona)>>', <<ch_vendedor>>, '<<Alltrim(ch_vendnm)>>', '<<myFacData>>', year('<<myFacData>>'),
			'<<lnPdata>>', Left('<<myFacHora>>',5),
			(select taxa from taxasiva (nolock) where codigo=1), (select taxa from taxasiva (nolock) where codigo=2),
 			(select taxa from taxasiva (nolock) where codigo=3), (select taxa from taxasiva (nolock) where codigo=4),
			(select taxa from taxasiva (nolock) where codigo=5), (select taxa from taxasiva (nolock) where codigo=6),
			(select taxa from taxasiva (nolock) where codigo=7), (select taxa from taxasiva (nolock) where codigo=8),
			(select taxa from taxasiva (nolock) where codigo=9),
			'<<Alltrim(uCrsClEfl.moeda)>>',
			1, @fno, '<<lnNmDoc>>'
			,'<<uf_gerais_getdate(lnDataUltimoDiaMes,"SQL")>>'
			,<<lnTotQtt>>
			,<<lnQtt1>>,
			<<lnTipoDoc>>,
			Left('<<Chrtran(myFacHora,":","")>>',4),
			'<<Alltrim(uCrsClEfl.moeda)>>',
			<<mlcEivaIn1>>, <<mlcEivaIn2>>, <<mlcEivaIn3>>,
			<<mlcEivaIn4>>, <<mlcEivaIn5>>,
			<<mlcEivaIn6>>, <<mlcEivaIn7>>, <<mlcEivaIn8>>, <<mlcEivaIn9>>,
			<<mlcEivaIn1*200.482>>, <<mlcEivaIn2*200.482>>, <<mlcEivaIn3*200.482>>,
			<<mlcEivaIn4*200.482>>, <<mlcEivaIn5*200.482>>,
			<<mlcEivaIn6*200.482>>, <<mlcEivaIn7*200.482>>, <<mlcEivaIn8*200.482>>, <<mlcEivaIn9*200.482>>,
			<<mlcEivaV1>>, <<mlcEivaV2>>, <<mlcEivaV3>>,
			<<mlcEivaV4>>, <<mlcEivaV5>>,
			<<mlcEivaV6>>, <<mlcEivaV7>>, <<mlcEivaV8>>, <<mlcEivaV9>>,
			<<mlcEivaV1*200.482>>, <<mlcEivaV2*200.482>>, <<mlcEivaV3*200.482>>,
			<<mlcEivaV4*200.482>>, <<mlcEivaV5*200.482>>,
			<<mlcEivaV6*200.482>>, <<mlcEivaV7*200.482>>, <<mlcEivaV8*200.482>>, <<mlcEivaV9*200.482>>, 
			<<lnEttIliq>>, <<lnEttIliq*200.482>>, <<lnEttIva>>, <<lnEttIva*200.482>>,
			<<lnEtotal>>, <<lnEtotal * 200.482>>, '<<uCrsClEfl.ccusto>>',
			'<<Alltrim(mySite)>>', '<<Alltrim(myTerm)>>', <<myTermNo>>, '<<Alltrim(nrAtendimento)>>',
			'<<m_chinis>>', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),112), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108), '<<m_chinis>>', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),112), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108), <<lcId_tesouraria_conta>>
		)
	ENDTEXT
	
	IF !uf_gerais_actgrelha("", "", lcSql)
		uf_perguntalt_chama("OCORREU UM ERRO A GERAR FACTURA. POR FAVOR CONTACTE O SUPORTE"+Chr(10)+Chr(10)+"Cod.[FT]","OK","", 16)
		Return .f.
	ENDIF

	myNrVenda = uf_gerais_getUmValor("ft", "fno", "ftstamp = '" + lnFtStamp + "'")
	
	Return .t.
ENDFUNC



**
Function uf_facturacaoentidades_GravarFt2
	LParameters lnFtStamp, lcEntidadeAbrev
	

	** Notas: vdollocal = "Caixa"; vdlocal = "C"
	Text to lcSql noshow textmerge
		Insert Into ft2 (
			ft2stamp,
			vdollocal, vdlocal,
			morada,
			[local], codpost, telefone,
			email,
			ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora, id_efr_externa
		)
		Values (
			'<<lnFtStamp>>',
			'Caixa', 'C',
			'<<alltrim(uCrsClEfl.morada)>>',
			'<<Alltrim(uCrsClEfl.local)>>', '<<Alltrim(uCrsClEfl.codpost)>>', '<<Alltrim(uCrsClEfl.telefone)>>',
			'<<Alltrim(uCrsClEfl.email)>>',
			'<<m_chinis>>', '<<myFacData>>', '<<myFacHora>>', '<<m_chinis>>', '<<myFacData>>', '<<myFacHora>>', '<<Alltrim(lcEntidadeAbrev)>>'
		)
	EndText
	if !uf_gerais_actgrelha("", "", lcSql)
		uf_perguntalt_chama("OCORREU UM ERRO A GERAR FACTURA. POR FAVOR CONTACTE O SUPORTE."+Chr(10)+Chr(10)+"Cod.[FT2]","OK","", 16)
		Return .f.	
	Endif
	
	Return .t.
Endfunc



**
Function uf_facturacaoentidades_gravarFi
	LParameters lnFtStamp, lnNmDoc, lnNdoc, lnTipoDoc, lnNo, lcEstab, lcEntidade
	
	**On Error Wait Window "OK_GravarFacEntFi" TIMEOUT 1
	
	Local lnFistamp, lcOrdem
	Store '' To lcFiStamp
	Store 0 To lcOrdem
	
	SELECT uCrsEFL1
	GO TOP

	
	** Notas: 	epvori = epv; pvori  = pv
	**			stipo=4???
	Select uCrsClEfl
	Go top
	

	Select uCrsEFL1
	Go top
	Scan FOR uCrsEFL1.no==lnNo AND  uCrsEFL1.estab == lcEstab AND  uf_gerais_compStr(uCrsEFL1.entidades,lcEntidade) 
		** stamp, ordem **
		lnFiStamp 		= uf_gerais_stamp()
		lcOrdem			= lcOrdem + 10000
		******************
	
	
		
		Text to lcSql noshow textmerge
			insert into fi (
				fistamp,
				ndoc, nmdoc, fno,
				ref, design, qtt,
				tiliquido, etiliquido, iva, ivaincl,
				tabiva, armazem,
				-- fi.fnoft, ndocft, ftanoft, ftregstamp, fi.facturada,
				litem2, -- lotes
				litem, -- nr receitas
				lobs2,
				rdata,
				cpoc, lrecno, lordem,
				altura, -- pvp
				largura, -- utente
				pvp4_fee, 
				ftstamp,
				stipo,
				tipodoc, familia,
				stns,
				bistamp,
				pv, epv, tliquido,
				slvu, eslvu, sltt, esltt,
				fivendedor, fivendnm, 
				pvori, epvori,
				zona, morada, [local],
				codpost, telefone, email, ficcusto,
				ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora
			)
			Values (
				'<<Alltrim(lnFistamp)>>',
				<<lnNdoc>>, '<<Alltrim(lnNmDoc)>>', <<myNrVenda>>, 
				'<<Alltrim(uCrsEFL1.ref)>>', '<<Alltrim(uCrsEFL1.design)>>', 1, 
				<<uCrsEFL1.comp*200.482>>, <<uCrsEFL1.comp>>,
				<<uCrsEFL1.iva>>, 1, <<uCrsEFL1.tabiva>>, <<myArmazem>>,
				-- fi.fnoft, ndocft, ftanoft, ftregstamp, fi.facturada,
				'<<astr(uCrsEFL1.lotes)>>',
				'<<astr(uCrsEFL1.receitas)>>',
				'<<astr(uCrsEFL1.embalagens)>>',
				'<<myFacData>>',
				1, '<<Alltrim(lnFiStamp)>>', <<lcOrdem>>,
				<<uCrsEFL1.pvp>>,
				<<uCrsEFL1.utente>>,
				<<uCrsEFL1.fee>>,
				'<<Alltrim(lnFtStamp)>>',
				4,
				<<lnTipoDoc>>, '<<Alltrim(uCrsEFL1.familia)>>',
				1,
				'<<Alltrim(uCrsEFL1.fistamp)>>',
				<<uCrsEFL1.comp*200.482>>, <<uCrsEFL1.comp>>, <<uCrsEFL1.comp>>,
				<<uCrsEFL1.comp*200.482>>, <<uCrsEFL1.comp>>,	<<uCrsEFL1.comp*200.482>>, <<uCrsEFL1.comp>>,
				<<ch_vendedor>>, '<<ch_vendnm>>',
				<<uCrsEFL1.comp*200.482>>, <<uCrsEFL1.comp>>,
				'<<Alltrim(uCrsClEfl.zona)>>', '<<Alltrim(uCrsClEfl.morada)>>', '<<Alltrim(uCrsClEfl.local)>>',
				'<<Alltrim(uCrsClEfl.codpost)>>', '<<Alltrim(uCrsClEfl.telefone)>>', '<<Alltrim(uCrsClEfl.email)>>', '<<uCrsClEfl.ccusto>>',
				'<<m_chinis>>', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),112), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108), '<<m_chinis>>', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),112), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108)
			)
		ENDTEXT
		IF !uf_gerais_actgrelha("", "", lcSql)
			uf_perguntalt_chama("OCORREU UM ERRO A GERAR FACTURA. POR FAVOR CONTACTE O SUPORTE."+Chr(10)+Chr(10)+"Cod.[FI]","OK","",16)
			RETURN .f.
		ENDIF
		
		SELECT uCrsEFL1
	ENDSCAN
	
	RETURN .t.
ENDFUNC




**
FUNCTION uf_facturacaoentidades_marcarReceitasComoFacturadas
		
	SELECT uCrsEFL1
	GO TOP
**	SCAN FOR sel AND !(ALLTRIM(fistamp)=="fee") && considerar apenas selecionadas e n�o incluir linha com o Fee (remunera��o especifica)
	SCAN FOR !(ALLTRIM(fistamp)=="fee") && considerar apenas selecionadas e n�o incluir linha com o Fee (remunera��o especifica)
	
		
		Text To lcSql Noshow textmerge
			UPDATE
				ctltrct
			Set
				u_fistamp = '<<uCrsEFL1.fistamp>>'
			where
				ano = <<uCrsEFL1.ano>> and mes = <<uCrsEFL1.mes>>
				and cptorgabrev='<<uCrsEFL1.entidades>>' and slote = <<uCrsEFL1.serie>> and (tlote = <<uCrsEFL1.tipos>> or tlote = <<uCrsEFL1.tlote>>)
				and site = '<<ALLTRIM(mysite)>>'
		ENDTEXT
		If !uf_gerais_actgrelha("", "", lcSql)
			uf_perguntalt_chama("OCORREU UM ERRO A MARCAR AS RECEITAS COMO FACTURADAS. POR FAVOR CONTACTE O SUPORTE.","OK","", 16)
			RETURN
		ENDIF
		
		SELECT uCrsEFL1
	ENDSCAN
ENDFUNC




**
Function uf_facturacaoentidades_imprimeSeguida
	Lparameters tcStamp, tcNmDoc, tcPrinter
	
**	On Error wait window 'A Imprimir Facturas...' timeout 1
	
	Local lcimpressao, lcNomeFicheiro, lcTabela, lcTemDuplicados, lcNumDuplicados, lcReport
	
	** Obtem Nome do Ficheiro Tabela de Impressoes **
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		Select Top 1
			nomeficheiro, tabela, temduplicados, numduplicados
		From
			B_impressoes (nolock)
		Where
			documento = '<<Alltrim(tcNmDoc)>>'
		order by
			idupordefeito desc
	Endtext
	If !uf_gerais_actgrelha("", "templcSQL", lcSql)
		uf_perguntalt_chama("N�O FOI POSS�VEL ENCONTRAR A CONFIGURA��O DE IMPRESS�ES. POR FAVOR CONTACTE A ASSISTENCIA T�CNICA.","OK","", 16)
		RETURN .f.
	ELSE
		If RECCOUNT("templcSQL") > 0
			lcNomeFicheiro		= Alltrim(templcSQL.nomeficheiro)
			lctabela			= templcSQL.tabela
			lcTemDuplicados 	= templcSQL.temduplicados
			lcNumDuplicados 	= templcSQL.numduplicados
		ELSE
			uf_perguntalt_chama("N�O FOI POSS�VEL ENCONTRAR A CONFIGURA��O DE IMPRESS�ES. POR FAVOR CONTACTE A ASSISTENCIA T�CNICA.","OK","",48)
			RETURN .f.
		ENDIF
		Fecha("templcSQL")
	Endif
	*******************************************
	
	** PREPARAR CURSORES PARA IMPRESS�ES **
	***************************************
	
	** Obtem dados Gerais da Empresa / Loja **
	SELECT uCrsE1
	
	
	** Guardar report a imprimir - ficheiro **
	lcReport = Alltrim(myPath)+"\analises\" + Alltrim(lcnomeFicheiro)
	IF !FILE(alltrim(lcReport))
		uf_perguntalt_chama("A IMPRESS�O N�O SE ENCONTRA DEFINIDA. POR FAVOR CONTACTE O SUPORTE","OK","",48)
		RETURN .f.
	ENDIF
	*********************************
	
	** criar cursores para factura��o
	If Alltrim(lctabela) == "FT"
		** Trata BASES INC. IVA
		lcSQL = ""
		TEXT TO lcSQL TEXTMERGE NOSHOW
			exec up_gerais_iduCert '<<Alltrim(tcStamp)>>'
 		Endtext
 		IF !uf_gerais_actgrelha("", "ucrsHash", lcSql)
			uf_perguntalt_chama("OCORREU UM ERRO A CALCULAR INFORMACAO PARA A IMPRESSAO. POR FAVOR CONTACTE O SUPORTE.","OK","", 16)
 			RETURN .f.
 		Endif
		If !Reccount("ucrsHash")>0
			Select ucrsHash
			Append Blank
			replace ucrsHash.temcert	with .f.
			replace uCrsHash.parte1		With ''
			replace uCrsHash.parte1		With ''
		Endif
		
		** carregar cursores de factura��o **
		If uf_gerais_actgrelha("", "ft", [select * from ft (nolock) where ftstamp=']+Alltrim(tcStamp)+['])
			uf_gerais_actgrelha("", "FTQRCODE", "exec up_print_qrcode_a4 '"+ALLTRIM(tcStamp)+"'")
			Select ft
			uf_gerais_actgrelha("", "td", [select * from td (nolock) where ndoc=]+ALLTRIM(str(ft.ndoc)))
			Select td
			uf_gerais_actgrelha("", "ft2", [select * from ft2 (nolock) where ft2stamp=']+Alltrim(tcStamp)+['])
			Select ft2
			uf_gerais_actgrelha("", "fi", [select * from fi (nolock) where ftstamp=']+Alltrim(tcStamp)+['])
			Select fi
			GO TOP
		ELSE
			uf_perguntalt_chama("OCORREU UM PROBLEMA A LEVANTAR OS DADOS DO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.","OK","", 16)
			RETURN .f.
		Endif
		****************************
		
		***** Se for uma factura a entidades ***
		Select td
		Go Top
		If (TD.u_tipodoc)==1
			Select fi
			
			LOCAL lnTot
			LOCAL pvp1,pvp2,pvp3,pvp4,pvp5,pvp6,pvp7,pvp8,pvp9
			LOCAL compUtente1,compUtente2,compUtente3,compUtente4,compUtente5,compUtente6,compUtente7,compUtente8,compUtente9
			Local myCompOrg1,myCompOrg2,myCompOrg3,myCompOrg4,myCompOrg5,myCompOrg6,myCompOrg7,myCompOrg8,myCompOrg9
			Local fee1,fee2,fee3,fee4,fee5,fee6,fee7,fee8,fee9, feetotal, feetotalbi, feetotaliva
			
			CALCULATE SUM(fi.etiliquido) TO lnTot
			inTot = uf_gerais_extenso(lnTot)
			
			******* Calcula Totais ************
			Store 0 To pvp1,pvp2,pvp3,pvp4,pvp5,pvp6,pvp7,pvp8,pvp9
			STORE 0 TO compUtente1,compUtente1,compUtente2,compUtente3,compUtente4,compUtente5,compUtente6,compUtente7,compUtente8,compUtente9
			Store 0 To myCompOrg1,myCompOrg2,myCompOrg3,myCompOrg4,myCompOrg5,myCompOrg6,myCompOrg7,myCompOrg8,myCompOrg9
			STORE 0 TO fee1,fee2,fee3,fee4,fee5,fee6,fee7,fee8,fee9, feetotal, feetotalbi, feetotaliva
			
			Select Fi
			Go top
			Scan  
				Do case
					Case fi.tabiva == 1
						pvp1 			= pvp1 + fi.altura
						compUtente1 	= compUtente1 + fi.largura
						myCompOrg1 		= myCompOrg1 + FI.etiliquido
						fee1 			= fee1 + uf_gerais_calcFee(FI.pvp4_fee)
						
					Case fi.tabiva == 2 
						pvp2 			= pvp2 + fi.altura
						compUtente2 	= compUtente2 + fi.largura
						myCompOrg2 		= myCompOrg2 + FI.etiliquido
						fee2			= fee2 + uf_gerais_calcFee(FI.pvp4_fee)
						
					Case fi.tabiva == 3
						pvp3			= pvp3 + fi.altura
						compUtente3 	= compUtente3 + fi.largura
						myCompOrg3 		= myCompOrg3 + FI.etiliquido
						fee3 			= fee3 + uf_gerais_calcFee(FI.pvp4_fee)
						
					Case fi.tabiva == 4
						pvp4 			= pvp4 + fi.altura
						compUtente4 	= compUtente4 + fi.largura
						myCompOrg4 		= myCompOrg4 + FI.etiliquido
						fee4 			= fee4 + uf_gerais_calcFee(FI.pvp4_fee)
						
					Case fi.tabiva == 5
						pvp5 			= pvp5 + fi.altura
						compUtente5 	= compUtente5 + fi.largura
						myCompOrg5 		= myCompOrg5 + FI.etiliquido
						fee5 			= fee5 + uf_gerais_calcFee(FI.pvp4_fee)
						
					Case fi.tabiva == 6 
						pvp6 			= pvp6 + fi.altura
						compUtente6 	= compUtente6 + fi.largura
						myCompOrg6 		= myCompOrg6 + FI.etiliquido
						fee6 			= fee6 + uf_gerais_calcFee(FI.pvp4_fee)
						
					Case fi.tabiva == 7
						pvp7 			= pvp5 + fi.altura
						compUtente7 	= compUtente7 + fi.largura
						myCompOrg7 		= myCompOrg7 + FI.etiliquido
						fee7 			= fee7 + uf_gerais_calcFee(FI.pvp4_fee)
						
					Case fi.tabiva == 8
						pvp8 			= pvp8 + fi.altura
						compUtente8 	= compUtente8 + fi.largura
						myCompOrg8 		= myCompOrg8 + FI.etiliquido
						fee8 			= fee8 + uf_gerais_calcFee(FI.pvp4_fee)
						
					Case fi.tabiva == 9
						pvp9 			= pvp5 + fi.altura
						compUtente9 	= compUtente9 + fi.largura
						myCompOrg9 		= myCompOrg9 + FI.etiliquido
						fee9 			= fee9 + uf_gerais_calcFee(FI.pvp4_fee)
				ENDCASE
				
				IF ALLTRIM(fi.design)=='Remunera��o Espec�fica'
					**MESSAGEBOX(fi.etiliquido)
					feetotal = uf_gerais_calcFee(fi.etiliquido)
					feetotalbi = uf_gerais_calcFee(ROUND(fi.etiliquido / 1.06, 2))
					feetotaliva = uf_gerais_calcFee(fi.etiliquido - ROUND(fi.etiliquido / 1.06, 2))
				ENDIF 
				
				If fi.iva == 0 AND fi.epromo == .f.
					myIsentoIva = .t.
				ENDIF
				
				Select fi
			Endscan 
			*************************
			
			
			Select fi
	
			lcSQL=""
			
			TEXT TO lcSQL TEXTMERGE NOSHOW
				exec up_facturacao_ImprimefacturacaoEntidades '<<Alltrim(tcStamp)>>'
			ENDTEXT
			IF !uf_gerais_actgrelha("", "tempFi", lcSql)
				uf_perguntalt_chama("OCORREU UM ERRO A CALCULAR INFORMACAO PARA A IMPRESSAO. POR FAVOR CONTACTE O SUPORTE.","OK","", 16)
 				RETURN
 			ENDIF
		
			SELECT ft
			SELECT tempFi
			GO TOP
		ENDIF
		Select td
		If (TD.u_tipodoc)==1
			Select tempFi
			GO TOP
		Else
			uf_perguntalt_chama("O DOCUMENTO DAS ENTIDADE EST� MAL CONFIGURADO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			Return
		Endif
	Endif
	
	** cria cursor texto rodap�
	uf_gerais_TextoRodape('FT')

	SELECT td
	GO TOP
	Select ft
	GO TOP
	SELECT FTQRCODE
	GO TOP 

	SET PROCEDURE TO LOCFILE("FoxBarcodeQR.prg") ADDITIVE
	PRIVATE poFbc
	m.poFbc = CREATEOBJECT("FoxBarcodeQR")

	m.poFbc.nCorrectionLevel = 3 
	m.poFbc.nBarColor = RGB(0, 0, 0)

	CREATE CURSOR TempQR (TempQR I)
	INSERT INTO TempQR VALUES (0)
	

	&& Cria codigo Data Matrix para impressao
	*uf_imprimirgerais_dataMatrixCriaImagem(ft.ndoc, compOrg1, compOrg2, compOrg3, compOrg4, compOrg5, compOrg6, compOrg7, compOrg8, compOrg9)
	uf_imprimirgerais_dataMatrixCriaImagem(ft.ndoc)
*!*		uf_gerais_actgrelha("", "FTQRCODE", "exec up_print_qrcode_a4 '"+ALLTRIM(ft.ftstamp)+"'")
	Select ft2
	Go TOP
	SELECT uCrsTextoRodape
	GO TOP
	If (TD.u_tipodoc)==1
		Select tempFi
		GO TOP
	ELSE	
		Select fi
		Go top
	ENDIF

	** define impressora **
	If !(alltrim(tcPrinter) == "Impressora por defeito do Windows")
		Set Printer To Name ''+alltrim(tcPrinter)+''
	Endif
	
	** Trata Duplicados **
	If lcTemDuplicados == .t. And lcNumDuplicados > 0
		For i=0 To lcNumDuplicados
			** IMPRIMIR
			myVersaoDoc = uf_gerais_versaodocumento(i+1)
			Report Form Alltrim(lcReport) To Printer Noconsole
		Endfor
	ELSE
		** IMPRIMIR
		myVersaoDoc=uf_gerais_versaodocumento(1)
		Report Form Alltrim(lcReport) To Printer Noconsole
	Endif
	
	USE IN TempQR
	m.poFbc = NULL
	
	SET PRINTER TO DEFAULT
ENDFUNC


**
FUNCTION uf_facturacaoentidades_selTodos

	IF EMPTY(facturacaoentidades.menu1.seltodos.tag) OR facturacaoentidades.menu1.seltodos.tag == "false"
		&& aplica sele��o
		SELECT uCrsEFL
		REPLACE ALL sel WITH .t.
		
		&& altera o botao
		facturacaoentidades.menu1.selTodos.tag = "true"
		facturacaoentidades.menu1.selTodos.config("Sel. Todos", myPath + "\imagens\icons\checked_w.png", "uf_facturacaoentidades_selTodos","T")

	ELSE
		&& aplica sele��o
		SELECT uCrsEfl
		REPLACE ALL sel WITH .f.
		
		&& altera o botao
		facturacaoentidades.menu1.selTodos.tag = "false"
		facturacaoentidades.menu1.selTodos.config("Sel. Todos", myPath + "\imagens\icons\unchecked_w.png", "uf_facturacaoentidades_selTodos","T")	
	ENDIF
		
	SELECT uCrsEfl
	GO TOP
	
	facturacaoentidades.grdEfl.setfocus
	facturacaoentidades.lstEntidade.setfocus
ENDFUNC


**
FUNCTION uf_facturacaoentidades_cursorEntidades
	LPARAMETERS lcAno, lcMes
	
	LOCAL lcSql
	lcSQL = ''
	
	facturacaoentidades.lstEntidade.rowsource 	= ''
	
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_receituario_listEntidadesPorData <<lcAno>>, <<lcMes>>, '<<ALLTRIM(mysite)>>'
	ENDTEXT
	If !uf_gerais_actgrelha("", "uCrsEflEntidades", lcSql)
		uf_perguntalt_chama('OCORREU UM ERRO AO CALCULAR AS ENTIDADES! POR FAVOR CONTACTE O SUPORTE.',"OK","",16)
		RETURN .f.
	ENDIF
	
	facturacaoentidades.lstEntidade.rowsource 		= 'uCrsEflEntidades.cptorgabrev'
	facturacaoentidades.lstEntidade.rowsourcetype 	= 2
	
	facturacaoentidades.lstEntidade.refresh
ENDFUNC


**  Selecciona o Ano
FUNCTION uf_facturacaoentidades_selAno
	
	&& Mes
	facturacaoentidades.cmbMes.Value 			= ''
	facturacaoentidades.cmbMes.displayValue		= ''

	&& Entidades
	SELECT uCrsEflEntidades
	DELETE all
	facturacaoentidades.lstEntidade.refresh()
	
	myfacturacaoentidadesAno = VAL(ALLTRIM(facturacaoentidades.cmbAno.value))
	myfacturacaoentidadesMes = 0
ENDFUNC



** Selecciona o Mes
FUNCTION uf_facturacaoentidades_selMes

	IF empty(facturacaoentidades.cmbAno.value)
		RETURN .F.
	ENDIF

	&& Cursor de Entidades
	myfacturacaoentidadesMes = uf_gerais_getMonthComboBox("facturacaoentidades.cmbMes")

	uf_facturacaoentidades_cursorEntidades(myfacturacaoentidadesAno, myfacturacaoentidadesMes)

	** carrega factura��o
	uf_facturacaoentidades_cursorEfl(.t.)
ENDFUNC



** Selecciona uma Entidade
FUNCTION uf_facturacaoentidades_selEntidade

	IF empty(facturacaoentidades.cmbAno.value)
		RETURN .F.
	ENDIF
	

	** Filtro � Grelha de factura��o
	uf_facturacaoentidades_filtraFacturacao()
ENDFUNC



**
FUNCTION uf_facturacaoentidades_filtraFacturacao


	SELECT uCrsEFL
	IF reccount("uCrsEFL")>0
		IF !Empty(facturacaoentidades.lstEntidade.value) AND !(Upper(Alltrim(facturacaoentidades.lstEntidade.value)) == "TODAS")
			SET Filter To Upper(Alltrim(uCrsEFL.entidades)) == Upper(Alltrim(facturacaoentidades.lstEntidade.value))
			SELECT * FROM uCrsEFL WITH (buffering =.t.) INTO CURSOR uCrsEFL1 READWRITE WHERE Upper(Alltrim(uCrsEFL.entidades)) == Upper(Alltrim(facturacaoentidades.lstEntidade.value))
		ELSE
			SET Filter To
			SELECT * FROM uCrsEFL WITH (buffering =.t.) INTO CURSOR uCrsEFL1 READWRITE
		ENDIF

		SELECT uCrsEFL
		GO TOP

		facturacaoentidades.grdEfl.refresh
		
		
	ENDIF
	
	uf_facturacaoentidades_resultado_envio()
	
ENDFUNC


** Fun��es para envio de factura��o ANF e AFP
FUNCTION uf_facturacaoentidades_envioFactAnf
	LOCAL lcSQL
	
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE
		select 
			abrev
			,id_anf
			,ISNULL(mcdt,0) as isMcdt
			,ISNULL(prestacao,0) as prestacao
		from 
			cptorg(nolock) 
		where 
			cptorg.abrev = '<<ALLTRIM(facturacaoentidades.lstEntidade.value)>>' 
			AND cptorg.id_anf != ''
	ENDTEXT 
	
	If !uf_gerais_actgrelha("", "uCrsEnviFtEntidades", lcSql)
		uf_perguntalt_chama('OCORREU UM ERRO AO CALCULAR AS ENTIDADES! POR FAVOR CONTACTE O SUPORTE.',"OK","",16)
		RETURN .f.
	ENDIF
	
	SELECT uCrsEnviFtEntidades	
	
	&& se nao for mcdt 
	if(EMPTY(uCrsEnviFtEntidades.isMcdt))
		
		*!*		DO CASE 
		SELECT uCrsE1
		GO TOP
		if(UPPER(ALLTRIM(uCrsE1.u_assfarm)) == "AFP")
			uf_perguntalt_chama("N�o pode gerar ficheiro para a entidade seleccionada.", "Oks", "", 48)
			RETURN .f.
		ENDIF
		 
		
		IF RECCOUNT("uCrsEnviFtEntidades") > 0 
			uf_facturacaoentidades_envioFactEntAnf(UPPER(ALLTRIM(facturacaoentidades.lstEntidade.value)), myfacturacaoentidadesAno, myfacturacaoentidadesMes, ALLTRIM(uCrsEnviFtEntidades.id_anf))
		ELSE
			uf_perguntalt_chama("N�o pode gerar ficheiro para a entidade seleccionada.", "Ok", "", 48)
		ENDIF
	ELSE
		uf_facturacaoentidades_mcdt()		
	ENDIF	
ENDFUNC



** FUNCAO PARA EMISS�O DO FICHEIRO DE FACTURACAO DETALHADA DAS ENTIDADES, PARA ASSOCIADOS DA ANF
FUNCTION uf_facturacaoentidades_envioFactEntAnf
	Lparameters tcEntAbrev, tcAno, tcMes, lcSubStrFicheiro
	
	** Variaveis texto e boolean
	LOCAL lcAnoMes, lcNomeMes, lcUltFacEnt, lcSql, lcAnfUser, lcAnfPass, lcMdir, lc7ZipPath, lcXmlGeral, lcXmlDetalhe, lcChecError, lcCommand, lcXmlZIP, lcDiretoriaLocal  
	Store "" To lcAnoMes,lcNomeMes, lcSql, lcMdir,lc7ZipPath,lcXmlGeral,lcXmlDetalhe,lcCommand,lcXmlZIP
	STORE .f. TO lcCheckError
	lcDiretoriaLocal = GETENV('TEMP')

	** Definir string para meter no nome do ficheiro
	lcAnoMes = Alltrim(Str(tcAno)) + Iif(Len(Alltrim(Str(tcMes)))=1,"0"+Alltrim(Str(tcMes)),Alltrim(Str(tcMes)))

	** Validar INPUT � fun��o
	If Empty(tcEntAbrev) Or Empty(tcAno) Or Empty(tcMes)
		uf_perguntalt_chama("Faltam argumentos para correr esta fun��o", "Ok", "", 48)
		RETURN .f.
	ENDIF 
		
	** Guardar nome do m�s por extenso para usar na MessageBox		
	lcNomeMes = Upper(uf_gerais_calculames(Str(tcMes)))
	
	** Confirmar o envio, oferecendo por defeito a op��o de NAO enviar o ficheiro de factura��o detalhada
	If !uf_perguntalt_chama("ATEN��O: Deseja gerar o ficheiro da "+tcEntAbrev+" relativo ao ano de " + Alltrim(Str(tcAno)) + " e do m�s de " + Alltrim(lcNomeMes) + CHR(13)+CHR(13) + "O processo poder� demorar alguns minutos.", "Sim", "N�o")
		RETURN .f.
	Endif
		
	** Obter utilizador e password, separados por virgula, para autenticar no WebService
	lcAnfUser = ""
	SELECT ucrsLojas
	GO TOP
	SCAN FOR ALLTRIM(UPPER(ucrsLojas.site)) == ALLTRIM(UPPER(mysite))
		IF EMPTY(ucrsLojas.anfpassfx)
			uf_perguntalt_chama("Os dados para emiss�o do ficheiro n�o est�o corretamente preenchidos. Por favor contacte o Suporte.", "Ok", "", 48)
			RETURN .f.
		ELSE
			lcAnfUser = ALLTRIM(ucrsLojas.anfpassfx)
		ENDIF
		
	ENDSCAN

	lcAnfPass = lcAnfUser

	** Validar o total de WORDS diferentes; devem ser um maximo de 2!
	If Getwordcount(lcAnfUser,";") == 2
			
		** Validar o parametro posi��o 1, utilizador		
		lcAnfUser = Alltrim(getwordnum(lcAnfUser,1,";"))
		IF EMPTY(lcAnfUser)
			uf_perguntalt_chama("N�o configurou correctamente o identificador ANF da farm�cia.", "Ok", "", 48)
			RETURN .f.
		ENDIF

		** Validar o parametro posi��o 2, password
		lcAnfPass = Alltrim(getwordnum(lcAnfPass,2,";"))
		IF EMPTY(lcAnfPass)
			uf_perguntalt_chama("N�o configurou correctamente a password do ficheiro zip.", "Ok", "", 48)
			RETURN .f.
		ENDIF
			
	ELSE
		uf_perguntalt_chama("Imposs�vel determinar a identifica��o completa ANF da farm�cia." +CHR(10)+CHR(10)+ "Por favor contacte o suporte.", "Ok", "", 48)
		RETURN .f.
	ENDIF
	
	
	** Validar o codigo ANF da farmacia, tem que ser usado no nome do ficheiro (nao o IDENT mas sim o CODIGO ANF 5 digitos)
	LOCAL lcCodigoANF
	SELECT uCrsE1
	lcCodigoANF = uCrsE1.u_codfarm
	
	** Preencher, conforme requisito obrigatorio da ANF, o codigo da farm�cia com zeros at� ter 5 digitos
	lcCodigoANF = Padl(Transform(ALLTRIM(lcCodigoANF)),5,"0")
	
	
	** VALIDAR SE HA DOCUMENTOS (FACTURAS; RECEITAS; ETC) A ENVIAR PARA A ENTIDADE, ANO E MES INDICADOS
	TEXT TO lcSql NOSHOW TEXTMERGE
		SELECT
			TotalDocs = count(ft.ftstamp)		
		from
			ft (nolock)
			inner join ctltrct (nolock) on (ft.u_ltstamp=ctltrctstamp or ft.u_ltstamp2=ctltrctstamp)
		where
			ctltrct.ano					= <<tcAno>>
			and ctltrct.mes				= <<tcMes>>
			and ctltrct.cptorgabrev		= '<<Alltrim(tcEntAbrev)>>'
			and ctltrct.site			in (select * from up_SplitToTable('<<uf_gerais_getSitePostos()>>',','))
	ENDTEXT
	If !uf_gerais_actgrelha("", [uCrsDocCount], lcSql)
		uf_perguntalt_chama("Ocorreu uma anomalia a verificar a exist�ncia de dados," +CHR(10)+CHR(10)+ "Por favor contacte o Suporte.", "Ok", "", 64)
		RETURN .f.
	ELSE
		SELECT uCrsDocCount
		If Reccount("uCrsDocCount")>0
			If uCrsDocCount.TotalDocs=0
				uf_perguntalt_chama("N�o existem documentos a enviar � entidade." +CHR(10)+CHR(10)+ "Por favor contacte o suporte.", "Ok", "", 48)
				RETURN .f.
			Endif
		ELSE
			uf_perguntalt_chama("Imposs�vel determinar a quantidade de documentos a enviar � entidade." +CHR(10)+CHR(10)+ "Por favor contacte o suporte.", "Ok", "", 64)
			RETURN .f.
		ENDIF

		Fecha("uCrsDocCount")
	ENDIF
	
	
	
		
		
	** Determinar numero da ULTIMA factura para este ANO, MES e ENTIDADE a ser enviada via Webservice
	TEXT TO lcSql NOSHOW TEXTMERGE
		select top 1
			fno
		from
			ft (nolock)
		where
			ndoc = <<mySEnt>>
			and ftano = <<tcAno>>
			and month(fdata) = <<tcMes>>
			and no = (select top 1 no from b_utentes (nolock) where no=(select u_no from cptorg (nolock) where abrev='<<Alltrim(tcEntAbrev)>>'))
			and site = '<<ALLTRIM(mySite)>>'
		order by
			fdata desc
	ENDTEXT
	
	If uf_gerais_actgrelha("", [uCrsUltFacEntGr], lcSql)
		If Reccount("uCrsUltFacEntGr")>0
			lcUltFacEnt = uCrsUltFacEntGr.fno
		Endif
		Fecha("uCrsUltFacEntGr")
	Endif
		
	If Empty(lcUltFacEnt)
		uf_perguntalt_chama("N�o foi encontrada factura emitida � entidade no ano e m�s em quest�o. Deve primeiro emitir a fatura � entidade.", "Ok", "", 48)
		RETURN .f.
	Endif


	** Pedir caminho do ficheiro a gravar em disco
	oShell = CREATEOBJECT("Shell.Application")
	**oFolder = oShell.Application.BrowseForFolder(_screen.HWnd,"Seleccione a Pasta de Destino", 1)
        
	oFolder = ALLTRIM(uf_gerais_getUmValor("B_Terminal", "LTRIM(RTRIM(pathExport))", "terminal = '" + alltrim(myTerm) + "'"))
	uf_gerais_createDir(oFolder)

	**
	IF isnull(oFolder)
		uf_perguntalt_chama("Aten��o: O caminho para o ficheiro n�o pode ser vazio.", "Ok", "", 48)
		RETURN .f.
	ELSE
		** Definir variavel com o PATH seleccionado com o utilizador, e usar a variavel para o nome do ficheiro XML de Detalhe
		**lcMdir= oFolder.self.path

        lcMdir = oFolder
		
		lcXmlDetalhe = ALLTRIM(lcMdir) + "\" + ALLTRIM(lcCodigoANF) + "_" + UPPER(ALLTRIM(lcSubStrFicheiro)) + "_" + lcAnoMes + '.xml'
	    lcXmlDetalhe = ALLTRIM(lcDiretoriaLocal) + "\" + ALLTRIM(lcCodigoANF) + "_" + UPPER(ALLTRIM(lcSubStrFicheiro)) + "_" + lcAnoMes + '.xml'
	ENDIF
	

	
	


*!*	    uv_path = ALLTRIM(uf_gerais_getUmValor("B_Terminal", "LTRIM(RTRIM(pathExport))", "terminal = '" + alltrim(myTerm) + "' and site = '" + alltrim(mySite) + "'"))
*!*	    
*!*	    MESSAGEBOX(uv_path)

*!*	    IF EMPTY(uv_path)

*!*			uf_perguntalt_chama('Aten��o!' + chr(13) + 'N�o preencheu o par�metro "Caminho Export.".' + chr(13) +  'Ter� de preencher o mesmo para continuar.', "Ok", "", 48)
*!*			RETURN .f.

*!*	    ENDIF


*!*	    lcXmlDetalhe = uv_path + "\" + ALLTRIM(lcCodigoANF) + "_" + lcSubStrFicheiro + "_" + lcAnoMes + '.xml'
	
	
	** Cleanup
	oShell = null


	** Validar se Sistema � x64, tem 7z x64 instsalado? gaurdar o PATH aonde o 7z esta instalado, e usar no comando 7z depois. nem sempre em todos os sistemas o 7z esta na PATH 
	IF !DIRECTORY(GETENV("ProgramW6432")+"\7-Zip")
		IF !DIRECTORY(GETENV("ProgramFiles")+"\7-Zip")
			IF!DIRECTORY(GETENV("ProgramFiles(x86)")+"\7-Zip")
				uf_perguntalt_chama("AVISO: O software de compress�o est� em falta." +CHR(10)+CHR(10)+ "Por favor contacte o suporte.", "Ok", "", 48)
				RETURN .f.
			ELSE
				lc7ZipPath = GETENV("ProgramFiles(x86)")+"\7-Zip"
			ENDIF
		ELSE
			lc7ZipPath = GETENV("ProgramFiles")+"\7-Zip"
		ENDIF
	ELSE
		lc7ZipPath = GETENV("ProgramW6432")+"\7-Zip"	
	ENDIF
		
		

	** Indicador de progresso: 0 de 3 passos de comunica��o
	regua(0,3,"A obter dados de Factura��o Detalhada...")
	

	** OBTER FICHEIRO DE FACTURA��O DETALHADA PARA CAMPO MEMO PHC, GUARDAR EM CURSOR
	TEXT TO lcSql NOSHOW TEXTMERGE
		exec up_receituario_GerarFileANF <<tcAno>>, <<tcMes>>, '<<Alltrim(tcEntAbrev)>>', '<<alltrim(mySite)>>'
	ENDTEXT

	IF !uf_gerais_actgrelha("", [uCrsAnfAdse], lcSql)
		uf_perguntalt_chama("Erro a obter dados para ficheiro de factura��o detalhada da ANF." +CHR(10)+CHR(10)+ "Por favor contacte o suporte.", "Ok", "", 16)
		RETURN .f.
	ENDIF

	** Indicador de progresso: iniciar protec��o do ficheiro de Factura��o Detalhada
	REGUA(1,3,"A gerar Ficheiro de Factura��o Detalhada ANF...")

	Try
		** Escrever para disco
		SELECT uCrsAnfAdse
		COPY MEMO uCrsAnfAdse.anf TO ALLTRIM(lcXmlDetalhe)

		Fecha("uCrsAnfAdse")
	Catch
		uf_perguntalt_chama("Ocorreu uma anomalia a gerar o ficheiro de base da factura��o detalhada." +CHR(10)+CHR(10)+ "Por favor contacte o suporte.", "Ok", "", 16)
		Fecha("uCrsAnfAdse")
		regua(2)
		lcCheckError = .t.
	ENDTRY
	
	IF lcCheckError = .t.
		RETURN .f.
	Endif


	** Novo aviso que vai ser compactado o novo ficheiro
	REGUA(3,3,"A compactar e proteger Ficheiro de Factura��o Detalhada ANF...")
		
		
	** Alterar a variavel para termos o nome do ficheiro ZIP a criar; definir comando 7Zip para compactar e proteger o ficheiro
	lcXmlZIP = STRTRAN(lcXmlDetalhe,".xml",".zip")
	lcXmlZIP  = UPPER(lcXmlZIP)
	lcCommand = CHR(34) + ALLTRIM(lc7ZipPath)  + "\7z" + CHR(34) + " a -tzip -p" + lcAnfPass + " " + lcXmlZIP + " " + lcXmlDetalhe

	** Criar o objecto Shell e correr o comando definido acima
	oShell = CREATEOBJECT("WScript.Shell")
	oShell.Run(lcCommand,1,.T.)

	IF !FILE(lcXmlZIP)
		uf_perguntalt_chama("Erro a comprimir ficheiro de factura��o detalhada para prote��o adicional." +CHR(10)+CHR(10)+ "Por favor contacte o suporte.", "Ok", "", 64)
		regua(2)
		RETURN .f.
	ELSE
		DELETE FILE (lcXmlDetalhe)	
	Endif


	** Validar se conseguimos carregar o ficheiro j� compactado e protegido. Importar o ficheiro usando convers�o para Base64 binary.
	LOCAL lcFileDump,gnFileHandle,nSize,cString
	STORE "" TO lcFileDump

	** Criar HANDLE para abertura de baixo nivel do ficheiro
	gnFileHandle = FOPEN(lcXmlZIP)

	** Determinar tamanho do ficheiro, para irmos ate ao fim
	nSize =  FSEEK(gnFileHandle, 0, 2)
	IF nSize <= 0
		uf_perguntalt_chama("Ficheiro de factura��o detalahada protegido est� vazio." +CHR(10)+CHR(10)+ "Por favor contacte o suporte.", "Ok", "", 64)
		RETURN  .f.
	ELSE
	 	FSEEK(gnFileHandle, 0, 0)     
		lcFileDump = FREAD(gnFileHandle, nSize)
		
		IF empty(lcFileDump)
			uf_perguntalt_chama("Imposs�vel recarregar ficheiro zip protegido." +CHR(10)+CHR(10) + "Por favor contacte o suporte.", "Ok", "", 16)
			regua(2)
			RETURN .f.
		ELSE
			** Codificar em Base64 o conteudo binario lido a baixo nivel
			lcFileDump = STRCONV(lcFileDump,13)
		Endif
	ENDIF
	
	
	** Fechar o HANDLE e o ficheiro para poder ser apagado
	FCLOSE(gnFileHandle)


	** limpar o ficheiro XML original, ja foi compactado,protegido e importado para Base64
	IF FILE(lcXmlZIP)
		DELETE FILE (lcXmlZIP)
	ENDIF

	
	** Gerar o XML de cabe�alho que ira incluir o novo ficheiro
	TEXT TO lcSql NOSHOW textmerge
		exec up_receituario_GerarCabFileANF <<tcAno>>, <<tcMes>>, '<<Alltrim(tcEntAbrev)>>', '<<alltrim(mySite)>>'
	ENDTEXT
	
	
	** correr o query e apanhar o XML em output para ficheiro de texto
	IF !uf_gerais_actgrelha("", [uCrsXmlGeral], lcSql)
		uf_perguntalt_chama("Erro a gerar ficheiro final." +CHR(10)+CHR(10)+ "Por favor contacte o suporte.", "Ok", "", 16)
		regua(2)
		RETURN .f.
	ELSE
		** validar se ha registos no Cursor, SQL pode correr e nao ter registos
		IF RECCOUNT("uCrsXmlGeral") > 0

			TRY
				** Como nao podemos passar o ficheiro como parametro a SP, fazer REPLACE euqnanto esta em RAM para trocarmos o valor fixo pelo verdadeiro DUMP do ficheiro
				SELECT uCrsXmlGeral
				REPLACE uCrsXmlGeral.anf2 WITH STRTRAN(uCrsXmlGeral.anf2,"KEYWORD_TO_BE_REPLACED_LATER",alltrim(lcFileDump))
				COPY MEMO uCrsXmlGeral.anf2 TO ALLTRIM(lcXmlDetalhe)
					

				** Compactar com password NOVAMENTE o mesmo ficheiro, que ja contem o string encriptado e convertido para Base64
				lcCommand = CHR(34) + ALLTRIM(lc7ZipPath)  + "\7z" + CHR(34) + " a -tzip -mx=1 -mmt=off -p" + lcAnfPass + " " + lcXmlZIP + " " + ALLTRIM(lcXmlDetalhe)


				** Criar o objecto Shell e correr o comando definido acima
				oShell = CREATEOBJECT("WScript.Shell")
				oShell.Run(lcCommand,1,.T.)
				oShell = null
				fecha("uCrsXmlGeral")
				
				uf_perguntalt_chama("Ficheiro Gerado com Sucesso.", "Ok", "", 64)
			
			CATCH
				uf_perguntalt_chama("Erro a escrever ficheiro final." +CHR(10)+CHR(10)+ "Por favor contacte o suporte.", "Ok", "", 16)
				
			FINALLY
				regua(2)


				**Copiar Ficheiro Para localiza��o escolhida pelo utilizador
				**COPY FILE(lcXmlDetalhe) TO (lcMdir)	

				uv_destino = ALLTRIM(lcMdir) + "\" + JUSTFNAME(lcXmlZIP)
				
				IF !uf_gerais_copyFile(lcXmlZIP, uv_destino)
					uf_perguntalt_chama("Erro a copiar o ficheiro." + CHR(13) + "Por favor contacte o suporte.", "OK", "", 48)
				ENDIF


				**COPY FILE(lcXmlZIP) TO (lcMdir)	
				
				IF FILE(lcXmlDetalhe)
					DELETE FILE (lcXmlDetalhe)
				ENDIF

			ENDTRY

		ELSE
			uf_perguntalt_chama("N�o foram encontrados dados para gerar ficheiro final." +CHR(10)+CHR(10)+ "Por favor contacte o suporte.", "Ok", "", 48)
			fecha("uCrsXmlGeral")
			regua(2)
			RETURN .f.
		ENDIF

	ENDIF

	** CLEANUP
	If Used("uCrsAnfAdse")
		fecha("uCrsAnfAdse")
	ENDIF
	
	IF USED("uCrsFinalFile")
		fecha("uCrsFinalFile")
	ENDIF
	
	IF USED("uCrsDocCount")
		fecha("uCrsDocCount")
	ENDIF
	
	IF USED("uCrsXmlGeral")
		fecha("uCrsXmlGeral")
	ENDIF
ENDFUNC


** FUNCAO PARA ENVIO DO FICHEIRO DE FACTURACAO DETALHADA
FUNCTION uf_facturacaoentidades_envioFact
	
	LOCAL lcEntidade, lcWSDLPath
	
	**
	** Validar entidades suportadas
	**
	IF !Empty(facturacaoentidades.lstEntidade.value) AND !(Upper(Alltrim(facturacaoentidades.lstEntidade.value)) == "TODAS")
		DO CASE
			CASE UPPER(ALLTRIM(facturacaoentidades.lstEntidade.value)) == "ADSE"
				lcEntidade = "ADSE"
				lcWSDLPath = uf_gerais_getParameter('ADM0000000154', 'TEXT')

			CASE UPPER(ALLTRIM(facturacaoentidades.lstEntidade.value)) == "LILLY"
				lcEntidade = "LILLY"
				*lcWSDLPath = uf_gerais_getParameter('ADM0000000154', 'TEXT')
		ENDCASE
	ENDIF
	
	
	IF !EMPTY(lcEntidade)
		
		**
		** correr valida��es partilhadas antes de chamar as fun��es pescif�cas da entidade
		**
		
		** Validar isoladamente se conseguimos aceder ao objecto SOAP Toolkit. 
		** Caso contrario, definir variavel de Check a false e abortar o codigo.
		LOCAL oWebServADSE_AFP_teste, lcCheckObject
		STORE .t. TO lcCheckObject
		
		TRY 
			oWebServADSE_AFP_teste = CREATEOBJECT("mssoap.soapclient30")
			oWebServADSE_AFP_teste = null
		
		CATCH TO lcError WHEN .t.
			lcCheckObject = .f.
			
		ENDTRY
		
		IF lcCheckObject == .f.
			uf_perguntalt_chama("Imposs�vel aceder � framework de valida��o XML." +CHR(10)+CHR(10)+ "Por favor contacte o suporte.", "Ok", "", 16)
			RETURN .f.
		ENDIF
		***********************************************************************
		
		
		** RE-VALIDAR NUMERO DE CONTRIBUINTE
		SELECT uCrsE1
		If Reccount("uCrsE1")>0
			If Empty(uCrsE1.ncont)
				uf_perguntalt_chama("Por favor preencha correctamente o NIF da farm�cia / empresa na ficha da empresa.", "Ok", "", 48)
				RETURN .f.
			ENDIF
		ENDIF
	
	
		** Validar INPUT � fun��o
		LOCAL lcAnoMes, lcNomeMes
		IF Empty(myfacturacaoentidadesAno) Or Empty(myfacturacaoentidadesMes)
			uf_perguntalt_chama("O ano e m�s devem estar correctamente selecionados.", "Ok", "", 48)
			RETURN .f.
		ELSE
			** Guardar ano e mes na mesma string para serem enviados como parametro do WebService mais tarde. Mes tem que ter 2 digitos.
			lcAnoMes = Alltrim(Str(myfacturacaoentidadesAno)) + Iif(Len(Alltrim(Str(myfacturacaoentidadesMes)))=1,"0"+Alltrim(Str(myfacturacaoentidadesMes)),Alltrim(Str(myfacturacaoentidadesMes)))
			lcNomeMes = Upper(uf_gerais_calculames(Str(myfacturacaoentidadesMes)))
		ENDIF
		
		IF lcEntidade == "ADSE"
			** Ler e validar o parametro de configura��o do caminho do ficheiro WSDL do WebService
			lcWSDLPath = Alltrim(mypath) + "\" + Alltrim(lcWSDLPath)
			
			** Validar se o ficheiro existe ou temos permiss�o de leitura.
			IF File(lcWSDLPath) == .F.
				uf_perguntalt_chama("Erro a aceder ao ficheiro de configura��o do webservice de factura��o." +CHR(10)+CHR(10)+ "Por favor contacte o suporte.", "Ok", "", 16)
				RETURN
			ENDIF
		ENDIF
	
		** Confirmar o envio, oferecendo por defeito a op��o de NAO enviar o ficheiro de factura��o detalhada
		IF !uf_perguntalt_chama("ATEN��O: Deseja enviar o ficheiro da " +lcEntidade+ " relativo ao ano de " + Alltrim(Str(myfacturacaoentidadesAno)) + " e do m�s de " + Alltrim(lcNomeMes) + CHR(13)+CHR(13) + "Se seleccionar 'SIM' o ficheiro ser� enviado � entidade!", "Sim", "N�o")
			RETURN .f.
		ENDIF
	
	
		DO CASE
			CASE lcEntidade == "ADSE"
				uf_facturacaoentidades_envioFactAfp(myfacturacaoentidadesAno, myfacturacaoentidadesMes, lcAnoMes, lcNomeMes, lcWSDLPath)

			CASE lcEntidade == "LILLY"
				*uf_facturacaoentidades_envioFactLilly(myfacturacaoentidadesAno, myfacturacaoentidadesMes, lcAnoMes, lcNomeMes, lcWSDLPath)
				uf_facturacaoEntidades_ChamaWEbServiceLILLY(myfacturacaoentidadesAno,myfacturacaoentidadesMes,lcEntidade)
		ENDCASE
	ELSE
		uf_perguntalt_chama("N�o pode enviar fatura��o para a entidade selecionada.", "Ok", "", 48)		
	ENDIF
ENDFUNC


** FUNCAO PARA ENVIO DO FICHEIRO DE FACTURACAO DETALHADA VIA WEBSERVICE DA ADSE, PARA ASSOCIADOS DA AFP
Function uf_facturacaoentidades_envioFactAfp
	Lparameters tcAno, tcMes, lcAnoMes, lcNomeMes, lcWSDLPath
	
	** Obter o ultimo numero de Documento de Factura�� � Entidade indicada criado na BD
	Local lcSql, lcOutput, oProxy, lcXml, lcSql, lcWSDLPath, lcWSDLParams, lcWSDLUser, lcWSDLPass, lcErrorString, lcOutVar,lcOutVar2
	Store "" To lcSql, lcErrorString, lcOutVar, lcOutVar2, lcWSDLParams
	
	** Esta variavel era publica antes mas agora existe em cada funcao.
	LOCAL tcEntAbrev
	tcEntAbrev = UPPER(ALLTRIM(facturacaoentidades.lstEntidade.value))
	
	** Array STRING simples para guardar o OUTPUT do WebService
	Dimension lcOutArray[3]
	
	** Obter utilizador e password, separados por virgula, para autenticar no WebService
	lcWSDLParams = uf_gerais_getParameter('ADM0000000153', 'TEXT')

	** Validar o total de WORDS diferentes; devem ser um maximo de 2!
	IF Getwordcount(lcWSDLParams,";") == 2
			
		** Validar o parametro posi��o 1, utilizador		
		lcWSDLUser = Alltrim(getwordnum(lcWSDLParams,1,";"))
		
		IF EMPTY(lcWSDLUser)
			uf_perguntalt_chama("N�o configurou correctamente o utilizador do webservice.", "Ok", "", 16)
			RETURN
		ENDIF

		** Validar o parametro posi��o 2, password
		lcWSDLPass = Alltrim(getwordnum(lcWSDLParams,2,";"))
		
		IF EMPTY(lcWSDLPass)
			uf_perguntalt_chama("N�o configurou correctamente a password do webservice.", "Ok", "", 16)
			RETURN .f.
		ENDIF
	ELSE
		uf_perguntalt_chama("Ocorreu um erro a validar configura��o do webservice." +CHR(10)+CHR(10)+ "Por favor contacte o suporte.", "Ok", "", 16)
		RETURN .f.
	ENDIF


	** VALIDAR SE HA DOCUMENTOS A ENVIAR PARA A ENTIDADE, ANO E MES INDICADOS
	Text To lcSql Noshow textmerge
		select
			TotalDocs = count(ft.ftstamp)
		from
			ft (nolock)
			inner join ctltrct (nolock) on (ft.u_ltstamp=ctltrctstamp or ft.u_ltstamp2=ctltrctstamp)
		where
			ctltrct.ano						= <<tcAno>>
			and ctltrct.mes					= <<tcMes>>
			and ctltrct.cptorgabrev			= '<<Alltrim(tcEntAbrev)>>'
			and ctltrct.site				= '<<ALLTRIM(mysite)>>'
	ENDTEXT
	
	IF !uf_gerais_actgrelha("", [uCrsDocCount], lcSql)
		uf_perguntalt_chama("Erro a determinar se h� dados a enviar � entidade." +CHR(10)+CHR(10)+ "Por favor contacte o suporte.", "Ok", "", 16)
		RETURN
	ELSE
		Select uCrsDocCount
		If Reccount("uCrsDocCount") > 0
			If uCrsDocCount.TotalDocs=0
				uf_perguntalt_chama("N�o h� documentos a enviar � entidade. Por favor seleccione o ano e m�s correctos a enviar.", "Ok", "", 48)
				Fecha("uCrsDocCount")
				RETURN
			ENDIF
		ELSE
			uf_perguntalt_chama("N�o foram encontrados documentos a enviar � entidade.", "Ok", "", 48)
			Fecha("uCrsDocCount")
			RETURN
		ENDIF

		Fecha("uCrsDocCount")
	ENDIF
		
		
	** Determinar numero da ULTIMA factura para este ANO, MES e ENTIDADE a ser enviada via Webservice
	Text To lcSql Noshow textmerge
		select top 1
			fno
		from
			ft (nolock)
		where 
			ndoc=<<mySEnt>>
			and ftano=<<tcAno>>
			and month(fdata)=<<tcMes>>
			and no=(select top 1 no from b_utentes (nolock) where no=(select u_no from cptorg (nolock) where abrev='<<Alltrim(tcEntAbrev)>>'))
			and site = '<<ALLTRIM(mySite)>>'
	Endtext
	
	If uf_gerais_actgrelha("", [uCrsUltFacEntGr], lcSql)
		If Reccount("uCrsUltFacEntGr") = 0
			uf_perguntalt_chama("N�o foi encontrada factura emitida � entidade no ano e m�s em quest�o. Deve primeiro facturar os lotes.", "Ok", "", 48)
			Return				
		Endif
		Fecha("uCrsUltFacEntGr")
	Endif


	** Determinar o ID sequencial da factura de cada mes. Se o NIF em varias farmacias for o mesmo, diferenciar com o parametro ADM0000000170 o ID de sequencia especifico por cada NIF
	** Cada membro do mesmo NIF ter� um ID sequencial unico, a ser preenchido no parametro ADM0000000170 
	LOCAL lcnSeqFactNIF
	
	lcnSeqFactNIF = uf_gerais_getParameter('ADM0000000170', 'TEXT')
	
	IF EMPTY(lcnSeqFactNIF)
		lcnSeqFactNIF = "01"						
	ELSE
		** Validar se o LENGHT() deste string � no maximo 2 caracteres, pode ser mal preenchido por erro.
		** Deve sempre por requisito do WebService ser enviado com 2 digitos.
		lcnSeqFactNIF  = Iif(Len(Alltrim(lcnSeqFactNIF))=1, "0"+Alltrim(lcnSeqFactNIF), Alltrim(lcnSeqFactNIF))
	ENDIF


	** OBTER FICHEIRO DE FACTURA��O DETALHADA PARA CAMPO MEMO PHC, GUARDAR EM CURSOR
	text to lcSql noshow textmerge
		exec up_receituario_genFactEnt '<<Alltrim(tcEntAbrev)>>', <<tcAno>>, <<tcMes>>, '<<ALLTRIM(uf_gerais_getSitePostos())>>'
	Endtext
	
	** Indicador de progresso: 0 de 3 passos de comunica��o
	regua(0,3,"A obter dados para factura��o...")

	if !uf_gerais_actgrelha("", [uCrsXMLadse], lcSql)
		uf_perguntalt_chama("Erro a obter dados para o ficheiro de factura��o electr�nica." +CHR(10)+CHR(10)+ "Por favor contacte o suporte.", "Ok", "", 16)
		Return
	ENDIF
	
	** Criar cursor para o XML obtido na SP, passar o campo myXML para variavel que ser� o input do WebService
	select uCrsXMLadse
	go top
	lcXml = uCrsXMLadse.myXML

		
	** Indicador de progresso: Iniciar comunica��o  com WebService
	REGUA(0,3,"Comunicando com WebService da entidade....")	
	
	** Criar e configurar o SOAP Toolkit para fazer o pedido usando WSDL local, autenticado, usando valores em variavel lidos acima
	oWebServADSE_AFP = CREATEOBJECT("mssoap.soapclient30")
	oWebServADSE_AFP.ClientProperty("ServerHTTPRequest") = .T.
	oWebServADSE_AFP.MSSoapInit(lcWSDLPath, "Service", "ServiceSoap")
	oWebServADSE_AFP.ConnectorProperty("AuthUser") = lcWSDLUser
	oWebServADSE_AFP.ConnectorProperty("AuthPassword")= lcWSDLPass

	** Pressupondo que o objecto est� criado, assume-se que a liga��o ao WebService foi feita com sucesso. Objecto "Undefined" presume erro.
	If Type("oWebServADSE_AFP") == "U"
		uf_perguntalt_chama("Erro a criar liga��o ao webservice da AFP." +CHR(10)+CHR(10)+ "Por favor contacte o suporte.", "Ok", "", 16)
		Return
	Endif

	** Indicador de progresso: enviar ficheiro de factura��o com m�todo
	REGUA(1,3,"Enviando dados de factura��o � Entidade....")
	
	** Invocar m�todo para enviar XML, guardar OUTPUT no array. Caso a 1� posi��o seja "OK" o ficheiro foi enviado; caso contrario nao.
**	lcOutArray =  oWebServADSE_AFP.RecFicFarm(Alltrim(lcAnoMes), Alltrim(uCrsE1.ncont), Alltrim(lcNoFact), Alltrim(lcXml))
		
	** PATCH aplicado a 14/05/2012: Invocar o metodo RecFicFarm com o novo ID Sequencial multi-NIF por cliente.
	lcOutArray =  oWebServADSE_AFP.RecFicFarm(Alltrim(lcAnoMes), Alltrim(uCrsE1.ncont), Alltrim(lcnSeqFactNIF), Alltrim(lcXml))
	lcOutVar =  lcOutArray(1)
	
	** Se o OUTPUT for falso, com parametros de envio correctos, assumir que o WEbService esta indisponivel. Acontece com pouca frequencia.
	IF !empty(lcOutVar)
	
		** Por vezes o OUTPUT pode ter caracteres invalidos, este REPLACE resolve isso.
		lcOutVar = Strtran(lcOutVar,Chr(39),'')
	
		* Se o primeiro item do array recebido for "OK", o ficheiro foi enviado
		If Alltrim(lcOutVar)=="OK"

			** Capturar a posi��o 2 do array para STring, avaliar e abrir o link no "browser" por defeito
			lcOutVar2 =  lcOutArray(2)
			
			uf_perguntalt_chama("Ficheiro de fatura��o enviado com sucesso.", "Ok", "", 64)
			
			** Gravar LOG do envio, melhorar tracing do documento orignial
			uf_gerais_registaOcorrencia("AFP", "Ficheiro [" + Alltrim(lcAnoMes) + "_" + Alltrim(uCrsE1.ncont) + "_" + Alltrim(lcnSeqFactNIF) + "] Entidade [" + Alltrim(tcEntAbrev) + "] (formato AFP) Enviado com sucesso", 3, "" , "","")

			** Loggar o endere�o WEb devolvido pelo WebService, nem todas as farmacias usarao o ADSE Directa hoje em dia. Pode depois ser consultado na analise Ocorrencias
			uf_gerais_registaOcorrencia("AFP", "Endere�o Web para comprovativo de Entrega do ano " + ALLTRIM(STR(tcAno)) + "/Mes " + ALLTRIM(STR(tcMes)) + " devolvido: [" + ALLTRIM(lcOutVar2) + "]",3,"","","")
			
			** Fechar REGUA
			REGUA(2)
			
			** Se o utilizador quiser, usar a segunda posi��o do ARray que contem o link de impressao do certificado de envio para consultar.
			If uf_perguntalt_chama("Deseja consultar ou imprimir a folha de resumo do envio?" + CHR(13)+CHR(13) + "Nota: Pode consultar ou Re-imprimir o Comprovativo em qualquer outra altura.", "Sim", "N�o")
				
				** Nao fazer RETURN aqui se a variavel for vazia; o WebService pode deixar consultar o Comprovativo online e ter deixado, a mesma, enviar o ficheiro
				IF Empty(lcOutVar2)
					uf_gerais_registaerrolog("Imposs�vel consultar Comprovativo de Envio de Factura��o da entidade: " + Alltrim(tcEntAbrev),"AFP","","","","","")
					uf_perguntalt_chama("Imposs�vel consultar comprovativo de envio. Por favor aceda � �rea privada da ADSE e re-imprima manualmente.", "Ok", "", 64)
				ELSE
					** Abrir o Website indicado pela ADSE
					DECLARE Long ShellExecute IN "Shell32.dll" ;
					Long hwnd, String lpVerb, String lpFile, ;
					String lpParameters, String lpDirectory, Long nShowCmd
					ShellExecute(0, "Open", lcOutVar2, "", "", SW_SHOWNORMAL)
				ENDIF
			ENDIF

		ELSE
		
			REGUA(2)

			** Definir o ERRORSTRING contendo todo o detalhe de erro do envio pass�vel de obter pelo SOAP Toolkit; passar como parametro a funcao de Log. Construir o ErrorString de uma vez so.
			Text To lcErrorString Noshow textmerge
				Resposta WEBSERVICE: <<Iif(empty(Alltrim(lcOutVar)),"Erro N�o Especificado!",Alltrim(lcOutVar))>>; Faultcode:<<Iif(Empty(oWebServADSE_AFP.faultcode),"N/A",oWebServADSE_AFP.FaulCode)>>; Faultstring:<<Iif(Empty(oWebServADSE_AFP.Faultstring),"N/A",oWebServADSE_AFP.FaultString)>>; Faultactor:<<Iif(Empty(oWebServADSE_AFP.FaultActor),"N/A",oWebServADSE_AFP.FaultActor)>>; Detalhe:<<Iif(empty(oWebServADSE_AFP.detail),"N/A",oWebServADSE_AFP.Detail)>>
			endtext
			
			** Gravar LOG do erro de envio completo
			uf_gerais_registaerrolog(lcErrorString,"AFP","Imposs�vel enviar ficheiro Factura��o da entidade: [" + Alltrim(tcEntAbrev) + "] do ano " + ALLTRIM(STR(tcAno)) + "/Mes " + ALLTRIM(STR(tcMes)),"","","","")
			
			uf_perguntalt_chama("Erro a enviar ficheiro de fatura��o." +CHR(10)+CHR(10)+ "Por favor contacte o suporte.", "Ok", "", 16)
			RETURN
		
		ENDIF
	
	ELSE
		** Neste caso o WEbService nem output devolve. Se for erro de autenteica��o, crasha; Aqui nem isso, devolve .F. 
		uf_gerais_registaerrolog("WebService ADSE indispon�vel","AFP","Imposs�vel enviar ficheiro Factura��o da entidade " + Alltrim(tcEntAbrev),"","","","")
		uf_perguntalt_chama("Webservice da ADSE est� indispon�vel de momento. Por favor tente mais tarde.", "Ok", "", 48)
		Return
	Endif
			

	** CLEANUP
	If Type("oWebServADSE_AFP") <> "U"
		RELEASE oWebServADSE_AFP
	Endif

	If Used("uCrsXMLadse")
		fecha("uCrsXMLadse")
	Endif


	lcXml = .null.
	lcSQl = .null.
	lcWSDLPath = .null.
	lcWSDLUser = .null.
	lcWSDLPass = .null.
ENDFUNC


** FUNCAO PARA VALIDAR BENEFICIO DE CADA UTENTE ADSE
Function uf_facturacaoentidades_checkBenefAfp

	**
	LOCAL lcVal
	
	IF !Empty(facturacaoentidades.lstEntidade.value) AND !(Upper(Alltrim(facturacaoentidades.lstEntidade.value)) == "TODAS")

		DO CASE
			CASE UPPER(ALLTRIM(facturacaoentidades.lstEntidade.value)) == "ADSE"
				lcVal = .t.
		ENDCASE
	ENDIF
	
	IF !lcVal
		uf_perguntalt_chama("N�o pode validar benefici�rios para a entidade selecionada.", "Ok", "", 48)
		RETURN ""
	ENDIF
	
	** Variaveis texto
	Local lcSql,lcWebOutput,lcCheckObject,lcCheckVariable, lcSql2
	Store "" To lcSql,lcSql2,lcWebOutput
	
	** Variavel BOOL que define no 2� Scan do cursor se ha beneficiarios invalidos; variavel BOOL que valida a existencia do objecto SOAP
	STORE .T. TO lcCheckVariable
	STORE .t. TO lcCheckObject
	
	myfacturacaoentidadesAno = VAL(ALLTRIM(facturacaoentidades.cmbAno.value))
	myfacturacaoentidadesMes = uf_gerais_getMonthComboBox("facturacaoentidades.cmbMes")
	
	If Empty(myfacturacaoentidadesAno) Or Empty(myfacturacaoentidadesMes)
		uf_perguntalt_chama("N�o existe ano ou m�s seleccionado. Assim n�o pode continuar.", "Ok", "", 48)
		Return
	Endif
	
	** Proceder apenas se o utilizador tiver a certeza. Notificar os benefs vazios serao trocados por 000000000.
	If uf_perguntalt_chama("Deseja validar os n�s de benefici�rio nas receitas?", "Sim", "N�o")

		** Validar isoladamente se conseguimos aceder ao objecto SOAP Toolkit. Caso contrario, definir variavel de CHeck a false e abortar o codigo.
		TRY 
			oWebServADSE_AFP_teste = CREATEOBJECT("mssoap.soapclient30")
			oWebServADSE_AFP_teste = null
			
		CATCH TO lcError WHEN .t.
			lcCheckObject = .f.

		ENDTRY
		
		IF lcCheckObject == .f.
			uf_perguntalt_chama("Imposs�vel aceder � framework de valida��o XML." + CHR(10)+CHR(10)+ "Por favor contacte o suporte.", "Ok", "", 48)
			RETURN
		ENDIF


		** criar cursor com listagem de receitas para validar benefici�rios da adse **
		TEXT TO lcSql NOSHOW TEXTMERGE
			 exec up_receituario_adseValidaBenef <<myfacturacaoentidadesAno>>, <<myfacturacaoentidadesMes>>, 'adse', '<<ALLTRIM(mySite)>>'
		ENDTEXT
		If !uf_gerais_actgrelha("", "uCrsValBenef", lcSql)
			uf_perguntalt_chama("Ocorreu um erro a gerar listagem de receitas para validar o benefici�rio." +CHR(10)+CHR(10)+ "Por favor contacte o suporte.", "Ok", "", 16)
			RETURN
		ELSE
			If Reccount("uCrsValBenef")<=0
				uf_perguntalt_chama("N�o foram encontradas receitas da ADSE para o ano e m�s seleccionados.", "Ok", "", 48)
				Fecha("uCrsValBenef")
				RETURN
			ENDIF
		ENDIF
		
		LOCAL lcCheckedBenef
		STORE "" TO lcCheckedBenef
		
		SELECT uCrsValBenef
		regua(0,RECCOUNT("uCrsValBenef"),"A validar Benefici�rios via WebService...")

		GO TOP
		SCAN
			
			REGUA(1,recno(),"Validando Utente: [" + ALLTRIM(uCrsValBenef.benef) + "]")
			
			** O parametro de input ao WebService tem que ser STRING; como validar isto adicionalmente?
			IF TYPE("uCrsValBenef.benef") == "C"

				** Nao tentar validar no WebService um beneficiario vazio ou igual a 000000000; nunca ira passar na valida��o. 
				IF !(EMPTY(ALLTRIM(uCrsValBenef.benef)) OR ALLTRIM(uCrsValBenef.benef) == "000000000")

					** Remover todas as letras no N� de Beneficiario para este ser passado de forma limpa ao WebService
					lcCheckedBenef = chrtran(ALLTRIM(uCrsValBenef.benef),"ABCDEFGHIJKLMNOPQRSTUVWXYZ","")

					** O metodo logga autonomamente cada valida��o. Depois de tudo validado � que temos que ver se ha receitas a corrigir!
					lcWebOutput = uf_facturacaoentidades_checkBenefAfpService(ALLTRIM(lcCheckedBenef), uCrsValBenef.cdata)
					
					IF !EMPTY(lcWebOutput)
						SELECT uCrsValBenef
						
						** Guardar o OUTPUT do WebService neste cursor; depois de ele terminar temos que ver se ha receitas que NAO sejam definidas como "CD"
						replace uCrsValBenef.RetCode WITH LEFT(ALLTRIM(lcWebOutput),3)
					ENDIF
				ENDIF
			ENDIF
		ENDSCAN
		
		
		** Fechar regua de progresso de valida��o WEbService, ja terminou.
		regua(2)
		
		** Abrir e fazer novo SCAN
		SELECT uCrsValBenef
		GO TOP
		SCAN

			** APENAS O CODIGO "CD" ESPECIFICO DEVOLVE OK DO WEBSERVICE; OU SEJA, APENAS ESTES BENEFICIARIOS PODEM TER AS RECEITAS ENVIADAS
			IF ALLTRIM(uCrsValBenef.RetCode) != "CD"
				lcCheckVariable = .F.
				
				** Loggar o resultado da valida��o
				uf_gerais_registaOcorrencia("AFP", " Receita Tipo Lote: " + alltrim(uCrsValBenef.tlote) + "/Lote:" + alltrim(str(uCrsValBenef.lote))  + "/Numero:" + alltrim(str(uCrsValBenef.nreceita))  + " tem Beneficiario Vazio ou Invalido! Corrigir.", 3, ALLTRIM(uCrsValBenef.benef) , ALLTRIM(uCrsValBenef.RetCode),"")				

			ENDIF
		ENDSCAN
		
		
		** Se a variavel Check tiver mudado de .T. para .F. alguma receita tem erro. Pedir para analisar e corrigir!
		IF lcCheckVariable = .F.
			uf_perguntalt_chama("Aviso: H� benefici�rios vazios/inv�lidos/sem benef�cio nas receitas analisadas." +CHR(10)+chr(10)+ "Por favor consulte a an�lise de ocorr�ncias e corrija as receitas." +CHR(10)+CHR(10)+ "Em caso de d�vida por favor contacte a ADSE DIRECTA: 707 284 707", "Ok", "", 48)
			RETURN
		ELSE
			uf_perguntalt_chama("Todos os benefici�rios foram validados." +CHR(10)+CHR(10)+ "Pode proceder ao envio da factura��o.", "Ok", "", 64)
			RETURN
		ENDIF
		
		** Cleanup
		Fecha("uCrsValBenef")
	
	
	ELSE && Utilizador nao quis continuar
		RETURN
	ENDIF
ENDFUNC


** FUN��O PARA VALIDAR O BENEFICIO DO UTENTE NO WEBSERVICE DA ADSE, VERS�O AFP
** Esta fun��o � chamada pela fun��o: uf_facturacaoentidades_checkBenefAfp
Function uf_facturacaoentidades_checkBenefAfpService
	Lparameters tcBenef, tcBenefDate

	** guardar entidade seleccionada
	LOCAL tcEntAbrev
	tcEntAbrev = UPPER(ALLTRIM(facturacaoentidades.lstEntidade.value))

	
	** Variaveis locais
	Local lcWSDLUser, lcWSDLPass, lcWSDLPath, lcOutVar2, lcBenefDate
	Store "" To lcWSDLUser, lcWSDLPass, lcWSDLPath, lcOutVar2
	
	** A data do benef�cio a validar, parametro da ADSE, ser� sempre a data de hoje � qual o Utente foi aviar a receita.
	tcBenefDate = date(year(tcBenefDate), month(tcBenefDate), day(tcBenefDate))

	** Array STRING simples para guardar o OUTPUT do WebService, com 1 posi��o
	Dimension lcOutArray2[1]
	
	** Validar parametros de input
	If !Empty(tcBenef)
		tcBenef = Alltrim(tcBenef)
	
		** Obter utilizador e password, separados por virgula, para autenticar no WebService
		lcWSDLParams = uf_gerais_getParameter('ADM0000000153', 'TEXT')

		** Validar o total de WORDS diferentes; devem ser um maximo de 2!
		If Getwordcount(lcWSDLParams,";") == 2
				
			** Validar o parametro posi��o 1, utilizador
			lcWSDLUser = Alltrim(getwordnum(lcWSDLParams,1,";"))
			IF EMPTY(lcWSDLUser)
				uf_perguntalt_chama("N�o configurou correctamente o utilizador do webservice.", "Ok", "", 16)
				RETURN ""
			ENDIF
					
			** Validar o parametro posi��o 2, password
			lcWSDLPass = Alltrim(getwordnum(lcWSDLParams,2,";"))
			IF EMPTY(lcWSDLPass)
				uf_perguntalt_chama("N�o configurou correctamente a password do webservice.", "Ok", "", 16)
				RETURN ""
			ENDIF
		ELSE
			uf_perguntalt_chama("Ocorreu um erro a validar configura��o do webservice." +CHR(10)+CHR(10)+ "Por favor contacte o suporte.", "Ok", "", 16)
			RETURN ""
		ENDIF
		
		
		** Ler e validar o parametro de configura��o do caminho do ficheiro WSDL do WebService
		lcWSDLPath = uf_gerais_getParameter('ADM0000000154', 'TEXT')
		
		** Percorrer resultados se houverem; agregar path do LData com o path 
		lcWSDLPath = Alltrim(mypath) + "\" + Alltrim(lcWSDLPath)
		
		** Validar se o ficheiro existe ou temos permiss�o de leitura.
		If File(lcWSDLPath) == .F.
			uf_perguntalt_chama("Erro a aceder ao ficheiro de configura��o do webservice de factura��o." +CHR(10)+CHR(10)+ "Por favor contacte o suporte.", "Ok", "", 16)
			RETURN ""
		ENDIF
	
		
		** Abrir objectos WebService, configurar autentica��o e propriedade WebRequest
		oWebServADSE_AFP_2 = CREATEOBJECT("mssoap.soapclient30")
		oWebServADSE_AFP_2.ClientProperty("ServerHTTPRequest") = .T.
		oWebServADSE_AFP_2.MSSoapInit(lcWSDLPath, "Service", "ServiceSoap")
		oWebServADSE_AFP_2.ConnectorProperty("AuthUser") = lcWSDLUser
		oWebServADSE_AFP_2.ConnectorProperty("AuthPassword")= lcWSDLPass

		** Pressupondo que o objecto est� criado, assume-se que a liga��o ao WebService foi feita com sucesso. Objecto "Undefined" presume erro.
		If Type("oWebServADSE_AFP_2") == "U"
			uf_perguntalt_chama("Erro a criar a liga��o ao webservice da ADSE." +CHR(10)+CHR(10)+ "Por favor contacte o suporte.", "Ok", "", 16)
			RETURN ""
		Endif
		
		
		** Invocar o metodo do WebService para guardar no array o seu Output. NAO usar string no 2� parametro, tem que ser DATE nativo do Fox.
		** Avaliar o Output em array. Pelo menos a 1� posi��o tem que ter conteudo e ser diferente de .F., senao o metdo nao funcinou.
		lcOutArray2 = oWebServADSE_AFP_2.SitBenefDirRC(Alltrim(tcBenef),tcBenefDate)
		lcOutVar2 =  lcOutArray2(1)
		
		** Se a lcOutVar for igual a .F., o WebService nao respondeu ao pedido ou houve pedido de comunica��o
		If Empty(lcOutVar2)
			Return	""		
		Else			
			** Se o tipo da variavel nao for CHAR, algo errado ocorreu; notificar de erro e abortar.
			If Type("lcOutVar2") <> "C"
				uf_perguntalt_chama("Webservice indispon�vel. Por favor tente mais tarde.", "Ok", "", 48)
				RETURN ""
			Endif
		Endif
	
*!*			** Se o Output nao e falso, o WebService respondeu; avaliar reposta
*!*			Do Case
*!*				Case lcOutVar2 = "NI"
*!*					uf_registaOcorrencia("AFP", "Utente " + "[" + Alltrim(tcBenef) + "]" + " n�o est� Inscrito na ADSE � data", 3, "" , "","")
*!*				Case lcOutVar2 = "CD"
*!*					uf_registaOcorrencia("AFP", "Utente " + "[" + Alltrim(tcBenef) + "]" + " tem direito ao benef�cio", 3, "" , "","")
*!*				Case lcOutVar2 = "S4"
*!*					uf_registaOcorrencia("AFP", "Utente " + "[" + Alltrim(tcBenef) + "]" + " Falecido, sem benef�cio", 3, "" , "","")
*!*				Case lcOutVar2 = "S5"
*!*					uf_registaOcorrencia("AFP", "Utente " + "[" + Alltrim(tcBenef) + "]" + " inv�lido (indefinido). Sem benef�cio.", 3, "" , "","")
*!*				Case lcOutVar2 = "S6"
*!*					uf_registaOcorrencia("AFP", "Utente " + "[" + Alltrim(tcBenef) + "]" + " Inv�lido (indefinido). Sem benef�cio.", 3, "" , "","")
*!*				Case lcOutVar2 = "CI"
*!*					uf_registaOcorrencia("AFP", "Utente " + "[" + Alltrim(tcBenef) + "]" + " tem Cart�o Inv�lido. Sem benef�cio.", 3, "" , "","")	
*!*				Case lcOutVar2 = "*"
*!*					uf_registaOcorrencia("AFP", "Utente " + "[" + Alltrim(tcBenef) + "]" + " Inv�lido/Desconhecido. Sem benef�cio.", 3, "" , "","")
*!*				Otherwise
*!*					RETURN ''
*!*			EndCase 
		
		
		** Cleanup objectos / Variaveis
		If Type("oWebServADSE_AFP_2") <> "U"
			RELEASE oWebServADSE_AFP_2
		Endif

		lcWSDLPath 	= .null.
		lcWSDLUser 	= .null.
		lcWSDLPass 	= .null.
		lcOutArray2 = .null.

		** OUTPUT de saida desta funcao	
		RETURN lcOutVar2 

	** Se os parametros nao forem correctos, abortar	
	ELSE
		uf_perguntalt_chama("Indique o n� de benefici�rio e data correctos para validar.", "Ok", "", 48)
		RETURN ''
	ENDIF
ENDFUNC




**
** Fecha o painel
**
FUNCTION uf_facturacaoentidades_sair
	** release variaveis
	
	RELEASE myfacturacaoentidadesMes
	RELEASE myfacturacaoentidadesAno
	
	RELEASE myFacHora
	RELEASE myFacData
	RELEASE myNrVenda
	
	RELEASE mlcEivaIn1
	RELEASE mlcEivaIn2
	RELEASE mlcEivaIn3
	RELEASE mlcEivaIn4
	RELEASE mlcEivaIn5
	RELEASE mlcEivaIn6
	RELEASE mlcEivaIn7
	RELEASE mlcEivaIn8
	RELEASE mlcEivaIn9
	RELEASE mlcEivaV1
	RELEASE mlcEivaV2
	RELEASE mlcEivaV3
	RELEASE mlcEivaV4
	RELEASE mlcEivaV5
	RELEASE mlcEivaV6
	RELEASE mlcEivaV7
	RELEASE mlcEivaV8
	RELEASE mlcEivaV9
	
	&& evita erros de rowsource ao sair
	facturacaoentidades.lstEntidade.rowsource	=''
	
	&& fecha cursores
	IF USED("uCrsEflEntidades")
		fecha("uCrsEflEntidades")
	ENDIF
		
	IF USED("uCrsEfl")
		fecha("uCrsEfl")
	ENDIF
		IF USED("uCrsEfl1")
		fecha("uCrsEfl1")
	ENDIF

	
	IF USED("uCrsImpFacEnt")
		fecha("uCrsImpFacEnt")
	ENDIF
	
	IF USED("uCrsTextoRodape")
		fecha("uCrsTextoRodape")
	ENDIF
	
	
 	IF(USED("ucrsEntidadeFact"))
		fecha("ucrsEntidadeFact")
	ENDIF
	
	IF(USED("uCrsAuxRespostaDet"))
		fecha("uCrsAuxRespostaDet")
	ENDIF
	

		
	
	&& fecha painel
	facturacaoentidades.hide
	

	facturacaoentidades.release
	

	
ENDFUNC

** Funcao que chama a DLL "Stub" ClrHost e que permite chamar objectos .Net a partir do Fox
FUNCTION uf_facturacaoEntidades_CreateClrInstanceFrom
	LPARAMETERS lcLibrary,lcClass,pClrInstanceError

	LOCAL lnDispHandle, lnSize, lcFile
	STORE '' TO lcFile

	*DECLARE Integer ClrCreateInstanceFrom IN "C:\Windows\Microsoft.NET\Framework\v2.0.50727\ClrHost.dll" string, string, string@, integer@
	lcFile = ALLTRIM(uf_gerais_getParameter("ADM0000000216","TEXT"))
	DECLARE Integer ClrCreateInstanceFrom IN lcFile string, string, string@, integer@
	
	pClrInstanceError = SPACE(2048)
	lnSize = 0

	lnDispHandle = ClrCreateInstanceFrom(lcLibrary,lcClass,@pClrInstanceError,@lnSize)

	IF lnDispHandle < 1
	  	pClrInstanceError = LEFT(pClrInstanceError,lnSize)
		RETURN NULL 
	ENDIF

	RETURN SYS(3096, lnDispHandle)
ENDFUNC

** Funcao para comunicar com o WebService de testes da Novexem
FUNCTION uf_facturacaoEntidades_ChamaWEbServiceLILLY
	LPARAMETERS lcAno, lcMes, lcCptOrgAbrev
	
	PUBLIC pClrInstanceError
	STORE "" TO pClrInstanceError
	
	LOCAL lcWSDLPath, lcNovexemDLLPath, lcWebServiceAFPLILLYOutput, lcSql, lcEncryptKey, lcNewKey, lcEncryptedText, lcFile
	STORE "" TO lcWSDLPath, lcNovexemDLLPath, lcWebServiceAFPLILLYOutput, lcSql, lcEncryptKey, lcNewKey, lcEncryptedText, lcFile
	
	** Carregar a DLL de encripta��o da Novexem dos EXTERNOS do LData usando variavel de caminho. Permite actualizar mais facilmente isto se for alterado por eles.
	lcNovexemDLLPath = mypath + "\externos\webservicefact\lilly\Co-PayEncryption.dll"	
	
	** validar se os ficheiros necessarios existem (Classe CLRHOST.DLL e CO-PAY ENCRYPTION.DLL)
	*IF FILE("C:\Windows\Microsoft.NET\Framework\v2.0.50727\ClrHost.dll")
	lcFile = ALLTRIM(uf_gerais_getParameter("ADM0000000216","TEXT"))
	IF FILE(lcFile)
		IF !FILE(lcNovexemDLLPath) 
			uf_perguntalt_chama("Biblioteca de encripta��o Co-Pay LILLY nao instalada. Contacte o Suporte T�cnico.","OK","",16)
			RETURN .f.
		ENDIF
	ELSE
		uf_perguntalt_chama("Ficheiro CLRHOST.DLL indispon�vel. Contacte o Suporte T�cnico.","OK","",16)
		RETURN .f.
	ENDIF	
	
	** Usar a biblioteca CLRHOST.DLL do Windows para carregar a biblioteca da LILLY atraves do .NET 2.0
	oWebServiceAFPLILLYCrypto = uf_facturacaoEntidades_CreateClrInstanceFrom(lcNovexemDLLPath,"Co_PayEncryptor.CryptoManager",@pClrInstanceError)
	IF TYPE("oWebServiceAFPLILLYCrypto") == "U"
		uf_perguntalt_chama("Impossivel carregar biblioteca de encripta��o LILLY usando .Net! Contacte o Suporte.","OK","",16)
		RETURN .f.
	ENDIF

	** Se estivermos em modo TESTES, usar o segundo ficheiro WSDL; caso contrario nao. 
	** O ficheiro de TESTES tem um URL Diferente que aponta para o dominio de Testes
	IF emDesenvolvimento
		IF uf_perguntalt_chama("Deseja usar o WSDL de Testes?","Sim","Nao")
			lcWSDLPath = mypath + "\externos\webservicefact\lilly\co-pagamento_lilly_Testes.wsdl"
			
			lcStamp = uf_gerais_stamp()
			** REGISTAR NAS OCORRENCIAS QUE O ENVIO � FEITO VIA TESTES COM O WSDL DE TESTES
			TEXT TO lcSQL TEXTMERGE NOSHOW
				INSERT INTO B_ocorrencias(
					stamp
					,tipo
					,grau
					,descr
					,oValor
					,dValor
					,usr
					,date
				)
				values(
					'<<lcStamp)>>'
					,'Lotes'
					,3
					,'AVISO: Factura��o Detalhada da entidade ' + '<<ALLTRIM(lcCptOrgAbrev)>>' + ' do Ano e Mes ' + '<<astr(lcAno)>>' + '/' + '<<astr(lcMes)>>' + ' vai usar o WSDL de TESTES'
					,''
					,''
					,<<ch_userno>>
					,dateadd(HOUR, <<difhoraria>>, getdate()))		
			endtext	
			
			IF !uf_gerais_actGrelha("","",lcSQL)
				uf_perguntalt_chama("Ocorreu um erro a gravar o registo de ocorr�ncia." +CHR(10)+CHR(10)+ "Por favor contacte o suporte.", "Ok", "", 16)
			ENDIF				
			
		ELSE
			lcWSDLPath = mypath + "\externos\webservicefact\lilly\co-pagamento_lilly.wsdl"
		ENDIF 
	ELSE
		lcWSDLPath = mypath + "\externos\webservicefact\lilly\co-pagamento_lilly.wsdl"
	ENDIF

	** Validar se o ficheiro WSDL seleccionado existe.
	IF !FILE(lcWSDLPath)
		uf_perguntalt_chama("Impossivel abrir ficheiro de configura��o WebService da LILLY." +CHR(10)+CHR(10)+ "Por favor contacte o suporte.", "Ok", "", 16)
		RETURN .f.
	ENDIF

	** Iniciar o SOAP Toolkit, definir propriedade HTTPRequest. Passar o WSDL como parametro, bem como a raiz e o m�todo
	oWebServAFP_LILLY = CREATEOBJECT("mssoap.soapclient30")
	oWebServAFP_LILLY.ClientProperty("ServerHTTPRequest") = .T.
	oWebServAFP_LILLY.MSSoapInit(lcWSDLPath, "SaleManager", "SaleManagerSoap")

	** Pressupondo que o objecto est� criado, assume-se que a liga��o ao WebService foi feita com sucesso. Objecto "Undefined" presume erro.
	If Type("oWebServAFP_LILLY") == "U"
		uf_perguntalt_chama("Erro a criar liga��o ao webservice da LILLY." +CHR(10)+CHR(10)+ "Por favor contacte o suporte.", "Ok", "", 16)
		RETURN .f.
	ENDIF
	
	** Ler a ultima Chave de Encripta��o usada pela farm�cia para envio. Se n�o houver nenhuma, usar a DEFAULT hardcoded e fornecida pela Novexem.
	IF lcEncryptKey == ""
		TEXT TO lcSQL TEXTMERGE NOSHOW
			SELECT stamp 
			FROM b_ultReg (nolock)
			where tabela = 'LILLY'
		ENDTEXT
		
		uf_gerais_actGrelha("","uCrsEncryptKey",lcSQL)
		
		SELECT uCrsEncryptKey
		IF RECCOUNT("uCrsEncryptKey") > 0
			lcEncryptKey = ALLTRIM(uCrsEncryptKey.stamp)
		ELSE
			lcEncryptKey = '41db6bd6-1bfd-457e-8763-44c1ba5dfaf7'
		ENDIF
	ENDIF
	
	** Definir o QUERY que nos vai dar os dados de Factura��o Detalhada, para associados AFP.
	TEXT TO lcSql NOSHOW textmerge
		exec dbo.up_receituario_gerarFileNovexem '<<lcAno>>','<<lcMes>>','<<ALLTRIM(lcCptOrgAbrev)>>'
	ENDTEXT

	** Apenas recolher os dados de Factura��o se o WebService estiver OK, nao gerar load na BD desnecessariamente.
	IF !uf_gerais_actGrelha("","uCrsDadosLILLY",lcSql)
		uf_perguntalt_chama("Erro a obter dados de Factura��o Detalhada da LILLY." +CHR(10)+CHR(10)+ "Por favor contacte o suporte.", "Ok", "", 16)
		RETURN .f.
	ELSE
		** Abrir o Cursor, ver se temos dados.
		SELECT uCrsDadosLILLY
		IF RECCOUNT("uCrsDadosLILLY") > 0
			** Encriptar o XML gerado pela BD usando a chave fornecida antes. Validar se existe texto encriptado depois. A biblioteca ja preparar o Output em XML para ser enviado ao WEbservice!
			lcEncryptedText = oWebServiceAFPLILLYCrypto.Encrypt(uCrsDadosLILLY.Dados,lcEncryptKey)

			IF lcEncryptedText == ""
				uf_perguntalt_chama("Erro a encriptar dados de Factura��o Detalhada da LILLY." +CHR(10)+CHR(10)+ "Por favor contacte o suporte.", "Ok", "", 16)
				RETURN .f.				
			ENDIF

			** Se estivermos na farmDev ou em desenvolvimento usar o outro metodo Test de envio. Caso contrario usar o metodo de Produ��o.
			SELECT uCrsE1
			IF emDesenvolvimento
				IF uf_perguntalt_chama("Deseja usar o WebService de Testes da NOVEXEM?","Sim","Nao")
					lcWebServiceAFPLILLYOutput = oWebServAFP_LILLY.TestAddSalesInformation(astr(uCrsE1.u_infarmed), lcEncryptedText)
					
				** REGISTAR NAS OCORRENCIAS QUE O ENVIO � FEITO VIA TESTES
				lcStamp = uf_gerais_stamp()
				TEXT TO lcSQL TEXTMERGE NOSHOW
					INSERT INTO B_ocorrencias(
						stamp
						,tipo
						,grau
						,descr
						,oValor
						,dValor
						,usr
						,date
					)
					values(
						'<<lcStamp>>'
						,'Lotes'
						,3
						,'AVISO: Factura��o Detalhada da entidade ' + '<<ALLTRIM(lcCptOrgAbrev)>>' + ' do Ano e Mes ' + '<<astr(lcAno)>>' + '/' + '<<astr(lcMes)>>' + ' vai ser enviada via WebService TESTES'
						,''
						,''
						,<<ch_userno>>
						,dateadd(HOUR, <<difhoraria>>, getdate()))
				ENDTEXT
				
				IF !uf_gerais_actGrelha("","",lcSQL)
					uf_perguntalt_chama("Ocorreu um erro a gravar o registo de ocorr�ncia." +CHR(10)+CHR(10)+ "Por favor contacte o suporte.", "Ok", "", 16)
				ENDIF					
					
				ELSE
					lcWebServiceAFPLILLYOutput = oWebServAFP_LILLY.AddSalesInformation(astr(uCrsE1.u_infarmed), lcEncryptedText)
				endif	
			else
				lcWebServiceAFPLILLYOutput = oWebServAFP_LILLY.AddSalesInformation(astr(uCrsE1.u_infarmed), lcEncryptedText)
			endif
			
			** Se o envio for feito COM SUCESSO, o output NAO sera vazio e uma nova chave sera devolvida.
			** Em caso de erro no envio ou processamento, tambem pode ser devolvido o texto "Error" que tem que parar o codigo.
			IF TYPE("lcWebServiceAFPLILLYOutput")!= "C" OR lcWebServiceAFPLILLYOutput == "" OR lcWebServiceAFPLILLYOutput == "Error"
				** Em caso de erro, logar e dar Output ao utilizador.
				uf_gerais_registaerrolog("Erro: Impossivel enviar Ficheiro LILLY [" + Alltrim(facturacaoentidades.cmbAno.value) + "/" + Alltrim(facturacaoentidades.cmbMes.value) + "]. Mensagem: [" + ALLTRIM(lcWebServiceAFPLILLYOutput) + "]" ,"LILLY","","","","","")
				uf_perguntalt_chama("Impossivel enviar dados de Factura��o Detalhada encriptada!. " +CHR(10)+CHR(10)+ "Por favor contacte o suporte.", "Ok", "", 16)
				
				RETURN .f.	
						
			ELSE
				** O envio foi feito com sucesso; registar a nova chave, DEPOIS DE A DESENCRIPTAR, para usar no pr�ximo envio.
				lcNewKey = oWebServiceAFPLILLYCrypto.Decrypt(lcWebServiceAFPLILLYOutput,lcEncryptKey)

				** Limpar a chave anterior e guardar a nova para o pr�ximo envio.
				TEXT TO lcSQL TEXTMERGE NOSHOW
					DELETE from b_ultReg WHERE tabela = 'LILLY'
					
					INSERT INTO b_ultReg(
						tabela
						,stamp
						,[data]
						,site
					)
					values(
						'LILLY'
						,'<<lcNewKey>>'
						,dateadd(HOUR, <<difhoraria>>, getdate())
						,'<<mySite>>'
					)
				ENDTEXT
				
				IF !uf_gerais_actGrelha("","",lcSQL)
					uf_perguntalt_chama("Ocorreu um erro a gravar a nova Chave de Encripta��o." +CHR(10)+CHR(10)+ "Por favor contacte o suporte.", "Ok", "", 16)
				ENDIF
				
				** REGISTAR NAS OCORRENCIAS
				lcStamp = uf_gerais_stamp()
				TEXT TO lcSQL TEXTMERGE NOSHOW
					INSERT INTO B_ocorrencias(
						stamp
						,tipo
						,grau
						,descr
						,oValor
						,dValor
						,usr
						,date
					)
					values(
						'<<lcStamp>>'
						,'Lotes'
						,3
						,'Factura��o Detalhada da entidade ' + '<<ALLTRIM(lcCptOrgAbrev)>>' + ' do Ano e Mes ' + '<<astr(lcAno)>>' + '/' + '<<astr(lcMes)>>' + ' enviada com sucesso.'
						,''
						,''
						,<<ch_userno>>
						,dateadd(HOUR, <<difhoraria>>, getdate()))
				ENDTEXT
				
				IF !uf_gerais_actGrelha("","",lcSQL)
					uf_perguntalt_chama("Ocorreu um erro a gravar o registo de ocorr�ncia." +CHR(10)+CHR(10)+ "Por favor contacte o suporte.", "Ok", "", 16)
				ENDIF
				
				** MESSAGEBOX DE SUCESSO
				uf_perguntalt_chama("Factura��o Detalhada da LILLY enviada com sucesso.", "Ok", "", 64)
			ENDIF
			
		** Se por algum motivo a SP Devolver vazio ou nao gerar nada, ignorar.
		ELSE
			uf_perguntalt_chama("Dados de Factura��o Detalhada da LILLY vazios. " +CHR(10)+CHR(10)+ "Por favor contacte o suporte.", "Ok", "", 16)			
			RETURN .f.
		ENDIF
	ENDIF
ENDFUNC






FUNCTION uf_facturacaoentidades_mcdt
	LOCAL lcNrEntidadeDep ,lcNomeJar, lcStampLigacao, lcStampFaturaSNS, lcIDCliente, lcAno, lcMes, lcResultadoFatura, lcDataAux, lcNrEntidade, lcAbrev,lcNrEntidade  
	STORE '' TO lcNomeJar, lcStampLigacao, lcStampFaturaSNS, lcIDCliente, lcResultadoFatura, lcAbrev  
	STORE '&' TO lcAdd
	STORE 0 TO lcAno, lcMes, lcNrEntidadeDep
	STORE .f. TO lcNrEntidade 
	
	
	&& Tipo de Cliente
	IF ALLTRIM(ucrsE1.tipoEmpresa) != "FARMACIA"
		uf_perguntalt_chama("FUNCIONALIDADE APENAS DISPON�VEL PARA CLIENTES DO TIPO FARM�CIA. OBRIGADO.", "Ok", "", 16)
		RETURN .f.
	ENDIF 
	

	** cria cursor com info da entidade de faturacao
	IF(USED("ucrsEntidadeFact"))
		fecha("ucrsEntidadeFact")
	ENDIF
	
	** valida se tem faturacao eletronica
		
	lcAbrev = ALLTRIM(UPPER(facturacaoentidades.lstEntidade.value))
	
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_receituario_facturacaoEntidadesEletronica '<<lcAbrev>>', '<<ALLTRIM(mysite)>>'
 	ENDTEXT
 	IF !uf_gerais_actGrelha("","ucrsEntidadeFact",lcSQL)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA A VERIFICAR OS DADOS DA ENTIDADE DE FACTURA��O","OK","",16)
 		RETURN .f.
 	ENDIF
 	

 	lcNrEntidade 		= ucrsEntidadeFact.entidadeID
 	lcNrEntidadeDep 	= ucrsEntidadeFact.entidadeDep
 	
 	
 	** limpa cursor
 	IF(USED("ucrsEntidadeFact"))
		fecha("ucrsEntidadeFact")
	ENDIF
	
	&& N� de Contribuinte
	SELECT uCrsE1
	IF RECCOUNT("uCrsE1")>0
		IF EMPTY(uCrsE1.ncont)
			uf_perguntalt_chama("O NIF DA FARM�CIA N�O EST� CORRETAMENTE PREENCHIDO. POR FAVOR VERIFIQUE.", "Ok", "", 16)
			RETURN .f.
		ENDIF
	ENDIF

	&& Verifica a configura��o do c�digo de farm�cia
	SELECT ucrsE1
	IF EMPTY(ALLTRIM(ucrsE1.u_codfarm))
		uf_perguntalt_chama("O C�DIGO INFARMED DA FARM�CIA N�O EST� CORRETAMENTE PREENCHIDO. POR FAVOR VERIFIQUE.","OK","", 16)
		RETURN .f.
	ENDIF 
	
	&& Verifica a configura��o do ID do Cliente
	SELECT uCrsE1
	IF EMPTY(ALLTRIM(uCrsE1.id_lt))
		uf_perguntalt_chama("O C�DIGO DE CLIENTE LOGITOOLS N�O EST� CORRETAMENTE PREENCHIDO. POR FAVOR CONTACTE O SUPORTE.","OK","", 16)
		RETURN .f.
	ENDIF 
	lcIDCliente = ALLTRIM(uCrsE1.id_lt)
	**

	&& buscar ftstamp da fatura sns referente ao periodo definido no painel para verificar se envio j� foi aceite
	lcDataAux = ALLTRIM(str(myfacturacaoentidadesAno)) + Substr("00",1,Len("00")-Len(ALLTRIM(str(myfacturacaoentidadesMes)))) + ALLTRIM(str(myfacturacaoentidadesMes)) + '01'
	
	
	lcSQL = ''
	TEXT TO lcSQL TEXTMERGE NOSHOW 
		select TOP 1 ftstamp 
		FROM ft (nolock) 
		INNER JOIN td (nolock) ON td.ndoc = ft.ndoc 
		WHERE ft.estab = <<lcNrEntidadeDep>> AND ft.no = <<lcNrEntidade>> AND td.u_tipodoc = 1 AND ft.cdata = DATEADD(DAY, -1, DATEADD(MONTH, DATEDIFF(MONTH, 0, '<<lcDataAux>>')+1, 0)) AND ft.site = '<<mySite>>' 
		order by ft.fno desc
	ENDTEXT 
	


	IF !uf_gerais_actGrelha("","uCrsAuxFT",lcSQL)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VERIFICAR A INFORMA��O DA FACTURA. POR FAVOR CONTACTE O SUPORTE.", "Ok", "", 16)
		regua(2)
		RETURN .f.
	ELSE
		IF RECCOUNT("uCrsAuxFT") < 1
			uf_perguntalt_chama("AINDA N�O FOI EMITIDA FACTURA PARA O PERIODO SELECCIONADO. POR FAVOR VERIFIQUE.", "Ok", "", 16)
			regua(2)
			RETURN .f.
		ELSE
			select uCrsAuxFT
			lcStampFaturaSNS = ALLTRIM(uCrsAuxFT.ftstamp)
		
		ENDIF
	ENDIF 
	
	
	LOCAL uv_path, mdir  
    uv_path = ALLTRIM(uf_gerais_getUmValor("B_Terminal", "LTRIM(RTRIM(pathExport))", "terminal = '" + alltrim(myTerm) + "'"))
            IF EMPTY(uv_path)
                uf_perguntalt_chama('Aten��o!' + chr(13) + 'N�o preenScheu o par�metro "Caminho Export.".' + chr(13) +  'Ter� de preencher o mesmo para continuar.', "Ok", "", 48)
                RETURN .f.
            ENDIF

   
    IF !uf_gerais_createDir(uv_path)
     	uf_perguntalt_chama('Caminho de rede n�o encontrado, Por favor contacte o suporte.', "Ok", "", 48)
        RETURN .F.
    ENDIF
    
    mdir = uv_path 
	

	&& Procede ao envio da fatura
	regua(0,4,"A preparar Fatura Eletronicamente...")
	&&
	regua(1,1,"A configurar software para gera��o do ficheiro...")

			
	&& Verifica se software existe
	lcNomeJar = 'FaturacaoEletronicaMcdt.jar'
	
	
			


	IF !FILE(ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\ltsFaturacaoEletronicaMcdt\' + ALLTRIM(lcNomeJar))
		uf_perguntalt_chama("FICHEIRO DE CONFIGURA��O N�O ENCONTRADO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		regua(2)
		RETURN .f.
	ENDIF 
	
	&& Verifica o servidor de BD
	IF !USED("uCrsServerName")
		lcSQL  = ''
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			Select @@SERVERNAME as dbServer
		ENDTEXT 
		IF !uf_gerais_actGrelha("", "ucrsServerName", lcSQL)
			uf_perguntalt_chama("OCORREU UMA ANOMALIA. POR FAVOR CONTACTE O SUPORTE." ,"OK","",16)
			regua(2)
			RETURN  .f.
		ENDIF
	ENDIF
	
	SELECT ucrsServerName
	lcDbServer = ALLTRIM(ucrsServerName.dbServer)
	
	&&
	regua(1,2,"A recolher informa��o para envio...")
	&& recolher informa��o daqui
	&& campo cdata na ft tem a data do ultimo dia ao mes que corresponde o documento
	lcStampLigacao = uf_gerais_stamp()
	lcAno = ALLTRIM(STR(myfacturacaoentidadesAno))
	lcMes = ALLTRIM(STR(myfacturacaoentidadesMes))
	
	lcResultadoFatura = ALLTRIM(STR(0))
	

		
				&&
	regua(1,3,"A criar informa��o...")
	&& efetua envio da fatura
	lcWsPath = ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\ltsFaturacaoEletronicaMcdt\' + ALLTRIM(lcNomeJar);
			+ " " + ALLTRIM(lcStampLigacao);	
			+ " " + lcStampFaturaSNS;
			+ " " + lcIDCliente;
			+ " " + lcAno;
			+ " " + lcMes;
			+ " " + ["] + ALLTRIM(mySite) + ["];
			+ " " + lcResultadoFatura;
			+ " 0"

	&& Envia comando Java
	lcWsPath = "javaw -jar " + lcWsPath 
	
	&& aguarda que software termine o envio da informa��o 3� PARAMETRO = .T.
	oWSShell = CREATEOBJECT("WScript.Shell")
	oWSShell.Run(lcWsPath, 1, .t.)
	
	IF emdesenvolvimento &&debug mode
 		_cliptext = lcWsPath 
 	ENDIF
 			
	 			
	&& verifica resposta 
	regua(1,4,"A verificar caminho do documento...")
	
	fecha("uCrsAuxResposta")

	lcSQL = ''
	TEXT TO lcSQL TEXTMERGE NOSHOW 
		select TOP 1  pathFile, fileName, fileType FROM faturacao_eletronica_med (nolock) WHERE id= '<<ALLTRIM(lcStampLigacao)>>' AND ftstamp = '<<ALLTRIM(lcStampFaturaSNS)>>'   ORDER BY data desc
	ENDTEXT 
	IF !uf_gerais_actGrelha("","uCrsAuxResposta",lcSQL)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VERIFICAR O CAMINHO DE RESPOSTA. POR FAVOR CONTACTE O SUPORTE.", "Ok", "", 16)
		fecha("uCrsAuxResposta")
		regua(2)
		RETURN .f.
	ELSE
		IF RECCOUNT("uCrsAuxResposta") > 0 
			IF EMPTY(ALLTRIM(uCrsAuxResposta.pathFile)) OR EMPTY(ALLTRIM(uCrsAuxResposta.pathFile))
				regua(2)
				fecha("uCrsAuxResposta")
				uf_perguntalt_chama("O DOCUMENTO GERADO CONT�M ERROS. POR FAVOR CONTACTE O SUPORTE T�CNICO PARA AN�LISE.", "Ok", "", 16)
				RETURN .f.
			ENDIF
		ELSE
			fecha("uCrsAuxResposta")
			regua(2)
			uf_perguntalt_chama("OCORREU UMA ANOMALIA AO CRIAR O DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.", "Ok", "", 16)
			RETURN .f.
		ENDIF
	ENDIF

	regua(2)
	


	
	&&SELECT uCrsAuxResposta
	&&GO TOP	
	&&COPY FILE ALLTRIM(uCrsAuxResposta.pathFile)  TO (mdir)
	
	
	SELECT uCrsAuxResposta
	mdir = mdir + "\" + ALLTRIM(uCrsAuxResposta.fileName) 
	

	
	LOCAL oFSO 
	oFSO = CREATEOBJECT("Scripting.FileSystemObject")
	ofso.CopyFile(ALLTRIM(uCrsAuxResposta.pathFile),mdir,.t.)

		
	uf_perguntalt_chama("Ficheiro gerado com sucesso.","Ok","",64)
	
	
	IF USED("uCrsAuxFT")
		fecha("uCrsAuxFT")
	ENDIF
	
	IF USED("uCrsAuxFTStAMP")
		fecha("uCrsAuxFTStAMP")
	ENDIF
	
	IF USED("uCrsAuxResposta")
		fecha("uCrsAuxResposta")
	ENDIF
	
ENDFUNC






** No envio da fatura caso seja Entidade ANF deve gerar o PDF aqui
FUNCTION uf_facturacaoentidades_envioFactSNS
	LOCAL lcNrEntidadeDep ,lcNomeJar, lcStampLigacao, lcStampFaturaSNS, lcIDCliente, lcAno, lcMes, lcResultadoFatura, lcDataAux, lcNrEntidade, lcAbrev,lcNrEntidade, lcIsMcdt, lcIsPemH
	STORE '' TO lcNomeJar, lcStampLigacao, lcStampFaturaSNS, lcIDCliente, lcResultadoFatura, lcAbrev  
	STORE '&' TO lcAdd
	STORE 0 TO lcAno, lcMes, lcNrEntidadeDep 
	STORE .f. TO lcNrEntidade 
	
	&& Tipo de Cliente
	IF ALLTRIM(ucrsE1.tipoEmpresa) != "FARMACIA"
		uf_perguntalt_chama("FUNCIONALIDADE APENAS DISPON�VEL PARA CLIENTES DO TIPO FARM�CIA. OBRIGADO.", "Ok", "", 16)
		RETURN .f.
	ENDIF 
	

	** cria cursor com info da entidade de faturacao
	IF(USED("ucrsEntidadeFact"))
		fecha("ucrsEntidadeFact")
	ENDIF
	
	** valida se tem faturacao eletronica
		
	lcAbrev = ALLTRIM(UPPER(facturacaoentidades.lstEntidade.value))
	
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_receituario_facturacaoEntidadesEletronica '<<lcAbrev>>',  '<<ALLTRIM(mysite)>>'
 	ENDTEXT
 	IF !uf_gerais_actGrelha("","ucrsEntidadeFact",lcSQL)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA A VERIFICAR OS DADOS DA ENTIDADE DE FACTURA��O","OK","",16)
 		RETURN .f.
 	ENDIF
 	
 	
 	lcTemFactEletronica = ucrsEntidadeFact.temFactEl
 	lcNrEntidade 		= ucrsEntidadeFact.entidadeID
 	lcNrEntidadeDep 	= ucrsEntidadeFact.entidadeDep
 	lcIsMcdt            = ucrsEntidadeFact.isMcdt
 	lcIsPEMH            = ucrsEntidadeFact.isPEMH
 	lcIsVaccine         = ucrsEntidadeFact.prestacao
 	
 	** limpa cursor
 	IF(USED("ucrsEntidadeFact"))
		fecha("ucrsEntidadeFact")
	ENDIF
	
 	
	&& Verificar entidade - e_fact=	
	IF(!lcTemFactEletronica)
		uf_perguntalt_chama("Funcionalidade dispon�vel apenas para entidades com fatura��o eletr�nica.", "Ok", "", 16)            	
		RETURN .f.
	ENDIF
	
	
	
	
	&& Valida a liga��o � Internet 
	IF uf_gerais_verifyInternet() == .f.
		uf_perguntalt_chama("ESTA FUNCIONALIDADE REQUER LIGA��O � INTERNET."+CHR(13)+"POR FAVOR VERIFIQUE A SUA LIGA��O.","OK","",16)
		RETURN .f.
	ENDIF
		
	&& N� de Contribuinte
	SELECT uCrsE1
	IF RECCOUNT("uCrsE1")>0
		IF EMPTY(uCrsE1.ncont)
			uf_perguntalt_chama("O NIF DA FARM�CIA N�O EST� CORRETAMENTE PREENCHIDO. POR FAVOR VERIFIQUE.", "Ok", "", 16)
			RETURN .f.
		ENDIF
	ENDIF

	&& Verifica a configura��o do c�digo de farm�cia
	SELECT ucrsE1
	IF EMPTY(ALLTRIM(ucrsE1.u_codfarm))
		uf_perguntalt_chama("O C�DIGO INFARMED DA FARM�CIA N�O EST� CORRETAMENTE PREENCHIDO. POR FAVOR VERIFIQUE.","OK","", 16)
		RETURN .f.
	ENDIF 
	
	&& Verifica a configura��o do ID do Cliente
	SELECT uCrsE1
	IF EMPTY(ALLTRIM(uCrsE1.id_lt))
		uf_perguntalt_chama("O C�DIGO DE CLIENTE LOGITOOLS N�O EST� CORRETAMENTE PREENCHIDO. POR FAVOR CONTACTE O SUPORTE.","OK","", 16)
		RETURN .f.
	ENDIF 
	lcIDCliente = ALLTRIM(uCrsE1.id_lt)
	**

	&& buscar ftstamp da fatura sns referente ao periodo definido no painel para verificar se envio j� foi aceite
	lcDataAux = ALLTRIM(str(myfacturacaoentidadesAno)) + Substr("00",1,Len("00")-Len(ALLTRIM(str(myfacturacaoentidadesMes)))) + ALLTRIM(str(myfacturacaoentidadesMes)) + '01'
	
	
	lcSQL = ''
	TEXT TO lcSQL TEXTMERGE NOSHOW 
		select TOP 1 ftstamp 
		FROM ft (nolock)
		INNER JOIN ft2(nolock) ON ft.ftstamp = ft2.ft2stamp 
		INNER JOIN td (nolock) ON td.ndoc = ft.ndoc 
		WHERE ft.estab = <<lcNrEntidadeDep>> AND ft.no = <<lcNrEntidade>> AND td.u_tipodoc = 1 AND ft.cdata = DATEADD(DAY, -1, DATEADD(MONTH, DATEDIFF(MONTH, 0, '<<lcDataAux>>')+1, 0)) AND ft.site = '<<mySite>>' 
		and ft2.id_efr_externa = '<<ALLTRIM(lcAbrev)>>'
		order by ft.fno desc
	ENDTEXT 
	


	IF !uf_gerais_actGrelha("","uCrsAuxFT",lcSQL)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VERIFICAR A INFORMA��O DA FACTURA. POR FAVOR CONTACTE O SUPORTE.", "Ok", "", 16)
		regua(2)
		RETURN .f.
	ELSE
		IF RECCOUNT("uCrsAuxFT") < 1
			uf_perguntalt_chama("AINDA N�O FOI EMITIDA FACTURA PARA O PERIODO SELECCIONADO. POR FAVOR VERIFIQUE.", "Ok", "", 16)
			regua(2)
			RETURN .f.
		ELSE
			select uCrsAuxFT
			lcStampFaturaSNS = ALLTRIM(uCrsAuxFT.ftstamp)
		
		ENDIF
	ENDIF 
	
	&& verificar se fatura j� foi enviada e envia novamente caso n�o tenha sido
	lcSQL = ''
	TEXT TO lcSQL TEXTMERGE NOSHOW 
		select TOP 1 ftstamp FROM [faturacao_eletronica_med](nolock) WHERE aceite = 1 AND ftstamp = '<<lcStampFaturaSNS>>' 
	ENDTEXT 
	IF !uf_gerais_actGrelha("","uCrsAuxFTStAMP",lcSQL)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VERIFICAR A INFORMA��O DA FACTURA. POR FAVOR CONTACTE O SUPORTE.", "Ok", "", 16)
		regua(2)
		RETURN .f.
	ELSE
		IF RECCOUNT("uCrsAuxFTStAMP") > 0
			uf_perguntalt_chama("FACTURA J� ENVIADA PARA O PERIODO SELECCIONADO. POR FAVOR VERIFIQUE.", "Ok", "", 16)
			regua(2)
			RETURN .f.
		ELSE
			&& Procede ao envio da fatura
			regua(0,4,"A preparar Fatura Eletronicamente...")
			&&
			regua(1,1,"A configurar software para envio...")
			

			
			
			LOCAL lcDc 
            lcAgenda  = uf_facturacaoentidades_deveAgendarEnvio(lcNrEntidade)
            
          	&&if(EMPTY(lcIsMcdt))
        
			if!(uf_facturacaoentidades_imprimeFactpdf(lcStampFaturaSNS))
				uf_perguntalt_chama("DOCUMENTO N�O ENVIADO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				regua(2)
				RETURN .f.
			ENDIF
			
			&&endif
			&& se deve agendar.
			&& para agendar a farmacia deve ser DataCenter e a entidade SNS
         
	
			**se  farmacia  local
			
				&& Verifica se software existe
			lcNomeJar = 'FaturacaoEletronicaMedicamentos.jar'
			
			LOCAL lcNomeJarMcdt 
			lcNomeJarMcdt = 'FaturacaoEletronicaMcdt.jar'
			
			LOCAL lcNomeJarPemH 
			lcNomeJarPEMH = 'FaturacaoEletronicaPEMH.jar'
			
			LOCAL lcNomeJarVacinas
			lcNomeJarVacinas= 'FaturacaoEletronicaVacinas.jar'
			
	

			IF !FILE(ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\ltsFaturacaoEletronica\' + ALLTRIM(lcNomeJar))
				uf_perguntalt_chama("FICHEIRO DE CONFIGURA��O N�O ENCONTRADO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				regua(2)
				RETURN .f.
			ENDIF 
		
			
			IF !FILE(ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\ltsFaturacaoEletronicaMcdt\' + ALLTRIM(lcNomeJarMcdt))
				uf_perguntalt_chama("FICHEIRO DE CONFIGURA��O N�O ENCONTRADO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				regua(2)
				RETURN .f.
			ENDIF 
			
	
			IF !FILE(ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\ltsFaturacaoEletronicaVacinas\' + ALLTRIM(lcNomeJarVacinas))
				uf_perguntalt_chama("FICHEIRO DE CONFIGURA��O N�O ENCONTRADO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				regua(2)
				RETURN .f.
			ENDIF 
			
			&&TODO SD-68009
*!*				IF !FILE(ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\ltsFaturacaoEletronicaPEMH\' + ALLTRIM(lcNomeJarPEMH))
*!*					uf_perguntalt_chama("FICHEIRO DE CONFIGURA��O N�O ENCONTRADO OU EM DESENVOLVIMENTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
*!*					regua(2)
*!*					RETURN .f.
*!*				ENDIF 
			
			&& Verifica o servidor de BD
			IF !USED("uCrsServerName")
				lcSQL  = ''
				TEXT TO lcSQL NOSHOW TEXTMERGE 
					Select @@SERVERNAME as dbServer
				ENDTEXT 
				IF !uf_gerais_actGrelha("", "ucrsServerName", lcSQL)
					uf_perguntalt_chama("OCORREU UMA ANOMALIA. POR FAVOR CONTACTE O SUPORTE." ,"OK","",16)
					regua(2)
					RETURN  .f.
				ENDIF
			ENDIF
			
			SELECT ucrsServerName
			lcDbServer = ALLTRIM(ucrsServerName.dbServer)
			
			&&
			regua(1,2,"A recolher informa��o para envio...")
			&& recolher informa��o daqui
			&& campo cdata na ft tem a data do ultimo dia ao mes que corresponde o documento
			lcStampLigacao = uf_gerais_stamp()
			lcAno = ALLTRIM(STR(myfacturacaoentidadesAno))
			lcMes = ALLTRIM(STR(myfacturacaoentidadesMes))
			
			lcResultadoFatura = ALLTRIM(STR(0))
			
			
			LOCAL lcNomeFinalJar, lcPath
			
			DO CASE
				CASE !(EMPTY(lcIsPemH)) AND lcIsPemH
					lcNomeFinalJar = lcNomeJarPemH
					lcPath =  ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\ltsFaturacaoEletronica\'

				CASE !(EMPTY(lcIsMcdt)) and lcIsMcdt					
					lcNomeFinalJar = lcNomeJarMcdt 
					lcPath =  ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\ltsFaturacaoEletronicaMcdt\'
					
				OTHERWISE
					lcNomeFinalJar = lcNomeJar
					lcPath =  ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\ltsFaturacaoEletronica\'
			ENDCASE
			
			
			if(EMPTY(lcIsMcdt))
				lcNomeFinalJar = lcNomeJar
				lcPath =  ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\ltsFaturacaoEletronica\'
			ELSE
				
			ENDIF
			
			**se vacinas
			if(!EMPTY(lcIsVaccine))
				lcNomeFinalJar = lcNomeJarVacinas
				lcPath =  ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\ltsFaturacaoEletronicaVacinas\'
			ENDIF
			
			
			&&SD-68009
			if(!EMPTY(lcIsPemH))
				&&uf_perguntalt_chama("Funcionalidade indispon�vel." ,"OK","",16)
				&&regua(2)
				lcNomeFinalJar = lcNomeJarPemH
				lcPath =  ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\ltsFaturacaoEletronicaPemh\'
	
			ENDIF
			
	

			if(EMPTY(lcAgenda ))	
				&&
				regua(1,3,"A enviar informa��o...")
				&& efetua envio da fatura
				lcWsPath = lcPath + ALLTRIM(lcNomeFinalJar);
						+ " " + ALLTRIM(lcStampLigacao);	
						+ " " + lcStampFaturaSNS;
						+ " " + lcIDCliente;
						+ " " + lcAno;
						+ " " + lcMes;
						+ " " + ["] + ALLTRIM(mySite) + ["];
						+ " " + lcResultadoFatura;
						+ " 0"

				&& Envia comando Java
				lcWsPath = "javaw -jar " + lcWsPath 
				
				&& aguarda que software termine o envio da informa��o 3� PARAMETRO = .T.
				oWSShell = CREATEOBJECT("WScript.Shell")
				oWSShell.Run(lcWsPath, 1, .t.)
				
				IF emdesenvolvimento &&debug mode
	 				_cliptext = lcWsPath 
	 			ENDIF

				
		
				
				
				&& verifica resposta 
				regua(1,4,"A verificar resposta dos Servi�os Partilhados...")

				lcSQL = ''
				TEXT TO lcSQL TEXTMERGE NOSHOW 
					select TOP 1 * FROM faturacao_eletronica_med (nolock) WHERE ftstamp = '<<lcStampFaturaSNS>>' ORDER BY data desc
				ENDTEXT 
				IF !uf_gerais_actGrelha("","uCrsAuxResposta",lcSQL)
					uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VERIFICAR A RESPOSTA DOS SERVI�OS PARTILHADOS. POR FAVOR CONTACTE O SUPORTE.", "Ok", "", 16)
					regua(2)
					RETURN .f.
				ELSE
					IF RECCOUNT("uCrsAuxResposta") > 0 
						IF uCrsAuxResposta.aceite = .t.
							regua(2)
							uf_perguntalt_chama("ENVIO DA FATURA EFETUADO COM SUCESSO.", "Ok", "", 16)
							RETURN .t.
						ELSE
							lcSQL = ''
							TEXT TO lcSQL TEXTMERGE NOSHOW 
								select rtrim(ltrim(response_code)) as response_code , rtrim(ltrim(response_descr)) as   response_descr from faturacao_eletronica_med_d (nolock) where id_faturacao_eletronica_med = '<<uCrsAuxResposta.id>>' 
							ENDTEXT 	
							IF !uf_gerais_actGrelha("facturacaoentidades.gridResposta","uCrsAuxRespostaDet",lcSQL)
								uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VERIFICAR A RESPOSTA DOS SERVI�OS PARTILHADOS. POR FAVOR CONTACTE O SUPORTE.", "Ok", "", 16)
								regua(2)
								RETURN .f.
							ELSE
								&& colocar informa��o detalhada do erro aqui - 
								uf_perguntalt_chama("O DOCUMENTO ENVIADO CONT�M ERROS. POR FAVOR CONTACTE O SUPORTE T�CNICO PARA AN�LISE.", "Ok", "", 16)
								facturacaoentidades.refresh
								regua(2)
								RETURN .f.
							ENDIF
						ENDIF
					ELSE
						regua(2)
						uf_perguntalt_chama("OCORREU UMA ANOMALIA AO ENVIAR A FACTURA. POR FAVOR CONTACTE O SUPORTE.", "Ok", "", 16)
						RETURN .t.
					ENDIF
				ENDIF
			&&se DC
			ELSE
				regua(1,2,"A agendar informa��o para envio...")
				if(uf_facturacaoentidades_agendaEnvioCron(ALLTRIM(lcStampLigacao),lcStampFaturaSNS,lcIDCliente,lcAno,lcMes,ALLTRIM(mySite),lcDbServer ))
					uf_perguntalt_chama("ENVIO DA FATURA AGENDADO COM SUCESSO.", "Ok", "", 64)
				ENDIF
					
				
			ENDIF
			
			regua(2)
		ENDIF
	ENDIF
	
	IF USED("uCrsAuxFT")
		fecha("uCrsAuxFT")
	ENDIF
	
	IF USED("uCrsAuxFTStAMP")
		fecha("uCrsAuxFTStAMP")
	ENDIF
	
	IF USED("uCrsAuxResposta")
		fecha("uCrsAuxResposta")
	ENDIF
	
ENDFUNC

FUNCTION uf_facturacaoentidades_agendaEnvioCron
	LPARAMETERS lcStampLigacao, lcStampFaturaSNS, lcIDCliente, lcAno, lcMes, lcSite, lcDbServer 
	
	
	LOCAL lcbd 
	LOCAL lcremotesql 
	LOCAL lcPrio
	
	lcPrio = 1
	
	lcbd = SUBSTR(uf_gerais_getparameter_site('ADM0000000090', 'TEXT', mysite), AT('.[', uf_gerais_getparameter_site('ADM0000000090', 'TEXT', mysite))+1, 30)
    lcremotesql = LEFT(uf_gerais_getparameter_site('ADM0000000090', 'TEXT', mysite), AT('.[', uf_gerais_getparameter_site('ADM0000000090', 'TEXT', mysite))-1)
    

    TEXT TO lcsql TEXTMERGE NOSHOW
        DECLARE @RunStoredProcSQL VARCHAR(1000);
        SET @RunStoredProcSQL = 'EXEC <<lcbd>>.[dbo].up_receituario_agenda_envio ''<<ALLTRIM(lcStampLigacao)>>'', ''<<ALLTRIM(lcStampFaturaSNS)>>'', ''<<ALLTRIM(lcIDCliente)>>'', ''<<lcAno)>>'', ''<<lcMes)>>'',  ''<<ALLTRIM(lcSite)>>'',  ''<<ALLTRIM(lcDbServer )>>'',  ''<<ALLTRIM(m_chinis)>>'',  ''<<lcPrio>>'''   
        EXEC (@RunStoredProcSQL) AT <<lcRemoteSQL >>;
    ENDTEXT
    
           
    IF !uf_gerais_actGrelha("",[ucrsTempResult],lcSql)
        uf_perguntalt_chama("N�O FOI POSS�VEL AGENDAR O ENVIO DA FATURA ELETRONICA","OK","",16)
        RETURN .f.
    ENDIF
    
	RETURN .t.
ENDFUNC

FUNCTION  uf_facturacaoentidades_deveAgendarEnvio
    LPARAMETERS lcNrEntidade
    
     IF(!EMPTY(uf_gerais_getParameter("ADM0000000323","BOOL")) AND lcNrEntidade==1 AND  !EMPTY(ALLTRIM(uf_gerais_getparameter_site('ADM0000000128', 'TEXT', mySite))))     	
     	RETURN .t.
     ELSE
     	RETURN .f.
     ENDIF
     
    
 

ENDFUNC






**
FUNCTION uf_facturacaoentidades_imprimeFactpdf
	LPARAMETERS lnstamp

	PUBLIC myIsentoIva
	STORE .f. TO myIsentoIva

	LOCAL lcimpressao, lcDefaultPrinter, lcnomeFicheiro, LcSQL, lclocalFicheiroLt, lcDefaultWprinter, lcPathFicheiroLt
	STORE '' TO lclocalFicheiroLt

	&& impressora default windows local lcSaveWinDefPrinter 
*!*		lcDefaultWprinter = SET("PRINTER", 2)

	&& func preparar PDF creator
*!*		uf_facturacaoentidades_preparaPDFCreator()
	
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_gerais_iduCert '<<ALLTRIM(lnstamp)>>'
 	ENDTEXT
 	IF !uf_gerais_actGrelha("","ucrsHash",lcSQL)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA A VERIFICAR A CERTIFICA��O DO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
 		RETURN .f.
 	ENDIF

	IF !RECCOUNT("ucrsHash") > 0
		SELECT ucrsHash
		APPEND Blank
		replace ucrsHash.temcert	WITH .f.
		replace uCrsHash.parte1		WITH ''
		replace uCrsHash.parte1		WITH ''
	ENDIF
	
	fecha("FTQRCODE")

	*** Obtem Dados da Tabela de Registo Digital
	IF !USED("FT") OR !USED("FT2") OR !USED("FI") OR !USED("FTQRCODE")
		lcSQL = ""
		TEXT TO lcSQL TEXTMERGE NOSHOW
			Select 	*
			From  	FT (nolock)
			Where 	ftstamp='<<Alltrim(lnstamp)>>'
		ENDTEXT
		IF !uf_gerais_actGrelha("","FT",lcSQL)
			uf_perguntalt_chama("OCORREU UMA ANOMALIA A VERIFICAR A INFORMA��O DO DOCUMENTO [ft]. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			RETURN .f.
		ENDIF 
		
		lcSQL = ""
		TEXT TO lcSQL TEXTMERGE NOSHOW
			Select  *
			From 	FT2 (nolock)
			Where 	ft2stamp='<<Alltrim(lnstamp)>>'
		ENDTEXT
		
		IF !uf_gerais_actGrelha("","FT2",lcSQL)
			uf_perguntalt_chama("OCORREU UMA ANOMALIA A VERIFICAR A INFORMA��O DO DOCUMENTO [ft2]. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			RETURN .f.
		ENDIF 
		
		lcSQL = ""
		TEXT TO lcSQL TEXTMERGE NOSHOW
			Select 	*
			From 	FI (nolock) 
			Where 	ftstamp='<<Alltrim(lnstamp)>>' and epromo=0
			order by lordem
		ENDTEXT
		If !uf_gerais_actGrelha("","FI",lcSQL)
			uf_perguntalt_chama("OCORREU UMA ANOMALIA A VERIFICAR A INFORMA��O DO DOCUMENTO [fi]. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			RETURN .f.
		ENDIF
		
		
		lcSQL = ""
		TEXT TO lcSQL TEXTMERGE NOSHOW
			exec up_print_qrcode_a4 '<<Alltrim(lnstamp)>>'
		ENDTEXT
		If !uf_gerais_actGrelha("","FTQRCODE",lcSQL)
			uf_perguntalt_chama("OCORREU UMA ANOMALIA A VERIFICAR A INFORMA��O DO DOCUMENTO [fi]. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			RETURN .f.
		ENDIF
		
		
		
	ENDIF
	
	

	
			
	Select FT
	GO TOP
	SELECT FT2
	GO TOP
	Select Fi
	
	fecha("ucrsTempDadosDc")
	
	** OBTEM O TIPO DE DOCUMENTO DE FACTURACAO 	
	lcSQL = ""
    TEXT TO lcSQL TEXTMERGE NOSHOW
            Select *
            from td (nolock)
            where nmdoc = '<<Alltrim(FT.nmdoc)>>'
    ENDTEXT
    IF !uf_gerais_actGrelha("","TD",lcSQL)
        uf_perguntalt_chama("OCORREU UMA ANOMALIA A CALCULAR INFORMA��O PARA A IMPRESSAO. COD.4","OK","",16)
        RETURN .f.
    ENDIF 	
	
	** OBTEM SE � ENTIDADE CERTIFICADA
	LOCAL lcDigitalCert
	lcDigitalCert  = .f.

	
	SELECT FT
	lcDigitalCert = uf_facturacaoentidades_validaSeEntidadeCertificavel(FT.no)
	
	**
	
	** guardar data e n� de atendimento para o nome do ficheiro pdf **
	SELECT Ft
	GO TOP
	lcNomeFicheirolt = ALLTRIM(uCrsE1.id_lt) +"_"+ ALLTRIM(astr(Ft.ftstamp))
	**

	** Veriricar se existem produtos isentos de iva **
	SELECT fi
	GO top
	SCAN FOR fi.iva==0
		myIsentoIva = .t.
	ENDSCAN
	SELECT fi 
	GO TOP
	**

	** Guardar report a imprimir - ficheiro **
	** Obtem dados a partir da tabela de impressoes **
	SELECT ft
	IF uf_facturacaoentidades_validaSeSNS("",ft.no)
		lcNomeFicheiro = "factura_sns_pdf.frx"
	ELSE
		lcNomeFicheiro = "factura_entidades.frx"
	ENDIF
	

	
	**se for mcdt
	if(ALLTRIM(FT.nmdoc)=="Factura SNS MCDT")
		lcNomeFicheiro = "factura_mcdt.frx"	
	ENDIF
	

	IF FILE(ALLTRIM(myPath)+"\analises\" + ALLTRIM(lcnomeFicheiro))
		lcreport = ALLTRIM(uCrsPath.textvalue)+"\analises\" + Alltrim(lcnomeFicheiro)
	ELSE
		uf_perguntalt_chama("N�o foi encontrado o modelo de fatura. Por favor contacte o Suporte. Obrigado.","OK","",16)
		RETURN .f.
	ENDIF
	**	
	


	** Se for uma factura a entidades 
	If (td.u_tipodoc)=1
		Select Fi
		
		PUBLIC lnTot,pvp1,pvp2,pvp3,pvp4,pvp5,pvp6,pvp7,pvp8,pvp9, mes
		PUBLIC MycompUtente1, MycompUtente2, MycompUtente3, MycompUtente4, MycompUtente5, MycompUtente6, MycompUtente7, MycompUtente8, MycompUtente9		
		&&O relatorio factura_entidades usa estas variaveis
		PUBLIC compUtente1, compUtente2, compUtente3, compUtente4, compUtente5, compUtente6, compUtente7, compUtente8, compUtente9
		****
		PUBLIC MycompOrg1, MycompOrg2, MycompOrg3, MyCompOrg4, MycompOrg5, MycompOrg6, MycompOrg7, MyCompOrg8, MyCompOrg9
		public fee1,fee2,fee3,fee4,fee5,fee6,fee7,fee8,fee9, feetotal, feetotalbi, feetotaliva

		CALCULATE SUM(Fi.etiliquido) TO lnTot
		inTot = uf_gerais_extenso(lnTot)
		
		** Calcula Totais 
		STORE 0 TO pvp1, pvp2, pvp3, pvp4, pvp5, pvp6, pvp7, pvp8, pvp9
		STORE 0 TO MycompUtente1, MycompUtente2, MycompUtente3, MycompUtente4, MycompUtente5, MycompUtente6, MycompUtente7, MycompUtente8, MycompUtente9
		STORE 0 TO MycompOrg1, MycompOrg2, MycompOrg3, MyCompOrg4, MycompOrg5, MycompOrg6, MycompOrg7, MyCompOrg8, MyCompOrg9
		STORE 0 TO fee1,fee2,fee3,fee4,fee5,fee6,fee7,fee8,fee9, feetotal, feetotalbi, feetotaliva
		


		SELECT Fi
		GO TOP 
		SCAN    
			DO CASE 
				CASE fi.tabiva==1
					pvp1 = pvp1 + Fi.altura
					MycompUtente1 = MycompUtente1 + Fi.largura
					MycompOrg1 = MycompOrg1 + Fi.ETILIQUIDO
					fee1 = fee1 + FI.pvp4_fee
					
				CASE Fi.tabiva == 2 
					pvp2 = pvp2 + Fi.altura
					MycompUtente2 = MycompUtente2 + Fi.largura
					MycompOrg2 = MycompOrg2 + Fi.ETILIQUIDO
					fee2 = fee2 + FI.pvp4_fee
					
				CASE Fi.tabiva == 3
					pvp3 = pvp3 + Fi.altura
					MycompUtente3 = MycompUtente3 + Fi.largura
					MycompOrg3 = MycompOrg3 + Fi.ETILIQUIDO
					fee3 = fee3 + FI.pvp4_fee
					
				CASE Fi.tabiva == 4
					pvp4 = pvp4 + Fi.altura
					MycompUtente4 = MycompUtente4 + Fi.largura
					MycompOrg4 = MycompOrg4 + Fi.ETILIQUIDO
					fee4 = fee4 + FI.pvp4_fee
					
				CASE fi.tabiva == 5
					pvp5 = pvp5 + fi.altura
					MycompUtente5 = MycompUtente5 + fi.largura
					MycompOrg5 = MycompOrg5 + FI.ETILIQUIDO
					fee5 = fee5 + FI.pvp4_fee
					
				CASE fi.tabiva == 6 
					pvp6 = pvp6 + fi.altura
					MycompUtente6 = MycompUtente6 + fi.largura
					MycompOrg6 = MycompOrg6 + FI.ETILIQUIDO
					fee6 = fee6 + FI.pvp4_fee
					
				CASE fi.tabiva == 7
					pvp7 = pvp7 + fi.altura
					MycompUtente7 = MycompUtente7 + fi.largura
					MycompOrg7 = MycompOrg7 + FI.ETILIQUIDO
					fee7 = fee7 + FI.pvp4_fee
					
				CASE fi.tabiva == 8
					pvp8 = pvp8 + fi.altura
					MycompUtente8 = MycompUtente8 + fi.largura
					MycompOrg8 = MycompOrg8 + FI.ETILIQUIDO
					fee8 = fee8 + FI.pvp4_fee
					
				CASE fi.tabiva == 9
					pvp9 = pvp9 + fi.altura
					MycompUtente9 = MycompUtente9 + fi.largura
					MycompOrg9 = MycompOrg9 + FI.ETILIQUIDO
					fee9 = fee9 + FI.pvp4_fee
					
			ENDCASE 
			
			IF ALLTRIM(fi.design)=='Remunera��o Espec�fica'
				**MESSAGEBOX(fi.etiliquido)
				feetotal = uf_gerais_calcFee(fi.etiliquido)
				feetotalbi = uf_gerais_calcFee(ROUND(fi.etiliquido / 1.06, 2))
				feetotaliva = uf_gerais_calcFee(fi.etiliquido - ROUND(fi.etiliquido / 1.06, 2))
			ENDIF 
			
			If fi.iva == 0 AND fi.epromo == .f.
				myIsentoIva = .t.
			ENDIF			
			
			SELECT Fi
		ENDSCAN   
		**
		
		
		**variaveis usadas no relatorio factura_entidades
		compUtente1 = MycompUtente1
		compUtente2 = MycompUtente2
		compUtente3 = MycompUtente3
		compUtente4 = MycompUtente4
		compUtente5 = MycompUtente5
		compUtente6 = MycompUtente6
		compUtente7 = MycompUtente7
		compUtente8 = MycompUtente8
		compUtente9 = MycompUtente9
					
		** calcula o mes
		myMesDocumento = ""
		SELECT FT
		GO TOP 
		myMesDocumento = uf_Gerais_CalculaMes(Month(FT.fdata))
		**
		SELECT Fi
		
		lcSQL=""
		TEXT TO lcSQL TEXTMERGE NOSHOW
			exec up_facturacao_ImprimefacturacaoEntidades '<<Alltrim(lnstamp)>>'
		ENDTEXT
		
		IF !uf_gerais_actGrelha("","tempFi",lcSQL)
			uf_perguntalt_chama("OCORREU UM ERRO A CALCULAR INFORMACAO PARA A IMPRESSAO. Cod.6","OK","",16)
 			RETURN .f.
 		ENDIF


		Select FT
		GO TOP
		SELECT FT2
		GO TOP
		Select Fi
		GO TOP
	ENDIF
	

	&& n�o precisava da condi��o, mas caso a venha ser usado para outras entidades fica j� feito 
	SELECT ft
	IF FT.no == 1 OR ft.no==142
		uf_imprimirgerais_dataMatrixCriaImagem(ft.ndoc)
	ENDIF 
		
	Select Fi
	IF (TD.u_tipodoc)==1
		Select tempFi
	ENDIF

	&& certificacao
	uf_gerais_actGrelha("","ucrsTextoRodape",[exec up_cert_TextoRodape '] + ALLTRIM(td.tiposaft) + ['])
	
	&& Metodo FoxyPreviewer
	
	
	&&se local ou datacenter

	
	SELECT ft
	IF(!uf_facturacaoentidades_deveAgendarEnvio(FT.no))
		lcPathFicheiroLt = ALLTRIM(uf_gerais_getParameter("ADM0000000015","TEXT")) + "\Cache\" + ALLTRIM(lcNomeFicheirolt) + ".pdf"
	ELSE
		lcPathFicheiroLt = ALLTRIM(uf_gerais_getparameter_site('ADM0000000128', 'TEXT', mySite)) + "\FE\" + ALLTRIM(lcNomeFicheirolt) + ".pdf"	
	ENDIF
	

	
	SET PROCEDURE TO LOCFILE("FoxBarcodeQR.prg") ADDITIVE
	PRIVATE poFbc
	m.poFbc = CREATEOBJECT("FoxBarcodeQR")

*!*		m.poFbc.nCorrectionLevel = 3 
*!*		m.poFbc.nBarColor = RGB(0, 0, 0)




	CREATE CURSOR TempQR (TempQR I)
	INSERT INTO TempQR VALUES (0)

	&&
	DO FoxyPreviewer.App
	
	SET PROCEDURE TO LOCFILE("FoxyPreviewer.App") ADDITIVE 
	LOCAL loReport as "PreviewHelper" OF "FoxyPreviewer.App" 
	loReport = CREATEOBJECT("PreviewHelper") 
	


	
	&& select cursor p evitar bug das linhas repetidas.
	SELECT Fi
	GO TOP
	SELECT FI
	IF (TD.u_tipodoc)==1
		Select tempFi
		GO TOP
		Select tempFi
	ENDIF	


	WITH loReport as ReportHelper 
        .AddReport(ALLTRIM(lcReport), "NODIALOG") 
        .cDestFile = ALLTRIM(lcPathFicheiroLt)  && Use to create an output without previewing
        .RunReport() 
	ENDWITH 
	
	

	if(!EMPTY(lcDigitalCert)) 
		if(!uf_facturacao_certficarDocumento('', ALLTRIM(lcPathFicheiroLt), ALLTRIM(lcNomeFicheirolt)+".pdf",.F.))
			uf_facturacaoentidades_limparVar()
			RETURN .f.
		ENDIF	
	ENDIF
		


	&& loReport = NULL 

*!*		&& Metodo pdf creator
*!*		** Imprimir para PDF
*!*		ReadyState = 0
*!*		oPDFC.cOption("AutosaveFilename")  = LTRIM(lcNomeFicheiroLt)

*!*		&& Grava automaticamente no diretorio 
*!*		oPDFC.cOption("AutosaveDirectory") = ALLTRIM(uf_gerais_getParameter("ADM0000000015","TEXT")) + "\Cache\"
*!*		oPDFC.cprinterstop=.F.
*!*		
*!*	 	REPORT FORM Alltrim(lcreport) TO PRINTER NOCONSOLE

*!*		&&Fecha tramento de impress�o
*!*		INKEY(1)


	
	&& coloca impressora por default a q estava anteriormente
*!*		MESSAGEBOX(lcDefaultWprinter)
*!*		SET PRINTER TO NAME (lcDefaultWprinter)

	uf_facturacaoentidades_limparVar()

	RETURN .t.


ENDFUNC

FUNCTION uf_facturacaoentidades_devolveNomeJavaFacturacaoEletronica
	LPARAMETERS lcNo, lcEstab , lcAbrev

	LOCAL lcPath
	lcPath =  "\ltsFaturacaoEletronica\FaturacaoEletronicaMedicamentos.jar"
	
	fecha("ucrsTempDadosDoc")
	
	** Procura entidade por loja
	TEXT TO lcSQL TEXTMERGE NOSHOW
	
			Select 
				ISNULL(prestacao,	0) as prestacao,
				ISNULL(pemh,		0) as pemh				
			from cptorg (nolock)
			inner join cptorg_dep_site on cptorg.cptorgStamp = cptorg_dep_site.cptorgstamp 
			where cptorg_dep_site.no = <<lcNo>> and cptorg_dep_site.estab = <<lcEstab>> 
			and abrev = '<<lcAbrev>>' and cptorg_dep_site.site = '<<mysite>>'
	ENDTEXT
	IF !uf_gerais_actGrelha("","ucrsTempDadosDoc",lcSQL)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA A VALIDAR TIPO DE ENTIDADE","OK","",16)
		RETURN .f.
	ENDIF 
	

	
	** Procura entidade por loja geral
	if(RECCOUNT("ucrsTempDadosDoc")==0)
		fecha("ucrsTempDadosDoc")
		TEXT TO lcSQL TEXTMERGE NOSHOW
				Select 
					ISNULL(prestacao,	0) as prestacao,
					ISNULL(pemh,		0) as pemh
				from cptorg (nolock)
				where cptorg.u_no = <<lcNo>> and cptorg.u_estab = <<lcEstab>>
		ENDTEXT
		IF !uf_gerais_actGrelha("","ucrsTempDadosDoc",lcSQL)
			uf_perguntalt_chama("OCORREU UMA ANOMALIA A VALIDAR TIPO DE ENTIDADE","OK","",16)
			RETURN .f.
		ENDIF 
	ENDIF
	

	
	
	if(USED("ucrsTempDadosDoc"))
		SELECT ucrsTempDadosDoc
		GO TOP
		DO CASE
			CASE ucrsTempDadosDoc.prestacao 
				lcPath  = "\ltsFaturacaoEletronicaVacinas\FaturacaoEletronicaVacinas.jar"
			CASE  ucrsTempDadosDoc.pemh 
				lcPath  = "\ltsFaturacaoEletronicaPEMH\FaturacaoEletronicaPEMH.jar"
			OTHERWISE
				**
		ENDCASE
		
		
				
	ENDIF
	

	
	
	RETURN ALLTRIM(lcPath) 
	
ENDFUNC


FUNCTION  uf_facturacaoentidades_validaSeEntidadeCertificavel()
	LPARAMETERS lcNo
	LOCAL lcDigitalCert 
	lcDigitalCert = .f.
	
		
	fecha("ucrsTempDadosDc")
	
	
	SELECT ucrsE1
	&&na anf n�o deve assinar o receituario
	if(UPPER(ALLTRIM(ucrsE1.u_assfarm))=='ANF')
		RETURN .f.
	ENDIF
	
	
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
			Select ISNULL(digitalCert,0) as digitalCert
			from cptorg (nolock)
			where cptorg.u_no = <<lcNo>>
	ENDTEXT
	IF !uf_gerais_actGrelha("","ucrsTempDadosDc",lcSQL)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA A CALCULAR INFORMA��O PARA A IMPRESS�O. COD.4","OK","",16)
		RETURN .f.
	ENDIF 
	
	if(USED("ucrsTempDadosDc"))
		SELECT ucrsTempDadosDc
		GO TOP
		lcDigitalCert = ucrsTempDadosDc.digitalCert
	ENDIF
	
		
	fecha("ucrsTempDadosDc")
	
	RETURN lcDigitalCert 
	
ENDFUNC


PROCEDURE  uf_facturacaoentidades_limparVar()
	&& fecha cursores e relase variavel publica pdfcreator
	IF USED("uCrsHash")
		fecha("uCrsHash")
	ENDIF
	
	IF USED("FT")
		fecha("FT")
	ENDIF
	IF USED("FT2")
		fecha("FT2")
	ENDIF
	IF USED("FI")
		fecha("FI")
	ENDIF
	
	IF USED("FTQRCODE")
		fecha("FTQRCODE")
	ENDIF
	
	IF USED("tempFi")
		fecha("tempFi")
	ENDIF

*!*		RELEASE oPDFC



	RELEASE MycompUtente1 
	RELEASE MycompUtente2
	RELEASE MycompUtente3
	RELEASE MycompUtente4
	RELEASE MycompUtente5
	RELEASE MycompUtente6
	RELEASE MycompUtente7
	RELEASE MycompUtente8
	RELEASE MycompUtente9
	
	RELEASE compUtente1 
	RELEASE compUtente2
	RELEASE compUtente3
	RELEASE compUtente4
	RELEASE compUtente5
	RELEASE compUtente6
	RELEASE compUtente7
	RELEASE compUtente8
	RELEASE compUtente9
		
	RELEASE MycompOrg1
	RELEASE MycompOrg2
	RELEASE MycompOrg3
	RELEASE MyCompOrg4
	RELEASE MycompOrg5
	RELEASE MycompOrg6
	RELEASE MycompOrg7
	RELEASE MyCompOrg8
	RELEASE MyCompOrg9

	RELEASE fee1
	RELEASE fee2
	RELEASE fee3
	RELEASE fee4
	RELEASE fee5
	RELEASE fee6
	RELEASE fee7
	RELEASE fee8
	RELEASE fee9
	
	RELEASE lnTot
	RELEASE pvp1
	RELEASE pvp2
	RELEASE pvp3
	RELEASE pvp4
	RELEASE pvp5
	RELEASE pvp6
	RELEASE pvp7
	RELEASE pvp8
	RELEASE pvp9
	RELEASE mes
	

ENDPROC	

** 
FUNCTION uf_facturacaoentidades_preparaPDFCreator
	TRY
		PUBLIC oPDFC
		oPDFC  = Createobject("PDFCreator.clsPDFCreator","pdfcreator")
		oPDFC.cStart("/NoProcessingAtStartup")
		oPDFC.cVisible = .f.
		oPDFC.cOption("UseAutosave") = 1
		oPDFC.cOption("UseAutosaveDirectory") = 1
		oPDFC.cOption("AutosaveFormat") = 0         
		oPDFC.cOption("DisableUpdateCheck") = 0         
		DefaultPrinter = oPDFC.cDefaultprinter
		oPDFC.cDefaultprinter = "PDFCreator"
		oPDFC.cClearCache
	CATCH
	ENDTRY
	
ENDFUNC

FUNCTION uf_facturacaoentidades_calculaTotalReceitasDocumento
	LPARAMETERS lcFtstamp
	
	local lcTotalReceita
	lcTotalReceita = 0
	
	fecha("ucrsAuxTotalReceita")
	if(!EMPTY(lcFtstamp))
		uf_gerais_meiaRegua("A calcular totais...")
		TEXT TO lcSQL TEXTMERGE NOSHOW
			exec up_receituario_facturacaoEntidades_getTotalReceitas  '<<Alltrim(lcFtstamp)>>'
	 	ENDTEXT
	 		
	 		
	 	IF !uf_gerais_actGrelha("" ,"ucrsAuxTotalReceita",lcSQL)
	 		regua(2)	
			uf_perguntalt_chama("OCORREU UMA ANOMALIA AO CALCULAR O TOTAL DE RECEITAS. POR FAVOR CONTACTE O SUPORTE.", "Ok", "", 16)
		ENDIF
		regua(2)
			
	ENDIF
	
	if(USED("ucrsAuxTotalReceita"))
		select ucrsAuxTotalReceita
		GO TOP
		lcTotalReceita = ucrsAuxTotalReceita.nrReceitasTotal
	ENDIF
	
	
	fecha("ucrsAuxTotalReceita")
	
	RETURN lcTotalReceita
	
ENDFUNC



**
*!*	FUNCTION uf_facturacaoentidades_validaMcdtResultadoPartilhado
*!*		LPARAMETERS lcAbrev, lcAno, lcMes
*!*			
*!*			
*!*			fecha("ucrsValidaMCDTTemp")
*!*			TEXT TO lcSQL TEXTMERGE NOSHOW
*!*				exec up_receituario_validaSeLotesFechadosEntidade <<lcNoEfEnt>> ,<<myfacturacaoentidadesAno>>,<<myfacturacaoentidadesMes>>,'<<ALLTRIM(uf_gerais_getSitePostos())>>'
*!*	 		ENDTEXT
*!*	 		IF !uf_gerais_actGrelha("" ,"uCrsLotesAbertos",lcSQL)
*!*	 			regua(2)	
*!*				uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VALIDAR O ESTADO DA PARTILHA DE RESULTADOS DOS LOTES MDCT. POR FAVOR CONTACTE O SUPORTE.", "Ok", "", 16)
*!*				regua(2)
*!*				fecha("ucrsValidaMCDTTemp")
*!*				RETURN .f.
*!*			ENDIF
*!*			
*!*			fecha("ucrsValidaMCDTTemp")
*!*		
*!*		
*!*	ENDFUNC

