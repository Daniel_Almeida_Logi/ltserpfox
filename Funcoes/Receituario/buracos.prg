**
** Fun��o Principal
**
FUNCTION uf_BURACOS_chama
	LOCAL lcSQL, lcMaxReceita, lcNReceita, lcDif, i, lcMaxNrReceita
	STORE "" to lcSQL
	STORE 0 to lcMaxReceita, lcNReceita, lcDif, i, lcMaxNrReceita

	IF USED("uCrsLotesTapar")
		SELECT uCrsLotesTapar
		DELETE ALL
	ENDIF 

	SELECT uCrsCloseLotes
	GO TOP 
	SCAN FOR uCrsCloseLotes.buracos > 0 AND uCrsCloseLotes.fechado == .f.
		
		TEXT TO lcSQL TEXTMERGE NOSHOW 
			select  
				cptorgabrev,tlote,slote,lote,nreceita, ctltrctstamp 
			from  
				ctltrct (nolock)
			where 
				mes = <<uf_gerais_getMonthComboBox("RECEITUARIO.cbMes")>>
				and ano = <<receituario.spinnerAno.value>>
				and cptorgabrev='<<uCrsCloseLotes.cptorgabrev>>'
				and tlote = '<<uCrsCloseLotes.tlote>>'
				and slote = <<uCrsCloseLotes.slote>>
				and lote = <<uCrsCloseLotes.lote>>
				and site = '<<mysite>>'
			order by 
				cptorgabrev
				, tlote
				, slote
				, lote
				, nreceita
		ENDTEXT

		uf_gerais_actGrelha("", "uCrsLoteBuracos", lcSQL)
		
		SELECT uCrsLoteBuracos
		
		IF !USED("uCrsLotesTapar")
			select * from uCrsLoteBuracos where 1=0 INTO CURSOR uCrsLotesTapar READWRITE 
		ENDIF 
		
		&& verifica o nr da ultima receita do Lote
		SELECT uCrsLoteBuracos
		CALCULATE MAX(nreceita) TO lcMaxNrReceita
			
		&& verifica se existem buracos nos lotes
		SELECT uCrsLoteBuracos
		GO TOP 
		SCAN 
			lcNReceita = lcNReceita + 1

			IF uCrsLoteBuracos.nreceita != lcNReceita

				lcDif = uCrsLoteBuracos.nreceita - lcNReceita		

				FOR i=1 TO lcDif
					SELECT uCrsLotesTapar
					APPEND blank
					replace ;
						uCrsLotesTapar.cptorgabrev  WITH uCrsLoteBuracos.cptorgabrev ;
						uCrsLotesTapar.tlote        WITH uCrsLoteBuracos.tlote ;
						uCrsLotesTapar.slote 	    WITH uCrsLoteBuracos.slote ;
						uCrsLotesTapar.lote 	    WITH uCrsLoteBuracos.lote ;
						uCrsLotesTapar.nreceita     WITH lcNReceita ;
						uCrsLotesTapar.ctltrctstamp WITH uCrsLoteBuracos.ctltrctstamp
						
					lcNReceita = lcNReceita + 1
				ENDFOR
			ENDIF 
	
			IF uCrsLoteBuracos.nreceita == 29 AND lcMaxNrReceita == 29
				lcNReceita = 30
			
				SELECT uCrsLotesTapar
					APPEND blank
					replace ;
						uCrsLotesTapar.cptorgabrev  WITH uCrsLoteBuracos.cptorgabrev ;
						uCrsLotesTapar.tlote        WITH uCrsLoteBuracos.tlote ;
						uCrsLotesTapar.slote 	    WITH uCrsLoteBuracos.slote ;
						uCrsLotesTapar.lote 	    WITH uCrsLoteBuracos.lote ;
						uCrsLotesTapar.nreceita     WITH lcNReceita ;
						uCrsLotesTapar.ctltrctstamp WITH uCrsLoteBuracos.ctltrctstamp
			ENDIF

		ENDSCAN
			
		lcNReceita = 0
		
	ENDSCAN
 
	IF USED("uCrsLotesTapar") AND RECCOUNT("uCrsLotesTapar") > 0
		SELECT uCrsLotesTapar
		GO TOP
	ELSE
		uf_perguntalt_chama("N�o existem buracos nos lotes abertos que est�o na grelha.", "OK", "", 64)
		RETURN .t.
	ENDIF 
	
	IF USED("uCrsLoteBuracos")
		fecha("uCrsLoteBuracos")
	ENDIF 
	
	&& Abre o painel
	IF TYPE("BURACOS")=="U"
		DO FORM BURACOS
	ELSE
		BURACOS.show
	ENDIF
ENDFUNC


**
FUNCTION uf_buracos_tapar
	LOCAL lcSQL, lcTexto, lcNrregistosTapar
	STORE "" TO lcSQL, lcTexto
	STORE 0 TO lcNrregistosTapar

	SELECT uCrsLotesTapar
	COUNT TO lcNrregistosTapar
	IF lcNrregistosTapar == 0
		uf_perguntalt_chama("N�o existem buracos a tapar.","OK","",32)
		RETURN .f.
	ENDIF 
	
	&& verificar se � posto e alterar o site
	LOCAL siteP
	IF myPosto
		siteP =  mySitePosto
	ELSE
		siteP = mySite
	ENDIF
	
	SELECT uCrsLotesTapar
	GO Top
	TEXT TO lcSQL TEXTMERGE NOSHOW
		with cte1 as
		(
		select MAX(lote) as maxlote 
		from ctltrct (nolock)
		where 
			ano = <<receituario.spinnerAno.value>>
			and mes = <<uf_gerais_getMonthComboBox("RECEITUARIO.cbMes")>>
			and cptorgabrev = '<<ALLTRIM(uCrsLotesTapar.cptorgabrev)>>'
			and tlote = '<<ALLTRIM(uCrsLotesTapar.tlote)>>'
			and slote = <<uCrsLotesTapar.slote>>
			and site = '<<siteP >>'
		)
		select top 1 max(nreceita) as Receita, (select maxlote from cte1) as lote, ctltrctstamp
		from ctltrct (nolock)
		where 
			ano = <<receituario.spinnerAno.value>>
			and mes = <<uf_gerais_getMonthComboBox("RECEITUARIO.cbMes")>>
			and cptorgabrev = '<<uCrsLotesTapar.cptorgabrev>>'
			and tlote = '<<ALLTRIM(uCrsLotesTapar.tlote)>>'
			and slote = <<uCrsLotesTapar.slote>>
			and lote = (select maxlote from cte1)
			and site = '<<siteP>>'
		group by ctltrctstamp
		order by receita desc
	ENDTEXT

	uf_gerais_actGrelha("","uCrsReceitaMover",lcSQL)

	SELECT uCrsReceitaMover
	GO Top
	COUNT TO LcNrRegistosReceitasMover
	IF LcNrRegistosReceitasMover == 0
		uf_perguntalt_chama("N�o foi possivel calcular o lote a atribuir. N�o � possivel continuar.","OK","",48)
		RETURN .f.
	ENDIF 
	
	SELECT uCrsLotesTapar
	SELECT uCrsReceitaMover
	GO Top
	
	lcTexto = "Vai mover a receita:" + CHR(13) + "Entidade: " + uCrsLotesTapar.cptorgabrev + CHR(13) + ;
				"Tipo: " + uCrsLotesTapar.tlote + CHR(13) + "S�rie: " + ALLTRIM(STR(uCrsLotesTapar.slote)) + CHR(13) + ;
				"Lote: " + ALLTRIM(STR(uCrsReceitaMover.lote)) + CHR(13) + "N� Receita: " + ALLTRIM(STR(uCrsReceitaMover.receita)) + CHR(13) + ;
				"Para o buraco que seleccionou." + CHR(13) + "Tem a certeza que pretende continuar?"
	
	IF uf_perguntalt_chama(lcTexto,"Sim","N�o")
		IF (uf_gerais_validaPermUser(ch_userno,ch_grupo,'Gest�o de Receitu�rio - Alterar contador'))
			IF uf_corrigirReceita_UpdateCtltrct(0, .t.)
				IF uf_perguntalt_chama("Buraco tapado com sucesso." + CHR(13) + CHR(13) + "Pretende imprimir a receita?", "Sim", "N�o")
					uf_imprimirpos_impRec(ALLTRIM(buracos.ftstamp),buracos.verso,"buracos")
				ENDIF 
				uf_BURACOS_chama()
			ENDIF 
		ELSE
			uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ALTERAR CONTADORES DE RECEITAS.","OK","",48)
			RETURN .F.
		ENDIF
	ENDIF 
	
	IF USED("uCrsReceitaMover")
		fecha("uCrsReceitaMover")
	ENDIF 
ENDFUNC 


**
FUNCTION uf_BURACOS_sair
	If Used("ucrsLotesTapar")
		fecha("ucrsLotesTapar")
	Endif
	
	IF TYPE("receituario") != "U"
		uf_receituario_actualiza()
	ENDIF 
	
	&& fecha painel
	BURACOS.hide
	BURACOS.release
ENDFUNC