PROCEDURE uf_DIFFACTELEC_chama


	IF !WEXIST("DIFFACTELEC")

		uf_diffactelec_criarCurs()
	
		DO FORM DIFFACTELEC
	
	ELSE
	
		DIFFACTELEC.release

		uf_diffactelec_criarCurs()

		DO FORM DIFFACTELEC
	
	ENDIF


ENDPROC

PROCEDURE uf_DIFFACTELEC_sair

	IF WEXIST("DIFFACTELEC")
		DIFFACTELEC.hide
		DIFFACTELEC.release
	ENDIF

ENDPROC

PROCEDURE uf_diffactelec_criarCurs

	LOCAL uv_sql

	TEXT TO uv_sql TEXTMERGE NOSHOW

		EXEC up_relatorio_errosDiferencasFED 0,0,'' 

	ENDTEXT

	IF !uf_gerais_actGrelha("", "uc_difsFed", uv_sql)
		RETURN .F.
	ENDIF

ENDPROC

FUNCTION uf_diffactelec_actualizar

	IF !WEXIST("DIFFACTELEC")
		RETURN .F.
	ENDIF

	LOCAL uv_ano, uv_mes, uv_loja, uv_sql


	uv_ano = ASTR(DIFFACTELEC.anoFT.value)
	uv_mes = ASTR(uf_gerais_getMonthComboBox("DIFFACTELEC.mesFT"))
	uv_loja = DIFFACTELEC.loja.value
	
	IF EMPTY(uv_ano) OR EMPTY(uv_mes) OR EMPTY(uv_loja)
		RETURN .F.
	ENDIF
	
	TEXT TO uv_sql TEXTMERGE NOSHOW
		
		EXEC up_relatorio_errosDiferencasFED <<ALLTRIM(uv_ano)>>,<<ALLTRIM(uv_mes)>>,'<<ALLTRIM(uv_loja)>>' 
		
	ENDTEXT

	IF !uf_gerais_actGrelha("DIFFACTELEC.grid1", "uc_difsFed", uv_sql)
		RETURN .F.
	ENDIF

	DIFFACTELEC.grid1.autoFit()
	DIFFACTELEC.grid1.erroDoc.width = 500
	
ENDFUNC

FUNCTION uf_diffactelec_showErrors

	IF !USED("uc_difsFed")
		RETURN .F.
	ENDIF
	
	SELECT uc_difsFed
	
	IF ISNULL(uc_difsFed.pathFile) OR EMPTY(uc_difsFed.pathFile)
	
		uf_perguntalt_chama("Esta fatura n�o tem nenhum erro a apresentar.", "", "OK", 16)
		RETURN .F.
	
	ENDIF
	
	IF EMPTY(uc_difsFed.xmlPrefix)
	
		uf_perguntalt_chama("N�o est� preenchido o NameSpace do XML. Por favor contacte o suporte.", "", "OK", 16)
		RETURN .F.
	
	ENDIF

	LOCAL uv_xmlPath, uv_abrev, uv_nmdoc, uv_fno, uv_ano, uv_data, uv_nome
	
	uv_xmlPath = uc_difsFed.pathFile
	
	IF !uf_gerais_getDifsFactElec(uv_xmlPath, ALLTRIM(uc_difsFed.xmlPrefix))
	
		uf_perguntalt_chama("Erro ao ler o ficheiro.", "", "OK", 16)
		RETURN .F.
	
	ENDIF
	
	CREATE CURSOR uc_errosXMLDesc (lote C(50), receita C(50), erroDesc M, total N(14,2))
	LOCAL uv_curRec, uv_erro, uv_curLote, uv_total

	SELECT uc_errosXmlFactElec
	GO TOP
	
	SELECT DISTINCT receita, lote FROM uc_errosXmlFactElec WITH (BUFFERING = .T.) INTO CURSOR uc_curReceitas

	SELECT uc_curReceitas
	GO TOP

	SCAN

		uv_curRec = ALLTRIM(uc_curReceitas.receita)
		uv_curLote = ALLTRIM(uc_curReceitas.lote)
		uv_total = 0
		uv_erro = ''

		SELECT uc_errosXmlFactElec
		GO TOP

		SCAN FOR ALLTRIM(uc_errosXmlFactElec.receita) == ALLTRIM(uv_curRec) AND (!EMPTY(uc_errosXmlFactElec.codigoErro) OR EMPTY(uc_errosXmlFactElec.medicamento))

			IF !EMPTY(uc_errosXmlFactElec.codigoErro)
				uv_erro = uv_erro + IIF(!EMPTY(uv_erro), CHR(13), '') + ALLTRIM(uc_errosXmlFactElec.codigoErro) + ' - ' + ALLTRIM(uc_errosXmlFactElec.erro)
			ELSE
				uv_erro = uv_erro + CHR(13)
			ENDIF

			SELECT uc_errosXmlFactElec
		ENDSCAN

		uv_erro = uv_erro + IIF(!EMPTY(uv_erro), CHR(13), '')

		SCAN FOR ALLTRIM(uc_errosXmlFactElec.receita) == ALLTRIM(uv_curRec) AND (!EMPTY(uc_errosXmlFactElec.medicamento) OR EMPTY(uc_errosXmlFactElec.codigoErro))

			uv_total = uv_total + uc_errosXmlFactElec.difCompart

			IF !EMPTY(uc_errosXmlFactElec.medicamento)

				uv_erro = uv_erro + IIF(!EMPTY(uv_erro), CHR(13), '') + 'Linha Nr� ' + ALLTRIM(uc_errosXmlFactElec.nrLinha) +;
						' ' + ALLTRIM(uc_errosXmlFactElec.medicamento) +;
						CHR(13) + 'Faturado: ' + ASTR(uc_errosXmlFactElec.ftCompart, 14, 2) + ' Calculado: ' + ASTR(uc_errosXmlFactElec.calcCompart, 14, 2) + ' Diferen�a: ' + ASTR(uc_errosXmlFactElec.difCompart, 14, 2)

			ELSE

				uv_erro = uv_erro + CHR(13)

			ENDIF

			SELECT uc_errosXmlFactElec
		ENDSCAN

		SELECT uc_errosXmlFactElec
		GO TOP 

		SELECT uc_errosXMLDesc
		APPEND BLANK

		REPLACE uc_errosXMLDesc.lote WITH ALLTRIM(uv_curLote),;
				uc_errosXMLDesc.receita WITH ALLTRIM(uv_curRec),;
				uc_errosXMLDesc.erroDesc WITH ALLTRIM(uv_erro),;
				uc_errosXMLDesc.total WITH uv_total

		SELECT uc_curReceitas
	ENDSCAN
	
	SELECT uc_difsFed
	
	uv_abrev = uc_difsFed.abrev
	uv_nmdoc = uc_difsFed.nomeDoc
	uv_fno = uc_difsFed.numDoc
	uv_ano = DIFFACTELEC.anoFT.value
	uv_data = uf_gerais_getDate(uc_difsFed.dataDoc)
	uv_nome = uc_difsFed.nome


	SELECT uc_errosXMLDesc
	GO TOP

	uf_difsFacElecDetalhe_chama(uv_abrev, uv_nmdoc, uv_fno, uv_ano, uv_data, uv_nome)


ENDFUNC


FUNCTION uf_diffactelec_syncTodos

	LOCAL uv_ano, uv_mes, uv_loja

	uv_ano = ASTR(DIFFACTELEC.anoFT.value)
	uv_mes = ASTR(uf_gerais_getMonthComboBox("DIFFACTELEC.mesFT"))
	uv_loja = DIFFACTELEC.loja.value

	IF EMPTY(uv_ano) OR EMPTY(uv_mes) OR EMPTY(uv_loja)
		RETURN .F.
	ENDIF

	DIFFACTELEC.lockScreen = .T.

	SELECT uc_difsFed
	GO TOP

	regua(0, RECCOUNT("uc_difsFed"), "A sincronizar Faturas...")

	SCAN

		regua(1, RECNO(), "A sincronizar a " + ALLTRIM(uc_difsFed.nomeDoc) + " n�" + ASTR(uc_difsFed.numDoc) + "...")

		IF !uf_diffactelec_sync(uc_difsFed.ftstamp, uv_ano, uv_mes, uv_loja)
			regua(2)
			DIFFACTELEC.lockScreen = .F.
			RETURN .F.
		ENDIF

	ENDSCAN

	uf_diffactelec_actualizar()
	
	regua(2)

	uf_perguntalt_chama("Faturas sincronizadas com sucesso.", "Ok", "", 64)

	DIFFACTELEC.lockScreen = .F.

ENDFUNC

FUNCTION uf_diffactelec_sync
	LPARAMETERS uv_ftstamp, uv_ano, uv_mes, uv_loja

	IF EMPTY(uv_ftstamp) OR EMPTY(uv_ano) OR EMPTY(uv_mes) OR EMPTY(uv_loja)
		RETURN .F.
	ENDIF

	LOCAL uv_jar, uv_token, uv_path, uv_wsPath, uv_idCliente

	uv_jar = 'FaturacaoEletronicaMedicamentos.jar'
	uv_token = uf_gerais_stamp()
	uv_path = ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\ltsFaturacaoEletronica\'
	SELECT uCrsE1
	uv_idCliente = ALLTRIM(uCrsE1.id_lt)

	IF !FILE(ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\ltsFaturacaoEletronica\' + ALLTRIM(uv_jar))
		uf_perguntalt_chama("FICHEIRO DE CONFIGURA��O N�O ENCONTRADO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		regua(2)
		RETURN .f.
	ENDIF 

	uv_wsPath = uv_path + ALLTRIM(uv_jar);
						+ " " + ALLTRIM(uv_token);	
						+ " " + uv_ftstamp;
						+ " " + uv_idCliente;
						+ " " + uv_ano;
						+ " " + uv_mes;
						+ " " + ["] + ALLTRIM(uv_loja) + ["];
						+ " 1"

	uv_wsPath = "java -jar " + uv_wsPath 
				
	&& aguarda que software termine o envio da informa��o 3� PARAMETRO = .T.
	oWSShell = CREATEOBJECT("WScript.Shell")
	oWSShell.Run(uv_wsPath, 0, .t.)
	
	IF emdesenvolvimento &&debug mode
		_cliptext = uv_wsPath 
	ENDIF

ENDFUNC