PROCEDURE uf_difsFacElecDetalhe_chama
	LPARAMETERS uv_abrev, uv_nmdoc, uv_fno, uv_ano, uv_data, uv_nome

	IF !USED("uc_errosXMLDesc")
		RETURN .F.
	ENDIF

	IF !WEXIST("DIFSFACELECDETALHE")
	
		DO FORM DIFSFACELECDETALHE WITH uv_abrev, uv_nmdoc, uv_fno, uv_ano, uv_data, uv_nome
	
	ELSE
	
		DIFSFACELECDETALHE.HIDE
		DIFSFACELECDETALHE.RELEASE
		
		DO FORM DIFSFACELECDETALHE WITH uv_abrev, uv_nmdoc, uv_fno, uv_ano, uv_data, uv_nome
	
	ENDIF

ENDPROC

PROCEDURE uf_difsFacElecDetalhe_sair

	IF WEXIST("DIFSFACELECDETALHE")
	
		FECHA("uc_errosXMLDesc")
	
		DIFSFACELECDETALHE.HIDE
		DIFSFACELECDETALHE.RELEASE
	
	ENDIF

ENDPROC

FUNCTION uf_difsFacElecDetalhe_imprimir

	PUBLIC myFileNameSeq, myFileTableSeq, myFilePathSeq, upv_anoFED, upv_nmdocFed, upv_abrevFed, upv_fnoFed, upv_dataFed, upv_nomeFed, upv_totalFED
	
	upv_anoFED = DIFSFACELECDETALHE.ano
	upv_nmdocFed = DIFSFACELECDETALHE.nmdoc
	upv_abrevFed = DIFSFACELECDETALHE.abrev
	upv_fnoFed = DIFSFACELECDETALHE.fno
	upv_dataFed = DIFSFACELECDETALHE.dataDoc
	upv_nomeFed = DIFSFACELECDETALHE.nomeEnt
	upv_totalFED 	= DIFSFACELECDETALHE.total.value

	LOCAL uv_path

	uv_path = ALLTRIM(uf_gerais_getUmValor("B_Terminal", "LTRIM(RTRIM(pathExport))", "terminal = '" + alltrim(myTerm) + "'"))
	
	IF EMPTY(uv_path)

		uf_perguntalt_chama('Aten��o!' + chr(13) + 'N�o preencheu o par�metro "Caminho Export.".' + chr(13) +  'Ter� de preencher o mesmo para continuar.', "Ok", "", 48)
		RETURN .f.

	ENDIF

	IF !uf_gerais_createDir(uv_path)
		RETURN .F.
	ENDIF

	myFilePathSeq = uv_path + '\'

   	myFileNameSeq = ALLTRIM(upv_abrevFed) + "_" + ALLTRIM(upv_nmdocFed) + "_" + ASTR(upv_fnoFed) + "_" + ASTR(upv_anoFed)

	myFileTableSeq = "ERROSFACTELEC"

	SELECT uc_errosXMLDesc 
	GO TOP

	uf_imprimirgerais_chama('ERROSFACTELEC', .t., .f., .f.)

	uf_perguntalt_chama("Documentos gerados com sucesso.","OK","",64)

ENDFUNC