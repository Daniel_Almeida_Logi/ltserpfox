** Fun��o Principal
FUNCTION uf_importExportReceitas_chama

	
	&& Abre o painel
	IF TYPE("importExportReceitas")=="U"
		DO FORM importExportReceitas
	ELSE
		importExportReceitas.show
	ENDIF
ENDFUNC


**
FUNCTION uf_importExportReceitas_carregaMenu
	importExportReceitas.menu1.adicionaOpcao("exportar", "Exportar", myPath + "\imagens\icons\exportar_w.png", "uf_importExportReceitas_exportar","E")
	importExportReceitas.menu1.adicionaOpcao("importar", "Importar", myPath + "\imagens\icons\doc_seta_w.png", "uf_importExportReceitas_importar","I")
	importExportReceitas.menu1.adicionaOpcao("visualizar", "Rec. Import.", myPath + "\imagens\icons\doc_seta_w.png", "uf_importExportReceitas_chamaReceitasImportadas","V")
ENDFUNC


**
FUNCTION uf_importExportReceitas_alternaMenu
	IF UPPER(ALLTRIM(uf_gerais_getParameter("ADM0000000038", "TEXT"))) == "EXPORT"
		importExportReceitas.menu1.estado("importar, visualizar", "HIDE")
	ELSE
		importExportReceitas.menu1.estado("exportar", "HIDE")	
	ENDIF
ENDFUNC


**  Selecciona o Ano
FUNCTION uf_importExportReceitas_selAno
	
	&& Mes
	importExportReceitas.pageframe1.page1.cmbMes.Value 			= ''
	importExportReceitas.pageframe1.page1.cmbMes.displayValue	= ''
ENDFUNC


**
FUNCTION uf_importexportreceitas_selDir
	Lparameters nBool
	
	Local mdir
	
	If nBool
**		oShell = CREATEOBJECT("Shell.Application")
**		oFolder = oShell.Application.BrowseForFolder(_screen.HWnd, "SELECCIONE A PASTA DE DESTINO:", 1)
**		if isnull(oFolder)
**			return
**		ELSE
**			? oFolder.self.Path
**		ENDIF
**	
**		mdir = oFolder.self.path

        **uv_path = ALLTRIM(uf_gerais_getUmValor("B_Terminal", "LTRIM(RTRIM(pathExport))", "terminal = '" + alltrim(myTerm) + "' and site = '" + alltrim(mySite) + "'"))
        uv_path = ALLTRIM(uf_gerais_getUmValor("B_Terminal", "LTRIM(RTRIM(pathExport))", "terminal = '" + alltrim(myTerm) + "'"))

        IF EMPTY(uv_path)

            uf_perguntalt_chama('Aten��o!' + chr(13) + 'N�o preencheu o par�metro "Caminho Export.".' + chr(13) +  'Ter� de preencher o mesmo para continuar.', "Ok", "", 48)
            RETURN .f.

        ENDIF

        IF !uf_gerais_createDir(uv_path)
            RETURN .F.
        ENDIF

        mdir = uv_path

		If !Empty(mdir)
			importexportreceitas.pageframe1.page1.dir.Value = Alltrim(mdir)
		Endif
	ELSE
		mdir = Getfile("zip")
		IF !Empty(mdir)
			importexportreceitas.pageframe1.page2.dir.Value = Alltrim(mdir)
		ENDIF
	ENDIF
ENDFUNC


**
FUNCTION uf_importExportReceitas_chamaReceitasImportadas
	uf_importExportReceitas_sair()
	uf_receituario_verReceitasImportadas()
ENDFUNC


** Funcao exportar ficheiro ZIP
Function uf_importExportReceitas_exportar

	Local lcPath, lcNewPath, lcLen, lcSql, lcZipFile, lcXmlFile, lcMes
	Store '' To lcSql, lcPath, lcNewPath, lcZipFile, lcXmlFile, lcMes
	
	** Validar se o parametro que define o Posto e S�rie de Factura��o existe e est� bem definido, tanto no par�metro "Export" como "SerieFact"
	IF EMPTY(uf_gerais_getParameter("ADM0000000038", 'TEXT'))
		uf_perguntalt_chama("A importa��o / exporta��o de receitas n�o est� activa nesta Base de Dados." +CHR(10)+CHR(10)+ "Por favor contacte o suporte.", "Ok", "", 48)
		RETURN .f.
	ENDIF
	
	IF !(Upper(Alltrim(uf_gerais_getParameter("ADM0000000038", 'TEXT'))) == 'EXPORT')
		uf_perguntalt_chama("Este sistema n�o permite a exporta��o de receitas.", "Ok", "", 48)
		RETURN .f.
	ENDIF

	** Validar dados para exporta��o **
	If Empty(ALLTRIM(importExportReceitas.pageframe1.page1.cmbano.displayvalue)) Or Empty(ALLTRIM(importExportReceitas.pageframe1.page1.cmbmes.displayvalue))
		uf_perguntalt_chama("O ano e m�s devem estar devidamente preenchidos.", "Ok", "", 48)
		RETURN .f.
	ENDIF
	
	If Empty(ALLTRIM(importExportReceitas.pageframe1.page1.dir.value))
		uf_perguntalt_chama("Deve primeiro escolher a Directoria onde ir� guardar o ficheiro.", "Ok", "", 48)
		RETURN .f.
	ENDIF
	
	If Empty(ALLTRIM(importExportReceitas.pageframe1.page1.site.value))
		uf_perguntalt_chama("A loja n�o est� identificada. Assim n�o pode continuar.", "Ok", "", 48)
		RETURN .f.
	ENDIF
	
	
	** Definir nome do ficheiro (se o mes tiver 1 digito acrescentar 0 para perfazer formato YYYYMMDD)
	lcMes 	= alltrim(str(uf_gerais_getMonthComboBox("importexportreceitas.pageframe1.page1.cmbmes")))
	lcMes 	= Iif(Len(lcMes) == 1, "0" + lcMes, lcMes)
	lcPath	= Alltrim(importExportReceitas.pageframe1.page1.dir.value)
	lcZipFile = lcPath + "\" + Alltrim(mySite) + "-" + ALLTRIM(importExportReceitas.pageframe1.page1.cmbano.displayvalue) + lcMes + uf_gerais_getdate(.f., "DIA") + ".zip"


	** Exportar ficheiro FT
	TEXT TO lcSql NOSHOW TEXTMERGE
		SELECT
			a.cxstamp,a.cxusername,a.efinv,a.eivain1,a.eivain2,a.eivain3,a.eivain4,a.eivain5,a.eivain6,a.eivain7,a.eivain8,a.eivain9,a.eivav1,a.eivav2,a.eivav3,a.eivav4,a.eivav5,a.eivav6,a.eivav7,a.eivav8,a.eivav9,a.cdata,
			a.estab,a.etot1,a.etot2,a.etot3,a.etot4,a.etotal,a.ettiliq,a.ettiva,a.fdata,a.fno,a.ftano,a.ftstamp,a.ivain1,a.ivain2,a.ivain3,a.ivain4,a.ivain5,a.ivain6,a.ivain7,a.ivain8,a.ivain9,a.ivamin1,a.ivamin2,a.ivamin3,
			a.ivamin4,a.ivamin5,a.ivamin6,a.ivamin7,a.ivamin8,a.ivamin9,a.ivamv1,a.ivamv2,a.ivamv3,a.ivamv4,a.ivamv5,a.ivamv6,a.ivamv7,a.ivamv8,a.ivamv9,a.ivatx1,a.ivatx2,a.ivatx3,a.ivatx4,a.ivatx5,a.ivatx6,a.ivatx7,
			a.ivatx8,a.ivatx9,a.ivav1,a.ivav2,a.ivav3,a.ivav4,a.ivav5,a.ivav6,a.ivav7,a.ivav8,a.ivav9,a.moeda,a.ncont,a.ndoc,a.nmdoc,a.no,
			nome = Case when a.nome='.' then 'Cliente Final' else a.nome end,
			a.qtt1,a.qtt2,a.qtt3,a.qtt4,a.ssstamp,a.ssusername,a.tipodoc,a.tmiva,a.tot1,a.tot2,a.tot3,a.tot4,a.total,a.totalmoeda,a.totqtt,a.ttiliq,a.ttiva,
			a.u_ltstamp,a.u_ltstamp2,a.u_nratend,a.u_nslote,a.u_nslote2,a.u_slote,a.u_slote2,
			a.u_tlote,a.u_tlote2,a.vendedor,a.vendnm,
			a.u_lote,a.u_lote2
		FROM
			ft as a (nolock) 
			inner join (
				SELECT DISTINCT
					ftstamp
				FROM 
					ft as c (nolock)
					INNER JOIN ctltrct (nolock) AS b ON c.u_ltstamp=b.ctltrctstamp OR c.u_ltstamp2=b.ctltrctstamp 
				where
					b.ano = <<ALLTRIM(importExportReceitas.pageframe1.page1.cmbano.displayvalue)>>
					and b.mes = <<lcMes>>
			) as b on a.ftstamp=b.ftstamp
	ENDTEXT
	If !uf_gerais_actgrelha("", [uCrsExportDocs], lcSql)
		uf_perguntalt_chama("Ocorreu um erro a obter os dados de fatura��o para exporta��o." +CHR(10)+CHR(10)+ " Por favor contacte o suporte.", "Ok", "", 16)
		RETURN .f.
	ENDIF
	
	
	** Se nao houverem registos a exportar, notificar o utilizador e abortar (se a FT nao tem nada as restantes n�o ter�o tamb�m); caso contr�rio apagar ficheiros de Export antigos e criar a nova pasta tempor�ria
	SELECT uCrsExportDocs
	If Reccount("uCrsExportDocs") == 0
		uf_perguntalt_chama("N�o existem documentos de factura��o a exportar.", "Ok", "", 64)
		RETURN .f.
	ENDIF
	
	
	** Remover ficheiros desactualizados ANTES de gerar os novos na pasta de destino para UNZIP (remover apenas os ficheiro "loja-*.zip")
	delete file alltrim(lcPath) + "\" + Alltrim(mySite) + "*.ZIP"
	
	
	** Apagar e recriar nova subpasta com o nome do ficheiro mas sem extens�o ZIP que vai ser gerado para exportarmos para l� os ficheiros .XML e depois a ziparmos
	lcNewPath = lcpath + "\ExportFiles"
	mkdir(lcNewPath)
	
	* Definir o nome do ficheiro XML a gerar e exportar
	lcXmlFile = Alltrim(lcNewPath)+"\" + "1." + Alltrim(mySite) + "-FT-" + ALLTRIM(importExportReceitas.pageframe1.page1.cmbano.displayvalue) + lcMes + uf_gerais_getdate(.f., "DIA") + ".xml"	
	cursortoxml('uCrsExportDocs', lcXmlFile, 1, 512, 0, "1")
	
	
	** exportar ficheiro FT2
	Text To lcSql Noshow textmerge
		SELECT 
			a.ft2stamp
			,a.vdollocal
			,a.vdlocal
			,a.u_abrev
			,a.u_abrev2
			,a.u_codigo
			,a.u_codigo2
			,a.u_design
			,a.u_design2
			,a.u_entpresc
			,a.u_nbenef
			,a.u_nopresc
			,u_receita = case when a.u_receita='.' then '' else a.u_receita end
			,a.u_vdsusprg
		FROM
			ft2 as a (nolock) 
		inner join (
			SELECT DISTINCT
				ftstamp
			FROM
				ft as c (nolock)
				INNER JOIN ctltrct (nolock) AS b ON c.u_ltstamp=b.ctltrctstamp OR c.u_ltstamp2=b.ctltrctstamp 
			where
				b.ano= <<ALLTRIM(importExportReceitas.pageframe1.page1.cmbano.displayvalue)>>
				and b.mes = <<lcMes>>
		) as b on a.ft2stamp=b.ftstamp
	Endtext
	If !uf_gerais_actgrelha("", [uCrsExportDocs], lcSql)
		uf_perguntalt_chama("Erro a obter dados de factura��o para exporta��o." +CHR(10)+CHR(10)+ "Por favor contacte o suporte", "Ok", "", 16)
		RETURN .t.
	EndIf	
	
	
	* Definir o nome do ficheiro XML a gerar e exportar
	lcXmlFile =Alltrim(lcNewPath)+"\" + "2." + Alltrim(mySite) + "-FT2-" + ALLTRIM(importExportReceitas.pageframe1.page1.cmbano.displayvalue) + lcMes + uf_gerais_getdate(.f., "DIA") + ".xml"	
	cursortoxml('uCrsExportDocs', lcXmlFile, 1, 512, 0, "1")
	
	** Exportar ficheiro FI
	Text To lcSql Noshow textmerge
		SELECT 
			a.codigo,a.custo,a.desc2,a.desc3,a.desc4,a.desc5,a.desc6,a.desconto,a.design,a.ecusto
			,a.etiliquido,a.epv,a.epvori,a.epcp,a.fivendedor,a.fivendnm,a.fistamp,a.fno,a.ftstamp
			,a.tiliquido,a.tliquido,a.tipodoc,a.tabiva,a.ref,a.pcp,a.iva,a.ivaincl,a.ndoc,a.nmdoc
			,a.ofistamp,a.oref,a.qtt,a.stns,a.u_bencont,a.u_benzo,a.u_cnp,a.u_comp
			,a.u_descval,a.u_diploma,a.u_epref,a.u_epvp,a.u_ettent1,a.u_ettent2
			,a.u_genalt,a.u_generico,a.u_ip,a.u_nbenef,a.u_nbenef2,a.u_psico,a.u_psicont
			,a.u_refvale,a.u_robot,a.u_stock,a.u_txcomp,a.evbase,a.vbase,a.evvenda,a.vvenda,a.tliquido,a.tmoeda
		FROM
			fi (nolock) as a
			inner join (
				SELECT DISTINCT
					ftstamp
				FROM
					ft as c (nolock)
					INNER JOIN ctltrct (nolock) AS b ON c.u_ltstamp=b.ctltrctstamp OR c.u_ltstamp2=b.ctltrctstamp 
				where
					b.ano = <<ALLTRIM(importExportReceitas.pageframe1.page1.cmbano.displayvalue)>>
					and b.mes = <<lcMes>>
			) as b on a.ftstamp=b.ftstamp		
	endtext
	
	If !uf_gerais_actgrelha("", [uCrsExportDocs], lcSql)
		uf_perguntalt_chama("Ocorreu um erro a obter os dados de fatura��o para exporta��o." + CHR(10)+CHR(10)+ "Por favor contacte o suporte.", "Ok", "", 16)
		RETURN .f.
	EndIf	
	
	
	* Definir o nome do ficheiro XML a gerar e exportar
	lcXmlFile = Alltrim(lcNewPath) + "\" + "3." + Alltrim(mySite) + "-FI-" + ALLTRIM(importExportReceitas.pageframe1.page1.cmbano.displayvalue) + lcMes + uf_gerais_getdate(.f., "DIA") + ".xml"	
	cursortoxml('uCrsExportDocs',lcXmlFile,1,512,0,"1")	
	
	
	** Exportar ficheiro CTLTRCT
	Text To lcSql Noshow textmerge
		Select
			a.*
		from
			ctltrct (nolock) as a
			INNER JOIN ft (nolock) AS b ON b.u_ltstamp=a.ctltrctstamp OR b.u_ltstamp2=a.ctltrctstamp 
		where
			a.ano = <<ALLTRIM(importExportReceitas.pageframe1.page1.cmbano.displayvalue)>>
			and a.mes = <<lcMes>>					
	Endtext	
	If !uf_gerais_actgrelha("", [uCrsExportDocs], lcSql)
		uf_perguntalt_chama("Ocorreu um erro a obter os dados de fatura��o para exporta��o." + CHR(10)+CHR(10)+ "Por favor contacte o suporte.", "Ok", "", 16)
		RETURN .f.
	ENDIF
	
	* Definir o nome do ficheiro XML a gerar e exportar
	lcXmlFile = Alltrim(lcNewPath)+"\" + "4." + Alltrim(mySite) + "-CTLTRCT-" + ALLTRIM(importExportReceitas.pageframe1.page1.cmbano.displayvalue) + lcMes + uf_gerais_getdate(.f., "DIA") + ".xml"	
	cursortoxml('uCrsExportDocs', lcXmlFile, 1, 512, 0, "1")
		
	
	** Chamar a funcao de zipar da classe agora que a pasta foi limpa de exporta��es anteriores
	objZip = CREATEOBJECT("zip")
	objzip.arquivozip(lcZipFile)
	objzip.diretorio(Alltrim(lcNewPath))
	IF !objzip.zip()
		uf_gerais_registaerrolog(objZip.Erro, "Import/Export", "Ficheiro n�o foi compactado com sucesso!") 
		uf_perguntalt_chama(objzip.erro, "Ok", "", 16)
		RETURN .f.
	ELSE
		** Como o Zip do Windows corre assincrono por design e n�o devolve erro, esperar um periodo de tempo (gen�rico) fixo que ele termine
		** Era bom aqui termos ou o OUTPUT do processo de zip OU uma janela qualquer para o operador perceber que o ficheiro esta a ser gerado
		Wait "" Timeout 3
		
		** Criar e apagar objecto FSO para apagar a pasta e todo o conteudo, o RMDIR da erro em pastas n�o vazias
		oFso = CREATEOBJECT("Scripting.FileSystemObject")
		oFso.DeleteFolder(lcNewPath)
		oFso = null
		uf_perguntalt_chama("FICHEIRO EXPORTADO COM SUCESSO!", "Ok", "", 64)			
	ENDIF
	
	If Used("uCrsExportDocs")
		Fecha("uCrsExportDocs")
	Endif
ENDFUNC


** Fun��o de Importa��o de Receitu�rio
Function uf_importExportReceitas_importar

	Local lcSql, lcCount, lcNr, lcNr2, lcFile, lcLoja, lcNdoc, lcValida
	Store '' To lcSql, lcFile, lcAno, lcMes, lcLoja

	** Valida��es
	IF EMPTY(uf_gerais_getParameter("ADM0000000038", 'TEXT'))
		uf_perguntalt_chama("A importa��o / exporta��o de receitas n�o est� activa nesta Base de Dados." +CHR(10)+CHR(10)+ "Por favor contacte o suporte.", "Ok", "", 48)
		RETURN .f.
	ENDIF

	IF !(Upper(Alltrim(uf_gerais_getParameter("ADM0000000038", 'TEXT'))) == 'IMPORT')
		uf_perguntalt_chama("Este sistema n�o permite a importa��o de receitas.", "Ok", "", 48)
		RETURN .f.
	ENDIF

	If Empty(ALLTRIM(importExportReceitas.pageframe1.page2.dir.value))
		uf_perguntalt_chama("Deve primeiro escolher o ficheiro a importar.", "Ok", "", 48)
		RETURN .f.
	ENDIF



	** Validar nome do ficheiro para obter nome do Posto e Loja **
	lcFile = Alltrim(importExportReceitas.pageframe1.page2.dir.value)

	** Validar extens�o do ficheiro
	IF !(Upper(Right(ALLTRIM(lcFile), 4)) == '.ZIP')
		uf_perguntalt_chama("A extens�o do ficheiro � inv�lida.", "Ok", "", 48)
		RETURN .f.
	ELSE
		** preparar nome do ficheiro e obter nome da Loja
		lcCount		= OCCURS("\", lcFile)
		lcNr 		= ATC('\', lcFile, lcCount)
		lcPath 		= substr(lcFile, 1, lcNr-1)
		
		** Mesmo nao vazio e com extensao valida o ficheiro pode vir com o nome da Loja mal formatado, validar			
		lcNr2 		= Atc('-', lcFile, 1)
		**lcLoja 		= left(lcFile, lcNr-1)
		lcLoja 		= substr(lcFile, lcNr+1, lcNr2-lcNr-1)
		
		** Guardar ano e mes
		lcAno = substr(lcFile, lcNr2+1, 4)
		lcMes = substr(lcFile, lcNr2+5, 2)

		If Empty(lcLoja)
			uf_perguntalt_chama("N�o est� a ser poss�vel validar o nome do posto no ficheiro.","Ok", "", 48)
			RETURN .f.
		ENDIF
		
		** O Operador tem mesmo a certeza que este � o ficheiro a importar?
		If !uf_perguntalt_chama("Vai importar as receitas do ficheiro: " + Alltrim(lcFile) +CHR(10)+CHR(10)+ "Tem a certeza que pretende continuar?", "Sim", "N�o")
			RETURN .f.
		ENDIF
			
		** Se algum Lote deste m�s j� estiver fechado, abortar o processo e obrigar o Operador a reabrir todos os Lotes fechados. Presume-se que mesmo importando todos os dias ficheiros a Farm�cia poder� apenas no final do m�s fechar e facturar os Lotes, especialmente porque est� dependente dos Postos para tal....
		TEXT TO lcSql NOSHOW TEXTMERGE
			SELECT distinct
				fechado
			From
				ctltrct (nolock)
			Where
				ano = <<ALLTRIM(lcAno)>>
				and mes= <<ALLTRIM(lcMes)>>
		ENDTEXT

		IF !uf_gerais_actgrelha("", [uCrsHaLoteFechado], lcSql)
			uf_perguntalt_chama("Ocorreu um erro a determinar lotes abertos no ano e mes actual."+CHR(10)+CHR(10)+"Por favor contacte o suporte.", "Ok", "", 16)
			RETURN .f.
		ELSE
			Local lcValidaFechados

			Select uCrsHaLoteFechado
			Go Top 
			Scan For uCrsHaLoteFechado.fechado==.T.
				lcValidaFechados = .t.
			Endscan

			Fecha("uCrsHaLoteFechado")
			
			If lcValidaFechados
				uf_perguntalt_chama("H� lotes j� fechados no ano e m�s actuais. Por favor reabra os mesmos e repita a importa��o.", "Ok", "", 48)
				RETURN .f.
			ENDIF
		ENDIF
		

		** Usar classe no fim do ficheiro para deszipar, output directo do erro em caso de falha
		objZip = CREATEOBJECT("zip")
		objzip.arquivozip(lcFile)
		objzip.diretorio(Alltrim(lcPath))
		
		IF !objzip.unzip()
			uf_gerais_registaerrolog(objZip.Erro, "Import/Export", "Ficheiro n�o foi descompactado com sucesso!") 
			uf_perguntalt_chama(objzip.erro, "Ok", "", 16)
			RETURN .f.
		ELSE
		
			** Usar agora a pasta de nome unico criada na pasta aonde o unzip foi feito
			lcPath = lcPath + "\ExportFiles"
			
			* Entrar na pasta lcPath e listar apenas ficheiros .XML
			chdir(lcPath)
			lcNumFiles = ADIR(array, '*.xml')
			
			** Se o array estiver indefinido nao ha ficheiros dentro da pasta temporaria que se exportou e o ficheiro deve ter sido mal gerado, abortar!
			If Type("array") == "u"
				uf_perguntalt_chama("O ficheiro indicado n�o cont�m conte�do para importar", "Ok", "", 48)
				RETURN .f.
			ELSE
				** validar uma contagem de 4 ficheiros no total, em caso contrario o ficheiro � invalido				
				lcTotal = 0
				CLEAR
				FOR nCount = 1 TO lcNumFiles
					lcCount = array[nCount,1] 
					lcTotal = lcTotal+1
				Endfor
					
				If !(lcTotal == 4)
					uf_perguntalt_chama("O ficheiro aberto est� incompleto." +CHR(10)+CHR(10)+ "Por favor contacte o suporte.", "Ok", "", 16)	
					
					** se este codigo pudesse ser integrado no TRY poupavam-se linhas
					chdir(Home())
					
					oFso = CREATEOBJECT("Scripting.FileSystemObject")
					oFso.DeleteFolder(lcPath)
					oFso = null							
					
					RETURN .f.
				ENDIF				
			ENDIF
			
			** Criar uma transac��o com ID gerado automaticamente de forma a fazermos ROLLBACK em caso de erro (uso 10 caracteres sem a virgula tradicional no STAMP para evitar erro e conflito com outras TRANs)
			lcTran = Substr(uf_gerais_stamp(),1,10)
		
			Text To lcSql Noshow textmerge
				Begin transaction <<Alltrim(lcTran)>>
			ENDTEXT
			If !uf_gerais_actgrelha("", "", lcSql)
				uf_perguntalt_chama("Imposs�vel iniciar transa��o para importa��o de receitu�rio", "Ok", "", 16)
				Return
			ENDIF
			
			
			** Correr a rotina de importa��o em TRY de forma a podermos fazer ROLLBACK facilmente em caso de erro
			Try 
			
				** Ordenar a lista de ficheiros pelo 1� nome, que j� � gerado com ordem de processamento num�rica (1. ; 2. ; 3. ; 4. )  e iniciar o processamento do ciclo 
				asort(array,1)
				CLEAR
				FOR nCount = 1 TO lcNumFiles
				
					lcFileName = array[nCount,1] 
					lcFileName = lcPath + "\" + lcFileName
			
			
					** Carregar o XML do ficheiro em cursor
					xmltocursor(lcFileName,'uCrsIDP',512) 
					select uCrsIDP
					
					** Guardar total de Docs para a regua PHC
					** lcTotalDocs = Reccount()
					mntotal = Reccount()
					regua(0,mntotal,"A processar os documentos...")
					
					** Deteminar de que tabela e Loja � o ficheiro que estamos a ler em 3 partes e 
					
					** primeiro isolar o nome do ficheiro
					lcCount = OCCURS("\", lcFileName)
					lcNr = ATC('\', lcFileName, lcCount)
					lcFile = Substr(lcFileName, lcNr+1, Len(lcFileName))
				      
				    ** depois extrair o nome da tabela 
					lcNr = ATC('-',lcFileName,1)
					lcNr2 = ATC('-', lcFileName, 2)
					lcTabela = Substr(lcFileName,lcNr+1, (lcNr2-1)-(lcNr))
					
					** Depois extrair o nome do Posto (Loja) que ser� usado no insert da CTLTRCT
					lcNr = ATC('.',lcFileName,1)
					lcNr2 = ATC('-', lcFileName, 1)
					lcPosto = Substr(lcFileName,lcNr+1, (lcNr2-1)-(lcNr))						
				
			
					*** Tabela FT			
					If lcTabela == "FT"
					
						Select uCrsIDP
						Go top
						Scan 
							
							regua[1,recno(),"Processando Fase 1 de 4 - Documento " + astr(uCrsIDP.ndoc) + " n� "+astr(uCrsIDP.fno)]
												
							** Inserir documento apenas se o STAMP nao existir j� na BD
							Text To lcSql Noshow textmerge
								select 
									ftstamp 
								from
									ft (nolock) 
								where 
									ftstamp='<<Alltrim(uCrsIDP.ftstamp)>>'
							Endtext
							If !uf_gerais_actgrelha("", [uCrsIsDocIn], lcSql)
								uf_perguntalt_chama("Ocorreu um erro a determinar se o documento j� existe na BD Central (FT)." +CHR(10)+CHR(10)+ "Por favor contacte o suporte.", "Ok", "", 16)
								RETURN .t.
							Else
								
								** Processar a linha do cursor apenas se este documento nao estiver ainda na BD
								Select uCrsIsDocIn
								
								If Reccount("uCrsIsDocIn") == 0
									** Ler o n� do ultimo documento Inser��o de Receita
									If !uf_gerais_actgrelha("", "uCrsLastDoc", [select fno = Isnull(Max(fno),0) from ft (nolock) where ndoc=75 and ftano=datepart(year,dateadd(HOUR, ]+STR(difhoraria)+[, getdate()))])
										uf_perguntalt_chama("Ocorreu um erro a determinar o �ltimo documento de inser��o de receita" +CHR(10)+CHR(10)+ "Por favor contacte o suporte", "Ok", "", 16)
										Exit
									Endif

									** Alterar serie de factura��o e nome do documento inserido (mesmas altera��es na SP antiga)
									Select uCrsIDP
									replace uCrsIDP.ndoc 			With 75
									replace uCrsIDP.fno 			With uCrsLastDoc.fno+1
									replace uCrsIDP.nmdoc			With 'Inser��o de Receita'
									replace uCrsIDP.tipodoc 		With 4
									
									Text To lcSql Noshow TextMerge
										Insert Into ft (
											cxstamp,cxusername,efinv,cdata
											,eivain1,eivain2,eivain3,eivain4
											,eivain5,eivain6,eivain7,eivain8
											,eivain9,eivav1,eivav2,eivav3
											,eivav4,eivav5,eivav6,eivav7,eivav8,eivav9
											,estab,etot1,etot2
											,etot3,etot4,etotal,ettiliq,ettiva,fdata
											,fno,ftano,ftstamp
											,ivain1,ivain2,ivain3,ivain4,ivain5,ivain6
											,ivain7,ivain8,ivain9
											,ivamin1,ivamin2,ivamin3,ivamin4,ivamin5
											,ivamin6,ivamin7,ivamin8
											,ivamin9,ivamv1,ivamv2,ivamv3,ivamv4
											,ivamv5,ivamv6,ivamv7
											,ivamv8,ivamv9,ivatx1,ivatx2,ivatx3
											,ivatx4,ivatx5,ivatx6
											,ivatx7,ivatx8,ivatx9,ivav1,ivav2,ivav3
											,ivav4,ivav5,ivav6
											,ivav7,ivav8,ivav9,moeda,ncont,ndoc
											,nmdoc,no,nome,qtt1
											,qtt2,qtt3,qtt4,ssstamp,ssusername
											,tipodoc,tmiva,tot1,tot2,tot3,tot4
											,total,totalmoeda,totqtt,ttiliq,ttiva,u_lote
											,u_lote2,u_ltstamp,u_ltstamp2
											,u_nratend,u_nslote,u_nslote2,u_slote
											,u_slote2,u_tlote,u_tlote2,vendedor,vendnm
											)
										Values (
											'<<uCrsIDP.cxstamp>>','<<uCrsIDP.cxusername>>','<<uCrsIDP.efinv>>','<<uf_gerais_getDate(uCrsIDP.cdata,"SQL")>>',
											,'<<uCrsIDP.eivain1>>','<<uCrsIDP.eivain2>>','<<uCrsIDP.eivain3>>','<<uCrsIDP.eivain4>>' 
											,'<<uCrsIDP.eivain5>>','<<uCrsIDP.eivain6>>','<<uCrsIDP.eivain7>>','<<uCrsIDP.eivain8>>'
											,'<<uCrsIDP.eivain9>>','<<uCrsIDP.eivav1>>','<<uCrsIDP.eivav2>>','<<uCrsIDP.eivav3>>'
											,'<<uCrsIDP.eivav4>>','<<uCrsIDP.eivav5>>','<<uCrsIDP.eivav6>>','<<uCrsIDP.eivav7>>','<<uCrsIDP.eivav8>>','<<uCrsIDP.eivav9>>'
											,'<<uCrsIDP.estab>>','<<uCrsIDP.etot1>>','<<uCrsIDP.etot2>>'
											,'<<uCrsIDP.etot3>>','<<uCrsIDP.etot4>>','<<uCrsIDP.etotal>>','<<uCrsIDP.ettiliq>>','<<uCrsIDP.ettiva>>','<<uf_gerais_getDate(uCrsIDP.fdata,"SQL")>>'
											,'<<uCrsIDP.fno>>','<<uCrsIDP.ftano>>','<<uCrsIDP.ftstamp>>'
											,'<<uCrsIDP.ivain1>>','<<uCrsIDP.ivain2>>','<<uCrsIDP.ivain3>>','<<uCrsIDP.ivain4>>','<<uCrsIDP.ivain5>>','<<uCrsIDP.ivain6>>'
											,'<<uCrsIDP.ivain7>>','<<uCrsIDP.ivain8>>','<<uCrsIDP.ivain9>>'
											,'<<uCrsIDP.ivamin1>>','<<uCrsIDP.ivamin2>>','<<uCrsIDP.ivamin3>>','<<uCrsIDP.ivamin4>>','<<uCrsIDP.ivamin5>>'
											,'<<uCrsIDP.ivamin6>>','<<uCrsIDP.ivamin7>>','<<uCrsIDP.ivamin8>>'
											,'<<uCrsIDP.ivamin9>>','<<uCrsIDP.ivamv1>>','<<uCrsIDP.ivamv2>>','<<uCrsIDP.ivamv3>>','<<uCrsIDP.ivamv4>>'
											,'<<uCrsIDP.ivamv5>>','<<uCrsIDP.ivamv6>>','<<uCrsIDP.ivamv7>>'
											,'<<uCrsIDP.ivamv8>>','<<uCrsIDP.ivamv9>>','<<uCrsIDP.ivatx1>>','<<uCrsIDP.ivatx2>>','<<uCrsIDP.ivatx3>>'
											,'<<uCrsIDP.ivatx4>>','<<uCrsIDP.ivatx5>>','<<uCrsIDP.ivatx6>>'
											,'<<uCrsIDP.ivatx7>>','<<uCrsIDP.ivatx8>>','<<uCrsIDP.ivatx9>>','<<uCrsIDP.ivav1>>','<<uCrsIDP.ivav2>>','<<uCrsIDP.ivav3>>'
											,'<<uCrsIDP.ivav4>>','<<uCrsIDP.ivav5>>','<<uCrsIDP.ivav6>>'
											,'<<uCrsIDP.ivav7>>','<<uCrsIDP.ivav8>>','<<uCrsIDP.ivav9>>','<<uCrsIDP.moeda>>','<<uCrsIDP.ncont>>','<<uCrsIDP.ndoc>>'
											,'<<uCrsIDP.nmdoc>>','<<uCrsIDP.no>>','<<uCrsIDP.nome>>','<<uCrsIDP.qtt1>>'
											,'<<uCrsIDP.qtt2>>','<<uCrsIDP.qtt3>>','<<uCrsIDP.qtt4>>','<<uCrsIDP.ssstamp>>','<<uCrsIDP.ssusername>>'
											,'<<uCrsIDP.tipodoc>>','<<uCrsIDP.tmiva>>','<<uCrsIDP.tot1>>','<<uCrsIDP.tot2>>','<<uCrsIDP.tot3>>','<<uCrsIDP.tot4>>'
											,'<<uCrsIDP.total>>','<<uCrsIDP.totalmoeda>>','<<uCrsIDP.totqtt>>','<<uCrsIDP.ttiliq>>','<<uCrsIDP.ttiva>>','<<uCrsIDP.u_lote>>'
											,'<<uCrsIDP.u_lote2>>','<<uCrsIDP.u_ltstamp>>','<<uCrsIDP.u_ltstamp2>>'
											,'<<uCrsIDP.u_nratend>>','<<uCrsIDP.u_nslote>>','<<uCrsIDP.u_nslote2>>','<<uCrsIDP.u_slote>>'
											,'<<uCrsIDP.u_slote2>>','<<uCrsIDP.u_tlote>>','<<uCrsIDP.u_tlote2>>','<<uCrsIDP.vendedor>>','<<uCrsIDP.vendnm>>'
											)
									ENDTEXT
									If !uf_gerais_actgrelha("", "", lcSql)
										uf_perguntalt_chama("Erro a inserir na base de dados central (FT)" +CHR(10)+CHR(10)+ "Por favor contacte o suporte", "Ok", "", 16)
										RETURN .f.
									ENDIF
								
								ENDIF
							ENDIF
							
							** Fechar cursor de posi��o na BD Central e Voltar a seleccionar o cursor principal
							Select uCrsIDP
						
						ENDSCAN

					** Fim processar tabela FT
					ENDIF
					
					
					** Tabela FT2
					If lcTabela == "FT2"
					
						SELECT uCrsIDP
						GO TOP
						SCAN
							
							regua[1, recno(), "Processando Fase 2 de 4..."]						
							
							** Inserir documento apenas se o STAMP nao existir j� na BD
							Text To lcSql Noshow textmerge
								select
									ft2stamp
								from
									ft2 (nolock)
								where
									ft2stamp='<<uCrsIDP.ft2stamp>>'
							Endtext
							If !uf_gerais_actgrelha("", [uCrsIsDocIn], lcSql)
								uf_perguntalt_chama("Erro a determinar se o documento j� exite na base de dados central (FT)" +CHR(10)+CHR(10)+ "Por favor contacte o suporte", "Ok", "", 16)
								RETURN .f.
							ELSE
								
								** Processar a linha do cursor apenas se este documento nao estiver ainda na BD
								Select uCrsIsDocIn
								If Reccount("uCrsIsDocIn")==0
								
									Text To lcSql Noshow TextMerge
										Insert Into ft2 (
											ft2stamp,vdollocal,vdlocal,u_abrev,u_abrev2,u_codigo,u_codigo2,u_design,u_design2,u_entpresc,u_nbenef,u_nopresc,u_receita,u_vdsusprg
											)
										Values (
											'<<uCrsIDP.ft2stamp>>','<<uCrsIDP.vdollocal>>','<<uCrsIDP.vdlocal>>','<<uCrsIDP.u_abrev>>','<<uCrsIDP.u_abrev2>>','<<uCrsIDP.u_codigo>>','<<uCrsIDP.u_codigo2>>','<<uCrsIDP.u_design>>','<<uCrsIDP.u_design2>>'
											,'<<uCrsIDP.u_entpresc>>','<<uCrsIDP.u_nbenef>>','<<uCrsIDP.u_nopresc>>','<<uCrsIDP.u_receita>>','<<Iif(uCrsIDP.u_vdsusprg,1,0)>>'
											)
									ENDTEXT
									If !uf_gerais_actgrelha("", "", lcSql)
										uf_perguntalt_chama("Erro a inserir dados na Base de Dados Central (FT2)."+CHR(10)+CHR(10)+"Por favor contacte o suporte.", "Ok", "", 16)
										RETURN .f.
									ENDIF
								
								ENDIF
							
							ENDIF
							
							** Fechar cursor de posi��o na BD Central e Voltar a seleccionar o cursor principal
							SELECT uCrsIDP
						
						ENDSCAN				
					** Fim processar tabela FT2
					ENDIF
					
					
					** Tabela FI
					If lcTabela == "FI"
				
						SELECT uCrsIDP
						GO TOP
						SCAN
							
							regua[1,recno(),"Processando Fase 3 de 4..."]						
							
							** Inserir documento apenas se o STAMP nao existir j� na BD
							Text To lcSql Noshow textmerge
								select 
									fistamp 
								from
									fi (nolock) 
								where 
									fistamp='<<uCrsIDP.fistamp>>'
							Endtext
							If !uf_gerais_actgrelha("", [uCrsIsDocIn], lcSql)
								uf_perguntalt_chama("Erro a determinar se o documento j� existe na Base de Dados central (FI)."+CHR(10)+CHR(10)+"Por favor contacte o suporte.", "Ok", "" ,16)
								RETURN .f.
							Else
								
								** Processar a linha do cursor apenas se este documento nao estiver ainda na BD
								Select uCrsIsDocIn
								If Reccount("uCrsIsDocIn")==0
									** Ler o n� do documento Inser��o de Receita relativo a factura original a qual esta linha pertence (o novo n� gerado no insert da FT e n�o o original)
									Text To lcSql Noshow textmerge
										SELECT
											fno
										From
											ft (nolock)
										Where
											ftstamp='<<uCrsIDP.ftstamp>>' and ndoc=75
									ENDTEXT
									If !uf_gerais_actgrelha("", [uCrsLastDoc], lcSql)
										uf_perguntalt_chama("Erro a determinar o �ltimo documento de inser��o de receita" +CHR(10)+CHR(10)+ "Por favor contacte o suporte.", "Ok", "", 16)
										RETURN .f.
									ENDIF
																					
									** Alterar serie de factura��o, n� e nome da linha a ser inserida agora para o n� do documento ao qual diz respeito
									Select uCrsIDP
									replace uCrsIDP.ndoc 			WITH 75
									replace uCrsIDP.fno 			With uCrsLastDoc.fno
									replace uCrsIDP.nmdoc			With 'Inser��o de Receita'
									replace uCrsIDP.tipodoc 		With 4

									Text To lcSql Noshow TextMerge
										Insert Into fi (
											codigo,custo,desc2,desc3,desc4,desc5,desc6,desconto,design,ecusto,etiliquido,epv,epvori,epcp,
											fivendedor,fivendnm,fistamp,fno,ftstamp,tiliquido,tliquido,tipodoc,tabiva,ref,pcp,
											iva,ivaincl,
											ndoc,nmdoc,
											ofistamp,oref,
											qtt,stns,
											u_bencont,u_benzo,u_cnp,u_comp,u_descval,u_diploma,u_epref,u_epvp,u_ettent1,u_ettent2,u_genalt,u_generico,u_ip,u_nbenef,u_nbenef2,u_psico,u_psicont,u_refvale,u_robot,u_stock,u_txcomp,
											evbase,vbase,evvenda,vvenda,tmoeda
											)
										Values (
											'<<uCrsIDP.codigo>>','<<uCrsIDP.custo>>','<<uCrsIDP.desc2>>','<<uCrsIDP.desc3>>','<<uCrsIDP.desc4>>','<<uCrsIDP.desc5>>','<<uCrsIDP.desc6>>','<<uCrsIDP.desconto>>','<<uCrsIDP.design>>'
											,'<<uCrsIDP.ecusto>>','<<uCrsIDP.etiliquido>>','<<uCrsIDP.epv>>','<<uCrsIDP.epvori>>','<<uCrsIDP.epcp>>','<<uCrsIDP.fivendedor>>','<<uCrsIDP.fivendnm>>','<<uCrsIDP.fistamp>>'
											,'<<uCrsIDP.fno>>','<<uCrsIDP.ftstamp>>','<<uCrsIDP.tiliquido>>','<<uCrsIDP.tliquido>>','<<uCrsIDP.tipodoc>>','<<uCrsIDP.tabiva>>','<<uCrsIDP.ref>>','<<uCrsIDP.pcp>>','<<uCrsIDP.iva>>'
											,'<<Iif(uCrsIDP.ivaincl,1,0)>>','<<uCrsIDP.ndoc>>','<<uCrsIDP.nmdoc>>','<<uCrsIDP.ofistamp>>','<<uCrsIDP.oref>>','<<uCrsIDP.qtt>>','<<Iif(uCrsIDP.stns,1,0)>>','<<uCrsIDP.u_bencont>>'
											,'<<Iif(uCrsIDP.u_benzo,1,0)>>','<<uCrsIDP.u_cnp>>','<<Iif(uCrsIDP.u_comp,1,0)>>','<<uCrsIDP.u_descval>>','<<uCrsIDP.u_diploma>>', '<<uCrsIDP.u_epref>>','<<uCrsIDP.u_epvp>>','<<uCrsIDP.u_ettent1>>'
											,'<<uCrsIDP.u_ettent2>>','<<Iif(uCrsIDP.u_genalt,1,0)>>','<<Iif(uCrsIDP.u_generico,1,0)>>','<<Iif(uCrsIDP.u_ip,1,0)>>','<<uCrsIDP.u_nbenef>>','<<uCrsIDP.u_nbenef2>>','<<Iif(uCrsIDP.u_psico,1,0)>>'
											,'<<uCrsIDP.u_psicont>>','<<uCrsIDP.u_refvale>>','<<uCrsIDP.u_robot>>','<<uCrsIDP.u_stock>>','<<uCrsIDP.u_txcomp>>','<<uCrsIDP.evbase>>','<<uCrsIDP.vbase>>','<<uCrsIDP.evvenda>>','<<uCrsIDP.vvenda>>','<<uCrsIDP.tmoeda>>'
											)
									ENDTEXT 
									If !uf_gerais_actgrelha("", "", lcSql)
										uf_perguntalt_chama("Erro a inserir dados na Base de Dados central (FI)." +CHR(10)+CHR(10)+ "Por favor contacte o suporte.", "Ok", "", 16)
										RETURN .f.
									Endif
								
								ENDIF
							ENDIF
							
							** Fechar cursor de posi��o na BD Central e Voltar a seleccionar o cursor principal							
							SELECT uCrsIDP
							
						ENDSCAN
					
					** Fim processar tabela FI
					Endif					
					
					** Tabela CTLTRCT
					If lcTabela == "CTLTRCT"
						SELECT uCrsIDP
						GO TOP
						SCAN
							
							regua[1,recno(),"Processando Fase 4 de 4..."]						
							
							** Inserir documento apenas se o STAMP nao existir j� na BD
							Text To lcSql Noshow textmerge
								select 
									ctltrctstamp 
								from
									ctltrct (nolock) 
								where 
									ctltrctstamp='<<uCrsIDP.ctltrctstamp>>'
							Endtext
							IF !uf_gerais_actgrelha("", [uCrsIsDocIn], lcSql)
								uf_perguntalt_chama("Erro a determinar se o documento j� existe na Base de Dados central (CTLTRCT)." +CHR(10)+CHR(10)+ "Por favor contacte o suporte.", "Ok", "", 16)
								RETURN .f.
							ELSE
								
								** Processar a linha do cursor apenas se este documento nao estiver ainda na BD
								Select uCrsIsDocIn
								If Reccount("uCrsIsDocIn")==0
									** Se a receita for nova, verificar ANTES de importar se n�o diz respeito a uma receita importada; se sim, ignorar a linha e continuar o ciclo!
									Text To lcSql Noshow textmerge
										select 
											ftstamp 
										from
											ft (nolock) 
										where 
											u_ltstamp = '<<uCrsIDP.ctltrctstamp>>'
											or u_ltstamp2 = '<<uCrsIDP.ctltrctstamp>>' 				
									endtext
									If !uf_gerais_actgrelha("", [uCrsIsRecIn], lcSql)
										uf_perguntalt_chama("Erro a determinar se a receita j� foi apagada da Base de Dados central (CTLTRCT)."+CHR(10)+CHR(10)+"Por favor contacte o suporte.", "Ok", "", 16)
										RETURN .f.
									ENDIF
									
									** Como h� uma venda em que esta receita est� mapeada (n�o foi apagada, entretanto, entre importa��es) podemos importar a linha
									If Reccount("uCrsIsRecIn") > 0
									
										** Alterar o nome do Posto para este ficheiro e Copiar os n�s originais de Lote e Receita para os campos lote_orig e no_orig que depois ser�o inseridos na CTLTRCT 
										Select uCrsIDP
										replace uCrsIDP.lote_orig		With lote
										replace uCrsIDP.no_orig			With nreceita
										replace uCrsIDP.posto 			With lcPosto
										
										** Deteminar o ultimo n�de Receita, Lote, S�rie para esta linha (Entidade/Tipo de Lote) se existirem j� lotes na BD Central
										Text To lcSql Noshow textmerge
											;with 
												cte1 (lote) as (
													Select 
														Max(lote)
													from
														ctltrct (nolock)
													where
														cptorgabrev='<<uCrsIDP.cptorgabrev>>'
														and tlote = '<<uCrsIDP.tlote>>'
														and ano = '<<uCrsIDP.ano>>'
														and mes = '<<uCrsIDP.mes>>'
												),
												cte2 (nreceita) as (
													Select 
														MAX(nreceita)
													from
														cte1, ctltrct (nolock)
													where
														cptorgabrev='<<uCrsIDP.cptorgabrev>>'
														and tlote = '<<uCrsIDP.tlote>>'
														and ano = '<<uCrsIDP.ano>>'
														and mes = '<<uCrsIDP.mes>>'
														and ctltrct.lote = cte1.lote
												)
											Select
												Lote = 
													Case
														 when c.nreceita is null then 1
														 Else
														 	Case
														 		when c.nreceita=30 then c.lote+1
														 		Else c.lote
														 	end
													end,
													
												nreceita = 
													Case
														when c.nreceita is null then 1 
														Else
															Case
																when c.nreceita=30 then 1 
																Else c.nreceita + 1
															end
													end,
												
												slote = 
													Case
														when c.slote is null then 1 
														Else c.slote
													end
											from 
												cte1 as a, 
												cte2 as b,
												ctltrct as c (nolock)
											where
												c.cptorgabrev='<<uCrsIDP.cptorgabrev>>'
												and c.tlote = '<<uCrsIDP.tlote>>'
												and c.ano = '<<uCrsIDP.ano>>'
												and c.mes = '<<uCrsIDP.mes>>'
												and c.lote = a.lote
												and c.nreceita = b.nreceita								
										
										ENDTEXT
										
										If !uf_gerais_actgrelha("", [uCrsNovaPosLotes], lcSql)
											uf_perguntalt_chama("Erro a determinar nova posi��o nos lotes na Base de Dados central."+CHR(10)+CHR(10)+"Por favor contacte o suporte.", "Ok", "", 16)
											RETURN .f.
										Endif								
										
										** Se nao tivermos resultados � sinal de que esta Entidade/Lote n�o teve ainda Lotes na Base de Dados central; determinar ent�o se h� alguma s�rie anterior ao m�s actual, porque o Lote e N� Receita ser�o "1"
										If Reccount("uCrsNovaPosLotes")==0
											Text To lcSql Noshow textmerge
												select 
													slote = Isnull(Max(slote),1)
												From
													ctltrct (nolock) 
												where 
													cptorgabrev='<<uCrsIDP.cptorgabrev>>' 
													and tlote = '<<uCrsIDP.tlote>>'
													and ano = '<<uCrsIDP.ano>>'
											endtext
											If !uf_gerais_actgrelha("", [uCrsNovaSerie], lcSql)
												uf_perguntalt_chama("Erro a determinar a nova s�rie nos lotes na Base de Dados central." +CHR(10)+CHR(10)+ "Por favor contacte o suporte.", "Ok", "", 16)
												RETURN .f.
											ENDIF
										
											Select 	uCrsIDP
											replace uCrsIDP.lote 			WITH 1
											replace uCrsIDP.nreceita 		With 1
											replace uCrsIDP.slote 			With uCrsNovaSerie.slote
											
											Fecha("uCrsNovaSerie")
											
										** O cursor acima tem resultados, portanto j� existem Lotes desta Entidade/Tipo na BD central; usar os dados do query acima
										ELSE
											Select uCrsIDP
											replace uCrsIDP.lote		WITH uCrsNovaPosLotes.lote	
											replace uCrsIDP.nreceita 	With uCrsNovaPosLotes.nreceita
											replace uCrsIDP.slote 		With uCrsNovaPosLotes.slote
										ENDIF
										
										** Fechar cursor auxiliar inicial
										Fecha("uCrsNovaPosLotes")
										
										** Fazer INSERT da linha do cursor j� com numera��o alterada
										Text To lcSql Noshow TextMerge
											INSERT INTO ctltrct (
												ano,cptorgabrev,cptorgstamp,cptplacode
												,ctltrctstamp,lote,lote_orig,mes,no_orig,nreceita,ousrdata,ousrhora,ousrinis,posto,slote,tlote,usrinis,usrlote
												,site
												)
											VALUES (
												'<<uCrsIDP.ano>>',
												'<<uCrsIDP.cptorgabrev>>',
												'<<uCrsIDP.cptorgstamp>>',
												'<<uCrsIDp.cptplacode>>',
												'<<uCrsIDP.ctltrctstamp>>',
												'<<uCrsIDP.lote>>',
												'<<uCrsIDP.lote_orig>>',
												'<<uCrsIDP.mes>>',
												'<<uCrsIDP.no_orig>>',
												'<<uCrsIDP.nreceita>>',
												'<<uf_gerais_getDate(uCrsIDP.ousrdata,"SQL")>>',
												'<<uCrsIDP.ousrhora>>',
												'<<uCrsIDP.ousrinis>>',
												'<<uCrsIDP.posto>>',						
												'<<uCrsIDP.slote>>',
												'<<uCrsIDP.tlote>>',
												'<<uCrsIDP.usrinis>>',
												'<<uCrsIDP.usrlote>>',
												'<<alltrim(mySite)>>'
											)
										Endtext
									
										IF !uf_gerais_actgrelha("", "", lcSql)
											uf_perguntalt_chama("Erro a inserir dados na Base de Dados central (CTLTRCT)." +CHR(10)+chr(10)+ "Por favor contacte o suporte.", "Ok", "", 16)
											RETURN .f.
										ENDIF
										
										
										** Actualizar o documento de factura��o correspondente com a nova posi��o nos Lotes, j� calculada para este stamp, para ambas as entidades (caso aplic�vel, pelo menos � primeira dever� ser feito sempre)
										
										Text To lcSql Noshow Textmerge && Entidade 1
											UPDATE
												ft
											set
												u_lote=lote,
												u_nslote=nreceita,
												u_slote=slote,
												u_tlote=tlote
											from
												ft
												inner join ctltrct (nolock) on u_ltstamp=ctltrctstamp 
											where
												ctltrctstamp='<<uCrsIDP.ctltrctstamp>>'
										ENDTEXT
					
										If !uf_gerais_actgrelha("", "", lcSql)
											uf_perguntalt_chama("Erro a atualizar nova posi��o da entidade 1 da inser��o de receita (FT)." +CHR(10)+CHR(10)+ "Por favor contacte o suporte.", "Ok", "", 16)
											RETURN .f.
										ENDIF								
										
										Text To lcSql Noshow Textmerge && Entidade 2
											UPDATE
												ft
											set
												u_lote2=lote,
												u_nslote2=nreceita,
												u_slote2=slote,
												u_tlote2=tlote
											from
												ft
												inner join ctltrct (nolock)  on u_ltstamp2=ctltrctstamp 
											where
												ctltrctstamp='<<uCrsIDP.ctltrctstamp>>'
										ENDTEXT
										
										If !uf_gerais_actgrelha("", "", lcSql)
											uf_perguntalt_chama("Erro a atualizar nova posi��o da entidade 2 da inser��o de receita (FT)." +CHR(10)+CHR(10)+ "Por favor contacte o suporte.", "Ok", "", 16)
											RETURN .f.
										Endif											
										
								
									Endif && Fim verificar se receita n�o foi entretanto apagada dos Lotes
									
								Endif && Fim processar STAMP ainda nao presente na BD
								
							Endif && Fim determinar se STAMP est� na BD ou n�o ok (se nao processado nao sabemos se o documento est� ou nao presente e tem que se abortar o INSERT)
							
							Select uCrsIDP && Fechar cursor de posi��o na BD Central e Voltar a seleccionar o cursor principal
						
						Endscan && Fim processar todos registos no cursor
					
					Endif && Fim processar ficheiro ZIP						
					
					REGUA(2) && Reset ao indicador de progresso
					
				Endfor  && Fim processamento ciclo de ficheiros
			 
			
				** Escrever a transac��o no disco ja que nada atras deu erro 
				Text To lcSql Noshow textmerge
					commit transaction <<alltrim(lcTran)>>
				ENDTEXT
				If !uf_gerais_actgrelha("", "", lcSql)
					uf_perguntalt_chama("Erro a escrever/finalizar importa��o de documentos." +CHR(10)+CHR(10)+ "Por favor contacte o suporte.", "Ok", "", 16)
					RETURN .f.
				ELSE
					uf_perguntalt_chama("Ficheiro importado com sucesso.", "Ok", "", 64)
					EXIT
				ENDIF

				
			** Handling de erros; fechar a REGUA da PHC, anular a lcTran e fazer RETURN
			Catch To lcError When .T.
	
				REGUA(2)
				
				Text To lcSql Noshow textmerge
					Rollback transaction <<alltrim(lcTran)>>
				endtext
			
				If !uf_gerais_actgrelha("", "", lcSql)
					uf_perguntalt_chama("Erro a anular importa��o de documentos" + CHR(10)+CHR(10)+ "Por favor contacte o suporte.", "Ok", "", 16)
				ENDIF
				
				uf_perguntalt_chama("Erro na importa��o do ficheiro." +CHR(10)+CHR(10)+ "Por favor contacte o suporte.", "Ok", "", 16)
				EXIT			
			
			** Cleanup; independentemente do que aconteceu acima limpar sempre a pasta de exporta��o:  
			Finally
		
				** Fechar TODOS os cursores eventualmente abertos independentemente do resultado!
				If Used("uCrsValImportExport")
					Fecha("uCrsValImportExport")
				ENDIF
				
				If Used("uCrsIDP")
					Fecha("uCrsIDP")
				ENDIF				
				
				If Used("uCrsIsDocIn")
					Fecha("uCrsIsDocIn")
				ENDIF
				
				If Used("uCrsLastDoc")
					Fecha("uCrsLastDoc")
				ENDIF											
					
				If Used("uCrsNovaPosLotes")
					Fecha("uCrsNovaPosLotes")
				ENDIF
				
				If Used("uCrsNovaSerie")
					Fecha("uCrsNovaSerie")
				ENDIF		
				
				If Used("uCrsHaLoteFechado")
					Fecha("uCrsHaLoteFechado")
				ENDIF
				
				** Fechar a liga��o � pasta tempor�ria do Unzip do ficheiro e apag�-la
				chdir(Home())
				oFso = CREATEOBJECT("Scripting.FileSystemObject")
				oFso.DeleteFolder(lcPath)
				oFso = null	

			ENDTRY
			
	
		** Fim deszipar ficheiro com sucesso
		ENDIF
	
	** Fim validar se o ficheiro tem nome v�lido (variavel preenchida, DEPRECATE)
	ENDIF
ENDFUNC


** Fecha o painel
FUNCTION uf_importExportReceitas_sair
	** fechar cursores
	IF USED("uCrsExportDocs")
		fecha("uCrsExportDocs")
	ENDIF 
	
	IF  USED("uCrsHaLoteFechado")
		fecha("uCrsHaLoteFechado")
	ENDIF 
	
	If Used("uCrsValImportExport")
		Fecha("uCrsValImportExport")
	Endif
	
	If Used("uCrsIDP")
		Fecha("uCrsIDP")
	Endif				
	
	If Used("uCrsIsDocIn")
		Fecha("uCrsIsDocIn")
	Endif
	
	If Used("uCrsLastDoc")
		Fecha("uCrsLastDoc")
	Endif											
		
	If Used("uCrsNovaPosLotes")
		Fecha("uCrsNovaPosLotes")
	Endif
	
	If Used("uCrsNovaSerie")
		Fecha("uCrsNovaSerie")
	Endif					
	
	If Used("uCrsHaLoteFechado")
		Fecha("uCrsHaLoteFechado")
	Endif
	
	&& fecha painel
	importExportReceitas.hide
	importExportReceitas.release
ENDFUNC


** Classe com metodos de Zip/Unzip adaptadas da API do Windows
DEFINE CLASS ZIP as Custom
	nomezip=.null.
    nomediretorio=.null.
    erro=""


	PROCEDURE init as VOID
		IF TYPE("_zipshell")#"U"
			RETURN
		ENDIF
		
		PUBLIC _zipshell as Object
			_zipshell=createobject("Shell.Application")
		RETURN
	ENDPROC


  	PROCEDURE arquivozip (m.lcArquivo as String) as Boolean
		IF !FILE(m.lcArquivo)
			STRTOFILE(Chr(80)+Chr(75)+Chr(5)+Chr(6)+Replicate(Chr(0),18), m.lcArquivo)
		ENDIF
		this.nomezip=_zipshell.namespace(m.lcArquivo)
		RETURN !ISNULL(this.nomezip)
	ENDPROC


	PROCEDURE diretorio (m.lcDiretorio as String) as Boolean
		this.nomediretorio=_zipshell.namespace(m.lcDiretorio)
		RETURN !ISNULL(this.nomediretorio)
	ENDPROC


	PROCEDURE zip as Boolean
		IF !this.verifica()
		      RETURN .f.
		ENDIF
		this.nomezip.copyhere(this.nomediretorio,"&H4")
		RETURN .t.
	ENDPROC


	PROCEDURE unzip as Boolean
		IF !this.verifica()
		      RETURN .f.
		ENDIF
		this.nomediretorio.copyhere(this.nomezip.items,"&H10")
	ENDPROC


	PROCEDURE verifica as Boolean
		IF ISNULL(this.nomezip)
		      this.erro="Arquivo ZIP n�o existe"
		      RETURN .f.
		ENDIF
		IF ISNULL(this.nomediretorio)
		      this.erro="Diretorio n�o existe"
		      RETURN .f.
		ENDIF
	ENDPROC
ENDDEFINE