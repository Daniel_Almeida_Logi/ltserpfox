**
DO facturacaoentidades
DO corrigirreceita
DO impressaorvr
DO importExportReceitas
DO buracos
DO consultadispensa
DO DIFFACTELEC
DO difsFacElecDetalhe

** Chamado do startup
FUNCTION uf_recoverAPP_lotes
	RETURN .t.
ENDFUNC


** Chamado do startup
FUNCTION uf_recoverAPP_lotes
	RETURN .t.
ENDFUNC 


**
FUNCTION uf_receituario_chama
	regua(0,4,"A carregar painel...")
	regua(1,1,"A carregar painel...")
	&& CONTROLA PERFIS DE ACESSO
	IF uf_gerais_validaPermUser(ch_userno,ch_grupo,'Gest�o de Receitu�rio')
		*
	ELSE
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ACEDER AO PAINEL GEST�O DE RECEITU�RIO.","OK","",48)
		regua(2)
		RETURN .f.
	ENDIF
	
	
	*** Abre o ecr� caso n�o esteja aberto
	IF TYPE("RECEITUARIO")=="U"

		**Valida Licenciamento	
		IF (uf_gerais_addConnection('REC') == .f.)
			regua(2)
			RETURN .F.
		ENDIF
		
		regua(1,2,"A carregar painel...")
		uf_receituario_cursoresDefault()
		regua(1,3,"A carregar painel...")
		DO FORM RECEITUARIO
		regua(1,4,"A carregar painel...")
		uf_receituario_CarregaMenu()
		
		&&Centra
		RECEITUARIO.autocenter = .t.
		RECEITUARIO.windowstate = 2
	ELSE
		regua(1,4,"A carregar painel...")
		RECEITUARIO.show
		
	ENDIF

    uf_receituario_actualiza()

	regua(2)
ENDFUNC


**
FUNCTION uf_receituario_cursoresDefault
	**
	Text To lcSql Noshow textmerge
		Set FMTONLY ON
			exec up_receituario_Closelotes 3000, 3, '<<ALLTRIM(uf_gerais_getSitePostos())>>'
		Set FMTONLY OFF
	Endtext
	If !uf_gerais_actgrelha("", "uCrsCloseLotes", lcSql)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA NA VERIFICA��O DE RECEITU�RIO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	ENDIF
		
	**
	Text To lcSql Noshow textmerge
		Set FMTONLY ON
			exec up_receituario_detalheLote 2012, 7, 'sns', 10, 1, '<<ALLTRIM(uf_gerais_getSitePostos())>>'
		Set FMTONLY OFF
	Endtext
	If !uf_gerais_actgrelha("", "uCrsDetalheLote", lcSql)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA NA VERIFICA��O DE RECEITU�RIO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	ENDIF
	
	**
	Text To lcSql Noshow textmerge
		SELECT convert(varchar,'') as cptorgabrev
	Endtext
	If !uf_gerais_actgrelha("", "uCrsEntidadesReceituario", lcSql)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA NA VERIFICA��O DAS ENTIDADES. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	ENDIF
	
	**
	** LISTAR RECEITAS IMPORTADAS
	Text To lcSql Noshow textmerge
		Set FMTONLY ON
		exec up_receituario_verReceitasImportadas 2050, 13, '', 0, ''
		Set FMTONLY OFF
	ENDTEXT
	If !uf_gerais_actgrelha("", [uCrsVisualRecImport], lcSql)
		uf_perguntalt_chama("Ocorreu um erro a verificar receitas importadas.", "Ok", "", 16)
		RETURN .f.
	Endif
ENDFUNC


**
FUNCTION uf_receituario_actualiza

	regua(0,1,"A processar informa��o de Lotes...")
	
	LOCAL lcSql, lcAno, lcMes, lcEntidade, lcDia, lcPosto
	STORE '' TO lcSql
	
	lcAno 		= RECEITUARIO.spinnerAno.value
	lcMes 		= uf_gerais_getMonthComboBox("RECEITUARIO.cbMes")
	lcEntidade	= ALLTRIM(RECEITUARIO.cbEntidade.displayvalue)
	lcDia		= ALLTRIM(RECEITUARIO.cbDia.displayvalue)
	lcPosto		= ALLTRIM(RECEITUARIO.cbPosto.displayvalue)
	
	DO CASE
		case Receituario.pageframe1.activepage == 1
			** WITH RECOMPILE, usado para evitar lentid�o da query
			
			Text To lcSql Noshow textmerge
				exec up_receituario_closelotes <<lcAno>>, <<lcMes>>, '<<ALLTRIM(uf_gerais_getSitePostos())>>' WITH RECOMPILE
			ENDTEXT
		
			uf_gerais_actGrelha("RECEITUARIO.pageframe1.page1.grdCloseLotes","uCrsCloseLotes",lcSQL)

			** Actualiza combobox Entidades
			Receituario.cbEntidade.value = ""
		
			SELECT distinct cptorgabrev FROM uCrsCloseLotes INTO CURSOR uCrsEntidadesReceituarioTemp readwrite
			select uCrsEntidadesReceituario
			DELETE ALL
			append blank 
			
			IF RECCOUNT("uCrsEntidadesReceituarioTemp")>0
				APPEND FROM DBF("uCrsEntidadesReceituarioTemp")
			ENDIF
		
		CASE receituario.pageframe1.activepage == 2
			uf_receituario_detalheDoLote(.f.)
		
		CASE receituario.pageframe1.activepage == 3
			**
			** LISTAR RECEITAS IMPORTADAS
			** exec up_receituario_verReceitasImportadas <<lcAno>>, <<lcMes>>, '<<lcEntidade>>', <<IIF(empty(lcDia), 0, lcDia)>>, '<<IIF(EMPTY(lcPosto), 'Todos', lcPosto)>>'
			Text To lcSql Noshow textmerge
				exec up_receituario_verReceitasImportadas <<lcAno>>, <<lcMes>>
			ENDTEXT
			If !uf_gerais_actgrelha("RECEITUARIO.pageframe1.page3.grdReceitasImportadas", [uCrsVisualRecImport], lcSql)
				uf_perguntalt_chama("Ocorreu um erro a verificar receitas importadas.", "Ok", "", 16)
				RETURN .f.
			ENDIF
			
			** Actualiza combobox Entidades
			Receituario.cbEntidade.value = ""
		
			SELECT distinct cptorgabrev FROM uCrsVisualRecImport INTO CURSOR uCrsEntidadesReceituarioTemp readwrite
			select uCrsEntidadesReceituario
			DELETE ALL
			append blank
			
			IF RECCOUNT("uCrsEntidadesReceituarioTemp")>0
				APPEND FROM DBF("uCrsEntidadesReceituarioTemp")
			ENDIF
	ENDCASE
	
	regua(2)
ENDFUNC


**
FUNCTION uf_receituario_filtraEntidade
	** Aplica Filtros de Entidade

	IF receituario.pageframe1.activepage == 1 OR receituario.pageframe1.activepage == 2

		Select uCrsCloseLotes
		If !EMPTY(Alltrim(Upper(RECEITUARIO.cbEntidade.Value)))
			Set Filter To Alltrim(Upper(uCrsCloseLotes.cptorgabrev)) == Alltrim(Upper(RECEITUARIO.cbEntidade.Value))
			Go TOP
		ELSE
			Select uCrsCloseLotes
			Set Filter To
			Go TOP
		ENDIF
		
		Receituario.pageframe1.page1.grdCloseLotes.refresh
	ELSE
		
		Select uCrsVisualRecImport
		
		Set Filter To alltrim(str(day(uCrsVisualRecImport.fdata))) == iif(!EMPTY(receituario.cbDia.displayValue), ALLTRIM(receituario.cbDia.displayValue), alltrim(str(day(uCrsVisualRecImport.fdata)))) AND Alltrim(Upper(uCrsVisualRecImport.cptorgabrev)) == iif(empty(receituario.cbEntidade.displayValue), Alltrim(Upper(uCrsVisualRecImport.cptorgabrev)), UPPER(ALLTRIM(receituario.cbEntidade.displayValue))) AND Alltrim(Upper(uCrsVisualRecImport.posto)) == iif(empty(receituario.cbPosto.displayValue), Alltrim(Upper(uCrsVisualRecImport.posto)), UPPER(ALLTRIM(receituario.cbPosto.displayValue)))
		
		Receituario.pageframe1.page3.grdReceitasImportadas.refresh
	ENDIF
ENDFUNC


**
FUNCTION uf_receituario_CarregaMenu
	
	RECEITUARIO.menu1.adicionaOpcao("opcoes", "Op��es", myPath + "\imagens\icons\opcoes_w.png", "","")
	**RECEITUARIO.menu1.adicionaOpcao("actualizar", "Atualizar", myPath + "\imagens\icons\actualizar_w.png", "uf_receituario_actualiza","")
	RECEITUARIO.menu1.adicionaOpcao("detalheLote", "Detalhe Lote", myPath + "\imagens\icons\rec_linhas_w.png", "uf_receituario_detalheDoLote with .f.","")
	RECEITUARIO.menu1.adicionaOpcao("verDoc", "Ver Doc.", myPath + "\imagens\icons\doc_seta_w.png", "uf_receituario_verDoc","")
	RECEITUARIO.menu1.adicionaOpcao("selTodos", "Sel. Todos", myPath + "\imagens\icons\unchecked_w.png", "uf_receituario_selTodos with .f.","")
	RECEITUARIO.menu1.adicionaOpcao("fecharLotes", "Fechar Lotes", myPath + "\imagens\icons\receitas_cadeado_w.png", "uf_receituario_fecharLotes","")
	RECEITUARIO.menu1.adicionaOpcao("editarReceita", "Editar", myPath + "\imagens\icons\lapis_w.png", "uf_receituario_editarReceita","")
	RECEITUARIO.menu1.adicionaOpcao("corrigirReceita", "Corrigir", myPath + "\imagens\icons\historico_w.png", "uf_receituario_corrigirReceitaAuto","")
	RECEITUARIO.menu1.adicionaOpcao("reinserirReceita", "Reinserir", myPath + "\imagens\icons\mais_w.png", "uf_receituario_reinserirReceita","")
	RECEITUARIO.menu1.adicionaOpcao("eliminarReceita", "Eliminar", myPath + "\imagens\icons\cruz_w.png", "uf_receituario_eliminarReceita","")
	RECEITUARIO.menu1.adicionaOpcao("emitirVerbere", "Verbete", myPath + "\imagens\icons\receitas_imprimir_w.png", "uf_receituario_chamaVerbete","")
	RECEITUARIO.menu1.adicionaOpcao("emitirResumo", "Resumo", myPath + "\imagens\icons\receitas_imprimir_w.png", "uf_receituario_chamaResumoLotes","")
	RECEITUARIO.menu1.adicionaOpcao("emitirFacturas", "Faturar", myPath + "\imagens\icons\doc_fact_w.png", "uf_receituario_emitirFacturas","")
	RECEITUARIO.menu1.adicionaOpcao("emitirGuiaFatura", "Guia Fatura", myPath + "\imagens\icons\receitas_imprimir_w.png", "uf_receituario_chamaGuiaFatura","")
	RECEITUARIO.menu1.adicionaOpcao("gestaoReceituario", "Voltar", myPath + "\imagens\icons\receitas_w.png", "uf_receituario_voltar","")
	RECEITUARIO.menu1.adicionaOpcao("prever", "Prever", myPath + "\imagens\icons\prever_imp_w.png", "uf_receituario_imprimir with .f.","")
	RECEITUARIO.menu1.adicionaOpcao("imprimir", "Imprimir", myPath + "\imagens\icons\imprimir_w.png", "uf_receituario_imprimir with .t.","")
	
	
	** configura menu de op��es
	RECEITUARIO.menu_opcoes.adicionaOpcao("abrirLotes", "Abrir Lotes", "", "uf_receituario_abrirLotes")
	RECEITUARIO.menu_opcoes.adicionaOpcao("impReceitas", "Imprimir Rec. Seguida", "", "uf_receituario_chamaImprimirReceitasSeguida")
	RECEITUARIO.menu_opcoes.adicionaOpcao("ImportExport", "Import/Export Receitas", "", "uf_receituario_chamaImportExportReceitas")
	RECEITUARIO.menu_opcoes.adicionaOpcao("receitasImport", "Receitas Importadas", "", "uf_receituario_verReceitasImportadas")
	RECEITUARIO.menu_opcoes.adicionaOpcao("buracos", "Buracos", "", "uf_receituario_buracos")
	SELECT ucrse1
	IF ALLTRIM(UPPER(ucrse1.u_assFarm)) == "ANF"
		RECEITUARIO.menu_opcoes.adicionaOpcao("fed", "Erros receitu�rio", "", "uf_DIFFACTELEC_chama")
	ENDIF
	**RECEITUARIO.menu_opcoes.adicionaOpcao("ConsultarReceitas", "Consultar Receitas", "", "uf_receituario_ConsultarReceitas")
	
	uf_receituario_alternaMenu()
	
	** Menu do container abrir lotes
	RECEITUARIO.abrelotes.menu1.estado("", "", "Gravar", .t., "Sair", .t.)
	receituario.abrelotes.menu1.funcaoGravar	= "uf_receituario_abrirLotesGuardar"
	receituario.abrelotes.menu1.funcaoSair		= "uf_receituario_abrirLotesFechar"
	
	RECEITUARIO.refresh
ENDFUNC




FUNCTION uf_receituario_loteCodeToLoteId
	LPARAMETERS lcLoteCode
	

	fecha("ucrsTempLoteAux")
	LOCAL lcSql 
	LOCAL  lcCodigoFinal
	lcCodigoFinal = lcLoteCode
	
	Text To lcSql Noshow textmerge
		SELECT loteId FROM tlote(nolock)  WHERE codigo= '<<lcLoteCode>>'
	Endtext
	If !uf_gerais_actgrelha("", "ucrsTempLoteAux", lcSql)
		RETURN lcLoteId
	ENDIF
	
	if(!EMPTY(ALLTRIM(ucrsTempLoteAux.loteId)))
		lcCodigoFinal = ALLTRIM(ucrsTempLoteAux.loteId)
	ENDIF
	

	
	fecha("ucrsTempLoteAux")
	
		
	RETURN lcCodigoFinal 
	
ENDFUNC
	
	



**
FUNCTION uf_receituario_alternaMenu
	
	LOCAL lcPag
	lcPag = RECEITUARIO.PAGEFRAME1.ACTIVEPAGE	
	
	** Import export de receitas
	IF !uf_gerais_getParameter('ADM0000000038', 'BOOL')
		receituario.menu_opcoes.estado("ImportExport, receitasImport", "HIDE")
	ELSE
		IF UPPER(ALLTRIM(uf_gerais_getParameter('ADM0000000038', 'TEXT'))) == "EXPORT"
			receituario.menu_opcoes.estado("receitasImport", "HIDE")
		ELSE
			receituario.menu_opcoes.estado("receitasImport", "SHOW")
		ENDIF
	ENDIF
	
	
	DO CASE
		CASE lcPag == 1
			RECEITUARIO.menu1.detalheLote.config("Detalhe Lote", myPath + "\imagens\icons\rec_linhas_w.png", "uf_receituario_detalheDoLote with .f.","D")
			
			Receituario.menu1.estado("verDoc, eliminarReceita, editarReceita, corrigirReceita, reinserirReceita, gestaoReceituario, prever, imprimir", "HIDE")
			*Receituario.menu1.estado("detalheLote, selTodos, fecharLotes, emitirResumo, emitirVerbere, emitirFacturas, buracos", "SHOW")
			Receituario.menu1.estado("detalheLote, selTodos, fecharLotes, emitirResumo, emitirVerbere, emitirGuiaFatura, emitirFacturas", "SHOW")
			
			** Coloca objectos readonly
			RECEITUARIO.spinnerAno.enabled = .t.
			RECEITUARIO.cbMes.enabled = .t.
			RECEITUARIO.cbEntidade.enabled = .t.
			receituario.dia.visible = .f.
			receituario.cbDia.visible = .f.
			receituario.posto.visible = .f.
			receituario.cbPosto.visible = .f.

		CASE lcPag == 2
			RECEITUARIO.menu1.detalheLote.config("Dados Lotes", myPath + "\imagens\icons\rec_linhas_w.png", "uf_receituario_detalheDoLote with .t.","D")
			
			Receituario.menu1.estado("detalheLote, verDoc, eliminarReceita, editarReceita, corrigirReceita, reinserirReceita", "SHOW")
			*Receituario.menu1.estado("selTodos, fecharLotes, emitirFacturas, gestaoReceituario, prever, imprimir, buracos", "HIDE")
			Receituario.menu1.estado("selTodos, fecharLotes, emitirResumo, emitirVerbere, emitirFacturas, emitirGuiaFatura, gestaoReceituario, prever, imprimir", "HIDE")
			
			** Coloca objectos readonly
			RECEITUARIO.spinnerAno.enabled = .f.
			RECEITUARIO.cbMes.enabled = .f.
			RECEITUARIO.cbEntidade.enabled = .f.
			receituario.dia.visible = .f.
			receituario.cbDia.visible = .f.
			receituario.posto.visible = .f.
			receituario.cbPosto.visible = .f.			
		
		CASE lcPag == 3
			*receituario.menu1.estado("detalheLote, selTodos, fecharLotes, editarReceita, eliminarReceita, emitirResumo, emitirVerbere, emitirFacturas, corrigirReceita, buracos", "HIDE")
			receituario.menu1.estado("detalheLote, selTodos, fecharLotes, editarReceita, eliminarReceita, emitirResumo, emitirVerbere, emitirFacturas, emitirGuiaFatura, corrigirReceita, reinserirReceita", "HIDE")
			receituario.menu1.estado("verdoc, prever, imprimir, gestaoReceituario", "SHOW")
			
			receituario.menu_opcoes.estado("receitasImport", "HIDE")
			
			** Coloca objectos readonly
			RECEITUARIO.spinnerAno.enabled = .t.
			RECEITUARIO.cbMes.enabled = .t.
			RECEITUARIO.cbEntidade.enabled = .t.
			receituario.dia.visible = .t.
			receituario.cbDia.visible = .t.
			receituario.posto.visible = .t.
			receituario.cbPosto.visible = .t.
			
	ENDCASE
	
	RECEITUARIO.menu1.refresh
ENDFUNC



**
Function uf_receituario_imprimir
	LPARAMETERS nImprime
	
	** receitas importadas
	IF receituario.pageframe1.activepage == 3
		Select uCrsVisualRecImport
		If !Empty(uCrsVisualRecImport.ftstamp)
			
			If Upper(Alltrim(uCrsVisualRecImport.ctltrctstamp)) == Upper(Alltrim(uCrsVisualRecImport.u_ltstamp))
				
				IF nImprime
					uf_imprimirpos_impRec(uCrsVisualRecImport.ftstamp, 1, "IMPRESSAORVR")
				ELSE
					uf_imprimirpos_prevRec(uCrsVisualRecImport.ftstamp, 1, .f.)
				ENDIF
				
			ELSE
			
				IF Upper(Alltrim(uCrsVisualRecImport.ctltrctstamp)) == Upper(Alltrim(uCrsVisualRecImport.u_ltstamp2))
					
					IF nImprime
						uf_imprimirpos_impRec(uCrsVisualRecImport.ftstamp, 2, "IMPRESSAORVR")
					ELSE
						uf_imprimirpos_prevRec(uCrsVisualRecImport.ftstamp, 2, .f.)
					ENDIF
					
				ENDIF
			ENDIF
			
		ENDIF
	ENDIF
ENDFUNC


**
FUNCTION uf_receituario_voltar
	receituario.pageframe1.activepage = 1
	
	uf_receituario_alternaMenu()
ENDFUNC


**
FUNCTION uf_receituario_chamaImportExportReceitas
	uf_receituario_sombra(.t.)
	
	uf_importExportReceitas_chama()

	uf_receituario_sombra(.f.)
ENDFUNC


**
FUNCTION uf_receituario_verReceitasImportadas

	receituario.pageframe1.activepage = 3
	
	uf_receituario_alternaMenu()
	
	uf_receituario_fillDataCbDiaPosto()

ENDFUNC


**
FUNCTION uf_receituario_fillDataCbDiaPosto

	RECEITUARIO.cbDia.clear
	RECEITUARIO.cbDia.value = ''
	RECEITUARIO.cbDia.displayvalue = ''
	
	RECEITUARIO.cbPosto.clear
	RECEITUARIO.cbPosto.value = ''
	RECEITUARIO.cbPosto.displayvalue = ''

	IF !EMPTY(Receituario.spinnerAno.value) AND !EMPTY(RECEITUARIO.cbMes.displayvalue)
		** Dia
		uf_gerais_fillDayComboBox("receituario.cbDia", uf_gerais_getMonthComboBox("RECEITUARIO.cbMes"), Receituario.spinnerAno.value, .t.)
		
		
		** Posto
		TEXT TO lcSql NOSHOW TEXTMERGE
			SELECT 
				posto = ''
			union all
			
			SELECT distinct
				posto
			from
				ctltrct (nolock)
			where
				posto != ''
				and ano = <<Receituario.spinnerAno.value>>
				and mes = <<uf_gerais_getMonthComboBox("RECEITUARIO.cbMes")>>
		ENDTEXT
		IF uf_gerais_actgrelha("", "uCrsTemp", lcSql)
			uf_gerais_fillDataComboBox("receituario.cbPosto", "uCrsTemp", "posto", .f.)
			fecha("uCrsTemp")
		ENDIF
	ENDIF
ENDFUNC


**
FUNCTION uf_receituario_emitirFacturas
	
	IF myPosto
		uf_perguntalt_chama("A Gest�o de Receitu�rio ter� que ser efectuada na farm�cia principal.","OK","",64)
		RETURN .f.
	ENDIF
	
	uf_facturacaoentidades_chama(receituario.spinnerAno.value, uf_gerais_getMonthComboBox("RECEITUARIO.cbMes"))
ENDFUNC


**
FUNCTION uf_receituario_chamaResumoLotes
	uf_receituario_sombra(.t.)

	uf_impressaoRVR_chama(.f., .f., "RESUMOLOTES")

	uf_receituario_sombra(.f.)
ENDFUNC


**
FUNCTION uf_receituario_chamaVerbete
	uf_receituario_sombra(.t.)
	
	uf_impressaoRVR_chama(.f., .f., "VERBETE")
	
	uf_receituario_sombra(.f.)
ENDFUNC


**
FUNCTION uf_receituario_verDoc
	
	IF !(TYPE('FACTURACAO')=="U")
		IF myFtAlteracao == .t. OR myFtIntroducao == .t.
			uf_perguntalt_chama("O ecr� de fatura��o encontra-se em modo de introdu��o ou edi��o.", "Ok", "", 48)
			RETURN .f.
		ENDIF
	ENDIF
	
	** detalhe do lote
	IF receituario.pageframe1.activepage == 2
		IF USED("uCrsDetalheLote")
			SELECT uCrsDetalheLote
			uf_facturacao_chama(Alltrim(uCrsDetalheLote.ftstamp))
			RETURN .t.
		ELSE
			RETURN .f.
		ENDIF
	ENDIF
	
	** receitas importadas
	IF receituario.pageframe1.activepage == 3
		IF USED("uCrsVisualRecImport")
			SELECT uCrsVisualRecImport
			uf_facturacao_chama(ALLTRIM(uCrsVisualRecImport.ftstamp))
			RETURN .t.
		ELSE
			RETURN .f.
		ENDIF
	ENDIF
ENDFUNC


**
FUNCTION uf_receituario_detalheDoLote
	LPARAMETERS tcBool

	IF !tcBool && detalhe do lote
		SELECT uCrsCloseLotes
		IF !EMPTY(uCrsCloseLotes.cptorgabrev)
			regua(0,1,"A processar informa��o do Lote...")
			

						
			TEXT TO lcSql NOSHOW textmerge
				exec up_receituario_detalheLote <<uCrsCloseLotes.ano>>, <<uCrsCloseLotes.mes>>, '<<ALLTRIM(uCrsCloseLotes.cptorgabrev)>>', <<uCrsCloseLotes.tlote>>, <<uCrsCloseLotes.lote>>, '<<ALLTRIM(uf_gerais_getSitePostos())>>'
			ENDTEXT
			
		
			uf_gerais_actGrelha("RECEITUARIO.pageframe1.page2.grdDetalhe","uCrsDetalheLote",lcSQL)
			

			** navega pageframe
			RECEITUARIO.pageframe1.activepage = 2
		ENDIF
		
		regua(2)

	ELSE && dados lotes
		** navega pageframe
		RECEITUARIO.pageframe1.activepage = 1
	ENDIF
	
	** alterna Menu

	uf_receituario_alternaMenu()

	
	
ENDFUNC


**
FUNCTION uf_receituario_selTodos
	LPARAMETERS tcBool
	
		IF EMPTY(receituario.menu1.seltodos.tag) OR receituario.menu1.seltodos.tag == "false"
		&& aplica sele��o
		REPLACE ALL fechar WITH .t. IN uCrsCloseLotes
		
		&& altera o botao
		receituario.menu1.selTodos.tag = "true"
		receituario.menu1.selTodos.config("Sel. Todos", myPath + "\imagens\icons\checked_w.png", "uf_receituario_selTodos","T")
		
		&& actualiza o ecra
		*pesqstocks.refresh()
	ELSE
		&& aplica sele��o
		REPLACE ALL fechar WITH .f. IN uCrsCloseLotes
		
		&& altera o botao
		receituario.menu1.selTodos.tag = "false"
		receituario.menu1.selTodos.config("Sel. Todos", myPath + "\imagens\icons\unchecked_w.png", "uf_receituario_selTodos","T")
		
		&& actualiza o ecra
		*pesqstocks.refresh()
	ENDIF
		
	SELECT uCrsCloseLotes
	GO top
	
	receituario.pageframe1.page1.grdCloseLotes.setfocus
ENDFUNC


**
FUNCTION uf_receituario_fecharLotes   
	
	IF myPosto
		uf_perguntalt_chama("A Gest�o de Receitu�rio ter� que ser efectuada na farm�cia principal.","OK","",64)
		RETURN .f.
	ENDIF
	
	Local lcLotes, lcSql
	lcLotes = 0
	lcSql = ""
	
	Select uCrsCloseLotes
	Calculate Cnt() FOR uCrsCloseLotes.Fechar=.T. TO lcLotes

	IF lcLotes=0
		uf_perguntalt_chama("NENHUM LOTE SE ENCONTRA SELECCIONADO.","OK","",48)
		Return .F.
	ENDIF

	If !uf_perguntalt_chama("VAI FECHAR OS LOTES SELECCIONADOS. TEM A CERTEZA QUE PRETENDE CONTINUAR?", "Sim", "N�o")
		Return .F.
	ENDIF
	
	SET HOURS TO 24
	Atime=ttoc(datetime()+(difhoraria*3600),2)
	Astr=left(Atime,8)
		
	SELECT uCrsCloseLotes
	SCAN FOR fechar == .T.
		Text To lcSql Noshow Textmerge
			UPDATE
				ctltrct
			Set
				fechado = Cast(1 as bit)
				,datafechado = '<<uf_gerais_getDate(Date(),"SQL")>>'
				,horafechado = '<<Astr>>'
				,usrlote = '<<m_chinis>>'
			WHERE
				ano = <<uCrsCloseLotes.ano>> AND mes = <<uCrsCloseLotes.mes>>
				AND tlote = '<<uCrsCloseLotes.tlote>>' AND slote = <<uCrsCloseLotes.slote>>
				AND lote = <<uCrsCloseLotes.lote>> AND cptorgabrev = '<<uCrsCloseLotes.cptorgabrev>>'
				and site = '<<ALLTRIM(mySite)>>'
		ENDTEXT
		IF !uf_gerais_actgrelha("", "", lcSql)
			uf_perguntalt_chama("OCORREU UM ERRO A FECHAR OS LOTES. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		ENDIF

	ENDSCAN
	
	
	SELECT uCrsCloseLotes
	GO TOP
	SCAN
		IF uCrsCloseLotes.fechar = .t.
			Replace uCrsCloseLotes.fechado	with .t.
		ENDIF
	ENDSCAN
	GO TOP
ENDFUNC


**
FUNCTION uf_receituario_abrirLotes
	
	IF myPosto
		uf_perguntalt_chama("A Gest�o de Receitu�rio ter� que ser efectuada na farm�cia principal.","OK","",64)
		RETURN .f.
	ENDIF
	
	** Colocar container visivel
	RECEITUARIO.abreLotes.visible = .t.
	
	** Colocar valores por defeito
	RECEITUARIO.abreLotes.spinnerAno.value = RECEITUARIO.spinnerAno.value
	RECEITUARIO.abreLotes.cbMes.value = RECEITUARIO.cbMes.value
	
	** Colocar sombra
	uf_receituario_sombra(.t.)
ENDFUNC


**
FUNCTION uf_receituario_sombra
	LPARAMETERS tcBool
	
	IF tcBool
		** remover foco da grelha
		receituario.crack.setfocus
		
		** Colocar sombra
		RECEITUARIO.sombra.width	= Receituario.width
		RECEITUARIO.sombra.height	= Receituario.height
		RECEITUARIO.sombra.left 	= 0
		RECEITUARIO.sombra.top 		= 0
		
		Receituario.sombra.visible 	= .t.
	ELSE
		** remover sombra
		Receituario.sombra.visible = .f.
	ENDIF
ENDFUNC


**
FUNCTION uf_receituario_abrirLotesFechar
	** Esconder o painel
	RECEITUARIO.abreLotes.visible = .f.

	uf_receituario_sombra(.f.)
ENDFUNC


**
FUNCTION uf_receituario_abrirLotesGuardar
	LOCAL lcAno, lcMes, lcEntidade, lcTipoLote, lcLoteInicio, lcLoteFim
	
	lcAno		= RECEITUARIO.abreLotes.spinnerAno.value
	lcMes		= uf_gerais_getMonthComboBox("RECEITUARIO.abreLotes.cbMes")
	lcEntidade	= ALLTRIM(RECEITUARIO.abreLotes.cbEntidade.value)
	lcTipoLote	= ALLTRIM(RECEITUARIO.abreLotes.cbTipolote.value)
	lcAno		= RECEITUARIO.abreLotes.spinnerAno.value
	lcLoteInicio = RECEITUARIO.abreLotes.LoteInicio.value
	lcLoteFim = RECEITUARIO.abreLotes.LoteFim.value
	
	** 
	IF EMPTY(lcEntidade) OR EMPTY(lcTipoLote)
		uf_perguntalt_chama("O preenchimento da Entidade e Tipo de Lote � obrigat�rio.","OK","", 16)
		RETURN .f.
	ENDIF
	
	** Validar os dados
	IF !EMPTY(lcEntidade) AND !EMPTY(lcTipoLote)
		** validar opera��o
		IF !uf_perguntalt_chama([ATEN��O: Vai re-abrir os lotes de ];
			+ RECEITUARIO.abreLotes.cbMes.value + [ de ] + ALLTRIM(STR(lcAno));
			+ [ para a(s) entidade(s): ] + lcEntidade;
			+ [ ; do(s) lote(s) do tipo: ] + lcTipoLote;
			+ [ ; intervalo de lotes: ] + ASTR(lcLoteInicio) + [ a ] + ASTR(lcLoteFim) + [.];
			+ CHR(13)+CHR(13);
			+ [Lotes facturados n�o ser�o re-abertos.], "Sim", "N�o")
			
			RETURN .f.
		ENDIF
	ENDIF
	
	** Fazer update a tabela ctltrct para reabrir os lotes
	TEXT TO lcSql TEXTMERGE NOSHOW
		UPDATE
			ctltrct
		SET
			fechado = 0
		where
			ano = <<lcAno>> 
			and mes = <<lcMes>>
			and cptorgabrev = case when '<<lcEntidade>>' = 'TODAS' then cptorgabrev else '<<lcEntidade>>' end
			and tlote = case when '<<lcTipoLote>>' = 'TODOS' then tlote else '<<lcTipoLote>>' end
			and fechado = 1 
			and u_fistamp = ''
			and site = '<<ALLTRIM(mySite)>>'
			and lote BETWEEN <<lcLoteInicio>> and <<lcLoteFim>>
			
		select @@ROWCOUNT as nrRows
	ENDTEXT

	IF !uf_gerais_actgrelha("", "uCrsAbriuLotes", lcSql)
		uf_perguntalt_chama("OCORREU UM ERRO A ABRIR OS LOTES FECHADOS. POR FAVOR CONTACTE O SUPORTE.","OK","", 16)
		RETURN .f.
	ELSE
		IF uCrsAbriuLotes.nrRows > 0
					
			** Verificar se os lotes abertos est�o a ser visualizados na grelha, se sim, actualizar grelha.
			IF lcEntidade == "TODAS"
				IF RECEITUARIO.spinnerAno.value = lcAno AND uf_gerais_getMonthComboBox("RECEITUARIO.abreLotes.cbMes") = lcMes
					SELECT uCrsCloseLotes
					replace ALL uCrsCloseLotes.fechado WITH .f.
				ENDIF
			ELSE
				SELECT uCrsCloseLotes
				GO TOP
				SCAN
					IF (uCrsCloseLotes.ano == lcAno) AND (uCrsCloseLotes.mes == lcMes) AND (upper(uCrsCloseLotes.cptorgabrev) = upper(lcEntidade)) AND (upper(uCrsCloseLotes.tlote) = upper(lcTipoLote))
						replace uCrsCloseLotes.fechado with .f.
					ENDIF
					SELECT uCrsCloseLotes
				ENDSCAN
			ENDIF
			*************
			
			uf_perguntalt_chama("OPERA��O REALIZADA COM SUCESSO. LOTES REABERTOS.","OK","", 64)
			
			Go TOP IN uCrsCloseLotes
		ELSE
			uf_perguntalt_chama("N�O FORAM ENCONTRADOS LOTES FECHADOS.","OK","", 48)
		ENDIF
		
		fecha("uCrsAbriuLotes")
	ENDIF
	
	** Esconder o container
	uf_receituario_abrirLotesFechar()
	
	uf_receituario_actualiza()
ENDFUNC


**
FUNCTION uf_receituario_eliminarReceita
	
	IF !uf_gerais_validaPermUser(ch_userno, ch_grupo, 'Gest�o de Receitu�rio - Eliminar receita')
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ELIMINAR RECEITAS.","OK","", 48)
		Return .F.
	ENDIF
	
	IF Used("uCrsDetalheLote")
		SELECT uCrsDetalheLote
		IF reccount("uCrsDetalheLote")>0
			IF uCrsDetalheLote.Facturado=.t.
				uf_perguntalt_chama("N�O PODE ELIMINAR UM REGISTO FACTURADO.","OK","", 48)
				Return
			ENDIF
			IF uCrsDetalheLote.fechado=.t.
				uf_perguntalt_chama("N�O PODE ELIMINAR UM REGISTO FECHADO.","OK","", 48)
				Return
			ENDIF
			
			If uf_perguntalt_chama("ATEN��O: VAI ELIMINAR ESTA RECEITA DOS LOTES. TEM A CERTEZA QUE PRETENDE CONTINUAR?", "Sim", "N�o")
				** guardar stamp da FT **
				Local lcFtStamp, lctoken_efectivacao_compl
				lcFtStamp = ''	
				
				lcSQL = ''
				TEXT TO lcSQL NOSHOW TEXTMERGE
					SELECT
						ftstamp 
						,ft2.token
						,ft2.u_receita
						,ft2.u_docorig
						,ft2.token_efectivacao_compl
						,anulaReceita = case 
						--when (u_ltstamp='' or u_ltstamp2='') and ft2.token!='' then 1
						when ((u_ltstamp='' and u_slote=0) or (u_ltstamp2='' and u_lote2=0)) and ft2.token!='' then 1
						when u_ltstamp='<<ALLTRIM(uCrsDetalheLote.ctltrctstamp)>>' and ft2.u_abrev='SNS' and ft2.token!='' then 1
						when u_ltstamp2='<<ALLTRIM(uCrsDetalheLote.ctltrctstamp)>>' and ft2.u_abrev2='SNS' and ft2.token!='' then 1
						else 0
						end
					FROM
						ft (nolock)
						inner join ft2 (nolock) on ft.ftstamp = ft2.ft2stamp
					WHERE
						u_ltstamp='<<ALLTRIM(uCrsDetalheLote.ctltrctstamp)>>' OR  u_ltstamp2 = '<<ALLTRIM(uCrsDetalheLote.ctltrctstamp)>>'
						
						
				ENDTEXT
				IF uf_gerais_actgrelha("", [uCrsDocFt], lcSql)
					IF Reccount([uCrsDocFt])>0
						lcFtStamp = Alltrim(uCrsDocFt.ftstamp)
						lctoken_efectivacao_compl = ALLTRIM(uCrsDocFt.token_efectivacao_compl)
					ENDIF 
				ENDIF 
				**
				&&u_ltstamp='<<uCrsDetalheLote.ctltrctstamp>>' OR  u_ltstamp2 = '<<uCrsDetalheLote.ctltrctstamp>>'
				

				** Anular Webservice
				SELECT uCrsDocFt
				IF !EMPTY(ALLTRIM(uCrsDocFt.token)) AND EMPTY(uCrsDocFt.u_docorig ) AND uCrsDocFt.anulaReceita==1
					IF(!uf_atendimento_receitasAnular(uCrsDocFt.u_receita,uCrsDocFt.token))
						RETURN .f.
					ENDIF
				ENDIF 
				
				SELECT uCrsDocFt
				IF !EMPTY(lctoken_efectivacao_compl) AND ALLTRIM(uCrsDetalheLote.cptorgabrev)<>'SNS'
					uf_compart_anula(lctoken_efectivacao_compl, uCrsDocFt.ftstamp)
				ENDIF 
				
				IF USED("uCrsDocFt")
					Fecha("uCrsDocFt")
				ENDIF 
				
				Local lcTran
				lcTran = "TT"+Sys(2015)
				
				TEXT TO lcSql NOSHOW TEXTMERGE
					Begin Transaction <<lcTran>>
					
						DELETE
							ctltrct
						where
							ctltrctstamp='<<uCrsDetalheLote.ctltrctstamp>>'
									
						UPDATE
							ft
						SET
							u_ltstamp	= ''
							,u_slote	= 0
						WHERE
							u_ltstamp = '<<uCrsDetalheLote.ctltrctstamp>>'
					
						UPDATE
							ft
						SET
							u_ltstamp2	= ''
							,u_slote2	= 0
						WHERE
							u_ltstamp2 = '<<uCrsDetalheLote.ctltrctstamp>>'
					
						If @@ERROR=0
							COMMIT TRANSACTION <<lcTran>>
						ELSE
							BEGIN
								RollBack Transaction <<lcTran>>
								RAISERROR('Erro na actualiza��o de Contadores',16,1)
					END
				ENDTEXT
				
				
		
		
				
				IF !uf_gerais_actgrelha("", "", lcSql)
					uf_perguntalt_chama("OCORREU UM ERRO A ELIMINAR A RECEITA. POR FAVOR CONTACTE O SUPORTE","OK","",16)
				ELSE
								
					&&Receitas Logs
					LOCAL lcSqlToSafe
					lcSqlToSafe=lcSql
					uf_gerais_registaErroLog("SQL Receitas logs: " + ALLTRIM(lcSqlToSafe), "R2")
					uf_perguntalt_chama("RECEITA ELIMINADA COM SUCESSO.","OK","", 64)
				
					** registo de ocorrencia **
					LOCAL lcOvalor
					SELECT uCrsDetalhelote
					lcOvalor = 'Ano:' + ALLTRIM(STR(uCrsDetalheLote.ano)) + ' Mes:' + ALLTRIM(STR(uCrsDetalheLote.mes)) + ' Abrev:' + alltrim(uCrsDetalheLote.cptorgabrev) + ' TLote:' + Alltrim(uCrsDetalheLote.tlote2) + ' Lote:' + ALLTRIM(STR(uCrsDetalheLote.lote)) + ' Receita:' + ALLTRIM(STR(uCrsDetalheLote.nreceita))
					uf_gerais_registaOcorrencia('Lotes', 'Receita Eliminada', 2, lcOvalor, '', lcFtStamp)
					**********************
					
					** ACTUALIZAR GRELHA **
					uf_receituario_actualiza()
				ENDIF
				
						
			
			Endif
		Endif
	EndIf

	uf_receituario_actualiza()
Endfunc


**
FUNCTION uf_receituario_editarReceita
	
	IF !uf_gerais_validaPermUser(ch_userno, ch_grupo, 'Gest�o de Receitu�rio - Alterar contador')
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ALTERAR CONTADORES DE RECEITAS.","OK","",48)
		Return .F.
	Endif
	
	If Used("uCrsDetalheLote")
		Select uCrsDetalheLote
		If reccount("uCrsDetalheLote")>0
			If uCrsDetalheLote.Facturado=.t.
				uf_perguntalt_chama("N�O PODE ALTERAR UM REGISTO FACTURADO.","OK","",64)
				Return
			Endif
			If uCrsDetalheLote.fechado=.t.
				uf_perguntalt_chama("N�O PODE ALTERAR UM REGISTO FECHADO.","OK","",64)
				Return
			Endif
			
			uf_receituario_sombra(.t.)
			SELECT uCrsDetalheLote
			uf_corrigirreceita_chama(ALLTRIM(uCrsDetalheLote.u_ltstamp), ALLTRIM(uCrsDetalheLote.u_ltstamp2), uCrsDetalheLote.usrdata, .t.)
			uf_receituario_sombra(.f.)
			uf_receituario_actualiza()
		Endif
	EndIf
ENDFUNC


**
FUNCTION uf_receituario_chamaImprimirReceitasSeguida
	uf_receituario_sombra(.t.)

	uf_impressaoRVR_chama(.f., .f., "RECEITAS")

	uf_receituario_sombra(.F.)
ENDFUNC


**
FUNCTION uf_receituario_sair

	**
	receituario.cbentidade.controlsource 	= ''
	receituario.cbentidade.rowsource 		= ''
	receituario.cbDia.controlsource 		= ''
	receituario.cbPosto.controlsource 		= ''
	
	
	**
	IF USED("uCrsEntidadesReceituario")
		fecha("uCrsEntidadesReceituario")
	ENDIF
	IF USED("uCrsEntidadesReceituarioTemp")
		fecha("uCrsEntidadesReceituarioTemp")
	ENDIF
	
	IF USED("uCrsCloseLotes")
		fecha("uCrsCloseLotes")
	ENDIF
	IF USED("uCrsDetalheLote")
		fecha("uCrsDetalheLote")
	ENDIF
	
	IF USED("uCrsVisualRecImport")
		fecha("uCrsVisualRecImport")
	ENDIF
	
	RECEITUARIO.hide
	RECEITUARIO.release
ENDFUNC


**
FUNCTION uf_receituario_gravar
ENDFUNC



FUNCTION uf_atendimento_retornaDataPrescLinhaDem()
	LOCAL lcData, lcIdValidacaoDem 
	lcData='19000101'

	IF(!USED("FI") OR !USED("uCrsVerificaRespostaDEM") )
		RETURN lcData
	ENDIF	

	IF(EMPTY(fi.dem))
		RETURN lcData
	ENDIF
	
	SELECT fi
	lcIdValidacaoDem = ALLTRIM(fi.id_validacaodem)
	
	IF(!EMPTY(lcIdValidacaoDem))
	
		SELECT uCrsVerificaRespostaDEM
		GO TOP
		LOCATE FOR ALLTRIM(uCrsVerificaRespostaDEM.id) == lcIdValidacaoDem 
		IF FOUND()
			lcData = uf_gerais_getdate(uCrsVerificaRespostaDEM.receita_data,"SQL")
		ENDIF
	
	ENDIF	
			
	RETURN lcData
ENDFUNC


FUNCTION uf_receituario_CombinadosValidaSeDeveComparticipar
	LPARAMETERS lcCodigo,lcRef,lcAuxDem
	
	LOCAL lcValidaRemoveComp
	lcValidaRemoveComp = .f.
	
	** so corrige comparticipa��o se for manual
	IF(!EMPTY(lcAuxDem))
		RETURN .f.
	ENDIF
	
	if(USED("ucrsValidaRemoveComp"))
		fecha("ucrsValidaRemoveComp")		
	ENDIF
	
		
	** retorna de deve retirar a comparticipa��o
	TEXT TO lcSql TEXTMERGE NOSHOW
		exec up_receituario_removeComparticipacao '<<ALLTRIM(lcCodigo)>>',  '<<ALLTRIM(lcRef)>>'
	ENDTEXT

	uf_gerais_actgrelha("", [ucrsValidaRemoveComp], lcSql)
		
	IF Reccount([ucrsValidaRemoveComp])==0
		lcValidaRemoveComp = .f.
	ELSE
		IF (ucrsValidaRemoveComp.pct > 0)
			lcValidaRemoveComp = .t.
		ELSE
			lcValidaRemoveComp = .f.
		ENDIF
					
	ENDIF
	
			
	if(USED("ucrsValidaRemoveComp"))
		fecha("ucrsValidaRemoveComp")		
	ENDIF
	
	RETURN lcValidaRemoveComp 
	

ENDFUNC



**valida se dem deve ser comparticipada
FUNCTION uf_receituario_validaSeDemComparticipavel
	LPARAMETERS lcIdDispensa, lcDem, lcExcecao, lcRef, lcCodigo
	
	LOCAL lcValidaSeCompart, lcPosFiTemp
	lcValidaSeCompart= .t.
	
	SELECT fi
    lcPosFiTemp = RECNO("fi")

	
	**se for manual ou cabecalho
	IF EMPTY(lcDem) OR EMPTY(lcIdDispensa) OR EMPTY(lcRef) OR EMPTY(lcCodigo)
		RETURN lcValidaSeCompart
	ENDIF
		
	fecha("ucrsValidaSeCompart")	
	
		** retorna de deve retirar a comparticipa��o
	TEXT TO lcSql TEXTMERGE NOSHOW
		exec up_receituario_validaSeDemComparticipavel  '<<ALLTRIM(lcCodigo)>>',  '<<ALLTRIM(lcRef)>>', '<<ALLTRIM(lcExcecao)>>', '<<ALLTRIM(lcIdDispensa)>>'
	ENDTEXT
	
	
	if(uf_gerais_actgrelha("", [ucrsValidaSeCompart], lcSql))
	
		IF(RECCOUNT("ucrsValidaSeCompart")>0)		
			SELECT ucrsValidaSeCompart
			GO TOP
			lcValidaSeCompart = ucrsValidaSeCompart.compart			
		ELSE
			fecha("ucrsValidaSeCompart")
			lcValidaSeCompart = .t.
		ENDIF
		
	ELSE
		lcValidaSeCompart = .t.
	ENDIF
	
	

	fecha("ucrsValidaSeCompart")
	
	
	SELECT fi
    TRY
       GOTO lcPosFiTemp 
    CATCH
    ENDTRY	
	
	return lcValidaSeCompart		
		
	
ENDFUNC

FUNCTION uf_receitario_deveUsarNovaCompartMulticareMC6ouMC7

	LOCAL lcSql, lcValidaSeCompart  
	lcValidaSeCompart  = .f.
	
	fecha("ucrsValidaSeCompartMulticareTemp")
	
	TEXT TO lcSql TEXTMERGE NOSHOW
		exec up_receituario_validaSeDemComparticipavelMulticareMC6ouMC7
	ENDTEXT
	
	
	if(uf_gerais_actgrelha("", [ucrsValidaSeCompartMulticareTemp], lcSql))
	
		IF(RECCOUNT("ucrsValidaSeCompartMulticareTemp")>0)		
			SELECT ucrsValidaSeCompartMulticareTemp
			GO TOP
			lcValidaSeCompart = ucrsValidaSeCompartMulticareTemp.resultado			
		ELSE
			lcValidaSeCompart = .f.
		ENDIF
		
	ELSE
		lcValidaSeCompart = .f.
	ENDIF
	
	
	fecha("ucrsValidaSeCompartMulticareTemp")
	
	RETURN lcValidaSeCompart

ENDFUNC



FUNCTION uf_receitario_especiaisMulticareMC5
	LPARAMETERS lcPlano
	
	
	if(EMPTY(lcPlano))
		RETURN .f.
	ENDIF
	

		
	**especiais:	
	DO CASE
		
			
		CASE ALLTRIM(Upper(lcPlano)) == "XO0"
			RETURN .t.
		
		CASE ALLTRIM(Upper(lcPlano)) == "XP1"
			RETURN .t.
			
		CASE ALLTRIM(Upper(lcPlano)) == "XQ0"
			RETURN .t.
			
		CASE ALLTRIM(Upper(lcPlano)) == "XR1"
			RETURN .t.
		
		CASE ALLTRIM(Upper(lcPlano)) == "XS0"
			RETURN .t.


		OTHERWISE
			RETURN .f.


	ENDCASE
	
	


ENDFUNC




FUNCTION uf_receitario_especiaisMulticareMC6ouMC7
	LPARAMETERS lcPlano
	
	
	if(EMPTY(lcPlano))
		RETURN .f.
	ENDIF
	
	
	**especiais:	
	DO CASE
		
			
		CASE ALLTRIM(Upper(lcPlano)) == "XY0"
			RETURN .t.
		
		CASE ALLTRIM(Upper(lcPlano)) == "XY1"
			RETURN .t.
			
		CASE ALLTRIM(Upper(lcPlano)) == "XY3"
			RETURN .t.
			
		CASE ALLTRIM(Upper(lcPlano)) == "XY4"
			RETURN .t.
		
		CASE ALLTRIM(Upper(lcPlano)) == "XY5"
			RETURN .t.
												
		CASE ALLTRIM(Upper(lcPlano)) == "ZU0"
			RETURN .t.
		
		CASE ALLTRIM(Upper(lcPlano)) == "ZU1"
			RETURN .t.
			
		CASE ALLTRIM(Upper(lcPlano)) == "ZU2"
			RETURN .t.
			
		CASE ALLTRIM(Upper(lcPlano)) == "ZV1"
			RETURN .t.

		OTHERWISE
			RETURN .f.


	ENDCASE
	
	


ENDFUNC


FUNCTION uf_receitario_especiaisCGD
	LPARAMETERS lcAbrev, lcPlanoDescr
		
	
	IF(EMPTY(lcPlanoDescr) OR EMPTY(lcAbrev))
		RETURN .f.
	ENDIF
	
	IF(ALLTRIM(lcAbrev)!='CGD')
		RETURN .f.
	ENDIF
	
	
	IF(LEFT(lcPlanoDescr,5)!='SSCGD')
		RETURN .f.
	ENDIF
			
	RETURN .t.

ENDFUNC


FUNCTION uf_receitario_especiaisMulticareMC5ouMC6ouMC7
	LPARAMETERS lcPlano
	
	
	if(EMPTY(lcPlano))
		RETURN .f.
	ENDIF
	
	
	**especiais:	
	DO CASE
		
			
		CASE ALLTRIM(Upper(lcPlano)) == "XY0"
			RETURN .t.
		
		CASE ALLTRIM(Upper(lcPlano)) == "XY1"
			RETURN .t.
			
		CASE ALLTRIM(Upper(lcPlano)) == "XY3"
			RETURN .t.
			
		CASE ALLTRIM(Upper(lcPlano)) == "XY4"
			RETURN .t.
		
		CASE ALLTRIM(Upper(lcPlano)) == "XY5"
			RETURN .t.
												
		CASE ALLTRIM(Upper(lcPlano)) == "ZU0"
			RETURN .t.
		
		CASE ALLTRIM(Upper(lcPlano)) == "ZU1"
			RETURN .t.
			
		CASE ALLTRIM(Upper(lcPlano)) == "ZU2"
			RETURN .t.
			
		CASE ALLTRIM(Upper(lcPlano)) == "ZV1"
			RETURN .t.
			
						
		CASE ALLTRIM(Upper(lcPlano)) == "XO0"
			RETURN .t.
		
		CASE ALLTRIM(Upper(lcPlano)) == "XP1"
			RETURN .t.
			
		CASE ALLTRIM(Upper(lcPlano)) == "XQ0"
			RETURN .t.
			
		CASE ALLTRIM(Upper(lcPlano)) == "XR1"
			RETURN .t.
		
		CASE ALLTRIM(Upper(lcPlano)) == "XS0"
			RETURN .t.


		OTHERWISE
			RETURN .f.


	ENDCASE
	
	


ENDFUNC

FUNCTION uf_receituario_validaSeEntidadeComparticipavel
	LPARAM lcCodigo
	
	LOCAL lbComparticipavel
	lbComparticipavel = .t.
	
	fecha("uscrsEntComp")
	
	if(!EMPTY(lcCodigo))
		TEXT TO lcSql TEXTMERGE NOSHOW
			exec up_receituario_getEntidadePorPlano '<<ALLTRIM(lcCodigo)>>'
		ENDTEXT
		uf_gerais_actgrelha("", "uscrsEntComp", lcSql)
		IF(EMPTY(uscrsEntComp.permiteCalcCompart))
			lbComparticipavel  = .f.
		ENDIF
		
	ENDIF
	fecha("uscrsEntComp")
	
	return lbComparticipavel 

ENDFUNC

FUNCTION uf_receituario_utenteDiplomaBASPorBonusId
	LPARAMETERS lcTipo
	
	RETURN ALLTRIM(uf_gerais_getUmValor("cptvalBonus ", " idExt ", " tipo= '" + ALLTRIM(lcTipo) + "'"))	

ENDFUNC
FUNCTION uf_receituario_utenteBonusDescr
	LPARAMETERS lcTipo
	
	RETURN ALLTRIM(uf_gerais_getUmValor("cptvalBonus ", " descr ", " tipo= '" + ALLTRIM(lcTipo) + "'"))	

ENDFUNC


FUNCTION uf_receituario_utenteBonusViaRNU
	


	LOCAL lcCodeAsList, lcBonusId
	lcCodeAsList = "" 
	lcBonusId = ""
	

	
    * Atualiza o cursor atendcl
    IF USED("ucrsAtendCl") AND TYPE("ucrsAtendCl.recmId") <> "U"
        SELECT ucrsAtendCl
        GO TOP
        lcCodeAsList  = ALLTRIM(ucrsAtendCl.recmId)
    ENDIF
    


    IF(!EMPTY(ALLTRIM(lcCodeAsList)))
    	

    	
    	fecha("ucrsUtenteRecms")
		CREATE CURSOR ucrsUtenteRecms (recmId C(50)) && Cria um cursor com um campo de texto


		
		LOCAL aValues
		DIMENSION aValues[1]

		* Converte a string lcCodeAsList em um array separado por v�rgulas
		ALINES(aValues, STRTRAN(lcCodeAsList, ",", CHR(13)))

		* Itera pelo array para inserir os valores no cursor
		FOR EACH lcValue IN aValues
		    IF !EMPTY(ALLTRIM(lcValue))  && Garante que n�o insere valores vazios
		        INSERT INTO ucrsUtenteRecms (recmId) VALUES (ALLTRIM(lcValue))
		    ENDIF
		ENDFOR
		

		RELEASE aValues
		
	
		if(USED("ucrsUtenteRecms"))
		
			SELECT ucrsUtenteRecms
			GO TOP
			SCAN
				LOCAL lcRecmId
				lcRecmId =ALLTRIM(ucrsUtenteRecms.recmId)
				DO CASE			
			        CASE uf_gerais_compStr(lcRecmId , "5003")
			            lcBonusId = "2" 
			            EXIT
			        CASE uf_gerais_compStr(lcRecmId , "5004")
			            lcBonusId = "3"
			            EXIT 
			        OTHERWISE
			            lcBonusId = "" 
		    	ENDCASE				
			ENDSCAN
			
		ENDIF
		
		
		fecha("ucrsUtenteRecms")

    
    ENDIF
    

    
    
	RETURN lcBonusId 

ENDFUNC





** Fun��es dos Lotes
** Nota: tcMVR - chamado a partir do painel de manipula��o de valores de receita
** uf_receituario_MakePrecos("01","5440987","5440987","fi","",.f.,.f., .f.,"1","99)
FUNCTION uf_receituario_MakePrecos
	LPARAMETERS tcCodigo, tcRef, tcCnp, tcTabLin, tcDiploma, lcFront, tcMVR, tcCorrecaoAutomatica, tcTipoBonusId, tcIdExt
	LOCAL lcTipoDescontoFinal, lcTemBonus, lcRecalcCompartSNS
	
	lcTemBonus = .f.
	lcRecalcCompartSNS = .t.
	
	
	**validacao se linha de entrar no fluxo de comparticipa��p
	IF EMPTY(uf_receituario_validaSeEntidadeComparticipavel(ALLTRIM(tcCodigo)))
		RETURN .f.
	ENDIF
	
	

	**Valida EAC (Estatuto Antigo Combatente), situa��o provisoria para receitas antes de 2025 sem diploma 95 ou 98
	
	if(EMPTY(tcTipoBonusId))
		
		tcTipoBonusId = uf_receituario_utenteBonusViaRNU()
		local lcDescrBonus 
		lcDescrBonus  = ''
		
		
		&&Diploma PSCI
		
		if(uf_gerais_compStr(tcDiploma, "18/2025/1-PSCI"))
			tcTipoBonusId = "5"
		ENDIF
		
		


		if(!EMPTY(tcTipoBonusId))
			tcIdExt = uf_receituario_utenteDiplomaBASPorBonusId(tcTipoBonusId)
			lcDescrBonus =  uf_receituario_utenteBonusDescr(tcTipoBonusId)
		ENDIF
		
		
				
		**marca as linha como bonus
		if(USED("fi2") AND !EMPTY(tcTipoBonusId) AND !EMPTY(tcIdExt))
			LOCAL lcFistampAux
	
			SELECT (tcTabLin)
			lcFistampAux =  &tcTabLin..fistamp
			

			UPDATE fi2 SET fi2.bonusId = ALLTRIM(tcTipoBonusId), fi2.bonusDescr=ALLTRIM(lcDescrBonus) WHERE fi2.fistamp = lcFistampAux
		ENDIF		
		
	ENDIF
	
	

	
	&&se aplica Comparticipacao suplementar � final
	if(EMPTY(tcTipoBonusId))
		lcTipoDescontoFinal = ""
	ELSE
		uf_gerais_actgrelha("", [uCrsCptComplTest], [exec up_gerais_compartCompl '] + ALLTRIM(tcCodigo) + ['])
        IF !uf_gerais_permiteBAS(tcCodigo)
            lcTipoDescontoFinal = ""
        ELSE
            lcTipoDescontoFinal = ALLTRIM(tcTipoBonusId)
        ENDIF
	ENDIF
	

	
	
	fecha("uCrsCptComplTest")
	&&comparticipa bonus se existir
	fecha("ucrsCompartBonus")
	if(!EMPTY(lcTipoDescontoFinal))
		TEXT TO lcSql TEXTMERGE NOSHOW
			exec up_receituario_CalcCompartBonus '<<ALLTRIM(lcTipoDescontoFinal)>>', '<<ALLTRIM(tcIdExt)>>', '<<ALLTRIM(tcCodigo)>>', '<<ALLTRIM(tcRef)>>'
		ENDTEXT
		uf_gerais_actgrelha("", "ucrsCompartBonus", lcSql)
		
		if(RECCOUNT("ucrsCompartBonus")>0)
			lcTemBonus = .t.
		ENDIF
	ENDIF
	
	SELECT fi
	IF !EMPTY(ALLTRIM(fi.bistamp)) AND !EMPTY(ALLTRIM(fi.receita))
		TEXT TO lcSql TEXTMERGE NOSHOW
			select e_compart from cptpla(nolock) where codigo='<<ALLTRIM(tcCodigo)>>' AND inactivo = 0
		ENDTEXT
		uf_gerais_actgrelha("", "ucrsplanores", lcSql)
		**IF ucrsplanores.e_compart = .t.
			LOCAL lcResult
			lcResult = uf_receituario_aplica_valores_receita(ALLTRIM(fi.receita), ALLTRIM(fi.bistamp), ALLTRIM(fi.ref))
			fecha("ucrsplanores")
			&&se encontrou receita, n�o calcula a comparticipa��o
			if(lcResult==.t.)				
				RETURN .f.
			ENDIF
				
		**ENDIF 
		fecha("ucrsplanores")
	ENDIF 
	

	
	LOCAL tcMvCompartPercent, tcMvCompartPercent2, tcMvPvp, tcMvPref, tcMvData, tcDataPrescricaoLinhaReceita
	LOCAL lcDesignAux, lcExcecaoAux, lcAuxDEM, lcAuxDiploma, lcRefAux, lcIdDispensa, lcGroupProd, lcTokenDem, lnQtt
	LOCAL lcMaxDataPrescricaoLinhaReceita
	STORE '' TO lcDesignAux, lcExcecaoAux, lcAuxDiploma, lcRefAux, lcIdDispensa,lcGroupProd, lcTokenDem
	lnQtt = 0
	
	lcMaxDataPrescricaoLinhaReceita='20170718'

	SELECT fi
	lcDesignAux 	= ALLTRIM(fi.design)
	lcExcecaoAux 	= ALLTRIM(fi.lobs3)
	lcAuxDem 		= IIF(!EMPTY(fi.token), .t., .f.)
	lcIdDispensa	= IIF(!EMPTY(fi.token), ALLTRIM(fi.id_validacaodem), '')
	lcRefAux 		= ALLTRIM(fi.ref)
	lcTokenDem      = ALLTRIM(fi.token)
	lnQtt           = fi.qtt
	 

	
	**validacao se linha da dem deve ser compaticipada
	IF(EMPTY(uf_receituario_validaSeDemComparticipavel(lcIdDispensa,lcAuxDem, lcExcecaoAux,lcRefAux,ALLTRIM(tcCodigo))))
		lcGroupProd  = 'ST'
	ENDIF


	** valida se deve comparticipar nos casos combinados 01 com OST/Manipulados/Camaras Expansoras
	** So comparticipa se for DEM
	** Se for receita manual n�o deve comparticipar a linha
*!*		IF(!EMPTY(uf_receituario_CombinadosValidaSeDeveComparticipar(ALLTRIM(tcCodigo),tcRef,lcAuxDem)))
*!*			return .f.
*!*		ENDIF
	

	IF (tcMVR) && Painel MVR
		IF !(tcCorrecaoAutomatica) && Painel MVR
			tcMvCompartPercent = astr(MVR.txtcomp.value)
			tcMvCompartPercent2 = astr(MVR.txtcomp2.value)
			tcMvPvp	= MVR.txtPVP.value
			tcMvPref= MVR.txtpref.value
			SELECT ft
			GO TOP
			tcMvData = uf_gerais_getDate(ft.cdata,"SQL")
		ELSE && correcao automatica de precos nas inser��es de receita na faturacao
			SELECT fi
			IF ALLTRIM(tcCodigo) == '48' AND fi.u_epvp == fi.pvpmaxre 
				tcMvCompartPercent = 95
				tcMvCompartPercent2 = 0
			ELSE
				tcMvCompartPercent = 0
				tcMvCompartPercent2 = 0
			ENDIF
			SELECT fi
			tcMvPvp	= fi.u_epvp   
			tcMvPref= fi.u_epref  
			SELECT ft
			GO TOP
			tcMvData = uf_gerais_getDate(ft.cdata,"SQL")		
		ENDIF
	ENDIF

	**LOCAL lcSql
	**STORE "" TO lcSql
	LOCAL lnPvp, lnBase1, lnBase2, lnPut, lnEnt1, lnEnt2, lnPvp4Fee, lnTotalPvpLinha, lcPref
	STORE 0 TO lnPvp, lnBase1, lnBase2, lnPut, lnEnt1, lnEnt2, lnPvp4Fee, lnTotalPvpLinha, lcPref
	LOCAL lcComp1, lcComp2 && Indica se h� complementaridade
	STORE .f. to lcTemVal1, lcTemVal2 && Indica se � comparticipado pela entidade 1 e/ou 2
	
	
	
	tcDataPrescricaoLinhaReceita = uf_atendimento_retornaDataPrescLinhaDem()

	&& Verificar se existe diploma na linha e o CFT lyrica e valida a data de presricao da linha de receita - 2017-07-17
	IF !EMPTY(tcDiploma)
		IF UPPER(ALLTRIM(tcDiploma)) == "CFT 2.10" AND tcDataPrescricaoLinhaReceita<lcMaxDataPrescricaoLinhaReceita AND tcDataPrescricaoLinhaReceita!="19000101"
			lcAuxDiploma = "CFT 2.10"
			tcDiploma='xxx'
		ELSE
			tcDiploma = ALLTRIM(tcDiploma)
		ENDIF
	ELSE
		tcDiploma='xxx'
	ENDIF

	&& Usa pre�o ST ou pre�o da Venda (caso n�o tcMVR)
	LOCAL choosePvp
	STORE 0 TO choosePvp
	Select (tcTabLin)
	IF &tcTabLin..amostra
		choosePvp = &tcTabLin..u_epvp
	ENDIF
	


	&& Criar cursor - valores para plano 1
	IF (!tcMVR) 
		TEXT TO lcSql TEXTMERGE NOSHOW
			exec up_receituario_CalcCompart '<<ALLTRIM(tcDiploma)>>', <<choosePvp>>, '<<ALLTRIM(tcRef)>>', '<<ALLTRIM(tcCodigo)>>', <<mysite_nr>> ,<<0>>, '<<ALLTRIM(lcGroupProd)>>'
		ENDTEXT
	ELSE && Painel M.V.
		TEXT TO lcSql TEXTMERGE NOSHOW
			exec up_receituario_CalcCompart '<<ALLTRIM(tcDiploma)>>', <<tcMvPvp>>, '<<ALLTRIM(tcRef)>>', '<<ALLTRIM(tcCodigo)>>', <<mysite_nr>>, <<tcMvCompartPercent >>, '<<ALLTRIM(lcGroupProd)>>'
		ENDTEXT
	ENDIF
	


	uf_gerais_actgrelha("", [uCrsCptVal], lcSql)
	
	IF USED("uCrsCptVal")
		IF RECCOUNT("uCrsCptVal") > 0
			lcTemVal1 = .t.
		ENDIF
	ENDIF

	
	&& Verificar se existe complementariedade
	uf_gerais_actgrelha("", [uCrsCptCompl], [select compl from cptpla (nolock) where codigo='] + ALLTRIM(tcCodigo) + ['])
	IF USED("uCrsCptCompl")
		SELECT uCrsCptCompl
		IF RECCOUNT([uCrsCptCompl])>0
			
			** Criar cursor - valores para plano 2
			IF (!tcMVR)
				TEXT TO lcSql NOSHOW TEXTMERGE
					exec up_receituario_CalcCompart '<<ALLTRIM(tcDiploma)>>', <<choosePvp>>, '<<ALLTRIM(tcRef)>>', '<<ALLTRIM(uCrsCptCompl.compl)>>', <<mysite_nr>>, <<0>>, '<<ALLTRIM(lcGroupProd)>>'
				ENDTEXT
			ELSE && Painel M.V.
				TEXT TO lcSql TEXTMERGE NOSHOW
					exec up_receituario_CalcCompart '<<ALLTRIM(tcDiploma)>>', <<tcMvPvp>>, '<<ALLTRIM(tcRef)>>', '<<ALLTRIM(uCrsCptCompl.compl)>>', <<mysite_nr>>, <<tcMvCompartPercent2>>, '<<ALLTRIM(lcGroupProd)>>'
				ENDTEXT
			ENDIF
			

			uf_gerais_actgrelha("", [uCrsCptVal2], lcSql)
			SELECT uCrsCptVal2
			If Reccount([uCrsCptVal2])>0
				lcTemVal2 = .t.
			ENDIF

		ENDIF
		fecha([uCrsCptCompl])
	ENDIF
	
	


	** Criar cursor - precos por tipo
	lcSQL = ''
	IF (!tcMVR)
		TEXT TO lcSQL NOSHOW TEXTMERGE
			exec up_receituario_getPRecos <<choosePvp>>, '<<ALLTRIM(tcRef)>>', <<mysite_nr>>
		ENDTEXT
	ELSE && Painel M.V.
		TEXT TO lcSql NOSHOW TEXTMERGE 
			exec up_receituario_getprecosIr <<tcMvPvp>>, <<tcMvPref>>, '<<Alltrim(tcRef)>>', <<mysite_nr>>
		ENDTEXT 
	ENDIF
	
	
	
	

	IF !uf_gerais_actgrelha("", [uCrsPrecos], lcSQL)
		uf_perguntalt_chama("Ocorreu uma anomalia a verificar os pre�os do produto: "+Chr(13)+lcSql,"OK","",16)
		RETURN .F.
	ENDIF
	IF Reccount([uCrsPrecos])==0
		*uf_perguntalt_chama("N�o encontrei o Produto '"+Alltrim(uCrsPrecos.cnp)+"'","OK","",48)
		RETURN .F.
	ENDIF
	

	
	** garantir o uso dos pre�os certo
	SELECT uCrsPrecos
	GO TOP
	lnPvp	= IIF(uCrsPrecos.pst1>0, uCrsPrecos.pst1, Nvl(uCrsPrecos.ppvp,0))
	lnPref  = uCrsPrecos.pref


	** Caso especial Lyrica, se excep��o C e CFT=2.10 mudar comparticipa��o - LL/HT 20160606 
	**  valida a data de presricao da linha de receita - 2017-07-17
	IF !lcAuxDem && dispensas manuais
		IF UPPER(lcDesignAux) = 'LYRICA'
			IF UPPER(lcExcecaoAux) = 'C' 
				uf_receituario_validaLyrica(tcRef, tcCodigo, lcTemVal2)
			ENDIF	
		ENDIF
		
	ELSE && dispensas eletr�nicas
	
		IF UPPER(lcDesignAux) = 'LYRICA' AND tcDataPrescricaoLinhaReceita<lcMaxDataPrescricaoLinhaReceita AND tcDataPrescricaoLinhaReceita!="19000101"
			IF UPPER(lcExcecaoAux) = 'C'  AND ALLTRIM(lcAuxDiploma) == "CFT 2.10"	
				uf_receituario_validaLyrica(tcRef, tcCodigo, lcTemVal2)
			ENDIF	
		ENDIF
	ENDIF
	
	&& se tem BAS sobre a comparticipacao, primeiro simula a compart do estado, para voltar a calcular

	
	

	** Calcular Base
	lnBase2	= IIF(lcTemVal2 , uf_receituario_GetBase(uCrsCptVal2.grppreco), 0)
	lnBase1 = IIF(lcTemVal1 , uf_receituario_GetBase(uCrsCptVal.grppreco), 0)


	if(lcTemBonus AND LEN(lcIdDispensa)>19 and lcAuxDem)
	
		uf_receituario_validaCompartBonus(tcRef, tcCodigo, lcTemVal2, lcTemBonus, lcTipoDescontoFinal, tcIdExt, .t., lnBase1, lnBase2, lnPvp, lcIdDispensa, lcTokenDem, lnPref)     
		lnBase1 = IIF(lcTemVal1 , uf_receituario_GetBase(uCrsCptVal.grppreco), 0)
		lcRecalcCompartSNS = .f.
	ENDIF
	
	 **Se receita eletronica com id de linha ir buscar o valor da comparticipacao
    **BAS n�o entra
    if(lcAuxDem AND !EMPTY(lcIdDispensa) AND LEN(lcIdDispensa)>19 AND uf_gerais_getParameter_site('ADM0000000230', 'BOOL', mySite) AND !lcTemBonus)
        uf_receituario_validaCompartDEM(tcRef, tcCodigo, lcTemVal2, tcIdExt, lnPvp, lcIdDispensa, lcTokenDem, lnPref)
        lcRecalcCompartSNS = .f.
    ENDIF
*!*		


	
	** Se troca de productos para quantidades maior que validar compartipa��o directamente

*!*		if(!lcTemBonus and lnQtt>1 and lcAuxDem)
*!*		
*!*			LOCAL lcPagoEntidadeSns
*!*			lcPagoEntidadeSns = 0
*!*			
*!*			
*!*			lcPagoEntidadeSns = uf_receituario_devolver_percentagem_compart_dem(lcIdDispensa,lcTokenDem,0,lnPvp,lnPref)
*!*			
*!*			if(!EMPTY(lcTemVal2))
*!*				SELECT  uCrsCptval2
*!*				GO TOP
*!*				replace  percentagem with lcPagoEntidadeSns
*!*				replace  grppreco with  "VAL" 
*!*			ELSE
*!*				SELECT  uCrsCptval
*!*				GO TOP
*!*				replace  percentagem with lcPagoEntidadeSns
*!*				replace  grppreco with  "VAL"   			
*!*			ENDIF
*!*		   
*!*			lnBase1 = IIF(lcTemVal1 , uf_receituario_GetBase(uCrsCptVal.grppreco), 0)
*!*			lcRecalcCompartSNS = .f.
*!*		
*!*		ENDIF
*!*		
	

		

	** caso especial SAMS - usar pvp em vez de pref (usar o preco mais baixo sempre quando existe complementariedade)
	IF lcTemVal2 && garantir que existe complementariedade
		SELECT uCrsCptVal
		GO TOP
		IF (ALLTRIM(uCrsCptVal.abrev) == 'SBSI' OR ALLTRIM(uCrsCptVal.abrev) == 'SBN' OR ALLTRIM(uCrsCptVal.abrev) == 'SBC') &&AND ALLTRIM(uCrsCptVal.compl) == '01'
			IF !ISNULL(uCrsPrecos.pref)
				lnBase1 = IIF(uCrsPrecos.pref < uCrsPrecos.pst1, uCrsPrecos.pref, uCrsPrecos.pst1)
			ENDIF
		ENDIF
	ENDIF

	** fail safe, posiciona cursores
	IF USED("uCrsCptVal2")
		SELECT uCrsCptVal2
		GO TOP 
	ENDIF
	SELECT uCrsCptVal
	GO TOP 
	
	



	If !lcTemVal2

		DO CASE
			CASE ALLTRIM(Upper(uCrsCptval.grppreco)) == "VAL"
				
			
				select uCrsCptVal
				&& calcular o total da linha, se for negativo a comparticipa��o = pvp
				

				
				
				lnTotalPvpLinha = Round(lnBase1 - uCrsCptVal.percentagem, 2)
				if(lnTotalPvpLinha<0)
					lnTotalPvpLinha  = lnPvp 
				ENDIF
				


				lnEnt1 = Iif(lnPvp >= lnTotalPvpLinha , Round(uCrsCptVal.percentagem, 2), lnPvp )
				
							
				
				lnPut = lnPvp - lnEnt1
		

			CASE ALLTRIM(Upper(uCrsCptval.grppreco)) == "VALF" && o valor FINAL a pagar pelo utente, independentemente do pre�o do medicamento
				lnEnt1 = IIF(Round(uCrsCptVal.percentagem, 2) >= lnPVP, lnPVP, lnPvp - Round(uCrsCptVal.percentagem, 2))
				
				
				lnPut = IIF(Round(uCrsCptVal.percentagem, 2) >= lnPVP, 0, Round(uCrsCptVal.percentagem, 2))
				

			OTHERWISE
				
				** alterei a regra aqui, por causa dos valores negativos que v�m do ppens>pvp
				
			
				
				lnEnt1 = Iif(lnPvp >= Round(lnBase1*uCrsCptVal.percentagem/100,2), Round(lnBase1*uCrsCptVal.percentagem/100,2), lnPvp)
				
				lnPut = lnPvp - lnEnt1

		ENDCASE
		

		
		** calculo fee farm�cia - s� sns
		IF (ALLTRIM(uCrsCptVal.abrev) == 'SNS')
			SELECT fi
			IF (fi.pvptop4 > 0)
				IF fi.u_epvp <= fi.pvptop4
				**IF (fi.u_generico == .t. AND fi.u_epvp <= fi.pvptop4) OR (fi.u_generico == .f. AND fi.pic <= fi.pvptop4)
					lnPvp4Fee = 0.35 * fi.qtt
				ENDIF						
			ENDIF
		ENDIF
		

		
		&&� 
	ELSE && com complementariedade
		
		
	
		
		DO CASE
			CASE ALLTRIM(Upper(uCrsCptval.grppreco)) == "VAL"
				*determinas o valor a comparticipar pela entidade 2 (SNS ou ADSE) com ou sem bonus
				
		
				if(!EMPTY(lcRecalcCompartSNS))
					lnEnt2	= iif(Round(lnBase2 * uCrsCptval2.percentagem/100,2)>lnPvp, lnPvp, Round(lnBase2 * uCrsCptval2.percentagem/100,2))
				ELSE
					lnEnt2	= uf_receituario_calcValorEntidadeSnsComBAS(uCrsCptval2.percentagem,lnPvp)
				ENDIF


				*determinas o valor a comparticipar pela entidade 1 (complementar)
				lnEnt1	= iif(Round(lnBase1 - uCrsCptval.percentagem, 2) > lnPvp, lnPvp, Round(uCrsCptval.percentagem, 2))
					*				lnPut	= lnPvp - Max(lnEnt2,lnEnt1)
					*				lnEnt1	= Max(lnPvp-lnPut-lnEnt2, 0)
				* valor a pagar pelo utente
				
		
				
				
				**No caso da comparticipa��o do estado + entidade for maior que o pvp, entao ajustamos o valor da entidade para o cliente pagar 0
				lnEnt1	= IIF(lnPvp - lnEnt2 - lnEnt1 >= 0, lnEnt1, lnPvp - lnEnt2)
				

				
			
				*No caso da entidade principal comparticipar tudo a entidade complementar nao  comparticipa
				lnPut	= IIF(lnPvp - lnEnt2 - lnEnt1 >= 0, lnPvp - lnEnt2 - lnEnt1, lnPvp - lnEnt2)
					
		
				
				**antigo
				**lnPut	= IIF(lnPvp - lnEnt2 - lnEnt1 >= 0, lnPvp - lnEnt2 - lnEnt1, lnPvp - lnEnt2)
				**lnEnt1	= IIF(lnPvp - lnEnt2 - lnEnt1 >= 0, lnEnt1, 0)

				
				


			CASE ALLTRIM(Upper(uCrsCptval.grppreco)) == "VALF" && o valor FINAL a pagar pelo utente, independentemente do pre�o do medicamento
				* determina o valor a comparticipar pela entidade 2 (SNS) com ou sem bonus
				
				
				if(!EMPTY(lcRecalcCompartSNS))
					lnEnt2	= IIF(Round(lnBase2 * uCrsCptval2.percentagem/100,2)>lnPvp, lnPvp, Round(lnBase2 * uCrsCptval2.percentagem/100,2))
				ELSE
					lnEnt2	= uf_receituario_calcValorEntidadeSnsComBAS(uCrsCptval2.percentagem,lnPvp)
				ENDIF
				

				

				** valor a pagar pelo utente
*!*					IF uCrsCptVal.percentagem == 0
*!*						lnPut = lnPVP - lnEnt2
*!*						lnEnt1 = 0
*!*					ELSE
					lnPut = IIF(Round(uCrsCptVal.percentagem, 2) >= lnPVP, 0, Round(uCrsCptVal.percentagem, 2))
					lnEnt1 = lnPVP - lnEnt2 - lnPut
*!*					ENDIF

			CASE ALLTRIM(Upper(uCrsCptval.grppreco)) == "ESP" && Multicare
				
				LOCAL lnValorEstado 
				lnValorEstado = Round(uCrsCptVal2.percentagem,2)
				
				
		
	
				if(!EMPTY(lcRecalcCompartSNS))			
					lnPut = Round(IIF(lnPvp - Round(lnBase2 * uCrsCptVal2.percentagem/100,2)>0, lnPvp - Round(lnBase2 * uCrsCptVal2.percentagem/100,2), 0) * (1-uCrsCptVal.percentagem/100),2)
				ELSE
					lnPut = Round(IIF(lnPvp - lnValorEstado > 0, lnPvp - lnValorEstado, 0) * (1-uCrsCptVal.percentagem/100),2)
				ENDIF
					
			

				** alterei a regra aqui, por causa dos valores negativos que v�m do ppens>pvp
				if(!EMPTY(lcRecalcCompartSNS))	
					lnEnt2 = IIF(lnPvp > Round(lnBase2 * uCrsCptVal2.percentagem/100, 2), Round(lnBase2 * uCrsCptVal2.percentagem/100, 2), lnPvp)
				ELSE
					lnEnt2	= uf_receituario_calcValorEntidadeSnsComBAS(uCrsCptval2.percentagem,lnPvp)
				ENDIF


				
				** no caso da APDL e APL: APDL  e APL comparticipam 50% do remanescente do Pref ou PVP, caso o medicamento n�o tenha Pref, n�o comparticipado pelo SNS.
				**  no caso da multicare MC6 e MC7 : 70% do remanescente do pre�o de refer�ncia n�o comparticipado pelo SNS. Apenas para Mc6 ou MC7
				**  no caso da multicare MC5 : 90% do remanescente do pre�o de refer�ncia n�o comparticipado pelo SNS. Apenas para Mc5
	
				IF (ALLTRIM(uCrsCptVal.abrev) == 'APDL' OR ALLTRIM(uCrsCptVal.abrev) == 'APL' OR uf_receitario_especiaisMulticareMC5ouMC6ouMC7(uCrsCptVal.cptplacode)) 
								
					
					lnEnt1 =  MAX(IIF(lnBase2>0, ROUND((lnBase2- lnEnt2) * uCrsCptVal.percentagem/100,2), ROUND((lnPvp - lnEnt2) * uCrsCptVal.percentagem/100,2)), 0)
					
		
					**Ajusta a valor da complementar, com a diferen�a entre o pvp e estado caso a comparticipa��o seja maior que o pvp
					IF(lnPvp-(lnEnt1+lnEnt2) < 0)
						lnEnt1 = lnPvp - lnEnt2
					ENDIF
					
				
										
					lnPut =  lnPvp-(lnEnt1+lnEnt2)
					

				ELSE
					lnEnt1 = MAX(lnPvp - lnEnt2 - lnPut, 0)
				ENDIF
				

				*!*	�CGD
				*!*	����������Se�PVP�< P.Ref:
				*!*	o����S�cios de plenos direitos � 90% de comparticipa��o (90% da diferen�a entre o�PVP�e a comparticipa��o do SNS);
				*!*	o����Benefici�rios de plenos direitos � 80% de comparticipa��o (80% da diferen�a entre o�PVP�e a comparticipa��o do SNS).
				*!*	����������Se PVP >�P.Ref:
				*!*	�
				*!*	S�cios de plenos direitos � 90% de comparticipa��o (90% da diferen�a entre o�pre�o de refer�ncia�e a comparticipa��o do SNS);Benefici�rios de plenos direitos � 80% de comparticipa��o (80% da diferen�a entre o�pre�o de refer�ncia�e a comparticipa��o do SNS).
				if(uf_receitario_especiaisCGD(ALLTRIM(uCrsCptVal.abrev),ALLTRIM(uCrsCptVal.design)))					
					lnEnt1 =  MAX(IIF(lnBase2>lnPvp , ROUND((lnPvp - lnEnt2) * uCrsCptVal.percentagem/100,2), ROUND((lnBase2- lnEnt2) * uCrsCptVal.percentagem/100,2)), 0)
					lnPut = IIF(lnPvp-(lnEnt1+lnEnt2) < 0, lnPvp-MAX(lnEnt2,lnEnt1), lnPvp-(lnEnt1+lnEnt2))
				ENDIF
				


			OTHERWISE
			
				

			
				if(!EMPTY(lcRecalcCompartSNS))	
					lnEnt2 = iif(Round(lnBase2 * uCrsCptval2.percentagem/100, 2)>lnPvp, lnPvp, Round(lnBase2 * uCrsCptval2.percentagem/100,2))
				ELSE
					lnEnt2	= uf_receituario_calcValorEntidadeSnsComBAS(uCrsCptval2.percentagem,lnPvp)
				ENDIF
				

				
				

				** Casos em que a entidade complementar comparticipa x% - y%sns
				** SBSI / SBN / SBC (90%-%sns)
				** && No caso da CML a comparticipa��o � feita sobre o pref/pvp e n�o sobre o remanescente do SNS 			
				**
		
				IF (ALLTRIM(uCrsCptVal.abrev) == 'SBSI';
					OR ALLTRIM(uCrsCptVal.abrev) == 'SBN';
					OR ALLTRIM(uCrsCptVal.abrev) == 'SBC';
					OR ALLTRIM(uCrsCptVal.abrev) == 'SNQTB';
					OR ALLTRIM(uCrsCptVal.abrev) == 'CML/SS';
					OR ALLTRIM(uCrsCptVal.abrev) == 'PT/CTT';
					OR ALLTRIM(uCrsCptVal.abrev) == 'CGD';
					OR ALLTRIM(uCrsCptVal.abrev) == 'MEDIS';
					OR ALLTRIM(uCrsCptVal.abrev) == 'MEDIS/CTT';
					)					
					** calcular comparticipa��o da entidade complementar e subtrair � do SNS
	
					
					
					lnEnt1 = iif(Round(lnBase1 * uCrsCptval.percentagem/100, 2)>lnPvp, lnPvp, Round(lnBase1 * uCrsCptval.percentagem/100,2))
					

					

					&& para todas as que n�o s�o CML
					&& No caso da CML a comparticipa��o � feita sobre o pref/pvp e n�o sobre o remanescente do SNS
					IF(!ALLTRIM(uCrsCptVal.abrev) == 'CML/SS')
						lnEnt1 = IIF(lnEnt1 - lnEnt2 < 0, 0, lnEnt1 - lnEnt2)
					ENDIF
					

*!*						IF (uCrsCptval.percentagem - uCrsCptval2.percentagem < 0)
*!*							lnEnt1 = 0
*!*						ELSE
*!*							lnEnt1 = iif(Round(lnBase1 * (uCrsCptval.percentagem - uCrsCptval2.percentagem)/100, 2)>lnPvp, lnPvp, Round(lnBase1 * (uCrsCptval.percentagem - uCrsCptval2.percentagem)/100,2))
*!*						ENDIF
				ELSE
		
					lnEnt1 = iif(Round(lnBase1 * uCrsCptval.percentagem/100,2)>lnPvp, lnPvp, Round(lnBase1 * uCrsCptval.percentagem/100,2))

					** caso especial SCML e SCML-ES, caso o pvp <= pref do grupo homog�nio o utente n�o paga nada, caso o pvp > pref o utente paga a diferen�a entre o pvp e o pref
					IF (ALLTRIM(uCrsCptVal.abrev) == 'SCML' OR ALLTRIM(uCrsCptVal.abrev) == 'SCML-ES')		
						IF (!ISNULL(uCrsPrecos.pref) AND lnPvp > uCrsPrecos.pref)
							lnEnt1 = MAX(lnEnt1 - lnEnt2 - (lnPvp-uCrsPrecos.pref), 0)
						ENDIF
					ENDIF
				ENDIF

				** Casos em que a comparticipa��o das duas entidades � > pvp, e que a entidade complementar comparticipa sempre o remanescente
				IF (ALLTRIM(uCrsCptVal.abrev) == 'SBSI';
					OR ALLTRIM(uCrsCptVal.abrev) == 'SBN';
					OR ALLTRIM(uCrsCptVal.abrev) == 'SBC';
					OR ALLTRIM(uCrsCptVal.abrev) == 'SNQTB';
					OR ALLTRIM(uCrsCptVal.abrev) == 'SAVIDA';
					OR ALLTRIM(uCrsCptVal.abrev) == 'PT/CTT';
					OR ALLTRIM(uCrsCptVal.abrev) == 'CGD';
					OR ALLTRIM(uCrsCptVal.abrev) == 'MEDIS';					
					OR ALLTRIM(uCrsCptVal.abrev) == 'MEDIS/CTT';
					)
					
					&&OR ALLTRIM(uCrsCptVal.abrev) == 'CML/SS';**					
					**lnPut = IIF(lnPvp-(lnEnt1+lnEnt2) < 0, 0, lnPvp-(lnEnt1+lnEnt2))
					lnPut = IIF(lnPvp-(lnEnt1+lnEnt2) < 0, IIF(lnPvp-lnEnt2<0,0,lnPvp-lnEnt2), lnPvp-(lnEnt1+lnEnt2))
				ELSE
					** utilizar as duas entidades ou apenas o mais alto se as duas forem > pvp			
					lnPut = IIF(lnPvp-(lnEnt1+lnEnt2) < 0, lnPvp-MAX(lnEnt2,lnEnt1), lnPvp-(lnEnt1+lnEnt2))
					
*!*						Ajustar a compart da entidade ao que sobra do estado
*!*						IF(lnPvp-(lnEnt1+lnEnt2) < 0)
*!*							lnEnt1 = lnPvp-lnEnt2
*!*						ENDIF				
*!*						lnPut = lnPvp-(lnEnt1+lnEnt2)
					
				ENDIF
				
	
				
				&& para todas as que s�o CML/SS
				&& VP5 = PVP e regime especial a CML n�o comparticipa
				lnEnt1 = Max(lnPvp-lnPut-lnEnt2, 0)
				
				
				IF(ALLTRIM(uCrsCptVal.abrev) == 'CML/SS'  )
					IF (ALLTRIM(tcCodigo) == '058' OR ALLTRIM(tcCodigo) == 'D51')AND fi.u_epvp <= fi.pvpmaxre			
						lnEnt1  = 0						
					ENDIF
				ENDIF
				
		ENDCASE

		
		** calculo fee farm�cia - s� sns
		IF (ALLTRIM(uCrsCptVal2.abrev) == 'SNS')
			SELECT fi
			IF (fi.pvptop4 > 0)
				IF  fi.u_epvp <= fi.pvptop4
				**IF (fi.u_generico == .t. AND fi.u_epvp <= fi.pvptop4) OR (fi.u_generico == .f. AND fi.pic <= fi.pvptop4)
					lnPvp4Fee = 0.35 * fi.qtt
				ENDIF						
			ENDIF
		ENDIF
	ENDIF
	
	
	

		
	** for�ar update de cursor
	TRY
		Select &tcTabLin
		GO RECNO()
	CATCH
	ENDTRY

    uv_compartManual = .F.
    


    IF ALLTRIM(UPPER(tcTabLin)) = "FI"
        uv_compartManual = uf_atendimento_aplicaTabCompart(tcCodigo, tcRef)
    ENDIF

    IF !uv_compartManual

        REPLACE	&tcTabLin..epv 			WITH ROUND(lnPut,2)
        replace	&tcTabLin..u_ettent1	WITH ROUND(lnEnt1 * (&tcTabLin..qtt),2)
        replace	&tcTabLin..u_ettent2	WITH ROUND(lnEnt2 * (&tcTabLin..qtt),2)
        replace	&tcTabLin..u_epvp		WITH ROUND(lnPvp,2)

        &&replace &tcTabLin..u_epref		WITH IIF(ISNULL(ROUND(uCrsPrecos.pref,2)),ROUND(lnBase1,2),ROUND(uCrsPrecos.pref,2))
        replace	&tcTabLin..u_epref		WITH IIF(ISNULL(ROUND(uCrsPrecos.pref,2)),0,ROUND(uCrsPrecos.pref,2))
        
        replace	&tcTabLin..comp_tipo 	WITH uf_receituario_compTipo("uCrsCptVal")
        replace	&tcTabLin..comp 		WITH IIF(uCrsCptVal.diploma != "xxx",uCrsCptVal.comp_s_diploma,uCrsCptVal.percentagem)
        replace	&tcTabLin..comp_diploma WITH uCrsCptVal.percentagem
        
        && pvp4 fee
        replace	&tcTabLin..pvp4_fee WITH ROUND(lnPvp4Fee,2)
        

        IF lcTemVal2
            
            
            &&valida se s�o exep��es de complementares, se sim � devolvido o comp_tipo = 1
            LOCAL lcValorExepcaoCombinados 
            lcValorExepcaoCombinados = 0
            lcValorExepcaoCombinados = uf_receituario_CombinadosExepcaoComplementar("uCrsCptVal2")
            
            
            IF(lcValorExepcaoCombinados==0)	
                replace	&tcTabLin..comp_tipo_2  WITH uf_receituario_compTipo("uCrsCptVal2")		
            ELSE
                replace	&tcTabLin..comp_tipo_2  WITH lcValorExepcaoCombinados 	
            ENDIF
                            
            replace	&tcTabLin..comp_2 		  WITH IIF(uCrsCptVal2.diploma != "xxx",uCrsCptVal2.comp_s_diploma,uCrsCptVal2.percentagem)
            replace	&tcTabLin..comp_diploma_2 WITH uCrsCptVal2.percentagem		
        

        ENDIF


        IF !(UPPER(ALLTRIM(tcTabLin)) == "UCRSPRESCL")
            replace	&tcTabLin..pvmoeda		WITH ROUND(lnPut,2)
            replace	&tcTabLin..pv			WITH Round(lnPut,5)
            replace	&tcTabLin..IVAINCL		WITH .T.
        ENDIF

        

        IF (tcMVR)
            ** Actualizar valores do painel **
            IF !(tcCorrecaoAutomatica)
                MVR.txtepv.value		= ROUND(lnPut,2)
                MVR.txtettent1.value	= ROUND(lnEnt1 * (MVR.txtQt.value),2)
                MVR.txtettent2.value	= ROUND(lnEnt2 * (MVR.txtQt.value),2)
                MVR.refresh
            ENDIF
        ENDIF

    ENDIF
    
    IF USED("ucrsCompartBonus")
    	fecha("ucrsCompartBonus")
    ENDIF
    


	IF USED([uCrsCptVal])
		FECHA([uCrsCptVal])
	ENDIF
	IF USED([uCrsCptVal2])
		fecha([uCrsCptVal2])
	ENDIF
ENDFUNC



FUNCTION uf_receituario_compTipo
	LPARAMETERS lcCursor
	

	SELECT &lcCursor	
	DO CASE 
		CASE UPPER(&lcCursor..grppreco) = "PVP"
			RETURN 2
		CASE UPPER(&lcCursor..grppreco) = "VAL"
			RETURN 3
		CASE UPPER(&lcCursor..grppreco) = "VALF"
			RETURN 4
		CASE UPPER(&lcCursor..grppreco) = "ESP"
			RETURN 5
		OTHERWISE
			&& Defeito REF 
			RETURN 1	
	ENDCASE 

ENDFUNC 




**
FUNCTION uf_receituario_CombinadosExepcaoComplementar
	LPARAMETERS lcCursor
	

	SELECT &lcCursor	
	IF(ALLTRIM(&lcCursor..abrev) = 'SNS')
		DO CASE 
			CASE UPPER(ALLTRIM(&lcCursor..grupo)) = "P8" &&DS + 01
				RETURN 1
			CASE UPPER(ALLTRIM(&lcCursor..grupo)) = "P1" &&DS + 01
				RETURN 1
			OTHERWISE
				RETURN 0	
		ENDCASE 		
	
	ELSE
		RETURN 0		
	ENDIF

ENDFUNC 


************
*** TEMP ***
** Fun��es dos Lotes
*!*	FUNCTION uf_receituario_MakePrecos
*!*		LPARAMETERS tcCodigo, tcRef, tcCnp, tcTabLin, tcDiploma, lcFront

*!*		Local lcSql
*!*		STORE "" TO lcSql
*!*		LOCAL lnPvp, lnBase1, lnBase2, lnPut, lnEnt1, lnEnt2, temDiploma
*!*		Store 0 TO lnPvp, lnBase1, lnBase2, lnPut, lnEnt1, lnEnt2
*!*		LOCAL lcComp1, lcComp2 && Indica se h� complementaridade
*!*		STORE .f. to lcTemVal1, lcTemVal2 && Indica se � comparticipado pela entidade 1 e/ou 2
*!*		
*!*		** Verificar se existe diploma na linha
*!*		IF !empty(tcDiploma)
*!*			tcDiploma = alltrim(tcDiploma)
*!*		ELSE
*!*			tcDiploma='xxx'
*!*		ENDIF

*!*		** 
*!*		** Usa pre�o ST ou pre�o da Venda
*!*		**
*!*		LOCAL choosePvp
*!*		STORE 0 TO choosePvp
*!*		Select (tcTabLin)
*!*		IF &tcTabLin..amostra
*!*			choosePvp = &tcTabLin..u_epvp
*!*		ENDIF

*!*		** 
*!*		** Criar cursor - valores para plano 1
*!*		**
*!*		TEXT TO lcSql TEXTMERGE NOSHOW
*!*			exec up_receituario_CalcCompart '<<ALLTRIM(tcDiploma)>>', <<choosePvp>>, '<<ALLTRIM(tcRef)>>', '<<ALLTRIM(tcCodigo)>>', <<mysite_nr>>
*!*		ENDTEXT
*!*		uf_gerais_actgrelha("", [uCrsCptVal], lcSql)
*!*		IF USED("uCrsCptVal")
*!*			IF RECCOUNT("uCrsCptVal")>0
*!*				lcTemVal1 = .t.
*!*			ENDIF
*!*		ENDIF
*!*		

*!*		** 
*!*		** Verificar se existe complementariedade
*!*		**
*!*		uf_gerais_actgrelha("", [uCrsCptCompl], [select compl from cptpla (nolock) where codigo='] + ALLTRIM(tcCodigo) + ['])
*!*		IF USED("uCrsCptCompl")
*!*			SELECT uCrsCptCompl
*!*			IF RECCOUNT([uCrsCptCompl])>0
*!*				
*!*				** 
*!*				** Criar cursor - valores para plano 2
*!*				**
*!*				TEXT TO lcSql NOSHOW TEXTMERGE
*!*					exec up_receituario_CalcCompart '<<ALLTRIM(tcDiploma)>>', <<choosePvp>>, '<<ALLTRIM(tcRef)>>', '<<ALLTRIM(uCrsCptCompl.compl)>>', <<mysite_nr>>
*!*				EndText
*!*				uf_gerais_actgrelha("", [uCrsCptVal2], lcSql)
*!*				SELECT uCrsCptVal2
*!*				If Reccount([uCrsCptVal2])>0
*!*					lcTemVal2 = .t.
*!*				ENDIF
*!*				
*!*			ENDIF
*!*			fecha([uCrsCptCompl])
*!*		ENDIF

*!*		
*!*		**	
*!*		** Criar cursor - precos por tipo
*!*		lcSQL = ''
*!*		TEXT TO lcSQL NOSHOW TEXTMERGE
*!*			exec up_receituario_getPRecos <<choosePvp>>, '<<ALLTRIM(tcRef)>>', <<mysite_nr>>
*!*		ENDTEXT
*!*		IF !uf_gerais_actgrelha("", [uCrsPrecos], lcSQL)
*!*			uf_perguntalt_chama("Ocorreu uma anomalia a verificar os pre�os do produto: "+Chr(13)+lcSql,"OK","",16)
*!*			Return .F.
*!*		ENDIF
*!*		If Reccount([uCrsPrecos])==0
*!*			*uf_perguntalt_chama("N�o encontrei o Produto '"+Alltrim(uCrsPrecos.cnp)+"'","OK","",48)
*!*			Return .F.
*!*		ENDIF

*!*		** garantir o uso dos pre�os certo
*!*		SELECT uCrsPrecos
*!*		GO top
*!*		lnPvp	= Iif(uCrsPrecos.pst1>0, uCrsPrecos.pst1, Nvl(uCrsPrecos.ppvp,0))

*!*		** 
*!*		** Calcular Base
*!*		** 

*!*		lnBase2	= Iif(lcTemVal2 , uf_receituario_GetBase(uCrsCptVal2.grppreco), 0)
*!*		lnBase1 = IIF(lcTemVal1 , uf_receituario_GetBase(uCrsCptVal.grppreco), 0)


*!*		** caso especial SAMS - usar pvp em vez de pref (usar o preco mais baixo sempre quando existe complementariedade)
*!*		IF lcTemVal2 && garantir que existe complementariedade
*!*			SELECT uCrsCptVal
*!*			GO TOP
*!*			IF (ALLTRIM(uCrsCptVal.abrev) == 'SBSI' OR ALLTRIM(uCrsCptVal.abrev) == 'SBN' OR ALLTRIM(uCrsCptVal.abrev) == 'SBC') &&AND ALLTRIM(uCrsCptVal.compl) == '01'
*!*				IF !ISNULL(uCrsPrecos.pref)
*!*					lnBase1 = IIF(uCrsPrecos.pref < uCrsPrecos.pst1, uCrsPrecos.pref, uCrsPrecos.pst1)
*!*				ENDIF
*!*			ENDIF
*!*		ENDIF
*!*		
*!*		** fail safe, posiciona cursores
*!*		IF used("uCrsCptVal2")
*!*			SELECT uCrsCptVal2
*!*			GO top
*!*		ENDIF
*!*		SELECT uCrsCptVal
*!*		GO top

*!*		** Calculos sem complementariedade
*!*		If !lcTemVal2

*!*			DO CASE
*!*				CASE ALLTRIM(Upper(uCrsCptval.grppreco)) == "VAL"
*!*					lnEnt1 = Iif(lnPvp >= Round(lnBase1 - uCrsCptVal.percentagem, 2), Round(uCrsCptVal.percentagem, 2), lnPvp)
*!*					lnPut = lnPvp - lnEnt1

*!*				CASE ALLTRIM(Upper(uCrsCptval.grppreco)) == "VALF" && o valor FINAL a pagar pelo utente, independentemente do pre�o do medicamento
*!*					lnEnt1 = IIF(Round(uCrsCptVal.percentagem, 2) >= lnPVP, lnPVP, lnPvp - Round(uCrsCptVal.percentagem, 2))
*!*					lnPut = IIF(Round(uCrsCptVal.percentagem, 2) >= lnPVP, 0, Round(uCrsCptVal.percentagem, 2))

*!*				OTHERWISE
*!*					** alterei a regra aqui, por causa dos valores negativos que v�m do ppens>pvp
*!*					lnEnt1 = Iif(lnPvp >= Round(lnBase1*uCrsCptVal.percentagem/100,2), Round(lnBase1*uCrsCptVal.percentagem/100,2), lnPvp)
*!*					lnPut = lnPvp - lnEnt1

*!*			ENDCASE

*!*		ELSE && com complementariedade

*!*			DO CASE
*!*				CASE ALLTRIM(Upper(uCrsCptval.grppreco)) == "VAL"
*!*					*determinas o valor a comparticipar pela entidade 2 (SNS ou ADSE)
*!*					lnEnt2	= iif(Round(lnBase2 * uCrsCptval2.percentagem/100,2)>lnPvp, lnPvp, Round(lnBase2 * uCrsCptval2.percentagem/100,2))
*!*					*determinas o valor a comparticipar pela entidade 1 (complementar)
*!*					lnEnt1	= iif(Round(lnBase1 - uCrsCptval.percentagem, 2) > lnPvp, lnPvp, Round(uCrsCptval.percentagem, 2))
*!*						*				lnPut	= lnPvp - Max(lnEnt2,lnEnt1)
*!*						*				lnEnt1	= Max(lnPvp-lnPut-lnEnt2, 0)
*!*					* valor a pagar pelo utente
*!*					lnPut	= IIF(lnPvp - lnEnt2 - lnEnt1 > 0, lnPvp - lnEnt2 - lnEnt1, lnPvp - lnEnt2)
*!*					*No caso da entidade principal comparticipar tudo a entidade complementar no comparticipa
*!*					lnEnt1	= IIF(lnPvp - lnEnt2 - lnEnt1 > 0, lnEnt1, 0)

*!*				CASE ALLTRIM(Upper(uCrsCptval.grppreco)) == "VALF" && o valor FINAL a pagar pelo utente, independentemente do pre�o do medicamento
*!*					* determina o valor a comparticipar pela entidade 2 (SNS)
*!*					lnEnt2	= IIF(Round(lnBase2 * uCrsCptval2.percentagem/100,2)>lnPvp, lnPvp, Round(lnBase2 * uCrsCptval2.percentagem/100,2))

*!*					** valor a pagar pelo utente
*!*	*!*					IF uCrsCptVal.percentagem == 0
*!*	*!*						lnPut = lnPVP - lnEnt2
*!*	*!*						lnEnt1 = 0
*!*	*!*					ELSE
*!*						lnPut = IIF(Round(uCrsCptVal.percentagem, 2) >= lnPVP, 0, Round(uCrsCptVal.percentagem, 2))
*!*						lnEnt1 = lnPVP - lnEnt2 - lnPut
*!*	*!*					ENDIF

*!*				CASE ALLTRIM(Upper(uCrsCptval.grppreco)) == "ESP" && Multicare; Savida
*!*					*** Casos Especiais : Multicare ***
*!*					IF (uCrsCptVal.cptplacode=='XO0' OR uCrsCptVal.cptplacode=='XP1' OR uCrsCptVal.cptplacode=='XQ0' OR uCrsCptVal.cptplacode=='XR1' OR uCrsCptVal.cptplacode=='XS0') &&MC5 utente= 10% + (PVP-PREF)
*!*						lnPut = Round(iif(lnPvp - Round(lnBase2 * uCrsCptVal2.percentagem/100,2)>0, lnPvp - Round(lnBase2 * uCrsCptVal2.percentagem/100,2), 0) * (1-uCrsCptVal.percentagem/100),2)
*!*						lnPut = lnPut + (MAX(lnPvp - lnBase2, 0))
*!*					ELSE
*!*						lnPut = Round(IIF(lnPvp - Round(lnBase2 * uCrsCptVal2.percentagem/100,2) > 0, lnPvp - Round(lnBase2 * uCrsCptVal2.percentagem/100,2), 0) * (1-uCrsCptVal.percentagem/100),2)
*!*					ENDIF
*!*					
*!*					** Se for Savida e o medicamento for de marca -> validar se o pre�o do medicamento � superior ao pre�o do gen�rico mais caro do mesmo grupo homog�neo
*!*					IF ALLTRIM(uCrsCptVal.abrev) == 'SAVIDA'
*!*						Select (tcTabLin)
*!*						IF !(&tcTabLin..u_generico)
*!*							TEXT TO lcSql NOSHOW textmerge
*!*								select
*!*									pvporig
*!*									,pvpmax_generico = isnull((select max(pvporig) from fprod fprod2 where fprod2.grphmgcode=fprod.grphmgcode and generico=1),0)
*!*								from
*!*									fprod (nolock)
*!*								where
*!*									cnp='<<alltrim(tcRef)>>' and grphmgcode!='' and grphmgcode!='GH0000'
*!*							ENDTEXT
*!*							IF uf_gerais_actgrelha("", [uCrsCasoEspSavida], lcSql)
*!*								SELECT uCrsCasoEspSavida
*!*								If Reccount([uCrsCasoEspSavida])>0
*!*									IF (uCrsCasoEspSavida.pvpmax_generico != 0 AND uCrsCasoEspSavida.pvporig > uCrsCasoEspSavida.pvpmax_generico)
*!*										** Utiliza o pre�o do gen�rico mais carago e subtrai o comparticipado
*!*										lnPut = Round(IIF(uCrsCasoEspSavida.pvpmax_generico - Round(lnBase2 * uCrsCptVal2.percentagem/100,2) > 0, uCrsCasoEspSavida.pvpmax_generico - Round(lnBase2 * uCrsCptVal2.percentagem/100,2), 0) * (1-uCrsCptVal.percentagem/100),2)

*!*										IF lnPut==0 && se a comparticipa��o do SNS for superior ao pre�o do gen�rico mais caro, a SAVIDA n�o tem nada a comparticipar
*!*											lnPut = lnPvp - MAX(Round(lnBase2 * uCrsCptVal2.percentagem/100,2),0)
*!*										else
*!*											lnPut = lnPut + (uCrsCasoEspSavida.pvporig - uCrsCasoEspSavida.pvpmax_generico)									
*!*										ENDIF
*!*									ENDIF
*!*								ENDIF
*!*								fecha([uCrsCasoEspSavida])
*!*							ENDIF
*!*						ENDIF
*!*					ENDIF
*!*					
*!*					** alterei a regra aqui, por causa dos valores negativos que v�m do ppens>pvp
*!*					lnEnt2 = iif(lnPvp>Round(lnBase2 * uCrsCptVal2.percentagem/100,2),Round(lnBase2 * uCrsCptVal2.percentagem/100,2),lnPvp)
*!*					lnEnt1 = MAX(lnPvp - lnEnt2 - lnPut, 0)

*!*				OTHERWISE
*!*					lnEnt2 = iif(Round(lnBase2 * uCrsCptval2.percentagem/100, 2)>lnPvp, lnPvp, Round(lnBase2 * uCrsCptval2.percentagem/100,2))

*!*					** Casos em que a entidade complementar comparticipa x% - y%sns
*!*					** SBSI / SBN / SBC (90%-%sns)
*!*					** CML (75%-%sns)
*!*					** Savida (100%-%sns)

*!*					IF (ALLTRIM(uCrsCptVal.abrev) == 'SBSI';
*!*						OR ALLTRIM(uCrsCptVal.abrev) == 'SBN';
*!*						OR ALLTRIM(uCrsCptVal.abrev) == 'SBC';
*!*						OR ALLTRIM(uCrsCptVal.abrev) == 'SNQTB';
*!*						OR ALLTRIM(uCrsCptVal.abrev) == 'CML/SS';
*!*						OR ALLTRIM(uCrsCptVal.abrev) == 'SAVIDA';
*!*						OR ALLTRIM(uCrsCptVal.abrev) == 'PT/CTT';
*!*						OR ALLTRIM(uCrsCptVal.abrev) == 'CGD';
*!*						OR ALLTRIM(uCrsCptVal.abrev) == 'MEDIS';
*!*						OR ALLTRIM(uCrsCptVal.abrev) == 'MEDIS/CTT';
*!*						)					
*!*						** calcular comparticipa��o da entidade complementar e subtrair � do SNS
*!*						lnEnt1 = iif(Round(lnBase1 * uCrsCptval.percentagem/100, 2)>lnPvp, lnPvp, Round(lnBase1 * uCrsCptval.percentagem/100,2))
*!*						lnEnt1 = IIF(lnEnt1 - lnEnt2 < 0, 0, lnEnt1 - lnEnt2)

*!*	*!*						IF (uCrsCptval.percentagem - uCrsCptval2.percentagem < 0)
*!*	*!*							lnEnt1 = 0
*!*	*!*						ELSE
*!*	*!*							lnEnt1 = iif(Round(lnBase1 * (uCrsCptval.percentagem - uCrsCptval2.percentagem)/100, 2)>lnPvp, lnPvp, Round(lnBase1 * (uCrsCptval.percentagem - uCrsCptval2.percentagem)/100,2))
*!*	*!*						ENDIF
*!*					ELSE
*!*						lnEnt1 = iif(Round(lnBase1 * uCrsCptval.percentagem/100,2)>lnPvp, lnPvp, Round(lnBase1 * uCrsCptval.percentagem/100,2))
*!*					ENDIF

*!*					** Casos em que a comparticipa��o das duas entidades � > pvp, e que a entidade complementar comparticipa sempre o remanescente
*!*					IF (ALLTRIM(uCrsCptVal.abrev) == 'SBSI';
*!*						OR ALLTRIM(uCrsCptVal.abrev) == 'SBN';
*!*						OR ALLTRIM(uCrsCptVal.abrev) == 'SBC';
*!*						OR ALLTRIM(uCrsCptVal.abrev) == 'SNQTB';
*!*						OR ALLTRIM(uCrsCptVal.abrev) == 'CML/SS';
*!*						OR ALLTRIM(uCrsCptVal.abrev) == 'SAVIDA';
*!*						OR ALLTRIM(uCrsCptVal.abrev) == 'PT/CTT';
*!*						OR ALLTRIM(uCrsCptVal.abrev) == 'CGD';
*!*						OR ALLTRIM(uCrsCptVal.abrev) == 'MEDIS';
*!*						OR ALLTRIM(uCrsCptVal.abrev) == 'MEDIS/CTT';
*!*						)
*!*						
*!*						**lnPut = IIF(lnPvp-(lnEnt1+lnEnt2) < 0, 0, lnPvp-(lnEnt1+lnEnt2))
*!*						lnPut = IIF(lnPvp-(lnEnt1+lnEnt2) < 0, IIF(lnPvp-lnEnt2<0,0,lnPvp-lnEnt2), lnPvp-(lnEnt1+lnEnt2))
*!*					ELSE
*!*						** utilizar as duas entidades ou apenas o mais alto se as duas forem > pvp
*!*						lnPut = IIF(lnPvp-(lnEnt1+lnEnt2) < 0, lnPvp-MAX(lnEnt2,lnEnt1), lnPvp-(lnEnt1+lnEnt2))
*!*					ENDIF
*!*						
*!*					lnEnt1 = Max(lnPvp-lnPut-lnEnt2, 0)
*!*			ENDCASE
*!*		ENDIF

*!*		** for�ar update de cursor
*!*		TRY
*!*			Select &tcTabLin
*!*			GO RECNO()
*!*		CATCH
*!*		ENDTRY

*!*		REPLACE	&tcTabLin..epv 			WITH ROUND(lnPut,2)
*!*		replace	&tcTabLin..u_ettent1	WITH ROUND(lnEnt1 * (&tcTabLin..qtt),2)
*!*		replace	&tcTabLin..u_ettent2	WITH ROUND(lnEnt2 * (&tcTabLin..qtt),2)
*!*		replace	&tcTabLin..u_epvp		WITH ROUND(lnPvp,2)
*!*	*!*	replace	&tcTabLin..u_epref		WITH ROUND(lnBase1,2)
*!*		replace	&tcTabLin..u_epref		WITH IIF(ISNULL(ROUND(uCrsPrecos.pref,2)),ROUND(lnBase1,2),ROUND(uCrsPrecos.pref,2))
*!*		
*!*		IF !(UPPER(ALLTRIM(tcTabLin))=="UCRSPRESCL")
*!*			replace	&tcTabLin..pvmoeda		WITH ROUND(lnPut,2)
*!*			replace	&tcTabLin..pv			WITH Round(lnPut,5)
*!*			replace	&tcTabLin..IVAINCL		WITH .T.
*!*		ENDIF

*!*	*!*		IF lcFront==.f.
*!*	*!*			Do u_ftiliq
*!*	*!*			Do fttots WITH .T.
*!*	*!*		ENDIF

*!*		IF USED([uCrsCptVal])
*!*			FECHA([uCrsCptVal])	
*!*		ENDIF
*!*		IF USED([uCrsCptVal2])
*!*			fecha([uCrsCptVal2])
*!*		ENDIF
*!*	ENDFUNC
*** TEMP ***
************


**
Function uf_receituario_GetBase
	LPARAMETERS tcGpreco
	

	
	LOCAL nBase
	nBase=0
	
	DO CASE
	
		
		
		CASE ALLTRIM(Upper(tcGPreco)) == "MNRP"
			LOCAL lnPvp, lnPref
			STORE 0 TO lnPvpMed, lnPrefMed
			
			lnPvpMed  = Iif(uCrsPrecos.pst1>0, uCrsPrecos.pst1, NVL(uCrsPrecos.ppvp,0))
			
			IF !IsNull(uCrsPrecos.pref)
				lnPrefMed = uCrsPrecos.pref
			ENDIF
			
			DO CASE
			    CASE lnPrefMed == 0
			        *-- Se lnPrefMed for 0, escolhe lnPvpMed
			        nBase = lnPvpMed
			    OTHERWISE
			        *-- Se ambos forem maiores que 0, escolhe o menor dos dois
			        nBase = IIF(lnPvpMed> lnPrefMed , lnPrefMed , lnPvpMed)
			ENDCASE
								
			
			
	
		CASE ALLTRIM(Upper(tcGPreco)) == "PVP"
			nBase = Iif(uCrsPrecos.pst1>0, uCrsPrecos.pst1, NVL(uCrsPrecos.ppvp,0))
		
			
		CASE ALLTRIM(Upper(tcGpreco)) == "REF"
			IF IsNull(uCrsPrecos.pref)
				If IsNull(uCrsPrecos.ppvp)
					*** N�o h� PREF nem PVP => Aplica-se ST **
					nBase = uCrsPrecos.pst1
				ELSE
					*** N�o h� PREF mas H� PVP ***
					
					IF uCrsPrecos.pst1>0	&& foi alterado para usar o pvp da ficha (nos casos em que nao existe pref)
						nBase = uCrsPrecos.pst1
					ELSE
						nBase = uCrsPrecos.ppvp
					ENDIF
				ENDIF
			ELSE
				*** Existe PREF =>  Comparar com PVP ***
				IF IsNull(uCrsPrecos.ppvp)
					nBase = uCrsPrecos.pref
				ELSE
					nBase = uCrsPrecos.pref
					*** aten��o: aqui utiliza-se fpreco.epv e no verso de receita 1 e 2 tem de se alterar o pre�o de referencia para um select directo
				ENDIF
			ENDIF
	
		CASE ALLTRIM(Upper(tcGpreco)) == "PENS"
			*** Semelhante ao que acontece em REF
			If IsNull(uCrsPrecos.ppens)
				If IsNull(uCrsPrecos.ppvp)
					nBase = uCrsPrecos.pst1
				ELSE
					IF uCrsPrecos.pst1>0	&& foi alterado para usar o pvp da ficha (nos casos em que nao existe pref)
						nBase = uCrsPrecos.pst1
					ELSE
						nBase = uCrsPrecos.ppvp
					ENDIF
				Endif
			Else
				nBase =Iif(uCrsPrecos.ppens < uCrsPrecos.pst1, uCrsPrecos.ppens , uCrsPrecos.pst1)
			ENDIF
			
		Case ALLTRIM(Upper(tcGpreco)) == "ESP"
			nBase=0
		
		CASE ALLTRIM(UPPER(tcGpreco)) == "VAL"
			nBase = Iif(uCrsPrecos.pst1>0, uCrsPrecos.pst1, NVL(uCrsPrecos.ppvp,0))
			
		CASE ALLTRIM(UPPER(tcGpreco)) == "VALF"
			nBase = Iif(uCrsPrecos.pst1>0, uCrsPrecos.pst1, NVL(uCrsPrecos.ppvp,0))			
			
		OTHERWISE
	        LOCAL lcvalidapr
	        STORE .f. TO lcvalidapr
	        SELECT uCrsAtendComp
	        GO TOP 
	        SCAN
		        IF  uCrsAtendComp.e_compart == .F. AND lcvalidapr = .F.
		        	lcvalidapr = .T.
		        ENDIF
	        ENDSCAN
	        
	        IF  lcvalidapr = .T.
				uf_perguntalt_chama("Grupo de Pre�o Inv�lido. Valores v�lidos:PVP,REF,ESP,VAL,VALF,RMP,RMV, MNRP","OK","",16)
				nBase = -9999
			ENDIF 
	ENDCASE
	
	RETURN nBase
ENDFUNC


** funcao chamada do pagamento para criar lotes
FUNCTION uf_receituario_VerificaLote
	LPARAMETERS tcEcra, tcCod, tcCod2, tcFtStamp, tcVia, tcReceitaNr, tcToken

	PRIVATE lcTab, lcTab2, lcTabLin, lcCodPla, lcCodPla2, lnAno, lnMes, lcNdocx, lcLote1x, lcLote2x, lcVdSuspRg, lnEttEnt1, lnEttEnt2, llLimpa2
	LOCAL lcSql, lnTipoDocF, llBack, lcValRet, lcCodAcesso, lcCodDirOpcao, lcNrSNS, lcIsDem
	STORE .t. TO lcValRet
	
	lcIsDem = .f.
	llBack=.F.

	IF PCOUNT()>0 AND VARTYPE(tcEcra)="C"
		IF ALLTRIM(UPPER(tcEcra)) == "BACK"
			llBack = .T.
			lcValRet = .t.
		ENDIF
	ENDIF

	&& TODO: estes cursores deviam estar a ser fechados
	&& TODO: colocar este c�digo em SP e chamar apenas as colunas necess�rias
	uf_gerais_actgrelha("", [lcTab], [select * from ft (nolock) where ftstamp=']+alltrim(tcFtStamp)+['])
	uf_gerais_actgrelha("", [lcTab2], [select * from ft2 (nolock) where ft2stamp=']+alltrim(tcFtStamp)+['])
	uf_gerais_actgrelha("", [lcTabLin], [select fi.*, ISNULL(fi2.isDem,0) as isDem from fi (nolock) left join fi2 (nolock) on fi2.fistamp = fi.fistamp where fi.ftstamp=']+alltrim(tcFtStamp)+['])


	select lcTab
	lcCodPla 	= tcCod
	lcCodPla2 	= tcCod2
	lnAno 		= lcTab.ftano
	lnMes 		= Month(lcTab.fdata)
	lcNdocx		= lcTab.ndoc
	lcLote1x	= lcTab.u_lote
	lcLote2x	= lcTab.u_lote2

	select lcTab2
	lcVdSuspRg	= lcTab2.u_vdsusprg
	lnEttEnt1	= 0
	lnEttEnt2	= 0
	llLimpa2 	= .T.
	lcSql		= ""
	lnTipoDocF	= 0
	IF EMPTY(tcReceitaNr)
		tcReceitaNr = ALLTRIM(lcTab2.u_receita)
	ENDIF

	SELECT lcTabLin
	CALCULATE SUM(u_ettent1), SUM(u_ettent2) TO lnEttEnt1, lnEttEnt2
	
	
	SELECT lcTabLin
	GO TOP
	SCAN
		if(!EMPTY(lcTabLin.isDem))
			lcIsDem = .t.
		endif
	ENDSCAN

	lnTipoDocF = lcTab.cobrado

	IF (!EMPTY(lcCodPla) AND lcLote1x == 0 AND lnTipoDocF == .f.) &&(lnTipoDocF <> 4 OR (lnTipoDocF = 4 AND lcVdSuspRg=.t.))) && rever estas condi��es
		IF (lnEttEnt1 <> 0 AND uf_receituario_FimCopia(.t.)) 

**todo: temp
&&MESSAGEBOX("Cria lote")

			lcValRet = uf_receituario_CriaLote(.F., .F., tcVia, tcReceitaNr, tcToken, lcIsDem)
		ENDIF

		&& Se H� Plano Complementar => Cria os Lotes para Este 
		IF (!EMPTY(lcCodPla2) AND lcLote2x=0 AND lnTipoDocF=.f.)
			IF lnEttEnt2 <> 0
				llLimpa2 = .F.

				lcValRet = uf_receituario_CriaLote(.T., .F., tcVia, tcReceitaNr, tcToken, lcIsDem)
			ENDIF
		ENDIF
	ENDIF

	RETURN lcValRet
ENDFUNC


** Insere as receitas no Lote
FUNCTION uf_receituario_CriaLote
	LPARAMETERS tlComp, tlCorrige, tcVia, tcReceitaNr, tcToken, tcIsDem

	LOCAL lcSufix, lcSql, lnRecMax, lcCod12, nlote, lcValidaRet
	STORE .f. TO lcValidaRet
	STORE "" TO lcSql
	lnRecMax = 0
	lcCod12 = IIF(tlComp,lcCodPla2,lcCodPla)
	nlote = 0
	

	
	&& Dados necess�rios para Pesquisar na Tabela CTLTRCT
	lcSQL = ''
	TEXT TO lcSQL NOSHOW TEXTMERGE
		SELECT
			tlotecode, cptorgstamp, cptorgabrev, codigo, compl_lote_eletronico, snsExterno, mcdt
		FROM
			cptpla (nolock)
		WHERE
			codigo='<<lcCod12>>'
	ENDTEXT
	uf_gerais_actgrelha("", [uCrsPla], lcSql)


	IF Reccount([uCrsPla])==0
		uf_perguntalt_chama("O plano selecionado n�o est� definido ou n�o foi encontrado. A venda n�o ficar� associada aos lotes." + chr(13) + "Por favor contacte o suporte.","OK","",16)
		fecha([uCrsPla])
		RETURN .t.
	ENDIF
	
	

	**
	SELECT uCrsPla
	GO TOP
	IF !EMPTY(tcToken) AND uCrsPla.compl_lote_eletronico == 97																																																		

**todo: temp
&&MESSAGEBOX("Token: " + tcToken)

		** Controla Tipo Lote DEM
		** 99 - RCP, sem erro
		** 98 - RCP, com erro
		** 97 - RSP, sem erro
		** 96 - RSP, com erro
		
		** 99 - 45 - SNSExterno
		
		
		If(EMPTY(uCrsPla.snsExterno))
				
			lcSQL = ''
			TEXT TO lcSQL NOSHOW TEXTMERGE 
				exec up_dem_tipoLote '<<ALLTRIM(tcToken)>>'
			ENDTEXT 
			uf_gerais_actgrelha("", [uCrsTipoLoteDem], lcSql)

			SELECT uCrsTipoLoteDem
			IF !EMPTY(uCrsTipoLoteDem.tipoLote)
				IF ALLTRIM(uCrsPla.cptorgabrev) == "SNS" 
					replace uCrsPla.tlotecode WITH uCrsTipoLoteDem.tipoLote 
				ELSE
					&& s� se for RSP
					IF uCrsTipoLoteDem.tipoLote == '96' OR uCrsTipoLoteDem.tipoLote == '97'
						replace uCrsPla.tlotecode WITH '97'
					ENDIF
				ENDIF
			ENDIF
			if !uf_gerais_getparameter("ADM0000000355", "bool")
				IF USED("uCrsTipoLoteDem")
					fecha("uCrsTipoLoteDem")
				ENDIF
			ENDIF
			
		ENDIF
		
	ENDIF
	
	
	


	** 97 - Requisi��es desmaterializadas
	
	if !uf_gerais_getparameter("ADM0000000355", "bool")
		SELECT uCrsPla
		GO top
		IF(!empty(uCrsPla.mcdt) and EMPTY(tcToken) and !EMPTY(tcIsDem))
			SELECT uCrsPla
			replace uCrsPla.tlotecode WITH '97'		
		ENDIF	
	ELSE
		
		SELECT uCrsPla
		GO top
		IF(!empty(uCrsPla.mcdt) and !EMPTY(tcToken) AND UPPER(ALLTRIM(uCrsTipoLoteDem.receita_tipo)) = 'RSP')
			SELECT uCrsPla
			replace uCrsPla.tlotecode WITH '97'		
		ENDIF	

	    IF USED("uCrsTipoLoteDem")
	        fecha("uCrsTipoLoteDem")
	    ENDIF
	
	ENDIF
	
	SELECT uCrsPla
	GO top
	
**todo: temp

	
	** Continuar no Maior Lote em Aberto s
	** Ex: Se existe 1 aberto com receita 11 a max 30 faz a 12 do mm lote 
		
	&& verificar se � posto e alterar o site
	LOCAL siteP
	IF myPosto
		siteP =  mySitePosto
	ELSE
		siteP = mySite
	ENDIF
	
	lcSQL = ''
	TEXT TO lcSql NOSHOW TEXTMERGE
		Select TOP 1
			ano, mes, slote, tlote, lote, nreceita mx
			,limite=isnull((SELECT nrctlt FROM cptpla (nolock) WHERE codigo='<<lcCod12>>'),0)
			,cptorgabrev
		FROM
			ctltrct (nolock) 
		WHERE
			fechado=0 AND ano=<<lnAno>> AND mes=<<lnMes>> AND tlote='<<uCrsPla.TloteCode>>' AND cptorgstamp='<<uCrsPla.cptorgstamp>>'
			and site='<<alltrim(siteP)>>'
		ORDER BY
			ano, mes, slote desc, tlote, cptorgabrev, lote desc, nreceita desc
	ENDTEXT
	uf_gerais_actgrelha("", [uCrsLoteAb], lcSql)
	


	SELECT uCrsLoteAb
	IF Reccount([uCrsLoteAb]) > 0 AND (uCrsLoteAb.mx < uCrsLoteAb.Limite)
		*** H� Lotes em Aberto com posi��es Livres ***
		DIMENSION aLoteReceita[2]
		aLoteReceita[1] = uCrsLoteAb.lote
		aLoteReceita[2] = 0
		nLote = 0
		uf_receituario_BuracoNoLote(lnAno, lnMes, uCrsPla.tlotecode, uCrsPla.cptOrgstamp, lcCod12, @aLoteReceita)
		nLote = IIF(aLoteReceita[2]>0, aLoteReceita[2], uCrsLoteAb.mx+1)
		lcValidaRet = uf_receituario_RegistaLote(uCrsPla.CptOrgStamp, uCrsPla.CptOrgAbrev, aLoteReceita[1], uCrsPla.TLoteCode, uCrsLoteAb.slote, lnAno, lnMes, nlote, tlComp, tlCorrige,tcVia, tcReceitaNr, tcToken)
	ELSE
		*** Criar um Novo lote que � o M�ximo deste Mes + 1 ***
		*** Sen�o existir este mes come�a no 1				***
		TEXT TO lcSql NOSHOW TEXTMERGE
			Select TOP 1
				ano, mes, slote, tlote, lote
			FROM
				ctltrct (nolock)
			WHERE
				ano = <<lnAno)>>
				AND mes = <<lnMes>>
				AND tlote = '<<uCrsPla.TloteCode>>'
				AND cptorgabrev = '<<uCrsPla.cptorgabrev>>'
				AND site = '<<ALLTRIM(siteP)>>'
			ORDER BY
				Lote desc
		ENDTEXT
		uf_gerais_actgrelha("", [uCrsLotesAll], lcSql)
		If Reccount([uCrsLotesAll]) > 0
			*** H� lotes este m�s. Mas n�o existem abertos abaixo do Limite. 	***
			*** Criamos novo igual ao max+1 e a receita inicializa em 1			***



			lcValidaRet = uf_receituario_RegistaLote(uCrsPla.CptOrgStamp,uCrsPla.CptOrgAbrev,uCrsLotesAll.lote+1,uCrsPla.TLoteCode,uCrsLotesAll.slote,lnAno,lnMes,1,tlComp,tlCorrige,tcVia, tcReceitaNr, tcToken)
		ELSE
		
**todo: temp
&&MESSAGEBOX("Vou registar Lote [novo]")

			*** Novo Lote come�a em 1. A s�rie continua a do mes anterior se existir ***
			TEXT TO lcSql NOSHOW TEXTMERGE
				Select TOP 1
					ano,mes,slote,tlote,lote
				FROM
					ctltrct (nolock)
				WHERE
					ano=<<Iif(lnMes=1,lnAno-1,lnAno)>> AND mes < <<Iif(lnMes=1,"=12",astr(lnMes))>>
					AND tlote='<<uCrsPla.TloteCode>>' AND cptorgabrev='<<uCrsPla.cptorgabrev>>'		
					and site='<<ALLTRIM(siteP)>>'
				ORDER BY
					slote desc
			ENDTEXT
			uf_gerais_actgrelha("", [uCrsSerieAnt], lcSql)
			If Reccount([uCrsSerieAnt])>0
				*** Receita 1 do Lote 1 da serie = Max(serie mes anterior) + 1
				lcValidaRet = uf_receituario_RegistaLote(uCrsPla.CptOrgStamp,uCrsPla.CptOrgAbrev,1,uCrsPla.TLoteCode,uCrsSerieAnt.slote+1,lnAno,lnMes,1,tlComp,tlCorrige, tcVia, tcReceitaNr, tcToken)
			Else
				*** N�o H� receitas no M�s Anterior => serie 1, lote 1, receita 1
				lcValidaRet = uf_receituario_RegistaLote(uCrsPla.CptOrgStamp,uCrsPla.CptOrgAbrev,1,uCrsPla.TLoteCode,1,lnAno,lnMes,1,tlComp,tlCorrige, tcVia, tcReceitaNr, tcToken)
			EndIf
			fecha([uCrsSerieAnt])
		EndIf
		fecha([uCrsLotesAll])
	ENDIF
	fecha([uCrsLoteAb])
	
	RETURN lcValidaRet
ENDFUNC


**
FUNCTION uf_receituario_RegistaLote
	LPARAMETERS tcOrgStamp, tcOrgAbrev, tnLote, tcTlote, tnSlote, tnAno, tnMes, tnReceita, tlComp2, tlCorrige2, tcVia, tcReceitaNr, tcToken

	Local lcSql, llRetVal, lcStamp, tlLoteComp, lcSuf, lcCodigo, lcStampx, lcReceita, lcCartao, lcVia, lcComprovativoRegisto 
	Store "" to lcRetVal, lcStampx, lcReceita, lcCartao, lcComprovativoRegisto 
	STORE 0 TO lcVia

	llRetVal=.F.

	DO CASE
		CASE EMPTY(tcVia) OR Upper(tcVia) == "NORMAL"
			lcVia = 0
		CASE Upper(tcVia) == "1� VIA"
			lcVia = 1
		CASE Upper(tcVia) == "2� VIA"
			lcVia = 2
		CASE Upper(tcVia) == "3� VIA"
			lcVia = 3
		Otherwise
			lcVia = 0
	ENDCASE

	select lcTab
	lcStampx=lcTab.ftstamp
	*lcStamp=IIF(tlComp2, u_stamp(2), u_stamp(1))
	lcStamp=uf_gerais_stamp()
	lcSuf=Iif(tlComp2,"2","")	&& Sufixo 2 no caso de Lote Complementar

	select lcTab2
	lcCodigo = Iif(tlComp2, lcTab2.u_codigo2, lcTab2.u_codigo)
	lcReceita = ALLTRIM(lcTab2.u_receita)
	lcCartao = ALLTRIM(lcTab2.c2codpost)
	
	&& verificar se � posto e alterar o site
	LOCAL siteP, lcPosto
	STORE '' TO lcPosto
	IF myPosto
		siteP = mySitePosto
		lcPosto=mySite
	ELSE
		siteP = mySite
	ENDIF
	
	SET HOURS TO 24
	Atime=ttoc(datetime()+(difhoraria*3600),2)
	Astr=left(Atime,8)
	
	lcSQL = ''
	TEXT TO lcSql NOSHOW TEXTMERGE PRETEXT 7
		INSERT INTO ctltrct
			(
			ctltrctstamp, cptorgstamp, cptorgabrev, Lote
			,Tlote, Slote, Ano, Mes, NReceita
			,ousrinis, usrinis, ousrhora, usrhora, cptplacode
			,site, receita, posto, cartao, via, token
			)
		VALUES
			(
			'<<lcStamp>>', '<<tcOrgStamp>>', '<<tcOrgAbrev>>', <<tnLote>>
			,'<<tcTlote>>', <<tnSlote>>, <<tnAno>>, <<tnMes>>, <<tnReceita>>
			,'<<m_chinis>>', '<<m_chinis>>', '<<Astr>>', '<<Astr>>', '<<lcCodigo>>'
			,'<<alltrim(siteP)>>', '<<lcReceita>>', '<<lcPosto>>', '<<lcCartao>>',<<lcVia>>
			,'<<ALLTRIM(tcToken)>>'
		)
	ENDTEXT 
	


	llRetVal = uf_gerais_actGrelha("","",lcSql) &&tem quer ser esta fun��o para n�o apresentar nenhuma msg caso d� erro de pk
	
	&&Receitas Logs
	LOCAL lcSqlToSafe
	lcSqlToSafe=lcSQL
	uf_gerais_registaErroLog("SQL Receitas logs: " + ALLTRIM(lcSqlToSafe), "R0")

	IF llRetVal
		lcSQL = ''
		TEXT TO lcSql NOSHOW TEXTMERGE PRETEXT 7
			UPDATE
				ft
			SET
				u_ltstamp<<Alltrim(lcSuf)>>='<<lcStamp>>'
				,u_slote<<Alltrim(lcSuf)>>=<<tnSlote>>
				,u_tlote<<Alltrim(lcSuf)>>='<<tcTlote>>'
				,u_lote<<Alltrim(lcSuf)>>=<<tnLote>>
				,u_nslote<<Alltrim(lcSuf)>>=<<tnReceita>>
				<<IIF(tlComp2,"",",u_ltstamp2=''"+Iif(llLimpa2,'','u_ltstamp2'))>>
			WHERE
				ftstamp='<<IIF(tlCorrige2,uCrsFt.ftstamp,lcStampx)>>'
		ENDTEXT
		

		
		
		IF !uf_gerais_actGrelha("","",lcSql) &&tem quer ser esta fun��o para n�o apresentar nenhuma msg caso d� erro de pk
			&&apagar registo da ctltrct
			uf_gerais_actGrelha("","","delete from ctltrct where ctltrctstamp='"+lcStamp+"'")
			RETURN .f.
		ELSE
			lcSqlToSafe=lcSQL
			uf_gerais_registaErroLog("SQL Receitas logs: " + ALLTRIM(lcSqlToSafe), "R1")
			
			
			IF USED("uCrsFt") && impress�o de tal�es no atendimento
				SELECT uCrsFt
				LOCATE FOR ALLTRIM(uCrsFt.ftstamp)==ALLTRIM(lcStampx)
				replace uCrsFt.u_lote WITH tnLote
			ENDIF
		ENDIF
		
			&&Receitas Logs
		
	ENDIF

	RETURN llRetVal
ENDFUNC


**
Function uf_receituario_FimCopia
	Lparameters tcBool
	LOCAL lcSql, lnLinhas, lnFechadas, llOk, lcBiStamp
	STORE 0 TO lnLinhas, lnFechadas
	llOk = .T.

	IF PCOUNT()>0
		lcBiStamp = lcTabLin.bistamp
		select lcTabLin
	ELSE
		lcBiStamp = &lcTabLin..bistamp
		SELECT (lcTabLin)
	ENDIF 

	SCAN FOR !EMPTY(lcBiStamp)
		lnLinhas = lnLinhas + 1
		TEXT TO lcSql NOSHOW TEXTMERGE
			SELECT
				count(*) ct
			FROM
				(
				SELECT
					bistamp, qtt, qtt2, bo.fechada
				FROM
					bo (nolock)
					INNER JOIN bi (nolock) ON bo.bostamp = bi.bostamp
				WHERE
					bo.bostamp = (SELECT bostamp FROM bi (nolock)
								WHERE bi.bistamp='<<lcBiStamp>>')
			) z
			WHERE
				qtt > qtt2 AND fechada = CAST(0 as Bit)
		ENDTEXT
		uf_gerais_actgrelha("", [uCrsCntLinBi], lcSql)
		llOk = !(uCrsCntLinBi.ct>0)
		FECHA([uCrsCntLinBi])
	ENDSCAN

	RETURN llOk
ENDFUNC


**
FUNCTION uf_receituario_BuracoNoLote
	LPARAMETERS tnAno, tnMes, tcTlote, tcOrgStamp, tcCod, taLoteReceita
	LOCAL lcSql, lnBuraco, j, l, lnAlias
	lcSql=""
	lnBuraco=0
	lnAlias = SELECT(0)
	
	
	&& verificar se � posto e alterar o site
	LOCAL siteP
	IF myPosto
		siteP =  mySitePosto
	ELSE
		siteP = mySite
	ENDIF
	
	TEXT TO lcSql NOSHOW TEXTMERGE
		SELECT
			ano, mes, slote, tlote, lote, nreceita
			,limite=isnull((SELECT nrctlt FROM cptpla (nolock) WHERE codigo='<<tcCod>>'),0)
			,cptorgabrev
		FROM
			ctltrct (nolock)
		WHERE
			 ano=<<tnAno>> AND mes=<<tnMes>> AND cptorgstamp='<<tcOrgStamp>>' AND tlote='<<tcTlote>>'
			 and site='<<ALLTRIM(siteP)>>'
		ORDER BY
			lote, nreceita
	ENDTEXT
	uf_gerais_actgrelha("", [uCrsBuracos], lcSql)

	SELECT uCrsBuracos
	GO TOP
	j=0
	l=uCrsBuracos.lote

	SELECT uCrsBuracos
	SCAN
		j=j+1
		
		** fail safe para quando existe duplica��o de numeros de receita, que n�o devia acontecer mas fnetch 20120215 **
		If j > uCrsBuracos.nreceita And l == uCrsBuracos.lote
			j=uCrsBuracos.nreceita
		Endif
		****************************************************************************************************************
		
		IF l=uCrsBuracos.lote
			*** Mesmo Lote ***
			IF j> uCrsBuracos.Limite
				j=1
			ENDIF
			IF j<uCrsBuracos.nreceita
				*** H� buraco ***
				lnBuraco = j
				taLoteReceita[1] = uCrsBuracos.lote
				taLoteReceita[2] = j
				EXIT
			ENDIF
		ELSE	
			*** Mudou Lote ***
			IF j<=uCrsBuracos.limite
				*** Trocou de Lote Antes do Limite ***
				lnBuraco = j
				taLoteReceita[1] = l
				taLoteReceita[2] = j
				EXIT
			ELSE
				*** Reset ao j ***
				j=1
				IF j<uCrsBuracos.nreceita
					lnBuraco = j
					taLoteReceita[1] = uCrsBuracos.lote
					taLoteReceita[2] = j
					EXIT
				ENDIF
			ENDIF
		ENDIF
		l=uCrsBuracos.lote
	ENDSCAN

	FECHA([uCrsBuracos])
	SELECT (lnAlias)
	RETURN lnBuraco
ENDFUNC


**
FUNCTION uf_receituario_corrigirReceitaAuto
	IF uf_receituario_verDoc()
		uf_facturacao_corrigirReceitaAuto()
	ENDIF 
ENDFUNC 


**
FUNCTION uf_receituario_buracos
	IF myPosto
		uf_perguntalt_chama("A Gest�o de Receitu�rio ter� que ser efectuada na farm�cia principal.","OK","",64)
		RETURN .f.
	ENDIF

	uf_receituario_sombra(.t.)
	uf_buracos_chama()
	uf_receituario_sombra(.f.)
ENDFUNC 



** funcionalidade para consultar estado das receitas por lote 
FUNCTION uf_receituario_ConsultarReceitas
	LOCAL lcLotesErrados, lcNomeJar, lcTestJar 
	STORE .f. TO lcLotesErrados 
	STORE '' TO lcNomeJar, lcTestJar
	
	&& verificar nr receita e token das receitas que est�o no lote seleccionado	
	&& funcionalidade dispon�vel apenas para os lotes 97,99 do SNS
	SELECT uCrsCloseLotes
	IF (ALLTRIM(uCrsCloseLotes.tlote) == '97' OR ALLTRIM(uCrsCloseLotes.tlote) == '99') AND ALLTRIM(uCrsCloseLotes.cptorgabrev) == 'SNS'
		lcSQL = ""
		TEXT TO lcSql NOSHOW TEXTMERGE 
			select receita, token, tlote from ctltrct (nolock) where ano = '<<uCrsCloseLotes.ano>>' 
					and mes = '<<uCrsCloseLotes.mes>>' 
					and cptorgabrev = '<<uCrsCloseLotes.cptorgabrev>>' 
					and tlote = '<<uCrsCloseLotes.tlote>>' 
					and lote = '<<uCrsCloseLotes.lote>>'
		ENDTEXT	
		IF !uf_gerais_actGrelha("","uCrsAuxRecConsulta",lcSQL)
			uf_perguntalt_chama("N�o foi poss�vel verificar a informa��o das receitas para consulta.","OK","", 32)
			RETURN .f.
		ELSE
			IF RECCOUNT("uCrsAuxRecConsulta") = 0
				uf_perguntalt_chama("N�o � poss�vel efetuar a consulta do lote seleccionado. Por favor contace o Suporte.","OK","", 32)
				RETURN .f.
			ENDIF	
		ENDIF
	ELSE
		&& N�o pertence aos lotes 97/99 do SNS
		uf_perguntalt_chama("Funcionalidade n�o dispon�vel para este tipo de Lote.","OK","", 32)
		RETURN .f.				
	ENDIF
	
	&& Valida��es Pr� consulta
	SELECT ucrsE1
	IF ALLTRIM(ucrsE1.tipoEmpresa) != "FARMACIA"
		uf_perguntalt_chama("FUNCIONALIDADE DISPON�VEL APENAS PARA CLIENTES DO TIPO FARM�CIA.","OK","",64)
		RETURN .t.
	ENDIF 
	
	&& Valida a liga��o � Internet 
	IF uf_gerais_verifyInternet() == .f.
		uf_perguntalt_chama("ESTA FUNCIONALIDADE REQUER LIGA��O � INTERNET."+CHR(13)+"POR FAVOR VERIFIQUE A SUA LIGA��O.","OK","",64)
		RETURN .f.
	ENDIF
	
	&& verifica a configura��o do c�digo de farm�cia
	SELECT ucrsE1
	IF EMPTY(ALLTRIM(ucrsE1.u_codfarm))
		uf_perguntalt_chama("C�digo de Farm�cia n�o configurado. Contacte o suporte.","OK","", 32)
		RETURN .f.
	ENDIF 
	
	&& C�digo Farm�cia 
	SELECT ucrsE1
	lcCodFarm = ALLTRIM(ucrsE1.u_codfarm)
	
	&& Datas - Neste cen�rios as datas v�o a vazio porque a pesquisa � feita pelo n� de receita
	lcDataIni = ''
	lcDataFim = ''
	
	regua(0,5,"A obter dados do Sistema Central de Prescri��es...")
	regua(1,1,"A obter dados do Sistema Central de Prescri��es...")


	&& verifica a exist�ncia do software DEM
	IF DIRECTORY(ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\DEM')

		&& verifica se utiliza webservice de testes
		lcNomeJar = "LtsDispensaEletronica.jar"
		IF uf_gerais_getParameter("ADM0000000227","BOOL") == .f.
			lcTestJar = "0"
		ELSE
			lcTestJar = "1"
		ENDIF 

		IF !FILE(ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\DEM\' + ALLTRIM(lcNomeJar))
			uf_perguntalt_chama("Ficheiro configura��o n�o encontrado. Por favor contacte o Suporte","OK","",64)
			RETURN .f.
		ENDIF 
		
*!*		Consulta:   consultaDispensas  CodFarmacia dataInicial dataFinal nrReceita dbServer dbName site
*!*		ex:  consultaDispensas  
*!*	         --CodFarmacia = 4600
*!*	         --dataInicial = ''
*!*	         --dataFinal = ''
*!*	         --nrReceita = zzzzzzzzzzzzz
*!*	         --dbServer = 172.20.5.4
*!*	         --dbName = ltdev29
*!*	         --Site = Loja 1
		
		&& verifica o servidor de BD
		IF !USED("uCrsServerName")
			TEXT TO lcSQL NOSHOW TEXTMERGE 
				Select @@SERVERNAME as dbServer
			ENDTEXT 
			IF !uf_gerais_actGrelha("", "ucrsServerName", lcSQL)
				uf_perguntalt_chama("N�o foi possivel proceder ao servi�o de Efetiva��o da Dispensa. Contacte o suporte." ,"OK","",32)
				regua(2)
				RETURN  .f.
			ENDIF
		ENDIF
				
		SELECT ucrsServerName
		lcDbServer = ALLTRIM(ucrsServerName.dbServer)
		
		&& Consulta todas as receitas do lote
		SELECT uCrsAuxRecConsulta
		GO TOP 
		SCAN 
			&& efetua consulta da receita	
			lcWsPath = ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\DEM\' + ALLTRIM(lcNomeJar) + ' consultaDispensas ';
				+ ' --codFarmacia=' + ALLTRIM(lcCodFarm);
				+ ' --dataIni=' + lcDataIni;
				+ ' --dataFim=' + lcDataFim;
				+ ' --nrReceita=' + ALLTRIM(uCrsAuxRecConsulta.receita);
				+ ' --dbServer=' + ALLTRIM(lcDbServer);
				+ ' --dbName=' +UPPER(ALLTRIM(sql_db));
				+ ' --site=' +UPPER(ALLTRIM(STRTRAN(mySite,' ','_')));
				+ ' "--token=' + ALLTRIM(uCrsAuxRecConsulta.token) + '"';
				+ ' --test=' + lcTestJar

			&& Informa que est� a usar webservice de testes
			IF uf_gerais_getParameter("ADM0000000227","BOOL") == .t.		
				**uf_perguntalt_chama("Webservice de teste..." ,"OK","",64)
			ENDIF 

			&& Envia comando Java
			lcWsPath = "javaw -jar " + lcWsPath 
				
			oWSShell = CREATEOBJECT("WScript.Shell")
			oWSShell.Run(lcWsPath,1,.t.)
			
			SELECT uCrsAuxRecConsulta

		ENDSCAN
		
		regua(2)
	ENDIF	
	
	&& verifica se alguma das receitas que est� no lote, est� alojada num lote diferente no registo central de receitas
	&& Consulta receitas / Lotes
	lcSQL = ""
	TEXT TO lcSql NOSHOW TEXTMERGE 
		select d.token, c.receita, d.lote as TipoLoteSNS, c.tlote as TipoLoteSoftware
		from ctltrct c (nolock)
		inner join dispensa_eletronica_d d (nolock) on c.token = d.token
		where c.ano = '<<uCrsCloseLotes.ano>>' 
		and c.mes = '<<uCrsCloseLotes.mes>>' 
		and c.cptorgabrev = '<<ALLTRIM(uCrsCloseLotes.cptorgabrev)>>' 
		and c.tlote = '<<uCrsCloseLotes.tlote>>' 
		and c.lote = '<<uCrsCloseLotes.lote>>'
	ENDTEXT	
	
	IF !uf_gerais_actGrelha("","uCrsAuxRecConsultaLoteSNS",lcSQL)
		uf_perguntalt_chama("N�o foi poss�vel verificar a informa��o das receitas para consulta.","OK","", 32)
		RETURN .f.
	ELSE
		&& Valida se tlote retornado da consultado � diferente do tipo de Lote no software
		SELECT uCrsAuxRecConsultaLoteSNS
		LOCATE FOR ALLTRIM(uCrsAuxRecConsultaLoteSNS.tipoloteSNS) != ALLTRIM(uCrsCloseLotes.tlote)
		IF FOUND()		
			lcLotesErrados = .t.
		ENDIF		
	ENDIF
	
	IF lcLotesErrados == .t.
		uf_perguntalt_chama("O lote consultado tem receitas que est�o colocadas num lote diferente no Registo Central de Prescri��es. Por favor consulte o detalhe do Lote. Obrigado.","OK","", 48)
	ELSE
		uf_perguntalt_chama("Todas as receitas est�o colocadas no lote correto segundo a informa��o proveniente do Registo Central de Prescri��es. Obrigado.","OK","", 64)
	ENDIF
	
	IF USED("uCrsAuxRecConsulta")	
		fecha("uCrsAuxRecConsulta")
	ENDIF
	
	IF USED("uCrsAuxRecConsultaLoteSNS")	
		fecha("uCrsAuxRecConsultaLoteSNS")
	ENDIF	
	
ENDFUNC 


** temp func corrigir receitu�rio [inserir RSPs manualmente] lu�s Leal 20160531
&&uf_temp_rec()

FUNCTION uf_temp_rec

	lcSQL = ""
	TEXT TO lcSql NOSHOW TEXTMERGE 
		SELECT * FROM AUXRECEITAS
		WHERE
			ANO = 2016
			AND MES = 3
			AND TLOTE = 97
			AND CPTORGABREV = 'SNS'
	ENDTEXT 
	
	IF !uf_gerais_actGrelha("","uCrsAux",lcSQL)
		uf_perguntalt_chama("N�o foi poss�vel verificar a informa��o das receitas para consulta.","OK","", 32)
		RETURN .f.
	ELSE
		SELECT uCrsAux
		GO TOP 
		SCAN 
			TEXT TO lcSql NOSHOW TEXTMERGE 
					declare @lote as numeric(10,0), @nreceita as numeric(4,0)
					set @lote = (select max(lote) from ctltrct where ano = 2016 and mes = 5 and tlote = 97 and cptorgabrev = 'SNS')
					set @nreceita = (select max(nreceita) from ctltrct where ano = 2016 and mes = 5 and tlote = 97 and cptorgabrev = 'SNS' and lote = @lote)
				

			INSERT INTO [ctltrct] (
				ctltrctstamp,cptorgstamp,cptorgabrev
				,lote,tlote,slote,ano,mes,nreceita
				,fechado,datafechado,horafechado,usrlote,cptplacode,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora
				,marcada,u_fistamp,site,no_orig,lote_orig,posto,receita,cartao,via,token,lote_dem)

				SELECT
					ctltrctstamp
					,cptorgstamp = LTRIM(RTRIM(cptorgstamp))
					,cptorgabrev
					,lote = case when @nreceita is null then 1 else
								case when @nreceita = 30 then @lote + 1 else @lote end end
					,tlote
					,slote = slote + 2
					,ano
					,mes = 5
					,nreceita = case when @nreceita is null then 1 else
										case when @nreceita = 30 then 1 else @nreceita + 1 end end
					,fechado = 0
					,datafechado = '1900-01-01 00:00:00.000'
					,horafechado = ''
					,usrlote = ''
					,cptplacode
					,ousrinis = 'ADM'
					,ousrdata = dateadd(HOUR, <<difhoraria>>, getdate())
					,ousrhora = CONVERT(TIME, dateadd(HOUR, <<difhoraria>>, getdate()))
					,usrinis = 'ADM'
					,usrdata = dateadd(HOUR, <<difhoraria>>, getdate())
					,usrhora = CONVERT(TIME, dateadd(HOUR, <<difhoraria>>, getdate()))
					,marcada = 0
					,u_fistamp = ''
					,site
					,no_orig
					,lote_orig
					,posto
					,receita,cartao,via,token,lote_dem
				FROM 
					AUXRECEITAS
				WHERE
					ctltrctstamp = '<<ALLTRIM(uCrsAux.ctltrctstamp)>>'
			ENDTEXT 
			
			IF !uf_gerais_actGrelha("","",lcSQL)
				uf_perguntalt_chama("N�o foi poss�vel terminar a opera��o","OK","", 32)
				RETURN .f.
			ENDIF
			
			SELECT uCrsAux
			
		ENDSCAN
		

	ENDIF
	
ENDFUNC 


** uf_receituario_validaLyrica() - LL/HT 20160606
** Nota: as comparticipa��es da lyrica deviam passar para a BD com valida��o por diploma
FUNCTION uf_receituario_validaLyrica
	LPARAMETERS tcRef, tcCodigo, lcTemVal2
	
	LOCAL lcCodx
	lcCodx = ALLTRIM(tcCodigo)
				
	lcSQL= ''
	TEXT TO lcSQL NOSHOW TEXTMERGE
		SELECT cftcode FROM b_cftfp (nolock) WHERE cftcode = '2.10' AND cnp = '<<ALLTRIM(tcRef)>>'
	ENDTEXT
	
	
	IF !uf_gerais_actgrelha("", [uCrsAuxLyrica], lcSQL)
		uf_perguntalt_chama("Ocorreu uma anomalia a verificar os pre�os do produto","OK","",16)
		RETURN .F.
	ELSE
		IF RECCOUNT("uCrsAuxLyrica") > 0
			IF lcTemVal2

				SELECT uCrsCptval2
				replace uCrsCptval2.grppreco WITH 'PVP'
				IF lcCodx == '48' OR lcCodx == '48A'
					replace uCrsCptval2.percentagem WITH 52
				ELSE
					IF lcCodx == '45A' OR lcCodx == '45B' OR lcCodx == '41' OR lcCodx == 'FN' OR lcCodx == '707' OR lcCodx == 'FMO' OR lcCodx == '019' OR lcCodx == 'S10' OR lcCodx == 'LS'
						replace uCrsCptval2.percentagem WITH 100
					ELSE
						replace uCrsCptval2.percentagem WITH 37
					ENDIF
				ENDIF
			ELSE

				SELECT uCrsCptval
				replace uCrsCptval.grppreco WITH 'PVP'
				IF lcCodx == '48' OR lcCodx == '48A'
					replace uCrsCptval.percentagem WITH 52
				ELSE
					IF lcCodx == '45A' OR lcCodx == '45B' OR lcCodx == '41' OR lcCodx == 'FN' OR lcCodx == '707' OR lcCodx == 'FMO' OR lcCodx == '019' OR lcCodx == 'S10' OR lcCodx == 'LS'
						replace uCrsCptval.percentagem WITH 100
					ELSE
						replace uCrsCptval.percentagem WITH 37
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDFUNC   


FUNCTION uf_receituario_devolve_valor_compart_sns_auto
	LPARAMETERS lnPvp, lcRef, lcPlano, lcDiploma, lcGroupProd, lnPref
	local lnPagoUtente, lcBase, lcGrpPreco
	
	lnPagoUtente = lnPvp
	lcGrpPreco = ""
	
	IF(!EMPTY(lcRef) and lnPvp> 0 AND !EMPTY(lcPlano))
		local lcSql 
		lcSql = ''
		
		fecha("uCrsCptValAuto")
		
		if(EMPTY(lcDiploma))
			lcDiploma = "xxx"
		ENDIF
		
		
		
		
		** Criar cursor - valores para plano 2		
		TEXT TO lcSql NOSHOW TEXTMERGE
			exec up_receituario_CalcCompart '<<ALLTRIM(lcDiploma)>>', <<lnPvp>>, '<<ALLTRIM(lcRef)>>', '<<ALLTRIM(lcPlano)>>', <<mysite_nr>>, <<0>>, '<<ALLTRIM(lcGroupProd)>>'
		ENDTEXT
		
				
		IF !uf_gerais_actgrelha("", [uCrsCptValAuto], lcSQL)			
			RETURN lnPagoUtente 
		ENDIF

		
		select uCrsCptValAuto
		GO TOP
		lcGrpPreco = alltrim(uCrsCptValAuto.grppreco)
		
		local lnBase 
		lnBase  = lnPvp
		
	
		
		
		** pre�o base de comparticipa��o	
		DO CASE
					
			CASE ALLTRIM(Upper(lcGrpPreco)) == "MNRP"
				local lnBase 
				local lnEnt1
				lnEnt1 = 0
				
				IF IsNull(lnPref)
					lnPref= 0
				ENDIF
				
				DO CASE
				    CASE lnPref == 0
				        lnBase = lnPvp
				    OTHERWISE
				        *-- Se ambos forem maiores que 0, escolhe o menor dos dois
				        lnBase = IIF(lnPvp> lnPref, lnPref, lnPvp)
				ENDCASE
				lnEnt1= Iif(lnPvp >= Round(lnBase*uCrsCptValAuto.percentagem/100,2), Round(lnBase*uCrsCptValAuto.percentagem/100,2), lnPvp)
				lnPagoUtente = lnPvp - lnEnt1 
											
				
			CASE ALLTRIM(Upper(lcGrpPreco)) == "PVP" 
				local lnBase 
				local lnEnt1
				lnEnt1 = 0
				lnBase=lnPvp
				lnEnt1 = Iif(lnPvp >= Round(lnBase*uCrsCptValAuto.percentagem/100,2), Round(lnBase*uCrsCptValAuto.percentagem/100,2), lnPvp)
				lnPagoUtente = lnPvp - lnEnt1 
				
			CASE  ALLTRIM(UPPER(lcGrpPreco)) == "VAL"
				lnPagoUtente  =   IIF(Round(uCrsCptValAuto.percentagem, 2 ) >= lnPvp, lnPvp, lnPvp - Round(uCrsCptValAuto.percentagem, 2 ))
				
			CASE  ALLTRIM(UPPER(lcGrpPreco)) == "VALF"
				lnPagoUtente  =  IIF(Round(uCrsCptValAuto.percentagem, 2 ) >= lnPvp, lnPvp, Round(uCrsCptValAuto.percentagem, 2 ))
				
			CASE ALLTRIM(Upper(lcGrpPreco)) == "REF" OR  ALLTRIM(Upper(lcGrpPreco)) == "PENS"
				local lnBase 
				local lnEnt1
				lnEnt1 = 0
				lnBase=lnPvp
				IF IsNull(lnPref)
					lnPref= 0
				ENDIF
				DO CASE
				    CASE lnPref == 0
				        lnBase = lnPvp
				    OTHERWISE
				        lnBase = lnPref
				ENDCASE
	

				lnEnt1 = Iif(lnPvp >= Round(lnBase*uCrsCptValAuto.percentagem/100,2), Round(lnBase*uCrsCptValAuto.percentagem/100,2), lnPvp)

				lnPagoUtente = lnPvp - lnEnt1
				
			Case ALLTRIM(Upper(lcGrpPreco)) == "ESP"
				lnPagoUtente = lnPvp 
						
			OTHERWISE
			
		       lnPagoUtente = lnPvp 
		ENDCASE 
		

		
	ENDIF
	


	
	IF lnPagoUtente>lnPvp OR lnPagoUtente<0 
		lnPagoUtente = lnPvp
	ENDIF


	
	RETURN lnPagoUtente 

ENDFUNC





**  Simula Compart SNS
FUNCTION uf_receituario_simula_sns
   LPARAMETERS lcTemVal2, lnBase1, lnBase2, lnPvp 
   
   
   
   LOCAL lnTotalPvpLinha , lnPagoEntidade, lnPagoUtente, lcPrecisao
   lnPagoUtente = 0
   lnTotalPvpLinha  = 0
   lnPagoUtente = 0
   lcPrecisao = uf_gerais_getParameter("ADM0000000373","NUM")
   


  
   **Fluxo SNS primeira entidade
   if(!lcTemVal2 and USED("uCrsCptval"))
		SELECT uCrsCptval
		GO TOP
		DO CASE
			CASE ALLTRIM(Upper(uCrsCptval.grppreco)) == "VAL"
				
				
				&& calcular o total da linha, se for negativo a comparticipa��o = pvp
				lnTotalPvpLinha = Round(lnBase1 - uCrsCptVal.percentagem, lcPrecisao )
				if(lnTotalPvpLinha<0)
					lnTotalPvpLinha  = lnPvp 
				ENDIF
				
		
				lnPagoEntidade = Iif(lnPvp > lnTotalPvpLinha , Round(uCrsCptVal.percentagem, lcPrecisao ), lnPvp)
				lnPagoUtente = lnPvp - lnPagoEntidade 
				

				
			CASE ALLTRIM(Upper(uCrsCptval.grppreco)) == "VALF" && o valor FINAL a pagar pelo utente, independentemente do pre�o do medicamento
				lnPagoEntidade  = IIF(Round(uCrsCptVal.percentagem, lcPrecisao ) >= lnPVP, lnPVP, lnPvp - Round(uCrsCptVal.percentagem, lcPrecisao ))
				lnPagoUtente  = IIF(Round(uCrsCptVal.percentagem, lcPrecisao ) >= lnPVP, 0, Round(uCrsCptVal.percentagem, lcPrecisao ))

			OTHERWISE
				** alterei a regra aqui, por causa dos valores negativos que v�m do ppens>pvp
				lnPagoEntidade  = Iif(lnPvp >= Round(lnBase1*uCrsCptVal.percentagem/100,lcPrecisao ), Round(lnBase1*uCrsCptVal.percentagem/100,lcPrecisao ), lnPvp)
				lnPagoUtente = lnPvp - lnPagoEntidade 


		ENDCASE
	

	ENDIF
	


	**Fluxo SNS segunda  entidade
	if(lcTemVal2 and USED("uCrsCptval2"))
		SELECT uCrsCptval
		GO TOP
		DO CASE
			CASE ALLTRIM(Upper(uCrsCptval2.grppreco)) == "VAL"
				
				
				&& calcular o total da linha, se for negativo a comparticipa��o = pvp
				lnTotalPvpLinha = Round(lnBase2 - uCrsCptval2.percentagem, lcPrecisao )
				if(lnTotalPvpLinha<0)
					lnTotalPvpLinha  = lnPvp 
				ENDIF
				
		
				lnPagoEntidade = Iif(lnPvp > lnTotalPvpLinha , Round(uCrsCptval2.percentagem, lcPrecisao ), lnPvp)
		
			CASE ALLTRIM(Upper(uCrsCptval2.grppreco)) == "VALF" && o valor FINAL a pagar pelo utente, independentemente do pre�o do medicamento
				lnPagoEntidade  = IIF(Round(uCrsCptval2.percentagem, lcPrecisao ) >= lnPVP, lnPVP, lnPvp - Round(uCrsCptval2.percentagem, lcPrecisao ))
				lnPagoUtente  = IIF(Round(uCrsCptval2.percentagem, lcPrecisao ) >= lnPVP, 0, Round(uCrsCptval2.percentagem, lcPrecisao ))

			OTHERWISE
				** alterei a regra aqui, por causa dos valores negativos que v�m do ppens>pvp
				
				
				lnPagoEntidade  = Iif(lnPvp >= Round(lnBase2*uCrsCptval2.percentagem/100,lcPrecisao ), Round(lnBase2*uCrsCptval2.percentagem/100,lcPrecisao ), lnPvp)
				lnPagoUtente = lnPvp - lnPagoEntidade 


		ENDCASE
		
	ENDIF
	
	


	IF(lnPagoUtente<0)
		lnPagoUtente= 0
	ENDIF
	IF(lnPagoEntidade<0)
		lnPagoEntidade= 0
	ENDIF
	

	
	fecha("ucrsTempCompartBonusAux")
		
    CREATE CURSOR ucrsTempCompartBonusAux(pagoUtente N(16,4), pagoEntidade N(16,4))
	INSERT INTO ucrsTempCompartBonusAux values (lnPagoUtente, lnPagoEntidade)
	



	return .t.

ENDFUNC   


** Calcula a comparticipa��o pelos valores do devolvido pelo estado
FUNCTION uf_receituario_validaCompartDEM
	LPARAMETERS tcRef, tcCodigo, lcTemVal2, tcIdExt,  lnPvp, lcIdDispensa, lcTokenDem, lnPref   
	
	
	LOCAL lnPagoSns 
	lnPagoSns  = 0
	lnPagoSns  = uf_receituario_devolver_percentagem_compart_dem(lcIdDispensa,lcTokenDem,0,lnPvp,lnPref)
	

	** garantir que n�o comparticipa o que nao deve
	IF(lnPagoSns)>lnPvp
		lnPagoSns  = 0
	ENDIF
	
	
		
	if(!EMPTY(lcTemVal2))
		SELECT  uCrsCptval2
		GO TOP
		replace  percentagem with lnPagoSns  
		replace  grppreco with  "VAL" 
	ELSE
		SELECT  uCrsCptval
		GO TOP
		replace  percentagem with lnPagoSns  
		replace  grppreco with  "VAL"   			
	ENDIF
	
	


ENDFUNC




** calcula bonus sobre a comparticipacao da entidade 
FUNCTION uf_receituario_validaCompartBonus
	LPARAMETERS tcRef, tcCodigo, lcTemVal2, lcTemBonus, lcTipoDescontoFinal, tcIdExt, isSNS, lnBase1, lnBase2, lnPvp, lcIdDispensa, lcTokenDem, lnPref      
	
	

	
	LOCAL lcPagoUtenteAntesBonus, lcPagoEntidadeAntesBonus, lnBase1AsDecimal, lnBase2AsDecimal, lnPvpDecimal, lcPrecisaoPerc
	lcPagoUtenteAntesBonus = 0
	lcPagoEntidadeAntesBonus = 0

	
	lcPrecisaoPerc  = uf_gerais_getParameter("ADM0000000374","NUM")


	lnBase1AsDecimal  = uf_gerais_moneyToDecimal(lnBase1)
	lnBase2AsDecimal  = uf_gerais_moneyToDecimal(lnBase2)
	lnPvpDecimal      = uf_gerais_moneyToDecimal(lnPvp)

	LOCAL lcPagoUtenteDepoisBonus, lcPagoEntidadeDepoisBonus, lcCompartTxDepoisBonus
	lcPagoUtenteDepoisBonus = 0
	lcPagoEntidadeDepoisBonus = 0
	lcCompartTxDepoisBonus = 0
	
	



	if(EMPTY("lcTipoDescontoFinal") or  !USED("ucrsCompartBonus") and !EMPTY(isSNS) and lnPvpDecimal<=0)
		RETURN .f.
	ENDIF
	
	


	
	** simula a comparticipa��o do SNS
	if(!EMPTY(isSNS))
		
		
	    uf_receituario_simula_sns(lcTemVal2, lnBase1AsDecimal, lnBase2AsDecimal, lnPvpDecimal)
	    

	    
	    if(!USED("ucrsTempCompartBonusAux"))
			RETURN .f.
		ENDIF
		
		
		SELECT ucrsTempCompartBonusAux
		GO top
		lcPagoUtenteAntesBonus  =  ucrsTempCompartBonusAux.pagoUtente
		lcPagoEntidadeAntesBonus =  ucrsTempCompartBonusAux.pagoEntidade 

		fecha("ucrsTempCompartBonusAux")
	
			
		if(lcPagoEntidadeAntesBonus <0)			
			RETURN .f.
		ENDIF			
	ENDIF
	
	LOCAL lcValorACompart 
	lcValorACompart = 0 
	
	

	if(lcTemVal2)
		lcValorACompart =  lnBase2AsDecimal
	ELSE
		lcValorACompart =  lnBase1AsDecimal
	ENDIF
	



	
	if(!EMPTY(lcTipoDescontoFinal) AND USED("ucrsCompartBonus"))
		SELECT ucrsCompartBonus
		GO top
		DO CASE

			CASE ALLTRIM(Upper(ucrsCompartBonus.grppreco)) == "RMP"
				
				
				
				LOCAL uv_sel
				TEXT TO uv_sel TEXTMERGE NOSHOW
					EXEC up_calc_compart_bonus <<lcPagoEntidadeAntesBonus>>, <<lcPagoUtenteAntesBonus>>, <<ucrsCompartBonus.compart>>
				ENDTEXT

			
				IF !uf_gerais_actGrelha("", "uc_calcCompartBas", uv_sel)
					RETURN .F.
				ENDIF

				
				SELECT uc_calcCompartBas
				
				lcPagoEntidadeDepoisBonus = uc_calcCompartBas.valor &&lcPagoEntidadeAntesBonus + Round(lcPagoUtenteAntesBonus*ucrsCompartBonus.compart/100,4)
				lcPagoUtenteDepoisBonus = lnPvp- lcPagoEntidadeDepoisBonus
				
				FECHA("uc_calcCompartBas")

			OTHERWISE
		
		ENDCASE

	ENDIF
	
	
	

	
	fecha("ucrsTempCompartBonusAux")
	fecha("ucrsCompartBonus")
	
	
	lcCompartTxDepoisBonus  = (lcPagoEntidadeDepoisBonus/lnPvp) * 100
	



	if(lcCompartTxDepoisBonus<0 or lcCompartTxDepoisBonus>100 or lcValorACompart <=0)
		RETURN .f.  
	ENDIF
	
	


	
	if(uf_gerais_getParameter_site('ADM0000000215', 'BOOL', mySite))
		lcPagoEntidadeDepoisBonus = uf_receituario_devolver_percentagem_compart_dem(lcIdDispensa,lcTokenDem,lcPagoEntidadeDepoisBonus,lnPvp,lnPref)
	ENDIF
	
	

	

	
	if(!EMPTY(lcTemVal2))
		SELECT  uCrsCptval2
		GO TOP
		replace  percentagem with lcPagoEntidadeDepoisBonus
		replace  grppreco with  "VAL" 
	ELSE
		SELECT  uCrsCptval
		GO TOP
		replace  percentagem with lcPagoEntidadeDepoisBonus
		replace  grppreco with  "VAL"   			
	ENDIF
	
	if(USED("uCrsCptval"))	
		SELECT uCrsCptval
	ENDIF

	
	if(USED("uCrsCptval2"))	
		SELECT uCrsCptval2
	ENDIF
	
	
	
	RETURN .t.

ENDFUNC   

FUNCTION uf_receituario_devolver_percentagem_compart_dem
    LPARAMETERS lcIdDem, lcToken, lnValorCalculadoSoftware, lnPvp, lnPref
    
    LOCAL lnValResult
    lnValResult = lnValorCalculadoSoftware
    
    LOCAL lcEcra  
    lcEcra = 0
    

    
     **COMPART AUTO DEM
	IF (TYPE("atendimento")<>"U")
	 	lcEcra  = 1
	ENDIF
	IF (TYPE("facturacao")<>"U")
	 	lcEcra = 2
	ENDIF
 


    LOCAL lcSqlEntidades
    lcSqlEntidade = ''
    
    fecha("urcsDadosPerc")
    
    if(EMPTY(lnPref) OR ISNULL(lnPref))
    	lnPref= 0
    endif	

    
    if(EMPTY(lnPvp) OR ISNULL(lnPref))
    	lnPvp= 0
    endif	
    

    TEXT TO lcSqlEntidade TEXTMERGE NOSHOW
        EXEC up_calc_compart_dem '<<ALLTRIM(lcIdDem)>>', <<lnValorCalculadoSoftware>>, '<<ALLTRIM(lcToken)>>', <<lcEcra>>, <<lnPvp>>, <<lnPref>>
    ENDTEXT
    
    &&debug mode
    IF emdesenvolvimento
    	_cliptext = lcSqlEntidade 
    ENDIF
    
    


    IF !uf_gerais_actGrelha("", "urcsDadosPerc", lcSqlEntidade)
        RETURN .F.
    ENDIF

    if(USED("urcsDadosPerc"))
        SELECT urcsDadosPerc
        GO TOP
        lnValResult = urcsDadosPerc.valorFinal
    ENDIF      
    

        
    fecha("urcsDadosPerc")    
    RETURN  lnValResult
ENDFUNC



** funcao para reinserir receitas devolvidas relativas a meses anteriores 
FUNCTION uf_receituario_reinserirReceita
	
	IF uf_receituario_verDoc()
		uf_facturacao_reinserirReceita()
	ENDIF 
	
ENDFUNC 


** funcao que permite emitir Guias de Fatura
FUNCTION uf_receituario_chamaGuiaFatura 
	LOCAL lcReport, lcSQL, lcAno, lcMes , lcCptorgabrev
	STORE '' TO lcReport, lcSQL,lcCptorgabrev
	STORE 0 TO lcAno, lcMes
	
	&& Path para report i cursor com informa��o dos duplicados

	
	TEXT TO lcSQL TEXTMERGE NOSHOW 
		SELECT * FROM b_impressoes (nolock) WHERE documento = 'Guia Fatura'
	ENDTEXT
	IF !uf_gerais_actgrelha("", "uCrsInfoReportGuiaFatura", lcSql)
		uf_perguntalt_chama("Ocorreu um erro a gerar dados para impress�o.", "OK", "", 16)
		RETURN .f.
	ENDIF
	
	&& variaveis para SP
	lcAno = RECEITUARIO.spinnerAno.value
	lcMes = uf_gerais_getMonthComboBox("RECEITUARIO.cbMes")
	IF !EMPTY(uCrsCloseLotes.Cptorgabrev)
		lcCptorgabrev = ALLTRIM(uCrsCloseLotes.Cptorgabrev)
	ENDIF 
	SELECT uCrsCloseLotes
	&& calc da informa��o
	TEXT TO lcSQL TEXTMERGE NOSHOW 
		exec up_receituario_GuiaFatura <<lcAno>>, <<lcMes>>, '<<lcCptorgabrev>>',  '<<ALLTRIM(mySite)>>'
	ENDTEXT

	IF !uf_gerais_actgrelha("", "uCrsInfoGuiaFatura", lcSql)
		uf_perguntalt_chama("Ocorreu um erro a gerar dados para impress�o.", "OK", "", 16)
		RETURN .f.
	ELSE
		SELECT uCrsInfoGuiaFatura
		IF !RECCOUNT("uCrsInfoGuiaFatura") > 0
			uf_perguntalt_chama("N�o foram encontrados dados para impress�o. Por favor verifique.", "OK", "", 48)
			RETURN .f.
		ENDIF
 		IF uCrsInfoGuiaFatura.fno = 0
			uf_perguntalt_chama("Ainda n�o foi emitida fatura para o periodo seleccionado. Por favor verifique.", "OK", "", 48)
			RETURN .f. 		
 		ENDIF
	ENDIF
	
	IF (!EMPTY(uCrsCloseLotes.e_datamatrix))
		if(!EMPTY(uCrsCloseLotes.e_datamatrix))
			uf_gerais_criaCursoresDataMatrix_Entidades(uCrsCloseLotes.Cptorgstamp,uCrsCloseLotes.u_fistamp,uCrsInfoGuiaFatura.ndoc)
	
			lcReport = ALLTRIM(mypath)+"\analises\report_receituario_guiafatura_entidades.frx"
		ELSE 
			uf_perguntalt_chama("N�o existe valores necessarios para imprimir o documento. Por favor verifique.", "OK", "", 48)
			RETURN .f.
		ENDIF 
	ELSE 
		lcReport = ALLTRIM(mypath)+"\analises\report_receituario_guiaFatura.frx"
	
	ENDIF 
	
	

	&& impress�o documento
	lcPrinter = Getprinter()
	IF(EMPTY(lcPrinter))
		RETURN .F.
	ENDIF
	lcDefaultPrinter=set("printer",2)
		
	SET Printer To Name ''+lcPrinter+''
	SET Device To print
	SET Console off
	SET REPORTBEHAVIOR 90

	SELECT uCrsInfoReportGuiaFatura
	GO TOP 
	SELECT uCrsE1
	SELECT uCrsInfoGuiaFatura
	GO TOP

	IF uCrsInfoReportGuiaFatura.temduplicados == .t. And uCrsInfoReportGuiaFatura.numduplicados > 0
		TRY		
			FOR i=0 TO uCrsInfoReportGuiaFatura.numduplicados			
				DO CASE 
					CASE i == 0
						myVersaoDoc = uf_gerais_versaodocumento(1)	
						REPORT FORM ALLTRIM(lcreport) TO PRINTER NOPAGEEJECT 
					CASE i < uCrsInfoReportGuiaFatura.numduplicados			
						myVersaoDoc = uf_gerais_versaodocumento(i+1)	
						REPORT FORM ALLTRIM(lcreport) TO PRINTER NOPAGEEJECT
					CASE i == uCrsInfoReportGuiaFatura.numduplicados			
						myVersaoDoc = uf_gerais_versaodocumento(uCrsInfoReportGuiaFatura.numduplicados+1)
						REPORT FORM ALLTRIM(lcreport) TO PRINTER 
				ENDCASE 
			ENDFOR
		CATCH
			uf_perguntalt_chama("Foi detectado um problema com a impressora." + CHR(13) + "Se o problema persistir por favor contacte o Suporte.","OK","",64)
		ENDTRY
	ELSE
		TRY		
			REPORT FORM ALLTRIM(lcreport) TO PRINTER 
		CATCH
			uf_perguntalt_chama("Foi detectado um problema com a impressora." + CHR(13) + "Se o problema persistir por favor contacte o Suporte.","OK","",64)
		ENDTRY
	ENDIF
	
	** por impressora no default 
	SET Device TO SCREEN 
	SET printer TO NAME ''+lcDefaultPrinter+''
	SET Console ON 
	
	IF USED("uCrsInfoGuiaFatura")
		fecha("uCrsInfoGuiaFatura")
	ENDIF
	IF USED("uCrsInfoReportGuiaFatura")
		fecha("uCrsInfoReportGuiaFatura")
	ENDIF
	
ENDFUNC 

FUNCTION uf_receituario_aplica_valores_receita
	LPARAMETERS lcfireceita, lcfibistamp, lcfiref
	
	LOCAL LcLetiliquido, LcLepv, LcLepvori, LcLu_epvp, LcLu_ettent1, LcLu_ettent2, LcLu_txcomp, lcResult, lcSQL 
	
	
	**se tem receita
	lcResult = .f.
	
		&& calc da informa��o
	TEXT TO lcSQL TEXTMERGE NOSHOW 
		exec up_receituario_retorna_valores_insercao '<<ALLTRIM(lcfireceita)>>', '<<ALLTRIM(lcfiref)>>', '<<ALLTRIM(lcfibistamp)>>'
	ENDTEXT
	
	
	uf_gerais_actgrelha("", "uCrsDadoaReceitaRes", lcSql)
	IF emdesenvolvimento
    	_cliptext = lcSQL 
    ENDIF
	
	**se encontrar a receita associada
	if(RECCOUNT("uCrsDadoaReceitaRes")>0)
	
		SELECT uCrsDadoaReceitaRes
		LcLetiliquido = uCrsDadoaReceitaRes.etiliquido
		LcLepv = uCrsDadoaReceitaRes.epv
		LcLepvori = uCrsDadoaReceitaRes.epvori
		LcLu_epvp = uCrsDadoaReceitaRes.u_epvp
		LcLu_ettent1 = uCrsDadoaReceitaRes.u_ettent1
		LcLu_ettent2 = uCrsDadoaReceitaRes.u_ettent2
		LcLu_txcomp  = uCrsDadoaReceitaRes.u_txcomp 
		
		SELECT fi
		replace fi.etiliquido  WITH LcLetiliquido 
		replace fi.epv WITH LcLepv 
		replace fi.epvori WITH LcLepvori 
		replace fi.u_epvp WITH LcLu_epvp 
		replace fi.u_ettent1 WITH LcLu_ettent1 
		replace fi.u_ettent2 WITH LcLu_ettent2 
		replace fi.u_txcomp  WITH LcLu_txcomp  
		lcResult  = .t.
	ENDIF
	

	
	fecha("uCrsDadoaReceitaRes")
	
	RETURN lcResult 
	
ENDFUNC

**uf_receituario_ConsultarReceitasDispensadas('1011000007722690106','','')
FUNCTION uf_receituario_ConsultarReceitasDispensadas
	LPARAMETERS lcNrReceita, lcNrLinhaReceita, lcDiaPesquisar
	
	LOCAL lcNomeJar, lcTestJar 
	STORE '' TO lcNomeJar, lcTestJar
	
	IF(EMPTY(lcNrReceita))
		lcNrReceita = ""
	ENDIF
	IF(EMPTY(lcNrLinhaReceita))
		lcNrLinhaReceita= ""
	ENDIF
	IF(EMPTY(lcDiaPesquisar))
		lcDiaPesquisar= "19000101"
	ENDIF
	
	IF(!USED("ucrsE1"))
		RETURN .F.
	ENDIF

	&& Valida��es Pr� consulta
	SELECT ucrsE1
	IF ALLTRIM(ucrsE1.tipoEmpresa) != "FARMACIA"
		uf_perguntalt_chama("FUNCIONALIDADE DISPON�VEL APENAS PARA CLIENTES DO TIPO FARM�CIA.","OK","",64)
		RETURN .t.
	ENDIF 
	
	&& Valida a liga��o � Internet 
	IF uf_gerais_verifyInternet() == .f.
		uf_perguntalt_chama("ESTA FUNCIONALIDADE REQUER LIGA��O � INTERNET."+CHR(13)+"POR FAVOR VERIFIQUE A SUA LIGA��O.","OK","",64)
		RETURN .f.
	ENDIF
	
	&& verifica a configura��o do c�digo de farm�cia
	SELECT ucrsE1
	IF EMPTY(ALLTRIM(ucrsE1.u_codfarm))
		uf_perguntalt_chama("C�digo de Farm�cia n�o configurado. Contacte o suporte.","OK","", 32)
		RETURN .f.
	ENDIF 
	
	&& C�digo Farm�cia 
	SELECT ucrsE1
	lcCodFarm = ALLTRIM(ucrsE1.u_codfarm)
	
	regua(0,5,"A obter dados do Sistema Central de Prescri��es...")
	regua(1,1,"A obter dados do Sistema Central de Prescri��es...")

	&& verifica a exist�ncia do software DEM
	IF DIRECTORY(ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\DEM')
	
		&& verifica se utiliza webservice de testes
		lcNomeJar = "LtsDispensaEletronica.jar"
		IF uf_gerais_getParameter("ADM0000000227","BOOL") == .f.
			lcTestJar = "0"
		ELSE
			lcTestJar = "1"
		ENDIF 

		IF !FILE(ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\DEM\' + ALLTRIM(lcNomeJar))
			uf_perguntalt_chama("Ficheiro configura��o n�o encontrado. Por favor contacte o Suporte","OK","",64)
			RETURN .f.
		ENDIF 
		
*!*		Consulta:   consultaDispensas  CodFarmacia dataInicial nrReceita dbServer dbName site
*!*		ex:  consultaDispensas  
*!*	         --CodFarmacia = 4600
*!*	         --dataInicial = ''
*!*	         --nrReceita = zzzzzzzzzzzzz
*!*			 --idLinha	= zzzz
*!*	         --dbServer = 172.20.5.4
*!*	         --dbName = ltdev29
*!*	         --Site = Loja 1
		
		&& verifica o servidor de BD
		IF !USED("uCrsServerName")
			TEXT TO lcSQL NOSHOW TEXTMERGE 
				Select @@SERVERNAME as dbServer
			ENDTEXT 
			IF !uf_gerais_actGrelha("", "ucrsServerName", lcSQL)
				uf_perguntalt_chama("N�o foi possivel proceder ao servi�o de Efetiva��o da Dispensa. Contacte o suporte." ,"OK","",32)
				regua(2)
				RETURN  .f.
			ENDIF
		ENDIF
			
		SELECT ucrsServerName
		lcDbServer = ALLTRIM(ucrsServerName.dbServer)
		
		&& Consulta todas as receitas do lote
			LOCAL lcToken, lcChavePedidoRelacionado
			lcToken = uf_gerais_gerarIdentifiers(36)
			lcChavePedidoRelacionado = uf_gerais_gerarIdentifiers(36)
			&& efetua consulta da receita	
			lcWsPath = ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\DEM\' + ALLTRIM(lcNomeJar) + ' consultaDispensas ';
				+ ' --codFarmacia=' + ALLTRIM(lcCodFarm);
				+ ' --dataIni=' + ALLTRIM(lcDiaPesquisar);
				+ ' --nrReceita=' + ALLTRIM(lcNrReceita);
				+ ' --idLinha=' + ALLTRIM(lcNrLinhaReceita);
				+ ' --dbServer=' + ALLTRIM(lcDbServer);
				+ ' --dbName=' +UPPER(ALLTRIM(sql_db));
				+ ' --site=' +UPPER(ALLTRIM(STRTRAN(mySite,' ','_')));
				+ ' --chavePedido= '+ ALLTRIM(lcChavePedidoRelacionado) ;
				+ ' --token= '+ALLTRIM(lcToken);
				+ ' --test=' + lcTestJar

			&& Envia comando Java
			lcWsPath = "javaw -jar " + lcWsPath 
		
			&& Informa que est� a usar webservice de testes
			IF uf_gerais_getParameter("ADM0000000227","BOOL") == .t.	
				_cliptext = lcWsPath 	
				uf_perguntalt_chama("Webservice de teste..." ,"OK","",64)
			ENDIF 
			
			oWSShell = CREATEOBJECT("WScript.Shell")
			oWSShell.Run(lcWsPath,1,.t.)
					
		regua(2)
	ENDIF	
	
	LOCAL lcSqlValidaResposta, lcSqlResLinha
	STORE '' TO lcsqlValidaResposta, lcSqlResLinha
    TEXT TO lcSqlValidaResposta TEXTMERGE NOSHOW
		exec up_dem_resultadoValidacaoLinhas '<<ALLTRIM(lcToken)>>'
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "ucrsValidaResposrtaLinha", lcSqlValidaResposta)
       uf_perguntalt_chama("N�o foi possivel verificar dados de valida��o da receita. Contacte o suporte.", "OK", "", 32)
       regua(2)
       RETURN .F.
    ENDIF
    
    uf_perguntalt_chama(ucrsValidaResposrtaLinha.resultado_consulta_descr, "OK", "", 32)
    
    **ALLTRIM(lcChavePedidoRelacionado)
    TEXT TO lcSqlResLinha TEXTMERGE NOSHOW
		exec up_dem_dadosReceita'<<ALLTRIM(lcChavePedidoRelacionado)>>'
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "ucrsRespLinha", lcSqlResLinha)
       uf_perguntalt_chama("N�o foi possivel verificar dados de valida��o da receita. Contacte o suporte.", "OK", "", 32)
       regua(2)
       RETURN .F.
    ENDIF
    
    IF(RECCOUNT("ucrsRespLinha")<1)
    	RETURN .F.
    ENDIF
    
ENDFUNC 


**uf_receituario_ConsultarAssinatura('1011000007722690106','')
FUNCTION uf_receituario_ConsultarAssinatura
    LPARAMETERS lcNrReceita, lcNrLinhaReceita

    LOCAL lcNomeJar, lcTestJar 
    STORE '' TO lcNomeJar, lcTestJar

    IF(EMPTY(lcNrReceita))
        uf_perguntalt_chama("O n�mero da linha n�o foi devolvido. Contacte o suporte por favor.","OK","",64)
        RETURN .F.
    ENDIF
    IF(EMPTY(lcNrLinhaReceita))
        uf_perguntalt_chama("O n�mero da receita n�o foi devolvido. Contacte o suporte por favor.","OK","",64)
        RETURN .F.
    ENDIF

    IF(!USED("ucrsE1"))
        RETURN .F.
    ENDIF

    && Valida��es Pr� consulta
    SELECT ucrsE1
    IF ALLTRIM(ucrsE1.tipoEmpresa) != "FARMACIA"
        uf_perguntalt_chama("FUNCIONALIDADE DISPON�VEL APENAS PARA CLIENTES DO TIPO FARM�CIA.","OK","",64)
        RETURN .t.
    ENDIF 

    && Valida a liga��o � Internet 
    IF uf_gerais_verifyInternet() == .f.
        uf_perguntalt_chama("ESTA FUNCIONALIDADE REQUER LIGA��O � INTERNET."+CHR(13)+"POR FAVOR VERIFIQUE A SUA LIGA��O.","OK","",64)
        RETURN .f.
    ENDIF

    && verifica a configura��o do c�digo de farm�cia
    SELECT ucrsE1
    IF EMPTY(ALLTRIM(ucrsE1.u_codfarm))
        uf_perguntalt_chama("C�digo de Farm�cia n�o configurado. Contacte o suporte.","OK","", 32)
        RETURN .f.
    ENDIF 

    && C�digo Farm�cia 
    SELECT ucrsE1
    lcCodFarm = ALLTRIM(ucrsE1.u_codfarm)

    regua(0,5,"A obter dados do Sistema Central de Prescri��es...")
    regua(1,1,"A obter dados do Sistema Central de Prescri��es...")

    && verifica a exist�ncia do software DEM
    IF DIRECTORY(ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\DEM')

        && verifica se utiliza webservice de testes
        lcNomeJar = "LtsDispensaEletronica.jar"
        IF uf_gerais_getParameter("ADM0000000227","BOOL") == .f.
            lcTestJar = "0"
        ELSE
            lcTestJar = "1"
        ENDIF 

        IF !FILE(ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\DEM\' + ALLTRIM(lcNomeJar))
            uf_perguntalt_chama("Ficheiro configura��o n�o encontrado. Por favor contacte o Suporte","OK","",64)
            RETURN .f.
        ENDIF 
        
        && verifica o servidor de BD
        IF !USED("uCrsServerName")
            TEXT TO lcSQL NOSHOW TEXTMERGE 
                Select @@SERVERNAME as dbServer
            ENDTEXT 
            IF !uf_gerais_actGrelha("", "ucrsServerName", lcSQL)
                uf_perguntalt_chama("N�o foi possivel proceder ao servi�o de Efetiva��o da Dispensa. Contacte o suporte." ,"OK","",32)
                regua(2)
                RETURN  .f.
            ENDIF
        ENDIF
            
        SELECT ucrsServerName
        lcDbServer = ALLTRIM(ucrsServerName.dbServer)
        
        && Consulta todas as receitas do lote
            LOCAL lcToken, lcChavePedidoRelacionado
            lcToken = uf_gerais_gerarIdentifiers(36)
            lcChavePedidoRelacionado = uf_gerais_gerarIdentifiers(36)
            && efetua consulta da receita	
            lcWsPath = ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\DEM\' + ALLTRIM(lcNomeJar) + ' consultaDispensas ';
                + ' --codFarmacia=' + ALLTRIM(lcCodFarm);
                + ' --nrReceita=' + ALLTRIM(lcNrReceita);
                + ' --idLinha=' + ALLTRIM(lcNrLinhaReceita);
                + ' --dbServer=' + ALLTRIM(lcDbServer);
                + ' --dbName=' +UPPER(ALLTRIM(sql_db));
                + ' --site=' +UPPER(ALLTRIM(STRTRAN(mySite,' ','_')));
                + ' --chavePedido= '+ ALLTRIM(lcChavePedidoRelacionado) ;
                + ' --token= '+ALLTRIM(lcToken);
                + ' --test=' + lcTestJar

            && Envia comando Java
            lcWsPath = "javaw -jar " + lcWsPath 
        
            && Informa que est� a usar webservice de testes
            IF uf_gerais_getParameter("ADM0000000227","BOOL") == .t.	
                _cliptext = lcWsPath 	
                uf_perguntalt_chama("Webservice de teste..." ,"OK","",64)
            ENDIF 
            
            oWSShell = CREATEOBJECT("WScript.Shell")
            oWSShell.Run(lcWsPath,1,.t.)
                    
        regua(2)
    ENDIF	

ENDFUNC 


&& garante que a compart nunca ultrapassa o pvp
&&uf_receituario_calcValorEntidadeSnsComBAS(9.03,6.86,9.03)
FUNCTION uf_receituario_calcValorEntidadeSnsComBAS
	LPARAMETERS lnValorCalculado, lnPvp 
	
	
	if(lnValorCalculado> lnPvp )
		RETURN lnPvp 
	ELSE
		RETURN lnValorCalculado	
	ENDIF
	

ENDFUNC





