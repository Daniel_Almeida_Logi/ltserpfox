** 
FUNCTION uf_corrigirreceita_chama
	LPARAMETERS lcStamp, lcStamp2, lcData, lcHideDelete

	LOCAL lcSQL, ll1, ll2
	STORE '' TO lcSQL
	STORE .f. TO ll1, ll2
	
	
	IF !EMPTY(lcStamp)
		lcSQL = ''
		TEXT TO lcSQl TEXTMERGE NOSHOW
			Select ano, mes, tlote, slote, lote, cptorgabrev, nreceita, cptplacode, ctltrctstamp, via, tlote.loteId as loteId
			FROM ctltrct (nolock)
			inner join tlote(nolock) on tlote.codigo=ctltrct.tlote
			WHERE ctltrctstamp = '<<ALLTRIM(lcStamp)>>'
		ENDTEXT 
		uf_gerais_actGrelha("","uCrsLote1",lcSql)
	ELSE
		lcSQL = ''
		TEXT TO lcSQl TEXTMERGE NOSHOW
			Select ano, mes, tlote, slote, lote, cptorgabrev, nreceita, cptplacode, ctltrctstamp, via, '' as loteId
			FROM ctltrct (nolock)
			WHERE ctltrctstamp = 'xxyyxx'
		ENDTEXT 
		uf_gerais_actGrelha("","uCrsLote1",lcSql)	
	ENDIF
	
	
	IF !EMPTY(lcStamp2)
		lcSQL = ''
		TEXT TO lcSQl TEXTMERGE NOSHOW
			Select ano, mes, tlote, slote, lote, cptorgabrev, nreceita, cptplacode, ctltrctstamp, via, tlote.loteId as loteId
			FROM ctltrct (nolock)
			inner join tlote(nolock) on tlote.codigo=ctltrct.tlote
			WHERE ctltrctstamp = '<<ALLTRIM(lcStamp2)>>'
		ENDTEXT 
		uf_gerais_actGrelha("","uCrsLote2",lcSql)
	ELSE
		lcSQL = ''
		TEXT TO lcSQl TEXTMERGE NOSHOW
			Select ano, mes, tlote, slote, lote, cptorgabrev, nreceita, cptplacode, ctltrctstamp, via, '' as loteId
			FROM ctltrct (nolock)
			WHERE ctltrctstamp = 'xxyyxx'
		ENDTEXT 
		uf_gerais_actGrelha("","uCrsLote2",lcSql)		
	ENDIF
	
	
	
	ll1 = (RECCOUNT([uCrsLote1])>0)
	

	ll2 = (RECCOUNT([uCrsLote2])>0)
	


	IF !ll1
		SELECT uCrsLote1
		DELETE ALL
		APPEND BLANK
		REPLACE uCrsLote1.ano WITH YEAR(lcData), uCrsLote1.mes WITH MONTH(lcData)
	ENDIF
	
	IF !ll2
		SELECT uCrsLote2
		DELETE ALL
		APPEND BLANK
		REPLACE uCrsLote2.ano WITH YEAR(lcData), uCrsLote2.mes WITH MONTH(lcData)
	ENDIF
	

	&& Abre o painel
	IF TYPE("corrigirreceita")=="U"
		DO FORM CORRIGIRRECEITA WITH ll1, ll2, lcHideDelete	
	ELSE	
		corrigirreceita.show
	ENDIF
	
	
ENDFUNC



** Fecha o painel
FUNCTION uf_corrigirreceita_sair
	If Used("uCrsLote1")
		fecha("uCrsLote1")
	Endif
	
	If Used("uCrsLote2")
		fecha("uCrsLote2")
	Endif
	
	If Used("uCrsLt")
		Fecha("uCrsLt")
	ENDIF
	
	IF USED("uCrsAuxLote")
		fecha("uCrsAuxLote")
	ENDIF
	
	&& fecha painel
	corrigirreceita.hide
	corrigirreceita.release
ENDFUNC

**
FUNCTION uf_corrigirreceita_gravar
	corrigirreceita.txtNReceita.setfocus
	IF (uf_gerais_validaPermUser(ch_userno,ch_grupo,'Gest�o de Receitu�rio - Alterar contador'))
		If uf_perguntalt_chama("Tem a certeza que pretende alterar a receita do plano " + ALLTRIM(STR(corrigirreceita.pageframe1.activepage)) + "?","Sim","N�o")
			uf_corrigirReceita_UpdateCtltrct(corrigirreceita.pageframe1.activepage)
		Endif		
	Else
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ALTERAR CONTADORES DE RECEITAS.","OK","",48)
		Return .F.
	Endif
ENDFUNC 

&&lcBool = .f. - painel corrigir receita
*	lcBool = .t. - painel corrigir buracos
FUNCTION uf_corrigirReceita_UpdateCtltrct
	LPARAMETERS tnCur, lcBool
	
	IF lcBool
		IF USED("uCrsReceitaMover") AND USED ("uCrsLotesTapar")
			TEXT TO lcSQL TEXTMERGE NOSHOW 
				select ftstamp, u_ltstamp,u_ltstamp2 
				from ft (nolock)
				where u_ltstamp='<<uCrsReceitaMover.ctltrctstamp>>' or u_ltstamp2='<<uCrsReceitaMover.ctltrctstamp>>'
			ENDTEXT 
			uf_gerais_actGrelha("","uCrsFtRecMov",lcSQL)
			IF RECCOUNT("uCrsFtRecMov") > 0
				SELECT uCrsFtRecMov
				GO TOP 
				IF !EMPTY(ALLTRIM(uCrsFtRecMov.u_ltstamp))
					tnCur = 1
					TEXT TO lcSQl TEXTMERGE NOSHOW
						Select ano, mes, tlote, slote, lote, cptorgabrev, nreceita, cptplacode, ctltrctstamp, via
						FROM ctltrct (nolock)
						WHERE ctltrctstamp = '<<ALLTRIM(uCrsFtRecMov.u_ltstamp)>>'
					ENDTEXT
					
					uf_gerais_actGrelha("","uCrsLote1",lcSQL)
					SELECT uCrsLote1
					replace ;
						uCrsLote1.lote WITH uCrsLotesTapar.lote ;
						uCrsLote1.nreceita WITH uCrsLotesTapar.nreceita
					
					buracos.verso = 1
				ELSE
					tnCur = 2
					TEXT TO lcSQl TEXTMERGE NOSHOW
						Select ano, mes, tlote, slote, lote, cptorgabrev, nreceita, cptplacode, ctltrctstamp, via
						FROM ctltrct (nolock)
						WHERE ctltrctstamp = '<<ALLTRIM(uCrsFtRecMov.u_ltstamp2)>>'
					ENDTEXT
					
					uf_gerais_actGrelha("","uCrsLote2",lcSQL)
					SELECT uCrsLote2
					replace ;
						uCrsLote2.lote WITH uCrsLotesTapar.lote ;
						uCrsLote2.nreceita WITH uCrsLotesTapar.nreceita
					
					buracos.verso = 2
				ENDIF
				buracos.ftstamp = uCrsFtRecMov.ftstamp
			ELSE
				uf_perguntalt_chama("N�o foi encontrada nenhuma venda associada � receita que pretende mover.", "OK", "", 64)
				RETURN .f.
			ENDIF 
		ELSE
			RETURN .f.
		ENDIF 
	ENDIF 
	
	LOCAL lcCur, lc, lcTran, lcVerificaContadores
	lcCur="uCrsLote" + ALLTRIM(STR(tnCur))
	lc=IIF(tnCur=1,"","2")
	lcTran = "TT" + Sys(2015)
	
	SELECT &lcCur
	GO TOP
	
	SELECT * FROM &lcCur INTO CURSOR uCrsAuxLote READWRITE
	
	SELECT uCrsAuxLote
	GO TOP
	if(!EMPTY(ALLTRIM(uCrsAuxLote.ctltrctstamp)))

	
		lcVerificaContadores = uf_corrigirReceita_VerificaContadores(lcCur)
		IF !lcVerificaContadores
			TEXT TO lcSql TEXTMERGE NOSHOW
				J� EXISTE A RECEITA N.� <<uCrsAuxLote.nreceita>> PARA:
				ANO:	<<uCrsAuxLote.ano>>
				M�S:	<<uCrsAuxLote.mes>>
				TIPO:	<<uCrsAuxLote.tlote>>
				S�RIE:	<<uCrsAuxLote.slote>>
				LOTE:	<<uCrsAuxLote.lote>>
			ENDTEXT
			uf_perguntalt_chama(lcSql,"OK","",64)
			RETURN .f.
		ENDIF
		
		** registo de ocorrencia **
		Local lcOstamp
		lcOstamp = uf_gerais_stamp()
		Text To lcSql Noshow textmerge
			Insert Into b_ocorrencias
				(stamp, linkStamp, tipo, grau
				,descr
				,oValor
				,usr, date)
			values
				('<<Alltrim(lcOstamp)>>', '<<Alltrim(uCrsAuxLote.ctltrctstamp)>>', 'Lotes', 2
				,'Posi��o Receita Alterada'
				,'Ano:<<uCrsAuxLote.ano>>' + ' Mes:<<uCrsAuxLote.mes>>' + ' Abrev:<<Iif(tnCur==1, Alltrim(oldval("cptorgabrev","uCrsLote1")), Alltrim(oldval("cptorgabrev","uCrsLote2")))>>' + ' TLote:<<Iif(tnCur==1, Alltrim(oldval("tlote","uCrsLote1")), Alltrim(oldval("tlote","uCrsLote2")))>>' + ' Lote:<<Iif(tnCur==1, Oldval("lote","uCrsLote1"), Oldval("lote","uCrsLote2"))>>' + ' Receita:<<Iif(tnCur==1, Oldval("nreceita","uCrsLote1"), Oldval("nreceita","uCrsLote2"))>>'
				,<<ch_userno>>, dateadd(HOUR, <<difhoraria>>, getdate()))
		Endtext
		If !uf_gerais_actGrelha("","",lcSql)
			****
		Endif
		**********************
		SET HOURS TO 24
		Atime=ttoc(datetime()+(difhoraria*3600),2)
		Astr=left(Atime,8)
		
		TEXT TO lcSql NOSHOW TEXTMERGE
			Begin Transaction <<lcTran>>
				Update ctltrct SET 
					lote=<<uCrsAuxLote.lote>>
					,nreceita=<<uCrsAuxLote.nreceita>>
					,usrinis='<<m_chinis>>'
					,usrdata='<<uf_gerais_getDate(Date(),"SQL")>>'
					,usrhora='<<Astr>>'
					,via = <<uCrsAuxLote.via>>
				WHERE ctltrctstamp='<<uCrsAuxLote.ctltrctstamp>>'
			
				Update ft SET 
					u_lote<<lc>>=<<uCrsAuxLote.lote>>
					,u_nslote<<lc>>=<<uCrsAuxLote.nreceita>>
				WHERE u_ltstamp<<lc>>='<<uCrsAuxLote.ctltrctstamp>>'
				
				Update ft2
				Set u_receita	= <<IIF(TYPE("corrigirReceita")!="U", "'"+Alltrim(corrigirReceita.txtNreceita.value)+"'", "ft2.u_receita")>>
					,u_nopresc	= <<IIF(TYPE("corrigirReceita")!="U", "'"+Alltrim(corrigirReceita.txtPrescritor.value)+"'", "ft2.u_nopresc")>>
					,u_entpresc	= <<IIF(TYPE("corrigirReceita")!="U", "'"+Alltrim(corrigirReceita.txtLPresc.value)+"'", "ft2.u_entpresc")>>
				from ft2
				inner join ft on ft.ftstamp=ft2.ft2stamp
				where ft.u_ltstamp<<lc>>='<<uCrsAuxLote.ctltrctstamp>>'
				
				If @@ERROR=0
					COMMIT TRANSACTION <<lcTran>>
				Else
					Begin
						RollBack Transaction <<lcTran>>
						RAISERROR('Erro na actualiza��o de Contadores',16,1)
			End
		ENDTEXT

		
		IF !uf_gerais_actGrelha("","",lcSql)
			&&Receitas Logs
			
			uf_perguntalt_chama("OCORREU UM ERRO A ACTUALIZAR POSI��O DA RECEITA."+Chr(13)+"PROVAVELMENTE O REGISTO PARA O QUAL TENTOU ALTERAR, J� EXISTE.","OK","",48)
			RETURN .f.
		ELSE
				
			LOCAL lcSqlSafe
			lcSqlSafe=lcSql
			uf_gerais_registaErroLog("SQL Receitas logs: " + ALLTRIM(lcSqlSafe), "R7")
			** update de ocorrencia **
			Text To lcSql Noshow textmerge
				Update b_ocorrencias
				Set dValor = 'Ano:<<uCrsAuxLote.ano>>' + ' Mes:<<uCrsAuxLote.mes>>' + ' Abrev:<<Alltrim(uCrsAuxLote.cptorgabrev)>>' + ' TLote:<<Alltrim(uCrsAuxLote.tlote)>>' + ' Lote:<<uCrsAuxLote.lote>>' + ' Receita:<<uCrsAuxLote.nreceita>>'
				where stamp = '<<Alltrim(lcOstamp)>>'
			Endtext
			If !uf_gerais_actGrelha("","",lcSql)
				***
			Endif
			**********************
			IF !lcBool
				uf_perguntalt_chama("RECEITA ACTUALIZADA COM SUCESSO.","OK","",64)
				
				uf_corrigirReceita_sair()
			ELSE
				RETURN .t.
			ENDIF 
		ENDIF
	ENDIF
	
ENDFUNC 

**
FUNCTION uf_corrigirReceita_VerificaContadores
	LPARAMETERS lcCur
	
	LOCAL ano, mes, tlote, lote, cptorgabrev, nreceita, ctltrctstamp
	
	SELECT &lcCur
	ano = &lcCur..ano
	mes = &lcCur..mes
	tlote = &lcCur..tlote
	lote = &lcCur..lote
	cptorgabrev = &lcCur..cptorgabrev
	nreceita = &lcCur..nreceita
	ctltrctstamp = &lcCur..ctltrctstamp
	
	TEXT TO lcsql NOSHOW TEXTMERGE
		SELECT COUNT(*) ct FROM ctltrct (nolock) 
		WHERE 
			ano = <<ano>> 
			AND mes = <<mes>> 
			AND tlote = '<<ALLTRIM(tlote)>>' 
			AND lote = <<lote>>
			AND cptorgabrev = '<<ALLTRIM(cptorgabrev)>>' 
			AND nreceita = <<nreceita>>
			AND ctltrctstamp != '<<ALLTRIM(ctltrctstamp)>>'
			AND site in (select * from up_SplitToTable('<<ALLTRIM(uf_gerais_getSitePostos())>>',','))
	ENDTEXT
	
	IF !uf_gerais_actGrelha("",[uCrsLt],lcsql)
		uf_perguntalt_chama("ERRO NA VERIFICA��O DO CONTADOR:" +CHR(13)+lcSql,"OK","",16)
		RETURN .F.
	ENDIF
	
	IF uCrsLt.ct > 0 
		RETURN .F.
	ENDIF
	
	RETURN .t.
ENDFUNC



&& tcBool = .f. - painel corrigir receita
*	tcBool = .t. - corrigir receita automaticamente
FUNCTION uf_corrigirReceita_EliminarContadoresPar
	LPARAMETERS tcNum, tcBool
	LOCAL lcEntidade 

	IF EMPTY(tcNum)
		tcNum = CORRIGIRRECEITA.pageframe1.ActivePage
	ENDIF

	IF !(uf_gerais_validaPermUser(ch_userno,ch_grupo,'Gest�o de Receitu�rio - Eliminar receita'))
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ELIMINAR RECEITAS.","OK","",48)
		RETURN .F.
	ENDIF	
	
	LOCAL lcCur, lc, lcTran, lcVerificaContadores, lcFtStamp, lctoken_efectivacao_compl
	lcCur="uCrsLote" + ALLTRIM(STR(tcNum))
	lc=IIF(tcNum=1,"","2")
	lcTran = "TT" + Sys(2015)
	lcFtStamp = ''
	lctoken_efectivacao_compl=''
	
	
	
	&& 
	IF USED("ft") AND tcBool
		Select ft
		
		IF tcNum == 1 
			lcFtStamp = ft.u_ltstamp
		ELSE
			lcFtStamp = ft.u_ltstamp2
		ENDIF 
		TEXT TO lcSQl TEXTMERGE NOSHOW
			Select ano, mes, tlote, slote, lote, cptorgabrev, nreceita, cptplacode, ctltrctstamp
			FROM ctltrct (nolock)
			WHERE ctltrctstamp = '<<ALLTRIM(lcFtStamp)>>'
		EndText
		uf_gerais_actGrelha("","uCrsAuxLote",lcSql)
	ENDIF 
	
	IF USED("ft2")
		SELECT ft2
		lctoken_efectivacao_compl = ft2.token_efectivacao_compl
	ENDIF 
	
	&&
	IF !tcBool
		SELECT * FROM &lcCur INTO CURSOR uCrsAuxLote READWRITE
		SELECT uCrsAuxLote
		lcFtStamp = uCrsAuxLote.ctltrctstamp
	ENDIF
	
	SELECT uCrsAuxLote
	lcEntidade = uCrsAuxLote.cptorgabrev
	
	IF uf_gerais_actGrelha("",[uCrsChkRecFechada],[select fechado from ctltrct (nolock) where ctltrctstamp=']+Alltrim(lcFtStamp)+['])
		If Reccount()>0
			If uCrsChkRecFechada.fechado && verificar se o lote esta fechado
				uf_perguntalt_chama("N�O PODE ELIMINAR UMA RECEITA DE UM LOTE FECHADO.","OK","",48)
				Fecha("uCrsChkRecFechada")
				RETURN .F.
			ENDIF
		ENDIF
		Fecha("uCrsChkRecFechada")
	ENDIF
	
	IF !tcBool
		If !uf_perguntalt_chama("ATEN��O: VAI ELIMINAR ESTA RECEITA DOS LOTES. TEM A CERTEZA QUE PRETENDE CONTINUAR?","Sim","N�o")
			RETURN .f.
		ENDIF
	ENDIF
	
	** verifica��o adicional para anula��o das receitas - JC 25-01-2018
	lcSQL = ''
	IF tcNum == 1 
		TEXT TO lcSQL NOSHOW TEXTMERGE
			SELECT
				ftstamp 
				,ft2.token
				,ft2.u_receita
				,ft2.u_docorig
				,anulaReceita = case 
				when u_ltstamp='<<ALLTRIM(lcFtStamp)>>' and ft2.u_abrev!='' and ft2.token!='' then 1
				else 0
				end
			FROM
				ft (nolock)
				inner join ft2 (nolock) on ft.ftstamp = ft2.ft2stamp
			WHERE
				u_ltstamp='<<ALLTRIM(lcFtStamp)>>' OR  u_ltstamp2 = '<<ALLTRIM(lcFtStamp)>>'
			
			
		ENDTEXT
	ELSE
			TEXT TO lcSQL NOSHOW TEXTMERGE
			SELECT
				ftstamp 
				,ft2.token
				,ft2.u_receita
				,ft2.u_docorig
				,anulaReceita = case 
				when u_ltstamp2='<<ALLTRIM(lcFtStamp)>>' and ft2.u_abrev2='SNS' and ft2.token!='' then 1
				else 0
				end
			FROM
				ft (nolock)
				inner join ft2 (nolock) on ft.ftstamp = ft2.ft2stamp
			WHERE
				u_ltstamp='<<ALLTRIM(lcFtStamp)>>' OR  u_ltstamp2 = '<<ALLTRIM(lcFtStamp)>>'
			
			
		ENDTEXT

	ENDIF
	
	uf_gerais_actgrelha("", [uCrsDocFtAnul], lcSql)
	** fim verifica��o adicional
	SELECT uCrsDocFtAnul
	** Anular DEM
	SELECT Ft2
**	IF !EMPTY(ALLTRIM(Ft2.token)) AND UPPER(ALLTRIM(lcEntidade)) == "SNS" AND EMPTY(ft2.u_docorig) - Altera��o JC 25-01-2018
	IF   !EMPTY(ALLTRIM(Ft2.token)) AND EMPTY(ft2.u_docorig) AND uCrsDocFtAnul.anulaReceita==1 ;
		    AND  ((USED("uCrsLote1") AND tcNum == 1 AND (ALLTRIM(uCrsLote1.cptorgabrev)== "SNS" OR uf_gerais_getUmValor("cptpla", "mcdt", "codigo = '" + ALLTRIM(uCrsLote1.cptplacode) + "'"))) ; 
            OR (USED("uCrsLote2") AND tcNum == 2 AND ALLTRIM(uCrsLote2.cptorgabrev)== "SNS"))
		IF(!uf_atendimento_receitasAnular(Ft2.u_receita, Ft2.token))
			RETURN .f.
		ENDIF
	ENDIF 
	
	**Anular DEM MCDT
	**TODO
	**IF   !EMPTY(ALLTRIM(Ft2.token)) AND EMPTY(ft2.u_docorig) AND uCrsDocFtAnul.anulaReceita==1 
		
	**endif
	
	

	** Anular quando � uma receita sozinha sem o SNS	
	IF !EMPTY(ALLTRIM(Ft2.token)) AND EMPTY(ft2.u_docorig) ;
		AND  tcNum == 1 AND ALLTRIM(ft2.u_abrev)!= "SNS" AND ft.u_lote2=0 AND !EMPTY(ft.u_tlote) AND EMPTY(ft.u_tlote2)
		IF(!uf_atendimento_receitasAnular(Ft2.u_receita, Ft2.token))
			RETURN .f.
		ENDIF
	ENDIF 

	IF USED("uCrsDocFtAnul")
		Fecha("uCrsDocFtAnul")
	ENDIF 
	
	IF !EMPTY(lctoken_efectivacao_compl) AND ((tcNum == 1 AND ALLTRIM(uCrsLote1.cptorgabrev)<> "SNS") OR (tcNum == 2 AND ALLTRIM(uCrsLote2.cptorgabrev)<> "SNS"))
		uf_compart_anula(lctoken_efectivacao_compl, ft.ftstamp)
	ENDIF 

	lcSQL = ''		
	TEXT TO lcSql NOSHOW TEXTMERGE
		Begin Transaction <<lcTran>>
		
			Delete ctltrct
			where ctltrctstamp='<<lcFtStamp>>'
						
			UPDATE ft
			SET u_ltstamp<<lc>> = ''
				,u_slote<<lc>> = 0
			WHERE u_ltstamp<<lc>> = '<<lcFtStamp>>'
			
			If @@ERROR=0
				COMMIT TRANSACTION <<lcTran>>
			Else
				BEGIN
					RollBack Transaction <<lcTran>>
					RAISERROR('Erro na actualiza��o de Contadores',16,1)
		END
	ENDTEXT

	
	
	**_cliptext=lcSql
	If !uf_gerais_actGrelha("","",lcSql)

		uf_perguntalt_chama("OCORREU UM ERRO A ELIMINAR A RECEITA. POR FAVOR CONTACTE O SUPORTE","OK","",16)
	ELSE
		&&Receitas Logs		
		LOCAL lcSqlSafe
		lcSqlSafe=lcSql
		uf_gerais_registaErroLog("SQL Receitas logs: " + ALLTRIM(lcSqlSafe), "R6")
	
		IF !tcBool
			uf_perguntalt_chama("RECEITA ELIMINADA COM SUCESSO.","OK","",64)
		ENDIF
		

		
		LOCAL lcStamp
		lcStamp = uf_gerais_stamp()
		
		** registo de ocorrencia **
		Text To lcSql Noshow textmerge
			Insert Into b_ocorrencias
				(stamp, linkStamp, tipo, grau
				,descr
				,oValor
				,usr, date)
			values
				('<<Alltrim(lcStamp)>>', '<<lcFtStamp>>', 'Lotes', 2
				,'Receita Eliminada'
				,'Ano:<<uCrsAuxLote.ano>>' + ' Mes:<<uCrsAuxLote.mes>>' + ' Abrev:<<Alltrim(uCrsAuxLote.cptorgabrev)>>' + ' TLote:<<Alltrim(uCrsAuxLote.tlote)>>' + ' Lote:<<uCrsAuxLote.lote>>' + ' Receita:<<uCrsAuxLote.nreceita>>'
				,<<ch_userno>>, dateadd(HOUR, <<difhoraria>>, getdate()))
		ENDTEXT 
		If !uf_gerais_actGrelha("","",lcSql)
			***
		Endif
		**********************
		IF !tcBool
			uf_corrigirReceita_sair()
		ENDIF 
	ENDIF
ENDFUNC