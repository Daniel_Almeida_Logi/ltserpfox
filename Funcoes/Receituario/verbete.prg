**
** Fun��o Principal
**
FUNCTION uf_verbete_chama
	LPARAMETERS lcAno, lcMes
	LOCAL lcSQL

	&& Valida Parametros
	PUBLIC myVerbeteAno, myVerbeteMes
	
	IF EMPTY(lcANO)
		myVerbeteAno = ALLTRIM(str(YEAR(DATE())))
	ELSE
		myVerbeteAno = ALLTRIM(STR(lcAno))
	ENDIF
	
	IF EMPTY(lcMes)
		myVerbeteMes = MONTH(DATE())
	ELSE
		myVerbeteMes = lcMes
	ENDIF
	
	&& Abre o painel
	IF TYPE("verbete")=="U"
		DO FORM verbete
	ELSE
		verbete.show
	ENDIF
ENDFUNC



**
FUNCTION uf_verbete_carregaMenu
	verbete.menu1.adicionaopcao("previsao", "Previs�o", "prever_b.png", "uf_verbete_emite with .f.")
	verbete.menu1.adicionaopcao("imprimir", "Imprimir", "imprimir_b.png", "uf_verbete_emite with .t.")
ENDFUNC



**
FUNCTION uf_verbete_cursorEntidades
	LPARAMETERS lcAno, lcMes
	
	LOCAL lcSql
	lcSQL = ''
	
	verbete.lstEntidade.rowsource 	= ''
	
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_receituario_listEntidadesPorData <<lcAno>>, <<lcMes>>, , '<<ALLTRIM(mysite)>>'
	ENDTEXT
	If !uf_gerais_actgrelha("", "uCrsVerbeteEntidades", lcSql)
		Messagebox('OCORREU UM ERRO AO CALCULAR AS ENTIDADES! POR FAVOR CONTACTE O SUPORTE.',16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF
	
	verbete.lstEntidade.rowsource 		= 'uCrsVerbeteEntidades.cptorgabrev'
	verbete.lstEntidade.rowsourcetype 	= 2
	
	verbete.lstEntidade.refresh
ENDFUNC



**
FUNCTION uf_verbete_cursorTipoLote
	LPARAMETERS lcAno, lcMes
	
	LOCAL lcSql
	lcSQL = ''
	
	verbete.cmbTlote.rowsource 		= ''
	verbete.cmbTlote.value	 		= ''
	
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_receituario_TipoLote <<lcAno>>, <<lcMes>>, '<<ALLTRIM(verbete.lstEntidade.value)>>', '<<ALLTRIM(mySite)>>'
	ENDTEXT
	If !uf_gerais_actgrelha("", "uCrsVerbeteTLote", lcSql)
		Messagebox('OCORREU UM ERRO AO CALCULAR OS TIPOS DE LOTES! POR FAVOR CONTACTE O SUPORTE.', 16, "LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF
	
	verbete.cmbTlote.rowsource 		= 'uCrsVerbeteTlote.tlote'
	verbete.cmbTlote.rowsourcetype 	= 2
	
	verbete.cmbTlote.refresh
ENDFUNC



**
FUNCTION uf_verbete_cursorLote
	
	Local lcSql
	Store '' To lcSQL
	
	verbete.cmbLoteIni.rowsource	= ''
	verbete.cmbLoteFim.rowsource	= ''
	verbete.cmbLoteIni.value		= ''
	verbete.cmbLoteFim.value		= ''	
	
	IF USED("uCrsVerbeteLote")
		fecha("uCrsVerbeteLote")
	ENDIF
	
	If !(verbete.lstEntidade.value ==  "TODAS")
		lcSQL=""
		TEXT TO lcSQL TEXTMERGE NOSHOW
			exec up_receituario_Lote <<verbete.cmbAno.value>>, <<verbete.cmbMes.list(verbete.cmbMes.listindex,2)>>, '<<Alltrim(verbete.lstEntidade.value)>>', '<<Alltrim(verbete.cmbTLote.value)>>', '<<ALLTRIM(mySite)>>'
		ENDTEXT

		IF !uf_gerais_actgrelha("", "uCrsVerbeteLote", lcSql)
			RETURN
		ENDIF

		verbete.cmbLoteIni.init()
		verbete.cmbLoteIni.refresh()
		verbete.cmbLoteFim.init()
		verbete.cmbLoteFim.refresh()
	ELSE
		verbete.cmbLoteIni.value = ''
		verbete.cmbLoteIni.displayvalue = ''
		verbete.cmbLoteFim.value = ''
		verbete.cmbLoteFim.displayvalue = ''
	ENDIF

	verbete.cmbLoteIni.rowsource		= 'uCrsVerbeteLote.lote'
	verbete.cmbLoteIni.rowsourcetype 	= 2
	verbete.cmbLoteFim.rowsource		= 'uCrsVerbeteLote.lote'
	verbete.cmbLoteFim.rowsourcetype 	= 2

	verbete.cmbLoteIni.refresh
	verbete.cmbLoteFim.refresh
ENDFUNC



**  Selecciona o Ano
FUNCTION uf_verbete_selAno
	
	&& Mes
	verbete.cmbMes.Value 			= ''
	verbete.cmbMes.displayValue		= ''

	&& Entidades
	SELECT uCrsVerbeteEntidades
	DELETE all
	verbete.lstEntidade.refresh()
			
	&& Tipo de Lote
	SELECT uCrsVerbeteTlote
	DELETE all
	verbete.cmbTlote.refresh()
	
	&& lote
	SELECT uCrsVerbeteLote
	DELETE all
	verbete.cmbLoteIni.refresh()
	verbete.cmbLoteFim.refresh()
ENDFUNC




** Selecciona o Mes
FUNCTION uf_verbete_selMes

	IF empty(verbete.cmbAno.value)
		RETURN .F.
	ENDIF 
		
	&& Entidades
	&& Cursor de Entidades
	uf_verbete_cursorEntidades(verbete.cmbAno.value, verbete.cmbMes.list(verbete.cmbMes.listindex,2))
			
	&& Tipo de Lote
	SELECT uCrsVerbeteTlote
	DELETE all
	verbete.cmbTlote.refresh()
	
	&& lote
	SELECT uCrsVerbeteLote
	DELETE all
	verbete.cmbLoteIni.refresh()
	verbete.cmbLoteFim.refresh()
ENDFUNC 




** Selecciona uma Entidade 
FUNCTION uf_verbete_selEntidade
	
	IF empty(verbete.cmbAno.value)
		RETURN .F.
	ENDIF 

	LOCAL lcSql
	
	&& Tipo de Lote
	uf_verbete_cursorTipoLote(verbete.cmbAno.value, verbete.cmbMes.list(verbete.cmbMes.listindex,2))
	
	** Lote ini e Lote Fim
	uf_verbete_cursorLote()
ENDFUNC 


** Selecciona uma Entidade 
FUNCTION uf_verbete_selTLote
	
	IF ALLTRIM(verbete.cmbAno.value) == ''
		RETURN .F.
	ENDIF 
	IF ALLTRIM(verbete.cmbTlote.value) == ''
		RETURN .F.
	ENDIF 
	
	LOCAL lcSql
		
	** Lote ini e Lote Fim
	uf_verbete_cursorLote()
ENDFUNC 




**
FUNCTION uf_verbete_emite
	Lparameters tcBool

	Local lcPrinter, lcDefaultPrinter, lcSQL
	Store '' To lcSQL
	
	**guarda caminho para report
	SELECT uCrsPath
	GO TOP
	lcReport = Alltrim(uCrsPath.textvalue)+"\analises\report_receituario_verbete.frx"
	
	
	If !tcBool &&Previsao
		If  !(Alltrim(verbete.lstEntidade.value) == 'TODAS')
			IF UPPER(Alltrim(verbete.cmbTLote.value))=='TODOS' 
       			messagebox("N�O � POSS�VEL FAZER A PREVIS�O DE TODOS OS VERBETES!", 64, "LOGITOOLS SOFTWARE")
       			RETURN
			ELSE
      			If Alltrim(verbete.cmbLoteIni.value)== '' or Alltrim(verbete.cmbLoteFim.value)== ''
					messagebox("POR FAVOR PREENCHA TODOS OS CAMPOS!",64,"LOGITOOLS SOFTWARE")
       				RETURN
				ENDIF
       		ENDIF
       	ELSE
       		messagebox("N�O � POSS�VEL FAZER A PREVIS�O DE TODOS OS VERBETES!", 64, "LOGITOOLS SOFTWARE")
       		Return
		ENDIF
		
		** Dados da Empresa **
		SELECT uCrsE1

		lcSQl = ""
		TEXT TO lcSQl TEXTMERGE NOSHOW
			exec up_receituario_verbete 
				<<verbete.cmbAno.value>>
				,<<verbete.cmbMes.list(verbete.cmbMes.listindex,2)>>
				,'<<Alltrim(verbete.lstEntidade.value)>>'
				,'<<Alltrim(verbete.cmbTLote.value)>>'
				,<<verbete.cmbLoteIni.value>>
				,<<verbete.cmbLoteFim.value>>
				,'<<ALLTRIM(mySite)>>'
		ENDTEXT
		
		IF !uf_gerais_actgrelha("", "SQLtmp", lcSql)
			Messagebox("N�O FORAM ENCONTRADOS DADOS PARA IMPRIMIR!",64,"LOGITOOLS SOFTWARE")
			RETURN
		ENDIF
		
		Public myMes
		myMes = uf_Gerais_CalculaMes(verbete.cmbMes.list(verbete.cmbMes.listindex,2))
				
		lcPrinter = Getprinter()
		lcDefaultPrinter=set("printer",2)
		
		Set Printer To Name ''+lcPrinter+''
		Set Device To print
		Set Console off
		
		Report Form Alltrim(lcReport) To Printer Prompt Nodialog preview
		
		** por impressora no default **
		Set Device To screen
		Set printer to Name ''+lcDefaultPrinter+''
		Set Console On
		**************************

		IF Used("SQLtmp")
			Fecha("SQLtmp")	
		ENDIF
			
	Else &&imprimir
		
		** valida��es **
		IF !(Alltrim(verbete.lstEntidade.value) == 'TODAS')
			IF !(UPPER(Alltrim(verbete.cmbTLote.value)) == 'TODOS')
      			IF Alltrim(verbete.cmbLoteIni.value) == '' or Alltrim(verbete.cmbLoteFim.value) == ''
					messagebox("POR FAVOR PREENCHA TODOS OS CAMPOS!",64,"LOGITOOLS SOFTWARE")
       				RETURN
				ENDIF
       		ENDIF
		ENDIF
		****************


		IF uf_PERGUNTAlt_chama("Vai imprimir o(s) Verbete(s). DESEJA CONTINUAR?", "Sim", "N�o")
			
			**** Dados da Empresa *****
			SELECT uCrsE1

			IF alltrim(verbete.lstEntidade.value) == "TODAS" Or UPPER(Alltrim(verbete.cmbTLote.value)) == 'TODOS'
						
				lcSQl = ""
				TEXT TO lcSQl TEXTMERGE NOSHOW
					exec up_receituario_DadosVerbete
						<<verbete.cmbAno.value>>
						,<<verbete.cmbMes.list(verbete.cmbMes.listindex,2)>>
						,'<<Alltrim(verbete.lstEntidade.value)>>'
						,'<<ALLTRIM(mySite)>>'
				ENDTEXT
					
				IF !uf_gerais_actgrelha("", "uCrsVerbete", lcSql)
					Messagebox("N�O FORAM ENCONTRADOS DADOS PARA IMPRIMIR!", 64, "LOGITOOLS SOFTWARE")
					RETURN
				ENDIF

			ELSE && cria cursor com valores das caixas
		
				Create Cursor uCrsVerbete ;
					(ano C(4), mes C(2), cptorgabrev C(60), tlote C(60), Min N(4), Max N(4))		
				
				Insert Into uCrsVerbete (ano, mes, cptorgabrev, tlote, min, max);
				values ( alltrim(verbete.cmbAno.value), ;
					ALLTRIM(verbete.cmbMes.list(verbete.cmbMes.listindex,2)), ;
					alltrim(verbete.lstEntidade.value), ;
					ALLTRIM(verbete.cmbTLote.value), ;
					VAL(verbete.cmbLoteIni.value), ;
					VAL(verbete.cmbLoteFim.value))

			ENDIF
			
			Public myMes
			myMes = uf_Gerais_CalculaMes(verbete.cmbMes.list(verbete.cmbMes.listindex,2))

			lcPrinter = Getprinter()
			lcDefaultPrinter=set("printer",2)
				
			If !Empty(lcPrinter)
					
				** Preparar Regua **
				Select uCrsVerbete
				mntotal=reccount()
				regua(0,mntotal,"A IMPRIMIR VERBETES...",.f.)
				****************
			
				Set Printer To Name ''+lcPrinter+''
				Set Device To print
				Set Console off

				SELECT uCrsVerbete
				GO TOP
				SCAN
					regua[1,recno("uCrsVerbete"),"A IMPRIMIR VERBETES...:",.f.]
						
					TEXT TO lcSql NOSHOW textmerge
						exec up_receituario_verbete 
							<<uCrsVerbete.ano>>
							,<<uCrsVerbete.mes>>
							,'<<alltrim(uCrsVerbete.cptorgabrev)>>'
							,'<<alltrim(uCrsVerbete.tlote)>>'
							,<<uCrsVerbete.min>>
							,<<uCrsVerbete.max>>
							,'<<ALLTRIM(mySite)>>'
					ENDTEXT
					

					IF uf_gerais_actgrelha("", "SQLTMP", lcSql)
						Report Form Alltrim(lcReport) To Printer
					ENDIF
						
					SELECT uCrsVerbete
				ENDSCAN
					
				** fecha a regua **
				regua(2)
			ELSE
				Messagebox("N�O � POSS�VEL IMPRIMIR SEM ESCOLHER UMA IMPRESSORA.", 64, "LOGITOOLS SOFTWARE")
			ENDIF
				
			** por impressora no default **
			Set Device To screen
			Set printer to Name ''+lcDefaultPrinter+''
			Set Console On
		ENDIF
	ENDIF
	
	&& evita erros do report
	verbete.picture = verbete.picture
	verbete.refresh
ENDFUNC





** Fecha o painel
FUNCTION uf_verbete_sair
	&& evita erros de rowsource ao sair
	verbete.lstEntidade.rowsource	= ''
	verbete.cmbTlote.rowsource		= ''
	verbete.cmbLoteIni.rowsource	= ''
	verbete.cmbloteFim.rowsource	= ''
	
	&& fecha cursores
	IF USED("uCrsVerbeteEntidades")
		fecha("uCrsVerbeteEntidades")
	ENDIF

	IF USED("uCrsVerbeteTLote")
		fecha("uCrsVerbeteTLote")
	ENDIF

	IF USED("uCrsVerbete")
		fecha("uCrsVerbete")
	ENDIF

	IF USED("uCrsImpVerbete")
		fecha("uCrsImpVerbete")
	ENDIF

	IF USED("uCrsVerbeteLote")
		fecha("uCrsVerbeteLote")
	ENDIF

	If Used("SQLtemp")
		Fecha("SQLtemp")
	ENDIF


	RELEASE myVerbeteAno
	release myVerbeteMes
	
	&& fecha painel
	verbete.hide
	verbete.release
ENDFUNC