** Fun��o Principal
FUNCTION uf_impressaoRVR_chama
	LPARAMETERS lcAno, lcMes, lcPainel

	&& Valida Parametros
	PUBLIC myimpressaoRVRAno, myimpressaoRVRMes, myimpressaoRVRPainel

	IF EMPTY(lcPainel)
		uf_perguntalt_chama("Falta o 3� par�metro �Painel�", "OK", "", 16)
		RETURN .f.
	ELSE
		myimpressaoRVRPainel = lcPainel
	ENDIF
		
	IF EMPTY(lcAno)
		myimpressaoRVRAno = ALLTRIM(str(YEAR(DATE())))
	ELSE
		myimpressaoRVRAno = ALLTRIM(STR(lcAno))
	ENDIF
	
	IF EMPTY(lcMes)
		myimpressaoRVRMes = MONTH(DATE())
	ELSE
		myimpressaoRVRMes = lcMes
	ENDIF
		
	&& Abre o painel
	IF TYPE("impressaoRVR")=="U"
		DO FORM impressaoRVR
	ELSE
		impressaoRVR.show
	ENDIF
ENDFUNC


**
FUNCTION uf_impressaoRVR_carregaMenu
	impressaoRVR.menu1.adicionaopcao("previsao", "Previs�o", myPath + "\imagens\icons\prever_imp_w.png", "uf_impressaoRVR_emite with .f.","P")
	impressaoRVR.menu1.adicionaopcao("imprimir", "Imprimir", myPath + "\imagens\icons\imprimir_w.png", "uf_impressaoRVR_emite with .t.","I")
ENDFUNC


**
FUNCTION uf_impressaoRVR_cursorEntidades
	LPARAMETERS lcAno, lcMes
	
	LOCAL lcSql
	lcSQL = ''
	
	impressaoRVR.lstEntidade.rowsource 	= ''
	
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_receituario_listEntidadesPorData <<lcAno>>, <<lcMes>>, '<<ALLTRIM(mysite)>>'
	ENDTEXT
	If !uf_gerais_actgrelha("", "uCrsIRVRentidades", lcSql)
		uf_perguntalt_chama("Ocorreu um erro ao calcular as entidades! Por favor contacte o suporte.", "OK", "", 16)
		RETURN .f.
	ENDIF
	
	impressaoRVR.lstEntidade.rowsource 		= 'uCrsIRVRentidades.cptorgabrev'
	impressaoRVR.lstEntidade.rowsourcetype 	= 2
	
	impressaoRVR.lstEntidade.refresh
ENDFUNC


**
FUNCTION uf_impressaoRVR_cursorTipoLote
	LPARAMETERS lcAno, lcMes
	
	LOCAL lcSql
	lcSQL = ''
	
	impressaoRVR.cmbTlote.rowsource 	= ''
	impressaoRVR.cmbTlote.value	 		= ''
		
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_receituario_TipoLote <<lcAno>>, <<lcMes>>, '<<ALLTRIM(impressaoRVR.lstEntidade.value)>>', '<<ALLTRIM(uf_gerais_getSitePostos())>>'
	ENDTEXT
	If !uf_gerais_actgrelha("", "uCrsIRVRtlote", lcSql)
		uf_perguntalt_chama("Ocorreu um erro ao calcular os tipos de lotes! Por favor contacte o suporte.", "OK", "", 16)
		RETURN .f.
	ENDIF
	
	impressaoRVR.cmbTlote.rowsource 		= 'uCrsIRVRtlote.tlote'
	impressaoRVR.cmbTlote.rowsourcetype 	= 2
	
	impressaoRVR.cmbTlote.refresh
ENDFUNC


**
FUNCTION uf_impressaoRVR_cursorLote
	
	Local lcSql
	Store '' To lcSQL
	
	impressaoRVR.cmbLoteIni.rowsource	= ''
	impressaoRVR.cmbLoteFim.rowsource	= ''
	impressaoRVR.cmbLoteIni.value		= ''
	impressaoRVR.cmbLoteFim.value		= ''	
	
	IF USED("uCrsIRVRlote")
		fecha("uCrsIRVRlote")
	ENDIF
	
	If !(impressaoRVR.lstEntidade.value ==  "TODAS")
				
		lcSQL=""
		TEXT TO lcSQL TEXTMERGE NOSHOW
			exec up_receituario_Lote <<ALLTRIM(impressaoRVR.cmbAno.value)>>, <<impressaoRVR.cmbMes.list(impressaoRVR.cmbMes.listindex,2)>>, '<<Alltrim(impressaoRVR.lstEntidade.value)>>', '<<Alltrim(impressaoRVR.cmbTLote.value)>>', '<<ALLTRIM(uf_gerais_getSitePostos())>>'
		ENDTEXT

		IF !uf_gerais_actgrelha("", "uCrsIRVRlote", lcSql)
			RETURN
		ENDIF

		impressaoRVR.cmbLoteIni.rowsource		= 'uCrsIRVRlote.lote'
		impressaoRVR.cmbLoteIni.rowsourcetype 	= 2
		impressaoRVR.cmbLoteFim.rowsource		= 'uCrsIRVRlote.lote'
		impressaoRVR.cmbLoteFim.rowsourcetype 	= 2
	ELSE
		impressaoRVR.cmbLoteIni.value = ''
		impressaoRVR.cmbLoteIni.displayvalue = ''
		impressaoRVR.cmbLoteFim.value = ''
		impressaoRVR.cmbLoteFim.displayvalue = ''
	ENDIF

	impressaoRVR.cmbLoteIni.refresh
	impressaoRVR.cmbLoteFim.refresh
ENDFUNC


**
FUNCTION uf_impressaoRVR_cursorReceitas
	
	Local lcSql
	Store '' To lcSQL
	
	impressaoRVR.cmbReceitaIni.rowsource	= ''
	impressaoRVR.cmbReceitaFim.rowsource	= ''
	impressaoRVR.cmbReceitaIni.value		= ''
	impressaoRVR.cmbReceitaFim.value		= ''	
	
	IF USED("uCrsIRVRreceitas")
		fecha("uCrsIRVRreceitas")
	ENDIF
	
	If !(impressaoRVR.lstEntidade.value == "TODAS") And !EMPTY(impressaoRVR.cmbLoteIni.value) AND impressaoRVR.cmbLoteIni.value == impressaoRVR.cmbLoteFim.value
				
		lcSQL=""
		TEXT TO lcSQL TEXTMERGE NOSHOW
			exec up_receituario_receitas
						<<impressaoRVR.cmbAno.value>>, <<impressaoRVR.cmbMes.list(impressaoRVR.cmbMes.listindex,2)>>
						,'<<Alltrim(impressaoRVR.lstEntidade.value)>>', '<<Alltrim(impressaoRVR.cmbTLote.value)>>'
						,<<alltrim(impressaoRVR.cmbLoteIni.value)>>
						,'<<ALLTRIM(uf_gerais_getSitePostos())>>'
		ENDTEXT
		
		If !uf_gerais_actgrelha("", "uCrsIRVRreceitas", lcSql)
			uf_perguntalt_chama("Ocorreu um erro ao calcular o n� de receitas! Por favor contacte o suporte.", "OK", "", 16)
			RETURN
		ENDIF
		
		impressaoRVR.cmbReceitaIni.rowsource		= 'uCrsIRVRreceitas.nreceita'
		impressaoRVR.cmbReceitaIni.rowsourcetype 	= 2
		impressaoRVR.cmbReceitaFim.rowsource		= 'uCrsIRVRreceitas.nreceita'
		impressaoRVR.cmbReceitaFim.rowsourcetype 	= 2
	
	ELSE
	
		impressaoRVR.cmbReceitaIni.value = ''
		impressaoRVR.cmbReceitaIni.displayvalue = ''
		impressaoRVR.cmbReceitaFim.value = ''
		impressaoRVR.cmbReceitaFim.displayvalue = ''
	ENDIF


	impressaoRVR.cmbReceitaIni.refresh
	impressaoRVR.cmbReceitaFim.refresh
ENDFUNC


**  Selecciona o Ano
FUNCTION uf_impressaoRVR_selAno
	
	&& Entidades
	SELECT uCrsIRVREntidades
	DELETE all
	impressaoRVR.lstEntidade.refresh()
	
	&& Mes
	impressaoRVR.cmbMes.Value 			= '1'
	impressaoRVR.cmbMes.displayValue	= 'Jan'
	impressaoRVR.cmbMes.interactivechange
	
	&& Tipo de Lote
	SELECT uCrsIRVRtlote
	DELETE all
	impressaoRVR.cmbTlote.refresh()
	
	&& lote
	IF !(myimpressaoRVRPainel=="RESUMOLOTES")
		uf_impressaoRVR_cursorLote()
	ENDIF
	
	&& receitas
	IF myimpressaoRVRPainel == "RECEITAS"
		uf_impressaoRVR_cursorReceitas()
	ENDIF
ENDFUNC


** Selecciona o Mes
FUNCTION uf_impressaoRVR_selMes

	IF empty(impressaoRVR.cmbAno.value)
		RETURN .F.
	ENDIF
		
	&& Entidades
	&& Cursor de Entidades
	uf_impressaoRVR_cursorEntidades(impressaoRVR.cmbAno.value, impressaoRVR.cmbMes.list(impressaoRVR.cmbMes.listindex,2))
			
	&& Tipo de Lote
	SELECT uCrsIRVRtlote
	DELETE all
	impressaoRVR.cmbTlote.refresh()
	
	&& lote
	IF !(myimpressaoRVRPainel=="RESUMOLOTES")
		uf_impressaoRVR_cursorlote()
	ENDIF
	
	&& receitas
	IF myimpressaoRVRPainel=="RECEITAS"
		uf_impressaoRVR_cursorReceitas()
	ENDIF
ENDFUNC


** Selecciona uma Entidade
FUNCTION uf_impressaoRVR_selEntidade

	IF empty(impressaoRVR.cmbAno.value)
		RETURN .F.
	ENDIF

	LOCAL lcSql

	&& Tipo de Lote
	uf_impressaoRVR_cursorTipoLote(impressaoRVR.cmbAno.value, impressaoRVR.cmbMes.list(impressaoRVR.cmbMes.listindex,2))

	** Lote ini e Lote Fim
	IF !(myimpressaoRVRPainel == "RESUMOLOTES")
		uf_impressaoRVR_cursorLote()
	ENDIF

	&& receitas
	IF myimpressaoRVRPainel == "RECEITAS"
		uf_impressaoRVR_cursorReceitas()
	ENDIF
ENDFUNC


** Selecciona um Tipo de Lote
FUNCTION uf_impressaoRVR_selTLote

	IF ALLTRIM(impressaoRVR.cmbAno.value) == ''
		RETURN .F.
	ENDIF
	IF ALLTRIM(impressaoRVR.cmbTlote.value) == ''
		RETURN .F.
	ENDIF

	** Lote ini e Lote Fim
	IF !(myimpressaoRVRPainel=="RESUMOLOTES")
		uf_impressaoRVR_cursorLote()
	ENDIF
	
	&& receitas
	IF myimpressaoRVRPainel=="RECEITAS"
		uf_impressaoRVR_cursorReceitas()
	ENDIF
ENDFUNC 


** Selecciona um Lote
FUNCTION uf_impressaoRVR_selLote

	IF ALLTRIM(impressaoRVR.cmbAno.value) == ''
		RETURN .F.
	ENDIF
	IF ALLTRIM(impressaoRVR.cmbTlote.value) == ''
		RETURN .F.
	ENDIF

	** Receita Ini e Receita Fim
	IF myimpressaoRVRPainel=="RECEITAS"
		uf_impressaoRVR_cursorReceitas()
	ENDIF
ENDFUNC 


**
FUNCTION uf_impressaoRVR_emite
	Lparameters tcBool

	Local lcPrinter, lcDefaultPrinter, lcSQL, lcTextoPergunta, lcTextoRegua, lcSQLDup
	Store '' To lcSQL, lcTextoPergunta, lcTextoRegua, lcSQLDup
	
	** valida��es
	LOCAL lcValida
	STORE 0 TO lcValida
	

	
	IF EMPTY(impressaoRVR.cmbAno.value) OR EMPTY(impressaoRVR.cmbMes.value) OR EMPTY(impressaoRVR.lstEntidade.value) OR EMPTY(impressaoRVR.cmbTLote.value)
		lcValida = 1
	ELSE
		IF !(ALLTRIM(impressaoRVR.lstEntidade.value) == "TODAS")
			IF EMPTY(impressaoRVR.cmbTlote.value)
				lcValida = 1
			ENDIF
			
			IF !(alltrim(impressaoRVR.cmbTLote.value) == "TODOS") AND !(myimpressaoRVRPainel=="RESUMOLOTES")
				IF EMPTY(impressaoRVR.cmbLoteIni.value) OR EMPTY(impressaoRVR.cmbLoteFim.value)
					lcValida = 1
				ENDIF
				
				IF myimpressaoRVRPainel=="RECEITAS" AND !(alltrim(impressaoRVR.cmbTLote.value) == "TODOS") AND !(impressaoRVR.cmbLoteIni.value == impressaoRVR.cmbLoteFim.value)
					IF EMPTY(impressaoRVR.cmbReceitaIni.value) OR EMPTY(impressaoRVR.cmbReceitaFim.value)
						lcValida = 1
					ENDIF
				ENDIF
		
			ENDIF
		ENDIF
	ENDIF
	
	IF myimpressaoRVRPainel=="RECEITAS"
		IF ALLTRIM(impressaoRVR.lstEntidade.value) == "TODAS"
			lcValida = 2
		ELSE
			IF ALLTRIM(impressaoRVR.cmbTlote.value) == "TODOS"
				lcValida = 2
			ELSE
				IF !tcBool AND (!(impressaoRVR.cmbLoteIni.value == impressaoRVR.cmbLoteFim.value) OR !(impressaoRVR.cmbReceitaIni.value == impressaoRVR.cmbReceitaFim.value))
					lcValida = 3
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	DO CASE
		CASE lcValida == 1
			uf_perguntalt_chama("Deve preencher todos os campos antes de continuar.", "OK", "", 48)
			RETURN .f.

		CASE lcValida == 2
			uf_perguntalt_chama("Apenas � permitido a impress�o/previs�o de receitas de seguida para uma Entidade e Tipo de Lote de cada vez.", "OK", "", 48)
			RETURN .f.
		
		CASE lcValida == 3
			uf_perguntalt_chama("Apenas � permitido a previs�o de receitas para uma receita/verso de cada vez.", "OK", "", 48)
			RETURN .f.
	ENDCASE
	**
	
	LOCAL lcIsMcdt
	lcIsMcdt = .f.
	
	if(!EMPTY(ALLTRIM(impressaoRVR.lstEntidade.value)))
		lcIsMcdt  = uf_gerais_getUmValor("cptorg", "mcdt", "abrev = '" + alltrim(impressaoRVR.lstEntidade.value) + "'")
	ENDIF
	

	
	** guarda caminho para report
	SELECT uCrsPath
	GO TOP
	
	DO CASE
		&& verbetes CGD
		CASE UPPER(ALLTRIM(myimpressaoRVRPainel)) == "VERBETE" AND UPPER(ALLTRIM(impressaoRVR.lstEntidade.value)) == "CGD"
			lcReport = Alltrim(uCrsPath.textvalue)+"\analises\report_receituario_verbete_entidade.frx"

			lcTextoPergunta = "Vai imprimir o(s) verbetes(s)." + CHR(10)+CHR(10) + "Deseja Continuar?"
			lcTextoRegua 	= "A Imprimir Verbete(s)..."

		&& verbetes SCML
		CASE UPPER(ALLTRIM(myimpressaoRVRPainel)) == "VERBETE" AND UPPER(ALLTRIM(impressaoRVR.lstEntidade.value)) == "SCML"
			lcReport = Alltrim(uCrsPath.textvalue)+"\analises\report_receituario_verbete_scml.frx"

			lcTextoPergunta = "Vai imprimir o(s) verbetes(s)." + CHR(10)+CHR(10) + "Deseja Continuar?"
			lcTextoRegua 	= "A Imprimir Verbete(s)..."
			
		CASE UPPER(ALLTRIM(myimpressaoRVRPainel)) == "VERBETE" AND !EMPTY(lcIsMcdt) 
			lcReport = Alltrim(uCrsPath.textvalue)+"\analises\report_receituario_verbete_mcdt.frx"

			lcTextoPergunta = "Vai imprimir o(s) verbetes(s)." + CHR(10)+CHR(10) + "Deseja Continuar?"
			lcTextoRegua 	= "A Imprimir Verbete(s)..."
		
		&& verbetes normais	
		CASE UPPER(ALLTRIM(myimpressaoRVRPainel)) == "VERBETE" AND UPPER(ALLTRIM(impressaoRVR.lstEntidade.value)) != "CGD"
			lcReport = Alltrim(uCrsPath.textvalue)+"\analises\report_receituario_verbete.frx"

			lcTextoPergunta = "Vai imprimir o(s) verbetes(s)." + CHR(10)+CHR(10) + "Deseja Continuar?"
			lcTextoRegua 	= "A Imprimir Verbete(s)..."
			
		CASE UPPER(ALLTRIM(myimpressaoRVRPainel)) == "RESUMOLOTES" AND EMPTY(lcIsMcdt) 
			lcReport = Alltrim(uCrsPath.textvalue)+"\analises\report_receituario_resumolotes.frx"
			lcTextoPergunta = "Vai imprimir a(s) rela��o(�es) resumo de lotes." + CHR(10)+CHR(10) + "Deseja Continuar?"
			lcTextoRegua	= "A Imprimir Rela��o(�es) Resumo de Lotes..."
			
		CASE UPPER(ALLTRIM(myimpressaoRVRPainel)) == "RESUMOLOTES" AND !EMPTY(lcIsMcdt) 
			lcReport = Alltrim(uCrsPath.textvalue)+"\analises\report_receituario_resumolotes_mcdt.frx"

			lcTextoPergunta = "Vai imprimir a(s) rela��o(�es) resumo de lotes." + CHR(10)+CHR(10) + "Deseja Continuar?"
			lcTextoRegua	= "A Imprimir Rela��o(�es) Resumo de Lotes..."
			
		CASE UPPER(ALLTRIM(myimpressaoRVRPainel)) == "RECEITAS"
			&&:TODO
			lcReport = Alltrim(uCrsPath.textvalue)+"\analises\Pe\receita.frx"

			lcTextoPergunta = "Vai imprimir a(s) receita(s)." + CHR(10)+CHR(10) + "Deseja Continuar?"
			lcTextoRegua	= "A Imprimir Receita(s)..."
			
		OTHERWISE
			RETURN .F.
	ENDCASE
	
	

	
	IF !FILE(lcReport)
		uf_perguntalt_chama("O report n�o est� a ser detectado!", "OK", "", 64)
       	RETURN .F.
	ENDIF
	
	&& vari�vel p�blica para o m�s, � necess�rio para o IDUs
	Public myMes
	myMes = uf_Gerais_CalculaMes(impressaoRVR.cmbMes.list(impressaoRVR.cmbMes.listindex,2))
	

			
	IF !tcBool &&Previsao
	
		If  !(Alltrim(impressaoRVR.lstEntidade.value) == 'TODAS')
			IF UPPER(Alltrim(impressaoRVR.cmbTLote.value))=='TODOS'
				uf_perguntalt_chama("N�o � poss�vel fazer a previs�o para todos os Tipos de Lote.", "OK", "", 48)
       			RETURN .f.
			ELSE
      			If !(myimpressaoRVRPainel=="RESUMOLOTES") AND (Alltrim(impressaoRVR.cmbLoteIni.value)== '' or Alltrim(impressaoRVR.cmbLoteFim.value)== '')
	      			uf_perguntalt_chama("Os campos Lote Inicial e Lote Final devem ser preenchidos.", "OK", "", 48)
       				RETURN .f.
				ENDIF
       		ENDIF
       	ELSE
       		uf_perguntalt_chama("N�o � poss�vel fazer a previs�o para todas as Entidades.", "OK", "", 48)
       		RETURN .f.
		ENDIF
		

		
		** Dados da Empresa **
		SELECT uCrsE1

		lcSQl = ""
		
		DO CASE
			&& verbete para a entidade CGD - alterado a 20160812 Lu�sLeal
			CASE UPPER(ALLTRIM(myimpressaoRVRPainel)) == "VERBETE" AND UPPER(ALLTRIM(impressaoRVR.lstEntidade.value)) == "CGD"
			
				TEXT TO lcSQl TEXTMERGE NOSHOW
					exec up_receituario_verbete_entidade
						<<impressaoRVR.cmbAno.value>>
						,<<impressaoRVR.cmbMes.list(impressaoRVR.cmbMes.listindex,2)>>
						,'<<Alltrim(impressaoRVR.lstEntidade.value)>>'
						,'<<Alltrim(impressaoRVR.cmbTLote.value)>>'
						,<<impressaoRVR.cmbLoteIni.value>>
						,<<impressaoRVR.cmbLoteFim.value>>
						,'<<ALLTRIM(uf_gerais_getSitePostos())>>'
				ENDTEXT
			&& verbete para todas as restantes entidades
			CASE UPPER(ALLTRIM(myimpressaoRVRPainel)) == "VERBETE" AND UPPER(ALLTRIM(impressaoRVR.lstEntidade.value)) != "CGD"
			
				TEXT TO lcSQl TEXTMERGE NOSHOW
					exec up_receituario_verbete
						<<impressaoRVR.cmbAno.value>>
						,<<impressaoRVR.cmbMes.list(impressaoRVR.cmbMes.listindex,2)>>
						,'<<Alltrim(impressaoRVR.lstEntidade.value)>>'
						,'<<Alltrim(impressaoRVR.cmbTLote.value)>>'
						,<<impressaoRVR.cmbLoteIni.value>>
						,<<impressaoRVR.cmbLoteFim.value>>
						,'<<ALLTRIM(uf_gerais_getSitePostos())>>'
				ENDTEXT

			CASE UPPER(ALLTRIM(myimpressaoRVRPainel)) == "RESUMOLOTES"
			
	
				Text To lcSQl TEXTMERGE NOSHOW
					EXEC up_receituario_RelResumLotes <<ALLTRIM(impressaoRVR.cmbAno.value)>>, <<impressaoRVR.cmbMes.list(impressaoRVR.cmbMes.listindex,2)>>, '<<ALLTRIM(impressaoRVR.lstEntidade.displayvalue)>>', <<impressaoRVR.cmbTlote.value>>, '<<ALLTRIM(uf_gerais_getSitePostos())>>'
				ENDTEXT

			CASE UPPER(ALLTRIM(myimpressaoRVRPainel)) == "RECEITAS"
		
				Text To lcSQl TEXTMERGE NOSHOW
					EXEC up_receituario_receitasList
						<<ALLTRIM(impressaoRVR.cmbAno.value)>>, <<impressaoRVR.cmbMes.list(impressaoRVR.cmbMes.listindex,2)>>, '<<ALLTRIM(impressaoRVR.lstEntidade.displayvalue)>>'
						,<<impressaoRVR.cmbTlote.value>>, <<impressaoRVR.cmbloteIni.value>>, <<impressaoRVR.cmbloteFim.value>>
						,<<IIF(EMPTY(impressaoRVR.cmbReceitaIni.value), 0, impressaoRVR.cmbReceitaIni.value)>>, <<IIF(EMPTY(impressaoRVR.cmbReceitaFim.value), 0, impressaoRVR.cmbReceitaFim.value)>>
						,'<<ALLTRIM(uf_gerais_getSitePostos())>>'
				ENDTEXT
		ENDCASE
		

	
		IF !uf_gerais_actgrelha("", "SQLtmp", lcSql)
			uf_perguntalt_chama("Ocorreu um erro a gerar dados para impress�o.", "OK", "", 16)
			RETURN .f.
		ELSE
			SELECT SQLtmp
			IF !RECCOUNT("SQLtmp") > 0
				uf_perguntalt_chama("N�o foram encontrados dados para fazer a previs�o.", "OK", "", 48)
				RETURN .f.
			ENDIF
		ENDIF
		
		IF !(UPPER(ALLTRIM(myimpressaoRVRPainel)) == "RECEITAS") && se for previs�o de resumos ou verbetes
			
			SELECT SQLtmp
			GO TOP
			
			lcPrinter = Getprinter()
			lcDefaultPrinter=set("printer",2)
		
			Set Printer To Name ''+lcPrinter+''
			Set Device To print
			Set Console off
			SET REPORTBEHAVIOR 90
			Report Form Alltrim(lcReport) To Printer Prompt Nodialog preview
			SET REPORTBEHAVIOR 80
			** por impressora no default **
			Set Device To screen
			Set printer to Name ''+lcDefaultPrinter+''
			Set Console On
			**
		
		ELSE && Receitas
		
			SELECT SQLtmp
			IF !EMPTY(SQLtmp.u_ltstamp)
				IF (SQLtmp.cptorgabrev == 'SNS' OR SQLtmp.cptorgabrev == 'ADSE') AND !EMPTY(SQLtmp.u_ltstamp2)
					uf_imprimirpos_prevRec(SQLtmp.ftstamp, 2, .f.)
				ELSE
					uf_imprimirpos_prevRec(SQLtmp.ftstamp, 1, .f.)
				ENDIF
			ENDIF
			
		ENDIF

		IF Used("SQLtmp")
			Fecha("SQLtmp")	
		ENDIF
			
	ELSE  && imprimir
	
		&& valida��es 
		IF !(Alltrim(impressaoRVR.lstEntidade.value) == 'TODAS')
			IF !(UPPER(Alltrim(impressaoRVR.cmbTLote.value)) == 'TODOS')
      			IF !(myimpressaoRVRPainel=="RESUMOLOTES") AND (Alltrim(impressaoRVR.cmbLoteIni.value) == '' or Alltrim(impressaoRVR.cmbLoteFim.value) == '')
	      			uf_perguntalt_chama("Por favor preencha todos os campos.", "OK", "", 48)
       				RETURN .f.
				ENDIF
       		ENDIF
		ENDIF
		**

		IF uf_perguntalt_chama(lcTextoPergunta, "Sim", "N�o")
			LOCAL lcExcluiVerbeteEletronicoSNS,lcAbrevEnt  
			lcExcluiVerbeteEletronicoSNS = .f.
			
			lcAbrevEnt =''
			
			&& Dados da Empresa 
			SELECT uCrsE1
			
			lcSQl = ""
			
			DO CASE
				CASE UPPER(ALLTRIM(myimpressaoRVRPainel)) == "VERBETE" AND UPPER(ALLTRIM(impressaoRVR.lstEntidade.value)) == "CGD"
					IF ALLTRIM(impressaoRVR.lstEntidade.value) == "TODAS" Or UPPER(ALLTRIM(impressaoRVR.cmbTLote.value)) == 'TODOS'
								
						TEXT TO lcSQl TEXTMERGE NOSHOW
							exec up_receituario_DadosVerbete
								<<impressaoRVR.cmbAno.value>>
								,<<impressaoRVR.cmbMes.list(impressaoRVR.cmbMes.listindex,2)>>
								,'<<Alltrim(impressaoRVR.lstEntidade.value)>>'
								,'<<ALLTRIM(uf_gerais_getSitePostos())>>'
						ENDTEXT

					ELSE && cria cursor com valores das caixas
				
						Create Cursor uCrsIRVRSeguidaDados ;
							(ano C(4), mes C(2), cptorgabrev C(60), tlote C(60), Min N(4), Max N(4))
						
						Insert Into uCrsIRVRSeguidaDados (ano, mes, cptorgabrev, tlote, min, max);
						values ( alltrim(impressaoRVR.cmbAno.value), ;
							ALLTRIM(impressaoRVR.cmbMes.list(impressaoRVR.cmbMes.listindex,2)), ;
							alltrim(impressaoRVR.lstEntidade.value), ;
							ALLTRIM(impressaoRVR.cmbTLote.value), ;
							VAL(impressaoRVR.cmbLoteIni.value), ;
							VAL(impressaoRVR.cmbLoteFim.value))

					ENDIF
					
					lcSQLDup = ""
					TEXT TO lcSQLDup TEXTMERGE NOSHOW
						Select Top 1 nomeficheiro, tabela, temduplicados, numduplicados
						From b_impressoes (nolock)
						Where nomeficheiro = 'report_receituario_verbete_entidade.frx'
					ENDTEXT

				CASE UPPER(ALLTRIM(myimpressaoRVRPainel)) == "VERBETE" AND UPPER(ALLTRIM(impressaoRVR.lstEntidade.value)) != "CGD"
					
					IF ALLTRIM(impressaoRVR.lstEntidade.value) == "TODAS" Or UPPER(ALLTRIM(impressaoRVR.cmbTLote.value)) == 'TODOS'
						
								
						TEXT TO lcSQl TEXTMERGE NOSHOW
							exec up_receituario_DadosVerbete
								<<impressaoRVR.cmbAno.value>>
								,<<impressaoRVR.cmbMes.list(impressaoRVR.cmbMes.listindex,2)>>
								,'<<Alltrim(impressaoRVR.lstEntidade.value)>>'
								,'<<ALLTRIM(uf_gerais_getSitePostos())>>'
						ENDTEXT
						
						if(ALLTRIM(impressaoRVR.lstEntidade.value) == 'SNS' OR ALLTRIM(impressaoRVR.lstEntidade.value) == 'TODAS')
						
							lcExcluiVerbeteEletronicoSNS = .t.
							lcAbrevEnt = 'SNS'
						ENDIF
							

					ELSE && cria cursor com valores das caixas
					
				
						Create Cursor uCrsIRVRSeguidaDados ;
							(ano C(4), mes C(2), cptorgabrev C(60), tlote C(60), Min N(4), Max N(4))
						
						Insert Into uCrsIRVRSeguidaDados (ano, mes, cptorgabrev, tlote, min, max);
						values ( alltrim(impressaoRVR.cmbAno.value), ;
							ALLTRIM(impressaoRVR.cmbMes.list(impressaoRVR.cmbMes.listindex,2)), ;
							alltrim(impressaoRVR.lstEntidade.value), ;
							ALLTRIM(impressaoRVR.cmbTLote.value), ;
							VAL(impressaoRVR.cmbLoteIni.value), ;
							VAL(impressaoRVR.cmbLoteFim.value))

					ENDIF
					
					LOCAL lcNomeFicheiro
					lcNomeFicheiro = 'report_receituario_verbete.frx'
					
					if(!EMPTY(lcIsMcdt))
						lcNomeFicheiro = 'report_receituario_verbete_mcdt.frx'
					ENDIF
					
					
					lcSQLDup = ""
					TEXT TO lcSQLDup TEXTMERGE NOSHOW
						Select Top 1 nomeficheiro, tabela, temduplicados, numduplicados
						From b_impressoes (nolock)
						Where nomeficheiro = '<<ALLTRIM(lcNomeFicheiro)>>'
					ENDTEXT 
					
				CASE UPPER(ALLTRIM(myimpressaoRVRPainel)) == "RESUMOLOTES"
				
					TEXT TO lcSQl TEXTMERGE NOSHOW
						EXEC up_receituario_Resumos <<ALLTRIM(impressaoRVR.cmbAno.value)>>, <<impressaoRVR.cmbMes.list(impressaoRVR.cmbMes.listindex,2)>>,'<<ALLTRIM(impressaoRVR.lstEntidade.displayvalue)>>', <<ALLTRIM(impressaoRVR.cmbTlote.value)>>, '<<ALLTRIM(uf_gerais_getSitePostos())>>'
					ENDTEXT
					
					LOCAL lcNomeFicheiro
					lcNomeFicheiro = 'report_receituario_resumolotes.frx'
					
					if(!EMPTY(lcIsMcdt))
						lcNomeFicheiro = 'report_receituario_resumolotes_mcdt.frx'
					ENDIF
					
					
					lcSQLDup = ""
					TEXT TO lcSQLDup TEXTMERGE NOSHOW
						Select Top 1 nomeficheiro, tabela, temduplicados, numduplicados
						From b_impressoes (nolock)
						Where nomeficheiro = '<<ALLTRIM(lcNomeFicheiro)>>'
					ENDTEXT
	
					
					
*!*						if(ALLTRIM(impressaoRVR.lstEntidade.value) == 'SNS' OR ALLTRIM(impressaoRVR.lstEntidade.value) == 'TODAS')
*!*							
*!*								lcExcluiVerbeteEletronicoSNS = .t.
*!*								lcAbrevEnt = 'SNS'
*!*						ENDIF
					
				CASE UPPER(ALLTRIM(myimpressaoRVRPainel)) == "RECEITAS"
					
					TEXT TO lcSQl TEXTMERGE NOSHOW
						EXEC up_receituario_receitasList 
							<<ALLTRIM(impressaoRVR.cmbAno.value)>>, <<impressaoRVR.cmbMes.list(impressaoRVR.cmbMes.listindex,2)>>, '<<ALLTRIM(impressaoRVR.lstEntidade.displayvalue)>>'
							,<<impressaoRVR.cmbTlote.value>>, <<impressaoRVR.cmbloteIni.value>>, <<impressaoRVR.cmbloteFim.value>>
							,<<IIF(EMPTY(impressaoRVR.cmbReceitaIni.value), 0, impressaoRVR.cmbReceitaIni.value)>>, <<IIF(EMPTY(impressaoRVR.cmbReceitaFim.value), 0, impressaoRVR.cmbReceitaFim.value)>>
							,'<<ALLTRIM(uf_gerais_getSitePostos())>>'
					ENDTEXT
			ENDCASE

			

			IF !EMPTY(lcSql)
				IF !uf_gerais_actgrelha("", "uCrsIRVRSeguidaDados", lcSql)
					uf_perguntalt_chama("OCORREU UM ERRO A OBTER OS DADOS!","OK","", 64)
					RETURN
				ENDIF
			ENDIF
			
			
			
			
			&&remove impressao de verbetes eletronicos do spms
			if(lcExcluiVerbeteEletronicoSNS AND USED("uCrsIRVRSeguidaDados") AND !EMPTY(ALLTRIM(lcAbrevEnt)))
		
				DELETE FROM uCrsIRVRSeguidaDados WHERE ALLTRIM(Cptorgabrev)=ALLTRIM(lcAbrevEnt) AND ALLTRIM(Tlote) in ('96','97')
			ENDIF
			
			IF !EMPTY(lcSQLDup)
				If !uf_gerais_actGrelha("","templcSQL",lcSQLDup)
					uf_perguntalt_chama("N�O FOI POSS�VEL ENCONTRAR A CONFIGURA��O DE IMPRESS�ES. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
					RETURN .f.
				Endif
				lctemduplicados = templcSQL.temduplicados
				lcnumduplicados = templcSQL.numduplicados
				
				fecha("templcSQL")
			ELSE
				lctemduplicados = .f.
				lcnumduplicados = 0
			ENDIF 
			
			&& validar se existem resultados
			SELECT uCrsIRVRSeguidaDados
			IF !(RECCOUNT("uCrsIRVRSeguidaDados")>0)
				uf_perguntalt_chama("N�o foram encontrados dados para fazer a impress�o.", "OK", "", 48)
				RETURN .f.
			ENDIF

			lcPrinter = Getprinter()
			lcDefaultPrinter = set("printer",2)

			IF !Empty(lcPrinter)
					
				&& Preparar Regua 
				Select uCrsIRVRSeguidaDados
				mntotal=reccount()
				regua(0, mntotal, lcTextoRegua, .f.)
				**

				Set Printer To Name ''+lcPrinter+''
				Set Device To print
				Set Console off

				SELECT uCrsIRVRSeguidaDados
				GO TOP
				SCAN
					regua[1, recno("uCrsIRVRSeguidaDados"), lcTextoRegua + ": ", .f.]

					lcSql = ''

					DO CASE
						
						CASE UPPER(ALLTRIM(myimpressaoRVRPainel)) == "VERBETE" AND UPPER(ALLTRIM(impressaoRVR.lstEntidade.value)) == "CGD"
						
							TEXT TO lcSql NOSHOW textmerge
								exec up_receituario_verbete_entidade
									<<uCrsIRVRSeguidaDados.ano>>
									,<<uCrsIRVRSeguidaDados.mes>>
									,'<<alltrim(uCrsIRVRSeguidaDados.cptorgabrev)>>'
									,'<<alltrim(uCrsIRVRSeguidaDados.tlote)>>'
									,<<uCrsIRVRSeguidaDados.min>>
									,<<uCrsIRVRSeguidaDados.max>>
									,'<<ALLTRIM(uf_gerais_getSitePostos())>>'
							ENDTEXT
							
						CASE UPPER(ALLTRIM(myimpressaoRVRPainel)) == "VERBETE" AND UPPER(ALLTRIM(impressaoRVR.lstEntidade.value)) != "CGD"
							
							TEXT TO lcSql NOSHOW textmerge
								exec up_receituario_verbete
									<<uCrsIRVRSeguidaDados.ano>>
									,<<uCrsIRVRSeguidaDados.mes>>
									,'<<alltrim(uCrsIRVRSeguidaDados.cptorgabrev)>>'
									,'<<alltrim(uCrsIRVRSeguidaDados.tlote)>>'
									,<<uCrsIRVRSeguidaDados.min>>
									,<<uCrsIRVRSeguidaDados.max>>
									,'<<ALLTRIM(uf_gerais_getSitePostos())>>'
							ENDTEXT						
						
						
		
						CASE UPPER(ALLTRIM(myimpressaoRVRPainel)) == "RESUMOLOTES"
						
							TEXT TO lcSql NOSHOW textmerge
								exec up_receituario_RelResumLotes <<uCrsIRVRSeguidaDados.Ano>>, <<uCrsIRVRSeguidaDados.Mes>>, '<<ALLTRIM(uCrsIRVRSeguidaDados.cptorgabrev)>>', <<uCrsIRVRSeguidaDados.tlote>>, '<<ALLTRIM(uf_gerais_getSitePostos())>>'
							ENDTEXT
							
							
	
						CASE UPPER(ALLTRIM(myimpressaoRVRPainel)) == "RECEITAS"
						
							IF !EMPTY(uCrsIRVRSeguidaDados.u_ltstamp)
								IF ALLTRIM(uCrsIRVRSeguidaDados.cptorgabrev) == 'SNS' AND !EMPTY(uCrsIRVRSeguidaDados.u_ltstamp2)
									uf_imprimirpos_impRec(uCrsIRVRSeguidaDados.ftstamp, 2, "IMPRESSAORVR")
								ELSE
									uf_imprimirpos_impRec(uCrsIRVRSeguidaDados.ftstamp, 1, "IMPRESSAORVR")
								ENDIF
							ENDIF
					ENDCASE
					
					

					
					IF !EMPTY(lcSql)
						IF uf_gerais_actgrelha("", "SQLTMP", lcSql)
							If lctemduplicados == .t. And lcnumduplicados > 0
								For i=0 To lcnumduplicados
									TRY
										SELECT SQLtmp
										GO TOP
										Report Form Alltrim(lcReport) To Printer
									CATCH
										uf_perguntalt_chama("Foi detectado algum problema com a impressora." + CHR(13) + "Se o problema persistir por favor contacte o Suporte.","OK","",64)
									ENDTRY
								ENDFOR
							ELSE
								TRY
									SELECT SQLtmp
									GO TOP
									Report Form Alltrim(lcReport) To Printer
								CATCH
									uf_perguntalt_chama("Foi detectado algum problema com a impressora." + CHR(13) + "Se o problema persistir por favor contacte o Suporte.","OK","",64)
								ENDTRY
							ENDIF
						ENDIF
					ENDIF
						
					SELECT uCrsIRVRSeguidaDados
				ENDSCAN
					
				** fecha a regua **
				regua(2)
	
			ELSE
				uf_perguntalt_chama("N�o � poss�vel imprimir sem escolher uma impressora.", "OK", "", 48)
			ENDIF
				
			** por impressora no default **
			Set Device To screen
			Set printer to Name ''+lcDefaultPrinter+''
			Set Console On
		ENDIF
	ENDIF
	
	&& evita erros do report
	impressaoRVR.picture = impressaoRVR.picture
	impressaoRVR.refresh
ENDFUNC


** Fecha o painel
FUNCTION uf_impressaoRVR_sair
	&& evita erros de rowsource ao sair
	impressaoRVR.lstEntidade.rowsource		= ''
	impressaoRVR.cmbTlote.rowsource			= ''
	impressaoRVR.cmbLoteIni.rowsource		= ''
	impressaoRVR.cmbloteFim.rowsource		= ''
	impressaoRVR.cmbReceitaIni.rowsource	= ''
	impressaoRVR.cmbReceitaFim.rowsource	= ''
	
	&& fecha cursores
	IF USED("uCrsIRVRentidades")
		fecha("uCrsIRVRentidades")
	ENDIF

	IF USED("uCrsIRVRtlote")
		fecha("uCrsIRVRtlote")
	ENDIF

	IF USED("uCrsIRVRlote")
		fecha("uCrsIRVRlote")
	ENDIF
	
	IF USED("uCrsIRVRreceitas")
		fecha("uCrsIRVRreceitas")
	ENDIF

	IF USED("uCrsIRVRSeguidaDados")
		fecha("uCrsIRVRSeguidaDados")
	ENDIF

	IF USED("SQLtmp")
		fecha("SQLtmp")
	ENDIF
	
	RELEASE myimpressaoRVRano
	release myimpressaoRVRmes
	RELEASE myimpressaoRVRpainel
	
	&& fecha painel
	impressaoRVR.hide
	impressaoRVR.release
ENDFUNC