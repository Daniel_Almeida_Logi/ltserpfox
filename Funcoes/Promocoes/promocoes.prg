DO pesquisapromo

FUNCTION uf_promocoes_chama
	LPARAMETERS lcRef
		
	IF EMPTY(astr(lcRef))
		lcRef = ""
	ENDIF
	
	IF !myPromocoes
		uf_perguntalt_chama("PARA ADQUIRIR ESTE M�DULO POR FAVOR ENTRE EM CONTACTO COM O SUPORTE. PE�A J� A SUA DEMONSTRA��O.","OK","", 64)
		RETURN .f.
	ENDIF
	
	&& CONTROLA PERFIS DE ACESSO
	IF uf_gerais_validaPermUser(ch_userno,ch_grupo,'Gest�o de Promo��es')				
		**
	ELSE
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ACEDER AO PAINEL DE PROMO��ES.","OK","",48)
		RETURN .f.
	ENDIF
	
	&& CABE�ALHO
	IF !(uf_gerais_actGrelha("","uCrsPromoC","SELECT * FROM B_PROMOC (NOLOCK) WHERE ref='" + astr(lcRef) + "'"))
		uf_perguntalt_chama("OCORREU UM ERRO A CARREGAR OS DADOS DA PROMO��O(1)!","OK","",16)
		RETURN .f.
	ENDIF
	
	SELECT uCrsPromoC
	
	IF uCrsPromoC.familia
		&& ORIGENS
			&&configurar grelha produtos
		uf_gerais_actGrelha(IIF(TYPE("PROMOCOES")=="U","","PROMOCOES.pageframe1.page3.grdOrigem"),"uCrsPromoO",[set fmtonly on SELECT * FROM B_PROMOO (NOLOCK) set fmtonly off])
		
		IF !(uf_gerais_actGrelha(IIF(TYPE("PROMOCOES")=="U","","PROMOCOES.pageframe1.page6.grdOrigem"),"uCrsPromoOF",[SELECT * FROM B_PROMOO (NOLOCK) WHERE cstamp=']+ALLTRIM(uCrsPromoC.cstamp)+[']))
			uf_perguntalt_chama("OCORREU UM ERRO A CRIAR O CURSOR DE PROMO��ES(2)!","OK","",16)
			RETURN .f.
		ENDIF
		
		SELECT uCrsPromoO
		
		&& DESTINOS
			&&configurar grelha produtos
		uf_gerais_actGrelha(IIF(TYPE("PROMOCOES")=="U","","PROMOCOES.pageframe1.page3.grdDestino"),"uCrsPromoD",[set fmtonly on SELECT * FROM B_PROMOD (NOLOCK) set fmtonly off])
		
		IF !(uf_gerais_actGrelha(IIF(TYPE("PROMOCOES")=="U","","PROMOCOES.pageframe1.page6.grdDestino"),"uCrsPromoDF",[SELECT * FROM B_PROMOD (NOLOCK) WHERE cstamp=']+ALLTRIM(uCrsPromoC.cstamp)+[']))
			uf_perguntalt_chama("OCORREU UM ERRO A CRIAR O CURSOR DE PROMO��ES(3)!","OK","",16)
			RETURN .f.
		ENDIF
		
		SELECT uCrsPromoD
	ELSE
		&& ORIGENS
			&&configurar grelha fam�lias
		uf_gerais_actGrelha(IIF(TYPE("PROMOCOES")=="U","","PROMOCOES.pageframe1.page6.grdOrigem"),"uCrsPromoOF",[set fmtonly on SELECT * FROM B_PROMOO (NOLOCK) set fmtonly off])

		IF !(uf_gerais_actGrelha(IIF(TYPE("PROMOCOES")=="U","","PROMOCOES.pageframe1.page3.grdOrigem"),"uCrsPromoO",[SELECT * FROM B_PROMOO (NOLOCK) WHERE cstamp=']+ALLTRIM(uCrsPromoC.cstamp)+[']))
			uf_perguntalt_chama("OCORREU UM ERRO A CRIAR O CURSOR DE PROMO��ES(2)!","OK","",16)
			RETURN .f.
		ENDIF
		
		SELECT uCrsPromoO
		
		&& DESTINOS
			&&configurar grelha fam�lias
		uf_gerais_actGrelha(IIF(TYPE("PROMOCOES")=="U","","PROMOCOES.pageframe1.page6.grdDestino"),"uCrsPromoDF",[set fmtonly on SELECT * FROM B_PROMOD (NOLOCK) set fmtonly off])

		IF !(uf_gerais_actGrelha(IIF(TYPE("PROMOCOES")=="U","","PROMOCOES.pageframe1.page3.grdDestino"),"uCrsPromoD",[SELECT * FROM B_PROMOD (NOLOCK) WHERE cstamp=']+ALLTRIM(uCrsPromoC.cstamp)+[']))
			uf_perguntalt_chama("OCORREU UM ERRO A CRIAR O CURSOR DE PROMO��ES(3)!","OK","",16)
			RETURN .f.
		ENDIF
		
		SELECT uCrsPromoD
	ENDIF
	
	&& EXCEPCOES
	IF !(uf_gerais_actGrelha(IIF(TYPE("PROMOCOES")=="U","","PROMOCOES.pageframe1.page4.grdExcecoes"),"uCrsPromoE",[Select B_PromoE.*, st.faminome as familia FROM B_PROMOE (NOLOCK) inner join st (nolock) on B_promoE.ref=st.ref WHERE cstamp=']+ALLTRIM(uCrsPromoC.cstamp)+[']))
		uf_perguntalt_chama("OCORREU UM ERRO A CRIAR O CURSOR DE EXCEP��ES(4)!","OK","",16)
		RETURN .f.
	ENDIF
	
	SELECT uCrsPromoE
	
	&& Cursor de Clientes da Campanha	
	LOCAL lcSQL
	lcSQL=''
	TEXT TO lcSQL TEXTMERGE NOSHOW 
		SELECT 
			b_utentes.nome
			,b_utentes.no
			,b_utentes.estab
			,b_utentes.tlmvl
			,b_utentes.tipo, 
			b_utentes.utstamp
			,B_promoCL.promostamp 
		FROM b_utentes (NOLOCK) 
		inner join B_promoCL (nolock) on B_promoCL.clstamp=b_utentes.utstamp 
		where B_promoCL.promostamp='<<ALLTRIM(uCrsPromoC.cstamp)>>' order by ordem
	ENDTEXT
	IF !(uf_gerais_actGrelha(IIF(TYPE("PROMOCOES")=="U","","PROMOCOES.pageframe1.page1.grdCl"),"uCrsCLPromo",lcSQL))
		uf_perguntalt_chama("OCORREU UM ERRO A CARREGAR OS CLIENTES DA PROMO��O!","OK",16)
		RETURN .f.
	ENDIF
		
	&& Controla Abertura do Painel			
	if type("PROMOCOES")=="U"
	
		**Valida Licenciamento	
		IF (uf_gerais_addConnection('PROMOCOES') == .f.)
			RETURN .F.
		ENDIF
		
		DO FORM PROMOCOES
	ELSE
		PROMOCOES.show
	ENDIF
	
	&& menu
	IF TYPE("PROMOCOES.menu1.pesquisar") == "U"
		PROMOCOES.menu1.adicionaOpcao("opcoes", "Op��es", "opcoes_w.png", "","O")
		PROMOCOES.menu1.adicionaOpcao("pesquisar", "Pesquisar", "lupa_w.png", "uf_pesquisapromo_Chama","P")
		PROMOCOES.menu1.adicionaOpcao("actualizar", "Actualizar", "actualizar_w.png", "uf_promocoes_actualizar","A")
		*PROMOCOES.menu1.adicionaOpcao("ultimo", "�ltimo", "ultimo_w.png", "uf_promocoes_ultimo","U")
		PROMOCOES.menu1.adicionaOpcao("novo", "Novo", "mais_w.png", "uf_promocoes_novo","N")
		PROMOCOES.menu1.adicionaOpcao("editar", "Editar", "lapis_w.png", "","E")
		PROMOCOES.menu1.adicionaOpcao("apagar", "Apagar", "cruz_w.png", "","X")
		PROMOCOES.menu1.adicionaOpcao("anterior", "Anterior", "ponta_seta_left_w.png", "uf_promocoes_ant","19")
		PROMOCOES.menu1.adicionaOpcao("seguinte", "Seguinte", "ponta_seta_rigth_w.png", "uf_promocoes_prox","04")
		
		PROMOCOES.menu_opcoes.adicionaOpcao("duplicar", "Duplicar Promo��o", "", "")
		PROMOCOES.menu_opcoes.adicionaOpcao("analises", "An�lises", "", "")
		PROMOCOES.menu_opcoes.adicionaOpcao("ultimo", "�ltimo", "", "uf_promocoes_ultimo")
	ENDIF
		
	&& Configura ecra
	IF !myPromoIntroducao AND !myPromoAlteracao
		uf_promocoes_configuraEcra(0)
	ELSE
		uf_promocoes_configuraEcra(1)
	ENDIF
		
	&& Actualiza Grelha Clientes
	PROMOCOES.pageframe1.page1.lblRegistos.caption = astr(RECCOUNT("uCrsCLPromo")) + " Registos"
	
	&& Actualiza Ecra
	SELECT uCrsPromoC
	PROMOCOES.refresh
	PROMOCOES.lbl2.click
ENDFUNC 


FUNCTION uf_promocoes_configuraEcra
	LPARAMETERS tcBool

	promocoes.pageframe1.tabs = .f.
	*promocoes.pageframe1.resize

	IF tcBool < 1 &&modo consulta
		promocoes.Caption="Gest�o de Promo��es"		
		
		&& Menu
		promocoes.menu1.estado("opcoes,pesquisar,actualizar,novo,editar,apagar,anterior,seguinte","SHOW","Gravar",.f.)
			
		&& Configura��o do Pageframe
		promocoes.pageframe1.enabled = .f.
		
		&& checkBox
		promocoes.pageframe1.page1.chkTodos.enable(.f.)
		promocoes.pageframe1.page1.chkTodoscFicha.enable(.f.)
		promocoes.pageframe1.page2.chkForn.enable(.f.)
		promocoes.pageframe1.page2.chkOverlap.enable(.f.)
		promocoes.pageframe1.page2.chkfamilia.enable(.f.)
		promocoes.pageframe1.page2.chkacumulaccl.enable(.f.)
		promocoes.pageframe1.page2.chkBackOffice.enable(.f.)
		promocoes.pageframe1.page2.chkAviso.enable(.f.)
		
	ELSE &&edi��o e introdu��o
		&& Menu
		promocoes.menu1.estado("opcoes,pesquisar,actualizar,novo,editar,apagar,anterior,seguinte","HIDE","Gravar",.t.)
		
		&& Configura��o do Pageframe
		promocoes.pageframe1.enabled = .t.
		
		&& checkBox
		promocoes.pageframe1.page1.chkTodos.enable(.t.)
		promocoes.pageframe1.page1.chkTodoscFicha.enable(.t.)
		promocoes.pageframe1.page2.chkForn.enable(.t.)
		promocoes.pageframe1.page2.chkOverlap.enable(.t.)
		promocoes.pageframe1.page2.chkfamilia.enable(.t.)
		promocoes.pageframe1.page2.chkacumulaccl.enable(.t.)
		promocoes.pageframe1.page2.chkBackOffice.enable(.t.)
		promocoes.pageframe1.page2.chkAviso.enable(.t.)
	ENDIF
	promocoes.refresh
ENDFUNC

FUNCTION uf_promocoes_ultimo
	LOCAL lcSQL
	lcSQL=''
	TEXT TO lcSQL TEXTMERGE noshow
		SELECT TOP 1 stamp
		from b_ultReg (nolock)
		where b_ultReg.tabela='B_promoC'
	ENDTEXT 
	IF uf_gerais_actGrelha("","uCrsUltPromo",lcSQL)
		IF RECCOUNT()>0		
			uf_promocoes_chama(uCrsUltPromo.stamp)
			PROMOCOES.lbl2.Click
		ENDIF
		fecha("uCrsUltPromo")
	ENDIF
ENDFUNC 

FUNCTION uf_promocoes_actualizar
	SELECT uCrsPromoC
	uf_promocoes_chama(uCrsPromoC.ref)
ENDFUNC 

*****************************************************************
* 						FUN��O NOVA PROMO						*
*****************************************************************
FUNCTION uf_promocoes_novo
	&& Valida acesso - Inserir
	IF !(uf_gerais_validaPermUser(ch_userno,ch_grupo,'Gest�o de Promo��es - Introduzir'))
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE INTRODUZIR PROMO��ES.","OK","",48)
		RETURN .f.
	ENDIF
	
	promocoes.Caption = "Gest�o de Promo��es - Modo de Introdu��o"
	myPromoIntroducao = .t.
	myPromoAlteracao = .f.
	
	&& recria cursores com dados vazios
	uf_promocoes_chama('')
	&& Cria Valores por defeito
	uf_promocoes_valoresDefeito()
ENDFUNC

FUNCTION uf_promocoes_valoresDefeito
	SELECT uCrsPromoC
	APPEND BLANK
	
	LOCAL lcStamp
	lcStamp = uf_gerais_stamp()
	
	&& Stamp
	SELECT uCrsPromoC
	replace uCrsPromoC.cstamp WITH lcStamp
		
	&& N� da Promo	
	If uf_gerais_actGrelha("","uCrsTempNo",[select isnull(MAX(ref),'0000000') as no from B_promoC (nolock)])
		If RECCOUNT("uCrsTempNo")>0
			IF EMPTY(uCrsTempNo.no)
				SELECT uCrsSMS
				replace uCrsSMS.no WITH '0000001'
			ELSE
				LOCAL lcRef
				STORE '' TO lcRef
				lcRef = astr(Val(alltrim(uCrsTempNo.no))+1)
				IF LEN(lcRef) < 7
					FOR i=1 TO 7-LEN(lcRef)
						lcRef = '0' + lcRef
					ENDFOR 
				ENDIF 
				SELECT uCrsPromoC
				replace uCrsPromoC.ref WITH lcRef
			ENDIF
		Endif
		Fecha("uCrsTempNo")
	ENDIF
	
	&& Datas
	promocoes.txtdataini.value = uf_gerais_getdate(date(),"DATA") 
	promocoes.txtdatafim.value = uf_gerais_getdate(date(),"DATA")
			 
	&& estado
	SELECT uCrsPromoC
	replace uCrsPromoC.status WITH 'Activo'
	
	&& loja
	replace uCrsPromoC.site WITH ALLTRIM(mySite)
			
	&& Regras
	REPLACE uCrsPromoC.Forn			WITH .f.
	REPLACE uCrsPromoC.Overlap		WITH .f.
	REPLACE uCrsPromoC.acumulaccl	WITH .f.
	REPLACE uCrsPromoC.maiscaro		WITH .f.
	REPLACE uCrsPromoC.BackOffice	WITH .f.
	REPLACE uCrsPromoC.familia		WITH .f.
	
	&& actualiza o painel
	promocoes.refresh
Endfunc

FUNCTION uf_PROMOCOES_RemoverClienteGrid
	SELECT uCrsCLPromo
	DELETE
	GO TOP 
	PROMOCOES.pageframe1.page1.grdCl.refresh
	
	SELECT uCrsCLPromo
	count for !deleted() TO X
	GO TOP 
	PROMOCOES.pageframe1.page1.lblRegistos.caption = astr(X) + " Registos"
ENDFUNC 

*********************************
*	 NAVEGA REGISTO ANTERIOR	*
*********************************
FUNCTION uf_promocoes_ant
	SELECT uCrsPromoC
	lcSQL=""
	
	TEXT TO lcSQL TEXTMERGE NOSHOW 
		SELECT TOP 1 ref FROM B_promoC (nolock)
		WHERE ISNULL(convert(int,ref),0) < <<IIF(EMPTY(uCrsPromoC.cstamp),0,uCrsPromoC.ref)>>
		order by ref desc
	ENDTEXT  
	If !uf_gerais_actGrelha("","uCrstempPromo",lcSQL)
		uf_perguntalt_chama("OCORREU UM ERRO A LER O REGISTO ANTERIOR.","OK","",48)
		RETURN .f.
	ENDIF
	
	IF RECCOUNT("uCrstempPromo")>0
		uf_promocoes_chama(uCrstempPromo.ref)
	ENDIF
	
	fecha("uCrstempPromo")
ENDFUNC

*********************************
*	 NAVEGA REGISTO SEGUINTE	*
*********************************
FUNCTION uf_promocoes_prox
	SELECT uCrsPromoC
	lcSQL=""
	
	TEXT TO lcSQL TEXTMERGE NOSHOW 
		SELECT TOP 1 ref FROM B_promoC (nolock)
		WHERE ISNULL(convert(int,ref),0) > <<IIF(EMPTY(uCrsPromoC.cstamp),9999999,uCrsPromoC.ref)>>
		order by ref desc
	ENDTEXT 
	If !uf_gerais_actGrelha("","uCrstempPromo",lcSQL)
		uf_perguntalt_chama("OCORREU UM ERRO A LER O REGISTO SEGUINTE.","OK","",48)
		RETURN .f.
	ENDIF
	
	IF RECCOUNT("uCrstempPromo")>0
		uf_promocoes_chama(uCrstempPromo.ref)
	ENDIF
	
	fecha("uCrstempPromo")
ENDFUNC


FUNCTION uf_promocoes_delLinhaOD
	LPARAMETERS lcCursor, lcObj
	
	Select &lcCursor
	DELETE

	select &lcCursor
	if recno() > 0
		SKIP -1
	ELSE
		GO TOP
	endif
					
	Select &lcCursor
	
	&lcObj..refresh
	&lcObj..SetFocus
	
	*DO uf_ValidaOverlap
ENDFUNC 

*****************************************************************
* 						FUN��O SAIR								*
*****************************************************************
FUNCTION uf_promocoes_sair
	IF myPromoIntroducao OR myPromoAlteracao
		If uf_perguntalt_chama("Deseja cancelar as altera��es?" + CHR(13) + "Ir� perder as �ltimas altera��es feitas.","Sim","N�o")
			
			promocoes.caption = "Gest�o de Promo��es"
			
			IF myPromoIntroducao
				myPromoIntroducao = .f.
				uf_promocoes_chama('')
			ELSE
				myPromoAlteracao = .f.
				SELECT uCrsPromoC
				uf_promocoes_chama(uCrsPromoC.ref)
			ENDIF
		Endif
	ELSE				
		IF used("uCrsPromoC")
			Fecha("uCrsPromoC")
		ENDIF
		IF used("uCrsCLPromo")
			Fecha("uCrsCLPromo")
		ENDIF

*!*			IF used("uCrsPromoO")
*!*				Fecha("uCrsPromoO")
*!*			ENDIF

*!*			IF used("uCrsPromoD")
*!*				Fecha("uCrsPromoD")
*!*			endif

*!*			IF used("uCrsPromoE")
*!*				Fecha("uCrsPromoE")
*!*			ENDIF

*!*			IF used("uCrsOverlap")
*!*				Fecha("uCrsOverlap")
*!*			ENDIF

		promocoes.hide
		*************************
		RELEASE PROMOCOES
	ENDIF	
ENDFUNC