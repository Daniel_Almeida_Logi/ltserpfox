FUNCTION uf_chamaPromo
	LPARAMETERS lcPromoStamp
		
	Public myVirtualVarD, myIntVarFl, myIntVarFl, myPublicVarInsereProd, myPublicVarPromo, myIntVarFam

	&& Cria Cursor com sobreposi�oes
		uf_ValidaOverlap()
				
	&& Configura as grelhas
		DO uf_confGridPromoO 
		DO uf_confGridPromoD
		DO uf_confGridPromoE
		DO uf_ConfGridOverlap
Endfunc

*****************************************************************
* 				CONFIGURA��O DA GRELHA DE ORIGEM				*
*****************************************************************

FUNCTION uf_ConfGridPromoO
	
	&& Cores das Linhas
		FOR i=1 TO &lcObj..columncount
			lcColumn = Upper(Alltrim(&lcObj..columns(i).header1.caption))	   		
		   	IF lcColumn == "QTD." AND !(uCrsPromoC.familia)
				&lcObj..columns(i).dynamicBackColor="IIF(recno()%2!=0,Rgb[152,251,152],rgb[152,251,152])"	
		 	else
		 		&lcObj..columns(i).dynamicBackColor="IIF(Recno()%2==0,Rgb[255,255,255],rgb[255,248,220])"		
		 	ENDIF
		ENDFOR
	
	&& Controla estado readonly da linhas
		FOR i=1 TO &lcObj..columncount
			DO case
				CASE UPPER(&lcObj..columns(i).controlsource)=="UCRSPROMOD.QT"
					IF promocoes.caption='Gest�o de Promo��es' AND (uCrsPromoC.familia)
						&lcObj..columns(i).readonly 	= .t.
						msg("1")
					ELSE
						&lcObj..columns(i).readonly 	= .F.
						msg("2")
					ENDIF	
				
				OTHERWISE
					***
			endcas
	    endfor

	DO uf_ValidaOverlap
ENDPROC



*****************************************************************
* 				CONFIGURA��O DA GRELHA DE DESTINO				*
*****************************************************************
FUNCTION uf_ConfGridPromoD
	&& Cores das Linhas
		FOR i=1 TO &lcObj..columncount
			lcColumn = Upper(Alltrim(&lcObj..columns(i).header1.caption))
	   		IF lcColumn == "QTD." Or lcColumn == "%" Or lcColumn == "�" 
				&lcObj..columns(i).dynamicBackColor="IIF(recno()%2!=0,Rgb[152,251,152],rgb[152,251,152])"
	     	else
	     		&lcObj..columns(i).dynamicBackColor="IIF(Recno()%2==0,Rgb[255,255,255],rgb[255,248,220])"
	     	ENDIF
	     	IF lcColumn == "QTD." AND (uCrsPromoC.familia)
	     		&lcObj..columns(i).dynamicBackColor="IIF(Recno()%2==0,Rgb[255,255,255],rgb[255,248,220])"
	     	endif
	    endfor	
   
	&& Controla estado readonly da linhas
		FOR i=1 TO &lcObj..columncount
			DO case
				CASE UPPER(&lcObj..columns(i).controlsource)=="UCRSPROMOD.QT"
					IF promocoes.caption='Gest�o de Promo��es'
						&lcObj..columns(i).readonly 	= .t.
					ELSE
						&lcObj..columns(i).readonly 	= .F.
					ENDIF
				
				CASE UPPER(&lcObj..columns(i).controlsource)=="UCRSPROMOD.PERCT"
					IF promocoes.caption='Gest�o de Promo��es'
						&lcObj..columns(i).readonly 	= .t.
					ELSE
						&lcObj..columns(i).readonly 	= .F.
					ENDIF
				
				CASE UPPER(&lcObj..columns(i).controlsource)=="UCRSPROMOD.PVP"
					IF promocoes.caption='Gest�o de Promo��es'
						&lcObj..columns(i).readonly 	= .t.
					ELSE
						&lcObj..columns(i).readonly 	= .F.
					endif			
					
				OTHERWISE
					***
			endcas
	    ENDFOR
	    
	DO uf_ValidaOverlap
ENDFUNC



*****************************************************************
* 				CONFIGURA��O DA GRELHA DE EXCEP��ES				*
*****************************************************************
FUNCTION uf_ConfGridPromoE
	
	DO uf_ValidaExcepcoes
ENDPROC



*****************************************************************
* 				CONFIGURA��O DA GRELHA DE SOBREPOSI��ES					*
*****************************************************************
FUNCTION uf_ConfGridOverlap
	lcObj = 'promocoes.pageframe1.page4.Grid1'
	
	&lcObj..recordsource=''
	&lcObj..recordsource='uCrsOverlap'	
		
	FOR i=1 to &lcObj..columncount
     	&lcObj..columns(i).visible= .F.
     	DO case  		
     		CASE UPPER(&lcObj..columns(i).controlsource)=="UCRSOVERLAP.STREF"
     			&lcObj..columns(i).columnorder 		= 1
     			&lcObj..columns(i).readonly			= .t.
     			&lcObj..columns(i).WIDTH 			= 70
     			&lcObj..columns(i).header1.caption 	= "Ref. Prod."
     			&lcObj..columns(i).visible			= .t.
     			
     		CASE UPPER(&lcObj..columns(i).controlsource)=="UCRSOVERLAP.STDESCR"
     			&lcObj..columns(i).columnorder 		= 2
     			&lcObj..columns(i).readonly			= .t.
     			&lcObj..columns(i).WIDTH 			= 200
     			&lcObj..columns(i).header1.caption	= "Designa��o Prod."
     			&lcObj..columns(i).visible			= .t.
     			
     		CASE UPPER(&lcObj..columns(i).controlsource)=="UCRSOVERLAP.REF"
     			&lcObj..columns(i).columnorder 		= 3
     			&lcObj..columns(i).readonly 		= .t.
     			&lcObj..columns(i).WIDTH 			= 70
     			&lcObj..columns(i).header1.caption	= "Ref. Promo."
     			&lcObj..columns(i).visible			= .t.
     			
     		CASE UPPER(&lcObj..columns(i).controlsource)=="UCRSOVERLAP.DESIGN"
     			&lcObj..columns(i).columnorder		= 4
     			&lcObj..columns(i).readonly 		= .F.
     			&lcObj..columns(i).WIDTH 			= 200
     			&lcObj..columns(i).header1.caption	= "Designa��o Promo."
     			&lcObj..columns(i).visible			= .t.
     			
      		CASE UPPER(&lcObj..columns(i).controlsource)=="UCRSOVERLAP.TIPODESCR"
     			&lcObj..columns(i).columnorder		= 5
     			&lcObj..columns(i).readonly 		= .t.
     			&lcObj..columns(i).WIDTH 			= 70
     			&lcObj..columns(i).header1.caption	= "Rela��o"
     			&lcObj..columns(i).visible			= .t.
     			
     		CASE UPPER(&lcObj..columns(i).controlsource)=="UCRSOVERLAP.DATAINI"
     			&lcObj..columns(i).columnorder 		= 6
     			&lcObj..columns(i).readonly 		= .t.
     			&lcObj..columns(i).WIDTH 			= 90
     			&lcObj..columns(i).header1.caption	= "Data Inicial"
     			&lcObj..columns(i).visible			= .t.
     			
     		CASE UPPER(&lcObj..columns(i).controlsource)=="UCRSOVERLAP.DATAFIM"
     			&lcObj..columns(i).columnorder		= 7
     			&lcObj..columns(i).readonly 		= .t.
     			&lcObj..columns(i).WIDTH 			= 90
     			&lcObj..columns(i).header1.caption	= "Data Final"
     			&lcObj..columns(i).visible			= .t.
     			
     		CASE UPPER(&lcObj..columns(i).controlsource)=="UCRSOVERLAP.DIAS"
     			&lcObj..columns(i).columnorder		= 8
     			&lcObj..columns(i).readonly 		= .t.
     			&lcObj..columns(i).WIDTH 			= 90
     			&lcObj..columns(i).header1.caption	= "Dias em conflito"
     			&lcObj..columns(i).visible			= .t.
     			
     		CASE UPPER(&lcObj..columns(i).controlsource)=="UCRSOVERLAP.OVERLAP"
     			&lcObj..columns(i).columnorder		= 9
     			&lcObj..columns(i).readonly 		= .t.
     			&lcObj..columns(i).WIDTH 			= 90
     			&lcObj..columns(i).header1.caption	= "Permite Sobrepos.?"
     			&lcObj..columns(i).visible			= .t.	
     		OTHERWISE 
     			***
     	endcase    	
     	&lcObj..columns(i).dynamicBackColor="IIF(Recno()%2==0,Rgb[255,255,255],rgb[255,248,220])"  
	ENDFOR
	&lcObj..refresh
ENDPROC



*****************************************
*			Valida OVERLAP				*
*****************************************
FUNCTION uf_ValidaOverlap 	
	SELECT uCrsPromoC
	IF (EMPTY(uCrsPromoC.Dataini))
		lcDataIni = dtoc(Date())
	ELSE
		lcDataIni = uCrsPromoC.Dataini
	ENDIF

	IF (EMPTY(uCrsPromoC.Datafim))
		lcDataFim = dtoc(Date())
	ELSE
		lcDataFim = uCrsPromoC.Datafim
	ENDIF
	
	IF (EMPTY(uCrsPromoC.Loja))
		lcLoja = 0
	ELSE
		lcLoja = uCrsPromoC.Loja
	ENDIF
	
	IF used("uCrsOverlap")
		Fecha("uCrsOverlap")
	ENDIF 
	
	create cursor uCrsOverlap (stref c(18), stdescr c(60),ref c(18),design c(60), tipodescr c(60), dataini d, datafim d, overlap c(3))
	
	SELECT uCrsPromoO
	IF RECCOUNT()>0
		GO TOP 
		Scan
			TEXT TO lcSQL TEXTMERGE NOSHOW
				exec up_promocoes_overlap_o '<<IIF(Empty(uCrsPromoO.cstamp),"",ucrsPromoO.cstamp)>>','<<ALLTRIM(uCrsPromoO.ref)>>', '<<lcLoja>>', '<<dtosql(lcDataIni)>>','<<dtosql(lcDataFim)>>'
			ENDTEXT
			IF (u_sqlexec(lcSQL,"uCrsTempOverlap"))
				SELECT uCrsOverlap
				APPEND FROM DBF("uCrsTempOverlap")
			ELSE
				MESSAGEBOX("OCORREU UM ERRO NA PESQUISA DE SOBREPOSI��ES! POR FAVOR CONTACTE O SUPORTE: Cod: Pesq-Overlap",16,"LOGITOOLS SOFTWARE")		
			endif
		endscan
	Endif
	
	SELECT uCrsPromoD
	IF RECCOUNT()>0
		GO TOP 
		Scan
			TEXT TO lcSQL TEXTMERGE NOSHOW
				exec up_promocoes_overlap_d '<<IIF(Empty(uCrsPromoD.cstamp),"",ucrsPromoD.cstamp)>>','<<ALLTRIM(uCrsPromoD.ref)>>', '<<lcLoja>>', '<<dtosql(lcDataIni)>>','<<dtosql(lcDataFim)>>'
			ENDTEXT
			IF (u_sqlexec(lcSQL,"uCrsTempOverlap"))
				SELECT uCrsOverlap
				APPEND FROM DBF("uCrsTempOverlap")
			ELSE
				MESSAGEBOX("OCORREU UM ERRO NA PESQUISA DE SOBREPOSI��ES! POR FAVOR CONTACTE O SUPORTE: Cod: Pesq-Overlap",16,"LOGITOOLS SOFTWARE")	
			endif
		endscan
	Endif
	
	promocoes.pageframe1.page4.grid1.refresh
	
	&& configura cor do botao de overlap
		IF RECCOUNT("uCrsOverlap")>0
			promocoes.ctnMenu.ctnBtns.btnOverlap.backcolor = RGB(132,0,0)
		else
			promocoes.ctnMenu.ctnBtns.btnOverlap.backcolor = RGB(240,240,240)
		endif	
	
	&& fecha o cursor temporario	
		IF USED("uCrsTempOverlap")
			Fecha("uCrsTempOverlap")
		ENDIF
	
	DO uf_validaExcepcoes
ENDFUNC


*****************************************************************
* 				VERIFICA SE EXISTEM SOBREPOSI��ES				*
*****************************************************************
FUNCTION uf_ValidaExcepcoes
	SELECT uCrsPromoE
	CALCULATE COUNT(VAL(uCrsPromoE.ref)) TO lcCountE
	
	IF lcCountE>0
		promocoes.ctnMenu.CtnBtns.btnExcepcoes.backcolor=RGB(132,0,0)
	ELSE
		promocoes.ctnMenu.CtnBtns.btnExcepcoes.backcolor=RGB(240,240,240)	
	endif
	
ENDFUNC

*****************************************************************
* 				VALIDA OS DADOS PARA GRAVAR						*
*****************************************************************

FUNCTION uf_validaPromo
	LOCAL lcCountO,lcCountD
	STORE 0 TO lcCountO,lcCountD
	
***********
* PROMO-C *
***********
	SELECT uCrsPromoC
	IF len(uCrsPromoC.ref)=0
		MESSAGEBOX("DESCULPE, MAS EXISTEM CAMPOS OBRIGAT�RIOS POR PREENCHER. POR FAVOR VERIFIQUE O CAMPO: REFER�NCIA!",48,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF

	SELECT uCrsPromoC
	IF len(uCrsPromoC.Design)=0
		MESSAGEBOX("DESCULPE, MAS EXISTEM CAMPOS OBRIGAT�RIOS POR PREENCHER. POR FAVOR VERIFIQUE O CAMPO: DESIGNA��O!",48,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF
	
	IF EMPTY(uCrsPromoC.loja)
		MESSAGEBOX("DESCULPE, MAS EXISTEM CAMPOS OBRIGAT�RIOS POR PREENCHER. POR FAVOR VERIFIQUE O CAMPO: LOJA!",48,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF
	
	IF EMPTY(uCrsPromoC.tipoDescr)
		MESSAGEBOX("DESCULPE, MAS EXISTEM CAMPOS OBRIGAT�RIOS POR PREENCHER. POR FAVOR VERIFIQUE O CAMPO: RELA��O!",48,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF

	if uCrsPromoC.dataini > uCrsPromoC.datafim
		MESSAGEBOX("DESCULPE, MAS A DATA INICIAL N�O PODE SER INFERIOR � DATA FINAL.",48,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF

	IF (uCrsPromoC.Forn) AND EMPTY(uCrsPromoC.Forn_nome)
		MESSAGEBOX("DESCULPE, MAS EXISTEM CAMPOS OBRIGAT�RIOS POR PREENCHER. POR FAVOR INDIQUE UM FORNECEDOR!",48,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF
	
***********
* PROMO-O *
***********
	SELECT uCrsPromoO
	CALCULATE COUNT(VAL(uCrsPromoO.ref)) TO lcCountO	
	
	IF lcCountO = 0
		MESSAGEBOX("DESCULPE, MAS TEM DE PREENCHER OS PRODUTOS DE ORIGEM!",48,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF
	
	IF Like('Um -*',uCrsPromoC.TipoDescr) AND lcCountO >1
		MESSAGEBOX("DESCULPE, MAS O TIPO DE RELA��O SELECCIONADA APENAS PERMITE UM PRODUTO NA GRELHA DE ORIGEM!",48,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF
	
	IF Like('Muitos -*',uCrsPromoC.TipoDescr) AND lcCountO <2
		MESSAGEBOX("DESCULPE, MAS O TIPO DE RELA��O SELECCIONADA REQUER MAIS DO QUE UM PRODUTO NA GRELHA DE ORIGEM!",48,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF
	
	SELECT uCrsPromoO
	GO TOP
	SCAN FOR uCrsPromoO.qt=0
			lcPos = RECNO()
			MESSAGEBOX("DESCULPE, MAS N�O PODE TER PRODUTOS COM QUANTIDADE 0 NA GRELHA DE ORIGEM!",48,"LOGITOOLS SOFTWARE")
			SELECT uCrsPromoO
			GO lcPos
			promocoes.pageframe1.page2.ctnOrigem.grdOrigem.click
			RETURN .f.
	ENDSCAN

***********
* PROMO-D *
***********	
	SELECT uCrsPromoD
	CALCULATE COUNT(VAL(uCrsPromoD.ref)) TO lcCountD
	
	IF lcCountO = 0
		MESSAGEBOX("DESCULPE, MAS TEM DE PREENCHER OS PRODUTOS DE DESTINO!",48,"LOGITOOLS SOFTWARE")
		RETURN .f.
	Endif
		
	IF  Like('*-> Um',uCrsPromoC.TipoDescr) AND lcCountD>1
		MESSAGEBOX("DESCULPE, MAS O TIPO DE RELA��O SELECCIONADA APENAS PERMITE UM PRODUTO NA GRELHA DE DESTINO!",48,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF
	
	IF Like('*-> Muitos',uCrsPromoC.TipoDescr) AND lcCountD<2
		MESSAGEBOX("DESCULPE, MAS O TIPO DE RELA��O SELECCIONADA REQUER MAIS DO QUE UM PRODUTO NA GRELHA DE DESTINO!",48,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF
	
	SELECT uCrsPromoD
	GO TOP
	SCAN FOR uCrsPromoD.qt = 0 AND uCrsPromoD.perct = 0 AND uCrsPromoD.valor = 0
		lcPos = RECNO()
		MESSAGEBOX("DESCULPE, MAS N�O PODE TER PRODUTOS COM QUANTIDADE, PERCENTAGEM, E VALOR TODOS A 0 NA GRELHA DE DESTINO!",48,"LOGITOOLS SOFTWARE")
		SELECT uCrsPromoD
		GO lcPos
		promocoes.pageframe1.page2.ctnDestino.grdDestino.click
		RETURN .f.
	endscan	


***********
* Overlap *
***********	
	SELECT uCrsPromoC
	IF RECCOUNT("uCrsOverlap")>0
		SELECT uCrsOverlap
		GO top
		SCAN FOR uCrsOverlap.overlap == 'Sim'
			MESSAGEBOX("DESCULPE, MAS O PRODUTO: "+uCrsOverlap.ref+" EST� INCLU�DO NA PROMO��O: "+uCrsOverlap.ref+" QUE N�O PERMITE SOBREPOSI��ES!",48,"LOGITOOLS SOFTWARE")
			RETURN .f.
		endscan
	ENDIF
	
************	
* FAMILIAS *
************	
	SELECT uCrsPromoC
	IF uCrsPromoC.Overlap
		lcValida = uf_ValidaFamilias(1)
	ELSE
		lcValida = uf_ValidaFamilias(2)
	endif	
	IF lcValida
		MESSAGEBOX("DESCULPE, MAS N�O PODE TER FAM�LIAS E PRODUTOS NA MESMA PRODU��O!",48,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF
	
*************	
* EXCEPCOES *
*************
	SELECT uCrsPromoE
	CALCULATE COUNT(VAL(uCrsPromoE.ref)) TO lcCountE	
	
	SELECT uCrsPromoC
	IF !(uCrsPromoC.familia) AND lcCountE>0
		MESSAGEBOX("DESCULPE, S� PODE ADICIONAR EXCEP��ES QUANDO A PROMO��O EST� APLICADA A FAM�LIAS!",48,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF
	
	LOCAL lcValidaAddPromoE
	STORE .f. TO lcValidaAddPromoE
	
	IF lcCountE > 0
		Select uCrsPromoE
		GO top
		SCAN 
				
			Select uCrsPromoO
			Go Top 
			SCAN FOR ALLTRIM(uCrsPromoE.familia)=ALLTRIM(uCrsPromoO.descr)
				lcValidaAddPromoE= .t.
			ENDSCAN
			
			Select uCrsPromoD
			Go Top 
			SCAN FOR ALLTRIM(uCrsPromoE.familia)=ALLTRIM(uCrsPromoD.descr)
				lcValidaAddPromoE = .t.
			endscan
			
			If !(lcValidaAddPromoE)
				Messagebox("DESCULPE, MAS O PRODUTO ["+ALLTRIM(uCrsPromoE.ref)+"] NAS EXCEP��ES N�O FAZ PARTE DA(S) FAM�LIA(S) A QUE A PROMO��O SE APLICA. POR FAVOR RECTIFIQUE.",48,"LOGITOOLS SOFTWARE")
				RETURN .f.
			ENDIF
			
			lcValidaAddPromoE = .f.
			
			SELECT uCrsPromoE
		ENDSCAN	
	Endif	
ENDFUNC


*****************************************************************
* 				INSERE CABE�ALHO DA PROMO						*
*****************************************************************

FUNCTION uf_inserePromoC
	*ON ERROR wait windows "A gravar PROMO-C." timeout 1
	
	SET POINT TO "."
	
	&& Verifica N� REF 
		u_sqlexec([Select Isnull(Max(ref),0)+1 as ref From B_promoC (nolock)],[uCrsTempRef])
		SELECT uCrsTempRef
		SELECT uCrsPromoC
		IF uCrsTempRef.ref > VAL(uCrsPromoC.Ref)
			replace uCrsPromoC.Ref WITH astr(uCrsTempRef.ref)
			promocoes.pageframe1.page1.txtRef.refresh
		Endif
		fecha("uCrsTempRef")
	**********
		
	lcSQL=""
	TEXT TO lcSQl TEXTMERGE NOSHOW
		INSERT INTO B_promoC
			(cStamp,tipo,tipoDescr,ref,design,status,dataIni,dataFim,loja,lojaDescr,overlap,maiscaro,acumulaccl,familia,forn,forn_no,forn_estab,
			 forn_nome,backOffice,oUsr,oData,oHora,aUsr,aData,ahora,limitex)
		VALUES 
			(
				'<<uCrsPromoC.cstamp>>',
				<<uCrsPromoC.Tipo>>,
				'<<uCrsPromoC.TipoDescr>>',
				<<uCrsPromoC.ReF)>>,
				'<<ALLTRIM(uCrsPromoC.Design)>>',
				'<<ALLTRIM(uCrsPromoC.Status)>>',
				'<<dtosql(uCrsPromoC.DataIni)>>',
				'<<dtosql(uCrsPromoC.DataFim)>>',
				'<<uCrsPromoC.Loja>>',
				'<<uCrsPromoC.LojaDescr)>>',
				<<IIF(uCrsPromoC.overlap,1,0)>>,
				<<IIF(uCrsPromoC.maiscaro,1,0)>>,
				<<IIF(uCrsPromoC.acumulaccl,1,0)>>,
				<<IIF(uCrsPromoC.familia,1,0)>>,
				<<IIF(uCrsPromoC.Forn,1,0)>>,
				<<uCrsPromoC.Forn_No>>,
				<<uCrsPromoC.Forn_Estab>>,
				'<<ALLTRIM(uCrsPromoC.Forn_Nome)>>',
				<<IIF(uCrsPromoC.BackOffice,1,0)>>,
				'<<m.m_chinis>>',
				convert(varchar,getdate(),102),
				convert(varchar,getdate(),108),
				'<<m.m_chinis>>',
				convert(varchar,getdate(),102),
				convert(varchar,getdate(),108),
				<<uCrsPromoC.Limitex>>
			)
	ENDTEXT	
	IF !u_sqlexec(lcSQL)
		MESSAGEBOX("DESCULPE, MAS OCORREU UM ERRO A GRAVAR O CABE�ALHO DA PROMO��O. POR FAVOR CONTACTE O SUPORTE."+CHR(10)+"Cod: DOC-PROMO-C",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ELSE
		RETURN .t.
	Endif

ENDFUNC

*****************************************************************
* 				INSERE LINHAS DA ORIGEM DA PROMO				*
*****************************************************************

FUNCTION uf_inserePromoO
	ON ERROR wait windows "A gravar PROMO-O." timeout 1

	SET POINT TO "."
	lcSQL=""
	SELECT uCrsPromoC
	SELECT uCrsPromoO
	GO TOP 
	scan
		TEXT TO lcSQl TEXTMERGE NOSHOW
			INSERT INTO B_PromoO (cStamp,oStamp,tipo,familia,ref,descr,qt,oUsr,oData,oHora,aUsr,aData,aHora)
			Values
				(
				 '<<uCrsPromoC.Cstamp>>',
				 '<<uCrsPromoO.ostamp>>',
				 <<uCrsPromoC.Tipo>>,
				 <<IIF(uCrsPromoC.familia,1,0)>>,
				 '<<ALLTRIM(uCrsPromoO.ref)>>',
				 '<<ALLTRIM(uCrsPromoO.descr)>>',
				 <<uCrsPromoO.qt>>,
				 '<<m.m_chinis>>',
				convert(varchar,getdate(),102),
				convert(varchar,getdate(),108),
				'<<m.m_chinis>>',
				convert(varchar,getdate(),102),
				convert(varchar,getdate(),108)
				)
		ENDTEXT	
	ENDSCAN
	IF !u_sqlexec(lcSQL)
		MESSAGEBOX("DESCULPE, MAS OCORREU UM ERRO A GRAVAR AS LINHAS DE ORIGEM DA PROMO��O. POR FAVOR CONTACTE O SUPORTE."+CHR(10)+"Cod: DOC-PROMO-O",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ELSE
		RETURN .t.
	Endif
ENDFUNC

*****************************************************************
* 				INSERE LINHAS DE DESTINO DA PROMO				*
*****************************************************************

FUNCTION uf_inserePromoD
	ON ERROR wait windows "A gravar PROMO-D." timeout 1

	SET POINT TO "."
	lcSQL=""
	SELECT uCrsPromoD
	GO TOP 
	scan
		TEXT TO lcSQl TEXTMERGE NOSHOW
			INSERT INTO B_PromoD (cStamp,dStamp,ref,descr,qt,perct,valor,oUsr,oData,oHora,aUsr,aData,aHora)
			Values
				(
				 '<<uCrsPromoC.cstamp>>',
				 '<<uCrsPromoD.dstamp>>',
				 '<<ALLTRIM(uCrsPromoD.ref)>>',
				 '<<ALLTRIM(uCrsPromoD.descr)>>',
				 <<uCrsPromoD.qt>>,
				 <<uCrsPromoD.perct>>,
				 <<uCrsPromoD.valor>>,
				 '<<m.m_chinis>>',
				convert(varchar,getdate(),102),
				convert(varchar,getdate(),108),
				'<<m.m_chinis>>',
				convert(varchar,getdate(),102),
				convert(varchar,getdate(),108)
				)
		ENDTEXT	
	ENDSCAN
	IF !u_sqlexec(lcSQL)
		MESSAGEBOX("DESCULPE, MAS OCORREU UM ERRO A GRAVAR AS LINHAS DE DESTINO DA PROMO��O. POR FAVOR CONTACTE O SUPORTE."+CHR(10)+"Cod: DOC-PROMO-D",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ELSE
		RETURN .t.
	Endif
ENDFUNC

*****************************************************************
* 				INSERE LINHAS DE EXCEPCOES DA PROMO				*
*****************************************************************

FUNCTION uf_inserePromoE
	ON ERROR wait windows "A gravar PROMO-E." timeout 1
	LOCAL lcCountE
	STORE 0 TO lcCountE
	
	SET POINT TO "."

	SELECT uCrsPromoE
	CALCULATE COUNT(VAL(uCrsPromoE.ref)) TO lcCountE

	lcSQL=""
	SELECT uCrsPromoE
	IF lcCountE>0
		GO TOP 
		scan
			TEXT TO lcSQl TEXTMERGE NOSHOW
				INSERT INTO B_PromoE (cStamp,eStamp,ref,descr,oUsr,oData,oHora,aUsr,aData,aHora)
				Values
					(
					 '<<uCrsPromoC.cstamp>>',
					 '<<uCrsPromoE.Estamp>>',
					 '<<ALLTRIM(uCrsPromoE.ref)>>',
					 '<<ALLTRIM(uCrsPromoE.descr)>>',
					 '<<m.m_chinis>>',
					 convert(varchar,getdate(),102),
					 convert(varchar,getdate(),108),
					 '<<m.m_chinis>>',
					 convert(varchar,getdate(),102),
					 convert(varchar,getdate(),108),
					 '<<m.m_chinis>>',
					 convert(varchar,getdate(),102),
					 convert(varchar,getdate(),108)
					)
			ENDTEXT	
		ENDSCAN
		IF !u_sqlexec(lcSQL)
			MESSAGEBOX("DESCULPE, MAS OCORREU UM ERRO A GRAVAR AS LINHAS DE EXCEP��ES DA PROMO��O. POR FAVOR CONTACTE O SUPORTE."+CHR(10)+"Cod: DOC-PROMO-E",16,"LOGITOOLS SOFTWARE")
			RETURN .f.
		ELSE
			RETURN .t.
		ENDIF
	ELSE
		&& NAO TEM LINHAS PARA INSERIR
		RETURN .t.	
	ENDIF
ENDFUNC

*****************************************************************
* 				ACTUALIZA LINHAS CABE�ALHO DA PROMO				*
*****************************************************************

FUNCTION uf_actualizaPromoC
	*ON ERROR wait windows "A actualizar PROMO-C." timeout 1
	SET POINT TO "."
	
	SELECT uCrsPromoC
	lcSQL=""
	TEXT TO lcSQl TEXTMERGE NOSHOW
		Update B_promoC
		SET
			tipo 		= <<uCrsPromoC.Tipo)>>,
			tipoDescr 	= '<<uCrsPromoC.TipoDescr>>',
			design 		= '<<ALLTRIM(uCrsPromoC.Design)>>',
			status 		= '<<ALLTRIM(uCrsPromoC.Status)>>', 
			dataIni 	= '<<dtosql(uCrsPromoC.DataIni)>>',
			dataFim 	= '<<dtosql(uCrsPromoC.DataFim)>>',
			loja 		= '<<uCrsPromoC.Loja>>',
			lojaDescr 	= '<<ALLTRIM(uCrsPromoC.LojaDescr)>>',
			overlap 	= <<IIF(uCrsPromoC.overlap,1,0)>>,
			maiscaro 	= <<IIF(uCrsPromoC.maiscaro,1,0)>>,
			acumulaccl 	= <<IIF(uCrsPromoC.acumulaccl,1,0)>>,
			familia		= <<IIF(uCrsPromoC.familia,1,0)>>,
			forn 		= <<IIF(uCrsPromoC.Forn,1,0)>>,
			forn_no 	=  <<uCrsPromoC.Forn_No>>,
			forn_estab 	= <<uCrsPromoC.Forn_Estab>>,
			forn_nome 	= '<<ALLTRIM(uCrsPromoC.Forn_Nome)>>',
			backOffice 	= <<IIF(uCrsPromoC.BackOffice,1,0)>>,
			oUsr 		= '<<m.m_chinis>>',
			oData 		= convert(varchar,getdate(),102),
			oHora 		= convert(varchar,getdate(),108),
			aUsr 		= '<<m.m_chinis>>',
			aData 		= convert(varchar,getdate(),102),
			ahora		= convert(varchar,getdate(),108),
			limitex		= <<uCrsPromoC.Limitex>>
		where cstamp 	= '<<uCrsPromoC.cstamp>>'	
	ENDTEXT	
	IF !u_sqlexec(lcSQL)
		MESSAGEBOX("DESCULPE, MAS OCORREU UM ERRO A ACTUALIZAR A PROMO��O. POR FAVOR CONTACTE O SUPORTE."+CHR(10)+"Cod: PROMO-ACT(1)",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ELSE
		RETURN .t.
	Endif
ENDFUNC

*****************************************************************
* 				ACTUALIZA LINHAS ORIGEM DA PROMO				*
*****************************************************************

FUNCTION uf_actualizaPromoO
	*ON ERROR wait windows "A actualizar PROMO-O." timeout 1
	LOCAL lcValida
	STORE .t. TO lcValida

	SET POINT TO "."
	
	&& Obtem os produtos anteriores para compara��o
		SELECT uCrsPromoC
		lcSQL = ""
		TEXT TO lcSQL NOSHOW textmerge

			SELECT	ostamp,ref
			FROM	B_PromoO (nolock)
			WHERE	cstamp = '<<ALLTRIM(uCrsPromoC.cStamp)>>'
		ENDTEXT

		IF !u_sqlexec(lcSQL,"uCrsPromoO_Anterior")
			MESSAGEBOX("DESCULPE, MAS OCORREU UM ERRO A ACTUALIZAR A PROMO��O. POR FAVOR CONTACTE O SUPORTE."+CHR(10)+"Cod: PROMO-ACT(2)",16,"LOGITOOLS SOFTWARE")
			lcValida = .f.
		ENDIF
	
	IF lcValida
		&& APAGA LINHAS QUE J� N�O EXISTEM, existem na BD Mas n�o existem no cursor proposto para grava��o
			Select * from uCrsPromoO_Anterior where oStamp not in (select oStamp from uCrsPromoO) into cursor lcCursorDeletePromoO
			
			Select lcCursorDeletePromoO
			Go top
			Scan
				lcSQL = ""
				TEXT TO lcSQL NOSHOW textmerge
					DELETE	FROM	B_PromoO
					WHERE	oStamp = '<<ALLTRIM(lcCursorDeletePromoO.otstamp)>>'
				ENDTEXT

				IF !u_sqlexec(lcSQL)
					MESSAGEBOX("DESCULPE, MAS OCORREU UM ERRO A ACTUALIZAR A PROMO��O. POR FAVOR CONTACTE O SUPORTE."+CHR(10)+"Cod: PROMO-ACT(3)",16,"LOGITOOLS SOFTWARE")
					lcValida = .f.
				Endif

			ENDSCAN
					
			IF lcValida
				&& INSERE LINHAS NOVAS, EXISTEM NO CURSOR MAS N�O NA BASE DE DADOS
					SELECT * from uCrsPromoO where oStamp not in (select ostamp from uCrsPromoO_Anterior) into cursor lcCursorInsertPromoO
					Select lcCursorInsertPromoO
					Go top
					SCAN
						lcSQL=""
						TEXT TO lcSQl TEXTMERGE NOSHOW
							INSERT INTO B_PromoO (cStamp,oStamp,tipo,familia,ref,descr,qt,oUsr,oData,oHora,aUsr,aData,aHora)
							Values
								(
								 '<<uCrsPromoC.Cstamp>>',
								 '<<uCrsPromoO.ostamp>>',
								 <<uCrsPromoC.Tipo>>,
								 <<IIF(uCrsPromoC.familia,1,0)>>,
								 '<<ALLTRIM(uCrsPromoO.ref)>>',
								 '<<ALLTRIM(uCrsPromoO.descr)>>',
								 <<uCrsPromoO.qt>>,
								 '<<m.m_chinis>>',
								 convert(varchar,getdate(),102),
								 convert(varchar,getdate(),108),
								 '<<m.m_chinis>>',
								 convert(varchar,getdate(),102),
								 convert(varchar,getdate(),108)
								)
						ENDTEXT	
						
						IF !u_sqlexec(lcSQL)
							MESSAGEBOX("DESCULPE, MAS OCORREU UM ERRO A ACTUALIZAR A PROMO��O. POR FAVOR CONTACTE O SUPORTE."+CHR(10)+"Cod: PROMO-ACT(4)",16,"LOGITOOLS SOFTWARE")
							lcValida = .f.
							exit
						ENDIF
					ENDSCAN	
			Endif
	Endif
	
	IF lcValida
		&& UPDATE �S LINHAS EXISTENTES, EXISTEM NA BD E NO CURSOR		
			SELECT * from uCrsPromoO where oStamp in (select oStamp from uCrsPromoO_Anterior) into cursor lcCursorUpdatePromoO
		
			SELECT lcCursorUpdatePromoO
			GO TOP 
			scan
				lcSQL=""
				TEXT TO lcSQl TEXTMERGE NOSHOW
					Update B_PromoO 
					set
						 tipo	 	= '<<uCrsPromoC.Tipo>>',
						 familia	= <<IIF(uCrsPromoC.familia,1,0)>>,
						 ref 		= '<<ALLTRIM(lcCursorUpdatePromoO.ref)>>',
						 descr		= '<<ALLTRIM(lcCursorUpdatePromoO.descr)>>',
						 qt 		= <<lcCursorUpdatePromoO.qt>>,
						 oUsr 		= '<<m.m_chinis>>',
						 oData 		= convert(varchar,getdate(),102),
						 oHora 		= convert(varchar,getdate(),108),
						 aUsr 		= '<<m.m_chinis>>',
						 aData 		= convert(varchar,getdate(),102),
						 ahora		= convert(varchar,getdate(),108)
					where oStamp 	= '<<lcCursorUpdatePromoO.ostamp>>'
				ENDTEXT	
				msg(lcsql)
				IF !u_sqlexec(lcSQL)
					MESSAGEBOX("DESCULPE, MAS OCORREU UM ERRO A ACTUALIZAR A PROMO��O. POR FAVOR CONTACTE O SUPORTE."+CHR(10)+"Cod: PROMO-ACT(5)",16,"LOGITOOLS SOFTWARE")
					lcValida = .f.
					exit
				Endif
			ENDSCAN	
	ENDIF
	
	&& Fecha Cursores
			
		IF USED("lcCursorDeletePromoO")
			Fecha("lcCursorDeletePromoO")
		ENDIF
		
		IF USED("lcCursorInsertPromoO")
			Fecha("lcCursorInsertPromoO")
		ENDIF
		
		IF USED("lcCursorUpdatePromoO")
			Fecha("lcCursorUpdatePromoO")
		ENDIF
		
		IF USED("uCrsPromoO_Anterior")
			Fecha("uCrsPromoO_Anterior")
		ENDIF
	
	&& Devolve Resultado
		IF lcValida
			RETURN .t.
		ELSE
			RETURN .f.
		endif		
ENDFUNC



*****************************************************************
* 				ACTUALIZA LINHAS DESTINO DA PROMO				*
*****************************************************************

FUNCTION uf_actualizaPromoD
	*ON ERROR wait windows "A actualizar PROMO-D." timeout 1
	LOCAL lcValida
	STORE .t. TO lcValida

	SET POINT TO "."
	
	&& Obtem os produtos anteriores para compara��o
		SELECT uCrsPromoC
		lcSQL = ""
		TEXT TO lcSQL NOSHOW textmerge

			SELECT	Dstamp,ref
			FROM	B_PromoD (nolock)
			WHERE	cstamp = '<<ALLTRIM(uCrsPromoC.cStamp)>>'
		ENDTEXT

		IF !u_sqlexec(lcSQL,"uCrsPromoD_Anterior")
			MESSAGEBOX("DESCULPE, MAS OCORREU UM ERRO A ACTUALIZAR A PROMO��O. POR FAVOR CONTACTE O SUPORTE."+CHR(10)+"Cod: PROMO-ACT(6)",16,"LOGITOOLS SOFTWARE")
			lcValida = .f.
		ENDIF
	
	IF lcValida
		&& APAGA LINHAS QUE J� N�O EXISTEM, existem na BD Mas n�o existem no cursor proposto para grava��o
			Select * from uCrsPromoD_Anterior where dStamp not in (select dStamp from uCrsPromoD) into cursor lcCursorDeletePromoD
			
			Select lcCursorDeletePromoD
			Go top
			Scan
				lcSQL = ""
				TEXT TO lcSQL NOSHOW textmerge
					DELETE	FROM	B_PromoD
					WHERE	DStamp = '<<ALLTRIM(lcCursorDeletePromod.dtstamp)>>'
				ENDTEXT

				IF !u_sqlexec(lcSQL)
					MESSAGEBOX("DESCULPE, MAS OCORREU UM ERRO A ACTUALIZAR A PROMO��O. POR FAVOR CONTACTE O SUPORTE."+CHR(10)+"Cod: PROMO-ACT(7)",16,"LOGITOOLS SOFTWARE")
					lcValida = .f.
				Endif

			ENDSCAN
					
			IF lcValida
				&& INSERE LINHAS NOVAS, EXISTEM NO CURSOR MAS N�O NA BASE DE DADOS
					SELECT * from uCrsPromoD where dStamp not in (select dstamp from uCrsPromoD_Anterior) into cursor lcCursorInsertPromoD
					Select lcCursorInsertPromoD
					Go top
					SCAN
						lcSQL=""
						TEXT TO lcSQl TEXTMERGE NOSHOW
							INSERT INTO B_PromoD (cStamp,dStamp,ref,descr,qt,perct,valor,oUsr,oData,oHora,aUsr,aData,aHora)
							Values
								(
								 '<<uCrsPromoC.cstamp>>',
								 '<<uCrsPromoD.dstamp>>',
								 '<<ALLTRIM(uCrsPromoD.ref)>>',
								 '<<ALLTRIM(uCrsPromoD.descr)>>',
								 <<uCrsPromoD.qt>>,
								 <<uCrsPromoD.perct>>,
								 <<uCrsPromoD.valor>>,
								 '<<m.m_chinis>>',
								 convert(varchar,getdate(),102),
								 convert(varchar,getdate(),108),
								 '<<m.m_chinis>>',
								 convert(varchar,getdate(),102),
								 convert(varchar,getdate(),108)
								)
						ENDTEXT	
						
						IF !u_sqlexec(lcSQL)
							MESSAGEBOX("DESCULPE, MAS OCORREU UM ERRO A ACTUALIZAR A PROMO��O. POR FAVOR CONTACTE O SUPORTE."+CHR(10)+"Cod: PROMO-ACT(8)",16,"LOGITOOLS SOFTWARE")
							lcValida = .f.
						Endif	
					ENDSCAN					
			Endif
	Endif
	
	IF lcValida
		&& UPDATE �S LINHAS EXISTENTES, EXISTEM NA BD E NO CURSOR		
			SELECT * from uCrsPromoD where dStamp in (select dStamp from uCrsPromoD_Anterior ) into cursor lcCursorUpdatePromoD
			
			SELECT lcCursorUpdatePromoD
			GO TOP 
			scan
				lcSQL=""
				TEXT TO lcSQl TEXTMERGE NOSHOW
					Update B_PromoD 
					set
						 ref	 	= '<<ALLTRIM(lcCursorUpdatePromoD.ref)>>',
						 descr 		= '<<ALLTRIM(lcCursorUpdatePromoD.descr)>>',
						 qt 		= <<lcCursorUpdatePromoD.qt>>,
						 perct		= <<lcCursorUpdatePromoD.perct>>,
						 valor		= <<lcCursorUpdatePromoD.valor>>,
						 oUsr 		= '<<m.m_chinis>>',
						 oData 		= convert(varchar,getdate(),102),
						 oHora 		= convert(varchar,getdate(),108),
						 aUsr 		= '<<m.m_chinis>>',
						 aData 		= convert(varchar,getdate(),102),
						 ahora		= convert(varchar,getdate(),108)
					where dstamp = '<<lcCursorUpdatePromoD.dstamp>>'
				ENDTEXT	
				
				IF !u_sqlexec(lcSQL)
					MESSAGEBOX("DESCULPE, MAS OCORREU UM ERRO A ACTUALIZAR A PROMO��O. POR FAVOR CONTACTE O SUPORTE."+CHR(10)+"Cod: PROMO-ACT(9)",16,"LOGITOOLS SOFTWARE")
					lcValida = .f.
					exit
				Endif	
			ENDSCAN
	ENDIF
	
	&& Fecha Cursores
		IF USED("lcCursorDeletePromoD")
			Fecha("lcCursorDeletePromoD")
		ENDIF
		
		IF USED("lcCursorInsertPromoD")
			Fecha("lcCursorInsertPromoD")
		ENDIF
		
		IF USED("lcCursorUpdatePromoD")
			Fecha("lcCursorUpdatePromoD")
		ENDIF
		
		IF USED("uCrsPromoD_Anterior")
			Fecha("uCrsPromoD_Anterior")
		ENDIF
	
	&& Devolve Resultado
		IF lcValida
			RETURN .t.
		ELSE
			RETURN .f.
		endif	
ENDFUNC



*****************************************************************
* 				ACTUALIZA LINHAS EXCEP�OES DA PROMO - E			*
*****************************************************************

FUNCTION uf_actualizaPromoE
	*ON ERROR wait windows "A actualizar PROMO-E." timeout 1
	LOCAL lcValida
	STORE .t. TO lcValida

	SET POINT TO "."
	
	&& Obtem os produtos anteriores para compara��o
		SELECT uCrsPromoC
		lcSQL = ""
		TEXT TO lcSQL NOSHOW textmerge

			SELECT	Estamp,ref
			FROM	B_PromoE (nolock)
			WHERE	cstamp = '<<ALLTRIM(uCrsPromoC.cStamp)>>'
		ENDTEXT

		IF !u_sqlexec(lcSQL,"uCrsPromoE_Anterior")
			MESSAGEBOX("DESCULPE, MAS OCORREU UM ERRO A ACTUALIZAR A PROMO��O. POR FAVOR CONTACTE O SUPORTE."+CHR(10)+"Cod: PROMO-ACT(10)",16,"LOGITOOLS SOFTWARE")
			lcValida = .f.
		ENDIF
	
	IF lcValida
		&& APAGA LINHAS QUE J� N�O EXISTEM, existem na BD Mas n�o existem no cursor proposto para grava��o
			Select * from uCrsPromoE_Anterior where eStamp not in (select eStamp from uCrsPromoE) into cursor lcCursorDeletePromoE
			
			Select lcCursorDeletePromoE
			Go top
			Scan
				lcSQL = ""
				TEXT TO lcSQL NOSHOW textmerge
					DELETE	FROM	B_PromoE
					WHERE	eStamp = '<<ALLTRIM(lcCursorDeletePromoE.eStamp)>>'
				ENDTEXT

				IF !u_sqlexec(lcSQL)
					MESSAGEBOX("DESCULPE, MAS OCORREU UM ERRO A ACTUALIZAR A PROMO��O. POR FAVOR CONTACTE O SUPORTE."+CHR(10)+"Cod: PROMO-ACT(11)",16,"LOGITOOLS SOFTWARE")
					lcValida = .f.
				Endif

			ENDSCAN
					
			IF lcValida
				&& INSERE LINHAS NOVAS, EXISTEM NO CURSOR MAS N�O NA BASE DE DADOS
					SELECT * from uCrsPromoE where eStamp not in (select eStamp from uCrsPromoE_Anterior) into cursor lcCursorInsertPromoE
					Select lcCursorInsertPromoE
					Go top
					SCAN
						lcSQL=""
						TEXT TO lcSQl TEXTMERGE NOSHOW
							INSERT INTO B_PromoE (cStamp,eStamp,ref,descr,oUsr,oData,oHora,aUsr,aData,aHora)
							Values
								(
								 '<<uCrsPromoC.cstamp>>',
								 '<<uCrsPromoE.Estamp>>',
								 '<<ALLTRIM(uCrsPromoE.ref)>>',
								 '<<ALLTRIM(uCrsPromoE.descr)>>',
								 '<<m.m_chinis>>',
								 convert(varchar,getdate(),102),
								 convert(varchar,getdate(),108),
								 '<<m.m_chinis>>',
								 convert(varchar,getdate(),102),
								 convert(varchar,getdate(),108),
								 '<<m.m_chinis>>',
								 convert(varchar,getdate(),102),
								 convert(varchar,getdate(),108)
								)
						ENDTEXT		
						
						IF !u_sqlexec(lcSQL)
							MESSAGEBOX("DESCULPE, MAS OCORREU UM ERRO A ACTUALIZAR A PROMO��O. POR FAVOR CONTACTE O SUPORTE."+CHR(10)+"Cod: PROMO-ACT(12)",16,"LOGITOOLS SOFTWARE")
							lcValida = .f.
							exit
						Endif	
					ENDSCAN					
			Endif
	Endif
	
	&& Fecha Cursores		
		IF USED("lcCursorDeletePromoE")
			Fecha("lcCursorDeletePromoE")
		ENDIF
		
		IF USED("lcCursorInsertPromoE")
			Fecha("lcCursorInsertPromoE")
		ENDIF
		
		IF USED("uCrsPromoE_Anterior")
			Fecha("uCrsPromoE_Anterior")
		ENDIF
	
	&& Devolve Resultado
		IF lcValida
			RETURN .t.
		ELSE
			RETURN .f.
		endif	
ENDFUNC


*****************************************************************
* 						FUN��O GRAVAR							*
*****************************************************************
FUNCTION uf_gravarPromo
	LOCAL lcValida
	STORE .t. TO lcValida
	
	lcvalida = uf_validaPromo()
	IF lcValida
		IF promocoes.Caption=="Gest�o de Promo��es - Modo de Introdu��o"
		
			** Trata Transac��es para garantir a Integridade de Dados
			IF u_sqlexec ([BEGIN TRANSACTION])	
				lcValidaInsercaoC 	=	uf_inserePromoC()
				lcValidaInsercaoO 	=	uf_inserePromoO()
				lcValidaInsercaoD 	= 	uf_inserePromoD()
				lcValidaInsercaoE 	= 	uf_inserePromoE()
					
				IF lcValidaInsercaoC AND lcValidaInsercaoO AND lcValidaInsercaoD  AND lcValidaInsercaoE && Tudo Ok, insere o Documento na BD
					u_sqlexec([COMMIT TRANSACTION])
					
					&&Repoe Configura��o do Ecr�
						MESSAGEBOX("REGISTO GRAVADO COM SUCESSO!",64,"LOGITOOLS SOFTWARE")	
						promocoes.Caption="Gest�o de Promo��es"
						SELECT uCrsPromoC
						uf_chamapromo(uCrsPromoC.cstamp)
				ELSE
					u_sqlexec([ROLLBACK])
							
					&& Regista Problema na Tabela de Logs
						Select uCrsPromoC
						Local lcmensagem, lcorigem 
						lcmensagem 	= "Erro na Inser��o na tabela PROMOC; REF: " + Alltrim(uCrsPromoC.Ref) + "; Design: " + Alltrim(uCrsPromoC.Design) + "; cStamp: " + Alltrim(uCrsPromoC.cstamp) + "; User:" + Alltrim(Str(m.ch_userno))
						lcorigem 	= "Painel Promocoes: uf_gravarPromo"
								
						lcSQL = ""
						TEXT TO lcSQL TEXTMERGE NOSHOW
							Insert Into B_elog (tipo, status, mensagem, origem)
							Values 	('E','A','<<Alltrim(lcmensagem)>>', '<<Alltrim(lcorigem)>>')
						ENDTEXT 
						u_sqlexec(lcSQL)
						
					RETURN
				ENDIF
					
			Else
				return
			ENDIF
		
		ELSE  && ALtera��o
			IF u_sqlexec ([BEGIN TRANSACTION])	
				&&Trata Transac��es para garantir a Integridade de Dados	
					lcValidaActualizacaoC 	=	uf_actualizaPromoC()
					lcValidaActualizacaoO	=	uf_actualizaPromoO()
					lcValidaActualizacaoD 	= 	uf_actualizaPromoD()
					lcValidaActualizacaoE 	= 	uf_actualizaPromoE()
						
					IF lcValidaActualizacaoC AND lcValidaActualizacaoO AND lcValidaActualizacaoD  AND lcValidaActualizacaoD && Tudo Ok, insere o Documento na BD
						u_sqlexec([COMMIT TRANSACTION])
						&&Repoe Configura��o do Ecr�
						MESSAGEBOX("REGISTO ALTERADO COM SUCESSO!",64,"LOGITOOLS SOFTWARE")	
						promocoes.Caption="Gest�o de Promo��es"
						SELECT uCrsPromoC
						uf_chamapromo(uCrsPromoC.cstamp)
					ELSE
						u_sqlexec([ROLLBACK])
								
						** Regista Problema na Tabela de Logs
							Select uCrsPromoC
							Local lcmensagem, lcorigem 
							lcmensagem 	= "Erro na Actualiza��o na tabela PROMOC; REF: " + Alltrim(uCrsPromoC.Ref) + "; Design: " + Alltrim(uCrsPromoC.Design) + "; cStamp: " + Alltrim(uCrsPromoC.cstamp) + "; User:" + Alltrim(Str(m.ch_userno))
							lcorigem 	= "Painel Promocoes: uf_gravarPromo"
									
							lcSQL = ""
							TEXT TO lcSQL TEXTMERGE NOSHOW
								Insert Into B_elog (tipo, status, mensagem, origem)
								Values 	('E','A','<<Alltrim(lcmensagem)>>', '<<Alltrim(lcorigem)>>')
							ENDTEXT 
							u_sqlexec(lcSQL)
						
						RETURN
					ENDIF	
			ELSE
				return
			endif			
		ENDIF
	Endif
ENDFUNC



***************
* UnBindEvent *
***************	
Function DesactivaEventosPromo
	lcObj = 'promocoes.pageframe1.page2.ctnDestino.grdDestino'
	
	FOR i=1 to &lcObj..columncount
     	DO case  		
     		CASE UPPER(&lcObj..columns(i).controlsource)=="UCRSPROMOD.QT"
     			UnBindEvent(&lcObj..columns(i).text1,"LostFocus",promocoes.oCust,"QT")
     			
      		CASE UPPER(&lcObj..columns(i).controlsource)=="UCRSPROMOD.PERCT"
     			UnBindEvent(&lcObj..columns(i).text1,"LostFocus",promocoes.oCust,"PERCT")
     			
     		CASE UPPER(&lcObj..columns(i).controlsource)=="UCRSPROMOD.VALOR" 		
     			UnBindEvent(&lcObj..columns(i).text1,"LostFocus",promocoes.oCust,"VALOR")
     		OTHERWISE 
     			***
     	endcase    	    	
	ENDFOR
ENDFUNC 


*****************************************************************
* 						FUN��O ELIMINA PROMO					*
*****************************************************************

FUNCTION uf_eliminaPromocao
	
	***************************
	*Valida acesso - Eliminar *
	***************************
	IF !(uf_gerais_validaPermUser(m.ch_userno,m.ch_grupo,'Gest�o de Promo�oes - Eliminar'))
		Messagebox("O SEU PERFIL N�O PERMITE ELIMINAR PROMO��ES.",48,"LOGITOOLS SOFTWARE")
		return
	ENDIF
	
	If PERGUNTA("Tem a certeza que deseja eliminar a Promo��o?",1,"As promo��o eliminadas s�o registadas")			
	
		** Trata Transac��es para garantir a Integridade de Dados
		IF u_sqlexec ([BEGIN TRANSACTION])
			IF u_sqlexec ([BEGIN TRANSACTION])	
				SELECT uCrsPromoC
				
				&& Cabe�alho
					IF u_sqlexec([delete from B_PromoC where cstamp=']+uCrsPromoC+['])
						&&Origens
						IF u_sqlexec([delete from B_PromoO where cstamp=']+uCrsPromoC+['])	
							&& Destinos
								IF u_sqlexec([delete from B_PromoD where cstamp=']+uCrsPromoC+['])	
									&& Excep��es
										IF u_sqlexec([delete from B_PromoE where cstamp=']+uCrsPromoC+['])	
											&& Regista na tabela de Logs
												SELECT uCrsPromoC
												Local lcmensagem, lcorigem 
												lcmensagem 	= "Foi eliminada a Promo��o, REF: " + Alltrim(uCrsPromoC.Ref) + "; Design: " + Alltrim(uCrsPromoC.Design) + "; cStamp: " + Alltrim(uCrsPromoC.cstamp) + "; User:" + Alltrim(Str(m.ch_userno))
												lcorigem 	= "Painel Promocoes: uf_eliminaPromocao"
															
												lcSQL = ""
												TEXT TO lcSQL TEXTMERGE NOSHOW
													Insert Into B_elog (tipo, status, mensagem, origem)
													Values 	('E','A','<<Alltrim(lcmensagem)>>', '<<Alltrim(lcorigem)>>')
												ENDTEXT 
												
												IF u_sqlexec(lcSQL)
													&& Se todos os deletes + insert no logs correram bem faz commit
														MESSAGEBOX("O REGISTO FOI ELIMINADO!",64,"LOGITOOLS SOFTARE")
														u_sqlexec([COMMIT TRANSACTION])
												ELSE
													&& faz rollback a tudo
														MESSAGEBOX("DESCULPE, MAS OCORREU UM ERRO A ELIMINAR A PROMO��O! POR FAVOR CONTACTE O SUPORTE : C�D: PROMO-DEL(5)",16,"LOGITOOLS SOFTARE")
														u_sqlexec([ROLLBACK])
												endif		
										ELSE
											MESSAGEBOX("DESCULPE, MAS OCORREU UM ERRO A ELIMINAR A PROMO��O! POR FAVOR CONTACTE O SUPORTE : C�D: PROMO-DEL(4)",16,"LOGITOOLS SOFTARE")
											u_sqlexec([ROLLBACK])					
										endif	
								ELSE
									MESSAGEBOX("DESCULPE, MAS OCORREU UM ERRO A ELIMINAR A PROMO��O! POR FAVOR CONTACTE O SUPORTE : C�D: PROMO-DEL(3)",16,"LOGITOOLS SOFTARE")
									u_sqlexec([ROLLBACK])					
								endif	
						ELSE
							MESSAGEBOX("DESCULPE, MAS OCORREU UM ERRO A ELIMINAR A PROMO��O! POR FAVOR CONTACTE O SUPORTE : C�D: PROMO-DEL(2)",16,"LOGITOOLS SOFTARE")
							u_sqlexec([ROLLBACK])					
						endif				
					ELSE
						MESSAGEBOX("DESCULPE, MAS OCORREU UM ERRO A ELIMINAR A PROMO��O! POR FAVOR CONTACTE O SUPORTE : C�D: PROMO-DEL(1)",16,"LOGITOOLS SOFTARE")
						u_sqlexec([ROLLBACK])
					EndiF
				
				&& Limpa os cursores
					uf_chamapromo('')
				
			ENDIF
		Else
			return
		ENDIF		
	endif	
ENDFUNC



*****************************************************************
* 						FUN��O ALTERAR PROMO					*
*****************************************************************
FUNCTION uf_alteraPromocao
	***************************
	*Valida acesso - ALTERAR *
	***************************
	IF !(uf_gerais_validaPermUser(m.ch_userno,m.ch_grupo,'Gest�o de Promo�oes - Alterar'))
		Messagebox("O SEU PERFIL N�O PERMITE ALTERAR PROMO��ES.",48,"LOGITOOLS SOFTWARE")
		return
	ENDIF
	
	SELECT ucrsPromoC
	IF Empty(ucrsPromoC.cstamp)
		MESSAGEBOX("DESCULPE, MAS TEM DE TER UM REGISTO SELECCIONADO PARA PODER EDITAR!",64,"LOGITOOLS SOFTWARE")
	else
		promocoes.Caption="Gest�o de Promo��es - Modo de Altera��o"
		DO uf_confPromocoes WITH 1
	Endif
ENDFUNC



*****************************************************************
* 						FUN��O PESQUISAR PROMO					*
*****************************************************************
FUNCTION uf_pesquisarPromocao
	***************************
	*Valida acesso - VISUALIZAR *
	***************************
	IF !(uf_gerais_validaPermUser(m.ch_userno,m.ch_grupo,'Gest�o de Promo�oes - Visualizar'))
		Messagebox("O SEU PERFIL N�O PERMITE VISUALIZAR PROMO��ES.",48,"LOGITOOLS SOFTWARE")
		return
	ENDIF
	
	DO FORM pesquisarpromo
ENDFUNC


*****************************************************************
* 						CLASSE PROMOCOES						*
*****************************************************************

DEFINE CLASS MyClass as Custom

*************
* BindEvent	*
*************
	PROCEDURE ActivaEventosPromo
		FOR i=1 to promocoes.pageframe1.page2.ctnDestino.grdDestino.columncount
	     	DO case  		
	     		CASE UPPER(promocoes.pageframe1.page2.ctnDestino.grdDestino.columns(i).controlsource)=="UCRSPROMOD.QT"
	     			BindEvent(promocoes.pageframe1.page2.ctnDestino.grdDestino.columns(i).text1,"LostFocus",promocoes.oCust,"QT")
	     			
	      		CASE UPPER(promocoes.pageframe1.page2.ctnDestino.grdDestino.columns(i).controlsource)=="UCRSPROMOD.PERCT"
	     			BindEvent(promocoes.pageframe1.page2.ctnDestino.grdDestino.columns(i).text1,"LostFocus",promocoes.oCust,"PERCT")
	     			
	     		CASE UPPER(promocoes.pageframe1.page2.ctnDestino.grdDestino.columns(i).controlsource)=="UCRSPROMOD.VALOR" 		
	     			BindEvent(promocoes.pageframe1.page2.ctnDestino.grdDestino.columns(i).text1,"LostFocus",promocoes.oCust,"VALOR")
	     		OTHERWISE 
	     			***
	     	endcase    	    	
		ENDFOR
	ENDPROC 
	
	
	PROCEDURE QT
		LOCAL lcTemp,lcQt
		
		SELECT uCrsPromoD
		lcTemp  = uCrsPromoD.ref
		lcQt	= uCrsPromoD.qt
		
		REPLACE uCrsPromoD.Perct	WITH	0	FOR uCrsPromoD.ref = lcTemp	IN	uCrsPromoD
		REPLACE uCrsPromoD.Valor	WITH	0	FOR uCrsPromoD.ref = lcTemp	IN	uCrsPromoD
		
		IF lcQt < 0 
			Replace uCrsPromoD.qt	WITH	(uCrsPromoD.qt *(-1)) FOR uCrsPromoD.ref = lcTemp IN uCrsPromoD
		ENDIF
		
		promocoes.pageframe1.page2.ctnDestino.grdDestino.refresh
		DO DesactivaEventosPromo
	ENDPROC

	
	PROCEDURE PERCT
		LOCAL lcTemp,lcPerct
		
		SELECT uCrsPromoD
		lcTemp  = uCrsPromoD.ref
		lcPerct = uCrsPromoD.perct
		
		REPLACE uCrsPromoD.Qt		WITH	0	FOR uCrsPromoD.ref = lcTemp	IN	uCrsPromoD
		REPLACE uCrsPromoD.Valor	WITH	0	FOR uCrsPromoD.ref = lcTemp	IN	uCrsPromoD
		
		IF lcPerct < 0 
			Replace uCrsPromoD.perct	WITH	(uCrsPromoD.perct *(-1)) FOR uCrsPromoD.ref = lcTemp IN uCrsPromoD
		ENDIF
		
		IF lcPerct > 100
			Replace uCrsPromoD.perct 	WITH	100 FOR uCrsPromoD.ref = lcTemp IN uCrsPromoD
		ENDIF
		
		promocoes.pageframe1.page2.ctnDestino.grdDestino.refresh
		DO DesactivaEventosPromo
	ENDPROC
	
	PROCEDURE VALOR
		LOCAL lcTemp, lcValor
		
		SELECT uCrsPromoD
		lcTemp  = uCrsPromoD.ref
		lcValor = uCrsPromoD.valor
		
		REPLACE uCrsPromoD.Qt		WITH	0	FOR uCrsPromoD.ref = lcTemp	IN	uCrsPromoD
		REPLACE uCrsPromoD.Perct	WITH	0	FOR uCrsPromoD.ref = lcTemp	IN	uCrsPromoD
		
		IF lcValor < 0 
			Replace uCrsPromoD.valor 	WITH	(uCrsPromoD.valor*(-1)) FOR uCrsPromoD.ref = lcTemp IN uCrsPromoD
		ENDIF
		
		promocoes.pageframe1.page2.ctnDestino.grdDestino.refresh
		DO DesactivaEventosPromo
	ENDPROC

	
	Procedure uf_navegast1
		If !Empty(uCrsPromoO.REF)
			* Verifica Existencia da Referencia
			TEXT TO SQL_EXISTEREF TEXTMERGE NOSHOW
				Select * FROM ST (nolock) WHERE REF = '<<Alltrim(uCrsPromoO.REF)>>'
			ENDTEXT
			If u_sqlexec(SQL_EXISTEREF,"tempSQL") And Reccount("tempSQL")>0
				NAVEGA("ST",tempSQL.STSTAMP)
				fecha("tempSQL")
			Endif
		Endif
	ENDPROC
	
	Procedure uf_navegast2
		If !Empty(uCrsPromoD.REF)
			* Verifica Existencia da Referencia
			TEXT TO SQL_EXISTEREF TEXTMERGE NOSHOW
				Select * FROM ST (nolock) WHERE REF = '<<Alltrim(uCrsPromoD.REF)>>'
			ENDTEXT
			If u_sqlexec(SQL_EXISTEREF,"tempSQL") And Reccount("tempSQL")>0
				NAVEGA("ST",tempSQL.STSTAMP)
				fecha("tempSQL")
			Endif
		Endif
	Endproc
	
enddefine