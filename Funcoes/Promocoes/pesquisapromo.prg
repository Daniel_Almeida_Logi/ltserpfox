FUNCTION uf_pesquisapromo_Chama
	
	TEXT TO lcSQL TEXTMERGE noshow
		SET fmtonly on
		exec up_promocoes_pesquisa 0,'','','','',0,''
		SET fmtonly off
	ENDTEXT 	
	IF !(uf_gerais_actGrelha("","uCrsPesqPromo",lcSQL))
		uf_perguntalt_chama("OCORREU UM ERRO NA PESQUISA DE CAMPANHAS.","OK","",16)
		RETURN .f.
	ENDIF
	
	IF TYPE("pesquisapromo")=="U"
		DO FORM pesquisapromo
	ELSE
		pesquisapromo.show
	ENDIF
	
	&&menu
	IF TYPE("pesquisapromo.menu1.actualizar") == "U"
		pesquisapromo.menu1.adicionaOpcao("actualizar", "Actualizar", "actualizar_w.png", "uf_pesquisapromo_pesquisar","A")
		pesquisapromo.menu1.adicionaOpcao("tecladoVirtual","Teclado","teclado_w.png","uf_pesquisapromo_tecladoVirtual","T")
		pesquisapromo.menu1.adicionaOpcao("subir","Subir","ponta_seta_up.png","uf_gerais_MovePage with .t., 'pesquisapromo.grdpesq','pesquisapromo'","05")
		pesquisapromo.menu1.adicionaOpcao("descer","Descer","ponta_seta_down.png","uf_gerais_MovePage with .f., 'pesquisapromo.grdpesq','pesquisapromo'","24")
	ENDIF
	
	pesquisapromo.ref.setfocus
ENDFUNC

FUNCTION uf_pesquisapromo_tecladoVirtual
	pesquisapromo.tecladoVirtual1.show("pesquisapromo.grdPesq", 161, "pesquisapromo.shape1", 161)
ENDFUNC

*********************************
*	 Fun��o que Pesquisa	 	*
*********************************
FUNCTION uf_pesquisapromo_pesquisar
	LOCAL lcSQL,lcTop,lcRef,lcDesign,lcDe,lcA, lcEmitido
	STORE '' TO lcSQL
	STORE 0 TO lcEmitido
	
	lcTop 		= INT(VAL(pesquisapromo.topo.value))
	lcRef 		= ALLTRIM(pesquisapromo.ref.value)
	lcDesign 	= ALLTRIM(pesquisapromo.design.value)
	lcDe		= IIF(EMPTY(ALLTRIM(pesquisapromo.de.value)), '19000101', uf_gerais_getdate(pesquisapromo.de.value,"SQL")) 
	lcA			= IIF(EMPTY(ALLTRIM(pesquisapromo.a.value)), '30000101', uf_gerais_getdate(pesquisapromo.a.value,"SQL"))
	lcEmitido   = IIF(pesquisapromo.chkEmitidas.tag=="true",1,0)
	lcLoja		= ALLTRIM(pesquisapromo.cmbLoja.value)
	
	TEXT TO lcSQL TEXTMERGE noshow
		exec up_promocoes_pesquisa <<lcTop>> ,'<<lcRef>>','<<lcDesign>>','<<lcDe>>','<<lcA>>',<<lcEmitido>>, '<<lcLoja>>'
	ENDTEXT 	
*msg(lcSQL)	
	IF !(uf_gerais_actGrelha("pesquisapromo.grdpesq","uCrsPesqPromo",lcSQL))
		uf_perguntalt_chama("OCORREU UM ERRO NA PESQUISA DE CAMPANHAS.","OK","",16)
		RETURN .f.
	ENDIF
	***

	pesquisapromo.lblRegistos.caption = astr(RECCOUNT("uCrsPesqPromo")) + ' Registos'
ENDFUNC 


*************************************
*		Fechar Ecra de Pesquisa		*
*************************************
FUNCTION uf_pesquisapromo_sair
	pesquisapromo.hide
	pesquisapromo.release
ENDFUNC