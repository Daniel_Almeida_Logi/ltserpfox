&& Inclui PRG's secundarios
DO pesqArtigos
DO interaccoes

** Painel de Dicionario Farmacia
FUNCTION uf_dicionario_chama
	LPARAMETERS nRefFprod, nOrigemClick
	
	PUBLIC myOrigemRefPDic
	
	IF Empty(nRefFprod)
		myOrigemRefPDic = ""
	ELSE
		myOrigemRefPDic = nRefFprod
	ENDIF
	
	LOCAL lcSQL
	lcSql = ''
	
	regua(0,11,"A carregar painel...")
	regua(1,1,"A carregar painel...")
	
	&& Controla abertura do painel
	IF TYPE("DICIONARIO")=="U"
		regua(1,2,"A carregar painel...")
		&& Cursor FPROD
		TEXT TO lcSql NOSHOW textmerge
			exec up_dicionario '<<Alltrim(myOrigemRefPDic)>>', '<<mysite>>'
		ENDTEXT
		IF !uf_gerais_actgrelha("", "fprod", lcSql)
			regua(2)
			RETURN .f.
		ENDIF
		
		regua(1,3,"A carregar painel...")
		&& Cursor substancias
		TEXT TO lcSql NOSHOW textmerge
			exec up_dicionario_substancias ''
		ENDTEXT
		IF !uf_gerais_actgrelha("", "uCrsGridFprodSubs", lcSql)
			regua(2)
			RETURN .f.
		ENDIF
		
		regua(1,4,"A carregar painel...")	
		&& Cursor pre�os
		TEXT TO lcSql NOSHOW textmerge
			exec up_dicionario_precos ''
		ENDTEXT	
		IF !uf_gerais_actgrelha("", "uCrsGridFprodPrecos", lcSql)
			regua(2)
			RETURN .f.
		ENDIF

		**regua(1,5,"A carregar painel...")
*!*			&& Cursor DCI
*!*			TEXT TO lcSql NOSHOW textmerge
*!*				exec up_dicionario_dci ''
*!*			ENDTEXT	
*!*			IF !uf_gerais_actgrelha("", "uCrsGridCI", lcSql)
*!*				regua(2)
*!*				RETURN .f.
*!*			ENDIF

		regua(1,6,"A carregar painel...")
		&& Cursor Interacoes Med
		TEXT TO lcSql NOSHOW textmerge
			exec up_dicionario_InteracoesMedicamentos '<<nRefFprod>>'
		ENDTEXT			
		IF !uf_gerais_actgrelha("", "uCrsInteracoesMed", lcSql)
			regua(2)
			RETURN .f.
		ENDIF
			
		&& Cursor Interacoes Aliment
		TEXT TO lcSql NOSHOW textmerge
			exec up_dicionario_InteracoesAlimentos '<<nRefFprod>>'
		ENDTEXT			
		IF !uf_gerais_actgrelha("", "uCrsInteracoesAlim", lcSql)
			regua(2)
			RETURN .f.
		ENDIF
		
		regua(1,7,"A carregar painel...")
		&& Cursor ALERGIAS
		TEXT TO lcSql NOSHOW textmerge
			exec up_dicionario_alertas ''
		ENDTEXT		
		IF !uf_gerais_actgrelha("", "uCrsGridAlertas", lcSql)
			regua(2)
			RETURN .f.
		ENDIF
		
		regua(1,8,"A carregar painel...")
		&& Cursor ATC
		TEXT TO lcSql NOSHOW textmerge
			exec up_dicionario_classATC ''
		ENDTEXT			
		IF !uf_gerais_actgrelha("", "uCrsClassAtc", lcSql)
			regua(2)
			RETURN .f.
		ENDIF
		
		regua(1,9,"A carregar painel...")
		&& Cursor CFT
		TEXT TO lcSql NOSHOW textmerge
			exec up_dicionario_classcft ''
		ENDTEXT			
		If !uf_gerais_actgrelha("", "uCrsClassCft", lcSql)	
			regua(2)
			RETURN .f.
		ENDIF
		
		regua(1,10,"A carregar painel...")

		** abrir painel
		DO FORM DICIONARIO
	ELSE
		regua(1,10,"A carregar painel...")
		DICIONARIO.SHOW
	ENDIF
	
	regua(1,11,"A carregar painel...")
	uf_dicionario_carregaDados()

	dicionario.refresh
	
	WITH dicionario.pageframe1
		.page1.refresh
		.page2.refresh
		.page3.refresh
		.page4.refresh
		.page5.refresh
		.page6.refresh
		.page7.refresh
	ENDWITH
	
	DO CASE
	    CASE uf_gerais_compStr("DADOSPRODUTO", nOrigemClick)
	    	uf_gerais_navegaPgframe("DICIONARIO","lbl6")
	    OTHERWISE
	        * C�digo a ser executado se nenhuma condi��o for verdadeira
	ENDCASE

	
	regua(2)
ENDFUNC



**
FUNCTION uf_dicionario_carregaMenu
	dicionario.menu1.adicionaopcao("pesquisar", "Pesquisar", myPath + "\imagens\icons\lupa_w.png", "uf_dicionario_pesquisar","P")
	dicionario.menu1.adicionaopcao("criarST", "Criar Ficha", myPath + "\imagens\icons\stocks_mais_w.png", "uf_dicionario_criarSt","C")
	dicionario.menu1.adicionaopcao("mostraBulaFI", "Folheto Inf.", myPath + "\imagens\icons\detalhe_micro.png", "uf_dicionario_mostraBula with 'fi'","C")
	dicionario.menu1.adicionaopcao("mostraBulaRCM", "Folheto RCM", myPath + "\imagens\icons\detalhe_micro.png", "uf_dicionario_mostraBula with 'rcm'","C")
ENDFUNC

FUNCTION uf_dicionario_mostraBula
	LPARAMETERS lcDocType
	
	*!*	local lcUrl
	if(!USED("fprod"))
		uf_perguntalt_chama("Producto inexistente na base de dados do infarmed!","OK","",48)
		RETURN .f.
	ENDIF

	
	LOCAL lcUrl , lcCacheFolder, lcMedGuid, lcDescritivo
	lcDescritivo = '';
	
	
	SELECT fprod
	GO TOP
	lcMedGuid= ALLTRIM(fprod.med_guid)
	
		
	
	if(EMPTY(lcMedGuid))
		uf_perguntalt_chama("Producto inexistente na base de dados do infarmed!","OK","",48)
		RETURN .f.
	ENDIF

	
	if(lcDocType=="fi")	
		DO CASE
		CASE LOWER(ALLTRIM(lcDocType))== "fi"
		  	lcDescritivo ='folheto informativo (FI)'
		CASE LOWER(ALLTRIM(lcDocType)) == "rcm"
		    lcDescritivo ='resumo das caracter�sticas medicamento (RCM)'
		OTHERWISE
		  	lcDescritivo =''
		ENDCASE
	ENDIF
	
		
	
	
	
	lcUrl = "https://extranet.infarmed.pt/INFOMED-fo/download-ficheiro.xhtml?med_guid="+ lcMedGuid +"&tipo_doc=" + ALLTRIM(lcDocType)
	

	
	lcCacheFolder= ALLTRIM(uf_gerais_getParameter('ADM0000000015','TEXT')) + "\Cache\PDF\" + ALLTRIM(lcDocType) + "_prod_" + lcMedGuid + ".pdf"
	


	LOCAL lcResult
	lcResult = uf_servicos_download(lcUrl ,lcCacheFolder)
	
	if(EMPTY(lcResult))
		uf_perguntalt_chama("N�o foi possivel descarregar o " + ALLTRIM(lcDescritivo) ,"OK","",48)
		RETURN .f.
	ENDIF
	
	
	if(lcResult == 0)
		uf_perguntalt_chama("N�o foi possivel descarregar o " + ALLTRIM(lcDescritivo),"OK","",48)
		RETURN .f.
	ENDIF
	
	
	if(lcResult == 1)
		
		if(FILE(lcCacheFolder))
			
			DECLARE INTEGER ShellExecute IN shell32.dll ; 
		  	INTEGER hndWin, ; 
		  	STRING cAction, ; 
		  	STRING cFileName, ; 
		  	STRING cParams, ;  
		  	STRING cDir, ; 
			INTEGER nShowWin

			ShellExecute(0,"open",lcCacheFolder,"","",1)
		ELSE
			uf_perguntalt_chama("N�o foi abrir o " + ALLTRIM(lcDescritivo),"OK","",48)
		ENDIF
		
	ENDIF
	
	RETURN .t.
ENDFUNC



**
FUNCTION uf_dicionario_carregaDados
	&& Cursor FPROD
	TEXT TO lcSql NOSHOW textmerge
		exec up_dicionario '<<Alltrim(myOrigemRefPDic)>>','<<mysite>>'
	ENDTEXT
	IF !uf_gerais_actgrelha("", "fprod", lcSql)
		RETURN .f.
	ENDIF
	
		

	SELECT fprod

	&& Cursor substancias
	IF !uf_gerais_actgrelha("dicionario.pageframe1.page1.grid1", "uCrsGridFprodSubs",  [exec up_dicionario_substancias'] + Alltrim(fprod.cnp) + ['])
		RETURN .f.
	ENDIF

	&& Cursor pre�os
	IF !uf_gerais_actgrelha("dicionario.pageframe1.page1.grid2", "uCrsGridFprodPrecos", [exec up_dicionario_precos'] + Alltrim(fprod.cnp) + ['])
		RETURN .f.
	ENDIF

*!*		&& Cursor DCI
*!*		IF !uf_gerais_actgrelha("dicionario.pageframe1.page3.grid1", "uCrsGridCI", [exec up_dicionario_dci '] + Alltrim(fprod.cnp) + ['])
*!*			RETURN .f.
*!*		ENDIF
	
	TEXT TO lcSql NOSHOW textmerge
		exec up_dicionario_InteracoesMedicamentos '<<Alltrim(fprod.cnp)>>'
	ENDTEXT			
	IF !uf_gerais_actgrelha("dicionario.pageframe1.page4.gridMed", "uCrsInteracoesMed", lcSql)
		regua(2)
		RETURN .f.
	ENDIF
	
	&& Cursor Interacoes Aliment
	TEXT TO lcSql NOSHOW textmerge
		exec up_dicionario_InteracoesAlimentos  '<<Alltrim(fprod.cnp)>>'
	ENDTEXT			
	IF !uf_gerais_actgrelha("dicionario.pageframe1.page4.gridAlim", "uCrsInteracoesAlim", lcSql)
		regua(2)
		RETURN .f.
	ENDIF

	&& Cursor ALERGIAS
	IF !uf_gerais_actgrelha("dicionario.pageframe1.page5.grid1", "uCrsGridAlertas", [exec up_dicionario_alertas ']+Alltrim(fprod.cnp)+['])
		RETURN .f.
	ENDIF

	&& Cursor ATC
	IF !uf_gerais_actgrelha("dicionario.pageframe1.page6.grid1", "uCrsClassAtc", [exec up_dicionario_classATC ']+Alltrim(fprod.cnp)+['])
		RETURN .f.
	ENDIF

	&& Cursor CFT
	If !uf_gerais_actgrelha("dicionario.pageframe1.page7.grid1", "uCrsClassCft", [exec up_dicionario_classcft ']+Alltrim(fprod.cnp)+['])	
		RETURN .f.
	ENDIF
ENDFUNC


**
FUNCTION uf_dicionario_pesquisar
	uf_pesqartigos_chama()
ENDFUNC


**
Function uf_dicionario_navegaSt
	LOCAL lcRef
	
	&& Valida Perfil de Pesquisar
	IF !(uf_gerais_validaPermUser(ch_userno, ch_grupo, 'Gest�o de Stock - Visualizar'))
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE CONSULTAR ARTIGOS.","OK","",48)
		RETURN .f.
	ENDIF
	
	IF uf_gerais_actgrelha("", "uCrsFpRef", [SELECT ref, ststamp FROM st (nolock) WHERE ref=']+Alltrim(fprod.ref)+['])
		IF RECCOUNT([uCrsFpRef])== 0
			uf_perguntalt_chama("A REFER�NCIA SELECCIONADA N�O EXISTE NA TABELA DE PRODUTOS.","OK","",64)
		ELSE
			&& VERIFICA SE ESTAVA ALGUM REGISTO EM EDI��O / INSER��O
			IF !(TYPE("stocks")=="U")
				IF myStocksAlteracao OR myStocksIntroducao
					uf_perguntalt_chama("O ECR� DE STOCKS ENCONTRA-SE EM MODO DE ALTERA��O / INTRODU��O! TERMINE ESTA OPERA��O E TENTE NOVAMENTE.","OK","", 64)
					RETURN
				ENDIF
			ENDIF
			
			uf_stocks_chama(ALLTRIM(uCrsFpref.ref))
		ENDIF

		FECHA("uCrsFpRef")
	ENDIF
ENDFUNC


FUNCTION uf_dicionario_guarda_imagem_anexos
	LPARAMETERS lcRef, lcSite	
	

	IF EMPTY(lcSite)
		lcSite= mysite
	ENDIF 
	
	
	if(EMPTY(lcRef))
		lcRef=''
	ENDIF
	
	

	TEXT TO lcSQL NOSHOW TEXTMERGE
		exec up_dicionario_guarda_imagem '<<Alltrim(lcRef)>>', '<<Alltrim(lcSite)>>'	
	ENDTEXT 

	**IF !(uf_startup_sqlExec(lcSQL,""))
    IF !uf_gerais_actGrelha("","",lcSQL)
		uf_perguntalt_chama("N�O FOI POSS�VEL GUARDAR A IMAGEM DO DICION�RIO!","OK","",16)
		RETURN .f.
	ENDIF	
	
  
	
	
	RETURN .t.

ENDFUNC


FUNCTION uf_dicionario_guarda_imagem_anexos_novoArtigo
	LPARAMETERS lcRef, lcSite	
	

	IF EMPTY(lcSite)
		lcSite= mysite
	ENDIF 
	
	
	if(EMPTY(lcRef))
		RETURN .F.
	ENDIF
	
    uv_path = uf_gerais_getUmValor("b_parameters_site","ltrim(rtrim(isnull(textValue,'')))", "stamp = 'ADM0000000106' and site = '" + ALLTRIM(lcSite) + "'")

    IF EMPTY(uv_path)
        RETURN .F.
    ENDIF

    uv_path = uv_path + '\st\'

    ADIR(aFiles ,UV_PATH + "*")
    CREATE CURSOR uc_files (FileName C(20), FileSize I, FileDate D, FileTime C(8), FileAttr C(5))
    INSERT INTO uc_files FROM ARRAY aFiles 

    SELECT uc_files
    go top

    LOCATE FOR UPPER(RIGHT(ALLTRIM(uc_files.fileName),4)) == '.PNG' AND len(ALLTRIM(uc_files.fileName)) == 11 AND LIKE("*" + ALLTRIM(lcRef) + "*", uc_files.fileName)

    uv_refFinal = ''

    IF FOUND()
        uv_refFinal = LEFT(ALLTRIM(uc_files.fileName),7)
    ENDIF


    TEXT TO lcSQL TEXTMERGE NOSHOW
    
        DECLARE @ref	varchar(25) = '<<ALLTRIM(lcRef)>>'
        DECLARE @site   varchar(20) = '<<ALLTRIM(lcSite)>>'
        Declare @siteNo int = 0
        declare @refFinal varchar(18) = '<<ALLTRIM(uv_refFinal)>>'

       	select 
            @siteNo=ltrim(rtrim(no)) 
        from 
            empresa(nolock)
        where 
            site = @site

        if   not exists (select * from anexos(nolock) inner join st(nolock) on st.ststamp = anexos.regstamp where   anexos.tipo='DIC' and st.site_nr=@siteNo and st.ref=@ref)
            
            begin
                
                    
            if exists (select *from st(nolock) where ref=@ref and site_nr=@siteNo) and isnull(@refFinal,'') !=''
                    
            begin
                        
                INSERT INTO [dbo].[anexos]
                        ([anexosstamp]
                        ,[regstamp]
                        ,[tabela]
                        ,[filename]
                        ,[descr]
                        ,[keyword]
                        ,[protected]
                        ,[ousrdata]
                        ,[ousrinis]
                        ,[usrdata]
                        ,[usrinis]
                        ,[tipo]
                        ,[validade]
                        ,[readOnly])
                    VALUES
                        (LEFT(newid(),30)
                        ,(select ltrim(rtrim(ststamp)) from st(nolock) where ref=@ref and site_nr=@siteNo )
                        ,'st'
                        ,@ref + '.png'
                        ,'Ref: ' + @ref
                        ,@ref
                        ,0
                        ,getdate()
                        ,'ADM'
                        ,getdate()
                        ,'ADM'
                        ,'DIC'
                        ,'1900-01-01'
                        ,1
                        )


            end
        end

    ENDTEXT


	**IF !(uf_startup_sqlExec(lcSQL,""))
    IF !uf_gerais_actGrelha("","",lcSQL)
		uf_perguntalt_chama("N�O FOI POSS�VEL GUARDAR A IMAGEM DO DICION�RIO!","OK","",16)
		RETURN .f.
	ENDIF	
	
	RETURN .t.

ENDFUNC


**
Function uf_dicionario_criarSt
	Local lcValidaSt, lcSql
	Store .f. to lcValidaSt
	STORE '' TO lcSql

	SET POINT TO "."

	&& VERIFICA SE ESTAVA ALGUM REGISTO EM EDI��O / INSER��O
	IF !(TYPE("stocks")=="U")
		IF myStocksAlteracao OR myStocksIntroducao
			uf_perguntalt_chama("O ECR� DE STOCKS ENCONTRA-SE EM MODO DE ALTERA��O / INTRODU��O! TERMINE ESTA OPERA��O E TENTE NOVAMENTE.","OK","",64)
			RETURN								
		Endif
	ENDIF
	
		** Valida��es
	
	&&verifica se pode criar produto com esta referencia
	SELECT fprod
	IF uf_stocks_temReferenciaBloqueada(.t., fprod.ref)
		uf_perguntalt_chama("N�O � POSS�VEL CRIAR O PRODUTO COM ESTA REFER�NCIA","OK","",48)
		RETURN .f.
	ENDIF

    IF !uf_gerais_checkNewRef(ALLTRIM(fprod.ref))
        RETURN .F.
    ENDIF
	

	&& Valida se o utilizador tem permiss�o para criar artigos
	IF !(uf_gerais_validaPermUser(ch_userno, ch_grupo, 'Gest�o de Stock - Introduzir'))
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE INTRODUZIR ARTIGOS.","OK","", 48)
		return
	ENDIF

	SELECT fprod
	TEXT TO lcSQL NOSHOW TEXTMERGE
		SELECT
			ref, ststamp 
		FROM
			st (nolock) 
		WHERE 
			ref = '<<ALLTRIM(fprod.ref)>>' AND site_nr = <<mysite_nr>>
	ENDTEXT
	
	** Informa��o do fornecedor **
	DIMENSION lcFornData(3)
	uf_stocks_retornaDadosFornecedorPreferencial(@lcFornData)

	IF !uf_gerais_actgrelha("", "uCrsFindRef", lcSQL  )
		RETURN .f.
	ELSE
		SELECT uCrsFindRef
		If RECCOUNT() > 0
			uf_perguntalt_chama("Este produto j� tem ficha criada. Por favor verifique.","OK","", 64)
			fecha("uCrsFindRef")
			RETURN .f.
		ENDIF
		fecha("uCrsFindRef")
	ENDIF

	&& CRIAR FICHA DO PRODUTO
	&& change 20200918 JG : inclusao dados do hmr para campos default existentes e mais um para segmentos
	&&--,depstamp
	&&		--,secstamp
	&&		--,catstamp
	
	TEXT TO lcSql NOSHOW TEXTMERGE
		SELECT
			fprod.ref
			,fprod.design
			,fprod.u_tabiva
			,fprod.u_marca
			,fprod.u_familia
			,nome = case when stfami.nome is null then ' ' else stfami.nome end
			,fprod.titaimdescr			
			,famstamp
			,sfstamp 
			,convert(varchar(4),id_grande_mercado_hmr) as depstamp
			,convert(varchar(4),id_mercado_hmr) as secstamp
			,convert(varchar(4),id_categoria_hmr) as catstamp
			,convert(varchar(4),id_segmento_hmr) as segstamp
			,convert(varchar(100),LTRIM(RTRIM(ISNULL(fprod.designOnline,'')))) as designOnline
			,convert(varchar(4000),LTRIM(RTRIM(ISNULL(fprod.caractOnline,'')))) as caractOnline
			,convert(varchar(4000),LTRIM(RTRIM(ISNULL(fprod.apresentOnline,'')))) as apresentOnline
		from
			fprod (nolock)
			left join stfami (nolock) on stfami.ref=fprod.u_familia
		where
			fprod.ref='<<ALLTRIM(fprod.ref)>>'
	ENDTEXT
	
	IF !uf_gerais_actgrelha("", "uCrsFprod", lcSql)
		RETURN .f.
	ELSE
		SELECT uCrsFprod
		IF !RECCOUNT()>0
			uf_perguntalt_chama("A REFER�NCIA N�O EXISTE NO DICION�RIO DE PRODUTOS.", "OK","",64)
			fecha("uCrsFprod")
			RETURN .f.
		ENDIF
			
		** dados �tico **
		LOCAL lcPreco, lcImpEtiq, lcStamp
		STORE 0 TO lcPreco
		STORE .f. TO lcimpEtiq
		lcStamp = uf_gerais_stamp()
		STORE '' TO lcSql
		
		SELECT fprod
		IF uf_gerais_actgrelha("", "uCrsFpreco", [select cnp, epreco, grupo from fpreco (nolock) where cnp=']+ ALLTRIM(fprod.ref) + [' and grupo='pvp'])
			If reccount("uCrsFpreco")>0
				lcPreco = uCrsFpreco.epreco
				lcImpEtiq = .t.		
			EndIf
			fecha("uCrsFpreco")
		ENDIF
		*****************
		
		** funcionalidade que permite inserir produto em v�rias Lojas simultaneamente		
		** Verifica n� de Lojas
		SELECT uCrsLojas
		IF RECCOUNT("uCrsLojas") != 1
			IF uf_perguntalt_chama("Deseja criar a ficha do produto em todas as Lojas?","Sim","N�o")
				SELECT uCrsLojas
				GO TOP 
				SCAN FOR !empty(uCrsLojas.no)
					
					uf_dicionario_InsereArtigo(uCrsLojas.no, uCrsLojas.site , @lcFornData)
								
					uf_stocks_controlaPCTzero(.t., uCrsLojas.no)
					uf_stocks_preencheContas(.t., uCrsLojas.no)

				ENDSCAN 

				uf_stocks_chama(ALLTRIM(uCrsfprod.ref))
				
			ELSE
			
				LOCAL lcCodigoMotivo, lcMotivo
				STORE '' TO lcCodigoMotivo, lcMotivo				
				
				SELECT uCrsfprod
				IF uf_dicionario_verificarTaxaIvaAZero(uCrsfprod.u_tabiva)
					lcCodigoMotivo = uf_dicionario_codigoMotivoInsencao(mysite_nr, mysite ,1)
					lcMotivo = uf_dicionario_codigoMotivoInsencao(mysite_nr, mysite,2)
				ENDIF
				
				** Inserir produto loja atual**
				TEXT TO lcSql NOSHOW TEXTMERGE
					INSERT INTO st
					(
						ststamp, ref, design,
						familia, faminome,
						tabiva, ivaincl, iva1incl, iva2incl, iva3incl, iva4incl, iva5incl,
						usr1, u_lab, u_fonte, cpoc, conversao,
						epv1, u_impetiq,
						ousrdata,ousrhora,ousrinis,
						usrdata,usrhora,usrinis,
						u_depstamp, u_secstamp, u_catstamp, u_segstamp, u_famstamp, site_nr,
						fornecedor, fornec , fornestab ,codmotiseimp,motiseimp
					)
					values
					(
						'<<ALLTRIM(lcStamp)>>', '<<ALLTRIM(uCrsfprod.ref)>>', '<<ALLTRIM(uCrsfprod.design)>>',
						'<<ALLTRIM(uCrsfprod.u_familia)>>', '<<ALLTRIM(uCrsfprod.nome)>>',
						<<uCrsfprod.u_tabiva>>, 1, 1, 1, 1, 1, 1,
						'<<ALLTRIM(uCrsfprod.u_marca)>>', '<<ALLTRIM(uCrsfprod.titaimdescr)>>', 'D', 1, 1,
						<<ROUND(lcPreco,2)>>, <<IIF(lcImpEtiq==.t.,1,0)>>,
						convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108), '<<m_chinis>>',
						convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108), '<<m_chinis>>',
						'<<ALLTRIM(uCrsfprod.depstamp)>>', '<<ALLTRIM(uCrsfprod.secstamp)>>','<<ALLTRIM(uCrsfprod.catstamp)>>','<<ALLTRIM(uCrsfprod.segstamp)>>','<<ALLTRIM(uCrsfprod.famstamp)>>', <<mysite_nr>>,
						'<<ALLTRIM(lcFornData(2))>>',<<lcFornData(1)>>,<<lcFornData(3)>>
						,'<<ALLTRIM(lcCodigoMotivo)>>', '<<ALLTRIM(lcMotivo)>>'
					)
				ENDTEXT
				IF !uf_gerais_actgrelha("", "", lcSql)
					uf_perguntalt_chama("Ocorreu uma anomalia a introduzir o produto. Por favor contacte o suporte.","OK","",16)
				ELSE
					
					&& grava o ultimo registo
					uf_gerais_gravaUltRegisto('ST', ALLTRIM(uCrsfprod.ref))
					
					uf_stocks_controlaPCTzero(.t.)
					uf_stocks_ValoresDefeito(.t.)
					uf_stocks_preencheContas(.t.)
					uf_stocks_chama(ALLTRIM(uCrsfprod.ref))
				ENDIF
				
				
				
			ENDIF
		ELSE
		
			LOCAL lcCodigoMotivo, lcMotivo
			STORE '' TO lcCodigoMotivo, lcMotivo
	
			SELECT uCrsfprod
			IF uf_dicionario_verificarTaxaIvaAZero(uCrsfprod.u_tabiva)
				lcCodigoMotivo = uf_dicionario_codigoMotivoInsencao(mysite_nr, mysite,1)
				lcMotivo = uf_dicionario_codigoMotivoInsencao(mysite_nr, mysite,2)
			ENDIF
	
			** Inserir produto loja atual**
			TEXT TO lcSql NOSHOW TEXTMERGE
				INSERT INTO st
				(
					ststamp, ref, design,
					familia, faminome,
					tabiva, ivaincl, iva1incl, iva2incl, iva3incl, iva4incl, iva5incl,
					usr1, u_lab, u_fonte, cpoc, conversao,
					epv1, u_impetiq,
					ousrdata,ousrhora,ousrinis,
					usrdata,usrhora,usrinis,
					u_depstamp, u_secstamp, u_catstamp, u_segstamp, u_famstamp, 
					site_nr,
					fornecedor, fornec, fornestab, u_tipoetiq
					,codmotiseimp ,motiseimp
				)
				values
				(
					'<<ALLTRIM(lcStamp)>>', '<<ALLTRIM(uCrsfprod.ref)>>', '<<ALLTRIM(uCrsfprod.design)>>',
					'<<ALLTRIM(uCrsfprod.u_familia)>>', '<<ALLTRIM(uCrsfprod.nome)>>',
					<<uCrsfprod.u_tabiva>>, 1, 1, 1, 1, 1, 1,
					'<<ALLTRIM(uCrsfprod.u_marca)>>', '<<ALLTRIM(uCrsfprod.titaimdescr)>>', 'D', 1, 1,
					<<ROUND(lcPreco,2)>>, <<IIF(lcImpEtiq==.t.,1,0)>>,
					convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108), '<<m_chinis>>',
					convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108), '<<m_chinis>>',
					'<<ALLTRIM(uCrsfprod.depstamp)>>', '<<ALLTRIM(uCrsfprod.secstamp)>>','<<ALLTRIM(uCrsfprod.catstamp)>>','<<ALLTRIM(uCrsfprod.segstamp)>>','<<ALLTRIM(uCrsfprod.famstamp)>>', <<mysite_nr>>,
					'<<ALLTRIM(lcFornData(2))>>',<<lcFornData(1)>>,<<lcFornData(3)>>, 1
					,'<<ALLTRIM(lcCodigoMotivo)>>', '<<ALLTRIM(lcMotivo)>>'
				)
			ENDTEXT
			
	
			
			IF !uf_gerais_actgrelha("", "", lcSql)
				uf_perguntalt_chama("Ocorreu uma anomalia a introduzir o produto. Por favor contacte o suporte.","OK","",16)
			ELSE
				
				&& grava o ultimo registo
				uf_gerais_gravaUltRegisto('ST', ALLTRIM(uCrsfprod.ref))
				
				uf_stocks_controlaPCTzero(.t.)
				uf_stocks_ValoresDefeito(.t.)
				uf_stocks_preencheContas(.t.)
				uf_stocks_chama(ALLTRIM(uCrsfprod.ref))
			ENDIF
			
			
		ENDIF
	ENDIF
	***************************

	IF USED("uCrsFprod")
		fecha("uCrsFprod")
	ENDIF
	
	SELECT fprod
ENDFUNC


**
FUNCTION uf_dicionario_AlteracaoEscalao

	If uf_perguntalt_chama("ATEN��O: Deseja alterar a comparticipa��o deste produto? "+CHR(13)+CHR(13)+"Esta altera��o ser� registada!","Sim","N�o")
		Local lcGrupo
		SELECT fprod
		lcGrupo = fprod.grupo
		
		SET HOURS TO 24
		Atime=ttoc(datetime()+(difhoraria*3600),2)
		Astr=left(Atime,8)

		*uf_gerais_actgrelha("", "uCrsActGrupo", [select descricao from cptgrp (nolock) where grupo='] + Alltrim(lcGrupo) + ['])

		*if Reccount("uCrsActGrupo") > 0

			TEXT TO lcSql NOSHOW TEXTMERGE
				UPDATE
					fprod
				set
					 grupo 			= '<<Alltrim(fprod.Grupo)>>'
					,cptgrpdescr 	= '<<Alltrim(fprod.cptgrpdescr)>>'
					,u_alterado 	= 1
					,usrinis 		= '<<m_chinis>>'
					,usrdata 		= CONVERT(varchar, dateadd(HOUR, <<difhoraria>>, getdate()), 102)
					,usrhora 		= '<<Astr>>'
				where
					ref 			= '<<Alltrim(fprod.ref)>>'
			ENDTEXT

			IF !uf_gerais_actgrelha("", "", lcSql)
				uf_perguntalt_chama("Ocorreu uma anomalia a atualizar a comparticipa��o! Por favor contacte o suporte.", "OK", "", 16)
			ENDIF

			uf_gerais_registaOcorrencia("Dicion�rio", "Altera��o do escal�o de comparticipa��o", 3, fprod.cnp, Alltrim(fprod.Grupo), fprod.fprodstamp)
		*ENDIF

		&& Actualizar o Ecra com nova selec��o
*!*			Select fprod
*!*			replace fprod.cptgrpdescr	with	Alltrim(uCrsActGrupo.descricao)

		Dicionario.pageframe1.page1.combo1.refresh
		dicionario.pageframe1.page1.txtEscalaoDescr.refresh

*!*			If Used("uCrsActGrupo")
*!*				fecha("uCrsActGrupo")
*!*			Endif
	ELSE
		uf_gerais_actgrelha("", "uCrsTempGrupo", [select grupo,cptgrpdescr from fprod (nolock) where ref=']+Alltrim(fprod.ref)+['])

		SELECT uCrsTempGrupo
		SELECT fprod
		replace fprod.cptgrpdescr WITH uCrsTempGrupo.cptgrpdescr
		replace fprod.grupo 	  WITH uCrsTempGrupo.grupo

		Dicionario.pageframe1.page1.combo1.refresh
		Dicionario.pageframe1.page1.txtEscalaoDescr.refresh
	ENDIF
ENDFUNC


**
FUNCTION uf_dicionario_chamaInteraccoes
	SELECT fprod
	IF !EMPTY(fprod.cnp)
		uf_interaccoes_chama(fprod.cnp)
	ENDIF
ENDFUNC


**
FUNCTION uf_dicionario_sair
	** Fecha Cursores
	IF USED("FPROD")
		fecha("FPROD")
	ENDIF
	
	IF USED("uCrsGridFprodSubs")
		fecha("uCrsGridFprodSubs")
	ENDIF
	
	IF USED("uCrsInteracoesMed")
		fecha("uCrsInteracoesMed")
	ENDIF
	
	IF USED("uCrsGridFprodPrecos")
		fecha("uCrsGridFprodPrecos")
	ENDIF
	
	IF USED("uCrsInteracoesAlim")
		fecha("uCrsInteracoesAlim")
	ENDIF
	
	IF USED("uCrsGridAlertas")
		fecha("uCrsGridAlertas")
	ENDIF
	
	IF USED("uCrsClassAtc")
		fecha("uCrsClassAtc")
	ENDIF
	
	IF USED("uCrsClassCFT")
		fecha("uCrsClassCFT")
	ENDIF	
	
	IF USED("uCrsIntProdDic")
		fecha("uCrsIntProdDic")
	ENDIF
	
	**
	RELEASE myOrigemRefPDic
	
	**
	dicionario.hide
	DICIONARIO.release
ENDFUNC


*********************
* RECUPERAR FUN��ES *
*********************
FUNCTION uf_recoverApp_dicionario
	RETURN .t.
ENDFUNC


**
FUNCTION uf_dicionario_InsereArtigo
	LPARAMETERS lcSite_nr, lcSite, lcFornData
	
	** dados �tico 
	LOCAL lcPreco, lcImpEtiq, lcStamp
	STORE 0 TO lcPreco
	STORE .f. TO lcimpEtiq
	STORE '' TO lcSql
	
	** Valida Pvp
	SELECT fprod
	IF uf_gerais_actgrelha("", "uCrsFpreco", [select cnp, epreco, grupo from fpreco (nolock) where cnp=']+ ALLTRIM(fprod.ref) + [' and grupo='pvp'])
		If reccount("uCrsFpreco")>0
			lcPreco = uCrsFpreco.epreco
			lcImpEtiq = .t.		
		EndIf
		fecha("uCrsFpreco")
	ENDIF
	
	**
	IF EMPTY(lcsite_nr)
		lcsite_nr  = mysite_nr
	ENDIF 
	IF EMPTY(lcSite)
		lcSite= mysite
	ENDIF 
	
	LOCAL lcRef
	lcRef = ''
	
	LOCAL lcCodigoMotivo, lcMotivo
	STORE '' TO lcCodigoMotivo, lcMotivo

	SELECT uCrsfprod
	IF uf_dicionario_verificarTaxaIvaAZero(uCrsfprod.u_tabiva)
		lcCodigoMotivo = uf_dicionario_codigoMotivoInsencao(lcsite_nr,lcSite,1)
		lcMotivo = uf_dicionario_codigoMotivoInsencao(lcsite_nr,lcSite,2)
	ENDIF

	**	
	SELECT uCrsfprod
	lcRef = ALLTRIM(uCrsfprod.ref)
	
	lcSQL=''
	TEXT TO lcSQL TEXTMERGE noshow
		IF not exists (select ref, site_nr from st where ref = '<<ALLTRIM(uCrsfprod.ref)>>' and site_nr = <<lcsite_nr>>)
		BEGIN 
			INSERT INTO st
			(
				ststamp, ref, design,
					familia, faminome,
					tabiva, ivaincl, iva1incl, iva2incl, iva3incl, iva4incl, iva5incl,
					usr1, u_lab, u_fonte, cpoc, conversao,
					epv1, u_impetiq,
					ousrdata,ousrhora,ousrinis,
					usrdata,usrhora,usrinis,
					u_depstamp, u_secstamp, u_catstamp, u_segstamp, u_famstamp, site_nr,
					fornecedor, fornec , fornestab, u_tipoetiq,
					designOnline, caractOnline, apresentOnline ,codmotiseimp ,motiseimp
			)
			values 
			(
				LEFT(NEWID(),25),'<<ALLTRIM(uCrsfprod.ref)>>', '<<ALLTRIM(uCrsfprod.design)>>',
				'<<ALLTRIM(uCrsfprod.u_familia)>>', '<<ALLTRIM(uCrsfprod.nome)>>',
				<<uCrsfprod.u_tabiva>>, 1, 1, 1, 1, 1, 1,
				'<<ALLTRIM(uCrsfprod.u_marca)>>', '<<ALLTRIM(uCrsfprod.titaimdescr)>>', 'D', 1, 1,
				<<ROUND(lcPreco,2)>>, <<IIF(lcImpEtiq==.t.,1,0)>>,
				convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108), '<<m_chinis>>',
				convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108), '<<m_chinis>>',
				'<<ALLTRIM(uCrsfprod.depstamp)>>', '<<ALLTRIM(uCrsfprod.secstamp)>>','<<ALLTRIM(uCrsfprod.catstamp)>>','<<ALLTRIM(uCrsfprod.segstamp)>>','<<ALLTRIM(uCrsfprod.famstamp)>>', <<lcsite_nr>>,
				'<<ALLTRIM(lcFornData(2))>>',<<lcFornData(1)>>,<<lcFornData(3)>>, 1,
				'<<ALLTRIM(uCrsfprod.designOnline)>>', '<<ALLTRIM(uCrsfprod.caractOnline)>>','<<ALLTRIM(uCrsfprod.apresentOnline)>>',
				'<<ALLTRIM(lcCodigoMotivo)>>', '<<ALLTRIM(lcMotivo)>>'
			)
		END 
	ENDTEXT

	IF !uf_gerais_actgrelha("", "", lcSql)
		uf_perguntalt_chama("Ocorreu uma anomalia a introduzir o produto. Por favor contacte o suporte.","OK","",16)
	ELSE
		uf_dicionario_guarda_imagem_anexos(lcRef,lcSite)			
	ENDIF

ENDFUNC


FUNCTION uf_dicionarioCarregaPosologia

	if(!uf_gerais_deveConsultarSimposium())
		RETURN .f.
	ENDIF

	IF(uf_dic_valida_tipoNomeCliente())

		if(USED("fprod"))
			
		
			LOCAL lcPosologiaFinal,lcref 			
			SELECT fprod
			lcref = ALLTRIM(fprod.ref)
			IF !EMPTY(lcref)
				uf_gerais_meiaRegua("A carregar posologia...")
				lcPosologiaFinal = uf_dicionarioDevolvePosologiaFinal(ALLTRIM(lcref),ALLTRIM(fprod.posologia_orientativa))						
				SELECT fprod
				REPLACE fprod.posologia_orientativa WITH lcPosologiaFinal
				regua(2)
				dicionario.refresh					
			ENDIF
			
		ENDIF
					
	ENDIF
	
ENDFUNC

FUNCTION uf_dicionarioDevolvePosologiaFinal
	LPARAMETERS lcCnp, lcPosologiaOriginal

	LOCAL lcPosologiaFinal
	lcPosologiaFinal = lcPosologiaOriginal
	
	uf_dic_retornaMedicamentoPorId(ALLTRIM(lcCnp) ,1)
		
	if(USED("ucrsDrugDosage"))
		LOCAL lcRespPosologia 
		lcRespPosologia = uf_dic_simposio_getPosologia()
		
		if(!EMPTY(lcRespPosologia))
			 lcPosologiaFinal = ALLTRIM(lcRespPosologia)  					
		ENDIF
		
	ENDIF
	fecha("ucrsDrugDosage")
	RETURN lcPosologiaFinal 
	
ENDFUNC




FUNCTION uf_dicionarioCarregaInteracaoAlimentacao

	if(!uf_gerais_deveConsultarSimposium())
		RETURN .f.
	ENDIF
	

	IF(uf_dic_valida_tipoNomeCliente())
	
		if(USED("fprod"))
					
			LOCAL lcref 			
			SELECT fprod
			lcref = ALLTRIM(fprod.ref)
			IF !EMPTY(lcref)
				uf_gerais_meiaRegua("A carregar intera��es com alimentos...")
				uf_dic_retornaIteracaoAlimentoMedicamentoPorId(ALLTRIM(lcref) ,1)
				if(USED("ucrsDrugFoodIteration") and USED("uCrsInteracoesAlim"))
					
					SELECT uCrsInteracoesAlim
					GO TOP
					DELETE FROM uCrsInteracoesAlim
					
					SELECT ucrsDrugFoodIteration
					GO TOP

					INSERT INTO uCrsInteracoesAlim (cnp,tipo,efeito,explicacao);
					SELECT; 
						ALLTRIM(lcref),LEFT(NVL(ucrsDrugFoodIteration.foodType,''),12),LEFT(NVL(ucrsDrugFoodIteration.severity,''),30),NVL(ucrsDrugFoodIteration.explanation,'');
					from; 
						ucrsDrugFoodIteration; 
					order by level desc

					fecha("ucrsDrugFoodIteration")
					
					SELECT uCrsInteracoesAlim 
					GO TOP
				ENDIF
				
				regua(2)
				dicionario.refresh					
			ENDIF
			
		ENDIF
					
	ENDIF
	
ENDFUNC

FUNCTION uf_dicionarioCarregaInteracaoMedicamentos
	
	IF(!uf_gerais_deveConsultarSimposium())
		RETURN .f.
	ENDIF

	IF(uf_dic_valida_tipoNomeCliente())
	
		IF(USED("fprod"))
					
			LOCAL lcref 			
			SELECT fprod
			lcref = ALLTRIM(fprod.ref)
			IF !EMPTY(lcref)
				uf_gerais_meiaRegua("A carregar intera��es entre medicamentos...")
				
				uf_dic_retornaIteracaoOutrosMedicamentosPorId(ALLTRIM(lcref) ,1)
				
				IF(USED("ucrsDrugIterationDrugs") and USED("uCrsInteracoesMed"))
				
					SELECT uCrsInteracoesMed
					GO TOP
					DELETE FROM uCrsInteracoesMed
					
					SELECT ucrsDrugIterationDrugs
					GO TOP

					INSERT INTO uCrsInteracoesMed(DesignMedInteracao,med_class_b,grau,explicacao, conselho);
					SELECT; 
						ALLTRIM(drugId),ALLTRIM(class);
					from; 
						ucrsDrugIterationDrugs; 
					order by level desc

					fecha("ucrsDrugIterationDrugs")
					
					SELECT uCrsInteracoesMed
					GO TOP	
			
				ENDIF				
	
				regua(2)
				dicionario.refresh					
			ENDIF
			
		ENDIF
					
	ENDIF
ENDFUNC

FUNCTION uf_dicionarioFisicoAlertas

	if(!uf_gerais_deveConsultarSimposium())
		RETURN .f.
	ENDIF
	

	IF(uf_dic_valida_tipoNomeCliente())
	
		if(USED("fprod"))
					
			LOCAL lcref 			
			SELECT fprod
			lcref = ALLTRIM(fprod.ref)
			IF !EMPTY(lcref)
				uf_gerais_meiaRegua("A carregar alertas...")
				uf_dic_retornaAlertasFisicosMedicamentoPorId(ALLTRIM(lcref) ,1)
				if(USED("ucrsPhysioAlerts") and USED("uCrsGridAlertas"))
					
					SELECT uCrsGridAlertas
					GO TOP
					DELETE FROM uCrsGridAlertas
					
					SELECT ucrsPhysioAlerts
					GO TOP

					INSERT INTO uCrsGridAlertas(cnp,tipo,via_admin,descr);
					SELECT; 
						ALLTRIM(lcref),LEFT(NVL(ucrsPhysioAlerts.type,''),20),LEFT(NVL(ucrsPhysioAlerts.route,''),50),NVL(ucrsPhysioAlerts.description,'');
					from; 
						ucrsPhysioAlerts; 
					order by level desc

					fecha("ucrsPhysioAlerts")
					
					SELECT uCrsGridAlertas
					GO TOP
				ENDIF
				
				regua(2)
				dicionario.refresh					
			ENDIF
			
		ENDIF
					
	ENDIF
	
ENDFUNC

FUNCTION uf_dicionario_codigoMotivoInsencao
	LPARAMETERS lcsitenr, lcSite, codigoOuMotivo

	fecha("ucrsCodMotivoInsencao")
	LOCAL lcSQLCodMotivo
	lcSQLCodMotivo = ""
	TEXT TO lcSQLCodMotivo TEXTMERGE NOSHOW
		select codmotiseimp,motivo_isencao_iva from empresa(nolock) WHERE (no = <<lcsitenr>> or site='<<lcSite>>') 
	ENDTEXT

	IF !uf_gerais_actGrelha("", "ucrsCodMotivoInsencao", lcSQLCodMotivo)
		uf_perguntalt_chama("N�o foi possivel encontrar regitos do motivo de insen��o.", "", "OK", 16)
		RETURN .f.
	ENDIF
	
	IF RECCOUNT("ucrsCodMotivoInsencao") = 0
		uf_perguntalt_chama("O motivo de exen��o da empresa n�o se encontra preenchido. Por favor preencha para depois poder proceder a esta altera��o.", "", "OK", 16)
		RETURN .f.
	ENDIF
	
	IF codigoOuMotivo == 1
		RETURN ucrsCodMotivoInsencao.codmotiseimp
	ENDIF
	IF codigoOuMotivo == 2
		RETURN ucrsCodMotivoInsencao.motivo_isencao_iva
	ENDIF
	fecha("ucrsCodMotivoInsencao")

ENDFUNC

FUNCTION uf_dicionario_verificarTaxaIvaAZero
	LPARAMETERS lcTaxaIva
	
	LOCAL lcSQLTaxaIva
	lcSQLTaxaIva = ""
	TEXT TO lcSQLTaxaIva TEXTMERGE NOSHOW
		select taxa from taxasiva (nolock) where  taxasiva.codigo = <<lcTaxaIva>>
	ENDTEXT
	IF !uf_gerais_actGrelha("", "ucrsTaxaIva", lcSQLTaxaIva)
		uf_perguntalt_chama("N�o foi possivel encontrar taxa de iva.", "", "OK", 16)
		RETURN .f.
	ENDIF
	
	IF ucrsTaxaIva.taxa != 0
		RETURN .F.
	ENDIF
	
	RETURN .T.
	
ENDFUNC

