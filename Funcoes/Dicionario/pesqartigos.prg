**
FUNCTION uf_pesqartigos_chama
	
	** Cria Cursor Inical
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_Dicionario_pesquisaRef2 0, 'xxxxx', '', '', '', '', '', 0, ''
	ENDTEXT
	
	IF !uf_gerais_actgrelha("", "uCrsPesqArtigoPresc", lcSQL)
		RETURN .F.
	ENDIF
	
	&& Controla abertura do painel
	IF TYPE("PESQARTIGOS")=="U"
		DO FORM PESQARTIGOS
	ELSE
		PESQARTIGOS.SHOW
	ENDIF
ENDFUNC 


**
FUNCTION uf_pesqartigos_carregaMenu
	pesqartigos.menu1.adicionaopcao("atualizar", "Atualizar", myPath + "\imagens\icons\actualizar_w.png", "uf_pesqartigos_carregaDados","A")
	pesqartigos.menu1.adicionaopcao("naoAutorizados", "Inc. n/ Autor.", myPath + "\imagens\icons\unchecked_w.png", "uf_pesqartigos_naoAutorizados","I")
	**pesqartigos.menu1.adicionaopcao("teclado", "Teclado", myPath + "\imagens\icons\teclado_w.png", "uf_pesqartigos_tecladoVirtual","T")
ENDFUNC


** Fun��es Painel de Pesquisa de Prescri��o
FUNCTION uf_pesqartigos_carregaDados
	
	IF 	EMPTY(ALLTRIM(PESQARTIGOS.ref.value));
		AND EMPTY(ALLTRIM(PESQARTIGOS.dci.value));
		AND EMPTY(ALLTRIM(PESQARTIGOS.nome_comercial.value));
		AND EMPTY(ALLTRIM(PESQARTIGOS.embalagem.value));
		AND EMPTY(ALLTRIM(PESQARTIGOS.cnpem.value));
		AND EMPTY(ALLTRIM(PESQARTIGOS.dosagem.value))
		
		IF !uf_perguntalt_chama("N�o colocou nenhum par�metro de pesquisa. Assim a pesquisa poder� ser muito demorada." + CHR(13) + "Tem a certeza que pretende continuar?","Sim","N�o")
			RETURN .f.
		ENDIF
	ENDIF
	
	LOCAL lcIncluiNaoAutorizados
	IF EMPTY(pesqartigos.menu1.naoAutorizados.tag) OR pesqartigos.menu1.naoAutorizados.tag == "false"
		lcIncluiNaoAutorizados = 0
	ELSE
		lcIncluiNaoAutorizados = 1
	ENDIF
		
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_dicionario_pesquisaRef2 
				500,
				'<<ALLTRIM(PESQARTIGOS.ref.value)>>'
				,'<<ALLTRIM(PESQARTIGOS.dci.value)>>'
				,'<<ALLTRIM(PESQARTIGOS.nome_comercial.value)>>'
				,'<<ALLTRIM(PESQARTIGOS.embalagem.value)>>'
				,'<<ALLTRIM(PESQARTIGOS.dosagem.value)>>'
				,'<<ALLTRIM(PESQARTIGOS.forma.value)>>'
				,<<lcIncluiNaoAutorizados>>
				,'<<ALLTRIM(PESQARTIGOS.cnpem.value)>>'
	ENDTEXT

	regua(0, 1, "A pesquisar produtos...")
	
	IF !uf_gerais_actgrelha("pesqartigos.gridpesq", "uCrsPesqArtigoPresc", lcSQL)
		regua(2)
		Return
	ELSE
		PESQARTIGOS.lblregistos.caption = astr(RECCOUNT("uCrsPesqArtigoPresc")) + ' Registos'
			
		PESQARTIGOS.Refresh
	ENDIF
	
	regua(2)
ENDFUNC


**
FUNCTION uf_pesqartigos_naoAutorizados
	IF EMPTY(pesqartigos.menu1.naoAutorizados.tag) OR pesqartigos.menu1.naoAutorizados.tag == "false"
		pesqartigos.menu1.naoAutorizados.config("Inc. n/ Autor.", myPath + "\imagens\icons\checked_w.png", "uf_pesqartigos_naoAutorizados","I")
		pesqartigos.menu1.naoAutorizados.tag = "true"
	ELSE
		pesqartigos.menu1.naoAutorizados.config("Inc. n/ Autor.", myPath + "\imagens\icons\unchecked_w.png", "uf_pesqartigos_naoAutorizados","I")
		pesqartigos.menu1.naoAutorizados.tag = "false"
	ENDIF
ENDFUNC


**
FUNCTION uf_pesqArtigos_tecladoVirtual
	
	PESQARTIGOS.tecladoVirtual1.show("PESQARTIGOS.gridPesq", 161, "PESQARTIGOS.shape1", 161)
ENDFUNC


** Fechar Ecra de Pesquisa de Prescri��es
FUNCTION uf_pesqartigos_sair
	&& fecha cursores
	IF USED("uCrsPesqArtigoPresc")
		fecha("uCrsPesqArtigoPresc")
	ENDIF
	
	&&Liberta variaveis publicas
	RELEASE myIntVarIncluir
	
	PESQARTIGOS.hide
	PESQARTIGOS.release
ENDFUNC