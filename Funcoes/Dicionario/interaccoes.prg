**
FUNCTION uf_interaccoes_chama
	LPARAMETERS lcRef

	IF !uf_gerais_actGrelha("", "uCrsIntProdDic", [exec up_dicionario_interacoes']+ ALLTRIM(lcRef) + ['])
		RETURN .f.
	ENDIF
	**INTERACCOES.grid1
	
	&& Controla abertura do painel
	IF TYPE("INTERACCOES")=="U"
		DO FORM INTERACCOES
	ELSE
		INTERACCOES.SHOW
	ENDIF
	
	
ENDFUNC


** Fechar Ecra de Pesquisa de Prescrições
FUNCTION uf_interaccoes_sair
	&& fecha cursores
	IF USED("uCrsIntProdDic")
		fecha("uCrsIntProdDic")
	ENDIF
	
	INTERACCOES.hide
	INTERACCOES.release
ENDFUNC