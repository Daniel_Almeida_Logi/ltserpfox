
parameters returnArray,arrayofValues,includestruct,formattedOutput, isArray

LOCAL o
o = nfCursorToObject( m.arrayOfValues,m.includestruct )

IF m.returnArray
	DIMENSION rows(1)
	ACOPY(m.o.rows,'rows')
	Return nfJsonCreate(@m.rows,m.formattedOutput,.t.,.f.,.f.,isArray)
ELSE
	Return nfJsonCreate(m.o,m.formattedOutput,.t.,.f.,.f.,isArray)
ENDIF