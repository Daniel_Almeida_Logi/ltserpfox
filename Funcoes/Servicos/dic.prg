FUNCTION uf_dic_simposium_enviaJson
    LPARAM  lcUrl, lcJsonText, lcToken, lcMetodo, lcRequest
      
    LOCAL loReq, loHttp, lnSuccess, lnSuccess1, lcJsonText, loResp, lnSuccess 

    uf_servicos_unblockChilkat()    
    
    loReq = CreateObject('Chilkat_9_5_0.HttpRequest')
    loHttp = CreateObject('Chilkat_9_5_0.Http')
    loJson = CreateObject('Chilkat_9_5_0.JsonObject')

    loHttp.UserAgent = ""
    loHttp.AcceptLanguage = ""
    loHttp.AcceptCharset =  "utf-8, ISO-8859-1;q=0.7,*;q=0.7"
    *  Suppress the Accept-Encoding header by disallowing
    *  a gzip response:
    loHttp.AllowGzip = 0
    loHttp.ConnectTimeout  = 10
    loHttp.ReadTimeout = 20
    


    IF(!EMPTY(lcToken))
        lnSuccess = loHttp.SetRequestHeader("Authorization",lcToken)
    ENDIF
    lcJsonText = ALLTRIM(lcJsonText)
    
    if(!EMPTY(emdesenvolvimento))
  		_cliptext = lcUrl
    ENDIF
    
    lcUrl = uf_gerais_cleanURL(lcUrl)
    
    DO CASE
        Case UPPER(lcMetodo)== 'POST'
            loResp = loHttp.PostJson2(ALLTRIM(lcUrl),"application/json",lcJsonText) 
        Case UPPER(lcMetodo)== 'GET'
            loResp = loHttp.QuickGetStr(lcUrl)
        OTHERWISE
            loResp = loHttp.QuickGetStr(lcUrl)
    ENDCASE

    IF (ISNULL(loResp)) OR loHttp.LastMethodSuccess <> 1 THEN
         _cliptext = loHttp.LastErrorText
    **ERRO
    ELSE
   	
        uf_dic_simposium_processa_resposta(loResp, loJson, lcRequest)
    
    ENDIF
    
    RELEASE loReq
    RELEASE loHttp
    RELEASE loResp
        
ENDFUNC


	

PROCEDURE uf_dic_simposium_processa_resposta
    PARAMETERS loResp, loJson, lcTipo

    LOCAL lcBody, lcTamanho
    STORE "" TO lcBody, lcTamanho   

    IF(VARTYPE(loResp)=="O")
         lcBody = loResp.BodyStr
    ENDIF 
    
    IF(VARTYPE(loResp)=="C")
         lcBody = loResp
    ENDIF  
	
	IF(EMPTY(lcBody))	 
		RETURN .f.
	ENDIF
	
	lcTamanho = STREXTRACT(lcBody,"[","]")

      IF LEN(lcTamanho) = 0
      	RETURN .F.
      ENDIF
          
    IF(!EMPTY(emdesenvolvimento))		
		_cliptext = lcBody 
	ENDIF     
	   
    DO CASE
        Case UPPER(lcTipo)== 'AUTH'
            uf_dic_simposio_saveAuth(lcBody, loJson )  
        Case UPPER(lcTipo)== 'DRUG'
            uf_dic_simposio_processDrug(lcBody)
        Case UPPER(lcTipo)== 'DRUG_BY_ID'
         	uf_dic_simposio_processDrugById(lcBody) 
        Case UPPER(lcTipo)== 'DRUG_MAPPER'
         	uf_dic_simposio_processDrugCodeMapper(lcBody) 
        CASE UPPER(lcTipo) == 'PRODUCT'
        	uf_dic_simposio_processProduct(lcBody)
        CASE UPPER(lcTipo) == 'FOOD_INTERACTIONS_BY_ID'
        	uf_dic_simposio_processFoodInteractionsById(lcBody)
        CASE UPPER(lcTipo) == 'DRUG_INTERACTIONS_BY_ID'
			uf_dic_simposio_processDrugInteractionsById(lcBody)
        CASE UPPER(lcTipo) == 'PHYSIO_ALERTS_BY_ID'
			uf_dic_simposio_processPhysioAlerts(lcBody)
		CASE UPPER(lcTipo) == 'MULTIPLE_INTERACTION'
			uf_dic_simposio_processMultipleInteraction(lcBody)
        OTHERWISE
    ENDCASE
   
ENDFUNC

FUNCTION uf_dic_simposio_processProduct
	LPARAMETERS lcBody
	LOCAL oJson
	

	CREATE CURSOR ucrsDrug (name C(254) NULL, dosage C(254) NULL,description C(254) NULL, dciName C(254)NULL, dciId N(4)NULL, isGeneric L NULL, isDrug L NULL)
	
	oJson = nfJsonRead(lcBody)
	
	FOR EACH uv_row IN oJson.array
		Insert Into ucrsProduct From Name uv_row
	NEXT
	
	
	fecha("ucrsProduct")
ENDFUNC


FUNCTION uf_dic_simposio_limpaStr
	LPARAMETERS lcStr
	
	if(EMPTY(lcStr))
		lcStr = ''
	ENDIF
	
	
	LOCAL lcCaract
	lcCaract = ''
	
	lcStr = ALLTRIM(lcStr)
	lcStr = STRTRAN(lcStr,'\n',lcCaract )
	lcStr = STRTRAN(lcStr,'\n\n',lcCaract )
	lcStr = STRTRAN(lcStr,'\r\n',lcCaract )
	lcStr = STRTRAN(lcStr,'<br>',lcCaract )
	lcStr = STRTRAN(lcStr,'<\br>',lcCaract )
	lcStr = STRTRAN(lcStr,'<br\>',lcCaract )
	lcStr = STRTRAN(lcStr,CHR(13),lcCaract )
	lcStr = STRTRAN(lcStr,CHR(10),lcCaract )
	lcStr = STRTRAN(lcStr,CHR(13)+CHR(10),lcCaract )
	
	RETURN lcStr 
ENDFUN



FUNCTION uf_dic_simposio_getPosologia

	LOCAL lcPosologia
	lcPosologia = ''
	
	if(USED("ucrsDrugDosage"))
		select ucrsDrugDosage
		IF(!EMPTY(ALLTRIM(ucrsDrugDosage.child)))	
			lcPosologia =  lcPosologia + "Crian�a: " + 	uf_dic_simposio_limpaStr(ucrsDrugDosage.child)
			lcPosologia  = lcPosologia +  chr(13) 
		ENDIF
		
		IF(!EMPTY(ALLTRIM(ucrsDrugDosage.adult)))
			lcPosologia =  lcPosologia + "Adulto: " + uf_dic_simposio_limpaStr(ucrsDrugDosage.adult)
			lcPosologia  = lcPosologia +  chr(13) 
		ENDIF
		
		IF(!EMPTY(ALLTRIM(ucrsDrugDosage.elder)))
			lcPosologia =  lcPosologia  + "Idoso: " + uf_dic_simposio_limpaStr(ucrsDrugDosage.elder)
			lcPosologia  = lcPosologia  +  chr(13) 
		ENDIF
		
		IF(!EMPTY(ALLTRIM(ucrsDrugDosage.fractionalDose)))
			lcPosologia =  lcPosologia  + "Divis�vel: " + uf_dic_simposio_limpaStr(ucrsDrugDosage.fractionalDose)
			lcPosologia  = lcPosologia  +  chr(13) 
		ENDIF

		
		IF(!EMPTY(ALLTRIM(ucrsDrugDosage.kidneyFailure)))
			lcPosologia =  lcPosologia  + "Insufici�ncia Renal: " + uf_dic_simposio_limpaStr(ucrsDrugDosage.kidneyFailure)
			lcPosologia  = lcPosologia  +  chr(13) 
		ENDIF

		IF(!EMPTY(ALLTRIM(ucrsDrugDosage.liverFailure)))
			lcPosologia =  lcPosologia  + "Insufici�ncia Hepatica: " + uf_dic_simposio_limpaStr(ucrsDrugDosage.liverFailure)
			lcPosologia  = lcPosologia  +  chr(13) 
		ENDIF
		
		IF(!EMPTY(ALLTRIM(ucrsDrugDosage.guideline)))
			lcPosologia =  lcPosologia + "Guideline: " + uf_dic_simposio_limpaStr(ucrsDrugDosage.guideline)
			lcPosologia  = lcPosologia +  chr(13) 
		ENDIF	
		
		IF(!EMPTY(ALLTRIM(ucrsDrugDosage.bibliography)))
			lcPosologia =  lcPosologia + "Bibliografia: " +  uf_dic_simposio_limpaStr(ucrsDrugDosage.bibliography)
			lcPosologia  = lcPosologia +  chr(13) 
		ENDIF				
		
	ENDIF

	fecha("ucrsDrugDosage")
	
	RETURN lcPosologia

ENDFUNC



FUNCTION uf_dic_simposio_processDrugById
	Lparameters lcBody
	LOCAL oJson, lcAdult, lcBibliography, lcElder, lcFractionalDose, lcGuideline, lcKidneyFailure,lcLiverFailure, lcRoute, lnId, lcName, lcValue , lcChild, lcCMax 
	
	STORE '' TO  lcAdult, lcBibliography, lcElder, lcFractionalDose, lcGuideline, lcKidneyFailure,lcLiverFailure, lcRoute, lcChild  
	STORE 0 TO lnId 
	
	lcMax= 4000
	
	fecha("ucrsDrugDosage")

	CREATE CURSOR ucrsDrugDosage(adult Memo NULL, bibliography  Memo   NULL, elder Memo   NULL, child Memo  NULL, fractionalDose Memo;
	  NULL,guideline Memo  NULL , id  N(10) NULL , kidneyFailure Memo  NULL,liverFailure Memo  NULL, route Memo  NULL)

	oJson = nfJsonRead(lcBody)
	
	IF vartype(oJson)=='O' AND vartype(oJson.dosageInfo)=='O'
		
		if vartype(oJson.dosageInfo.adult)!='U' and !uf_servicos_chilkat_isNull(oJson.dosageInfo.adult)
			lcAdult = LEFT(ALLTRIM(oJson.dosageInfo.adult),lcMax)
		ENDIF
		if vartype(oJson.dosageInfo.bibliography)!='U' and !uf_servicos_chilkat_isNull(oJson.dosageInfo.bibliography)
			lcBibliography = LEFT(ALLTRIM(oJson.dosageInfo.bibliography),lcMax)
		ENDIF
		if vartype(oJson.dosageInfo.elder)!='U' and !uf_servicos_chilkat_isNull(oJson.dosageInfo.elder)
			lcElder = LEFT(ALLTRIM(oJson.dosageInfo.elder),lcMax)
		ENDIF
		if vartype(oJson.dosageInfo.fractionalDose)!='U' and !uf_servicos_chilkat_isNull(oJson.dosageInfo.fractionalDose)
			lcFractionalDose = LEFT(ALLTRIM(oJson.dosageInfo.fractionalDose),lcMax)
		ENDIF

		if vartype(oJson.dosageInfo.guideline)!='U' and !uf_servicos_chilkat_isNull(oJson.dosageInfo.guideline)
			lcGuideline = LEFT(ALLTRIM(oJson.dosageInfo.guideline),lcMax)
		ENDIF

		if vartype(oJson.dosageInfo.liverFailure)!='U' and !uf_servicos_chilkat_isNull(oJson.dosageInfo.liverFailure)
			lcLiverFailure = LEFT(ALLTRIM(oJson.dosageInfo.liverFailure),lcMax)
		ENDIF	

		if vartype(oJson.dosageInfo.kidneyFailure)!='U' and !uf_servicos_chilkat_isNull(oJson.dosageInfo.kidneyFailure)
			lcKidneyFailure = LEFT(ALLTRIM(oJson.dosageInfo.kidneyFailure),lcMax)
		ENDIF	

		if vartype(oJson.dosageInfo.route)!='U' and !uf_servicos_chilkat_isNull(oJson.dosageInfo.route)
			lcRoute = LEFT(ALLTRIM(oJson.dosageInfo.route),lcMax)
		ENDIF	
		
		if vartype(oJson.dosageInfo.child)!='U' and !uf_servicos_chilkat_isNull(oJson.dosageInfo.child)
			lcChild = LEFT(ALLTRIM(oJson.dosageInfo.child),lcMax)
		ENDIF		

		if vartype(oJson.dosageInfo.id)!='U' and !uf_servicos_chilkat_isNull(oJson.dosageInfo.id)
			lnId = oJson.dosageInfo.id
		ENDIF		
	ENDIF

	if !empty(ALLTRIM(lcAdult)) OR  !empty(ALLTRIM(lcChild)) OR  !empty(ALLTRIM(lcElder)) OR !empty(ALLTRIM(lcGuideline))
		insert into ucrsDrugDosage(child,adult,bibliography,elder,fractionalDose,guideline,kidneyFailure,liverFailure,route,id);
		values(lcChild,lcAdult,lcBibliography,lcElder,lcFractionalDose,lcGuideline,lcKidneyFailure,lcLiverFailure,lcRoute,lnId)
	ENDIF	

	if(RECCOUNT("ucrsDrugDosage")>0)
		RETURN .t.
	ELSE
		RETURN .f.	
	ENDIF	
ENDFUNC

FUNCTION uf_dic_simposio_processPhysioAlerts
	LPARAM	lcBody
	
	LOCAL oJson,  uv_row 

	fecha("ucrsPhysioAlerts")

	CREATE CURSOR ucrsPhysioAlerts (drugId N(10) NULL , type Memo NULL, route Memo   NULL, description Memo   NULL, level N(10) NULL, risk Memo   NULL, advice Memo   NULL)

	oJson = nfJsonRead(lcBody)

	FOR EACH uv_row IN oJson.array
		Insert Into ucrsPhysioAlerts From Name uv_row
	NEXT

	if(RECCOUNT("ucrsPhysioAlerts")>0)
		RETURN .t.
	ELSE
		RETURN .f.	
	ENDIF	
ENDFUNC

FUNCTION uf_dic_simposio_processFoodInteractionsById
	Lparameters lcBody
	LOCAL oJson, uv_row 
		
	fecha("ucrsDrugFoodIteration")	

	CREATE CURSOR ucrsDrugFoodIteration(description Memo NULL, drugId N(10) NULL , explanation Memo   NULL, foodType Memo   NULL, level N(10) NULL, severity Memo   NULL)	
	
	oJson = nfJsonRead(lcBody)

	FOR EACH uv_row IN oJson.array
		Insert Into ucrsDrugFoodIteration From Name uv_row
	NEXT

	if(RECCOUNT("ucrsDrugFoodIteration")>0)
		RETURN .t.
	ELSE
		RETURN .f.	
	ENDIF	
ENDFUNC



FUNCTION uf_dic_simposio_processDrugInteractionsById
	Lparameters lcBody
	LOCAL oJson, uv_row    

	fecha("ucrsDrugIterationDrugs")
	fecha("ucrsDrugIterationSeverity")

	CREATE CURSOR ucrsDrugIterationDrugs(class Memo NULL, drugId Memo NULL, moleculeName Memo NULL, name Memo NULL)
		
	CREATE CURSOR ucrsDrugIterationSeverity(advice Memo NULL, description Memo NULL, level N(10) NULL)
	
	oJson = nfJsonRead(lcBody)
		
	
	IF vartype(oJson)!='O' AND vartype(oJson.drugInteractions)=='U'
		RETURN .f.
	ENDIF
	
	FOR EACH uv_row IN oJson.drugInteractions.array
		Insert Into ucrsDrugIteration From Name uv_row
	NEXT

	IF(RECCOUNT("ucrsDrugIteration")>0)
		RETURN .t.
	ELSE
		RETURN .f.	
	ENDIF	
ENDFUNC

FUNCTION uf_dic_simposio_processDrug
	LPARAMETERS lcBody
	LOCAL oJson
	
	fecha("ucrsDrug")

	CREATE CURSOR ucrsDrug (name C(254) NULL,itemId N(19) NULL, dosage C(254) NULL,description C(254) NULL, dciName C(254)NULL, dciId N(4)NULL, isGeneric L NULL, isDrug L NULL)
	
	oJson = nfJsonRead(lcBody)
	
	FOR EACH uv_row IN oJson.array
		Insert Into ucrsDrug From Name uv_row
	NEXT
	
	uf_dic_insertCacheMedicamentos()
	uf_dic_deleteCache('drug','drug')
	
	fecha("ucrsDrug")
ENDFUNC



FUNCTION uf_dic_simposio_processDrugCodeMapper
	LPARAMETERS lcBody
	LOCAL oJson
	
	fecha("ucrsDrug")
	
	CREATE CURSOR ucrsDrug (drugId N(19) NULL, name C(254) NULL, dciptId N(4)NULL,  dciName C(254)NULL, dciId N(4)NULL, registryNumber N(19) NULL, chnm N(19) NULL,cnpem N(19) NULL)
	
	oJson = nfJsonRead(lcBody)
	
	FOR EACH uv_row IN oJson.array
		Insert Into ucrsDrug From Name uv_row
	NEXT
	
	uf_dic_insertCacheMedicamentos()
	uf_dic_deleteCache('drug','drug')
	
	fecha("ucrsDrug")
ENDFUNC

FUNCTION uf_dic_servicesAuthentication_gravar
        PARAMETERS lcAccessToken,lcRefreshToken,lcExpDate,lcType, lcDescr
        
        LOCAL lcStamp 
        lcStamp = uf_gerais_stamp()
        
        lcSQL = ""
        TEXT TO lcSQL NOSHOW textmerge
            INSERT INTO servicesAuthentication (stamp,acessToken,refreshToken,endDateToken,typeNr,typeDesc,ousrdata)
            VALUES ('<<ALLTRIM(lcStamp)>>','<<ALLTRIM(lcAccessToken)>>','<<ALLTRIM(lcRefreshToken)>>', CAST(CONVERT(datetimeoffset,'<<ALLTRIM(lcExpDate)>>',127) AS DATETIME),<<lcType)>>,'<<ALLTRIM(lcDescr)>>',GETDATE()) 
        ENDTEXT
        IF !uf_gerais_actgrelha("", "", lcSql)
            uf_perguntalt_chama("OCORREU UMA ANOMALIA A GUARDAR TOKEN DE AUTENTICA��O", "OK",16)
            RETURN .f.
        ENDIF
        
        RETURN .t.
        
ENDFUNC 

FUNCTION  uf_dic_simposio_saveAuth
    LPARAMETERS lcBody, loJson
    
    LOCAL lcAccessToken, lcRefreshToken, lcExpDate
    STORE "" TO lcAccessToken, lcRefreshToken,  lcExpDate
    
    IF(!EMPTY(lcBody))
        loJson.Load(lcBody)
        loJson.EmitCompact = 0
                
        IF(!uf_servicos_chilkat_isNull(loJson.StringOf("accessToken")))
            lcAccessToken= loJson.StringOf("accessToken")
        ENDIF
        IF(!uf_servicos_chilkat_isNull(loJson.StringOf("refreshToken")))
            lcRefreshToken= loJson.StringOf("refreshToken")
        ENDIF
        IF(!uf_servicos_chilkat_isNull(loJson.StringOf("expires")))
            lcExpDate = loJson.StringOf("expires")
        ENDIF   
        IF(!EMPTY(lcAccessToken) AND !EMPTY(lcExpDate))
            uf_dic_servicesAuthentication_gravar(lcAccessToken,lcRefreshToken,lcExpDate,1, "Simposium")
        ENDIF
                
    ENDIF   

    RETURN lcAccessToken
ENDFUNC

FUNCTION uf_dic_simposio_Authenticate
    
    LOCAL lcUser, lcPass, lcSQLUserSimpo, lcUrl, lcValidadeToken, lcJson, lcRequest
    STORE "" TO lcUser, lcPass, lcJson, lcRequest

    lcRequest = "AUTH"
    
    TEXT TO lcSQLAuthenticate NOSHOW TEXTMERGE
        SELECT UserSimposio = ISNULL((select ISNULL(textvalue,'') from b_parameters_site(nolock) where stamp='ADM0000000193' and site='<<ALLTRIM(mySite)>>'),'')
             , PassSimposio = ISNULL((select ISNULL(textvalue,'') from b_parameters_site(nolock)where stamp='ADM0000000194' and site='<<ALLTRIM(mySite)>>'),'')
    ENDTEXT 
    IF !uf_gerais_actGrelha("","ucrsAuthenticate",lcSQLAuthenticate )
        uf_perguntalt_chama("N�O FOI OBTIVO OS VALORES PARA AUTENTICA��O. POR FAVOR CONTACTE O SUPORTE","OK","",16)
        RETURN .F.
    ENDIF
    
    IF(USED("ucrsAuthenticate"))
        lcUser = ALLTRIM(ucrsAuthenticate.UserSimposio)
        lcPass = ALLTRIM(ucrsAuthenticate.PassSimposio) 
    ENDIF
    
    IF (EMPTY(lcUser) OR EMPTY(lcPass)) 
        uf_perguntalt_chama("Password ou UserName n�o preenchidos para Autentica��o. POR FAVOR CONTACTE O SUPORTE", "", "OK", 16)
        RETURN .F.
    ENDIF
       
    lcUrl = uf_dic_simposio_getURL(lcRequest)
    
    CREATE CURSOR ucrsLogin(username c(254), pw c(254))
    
    INSERT INTO ucrsLogin (userName, pw) VALUES(lcUser, lcPass)
    
    DIMENSION ua_fields[1,2]
    ua_fields[1,1] = "username"
    ua_fields[1,2] = "password"
    
    lcJson = uf_gerais_cursorToJson("ucrsLogin", "", .F., @ua_fields)  
    
    fecha("ucrsLogin")
    RELEASE ua_fields
             
    uf_dic_simposium_enviaJson(lcUrl,lcJson,"","POST",lcRequest)
    
    fecha("ucrsAuthenticate")
ENDFUNC

FUNCTION uf_dic_simposio_getURL 
    LPARAMETERS lcTipo
    LOCAL lcGetUrl
    STORE "" TO lcGetUrl, lcSufix
    
    lcTipo= ALLTRIM(lcTipo)
    
    TEXT TO lcSQLGetUrl NOSHOW TEXTMERGE
        SELECT GetUrl = (SELECT ISNULL(textvalue,'') FROM b_parameters_site(NOLOCK) WHERE stamp='ADM0000000192' and site='<<ALLTRIM(mySite)>>')
    ENDTEXT 
    IF !uf_gerais_actGrelha("","ucrsGetUrl",lcSQLGetUrl)
        uf_perguntalt_chama("N�O FOI OBTER A URL BASE. POR FAVOR CONTACTE O SUPORTE","OK","",16)
        RETURN .F.
    ENDIF       
    
    IF(USED("ucrsGetUrl"))
        lcGetUrl= ALLTRIM(ucrsGetUrl.GetUrl)
    ENDIF   
    IF (EMPTY(lcGetUrl)) 
        uf_perguntalt_chama("URL n�o preenchida. POR FAVOR CONTACTE O SUPORTE", "", "OK", 16)
        RETURN .F.
    ENDIF
        
    fecha("ucrsGetUrl")
    
    DO CASE
        Case UPPER(lcTipo)== 'AUTH'
            lcGetUrl=lcGetUrl + "user/authenticate"         
        Case UPPER(lcTipo)== 'DRUG' OR UPPER(lcTipo)== 'DRUG_BY_ID' 
            lcGetUrl=lcGetUrl + "drug"
        Case UPPER(lcTipo)== 'DRUG_MAPPER'
            lcGetUrl=lcGetUrl + "drug/codemapper"            
        CASE UPPER(lcTipo) == 'PRODUCT'
         	lcGetUrl=lcGetUrl + "product"
        CASE UPPER(lcTipo) == 'FOOD_INTERACTIONS_BY_ID'
        	lcGetUrl=lcGetUrl + "drug/{REPLACE}/food-interactions"
        CASE UPPER(lcTipo) == 'DRUG_INTERACTIONS_BY_ID'
			lcGetUrl=lcGetUrl + "drug/{REPLACE}/interactions"
        CASE UPPER(lcTipo) == 'PHYSIO_ALERTS_BY_ID'
			lcGetUrl=lcGetUrl + "drug/{REPLACE}/physio-alerts"
		CASE UPPER(lcTipo) == 'MULTIPLE_INTERACTION'
			lcGetUrl=lcGetUrl + "drug/{REPLACE}/interaction"
        OTHERWISE
    ENDCASE    
	

	
    RETURN lcGetUrl 
ENDFUNC


** uf_dic_retornaMedicamentoMapper("5440987",1)
FUNCTION uf_dic_retornaMedicamentoMapper
    LPARAMETERS lcQuery, lcExato
    LOCAL lcHeaderUrl, lcExatoRes, lcGetUrl, lcToken, lcUrlFinal, lcRequest
    STORE ""  TO lcHeaderUrl, lcExatoRes, lcGetUrl, lcToken, lcUrlFinal, lcRequest

    lcRequest = "DRUG_MAPPER"  
    
    lcHeaderUrl= uf_dic_return_tipoNomeCliente()
    if(EMPTY(lcHeaderUrl))
    	return .f.
    ENDIF
   
    lcExatoRes = uf_dic_return_exact(lcExato)

    lcToken = uf_dic_validaToken(1)
        
    lcGetUrl = uf_dic_simposio_getURL(lcRequest) 
       
    lcUrlFinal= ALLTRIM(lcGetUrl)+ "?id=" +lcQuery + lcExatoRes + lcHeaderUrl + "&ist=RegistryNumber&skip=0&take=10"

    RELEASE myRefSimposium
    myRefSimposium = ALLTRIM(lcQuery)
    
    IF(EMPTY(lcToken))
        uf_perguntalt_chama("Token de autentica��o n�o retornado. POR FAVOR CONTACTE O SUPORTE", "", "OK", 16)
        RETURN .f.
    ENDIF
     
    uf_dic_simposium_enviaJson(ALLTRIM(lcUrlFinal),"",lcToken,"GET",lcRequest)
                   
ENDFUNC


** uf_dic_retornaMedicamento("5440987",1)
FUNCTION uf_dic_retornaMedicamento
    LPARAMETERS lcQuery, lcExato
    LOCAL lcHeaderUrl, lcExatoRes, lcGetUrl, lcToken, lcUrlFinal, lcRequest
    STORE ""  TO lcHeaderUrl, lcExatoRes, lcGetUrl, lcToken, lcUrlFinal, lcRequest

    lcRequest = "DRUG"  
    
    lcHeaderUrl= uf_dic_return_tipoNomeCliente()
    if(EMPTY(lcHeaderUrl))
    	return .f.
    ENDIF
   
    lcExatoRes = uf_dic_return_exact(lcExato)

    lcToken = uf_dic_validaToken(1)
        
    lcGetUrl = uf_dic_simposio_getURL(lcRequest) 
       
    lcUrlFinal= ALLTRIM(lcGetUrl)+ "?query=" +lcQuery + lcExatoRes + lcHeaderUrl

    RELEASE myRefSimposium
    myRefSimposium = ALLTRIM(lcQuery)
    
    IF(EMPTY(lcToken))
        uf_perguntalt_chama("Token de autentica��o n�o retornado. POR FAVOR CONTACTE O SUPORTE", "", "OK", 16)
        RETURN .f.
    ENDIF
 
    uf_dic_simposium_enviaJson(ALLTRIM(lcUrlFinal),"",lcToken,"GET",lcRequest)
                   
ENDFUNC

FUNCTION uf_dic_getItemIDFromRefFromCache
	LPARAMETERS lcRef	
	
	LOCAL  	lcItemId 
	lcItemId = ""
	fecha("ucrsPesqInCache")
	IF(uf_dic_pesqInCache('drug', 'drug','ref',ALLTRIM(lcRef)))
		if(USED("ucrsPesqInCache"))
	    	SELECT ucrsPesqInCache
	    	lcItemId = ucrsPesqInCache.itemId
    	ENDIF
    	
    ENDIF
    fecha("ucrsPesqInCache")    
	RETURN lcItemId 
ENDFUNC

&&uf_dic_retornaMedicamentoPorId("5440987",1)
FUNCTION uf_dic_retornaMedicamentoPorId
	LPARAMETERS lcQuery, lcExato
	LOCAL lcHeaderUrl, lcGetUrl, lcToken, lcUrlFinal, lcRequest, lcItemId
    STORE ""  TO lcHeaderUrl, lcGetUrl, lcToken, lcUrlFinal, lcRequest, lcItemId
    
	lcRequest = "DRUG_BY_ID"
    lcQuery = ALLTRIM(lcQuery)
    lcHeaderUrl= uf_dic_return_tipoNomeCliente()
    if(EMPTY(lcHeaderUrl))
    	return .f.
    ENDIF
  
    lcToken = uf_dic_validaToken(1)
      
    lcGetUrl = uf_dic_simposio_getURL(lcRequest) 
  
     ** pesquisa na cache
    lcItemId  = uf_dic_getItemIDFromRefFromCache(lcQuery)   

    ** sen�o tem na cache, volta a pedir
    if(EMPTY(lcItemId))
   	   uf_dic_retornaMedicamentoMapper(lcQuery,1)  	    	
	   lcItemId  = uf_dic_getItemIDFromRefFromCache(lcQuery) 	     
    ENDIF
    
    if(EMPTY(lcItemId))
    	uf_perguntalt_chama("Producto n�o existe.", "", "OK", 16)
        RETURN .f.
    ENDIF
    	
	lcUrlFinal= ALLTRIM(lcGetUrl)+ "/" +  ALLTRIM(STR(lcItemId)) +"?"+ lcHeaderUrl
	
	IF(EMPTY(lcToken))
        uf_perguntalt_chama("Token de autentica��o n�o retornado. POR FAVOR CONTACTE O SUPORTE", "", "OK", 16)
        RETURN .f.
    ENDIF
    
    lcRequest = "DRUG_BY_ID"

	uf_dic_simposium_enviaJson(ALLTRIM(lcUrlFinal),"",lcToken,"GET",lcRequest)
	
ENDFUNC

&&uf_dic_retornaAlertasFisicosMedicamentoPorId("5440987",1)
FUNCTION uf_dic_retornaAlertasFisicosMedicamentoPorId()
	LPARAMETERS lcQuery, lcExato
	LOCAL lcHeaderUrl, lcGetUrl, lcToken, lcUrlFinal, lcRequest, lcItemId
    STORE ""  TO lcHeaderUrl, lcGetUrl, lcToken, lcUrlFinal, lcRequest, lcItemId
    
	lcRequest = "PHYSIO_ALERTS_BY_ID"
    lcQuery = ALLTRIM(lcQuery)
    lcHeaderUrl= uf_dic_return_tipoNomeCliente()
    if(EMPTY(lcHeaderUrl))
    	return .f.
    ENDIF
    
    lcToken = uf_dic_validaToken(1)
        
    lcGetUrl = uf_dic_simposio_getURL(lcRequest) 
    
     ** pesquisa na cache
    lcItemId  = uf_dic_getItemIDFromRefFromCache(lcQuery)   
            
    ** sen�o tem na cache, volta a pedir
    if(EMPTY(lcItemId ))
   	   uf_dic_retornaMedicamentoMapper(lcQuery,1)   	
	   lcItemId  = uf_dic_getItemIDFromRefFromCache(lcQuery)   
    ENDIF
    
    if(EMPTY(lcItemId))
    	uf_perguntalt_chama("Producto n�o existe.", "", "OK", 16)
        RETURN .f.
    ENDIF
    
    lcGetUrl = STRTRAN(lcGetUrl,"{REPLACE}",ALLTRIM(STR(lcItemId)))  
    	
	lcUrlFinal= ALLTRIM(lcGetUrl) + "?"+ lcHeaderUrl
	
	IF(EMPTY(lcToken))
        uf_perguntalt_chama("Token de autentica��o n�o retornado. POR FAVOR CONTACTE O SUPORTE", "", "OK", 16)
        RETURN .f.
    ENDIF

	uf_dic_simposium_enviaJson(ALLTRIM(lcUrlFinal),"",lcToken,"GET",lcRequest)
	
ENDFUNC

&&uf_dic_retornaIteracaoOutrosMedicamentosPorId("5440987",1)
FUNCTION uf_dic_retornaIteracaoOutrosMedicamentosPorId
	LPARAMETERS lcQuery, lcExato
	LOCAL lcHeaderUrl, lcGetUrl, lcToken, lcUrlFinal, lcRequest, lcItemId
    STORE ""  TO lcHeaderUrl, lcGetUrl, lcToken, lcUrlFinal, lcRequest, lcItemId   

	lcRequest = "DRUG_INTERACTIONS_BY_ID"
    lcQuery = ALLTRIM(lcQuery)
    lcHeaderUrl= uf_dic_return_tipoNomeCliente()
    if(EMPTY(lcHeaderUrl))
    	return .f.
    ENDIF
    
    lcToken = uf_dic_validaToken(1)
        
    lcGetUrl = uf_dic_simposio_getURL(lcRequest) 
    
     ** pesquisa na cache
    lcItemId  = uf_dic_getItemIDFromRefFromCache(lcQuery)   
         
    ** sen�o tem na cache, volta a pedir
    if(EMPTY(lcItemId ))
   	   uf_dic_retornaMedicamentoMapper(lcQuery,1)   	
	   lcItemId  = uf_dic_getItemIDFromRefFromCache(lcQuery)   
    ENDIF
    
    if(EMPTY(lcItemId))
    	uf_perguntalt_chama("Producto n�o existe.", "", "OK", 16)
        RETURN .f.
    ENDIF
    
    lcGetUrl = STRTRAN(lcGetUrl,"{REPLACE}",ALLTRIM(STR(lcItemId)))   
    	
	lcUrlFinal= ALLTRIM(lcGetUrl) + "?"+ lcHeaderUrl
	
	IF(EMPTY(lcToken))
        uf_perguntalt_chama("Token de autentica��o n�o retornado. POR FAVOR CONTACTE O SUPORTE", "", "OK", 16)
        RETURN .f.
    ENDIF

	uf_dic_simposium_enviaJson(ALLTRIM(lcUrlFinal),"",lcToken,"GET",lcRequest)
	
ENDFUNC

&&uf_dic_retornaIteracaoAlimentoMedicamentoPorId("5440987",1)
FUNCTION uf_dic_retornaIteracaoAlimentoMedicamentoPorId()
	LPARAMETERS lcQuery, lcExato
	LOCAL lcHeaderUrl, lcGetUrl, lcToken, lcUrlFinal, lcRequest, lcItemId
    STORE ""  TO lcHeaderUrl, lcGetUrl, lcToken, lcUrlFinal, lcRequest, lcItemId 

	lcRequest = "FOOD_INTERACTIONS_BY_ID"
    lcQuery = ALLTRIM(lcQuery)
    lcHeaderUrl= uf_dic_return_tipoNomeCliente()
    if(EMPTY(lcHeaderUrl))
    	return .f.
    ENDIF
    
    lcToken = uf_dic_validaToken(1)
        
    lcGetUrl = uf_dic_simposio_getURL(lcRequest) 
    
     ** pesquisa na cache
    lcItemId  = uf_dic_getItemIDFromRefFromCache(lcQuery)    
        
    ** sen�o tem na cache, volta a pedir
    IF(EMPTY(lcItemId ))
   	   uf_dic_retornaMedicamentoMapper(lcQuery,1)   	
	   lcItemId  = uf_dic_getItemIDFromRefFromCache(lcQuery)   
    ENDIF
    
    IF(EMPTY(lcItemId))
    	uf_perguntalt_chama("Producto n�o existe.", "", "OK", 16)
        RETURN .f.
    ENDIF
    
    lcGetUrl = STRTRAN(lcGetUrl,"{REPLACE}",ALLTRIM(STR(lcItemId)))
     	
	lcUrlFinal= ALLTRIM(lcGetUrl) + "?"+ lcHeaderUrl
	
	IF(EMPTY(lcToken))
        uf_perguntalt_chama("Token de autentica��o n�o retornado. POR FAVOR CONTACTE O SUPORTE", "", "OK", 16)
        RETURN .f.
    ENDIF
	
	uf_dic_simposium_enviaJson(ALLTRIM(lcUrlFinal),"",lcToken,"GET",lcRequest)
	
ENDFUNC

&&uf_dic_retornaProduct("5440987",1)
FUNCTION uf_dic_retornaProduct
	LPARAMETERS lcQuery, lcExato
	LOCAL lcHeaderUrl, lcExatoRes, lcGetUrl, lcToken, lcUrlFinal, lcRequest, lcItemId
    STORE ""  TO lcHeaderUrl, lcExatoRes, lcGetUrl, lcToken, lcUrlFinal, lcRequest, lcItemId
	
	lcRequest = "PRODUCT"
    lcQuery = ALLTRIM(lcQuery)
    lcHeaderUrl= uf_dic_return_tipoNomeCliente()
    if(EMPTY(lcHeaderUrl))
    	return .f.
    ENDIF
    
    lcExatoRes = uf_dic_return_exact(lcExato)

    lcToken = uf_dic_validaToken(1)
        
    lcGetUrl = uf_dic_simposio_getURL(lcRequest) 
    
     ** pesquisa na cache
    lcItemId  = uf_dic_getItemIDFromRefFromCache(lcQuery)   
           
    ** sen�o tem na cache, volta a pedir
    IF(EMPTY(lcItemId ))
   		 uf_dic_retornaMedicamentoMapper(lcQuery,1)   	
	   lcItemId  = uf_dic_getItemIDFromRefFromCache(lcQuery)   
    ENDIF
    
    IF(EMPTY(lcItemId))
    	uf_perguntalt_chama("Producto n�o existe.", "", "OK", 16)
        RETURN .f.
    ENDIF
        	
	lcUrlFinal= ALLTRIM(lcGetUrl)+ "?query=" +lcQuery + lcExatoRes + lcHeaderUrl
	
	IF(EMPTY(lcToken))
        uf_perguntalt_chama("Token de autentica��o n�o retornado. POR FAVOR CONTACTE O SUPORTE", "", "OK", 16)
        RETURN .f.
    ENDIF
	
	uf_dic_simposium_enviaJson(ALLTRIM(lcUrlFinal),"",lcToken,"GET",lcRequest)
	
ENDFUNC


FUNCTION uf_dic_getToken
	LPARAMETERS lcTipo
	fecha("ucrsValidaToken")
	
    TEXT TO lcSQLValidaToken NOSHOW TEXTMERGE
        exec up_get_servicesAuthentication <<lcTipo>>
    ENDTEXT 
    IF !uf_gerais_actGrelha("","ucrsValidaToken",lcSQLValidaToken )
        uf_perguntalt_chama("N�O FOI OBTER A URL BASE. POR FAVOR CONTACTE O SUPORTE","OK","",16)
        RETURN ''
    ENDIF   

ENDFUNC
 
FUNCTION uf_dic_validaToken
    LPARAMETERS lcTipo
    LOCAL lcAcessToken ,lcTokenRefresh, lcDataToRefresh
    STORE "" TO lcTokenRefresh, lcAcessToken 
    lcDataToRefresh = 0 
    
    ** procura token j� utilizado
    uf_dic_getToken(lcTipo)
    
    IF(USED("ucrsValidaToken"))
        IF(RECCOUNT("ucrsValidaToken")>0)
            lcAcessToken =  ALLTRIM(ucrsValidaToken.acessToken)
            lcTokenRefresh = ALLTRIM(ucrsValidaToken.refreshToken)
            lcDataToRefresh = ucrsValidaToken.dateToRefresh
        ENDIF       
    ENDIF      
	
	fecha("ucrsValidaToken")

    IF (lcDataToRefresh <= 2 OR EMPTY(lcAcessToken)  OR EMPTY(lcTokenRefresh))
        ** pede novo token
       uf_dic_simposio_Authenticate()
    ENDIF
    fecha("ucrsValidaToken")
       
    ** procura outro apos segunda autenticacao
    if(EMPTY(lcAcessToken))
    	uf_dic_getToken(lcTipo)
    	lcAcessToken =  ALLTRIM(ucrsValidaToken.acessToken)
    	fecha("ucrsValidaToken")
    ENDIF
    
    IF(EMPTY(lcAcessToken))
        uf_perguntalt_chama("N�O FOI POSSIVEL AUTENTICAR. POR FAVOR CONTACTE O SUPORTE","OK","",16)
        RETURN ''
    ENDIF
           
    RETURN lcAcessToken
ENDFUNC

FUNCTION uf_dic_valida_tipoNomeCliente

    LOCAL lcNrInfarmed, lcNrOdem, lcConcat, lcTipoCliente
    STORE "" TO lcNrInfarmed, lcNrOdem
        
    lcNomeCliente  = uf_dic_return_nomeCliente()
    
    lcTipoCliente  = uf_dic_return_tipoCliente()
    
    lcNrOdem = uf_dic_return_nrOrdem()
        
    IF(EMPTY(lcTipoCliente) OR EMPTY(lcNomeCliente))
       RETURN .f.
    ENDIF
       
    RETURN .t.

ENDFUNC

FUNCTION uf_dic_return_tipoNomeCliente
    LOCAL lcNrInfarmed, lcNrOdem, lcConcat, lcTipoCliente
    STORE "" TO lcNrInfarmed, lcNrOdem
        
    lcNomeCliente  = uf_dic_return_nrInfarmed()
    
    lcTipoCliente  = uf_dic_return_tipoCliente()
    
    lcNrOdem = uf_dic_return_nrOrdem()
        
    IF(EMPTY(lcTipoCliente) OR EMPTY(lcNomeCliente))
        uf_perguntalt_chama("Abreviatura do cliente ou tipo cliente vazios. POR FAVOR CONTACTE O SUPORTE","OK","",16)
        RETURN ''
    ENDIF
    
    lcConcat = "&Workplace="  + ALLTRIM(lcNomeCliente)+ "&Service=" +ALLTRIM(lcTipoCliente)
    
    if(!empty(lcNrOdem))
    	lcConcat = lcConcat + "&MemberId="  + ALLTRIM(lcNrOdem)
    endif
    
    RETURN lcConcat 
ENDFUNC

FUNCTION uf_dic_return_nrInfarmed
    LOCAL lcNrInfarmed, lcNomeAbrev, lcDevolve
    STORE '' TO lcNrInfarmed, lcNomeAbrev, lcDevolve
    
    IF(!USED("uCrse1"))
        uf_perguntalt_chama("N�O FOI POSSIVEL OBTER INFORMA��O DA EMPRESA. POR FAVOR CONTACTE O SUPORTE","OK","",16)
        RETURN 0
    ENDIF
  
    SELECT ucrse1 
    GO TOP
    lcNrInfarmed = ucrse1.u_infarmed
    
    lcNomeAbrev = ALLTRIM(ucrse1.nomabrv)
    
    IF(EMPTY(lcNrInfarmed) AND EMPTY(lcNomeAbrev))
        uf_perguntalt_chama("NUMERO INFARMED POR PREENCHER. POR FAVOR CONTACTE O SUPORTE","OK","",16)
        RETURN lcDevolve
    ENDIF
	
	IF(!EMPTY(lcNrInfarmed))
		lcDevolve = ''
		lcDevolve = lcNrInfarmed
	ENDIF
	
	IF(!EMPTY(lcNomeAbrev))
		lcDevolve = ''
		lcDevolve = lcNomeAbrev
	ENDIF
	
	IF(!EMPTY(lcNrInfarmed) AND !EMPTY(lcNomeAbrev))
		lcDevolve = ''
		lcDevolve = ALLTRIM(STR(lcNrInfarmed)) +' - ' + lcNomeAbrev
		lcDevolve = LEFT(lcDevolve ,30)
	ENDIF
	
    RETURN lcDevolve
ENDFUNC

FUNCTION uf_dic_return_nomeCliente
    LOCAL lcNomeEmpresa
    STORE '' TO lcNomeEmpresa
    
    IF(!USED("uCrse1"))
        uf_perguntalt_chama("N�O FOI POSSIVEL OBTER INFORMA��O DA EMPRESA. POR FAVOR CONTACTE O SUPORTE","OK","",16)
        RETURN 0
    ENDIF
    
    SELECT ucrse1 
    GO TOP
    lcNomeEmpresa= ALLTRIM(ucrse1.nomecomp)
    
    IF(EMPTY(lcNomeEmpresa))
        uf_perguntalt_chama("NOME DA EMPRESA POR PREENCHER. POR FAVOR CONTACTE O SUPORTE","OK","",16)
        RETURN ''
    ENDIF

    RETURN lcNomeEmpresa
ENDFUNC

FUNCTION uf_dic_return_tipoCliente
    LOCAL lcTipoEmpresa
    STORE '' TO lcTipoEmpresa
    
    IF(!USED("uCrse1"))
        uf_perguntalt_chama("N�O FOI POSSIVEL OBTER INFORMA��O DA EMPRESA. POR FAVOR CONTACTE O SUPORTE","OK","",16)
        RETURN 0
    ENDIF
    
    SELECT ucrse1 
    GO TOP
    lcTipoEmpresa= ALLTRIM(ucrse1.tipoempresa)
    
    IF(EMPTY(lcTipoEmpresa))
        uf_perguntalt_chama("TIPO DA EMPRESA POR PREENCHER. POR FAVOR CONTACTE O SUPORTE","OK","",16)
        RETURN ''
    ENDIF

    RETURN lcTipoEmpresa
ENDFUNC

FUNCTION uf_dic_return_nrOrdem
    LOCAL lcNrOdem, lcNo
    STORE "" TO lcNrOdem
    
    IF(!USED("ucrsuser"))
        uf_perguntalt_chama("N�O FOI POSSIVEL OBTER INFORMA��O DO OPERADOR. POR FAVOR CONTACTE O SUPORTE","OK","",16)
        RETURN ''
    ENDIF
        
    SELECT ucrsuser
    GO TOP 
    lcNo = ucrsuser.userno
    
    TEXT TO lcSQLGetNrOrdem NOSHOW TEXTMERGE
        SELECT drcedula FROM b_us(NOLOCK) WHERE userno = <<lcNo>> 
    ENDTEXT 
    IF !uf_gerais_actGrelha("","ucrsGetNrOrdem",lcSQLGetNrOrdem )
        uf_perguntalt_chama("N�O FOI OBTER O N�MERO DO OPERADOR. POR FAVOR CONTACTE O SUPORTE","OK","",16)
        RETURN ''
    ENDIF   
    
    lcNrOdem = ucrsGetNrOrdem.drcedula
     
    fecha("ucrsGetNrOrdem")
    
    RETURN lcNrOdem
ENDFUNC

FUNCTION uf_dic_return_exact
    LPARAMETERS lcExact
    LOCAL lcExacRes
    STORE "" TO lcExacRes
    
    DO CASE
        CASE EMPTY(lcExact)
            lcExacRes = ""
        CASE lcExact = 0
            lcExacRes = "&exact=false"
        CASE lcExact = 1
            lcExacRes =  "&exact=true"
        OTHERWISE
            lcExacRes = "&exact=false"
    ENDCASE
    
    RETURN lcExacRes
    
ENDFUNC

FUNCTION uf_dic_insertCacheMedicamentos
	LPARAMETERS lcRef
	
	LOCAL lcSQLInsertCache, lcStamp 
	STORE '' TO lcSQLInsertCache
	
	IF(!USED("ucrsDrug") AND RECCOUNT("ucrsDrug")==0)
		uf_perguntalt_chama("Ocorreu um  erro ao  inserir na cache. POR FAVOR CONTACTE O SUPORTE","OK","",16)
		RETURN .F.
	ENDIF
	
	lcStamp = uf_gerais_stamp()
	
	LOCAL lcName, lcDosage, lcDescription, lcDciName, lcDciId,lcIsGeneric, lcIsDrug, lcItemId, lcRef

	STORE ''  TO lcName, lcDescription, lcDciName, lcRef
	STORE 1   TO lcIsGeneric, lcIsDrug
	STORE 0   TO lcDosage , lcDciId, itemId
	
	SELECT ucrsDrug 
	GO TOP
	if(!uf_servicos_chilkat_isNull(ucrsDrug.name))
		lcName = STRTRAN(ALLTRIM(ucrsDrug.name),"'","")
	ENDIF
	if(!uf_servicos_chilkat_isNull(ucrsDrug.drugId))
		itemId= ucrsDrug.drugId
	ENDIF

	if(!uf_servicos_chilkat_isNull(ucrsDrug.dciId))
		lcDciId= ucrsDrug.dciId
	ENDIF
	


	if(VARTYPE("myRefSimposium")!='U')
		lcRef = ALLTRIM(myRefSimposium)	    
	    RELEASE myRefSimposium
	ENDIF
	    

	TEXT TO lcSQLInsertCache Noshow Textmerge
		
		DELETE FROM cache_simposio_medicamentos WHERE ref =  '<<ALLTRIM(lcRef)>>'
	
		INSERT INTO cache_simposio_medicamentos
			(stamp, loja, itemId, name, dosage, description,
				dciName,dciId,isGeneric, isDrug,
				ousrinis,usrinis, ref)
		VALUES
			('<<lcStamp>>','<<mysite>>' , '<<itemId>>', '<<lcName>>', '<<lcDosage>>','<<lcDescription>>',
				'<<lcDciName>>', <<lcDciId>>,  <<lcIsGeneric>>, <<lcIsDrug>>,
				'<<ALLTRIM(m_chinis)>>', '<<ALLTRIM(m_chinis)>>', '<<ALLTRIM(lcRef)>>')
	ENDTEXT
	uf_gerais_actGrelha("","",lcSQLInsertCache)
	
ENDFUNC

FUNCTION uf_dic_deleteCache
	LPARAMETERS lcGroup, lcSubGroup
	
	LOCAL lcSQLDelteCache
 	STORE '' TO lcSQLDelteCache
 	
 	IF (EMPTY(lcGroup) AND EMPTY(lcSubGroup))
 		RETURN .F.
 	ENDIF
 	
    TEXT TO lcSQLDelteCache NOSHOW TEXTMERGE
        EXEC up_delete_cache_simposio '<<lcGroup>>', '<<lcSubGroup>>'
    ENDTEXT 
    IF !uf_gerais_actGrelha("","",lcSQLDelteCache)
        uf_perguntalt_chama("Ocorreu um  erro ao  eliminar a cache. POR FAVOR CONTACTE O SUPORTE","OK","",16)
        RETURN .F.
    ENDIF  
     
ENDFUNC

FUNCTION uf_dic_pesqInCache
	LPARAMETERS lcGrupo, lcSubGrupo,lcAttr, lcItemID
	
	LOCAL lcSQLPesqInCache 
 	STORE '' TO lcSQLPesqInCache
 	
 	IF (EMPTY(lcGrupo) AND EMPTY(lcSubGrupo))
		uf_perguntalt_chama("Ocorreu um  erro a inicar a pesquisa por grupo e sub-grupo. POR FAVOR CONTACTE O SUPORTE","OK","",16)
 		RETURN .F.
 	ENDIF
 	
 	fecha("ucrsPesqInCache")
 	
 	uf_dic_deleteCache(lcGrupo,lcSubGrupo)
 	
 	
 	IF(EMPTY(lcGrupo))
 		lcGrupo = ""
 	ENDIF
 	IF(EMPTY(lcSubGrupo))
 		lcSubGrupo = ""
 	ENDIF
 	IF(EMPTY(lcAttr))
 		lcAttr = ""
 	ENDIF
 	IF(EMPTY(lcItemID))
 		lcItemID = ""
 	ENDIF
 	
 	fecha("ucrsPesqInCache")
 	 	
    TEXT TO lcSQLPesqInCache NOSHOW TEXTMERGE
        EXEC up_pesq_cacheSimposio  '<<ALLTRIM(lcGrupo)>>', '<<ALLTRIM(lcSubGrupo)>>', '<<ALLTRIM(lcAttr)>>','<<ALLTRIM(lcItemID)>>'
    ENDTEXT 
    IF !uf_gerais_actGrelha("","ucrsPesqInCache",lcSQLPesqInCache )
        uf_perguntalt_chama("Ocorreu um  erro ao  eleminar a cache. POR FAVOR CONTACTE O SUPORTE","OK","",16)
        RETURN .F.
    ENDIF  
	
	IF(RECCOUNT("ucrsPesqInCache"))>0
		RETURN .T.
	ELSE
		RETURN .F.
	ENDIF
	
ENDFUNC

FUNCTION uf_dic_simposio_processMultipleInteraction
	LPARAMETERS lcBody
	LOCAL oJson
	
ENDFUNC

&&uf_dic_retornaMultipleInteraction("5440987,8776476",1)
FUNCTION uf_dic_retornaMultipleInteraction()
	LPARAMETERS lcQuery, lcExato
	LOCAL lcHeaderUrl, lcGetUrl, lcToken, lcUrlFinal, lcRequest, lcItemId, lcConcatRefs  
    STORE ""  TO lcHeaderUrl, lcGetUrl, lcToken, lcUrlFinal, lcRequest, lcItemId, lcConcatRefs   
    
	lcRequest = "MULTIPLE_INTERACTION"
    lcQuery = ALLTRIM(lcQuery)
	lcHeaderUrl = uf_dic_return_tipoNomeCliente()
    IF(EMPTY(lcHeaderUrl))
    	RETURN .f.
    ENDIF

    lcToken = uf_dic_validaToken(1)
        
    lcGetUrl = uf_dic_simposio_getURL(lcRequest) 
    
    FOR i = 1 TO ALINES(ua_refs, lcQuery, ",")
   
	    ** pesquisa na cache
	    lcItemId  = uf_dic_getItemIDFromRefFromCache(ua_refs[i]) 
	    
	    ** sen�o tem na cache, volta a pedir
	    IF(EMPTY(lcItemId))
	   	   uf_dic_retornaMedicamentoMapper(ua_refs[i],1)   			   
		   lcItemId  = uf_dic_getItemIDFromRefFromCache(ua_refs[i])  
	    ENDIF
       	
		lcItemId  = uf_dic_getItemIDFromRefFromCache(ua_refs[i])   
    
	    IF(EMPTY(lcItemId))
	    	uf_perguntalt_chama("Producto n�o existe.", "", "OK", 16)
	        RETURN .f.
	    ENDIF
	    
	    lcConcatRefs  = IIF(EMPTY(lcConcatRefs),"?ids=", ALLTRIM(lcConcatRefs) + "&ids=") + ALLTRIM(STR(lcItemId))

    ENDFOR
  
    lcGetUrl = STRTRAN(lcGetUrl,"{REPLACE}",ALLTRIM(lcConcatRefs))
 	
	lcUrlFinal= ALLTRIM(lcGetUrl) + "?"+ lcHeaderUrl

	IF(EMPTY(lcToken))
        uf_perguntalt_chama("Token de autentica��o n�o retornado. POR FAVOR CONTACTE O SUPORTE", "", "OK", 16)
        RETURN .f.
    ENDIF

	uf_dic_simposium_enviaJson(ALLTRIM(lcUrlFinal),"",lcToken,"GET",lcRequest)

ENDFUNC

FUNCTION uf_dic_simposium_pedido_tipo_descricao
    PARAMETERS lnTipoPedido
    
    DO CASE 
        CASE lnTipoPedido == 1
            RETURN "drugs_interaction"
        CASE lnTipoPedido == 2
            RETURN "drug_info"
        CASE lnTipoPedido == 3
            RETURN "food_interactions"
        CASE lnTipoPedido == 4
            RETURN "physio_alerts"
        CASE lnTipoPedido == 5
            RETURN "drug_interaction_Id"
        OTHERWISE
            RETURN ""
    ENDCASE         
    
ENDFUNC


FUNCTION uf_dic_simposium_preparaInteracoes

    * Fecha os cursores se estiverem abertos
    fecha("uCrsListaInteracaoMed")
    fecha("ucrsTempSimposiumSaida")
    fecha("uCrsListaInteracaoMedFinal")
    
    if(EMPTY(uf_gerais_getParameter_site('ADM0000000225', 'BOOL', mySite)))
    	RETURN .f.
    ENDIF
    

    IF USED("fi")
        * Seleciona registo com 'ref' num�rica de 7 caracteres
         SELECT DISTINCT ref FROM fi WHERE LEN(ALLTRIM(ref)) = 7 AND uf_gerais_IsNumeric(ALLTRIM(ref)) AND EMPTY(stns);
        AND (ALLTRIM(fi.familia)=="1" OR  ALLTRIM(fi.familia)=="2"  OR  ALLTRIM(fi.familia)=="58")  INTO CURSOR uCrsListaInteracaoMed READWRITE

        * Verifica se o cursor foi criado corretamente
        IF !USED("uCrsListaInteracaoMed")
            regua(2)
            fecha("uCrsListaInteracaoMed")
    		fecha("ucrsTempSimposiumSaida")
    		fecha("uCrsListaInteracaoMedFinal")
            RETURN .F.
        ENDIF
        
        
     
        LOCAL lcNo, lcEstab
        lcNo=0
        lcEstab = 0
        if(USED("ft"))
        	lcNo = ft.no
        	lcEstab = ft.estab      
        ENDIF
        
        if(USED("ucrsatendcl"))
        	lcNo = ucrsatendcl.no
        	lcEstab = ucrsatendcl.estab      
        ENDIF
        
        fecha("ucrsPesqInCac")
        fecha("ucrsRefsTemp")
        
        ** acrescenta historico
        ** ignorar as entidades e cliente 200
        IF  uf_gerais_getParameter_site('ADM0000000226', 'NUM', mySite)>0 AND lcNo>200
        	
        	LOCAL lcDataFim, lcDataInit, lcMesesEmFalta
        	lcMesesEmFalta = uf_gerais_getParameter_site('ADM0000000226', 'NUM', mySite)
           	lcDataInit = uf_gerais_getdate(GOMONTH(DATE(),lcMesesEmFalta*-1),"SQL")
           	lcDataFim = uf_gerais_getdate(DATE(),"SQL")
            	
        	LOCAL  lcSQLPesq1
        	lcSQLPesq1 = ''      	 	 	
		    TEXT TO lcSQLPesq1 NOSHOW TEXTMERGE
		        exec up_touch_produtosVendidosCliente <<lcNo>>,<<lcEstab>>,'<<lcDataInit>>','<<lcDataFim>>'
		    ENDTEXT 
		    IF !uf_gerais_actGrelha("","ucrsPesqInCac",lcSQLPesq1)
		        uf_perguntalt_chama("OCORREU UM ERRO A PESQUISAR OS HIST�RICO DE PRODUTOS.", "OK", "", 16)
		        RETURN .F.
	    	ENDIF  
	    	
	    	IF(USED("ucrsPesqInCac"))
	    		SELECT DISTINCT REF FROM ucrsPesqInCac  WHERE LEN(ALLTRIM(ref)) = 7 AND uf_gerais_IsNumeric(ALLTRIM(ref)) INTO CURSOR ucrsRefsTemp READWRITE
	    		
	    		IF(USED("uCrsListaInteracaoMed") and USED("ucrsRefsTemp") AND RECCOUNT("ucrsRefsTemp")>0 AND RECCOUNT("uCrsListaInteracaoMed")>0)
				  INSERT INTO uCrsListaInteracaoMed (ref);
   				  SELECT ref FROM ucrsRefsTemp
				ENDIF	    		
	    	ENDIF
	    	 		
   	 	ENDIF
   	 	
   	 	
    	fecha("ucrsRefsTemp")
        fecha("ucrsPesqInCac")
        
        
        SELECT DISTINCT REF FROM uCrsListaInteracaoMed WHERE LEN(ALLTRIM(ref)) = 7 AND uf_gerais_IsNumeric(ALLTRIM(ref)) INTO CURSOR uCrsListaInteracaoMedFinal READWRITE
        fecha("uCrsListaInteracaoMed")
  

        * Se houver mais de um registo, cria o cursor de sa�da e chama a API
        IF USED("uCrsListaInteracaoMedFinal") AND RECCOUNT("uCrsListaInteracaoMedFinal") > 1
            uf_dic_simposium_api("uCrsListaInteracaoMedFinal",1)
        ENDIF
    ENDIF
    
	
	** cursor tem de existir sen�o parte na grelha
	if(!USED("UCRSINTERACCOES"))
		LOCAL lcSQlTempInt
		lcSQlTempInt = ""
		Text to lcSQlTempInt noshow textmerge
			exec up_dicionario_interacoes_simposium '', 0, 0
		ENDTEXT
		uf_gerais_actGrelha("", "uCrsInteraccoes", lcSQlTempInt )	
	ENDIF
	
    
    
     * Fecha os cursores se estiverem abertos
    fecha("uCrsListaInteracaoMed")
    fecha("ucrsTempSimposiumSaida")
    fecha("uCrsListaInteracaoMedFinal")

    RETURN .T.

ENDFUNC

FUNCTION uf_dic_simposium_api_processa_resposta_sucesso
	PARAMETERS   lnTipoPedido, lcTokenPedido
	LOCAL lcSqlInt
	lcSqlInt = ""
	IF(lnTipoPedido!=1)
		RETURN .f.
	ENDIF
	
	
	
	fecha("uCrsInteraccoes")
	
	IF(lnTipoPedido==1)
		
		LOCAL lnNivel
		lnNivel = 0
		
		lnNivel  = uf_gerais_getParameter_site('ADM0000000228', 'NUM', mySite)
		IF(EMPTY(lnNivel))
			lnNivel   = 0
		ENDIF
		IF(lnNivel < 0)
			lnNivel = 0
		ENDIF
			
		Text to lcSqlInt noshow textmerge
				exec up_dicionario_interacoes_simposium '<<ALLTRIM(lcTokenPedido)>>', <<mysite_nr>>, <<lnNivel>>
		ENDTEXT
		IF uf_gerais_actGrelha("", "uCrsInteraccoes", lcSqlInt)
			IF RECCOUNT("uCrsInteraccoes") > 0
				RETURN .T.
			ENDIF 
		ELSE
			MESSAGEBOX("OCORREU UM ERRO A VALIDAR AS INTERAC��ES ENTRE PRODUTOS.",64,"LOGITOOLS SOFTWARE")
			RETURN .F.
		ENDIF
			
			
	ENDIF
	
	
	RETURN .T.
ENDFUNC

FUNCTION uf_dic_simposium_api
    PARAMETERS ucrsCursorEntrada, lnTipoPedido

    LOCAL lcTokenPedido, lcSQLCab, lcSQLLin, lcTipoPedidoDescr, lcNomeCliente, lcTipoCliente, lcNrOrdem, lcWsPath, lcPath, lcwsdir, lcStatus, lcDescr, lcSQLFinal 


    * Inicializa��o de vari�veis
    lcTokenPedido = uf_gerais_stamp()
    lcwsdir = ALLTRIM(uf_gerais_getParameter("ADM0000000185", "TEXT")) + '\Simposium'
    lcNomeJar = 'ltsSimposium.jar'
    lcPath = lcwsdir + '\' + lcNomeJar

    * Valida��o inicial de par�metros
    IF EMPTY(lnTipoPedido)
        regua(2)
        RETURN .F.
    ENDIF

    * Verifica conex�o com a internet
    IF uf_gerais_verifyInternet() == .F.
        regua(2)
        uf_perguntalt_chama("ESTA FUNCIONALIDADE REQUER LIGA��O � INTERNET." + CHR(13) + "POR FAVOR VERIFIQUE A SUA LIGA��O.", "OK", "", 16)
        RETURN .F.
    ENDIF

    * Verifica configura��o do c�digo de farm�cia
    SELECT ucrse1
    IF EMPTY(ALLTRIM(ucrse1.u_codfarm))
        regua(2)
        uf_perguntalt_chama("O C�DIGO INFARMED DA FARM�CIA N�O EST� CORRETAMENTE PREENCHIDO. POR FAVOR VERIFIQUE.", "OK", "", 16)
        RETURN .F.
    ENDIF

    * Valida par�metros do Simposium
    IF EMPTY(ALLTRIM(uf_gerais_getParameter_site('ADM0000000193', 'TEXT', mySite))) OR ;
       EMPTY(ALLTRIM(uf_gerais_getParameter_site('ADM0000000194', 'TEXT', mySite)))
        regua(2)
        uf_perguntalt_chama("Falta preencher o user/password do Simposium.", "OK", "", 16)
        RETURN .F.
    ENDIF

    * Verifica ativa��o da API de intera��es
    IF EMPTY(uf_gerais_getParameter_site('ADM0000000225', 'BOOL', mySite)) AND lnTipoPedido == 1
        regua(2)
        uf_perguntalt_chama("Falta ativar a valida��o de intera��es de medicamentos via API Simposium." + CHR(13) + "POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
        RETURN .F.
    ENDIF

    * Define modo de teste e obt�m dados do cliente
    lctestjar = 0
    lcTipoPedidoDescr = uf_dic_simposium_pedido_tipo_descricao(lnTipoPedido)
    lcNomeCliente = uf_dic_return_nrInfarmed()
    lcTipoCliente = uf_dic_return_tipoCliente()
    lcNrOrdem = uf_dic_return_nrOrdem()
    
        
    if(EMPTY(lcNrOrdem))
    	lcNrOrdem = "00000"
    ENDIF
    

    * Prepara cabe�alho do pedido
    uf_gerais_meiaRegua("A validar intera��es entre medicamentos...")

    LOCAL lcNrAtendimento
    lcNrAtendimento = IIF(VARTYPE(nrAtendimento) == 'C', nrAtendimento, '')

    * Insere cabe�alho na tabela simposiumRequest
    TEXT TO lcSQLCab TEXTMERGE NOSHOW
        INSERT INTO simposiumRequest (token, typeRequest, typeRequestDesc, test, workplace, service, memberId, site, ousrinis, ousrdata, usrinis, usrdata, nrAtend)
        VALUES ('<<ALLTRIM(lcTokenPedido)>>', <<lnTipoPedido>>, '<<ALLTRIM(lcTipoPedidoDescr)>>', <<lctestjar>>, '<<ALLTRIM(lcNomeCliente)>>',
                '<<ALLTRIM(lcTipoCliente)>>', '<<ALLTRIM(lcNrOrdem)>>', '<<ALLTRIM(mysite)>>', '<<ALLTRIM(m_chinis)>>', dateadd(HOUR, <<difhoraria>>, getdate()),
                '<<ALLTRIM(m_chinis)>>', dateadd(HOUR, <<difhoraria>>, getdate()), '<<ALLTRIM(lcNrAtendimento)>>')
    ENDTEXT

    IF !uf_gerais_actgrelha("", "", lcSQLCab)
        regua(2)
        uf_perguntalt_chama("OCORREU UM PROBLEMA AO GUARDAR O REGISTO DE CABE�ALHO DAS INTERA��ES DE MEDICAMENTOS.", "OK", "", 16)
        RETURN .F.
    ENDIF

    * Processa as linhas do cursor de entrada
    SELECT &ucrsCursorEntrada.
    SCAN
        LOCAL lcRef, lcStamp
        lcRef = ALLTRIM(&ucrsCursorEntrada..ref)
        lcStamp = uf_gerais_stamp()

        IF !EMPTY(lcRef)
            TEXT TO lcSQLLin TEXTMERGE NOSHOW
                INSERT INTO simposiumRequestLines (stamp, token, ref, ousrinis, ousrdata, usrinis, usrdata)
                VALUES ('<<ALLTRIM(lcStamp)>>', '<<ALLTRIM(lcTokenPedido)>>', '<<ALLTRIM(lcRef)>>', '<<ALLTRIM(m_chinis)>>', 
                        dateadd(HOUR, <<difhoraria>>, getdate()), '<<ALLTRIM(m_chinis)>>', dateadd(HOUR, <<difhoraria>>, getdate()))
            ENDTEXT

            IF !uf_gerais_actgrelha("", "", lcSQLLin)
                regua(2)
                uf_perguntalt_chama("OCORREU UM PROBLEMA AO GUARDAR REGISTO DAS INTERA��ES DE MEDICAMENTOS.", "OK", "", 16)
                RETURN .F.
            ENDIF
        ENDIF
    ENDSCAN

    * Verifica se o arquivo JAR existe e executa
    IF !FILE(lcPath)
        regua(2)
        uf_perguntalt_chama("FICHEIRO DE CONFIGURA��O N�O ENCONTRADO. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
        RETURN .F.
    ENDIF

    * Executa o comando do JAR
    lcWsPath = 'cmd /c cd /d ' + lcwsdir + ' & javaw -jar "' + lcPath + '" --TOKEN="' + lcTokenPedido + '" --IDCL="' + UPPER(sql_db) + '"'
    oWSShell = CREATEOBJECT("WScript.Shell")
    oWSShell.Run(lcWsPath, 0, .t.)


    * Modo de depura��o
    IF emdesenvolvimento
        _cliptext = lcWsPath
    ENDIF

    * Verifica o status do pedido
    fecha("uCrsInfoSimposiumAux")
    TEXT TO lcSQLFinal TEXTMERGE NOSHOW
        SELECT status, message FROM simposiumResp (nolock) WHERE token = '<<ALLTRIM(lcTokenPedido)>>'
    ENDTEXT

    IF !uf_gerais_actgrelha("", "uCrsInfoSimposiumAux", lcSQLFinal)
        uf_perguntalt_chama("N�o foi poss�vel validar as intera��es entre medicamentos.", "OK", "", 32)
        regua(2)
        RETURN .F.
    ENDIF

    * Verifica status da resposta
    IF USED("uCrsInfoSimposiumAux")
        lcStatus = uCrsInfoSimposiumAux.status
        lcDescr  = ""
        IF(!EMPTY(uCrsInfoSimposiumAux.status))
			lcDescr =  ALLTRIM(uCrsInfoSimposiumAux.message) 
		ELSE
			lcDescr =  "N�o foi poss�vel validar as intera��es entre medicamentos. Por favor contacte o suporte da Logitools."
		ENDIF
		lcDescr  = LEFT(ALLTRIM(lcDescr) ,254)
        DO CASE
            CASE lcStatus == 200
            	uf_dic_simposium_api_processa_resposta_sucesso(lnTipoPedido, lcTokenPedido)
                regua(2)
                
            CASE lcStatus == 204
                regua(2)
                fecha("uCrsInfoSimposiumAux")
                RETURN .T.
            OTHERWISE
                regua(2)
                fecha("uCrsInfoSimposiumAux")
                uf_perguntalt_chama(lcDescr, "OK", "", 32)
                RETURN .F.
        ENDCASE
    ENDIF

    RETURN .T.
ENDFUNC

