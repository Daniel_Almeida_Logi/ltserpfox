DO dic
** Carrega app
FUNCTION uf_servicos_unblockChilkat
	LOCAL loGlob
	LOCAL lnSuccess
	
	loGlob = CreateObject('Chilkat_9_5_0.Global')
	lnSuccess = loGlob.UnlockBundle("LGTLSP.CB1052019_eQLZAyAR9RkB")
	loGlob.DefaultUtf8 = 1
	IF (lnSuccess <> 1) THEN
	    ? loGlob.LastErrorText
	    RELEASE loGlob
	    CANCEL
	ENDIF
	
	RELEASE loGlob

ENDFUNC	





** descarrega um ficheiro
FUNCTION  uf_servicos_download
	LPARAM	lcUrl, lcDirFile
	
	
	LOCAL loReq
	LOCAL loHttp
	LOCAL lnSuccess



	uf_servicos_unblockChilkat()	

		
	loReq = CreateObject('Chilkat_9_5_0.HttpRequest')
	loHttp = CreateObject('Chilkat_9_5_0.Http')

	
		
	* Download an XML file:
	* To download using SSL/TLS, simply use "https://" in the URL.
	lnSuccess = loHttp.Download(lcUrl,lcDirFile)
	IF (lnSuccess <> 1) THEN
	    RELEASE loHttp
	ENDIF
		
	RETURN lnSuccess 
	
	
ENDFUNC




** Valida SignIn JWT
FUNCTION uf_servicos_enviaJson

	LPARAM	lcUrl, lcCursor, lcToken
	
	
	LOCAL loReq
	LOCAL loHttp
	LOCAL lnSuccess, lnSuccess1
	LOCAL lcJsonText
	LOCAL loResp
	LOCAL lcToken

	uf_servicos_unblockChilkat()	

	
	loReq = CreateObject('Chilkat_9_5_0.HttpRequest')
	loHttp = CreateObject('Chilkat_9_5_0.Http')


	loHttp.AcceptCharset = ""
	loHttp.UserAgent = ""
	loHttp.AcceptLanguage = ""
	*  Suppress the Accept-Encoding header by disallowing
	*  a gzip response:
	loHttp.AllowGzip = 0

	
	*  AddQuickHeader:
	lnSuccess = loHttp.AddQuickHeader("Content-Type","application/json")
	**lnSuccess1 = loHttp.SetRequestHeader("x-auth-token",lcToken)
	lnSuccess1 = loHttp.SetRequestHeader("o-auth-token",lcToken)

**	lcJsonText = '{"email":"cartoes@lts.pt","password":"lts.cartoes%&_123","rememberMe":true}'
	lcJsonText  = uf_servicos_criaObjectoJson(lcCursor)

	loResp = loHttp.PostJson(ALLTRIM(lcUrl),lcJsonText)
	IF (ISNULL(loResp)) THEN
	     MESSAGEBOX(loHttp.LastErrorText)
	ELSE
	    *  Display the JSON response..
	    **MESSAGEBOX(loResp.BodyStr)
	    lcCursor = uf_servicos_criaObjectoCursor(loResp.BodyStr)
	    RELEASE loResp
	ENDIF

	


ENDFUNC


** Valida se api est� live
FUNCTION uf_servicos_validaEstadoApi
	LPARAM lcEndpoint
		
	
	LOCAL loReq
	LOCAL loHttp
	LOCAL lnSuccess
	LOCAL loResp

	loHttp = CreateObject('Chilkat_9_5_0.Http')


	uf_servicos_unblockChilkat()	


	*  Send the HTTP GET and return the content in a string.

	lcHtml = loHttp.QuickGetStr(lcEndpoint)
	
	IF (loHttp.LastMethodSuccess <> 1) THEN
	    loReq = .t.
	ENDIF


	if(len(alltrim(lcHtml)))>0
		loReq = .t.
	else
		loReq = .f.
	endif
	
	RETURN loReq 
	
ENDFUN



** Valida SignIn JWT
FUNCTION uf_servicos_signInJwt
	LPARAM	lcUrl, lcCursor
	
	
	LOCAL loReq
	LOCAL loHttp
	LOCAL lnSuccess
	LOCAL lcJsonText
	LOCAL loResp
	LOCAL lcToken

	uf_servicos_unblockChilkat()	

	
	loReq = CreateObject('Chilkat_9_5_0.HttpRequest')
	loHttp = CreateObject('Chilkat_9_5_0.Http')


	loHttp.AcceptCharset = ""
	loHttp.UserAgent = ""
	loHttp.AcceptLanguage = ""
	*  Suppress the Accept-Encoding header by disallowing
	*  a gzip response:
	loHttp.AllowGzip = 0

	
	*  AddQuickHeader:
	lnSuccess = loHttp.AddQuickHeader("Content-Type","application/json")

**	lcJsonText = '{"email":"cartoes@lts.pt","password":"lts.cartoes%&_123","rememberMe":true}'
	lcJsonText  = uf_servicos_criaObjectoJson(lcCursor)

	loResp = loHttp.PostJson(ALLTRIM(lcUrl),lcJsonText)
	IF (ISNULL(loResp)) THEN
	     MESSAGEBOX(loHttp.LastErrorText)
	ELSE
	    *  Display the JSON response..
	    **MESSAGEBOX(loResp.BodyStr)
	    lcCursor = uf_servicos_criaObjectoCursor(loResp.BodyStr)
	    RELEASE loResp
	ENDIF

	RELEASE loReq
	RELEASE loHttp

ENDFUNC

FUNCTION uf_chilkat_generate_key
	LOCAL  loFortuna, lnBDigits, lnBLowercase,	lnBUppercase, lcGeneratedKey
	
	uf_servicos_unblockChilkat()

	loFortuna = CreateObject('Chilkat_9_5_0.Prng')

	lnBDigits = 1
	lnBUppercase = 1
	lnBLowercase = 1

	lcGeneratedKey = ALLTRIM(loFortuna.RandomString(20,lnBDigits,lnBLowercase,lnBUppercase))

	RELEASE loChilkatGlob
	RELEASE loFortuna
	
	return(lcGeneratedKey)
	
ENDFUNC 


&& VERIFICA SE J� EXISTE REGISTO CRIADO NA TABELA service_credentials . SE N�O EXISTE CRIA O MESMO NA BD E NA BD DO ROUTER
FUNCTION uf_verifica_cria_token_existe
	
	** SE FOR PORTAL NAO VALIDA
	if(ALLTRIM(lcTipoEntrada) == "PORTAL") THEN
		return(.t.)
	ENDIF
		
	TEXT TO lcSQL NOSHOW TEXTMERGE
		select * from service_credentials where service='Logitools' and site_nr= <<mySite_nr>>
	ENDTEXT  
	uf_gerais_actgrelha("", "uCrsVerifToken", lcSQL)
	IF RECCOUNT("uCrsVerifToken")>0
		return(.t.)
	ELSE
		LOCAL lcTokenResponse, lcUsedKey, lcINSResponse
		STORE '' TO lcTokenResponse
		
		lcUsedKey = uf_chilkat_generate_key()
		TEXT TO lcSQL NOSHOW TEXTMERGE
			OPEN SYMMETRIC KEY UPWD_Key_01
			DECRYPTION BY CERTIFICATE userpassword;

			insert into service_credentials (stamp, service, username, email, encryptpassword, token, site_nr, usrdata, ousrdata, password)
			select 
				LEFT(NEWID(),21)
				, 'Logitools'
				, '<<ALLTRIM(uCrsE1.id_lt)>>'+'@logitools.pt'
				, '<<ALLTRIM(uCrsE1.id_lt)>>'+'@logitools.pt'
				, EncryptByKey(Key_GUID('UPWD_Key_01'), '<<ALLTRIM(lcUsedKey)>>')
				,'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3f'
				,<<mySite_nr>>
				,GETDATE()
				,GETDATE()
				,''
			from 
				empresa 
			where 
				no = <<mySite_nr>>
				
			close SYMMETRIC KEY UPWD_Key_01
		ENDTEXT  

		uf_gerais_actgrelha("", "", lcSQL)

		**LOCAL lcMensagemTk, lcMensagemIns

		**lcMensagemIns = '{"firstName": "' + ALLTRIM(ucrse1.nomabrv) + '","lastName": "' + ALLTRIM(ucrse1.siteloja) + '", "email": "' + ALLTRIM(uCrsE1.id_lt) + '@logitools.pt",	"password": "' + ALLTRIM(lcUsedKey) + '"}'
		**lcINSResponse = uf_chilkat_send_request(myServicosSite+'auth/signUp', lcMensagemIns, 'INS_FARM_BD_ROUTER')
		
		**MESSAGEBOX('envia')
		**MESSAGEBOX(lcMensagemIns )
		**MESSAGEBOX(lcINSResponse )
		
		**lcMensagemTk= '{"email": "' + ALLTRIM(uCrsE1.id_lt) + '@logitools.pt", "password": "' + ALLTRIM(lcUsedKey) + '", "rememberMe": true}'
		**lcTokenResponse = uf_chilkat_send_request(myServicosSite+'auth/signIn', lcMensagemTk, 'TOKEN')

		**TEXT TO lcSQL NOSHOW TEXTMERGE
		**	update service_credentials set token='<<ALLTRIM(lcINSResponse )>>' where service='Logitools' and site_nr=<<mySite_nr>>
		**ENDTEXT  
		**uf_gerais_actgrelha("", "", lcSQL)
	ENDIF 

ENDFUNC 



FUNCTION uf_chilkat_send_request
	PARAMETERS lcPostHTTP, lcText, lcTipo
	LOCAL loReq, loHttp, lnSuccess,	lnSuccess1,	lcJsonText,	loResp

	uf_servicos_unblockChilkat()
	
	loReq = CreateObject('Chilkat_9_5_0.HttpRequest')
	loHttp = CreateObject('Chilkat_9_5_0.Http')

	loJson = CreateObject('Chilkat_9_5_0.JsonObject')

	loHttp.AcceptCharset = ""
	loHttp.UserAgent = ""
	loHttp.AcceptLanguage = ""
	* Suppress the Accept-Encoding header by disallowing 
	* a gzip response:
	loHttp.AllowGzip = 0
	


	lnSuccess = loHttp.AddQuickHeader("Content-Type","application/json")

	**lcJsonText = '{"email": "cartoes@lts.pt", "password": "lts.cartoes%&_123", "rememberMe": true}'
	**_cliptext=lcText
	**MESSAGEBOX('wait')
	loResp = loHttp.PostJson(lcPostHTTP,lcText)
	**loResp = loHttp.PostJson("https://app.lts.pt:50001/api/auth/signIn",lcJsonText)
	IF (loHttp.LastMethodSuccess <> 1) THEN
	    MESSAGEBOX(loHttp.LastErrorText)
	ELSE
	    * Display the JSON response.
	    **MESSAGEBOX(loResp.BodyStr)
	    **_cliptext = loResp.BodyStr
	    **RELEASE loResp	
	ENDIF

	RELEASE loReq
	RELEASE loHttp
	IF ALLTRIM(lcTipo) == 'TOKEN'
		loJson.Load(loResp.BodyStr)
		loJson.EmitCompact = 0
		lcRev = loJson.ObjectOf("status")
	**	messagebox(lcRev.StringOf("i"))
	**	messagebox(lcRev.StringOf("t"))
		return(lcRev.StringOf("t"))
	ENDIF 
	IF ALLTRIM(lcTipo) == 'INS_FARM_BD_ROUTER' OR ALLTRIM(lcTipo) == 'VALIDLOGIN'
		loJson.Load(loResp.BodyStr)
		loJson.EmitCompact = 0
		lcRev = loJson.ObjectOf("status")
		return(lcRev.StringOf("t"))
	ENDIF 
	
	RELEASE loResp
	
ENDFUNC 


&& PEDIDO DE NOVO TOKEN QUANDO D� AUTENTICA��O INV�LIDA
FUNCTION uf_pede_novo_token

	LOCAL lcTokenResponse, lcPassword
	STORE '' TO lcTokenResponse, lcPassword
	
	TEXT TO lcSQL NOSHOW TEXTMERGE
		OPEN SYMMETRIC KEY UPWD_Key_01
		DECRYPTION BY CERTIFICATE userpassword;
		
		select 
			service
			, username
			, password=CONVERT(varchar(50), DecryptByKey(Encryptpassword))
		from 
			service_credentials (nolock)
		where
			service='Logitools'
			and username='<<ALLTRIM(uCrsE1.id_lt)>>@logitools.pt'
		close SYMMETRIC KEY UPWD_Key_01
	ENDTEXT  
	uf_gerais_actgrelha("", "curPassw", lcSQL)
	lcPassword = ALLTRIM(curPassw.password)

	LOCAL lcMensagemTk, lcMensagemIns

	lcMensagemTk= '{"email": "' + ALLTRIM(curPassw.username) + '", "password": "' + ALLTRIM(lcPassword) + '", "rememberMe": true}'
	lcTokenResponse = uf_chilkat_send_request(myServicosSite+'auth/signIn', lcMensagemTk, 'TOKEN')

	TEXT TO lcSQL NOSHOW TEXTMERGE
		update service_credentials set token='<<ALLTRIM(lcTokenResponse)>>' where service='Logitools' and site_nr=<<mySite_nr>>
	ENDTEXT  
	uf_gerais_actgrelha("", "", lcSQL)

ENDFUNC 


FUNCTION uf_servicos_getJsonVacinacao
	LPARAMETERS lcStamp, lcCode, lcRef
	
	LOCAL lcNome,lcAssoc,lcId,lcSendMsg, lcProfId
	STORE '' TO lcNome,lcAssoc,lcId,lcSendMsg, lcProfId	
	
	if(USED("ucrse1"))
	SELECT ucrse1
	GO TOP
		lcNome = ALLTRIM(ucrse1.nomAbrv)
		lcAssoc= ALLTRIM(ucrse1.u_assfarm)
		lcId= ALLTRIM(ucrse1.id_lt)
		lcInfarmed = ucrse1.u_infarmed
	ENDIF
			
	SELECT uCrsVacinacao
	GO TOP
	

	lcSendMsg = '{'
	lcSendMsg = lcSendMsg  + '"vaccine": {'

	lcSendMsg = lcSendMsg + '"token": "'+ALLTRIM(uf_gerais_stamp())+'",'
	
	lcSendMsg = lcSendMsg + '"entity": {'
	IF !EMPTY(myServicosEntApp )
		lcSendMsg = lcSendMsg + '"app": "'+ALLTRIM(myServicosEntApp)+'",'
	ENDIF 
	IF !EMPTY(myServicosEntId)
		lcSendMsg = lcSendMsg + '"id": "'+myServicosEntId+'",'
	ENDIF 
	IF !EMPTY(myServicosEntName)
		lcSendMsg = lcSendMsg + '"name": "'+ALLTRIM(myServicosEntName)+'"'
	ENDIF 
	lcSendMsg = lcSendMsg + '},'
	lcSendMsg = lcSendMsg + '"vaccineDetails": {'
	
	SELECT uCrsVacinacao
	IF !EMPTY(uCrsVacinacao.nrSns)
		lcSendMsg = lcSendMsg + '"sns": '+ALLTRIM(uCrsVacinacao.nrSns)
	ENDIF 
	IF !EMPTY(lcStamp)
		lcSendMsg = lcSendMsg + ',"key": "'+lcStamp+'"'
	ENDIF 
	IF !EMPTY(lcCode)
		lcSendMsg = lcSendMsg + ',"vaccineCode": "'+lcCode+'"'
	ENDIF 
	
	IF !EMPTY(lcCode)
		lcSendMsg = lcSendMsg + ',"type": '+ALLTRIM(uCrsVacinacao.typeVacinacao)
	ENDIF 
	
		
	IF !EMPTY(uCrsVacinacao.dataAdministracao)
		lcSendMsg = lcSendMsg + ',"date": "'+uf_gerais_getDate(uCrsVacinacao.dataAdministracao,"PRESCRSP")+ ' ' + TIME() + '"' 
	ENDIF 
	
		
	IF !EMPTY(ALLTRIM(lcRef))
		lcSendMsg = lcSendMsg + ',"infarmedCode": "'+ALLTRIM(lcRef)+'"'
	ENDIF
		
	IF !EMPTY(uCrsVacinacao.dosagem)
		lcSendMsg = lcSendMsg + ',"dosage": '+ALLTRIM(STR(uCrsVacinacao.dosagem,10,2))
	ENDIF 
	
			
	IF !EMPTY(uCrsVacinacao.unidades)
		lcSendMsg = lcSendMsg + ',"units": '+ALLTRIM(STR(uCrsVacinacao.unidades,10,2))
	ENDIF 
		
	IF !EMPTY(ALLTRIM(uCrsVacinacao.lote))
		lcSendMsg = lcSendMsg + ',"lot": "'+ALLTRIM(uCrsVacinacao.lote) +'"'
	ENDIF 
	
	IF !EMPTY(ALLTRIM(uCrsVacinacao.localAnatomico_id))
		lcSendMsg = lcSendMsg + ',"anatomicLocationID": '+ALLTRIM(uCrsVacinacao.localAnatomico_id)
	ENDIF 
	
	IF !EMPTY(ALLTRIM(uCrsVacinacao.viaAdmin_id))
		lcSendMsg = lcSendMsg + ',"administrationWayID": '+ALLTRIM(uCrsVacinacao.viaAdmin_id)
	ENDIF 
	
	IF !EMPTY(ALLTRIM(uCrsVacinacao.obs))
		lcSendMsg = lcSendMsg + ',"obs": "'+ALLTRIM(uCrsVacinacao.obs)+'"'
	ENDIF 
		
	IF !EMPTY(lcInfarmed)
		lcSendMsg = lcSendMsg + ',"locationCd": '+ALLTRIM(STR(lcInfarmed))
	ENDIF
		
	IF !EMPTY(ALLTRIM(lcNome))
		lcSendMsg = lcSendMsg + ',"location": "'+ALLTRIM(lcNome)+'"'
	ENDIF
	
	IF !EMPTY(ALLTRIM(uCrsVacinacao.administrante))
		lcSendMsg = lcSendMsg + ',"professional": "'+ALLTRIM(uCrsVacinacao.administrante)+'"'
	ENDIF
	
	IF !EMPTY(ALLTRIM(uCrsVacinacao.typeUtente))
		lcSendMsg = lcSendMsg + ',"isProfessional":  '+ALLTRIM(uCrsVacinacao.typeUtente)
	ENDIF
	
	IF !EMPTY(ALLTRIM(ucrsuser.drcedula))
		lcProfId  = uf_gerais_removeAllLeterFromString(ucrsuser.drcedula)
		lcSendMsg = lcSendMsg + ',"ProfessionalCd": "'+ALLTRIM(lcProfId)+'"'
	ENDIF
	
	IF !EMPTY(ALLTRIM(uCrsVacinacao.localAdmistracao))
		lcSendMsg = lcSendMsg + ',"locationAdmID": '+ALLTRIM(uCrsVacinacao.localAdmistracao)
	ENDIF
	
	IF !EMPTY(ALLTRIM(uCrsVacinacao.numInoculacao))
		lcSendMsg = lcSendMsg + ',"inocNumber": '+ALLTRIM(uCrsVacinacao.numInoculacao)
	ENDIF
		
	IF !EMPTY(ALLTRIM(uCrsVacinacao.localAnatomico_id))
		lcSendMsg = lcSendMsg + ',"anatomicLocationID": '+ALLTRIM(uCrsVacinacao.localAnatomico_id)
	ENDIF
	
	IF !EMPTY(ALLTRIM(uCrsVacinacao.lateralidade))
		lcSendMsg = lcSendMsg + ',"sideId": '+ALLTRIM(uCrsVacinacao.lateralidade)
	ENDIF
		
	lcSendMsg = lcSendMsg + '},'
	

	lcSendMsg = lcSendMsg + '"sender": {'
	lcSendMsg = lcSendMsg + '"id": "'+ALLTRIM(lcId)+'",'
	lcSendMsg = lcSendMsg + '"name": "'+ALLTRIM(lcNome )+'",'
	lcSendMsg = lcSendMsg + '"assoc": "'+ALLTRIM(lcAssoc)+'",'
	lcSendMsg = lcSendMsg + '"app": "'+ALLTRIM(myServicosEntApp)+'"'
	lcSendMsg = lcSendMsg + '}'
	lcSendMsg = lcSendMsg + '}'
	lcSendMsg = lcSendMsg + '}'

	RETURN lcSendMsg 

ENDFUNC




&& 0 - nao valida vacina
&& -1 - vacina comunicada com erro
&& 1  - vacina comunicada com sucesso


FUNCTION uf_servicos_enviaRegistoVacinacao
	LPARAMETERS lcStamp
	
	LOCAL lcRef,lcVacCode, lcRefVac, lcResultEnvio
	STORE "" TO lcRef ,lcVacCode, lcRefVac
	
	lcResultEnvio = 0
	
	if(!USED("uCrsVacinacao"))
		RETURN  0
	ENDIF

	SELECT uCrsVacinacao
	GO TOP
	lcRef  = ALLTRIM(uCrsVacinacao.comercial_id)
	lcResultEnvio  = uCrsVacinacao.result

	if(lcResultEnvio==1)
		RETURN  0
	ENDIF
	
	
	
	fecha("ucrsInfoVacTemp")
	
	TEXT TO lcSQL NOSHOW TEXTMERGE
		select TOP 1 ISNULL(vaccine,0) as vac, isnull(vaccineCode,'') as vacCode, ref from st(nolock)  where ref ='<<lcRef>>' and site_nr= <<mySite_nr>>
	ENDTEXT  
	
	uf_gerais_actgrelha("", "ucrsInfoVacTemp", lcSQL)
	IF RECCOUNT("ucrsInfoVacTemp")==0
		RETURN  0
	ELSE
		SELECT ucrsInfoVacTemp
		GO TOP
		lcVacCode  = ALLTRIM(ucrsInfoVacTemp.vacCode) 
		lcIsVac    = ucrsInfoVacTemp.vac
		lcRefVac   = ALLTRIM(ucrsInfoVacTemp.ref) 
		
		if(EMPTY(lcIsVac))
			RETURN  0
		ENDIF
	
	ENDIF
	

	
	fecha("ucrsInfoVacTemp")
	
	
	
	LOCAL loReq, loHttp, lnSuccess,	lnSuccess1,	lcJsonText,	loResp, lcToken, lcPostHTTP
	
	uf_gerais_meiaRegua("A comunicar registo de vacina��o...")
	
	
	lcToken = 'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3f'
	
	
	IF uf_gerais_getparameter("ADM0000000227", "BOOL")==.T.
       lcPostHTTP = "https://app.lts.pt:50001/api/afp/docs/vaccines-test"
    ELSE
       lcPostHTTP = "https://app.lts.pt:50001/api/afp/docs/vaccines"
    ENDIF
    
    IF uf_gerais_getparameter("ADM0000000227", "BOOL")==.T.
       uf_perguntalt_chama("Webservice de teste...", "OK", "", 64)
    ENDIF
	
	

	uf_servicos_unblockChilkat()
	
	loReq = CreateObject('Chilkat_9_5_0.HttpRequest')
	loHttp = CreateObject('Chilkat_9_5_0.Http')

	loJson = CreateObject('Chilkat_9_5_0.JsonObject')
	
	loHttp.AcceptCharset =  "utf-8, ISO-8859-1;q=0.7,*;q=0.7"
	loHttp.UserAgent = ""
	loHttp.AcceptLanguage = ""
	* Suppress the Accept-Encoding header by disallowing 
	* a gzip response:
	loHttp.AllowGzip = 0
	loHttp.ConnectTimeout  = 10
	loHttp.ReadTimeout = 30
	
	*  AddQuickHeader:
	lnSuccess = loHttp.AddQuickHeader("Content-Type","application/json; charset=utf-8")
	lnSuccess1 = loHttp.SetRequestHeader("o-auth-token",lcToken)

	
	LOCAL lcText
	lcText = ""
	

	lcText  = uf_servicos_getJsonVacinacao(ALLTRIM(lcStamp),ALLTRIM(lcVacCode),ALLTRIM(lcRefVac))
		
	loResp = loHttp.PostJson2(lcPostHTTP,"application/json; charset=utf-8",lcText)
		
		

	IF (loHttp.LastMethodSuccess <> 1) THEN
		    lcBody = ALLTRIM(loHttp.LastErrorText)
		    LOCAL lcHeader
		    lcHeader = ""
		    lcStatusCode= 0

	    	if(USED("uCrsE1"))
	    		SELECT uCrsE1
	    		GO TOP
	    		lcHeader = "Chilkat vacinacao : " + ALLTRIM(uCrsE1.id_lt)
	    	ENDIF
		    	
			uf_startup_sendmail_errors(lcHeader ,lcBody)
			
			RELEASE loReq
			RELEASE loHttp
			RELEASE loJson 
			
			regua(2)
			
			uf_perguntalt_chama("ERRO COMUNICA��O VACINACAO!" ,"OK","", 16)	
			
			RETURN -1		
	ELSE
		lcResponseHeaderCode = loResp.StatusCode
		LOCAL lcResp 
		lcResp = .f.
		LOCAL lcReturnI ,lcReturnS  
		
		lcReturnI = ""
		lcReturnS = ""
		
		regua(2)
		
		
			
		if(EMPTY(loResp.BodyStr))
			RETURN -1
		ENDIF
		
	
		
		loJson.Load(loResp.BodyStr)
		loJson.EmitCompact = 0

		IF(uf_servicos_chilkat_isNull(loJson.ObjectOf("status")))
			uf_perguntalt_chama("ERRO COMUNICA��O VACINACAO!" ,"OK","", 16)	
			RETURN -1
		ELSE		
			lcRev = loJson.ObjectOf("status")
			lcReturnI = (lcRev.StringOf("i")) 
			lcReturnS = (lcRev.StringOf("s"))
	
		ENDIF
		
		IF  lcResponseHeaderCode =200					
			IF(lcReturnI=="200")
				lcResp = 1
			ELSE
				uf_perguntalt_chama(LEFT("ERRO COMUNICA��O VACINACAO: " + ALLTRIM(lcReturnS),254) ,"OK","", 16)	
				lcResp = -1	
			ENDIF
			
		ELSE
			uf_perguntalt_chama(LEFT("ERRO NA LIGA��O DA COMUNICA��O DA VACINACAO: " + ALLTRIM(lcReturnS),254) ,"OK","", 16)	
			lcResp = -1	
		ENDIF 
		
			
		RELEASE loReq
		RELEASE loHttp
		RELEASE loJson 
		
		RETURN lcResp 
	
	ENDIF
	
	
	RETURN 0
	
ENDFUNC

FUNCTION uf_single_pack_createCursorResponse
	PARAMETER lcLang
	LOCAL lcSQLMvoDesc
	STORE "" TO lcSQLMvoDesc
	
	IF !USED("ucrsMvoDesc")
		TEXT TO lcSQLMvoDesc NOSHOW TEXTMERGE
			exec up_get_mvoTranslation '','<<ALLTRIM(lcLang)>>'
		ENDTEXT
	
		uf_gerais_actGrelha("","ucrsMvoDesc",lcSQLMvoDesc)
	ENDIF
	
ENDFUNC

FUNCTION uf_single_pack_translateResponse
	LPARAMETERS lcCode, lcCountry, lcOriResponse
	LOCAL lcFinalResponse
	
	lcFinalResponse = ALLTRIM(lcOriResponse)
	
	IF !USED("ucrsMvoDesc")
		uf_single_pack_createCursorResponse('PT')
	ENDIF
	
	IF EMPTY(ALLTRIM(lcCode)) OR EMPTY(ALLTRIM(lcCountry))
		RETURN lcFinalResponse
	ENDIF
	
	IF USED("ucrsMvoDesc")
		SELECT ucrsMvoDesc
		GO TOP
		SCAN FOR UPPER(ALLTRIM(ucrsMvoDesc.code)) == UPPER(ALLTRIM(lcCode)) AND UPPER(ALLTRIM(ucrsMvoDesc.language)) == UPPER(ALLTRIM(lcCountry))
			lcFinalResponse = ALLTRIM(ucrsMvoDesc.descr)
		ENDSCAN
	ENDIF
	
	RETURN lcFinalResponse
ENDFUNC

&& FUN��O PARA OS PEDIDOS SINGLE PACK
FUNCTION uf_single_pack_requests
	PARAMETERS lcTipo, lcPostHTTP, lcText, lcSentBatchid, lcSentStamp, LcTokenReq



	**uf_perguntalt_responsivo_chama(lcPostHTTP,"OK","", 16)

	LOCAL loReq, loHttp, lnSuccess,	lnSuccess1,	lcJsonText,	loResp
	
	uf_single_pack_createCursorResponse('PT')
	
	**IF ALLTRIM(ucrse1.u_assfarm)=='AFP'
		uf_servicos_unblockChilkat()
		
		loReq = CreateObject('Chilkat_9_5_0.HttpRequest')
		loHttp = CreateObject('Chilkat_9_5_0.Http')

		loJson = CreateObject('Chilkat_9_5_0.JsonObject')

		loHttp.UserAgent = ""
    	loHttp.AcceptLanguage = ""
    	loHttp.AcceptCharset =  "utf-8, ISO-8859-1;q=0.7,*;q=0.7"

		loHttp.AllowGzip = 0
		
		** Timeout
		** ConnectTimeout : Tempo que  demora a connectar ao servidor 
		** ReadTimeout : Tempo que demora a receber a resposta do servidor, deve variar por servico: single -> 10 segundos, mixed bulk -> para ai uns 10 minutos
		loHttp.ConnectTimeout  = 15
	  	loHttp.ReadTimeout = 20

	   	**lnSuccess = loHttp.AddQuickHeader("x-auth-token",LcTokenReq)
	   	**lnSuccess = loHttp.AddQuickHeader("Content-Type","application/json; charset=utf-8")
	  	lnSuccess1 = loHttp.AddQuickHeader("o-auth-token",LcTokenReq)
	  	
	  	lcLocation  = ALLTRIM(uf_gerais_getparameter("ADM0000000015", "TEXT"))+"\Logs\"	
	  	  	 	
		**uf_gerais_saveLog(lcLocation,"concentrador.txt","request: " + ALLTRIM(lcText),"INF")
		
		&&pedido
		&&MESSAGEBOX(lcPostHTTP)

		loResp = loHttp.PostJson2(lcPostHTTP,"application/json; charset=utf-8",lcText)
		
		LOCAL lcFistampTransInfo
		

		if(USED("fi_trans_info"))
		
		
			
			SELECT fi_trans_info		
			lcFistampTransInfo = ALLTRIM(fi_trans_info.recStamp)
			replace fi_trans_info.reqType WITH ALLTRIM(lcTipo)
		
			
		ELSE
			lcFistampTransInfo  = ""	
		ENDIF
		
	

		IF (loHttp.LastMethodSuccess <> 1) THEN
		
			**uf_perguntalt_chama("OCORREU UM ERRO NA COMUNICA��O COM O CONCENTRADOR! POR FAVOR CONTACTE O SUPORTE!","OK","", 16)
            **_cliptext=loHttp.LastErrorText
		    **MESSAGEBOX(loHttp.LastErrorText)

		    lcBody = ALLTRIM(loHttp.LastErrorText)
		    LOCAL lcHeader
		    lcHeader = ""
		  
	    	if(USED("uCrsE1"))
	    		SELECT uCrsE1
	    		GO TOP
	    		lcHeader = "Chilkat : " + ALLTRIM(uCrsE1.id_lt)
	    	ENDIF

		    	
			uf_startup_sendmail_errors(lcHeader ,lcBody)
			uf_servicos_store_bulk(ALLTRIM(lcTipo),lcFistampTransInfo)
			return(.f.)
			
		ELSE
			

		
			LOCAL lcReturnI, lcReturnS, lcNmvsTrxId, lcState, lcReasons, lcCode, lcDescription, lcReturnCode, lcResponseHeaderCode, lcNmvsTimeStamp,;
				  lcBatchIdReturn, lcBatchExpiryDateReturn, lcProductCodeReturn, lcProductNameReturn, lcProductCommonNameReturn, lcSnReturn,;
				  lcUndoPossibleReturn, lcUserMatchesReturn, lcProductNhrnReturn, lcAlertIdReturn
			STORE '' to lcReturnI, lcReturnS, lcNmvsTrxId, lcState, lcReasons, lcCode, lcDescription, lcReturnCode, lcResponseHeaderCode, lcNmvsTimeStamp,;
						lcBatchIdReturn, lcBatchExpiryDateReturn, lcProductCodeReturn, lcProductNameReturn, lcProductCommonNameReturn, lcSnReturn,;
				  		lcProductNhrnReturn, lcAlertIdReturn

			STORE .F. TO lcUserMatchesReturn, lcUndoPossibleReturn

			lcResponseHeaderCode = loResp.StatusCode
			
			if(EMPTY(loResp.BodyStr))
				uf_servicos_store_bulk(ALLTRIM(lcTipo),lcFistampTransInfo  )
				RETURN .f.
			ENDIF
			

			IF(VARTYPE(loJson.ObjectOf("status"))=='U')
				uf_servicos_store_bulk(ALLTRIM(lcTipo),lcFistampTransInfo)
				RETURN .f.
			ENDIF
			
		
		
			if(USED("nrserie_com_results"))
			    SELECT nrserie_com_results
			    GO BOTTOM 
			    APPEND BLANK
			    replace nrserie_com_results.type WITH ALLTRIM(lcTipo)
				replace nrserie_com_results.nrserie WITH ALLTRIM(lcSentBatchid)
				replace nrserie_com_results.source_table WITH 'FI'
				replace nrserie_com_results.source_stamp WITH ALLTRIM(lcSentStamp)
				replace nrserie_com_results.sent_string WITH lcText
				replace nrserie_com_results.response_string WITH loResp.BodyStr
				replace nrserie_com_results.ousrinis WITH ALLTRIM(ch_vendnm)
				replace nrserie_com_results.ousrdata WITH DATETIME()
			ENDIF
			
		    RELEASE loReq
			RELEASE loHttp
			loJson.Load(loResp.BodyStr)
			loJson.EmitCompact = 0

			
			IF  lcResponseHeaderCode !=200
				**uf_perguntalt_chama("OCORREU UMA FALHA NA COMUNICA��O COM O CONCENTRADOR! POR FAVOR CONTACTE O SUPORTE!","OK","", 16)
				uf_servicos_store_bulk(ALLTRIM(lcTipo),lcFistampTransInfo  )				
				return(.f.)
			ENDIF 
			
			
			IF !(VARTYPE(loJson.ObjectOf("status"))=='U')
				lcRev = loJson.ObjectOf("status")
				lcReturnI = (lcRev.StringOf("i")) 
				lcReturnS = (lcRev.StringOf("s"))
			ELSE
				uf_servicos_store_bulk(ALLTRIM(lcTipo), lcFistampTransInfo)
				RETURN .f.	
			ENDIF 		
						
			IF !(ISNULL(loJson.ObjectOf("transaction")))
				IF !(ISNULL(loJson.ObjectOf("transaction")))
					lcRev1 = loJson.ObjectOf("transaction")
					IF !(ISNULL("lcRev1"))
						IF !(ISNULL(lcRev1.StringOf("c")))
							lcNmvsTrxId = (lcRev1.StringOf("nmvsTrxId"))
						ENDIF 
						IF !(ISNULL(lcRev1.StringOf("code")))
							lcCode = (lcRev1.StringOf("code"))
						ENDIF 
						IF !(ISNULL(lcRev1.StringOf("description")))
							lcDescription = uf_single_pack_translateResponse(lcCode, 'PT', lcRev1.StringOf("description"))										
						ENDIF 
						IF !(ISNULL(lcRev1.StringOf("code")))
							lcReturnCode= (lcRev1.StringOf("code"))
						ENDIF 
						IF !(ISNULL(lcRev1.StringOf("nmvsTimeStamp")))
							lcNmvsTimeStamp = (lcRev1.StringOf("nmvsTimeStamp"))
						ENDIF 

						IF !(ISNULL(lcRev1.StringOf("batchId")))
							lcBatchIdReturn = (lcRev1.StringOf("batchId"))
						ENDIF 

						IF !(ISNULL(lcRev1.StringOf("batchExpiryDate")))
							lcBatchExpiryDateReturn = (lcRev1.StringOf("batchExpiryDate"))
						ENDIF 

						IF !(ISNULL(lcRev1.StringOf("productCode")))
							lcProductCodeReturn = (lcRev1.StringOf("productCode"))
						ENDIF 

						IF !(ISNULL(lcRev1.StringOf("productName")))
							lcProductNameReturn = (lcRev1.StringOf("productName"))
						ENDIF 

						IF !(ISNULL(lcRev1.StringOf("productCommonName")))
							lcProductCommonNameReturn = (lcRev1.StringOf("productCommonName"))
						ENDIF 

						IF !(ISNULL(lcRev1.StringOf("productNhrn")))
							lcProductNhrnReturn = (lcRev1.StringOf("productNhrn"))
						ENDIF 

						IF !(ISNULL(lcRev1.StringOf("alertId")))
							lcAlertIdReturn = (lcRev1.StringOf("alertId"))
						ENDIF 

						IF !(ISNULL(lcRev1.ObjectOf("pack")))
							lcRev2 = lcRev1.ObjectOf("pack")
							IF !(ISNULL(lcRev2.StringOf("state")))
								lcState = (lcRev2.StringOf("state"))
							ENDIF 
							IF !(ISNULL(lcRev2.ArrayOf("reasons")))
								loArr1 = (lcRev2.ArrayOf("reasons"))
								lcReasons = ''
								lnSzArr1 = loArr1.Size
								FOR i = 0 TO lnSzArr1 - 1
								    lcReasons = lcReasons + " " + loArr1.StringAt(i)
								NEXT
								RELEASE loArr1
							ENDIF

							IF !(ISNULL(lcRev2.StringOf("sn")))
								lcSnReturn = (lcRev2.StringOf("sn"))
							ENDIF 

							IF !(ISNULL(lcRev2.StringOf("undoPossible")))
								lcUndoPossibleReturn = IIF(uf_gerais_compStr((lcRev2.StringOf("undoPossible")), "true"), .T., .F.)
							ENDIF 

							IF !(ISNULL(lcRev2.StringOf("userMatches")))
								lcUserMatchesReturn = IIF(uf_gerais_compStr((lcRev2.StringOf("userMatches")), "true"), .T., .F.)
							ENDIF 

						ENDIF
					ENDIF  
				ENDIF 
			ENDIF 
			SELECT fi_trans_info
			DO CASE 
				CASE ALLTRIM(lcTipo) == 'VERIFY' OR ALLTRIM(lcTipo) == 'VERIFY_MANUAL'
	 				replace fi_trans_info.verify_i WITH lcReturnI 
					replace fi_trans_info.verify_s WITH lcReturnS
					replace fi_trans_info.tcode WITH lcReturnCode
				CASE ALLTRIM(lcTipo) == 'DISPENSE' OR ALLTRIM(lcTipo) == 'DISPENSE_MANUAL'
					replace fi_trans_info.dispense_i WITH lcReturnI 
					replace fi_trans_info.dispense_s WITH lcReturnS
				CASE ALLTRIM(lcTipo) == 'UNDODISPENSE'
					replace fi_trans_info.undo_i WITH lcReturnI 
					replace fi_trans_info.undo_s WITH lcReturnS
				OTHERWISE 
			ENDCASE  
			

			
			SELECT fi_trans_info
			replace fi_trans_info.NmvsTrxId  WITH ALLTRIM(lcNmvsTrxId)
			replace fi_trans_info.Code WITH ALLTRIM(lcCode) 
			replace fi_trans_info.Description WITH ALLTRIM(lcDescription) 
			replace fi_trans_info.State WITH ALLTRIM(lcState)
			replace fi_trans_info.nmvsTimeStamp WITH ALLTRIM(lcNmvsTimeStamp)

			REPLACE fi_trans_info.BatchIdReturn WITH lcBatchIdReturn
			REPLACE fi_trans_info.BatchExpiryDateReturn WITH lcBatchExpiryDateReturn
			REPLACE fi_trans_info.ProductCodeReturn WITH lcProductCodeReturn
			REPLACE fi_trans_info.ProductNameReturn WITH lcProductNameReturn
			REPLACE fi_trans_info.ProductCommonNameReturn WITH lcProductCommonNameReturn
			REPLACE fi_trans_info.SnReturn WITH lcSnReturn
			REPLACE fi_trans_info.UndoPossibleReturn WITH lcUndoPossibleReturn
			REPLACE fi_trans_info.UserMatchesReturn  WITH lcUserMatchesReturn 
			REPLACE fi_trans_info.ProductNhrnReturn WITH lcProductNhrnReturn
			replace fi_trans_info.Reasons WITH ALLTRIM(lcReasons)
			REPLACE fi_trans_info.alertIdReturn WITH ALLTRIM(lcAlertIdReturn)

			SELECT fi_trans_info
			DO CASE 
				CASE ALLTRIM(lcTipo) == 'VERIFY' OR  ALLTRIM(lcTipo) == 'VERIFY_MANUAL' 
					replace fi_trans_info.Reasons WITH ALLTRIM(lcReasons)
					
					if(!EMPTY(ALLTRIM(lcReasons)))
						replace fi_trans_info.verifymsg WITH  ALLTRIM(lcReturnCode) + '|' + ALLTRIM(lcDescription) + '| Raz�o:' + ALLTRIM(lcReasons)
					ELSE
						replace fi_trans_info.verifymsg WITH  ALLTRIM(lcReturnCode) + '|' + ALLTRIM(lcDescription)
					ENDIF
										
				CASE ALLTRIM(lcTipo) == 'DISPENSE' OR  ALLTRIM(lcTipo) == 'DISPENSE_MANUAL' 
					replace fi_trans_info.Reasons WITH ALLTRIM(lcReasons)
					if(!EMPTY(ALLTRIM(lcReasons)))
						replace fi_trans_info.dispensemsg WITH  ALLTRIM(lcReturnCode) + '|' + ALLTRIM(lcDescription) + '| Raz�o:' + ALLTRIM(lcReasons)
					ELSE
						replace fi_trans_info.dispensemsg WITH  ALLTRIM(lcReturnCode) + '|' + ALLTRIM(lcDescription)
					ENDIF
					
				CASE ALLTRIM(lcTipo) == 'UNDODISPENSE'
					replace fi_trans_info.Reasons WITH ALLTRIM(lcReasons)
					
					if(!EMPTY(ALLTRIM(lcReasons)))
						replace fi_trans_info.undodispensemsg WITH ALLTRIM(lcReturnCode) + '|' + ALLTRIM(lcDescription) + '| Raz�o:' + ALLTRIM(lcReasons)
					ELSE
						replace fi_trans_info.undodispensemsg WITH  ALLTRIM(lcReturnCode) + '|' + ALLTRIM(lcDescription)
					ENDIF					
				
				OTHERWISE
				
			ENDCASE 


			LOCAL lcSqlfi_trans_info, lcSql 
			STORE '' TO lcSqlfi_trans_info, lcSql 
			
			lcSqlfi_trans_info = uf_PAGAMENTO_GravarReg_fi_trans_info_line()
			lcSqlfi_trans_info = uf_gerais_trataPlicasSQL(lcSqlfi_trans_info)
		
								
			IF !EMPTY(lcSqlfi_trans_info) 
				
				LOCAL lcPosTemp
				SELECT fi_trans_info
				lcPosTemp= RECNO("fi_trans_info")
			
				
				TEXT TO lcSQL NOSHOW TEXTMERGE
					exec up_gerais_execSql 'Verifica Pack - Insert', 1,'<<lcSqlfi_trans_info>>', '', '', '' , '', ''
				ENDTEXT 
				uf_gerais_actgrelha("", "", lcSQL )
				
					
				SELECT fi_trans_info
				TRY
					GOTO lcPosTemp
				ENDTRY
				
			ENDIF
			
		ENDIF 
		RELEASE loResp
	**ENDIF 
	

	return(.t.)
ENDFUNC 


FUNCTION uf_Atendimento_SinglePackVerify
	LPARAMETERS uv_token
	
	**IF ALLTRIM(ucrse1.u_assfarm)=='AFP'
		
		SELECT fi_trans_info 
		GO TOP 
	*!*		SELECT * FROM fi_trans_info WHERE EMPTY(fi_trans_info.productcode) OR EMPTY(fi_trans_info.batchid) OR EMPTY(fi_trans_info.packserialnumber) OR YEAR(fi_trans_info.batchexpirydate)=1900  INTO CURSOR fi_trans_info_aux READWRITE 
	*!*		SELECT fi_trans_info_aux 
	*!*		IF RECCOUNT("fi_trans_info_aux")>0 then
	*!*			uf_perguntalt_chama("OS PRODUTOS A VALIDAR N�O T�M TODA A INFORMA��O PREENCHIDA. POR FAVOR COMPLEMENTE A INFORMA��O EM FALTA!","OK","", 16)
	*!*			uf_INFOEMBAL_chama()
	*!*			fecha("fi_trans_info_aux")
	*!*			RETURN(.F.)
	*!*		ENDIF 
	*!*		fecha("fi_trans_info_aux")
		
		TEXT TO lcSQL NOSHOW TEXTMERGE
			select 
				token
			from 
				service_credentials (nolock)
			where
				service='Logitools'
				and username='<<ALLTRIM(uCrsE1.id_lt)>>@logitools.pt'
		ENDTEXT  
		uf_gerais_actgrelha("", "curToken", lcSQL)
		LcTokenReq = ALLTRIM(curToken.token)
		fecha("curToken")
				
		LOCAL lcCont2, uv_filtro
		lcCont2 = 1

		uv_filtro = ""

		IF !EMPTY(uv_token)
			uv_filtro = "uf_gerais_compStr(fi_trans_info.token, uv_token)"
		ELSE
			uv_filtro = "!EMPTY(ALLTRIM(fi_trans_info.productcode)) AND !EMPTY(ALLTRIM(fi_trans_info.packserialnumber)) AND  !EMPTY(ALLTRIM(fi_trans_info.batchid)) "
		ENDIF

		SELECT fi_trans_info
		regua(0,RECCOUNT("fi_trans_info"),"A validar dados das embalagens...")
		GO TOP 
		SCAN FOR &uv_filtro.
			regua(1,lcCont2,"A validar dados da embalagem " + ALLTRIM(fi_trans_info.packserialnumber))
			LOCAL lcSendMsg, lcSentBatchid, lcSentStamp
			STORE '' TO  lcSendMsg, lcSentBatchid, lcSentStamp
			lcSendMsg = ''
			
			lcSendMsg = '{'
			IF !EMPTY(fi_trans_info.token)
				lcSendMsg = lcSendMsg + '"token": "'+ALLTRIM(fi_trans_info.token)+'",'
			ENDIF 
			lcSendMsg = lcSendMsg + '"entity": {'
			IF !EMPTY(myServicosEntApp )
				lcSendMsg = lcSendMsg + '"app": "'+ALLTRIM(myServicosEntApp)+'",'
			ENDIF 
			IF !EMPTY(myServicosEntId)
				lcSendMsg = lcSendMsg + '"id": "'+myServicosEntId+'",'
			ENDIF 
			IF !EMPTY(myServicosEntName)
				lcSendMsg = lcSendMsg + '"name": "'+ALLTRIM(myServicosEntName)+'"'
			ENDIF 
			lcSendMsg = lcSendMsg + '},'
			lcSendMsg = lcSendMsg + '"transaction": {'
			IF !EMPTY(fi_trans_info.productcode)
				lcSendMsg = lcSendMsg + '"productCode": "'+ALLTRIM(fi_trans_info.productcode)+'"'
			ENDIF 
			IF !EMPTY(fi_trans_info.productcode)
				lcSendMsg = lcSendMsg + ',"productCodeScheme": "GTIN"'
			ENDIF				
			IF !EMPTY(fi_trans_info.batchid )
				lcSendMsg = lcSendMsg + ',"batchId": "'+ALLTRIM(fi_trans_info.batchid)+'"'
				lcSentBatchid = ALLTRIM(fi_trans_info.batchid)
			ENDIF 
			IF !EMPTY(fi_trans_info.batchexpirydate) AND YEAR(fi_trans_info.batchexpirydate)<>1900
				lcSendMsg = lcSendMsg + ',"batchExpiryDate": "'+RIGHT(ALLTRIM(STR(YEAR(fi_trans_info.batchexpirydate))),2)+RIGHT('0'+ALLTRIM(STR(MONTH(fi_trans_info.batchexpirydate))),2)+RIGHT('0'+ALLTRIM(STR(DAY(fi_trans_info.batchexpirydate))),2)+'"'
			ENDIF 
			IF !EMPTY(fi_trans_info.packserialnumber)
				lcSendMsg = lcSendMsg + ',"packSerialNumber": "'+ALLTRIM(fi_trans_info.packserialnumber)+'"'
			ENDIF 
			IF !EMPTY(fi_trans_info.clienttrxid)
				lcSendMsg = lcSendMsg + ',"clientTrxId": "'+ALLTRIM(fi_trans_info.clienttrxid)+'"'
			ENDIF 
			IF !EMPTY(fi_trans_info.posterminal)
				lcSendMsg = lcSendMsg + ',"posTerminal" : "'+ALLTRIM(fi_trans_info.posterminal)+'"'
			ENDIF 
			**IF ALLTRIM(STR(fi_trans_info.country_productNhrn))='0' AND LEN(ALLTRIM(fi_trans_info.productcode))=7
			**	replace fi_trans_info.country_productNhrn WITH 714
			**ENDIF 
			IF !EMPTY(fi_trans_info.productNhrn)
				lcSendMsg = lcSendMsg + ',"productNhrn" : "('+ALLTRIM(STR(fi_trans_info.country_productNhrn))+')'+ALLTRIM(fi_trans_info.productNhrn)+'"'
			ENDIF 
			lcSendMsg = lcSendMsg + '},'
			lcSendMsg = lcSendMsg + '"sender": {'
			lcSendMsg = lcSendMsg + '"user": "'+ALLTRIM(myServicosUser)+'",'
			lcSendMsg = lcSendMsg + '"pw": "'+ALLTRIM(myServicosPassw)+'"'
			lcSendMsg = lcSendMsg + '}'
			lcSendMsg = lcSendMsg + '}'
			lcSentStamp = ALLTRIM(fi_trans_info.recStamp)

			**_clipText = lcSendMsg
			
			
			LOCAL lcRequest
			LOCAL lcSufixUrl 
			lcSufixUrl ='verifySinglePack'
			lcRequest='VERIFY'
			
				if(EMPTY(fi_trans_info.reqManual))
					lcRequest = 'VERIFY'
					lcSufixUrl ='verifySinglePack'
				ELSE
					lcRequest = 'VERIFY_MANUAL'
					lcSufixUrl ='verifySinglePackManual'	
				ENDIF
		
	
			IF !(uf_single_pack_requests(lcRequest , myServicosSite + myServicosSiteSuf +'/'+lcSufixUrl, ALLTRIM(lcSendMsg), ALLTRIM(lcSentBatchid), ALLTRIM(lcSentStamp), ALLTRIM(LcTokenReq) ))
						
			ENDIF 
			lcCont2 = lcCont2 + 1
			SELECT fi_trans_info
		ENDSCAN 
		SELECT fi_trans_info
		regua(2)
	**ENDIF 
ENDFUNC 




FUNCTION uf_Atendimento_DispensePackVerify
	**IF ALLTRIM(ucrse1.u_assfarm)=='AFP'
		TEXT TO lcSQL NOSHOW TEXTMERGE
			select 
				token
			from 
				service_credentials (nolock)
			where
				service='Logitools'
				and username='<<ALLTRIM(uCrsE1.id_lt)>>@logitools.pt'
		ENDTEXT  
		uf_gerais_actgrelha("", "curToken", lcSQL)
		LcTokenReq = ALLTRIM(curToken.token)
		fecha("curToken")
		

		

		LOCAL lcCont2
		lcCont2 = 1
		SELECT fi_trans_info
		regua(0,RECCOUNT("fi_trans_info"),"A dispensar embalagens...")
		GO TOP 
		SCAN FOR !EMPTY(ALLTRIM(fi_trans_info.productcode)) AND !EMPTY(ALLTRIM(fi_trans_info.packserialnumber)) AND  !EMPTY(ALLTRIM(fi_trans_info.batchid))
			IF ALLTRIM(fi_trans_info.tipo)='S' AND EMPTY(ALLTRIM(fi_trans_info.dispense_i))
				regua(1,lcCont2,"A dispensar a embalagem " + ALLTRIM(fi_trans_info.packserialnumber))
				LOCAL lcSendMsg, lcSentBatchid, lcSentStamp
				STORE '' TO  lcSendMsg, lcSentBatchid, lcSentStamp
				lcSendMsg = ''
				
				lcSendMsg = '{'
				IF !EMPTY(fi_trans_info.token)
					lcSendMsg = lcSendMsg + '"token": "'+ALLTRIM(fi_trans_info.token)+'",'
				ENDIF 
				lcSendMsg = lcSendMsg + '"entity": {'
				IF !EMPTY(myServicosEntApp )
					lcSendMsg = lcSendMsg + '"app": "'+ALLTRIM(myServicosEntApp)+'",'
				ENDIF 
				IF !EMPTY(myServicosEntId)
					lcSendMsg = lcSendMsg + '"id": "'+myServicosEntId+'",'
				ENDIF 
				IF !EMPTY(myServicosEntName)
					lcSendMsg = lcSendMsg + '"name": "'+ALLTRIM(myServicosEntName)+'"'
				ENDIF 
				lcSendMsg = lcSendMsg + '},'
				lcSendMsg = lcSendMsg + '"transaction": {'
				IF !EMPTY(fi_trans_info.productcode)
					lcSendMsg = lcSendMsg + '"productCode": "'+ALLTRIM(fi_trans_info.productcode)+'"'
				ENDIF 
				IF !EMPTY(fi_trans_info.productcode)
					lcSendMsg = lcSendMsg + ',"productCodeScheme": "GTIN"'
				ENDIF 
				
				IF !EMPTY(fi_trans_info.batchid )
					lcSendMsg = lcSendMsg + ',"batchId": "'+ALLTRIM(fi_trans_info.batchid)+'"'
					lcSentBatchid = ALLTRIM(fi_trans_info.batchid)
				ENDIF 
				IF !EMPTY(fi_trans_info.batchexpirydate) AND YEAR(fi_trans_info.batchexpirydate)<>1900
					lcSendMsg = lcSendMsg + ',"batchExpiryDate": "'+RIGHT(ALLTRIM(STR(YEAR(fi_trans_info.batchexpirydate))),2)+RIGHT('0'+ALLTRIM(STR(MONTH(fi_trans_info.batchexpirydate))),2)+RIGHT('0'+ALLTRIM(STR(DAY(fi_trans_info.batchexpirydate))),2)+'"'
				ENDIF 
				IF !EMPTY(fi_trans_info.packserialnumber)
					lcSendMsg = lcSendMsg + ',"packSerialNumber": "'+ALLTRIM(fi_trans_info.packserialnumber)+'"'
				ENDIF 
				IF !EMPTY(fi_trans_info.clienttrxid)
					lcSendMsg = lcSendMsg + ',"clientTrxId": "'+ALLTRIM(fi_trans_info.clienttrxid)+'"'
				ENDIF 
				IF !EMPTY(fi_trans_info.posterminal)
					lcSendMsg = lcSendMsg + ',"posTerminal" : "'+ALLTRIM(fi_trans_info.posterminal)+'"'
				ENDIF 

				IF !EMPTY(fi_trans_info.productNhrn)
					lcSendMsg = lcSendMsg + ',"productNhrn" : "('+ALLTRIM(STR(fi_trans_info.country_productNhrn))+')'+ALLTRIM(fi_trans_info.productNhrn)+'"'
				ENDIF 

				lcSendMsg = lcSendMsg + '},'
				lcSendMsg = lcSendMsg + '"sender": {'
				lcSendMsg = lcSendMsg + '"user": "'+ALLTRIM(myServicosUser)+'",'
				lcSendMsg = lcSendMsg + '"pw": "'+ALLTRIM(myServicosPassw )+'"'
				lcSendMsg = lcSendMsg + '}'
				lcSendMsg = lcSendMsg + '}'
				lcSentStamp = ALLTRIM(fi_trans_info.recStamp)

				_clipText = lcSendMsg
				
				LOCAL lcRequest
				LOCAL lcSufixUrl 
				lcSufixUrl ='dispenseSinglePack'
				
				if(EMPTY(fi_trans_info.reqManual))
					lcRequest = 'DISPENSE'
					lcSufixUrl ='dispenseSinglePack'
				ELSE
					lcRequest = 'DISPENSE_MANUAL'
					lcSufixUrl ='dispenseSinglePackManual'	
				ENDIF
				
					
			
				IF !(uf_single_pack_requests(lcRequest, myServicosSite + myServicosSiteSuf +'/'+lcSufixUrl , ALLTRIM(lcSendMsg), ALLTRIM(lcSentBatchid), ALLTRIM(lcSentStamp), ALLTRIM(LcTokenReq) ))
		
				ENDIF 
				
				**uf_single_pack_requests('DISPENSE', myServicosSite+'afp/concentrator/dispenseSinglePack-test', ALLTRIM(lcSendMsg), ALLTRIM(lcSentBatchid), ALLTRIM(lcSentStamp), ALLTRIM(LcTokenReq) )
				lcCont2 = lcCont2 + 1
				SELECT fi_trans_info
			ENDIF 
			SELECT fi_trans_info
		ENDSCAN 
		

		
		regua(2)
	**ENDIF 
ENDFUNC 

FUNCTION uf_servico_preenche_nmvs_trxidUltimoDispensa
	
	SELECT fi_trans_info
	GO TOP
	SCAN
		LOCAL lcSQL, lcNmvsTrxId 
		IF ALLTRIM(fi_trans_info.tipo)='E' 	
			
			lcSQL = ''
			TEXT TO lcSql NOSHOW TEXTMERGE
				exec up_servico_getOriginalDispense '<<ALLTRIM(fi_trans_info.productcode)>>','<<ALLTRIM(fi_trans_info.batchid)>>','<<ALLTRIM(fi_trans_info.packserialnumber)>>'
			ENDTEXT
			
			lcNmvsTrxId  = ''
			IF uf_gerais_actGrelha("",[uCrsLastDispense],lcSql)
				if(REccount("uCrsLastDispense")>0)
					SELECT uCrsLastDispense
					lcNmvsTrxId  = ALLTRIM(uCrsLastDispense.nmvsTrxId)
				ENDIF				
			ENDIF

			
			SELECT fi_trans_info
			replace fi_trans_info.nmvsTrxId WITH lcNmvsTrxId  
							
			fecha("uCrsLastDispense")
			
		ENDIF
		SELECT  fi_trans_info
	ENDSCAN 
	
	
ENDFUNC



FUNCTION uf_Atendimento_UndoDispenseSinglePack
	**IF ALLTRIM(ucrse1.u_assfarm)=='AFP'

	
		TEXT TO lcSQL NOSHOW TEXTMERGE
			select 
				token
			from 
				service_credentials (nolock)
			where
				service='Logitools'
				and username='<<ALLTRIM(uCrsE1.id_lt)>>@logitools.pt'
		ENDTEXT  
		uf_gerais_actgrelha("", "curToken", lcSQL)
		LcTokenReq = ALLTRIM(curToken.token)
		fecha("curToken")
		
		
		uf_servico_preenche_nmvs_trxidUltimoDispensa()
		
		LOCAL lcCont2
		lcCont2 = 1
		SELECT fi_trans_info
		regua(0,RECCOUNT("fi_trans_info"),"A anular dispensa das embalagens...")
		GO TOP 
		SCAN FOR !EMPTY(ALLTRIM(fi_trans_info.productcode)) AND !EMPTY(ALLTRIM(fi_trans_info.packserialnumber)) AND  !EMPTY(ALLTRIM(fi_trans_info.batchid)) 
			IF ALLTRIM(fi_trans_info.tipo)='E' AND EMPTY(ALLTRIM(fi_trans_info.undo_i))
				regua(1,lcCont2,"A anular a dispensa da embalagem " + ALLTRIM(fi_trans_info.packserialnumber))
				

				LOCAL lcSendMsg, lcSentBatchid, lcSentStamp
				STORE '' TO  lcSendMsg, lcSentBatchid, lcSentStamp
				lcSendMsg = ''
				lcSendMsg = '{'
				IF !EMPTY(fi_trans_info.token)
					lcSendMsg = lcSendMsg + '"token": "'+ALLTRIM(fi_trans_info.token)+'",'
				ENDIF 
				lcSendMsg = lcSendMsg + '"entity": {'
				IF !EMPTY(myServicosEntApp )
					lcSendMsg = lcSendMsg + '"app": "'+ALLTRIM(myServicosEntApp)+'",'
				ENDIF 
				IF !EMPTY(myServicosEntId)
					lcSendMsg = lcSendMsg + '"id": "'+myServicosEntId+'",'
				ENDIF 
				IF !EMPTY(myServicosEntName)
					lcSendMsg = lcSendMsg + '"name": "'+ALLTRIM(myServicosEntName)+'"'
				ENDIF 
				lcSendMsg = lcSendMsg + '},'
				lcSendMsg = lcSendMsg + '"transaction": {'
				IF !EMPTY(fi_trans_info.productcode)
					lcSendMsg = lcSendMsg + '"productCode": "'+ALLTRIM(fi_trans_info.productcode)+'"'
				ENDIF 
				IF !EMPTY(fi_trans_info.productcode)
					lcSendMsg = lcSendMsg + ',"productCodeScheme": "GTIN"'
				ENDIF 
				
				IF !EMPTY(fi_trans_info.batchid )
					lcSendMsg = lcSendMsg + ',"batchId": "'+ALLTRIM(fi_trans_info.batchid)+'"'
					lcSentBatchid = ALLTRIM(fi_trans_info.batchid)
				ENDIF 
				IF !EMPTY(fi_trans_info.batchexpirydate) AND YEAR(fi_trans_info.batchexpirydate)<>1900
					lcSendMsg = lcSendMsg + ',"batchExpiryDate": "'+RIGHT(ALLTRIM(STR(YEAR(fi_trans_info.batchexpirydate))),2)+RIGHT('0'+ALLTRIM(STR(MONTH(fi_trans_info.batchexpirydate))),2)+RIGHT('0'+ALLTRIM(STR(DAY(fi_trans_info.batchexpirydate))),2)+'"'
				ENDIF 
				IF !EMPTY(fi_trans_info.packserialnumber)
					lcSendMsg = lcSendMsg + ',"packSerialNumber": "'+ALLTRIM(fi_trans_info.packserialnumber)+'"'
				ENDIF 
				IF !EMPTY(fi_trans_info.clienttrxid)
					lcSendMsg = lcSendMsg + ',"clientTrxId": "'+ALLTRIM(fi_trans_info.clienttrxid)+'"'
				ENDIF 
				IF !EMPTY(fi_trans_info.posterminal)
					lcSendMsg = lcSendMsg + ',"posTerminal" : "'+ALLTRIM(fi_trans_info.posterminal)+'"'
				ENDIF 
				IF !EMPTY(fi_trans_info.nmvsTrxId)
				
					lcSendMsg = lcSendMsg + ',"nmvsTrxId" : "'+ALLTRIM(fi_trans_info.nmvsTrxId)+'"'
				ENDIF 
				
				IF !EMPTY(fi_trans_info.productNhrn)
					lcSendMsg = lcSendMsg + ',"productNhrn" : "('+ALLTRIM(STR(fi_trans_info.country_productNhrn))+')'+ALLTRIM(fi_trans_info.productNhrn)+'"'
				ENDIF  
				
				lcSendMsg = lcSendMsg + '},'
				lcSendMsg = lcSendMsg + '"sender": {'
				lcSendMsg = lcSendMsg + '"user": "'+ALLTRIM(myServicosUser)+'",'
				lcSendMsg = lcSendMsg + '"pw": "'+ALLTRIM(myServicosPassw)+'"'
				lcSendMsg = lcSendMsg + '}'
				lcSendMsg = lcSendMsg + '}'
				lcSentStamp = ALLTRIM(fi_trans_info.recStamp)
				**_cliptext=lcSendMsg 
				
				IF !(uf_single_pack_requests('UNDODISPENSE', myServicosSite + myServicosSiteSuf +'/undoDispenseSinglePack', ALLTRIM(lcSendMsg), ALLTRIM(lcSentBatchid), ALLTRIM(lcSentStamp), ALLTRIM(LcTokenReq) ))
					**uf_pede_novo_token()
*!*						TEXT TO lcSQL NOSHOW TEXTMERGE
*!*							select 
*!*								token
*!*							from 
*!*								service_credentials (nolock)
*!*							where
*!*								service='Logitools'
*!*								and username='<<ALLTRIM(uCrsE1.id_lt)>>@logitools.pt'
*!*						ENDTEXT  
*!*						uf_gerais_actgrelha("", "curToken", lcSQL)
*!*						LcTokenReq = ALLTRIM(curToken.token)
*!*						fecha("curToken")
*!*						IF !(uf_single_pack_requests('UNDODISPENSE', myServicosSite + myServicosSiteSuf +'/dispenseSinglePack', ALLTRIM(lcSendMsg), ALLTRIM(lcSentBatchid), ALLTRIM(lcSentStamp), ALLTRIM(LcTokenReq) ))
*!*							**uf_perguntalt_chama("OCOREEU UM ERRO AO AUTENTICAR OS PEDIDOS DE ANULA��O DA DISPENSA DAS EMBALAGENS. POR FAVOR CONTACTE O SUPORTE","OK","",64)
*!*							**return(.f.)
*!*						ENDIF 			
				ENDIF 
				
				lcCont2 = lcCont2 + 1
				SELECT fi_trans_info
				
			ENDIF 
		ENDSCAN 
		regua(2)
	**ENDIF 
ENDFUNC 

FUNCTION uf_valida_info_caixas_verificacao
	**IF ALLTRIM(ucrse1.u_assfarm)=='AFP'
		SELECT fi_trans_info
		IF RECCOUNT("fi_trans_info") > 0 then
			
			SELECT fi_trans_info 
			GO TOP 
			SCAN 
				IF EMPTY(fi_trans_info.productcode) OR EMPTY(fi_trans_info.batchid) OR EMPTY(fi_trans_info.packserialnumber) OR YEAR(fi_trans_info.batchexpirydate)=1900
					DELETE 
				ENDIF 
			ENDSCAN 				
			SELECT * FROM fi_trans_info WHERE EMPTY(fi_trans_info.productcode) OR EMPTY(fi_trans_info.batchid) OR EMPTY(fi_trans_info.packserialnumber) OR YEAR(fi_trans_info.batchexpirydate)=1900  INTO CURSOR fi_trans_info_aux READWRITE 
			SELECT fi_trans_info_aux 
			IF RECCOUNT("fi_trans_info_aux")>0 then
				&& descomentar nmmvo
				&&uf_perguntalt_chama("OS PRODUTOS A VALIDAR N�O T�M TODA A INFORMA��O PREENCHIDA. POR FAVOR COMPLEMENTE A INFORMA��O EM FALTA!","OK","", 16)
				&&uf_INFOEMBAL_chama()
				fecha("fi_trans_info_aux")
				&&RETURN(.F.)
			ENDIF 
			fecha("fi_trans_info_aux")
			
			uf_Atendimento_SinglePackVerify()
			

			
			SELECT fi_trans_info 
			GO TOP 
			SELECT * FROM fi_trans_info WHERE (ALLTRIM(fi_trans_info.state) != 'ACTIVE' AND ALLTRIM(fi_trans_info.tipo)=='S');
													OR (ALLTRIM(fi_trans_info.state) != 'INACTIVE' AND ALLTRIM(fi_trans_info.tipo)=='E');
													 OR ALLTRIM(fi_trans_info.code) != 'NMVS_SUCCESS' OR fi_trans_info.verify_i<>'200' INTO CURSOR fi_trans_info_aux READWRITE 
			&&SELECT * FROM fi_trans_info WHERE fi_trans_info.verify_i<>'200' INTO CURSOR fi_trans_info_aux READWRITE 								 
			SELECT fi_trans_info_aux 
			IF RECCOUNT("fi_trans_info_aux")>0 then
				&& descomentar nmmvo
				&&uf_perguntalt_chama("A VALIDA��O DAS CAIXAS APRESENTOU ERROS! POR FAVOR VERIFIQUE.","OK","", 16) &&descomentar para bloquear
				&&uf_INFOEMBAL_chama('ERROS')
				fecha("fi_trans_info_aux")
				&&RETURN(.F.)  &&descomentar para bloquear
				
			ENDIF 
			fecha("fi_trans_info_aux")
		ENDIF 
	**ENDIF 
	RETURN(.T.)
	
ENDFUNC 

FUNCTION uf_valida_info_caixas_dispensa
	LPARAMETERS lcBackOffice, lcDocumentos

	SELECT fi_trans_info
	IF RECCOUNT("fi_trans_info") > 0

		LOCAL uv_param, uv_mostra

		uv_param = uf_gerais_getParameter_site("ADM0000000209", "NUM", mySite)
		uv_mostra = .F.

		DO CASE
			CASE uv_param = 1
				uv_mostra = .T.

			CASE uv_param = 2

				SELECT fi_trans_info 
				COUNT TO uv_count FOR ALLTRIM(fi_trans_info.code) <> 'NMVS_SUCCESS' 

				IF uv_count > 0

					uv_mostra = .T.

					SELECT fi_trans_info
					GO TOP
					DELETE FOR uf_gerais_compStr(fi_trans_info.code, 'NMVS_SUCCESS')

				ENDIF

			CASE uv_param = 3

				SELECT fi_trans_info 
				COUNT TO uv_count FOR ALLTRIM(fi_trans_info.code) <> 'NMVS_SUCCESS' 

				IF uv_count > 0

					uv_mostra = .T.

				ENDIF

		OTHERWISE
			uv_mostra = .F.
		ENDCASE
	
		
		IF uv_mostra


			&&uf_perguntalt_chama("A DISPENSA / DEVOLU��O DAS CAIXAS APRESENTOU ERROS! POR FAVOR VERIFIQUE.","OK","", 16)

			uf_INFOEMBAL_chama(lcBackOffice, lcDocumentos, .T.)
		ENDIF 
	ENDIF 
	RETURN(.T.)
	
ENDFUNC 

FUNCTION uf_servicos_store_bulk
	**IF ALLTRIM(ucrse1.u_assfarm)=='AFP'
		PARAMETERS lcTipoBulk, lcStampLin
		
		if(!USED("mixed_bulk_pend"))
			RETURN .f.
		endif
		
		LOCAL lcPosTemp
		SELECT fi_trans_info
		lcPosTemp= RECNO("fi_trans_info")
		

		IF EMPTY(lcStampLin)
			SELECT fi_trans_info
			GO TOP 
			SCAN 

				IF (ALLTRIM(lcTipoBulk)='DISPENSE' OR ALLTRIM(lcTipoBulk)='UNDODISPENSE' OR ALLTRIM(lcTipoBulk)='DISPENSE_MANUAL' OR ALLTRIM(lcTipoBulk)='VERIFY' OR ALLTRIM(lcTipoBulk)='VERIFY_MANUAL');
					AND !EMPTY(fi_trans_info.productCode) AND !EMPTY(fi_trans_info.productCodeScheme)  AND !EMPTY(fi_trans_info.packSerialNumber) AND !EMPTY(fi_trans_info.batchId);
					AND (EMPTY(ALLTRIM(fi_trans_info.state)) OR ALLTRIM(lcTipoBulk)='VERIFY' OR ALLTRIM(lcTipoBulk)='VERIFY_MANUAL')

					
					SELECT mixed_bulk_pend
					GO BOTTOM 
					APPEND blank 
					replace mixed_bulk_pend.token WITH ALLTRIM(fi_trans_info.token)
					replace mixed_bulk_pend.recStamp WITH ALLTRIM(fi_trans_info.recStamp)
					replace mixed_bulk_pend.recTable WITH ALLTRIM(fi_trans_info.recTable)
					replace mixed_bulk_pend.productCode WITH ALLTRIM(fi_trans_info.productCode)
					replace mixed_bulk_pend.productCodeScheme WITH ALLTRIM(fi_trans_info.productCodeScheme)
					replace mixed_bulk_pend.batchId WITH ALLTRIM(fi_trans_info.batchId)
					replace mixed_bulk_pend.batchExpiryDate WITH fi_trans_info.batchExpiryDate
					replace mixed_bulk_pend.packSerialNumber WITH ALLTRIM(fi_trans_info.packSerialNumber)
					replace mixed_bulk_pend.clientTrxId WITH ALLTRIM(fi_trans_info.clientTrxId)
					replace mixed_bulk_pend.posTerminal WITH ALLTRIM(fi_trans_info.posTerminal)
					replace mixed_bulk_pend.country_productNhrn WITH fi_trans_info.country_productNhrn
					replace mixed_bulk_pend.productNhrn WITH ALLTRIM(fi_trans_info.productNhrn)
					replace mixed_bulk_pend.ousrinis WITH ALLTRIM(m_chinis)
					replace mixed_bulk_pend.tipo WITH ALLTRIM(lcTipoBulk)
					replace mixed_bulk_pend.reqType WITH ALLTRIM(fi_trans_info.reqType)
					replace mixed_bulk_pend.reasons WITH ALLTRIM(fi_trans_info.reasons)
					replace mixed_bulk_pend.nmvsTrxId WITH ALLTRIM(fi_trans_info.nmvsTrxId)
					replace mixed_bulk_pend.reqManual WITH fi_trans_info.reqManual
					replace mixed_bulk_pend.code WITH ALLTRIM(fi_trans_info.code)
					replace mixed_bulk_pend.state WITH ALLTRIM(fi_trans_info.state)
					replace mixed_bulk_pend.ousrdata WITH fi_trans_info.ousrdata
					replace mixed_bulk_pend.nmvsTimeStamp WITH ALLTRIM(fi_trans_info.nmvsTimeStamp)
					
					IF ALLTRIM(lcTipoBulk)<>'VERIFY' AND ALLTRIM(lcTipoBulk)<>'VERIFY_MANUAL'

						SELECT fi_trans_info
						replace fi_trans_info.deleted WITH .t.							

					ENDIF
				ENDIF 
			ENDSCAN 
			
			
		ELSE
			SELECT fi_trans_info
			GO TOP 
			SCAN 
				IF (ALLTRIM(lcTipoBulk)='DISPENSE' OR ALLTRIM(lcTipoBulk)='UNDODISPENSE' OR ALLTRIM(lcTipoBulk)='DISPENSE_MANUAL' OR ALLTRIM(lcTipoBulk)='VERIFY' OR ALLTRIM(lcTipoBulk)='VERIFY_MANUAL');
					AND !EMPTY(fi_trans_info.productCode) AND !EMPTY(fi_trans_info.productCodeScheme) ; 
					AND !EMPTY(fi_trans_info.packSerialNumber) AND !EMPTY(fi_trans_info.batchId) AND ALLTRIM(fi_trans_info.recStamp)==ALLTRIM(lcStampLin);
					AND (EMPTY(ALLTRIM(fi_trans_info.state)) OR ALLTRIM(lcTipoBulk)='VERIFY' OR ALLTRIM(lcTipoBulk)='VERIFY_MANUAL')
					
					
					SELECT mixed_bulk_pend
					GO BOTTOM 
					APPEND blank 
					replace mixed_bulk_pend.token WITH ALLTRIM(fi_trans_info.token)
					replace mixed_bulk_pend.recStamp WITH ALLTRIM(fi_trans_info.recStamp)
					replace mixed_bulk_pend.recTable WITH ALLTRIM(fi_trans_info.recTable)
					replace mixed_bulk_pend.productCode WITH ALLTRIM(fi_trans_info.productCode)
					replace mixed_bulk_pend.productCodeScheme WITH ALLTRIM(fi_trans_info.productCodeScheme)
					replace mixed_bulk_pend.batchId WITH ALLTRIM(fi_trans_info.batchId)
					replace mixed_bulk_pend.batchExpiryDate WITH fi_trans_info.batchExpiryDate
					replace mixed_bulk_pend.packSerialNumber WITH ALLTRIM(fi_trans_info.packSerialNumber)
					replace mixed_bulk_pend.clientTrxId WITH ALLTRIM(fi_trans_info.clientTrxId)
					replace mixed_bulk_pend.posTerminal WITH ALLTRIM(fi_trans_info.posTerminal)
					replace mixed_bulk_pend.country_productNhrn WITH fi_trans_info.country_productNhrn
					replace mixed_bulk_pend.productNhrn WITH ALLTRIM(fi_trans_info.productNhrn)
					replace mixed_bulk_pend.ousrinis WITH ALLTRIM(m_chinis)
					replace mixed_bulk_pend.tipo WITH ALLTRIM(lcTipoBulk)
					replace mixed_bulk_pend.reqType WITH ALLTRIM(fi_trans_info.reqType)
					replace mixed_bulk_pend.reasons WITH ALLTRIM(fi_trans_info.reasons)
					replace mixed_bulk_pend.nmvsTrxId WITH ALLTRIM(fi_trans_info.nmvsTrxId)
					replace mixed_bulk_pend.reqManual WITH fi_trans_info.reqManual
					replace mixed_bulk_pend.code WITH ALLTRIM(fi_trans_info.code)
					replace mixed_bulk_pend.state WITH ALLTRIM(fi_trans_info.state)					
					replace mixed_bulk_pend.ousrdata WITH fi_trans_info.ousrdata
					replace mixed_bulk_pend.nmvsTimeStamp WITH ALLTRIM(fi_trans_info.nmvsTimeStamp)					


					IF ALLTRIM(lcTipoBulk)<>'VERIFY' AND ALLTRIM(lcTipoBulk)<>'VERIFY_MANUAL'

						SELECT fi_trans_info
						replace fi_trans_info.deleted WITH .t.							

					ENDIF

				ENDIF 
			ENDSCAN 
		ENDIF 
		
		
		DELETE FROM fi_trans_info WHERE !EMPTY(fi_trans_info.deleted)
		
		
		SELECT fi_trans_info
	    TRY
	       GOTO lcPosTemp
	    CATCH
	       GOTO BOTTOM  
	    ENDTRY
		    
		
	**ENDIF
ENDFUNC 


FUNCTION uf_chilkat_send_request_resetCounters_ticket

	LOCAL loReq, loHttp, lnSuccess,	lnSuccess1,	lcJsonText,	loResp, lcUrl, lcUser, lcResponseHeaderCode, lctoken, lcSite
	lcSite = mysite

	uf_servicos_unblockChilkat()
	lcUrl= uf_gerais_getparameter_site('ADM0000000043', 'TEXT', lcSite )

	loReq = CreateObject('Chilkat_9_5_0.HttpRequest')
	loHttp = CreateObject('Chilkat_9_5_0.Http')
	loJson = CreateObject('Chilkat_9_5_0.JsonObject')

	lcPostHTTP=lcUrl + "resetcounters.php"

	loHttp.AcceptCharset = ""
	loHttp.UserAgent = ""
	loHttp.AcceptLanguage = ""
	loHttp.AllowGzip = 0

	lcUser = uf_gerais_getparameter_site('ADM0000000044', 'TEXT', lcSite)
	lnSuccess = loHttp.AddQuickHeader("user",lcUser)
	lnSuccess = loHttp.AddQuickHeader("password",uf_gerais_getparameter_site('ADM0000000045', 'TEXT', lcSite ))
	loHttp.ConnectTimeout = 30
	loHttp.ReadTimeout = 60
	lcToken = uf_gerais_stamp()
	loResp = loHttp.PostJson(lcPostHTTP,"")
	IF (loHttp.LastMethodSuccess <> 1) THEN
		uf_perguntalt_chama("Ocorreu um erro a reiniciar o contador das senhas","OK","",48)
		uf_chilkat_Save_msg_status_Ticket(lcToken,-1,STRTRAN(LEFT(ALLTRIM(loHttp.LastErrorText),4000),"'",""))
	ELSE
		lcResponseHeaderCode = loResp.StatusCode
		IF (lcResponseHeaderCode !=200)
			uf_chilkat_Save_StatusError_Ticket(lcToken,lcResponseHeaderCode)
		ELSE
			uf_perguntalt_chama("Senhas reiniciadas com sucesso.","OK","",64)
			uf_gerais_registaOcorrencia('Senhas', 'Senhas reiniciadas com sucesso', 2)								
		ENDIF
	ENDIF

	RELEASE loReq
	RELEASE loHttp	
	RELEASE loResp
ENDFUNC 


FUNCTION uf_chilkat_send_request_ReCall_Ticket
	PARAMETERS lcDesk,lcOp ,lcSite
	LOCAL loReq, loHttp, lnSuccess,	lnSuccess1,	lcJsonText,	loResp, lcUrl, lcUser, lcResponseHeaderCode, lctoken,lcLocal

	uf_servicos_unblockChilkat()
	lcUrl= uf_gerais_getparameter_site('ADM0000000043', 'TEXT', lcSite)
	lcLocal = uf_gerais_getparameter_site('ADM0000000046', 'TEXT', lcSite)
	loReq = CreateObject('Chilkat_9_5_0.HttpRequest')
	loHttp = CreateObject('Chilkat_9_5_0.Http')
	loJson = CreateObject('Chilkat_9_5_0.JsonObject')

	lcPostHTTP=lcUrl + "reCall.php?" + "idbalcao="+lcDesk+"&idlocal=" + lcLocal + "&op=" + lcOp

	loHttp.AcceptCharset = ""
	loHttp.UserAgent = ""
	loHttp.AcceptLanguage = ""
	loHttp.AllowGzip = 0

	lcUser = uf_gerais_getparameter_site('ADM0000000044', 'TEXT', lcSite)
	lnSuccess = loHttp.AddQuickHeader("user",lcUser)
	lnSuccess = loHttp.AddQuickHeader("password",uf_gerais_getparameter_site('ADM0000000045', 'TEXT', lcSite))
	loHttp.ConnectTimeout = 30
	loHttp.ReadTimeout = 60
	lcToken = uf_gerais_stamp()
	loResp = loHttp.PostJson(lcPostHTTP,"")
	IF (loHttp.LastMethodSuccess <> 1) THEN
		uf_perguntalt_chama("Ocorreu um erro a receber a resposta das senhas","OK","",48)
		uf_chilkat_Save_msg_status_Ticket(lcToken,-1,STRTRAN(LEFT(ALLTRIM(loHttp.LastErrorText),4000),"'",""))
	ELSE
*!*			uf_atendimento_savetime("2","uf_ticket_recall")	
		lcResponseHeaderCode = loResp.StatusCode
		IF (lcResponseHeaderCode !=200)
			uf_chilkat_Save_StatusError_Ticket(lcToken,lcResponseHeaderCode)
		ENDIF
	ENDIF

	RELEASE loReq
	RELEASE loHttp	
	RELEASE loResp
ENDFUNC 


FUNCTION uf_chilkat_send_request_GetWaitingQueue_Ticket
	PARAMETERS lcSite , lctoken ,lcSiteNr
	LOCAL loReq, loHttp, lnSuccess,	lnSuccess1,	lcJsonText,	loResp, lcUrl,lcResponseBody, lcDataSet , lcDepartment_id, lcTickets_count, lcStart_date, lcDepartamentName, lcWaiting_minutes, lcResponseHeaderCode,lcRevSize
	STORE '' to lcResponseBody ,lcDataSet , lcDepartment_id, LcTickets_count, lcStart_date, lcDepartamentName, lcWaiting_minutes
	uf_servicos_unblockChilkat()
	
	lcUrl= uf_gerais_getparameter_site('ADM0000000043', 'TEXT', lcSite)
	loReq = CreateObject('Chilkat_9_5_0.HttpRequest')
	loHttp = CreateObject('Chilkat_9_5_0.Http')

	loJson = CreateObject('Chilkat_9_5_0.JsonObject')

	lcPostHTTP=lcUrl + "getWaitingQueue.php"

	loHttp.AcceptCharset = ""
	loHttp.UserAgent = ""
	loHttp.AcceptLanguage = ""
	loHttp.AllowGzip = 0
	lcUser = uf_gerais_getparameter_site('ADM0000000044', 'TEXT', lcSite)
	lnSuccess = loHttp.AddQuickHeader("user",lcUser)
	lnSuccess = loHttp.AddQuickHeader("password",uf_gerais_getparameter_site('ADM0000000045', 'TEXT', lcSite))
	loHttp.ConnectTimeout = 30
	loHttp.ReadTimeout = 60
	loResp = loHttp.PostJson(lcPostHTTP,"")
	IF (loHttp.LastMethodSuccess <> 1) THEN
		uf_perguntalt_chama("Ocorreu um erro a receber a resposta","OK","",48)
		uf_chilkat_Save_msg_status_Ticket(lcToken,-1,STRTRAN(LEFT(ALLTRIM(loHttp.LastErrorText),4000),"'",""))
	ELSE
*!*			uf_atendimento_savetime("2","uf_filaEspera_carregaDados")
		lcResponseHeaderCode = loResp.StatusCode
		IF (lcResponseHeaderCode ==200)
			loJson.Load(loResp.BodyStr)
			loJson.EmitCompact = 0
			lcResponseBody = loJson.Arrayof("dataset")
			IF  (loJson.LastMethodSuccess !=0)
				lcResponseBodySize = lcResponseBody.Size
				FOR lnIndex = 0 TO lcResponseBodySize -1
					lcDataSet = lcResponseBody.ObjectAt(lnIndex)
					IF !(ISNULL("lcDataSet"))
						IF 	(Alltrim(UPPER(lcDataSet.StringOf("iddepartamento")))!="NULL" AND len(lcDataSet.StringOf("iddepartamento"))!=0)
							lcDepartment_id = (lcDataSet.StringOf("iddepartamento"))
						ENDIF 
						IF (Alltrim(UPPER(lcDataSet.StringOf("senhas")))!="NULL" AND len(lcDataSet.StringOf("senhas"))!=0)
							lcTickets_count = (lcDataSet.StringOf("senhas"))
						ENDIF 
						IF  (Alltrim(UPPER(lcDataSet.StringOf("dataini")))!="NULL" AND len(lcDataSet.StringOf("dataini"))!=0)
							lcStart_date= (lcDataSet.StringOf("dataini"))
						ENDIF 
						IF (Alltrim(UPPER(lcDataSet.StringOf("departamento")))!="NULL" AND len(lcDataSet.StringOf("departamento"))!=0)
							lcDepartamentName= (lcDataSet.StringOf("departamento"))
						ENDIF 
						IF (Alltrim(UPPER(lcDataSet.StringOf("minutosespera")))!="NULL" AND len(lcDataSet.StringOf("minutosespera"))!=0)
							lcWaiting_minutes= (lcDataSet.StringOf("minutosespera"))
						ENDIF
						uf_chilkat_Save_WaitingQueue(lcToken,VAL(ALLTRIM(lcDepartment_id)),VAL(ALLTRIM(lcTickets_count)),lcStart_date,lcDepartamentName, VAL(ALLTRIM(lcWaiting_minutes)),VAL(lcSiteNr))
					ELSE
						uf_perguntalt_chama("Ocorreu um erro a receber a resposta","OK","",48)
						uf_chilkat_Save_msg_status_Ticket(lcToken,lcResponseHeaderCode,"N�o existe ninguem na fila de espera")
					ENDIF
				ENDFOR
			ELSE
				uf_perguntalt_chama("Ocorreu um erro a receber a resposta","OK","",48)
				uf_chilkat_Save_msg_status_Ticket(lcToken,lcResponseHeaderCode,"N�o existe ninguem na fila de espera")
			ENDIF
		ELSE
			uf_chilkat_Save_StatusError_Ticket(lcToken,lcResponseHeaderCode)
		ENDIF
	ENDIF

	RELEASE loReq
	RELEASE loHttp
	
	RELEASE loResp

ENDFUNC 


FUNCTION uf_chilkat_send_request_CallNext_Ticket
	PARAMETERS lcDesk, lcOp, lcSite, lcUserno, lcSiteNr
	LOCAL loReq, loHttp, lnSuccess,	lnSuccess1,	lcJsonText,	loResp, lcUrl,lcResponseBody  ,lcToken,lcResponseHeaderCode ,lcTicketsId, lcCreatedDate, lcNumerator, lcDepartamentName, lcSerialNumber, lcTickets_count , lcDepartamentId, lcStartDate, lcEndDate, lcOpen, lcDeskName ,lcTstamp, lcIdType, lcOccurentyId,lcMessage, lcLocal
	STORE '' to lcResponseBody ,lcToken,lcResponseHeaderCode ,lcTicketsId, lcCreatedDate, lcNumerator, lcDepartamentName, lcSerialNumber, lcTickets_count , lcDepartamentId, lcStartDate, lcEndDate, lcOpen, lcDeskName, lcIdType,lcMessage
	uf_servicos_unblockChilkat()
	lcUrl= uf_gerais_getparameter_site('ADM0000000043', 'TEXT', lcSite)
	lcLocal = uf_gerais_getparameter_site('ADM0000000046', 'TEXT', lcSite)
	loReq = CreateObject('Chilkat_9_5_0.HttpRequest')
	loHttp = CreateObject('Chilkat_9_5_0.Http')
	loJson = CreateObject('Chilkat_9_5_0.JsonObject')
	lcPostHTTP=lcUrl + "callNext.php?" + "idbalcao="+lcDesk+"&idlocal=" + lcLocal + "&op=" + lcOp
	loHttp.AcceptCharset = ""
	loHttp.UserAgent = ""
	loHttp.AcceptLanguage = ""
	loHttp.AllowGzip = 0
	lcUser = uf_gerais_getparameter_site('ADM0000000044', 'TEXT', lcSite)
	lnSuccess = loHttp.AddQuickHeader("user",lcUser)
	lnSuccess = loHttp.AddQuickHeader("password",uf_gerais_getparameter_site('ADM0000000045', 'TEXT', lcSite))
	loHttp.ConnectTimeout = 30
	loHttp.ReadTimeout = 60
	lcToken = uf_gerais_stamp()
*!*		
*!*		if(!EMPTY(emDesenvolvimento))
*!*			_cliptext = lcPostHTTP
*!*			MESSAGEBOX(lcPostHTTP)
*!*		end	
*!*		
	
	loResp = loHttp.PostJson(lcPostHTTP,"")
	IF !USED("uCrsCallNext")
		create cursor uCrsCallNext(token c(40),status n(5) ,message c(254), serie c(30), numerator_counter n(10), total_counter n(10))
	ENDIF
	
	IF (loHttp.LastMethodSuccess <> 1) THEN
		INSERT INTO uCrsCallNext (token,status,message ,numerator_counter, total_counter) VALUES (lcToken,-1,"N�o foi poss�vel obter a pr�xima senha.",0,0)
		uf_chilkat_Save_msg_status_Ticket(lcToken,-1,STRTRAN(LEFT(ALLTRIM(loHttp.LastErrorText),4000),"'",""))
	ELSE
*!*			uf_atendimento_savetime("2","uf_ticket_call_next")
	
*!*			if(!EMPTY(emDesenvolvimento))
*!*				_cliptext = loResp.BodyStr
*!*				MESSAGEBOX(loResp.BodyStr)
*!*			ENDIF
		
		lcResponseHeaderCode = loResp.StatusCode
		IF (lcResponseHeaderCode ==200)
			loJson.Load(loResp.BodyStr)
			loJson.EmitCompact = 0
			IF (!ISNULL(loJson.Arrayof("dataset")))
				lcResponseBody = loJson.Arrayof("dataset")
				IF (lcResponseBody.size !=0 )
					IF (loJson.LastMethodSuccess !=0)
						lcDataSet = lcResponseBody.ObjectAt(0)
						IF (!ISNULL("lcDataSet"))
							IF (Alltrim(UPPER(lcDataSet.StringOf("idsenha")))!="NULL" AND len(lcDataSet.StringOf("idsenha"))!=0)
								lcTicketsId= (lcDataSet.StringOf("idsenha"))
							ENDIF 
							IF (Alltrim(UPPER(lcDataSet.StringOf("datacriacao")))!="NULL"  AND len(lcDataSet.StringOf("datacriacao"))!=0)
								lcCreatedDate = (lcDataSet.StringOf("datacriacao"))
							ENDIF
							IF  (Alltrim(UPPER(lcDataSet.StringOf("contadornumerador")))!="NULL" AND len(lcDataSet.StringOf("contadornumerador"))!=0)
								lcNumerator= (lcDataSet.StringOf("contadornumerador"))
							ENDIF  
							IF  (Alltrim(UPPER(lcDataSet.StringOf("nome")))!="NULL" AND len(lcDataSet.StringOf("nome"))!=0)
								lcDepartamentName= (lcDataSet.StringOf("nome"))
							ENDIF 
							IF (Alltrim(UPPER(lcDataSet.StringOf("serie")))!="NULL"  AND len(lcDataSet.StringOf("serie"))!=0)
								lcSerialNumber= (lcDataSet.StringOf("serie"))
							ENDIF 
							IF (Alltrim(UPPER(lcDataSet.StringOf("contadortotal")))!="NULL"  AND len(lcDataSet.StringOf("contadortotal"))!=0)
								lcTickets_count = (lcDataSet.StringOf("contadortotal"))
							ENDIF 
							IF (Alltrim(UPPER(lcDataSet.StringOf("iddepartamento")))!="NULL"  AND !isnull(lcDataSet.StringOf("iddepartamento")) AND len(lcDataSet.StringOf("iddepartamento"))!=0)
								lcDepartamentId= (lcDataSet.StringOf("iddepartamento"))	
							ENDIF 
							IF (Alltrim(UPPER(lcDataSet.StringOf("DataInicio")))!="NULL"  AND len(lcDataSet.StringOf("DataInicio"))!=0)
								lcStartDate = (lcDataSet.StringOf("DataInicio"))
							ENDIF 
							IF  (Alltrim(UPPER(lcDataSet.StringOf("DataFim")))!="NULL" AND len(lcDataSet.StringOf("DataFim"))!=0)
								lcEndDate = (lcDataSet.StringOf("DataFim"))
							ENDIF 
							IF (Alltrim(UPPER(lcDataSet.StringOf("aberto")))!="NULL"  AND len(lcDataSet.StringOf("aberto"))!=0)
								lcOpen = (lcDataSet.StringOf("aberto"))
							ENDIF 
							IF (Alltrim(UPPER(lcDataSet.StringOf("nomebalcao")))!="NULL"  AND len(lcDataSet.StringOf("nomebalcao"))!=0)
								lcDeskName = (lcDataSet.StringOf("nomebalcao"))
							ENDIF 
							lcOccurentyId = uf_servicos_getticket_occurency("chamada")
							INSERT INTO uCrsCallNext (token,status,serie ,numerator_counter, total_counter) VALUES (lcToken,lcResponseHeaderCode,lcSerialNumber ,VAL(lcNumerator),VAL(lcTickets_count))
							uf_chilkat_Save_queue_management(lcToken, lcStartDate, lcEndDate,VAL(lcOpen),VAL(lcNumerator),VAL(lcTickets_count),VAL(lcDepartamentId),VAL(lcTicketsId),lcDepartamentName,lcSerialNumber,VAL(lcSiteNr),VAL(lcLocal),myTermStamp,VAL(lcUserno),lcOccurentyId,lcCreatedDate)
						ELSE
							INSERT INTO uCrsCallNext (token,status,numerator_counter, total_counter) VALUES (lcToken,lcResponseHeaderCode,0,0)
							uf_chilkat_Save_msg_status_Ticket(lcToken,lcResponseHeaderCode,"N�o existem senhas por atender.")
						ENDIF
					ELSE	
							INSERT INTO uCrsCallNext (token,status ,numerator_counter, total_counter) VALUES (lcToken,lcResponseHeaderCode,0,0)
							uf_chilkat_Save_msg_status_Ticket(lcToken,lcResponseHeaderCode,"N�o existem senhas por atender.")
					ENDIF
				ELSE	
					INSERT INTO uCrsCallNext (token,status ,numerator_counter, total_counter) VALUES (lcToken,lcResponseHeaderCode,0,0)
					uf_chilkat_Save_msg_status_Ticket(lcToken,lcResponseHeaderCode,"N�o existem senhas por atender.")
				ENDIF
			ELSE 
				INSERT INTO uCrsCallNext (token,status ,numerator_counter, total_counter) VALUES (lcToken,lcResponseHeaderCode,0,0)
				uf_chilkat_Save_msg_status_Ticket(lcToken,lcResponseHeaderCode,"N�o existem senhas por atender.")
			ENDIF 
		ELSE		
			IF  (lcResponseHeaderCode== 401) THEN
        		lcMessage= "Autentica��o inv�lida"
        	ELSE
        		lcMessage= "Erro a establecer comunica��o com o servidor"
        	ENDIF
			INSERT INTO uCrsCallNext (token,status,message ,numerator_counter, total_counter) VALUES (lcToken,lcResponseHeaderCode,lcMessage,-1,-1)
			uf_chilkat_Save_msg_status_Ticket(lcToken, lcStatus,lcMessage)	
		ENDIF	
	ENDIF

	RELEASE loReq
	RELEASE loHttp
	
	RELEASE loResp

ENDFUNC 



FUNCTION uf_chilkat_send_request_Setmotive_Ticket
	PARAMETERS lcDesk, lcIdmotive,lcOp ,lcSite
	LOCAL loReq,lcResponseBody ,loHttp, lnSuccess,	lnSuccess1,	lcJsonText,	loResp,lcUrl,lcResponse,lcDataSet,lcResponseMotive,lcMessage ,lcToken,lcMessageUser 
	uf_servicos_unblockChilkat()
			

	lcUrl= uf_gerais_getparameter_site('ADM0000000043', 'TEXT', lcSite)
	loReq = CreateObject('Chilkat_9_5_0.HttpRequest')
	loHttp = CreateObject('Chilkat_9_5_0.Http')

	loJson = CreateObject('Chilkat_9_5_0.JsonObject')

	lcPostHTTP=	lcUrl + "setmotive.php?" + "idbalcao=" + lcDesk + "&idmotive="+lcIdmotive +"&op=" + lcOp

	loHttp.AcceptCharset = ""
	loHttp.UserAgent = ""
	loHttp.AcceptLanguage = ""
	loHttp.AllowGzip = 0
	
	lcUser = uf_gerais_getparameter_site('ADM0000000044', 'TEXT', lcSite)
	lnSuccess = loHttp.AddQuickHeader("user",lcUser)
	lnSuccess = loHttp.AddQuickHeader("password",uf_gerais_getparameter_site('ADM0000000045', 'TEXT', lcSite))
	loHttp.ConnectTimeout = 10
	loHttp.ReadTimeout = 60
	lcToken = uf_gerais_stamp()
	loResp = loHttp.PostJson(lcPostHTTP,"")

	IF !USED("uCrsSetmotive")
		create cursor uCrsSetmotive(token c(40),status n(5) ,message c(254))
	ENDIF

	
	IF (loHttp.LastMethodSuccess <> 1) THEN
		INSERT INTO uCrsSetmotive(token,status,message) VALUES (lcToken,-1,"N�o foi poss�vel obter a pr�xima senha.")
		uf_chilkat_Save_msg_status_Ticket(lcToken,-1,STRTRAN(LEFT(ALLTRIM(loHttp.LastErrorText),4000),"'",""))
	ELSE
*!*			uf_atendimento_savetime("2","uf_update_pause_motive_in_ext_service")		
		lcResponseHeaderCode = loResp.StatusCode
		IF (lcResponseHeaderCode ==200)
			loJson.Load(loResp.BodyStr)
			loJson.EmitCompact = 0
			lcResponseBody = loJson.Arrayof("dataset")
			IF(loJson.LastMethodSuccess !=0 and lcResponseBody.size !=0 )
				lcDataSet = lcResponseBody.BoolAt(0)				
				IF (!ISNULL("lcDataSet"))
					IF  (lcDataSet== 1) THEN
        				lcMessage= "Pausa ativada com sucesso."
        				lcMessageUser ="Pausa ativada com sucesso."
        			ELSE
        				lcMessage= "O motivo n�o foi validado: " +LEFT(loResp.BodyStr,240)
        				lcMessageUser ="O motivo n�o foi validado."
        			ENDIF
					INSERT INTO uCrsSetmotive(token,status,message) VALUES (lcToken,lcResponseHeaderCode ,lcMessageUser)				
					uf_chilkat_Save_msg_status_Ticket(lcToken, lcResponseHeaderCode ,lcMessage)	
				ELSE
					INSERT INTO uCrsSetmotive(token,status,message) VALUES (lcToken,lcResponseHeaderCode ,"Ocorreu um erro a receber a resposta")					
					uf_chilkat_Save_msg_status_Ticket(lcToken, lcResponseHeaderCode ,"Ocorreu um erro a receber a resposta, n�o existe DataSet")	
				ENDIF
			ELSE
				INSERT INTO uCrsSetmotive(token,status,message) VALUES (lcToken,lcResponseHeaderCode ,"Ocorreu um erro a receber a resposta")					
				uf_chilkat_Save_msg_status_Ticket(lcToken, lcResponseHeaderCode ,"Ocorreu um erro a receber a resposta, n�o existe body")	
			ENDIF
		ELSE
			IF (lcResponseHeaderCode == 401) THEN
        		lcMessage= "Autentica��o inv�lida"
        	ELSE
        		lcMessage= "Erro a establecer comunica��o com o servidor"
        	ENDIF
			INSERT INTO uCrsSetmotive(token,status,message) VALUES (lcToken,lcResponseHeaderCode ,lcMessage)
			uf_chilkat_Save_msg_status_Ticket(lcToken,lcResponseHeaderCode,lcMessage)
		ENDIF	
	ENDIF

	RELEASE loReq
	RELEASE loHttp	
	RELEASE loResp

ENDFUNC 


FUNCTION uf_chilkat_Save_StatusError_Ticket
		PARAMETERS lcToken,lcStatus
		LOCAL lcMessage	
		
		IF  (lcStatus== 401) THEN
        	lcMessage= "Autentica��o inv�lida"
        ELSE
        	lcMessage= "Erro a establecer comunica��o com o servidor"
        ENDIF
        uf_perguntalt_chama(lcMessage,"OK","",48)
        uf_chilkat_Save_msg_status_Ticket(lcToken, lcStatus,lcMessage)	
        	
ENDFUNC 

FUNCTION uf_chilkat_Save_msg_status_Ticket
		PARAMETERS lcToken,lcStatus,lcMessage
        
        lcSQL = ""
		TEXT TO lcSQL NOSHOW textmerge
			INSERT INTO ext_xopvision_msg_status (token, start_date, status, message )
			values ('<<ALLTRIM(lcToken)>>',GETDATE(),<<lcStatus>>,'<<ALLTRIM(lcMessage)>>')	
		ENDTEXT
		IF !uf_gerais_actgrelha("", "", lcSql)
			uf_perguntalt_chama("OCORREU UMA ANOMALIA A GUARDAR STATUS DAS SENHAS","OK","",16)
			RETURN .f.
		ENDIF
		
ENDFUNC 


FUNCTION uf_chilkat_Save_WaitingQueue
		PARAMETERS lcToken,lcDepartment_id,lcTickets_count,lcStart_date,lcName,lcWaiting_minutes,lcSite_nr
        
        lcSQL = ""
		TEXT TO lcSQL NOSHOW textmerge
			INSERT INTO ext_xopvision_waiting_queue(token, department_id, tickets_count, start_date, name, waiting_minutes, site_nr)
			values ('<<ALLTRIM(lcToken)>>',<<lcDepartment_id>>,<<lcTickets_count>>,convert(DATETIME,'<<lcStart_date>>'),'<<ALLTRIM(lcName)>>',<<lcWaiting_minutes>>,<<lcSite_nr>>)	
		ENDTEXT
		
		IF !uf_gerais_actgrelha("", "", lcSql)
			uf_perguntalt_chama("OCORREU UMA ANOMALIA A GUARDAR WAITINGQUEUE DAS SENHAS","OK","",16)
			RETURN .f.
		ENDIF
		
ENDFUNC 


FUNCTION uf_chilkat_Save_queue_management
		PARAMETERS lcToken,lcStart_Date,lcEnd_date,lcIsOpen,lcNumerator_counter,lcTotal_counter,lcDepartment_id, lcTicket_id, lcName,lcSerie,lcSite_nr,lcLocal_id,lcTstamp,lcUseno,lcTicketOccur,lcCreatedDate
		
		 lcSQL = ""
		TEXT TO lcSQL NOSHOW textmerge
			INSERT INTO ext_xopvision_queue_management(token,start_date,end_date,is_open,numerator_counter,total_counter,department_id,ticket_id,name,serie,site_nr,local_id,tstamp,userno,ext_ticket_occurency_id,creation_date)
			values ('<<ALLTRIM(lcToken)>>',convert(DATETIME,'<<lcStart_date>>'),convert(DATETIME,'<<lcEnd_date>>'),<<lcIsOpen>>,<<lcNumerator_counter>>,<<lcTotal_counter>>,<<lcDepartment_id>>,<<lcTicket_id>>,'<<lcName>>','<<lcSerie>>',<<lcSite_nr>>,<<lcLocal_id>>,'<<lcTstamp>>',<<lcUseno>>,<<lcTicketOccur>>,convert(DATETIME,'<<lcCreatedDate>>'))	
		ENDTEXT
		IF !uf_gerais_actgrelha("", "", lcSql)
			uf_perguntalt_chama("OCORREU UMA ANOMALIA A GUARDAR QUEUE_MANAGEMENT DAS SENHAS","OK","",16)
			RETURN .f.
		ENDIF
ENDFUNC 



FUNCTION uf_servicos_getticket_occurency
		PARAMETERS lcType
		local occurentyId
		 lcSQL = ""
		 TEXT TO lcSQL NOSHOW TEXTMERGE
		 SELECT TOP 1 
			id
		from 
			ext_ticket_occurency(nolock)
		where
			type='<<lcType>>'
							
		ENDTEXT
		IF !uf_gerais_actgrelha("", "occurentyId", lcSql)
			RETURN 0
		ENDIF 
		RETURN occurentyId.id		
ENDFUNC 


** Servi�o de autentica��o JWT no msb-router 
** valida credenciais e devolve o token se valido
** retorna cursor (code, token, tokenExpDate)
**   . -1, '','19000101' se n�o autorizado
**   .  1, 'sadasda','20200330'  se tudo ok
** uf_servicos_singInJwt('https://app.lts.pt:50001/api/auth/signInUser','ltdev30','l!t5s3rv!2k20...')

FUNCTION uf_servicos_singInJwt
	PARAMETERS lcPostHTTP, lcUser, lcPw
	LOCAL loReq, loHttp, lnSuccess,	lcJsonText,	loResp, lcStatus, lcExpDate, lcCodeI, lcCodeReturn, lcTokenReturn, lcDateReturn
	
	lcCodeReturn = -1
	lcTokenReturn = ""
	lcDateReturn = "19000101"	
	


	uf_servicos_unblockChilkat()
	
	loReq = CreateObject('Chilkat_9_5_0.HttpRequest')
	loHttp = CreateObject('Chilkat_9_5_0.Http')

	loJson = CreateObject('Chilkat_9_5_0.JsonObject')

	loHttp.AcceptCharset = ""
	loHttp.UserAgent = ""
	loHttp.AcceptLanguage = ""

	loHttp.AllowGzip = 0
	
	
	** Timeout
	** ConnectTimeout : Tempo que  demora a connectar ao servidor 
	** ReadTimeout : Tempo que demora a receber a resposta do servidor, deve variar por servico: single -> 10 segundos, mixed bulk -> para ai uns 10 minutos
	loHttp.ConnectTimeout  = 10
	loHttp.ReadTimeout = 10
	
	
	lcJsonText =  '{"user": "' + ALLTRIM(lcUser) + '", "password": "' + ALLTRIM(lcPw) + '"}'
	

	fecha("ucrsSignJwrResp")
	
	create cursor ucrsSignJwrResp (code int, token  varchar(100), tokenExpDate varchar(50))
	

	lnSuccess = loHttp.AddQuickHeader("Content-Type","application/json")

	loResp = loHttp.PostJson(lcPostHTTP,lcJsonText)

	IF (loHttp.LastMethodSuccess <> 1) THEN
			
		LOCAL lcbody 
		lcbody = loHttp.LastErrorText
		
		SELECT ucrse1
		GO TOP
			
		LOCAL lcMensagem
		lcMensagem = 'ERRO NA APLICA��O'
		lcMensagem = lcMensagem  + CHR(13) + 'Cliente: ' + ALLTRIM(ucrse1.nomecomp)
		lcMensagem = lcMensagem  + CHR(13) + 'Loja: ' + ALLTRIM(ucrse1.siteloja)
		lcMensagem = lcMensagem  + CHR(13) + 'Msg. Erro : ' + ALLTRIM(lcbody )

		uf_startup_sendmail_errors('Erro no Logitools', lcMensagem )
		
		uf_perguntalt_chama("Ocorreu uma anomalia a autenticar nos servi�os centrais de autentica��o. Por favor contacte o Suporte.","OK","",16)	    
	    INSERT INTO ucrsSignJwrResp  VALUES (lcCodeReturn ,lcTokenReturn, lcDateReturn  )
	    
	    RELEASE loReq
		RELEASE loHttp
		RELEASE loJson 
	    
	    RETURN 
	ENDIF

	RELEASE loReq
	RELEASE loHttp

	loJson.Load(loResp.BodyStr)
	loJson.EmitCompact = 0
	lcStatus= loJson.ObjectOf("status")
	


	
	** valida se o objectos status existe
	if(!uf_servicos_chilkat_isNull(lcStatus))
		 ** valida se todos os atributos est�o preenchidos
		 if(!uf_servicos_chilkat_isNull(lcStatus.IntOf("i")) and  !uf_servicos_chilkat_isNull(lcStatus.StringOf("t")) and  !uf_servicos_chilkat_isNull(loJson.StringOf("expirationDate")))
		 	** valida se tudo ok
		 	IF lcStatus.IntOf("i")==200 	
			  	lcCodeReturn = 1
				lcTokenReturn = ALLTRIM(lcStatus.StringOf("t"))
				lcDateReturn =  ALLTRIM(loJson.StringOf("expirationDate"))
			ELSE
				lcCodeReturn = -1
				lcTokenReturn = ""
				lcDateReturn = "19000101"	
		  	ENDIF
		 endif
	
	ENDIF
	


	INSERT INTO ucrsSignJwrResp  VALUES (lcCodeReturn ,lcTokenReturn, lcDateReturn  )
	


	RELEASE loResp
	
ENDFUNC 



** Servi�o de valida��o do estado no pica ponto (clockIn)
** valida se token existe ou se encontra expirado
** se receber 401 volta a tentar autenticar para receber novo token valido
** retorna cursor (code, descr) com o resultado
**   . 200, 'OK', Criado com sucesso
**   . 400, 'Bad request' Erro de estrutura
** uf_servicos_clock_status('https://app.lts.pt:50001/api/clock/relay?id=ltdev30','adm13246',.t.)


FUNCTION uf_servicos_clock_status
	PARAMETERS lcPostHTTP, lcPedStamp, lcVoltaTentar
	
	LOCAL loReq, loHttp, lnSuccess,	lcJsonText,	loResp, lcStatus, lcCodeI, lcCodeReturn 
	LOCAL  lcDescrReturn, lcShouldWait, lcPayloadJson, lcToken, lcOperation 
	
	regua(0,2,"A validar o estado do equipamento. Por favor aguarde")
	
	lcCodeReturn = 500
	lcDescrReturn = ""
	
	lcShouldWait = .t.
	
	if(EMPTY(lcPedStamp))
		lcPedStamp= ALLTRIM(uf_gerais_stamp())
	ENDIF
		
	


	uf_servicos_unblockChilkat()
	
	loReq = CreateObject('Chilkat_9_5_0.HttpRequest')
	loHttp = CreateObject('Chilkat_9_5_0.Http')

	loJson = CreateObject('Chilkat_9_5_0.JsonObject')
	



	loHttp.AcceptCharset = ""
	loHttp.UserAgent = ""
	loHttp.AcceptLanguage = ""

	loHttp.AllowGzip = 0
	
		** Timeout
	** ConnectTimeout : Tempo que  demora a connectar ao servidor 
	** ReadTimeout : Tempo que demora a receber a resposta do servidor, deve variar por servico: single -> 10 segundos, mixed bulk -> para ai uns 10 minutos
	loHttp.ConnectTimeout  = 15
	loHttp.ReadTimeout = 15

	
	** gera a parte comum do pedido a todos os servi�os clockIn
	lcJsonText = uf_servicos_clock_header("status",lcShouldWait,15,lcPedStamp)
	
	
	lcPayloadJson = ''	
	lcJsonText =  STRTRAN(lcJsonText , "PAYLOAD", lcPayloadJson)
	
	
	**
	lcToken = uf_servicos_jwt_getToken("JWT msb-router",.f.)
	
	if(EMPTY(lcToken))
		regua(2)
		uf_perguntalt_chama("TOKEN INV�LIDO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	ENDIF

	

	lnSuccess = loHttp.AddQuickHeader("x-auth-t",lcToken)
	
	loResp = loHttp.PostJson2(lcPostHTTP,"application/json",lcJsonText)
	
	IF (loHttp.LastMethodSuccess <> 1) THEN
			
		LOCAL lcbody 
		lcbody = loHttp.LastErrorText
		
		SELECT ucrse1
		GO TOP
			
		LOCAL lcMensagem
		lcMensagem = 'ERRO NA APLICA��O'
		lcMensagem = lcMensagem  + CHR(13) + 'Cliente: ' + ALLTRIM(ucrse1.nomecomp)
		lcMensagem = lcMensagem  + CHR(13) + 'Loja: ' + ALLTRIM(ucrse1.siteloja)
		lcMensagem = lcMensagem  + CHR(13) + 'Msg. Erro : ' + ALLTRIM(lcbody )

		uf_startup_sendmail_errors('Erro no Logitools', lcMensagem )
		
		uf_perguntalt_chama("O Logitools n�o obteve resposta do equipamento, por favor volte a tentar. Obrigado","OK","",48)    
	    
	    RELEASE loReq
		RELEASE loHttp
		RELEASE loJson 
	
	  	regua(2)
	    RETURN .f.
	ENDIF
	
	

	RELEASE loReq
	RELEASE loHttp
	
	
	loJson.Load(loResp.BodyStr)
	loJson.EmitCompact = 0
	lcStatus= loJson.ObjectOf("status")
	lcOperation = loJson.ObjectOf("operation")
	

	** valida se o objecto status existe
	if(!uf_servicos_chilkat_isNull(lcStatus))
		 ** valida se todos os atributos est�o preenchidos
		 if(!uf_servicos_chilkat_isNull(lcStatus.IntOf("i")))
		 	** valida se tudo ok
		 	
		 	** se recebeu unauthorized volta a tentar
		 	IF lcStatus.IntOf("i")==401 AND lcVoltaTentar	
		 		RELEASE loJson 
			  	regua(2)
			  	LOCAL lcToken 
			  	lcToken = uf_servicos_jwt_getToken("JWT msb-router",.t.)
			  	
			  	regua(2)
				RELEASE loJson 	
			  	
			  	**novo token
			  	if(!EMPTY(lcToken ))
			  	** recursiva
			  		RETURN  uf_servicos_clock_status(lcPostHTTP,lcPedStamp, .f.)	
			  	ELSE			  	  		
					uf_perguntalt_chama("Ocorreu uma anomalia a autenticar nos servi�os centrais de autentica��o." + 'Msg. Erro : ' + ALLTRIM(STR(lcStatus.IntOf("i"))) + "." + CHR(13) + "Por favor contacte o Suporte.","OK","",48)
			  		RETURN .f.	
			  	ENDIF
			 	
		 	ENDIF
		 	
			** Se n�o obteve sucesso ou erro na autentica��o 
		 	IF lcStatus.IntOf("i")!=200 
		 	
			  	uf_perguntalt_chama("O Logitools n�o obteve resposta do equipamento, por favor volte a tentar." +CHR(13) + 'Msg. Erro : ' + ALLTRIM(STR(lcStatus.IntOf("i"))) + "." + CHR(13) + + "Obrigado. ","OK","",48)
			  	RELEASE loJson 
			  	regua(2)
			  	RETURN 	.f.
		  	ENDIF
		 endif	
	ENDIF
	
	
	** valida se o operation status existe
	if(!uf_servicos_chilkat_isNull(lcOperation))
		 ** valida se todos os atributos est�o preenchidos
		 if(!uf_servicos_chilkat_isNull(lcOperation.IntOf("status")) and lcOperation.IntOf("status")!=200)
		 	** valida se tudo ok
			  	uf_perguntalt_chama("O equipamento, encontra-se com problemas. Por favor valide."+  CHR(13) + " Msg. Erro: " + ALLTRIM(STR(lcOperation.IntOf("status"))) + "." + CHR(13) + "Obrigado. ","OK","",48)
			  	regua(2)
				RELEASE loJson
			  	RETURN .f. 	
		  	
		 ELSE
		 	** valida se existe objecto payload
		 	if(uf_servicos_chilkat_isNull(lcOperation.ObjectOf("payload")))		 	
			 	uf_perguntalt_chama("O Logitools n�o conseguiu criar o operador no equipamento, por favor volte a tentar. Obrigado","OK","",48)
			 	regua(2)
				RELEASE loJson
				RETURN .f. 			
			ELSE
				**valida se existe machineState dentro do obecto payload
				local lcPayload
				lcPayload = lcOperation.ObjectOf("payload")
				if(!uf_servicos_chilkat_isNull(lcPayload.IntOf("machineState")) and lcPayload.IntOf("machineState")!=1 )
					
					LOCAL lcStatus 
					lcStatus  = ""
					**valida se existe infodentro do obecto payload
					if(!uf_servicos_chilkat_isNull(lcPayload.StringOf("info")))
						lcStatus   = ALLTRIM(lcPayload.StringOf("info"))
					ELSE
						lcStatus   = "Opera��o Inv�lida"	
					ENDIF

					uf_perguntalt_chama("O equipamento, encontra-se com problemas. Por favor valide."+  CHR(13) + " Msg. Erro: " + lcStatus   + "." + CHR(13) + "Obrigado. ","OK","",48)
				  	regua(2)
					RELEASE loJson
				  	RETURN .f. 	
				ENDIF 	
			
			ENDIF		 	
		 ENDIF	
	ELSE
		uf_perguntalt_chama("O Logitools n�o obteve resposta do equipamento, por favor volte a tentar. Obrigado","OK","",48)
		regua(2)
		RELEASE loJson
		RETURN .f. 		 
	ENDIF
	
	
	
	regua(2)
	RELEASE loJson 
	
	RETURN .t.
	

ENDFUNC




** Servi�o de cria��o de operadores no pica ponto (clockIn)
** valida se token existe ou se encontra expirado
** retorna boolean
** uf_servicos_clock_addUser('https://app.lts.pt:50001/api/clock/relay?id=ltdev30',1,.t.,'teste','',0,'adm13246')


FUNCTION uf_servicos_clock_addUser
	PARAMETERS lcPostHTTP, lcUserNo, lcEnable, lcName, lcRfId, lcPrivilege, lcPedStamp
	
	LOCAL loReq, loHttp, lnSuccess,	lcJsonText,	loResp, lcStatus, lcCodeI, lcCodeReturn 
	LOCAL  lcDescrReturn, lcShouldWait, lcPayloadJson, lcToken, lcOperation, lcResult
	
	regua(0,2,"A criar o operador no equipamento. Por favor aguarde")
	
	lcCodeReturn = 500
	lcDescrReturn = ""
	
	lcShouldWait = .t.
	

	uf_servicos_unblockChilkat()
	
	loReq = CreateObject('Chilkat_9_5_0.HttpRequest')
	loHttp = CreateObject('Chilkat_9_5_0.Http')

	loJson = CreateObject('Chilkat_9_5_0.JsonObject')
	
	if(EMPTY(lcPedStamp))
		lcPedStamp= ALLTRIM(uf_gerais_stamp())
	ENDIF
	



	loHttp.AcceptCharset = ""
	loHttp.UserAgent = ""
	loHttp.AcceptLanguage = ""

	loHttp.AllowGzip = 0
	
		** Timeout
	** ConnectTimeout : Tempo que  demora a connectar ao servidor 
	** ReadTimeout : Tempo que demora a receber a resposta do servidor, deve variar por servico: single -> 10 segundos, mixed bulk -> para ai uns 10 minutos
	loHttp.ConnectTimeout  = 10
	loHttp.ReadTimeout = 20

	
	** gera a parte comum do pedido a todos os servi�os clockIn
	lcJsonText = uf_servicos_clock_header("addUser",lcShouldWait,10,lcPedStamp)
	
	
	lcPayloadJson = ''
	lcPayloadJson = lcPayloadJson + '"userId": '+ALLTRIM(STR(lcUserNo))+','
	lcPayloadJson = lcPayloadJson + '"name": "'+ALLTRIM(lcName)+'",'
	lcPayloadJson = lcPayloadJson + '"privilege": '+ALLTRIM(STR(lcPrivilege))+','
	lcPayloadJson = lcPayloadJson + '"enabled": '+IIF(lcEnable=.t., 'true', 'false')+','
	lcPayloadJson = lcPayloadJson + '"rfIdCard": "'+ALLTRIM(lcRfId)+'"'
	
	lcJsonText =  STRTRAN(lcJsonText , "PAYLOAD", lcPayloadJson)
	
	**
	lcToken = uf_servicos_jwt_getToken("JWT msb-router",.f.)
	
	if(EMPTY(lcToken))
		regua(2)
		uf_perguntalt_chama("TOKEN INV�LIDO. POR FAVOR CONTACTE O SUPORTE.","OK","",48)
		RETURN .f.
	ENDIF
	
	
	

	lnSuccess = loHttp.AddQuickHeader("x-auth-t",lcToken)
	
	loResp = loHttp.PostJson2(lcPostHTTP,"application/json",lcJsonText)
	
	IF (loHttp.LastMethodSuccess <> 1) THEN
			
		LOCAL lcbody 
		lcbody = loHttp.LastErrorText
		
		SELECT ucrse1
		GO TOP
			
		LOCAL lcMensagem
		lcMensagem = 'ERRO NA APLICA��O'
		lcMensagem = lcMensagem  + CHR(13) + 'Cliente: ' + ALLTRIM(ucrse1.nomecomp)
		lcMensagem = lcMensagem  + CHR(13) + 'Loja: ' + ALLTRIM(ucrse1.siteloja)
		lcMensagem = lcMensagem  + CHR(13) + 'Msg. Erro : ' + ALLTRIM(lcbody )

		uf_startup_sendmail_errors('Erro no Logitools', lcMensagem )
		
		uf_perguntalt_chama("Ocorreu uma anomalia a autenticar nos servi�os centrais de autentica��o. Por favor contacte o Suporte.","OK","",48)	    
	   
	    
	    
	    RELEASE loReq
		RELEASE loHttp
		RELEASE loJson 
	
	  	regua(2)
	    RETURN .f.
	ENDIF
	
	

	RELEASE loReq
	RELEASE loHttp
	
	
	loJson.Load(loResp.BodyStr)
	loJson.EmitCompact = 0
	lcStatus= loJson.ObjectOf("status")
	lcOperation = loJson.ObjectOf("operation")
		

	** valida se o objecto status existe
	if(!uf_servicos_chilkat_isNull(lcStatus))
		 ** valida se todos os atributos est�o preenchidos
		 if(!uf_servicos_chilkat_isNull(lcStatus.IntOf("i")))
		 	** valida se tudo ok
		 	IF lcStatus.IntOf("i")!=200 	
			  	uf_perguntalt_chama("O Logitools n�o obteve resposta do equipamento, por favor volte a tentar." + CHR(13) + " Msg. Erro: " + ALLTRIM(STR(lcStatus.IntOf("i"))) + "." + CHR(13) + "Obrigado.","OK","",48)
			  	RELEASE loJson 
			  	regua(2)
			  	RETURN 	.t.
		  	ENDIF
		 endif	
	ENDIF
	
	
	** valida se o operation status existe
	if(!uf_servicos_chilkat_isNull(lcOperation))
		 ** valida se todos os atributos est�o preenchidos
		 if(!uf_servicos_chilkat_isNull(lcOperation.IntOf("status")))
		 	** valida se tudo ok
		 	IF lcOperation.IntOf("status")!=200 	
			  	uf_perguntalt_chama("O Logitools n�o conseguiu criar o operador no equipamento, por favor volte a tentar." + CHR(13) + " Msg. Erro: " + ALLTRIM(STR(lcOperation.IntOf("status"))) + "." + CHR(13) + "Obrigado.","OK","",48)
			  	lcResult = .f.
			ELSE
				uf_perguntalt_chama("O Logitools conseguiu criar o operador no equipamento. Obrigado","OK","",64)
				lcResult = .t.	 	
		  	ENDIF
		 ELSE
		 	uf_perguntalt_chama("O Logitools n�o conseguiu criar o operador no equipamento, por favor volte a tentar. Obrigado","OK","",48) 
		 	lcResult = .f.	
		 ENDIF	
	ELSE
		uf_perguntalt_chama("1 O Logitools n�o obteve resposta do equipamento, por favor volte a tentar. Obrigado","OK","",48)
		lcResult = .f.		 
	ENDIF
	
	
	
	regua(2)
	RELEASE loJson 
	
	RETURN lcResult 
	

ENDFUNC


FUNCTION uf_servicos_jwt_getToken
	LPARAMETERS lcServiceType, lcForceLogin
	LOCAL lcSQL, lcTokenReq, lcUsername, lcPw, lcTokenExpDate, loBd
	lcTokenReq = '' 
	
	
	
	fecha("ucrsToken")
	

	TEXT TO lcSQL NOSHOW TEXTMERGE
		select 
			top 1
			username,
			email,
			password,
			token = case when  tokenExpDate > DATEADD(mi,-120,GETDATE()) then token else '' end
		from 
			service_credentials (nolock)
		where
			service= '<<ALLTRIM(lcServiceType)>>'
			and site_nr  = <<mysite_nr>>
		order by ousrdata desc

	ENDTEXT
	
	IF !uf_gerais_actgrelha("", "ucrsToken", lcSql)
		uf_perguntalt_chama("OCORREU UM ERRO A LER DADOS DA TABELA DE SERVI�OS. POR FAVOR CONTACTE O SUPORTE.","OK","",48)
		RETURN ''
	ENDIF
	
	IF(RECCOUNT("ucrsToken")==0)
		RETURN ''
	ENDIF
	
	
	lcTokenReq = ALLTRIM(ucrsToken.token)
	lcUsername  = ALLTRIM(ucrsToken.username)
	lcPw =  ALLTRIM(ucrsToken.password)
	

	if(!empty(lcPw))
		** decode base 64
		lcPw = STRCONV(lcPw ,14) 
	ENDIF
	

	
	fecha("ucrsToken")

	if(EMPTY(lcTokenReq ) OR lcForceLogin)

		** retornar o cursor ucrsSignJwrResp  
		uf_servicos_singInJwt("https://app.lts.pt:50001/api/auth/signInUser",lcUsername,lcPw )
		
		SELECT ucrsSignJwrResp 
		GO TOP
		IF ucrsSignJwrResp.code == 1
			lcTokenReq = ALLTRIM(ucrsSignJwrResp.token)
			lcTokenDate = ALLTRIM(ucrsSignJwrResp.tokenExpDate)
			
			if(EMPTY(lcTokenDate))
				lcTokenDate = "1900-01-01"
			endif
			
			
			lcSQL = ""
			
			TEXT TO lcSQL TEXTMERGE NOSHOW
				update service_credentials 
				SET 
					token =  '<<Alltrim(lcTokenReq)>>', 
					tokenExpDate =  '<<Alltrim(lcTokenDate )>>' 
				where
					service='JWT msb-router'
					and site_nr  = <<mysite_nr>>	
					and username =  '<<Alltrim(lcUsername)>>' 
			ENDTEXT
			uf_gerais_actGrelha("","",lcSQL)
			
	
		ENDIF
		
		fecha("ucrsSignJwrResp")
	
	ENDIF

	
	
	RETURN lcTokenReq 

ENDFUNC 



FUNCTION uf_servicos_clock_header
	PARAMETERS lcOperationType, lcShouldWait, lcTimeOut, lcPedStamp
	LOCAL lcSendMsg, loDateTime, lnBLocal, lcUnixtime
	

	loDateTime = CreateObject('Chilkat_9_5_0.CkDateTime')
	uf_servicos_unblockChilkat()
	loDateTime.SetFromCurrentSystemTime()

	lnBLocal = 1
	lcUnixtime = ALLTRIM(loDateTime.GetAsUnixTimeStr(lnBLocal))
	RELEASE loDateTime


	
	SELECT ucrse1
	GO TOP

	lcSendMsg = ''
	lcSendMsg = lcSendMsg + '{'
	lcSendMsg = lcSendMsg + '"operation": {'
	lcSendMsg = lcSendMsg + '"machineId": "'+ALLTRIM(myTipoClock)+'",'
	lcSendMsg = lcSendMsg + '"operationType": "'+ALLTRIM(lcOperationType)+'",'
	lcSendMsg = lcSendMsg + '"sender": "'+LOWER(ALLTRIM(ucrse1.id_lt))+'",'
	lcSendMsg = lcSendMsg + '"terminal": "'+LOWER(ALLTRIM(myTerm))+'",'
	lcSendMsg = lcSendMsg + '"timestamp": '+ALLTRIM(lcUnixtime)+','
	lcSendMsg = lcSendMsg + '"token": "'+ALLTRIM(lcPedStamp)+'",'
	lcSendMsg = lcSendMsg + '"shouldWait": '+IIF(lcShouldWait=.t., 'true', 'false')+','
	lcSendMsg = lcSendMsg + '"timeout": '+ALLTRIM(STR(lcTimeOut))+','
	lcSendMsg = lcSendMsg + '"siteNr": '+ALLTRIM(STR(mySite_nr))+','
	lcSendMsg = lcSendMsg + '"payload": { PAYLOAD } '
	lcSendMsg = lcSendMsg + '}}'

	RETURN lcSendMsg 
	
ENDFUNC 





FUNCTION uf_servicos_chilkat_isNull
	PARAMETERS lcObject
	
	LOCAL lcIsNull, lcType 

	
	lcType  = ALLTRIM(VARTYPE(lcObject))
	lcIsNull = .t.

	DO CASE
		** Object 
		CASE lcType = "O" 
	
		  IF ISNULL(lcObject) OR lcObject==NULL
		  	lcIsNull  = .t.
		  ELSE
		  	lcIsNull =.f.	
		  ENDIF
		  
		** String or caracter	  
		CASE lcType = "X" OR lcType = "C"
	
		  IF ISNULL(lcObject)  OR  lcObject==".NULL." OR lcObject==NULL
		  	lcIsNull  = .t.
		  ELSE
		  	lcIsNull =.f.	
		  ENDIF
		
		** Numeric
		CASE lcType = "N" 
	
		  IF ISNULL(lcObject) OR lcObject==NULL
		  	lcIsNull  = .t.
		  ELSE
		  	lcIsNull =.f.	
		  ENDIF

	OTHERWISE
	 
	ENDCASE
	
	
	RETURN  lcIsNull 


ENDFUNC 

FUNCTION uf_servicos_getJsonVacinacaoDetalhes
	LPARAM lcSearch, lcCode
	LOCAL lcNome,lcAssoc,lcId,lcSendMsg, lcOperatorId, lcOperatorName
	STORE '' TO lcNome,lcAssoc,lcId,lcSendMsg, lcOperatorId, lcOperatorName
	
	if(USED("ucrse1"))
	SELECT ucrse1
	GO TOP
		lcNome = ALLTRIM(ucrse1.nomAbrv)
		lcAssoc= ALLTRIM(ucrse1.u_assfarm)
		lcId= ALLTRIM(ucrse1.id_lt)
		lcInfarmed = ALLTRIM(STR(ucrse1.u_infarmed))
	ENDIF

	lcSendMsg = '{'
	lcSendMsg = lcSendMsg  + '"vaccine": {'

	lcSendMsg = lcSendMsg + '"token": "'+ALLTRIM(uf_gerais_stamp())+'",'
	
	if(!EMPTY(lcSearch))
		lcSendMsg = lcSendMsg + '"search": "'+ALLTRIM(lcSearch)+'",'
	ENDIF
	
	if(!EMPTY(lcCode))
		lcSendMsg = lcSendMsg + '"code": "'+ALLTRIM(lcCode)+'",'
	ENDIF
	
	lcSendMsg = lcSendMsg + '"entity": {'
	IF !EMPTY(myServicosEntApp )
		lcSendMsg = lcSendMsg + '"app": "'+ALLTRIM(myServicosEntApp)+'",'
	ENDIF 
	IF !EMPTY(myServicosEntId)
		lcSendMsg = lcSendMsg + '"id": "'+myServicosEntId+'",'
	ENDIF 
	IF !EMPTY(myServicosEntName)
		lcSendMsg = lcSendMsg + '"name": "'+ALLTRIM(myServicosEntName)+'"'
	ENDIF 
	lcSendMsg = lcSendMsg + '},'
			

	fecha("uCrsTempBus")
	
	TEXT TO lcSQL TEXTMERGE NOSHOW
		select nome,drordem as ordem,drcedula as cedula from b_us(nolock)  WHERE userno=<<ch_vendedor>> order by nome 
	ENDTEXT
	If !uf_gerais_actGrelha("", "uCrsTempBus", lcSql)
		uf_perguntalt_chama("N�o foi possivel encontrar a informa��o do operador.", "", "OK", 16)
		RETURN .f.
	ENDIF
	
	if(USED("uCrsTempBus"))
		SELECT uCrsTempBus
		GO TOP
		lcOperatorId = ALLTRIM(uCrsTempBus.ordem)
		lcOperatorName= ALLTRIM(uCrsTempBus.nome)	
	ENDIF
	
	if(!EMPTY(ALLTRIM(lcOperatorId)) and !EMPTY(ALLTRIM(lcOperatorName)) )	
		lcSendMsg = lcSendMsg + '"professional": {'
		lcSendMsg = lcSendMsg + '"id": "'+ALLTRIM(lcOperatorId)+'",'
		lcSendMsg = lcSendMsg + '"name": "'+ALLTRIM(lcOperatorName)+'"'
		lcSendMsg = lcSendMsg + '},'
	ENDIF
	
	
	
	fecha("uCrsTempBus")

	lcSendMsg = lcSendMsg + '"sender": {'
	lcSendMsg = lcSendMsg + '"id": "'+ALLTRIM(lcId)+'",'
	lcSendMsg = lcSendMsg + '"name": "'+ALLTRIM(lcNome )+'",'
	lcSendMsg = lcSendMsg + '"assoc": "'+ALLTRIM(lcAssoc)+'",'
	lcSendMsg = lcSendMsg + '"app": "'+ALLTRIM(myServicosEntApp)+'",'
	lcSendMsg = lcSendMsg + '"assocId": "'+ALLTRIM(lcInfarmed )+'"'
	lcSendMsg = lcSendMsg + '}'
	lcSendMsg = lcSendMsg + '}'
	lcSendMsg = lcSendMsg + '}'

	
	
	RETURN lcSendMsg 


ENDFUNC


**uf_servicos_getVacinacaoDetalhes("vaccinesCalendar","A consultar hist�rico","123132132")
FUNCTION uf_servicos_getVacinacaoDetalhes
	LPARAMETERS lcUrlSufixo, lcMensagem, lcSearch, lcCode
	
	LOCAL loReq, loHttp, lnSuccess,	lnSuccess1,	lcJsonText,	loResp, lcToken, lcPostHTTP
	
	uf_gerais_meiaRegua(lcMensagem)
	
	
	lcToken = 'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3f'
	
	
	IF uf_gerais_getparameter("ADM0000000227", "BOOL")==.T.
       lcPostHTTP = "https://app.lts.pt:50001/api/afp/docs/"+ALLTRIM(lcUrlSufixo)+"-test"
    ELSE
       lcPostHTTP = "https://app.lts.pt:50001/api/afp/docs/"+ALLTRIM(lcUrlSufixo)
    ENDIF

	uf_servicos_unblockChilkat()
	
	loReq = CreateObject('Chilkat_9_5_0.HttpRequest')
	loHttp = CreateObject('Chilkat_9_5_0.Http')

	loJson = CreateObject('Chilkat_9_5_0.JsonObject')

	loHttp.AcceptCharset =  "utf-8,ISO-8859-1;q=0.7,*;q=0.7"
	loHttp.UserAgent = "logitools"
	loHttp.AcceptLanguage = ""
	* Suppress the Accept-Encoding header by disallowing 
	* a gzip response:
	loHttp.AllowGzip = 0
	loHttp.ConnectTimeout  = 10
	loHttp.ReadTimeout = 30
	
	

		
	*  AddQuickHeader:
	lnSuccess = loHttp.AddQuickHeader("Content-Type","application/json; charset=utf-8")
	lnSuccess1 = loHttp.SetRequestHeader("o-auth-token",lcToken)

	
	LOCAL lcText
	lcText = ""
	
	

	lcText  = uf_servicos_getJsonVacinacaoDetalhes(lcSearch, lcCode)



	loResp = loHttp.PostJson2(lcPostHTTP,"application/json; charset=utf-8",lcText)
	
*!*		_CLIPTEXT = lcText
*!*		MESSAGEBOX(lcText)
*!*		MESSAGEBOX(lcPostHTTP)
		
	IF (loHttp.LastMethodSuccess <> 1) THEN
		    lcBody = ALLTRIM(loHttp.LastErrorText)
*!*			    _cliptext = lcBody 
*!*			    MESSAGEBOX(lcBody )
		    LOCAL lcHeader
		    lcHeader = ""
		  
	    	if(USED("uCrsE1"))
	    		SELECT uCrsE1
	    		GO TOP
	    		
	    		lcHeader = "Chilkat GetVacinacao : " + ALLTRIM(uCrsE1.id_lt)
	    	ENDIF
		    	
				uf_startup_sendmail_errors(lcHeader ,lcBody)
			
			RELEASE loReq
			RELEASE loHttp
			RELEASE loJson 
			
			regua(2)
			
			uf_perguntalt_chama("ERRO A RECEBER DADOS DE  VACINACAO!" ,"OK","", 16)	
			
			RETURN -1		
	ELSE
		lcResponseHeaderCode = loResp.StatusCode
	
		&&MESSAGEBOX(lcResponseHeaderCode )
		LOCAL lcResp 
		lcResp = .f.
		LOCAL lcReturnI ,lcReturnS  
		
		lcReturnI = ""
		lcReturnS = ""
		
		regua(2)

		if(EMPTY(loResp.BodyStr))
			RETURN -1
		ENDIF
		
*!*			_cliptext = loResp.BodyStr
*!*			MESSAGEBOX(loResp.BodyStr)

		loJson.Load(loResp.BodyStr)
		loJson.EmitCompact = 0

		IF(uf_servicos_chilkat_isNull(loJson.ObjectOf("status")))
			uf_perguntalt_chama("ERRO A RECEBER ESTADO DE VACINACAO!" ,"OK","", 16)	
			RETURN -1
		ELSE		
			lcRev = loJson.ObjectOf("status")
			lcReturnI = (lcRev.StringOf("i")) 
			lcReturnS = (lcRev.StringOf("s"))
	
		ENDIF

		IF  lcResponseHeaderCode =200					
			IF(lcReturnI=="200")
				lcResp = 1
                IF(!uf_servicos_chilkat_isNull(loJson.arrayof("result")))
                    uf_servicos_criaCursorResultadoVacinacao(loJson.arrayof("result"))
                ENDIF
                
                IF(!uf_servicos_chilkat_isNull(loJson.ObjectOf("result")))
                    uf_servicos_criaCursorResultadoCalendarioVacinacao(loJson.ObjectOf("result"))
                ENDIF

			ELSE
				uf_perguntalt_chama(LEFT("ERRO COMUNICA��O VACINACAO: " + ALLTRIM(lcReturnS),254) ,"OK","", 16)	
				lcResp = -1	
			ENDIF
			
		ELSE
			uf_perguntalt_chama(LEFT("ERRO NA LIGA��O DA COMUNICA��O DA VACINACAO: " + ALLTRIM(lcReturnS),254) ,"OK","", 16)	
			lcResp = -1	
		ENDIF 

		RELEASE loReq
		RELEASE loHttp
		RELEASE loJson 
		
		RETURN lcResp 
	
	ENDIF
	
	RETURN 0
	
ENDFUNC

FUNCTION  uf_servicos_criaCursorResultadoCalendarioVacinacao()
    LPARAMETERS lcResult
	
	LOCAL i, lcObjPatientStatus, lcObjInfectionDates, lcObjInfectionDatesSize , lcObjLastRegistration, lcObjNoVacMotives, lcObjNoVacMotivesSize, lcObjregistrations, lcObjregistrationsSize, lcObjregistrations, lcObjregistrationsSize
	LOCAL lcObjVaccineInfo, lcObjVaccineType, lcObjVaccineInfoAdministrationWay, lcResultVaccineInfo, lcResultComercialNames 
	LOCAL lcObjVaccineInfoAnatomicLocation, lcObjVaccineInfoAnatomicLocationSize, lcObjVaccineInfoAdverseReaction
	LOCAL lcObjVaccineInfoNoVaccinationMotive, lcObjVaccineInfoNoVaccinationMotiveSize, lcObjVaccineInfoDiseases, lcObjVaccineInfoDiseasesSize
	LOCAL lcObjVaccineInfoWarnings, lcObjVaccineInfoWarningsSize, lcObjCommercialNames, lcObjCommercialNamesSize, lcObjCommercialNamesLots, lcObjCommercialNamesLotsSize
	LOCAL lcObjCommercialNamesDosages, lcObjCommercialNamesDosagesSize, lcObjAnatomicLocations, lcObjAnatomicLocationsSize
	LOCAL lcObjAdministrationWays, lcObjAdministrationWaysSize, lcObjLotOrigin, lcObjLotOriginSize, lcObjSide, lcObjSideSize
	LOCAL lcObjLocationAdms, lcObjLocationAdmsSize, lcObjCommercialNamesInfarmedCode, lcObjCommercialNamesInfarmedCodeInfarmedCodeSize
	
	STORE 0 TO i, lcObjInfectionDatesSize, lcObjNoVacMotivesSize, lcObjregistrationsSize, lcObjregistrationsSize
	STORE 0 TO lcObjVaccineInfoAnatomicLocationSize, lcObjCommercialNamesInfarmedCodeSize
	STORE 0 TO lcObjVaccineInfoNoVaccinationMotiveSize, lcObjVaccineInfoDiseasesSize, lcObjVaccineInfoWarningsSize, lcObjCommercialNamesSize, lcObjCommercialNamesLotsSize
	STORE 0 TO lcObjCommercialNamesDosagesSize, lcObjAnatomicLocationsSize, lcObjAdministrationWaysSize, lcObjLotOriginSize, lcObjSideSize, lcObjLocationAdmsSize
	
	STORE '' TO lcObjPatientStatus, lcObjInfectionDates, lcObjLastRegistration, lcObjNoVacMotives, lcObjregistrations, lcObjregistrations 
	STORE '' TO lcObjVaccineInfo, lcObjVaccineType, lcObjVaccineInfoAdministrationWay , lcObjVaccineInfoAnatomicLocation, lcObjVaccineInfoAdverseReaction
	STORE '' TO lcObjVaccineInfoNoVaccinationMotive, lcObjVaccineInfoDiseases, lcObjVaccineInfoWarnings, lcObjCommercialNames, lcObjCommercialNamesLots, lcObjCommercialNamesInfarmedCode, lcResultComercialNames 
	STORE '' TO lcObjCommercialNamesDosages, lcObjAnatomicLocations, lcObjAdministrationWays, lcObjLotOrigin, lcObjSide, lcObjLocationAdms, lcResultVaccineInfo

	IF lcResult.size<1
		RETURN .F.
	ENDIF
	
	** objeto raiz locationAdms	
	IF(!uf_servicos_chilkat_isNull(lcResult.arrayof("locationAdms")))
		lcObjLocationAdms = lcResult.arrayof("locationAdms")
		lcObjLocationAdmsSize = lcObjLocationAdms.size
	ENDIF

	** objeto raiz side
	IF(!uf_servicos_chilkat_isNull(lcResult.arrayof("side")))
		lcObjSide = lcResult.arrayof("side")
		lcObjSideSize = lcObjSide.size
	ENDIF
	
	** objeto raiz lotOrigin	
	IF(!uf_servicos_chilkat_isNull(lcResult.arrayof("lotOrigin")))
		lcObjLotOrigin = lcResult.arrayof("lotOrigin")
		lcObjLotOriginSize = lcObjLotOrigin.size
	ENDIF

	**objeto da raiz admisntrationWays
	IF(!uf_servicos_chilkat_isNull(lcResult.arrayof("administrationWays")))
		lcObjAdministrationWays = lcResult.arrayof("administrationWays")
		lcObjAdministrationWaysSize = lcObjAdministrationWays.size
	ENDIF

	**objeto da raiz anatomic Locations 
	IF(!uf_servicos_chilkat_isNull(lcResult.arrayof("anatomicLocations")))
		lcObjAnatomicLocations = lcResult.arrayof("anatomicLocations")
		lcObjAnatomicLocationsSize = lcObjAnatomicLocations.size
	ENDIF
	
	** objeto raiz patient status
	IF(!uf_servicos_chilkat_isNull(lcResult.ObjectOf("patientStatus")))
		lcObjPatientStatus = lcResult.ObjectOf("patientStatus")
	ENDIF
	
	** objeto raiz infectionDates
	IF(!uf_servicos_chilkat_isNull(lcResult.arrayof("infectionDates")))
		lcObjInfectionDates = lcResult.arrayof("infectionDates")
		lcObjInfectionDatesSize = lcObjInfectionDates.size
	ENDIF

	** objeto raiz lastRegistration
	IF(!uf_servicos_chilkat_isNull(lcResult.ObjectOf("lastRegistration")))
		lcObjLastRegistration = lcResult.ObjectOf("lastRegistration")
	ENDIF
	
	** objeto raiz noVacMotives
	IF(!uf_servicos_chilkat_isNull(lcResult.arrayof("noVacMotives")))
		lcObjNoVacMotives = lcResult.arrayof("noVacMotives")
		lcObjNoVacMotivesSize = lcObjNoVacMotives.size
	ENDIF
	
	** objeto raiz registrations
	IF(!uf_servicos_chilkat_isNull(lcResult.arrayof("registrations")))
		lcObjregistrations = lcResult.arrayof("registrations")
		lcObjregistrationsSize = lcObjregistrations.size
	ENDIF
		
	** objeto raiz Vaccine Info
	IF(!uf_servicos_chilkat_isNull(lcResult.ObjectOf("vaccineInfo")))
		lcObjVaccineInfo = lcResult.ObjectOf("vaccineInfo")
		lcResultVaccineInfo = lcResult.ObjectOf("vaccineInfo")
	ENDIF 

		** objeto dentro do VaccineInfo - Warnigs 
		IF(!uf_servicos_chilkat_isNull(lcResultVaccineInfo.arrayof("warnings")))
			lcObjVaccineInfoWarnings= lcResultVaccineInfo.arrayof("warnings")
			lcObjVaccineInfoWarningsSize = lcObjVaccineInfoWarnings.size
		ENDIF

		** objeto dentro do VaccineInfo - diseases
		IF(!uf_servicos_chilkat_isNull(lcResultVaccineInfo.arrayof("diseases")))
			lcObjVaccineInfoDiseases = lcResultVaccineInfo.arrayof("diseases")
			lcObjVaccineInfoDiseasesSize = lcObjVaccineInfoDiseases.size
		ENDIF
		
		** objeto dentro do VaccineInfo - noVaccinationMotive
		IF(!uf_servicos_chilkat_isNull(lcResultVaccineInfo.arrayof("noVaccinationMotive")))
			lcObjVaccineInfoNoVaccinationMotive = lcResultVaccineInfo.arrayof("noVaccinationMotive")
			lcObjVaccineInfoNoVaccinationMotiveSize = lcObjVaccineInfoNoVaccinationMotive.size
		ENDIF
		
		** objeto dentro do VaccineInfo - adverseReaction
		IF(!uf_servicos_chilkat_isNull(lcResultVaccineInfo.ObjectOf("adverseReaction")))
			lcObjVaccineInfoAdverseReaction = lcResultVaccineInfo.ObjectOf("adverseReaction")
		ENDIF
		
		** objeto dentro do VaccineInfo - anatomicLocation
		IF(!uf_servicos_chilkat_isNull(lcResultVaccineInfo.arrayof("anatomicLocation")))
			lcObjVaccineInfoAnatomicLocation = lcResultVaccineInfo.arrayof("anatomicLocation")
			lcObjVaccineInfoAnatomicLocationSize = lcObjVaccineInfoAnatomicLocation.size
		ENDIF
		
		** objeto dentro do VaccineInfo - administrationWay
		IF(!uf_servicos_chilkat_isNull(lcResultVaccineInfo.ObjectOf("administrationWay")))
			lcObjVaccineInfoAdministrationWay = lcResultVaccineInfo.ObjectOf("administrationWay")
		ENDIF
		
		** objeto dentro do VaccineInfo - vaccineType
		IF(!uf_servicos_chilkat_isNull(lcResultVaccineInfo.ObjectOf("vaccineType")))
			lcObjVaccineType = lcResultVaccineInfo.ObjectOf("vaccineType")
		ENDIF
		
	** objeto raiz Comercial Names
	IF(!uf_servicos_chilkat_isNull(lcResult.arrayof("commercialNames")))
		lcObjCommercialNames = lcResult.arrayof("commercialNames")
		lcObjCommercialNamesSize = lcObjCommercialNames.size		
		lcResultComercialNames = lcResult.arrayof("commercialNames")
	ENDIF
		
		** objeto dentro do Comercial Names - infarmedCode
		IF(!uf_servicos_chilkat_isNull(lcResultComercialNames.ArrayOf("infarmedCode")))
			lcObjCommercialNamesInfarmedCode = lcResultComercialNames.ArrayOf("infarmedCode")
			lcObjCommercialNamesInfarmedCodeSize = lcObjCommercialNamesInfarmedCode.size
		ENDIF

		** objeto dentro do comercialNames - dosages 
		IF(!uf_servicos_chilkat_isNull(lcResultComercialNames.arrayof("dosages")))
			lcObjCommercialNamesDosages = lcResultComercialNames.arrayof("dosages")
			lcObjCommercialNamesDosagesSize = lcObjCommercialNamesDosages.size
		ENDIF

		** objeto dentro do comercialNames - lots 	
		IF(!uf_servicos_chilkat_isNull(lcResultComercialNames.arrayof("lots")))
			lcObjCommercialNamesLots = lcResultComercialNames.arrayof("lots")
			lcObjCommercialNamesLotsSize = lcObjCommercialNamesLots.size
		ENDIF

	
	** objeto raiz patientStatus
	IF(!uf_servicos_chilkat_isNull(lcResult.ObjectOf("patientStatus")))
		
		fecha("ucrsPatientStatus")
		CREATE CURSOR ucrsPatientStatus (description c(254) NULL, obs c(254) NULL, status n(5) NULL)
  
  	        SELECT ucrsPatientStatus 
            APPEND BLANK
            IF(!uf_servicos_chilkat_isNull(lcObjPatientStatus.stringof("description"))) 
                SELECT ucrsPatientStatus 
                REPLACE description WITH lcObjPatientStatus.stringof("description")
            ENDIF 
            IF(!uf_servicos_chilkat_isNull(lcObjPatientStatus.stringof("obs"))) 
                SELECT ucrsPatientStatus 
                REPLACE obs WITH lcObjPatientStatus.stringof("obs")
            ENDIF  
            IF(!uf_servicos_chilkat_isNull(lcObjPatientStatus.intof("status"))) 
                SELECT ucrsPatientStatus 
                REPLACE status WITH lcObjPatientStatus.intof("status")
            ENDIF   
	ENDIF

	i=0
	** objeto raiz InfectionDates
	IF(lcObjInfectionDatesSize >0)
		fecha("ucrsInfectionDates")
		CREATE CURSOR ucrsInfectionDates(date c(200) NULL, number n(5) NULL)
		DO WHILE i < lcObjInfectionDatesSize 
	        IF(!uf_servicos_chilkat_isNull(lcObjInfectionDates.ObjectAt(i)))
                SELECT ucrsInfectionDates
                APPEND BLANK
                IF(!uf_servicos_chilkat_isNull(lcObjInfectionDates.ObjectAt(i).stringof("date"))) 
                    SELECT ucrsInfectionDates
                    REPLACE date WITH lcObjInfectionDates.ObjectAt(i).stringof("date")
                ENDIF  
                IF(!uf_servicos_chilkat_isNull(lcObjInfectionDates.ObjectAt(i).intof("number"))) 
                    SELECT ucrsInfectionDates
                    REPLACE number WITH lcObjInfectionDates.ObjectAt(i).intof("number")
                ENDIF  
	        ENDIF	  
	        i = i+1
		ENDDO
	ENDIF


	i=0	
	** obejto raiz last Registration
	IF(!uf_servicos_chilkat_isNull(lcResult.ObjectOf("lastRegistration")))
		
		fecha("ucrsLastRegistration")
		CREATE CURSOR ucrsLastRegistration(date c(254) NULL, inocNumber n(5) NULL, vaccineCode c(254) NULL)
  
  	        SELECT ucrsLastRegistration
            APPEND BLANK
            IF(!uf_servicos_chilkat_isNull(lcObjLastRegistration.stringof("date"))) 
                SELECT ucrsLastRegistration
                REPLACE date WITH lcObjLastRegistration.stringof("date")
            ENDIF 
            IF(!uf_servicos_chilkat_isNull(lcObjLastRegistration.intof("inocNumber"))) 
                SELECT ucrsLastRegistration
                REPLACE inocNumber with lcObjLastRegistration.intof("inocNumber")
            ENDIF 
            IF(!uf_servicos_chilkat_isNull(lcObjLastRegistration.stringof("vaccineCode"))) 
                SELECT ucrsLastRegistration
                REPLACE vaccineCode WITH lcObjLastRegistration.stringof("vaccineCode")
            ENDIF  	          
	ENDIF

	i=0	
	** objeto raiz NoVacMotives
	IF(lcObjNoVacMotivesSize >0)
		fecha("ucrsNoVacMotives")
		CREATE CURSOR ucrsNoVacMotives(description c(254) NULL, startDate c(254) NULL,noVaccinationMotiveGroup c(254) NULL, vaccineCode c(254) NULL, obs c(254) NULL)
		DO WHILE i < lcObjNoVacMotivesSize 
	        IF(!uf_servicos_chilkat_isNull(lcObjNoVacMotives.ObjectAt(i)))
                SELECT ucrsNoVacMotives
                APPEND BLANK
                IF(!uf_servicos_chilkat_isNull(lcObjNoVacMotives.ObjectAt(i).stringof("description"))) 
                    SELECT ucrsNoVacMotives
                    REPLACE description WITH lcObjNoVacMotives.ObjectAt(i).stringof("description")
                ENDIF  
				IF(!uf_servicos_chilkat_isNull(lcObjNoVacMotives.ObjectAt(i).stringof("startDate"))) 
                    SELECT ucrsNoVacMotives
                    REPLACE startDate WITH lcObjNoVacMotives.ObjectAt(i).stringof("startDate")
                ENDIF
                IF(!uf_servicos_chilkat_isNull(lcObjNoVacMotives.ObjectAt(i).stringof("noVaccinationMotiveGroup"))) 
                    SELECT ucrsNoVacMotives
                    REPLACE noVaccinationMotiveGroup WITH lcObjNoVacMotives.ObjectAt(i).stringof("noVaccinationMotiveGroup")
                ENDIF
                IF(!uf_servicos_chilkat_isNull(lcObjNoVacMotives.ObjectAt(i).stringof("vaccineCode"))) 
                    SELECT ucrsNoVacMotives
                    REPLACE vaccineCode WITH lcObjNoVacMotives.ObjectAt(i).stringof("vaccineCode")
                ENDIF   
                IF(!uf_servicos_chilkat_isNull(lcObjNoVacMotives.ObjectAt(i).stringof("obs"))) 
                    SELECT ucrsNoVacMotives
                    REPLACE obs WITH lcObjNoVacMotives.ObjectAt(i).stringof("obs")
                ENDIF 
	        ENDIF	  
	        i = i+1
		ENDDO
	ENDIF

	i=0	
	
	**obejto raiz registrations
	IF(lcObjregistrationsSize >0)
		fecha("ucrsRegistrations")
		CREATE CURSOR ucrsRegistrations(date c(254) NULL, inocNumber n(5) NULL, vaccineCode c(254) NULL, description c(254) NULL)
		DO WHILE i < lcObjregistrationsSize
	        IF(!uf_servicos_chilkat_isNull(lcObjregistrations.ObjectAt(i)))
                SELECT ucrsRegistrations
                APPEND BLANK
                IF(!uf_servicos_chilkat_isNull(lcObjregistrations.ObjectAt(i).stringof("date"))) 
                    SELECT ucrsRegistrations
                    REPLACE date WITH lcObjregistrations.ObjectAt(i).stringof("date")
                ENDIF  
                IF(!uf_servicos_chilkat_isNull(lcObjregistrations.ObjectAt(i).intof("inocNumber"))) 
                    SELECT ucrsRegistrations
                    REPLACE inocNumber WITH lcObjregistrations.ObjectAt(i).intof("inocNumber")
                ENDIF
                IF(!uf_servicos_chilkat_isNull(lcObjregistrations.ObjectAt(i).stringof("vaccineCode"))) 
                    SELECT ucrsRegistrations
                    REPLACE vaccineCode WITH lcObjregistrations.ObjectAt(i).stringof("vaccineCode")
                ENDIF 
	        ENDIF	  
	        i = i+1
		ENDDO
	ENDIF


	i=0	
	**obejto raiz vaccineInfo
	IF(!uf_servicos_chilkat_isNull(lcResult.ObjectOf("vaccineInfo")))
		
		fecha("ucrsVaccineInfo")
		CREATE CURSOR ucrsVaccineInfo(id n(5) NULL, code c(254) NULL, shortName c(254) NULL, name c(254) NULL)
  
  	        SELECT ucrsVaccineInfo
            APPEND BLANK
            IF(!uf_servicos_chilkat_isNull(lcObjVaccineInfo.intof("id"))) 
                SELECT ucrsVaccineInfo
                REPLACE id WITH lcObjVaccineInfo.intof("id")
            ENDIF 
            IF(!uf_servicos_chilkat_isNull(lcObjVaccineInfo.stringof("code"))) 
                SELECT ucrsVaccineInfo
                REPLACE code with lcObjVaccineInfo.stringof("code")
            ENDIF 
            IF(!uf_servicos_chilkat_isNull(lcObjVaccineInfo.stringof("shortName"))) 
                SELECT ucrsVaccineInfo
                REPLACE shortName WITH lcObjVaccineInfo.stringof("shortName")
            ENDIF
            IF(!uf_servicos_chilkat_isNull(lcObjVaccineInfo.stringof("name"))) 
                SELECT ucrsVaccineInfo
                REPLACE name WITH lcObjVaccineInfo.stringof("name")
            ENDIF              
	ENDIF

	i=0	
		
	** objeto dentro do VaccineInfo - vaccineType
	IF(!uf_servicos_chilkat_isNull(lcResultVaccineInfo.ObjectOf("vaccineType")))
		
		fecha("ucrsVaccineInfoVaccineType")
		CREATE CURSOR ucrsVaccineInfoVaccineType(bkId n(5) NULL, description c(254) NULL, type c(254) NULL)
  
  	        SELECT ucrsVaccineInfoVaccineType
            APPEND BLANK
            IF(!uf_servicos_chilkat_isNull(lcObjVaccineType.intof("id"))) 
                SELECT ucrsVaccineInfoVaccineType
                REPLACE bkId  WITH lcObjVaccineType.intof("id")
            ENDIF 
            IF(!uf_servicos_chilkat_isNull(lcObjVaccineType.stringof("description"))) 
                SELECT ucrsVaccineInfoVaccineType
                REPLACE description with lcObjVaccineType.stringof("description")
            ENDIF 
            IF(!uf_servicos_chilkat_isNull(lcObjVaccineType.stringof("type"))) 
                SELECT ucrsVaccineInfoVaccineType
                REPLACE type WITH lcObjVaccineType.stringof("type")
            ENDIF          
	ENDIF

	i=0	
		
	** objeto dentro do VaccineInfo - administrationWay
	IF(!uf_servicos_chilkat_isNull(lcResultVaccineInfo.ObjectOf("administrationWay")))
		
		fecha("ucrsVaccineInfoAdministrationWay")
		CREATE CURSOR ucrsVaccineInfoAdministrationWay(id n(5) NULL, description c(254) NULL)
  
  	    SELECT ucrsVaccineInfoAdministrationWay
        APPEND BLANK
        IF(!uf_servicos_chilkat_isNull(lcObjVaccineInfoAdministrationWay.intof("id"))) 
            SELECT ucrsVaccineInfoAdministrationWay
            REPLACE id WITH lcObjVaccineInfoAdministrationWay.intof("id")
        ENDIF 
        IF(!uf_servicos_chilkat_isNull(lcObjVaccineInfoAdministrationWay.stringof("description"))) 
            SELECT ucrsVaccineInfoAdministrationWay
            REPLACE description with lcObjVaccineInfoAdministrationWay.stringof("description")
        ENDIF         
	ENDIF
	
	i=0	
		
	** objeto dentro do VaccineInfo - anatomicLocation
	IF(lcObjVaccineInfoAnatomicLocationSize>0)
		fecha("ucrsVaccineInfoAnatomicLocation")
		CREATE CURSOR ucrsVaccineInfoAnatomicLocation(bkId n(5) NULL,description c(254) NULL, side n(5) NULL, sideDesc c(254) NULL, hasSide bool NULL)
		
		DO WHILE i < lcObjVaccineInfoAnatomicLocationSize
	        IF(!uf_servicos_chilkat_isNull(lcObjVaccineInfoAnatomicLocation.ObjectAt(i)))
                SELECT ucrsVaccineInfoAnatomicLocation
                APPEND BLANK
                IF(!uf_servicos_chilkat_isNull(lcObjVaccineInfoAnatomicLocation.ObjectAt(i).intof("id"))) 
                    SELECT ucrsVaccineInfoAnatomicLocation
                    REPLACE bkId WITH lcObjVaccineInfoAnatomicLocation.ObjectAt(i).intof("id")
                ENDIF  
                IF(!uf_servicos_chilkat_isNull(lcObjVaccineInfoAnatomicLocation.ObjectAt(i).stringof("description"))) 
                    SELECT ucrsVaccineInfoAnatomicLocation
                    REPLACE description WITH lcObjVaccineInfoAnatomicLocation.ObjectAt(i).stringof("description")
                ENDIF
                IF(!uf_servicos_chilkat_isNull(lcObjVaccineInfoAnatomicLocation.ObjectAt(i).intof("side"))) 
                    SELECT ucrsVaccineInfoAnatomicLocation
                    REPLACE side WITH lcObjVaccineInfoAnatomicLocation.ObjectAt(i).intof("side")
                ENDIF
                IF(!uf_servicos_chilkat_isNull(lcObjVaccineInfoAnatomicLocation.ObjectAt(i).stringof("sideDesc"))) 
                    SELECT ucrsVaccineInfoAnatomicLocation
                    REPLACE sideDesc WITH lcObjVaccineInfoAnatomicLocation.ObjectAt(i).stringof("sideDesc")
                ENDIF
                IF(!uf_servicos_chilkat_isNull(lcObjVaccineInfoAnatomicLocation.ObjectAt(i).intof("hasSide"))) 
                    SELECT ucrsVaccineInfoAnatomicLocation
                    REPLACE hasSide WITH lcObjVaccineInfoAnatomicLocation.ObjectAt(i).intof("hasSide")
                ENDIF
	        ENDIF	  
	        i = i+1
		ENDDO
	ENDIF

	i=0	
		
	** objeto dentro do VaccineInfo - adverseReaction
	IF(!uf_servicos_chilkat_isNull(lcResultVaccineInfo.ObjectOf("adverseReaction")))
		
		fecha("ucrsVaccineInfoAdverseReaction")
		CREATE CURSOR ucrsVaccineInfoAdverseReaction(id n(5) NULL, description c(254) NULL)
  
  	    SELECT ucrsVaccineInfoAdverseReaction
        APPEND BLANK
        IF(!uf_servicos_chilkat_isNull(lcObjVaccineInfoAdverseReaction.intof("id"))) 
            SELECT ucrsVaccineInfoAdverseReaction
            REPLACE id WITH lcObjVaccineInfoAdverseReaction.intof("id")
        ENDIF 
        IF(!uf_servicos_chilkat_isNull(lcObjVaccineInfoAdverseReaction.stringof("description"))) 
            SELECT ucrsVaccineInfoAdverseReaction
            REPLACE description with lcObjVaccineInfoAdverseReaction.stringof("description")
        ENDIF        
	ENDIF

	i=0	
	** objeto dentro do VaccineInfo - noVaccinationMotive
	IF(lcObjVaccineInfoNoVaccinationMotiveSize>0)
		fecha("ucrsVaccineInfoNoVaccinationMotive")
		CREATE CURSOR ucrsVaccineInfoNoVaccinationMotive(bkId n(5) NULL,description c(254) NULL, noVaccinationMotiveGroup n(5) NULL, noVaccinationMotiveGroupDesc c(254) NULL)
		
		DO WHILE i < lcObjVaccineInfoNoVaccinationMotiveSize
	        IF(!uf_servicos_chilkat_isNull(lcObjVaccineInfoNoVaccinationMotive.ObjectAt(i)))
                SELECT ucrsVaccineInfoNoVaccinationMotive
                APPEND BLANK
                IF(!uf_servicos_chilkat_isNull(lcObjVaccineInfoNoVaccinationMotive.ObjectAt(i).intof("id"))) 
                    SELECT ucrsVaccineInfoNoVaccinationMotive
                    REPLACE bkId WITH lcObjVaccineInfoNoVaccinationMotive.ObjectAt(i).intof("id")
                ENDIF  
                IF(!uf_servicos_chilkat_isNull(lcObjVaccineInfoNoVaccinationMotive.ObjectAt(i).stringof("description"))) 
                    SELECT ucrsVaccineInfoNoVaccinationMotive
                    REPLACE description WITH lcObjVaccineInfoNoVaccinationMotive.ObjectAt(i).stringof("description")
                ENDIF
                IF(!uf_servicos_chilkat_isNull(lcObjVaccineInfoNoVaccinationMotive.ObjectAt(i).intof("noVaccinationMotiveGroup"))) 
                    SELECT ucrsVaccineInfoNoVaccinationMotive
                    REPLACE noVaccinationMotiveGroup WITH lcObjVaccineInfoNoVaccinationMotive.ObjectAt(i).intof("noVaccinationMotiveGroup")
                ENDIF
                IF(!uf_servicos_chilkat_isNull(lcObjVaccineInfoNoVaccinationMotive.ObjectAt(i).stringof("noVaccinationMotiveGroupDesc"))) 
                    SELECT ucrsVaccineInfoNoVaccinationMotive
                    REPLACE noVaccinationMotiveGroupDesc WITH lcObjVaccineInfoNoVaccinationMotive.ObjectAt(i).stringof("noVaccinationMotiveGroupDesc")
                ENDIF

	        ENDIF	  
	        i = i+1
		ENDDO
	ENDIF	
	
	i=0	
	
	** objeto dentro do VaccineInfo - diseases	
	IF(lcObjVaccineInfoDiseasesSize >0)
		fecha("ucrsVaccineInfoDiseases")
		CREATE CURSOR ucrsVaccineInfoDiseases(bkId n(5) NULL,description c(254) NULL)
		
		DO WHILE i < lcObjVaccineInfoDiseasesSize
	        IF(!uf_servicos_chilkat_isNull(lcObjVaccineInfoDiseases.ObjectAt(i)))
                SELECT ucrsVaccineInfoDiseases
                APPEND BLANK
                IF(!uf_servicos_chilkat_isNull(lcObjVaccineInfoDiseases.ObjectAt(i).intof("id"))) 
                    SELECT ucrsVaccineInfoDiseases
                    REPLACE bkId WITH lcObjVaccineInfoDiseases.ObjectAt(i).intof("id")
                ENDIF  
                IF(!uf_servicos_chilkat_isNull(lcObjVaccineInfoDiseases.ObjectAt(i).stringof("description"))) 
                    SELECT ucrsVaccineInfoDiseases
                    REPLACE description WITH lcObjVaccineInfoDiseases.ObjectAt(i).stringof("description")
                ENDIF
	        ENDIF	  
	        i = i+1
		ENDDO
	ENDIF

	i=0	
	** objeto dentro do VaccineInfo - Warnigs 		
	IF(lcObjVaccineInfoWarningsSize>0)
		fecha("ucrsVaccineInfoWarnings")
		CREATE CURSOR ucrsVaccineInfoWarnings(bkId n(5) NULL,description c(254) NULL, name c(254) NULL)
		
		DO WHILE i < lcObjVaccineInfoWarningsSize
	        IF(!uf_servicos_chilkat_isNull(lcObjVaccineInfoWarnings.ObjectAt(i)))
                SELECT ucrsVaccineInfoWarnings
                APPEND BLANK
                IF(!uf_servicos_chilkat_isNull(lcObjVaccineInfoWarnings.ObjectAt(i).intof("id"))) 
                    SELECT ucrsVaccineInfoWarnings
                    REPLACE bkId WITH lcObjVaccineInfoWarnings.ObjectAt(i).intof("id")
                ENDIF  
                IF(!uf_servicos_chilkat_isNull(lcObjVaccineInfoWarnings.ObjectAt(i).stringof("description"))) 
                    SELECT ucrsVaccineInfoWarnings
                    REPLACE description WITH lcObjVaccineInfoWarnings.ObjectAt(i).stringof("description")
                ENDIF
	        ENDIF	  
	        i = i+1
		ENDDO
	ENDIF

	i=0	
	
	** objeto raiz Comercial Names
	IF(lcObjCommercialNamesSize>0)
		fecha("ucrsCommercialNames")
		CREATE CURSOR ucrsCommercialNames(id n(5) NULL,commercialName c(254) NULL, gender c(254) NULL, codeEma c(254) NULL, administeredForeign Boolean NULL, certificate Boolean NULL, active Boolean NULL)
		
		DO WHILE i < lcObjCommercialNamesSize
	        IF(!uf_servicos_chilkat_isNull(lcObjCommercialNames.ObjectAt(i)))
                SELECT ucrsCommercialNames
                APPEND BLANK
                IF(!uf_servicos_chilkat_isNull(lcObjCommercialNames.ObjectAt(i).intof("id"))) 
                    SELECT ucrsCommercialNames
                    REPLACE id WITH lcObjCommercialNames.ObjectAt(i).intof("id")
                ENDIF  
                IF(!uf_servicos_chilkat_isNull(lcObjCommercialNames.ObjectAt(i).stringof("commercialName"))) 
                    SELECT ucrsCommercialNames
                    REPLACE commercialName WITH lcObjCommercialNames.ObjectAt(i).stringof("commercialName")
                ENDIF
                IF(!uf_servicos_chilkat_isNull(lcObjCommercialNames.ObjectAt(i).stringof("gender"))) 
                    SELECT ucrsCommercialNames
                    REPLACE gender WITH lcObjCommercialNames.ObjectAt(i).stringof("gender")
                ENDIF
                IF(!uf_servicos_chilkat_isNull(lcObjCommercialNames.ObjectAt(i).stringof("codeEma"))) 
                    SELECT ucrsCommercialNames
                    REPLACE codeEma WITH lcObjCommercialNames.ObjectAt(i).stringof("codeEma")
                ENDIF
                IF(!uf_servicos_chilkat_isNull(lcObjCommercialNames.ObjectAt(i).BoolOf("administeredForeign"))) 
                    SELECT ucrsCommercialNames
                    REPLACE administeredForeign WITH lcObjCommercialNames.ObjectAt(i).BoolOf("administeredForeign")
                ENDIF
                IF(!uf_servicos_chilkat_isNull(lcObjCommercialNames.ObjectAt(i).BoolOf("certificate"))) 
                    SELECT ucrsCommercialNames
                    REPLACE certificate WITH lcObjCommercialNames.ObjectAt(i).BoolOf("certificate")
                ENDIF
                IF(!uf_servicos_chilkat_isNull(lcObjCommercialNames.ObjectAt(i).BoolOf("active"))) 
                    SELECT ucrsCommercialNames
                    REPLACE active WITH lcObjCommercialNames.ObjectAt(i).BoolOf("active")
                ENDIF
	        ENDIF	  
	        i = i+1
		ENDDO
	ENDIF
		
	i=0	
	** objeto dentro do Comercial Names - infarmedCode
	IF(lcObjCommercialNamesInfarmedCodeSize>0)
		fecha("ucrsCommercialNamesInfarmedCode")
		CREATE CURSOR ucrsCommercialNamesInfarmedCode(infarmedCode n(5) NULL)
		
		DO WHILE i < lcObjCommercialNamesInfarmedCodeSize
	        IF(!uf_servicos_chilkat_isNull(lcObjCommercialNamesInfarmedCode.ObjectAt(i)))
                SELECT ucrsCommercialNames
                APPEND BLANK
                IF(!uf_servicos_chilkat_isNull(lcObjCommercialNamesInfarmedCode.ObjectAt(i).intof("infarmedCode"))) 
                    SELECT ucrsCommercialNames
                    REPLACE infarmedCode WITH lcObjCommercialNamesInfarmedCode.ObjectAt(i).intof("infarmedCode")
                ENDIF  
	        ENDIF	  
	        i = i+1
		ENDDO
	ENDIF

	i=0	
	** objeto dentro do comercialNames - lots	
	IF(lcObjCommercialNamesLotsSize>0)
		fecha("ucrsCommercialNamesLots")
		CREATE CURSOR ucrsCommercialNamesLots(number n(5) NULL, endDate c(254) NULL)
		
		DO WHILE i < lcObjCommercialNamesLotsSize
	        IF(!uf_servicos_chilkat_isNull(lcObjCommercialNamesLots.ObjectAt(i)))
                SELECT ucrsCommercialNamesLots
                APPEND BLANK
                IF(!uf_servicos_chilkat_isNull(lcObjCommercialNamesLots.ObjectAt(i).intof("number"))) 
                    SELECT ucrsCommercialNamesLots
                    REPLACE number WITH lcObjCommercialNamesLots.ObjectAt(i).intof("number")
                ENDIF  
                IF(!uf_servicos_chilkat_isNull(lcObjCommercialNamesLots.ObjectAt(i).stringof("endDate"))) 
                    SELECT ucrsCommercialNamesLots
                    REPLACE endDate WITH lcObjCommercialNamesLots.ObjectAt(i).stringof("endDate")
                ENDIF  
	        ENDIF	  
	        i = i+1
		ENDDO
	ENDIF

	i=0	
	** objeto dentro do comercialNames - dosages	
	IF(lcObjCommercialNamesDosagesSize > 0)
		fecha("ucrsCommercialNamesDosages")
		CREATE CURSOR ucrsCommercialNamesDosages(dose n(5,9) NULL)
		
		DO WHILE i < lcObjCommercialNamesDosagesSize
	        IF(!uf_servicos_chilkat_isNull(lcObjCommercialNamesDosages.ObjectAt(i)))
                SELECT ucrsCommercialNamesDosages
                APPEND BLANK
                IF(!uf_servicos_chilkat_isNull(lcObjCommercialNamesDosages.ObjectAt(i).intof("dose"))) 
                    SELECT ucrsCommercialNamesDosages
                    REPLACE dose WITH lcObjCommercialNamesDosages.ObjectAt(i).intof("dose")
                ENDIF  
	        ENDIF	  
	        i = i+1
		ENDDO
	ENDIF

	i=0	
	**objeto da raiz anatomic Locations 
	IF(lcObjAnatomicLocationsSize > 0)
		fecha("ucrsAnatomicLocations")
		CREATE CURSOR ucrsAnatomicLocations(bkid n(5) NULL, description c(254) NULL, side n(5) NULL, sideDesc c(254) NULL, hasSide Boolean NULL)
		
		DO WHILE i < lcObjAnatomicLocationsSize
	        IF(!uf_servicos_chilkat_isNull(lcObjAnatomicLocations.ObjectAt(i)))
                SELECT ucrsAnatomicLocations
                APPEND BLANK
                IF(!uf_servicos_chilkat_isNull(lcObjAnatomicLocations.ObjectAt(i).intof("id"))) 
                    SELECT ucrsAnatomicLocations
                    REPLACE bkid WITH lcObjAnatomicLocations.ObjectAt(i).intof("id")
                ENDIF 
	                IF(!uf_servicos_chilkat_isNull(lcObjAnatomicLocations.ObjectAt(i).stringof("description"))) 
                    SELECT ucrsAnatomicLocations
                    REPLACE description WITH lcObjAnatomicLocations.ObjectAt(i).stringof("description")
                ENDIF 
                IF(!uf_servicos_chilkat_isNull(lcObjAnatomicLocations.ObjectAt(i).intof("side"))) 
                    SELECT ucrsAnatomicLocations
                    REPLACE side WITH lcObjAnatomicLocations.ObjectAt(i).intof("side")
                ENDIF 
                IF(!uf_servicos_chilkat_isNull(lcObjAnatomicLocations.ObjectAt(i).stringof("sideDesc"))) 
                    SELECT ucrsAnatomicLocations
                    REPLACE sideDesc WITH lcObjAnatomicLocations.ObjectAt(i).stringof("sideDesc")
                ENDIF 
                IF(!uf_servicos_chilkat_isNull(lcObjAnatomicLocations.ObjectAt(i).BoolOf("hasSide"))) 
                    SELECT ucrsAnatomicLocations
                    REPLACE hasSide WITH lcObjAnatomicLocations.ObjectAt(i).BoolOf("hasSide")
                ENDIF 
	        ENDIF	  
	        i = i+1
		ENDDO
	ENDIF

	i=0	
	** objeto raiz lotOrigin	
	IF(lcObjLotOriginSize > 0)
		fecha("ucrsLotOrigin")
		CREATE CURSOR ucrsLotOrigin(id n(5) NULL, description c(254) NULL)
		
		DO WHILE i < lcObjLotOriginSize
	        IF(!uf_servicos_chilkat_isNull(lcObjLotOrigin.ObjectAt(i)))
                SELECT ucrsLotOrigin
                APPEND BLANK
                IF(!uf_servicos_chilkat_isNull(lcObjLotOrigin.ObjectAt(i).intof("id"))) 
                    SELECT ucrsLotOrigin
                    REPLACE id WITH lcObjLotOrigin.ObjectAt(i).intof("id")
                ENDIF 
                IF(!uf_servicos_chilkat_isNull(lcObjLotOrigin.ObjectAt(i).stringof("description"))) 
                    SELECT ucrsLotOrigin
                    REPLACE description WITH lcObjLotOrigin.ObjectAt(i).stringof("description")
                ENDIF 
	        ENDIF	  
	        i = i+1
		ENDDO
	ENDIF

	i=0	
	** objeto raiz side
	IF(lcObjSideSize > 0)
		fecha("ucrsSide")
		CREATE CURSOR ucrsSide(id n(5) NULL, description c(254) NULL)
		
		DO WHILE i < lcObjSideSize
	        IF(!uf_servicos_chilkat_isNull(lcObjSide.ObjectAt(i)))
                SELECT ucrsSide
                APPEND BLANK
                IF(!uf_servicos_chilkat_isNull(lcObjSide.ObjectAt(i).intof("id"))) 
                    SELECT ucrsSide
                    REPLACE id WITH lcObjSide.ObjectAt(i).intof("id")
                ENDIF 
                IF(!uf_servicos_chilkat_isNull(lcObjSide.ObjectAt(i).stringof("description"))) 
                    SELECT ucrsSide
                    REPLACE description WITH lcObjSide.ObjectAt(i).stringof("description")
                ENDIF 
	        ENDIF	  
	        i = i+1
		ENDDO
	ENDIF

		i=0	
	** objeto raiz locationAdms	
	IF(lcObjLocationAdmsSize > 0)
		fecha("ucrsLocationAdms")
		CREATE CURSOR ucrsLocationAdms(id n(5) NULL, description c(254) NULL)
		
		DO WHILE i < lcObjLocationAdmsSize
	        IF(!uf_servicos_chilkat_isNull(lcObjLocationAdms.ObjectAt(i)))
                SELECT ucrsSide
                APPEND BLANK
                IF(!uf_servicos_chilkat_isNull(lcObjLocationAdms.ObjectAt(i).intof("id"))) 
                    SELECT ucrsLocationAdms
                    REPLACE id WITH lcObjSide.ObjectAt(i).intof("id")
                ENDIF 
                IF(!uf_servicos_chilkat_isNull(lcObjLocationAdms.ObjectAt(i).stringof("description"))) 
                    SELECT ucrsLocationAdms
                    REPLACE description WITH lcObjLocationAdms.ObjectAt(i).stringof("description")
                ENDIF 
	        ENDIF	  
	        i = i+1
		ENDDO
	ENDIF

ENDFUNC

FUNCTION uf_servicos_criaCursoresVaziosVacinas ()
	fecha("ucrsPatientStatus")
	CREATE CURSOR ucrsPatientStatus (description c(254) NULL, obs c(254) NULL, status n(5) NULL)

	fecha("ucrsInfectionDates")
	CREATE CURSOR ucrsInfectionDates(date c(200) NULL, number n(5) NULL)

	fecha("ucrsLastRegistration")
	CREATE CURSOR ucrsLastRegistration(date c(254) NULL, inocNumber n(5) NULL, vaccineCode c(254) NULL)

	fecha("ucrsNoVacMotives")
	CREATE CURSOR ucrsNoVacMotives(description c(254) NULL, startDate c(254) NULL,noVaccinationMotiveGroup c(254) NULL, vaccineCode c(254) NULL, obs c(254) NULL)

	fecha("ucrsRegistrations")
	CREATE CURSOR ucrsRegistrations(date c(254) NULL, inocNumber n(5) NULL, vaccineCode c(254) NULL, description c(254) NULL)

	fecha("ucResultCombo")
	CREATE CURSOR ucResultCombo (id N(20), codigo c(200), Descricao c(200))
	
	fecha("ucrsVaccineInfo")
	CREATE CURSOR ucrsVaccineInfo(id n(5) NULL, code c(254) NULL, shortName c(254) NULL, name c(254) NULL)
	
	fecha("ucrsVaccineInfoVaccineType")
	CREATE CURSOR ucrsVaccineInfoVaccineType(bkId n(5) NULL, description c(254) NULL, type c(254) NULL)

	fecha("ucrsVaccineInfoAdministrationWay")
	CREATE CURSOR ucrsVaccineInfoAdministrationWay(id n(5) NULL, description c(254) NULL)

	fecha("ucrsVaccineInfoAnatomicLocation")
	CREATE CURSOR ucrsVaccineInfoAnatomicLocation(bkId n(5) NULL,description c(254) NULL, side n(5) NULL, sideDesc c(254) NULL, hasSide bool NULL)
	
	fecha("ucrsVaccineInfoAdverseReaction")
	CREATE CURSOR ucrsVaccineInfoAdverseReaction(id n(5) NULL, description c(254) NULL)
	
	fecha("ucrsVaccineInfoNoVaccinationMotive")
	CREATE CURSOR ucrsVaccineInfoNoVaccinationMotive(bkId n(5) NULL,description c(254) NULL, noVaccinationMotiveGroup n(5) NULL, noVaccinationMotiveGroupDesc c(254) NULL)
	
	fecha("ucrsVaccineInfoDiseases")
	CREATE CURSOR ucrsVaccineInfoDiseases(bkId n(5) NULL,description c(254) NULL)
	
	fecha("ucrsVaccineInfoWarnings")
	CREATE CURSOR ucrsVaccineInfoWarnings(bkId n(5) NULL,description c(254) NULL, name c(254) NULL)
	
	fecha("ucrsCommercialNames")
	CREATE CURSOR ucrsCommercialNames(id n(5) NULL,commercialName c(254) NULL, gender c(254) NULL, codeEma c(254) NULL, administeredForeign Boolean NULL, certificate Boolean NULL, active Boolean NULL)
	
	fecha("ucrsCommercialNamesInfarmedCode")
	CREATE CURSOR ucrsCommercialNamesInfarmedCode(infarmedCode n(5) NULL)

	fecha("ucrsCommercialNamesLots")
	CREATE CURSOR ucrsCommercialNamesLots(number n(5) NULL, endDate c(254) NULL)

	fecha("ucrsCommercialNamesDosages")
	CREATE CURSOR ucrsCommercialNamesDosages(dose n(5,9) NULL)
	
	fecha("ucrsAnatomicLocations")
	CREATE CURSOR ucrsAnatomicLocations(bkid n(5) NULL, description c(254) NULL, side n(5) NULL, sideDesc c(254) NULL, hasSide Boolean NULL)
	
	fecha("ucrsLotOrigin")
	CREATE CURSOR ucrsLotOrigin(id n(5) NULL, description c(254) NULL)

	fecha("ucrsSide")
	CREATE CURSOR ucrsSide(id n(5) NULL, description c(254) NULL)

	fecha("ucrsLocationAdms")
	CREATE CURSOR ucrsLocationAdms(id n(5) NULL, description c(254) NULL)
ENDFUNC

FUNCTION  uf_servicos_fecharCursoresVacinas()
	IF USED("ucrsPatientStatus")
		fecha("ucrsPatientStatus")
	ENDIF
	IF USED("ucrsInfectionDates")
		fecha("ucrsInfectionDates")
	ENDIF
	IF USED("ucrsLastRegistration")
		fecha("ucrsLastRegistration")
	ENDIF
	IF USED("ucrsNoVacMotives")
		fecha("ucrsNoVacMotives")
	ENDIF
	IF USED("ucrsRegistrations")
		fecha("ucrsRegistrations")
	ENDIF
	IF USED("ucrsVaccineCodes")
		fecha("ucrsVaccineCodes")
	ENDIF
	IF USED("ucResultCombo")
		fecha("ucResultCombo")
	ENDIF
	IF USED("ucrsVaccineInfo")
		fecha("ucrsVaccineInfo")
	ENDIF
	IF USED("ucrsVaccineInfoVaccineType")
		fecha("ucrsVaccineInfoVaccineType")
	ENDIF
	IF USED("ucrsVaccineInfoAdministrationWay")
		fecha("ucrsVaccineInfoAdministrationWay")
	ENDIF
	IF USED("ucrsVaccineInfoAnatomicLocation")
		fecha("ucrsVaccineInfoAnatomicLocation")
	ENDIF
	IF USED("ucrsVaccineInfoAdverseReaction")
		fecha("ucrsVaccineInfoAdverseReaction")
	ENDIF
	IF USED("ucrsVaccineInfoNoVaccinationMotive")
		fecha("ucrsVaccineInfoNoVaccinationMotive")
	ENDIF
	IF USED("ucrsVaccineInfoDiseases")
		fecha("ucrsVaccineInfoDiseases")
	ENDIF
	IF USED("ucrsVaccineInfoWarnings")
		fecha("ucrsVaccineInfoWarnings")
	ENDIF
	IF USED("ucrsCommercialNames")
		fecha("ucrsCommercialNames")
	ENDIF
	IF USED("ucrsCommercialNamesInfarmedCode")
		fecha("ucrsCommercialNamesInfarmedCode")
	ENDIF
	IF USED("ucrsCommercialNamesLots")
		fecha("ucrsCommercialNamesLots")
	ENDIF
	IF USED("ucrsCommercialNamesDosages")
		fecha("ucrsCommercialNamesDosages")
	ENDIF
	IF USED("ucrsAnatomicLocations")
		fecha("ucrsAnatomicLocations")
	ENDIF
	IF USED("ucrsLotOrigin")
		fecha("ucrsLotOrigin")
	ENDIF
	IF USED("ucrsSide")
		fecha("ucrsSide")
	ENDIF
	IF USED("ucrsLocationAdms")
		fecha("ucrsLocationAdms")
	ENDIF
ENDFUNC


FUNCTION  uf_servicos_criaCursorResultadoVacinacao()
    LPARAMETERS lcResult
    
    fecha("ucResultCombo")

	local i    
	i=0
	lcresultsize = lcResult.size
	if(lcresultsize>0)
	    CREATE CURSOR ucResultCombo (id N(20), codigo c(200), Descricao c(200))
	    DO WHILE i < lcresultsize
	        IF(!uf_servicos_chilkat_isNull(lcResult.ObjectAt(i)))
	                select ucResultCombo
	                APPEND BLANK
	                if(!uf_servicos_chilkat_isNull(lcResult.ObjectAt(i).intof("id"))) 
	                        select ucResultCombo
	                        replace id with lcResult.ObjectAt(i).intof("id")
	                endif 
	                if(!uf_servicos_chilkat_isNull(lcResult.ObjectAt(i).stringof("code"))) 
	                        select ucResultCombo
	                        replace codigo with lcResult.ObjectAt(i).stringof("code")
	                endif  
	                  if(!uf_servicos_chilkat_isNull(lcResult.ObjectAt(i).stringof("description"))) 
	                        select ucResultCombo
	                        replace Descricao with lcResult.ObjectAt(i).stringof("description")
	                endif   
	        endif	  
	        i = i+1
		ENDDO
	ENDIF

ENDFUNC

FUNCTION uf_servicos_devolveJsonDeCursor
    LPARAMETERS lctagName, lcIsArray, lsIsRoot
    
    
    LOCAL lcJson
    lcJson = ""
    lcJson = nfCursortoJson(.t.,.f.,.f.,.t.,lcIsArray)
    
    IF!(EMPTY(ALLTRIM(lcJson)))
    
        IF(!EMPTY(lctagName))
            lcJson = '"' + ALLTRIM(lctagName)+'": '+lcJson    
        ENDIF

        IF(!EMPTY(lsIsRoot))
            lcJson = '{"'+lcJson +'}'    
        ENDIF
    ENDIF
    
    
    RETURN lcJson 

ENDFUNC


&&apenas para testar
&&TODO::
FUNCTION uf_servicos_devolveJsonDuploEcra
    
    local lcPosFiTemp, lcJson, lcJsonLinhas, lcJsonTotal
    lcJson = ""

    SELECT fi
    lcPosFiTemp= RECNO("fi")
    
    if(!USED("ft") or !USED("fi"))
        RETURN ""
    ENDIF
    
    SELECT fi
    lcPosFiTemp= RECNO("fi")
    

    fecha("ucrsFiTemp")
    fecha("ucrsTotalTemp")
    
    SELECT fi
    GO TOP
    select ref, epv, design from fi into cursor ucrsFiTemp readwrite
    
    
    SELECT ucrsFiTemp 
    lcJsonLinhas = uf_servicos_devolveJsonDeCursor("linhas",.t.,.f.)
    
    SELECT etotal, ft.nome, ft.no FROM ft into cursor ucrsTotalTemp readwrite
    
    SELECT ucrsTotalTemp 
    lcJsonTotal=  uf_servicos_devolveJsonDeCursor("totais",.f.,.f.)
    
    IF !EMPTY(ALLTRIM(lcJsonLinhas)) AND !EMPTY(ALLTRIM(lcJsonTotal))
        lcJson = ALLTRIM(lcJsonLinhas) + "," + ALLTRIM(lcJsonTotal)
    ENDIF
    
    IF(!EMPTY(lsIsRoot))
        lcJson = '{"'+lcJson +'}'    
    ENDIF
    


    SELECT fi
        TRY
           GOTO lcPosFiTemp
        CATCH
           GOTO TOP  
        ENDTRY
    
    fecha("ucrsFiTemp")
    fecha("ucrsTotalTemp")
    
    RETURN lcJson 
    
ENDFUNC

FUNCTION uf_servicos_pricesDem_createSend
	LPARAMETERS lcToken, lcRef, lcNrAtendimento
	
	LOCAL lcStamp, lcSQLCreateSend, lcTeste  
	STORE '' TO lcStamp, lcSQLCreateSend, lcTeste   
	
	lcStamp = uf_gerais_gerarIdentifiers(36)
	
	IF uf_gerais_getparameter("ADM0000000227", "BOOL")==.F.
       lcTeste = 0
    ELSE
       lcTeste = 1
    ENDIF

	IF EMPTY(lcToken)
		uf_perguntalt_chama("Token vazio/invalido. Por favor contacte o suporte.","OK","",48)
		RETURN .f.
	ENDIF
	IF EMPTY(lcRef)
		uf_perguntalt_chama("Referencia vazia/invalida. Por favor contacte o suporte.","OK","",48)
		RETURN .f.
	ENDIF

	TEXT TO lcSQLCreateSend NOSHOW textmerge
		exec up_save_sendSearchPrice '<<lcStamp>>', '<<lcToken>>', '<<lcRef>>','<<mysite>>', <<lcTeste>>, '<<lcNrAtendimento>>' 
	ENDTEXT
	IF !uf_gerais_actgrelha("", "", lcSQLCreateSend)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA A GUARDAR QUEUE_MANAGEMENT DAS SENHAS","OK","",16)
		RETURN .f.
	ENDIF
	
	RETURN .T.
ENDFUNC

FUNCTION uf_servicos_pricesDem_verifySend
	LPARAMETERS lcTokenVerify, lcNratendVerify
	
	LOCAL lcSQLVerify 
	STORE '' TO lcSQLVerify 
	
	IF USED("ucrsVerify")
		fecha("ucrsVerify")
	ENDIF
	
	TEXT TO lcSQLVerify NOSHOW textmerge
		exec up_get_sendSearchPrice'<<lcTokenVerify>>', '<<lcNratendVerify>>'
	ENDTEXT
	IF !uf_gerais_actgrelha("", "ucrsVerify", lcSQLVerify)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA A GUARDAR QUEUE_MANAGEMENT DAS SENHAS","OK","",16)
		RETURN .f.
	ENDIF
	
	RETURN .T.	
ENDFUNC

FUNCTION uf_servicos_pricesDem
	LPARAMETERS lcTokenService
	
	IF EMPTY(lcTokenService)
		uf_perguntalt_chama("Token para chamar o servi�o vazio. Por favor contacte o suporte.","OK","",48)
		RETURN .f.
	ENDIF

	uf_servicos_pricesDem_callService(lcTokenService)
	
	IF !uf_servicos_pricesDem_createCursors(lcTokenService)
		RETURN .F.
	ENDIF
	
	RETURN .T.
	
ENDFUNC

FUNCTION uf_servicos_pricesDem_callService
	LPARAMETERS lcTokenSend
	
	IF EMPTY(lcTokenSend)
		uf_perguntalt_chama("Token para chamar o servi�o vazio. Por favor contacte o suporte.","OK","",48)
		RETURN .f.
	ENDIF
	
	 
    IF  .NOT. USED("ucrsServerName")
       TEXT TO lcsql TEXTMERGE NOSHOW
				Select @@SERVERNAME as dbServer
       ENDTEXT
       IF  .NOT. uf_gerais_actgrelha("", "ucrsServerName", lcsql)
          uf_perguntalt_chama("N�o foi possivel proceder ao servi�o de Pesquisa Precos. Contacte o suporte.", "OK", "", 32)
          regua(2)
          RETURN .F.
       ENDIF
    ENDIF
	
    SELECT ucrsservername
    lcdbserver = ALLTRIM(ucrsservername.dbserver)
    lcnomejar = "LtsDispensaEletronica.jar"
    IF uf_gerais_getparameter("ADM0000000227", "BOOL")==.F.
       lctestjar = "0"
    ELSE
       lctestjar = "1"
    ENDIF
    
    SELECT ucrse1
	lccodfarm = ALLTRIM(ucrse1.u_codfarm)
    
    IF(EMPTY(lccodfarm))
      uf_perguntalt_chama("N�o foi possivel obter o codigo da farmacia. Por favor contacte o suporte.", "OK", "", 32)
      regua(2)
      RETURN .F.
	ENDIF
    
    LOCAL lcComando   
    lcComando = "PESQUISAPRECOS"
	
    lcwspath = ALLTRIM(uf_gerais_getparameter("ADM0000000185", "TEXT"))+'\DEM\'+ALLTRIM(lcnomejar)+' ' + ALLTRIM(lcComando) +'; 
	--token='+ALLTRIM(lcTokenSend) +' --codFarmacia='+ALLTRIM(lccodfarm)+' --dbServer='+ALLTRIM(lcdbserver)+;
	' --dbName='+UPPER(ALLTRIM(sql_db))+' --site='+UPPER(ALLTRIM(STRTRAN(ALLTRIM(mysite), ' ', '_')))+' --test='+lctestjar;
	+' --iniciais='+ALLTRIM(m_chinis)
	
    IF uf_gerais_getparameter("ADM0000000227", "BOOL")==.T.
       _CLIPTEXT = lcwspath
       uf_perguntalt_chama("Webservice de teste...", "OK", "", 64)
    ENDIF

    regua(1, RECNO(), "A obter pre�os...")
	
    lcwspath = "javaw -jar "+myEncodingFileJar+lcwspath
    owsshell = CREATEOBJECT("WScript.Shell")
    owsshell.run(lcwspath, 1, .T.)
	
	regua(2)
	
	RETURN .T.
ENDFUNC

FUNCTION uf_servicos_pricesDem_createCursors
	LPARAMETERS lcTokenCreateCursors	
	
	uf_servicos_pricesDem_closeCursors()
	
	IF !uf_servicos_pricesDem_createCursors_response(lcTokenCreateCursors)
		RETURN .F.
	ENDIF

	IF !uf_servicos_pricesDem_createCursors_pvp(lcTokenCreateCursors)
		RETURN .F.
	ENDIF
	
	uf_servicos_pricesDem_mergeCursor()
	
	RETURN .T.
ENDFUNC

FUNCTION uf_servicos_pricesDem_createCursors_response
	LPARAMETERS lcTokenCreateCursorsResponse
 	
 	LOCAL lcSqlResponse
 	STORE '' TO lcSqlResponse
	
 	IF (USED("antes ucrsResponseSearchPrice"))
		fecha("ucrsResponseSearchPrice")
	ENDIF
  	
 	TEXT TO lcSqlResponse NOSHOW textmerge
		exec up_get_responseSearchPrice '<<lcTokenCreateCursorsResponse>>'
	ENDTEXT
	IF !uf_gerais_actgrelha("", "ucrsResponseSearchPrice", lcSqlResponse)
		uf_perguntalt_chama("Ocorreu uma anomalia a criar o cursor Response Search Price","OK","",16)
		RETURN .f.
	ENDIF
	
	IF !USED("ucrsResponseSearchPrice") OR RECCOUNT("ucrsResponseSearchPrice") <= 0
		uf_perguntalt_chama("N�o foi obtida resposta por parte do servi�o.","OK","",16)
		RETURN .f.
	ENDIF
	
	RETURN .T.
ENDFUNC

FUNCTION uf_servicos_pricesDem_createCursors_pvp
	LPARAMETERS lcTokenCreateCursorsPvp
	
	LOCAL lcSqlResponsePvp
 	STORE '' TO lcSqlResponsePvp
 	
 	IF (USED("ucrsSearchPricePvp"))	
		fecha("ucrsSearchPricePvp")
	ENDIF
	
 	TEXT TO lcSqlResponsePvp NOSHOW textmerge
		exec up_get_searchPricePvp '<<lcTokenCreateCursorsPvp>>'
	ENDTEXT
	IF !uf_gerais_actgrelha("", "ucrsSearchPricePvp", lcSqlResponsePvp)
		uf_perguntalt_chama("Ocorreu uma anomalia a criar o cursor de PVP. Por favor contactar o suporte.","OK","",16)
		RETURN .f.
	ENDIF
	
	IF !USED("ucrsSearchPricePvp") OR RECCOUNT("ucrsSearchPricePvp") <= 0
		uf_perguntalt_chama("O servi�o Pre�os SPMS n�o tem valores para a referencia em quest�o.","OK","",16)
		RETURN .f.
	ENDIF
		
	RETURN .T.
	
ENDFUNC

FUNCTION uf_servicos_pricesDem_mergeCursor

	IF (USED("ucrsMergeSearch"))		
		fecha("ucrsMergeSearch")
	ENDIF	
		
	SELECT  ROUND(ucrsSearchPricePvp.pvp,2) as pvp, ucrsResponseSearchPrice.priceMax, ucrsResponseSearchPrice.fourLowestPrice, ucrsResponseSearchPrice.priceRefere, ucrsResponseSearchPrice.priceNotified, ucrsResponseSearchPrice.ref FROM ucrsResponseSearchPrice; 
	inner join ucrsSearchPricePvp on ucrsSearchPricePvp.token = ucrsResponseSearchPrice.tokenSearchPricePvp AND ucrsSearchPricePvp.ref = ucrsResponseSearchPrice.ref;
	INTO CURSOR ucrsMergeSearch readwrite
	
	RETURN .T.
ENDFUNC

FUNCTION uf_servicos_pricesDem_closeCursors

	IF USED("ucrsResponseSearchPrice")
		fecha("ucrsResponseSearchPrice")
	ENDIF
	
	IF USED("ucrsSearchPricePvp")
		fecha("ucrsSearchPricePvp")
	ENDIF
	
	IF USED("ucrsMergeSearch")
		fecha("ucrsMergeSearch")
	ENDIF
	
	IF USED("ucrsVerify")
		fecha("ucrsVerify")
	ENDIF
	RETURN .T.
ENDFUNC

**uf_servicos_getAssinaturas('101100000772470810011')
FUNCTION uf_servicos_getAssinaturas
	LPARAMETERS lcNrLinha
	
	LOCAL lcNrReceita, lcNrFarmacia, lcToken 
	STORE 0 TO lcNrReceita, lcNrFarmacia
	STORE '' TO lcToken 
	
	IF(EMPTY(lcNrLinha))
		uf_perguntalt_chama("N�mero de linha n�o preenchido. Contacte o suporte por favor.","OK","",16)
		RETURN .f.
	ENDIF
	
	IF(uf_servicos_getAssinaturas_getNrReceita(lcNrLinha))
		
		IF(!USED("ucrsDispEletro"))
			
			IF(!USED("ucrsDispEletro"))
				fecha("ucrsDispEletro")
			ENDIF
			uf_perguntalt_chama("N�o foi possivel obter numero Receita. Contacte o suporte por favor.", "", "OK", 16)
			RETURN .f.
		ENDIF
		SELECT ucrsDispEletro
		lcNrReceita = ucrsDispEletro.receita_nr
		
		IF(EMPTY(lcNrReceita))
			uf_perguntalt_chama("N�o foi possivel obter numero Receita. Contacte o suporte por favor.", "", "OK", 16)
			RETURN .f.
		ENDIF
		
		lcNrFarmacia = uf_gerais_codigoFarmacia()
		
		IF(EMPTY(lcNrFarmacia))
			uf_perguntalt_chama("N�o foi possivel obter codigo infarmed da farmacia. Contacte o suporte por favor.", "", "OK", 16)
			RETURN .f.
		ENDIF
		
		lcToken = uf_gerais_gerarIdentifiers(36)
		
		uf_servicos_getAssinaturas_java(lcNrLinha, lcNrReceita, lcNrFarmacia, lcToken)
		
		uf_servicos_getAssinaturas_result(lcToken)
		
	ENDIF	
	
	IF(!USED("ucrsDispEletro"))
		fecha("ucrsDispEletro")
	ENDIF
	
	RETURN .T.
ENDFUNC

FUNCTION uf_servicos_getAssinaturas_getNrReceita
	LPARAMETERS lcNumeroLinha
	
	LOCAL lcSQLDD, lcSQLDispensaEle
	STORE "" TO  lcSQLDD, lcSQLDispensaEle

	TEXT TO lcSQLDD TEXTMERGE NOSHOW
		SELECT token FROM Dispensa_Eletronica_DD WHERE id='<<lcNumeroLinha>>'
	ENDTEXT
	If !uf_gerais_actGrelha("", "ucrsDispDD", lcSQLDD)
		uf_perguntalt_chama("N�o foi possivel encontrar regitos. Contacte o suporte por favor.", "", "OK", 16)
		RETURN .f.
	ENDIF
	
	IF(EMPTY("ucrsDispDD"))
		uf_perguntalt_chama("N�o foi possivel encontrar regitos. Contacte o suporte por favor.", "", "OK", 16)
		RETURN .f.
	ENDIF
	
	SELECT ucrsDispDD
	TEXT TO lcSQLDispensaEle TEXTMERGE NOSHOW
		SELECT receita_nr FROM Dispensa_Eletronica(nolock) WHERE token ='<<ucrsDispDD.token>>'
	ENDTEXT
	If !uf_gerais_actGrelha("", "ucrsDispEletro", lcSQLDispensaEle)
		uf_perguntalt_chama("N�o foi possivel encontrar regitos. Contacte o suporte por favor.", "", "OK", 16)
		RETURN .f.
	ENDIF
	
	IF(!USED("ucrsDispDD"))
		fecha("ucrsDispDD")
	ENDIF
	
	IF(EMPTY("ucrsDispEletro"))
		IF(!USED("ucrsDispEletro"))
			fecha("ucrsDispEletro")
		ENDIF
		uf_perguntalt_chama("N�o foi possivel obter numero Receita. Contacte o suporte por favor.", "", "OK", 16)
		RETURN .f.
	ENDIF
	
	RETURN .T.	
ENDFUNC

FUNCTION uf_servicos_getAssinaturas_java
	LPARAMETERS lcNumeroLinha, lcNumeroReceita, lcCodigoFarmacia, lcToken
				 
    IF  .NOT. USED("ucrsServerName")
       TEXT TO lcsql TEXTMERGE NOSHOW
				Select @@SERVERNAME as dbServer
       ENDTEXT
       IF  .NOT. uf_gerais_actgrelha("", "ucrsServerName", lcsql)
          uf_perguntalt_chama("N�o foi possivel proceder ao servi�o de Pesquisa Precos. Contacte o suporte.", "OK", "", 32)
          regua(2)
          RETURN .F.
       ENDIF
    ENDIF
	
    SELECT ucrsservername
    lcdbserver = ALLTRIM(ucrsservername.dbserver)
    lcnomejar = "LtsDispensaEletronica.jar"
    IF uf_gerais_getparameter("ADM0000000227", "BOOL")==.F.
       lctestjar = "0"
    ELSE
       lctestjar = "1"
    ENDIF
    
    
    LOCAL lcComando   
    lcComando = "Consulta_Assinaturas"
	
    lcwspath = ALLTRIM(uf_gerais_getparameter("ADM0000000185", "TEXT"))+'\DEM\'+ALLTRIM(lcnomejar)+' ' + ALLTRIM(lcComando) +'; 
	--token=' + ALLTRIM(lcToken); 
	+ ' --chavePedido=' + ALLTRIM(lcToken);
	+ ' --codFarmacia=' + ALLTRIM(lcCodigoFarmacia);
	+ ' --nrReceita=' + ALLTRIM(lcNumeroReceita);
	+ ' --nrLinha=' + ALLTRIM(lcNumeroLinha);
	+ ' --dbServer=' + ALLTRIM(lcdbserver);
	+ ' --dbName=' + UPPER(ALLTRIM(sql_db));
	+ ' --site=' + UPPER(ALLTRIM(STRTRAN(ALLTRIM(mysite), ' ', '_')));
	+ ' --test=' + lctestjar
	
    IF uf_gerais_getparameter("ADM0000000227", "BOOL")==.T.
       _CLIPTEXT = lcwspath
       uf_perguntalt_chama("Webservice de teste...", "OK", "", 64)
    ENDIF

    regua(1, RECNO(), "A obter pre�os...")
	
    lcwspath = "javaw -jar "+myEncodingFileJar+lcwspath
    owsshell = CREATEOBJECT("WScript.Shell")
    owsshell.run(lcwspath, 1, .T.)
	
	regua(2)
	
	RETURN .T.
	
ENDFUNC

FUNCTION uf_servicos_getAssinaturas_result
	LPARAMETERS lcTokenAssinatura
	
	LOCAL lcSQLDAssi
	STORE "" TO  lcSQLDAssi

	TEXT TO lcSQLDAssi TEXTMERGE NOSHOW
		select cast(Dispensa_Eletronica_D_Assinatura.infoAssinatura as text) as infoAssinatura from
		Dispensa_Eletronica (nolock)
		inner join Dispensa_Eletronica_D (nolock)on Dispensa_Eletronica_D.token = Dispensa_Eletronica.token
		inner join Dispensa_Eletronica_D_Assinatura (nolock)on Dispensa_Eletronica_D_Assinatura.tokenAssinatura =
		Dispensa_Eletronica_D.tokenAssinatura
		where chave_pedido = '<<ALLTRIM(lcTokenAssinatura)>>'
	ENDTEXT
	If !uf_gerais_actGrelha("", "ucrsDispDAss", lcSQLDAssi)
		uf_perguntalt_chama("N�o foi possivel encontrar regitos. Contacte o suporte por favor.", "", "OK", 16)
		RETURN .f.
	ENDIF
	
	IF(EMPTY("ucrsDispDAss"))
		uf_perguntalt_chama("N�o foi possivel encontrar a assinatura. Contacte o suporte por favor.", "", "OK", 16)
		RETURN .f.
	ENDIF

	IF EMPTY(ucrsDispDAss.infoAssinatura)
		uf_perguntalt_chama("N�o foi possivel encontrar a assinatura. Contacte o suporte por favor.", "", "OK", 16)
		RETURN .f.
	ENDIF
	
	SELECT ucrsDispDAss
	_CLIPTEXT = ucrsDispDAss.infoAssinatura
	
	MESSAGEBOX("A assinatura esta no CTRL + V � so colar")
	
	RETURN .T.
ENDFUNC


** retorno
** 1 - tudo ok
** -1 - serial number invalido
** -2 - product code invalido
** -3 - lote invalido
** -4 - cnp invalido

FUNCTION uf_servicos_valida_campos_nmvo
	LPARAMETERS lcSn, lcPC, lcBatch, lcCnp
	
	LOCAL lcResposta, lcRegexAlfaNumericos, lcRegexAlfaNumericosEspeciais, lcRegexNumericos


	* Defini��o das regex
	lcRegexAlfaNumericos = "^[A-Za-z0-9]+$"
	lcRegexAlfaNumericosEspeciais = "^[A-Za-z0-9_!?%&"+"'"+ '*;<>""/\-\(\)+,.:=]+$'
	lcRegexNumericos = "^[0-9]+$"
	
	* Valida��o do Serial Number
	IF !EMPTY(lcSn)
		IF !uf_gerais_checkRegex(ALLTRIM(lcSn), lcRegexAlfaNumericosEspeciais)
			RETURN -1
		ENDIF
	ENDIF
	
	* Valida��o do Product Code (apenas n�meros)
	IF !EMPTY(lcPC)
		IF !uf_gerais_checkRegex(ALLTRIM(lcPC), lcRegexNumericos)
			RETURN -2
		ENDIF
	ENDIF
	
	* Valida��o do Lote
	IF !EMPTY(lcBatch)
		IF !uf_gerais_checkRegex(ALLTRIM(lcBatch), lcRegexAlfaNumericosEspeciais)
			RETURN -3
		ENDIF
	ENDIF
	
	* Valida��o do CNP (apenas n�meros)
	IF !EMPTY(lnCnp)
		IF !uf_gerais_checkRegex(ALLTRIM(lcCnp), lcRegexNumericos)
			RETURN -4
		ENDIF			
	ENDIF
	
	RETURN 1  && Retorna 1 se tudo estiver correto
ENDFUNC

