**
FUNCTION uf_MAPAROBOT_Chama
	

	PUBLIC myMapaRobotIntroducao, myMapaRobotAlteracao, myOrderConsis
	
	** cursor de pesquisa
	IF !USED("uCrsMapaRobot")
		uf_MAPAROBOT_carregaDados(.t.)
	ENDIF
	
	IF TYPE("MAPAROBOT") == "U"
		DO FORM MAPAROBOT
	ELSE
		MAPAROBOT.show
	ENDIF

	SELECT uCrsMapaRobot  
	GO top
ENDFUNC


**
FUNCTION uf_MapaRobot_CarregaMenu
	
	** Configura Menu 
	WITH maparobot.menu1
		.adicionaOpcao("opcoes", "Op��es", myPath + "\imagens\icons\opcoes_w.png", "uf_gerais_menuOpcoes With 'MapaRobot'","O")
		.adicionaOpcao("actualizar","Pesquisar",myPath + "\imagens\icons\actualizar_w.png","uf_MapaRobot_actualizar", "A")
		.adicionaOpcao("novo","Novo",myPath + "\imagens\icons\mais_w.png","uf_MapaRobot_novo","N")
		.adicionaOpcao("editar", "Editar", myPath + "\imagens\icons\lapis_w.png", "uf_MapaRobot_editar","E")
		.adicionaOpcao("sinalizar", "Sinalizar", myPath + "\imagens\icons\robot_w.png", "uf_MapaRobot_sinalizar","S")
	ENDWITH
	
	WITH maparobot.menu_opcoes
		.adicionaOpcao("imprimir", "Imprimir Dados", "", "uf_MapaRobot_imprimir")
		.adicionaOpcao("eliminar", "Eliminar", myPath + "\imagens\icons\cruz_w.png", "uf_MapaRobot_eliminar")
	ENDWITH 


ENDFUNC


**
FUNCTION uf_MAPAROBOT_carregaDados
	LPARAMETERS lcBool

	LOCAL lcSQL
	
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_robot_consis_getMap '', 0, 0, 0, <<mysite_nr>>
	ENDTEXT 
	
	IF !uf_gerais_actGrelha("","uCrsMapaRobot",lcSQL)
		uf_perguntalt_chama("Ocorreu uma anomalia a verificar o Mapa. Por favor contacte o Suporte.","OK","",16)
		RETURN .f.
	ENDIF
	
	** Cria cursor de inser��es
	Select  * FROM uCrsMapaRobot  where ref = 'xxxx'  INTO cursor uCrsMapaRobotInsert READWRITE 

ENDFUNC


**
FUNCTION uf_MapaRobot_actualizar
	LOCAL lcFiltro
	
	IF EMPTY(ALLTRIM(MapaRobot.canal.value))
		lcCanal = 0
	ELSE
		lcCanal = ALLTRIM(MapaRobot.canal.value)
	ENDIF
	
	IF EMPTY(ALLTRIM(MapaRobot.armario.value))
		lcarmario = 0
	ELSE
		lcarmario = ALLTRIM(MapaRobot.armario.value)
	ENDIF
	
	IF EMPTY(ALLTRIM(MapaRobot.Prateleira.value))
		lcPrateleira = 0
	ELSE
		lcPrateleira = ALLTRIM(MapaRobot.Prateleira.value)
	ENDIF

	TEXT TO lcSQL NOSHOW TEXTMERGE 
		exec up_robot_consis_getMap '<<ALLTRIM(MapaRobot.ref.value)>>',<<lcarmario>>,<<lccanal>>,<<lcPrateleira>>, <<mysite_nr>>
	ENDTEXT 

	IF !uf_gerais_actGrelha("MapaRobot.gridPesq","uCrsMapaRobot",lcSQL)
		uf_perguntalt_chama("Ocorreu uma anomalia a verificar o Mapa. Por favor contacte o Suporte.","OK","",16)
		RETURN .f.
	ENDIF
	
	MapaRobot.GridPesq.ref.setfocus
	
	IF RECCOUNT("uCrsMapaRobot") == 1
		uf_MapaRobot_sinalizar()
	ENDIF

ENDFUNC



**
FUNCTION uf_MapaRobot_alternaMenu
	
	WITH MapaRobot.menu1	
		IF myMapaRobotIntroducao == .t. OR myMapaRobotAlteracao == .t.
			WITH MapaRobot.CtneditarMapa.menu1
				.estado("", "SHOW", "Gravar", .t., "Cancelar", .t.)
			ENDWITH
			
			
			MapaRobot.CtneditarMapa.visible =  .t.
			uf_MapaRobot_sombra(.t.)
		ELSE 
			MapaRobot.CtneditarMapa.visible =  .f.
			uf_MapaRobot_sombra(.f.)
		ENDIF 
	ENDWITH

		
	MapaRobot.refresh
ENDFUNC


**
FUNCTION uf_MapaRobot_editar
	
	Select uCrsMapaRobot
	IF EMPTY(ALLTRIM(uCrsMapaRobot.ref))
		uf_perguntalt_chama("DEVE SELECIONAR UM REGISTO PARA ALTERA��O.","OK","",64)
		RETURN .f.
	ENDIF
	
	Select uCrsMapaRobotInsert
	SCAN
		DELETE
	ENDSCAN

	Select uCrsMapaRobot
	Select uCrsMapaRobotInsert 
	APPEND BLANK
	Replace uCrsMapaRobotInsert.ref WITH uCrsMapaRobot.ref
	Replace uCrsMapaRobotInsert.armario WITH uCrsMapaRobot.armario 
	Replace uCrsMapaRobotInsert.canal WITH uCrsMapaRobot.canal
	Replace uCrsMapaRobotInsert.Prateleira WITH uCrsMapaRobot.Prateleira
	Replace uCrsMapaRobotInsert.stock WITH uCrsMapaRobot.stock 
	
	MapaRobot.lcCanal = uCrsMapaRobot.canal
	MapaRobot.lcArmario = uCrsMapaRobot.armario 
	
	myMapaRobotAlteracao = .t.
	uf_MapaRobot_alternaMenu()
	
ENDFUNC


**
FUNCTION uf_MapaRobot_novo
	myMapaRobotIntroducao = .t.
	
	MapaRobot.lcCanal = 0
	MapaRobot.lcArmario = 0
	
	Select uCrsMapaRobotInsert
	SCAN
		DELETE
	ENDSCAN
		
	select uCrsMapaRobotInsert
	APPEND BLANK
	
	select uCrsMapaRobotInsert
	GO Top
	
	uf_MapaRobot_alternaMenu()
ENDFUNC 


**
FUNCTION uf_MapaRobot_imprimir
	LOCAL lcRef, lcArmario, lcCanal, lcPrateleira
	STORE "" TO lcRef, lcArmario, lcCanal, lcPrateleira
	
	&& Parametros de pesquisa
	lcRef			= ALLTRIM(MAPAROBOT.ref.value)
	lcArmario		= IIF(EMPTY(ALLTRIM(MAPAROBOT.armario.value)), 0, ALLTRIM(MAPAROBOT.armario.value))
	lcCanal			= IIF(EMPTY(ALLTRIM(MAPAROBOT.canal.value)), 0, ALLTRIM(MAPAROBOT.canal.value))
	lcPrateleira	= IIF(EMPTY(ALLTRIM(MAPAROBOT.prateleira.value)), 0, ALLTRIM(MAPAROBOT.prateleira.value))
	
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		&ref=<<lcRef>>&armario=<<lcArmario>>&canal=<<lcCanal>>&prateleira=<<lcPrateleira>>&site=<<mySite>>
	ENDTEXT
	SELECT uCrsMapaRobot
		GO TOP 
		
		IF RECCOUNT("uCrsMapaRobot") > 0
			uf_gerais_chamaReport("Listagem_MapaRobot_pesquisa", lcSql)
		ELSE
			uf_perguntalt_chama("N�o existem dados para Impress�o. Por favor verifique. Obrigado.","OK","",16)						
		ENDIF

ENDFUNC


**
FUNCTION uf_MapaRobot_sombra
	LPARAMETERS tcBool
	
	IF tcBool
		** remover foco da grelha
		MapaRobot.ref.setfocus
		
		** Colocar sombra
		MapaRobot.sombra.width	= MapaRobot.width
		MapaRobot.sombra.height	= MapaRobot.height
		MapaRobot.sombra.left 	= 0
		MapaRobot.sombra.top 	= 0
		
		MapaRobot.sombra.visible 	= .t.
	ELSE
		** remover sombra
		MapaRobot.sombra.visible 	= .f.
	ENDIF
ENDFUNC


**
FUNCTION uf_MapaRobot_gravar
	LOCAL lcSQL 
	lcSQL = ''
	
	IF uf_MapaRobot_regras() == .f.
		RETURN .f.
	ENDIF
	SELECT uCrsMapaRobot	
	SELECT uCrsMapaRobotInsert

	IF myMapaRobotIntroducao == .t.
		
		**
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			exec up_robot_consis_insertMap '<<ALLTRIM(uCrsMapaRobotInsert.ref)>>', <<uCrsMapaRobotInsert.Armario>>, <<uCrsMapaRobotInsert.Canal>>, <<uCrsMapaRobotInsert.Prateleira>>, <<uCrsMapaRobotInsert.stock>>
		ENDTEXT 
	ELSE
				
		**
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			exec up_robot_consis_updateMap '<<ALLTRIM(uCrsMapaRobotInsert.ref)>>', <<uCrsMapaRobotInsert.Armario>>, <<uCrsMapaRobotInsert.Canal>>, <<uCrsMapaRobotInsert.Prateleira>>, <<uCrsMapaRobotInsert.stock>>, <<uCrsMapaRobot.Armario>>, <<uCrsMapaRobot.Canal>>
		ENDTEXT 
	ENDIF
	
	IF !uf_gerais_actGrelha("","",lcSQL)
		uf_perguntalt_chama("Ocorreu uma anomalia a atualizar o Mapa. Por favor contacte o suporte.","OK","",16)
		RETURN .f.
	ENDIF	

 	myMapaRobotIntroducao = .f. 
	myMapaRobotAlteracao = .f.
	uf_MapaRobot_alternaMenu()
	uf_MapaRobot_actualizar() 
	MapaRobot.ref.setfocus
	MapaRobot.refresh
	
ENDFUNC


**
FUNCTION uf_MapaRobot_regras
	Select uCrsMapaRobot
	Select uCrsMapaRobotInsert
	
	GO Top
	IF EMPTY(ALLTRIM(ref)) OR armario = 0 OR canal = 0 OR prateleira = 0 
		uf_perguntalt_chama("Existem com campos por preencher. Todos os campos s�o de preenchimento obrigat�rio.","OK","",16)
		RETURN .f.
	ENDIF
	
	** A fun�ao java s� aceita referencia com 7 digitos certos
	Select uCrsMapaRobotInsert
	IF LEN(ALLTRIM(uCrsMapaRobotInsert.ref)) != 7
		uf_perguntalt_chama("A refer�ncias devem ter 7 digitos.","OK","",16)
		RETURN .f.
	ENDIF
		
	** Existencia de referencia
	TEXT TO lcSQL NOSHOW TEXTMERGE
		exec up_stocks_procuraRef '<<ALLTRIM(uCrsMapaRobotInsert.ref)>>', <<mysite_nr>>
	ENDTEXT
	IF !uf_gerais_actGrelha("","ucrsExisteRefMapa",lcSQL)
		uf_perguntalt_chama("Ocorreu uma anomalia a verificar a exist�ncia da refer�ncia.","OK","",16)
		RETURN .f.
	ENDIF	

	IF RECCOUNT("ucrsExisteRefMapa")==0
		uf_perguntalt_chama("A Refer�ncia introduzida n�o existe!","OK","",32)
		RETURN .f.
	ENDIF
	
	
	** Se mudou o armario ou o canal
	IF uCrsMapaRobotInsert.Armario != MapaRobot.lcarmario OR uCrsMapaRobotInsert.Canal != MapaRobot.lcCanal
		** Valida Campos chave
		TEXT TO lcSQL NOSHOW TEXTMERGE
			Select 
				ref 
			from 
				robo_consis_map 
			where 
				armario = <<uCrsMapaRobotInsert.Armario>>
				and canal = <<uCrsMapaRobotInsert.Canal>>
		ENDTEXT

		IF !uf_gerais_actGrelha("","ucrsValidaChaveMapa",lcSQL)
			uf_perguntalt_chama("Ocorreu uma anomalia a atualizar o Mapa. Por favor contacte o suporte.","OK","",16)
			RETURN .f.
		ENDIF	

		IF RECCOUNT("ucrsValidaChaveMapa")>0
			uf_perguntalt_chama("A posi��o no Armario:" + ALLTRIM(STR(uCrsMapaRobotInsert.Armario)) + ", Canal: " + ALLTRIM(STR(uCrsMapaRobotInsert.canal)) + " j� est� preenchida!","OK","",64)
			RETURN .f.
		ENDIF
	ENDIF
	
	RETURN .t. 
ENDFUNC 


**
FUNCTION uf_MapaRobot_sinalizar
	LOCAL lcWsPath, lcTerm 
	lcWsPath = ""
	lcTerm	= substr("000",1,len("000")-len(astr(myTermNo))) + astr(myTermNo)
	lcIDCliente = uf_gerais_getParameter('ADM0000000157','TEXT')
	

	SELECT uCrsMapaRobot
	IF EMPTY(ALLTRIM(uCrsMapaRobot.ref))
		uf_perguntalt_chama("Selecione o produto a sinalizar.","OK","",64)
		RETURN .f.
	ENDIF
	
	IF FILE(ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\Robo\consis\ConsisClient.jar')
		
		lcWsPath = ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + "\Robo\consis\ConsisClient.jar Z00000001" + ALLTRIM(lcTerm) + ALLTRIM(uCrsMapaRobot.ref) + "00001 " + ALLTRIM(lcIDCliente) + " " + ALLTRIM(lcTerm) + " 0"

*!*		_cliptext = lcWsPath
*!*		messagebox(lcWsPath)

		lcWsPath = "javaw -jar " + lcWsPath 
		oWSShell = CREATEOBJECT("WScript.Shell")
		oWSShell.Run(lcWsPath,1,.f.)
	ELSE
		uf_perguntalt_chama("Ficheiro de Localiza��o n�o encontrado. Contacte o suporte.","OK","",32)
		RETURN .f.
	ENDIF
ENDFUNC


**
FUNCTION uf_MapaRobot_eliminar

	Select uCrsMapaRobot
	IF uf_perguntalt_chama("Pretende eliminar esta posi��o do Mapa?","Sim","N�o")
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			exec up_robot_consis_deleteMap <<uCrsMapaRobot.Armario>>,<<uCrsMapaRobot.Canal>>
		ENDTEXT 
		IF !uf_gerais_actGrelha("","",lcSQL)
			uf_perguntalt_chama("OCORREU UM ERRO A ELIMINAR POSI��O DO MAPA.","OK","",16)
			RETURN .f.
		ELSE
			uf_perguntalt_chama("REGISTO ELIMINADO COM SUCESSO.","OK","",64)
		ENDIF	
	ENDIF	
	
	uf_MapaRobot_actualizar()
ENDFUNC


**
FUNCTION uf_MAPAROBOT_sair

	IF myMapaRobotIntroducao == .t. OR myMapaRobotAlteracao == .t.
		**If uf_perguntalt_chama("Deseja cancelar as altera��es?" + CHR(13) + "Ir� perder as �ltimas altera��es feitas.","Sim","N�o")
 			myMapaRobotIntroducao = .f. 
 			myMapaRobotAlteracao = .f.
				
			uf_MapaRobot_alternaMenu()
			uf_MapaRobot_actualizar()
		
		**ENDIF	
	ELSE
		** painel
		MAPAROBOT.hide
		MAPAROBOT.Release
	ENDIF
ENDFUNC






