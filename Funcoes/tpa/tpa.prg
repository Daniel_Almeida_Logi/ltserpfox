** Carrega APPs



** Valida estado do tpa
FUNCTION uf_tpa_operation
	LPARAMETERS lcResponseArray, lcWaitingMessage, lcMessage, lcMessageType, lcAmount
	

	LOCAL lcWsPath,lcWsParams,lcWsCmd,lcToken , lcTPAParam
	STORE '' TO lcWsPath, lcWsParams, lcToken, lcWsCmd
	STORE '&' TO lcAdd
	&& validate if terminal has tpa
	IF EMPTY(ALLTRIM(myTpaStamp)) OR  !uf_gerais_isIp(myTpaIp)         
		RETURN .f.	
	ENDIF
	

	&&maximo a pagar
	
	IF(!EMPTY(lcAmount))
		IF(lcAmount>50000)
			uf_perguntalt_chama("Valor m�ximo ultrapassado.","OK","",16)
			RETURN .f.	
		ENDIF
	ENDIF
	

	IF (ALLTRIM(LcMessageType)=="app.msg.purchase.operation")
		IF(EMPTY(lcAmount))
			uf_perguntalt_chama("Tem de definir um valor a pagar","OK","",16)
			RETURN .f.	
		ENDIF	
	ENDIF

	regua(0,4,lcWaitingMessage)
	lcToken = uf_gerais_stamp()
	lcTPAParam = uf_gerais_getParameter_site('ADM0000000047', 'BOOL', mySite)
	IF (lcTPAParam == .f.)		
		lcNomeJar = 'TPACli.jar'	
		lcWsPath = ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\ltsTpa\' + ALLTRIM(lcNomeJar)
		lcWsDir	= ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\ltsTpa'
		&& validate path to file
		IF !FILE(lcWsPath)
			uf_perguntalt_chama("FICHEIRO DE CONFIGURA��O N�O ENCONTRADO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			regua(2)
			RETURN .f.
		ENDIF 
		

		&&construct param to java
		
		
		regua(1,3,lcWaitingMessage)
			
		
		lcWsParams = ' "--TOKEN=' 						+  ALLTRIM(lcToken)  	  	 	 + ["] +;
					 ' "--MSGTYPE=' 					+  ALLTRIM(lcMessageType) 	 	 + ["] +; 
					 ' "--MSG=' 						+  ALLTRIM(lcMessage) 	  	 	 + ["] +;
					 ' "--TERMINAL=' 					+  ALLTRIM(STR(myTermNo))	 	 + ["] +;
					 ' "--IDCL=' 						+  ALLTRIM(uCrsE1.id_lt)   	 	 + ["] +; 
					 ' "--AMOUNT=' 						+  ALLTRIM(PADL(lcAmount,20))	 + ["] +; 
					 ' "--TPAMODEL=' 					+  ALLTRIM(myTpaModel) 	  	 	 + ["] +; 
					 ' "--TPAVERSION=' 					+  ALLTRIM(myTpaVersion)  	 	 + ["] +; 
					 ' "--TPASPECIFICATION_VERSION=' 	+  ALLTRIM(myTpaSpecVersion) 	 + ["] +; 
					 ' "--TPAIP=' 						+  ALLTRIM(myTpaIp) 		 	 + ["] +; 
					 ' "--TPAPORT=' 					+  ALLTRIM(STR(myTpaPort))   	 + ["] +;
					 ' "--WAINTING_MESSAGE=' 			+  ALLTRIM(lcWaitingMessage)   	 + ["] 
					 
		
		regua(2)
		
		
		
		
		&&execute
		&&cmd /c cd C:\Logitools\Versoes\15_13_58_02\ltsTpa\ & javaw.exe -jar C:\Logitools\Versoes\15_13_58_02\ltsTpa\TPACli.jar
		 
		&&lcWsCmd = " javaw -jar " + lcWsPath + " " + lcWsParams
	    lcWsCmd = "cmd /c cd /d " + lcWsDir+ " "+  lcAdd + lcAdd + " javaw -Dfile.encoding=UTF-8  -jar " + lcWsPath + " " + lcWsParams 
		
		oWSShell = CREATEOBJECT("WScript.Shell")
		oWSShell.Run(lcWsCmd, 0, .t.)
	
	ELSE 
		uf_tpa_sendTPAOperation (ALLTRIM(lcToken),ALLTRIM(lcMessageType),ALLTRIM(lcMessage),ALLTRIM(PADL(lcAmount,20)),ALLTRIM(STR(myTermNo)),LOWER(ALLTRIM(uCrsE1.id_lt)),ALLTRIM(myTpaIp),ALLTRIM(STR(myTpaPort)),ALLTRIM(myTpaVersion),ALLTRIM(myTpaModel),ALLTRIM(myTpaSpecVersion),ALLTRIM(lcWaitingMessage))
		regua(2)
	ENDIF 
	&&regua(1,4,lcWaitingMessage)

	
	&& resultado to pedido
	TEXT TO lcSQL NOSHOW TEXTMERGE
		EXEC [dbo].[up_tpa_resultadoOp]  '<<ALLTRIM(lcToken)>>'
	ENDTEXT 
	IF !uf_gerais_actgrelha("", "uCrsResOp", lcSQL)
		uf_perguntalt_chama("N�o foi poss�vel comunicar com a base de dados. Por favor contacte o suporte." ,"OK","",32)
		regua(2)
		fecha("uCrsResOp")
		RETURN .f.
	ELSE
		IF(RECCOUNT("uCrsResOp")==0)	
			uf_perguntalt_chama("N�o foi poss�vel obter os dados. Por favor contacte o suporte." ,"OK","",32)
			regua(2)
			RETURN .f.
		ENDIF		
	ENDIF
	
	&&regua(1,4,lcWaitingMessage)

	
	uf_tpa_processResponse(lcMessageType,@lcResponseArray)


	fecha("uCrsResOp")

	&&regua(2)
	
	RETURN lcResponseArray
	

ENDFUNC

FUNCTION  uf_tpa_messagemResposta
	LPARAMETERS lcMessage
	
	
	IF(!EMPTY(lcMessage))
		RETURN "N�o foi poss�vel realizar a opera��o no TPA."
	ELSE
		RETURN  ALLTRIM(lcResponseArray(2))
	ENDIF
	

ENDFUNC



FUNCTION uf_tpa_processResponse
	LPARAMETERS lcMessageType, lcResponseArray
	
	
	 DO CASE 
	 
   		CASE ALLTRIM(lcMessageType)=="app.msg.status.tpa"
   			
   			&&caso erro
   			STORE uCrsResOp.code TO lcResponseArray(1)
   			IF(uCrsResOp.code==1)
   				STORE uCrsResOp.msg TO lcResponseArray(2)

   			ELSE
   				IF(ALLTRIM(uCrsResOp.statusTpa))=="2"
   					STORE "Aberto" TO lcResponseArray(2)	
   				ELSE
   					IF(ALLTRIM(uCrsResOp.statusTpa))=="1"
   						STORE "Fechado" TO lcResponseArray(2)
   					ELSE
   						STORE "N/D" TO lcResponseArray(2)
   					ENDIF   					
   				ENDIF			
   			ENDIF
   			
	
  		OTHERWISE			
  				
  			STORE uCrsResOp.code TO lcResponseArray(1)
  			STORE ALLTRIM(uCrsResOp.msg)  TO lcResponseArray(2)
  			
	ENDCASE
	

ENDFUNC



FUNCTION uf_tpa_sendTPAOperation
	LPARAMETERS lcToken, lcMsgType, lcMsg ,lcAmount, lcTerminal, lcIDCl, lcTPAIP, lcTPAPort, lcTPAVersion, lcTPAModel, lcTPASpecification_Version, lcWaiting_Message
	LOCAL loReq, loHttp, lnSuccess,	lnSuccess1,	lcJsonText,	loResp , lcBodyTPA,lcResponseHeaderCode,lcGeneric_reply, lcMsgTypeReply,lcCode,lcServiceHash,lcStringMessage, lcI,lcAi,lcErroStatus,lcStatusTpa_reply,lcAccountPeriodOpenClose,lcLastReceipt,lcPurchaseOperation
	REGUA(1,2,"A Comunicar com o TPA",.f.,lcWaiting_Message)
	uf_servicos_unblockChilkat()
	loReq = CreateObject('Chilkat_9_5_0.HttpRequest')
	loHttp = CreateObject('Chilkat_9_5_0.Http')

	loJson = CreateObject('Chilkat_9_5_0.JsonObject')

	loHttp.AcceptCharset = ""
	loHttp.UserAgent = ""
	loHttp.AcceptLanguage = ""

	loHttp.AllowGzip = 0
	IF (lcMsgType== "app.msg.purchase.operation")
		loHttp.ConnectTimeout  = 180
  		loHttp.ReadTimeout = 180
	ELSE 
		loHttp.ConnectTimeout  = 80
  		loHttp.ReadTimeout = 80
	ENDIF 				

	LcTokenReq = 'cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3Z'
  	lnSuccess = loHttp.AddQuickHeader("o-auth-token",LcTokenReq)
  	lcPostHTTP='https://app.lts.pt:50001/api/tpa/relay?id='+LOWER(ALLTRIM(lcIDCl))
  	lcBodyTPA = uf_tpa_createBodyTPAOperation(lcToken, lcMsgType, lcMsg, lcAmount,'', '', lcTerminal,lcIDCl,'0', lcTPAIP, lcTPAPort, lcTPAVersion, lcTPAModel, lcTPASpecification_Version )
	WAIT WINDOW '' TIMEOUT 0.1
	loResp = loHttp.PostJson2(lcPostHTTP,"application/json",lcBodyTPA)
	
	IF (loHttp.LastMethodSuccess <> 1) THEN
	    uf_tpa_saveStatus (lcToken,lcMsgType,-1,"Erro timeout de comunica��o",'',lcTerminal)
	ELSE
	    lcResponseHeaderCode = loResp.StatusCode
	    loJson.Load(STRCONV(loResp.BodyStr,11))
		loJson.EmitCompact = 0
	    lcErroStatus  =  loJson.ObjectOf("status")
		IF (lcResponseHeaderCode ==200 AND loJson.LastMethodSuccess==0)
			lcGeneric_reply  = loJson.ObjectOf("generic_reply")
			IF (loJson.LastMethodSuccess ==1)
				IF (Alltrim(UPPER(lcGeneric_reply.StringOf("msgType")))!="NULL" AND len(lcGeneric_reply.StringOf("msgType"))!=0)
					lcMsgTypeReply= Alltrim(lcGeneric_reply.StringOf("msgType"))
				ELSE 
					lcMsgTypeReply =''
				ENDIF 
				IF (Alltrim(UPPER(lcGeneric_reply.StringOf("responseCode")))!="NULL" AND len(lcGeneric_reply.StringOf("responseCode"))!=0)
					lcCode = Alltrim(lcGeneric_reply.StringOf("responseCode"))
				ELSE 
					lcCode = ''
				ENDIF  
				lcStringMessage = uf_tpa_translateMessage(lcMsgTypeReply,lcCode,lcWaiting_Message)
				uf_tpa_saveStatus (lcToken,UPPER(lcMsgType),lcCode,lcStringMessage,'',lcTerminal)
				DO CASE				
					CASE lcMsgTypeReply== "app.msg.accounting.period.open"
						lcAccountPeriodOpenClose = loJson.ObjectOf("accountPeriodOpenClose_reply")
						IF (!ISNULL("lcAccountPeriodOpenClose"))
							uf_tpa_readOpenCloseTPA(lcToken,lcAccountPeriodOpenClose,lcStringMessage)
							wait timeout 1
						ELSE 
							uf_perguntalt_chama("OCORREU UMA ANOMALIA A RECEBER A RESPOSTA DO OPEN ", "OK",16)
						ENDIF
					CASE lcMsgTypeReply== "app.msg.accounting.period.close"
						lcAccountPeriodOpenClose = loJson.ObjectOf("accountPeriodOpenClose_reply")
						IF (!ISNULL("lcAccountPeriodOpenClose"))
							uf_tpa_readOpenCloseTPA(lcToken,lcAccountPeriodOpenClose,lcStringMessage)
							wait timeout 1
						ELSE 
							uf_perguntalt_chama("OCORREU UMA ANOMALIA A RECEBER A RESPOSTA DO CLOSE ", "OK",16)
						ENDIF
					CASE lcMsgTypeReply== "app.msg.menu.maintenance"
						lcLastReceipt = loJson.ObjectOf("generic_reply")
						IF (!ISNULL("lcLastReceipt"))
							uf_tpa_lastReceiptTPAGeneric(lcToken,lcLastReceipt,lcStringMessage)
							wait timeout 1
						ELSE 
							uf_perguntalt_chama("OCORREU UMA ANOMALIA A RECEBER A RESPOSTA DA ULTIMA IMPRESS�O", "OK",16)
						ENDIF				
					CASE lcMsgTypeReply== "app.msg.status.tpa"
						lcStatusTpa_reply  = loJson.ObjectOf("statusTpa_reply")
						IF (!ISNULL("lcStatusTpa_reply"))
							uf_tpa_readDetailTPA(lcToken,lcStatusTpa_reply,lcStringMessage)
							wait timeout 1
						ELSE 
							uf_perguntalt_chama("OCORREU UMA ANOMALIA A RECEBER A RESPOSTA DO STATUS ", "OK",16)
						ENDIF 
					CASE lcMsgTypeReply== "app.msg.purchase.operation"
						lcPurchaseOperation = loJson.ObjectOf("puchase_reply")
						IF (!ISNULL("lcPurchaseOperation"))
							uf_tpa_PurchaseOperationTPA(lcToken,lcPurchaseOperation,lcStringMessage)
							wait timeout 3
						ELSE 
							uf_perguntalt_chama("OCORREU UMA ANOMALIA A RECEBER A RESPOSTA DA OPERACAO", "OK",16)
						ENDIF  	
					OTHERWISE
							uf_perguntalt_chama("OCORREU UMA ANOMALIA A RECEBER A RESPOSTA", "OK",16)
				ENDCASE
			ENDIF   
		ELSE
			IF (loJson.LastMethodSuccess ==1)
				IF (Alltrim(UPPER(lcErroStatus.StringOf("i")))!="NULL" AND len(lcErroStatus.StringOf("i"))!=0)
					lcI= Alltrim(lcErroStatus.StringOf("i"))
				ENDIF 
				IF (Alltrim(UPPER(lcErroStatus.StringOf("ai")))!="NULL" AND len(lcErroStatus.StringOf("ai"))!=0)
					lcAi= Alltrim(lcErroStatus.StringOf("ai"))
				ENDIF 			
				lcStringMessage = uf_tpa_translateMessage("erro","9999",lcWaiting_Message)
				uf_tpa_saveStatus (lcToken,lcMsgType,lcI,lcStringMessage, lcAi,lcTerminal)
				REGUA(1,4,"A Comunicar com o TPA",.f.,lcStringMessage)
				wait timeout 1
			ENDIF 		
		ENDIF  
	ENDIF
ENDFUNC


FUNCTION uf_tpa_createBodyTPAOperation
	LPARAMETERS lcToken, lcMsgType, lcMsg, lcAmount, lcReceipDate, lcReceiptTime ,lcTerminal, lcIDCl,lcTest, lcTPAIP, lcTPAPort, lcTPAVersion, lcTPAModel, lcTPASpecification_Version
	LOCAL lcBody
	
	lcBody = ''
	lcBody = lcBody+ '{'
	lcBody = lcBody+ '"token": "'+ALLTRIM(lcToken)+'",'
	lcBody = lcBody+ '"msgType": "'+ALLTRIM(lcMsgType)+'",'
	lcBody = lcBody+ '"msg": "'+ALLTRIM(lcMsg)+'",'
	lcBody = lcBody+ '"amount": "'+ALLTRIM(lcAmount)+'",'
	lcBody = lcBody+ '"receiptDate": "'+ALLTRIM(lcReceipDate)+'",'
	lcBody = lcBody+ '"lcReceiptTime ": "'+ALLTRIM(lcReceiptTime)+'",'
	lcBody = lcBody+ '"terminal": "'+ALLTRIM(lcTerminal)+'",'
	lcBody = lcBody+ '"idCl": "'+ALLTRIM(lcIDCl)+'",'
	lcBody = lcBody+ '"test": "'+ALLTRIM(lcTest)+'",'
	lcBody = lcBody+ '"ip": "'+ALLTRIM(lcTPAIP)+'",'
	lcBody = lcBody+ '"port":'+ALLTRIM(lcTPAPort)+','
	lcBody = lcBody+ '"version": "'+ALLTRIM(lcTPAVersion)+'",'
	lcBody = lcBody+ '"model": "'+ALLTRIM(lcTPAModel)+'",'
	lcBody = lcBody+ '"specification_version": "'+ALLTRIM(lcTPASpecification_Version)+'"'
	lcBody = lcBody+ '}'
	
	RETURN lcBody

ENDFUNC

FUNCTION uf_tpa_translateMessage
	LPARAMETERS lcMsgType,lcCode,lcWaiting_Message
	LOCAL lcTextmessage
	STORE '' TO lcTextmessage
	lcTextmessage = ""
	IF USED("uCrsTPAMessage")
		SELECT uCrsTPAMessage
		GO TOP
		LOCATE FOR (ALLTRIM(uCrsTPAMessage.code) == ALLTRIM(lcCode) AND ALLTRIM(uCrsTPAMessage.type) == ALLTRIM(lcMsgType))
		IF  FOUND()
			lcTextmessage = ALLTRIM(uCrsTPAMessage.message)
		ELSE 
			lcTextmessage = "N�o foi poss�vel realizar a opera��o no tpa."
		ENDIF   
	ENDIF 
	IF (!EMPTY(lcTextmessage))
		RETURN lcTextmessage 
	ELSE
		RETURN lcWaiting_Message
	ENDIF 
		
ENDFUNC


FUNCTION uf_tpa_saveStatus 
		LPARAMETERS lcToken,lcMsgType,lcCode,lcStringMessage, lcServiceHash,lcTerminal
	lcSQL = ""
	TEXT TO lcSQL NOSHOW textmerge
		INSERT INTO tpa_msg(token, request, code, msg, serviceHash, terminal, ousrdata)
		values ('<<lcToken>>','<<lcMsgType>>',<<lcCode>>,'<<lcStringMessage>>','<<lcServiceHash>>',<<lcTerminal>>,GETDATE())	
	ENDTEXT
	IF !uf_gerais_actgrelha("", "", lcSql)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA A GUARDAR STATUS DO TPA", "OK",16)
		RETURN .f.
	ENDIF


ENDFUNC


FUNCTION  uf_tpa_readDetailTPA
	LPARAMETERS lcToken,lcStatusTpa_reply,lcStringMessage
	LOCAL lcOperatorMessage,lcProfileDescription,lcPosStatusDescription, lcOperatorMessageDetail
	
	IF (Alltrim(UPPER(lcStatusTpa_reply.StringOf("operatorMessage")))!="NULL" AND len(lcStatusTpa_reply.StringOf("operatorMessage"))!=0)
		lcOperatorMessage= STRCONV(Alltrim(lcStatusTpa_reply.StringOf("operatorMessage")),10)
	ELSE 
		lcOperatorMessage = ""
	ENDIF 
	IF (len(lcStringMessage)!=0)
		lcOperatorMessageDetail=lcStringMessage
	ELSE 
		lcOperatorMessageDetail= lcOperatorMessageDetail
	ENDIF 
	IF (Alltrim(UPPER(lcStatusTpa_reply.StringOf("profileDescription")))!="NULL" AND len(lcStatusTpa_reply.StringOf("profileDescription"))!=0)
		lcProfileDescription= Alltrim(lcStatusTpa_reply.StringOf("profileDescription"))
	ELSE 
		lcProfileDescription = ""
	ENDIF 
	IF (Alltrim(UPPER(lcStatusTpa_reply.StringOf("posStatusDescription")))!="NULL" AND len(lcStatusTpa_reply.StringOf("posStatusDescription"))!=0)
		lcPosStatusDescription= Alltrim(lcStatusTpa_reply.StringOf("posStatusDescription"))
	ELSE 
		lcPosStatusDescription= ""
	ENDIF 		
	IF (lcOperatorMessage== "")
		lcOperatorMessage = lcOperatorMessageDetail
	ENDIF 
	REGUA(1,4,"A Comunicar com o TPA",.f.,lcOperatorMessage)	
	uf_tpa_saveDetailTPA(lcToken,lcOperatorMessageDetail,lcProfileDescription,lcPosStatusDescription)
	
ENDFUNC

FUNCTION uf_tpa_saveDetailTPA
		LPARAMETERS lcToken,lcOperatorMessage,lcProfileDescription,lcPosStatusDescription

	lcSQL = ""
	TEXT TO lcSQL NOSHOW textmerge
		INSERT INTO tpa_msg_d (token,operatorMessage,profileDescription,posStatusDescription) 
		VALUES ('<<lcToken>>','<<lcOperatorMessage>>','<<lcProfileDescription>>','<<lcPosStatusDescription>>')	
	ENDTEXT

	IF !uf_gerais_actgrelha("", "", lcSql)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA A GUARDAR DETAIL DO TPA GETSTATUS", "OK",16)
		RETURN .f.
	ENDIF

ENDFUNC


FUNCTION  uf_tpa_readOpenCloseTPA
	LPARAMETERS lcToken,lcAccountPeriodOpenClose,lcStringMessage,lcOperatorMessageOpenClose
	LOCAL lcrfu,lccontrolNr,lcoperatorMessage,lctotal,lcError
	
	IF (Alltrim(UPPER(lcAccountPeriodOpenClose.StringOf("rfu")))!="NULL" AND len(lcAccountPeriodOpenClose.StringOf("rfu"))!=0)
		lcRfu= Alltrim(lcAccountPeriodOpenClose.StringOf("rfu"))
	ELSE
		lcRfu =""	
	ENDIF 
	IF (Alltrim(UPPER(lcAccountPeriodOpenClose.StringOf("controlNr")))!="NULL" AND len(lcAccountPeriodOpenClose.StringOf("controlNr"))!=0)
		lcControlNr= Alltrim(lcAccountPeriodOpenClose.StringOf("controlNr"))
	ENDIF 	
	IF (Alltrim(UPPER(lcAccountPeriodOpenClose.StringOf("operatorMessage")))!="NULL" AND len(lcAccountPeriodOpenClose.StringOf("operatorMessage"))!=0)
		lcOperatorMessage= Alltrim(lcAccountPeriodOpenClose.StringOf("operatorMessage"))
	ELSE 
		lcOperatorMessage= ""
	ENDIF 	
	IF (len(lcStringMessage)!=0)
		lcOperatorMessageOpenClose=lcStringMessage
	ELSE 
		lcOperatorMessageOpenClose =lcOperatorMessage
	ENDIF 	
	IF (Alltrim(UPPER(lcAccountPeriodOpenClose.StringOf("total")))!="NULL" AND len(lcAccountPeriodOpenClose.StringOf("total"))!=0)
		lcTotal= Alltrim(lcAccountPeriodOpenClose.StringOf("total"))
	ELSE 
		lcTotal=""
	ENDIF  
	IF (Alltrim(UPPER(lcAccountPeriodOpenClose.StringOf("error")))!="NULL" AND len(lcAccountPeriodOpenClose.StringOf("error"))!=0)
		lcError= Alltrim(lcAccountPeriodOpenClose.StringOf("error"))
	ELSE 
		lcError = ""
	ENDIF 
	IF (lcOperatorMessage== "")
		lcOperatorMessage = lcOperatorMessageOpenClose
	ENDIF 
	
	REGUA(1,4,"A Comunicar com o TPA",.f.,lcOperatorMessage)	
	uf_tpa_saveOpenCloseTPA(lcToken,lcOperatorMessageOpenClose,lcRfu,lcControlNr,lcTotal,lcError)
	
ENDFUNC

FUNCTION uf_tpa_saveOpenCloseTPA
		LPARAMETERS lcToken,lcOperatorMessage,lcRfu,lcControlNr,lcTotal,lcError

	lcSQL = ""
	TEXT TO lcSQL NOSHOW textmerge
		INSERT INTO tpa_msg_d (token,operatorMessage,rfu,controlNr,total,error) 
		VALUES ('<<lcToken>>','<<lcOperatorMessage>>','<<lcRfu>>','<<lcControlNr>>','<<lcTotal>>','<<lcError>>')	
	ENDTEXT

	IF !uf_gerais_actgrelha("", "", lcSql)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA A GUARDAR DETAIL DO TPA GETSTATUS", "OK",16)
		RETURN .f.
	ENDIF

ENDFUNC

FUNCTION  uf_tpa_lastReceiptTPA
	LPARAMETERS lcToken,lcEcrsPos ,lcStringMessage
	LOCAL lcOperatorMessage,lcProfileDescription,lcPosStatusDescription,lcOperatorMessageLastReceipt
	
	IF (Alltrim(UPPER(lcEcrsPos.StringOf("operatorMessage")))!="NULL" AND len(lcEcrsPos.StringOf("operatorMessage"))!=0)
		lcOperatorMessage= Alltrim(lcEcrsPos.StringOf("operatorMessage"))
	ELSE 
		lcOperatorMessage= ""
	ENDIF 
			
	IF (len(lcStringMessage)!=0)
		lcOperatorMessageLastReceipt= lcStringMessage
	ELSE 
		lcOperatorMessageLastReceipt = lcOperatorMessage
	ENDIF 
	
	IF (lcOperatorMessage== "")
		lcOperatorMessage = lcOperatorMessageLastReceipt 
	ENDIF 
	
	REGUA(1,4,"A Comunicar com o TPA",.f.,lcOperatorMessage)
	
ENDFUNC



FUNCTION  uf_tpa_lastReceiptTPAGeneric
	LPARAMETERS lcToken,lcEcrsPos ,lcStringMessage
	LOCAL lcOperatorMessage,lcProfileDescription,lcPosStatusDescription,lcOperatorMessageLastReceipt
	
	IF (Alltrim(UPPER(lcEcrsPos.StringOf("responseCode")))!="NULL")
		if(lcEcrsPos.StringOf("responseCode")=="000")
			lcOperatorMessage= "OK"
		ELSE
			lcOperatorMessage="NOT OK"
		ENDIF
			
	ELSE 
		lcOperatorMessage= ""
	ENDIF 
			
	IF (len(lcStringMessage)!=0)
		lcOperatorMessageLastReceipt= lcStringMessage
	ELSE 
		lcOperatorMessageLastReceipt = lcOperatorMessage
	ENDIF 
	
	IF (lcOperatorMessage== "")
		lcOperatorMessage = lcOperatorMessageLastReceipt 
	ENDIF 
	
	REGUA(1,4,"A Comunicar com o TPA",.f.,lcOperatorMessage)
	
ENDFUNC

FUNCTION  uf_tpa_PurchaseOperationTPA
	LPARAMETERS lcToken,lcPurchaseOperation,lcStringMessage
	LOCAL lcOperatorMessage,lcControlNumber,lcRfu,lcPosIdentification,lcResponseDateTime,lcsAnAuthorization,lcIssuerName,lcAmmountText,lcReceiptFormat,lcBinCard,lcTransactionType,lcNrOfOfflineTRXsavedInPos,lcErrorClass, lcOperatorMessagePurchase
	
	IF (Alltrim(UPPER(lcPurchaseOperation.StringOf("controlNumber")))!="NULL" AND len(lcPurchaseOperation.StringOf("controlNumber"))!=0)
		lcControlNumber= Alltrim(lcPurchaseOperation.StringOf("controlNumber"))
	ELSE
		lcControlNumber=""	
	ENDIF 
	IF (Alltrim(UPPER(lcPurchaseOperation.StringOf("rfu")))!="NULL" AND len(lcPurchaseOperation.StringOf("rfu"))!=0)
		lcRfu= Alltrim(lcPurchaseOperation.StringOf("rfu"))
	ELSE
		lcRfu=""	
	ENDIF 
	

	IF (Alltrim(UPPER(lcPurchaseOperation.StringOf("operatorMessage")))!="NULL" AND len(lcPurchaseOperation.StringOf("operatorMessage"))!=0)
		lcOperatorMessage= Alltrim(lcPurchaseOperation.StringOf("operatorMessage"))
	ELSE 
		lcOperatorMessage= ""
	ENDIF 
 
 	IF (len(lcStringMessage)!=0)
		lcOperatorMessagePurchase=lcStringMessage
	ELSE 
		lcOperatorMessagePurchase=lcOperatorMessage
	ENDIF 
 
	IF (Alltrim(UPPER(lcPurchaseOperation.StringOf("posIdentification")))!="NULL" AND len(lcPurchaseOperation.StringOf("posIdentification"))!=0)
		lcPosIdentification= Alltrim(lcPurchaseOperation.StringOf("posIdentification"))
	ELSE 
		lcPosIdentification=""
	ENDIF  
	IF (Alltrim(UPPER(lcPurchaseOperation.StringOf("responseDateTime")))!="NULL" AND len(lcPurchaseOperation.StringOf("responseDateTime"))!=0)
		lcResponseDateTime= Alltrim(lcPurchaseOperation.StringOf("responseDateTime"))
	ELSE 
		lcResponseDateTime= ""
	ENDIF 
	IF (Alltrim(UPPER(lcPurchaseOperation.StringOf("sanAuthorization")))!="NULL" AND len(lcPurchaseOperation.StringOf("sanAuthorization"))!=0)
		lcsAnAuthorization= Alltrim(lcPurchaseOperation.StringOf("sanAuthorization"))
	ELSE
		lcsAnAuthorization=""	
	ENDIF 
	IF (Alltrim(UPPER(lcPurchaseOperation.StringOf("issuerName")))!="NULL" AND len(lcPurchaseOperation.StringOf("issuerName"))!=0)
		lcIssuerName= Alltrim(lcPurchaseOperation.StringOf("issuerName"))
	ELSE
		lcIssuerName=""	
	ENDIF 
	IF (Alltrim(UPPER(lcPurchaseOperation.StringOf("ammountText")))!="NULL" AND len(lcPurchaseOperation.StringOf("ammountText"))!=0)
		lcAmmountText= Alltrim(lcPurchaseOperation.StringOf("ammountText"))
	ELSE
		lcAmmountText=""	
	ENDIF 
	IF (Alltrim(UPPER(lcPurchaseOperation.StringOf("receiptFormat")))!="NULL" AND len(lcPurchaseOperation.StringOf("receiptFormat"))!=0)
		lcReceiptFormat= Alltrim(lcPurchaseOperation.StringOf("receiptFormat"))
	ELSE
		lcReceiptFormat=""	
	ENDIF 
	IF (Alltrim(UPPER(lcPurchaseOperation.StringOf("binCard")))!="NULL" AND len(lcPurchaseOperation.StringOf("binCard"))!=0)
		lcBinCard= Alltrim(lcPurchaseOperation.StringOf("binCard"))
	ELSE
		lcBinCard=""	
	ENDIF 
	IF (Alltrim(UPPER(lcPurchaseOperation.StringOf("transactionType")))!="NULL" AND len(lcPurchaseOperation.StringOf("transactionType"))!=0)
		lcTransactionType= Alltrim(lcPurchaseOperation.StringOf("transactionType"))
	ELSE
		lcTransactionType=""	
	ENDIF 
		IF (Alltrim(UPPER(lcPurchaseOperation.StringOf("nrOfOfflineTRXsavedInPos")))!="NULL" AND len(lcPurchaseOperation.StringOf("nrOfOfflineTRXsavedInPos"))!=0)
		lcNrOfOfflineTRXsavedInPos= Alltrim(lcPurchaseOperation.StringOf("nrOfOfflineTRXsavedInPos"))
	ELSE
		lcNrOfOfflineTRXsavedInPos=""	
	ENDIF 
	IF (Alltrim(UPPER(lcPurchaseOperation.StringOf("errorClass")))!="NULL" AND len(lcPurchaseOperation.StringOf("errorClass"))!=0)
		lcErrorClass= Alltrim(lcPurchaseOperation.StringOf("errorClass"))
	ELSE
		lcErrorClass=""	
	ENDIF 
	
	IF (lcOperatorMessage== "")
		lcOperatorMessage = lcOperatorMessagePurchase
	ENDIF 
	
	REGUA(1,4,"A Comunicar com o TPA",.f.,lcOperatorMessage)
	uf_tpa_savePurchaseOperationTPA(lcToken,lcOperatorMessagePurchase,lcControlNumber,lcRfu,lcPosIdentification,lcResponseDateTime,lcsAnAuthorization,lcIssuerName,lcAmmountText,lcReceiptFormat,lcBinCard,lcTransactionType,lcNrOfOfflineTRXsavedInPos,lcErrorClass)
	
ENDFUNC

FUNCTION uf_tpa_savePurchaseOperationTPA
		LPARAMETERS lcToken,lcOperatorMessage,lcControlNumber,lcRfu,lcPosIdentification,lcResponseDateTime,lcsAnAuthorization,lcIssuerName,lcAmmountText,lcReceiptFormat,lcBinCard,lcTransactionType,lcNrOfOfflineTRXsavedInPos,lcErrorClass

	lcSQL = ""
	TEXT TO lcSQL NOSHOW textmerge
		INSERT INTO tpa_msg_d (token,operatorMessage,controlNumber,rfu,posIdentification,responseDateTime,sanAuthorization,issuerName,ammountText,receiptFormat,binCard,transactionType,nrOfOfflineTRXsavedInPos,errorClass)
		VALUES ('<<lcToken>>','<<lcOperatorMessage>>','<<lcControlNumber>>','<<lcRfu>>','<<lcPosIdentification>>','<<lcResponseDateTime>>','<<lcsAnAuthorization>>','<<lcIssuerName>>','<<lcAmmountText>>','<<lcReceiptFormat>>','<<lcBinCard>>','<<lcTransactionType>>','<<lcNrOfOfflineTRXsavedInPos>>','<<lcErrorClass>>')	
	ENDTEXT

	IF !uf_gerais_actgrelha("", "", lcSql)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA A GUARDAR DETAIL DO TPA GETSTATUS", "OK",16)
		RETURN .f.
	ENDIF

ENDFUNC


