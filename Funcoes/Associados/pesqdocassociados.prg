**Prgs associados
DO MOVBANCARIOS
DO permissoesassociados
DO faturacaoExterna
DO importCartao

** Func�o Abrir Painel Pesquisa Documentos
FUNCTION uf_PESQDOCASSOCIADOS_chama

	PUBLIC myPesqdocassociadosIntroducao, myPesqdocassociadosAlteracao
	
	STORE .f. TO myPesqdocassociadosIntroducao
	STORE .f. TO myPesqdocassociadosAlteracao
		
	** Verifica Licenciamento
	IF (uf_ligacoes_addConnection('ASS') == .f.)
		RETURN .F.
	ENDIF

	IF TYPE("PESQDOCASSOCIADOS") == "U"
		
		uf_PESQDOCASSOCIADOS_CriaCursoresDefault()
		
		DO FORM PESQDOCASSOCIADOS
		
		&&Configura Menu Barra Lateral
		WITH PESQDOCASSOCIADOS.menu1
			 .adicionaOpcao("aplicacoes", "Aplica��es", myPath + "\imagens\icons\aplicacoes_w.png", "uf_gerais_menuAplicacoes with 'PESQDOCASSOCIADOS'","F1")	
			 .adicionaOpcao("opcoes","Op��es", myPath + "\imagens\icons\opcoes_w.png","","F2")
			 .adicionaOpcao("actualizar","Pesquisar",myPath + "\imagens\icons\lupa_w.png","uf_PESQDOCASSOCIADOS_actualizar","F3")
			 .adicionaOpcao("novo","Novo", myPath + "\imagens\icons\mais_w.png","uf_PESQDOCASSOCIADOS_novoDoc","F4")
			 .adicionaOpcao("editar","Editar", myPath + "\imagens\icons\lapis_w.png","uf_PESQDOCASSOCIADOS_alterarDoc","F5")
       		 .adicionaOpcao("novaLinha","Nova Linha", myPath + "\imagens\icons\mais_w.png","uf_PESQDOCASSOCIADOS_adicionaLinha","F6")
			 .adicionaOpcao("eliminarLinha","Eliminar Linha", myPath + "\imagens\icons\cruz_w.png","uf_PESQDOCASSOCIADOS_eliminaLinha","F7")
			 .adicionaOpcao("navegaFT","Ver Doc. FT.", myPath + "\imagens\icons\entrar_w.png","uf_PESQDOCASSOCIADOS_navegaFT","")
			 .adicionaOpcao("seltodos","Sel. Todos", myPath + "\imagens\icons\checked_w.png","uf_PESQDOCASSOCIADOS_seltodos","")
 			 .adicionaOpcao("emitir","Nt. Lan�am.", myPath + "\imagens\icons\doc_linhas_w.png","uf_PESQDOCASSOCIADOS_emitir_NL","")
 			 .adicionaOpcao("emitirr","Resumo Ent.", myPath + "\imagens\icons\doc_linhas_w.png","uf_PESQDOCASSOCIADOS_emitir_res","")
  			 .adicionaOpcao("emitirnle","A.Lanc. Ent.", myPath + "\imagens\icons\doc_linhas_w.png","uf_PESQDOCASSOCIADOS_emitir_nlentidades","")
  			 .adicionaOpcao("exportardoc","Exportar Doc.", myPath + "\imagens\icons\pasta_w.png","uf_imprimirGerais_ExportarDoc","")
		ENDWITH 

		&&Configura Menu Aplica��es
		WITH PESQDOCASSOCIADOS.menu_aplicacoes
			 .adicionaOpcao("resassociados","Gest�o Associados","","uf_PESQDOCASSOCIADOS_ResumoAssociados")
			 .adicionaOpcao("movBancarios","Registo Transfer�ncias","","uf_PESQDOCASSOCIADOS_MOVBANCARIOS")
			 .adicionaOpcao("calculadora", "Calculadora", "", "uf_calculadora_chama")
		ENDWITH 

		&&Configura Menu Op��es		 
		WITH PESQDOCASSOCIADOS.menu_opcoes
			.adicionaOpcao("importdoccompart","Importar Faturas","","uf_PESQDOCASSOCIADOS_importdoccompart WITH 1") 
            .adicionaOpcao("importNc","Importar N.Cr�dito","","uf_PESQDOCASSOCIADOS_importdoccompart WITH 2") 
			.adicionaOpcao("importCartao", "Importar Cart�es", "", "uf_pesqdocassociados_importCartao")
		**	.adicionaOpcao("relatorioftentidades","Resumo Ft. Entidades","","uf_imprimirgerais_Chama with'RESUMOFTENTIDADES'") 
		**	.adicionaOpcao("notalancamentoassociados","Nota Lan�a. Associados","","uf_imprimirgerais_Chama with 'RESUMOFTASSOCIADOS'")
	 	**	.adicionaOpcao("avisolancamentoassociados","Aviso Lan�a. Entidades","","uf_imprimirgerais_Chama with 'RESUMORTASSOCIADOS'") 
		**	.adicionaOpcao("reciboentidades","Recibo Trf. Entidades","","uf_imprimirgerais_Chama with 'RECIBOENTASSOCIADOSREIMP'") 
		ENDWITH
				
	ELSE
		PESQDOCASSOCIADOS.show
	ENDIF
	
	uf_PESQDOCASSOCIADOS_alternaMenu()
ENDFUNC		


**
FUNCTION uf_PESQDOCASSOCIADOS_CriaCursoresDefault
	TEXT TO lcSQL TEXTMERGE NOSHOW
    	exec up_PESQDOCASSOCIADOS_pesquisarDocumentos 'xxxxxx', -1, '', '', 0, '', '', 1900, '', 0, '', ''
   	ENDTEXT 
	
	IF !uf_gerais_actGrelha("","uCrsDocAssociados",lcSql)
		uf_perguntalt_chama("N�O FOI POSSIVEL ACTUALIZAR A LISTAGEM DE DOCUMENTOS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	ENDIF
	
	** Fatura��o a Entidades
	TEXT TO lcSQL TEXTMERGE NOSHOW
    	exec up_PESQDOCASSOCIADOS_pesquisarDocumentos 'xxxxxx', -1, '', '', 0, '', '', 1900, '', 0, '', ''
   	ENDTEXT 
	
	IF !uf_gerais_actGrelha("","uCrsAssociadosFtEntidade",lcSql)
		uf_perguntalt_chama("N�O FOI POSSIVEL ACTUALIZAR A LISTAGEM DE DOCUMENTOS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	ENDIF

	** Faturado �s Farmacias
	TEXT TO lcSQL TEXTMERGE NOSHOW
    	exec up_resumo_faturas_associados 1900, 1, 0, 0, '', ''  
   	ENDTEXT 
	
	IF !uf_gerais_actGrelha("","uCrsAssociadosFtCliente",lcSql)
		uf_perguntalt_chama("N�O FOI POSSIVEL ACTUALIZAR A LISTAGEM DE DOCUMENTOS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	ENDIF
	
	** Faturado �s Farm�cias Detalhe
	TEXT TO lcSQL TEXTMERGE NOSHOW
    	exec up_resumo_faturas_associados_detalhe 1900, 1, 0, 0, '', ''  
   	ENDTEXT 
	
	IF !uf_gerais_actGrelha("","uCrsAssociadosFtClienteDetalhe",lcSql)
		uf_perguntalt_chama("N�O FOI POSSIVEL ACTUALIZAR A LISTAGEM DE DOCUMENTOS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	ENDIF
	
ENDFUNC 


** Func�o Criar Cursor
FUNCTION uf_PESQDOCASSOCIADOS_actualizar
	LOCAL lcSQL
	WITH PESQDOCASSOCIADOS.pageframe1
		IF .activepage = 1

			lcSQL = ""
			TEXT TO lcSql NOSHOW TEXTMERGE 
				exec up_PESQDOCASSOCIADOS_pesquisarDocumentos 
					'<<AllTRIM(PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.nome_org.Value)>>'
					,<<IIF(EMPTY(ALLTRIM(PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.nr_org.Value)),0,ALLTRIM(PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.nr_org.Value))>>
					,'<<ALLTRIM(PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.id_org.Value)>>'
					,'<<Alltrim(PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.nome_cl.Value)>>'
					,<<IIF(EMPTY(ALLTRIM(PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.nr_cl.Value)),0,ALLTRIM(PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.nr_cl.Value))>>
					,'<<Alltrim(PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.id_cl.Value)>>'
					,'<<Alltrim(PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.nrdoc.Value)>>'
					,<<IIF(EMPTY(PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.ano.Value),0,PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.ano.Value)>>
					,<<IIF(EMPTY(PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.mes.Value),0,PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.mes.Value)>>
					,<<IIF(PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.chkdata_tratamento.tag = "true", 1, 0)>>
					,''
					,'<<Alltrim(PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.nrdocimp.Value)>>'
			ENDTEXT

			IF !uf_gerais_actGrelha("PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.GridPesq","uCrsDocAssociados",lcSql)
				uf_perguntalt_chama("N�O FOI POSSIVEL ACTUALIZAR A LISTAGEM DE DOCUMENTOS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				RETURN .f.
			ENDIF
			**SELECT uCrsDocAssociados
			**GO TOP 

			PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.gridpesq.setfocus
			
		ELSE
		
			** Obrigat�rio seleccionar o Cliente
*!*				IF EMPTY(Alltrim(PESQDOCASSOCIADOS.Pageframe1.Page2.nome_cl.Value)) AND EMPTY(Alltrim(PESQDOCASSOCIADOS.Pageframe1.Page2.nr_cl.Value)) AND EMPTY(Alltrim(PESQDOCASSOCIADOS.Pageframe1.Page2.id_cl.Value))
*!*					uf_perguntalt_chama("Indique o cliente por favor.","OK","",64)
*!*					RETURN .f.
*!*				ENDIF

			** Faturado a Entidades
			TEXT TO lcSQL TEXTMERGE NOSHOW
		    	exec up_PESQDOCASSOCIADOS_pesquisarDocumentos 
		    		 ''
		    		,0
		    		,'' 
		    		,'<<Alltrim(PESQDOCASSOCIADOS.Pageframe1.Page2.nome_cl.Value)>>'
		    		,<<IIF(EMPTY(ALLTRIM(PESQDOCASSOCIADOS.Pageframe1.Page2.nr_cl.Value)),0,ALLTRIM(PESQDOCASSOCIADOS.Pageframe1.Page2.nr_cl.Value))>>
		    		,'<<Alltrim(PESQDOCASSOCIADOS.Pageframe1.Page2.id_cl.Value)>>'
		    		,''
			    	,<<IIF(EMPTY(PESQDOCASSOCIADOS.Pageframe1.Page2.ano.Value),0,PESQDOCASSOCIADOS.Pageframe1.Page2.ano.Value)>>
					,<<IIF(EMPTY(PESQDOCASSOCIADOS.Pageframe1.Page2.mes.Value),0,PESQDOCASSOCIADOS.Pageframe1.Page2.mes.Value)>>
					,<<IIF(PESQDOCASSOCIADOS.Pageframe1.Page2.chkdata_tratamento.tag = "true", 1, 0)>>
					,''
					,''
		   	ENDTEXT 	
		   	
 			IF !uf_gerais_actGrelha("PESQDOCASSOCIADOS.Pageframe1.Page2.GridFtEntidade","uCrsAssociadosFtEntidade",lcSql)
				uf_perguntalt_chama("N�O FOI POSSIVEL ACTUALIZAR A LISTAGEM DE DOCUMENTOS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				RETURN .f.
			ENDIF

			** Faturado �s Farmacias
			TEXT TO lcSQL TEXTMERGE NOSHOW
		    	exec up_resumo_faturas_associados_detalhe
			    	<<IIF(EMPTY(PESQDOCASSOCIADOS.Pageframe1.Page2.ano_ft.Value),0,PESQDOCASSOCIADOS.Pageframe1.Page2.ano_ft.Value)>>
			    	,<<IIF(EMPTY(PESQDOCASSOCIADOS.Pageframe1.Page2.mes_ft.Value),0,PESQDOCASSOCIADOS.Pageframe1.Page2.mes_ft.Value)>>
					,<<IIF(EMPTY(ALLTRIM(PESQDOCASSOCIADOS.Pageframe1.Page2.nr_cl.Value)),0,ALLTRIM(PESQDOCASSOCIADOS.Pageframe1.Page2.nr_cl.Value))>>
					,0
					,'<<Alltrim(PESQDOCASSOCIADOS.Pageframe1.Page2.id_cl.Value)>>'
					,'<<Alltrim(PESQDOCASSOCIADOS.Pageframe1.Page2.nome_cl.Value)>>'  
		   	ENDTEXT 
				
			IF !uf_gerais_actGrelha("PESQDOCASSOCIADOS.Pageframe1.Page2.GridFtCliente","uCrsAssociadosFtClienteDetalhe",lcSql)
				uf_perguntalt_chama("N�O FOI POSSIVEL ACTUALIZAR A LISTAGEM DE DOCUMENTOS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				RETURN .f.
			ENDIF
		
			SELECT uCrsAssociadosFtEntidade
			GO TOP
			SELECT uCrsAssociadosFtCliente
			GO TOP
			
			**	
			SELECT uCrsAssociadosFtEntidade
			CALCULATE SUM(valor_pagar) TO lcSubTotalEntidades
			PESQDOCASSOCIADOS.Pageframe1.Page2.SubTotalEntidade.value = lcSubTotalEntidades
			
			SELECT uCrsAssociadosFtClienteDetalhe
			CALCULATE SUM(debito-credito) TO lcSubTotalClientes
			PESQDOCASSOCIADOS.Pageframe1.Page2.SubTotalClientes.value = lcSubTotalClientes
			
			**
			PESQDOCASSOCIADOS.Pageframe1.Page2.TotalEntClientes.value = lcSubTotalEntidades - lcSubTotalClientes
			
			SELECT uCrsAssociadosFtEntidade
			GO Top
			SELECT uCrsAssociadosFtCliente
			GO Top
			
		ENDIF
	ENDWITH
	
	uf_PESQDOCASSOCIADOS_calcvalorestotal()
	
ENDFUNC


** Funcao Criar Novo documento
FUNCTION uf_PESQDOCASSOCIADOS_novoDoc
	
	** Limpa Campos de Pesquisa
	WITH PESQDOCASSOCIADOS.PageFrame1.lancamentoreceituario
		.nome_org.value = ''
		.nr_org.value = ''
		.id_org.value = ''
		.nome_cl.value = ''
		.nr_cl.value = ''
		.id_cl.value = ''
		.nrdoc.value = ''
	ENDWITH 
	
	**
	STORE .t. TO myPesqdocassociadosIntroducao 
	STORE .f. TO myPesqdocassociadosAlteracao

	** Limpa Cursor
	IF USED ("uCrsDocAssociados")
		Select uCrsDocAssociados
		GO Top
		SCAN
			DELETE
		ENDSCAN
		Select uCrsDocAssociados
		APPEND BLANK
		Replace uCrsDocAssociados.ano				 WITH IIF(MONTH(DATE())==1,YEAR(DATE())-1,YEAR(DATE()))
		Replace uCrsDocAssociados.mes				 WITH MONTH(GOMONTH(DATE(YEAR(DATE()), MONTH(DATE()),1),+0)-1)
		Replace uCrsDocAssociados.ano_tratamento 	 WITH IIF(MONTH(DATE())==1,YEAR(DATE())-1,YEAR(DATE()))
		Replace uCrsDocAssociados.mes_tratamento	 WITH MONTH(GOMONTH(DATE(YEAR(DATE()), MONTH(DATE()),1),+0)-1)
		Replace uCrsDocAssociados.imprimir			 WITH .t.
	ENDIF	
	
	uf_PESQDOCASSOCIADOS_alternaMenu()
	uf_PESQDOCASSOCIADOS_calcvalorestotal()
	
	PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.gridPesq.setfocus
	PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.id_cl_novo.setfocus

ENDFUNC 


**Funcao editar documentos
FUNCTION uf_PESQDOCASSOCIADOS_alterarDoc

	STORE .f. TO myPesqdocassociadosIntroducao 
	STORE .t. TO myPesqdocassociadosAlteracao
	
	uf_PESQDOCASSOCIADOS_alternaMenu()

	PESQDOCASSOCIADOS.refresh	
	
ENDFUNC

** Funcao alternar menu dependendo do estado do documento
FUNCTION uf_PESQDOCASSOCIADOS_alternaMenu()
	
	DO CASE
		** Em modo Consulta	
		CASE myPesqdocassociadosIntroducao == .f. AND myPesqdocassociadosAlteracao == .f.
			
			IF PESQDOCASSOCIADOS.PageFrame1.activepage == 1
				&&Configura Menu Barra Lateral
				PESQDOCASSOCIADOS.menu1.estado("novaLinha, eliminarLinha, navegaFT", "HIDE" , "Gravar", .f., "Sair" , .t. )
				PESQDOCASSOCIADOS.menu1.estado("aplicacoes, opcoes, actualizar, novo, editar, seltodos, emitir, emitirr, emitirnle, exportardoc", "SHOW")
			**	PESQDOCASSOCIADOS.menu_opcoes.estado("relatorioftentidades, avisolancamentoassociados, reciboentidades", "SHOW")
			**	PESQDOCASSOCIADOS.menu_opcoes.estado("notalancamentoassociados", "HIDE")
			ELSE
				PESQDOCASSOCIADOS.menu1.estado("novaLinha, eliminarLinha, novo, editar, seltodos, emitir, emitirr, emitirnle, exportardoc", "HIDE" , "Gravar", .f., "Sair" , .t. )
				PESQDOCASSOCIADOS.menu1.estado("aplicacoes, opcoes, actualizar, navegaFT", "SHOW")
				
			**	PESQDOCASSOCIADOS.menu_opcoes.estado("relatorioftentidades, avisolancamentoassociados, reciboentidades", "HIDE")
			**	PESQDOCASSOCIADOS.menu_opcoes.estado("notalancamentoassociados", "SHOW")
			ENDIF 
			&&Configura Objectos
			WITH PESQDOCASSOCIADOS.PageFrame1.lancamentoreceituario
				.nr_org_novo.readonly = .t.
				.id_cl_novo.readonly = .t.
				.ano_novo.readonly = .t.
				.mes_novo.readonly = .t.
				.ano_tratamento_novo.readonly = .t.
				.mes_tratamento_novo.readonly = .t.
				.valor_novo.readonly = .t.
				.valor_ret_novo.readonly = .t.
				.valor_reg_novo.readonly = .t.
				.num_fat_cl.readonly = .t.
				.data_fat_cl.readonly = .t.
				.obs.readonly = .t.
				.nome_org.readonly = .f.
				.nr_org.readonly = .f.
				.id_org.readonly = .f.
				.nome_cl.readonly = .f.
				.nr_cl.readonly = .f.
				.id_cl.readonly = .f.
				.ano.readonly = .f.
				.mes.readonly = .f.
				.nrdoc.readonly = .f.
				.chkdata_insercaoretificado.visible = .f.
				.label28.visible = .f.
			ENDWITH 
			
		** Em modo Edi��o - Novo 
		CASE myPesqdocassociadosIntroducao  == .t. and myPesqdocassociadosAlteracao == .f.	
			
			&&Configura Menu Barra Lateral
			PESQDOCASSOCIADOS.Menu1.estado("aplicacoes, opcoes, actualizar, novo, editar, seltodos, emitir, emitirr, emitirnle, exportardoc", "HIDE", "Sair", .f. )
			PESQDOCASSOCIADOS.Menu1.estado("novaLinha, eliminarLinha", "SHOW" , "Gravar", .t., "Cancelar", .t.)
			
			&&Configura Menu Op��es
			**PESQDOCASSOCIADOS.menu_opcoes.estado("notalancamentoassociados, relatorioftentidades, avisolancamentoassociados", "HIDE")
			
			
			&&Configura Objectos
			WITH PESQDOCASSOCIADOS.PageFrame1.lancamentoreceituario
				.nr_org_novo.readonly = .f.
				.id_cl_novo.readonly = .f.
				.ano_novo.readonly = .f.
				.mes_novo.readonly = .f.
				.ano_tratamento_novo.readonly = .f.
				.mes_tratamento_novo.readonly = .f.
				.valor_novo.readonly = .f.
				.valor_ret_novo.readonly = .f.
				.valor_reg_novo.readonly = .t.
				.num_fat_cl.readonly = .f.
				.data_fat_cl.readonly = .f.
				.obs.readonly = .f.
				.nome_org.readonly = .t.
				.nr_org.readonly = .t.
				.id_org.readonly = .t.
				.nome_cl.readonly = .t.
				.nr_cl.readonly = .t.
				.id_cl.readonly = .t.
				.ano.readonly = .t.
				.mes.readonly = .t.		
				.nrdoc.readonly = .t.
				.chkdata_insercaoretificado.visible = .t.
				.label28.visible = .t.
			ENDWITH 							
		
		** Em modo Edi��o - Alterar
		CASE myPesqdocassociadosIntroducao  == .f. and myPesqdocassociadosAlteracao == .t.	
			
				&&Configura Menu Barra Lateral
				PESQDOCASSOCIADOS.Menu1.estado("aplicacoes, opcoes, actualizar, novo, editar, seltodos, emitir, emitirr, emitirnle, exportardoc", "HIDE", "Sair", .f. )
				PESQDOCASSOCIADOS.Menu1.estado("eliminarLinha", "SHOW" , "Gravar", .t., "Cancelar", .t.)
				
				&&Configura Menu Op��es
			**	PESQDOCASSOCIADOS.menu_opcoes.estado("notalancamentoassociados, relatorioftentidades, avisolancamentoassociados", "HIDE")
			
			&&Configura Objectos
			WITH PESQDOCASSOCIADOS.PageFrame1.lancamentoreceituario
				.nr_org_novo.readonly = .f.
				.id_cl_novo.readonly = .f.
				.ano_novo.readonly = .f.
				.mes_novo.readonly = .f.
				.ano_tratamento_novo.readonly = .f.
				.mes_tratamento_novo.readonly = .f.
				.valor_novo.readonly = .f.
				.valor_ret_novo.readonly = .f.
				.valor_reg_novo.readonly = .f.
				.num_fat_cl.readonly = .f.
				.data_fat_cl.readonly = .f.
				.obs.readonly = .f.
				.nome_org.readonly = .t.
				.nr_org.readonly = .t.
				.id_org.readonly = .t.
				.nome_cl.readonly = .t.
				.nr_cl.readonly = .t.
				.id_cl.readonly = .t.
				.ano.readonly = .t.
				.mes.readonly = .t.		
				.nrdoc.readonly = .t.
				.chkdata_insercaoretificado.visible = .t.
				.label28.visible = .t.
			ENDWITH 
	ENDCASE

ENDFUNC


** Func�o adicionar nova linha 
Function uf_PESQDOCASSOCIADOS_adicionaLinha
	LOCAL lcIdCl, lcNrOrg
	lcIdCl = ""
	lcNrOrg = ""
		
	If myPesqdocassociadosIntroducao == .f. and myPesqdocassociadosAlteracao == .f.
		RETURN .f.
	ENDIF
	
	IF PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.chkdata_insercaoretificado.tag = "false"
		SELECT uCrsDocAssociados
		lcIdCl = uCrsDocAssociados.id_cl
		
		SELECT uCrsDocAssociados
		APPEND BLANK
		Replace uCrsDocAssociados.ano 				WITH IIF(MONTH(DATE())==1,YEAR(DATE())-1,YEAR(DATE()))
		Replace uCrsDocAssociados.mes 				WITH MONTH(GOMONTH(DATE(YEAR(DATE()), MONTH(DATE()),1),+0)-1)
		Replace uCrsDocAssociados.ano_tratamento	WITH IIF(MONTH(DATE())==1,YEAR(DATE())-1,YEAR(DATE()))
		Replace uCrsDocAssociados.mes_tratamento 	WITH MONTH(GOMONTH(DATE(YEAR(DATE()), MONTH(DATE()),1),+0)-1)
		Replace uCrsDocAssociados.imprimir 			WITH .t.
		Replace uCrsDocAssociados.id_cl 			WITH lcIdCl 
			
		uf_PESQDOCASSOCIADOS_calcvalorestotal()
		PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.refresh
		
		PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.gridPesq.setfocus
		PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.id_cl_novo.setfocus
		
	ELSE
		SELECT uCrsDocAssociados
		lcNrOrg = uCrsDocAssociados.nr_org
		
		SELECT uCrsDocAssociados
		APPEND BLANK
		Replace uCrsDocAssociados.ano 				WITH IIF(MONTH(DATE())==1,YEAR(DATE())-1,YEAR(DATE()))
		Replace uCrsDocAssociados.mes 				WITH MONTH(GOMONTH(DATE(YEAR(DATE()), MONTH(DATE()),1),+0)-1)
		Replace uCrsDocAssociados.ano_tratamento	WITH IIF(MONTH(DATE())==1,YEAR(DATE())-1,YEAR(DATE()))
		Replace uCrsDocAssociados.mes_tratamento 	WITH MONTH(GOMONTH(DATE(YEAR(DATE()), MONTH(DATE()),1),+0)-1)
		Replace uCrsDocAssociados.imprimir 			WITH .t.
		Replace uCrsDocAssociados.nr_org			WITH lcNrOrg
		
			
		uf_PESQDOCASSOCIADOS_calcvalorestotal()
		PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.refresh
		
		PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.gridPesq.setfocus
		PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.mes_tratamento_novo.setfocus
		
	ENDIF
	
ENDFUNC


** Fun��o eliminar linha seleccionada
FUNCTION uf_PESQDOCASSOCIADOS_eliminaLinha
	LOCAL lcID 
	
	If myPesqdocassociadosIntroducao == .f. and myPesqdocassociadosAlteracao == .f.
		RETURN .f.
	ENDIF	
	
	IF uCrsDocAssociados.emitido=.t. then
		uf_perguntalt_chama("N�O � POSS�VEL ELIMINAR UMA LINHA J� EMITIDA.","OK","",16)
		RETURN .f.
	ELSE
	
		**
		select uCrsDocAssociados
		
		IF !USED("ucrsDocAssociadosLinhasEliminadas")
			select * FROM uCrsDocAssociados WHERE 1=0  INTO CURSOR ucrsDocAssociadosLinhasEliminadas READWRITE 
		ENDIF 	

		Select ucrsDocAssociadosLinhasEliminadas 
		APPEND BLANK
		Replace ucrsDocAssociadosLinhasEliminadas.id WITH uCrsDocAssociados.ID

		select uCrsDocAssociados
		DELETE 
		
		TRY
			IF recno()>1
				TRY
					go recno()-1
				CATCH
				ENDTRY
			ENDIF
		CATCH
			SELECT uCrsDocAssociados
			GO bottom
		ENDTRY

		uf_PESQDOCASSOCIADOS_calcvalorestotal()
	ENDIF 
	PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.gridPesq.refresh

ENDFUNC 


**Funcao Calcular valor pagar Grelha
FUNCTION uf_PESQDOCASSOCIADOS_calcvalores
	**LOCAL lcValorTotal, lcNrRegistos, lcRetificadoTotal, lcPagarTotal, lcRegularizadoTotal
	**STORE 0 TO lcValorTotal, lcNrRegistos, lcRetificadoTotal, lcPagarTotal, lcRegularizadoTotal
	
	SELECT uCrsDocAssociados
	**REPLACE uCrsDocAssociados.valor_pagar WITH uCrsDocAssociados.valor - uCrsDocAssociados.valor_ret
	REPLACE uCrsDocAssociados.valor_pagar WITH uCrsDocAssociados.valor + uCrsDocAssociados.valor_ret
	
	PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.valor_pagar_novo.refresh
	PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.gridPesq.refresh
	
ENDFUNC


**Funcao Calculo Valores Total 
FUNCTION uf_PESQDOCASSOCIADOS_calcvalorestotal
	LOCAL lcNrRegistos, lcRetificadoTotalPositivo, lcRetificadoTotalNegativo, lcPagarTotal
	STORE 0 TO lcNrRegistos, lcRetificadoTotalPositivo, lcRetificadoTotalNegativo, lcPagarTotal

	SELECT uCrsDocAssociados
	GO TOP
	CALCULATE SUM(uCrsDocAssociados.valor_pagar) TO lcValorTotal
	
	PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.soma_valor.value = lcValorTotal
	PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.soma_valor.refresh
	
	SELECT uCrsDocAssociados
	GO TOP
	CALCULATE SUM(uCrsDocAssociados.valor_ret)  FOR uCrsDocAssociados.valor_ret >= 0 TO lcRetificadoTotalPositivo
	
	PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.soma_retificado_positivo.value = lcRetificadoTotalPositivo
	PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.soma_retificado_positivo.refresh
	
	SELECT uCrsDocAssociados
	GO TOP
	CALCULATE SUM(uCrsDocAssociados.valor_ret)  FOR uCrsDocAssociados.valor_ret < 0 TO lcRetificadoTotalNegativo
	
	PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.soma_retificado_negativo.value = lcRetificadoTotalNegativo
	PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.soma_retificado_negativo.refresh
	
	SELECT uCrsDocAssociados
	GO TOP
	CALCULATE SUM(uCrsDocAssociados.valor) TO lcPagarTotal 
	
	PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.soma_faturado.value = lcPagarTotal
	PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.soma_faturado.refresh
	
	SELECT uCrsDocAssociados
	GO TOP
	CALCULATE SUM(uCrsDocAssociados.valor_regularizado) TO lcRegularizadoTotal
	
	PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.soma_regularizado.value = lcRegularizadoTotal
	PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.soma_regularizado.refresh
	
	SELECT uCrsDocAssociados
	COUNT TO lcNrRegistos
	
	PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.soma_registo.value = lcNrRegistos
	PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.soma_registo.refresh	
	
**	SELECT uCrsDocAssociados
**	GO BOTTOM 
	
	PESQDOCASSOCIADOS.refresh
ENDFUNC


**Funcao atualiza Nome Farmacia Grelha
FUNCTION uf_PESQDOCASSOCIADOS_LostFocusIDFarmacia
	LPARAMETERS lcResumo
	
	IF EMPTY(lcResumo)
	
		IF EMPTY(uCrsDocAssociados.id_cl)
			SELECT uCrsDocAssociados
			REPLACE uCrsDocAssociados.nome_cl WITH ""
			RETURN .f.
		ENDIF
			
		TEXT TO lcSQL TEXTMERGE NOSHOW 	 
			Select 
				nome
			from
				B_utentes as Clientes
			where
				Clientes.id = '<<uCrsDocAssociados.id_cl>>'
		ENDTEXT 
		
		IF !uf_gerais_actGrelha("","uCrsDocAssociadosAux",lcSql)
			uf_perguntalt_chama("N�O FOI POSS�VEL VERIFICAR O NOME DO CLIENTE. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			RETURN .f.
		ENDIF
		
		IF RECCOUNT("uCrsDocAssociadosAux") > 0
			SELECT uCrsDocAssociadosAux
			SELECT uCrsDocAssociados
			REPLACE uCrsDocAssociados.nome_cl WITH uCrsDocAssociadosAux.nome
			PESQDOCASSOCIADOS.Pageframe1.refresh
		ELSE
			uf_perguntalt_chama("N�O EXISTE CLIENTE ASSOCIADO PARA O ID INSERIDO. POR FAVOR VERIFIQUE.","OK","",16)
			SELECT uCrsDocAssociados
			REPLACE uCrsDocAssociados.id_cl WITH ""
			REPLACE uCrsDocAssociados.nome_cl WITH ""
			PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.gridPesq.setfocus
			PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.id_cl_novo.setfocus
			
		ENDIF
		
		IF USED  ("uCrsDocAssociadosAUX")
			FECHA ("uCrsDocAssociadosAUX")
		ENDIF
		PESQDOCASSOCIADOS.Pageframe1.refresh

	ELSE
	
		IF EMPTY(PESQDOCASSOCIADOS.Pageframe1.Page2.id_cl.Value)
			RETURN .f.
		ENDIF 
	
		TEXT TO lcSQL TEXTMERGE NOSHOW 	 
			Select 
				nome, no
			from
				B_utentes as Clientes
			where
				Clientes.id = '<<PESQDOCASSOCIADOS.Pageframe1.Page2.id_cl.Value>>'
		ENDTEXT 
		
		IF !uf_gerais_actGrelha("","uCrsDocAssociadosAux",lcSql)
			uf_perguntalt_chama("N�O FOI POSS�VEL VERIFICAR O NOME DO CLIENTE. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			RETURN .f.
		ENDIF
		
		IF RECCOUNT("uCrsDocAssociadosAux")>0
			SELECT uCrsDocAssociadosAux
			PESQDOCASSOCIADOS.Pageframe1.Page2.nome_cl.Value = uCrsDocAssociadosAux.nome
			PESQDOCASSOCIADOS.Pageframe1.Page2.nr_cl.Value = ASTR(uCrsDocAssociadosAux.no)
			PESQDOCASSOCIADOS.Pageframe1.Page2.nome_cl.refresh
			PESQDOCASSOCIADOS.Pageframe1.Page2.nr_cl.refresh
		ELSE
			PESQDOCASSOCIADOS.Pageframe1.Page2.nome_cl.Value = ""
			PESQDOCASSOCIADOS.Pageframe1.Page2.nr_cl.Value = ""
			PESQDOCASSOCIADOS.Pageframe1.Page2.nome_cl.refresh
			PESQDOCASSOCIADOS.Pageframe1.Page2.nr_cl.refresh
		ENDIF
	ENDIF 
ENDFUNC


**Func�o verifica se existe entidade
FUNCTION uf_PESQDOCASSOCIADOS_LostFocusEntidade
	
	IF EMPTY(uCrsDocAssociados.nr_org)
		RETURN .f.
	ENDIF
	
	TEXT TO lcSQL TEXTMERGE NOSHOW 	 
		Select 
			nome
		from
			B_utentes as Entidade
		where
			Entidade.no = <<uCrsDocAssociados.nr_org>>
			and Entidade.no < 199
	ENDTEXT 
	
	IF !uf_gerais_actGrelha("","uCrsDocAssociadosAux1",lcSql)
		uf_perguntalt_chama("N�O FOI POSS�VEL VERIFICAR O NOME DA ENTIDADE. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	ENDIF

	IF RECCOUNT("uCrsDocAssociadosAux1") > 0
		SELECT uCrsDocAssociadosAux1
		SELECT uCrsDocAssociados
		REPLACE uCrsDocAssociados.nome_org WITH uCrsDocAssociadosAux1.nome
		PESQDOCASSOCIADOS.Pageframe1.refresh
	ELSE
		uf_perguntalt_chama("N�O EXISTE ENTIDADE CRIADA PARA O N�MERO INSERIDO. POR FAVOR VERIFIQUE.","OK","",16)
		SELECT uCrsDocAssociados
		REPLACE uCrsDocAssociados.nr_org WITH 0
	ENDIF
	
	IF USED  ("uCrsDocAssociadosAUX1")
		FECHA ("uCrsDocAssociadosAUX1")
	ENDIF
	
ENDFUNC


**Func�o verifica se existe Cliente
FUNCTION uf_PESQDOCASSOCIADOS_LostFocusClienteNo
	
	IF EMPTY(PESQDOCASSOCIADOS.Pageframe1.Page2.nr_cl.Value)
		RETURN .f.
	ENDIF 
	
	TEXT TO lcSQL TEXTMERGE NOSHOW 	 
		Select 
			nome, no, id
		from
			B_utentes as Clientes
		where
			Clientes.no = <<PESQDOCASSOCIADOS.Pageframe1.Page2.nr_cl.Value>>
	ENDTEXT 
	
	IF !uf_gerais_actGrelha("","uCrsDocAssociadosAux",lcSql)
		uf_perguntalt_chama("N�O FOI POSS�VEL VERIFICAR OS DADOS DO CLIENTE. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	ENDIF
	
	IF RECCOUNT("uCrsDocAssociadosAux")>0
		SELECT uCrsDocAssociadosAux
		PESQDOCASSOCIADOS.Pageframe1.Page2.nome_cl.Value = uCrsDocAssociadosAux.nome
		PESQDOCASSOCIADOS.Pageframe1.Page2.id_cl.Value = uCrsDocAssociadosAux.id
		PESQDOCASSOCIADOS.Pageframe1.Page2.nome_cl.refresh
		PESQDOCASSOCIADOS.Pageframe1.Page2.id_cl.refresh
	ELSE
		PESQDOCASSOCIADOS.Pageframe1.Page2.nome_cl.Value = ""
		PESQDOCASSOCIADOS.Pageframe1.Page2.id_cl.Value = ""
		PESQDOCASSOCIADOS.Pageframe1.Page2.nome_cl.refresh
		PESQDOCASSOCIADOS.Pageframe1.Page2.id_cl.refresh
	ENDIF

ENDFUNC


**Fun��o Gravar Documentos
FUNCTION uf_PESQDOCASSOCIADOS_gravar

	IF !uf_PESQDOCASSOCIADOS_camposObrigatorios()
		RETURN .f.
	ENDIF 

	&& validar data. alertar se mes for superior a 3 meses atr�s.
	IF myPesqdocassociadosIntroducao == .t.
		SELECT uCrsDocAssociados
		GO TOP
	
		uf_PESQDOCASSOCIADOS_inseredados()

		myPesqdocassociadosIntroducao = .f.
		myPesqdocassociadosAlteracao = .f.

		uf_PESQDOCASSOCIADOS_alternaMenu()

		PESQDOCASSOCIADOS.refresh	
		
	ELSE
 		SELECT uCrsDocAssociados
		GO TOP

		uf_PESQDOCASSOCIADOS_eliminaLinhas()
		uf_PESQDOCASSOCIADOS_actualiza()

		myPesqdocassociadosIntroducao = .f.
		myPesqdocassociadosAlteracao = .f.

		uf_PESQDOCASSOCIADOS_alternaMenu()
		
		PESQDOCASSOCIADOS.refresh			

	ENDIF
	
	uf_PESQDOCASSOCIADOS_calcvalorestotal()
	**uf_PESQDOCASSOCIADOS_actualizar()

ENDFUNC

** Funcao eliminar Linhas
FUNCTION uf_PESQDOCASSOCIADOS_eliminaLinhas
	IF USED("ucrsDocAssociadosLinhasEliminadas")
		SELECT ucrsDocAssociadosLinhasEliminadas
		GO TOP 
		SCAN FOR !EMPTY(ucrsDocAssociadosLinhasEliminadas.id)
			lcSQL = ""
			TEXT TO lcSQL NOSHOW TEXTMERGE
				DELETE FROM cl_fatc_org WHERE id = '<<ALLTRIM(ucrsDocAssociadosLinhasEliminadas.id)>>'
			ENDTEXT

			IF !uf_gerais_actgrelha("", "", lcSql)
				uf_perguntalt_chama("OCORREU UMA ANOMALIA A GUARDAR O DOCUMENTO. POR FAVOR CONTATE O SUPORTE", "OK", 16)
				RETURN .f.
			ENDIF
		ENDSCAN
	
		fecha("ucrsDocAssociadosLinhasEliminadas")	
	ENDIF 
ENDFUNC 


**Func�o Insere dados BD
FUNCTION uf_PESQDOCASSOCIADOS_inseredados
	LPARAMETERS lnId
	LOCAL lcSQL, lcData
	
	
	SET CENTURY ON

	lcData = datetime()

	SELECT * FROM uCrsDocAssociados INTO CURSOR uCrsDocAssociadosAuxGravar READWRITE 

	SELECT uCrsDocAssociadosAuxGravar
	GO TOP 
	SCAN FOR !EMPTY(Alltrim(uCrsDocAssociadosAuxGravar.id_cl))
	
		lnId = uf_gerais_stampid()

		lcSQL = ""
		TEXT TO lcSQL NOSHOW textmerge
			INSERT INTO cl_fatc_org
			(	
				id
				,id_cl
				,nr_cl_org
				,ano
				,mes
				,ano_tratamento
				,mes_tratamento
				,nrDoc
				,valor
				,valor_ret
				,valor_pagar
				,valor_regularizado
				,recebido
				,imprimir
				,obs
				,data
				,data_alt
				,fno_cl
				,data_fno_cl
				,ousrinis
				,usrinis
			) 
			VALUES
			(
				'<<lnId>>'
				,'<<Alltrim(uCrsDocAssociadosAuxGravar.id_cl)>>'
				,'<<uCrsDocAssociadosAuxGravar.nr_org>>'
				,'<<uCrsDocAssociadosAuxGravar.ano>>'
				,'<<uCrsDocAssociadosAuxGravar.mes>>'
				,'<<uCrsDocAssociadosAuxGravar.ano_tratamento>>'
				,'<<uCrsDocAssociadosAuxGravar.mes_tratamento>>'
				,(select ISNULL(max(nrdoc),0) + 1  from cl_fatc_org)
				,'<<uCrsDocAssociadosAuxGravar.valor>>'
				,'<<uCrsDocAssociadosAuxGravar.valor_ret>>'
				,'<<uCrsDocAssociadosAuxGravar.valor_pagar>>'
				,'<<uCrsDocAssociadosAuxGravar.valor_regularizado>>'
				,<<IIF(uCrsDocAssociados.recebido,1,0)>>
				,<<IIF(uCrsDocAssociados.imprimir,1,0)>>
				,'<<uCrsDocAssociadosAuxGravar.obs>>'
				,Convert(datetime,'<<lcData>>',120)
				,Convert(datetime,'<<lcData>>',120)
				,'<<ALLTRIM(IIF(EMPTY(uCrsDocAssociadosAuxGravar.fno_cl), ' ', uCrsDocAssociadosAuxGravar.fno_cl))>>'
				,'<<uf_gerais_getdate(uCrsDocAssociadosAuxGravar.data_fno_cl,"SQL")>>'
				,'<<ALLTRIM(m_chinis)>>'
				,'<<ALLTRIM(m_chinis)>>'	
			)
			
		ENDTEXT
	
		IF !uf_gerais_actGrelha("","",lcSql)
			uf_perguntalt_chama("OCORREU UMA ANOMALIA A GUARDAR O DOCUMENTO. POR FAVOR CONTATE O SUPORTE.","OK","",16)
			RETURN .f.
		ENDIF

	ENDSCAN

    TEXT TO lcSQL TEXTMERGE NOSHOW
        select
            cfo.id ,cfo.ano ,cfo.mes ,cfo.ano_tratamento ,cfo.mes_tratamento
            ,cfo.nrDoc ,cfo.valor ,cfo.valor_ret ,cfo.valor_pagar ,cfo.valor_regularizado
            ,cfo.recebido ,cfo.imprimir ,cfo.obs ,cfo.data ,cfo.data_alt
            ,cfo.id_cl, cl.no as nr_cl, cl.estab as dep_cl, cl.nome as nome_cl
            ,ncont_cl = cl.ncont, morada_cl = cl.morada, codpost_cl = cl.codpost
            ,Entidades.no as nr_org, Entidades.id as id_org, Entidades.nome as nome_org
            ,Entidades.morada as morada_org, Entidades.codpost as codpost_org, Entidades.ncont as ncont_org, Entidades.nome2 as abrev_org, Entidades.local as local_org
            ,referencia = upper(left(datename(month, DateAdd(month , mes-1 , 0)),3)) +  + '/' + convert(varchar,ano)
            ,cfo.fno_cl, cfo.data_fno_cl
            ,cfo.ousrinis
            ,cfo.usrinis
            ,nrDocImp = 0
            ,cfo.emitido
            ,cfo.emitidoe
            ,cfo.emitidoale
            ,CAST(0 AS bit) as emitir
            ,cfo.docNr
        from
            cl_fatc_org cfo (nolock)
            left join B_utentes as cl (nolock) on cl.id = cfo.id_cl
            left join B_utentes as Entidades (nolock) on Entidades.no = cfo.nr_cl_org
        where 
            data = Convert(datetime,'<<lcData>>',120)		
    ENDTEXT

    IF !uf_gerais_actGrelha("PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.GridPesq","uCrsDocAssociados",lcSql)
        uf_perguntalt_chama("OCORREU UMA ANOMALIA A GUARDAR O DOCUMENTO. POR FAVOR CONTATE O SUPORTE.","OK","",16)
        RETURN .f.
    ENDIF

	IF USED("uCrsDocAssociadosAuxGravar")	
		FECHA("uCrsDocAssociadosAuxGravar")
	ENDIF

ENDFUNC


**Funcao atualizar dados
FUNCTION uf_PESQDOCASSOCIADOS_actualiza
	LOCAL lnId

	SET CENTURY ON
	
	lcData = datetime()

	SELECT uCrsDocAssociados
	GO TOP
	SCAN FOR !EMPTY(Alltrim(uCrsDocAssociados.id_cl))
		
		lcSQL = ''		
		TEXT TO lcSQL NOSHOW textmerge
		
			UPDATE 
				 cl_fatc_org
			SET 
				id					 = '<<Alltrim(uCrsDocAssociados.id)>>'
				,id_cl				 = '<<Alltrim(uCrsDocAssociados.id_cl)>>'		
				,nr_cl_org			 = '<<uCrsDocAssociados.nr_org>>'
				,ano				 = '<<uCrsDocAssociados.ano>>'
				,mes				 = '<<uCrsDocAssociados.mes>>'
				,ano_tratamento		 = '<<uCrsDocAssociados.ano_tratamento>>'
				,mes_tratamento		 = '<<uCrsDocAssociados.mes_tratamento>>'
				,nrDoc				 = <<uCrsDocAssociados.nrDoc>>
				,valor				 = '<<uCrsDocAssociados.valor>>'
				,valor_ret			 = '<<uCrsDocAssociados.valor_ret>>'
				,valor_pagar		 = '<<uCrsDocAssociados.valor_pagar>>'
				,valor_regularizado  = '<<uCrsDocAssociados.valor_regularizado>>' 
				,recebido 			 = <<IIF(uCrsDocAssociados.recebido,1,0)>>
				,imprimir			 = <<IIF(uCrsDocAssociados.imprimir,1,0)>>
				,obs				 = '<<uCrsDocAssociados.obs>>'
				,data				 = '<<uCrsDocAssociados.data>>'
				,data_alt			 = '<<lcData>>'
				,fno_cl				 = '<<ALLTRIM(IIF(EMPTY(uCrsDocAssociados.fno_cl), ' ', uCrsDocAssociados.fno_cl))>>'
				,data_fno_cl		 = '<<uf_gerais_getdate(uCrsDocAssociados.data_fno_cl,"SQL")>>'
				,ousrinis			 = '<<ALLTRIM(uCrsDocAssociados.ousrinis)>>'
				,usrinis			 = '<<ALLTRIM(m_chinis)>>'
			WHERE
				cl_fatc_org.id		 = '<<uCrsDocAssociados.id>>'
				
		ENDTEXT

		IF !uf_gerais_actGrelha("","",lcSql)
			uf_perguntalt_chama("N�O FOI POSS�VEL ACTUALIZAR O DOCUMENTO. POR FAVOR CONTACTE O SUPORTE. ","OK","",16)
			Return .f.
		ENDIF

	ENDSCAN
	
**	SELECT uCrsDocAssociados
**	GO TOP 
	
	Return .t.
		
ENDFUNC 


**Fun��o Sair Painel
FUNCTION uf_PESQDOCASSOCIADOS_sair

	IF myPesqdocassociadosIntroducao == .f. AND myPesqdocassociadosAlteracao == .f.

		uf_PESQDOCASSOCIADOS_exit()

	ELSE
		If uf_perguntalt_chama("Quer mesmo cancelar o documento? Ir� perder as �ltimas altera��es feitas.","Sim","N�o")
			myPesqdocassociadosIntroducao	= .f.
			myPesqdocassociadosAlteracao	= .f.

			uf_PESQDOCASSOCIADOS_alternaMenu()
			uf_PESQDOCASSOCIADOS_actualizar()
		Endif
	ENDIF
ENDFUNC


**
FUNCTION uf_PESQDOCASSOCIADOS_exit
	PESQDOCASSOCIADOS.hide
	PESQDOCASSOCIADOS.release
ENDFUNC 


** Funcao escolher Entidade 
FUNCTION uf_PESQDOCASSOCIADOS_escolheEntidade
	LPARAMETERS lcFiltro
	
	IF EMPTY(lcFiltro)	
		If myPesqdocassociadosIntroducao Or myPesqdocassociadosAlteracao
			uf_pesqUtentes_Chama("DOCASSOCIADOSENTIDADE")
		Endif	
	ELSE
		uf_pesqUtentes_Chama("DOCASSOCIADOSENTIDADEFILTRO")
	ENDIF 
ENDFUNC


** Funcao escolher Cliente  
FUNCTION uf_PESQDOCASSOCIADOS_escolheCliente
	LPARAMETERS lcFiltro
	
	IF EMPTY(lcFiltro)
		If myPesqdocassociadosIntroducao Or myPesqdocassociadosAlteracao
			uf_pesqUtentes_Chama("DOCASSOCIADOSCLIENTE")
		Endif	
	ELSE
		uf_pesqUtentes_Chama("DOCASSOCIADOSCLIENTEFILTRO")
	ENDIF 
ENDFUNC


**
FUNCTION uf_PESQDOCASSOCIADOS_escolheClienteResumo
	uf_pesqUtentes_Chama("RESUMOASSOCIADOSCLIENTE")
ENDFUNC


**Func�o configura menu lateral 
FUNCTION uf_PESQDOCASSOCIADOS_ResumoAssociados
	WITH PESQDOCASSOCIADOS.pageframe1
		IF .activepage = 1
			.activepage = 2
			 PESQDOCASSOCIADOS.menu_aplicacoes.resassociados.config("Gest�o Entidades","","uf_PESQDOCASSOCIADOS_ResumoAssociados")
			 PESQDOCASSOCIADOS.caption = "Gest�o Associados"
			 PESQDOCASSOCIADOS.Pageframe1.Page2.nome_cl.setfocus
			 PESQDOCASSOCIADOS.Menu1.estado("novo, editar", "HIDE")
		ELSE
			.activepage = 1
			 PESQDOCASSOCIADOS.menu_aplicacoes.resassociados.config("Gest�o Associados","","uf_PESQDOCASSOCIADOS_ResumoAssociados")
			 PESQDOCASSOCIADOS.caption = "Gest�o Entidades"
			 PESQDOCASSOCIADOS.Menu1.estado("novo, editar", "SHOW")
		ENDIF
	ENDWITH
	
	uf_PESQDOCASSOCIADOS_alternaMenu()
ENDFUNC 


**
FUNCTION uf_PESQDOCASSOCIADOS_MOVBANCARIOS 
	WITH PESQDOCASSOCIADOS.pageframe1
		DO CASE 
			CASE .activepage = 2
				uf_MOVBANCARIOS_chama('PESQDOCASSOCIADOS_CLIENTE')		
			CASE .activepage = 1
				uf_MOVBANCARIOS_chama('PESQDOCASSOCIADOS_ENTIDADE')		
		ENDCASE
	ENDWITH
ENDFUNC 


**
FUNCTION uf_PESQDOCASSOCIADOS_camposObrigatorios
	local dayToCompare, lcValueFound

	SELECT uCrsDocAssociados
	GO Top
	LOCATE FOR EMPTY(uCrsDocAssociados.ano)
	IF FOUND()
		uf_perguntalt_chama("O campo Ano � obrigat�rio.","OK","",64)
		RETURN .f.
	ENDIF 
	
	SELECT uCrsDocAssociados
	GO Top
	LOCATE FOR EMPTY(uCrsDocAssociados.mes)
	IF FOUND()
		uf_perguntalt_chama("O campo M�s � obrigat�rio.","OK","",64)
		RETURN .f.
	ENDIF 
	
	SELECT uCrsDocAssociados
	GO Top
	LOCATE FOR EMPTY(uCrsDocAssociados.ano_tratamento)
	IF FOUND()
		uf_perguntalt_chama("O campo Ano Tratamento � obrigat�rio.","OK","",64)
		RETURN .f.
	ENDIF 

	&& validar para o caso em que o mes � fevereiro
	dayToCompare = day(date())	
	IF dayToCompare > 28 and month(date()) = 2
		dayToCompre = 28
	ENDIF
	
	&& validar casos em que valor faturado � negativo.
	SELECT uCrsDocAssociados
	GO Top
	LOCATE FOR uCrsDocAssociados.valor < 0
	IF FOUND()
		uf_perguntalt_chama("O campo Valor Faturado n�o pode ser negativo.","OK","",64)
		RETURN .f.
	ENDIF 

	SELECT uCrsDocAssociados
	GO Top
	LOCATE FOR (date() - date(uCrsDocAssociados.ano_tratamento, uCrsDocAssociados.mes_tratamento, day(date())))/31 > 3
	IF FOUND()
		uf_perguntalt_chama("� aconselhavel n�o usar data de tratamento anterior a 3 meses.","OK","",64)
	ENDIF 
	
	SELECT uCrsDocAssociados
	GO Top
	LOCATE FOR (month(date()) < uCrsDocAssociados.mes_tratamento and year(date()) <= uCrsDocAssociados.ano_tratamento)
	IF FOUND()
		uf_perguntalt_chama("A data de tratamento n�o pode ser superior � data de hoje.","OK","",64)
		return .f.
	ENDIF 

	SELECT uCrsDocAssociados
	GO Top
	LOCATE FOR EMPTY(uCrsDocAssociados.mes_tratamento)
	IF FOUND()
		uf_perguntalt_chama("O campo M�s Tratamento � obrigat�rio.","OK","",64)
		RETURN .f.
	ENDIF 
	
	SELECT uCrsDocAssociados
	GO Top
	LOCATE FOR EMPTY(uCrsDocAssociados.id_cl)
	IF FOUND()
		uf_perguntalt_chama("O campo ID Farm�cia � obrigat�rio.","OK","",64)
		RETURN .f.
	ENDIF 
	
	SELECT uCrsDocAssociados
	GO Top
	LOCATE FOR EMPTY(uCrsDocAssociados.nr_org)
	IF FOUND()
		uf_perguntalt_chama("O campo N�m. Entidade � obrigat�rio.","OK","",64)
		RETURN .f.
	ENDIF 
	
	RETURN .t.
ENDFUNC


** funcao q permite navegar para a linha seleccionada.
FUNCTION uf_PESQDOCASSOCIADOS_navegaFT

	IF RECCOUNT("uCrsAssociadosFtClienteDetalhe") = 0
		uf_perguntalt_chama("Por favor seleccione uma fatura para consulta. Obrigado.","OK","",64)
		RETURN .f.
	ENDIF 
	
	SELECT uCrsAssociadosFtClienteDetalhe
	uf_facturacao_Chama(ALLTRIM(uCrsAssociadosFtClienteDetalhe.ftstamp))

ENDFUNC 

FUNCTION uf_PESQDOCASSOCIADOS_seltodos

	SELECT uCrsDocAssociados 
	GO TOP 
	SCAN 
	**	IF uCrsDocAssociados.emitido=.f.
			replace uCrsDocAssociados.emitir WITH .t.
	**	ENDIF 
	ENDSCAN 
	
	SELECT uCrsDocAssociados 
	GO TOP 
	PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.gridPesq.refresh
	
ENDFUNC 

** Emitir nota de lan�amento do associado
FUNCTION uf_PESQDOCASSOCIADOS_emitir_NL

	LOCAL lcFtStamp, lcFno, lcClNcont, lcClno, lcClestab, lcTotal, lcQtt, lcFiStamp, lcLordem, lcTotRecebido
	STORE '' TO lcFtStamp, lcClNcont, lcFiStamp
	STORE 0 TO lcFno, lcClno, lcClestab, lcTotal, lcQtt, lcLordem, lcTotRecebido
	
	** Criar cursores para emitir documentos de fatura��o
	IF !USED("FT")
		IF !uf_gerais_actGrelha("",[FT],[set fmtonly on exec up_touch_ft '' set fmtonly off])
			uf_perguntalt_chama("Ocorreu uma anomalia ao criar o cursor [FT]. Por favor reinicie o Software.","OK","",16)
			RETURN .F.
		ENDIF
	ELSE
		DELETE from ft
	ENDIF
	
	IF !USED("FT2")
		IF !uf_gerais_actGrelha("",[FT2],[set fmtonly on select * from FT2 (nolock) set fmtonly off])
			uf_perguntalt_chama("Ocorreu uma anomalia ao criar o cursor [FT2]. Por favor reinicie o Software.","OK","",16)
			RETURN .F.
		ENDIF	
	ELSE
		DELETE FROM FT2
	ENDIF
	
	uf_perguntalt_chama("POR FAVOR INSIRA A DATA PARA EMISS�O DOS DOCUMENTOS!","OK","",64)
	public lcFData
	uf_getdate_chama(.f., "lcFData", 0, .f., .f., .t.)
	
	IF uf_perguntalt_chama("CONFIRMA O LAN�AMENTO DOS DOCUMENTOS SELECIONADOS?","Sim","N�o", 64) 
	
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			select 
				convert(bit,0) as manipulado
				,convert(varchar(3),'') as tipoR
				,fi.id_Dispensa_Eletronica_D as id_validacaoDem
				,convert(varchar(20),'') as receita
				,convert(varchar(40),'') as token
				,convert(bit,0) as dem
				,0 as qtDem
				,convert(varchar(250),design) as design
				,convert(bit,0) as usalote
				,convert(bit,0) as alertalote
				,convert(bit,0) as compSNS
				,convert(varchar(8),'') as CNPEM
				,0.000 as PvpTop4,* 
			from 
				FI (nolock) 
				left join fi_comp (nolock) on fi.fistamp = fi_comp.id_fi
			WHERE 
				0=1
		ENDTEXT 
		uf_gerais_actGrelha("","FI",lcSQL)
		
		IF USED("ucrs_ft_aux")
			fecha("ucrs_ft_aux ")
		ENDIF 
		
		LOCAL lcCont2
		lcCont2 = 1
		select distinct nr_cl, dep_cl, nome_cl, ncont_cl, morada_cl, codpost_cl, local_cl from uCrsDocAssociados where emitir=.t. AND emitido=.f. order by nome_cl into cursor ucrs_ft_aux readwrite
		SELECT ucrs_ft_aux 
		IF RECCOUNT("ucrs_ft_aux")=0 then
			uf_perguntalt_chama("N�o escolheu nenhum registo para emitir a Nota de Lan�amento ou os registos escolhidos j� foram emitidos.", "", "OK", 16)
		ELSE
			regua(0,RECCOUNT("ucrs_ft_aux"),"A emitir Documentos...")
			SELECT ucrs_ft_aux 
			GO TOP 
			SCAN
				regua(1,lcCont2,"A emitir Documento: " + astr(lcCont2))
				lcClno = ucrs_ft_aux.nr_cl
				lcClestab = ucrs_ft_aux.dep_cl
				TEXT TO lcSQL TEXTMERGE NOSHOW
					select isnull(max(fno),0)+1 as fno from ft where nmdoc='Nt. Lan�am. Assoc.' and ftano= YEAR('<<uf_gerais_getDate(lcFData,"SQL")>>')
				ENDTEXT
				If !uf_gerais_actGrelha("", "ucrsDadosFt", lcSql)
					uf_perguntalt_chama("N�O FOI POSS�VEL O N� DO PR�XIMO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE","OK","",16)
					RETURN .f.
				ENDIF
				lcFno=ucrsDadosFt.fno
				TEXT TO lcSQL TEXTMERGE NOSHOW
					select * from b_utentes where no=<<lcClno>> and estab=<<lcClestab>>
				ENDTEXT
				If !uf_gerais_actGrelha("", "ucrsDadosUt", lcSql)
					uf_perguntalt_chama("N�O FOI POSS�VEL O N� DO PR�XIMO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE","OK","",16)
					RETURN .f.
				ENDIF	
				lcClNcont=ALLTRIM(ucrsDadosUt.ncont) 		
				lcFtStamp=uf_gerais_stamp()
				
				SELECT ft
				DELETE ALL 
				APPEND BLANK
				replace	ft.ftstamp 	WITH 	ALLTRIM(lcFtStamp)
				replace	ft.pais 	WITH 	1
				replace	ft.nmdoc 	WITH 	'Nt. Lan�am. Assoc.'
				replace	ft.fno 		WITH 	lcFno
				replace	ft.no 		WITH 	lcClno 
				replace	ft.estab	WITH 	lcClestab 
				replace	ft.nome 	WITH 	ucrs_ft_aux.nome_cl
				replace	ft.morada 	WITH 	ucrs_ft_aux.morada_cl
				replace	ft.local 	WITH 	ucrs_ft_aux.local_cl
				replace	ft.codpost 	WITH 	ucrs_ft_aux.codpost_cl
				replace	ft.ncont 	WITH 	lcClNcont
				replace	ft.vendedor WITH 	ch_vendedor
				replace	ft.vendnm 	WITH 	ALLTRIM(ch_vendnm)
				replace	ft.fdata 	WITH 	DATE()
				replace	ft.pdata 	WITH 	DATE()
				replace	ft.ftano 	WITH 	YEAR(DATE())
				replace	Ft.Ivatx1 	With	uf_gerais_getTabelaIVA(1,"TAXA") 
				replace	Ft.Ivatx2 	With	uf_gerais_getTabelaIVA(2,"TAXA") 
				replace	Ft.Ivatx3 	With	uf_gerais_getTabelaIVA(3,"TAXA") 
				replace	Ft.Ivatx4 	With	uf_gerais_getTabelaIVA(4,"TAXA") 
				replace	Ft.Ivatx5 	With	uf_gerais_getTabelaIVA(5,"TAXA") 
				replace	Ft.Ivatx6 	With	uf_gerais_getTabelaIVA(6,"TAXA") 
				replace	Ft.Ivatx7 	With	uf_gerais_getTabelaIVA(7,"TAXA") 
				replace	Ft.Ivatx8 	With	uf_gerais_getTabelaIVA(8,"TAXA") 
				replace	Ft.Ivatx9 	With	uf_gerais_getTabelaIVA(9,"TAXA") 
				replace	ft.ndoc		with	81
				replace	ft.moeda	with	'EURO'
				replace	ft.cdata	with 	DATE()
				replace	ft.tipo		with	'Farm�cia'
				replace	ft.tipodoc	with	3
				replace	ft.memissao	with 	'EURO'
				replace	ft.site		with	ALLTRIM(mySite)
				replace	ft.pnome	with	ALLTRIM(myTerm)
				replace	ft.pno 		WITH 	myTermNo
				replace	ft.ousrinis	WITH 	m_chinis
				replace	ft.ousrdata	WITH 	DATE()
				replace	ft.ousrhora	WITH 	time()
				replace	ft.usrinis	WITH 	m_chinis
				replace	ft.usrdata	WITH 	DATE()
				replace	ft.usrhora	WITH 	time()
				replace	ft.u_nratend	WITH 	'AUTOMATICO'
					
				select ft2
				DELETE ALL 
				APPEND BLANK 
				replace	ft2.ft2stamp 	WITH 	ALLTRIM(lcFtStamp)
				replace	ft2.vdollocal	WITH 	'Caixa'
				replace	ft2.vdlocal		WITH 	'C'
				replace	ft2.formapag	WITH 	'1'
				replace	ft2.ousrinis	WITH 	m_chinis
				replace	ft2.ousrdata	WITH 	DATE()
				replace	ft2.ousrhora	WITH 	time()
				replace	ft2.usrinis		WITH 	m_chinis
				replace	ft2.usrdata		WITH 	DATE()
				replace	ft2.usrhora		WITH 	time()
				
				SELECT fi
				DELETE ALL 
				lcTotal = 0 
				lcTotRecebido = 0
				lcQtt = 0
				lcLordem = 100000
				SELECT uCrsDocAssociados 
				GO top
				SCAN
					IF uCrsDocAssociados.emitir = .t. AND uCrsDocAssociados.nr_cl=lcClno AND uCrsDocAssociados.dep_cl=lcClestab
						lcFiStamp = uf_gerais_stamp()
						SELECT fi 
						APPEND blank
							replace fi.fistamp		WITH	alltrim(lcFiStamp)
							replace fi.nmdoc		WITH 	'Nt. Lan�am. Assoc.'
							replace fi.fno			WITH 	lcFno
							replace fi.ref			WITH 	'999999'
							replace fi.design		WITH 	ALLTRIM(uCrsDocAssociados.nome_org)
							replace fi.qtt			with	1
							replace fi.etiliquido	WITH 	(uCrsDocAssociados.valor+uCrsDocAssociados.valor_ret)*(-1)
							replace fi.tiliquido	WITH 	((uCrsDocAssociados.valor+uCrsDocAssociados.valor_ret)*(-1))*200.482
							replace fi.iva			with	0
							replace fi.ivaincl		with	.t.
							replace fi.tabiva		with	4
							replace fi.ndoc			with	81
							replace fi.armazem		with	1
							replace fi.ftanoft		with	YEAR(DATE())
							replace fi.litem2		with	ALLTRIM(STR(uCrsDocAssociados.mes_tratamento))
							replace fi.litem		with	ALLTRIM(STR(uCrsDocAssociados.ano_tratamento))
							replace fi.rdata		with	DATE()
							replace fi.cpoc			with 	1
							replace fi.lrecno		with 	alltrim(lcFiStamp)
							replace fi.lordem		with	lcLordem 
							replace fi.ftstamp		with	ALLTRIM(lcFtStamp)
							replace fi.stipo		with	1
							replace fi.tipodoc		with	3
							replace fi.stns 		with	.t.
							replace fi.epv			WITH 	uCrsDocAssociados.valor
							replace fi.pv			WITH 	uCrsDocAssociados.valor*200.482
							**replace fi.pbruto		with	uCrsDocAssociados.valor_ret
							replace fi.epcp			with	uCrsDocAssociados.valor_ret
							replace fi.eslvu		WITH 	uCrsDocAssociados.valor+uCrsDocAssociados.valor_ret
							replace fi.slvu			WITH 	(uCrsDocAssociados.valor+uCrsDocAssociados.valor_ret)*200.482
							replace fi.esltt		WITH 	(uCrsDocAssociados.valor+uCrsDocAssociados.valor_ret)*(-1)
							replace fi.sltt			WITH 	((uCrsDocAssociados.valor+uCrsDocAssociados.valor_ret)*(-1))*200.482
							replace fi.fivendedor 	WITH 	ch_vendedor
							replace fi.fivendnm 	WITH 	ALLTRIM(ch_vendnm)
							replace fi.epvori		WITH 	uCrsDocAssociados.valor+uCrsDocAssociados.valor_ret
							replace fi.pvori		WITH 	(uCrsDocAssociados.valor+uCrsDocAssociados.valor_ret)*200.482
							replace fi.ousrinis		WITH 	m_chinis
							replace fi.ousrdata		WITH 	DATE()
							replace fi.ousrhora		WITH 	time()
							replace fi.usrinis		WITH 	m_chinis
							replace fi.usrdata		WITH 	DATE()
							replace fi.usrhora		WITH 	time()
							replace fi.u_epvp		WITH 	uCrsDocAssociados.valor+uCrsDocAssociados.valor_ret
							replace fi.u_txcomp		WITH 	100
							
						lcTotal=lcTotal + ((uCrsDocAssociados.valor+uCrsDocAssociados.valor_ret)*(-1))	
						lcQtt = lcQtt + 1
						lcLordem = lcLordem + 101000
						IF uCrsDocAssociados.recebido = .t. 
							lcTotRecebido = lcTotRecebido + (uCrsDocAssociados.valor+uCrsDocAssociados.valor_ret)
						ENDIF 
					ENDIF
					SELECT uCrsDocAssociados  
				ENDSCAN 
				
				SELECT ft
				replace ft.totqtt		with	lcQtt
				replace ft.qtt1 		with	lcQtt
				replace	ft.eivain4		with	lcTotal
				replace	ft.ivain4		with	lcTotal*200.482
				replace	ft.ettiliq		with	lcTotal
				replace	ft.ttiliq		with	lcTotal*200.482
				replace	ft.etotal		with	lcTotal
				replace	ft.total		with	lcTotal*200.824
				replace ft.edebreg		WITH 	0
					
				lcExecuteSQL = ''
				
				lcSQL = ''
				TEXT TO lcSQL NOSHOW TEXTMERGE
					Insert Into ft2 (
						contacto, u_viatura, morada, codpost, local, telefone, email, horaentrega
						,ft2stamp, u_vddom, obsdoc, u_receita, u_entpresc, u_nopresc, u_codigo, u_codigo2
						,u_nbenef, u_nbenef2, u_abrev, u_abrev2, c2no, c2estab, subproc, u_vdsusprg
						,u_design, u_design2, c2nome, c2pais, evdinheiro, epaga1, epaga2, etroco
						,u_acertovs, c2codpost, eacerto, vdollocal, vdlocal, modop1, modop2, ousrdata
						,ousrinis, ousrhora, usrdata, usrinis, usrhora, motiseimp, formapag, epaga3
						,epaga4, epaga5, epaga6, codAcesso, codDirOpcao, token, u_docorig
						, token_efectivacao_compl, token_anulacao_compl
					)
					Values (
						'<<ALLTRIM(ft2.contacto)>>', '<<ALLTRIM(ft2.u_viatura)>>', '<<ALLTRIM(ft2.morada)>>', '<<ALLTRIM(ft2.codpost)>>', '<<ALLTRIM(ft2.local)>>', '<<ALLTRIM(ft2.telefone)>>', '<<ALLTRIM(ft2.email)>>', '<<ALLTRIM(ft2.horaentrega)>>'
						,'<<ALLTRIM(ft2.Ft2Stamp)>>', <<IIF(ft2.u_vddom,1,0)>>, '<<ALLTRIM(ft2.obsdoc)>>', '<<LEFT(ft2.u_receita,20)>>', '<<ALLTRIM(ft2.u_entpresc)>>', '<<ALLTRIM(ft2.u_nopresc)>>', '<<ALLTRIM(ft2.u_codigo)>>', '<<ALLTRIM(ft2.u_codigo2)>>'
						,'<<ALLTRIM(ft2.u_nbenef)>>', '<<ALLTRIM(ft2.u_nbenef2)>>', '<<ALLTRIM(ft2.u_abrev)>>', '<<ALLTRIM(ft2.u_abrev2)>>', <<ft2.c2no>>, <<ft2.c2estab>>, '<<ALLTRIM(ft2.subproc)>>', <<IIF(ft2.u_vdsusprg,1,0)>>
						,'<<ALLTRIM(ft2.u_design)>>', '<<ALLTRIM(ft2.u_design2)>>', '<<ALLTRIM(ft2.c2nome)>>', <<ft2.c2pais>>, <<ft2.evdinheiro)>>, <<ft2.epaga1>>, <<ft2.epaga2>>, <<ft2.etroco>>
						,<<ft2.u_acertovs>>, '<<ALLTRIM(ft2.c2codpost)>>', <<ft2.eacerto>>, 'Caixa', 'C', '<<ALLTRIM(ft2.modop1)>>', '<<ALLTRIM(ft2.modop2)>>', CONVERT(varchar,getdate(),102)
						,'<<ALLTRIM(m_chinis)>>', '<<ALLTRIM(time())>>', CONVERT(varchar,getdate(),102), '<<ALLTRIM(m_chinis)>>', '<<ALLTRIM(time())>>', '<<ALLTRIM(ft2.motiseimp)>>', '1', <<ft2.epaga3>>
						,<<ft2.epaga4>>, <<ft2.epaga5>>, <<ft2.epaga6>>, '<<ALLTRIM(ft2.codAcesso)>>', '<<ALLTRIM(ft2.CodDirOpcao)>>','<<ALLTRIM(ft2.token)>>', '<<ALLTRIM(ft2.u_docorig)>>'
						,'',''
					)
				ENDTEXT
				lcExecuteSQL  = lcExecuteSQL + CHR(13) + lcSQL

				
				lcSQL = ''
				TEXT TO lcSql NOSHOW TEXTMERGE
					INSERT INTO ft (
						ftstamp, pais, nmdoc, fno, [no], nome, morada, [local]
						,codpost, ncont, bino, bidata, bilocal, telefone, zona, vendedor
						,vendnm, fdata, ftano, pdata, carga, descar, saida, ivatx1
						,ivatx2, ivatx3, fin, ndoc, moeda, fref, ccusto, ncusto
						,facturada, fnoft, nmdocft,	estab, cdata, ivatx4, segmento, totqtt
						,qtt1, qtt2, tipo, cobrado, cobranca, tipodoc, chora, ivatx5
						,ivatx6, ivatx7, ivatx8, ivatx9, cambiofixo, memissao, cobrador, rota
						,multi, cheque, clbanco, clcheque, chtotal,	echtotal, custo, eivain1
						,eivain2, eivain3, eivav1, eivav2, eivav3, ettiliq, edescc, ettiva
						,etotal, eivain4, eivav4, efinv, ecusto, eivain5, eivav5, edebreg
						,eivain6, eivav6, eivain7, eivav7, eivain8, eivav8, eivain9, eivav9
						,total, totalmoeda, ivain1, ivain2, ivain3, ivain4, ivain5, ivain6
						,ivain7, ivain8, ivain9, ivav1, ivav2, ivav3, ivav4, ivav5
						,ivav6, ivav7, ivav8, ivav9, ttiliq, ttiva, descc, debreg
						,debregm, intid, nome2, tpstamp, tpdesc, erdtotal, rdtotal
						,rdtotalm, cambio, [site], pnome, pno, cxstamp, cxusername, ssstamp, ssusername
						,anulado, virs, evirs, valorm2, u_lote, u_tlote, u_tlote2, u_ltstamp
						,u_slote, u_slote2, u_nslote, u_nslote2, u_ltstamp2, u_lote2, ousrinis, ousrdata
						,ousrhora, usrinis, usrdata, usrhora, u_nratend, u_hclstamp, datatransporte,localcarga
						,id_tesouraria_conta
					)
					Values (
						'<<ALLTRIM(ft.ftstamp)>>', <<ft.pais>>, '<<ALLTRIM(ft.NmDoc)>>', <<ft.fno>>, <<ft.no>>, '<<Alltrim(ft.nome)>>', '<<Alltrim(ft.morada)>>', '<<Alltrim(ft.local)>>'
						,'<<Alltrim(ft.codpost)>>', '<<ft.NCont>>', '<<Alltrim(ft.bino)>>', '<<uf_gerais_getDate(Ft.bidata,"SQL")>>', '<<Alltrim(ft.bilocal)>>', '<<Alltrim(ft.telefone)>>', '<<Alltrim(ft.zona)>>', <<ft.vendedor>>
						,'<<Alltrim(ft.vendnm)>>', '<<uf_gerais_getDate(lcFData,"SQL")>>', <<ft.ftano>>, '<<uf_gerais_getDate(lcFData,"SQL")>>', '<<ALLTRIM(ft.carga)>>', '<<ALLTRIM(ft.descar)>>', Left('<<time()>>',5), <<ft.ivatx1>>
						,<<ft.ivatx2>>, <<ft.ivatx3>>, <<ft.fin>>, <<ft.Ndoc>>, '<<ALLTRIM(ft.moeda)>>',	'<<ALLTRIM(ft.fref)>>', '<<ALLTRIM(ft.ccusto)>>', '<<ALLTRIM(ft.ncusto)>>'
						,<<IIF(ft.facturada,1,0)>>, <<ft.fnoft>>, '<<ALLTRIM(ft.nmdocft)>>', <<ft.estab>>, '<<uf_gerais_getDate(Ft.cdata,"SQL")>>', <<ft.ivatx4>>, '<<ALLTRIM(ft.segmento)>>', <<ft.TotQtt>>
						,<<ft.Qtt1>>, <<ft.qtt2>>, '<<ALLTRIM(ft.tipo)>>', <<IIF(ft.cobrado,1,0)>>, '<<ALLTRIM(ft.cobranca)>>', <<ft.TipoDoc>>, Left('<<Chrtran(time(),":","")>>',4), <<ft.ivatx5>>
						,<<ft.ivatx6>>, <<ft.ivatx7>>, <<ft.ivatx8>>, <<ft.ivatx9>>, <<IIF(ft.cambiofixo,1,0)>>, '<<ALLTRIM(ft.memissao)>>', '<<ALLTRIM(ft.cobrador)>>', '<<ALLTRIM(ft.rota)>>'
						,<<IIF(ft.multi,1,0)>>, <<IIF(ft.cheque,1,0)>>, '<<ALLTRIM(ft.clbanco)>>', '<<ALLTRIM(ft.clcheque)>>', <<ft.chtotal>>,<<ft.echtotal>>,<<ft.custo>>, <<ft.EivaIn1>>
						,<<ft.EivaIn2>>, <<ft.EivaIn3>>, <<ft.EivaV1>>, <<ft.EivaV2>>, <<ft.EivaV3>>, <<ft.EttIliq>>, <<ft.edescc>>, <<ft.EttIva>>
						,<<ft.Etotal>>, <<ft.EivaIn4>>, <<ft.EivaV4>>, <<ft.efinv>>, <<ft.Ecusto>>, <<ft.EivaIn5>>, <<ft.EivaV5>>, <<ft.edebreg>>
						,<<ft.eivain6>>, <<ft.eivav6>>, <<ft.eivain7>>, <<ft.eivav7>>, <<ft.eivain8>>, <<ft.eivav8>>, <<ft.eivain9>>, <<ft.eivav9>>
						,<<ft.total>>, <<ft.totalmoeda>>, <<ft.ivaIn1>>, <<ft.ivaIn2>>, <<ft.ivaIn3>>, <<ft.ivaIn4>>, <<ft.ivaIn5>>, <<ft.ivain6>>
						,<<ft.ivain7>>, <<ft.ivain8>>, <<ft.ivain9>>, <<ft.ivaV1>>, <<ft.ivaV2>>, <<ft.ivaV3>>, <<ft.ivaV4>>, <<ft.ivaV5>>
						,<<ft.ivav6>>, <<ft.ivav7>>, <<ft.ivav8>>, <<ft.ivav9>>, <<ft.ttIliq>>,	<<ft.ttIva>>, <<ft.descc>>, <<ft.debreg>>
						,<<ft.debregm>>, '<<ALLTRIM(ft.intid)>>', '<<ALLTRIM(ft.nome2)>>', '<<ALLTRIM(ft.tpstamp)>>', '<<ALLTRIM(ft.tpdesc)>>', <<ft.erdtotal>>, <<ft.rdtotal>>, <<ft.rdtotalm>>
						,<<ft.cambio>>, '<<ALLTRIM(ft.Site)>>', '<<ALLTRIM(ft.pnome)>>', <<ft.pNo>>, '<<ALLTRIM(ft.cxstamp)>>', '<<ALLTRIM(ft.cxusername)>>', '<<ALLTRIM(ft.ssstamp)>>', '<<ALLTRIM(ft.ssusername)>>'
						,<<IIF(ft.anulado,1,0)>>, <<ft.virs>>, <<ft.evirs>>, <<ft.valorm2>>, '<<ft.u_lote>>', '<<ft.u_tlote>>', '<<ft.u_tlote2>>', '<<ALLTRIM(ft.u_ltstamp)>>'
						,<<ft.u_slote>>, <<ft.u_slote2>>, <<ft.u_nslote>>, <<ft.u_nslote2>>, '<<ALLTRIM(ft.u_ltstamp2)>>', <<ft.u_lote2>>, '<<ALLTRIM(m_chinis)>>', CONVERT(varchar,getdate(),102)
						, '<<ALLTRIM(time())>>', '<<ALLTRIM(m_chinis)>>', CONVERT(varchar,getdate(),102), '<<ALLTRIM(time())>>', '<<ALLTRIM(ft.u_nratend)>>','<<ALLTRIM(ft.u_hclstamp)>>', '<<ALLTRIM(uf_gerais_getDate(ft.datatransporte,"SQL"))>>','<<ALLTRIM(ft.localcarga)>>'
						,<<ft.id_tesouraria_conta>>
					)
				ENDTEXT
				lcExecuteSQL = lcExecuteSQL + CHR(13) + lcSQL
							
				SELECT fi
				GO TOP 
				SCAN
					lcSQL = ''
					TEXT TO lcSql NOSHOW TEXTMERGE 
							insert into fi (
								fistamp, nmdoc, fno, ref, design, qtt, tiliquido, etiliquido
								,unidade, unidad2, iva, ivaincl, codigo, tabiva, ndoc, armazem
								,fnoft, ndocft, ftanoft, ftregstamp, lobs2, litem2, litem, lobs3
								,rdata,	cpoc, composto, lrecno, lordem, fmarcada, partes, altura
								,largura, oref, lote, usr1, usr2, usr3,	usr4, usr5
								,ftstamp, cor, tam, stipo, fifref, tipodoc, familia, bistamp
								,stns, ficcusto, fincusto, ofistamp, pv, pvmoeda, epv, tmoeda
								,eaquisicao, custo, ecusto, slvu, eslvu, sltt,esltt,slvumoeda
								,slttmoeda, nccod, epromo, fivendedor, fivendnm, desconto, desc2, desc3
								,desc4, desc5, desc6, u_descval, iectin, eiectin, vbase, evbase
								,tliquido, num1, pcp, epcp, pvori,epvori, zona, morada
								,[local], codpost, telefone, email, tkhposlstamp, u_generico, u_cnp, u_psicont
								,u_bencont, u_psico, u_benzo, u_ettent1, u_ettent2, u_epref, pic, pvpmaxre
								,opcao, u_stock, u_epvp, u_ip, u_comp, u_diploma, ousrinis, ousrdata
								,ousrhora, usrinis, usrdata, usrhora, marcada, u_genalt, u_txcomp, amostra
								,u_refvale, EVALDESC, VALDESC, u_codemb, refentidade, campanhas, id_Dispensa_Eletronica_D
								,pvp4_fee
							)
							Values (
								'<<ALLTRIM(fi.FiStamp)>>', '<<fi.nmDoc>>', <<fi.fno>>, '<<ALLTRIM(fi.ref)>>', '<<ALLTRIM(fi.design)>>', <<fi.qtt>>, <<fi.tiliquido>>, <<fi.etiliquido>>
								,'<<ALLTRIM(fi.unidade)>>', '<<ALLTRIM(fi.unidad2)>>', <<fi.iva>>, <<IIF(fi.ivaincl,1,0)>>,	'<<ALLTRIM(fi.codigo)>>', <<fi.tabiva>>, <<fi.Ndoc>>, <<fi.armazem>>
								,<<fi.fnoft>>, <<fi.ndocft>>, <<fi.ftanoft>>, '<<ALLTRIM(fi.ftregstamp)>>', '<<ALLTRIM(fi.lobs2)>>', '<<ALLTRIM(fi.litem2)>>', '<<ALLTRIM(fi.litem)>>', '<<ALLTRIM(fi.lobs3)>>'
								,'<<uf_gerais_getDate(lcFData,"SQL")>>', <<fi.cpoc>>, <<IIF(fi.composto,1,0)>>, '<<ALLTRIM(fi.lrecno)>>', <<fi.lordem>>, <<IIF(fi.fmarcada,1,0)>>, <<fi.partes>>, <<fi.altura>>
								,<<fi.largura>>, '<<ALLTRIM(fi.oref)>>', '<<ALLTRIM(fi.lote)>>', '<<ALLTRIM(fi.usr1)>>', '<<ALLTRIM(fi.usr2)>>', '<<ALLTRIM(fi.usr3)>>', '<<ALLTRIM(fi.usr4)>>', '<<ALLTRIM(fi.usr5)>>'
								,'<<ALLTRIM(fi.FtStamp)>>','<<ALLTRIM(fi.cor)>>', '<<ALLTRIM(fi.tam)>>', 1, '<<ALLTRIM(fi.fifref)>>', <<fi.TipoDoc>>, '<<ALLTRIM(fi.familia)>>', '<<ALLTRIM(fi.bistamp)>>'
								,<<IIF(fi.stns,1,0)>>, '<<ALLTRIM(fi.ficcusto)>>', '<<ALLTRIM(fi.fincusto)>>', '<<ALLTRIM(fi.ofistamp)>>', <<fi.pv>>, <<fi.pvmoeda>>, <<fi.epv>>, <<fi.tmoeda>>
								,<<fi.eaquisicao>>, <<fi.custo>>, <<fi.ecusto>>, <<fi.slvu>>, <<fi.eslvu>>,	<<fi.sltt>>, <<fi.esltt>>, <<fi.slvumoeda>>
								,<<fi.slttmoeda>>, '<<ALLTRIM(fi.nccod)>>', <<IIF(fi.epromo,1,0)>>, <<ft.vendedor>>, '<<ALLTRIM(ft.vendnm)>>', <<fi.desconto>>, <<fi.desc2>>, <<fi.desc3>>
								,<<fi.desc4>>, <<fi.desc5>>, <<fi.desc6>>, <<fi.u_descval>>, <<fi.iectin>>, <<fi.eiectin>>,	<<fi.vbase>>, <<fi.evbase>>
								,<<fi.tliquido>>, <<fi.num1>>, <<fi.pcp>>, <<fi.epcp>>, <<fi.pv>>, <<fi.epv>>, '<<ALLTRIM(fi.zona)>>', '<<ALLTRIM(fi.morada)>>'
								,'<<ALLTRIM(fi.local)>>', '<<ALLTRIM(fi.codpost)>>', '<<ALLTRIM(fi.telefone)>>', '<<ALLTRIM(fi.email)>>', '<<ALLTRIM(fi.tkhposlstamp)>>', <<IIF(fi.u_generico,1,0)>>, '<<ALLTRIM(fi.u_cnp)>>', <<fi.u_psicont>>
								, <<fi.u_bencont>>, <<IIF(fi.u_psico,1,0)>>, <<IIF(fi.u_benzo,1,0)>>, <<fi.u_ettent1>>, <<fi.u_ettent2>>, <<fi.u_epref>>, <<fi.pic>>, <<fi.pvpmaxre>>
								,'<<ALLTRIM(fi.opcao)>>', <<fi.u_stock>>, <<fi.u_epvp>>, <<IIF(fi.u_ip,1,0)>>, <<IIF(fi.u_comp,1,0)>>, '<<ALLTRIM(fi.u_diploma)>>', '<<ALLTRIM(m_chinis)>>', CONVERT(varchar,getdate(),102)
								,'<<ALLTRIM(time())>>', '<<ALLTRIM(m_chinis)>>', CONVERT(varchar,getdate(),102), '<<ALLTRIM(time())>>', 0, <<IIF(fi.u_genalt,1,0)>>, <<fi.u_txcomp>>, <<IIF(fi.amostra, 1, 0)>>
								,'<<ALLTRIM(fi.u_refvale)>>', <<IIF(fi.qtt == 0,0,(fi.u_descval / fi.qtt))>>, <<IIF(fi.qtt == 0,0,(fi.u_descval / fi.qtt) * 200.482)>>, '<<ALLTRIM(fi.u_codemb)>>', '<<ALLTRIM(fi.refentidade)>>', '<<ALLTRIM(fi.campanhas)>>', '<<ALLTRIM(fi.id_validacaoDem)>>'
								,<<fi.pvp4_fee>>
							)
						ENDTEXT
						
						lcExecuteSQL  = lcExecuteSQL + CHR(13) + lcSQL
				
					SELECT fi 
				ENDSCAN
				
				SELECT uCrsDocAssociados 
				GO top
				SCAN
					IF uCrsDocAssociados.emitir = .t. AND uCrsDocAssociados.nr_cl=lcClno AND uCrsDocAssociados.dep_cl=lcClestab
						lcSQL = ''
						TEXT TO lcSql NOSHOW TEXTMERGE 
							UPDATE cl_fatc_org SET emitido=1 WHERE id='<<ALLTRIM(uCrsDocAssociados.id)>>'
						ENDTEXT
						lcExecuteSQL  = lcExecuteSQL + CHR(13) + lcSQL
					ENDIF 
				ENDSCAN 
				
	*!*				IF lcTotRecebido > 0 then
	*!*					lcSQL = ''
	*!*					TEXT TO lcSql NOSHOW TEXTMERGE 
	*!*						UPDATE cc SET ecredf=<<lcTotRecebido>>, credf=<<lcTotRecebido*200.482>> WHERE ftstamp='<<ALLTRIM(lcFtStamp)>>'
	*!*					ENDTEXT
	*!*					lcExecuteSQL  = lcExecuteSQL + CHR(13) + lcSQL
	*!*				ENDIF 

				
											
				IF !uf_gerais_actGrelha("","",lcExecuteSQL)
					uf_perguntalt_chama("OCORREU UMA ANOMALIA A GRAVAR O DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				ELSE
					&& Tudo Ok
					**lcValidaInsercaoDocumento = .t.
				ENDIF
				SELECT ucrs_ft_aux 
				lcCont2 = lcCont2 + 1	
			ENDSCAN 
			uf_perguntalt_chama("PROCESSO CONCLU�DO COM SUCESSO.","OK","",64)
			regua(2)
		ENDIF 	
		uf_PESQDOCASSOCIADOS_actualizar()
	ELSE
		uf_perguntalt_chama("PROCESSAMENTO CANCELADO.","OK","",64)
	ENDIF
		
ENDFUNC 


** Emitir Resumo entidades
FUNCTION uf_PESQDOCASSOCIADOS_emitir_res

	LOCAL lcFtStamp, lcFno, lcClNcont, lcClno, lcClestab, lcTotal, lcQtt, lcFiStamp, lcLordem, lcTotRecebido
	STORE '' TO lcFtStamp, lcClNcont, lcFiStamp
	STORE 0 TO lcFno, lcClno, lcClestab, lcTotal, lcQtt, lcLordem, lcTotRecebido
	
	** Criar cursores para emitir documentos de fatura��o
	IF !USED("FT")
		IF !uf_gerais_actGrelha("",[FT],[set fmtonly on exec up_touch_ft '' set fmtonly off])
			uf_perguntalt_chama("Ocorreu uma anomalia ao criar o cursor [FT]. Por favor reinicie o Software.","OK","",16)
			RETURN .F.
		ENDIF
	ELSE
		DELETE from ft
	ENDIF
	
	IF !USED("FT2")
		IF !uf_gerais_actGrelha("",[FT2],[set fmtonly on select * from FT2 (nolock) set fmtonly off])
			uf_perguntalt_chama("Ocorreu uma anomalia ao criar o cursor [FT2]. Por favor reinicie o Software.","OK","",16)
			RETURN .F.
		ENDIF	
	ELSE
		DELETE FROM FT2
	ENDIF
	
	uf_perguntalt_chama("POR FAVOR INSIRA A DATA PARA EMISS�O DOS DOCUMENTOS!","OK","",64)
	public lcFData
	uf_getdate_chama(.f., "lcFData", 0, .f., .f., .t.)
	
	IF uf_perguntalt_chama("CONFIRMA O LAN�AMENTO DOS DOCUMENTOS SELECIONADOS?","Sim","N�o", 64) 
	
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			select 
				convert(bit,0) as manipulado
				,convert(varchar(3),'') as tipoR
				,fi.id_Dispensa_Eletronica_D as id_validacaoDem
				,convert(varchar(20),'') as receita
				,convert(varchar(40),'') as token
				,convert(bit,0) as dem
				,0 as qtDem
				,convert(varchar(250),design) as design
				,convert(bit,0) as usalote
				,convert(bit,0) as alertalote
				,convert(bit,0) as compSNS
				,convert(varchar(8),'') as CNPEM
				,0.000 as PvpTop4,* 
			from 
				FI (nolock) 
				left join fi_comp (nolock) on fi.fistamp = fi_comp.id_fi
			WHERE 
				0=1
		ENDTEXT 
		uf_gerais_actGrelha("","FI",lcSQL)
		
		IF USED("ucrs_ft_aux")
			fecha("ucrs_ft_aux ")
		ENDIF 
		
		LOCAL lcCont2
		lcCont2 = 1
		select distinct nr_org, id_org, nome_org, ncont_org, morada_org, codpost_org, local_org from uCrsDocAssociados where emitir=.t. AND emitidoe=.f. AND (valor<>0 OR valor_pagar <> 0 OR valor_ret <>0)order by nome_org into cursor ucrs_ft_aux readwrite
		SELECT ucrs_ft_aux 
		IF RECCOUNT("ucrs_ft_aux")=0 then
			uf_perguntalt_chama("N�o escolheu nenhum registo para emitir o Resumo de Entidades ou os registos escolhidos j� foram emitidos.", "", "OK", 16)
		ELSE
			regua(0,RECCOUNT("ucrs_ft_aux"),"A emitir Documentos...")
			SELECT ucrs_ft_aux 
			GO TOP 
			SCAN
				regua(1,lcCont2,"A emitir Documento: " + astr(lcCont2))
				lcClno = ucrs_ft_aux.nr_org
				lcClestab = 0
				TEXT TO lcSQL TEXTMERGE NOSHOW
					select isnull(max(fno),0)+1 as fno from ft where nmdoc='Resumo Entid.' and ftano= YEAR('<<uf_gerais_getDate(lcFData,"SQL")>>')
				ENDTEXT
				If !uf_gerais_actGrelha("", "ucrsDadosFt", lcSql)
					uf_perguntalt_chama("N�O FOI POSS�VEL O N� DO PR�XIMO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE","OK","",16)
					RETURN .f.
				ENDIF
				lcFno=ucrsDadosFt.fno
				TEXT TO lcSQL TEXTMERGE NOSHOW
					select * from b_utentes where no=<<lcClno>> and estab=<<lcClestab>>
				ENDTEXT
				If !uf_gerais_actGrelha("", "ucrsDadosUt", lcSql)
					uf_perguntalt_chama("N�O FOI POSS�VEL O N� DO PR�XIMO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE","OK","",16)
					RETURN .f.
				ENDIF	
				lcClNcont=ALLTRIM(ucrsDadosUt.ncont) 		
				lcFtStamp=uf_gerais_stamp()
				
				SELECT ft
				DELETE ALL 
				APPEND BLANK
				replace	ft.ftstamp 	WITH 	ALLTRIM(lcFtStamp)
				replace	ft.pais 	WITH 	1
				replace	ft.nmdoc 	WITH 	'Resumo Entid.'
				replace	ft.fno 		WITH 	lcFno
				replace	ft.no 		WITH 	lcClno 
				replace	ft.estab	WITH 	lcClestab 
				replace	ft.nome 	WITH 	ucrs_ft_aux.nome_org
				replace	ft.morada 	WITH 	ucrs_ft_aux.morada_org
				replace	ft.local 	WITH 	ucrs_ft_aux.local_org
				replace	ft.codpost 	WITH 	ucrs_ft_aux.codpost_org
				replace	ft.ncont 	WITH 	lcClNcont
				replace	ft.vendedor WITH 	ch_vendedor
				replace	ft.vendnm 	WITH 	ALLTRIM(ch_vendnm)
				replace	ft.fdata 	WITH 	DATE()
				replace	ft.pdata 	WITH 	DATE()
				replace	ft.ftano 	WITH 	YEAR(DATE())
				replace	Ft.Ivatx1 	With	uf_gerais_getTabelaIVA(1,"TAXA") 
				replace	Ft.Ivatx2 	With	uf_gerais_getTabelaIVA(2,"TAXA") 
				replace	Ft.Ivatx3 	With	uf_gerais_getTabelaIVA(3,"TAXA") 
				replace	Ft.Ivatx4 	With	uf_gerais_getTabelaIVA(4,"TAXA") 
				replace	Ft.Ivatx5 	With	uf_gerais_getTabelaIVA(5,"TAXA") 
				replace	Ft.Ivatx6 	With	uf_gerais_getTabelaIVA(6,"TAXA") 
				replace	Ft.Ivatx7 	With	uf_gerais_getTabelaIVA(7,"TAXA") 
				replace	Ft.Ivatx8 	With	uf_gerais_getTabelaIVA(8,"TAXA") 
				replace	Ft.Ivatx9 	With	uf_gerais_getTabelaIVA(9,"TAXA") 
				replace	ft.ndoc		with	82
				replace	ft.moeda	with	'EURO'
				replace	ft.cdata	with 	DATE()
				replace	ft.tipo		with	'Farm�cia'
				replace	ft.tipodoc	with	3
				replace	ft.memissao	with 	'EURO'
				replace	ft.site		with	ALLTRIM(mySite)
				replace	ft.pnome	with	ALLTRIM(myTerm)
				replace	ft.pno 		WITH 	myTermNo
				replace	ft.ousrinis	WITH 	m_chinis
				replace	ft.ousrdata	WITH 	DATE()
				replace	ft.ousrhora	WITH 	time()
				replace	ft.usrinis	WITH 	m_chinis
				replace	ft.usrdata	WITH 	DATE()
				replace	ft.usrhora	WITH 	time()
				replace	ft.u_nratend	WITH 	'AUTOMATICO'
					
				select ft2
				DELETE ALL 
				APPEND BLANK 
				replace	ft2.ft2stamp 	WITH 	ALLTRIM(lcFtStamp)
				replace	ft2.vdollocal	WITH 	'Caixa'
				replace	ft2.vdlocal		WITH 	'C'
				replace	ft2.formapag	WITH 	'1'
				replace	ft2.ousrinis	WITH 	m_chinis
				replace	ft2.ousrdata	WITH 	DATE()
				replace	ft2.ousrhora	WITH 	time()
				replace	ft2.usrinis		WITH 	m_chinis
				replace	ft2.usrdata		WITH 	DATE()
				replace	ft2.usrhora		WITH 	time()
				
				SELECT fi
				DELETE ALL 
				lcTotal = 0 
				lcTotRecebido = 0
				lcQtt = 0
				lcLordem = 100000
				SELECT uCrsDocAssociados 
				GO top
				SCAN
					IF uCrsDocAssociados.emitir = .t. AND uCrsDocAssociados.nr_org=lcClno AND (uCrsDocAssociados.valor<>0 OR uCrsDocAssociados.valor_pagar <> 0 OR uCrsDocAssociados.valor_ret <>0) &&AND uCrsDocAssociados.id_org=lcClestab
						lcFiStamp = uf_gerais_stamp()
						SELECT fi 
						APPEND blank
							replace fi.fistamp		WITH	alltrim(lcFiStamp)
							replace fi.nmdoc		WITH 	'Resumo Entid.'
							replace fi.fno			WITH 	lcFno
							replace fi.ref			WITH 	'999999'
							replace fi.design		WITH 	ALLTRIM(uCrsDocAssociados.nome_cl)
							replace fi.qtt			with	1
							replace fi.etiliquido	WITH 	(uCrsDocAssociados.valor+uCrsDocAssociados.valor_ret)*(-1)
							replace fi.tiliquido	WITH 	((uCrsDocAssociados.valor+uCrsDocAssociados.valor_ret)*(-1))*200.482
							replace fi.iva			with	0
							replace fi.ivaincl		with	.t.
							replace fi.tabiva		with	4
							replace fi.ndoc			with	82
							replace fi.armazem		with	1
							replace fi.ftanoft		with	YEAR(DATE())
							replace fi.litem2		with	ALLTRIM(STR(uCrsDocAssociados.mes_tratamento))
							replace fi.litem		with	ALLTRIM(STR(uCrsDocAssociados.ano_tratamento))							
							replace fi.rdata		with	DATE()
							replace fi.cpoc			with 	1
							replace fi.lrecno		with 	alltrim(lcFiStamp)
							replace fi.lordem		with	lcLordem 
							replace fi.ftstamp		with	ALLTRIM(lcFtStamp)
							replace fi.stipo		with	1
							replace fi.tipodoc		with	3
							replace fi.stns 		with	.t.
							replace fi.epv			WITH 	uCrsDocAssociados.valor
							replace fi.pv			WITH 	uCrsDocAssociados.valor*200.482
							**replace fi.pbruto		with	uCrsDocAssociados.valor_ret
							replace fi.epcp			with	uCrsDocAssociados.valor_ret
							replace fi.eslvu		WITH 	uCrsDocAssociados.valor+uCrsDocAssociados.valor_ret
							replace fi.slvu			WITH 	(uCrsDocAssociados.valor+uCrsDocAssociados.valor_ret)*200.482
							replace fi.esltt		WITH 	(uCrsDocAssociados.valor+uCrsDocAssociados.valor_ret)*(-1)
							replace fi.sltt			WITH 	((uCrsDocAssociados.valor+uCrsDocAssociados.valor_ret)*(-1))*200.482
							replace fi.fivendedor 	WITH 	ch_vendedor
							replace fi.fivendnm 	WITH 	ALLTRIM(ch_vendnm)
							replace fi.epvori		WITH 	uCrsDocAssociados.valor+uCrsDocAssociados.valor_ret
							replace fi.pvori		WITH 	(uCrsDocAssociados.valor+uCrsDocAssociados.valor_ret)*200.482
							replace fi.ousrinis		WITH 	m_chinis
							replace fi.ousrdata		WITH 	DATE()
							replace fi.ousrhora		WITH 	time()
							replace fi.usrinis		WITH 	m_chinis
							replace fi.usrdata		WITH 	DATE()
							replace fi.usrhora		WITH 	time()
							replace fi.u_epvp		WITH 	uCrsDocAssociados.valor+uCrsDocAssociados.valor_ret
							replace fi.u_txcomp		WITH 	100
							replace fi.lobs2 		WITH 	uCrsDocAssociados.id_cl
							replace fi.lobs3 		WITH 	ALLTRIM(STR(uCrsDocAssociados.nrdoc))
							
						lcTotal=lcTotal + ((uCrsDocAssociados.valor+uCrsDocAssociados.valor_ret)*(-1))	
						lcQtt = lcQtt + 1
						lcLordem = lcLordem + 101000
						IF uCrsDocAssociados.recebido = .t. 
							lcTotRecebido = lcTotRecebido + (uCrsDocAssociados.valor+uCrsDocAssociados.valor_ret)
						ENDIF 
					ENDIF
					SELECT uCrsDocAssociados  
				ENDSCAN 
				
				SELECT ft
				replace ft.totqtt		with	lcQtt
				replace ft.qtt1 		with	lcQtt
				replace	ft.eivain4		with	lcTotal
				replace	ft.ivain4		with	lcTotal*200.482
				replace	ft.ettiliq		with	lcTotal
				replace	ft.ttiliq		with	lcTotal*200.482
				replace	ft.etotal		with	lcTotal
				replace	ft.total		with	lcTotal*200.824
				replace ft.edebreg		WITH 	0
					
				lcExecuteSQL = ''
				
				lcSQL = ''
				TEXT TO lcSQL NOSHOW TEXTMERGE
					Insert Into ft2 (
						contacto, u_viatura, morada, codpost, local, telefone, email, horaentrega
						,ft2stamp, u_vddom, obsdoc, u_receita, u_entpresc, u_nopresc, u_codigo, u_codigo2
						,u_nbenef, u_nbenef2, u_abrev, u_abrev2, c2no, c2estab, subproc, u_vdsusprg
						,u_design, u_design2, c2nome, c2pais, evdinheiro, epaga1, epaga2, etroco
						,u_acertovs, c2codpost, eacerto, vdollocal, vdlocal, modop1, modop2, ousrdata
						,ousrinis, ousrhora, usrdata, usrinis, usrhora, motiseimp, formapag, epaga3
						,epaga4, epaga5, epaga6, codAcesso, codDirOpcao, token, u_docorig
						, token_efectivacao_compl, token_anulacao_compl
					)
					Values (
						'<<ALLTRIM(ft2.contacto)>>', '<<ALLTRIM(ft2.u_viatura)>>', '<<ALLTRIM(ft2.morada)>>', '<<ALLTRIM(ft2.codpost)>>', '<<ALLTRIM(ft2.local)>>', '<<ALLTRIM(ft2.telefone)>>', '<<ALLTRIM(ft2.email)>>', '<<ALLTRIM(ft2.horaentrega)>>'
						,'<<ALLTRIM(ft2.Ft2Stamp)>>', <<IIF(ft2.u_vddom,1,0)>>, '<<ALLTRIM(ft2.obsdoc)>>', '<<LEFT(ft2.u_receita,20)>>', '<<ALLTRIM(ft2.u_entpresc)>>', '<<ALLTRIM(ft2.u_nopresc)>>', '<<ALLTRIM(ft2.u_codigo)>>', '<<ALLTRIM(ft2.u_codigo2)>>'
						,'<<ALLTRIM(ft2.u_nbenef)>>', '<<ALLTRIM(ft2.u_nbenef2)>>', '<<ALLTRIM(ft2.u_abrev)>>', '<<ALLTRIM(ft2.u_abrev2)>>', <<ft2.c2no>>, <<ft2.c2estab>>, '<<ALLTRIM(ft2.subproc)>>', <<IIF(ft2.u_vdsusprg,1,0)>>
						,'<<ALLTRIM(ft2.u_design)>>', '<<ALLTRIM(ft2.u_design2)>>', '<<ALLTRIM(ft2.c2nome)>>', <<ft2.c2pais>>, <<ft2.evdinheiro)>>, <<ft2.epaga1>>, <<ft2.epaga2>>, <<ft2.etroco>>
						,<<ft2.u_acertovs>>, '<<ALLTRIM(ft2.c2codpost)>>', <<ft2.eacerto>>, 'Caixa', 'C', '<<ALLTRIM(ft2.modop1)>>', '<<ALLTRIM(ft2.modop2)>>', CONVERT(varchar,getdate(),102)
						,'<<ALLTRIM(m_chinis)>>', '<<ALLTRIM(time())>>', CONVERT(varchar,getdate(),102), '<<ALLTRIM(m_chinis)>>', '<<ALLTRIM(time())>>', '<<ALLTRIM(ft2.motiseimp)>>', '1', <<ft2.epaga3>>
						,<<ft2.epaga4>>, <<ft2.epaga5>>, <<ft2.epaga6>>, '<<ALLTRIM(ft2.codAcesso)>>', '<<ALLTRIM(ft2.CodDirOpcao)>>','<<ALLTRIM(ft2.token)>>', '<<ALLTRIM(ft2.u_docorig)>>'
						,'',''
					)
				ENDTEXT
				lcExecuteSQL  = lcExecuteSQL + CHR(13) + lcSQL

				
				lcSQL = ''
				TEXT TO lcSql NOSHOW TEXTMERGE
					INSERT INTO ft (
						ftstamp, pais, nmdoc, fno, [no], nome, morada, [local]
						,codpost, ncont, bino, bidata, bilocal, telefone, zona, vendedor
						,vendnm, fdata, ftano, pdata, carga, descar, saida, ivatx1
						,ivatx2, ivatx3, fin, ndoc, moeda, fref, ccusto, ncusto
						,facturada, fnoft, nmdocft,	estab, cdata, ivatx4, segmento, totqtt
						,qtt1, qtt2, tipo, cobrado, cobranca, tipodoc, chora, ivatx5
						,ivatx6, ivatx7, ivatx8, ivatx9, cambiofixo, memissao, cobrador, rota
						,multi, cheque, clbanco, clcheque, chtotal,	echtotal, custo, eivain1
						,eivain2, eivain3, eivav1, eivav2, eivav3, ettiliq, edescc, ettiva
						,etotal, eivain4, eivav4, efinv, ecusto, eivain5, eivav5, edebreg
						,eivain6, eivav6, eivain7, eivav7, eivain8, eivav8, eivain9, eivav9
						,total, totalmoeda, ivain1, ivain2, ivain3, ivain4, ivain5, ivain6
						,ivain7, ivain8, ivain9, ivav1, ivav2, ivav3, ivav4, ivav5
						,ivav6, ivav7, ivav8, ivav9, ttiliq, ttiva, descc, debreg
						,debregm, intid, nome2, tpstamp, tpdesc, erdtotal, rdtotal
						,rdtotalm, cambio, [site], pnome, pno, cxstamp, cxusername, ssstamp, ssusername
						,anulado, virs, evirs, valorm2, u_lote, u_tlote, u_tlote2, u_ltstamp
						,u_slote, u_slote2, u_nslote, u_nslote2, u_ltstamp2, u_lote2, ousrinis, ousrdata
						,ousrhora, usrinis, usrdata, usrhora, u_nratend, u_hclstamp, datatransporte,localcarga
						,id_tesouraria_conta
					)
					Values (
						'<<ALLTRIM(ft.ftstamp)>>', <<ft.pais>>, '<<ALLTRIM(ft.NmDoc)>>', <<ft.fno>>, <<ft.no>>, '<<Alltrim(ft.nome)>>', '<<Alltrim(ft.morada)>>', '<<Alltrim(ft.local)>>'
						,'<<Alltrim(ft.codpost)>>', '<<ft.NCont>>', '<<Alltrim(ft.bino)>>', '<<uf_gerais_getDate(Ft.bidata,"SQL")>>', '<<Alltrim(ft.bilocal)>>', '<<Alltrim(ft.telefone)>>', '<<Alltrim(ft.zona)>>', <<ft.vendedor>>
						,'<<Alltrim(ft.vendnm)>>', '<<uf_gerais_getDate(lcFData,"SQL")>>', <<ft.ftano>>, '<<uf_gerais_getDate(lcFData,"SQL")>>', '<<ALLTRIM(ft.carga)>>', '<<ALLTRIM(ft.descar)>>', Left('<<time()>>',5), <<ft.ivatx1>>
						,<<ft.ivatx2>>, <<ft.ivatx3>>, <<ft.fin>>, <<ft.Ndoc>>, '<<ALLTRIM(ft.moeda)>>',	'<<ALLTRIM(ft.fref)>>', '<<ALLTRIM(ft.ccusto)>>', '<<ALLTRIM(ft.ncusto)>>'
						,<<IIF(ft.facturada,1,0)>>, <<ft.fnoft>>, '<<ALLTRIM(ft.nmdocft)>>', <<ft.estab>>, '<<uf_gerais_getDate(Ft.cdata,"SQL")>>', <<ft.ivatx4>>, '<<ALLTRIM(ft.segmento)>>', <<ft.TotQtt>>
						,<<ft.Qtt1>>, <<ft.qtt2>>, '<<ALLTRIM(ft.tipo)>>', <<IIF(ft.cobrado,1,0)>>, '<<ALLTRIM(ft.cobranca)>>', <<ft.TipoDoc>>, Left('<<Chrtran(time(),":","")>>',4), <<ft.ivatx5>>
						,<<ft.ivatx6>>, <<ft.ivatx7>>, <<ft.ivatx8>>, <<ft.ivatx9>>, <<IIF(ft.cambiofixo,1,0)>>, '<<ALLTRIM(ft.memissao)>>', '<<ALLTRIM(ft.cobrador)>>', '<<ALLTRIM(ft.rota)>>'
						,<<IIF(ft.multi,1,0)>>, <<IIF(ft.cheque,1,0)>>, '<<ALLTRIM(ft.clbanco)>>', '<<ALLTRIM(ft.clcheque)>>', <<ft.chtotal>>,<<ft.echtotal>>,<<ft.custo>>, <<ft.EivaIn1>>
						,<<ft.EivaIn2>>, <<ft.EivaIn3>>, <<ft.EivaV1>>, <<ft.EivaV2>>, <<ft.EivaV3>>, <<ft.EttIliq>>, <<ft.edescc>>, <<ft.EttIva>>
						,<<ft.Etotal>>, <<ft.EivaIn4>>, <<ft.EivaV4>>, <<ft.efinv>>, <<ft.Ecusto>>, <<ft.EivaIn5>>, <<ft.EivaV5>>, <<ft.edebreg>>
						,<<ft.eivain6>>, <<ft.eivav6>>, <<ft.eivain7>>, <<ft.eivav7>>, <<ft.eivain8>>, <<ft.eivav8>>, <<ft.eivain9>>, <<ft.eivav9>>
						,<<ft.total>>, <<ft.totalmoeda>>, <<ft.ivaIn1>>, <<ft.ivaIn2>>, <<ft.ivaIn3>>, <<ft.ivaIn4>>, <<ft.ivaIn5>>, <<ft.ivain6>>
						,<<ft.ivain7>>, <<ft.ivain8>>, <<ft.ivain9>>, <<ft.ivaV1>>, <<ft.ivaV2>>, <<ft.ivaV3>>, <<ft.ivaV4>>, <<ft.ivaV5>>
						,<<ft.ivav6>>, <<ft.ivav7>>, <<ft.ivav8>>, <<ft.ivav9>>, <<ft.ttIliq>>,	<<ft.ttIva>>, <<ft.descc>>, <<ft.debreg>>
						,<<ft.debregm>>, '<<ALLTRIM(ft.intid)>>', '<<ALLTRIM(ft.nome2)>>', '<<ALLTRIM(ft.tpstamp)>>', '<<ALLTRIM(ft.tpdesc)>>', <<ft.erdtotal>>, <<ft.rdtotal>>, <<ft.rdtotalm>>
						,<<ft.cambio>>, '<<ALLTRIM(ft.Site)>>', '<<ALLTRIM(ft.pnome)>>', <<ft.pNo>>, '<<ALLTRIM(ft.cxstamp)>>', '<<ALLTRIM(ft.cxusername)>>', '<<ALLTRIM(ft.ssstamp)>>', '<<ALLTRIM(ft.ssusername)>>'
						,<<IIF(ft.anulado,1,0)>>, <<ft.virs>>, <<ft.evirs>>, <<ft.valorm2>>, '<<ft.u_lote>>', '<<ft.u_tlote>>', '<<ft.u_tlote2>>', '<<ALLTRIM(ft.u_ltstamp)>>'
						,<<ft.u_slote>>, <<ft.u_slote2>>, <<ft.u_nslote>>, <<ft.u_nslote2>>, '<<ALLTRIM(ft.u_ltstamp2)>>', <<ft.u_lote2>>, '<<ALLTRIM(m_chinis)>>', CONVERT(varchar,getdate(),102)
						, '<<ALLTRIM(time())>>', '<<ALLTRIM(m_chinis)>>', CONVERT(varchar,getdate(),102), '<<ALLTRIM(time())>>', '<<ALLTRIM(ft.u_nratend)>>','<<ALLTRIM(ft.u_hclstamp)>>', '<<ALLTRIM(uf_gerais_getDate(ft.datatransporte,"SQL"))>>','<<ALLTRIM(ft.localcarga)>>'
						,<<ft.id_tesouraria_conta>>
					)
				ENDTEXT
				lcExecuteSQL = lcExecuteSQL + CHR(13) + lcSQL
							
				SELECT fi
				GO TOP 
				SCAN
					lcSQL = ''
					TEXT TO lcSql NOSHOW TEXTMERGE 
							insert into fi (
								fistamp, nmdoc, fno, ref, design, qtt, tiliquido, etiliquido
								,unidade, unidad2, iva, ivaincl, codigo, tabiva, ndoc, armazem
								,fnoft, ndocft, ftanoft, ftregstamp, lobs2, litem2, litem, lobs3
								,rdata,	cpoc, composto, lrecno, lordem, fmarcada, partes, altura
								,largura, oref, lote, usr1, usr2, usr3,	usr4, usr5
								,ftstamp, cor, tam, stipo, fifref, tipodoc, familia, bistamp
								,stns, ficcusto, fincusto, ofistamp, pv, pvmoeda, epv, tmoeda
								,eaquisicao, custo, ecusto, slvu, eslvu, sltt,esltt,slvumoeda
								,slttmoeda, nccod, epromo, fivendedor, fivendnm, desconto, desc2, desc3
								,desc4, desc5, desc6, u_descval, iectin, eiectin, vbase, evbase
								,tliquido, num1, pcp, epcp, pvori,epvori, zona, morada
								,[local], codpost, telefone, email, tkhposlstamp, u_generico, u_cnp, u_psicont
								,u_bencont, u_psico, u_benzo, u_ettent1, u_ettent2, u_epref, pic, pvpmaxre
								,opcao, u_stock, u_epvp, u_ip, u_comp, u_diploma, ousrinis, ousrdata
								,ousrhora, usrinis, usrdata, usrhora, marcada, u_genalt, u_txcomp, amostra
								,u_refvale, EVALDESC, VALDESC, u_codemb, refentidade, campanhas, id_Dispensa_Eletronica_D
								,pvp4_fee
							)
							Values (
								'<<ALLTRIM(fi.FiStamp)>>', '<<fi.nmDoc>>', <<fi.fno>>, '<<ALLTRIM(fi.ref)>>', '<<ALLTRIM(fi.design)>>', <<fi.qtt>>, <<fi.tiliquido>>, <<fi.etiliquido>>
								,'<<ALLTRIM(fi.unidade)>>', '<<ALLTRIM(fi.unidad2)>>', <<fi.iva>>, <<IIF(fi.ivaincl,1,0)>>,	'<<ALLTRIM(fi.codigo)>>', <<fi.tabiva>>, <<fi.Ndoc>>, <<fi.armazem>>
								,<<fi.fnoft>>, <<fi.ndocft>>, <<fi.ftanoft>>, '<<ALLTRIM(fi.ftregstamp)>>', '<<ALLTRIM(fi.lobs2)>>', '<<ALLTRIM(fi.litem2)>>', '<<ALLTRIM(fi.litem)>>', '<<ALLTRIM(fi.lobs3)>>'
								,'<<uf_gerais_getDate(lcFData,"SQL")>>', <<fi.cpoc>>, <<IIF(fi.composto,1,0)>>, '<<ALLTRIM(fi.lrecno)>>', <<fi.lordem>>, <<IIF(fi.fmarcada,1,0)>>, <<fi.partes>>, <<fi.altura>>
								,<<fi.largura>>, '<<ALLTRIM(fi.oref)>>', '<<ALLTRIM(fi.lote)>>', '<<ALLTRIM(fi.usr1)>>', '<<ALLTRIM(fi.usr2)>>', '<<ALLTRIM(fi.usr3)>>', '<<ALLTRIM(fi.usr4)>>', '<<ALLTRIM(fi.usr5)>>'
								,'<<ALLTRIM(fi.FtStamp)>>','<<ALLTRIM(fi.cor)>>', '<<ALLTRIM(fi.tam)>>', 1, '<<ALLTRIM(fi.fifref)>>', <<fi.TipoDoc>>, '<<ALLTRIM(fi.familia)>>', '<<ALLTRIM(fi.bistamp)>>'
								,<<IIF(fi.stns,1,0)>>, '<<ALLTRIM(fi.ficcusto)>>', '<<ALLTRIM(fi.fincusto)>>', '<<ALLTRIM(fi.ofistamp)>>', <<fi.pv>>, <<fi.pvmoeda>>, <<fi.epv>>, <<fi.tmoeda>>
								,<<fi.eaquisicao>>, <<fi.custo>>, <<fi.ecusto>>, <<fi.slvu>>, <<fi.eslvu>>,	<<fi.sltt>>, <<fi.esltt>>, <<fi.slvumoeda>>
								,<<fi.slttmoeda>>, '<<ALLTRIM(fi.nccod)>>', <<IIF(fi.epromo,1,0)>>, <<ft.vendedor>>, '<<ALLTRIM(ft.vendnm)>>', <<fi.desconto>>, <<fi.desc2>>, <<fi.desc3>>
								,<<fi.desc4>>, <<fi.desc5>>, <<fi.desc6>>, <<fi.u_descval>>, <<fi.iectin>>, <<fi.eiectin>>,	<<fi.vbase>>, <<fi.evbase>>
								,<<fi.tliquido>>, <<fi.num1>>, <<fi.pcp>>, <<fi.epcp>>, <<fi.pv>>, <<fi.epv>>, '<<ALLTRIM(fi.zona)>>', '<<ALLTRIM(fi.morada)>>'
								,'<<ALLTRIM(fi.local)>>', '<<ALLTRIM(fi.codpost)>>', '<<ALLTRIM(fi.telefone)>>', '<<ALLTRIM(fi.email)>>', '<<ALLTRIM(fi.tkhposlstamp)>>', <<IIF(fi.u_generico,1,0)>>, '<<ALLTRIM(fi.u_cnp)>>', <<fi.u_psicont>>
								, <<fi.u_bencont>>, <<IIF(fi.u_psico,1,0)>>, <<IIF(fi.u_benzo,1,0)>>, <<fi.u_ettent1>>, <<fi.u_ettent2>>, <<fi.u_epref>>, <<fi.pic>>, <<fi.pvpmaxre>>
								,'<<ALLTRIM(fi.opcao)>>', <<fi.u_stock>>, <<fi.u_epvp>>, <<IIF(fi.u_ip,1,0)>>, <<IIF(fi.u_comp,1,0)>>, '<<ALLTRIM(fi.u_diploma)>>', '<<ALLTRIM(m_chinis)>>', CONVERT(varchar,getdate(),102)
								,'<<ALLTRIM(time())>>', '<<ALLTRIM(m_chinis)>>', CONVERT(varchar,getdate(),102), '<<ALLTRIM(time())>>', 0, <<IIF(fi.u_genalt,1,0)>>, <<fi.u_txcomp>>, <<IIF(fi.amostra, 1, 0)>>
								,'<<ALLTRIM(fi.u_refvale)>>', <<IIF(fi.qtt == 0,0,(fi.u_descval / fi.qtt))>>, <<IIF(fi.qtt == 0,0,(fi.u_descval / fi.qtt) * 200.482)>>, '<<ALLTRIM(fi.u_codemb)>>', '<<ALLTRIM(fi.refentidade)>>', '<<ALLTRIM(fi.campanhas)>>', '<<ALLTRIM(fi.id_validacaoDem)>>'
								,<<fi.pvp4_fee>>
							)
						ENDTEXT
						
						lcExecuteSQL  = lcExecuteSQL + CHR(13) + lcSQL
				
					SELECT fi 
				ENDSCAN
				
				SELECT uCrsDocAssociados 
				GO top
				SCAN
					IF uCrsDocAssociados.emitir = .t. AND uCrsDocAssociados.nr_org=lcClno AND uCrsDocAssociados.valor<>0 &&AND uCrsDocAssociados.id_org=lcClestab
						lcSQL = ''
						TEXT TO lcSql NOSHOW TEXTMERGE 
							UPDATE cl_fatc_org SET emitidoe=1 WHERE id='<<ALLTRIM(uCrsDocAssociados.id)>>'
						ENDTEXT
						lcExecuteSQL  = lcExecuteSQL + CHR(13) + lcSQL
					ENDIF 
				ENDSCAN 
				
	*!*				IF lcTotRecebido > 0 then
	*!*					lcSQL = ''
	*!*					TEXT TO lcSql NOSHOW TEXTMERGE 
	*!*						UPDATE cc SET ecredf=<<lcTotRecebido>>, credf=<<lcTotRecebido*200.482>> WHERE ftstamp='<<ALLTRIM(lcFtStamp)>>'
	*!*					ENDTEXT
	*!*					lcExecuteSQL  = lcExecuteSQL + CHR(13) + lcSQL
	*!*				ENDIF 

				
											
				IF !uf_gerais_actGrelha("","",lcExecuteSQL)
					uf_perguntalt_chama("OCORREU UMA ANOMALIA A GRAVAR O DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				ELSE
					&& Tudo Ok
					**lcValidaInsercaoDocumento = .t.
				ENDIF
				SELECT ucrs_ft_aux 
				lcCont2 = lcCont2 + 1	
			ENDSCAN 
			uf_perguntalt_chama("PROCESSO CONCLU�DO COM SUCESSO.","OK","",64)
			regua(2)
		ENDIF 	
		uf_PESQDOCASSOCIADOS_actualizar()
	ELSE
		uf_perguntalt_chama("PROCESSAMENTO CANCELADO.","OK","",64)
	ENDIF
		
ENDFUNC 


** Emitir Aviso lan�amento de entidades
FUNCTION uf_PESQDOCASSOCIADOS_emitir_nlentidades

	LOCAL lcFtStamp, lcFno, lcClNcont, lcClno, lcClestab, lcTotal, lcQtt, lcFiStamp, lcLordem, lcTotRecebido
	STORE '' TO lcFtStamp, lcClNcont, lcFiStamp
	STORE 0 TO lcFno, lcClno, lcClestab, lcTotal, lcQtt, lcLordem, lcTotRecebido
	
	** Criar cursores para emitir documentos de fatura��o
	IF !USED("FT")
		IF !uf_gerais_actGrelha("",[FT],[set fmtonly on exec up_touch_ft '' set fmtonly off])
			uf_perguntalt_chama("Ocorreu uma anomalia ao criar o cursor [FT]. Por favor reinicie o Software.","OK","",16)
			RETURN .F.
		ENDIF
	ELSE
		DELETE from ft
	ENDIF
	
	IF !USED("FT2")
		IF !uf_gerais_actGrelha("",[FT2],[set fmtonly on select * from FT2 (nolock) set fmtonly off])
			uf_perguntalt_chama("Ocorreu uma anomalia ao criar o cursor [FT2]. Por favor reinicie o Software.","OK","",16)
			RETURN .F.
		ENDIF	
	ELSE
		DELETE FROM FT2
	ENDIF
	
	uf_perguntalt_chama("POR FAVOR INSIRA A DATA PARA EMISS�O DOS DOCUMENTOS!","OK","",64)
	public lcFData
	uf_getdate_chama(.f., "lcFData", 0, .f., .f., .t.)
	
	IF uf_perguntalt_chama("CONFIRMA O LAN�AMENTO DOS DOCUMENTOS SELECIONADOS?","Sim","N�o", 64) 
	
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			select 
				convert(bit,0) as manipulado
				,convert(varchar(3),'') as tipoR
				,fi.id_Dispensa_Eletronica_D as id_validacaoDem
				,convert(varchar(20),'') as receita
				,convert(varchar(40),'') as token
				,convert(bit,0) as dem
				,0 as qtDem
				,convert(varchar(250),design) as design
				,convert(bit,0) as usalote
				,convert(bit,0) as alertalote
				,convert(bit,0) as compSNS
				,convert(varchar(8),'') as CNPEM
				,0.000 as PvpTop4,* 
			from 
				FI (nolock) 
				left join fi_comp (nolock) on fi.fistamp = fi_comp.id_fi
			WHERE 
				0=1
		ENDTEXT 
		uf_gerais_actGrelha("","FI",lcSQL)
		
		IF USED("ucrs_ft_aux")
			fecha("ucrs_ft_aux ")
		ENDIF 
		
		LOCAL lcCont2
		lcCont2 = 1
		select distinct nr_org, id_org, nome_org, ncont_org, morada_org, codpost_org, local_org from uCrsDocAssociados where emitir=.t. AND emitidoale=.f. AND valor_ret<>0 order by nome_org into cursor ucrs_ft_aux readwrite
		SELECT ucrs_ft_aux 
		IF RECCOUNT("ucrs_ft_aux")=0 then
			uf_perguntalt_chama("N�o escolheu nenhum registo para emitir o Aviso de Lan�amento ou os registos escolhidos j� foram emitidos.", "", "OK", 16)
		ELSE
			regua(0,RECCOUNT("ucrs_ft_aux"),"A emitir Documentos...")
			SELECT ucrs_ft_aux 
			GO TOP 
			SCAN
				regua(1,lcCont2,"A emitir Documento: " + astr(lcCont2))
				lcClno = ucrs_ft_aux.nr_org
				lcClestab = 0
				TEXT TO lcSQL TEXTMERGE NOSHOW
					select isnull(max(fno),0)+1 as fno from ft where nmdoc='Aviso Lanc. Ent.' --and ftano=year(getdate())
				ENDTEXT
				If !uf_gerais_actGrelha("", "ucrsDadosFt", lcSql)
					uf_perguntalt_chama("N�O FOI POSS�VEL O N� DO PR�XIMO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE","OK","",16)
					RETURN .f.
				ENDIF
				lcFno=ucrsDadosFt.fno
				TEXT TO lcSQL TEXTMERGE NOSHOW
					select * from b_utentes where no=<<lcClno>> and estab=<<lcClestab>>
				ENDTEXT
				If !uf_gerais_actGrelha("", "ucrsDadosUt", lcSql)
					uf_perguntalt_chama("N�O FOI POSS�VEL O N� DO PR�XIMO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE","OK","",16)
					RETURN .f.
				ENDIF	
				lcClNcont=ALLTRIM(ucrsDadosUt.ncont) 		
				lcFtStamp=uf_gerais_stamp()
				
				SELECT ft
				DELETE ALL 
				APPEND BLANK
				replace	ft.ftstamp 	WITH 	ALLTRIM(lcFtStamp)
				replace	ft.pais 	WITH 	1
				replace	ft.nmdoc 	WITH 	'Aviso Lanc. Ent.'
				replace	ft.fno 		WITH 	lcFno
				replace	ft.no 		WITH 	lcClno 
				replace	ft.estab	WITH 	lcClestab 
				replace	ft.nome 	WITH 	ucrs_ft_aux.nome_org
				replace	ft.morada 	WITH 	ucrs_ft_aux.morada_org
				replace	ft.local 	WITH 	ucrs_ft_aux.local_org
				replace	ft.codpost 	WITH 	ucrs_ft_aux.codpost_org
				replace	ft.ncont 	WITH 	lcClNcont
				replace	ft.vendedor WITH 	ch_vendedor
				replace	ft.vendnm 	WITH 	ALLTRIM(ch_vendnm)
				replace	ft.fdata 	WITH 	date()
				replace	ft.pdata 	WITH 	date()
				replace	ft.ftano 	WITH 	YEAR(date())
				replace	Ft.Ivatx1 	With	uf_gerais_getTabelaIVA(1,"TAXA") 
				replace	Ft.Ivatx2 	With	uf_gerais_getTabelaIVA(2,"TAXA") 
				replace	Ft.Ivatx3 	With	uf_gerais_getTabelaIVA(3,"TAXA") 
				replace	Ft.Ivatx4 	With	uf_gerais_getTabelaIVA(4,"TAXA") 
				replace	Ft.Ivatx5 	With	uf_gerais_getTabelaIVA(5,"TAXA") 
				replace	Ft.Ivatx6 	With	uf_gerais_getTabelaIVA(6,"TAXA") 
				replace	Ft.Ivatx7 	With	uf_gerais_getTabelaIVA(7,"TAXA") 
				replace	Ft.Ivatx8 	With	uf_gerais_getTabelaIVA(8,"TAXA") 
				replace	Ft.Ivatx9 	With	uf_gerais_getTabelaIVA(9,"TAXA") 
				replace	ft.ndoc		with	83
				replace	ft.moeda	with	'EURO'
				replace	ft.cdata	with 	date()
				replace	ft.tipo		with	'Farm�cia'
				replace	ft.tipodoc	with	1
				replace	ft.memissao	with 	'EURO'
				replace	ft.site		with	ALLTRIM(mySite)
				replace	ft.pnome	with	ALLTRIM(myTerm)
				replace	ft.pno 		WITH 	myTermNo
				replace	ft.ousrinis	WITH 	m_chinis
				replace	ft.ousrdata	WITH 	DATE()
				replace	ft.ousrhora	WITH 	time()
				replace	ft.usrinis	WITH 	m_chinis
				replace	ft.usrdata	WITH 	DATE()
				replace	ft.usrhora	WITH 	time()
				replace	ft.u_nratend	WITH 	'AUTOMATICO'
					
				select ft2
				DELETE ALL 
				APPEND BLANK 
				replace	ft2.ft2stamp 	WITH 	ALLTRIM(lcFtStamp)
				replace	ft2.vdollocal	WITH 	'Caixa'
				replace	ft2.vdlocal		WITH 	'C'
				replace	ft2.formapag	WITH 	'1'
				replace	ft2.ousrinis	WITH 	m_chinis
				replace	ft2.ousrdata	WITH 	DATE()
				replace	ft2.ousrhora	WITH 	time()
				replace	ft2.usrinis		WITH 	m_chinis
				replace	ft2.usrdata		WITH 	DATE()
				replace	ft2.usrhora		WITH 	time()
				
				SELECT fi
				DELETE ALL 
				lcTotal = 0 
				lcTotRecebido = 0
				lcQtt = 0
				lcLordem = 100000
				SELECT uCrsDocAssociados 
				GO top
				SCAN
					IF uCrsDocAssociados.emitir = .t. AND uCrsDocAssociados.nr_org=lcClno AND uCrsDocAssociados.valor_ret<>0 &&AND uCrsDocAssociados.id_org=lcClestab
						lcFiStamp = uf_gerais_stamp()
						SELECT fi 
						APPEND blank
							replace fi.fistamp		WITH	alltrim(lcFiStamp)
							replace fi.nmdoc		WITH 	'Aviso Lanc. Ent.'
							replace fi.fno			WITH 	lcFno
							replace fi.ref			WITH 	'999999'
							replace fi.design		WITH 	ALLTRIM(uCrsDocAssociados.nome_cl)
							replace fi.qtt			with	1
							replace fi.etiliquido	WITH 	(uCrsDocAssociados.valor+uCrsDocAssociados.valor_ret)*(-1)
							replace fi.tiliquido	WITH 	((uCrsDocAssociados.valor+uCrsDocAssociados.valor_ret)*(-1))*200.482
							replace fi.iva			with	0
							replace fi.ivaincl		with	.t.
							replace fi.tabiva		with	4
							replace fi.ndoc			with	83
							replace fi.armazem		with	1
							replace fi.ftanoft		with	YEAR(date())
							replace fi.litem2		with	ALLTRIM(STR(uCrsDocAssociados.mes_tratamento))
							replace fi.litem		with	ALLTRIM(STR(uCrsDocAssociados.ano_tratamento))
							replace fi.rdata		with	date()
							replace fi.cpoc			with 	1
							replace fi.lrecno		with 	alltrim(lcFiStamp)
							replace fi.lordem		with	lcLordem 
							replace fi.ftstamp		with	ALLTRIM(lcFtStamp)
							replace fi.stipo		with	1
							replace fi.tipodoc		with	1
							replace fi.stns 		with	.t.
							replace fi.epv			WITH 	uCrsDocAssociados.valor
							replace fi.pv			WITH 	uCrsDocAssociados.valor*200.482
							**replace fi.pbruto		with	uCrsDocAssociados.valor_ret
							replace fi.epcp			with	uCrsDocAssociados.valor_ret
							replace fi.eslvu		WITH 	uCrsDocAssociados.valor+uCrsDocAssociados.valor_ret
							replace fi.slvu			WITH 	(uCrsDocAssociados.valor+uCrsDocAssociados.valor_ret)*200.482
							replace fi.esltt		WITH 	(uCrsDocAssociados.valor+uCrsDocAssociados.valor_ret)*(-1)
							replace fi.sltt			WITH 	((uCrsDocAssociados.valor+uCrsDocAssociados.valor_ret)*(-1))*200.482
							replace fi.fivendedor 	WITH 	ch_vendedor
							replace fi.fivendnm 	WITH 	ALLTRIM(ch_vendnm)
							replace fi.epvori		WITH 	uCrsDocAssociados.valor+uCrsDocAssociados.valor_ret
							replace fi.pvori		WITH 	(uCrsDocAssociados.valor+uCrsDocAssociados.valor_ret)*200.482
							replace fi.ousrinis		WITH 	m_chinis
							replace fi.ousrdata		WITH 	DATE()
							replace fi.ousrhora		WITH 	time()
							replace fi.usrinis		WITH 	m_chinis
							replace fi.usrdata		WITH 	DATE()
							replace fi.usrhora		WITH 	time()
							replace fi.u_epvp		WITH 	uCrsDocAssociados.valor+uCrsDocAssociados.valor_ret
							replace fi.u_txcomp		WITH 	100
							replace fi.lobs2 		WITH 	uCrsDocAssociados.id_cl
							replace fi.lobs3 		WITH 	ALLTRIM(STR(uCrsDocAssociados.nrdoc))
							
						lcTotal=lcTotal + ((uCrsDocAssociados.valor+uCrsDocAssociados.valor_ret)*(-1))	
						lcQtt = lcQtt + 1
						lcLordem = lcLordem + 101000
						IF uCrsDocAssociados.recebido = .t. 
							lcTotRecebido = lcTotRecebido + (uCrsDocAssociados.valor+uCrsDocAssociados.valor_ret)
						ENDIF 
					ENDIF
					SELECT uCrsDocAssociados  
				ENDSCAN 
				
				SELECT ft
				replace ft.totqtt		with	lcQtt
				replace ft.qtt1 		with	lcQtt
				replace	ft.eivain4		with	lcTotal
				replace	ft.ivain4		with	lcTotal*200.482
				replace	ft.ettiliq		with	lcTotal
				replace	ft.ttiliq		with	lcTotal*200.482
				replace	ft.etotal		with	lcTotal
				replace	ft.total		with	lcTotal*200.824
				replace ft.edebreg		WITH 	0
					
				lcExecuteSQL = ''
				
				lcSQL = ''
				TEXT TO lcSQL NOSHOW TEXTMERGE
					Insert Into ft2 (
						contacto, u_viatura, morada, codpost, local, telefone, email, horaentrega
						,ft2stamp, u_vddom, obsdoc, u_receita, u_entpresc, u_nopresc, u_codigo, u_codigo2
						,u_nbenef, u_nbenef2, u_abrev, u_abrev2, c2no, c2estab, subproc, u_vdsusprg
						,u_design, u_design2, c2nome, c2pais, evdinheiro, epaga1, epaga2, etroco
						,u_acertovs, c2codpost, eacerto, vdollocal, vdlocal, modop1, modop2, ousrdata
						,ousrinis, ousrhora, usrdata, usrinis, usrhora, motiseimp, formapag, epaga3
						,epaga4, epaga5, epaga6, codAcesso, codDirOpcao, token, u_docorig
						, token_efectivacao_compl, token_anulacao_compl
					)
					Values (
						'<<ALLTRIM(ft2.contacto)>>', '<<ALLTRIM(ft2.u_viatura)>>', '<<ALLTRIM(ft2.morada)>>', '<<ALLTRIM(ft2.codpost)>>', '<<ALLTRIM(ft2.local)>>', '<<ALLTRIM(ft2.telefone)>>', '<<ALLTRIM(ft2.email)>>', '<<ALLTRIM(ft2.horaentrega)>>'
						,'<<ALLTRIM(ft2.Ft2Stamp)>>', <<IIF(ft2.u_vddom,1,0)>>, '<<ALLTRIM(ft2.obsdoc)>>', '<<LEFT(ft2.u_receita,20)>>', '<<ALLTRIM(ft2.u_entpresc)>>', '<<ALLTRIM(ft2.u_nopresc)>>', '<<ALLTRIM(ft2.u_codigo)>>', '<<ALLTRIM(ft2.u_codigo2)>>'
						,'<<ALLTRIM(ft2.u_nbenef)>>', '<<ALLTRIM(ft2.u_nbenef2)>>', '<<ALLTRIM(ft2.u_abrev)>>', '<<ALLTRIM(ft2.u_abrev2)>>', <<ft2.c2no>>, <<ft2.c2estab>>, '<<ALLTRIM(ft2.subproc)>>', <<IIF(ft2.u_vdsusprg,1,0)>>
						,'<<ALLTRIM(ft2.u_design)>>', '<<ALLTRIM(ft2.u_design2)>>', '<<ALLTRIM(ft2.c2nome)>>', <<ft2.c2pais>>, <<ft2.evdinheiro)>>, <<ft2.epaga1>>, <<ft2.epaga2>>, <<ft2.etroco>>
						,<<ft2.u_acertovs>>, '<<ALLTRIM(ft2.c2codpost)>>', <<ft2.eacerto>>, 'Caixa', 'C', '<<ALLTRIM(ft2.modop1)>>', '<<ALLTRIM(ft2.modop2)>>', CONVERT(varchar,getdate(),102)
						,'<<ALLTRIM(m_chinis)>>', '<<ALLTRIM(time())>>', CONVERT(varchar,getdate(),102), '<<ALLTRIM(m_chinis)>>', '<<ALLTRIM(time())>>', '<<ALLTRIM(ft2.motiseimp)>>', '1', <<ft2.epaga3>>
						,<<ft2.epaga4>>, <<ft2.epaga5>>, <<ft2.epaga6>>, '<<ALLTRIM(ft2.codAcesso)>>', '<<ALLTRIM(ft2.CodDirOpcao)>>','<<ALLTRIM(ft2.token)>>', '<<ALLTRIM(ft2.u_docorig)>>'
						,'',''
					)
				ENDTEXT
				lcExecuteSQL  = lcExecuteSQL + CHR(13) + lcSQL

				
				lcSQL = ''
				TEXT TO lcSql NOSHOW TEXTMERGE
					INSERT INTO ft (
						ftstamp, pais, nmdoc, fno, [no], nome, morada, [local]
						,codpost, ncont, bino, bidata, bilocal, telefone, zona, vendedor
						,vendnm, fdata, ftano, pdata, carga, descar, saida, ivatx1
						,ivatx2, ivatx3, fin, ndoc, moeda, fref, ccusto, ncusto
						,facturada, fnoft, nmdocft,	estab, cdata, ivatx4, segmento, totqtt
						,qtt1, qtt2, tipo, cobrado, cobranca, tipodoc, chora, ivatx5
						,ivatx6, ivatx7, ivatx8, ivatx9, cambiofixo, memissao, cobrador, rota
						,multi, cheque, clbanco, clcheque, chtotal,	echtotal, custo, eivain1
						,eivain2, eivain3, eivav1, eivav2, eivav3, ettiliq, edescc, ettiva
						,etotal, eivain4, eivav4, efinv, ecusto, eivain5, eivav5, edebreg
						,eivain6, eivav6, eivain7, eivav7, eivain8, eivav8, eivain9, eivav9
						,total, totalmoeda, ivain1, ivain2, ivain3, ivain4, ivain5, ivain6
						,ivain7, ivain8, ivain9, ivav1, ivav2, ivav3, ivav4, ivav5
						,ivav6, ivav7, ivav8, ivav9, ttiliq, ttiva, descc, debreg
						,debregm, intid, nome2, tpstamp, tpdesc, erdtotal, rdtotal
						,rdtotalm, cambio, [site], pnome, pno, cxstamp, cxusername, ssstamp, ssusername
						,anulado, virs, evirs, valorm2, u_lote, u_tlote, u_tlote2, u_ltstamp
						,u_slote, u_slote2, u_nslote, u_nslote2, u_ltstamp2, u_lote2, ousrinis, ousrdata
						,ousrhora, usrinis, usrdata, usrhora, u_nratend, u_hclstamp, datatransporte,localcarga
						,id_tesouraria_conta
					)
					Values (
						'<<ALLTRIM(ft.ftstamp)>>', <<ft.pais>>, '<<ALLTRIM(ft.NmDoc)>>', <<ft.fno>>, <<ft.no>>, '<<Alltrim(ft.nome)>>', '<<Alltrim(ft.morada)>>', '<<Alltrim(ft.local)>>'
						,'<<Alltrim(ft.codpost)>>', '<<ft.NCont>>', '<<Alltrim(ft.bino)>>', '<<uf_gerais_getDate(Ft.bidata,"SQL")>>', '<<Alltrim(ft.bilocal)>>', '<<Alltrim(ft.telefone)>>', '<<Alltrim(ft.zona)>>', <<ft.vendedor>>
						,'<<Alltrim(ft.vendnm)>>', '<<uf_gerais_getDate(lcFData,"SQL")>>', YEAR('<<uf_gerais_getDate(lcFData,"SQL")>>'), '<<uf_gerais_getDate(lcFData,"SQL")>>', '<<ALLTRIM(ft.carga)>>', '<<ALLTRIM(ft.descar)>>', Left('<<time()>>',5), <<ft.ivatx1>>
						,<<ft.ivatx2>>, <<ft.ivatx3>>, <<ft.fin>>, <<ft.Ndoc>>, '<<ALLTRIM(ft.moeda)>>',	'<<ALLTRIM(ft.fref)>>', '<<ALLTRIM(ft.ccusto)>>', '<<ALLTRIM(ft.ncusto)>>'
						,<<IIF(ft.facturada,1,0)>>, <<ft.fnoft>>, '<<ALLTRIM(ft.nmdocft)>>', <<ft.estab>>, '<<uf_gerais_getDate(Ft.cdata,"SQL")>>', <<ft.ivatx4>>, '<<ALLTRIM(ft.segmento)>>', <<ft.TotQtt>>
						,<<ft.Qtt1>>, <<ft.qtt2>>, '<<ALLTRIM(ft.tipo)>>', <<IIF(ft.cobrado,1,0)>>, '<<ALLTRIM(ft.cobranca)>>', <<ft.TipoDoc>>, Left('<<Chrtran(time(),":","")>>',4), <<ft.ivatx5>>
						,<<ft.ivatx6>>, <<ft.ivatx7>>, <<ft.ivatx8>>, <<ft.ivatx9>>, <<IIF(ft.cambiofixo,1,0)>>, '<<ALLTRIM(ft.memissao)>>', '<<ALLTRIM(ft.cobrador)>>', '<<ALLTRIM(ft.rota)>>'
						,<<IIF(ft.multi,1,0)>>, <<IIF(ft.cheque,1,0)>>, '<<ALLTRIM(ft.clbanco)>>', '<<ALLTRIM(ft.clcheque)>>', <<ft.chtotal>>,<<ft.echtotal>>,<<ft.custo>>, <<ft.EivaIn1>>
						,<<ft.EivaIn2>>, <<ft.EivaIn3>>, <<ft.EivaV1>>, <<ft.EivaV2>>, <<ft.EivaV3>>, <<ft.EttIliq>>, <<ft.edescc>>, <<ft.EttIva>>
						,<<ft.Etotal>>, <<ft.EivaIn4>>, <<ft.EivaV4>>, <<ft.efinv>>, <<ft.Ecusto>>, <<ft.EivaIn5>>, <<ft.EivaV5>>, <<ft.edebreg>>
						,<<ft.eivain6>>, <<ft.eivav6>>, <<ft.eivain7>>, <<ft.eivav7>>, <<ft.eivain8>>, <<ft.eivav8>>, <<ft.eivain9>>, <<ft.eivav9>>
						,<<ft.total>>, <<ft.totalmoeda>>, <<ft.ivaIn1>>, <<ft.ivaIn2>>, <<ft.ivaIn3>>, <<ft.ivaIn4>>, <<ft.ivaIn5>>, <<ft.ivain6>>
						,<<ft.ivain7>>, <<ft.ivain8>>, <<ft.ivain9>>, <<ft.ivaV1>>, <<ft.ivaV2>>, <<ft.ivaV3>>, <<ft.ivaV4>>, <<ft.ivaV5>>
						,<<ft.ivav6>>, <<ft.ivav7>>, <<ft.ivav8>>, <<ft.ivav9>>, <<ft.ttIliq>>,	<<ft.ttIva>>, <<ft.descc>>, <<ft.debreg>>
						,<<ft.debregm>>, '<<ALLTRIM(ft.intid)>>', '<<ALLTRIM(ft.nome2)>>', '<<ALLTRIM(ft.tpstamp)>>', '<<ALLTRIM(ft.tpdesc)>>', <<ft.erdtotal>>, <<ft.rdtotal>>, <<ft.rdtotalm>>
						,<<ft.cambio>>, '<<ALLTRIM(ft.Site)>>', '<<ALLTRIM(ft.pnome)>>', <<ft.pNo>>, '<<ALLTRIM(ft.cxstamp)>>', '<<ALLTRIM(ft.cxusername)>>', '<<ALLTRIM(ft.ssstamp)>>', '<<ALLTRIM(ft.ssusername)>>'
						,<<IIF(ft.anulado,1,0)>>, <<ft.virs>>, <<ft.evirs>>, <<ft.valorm2>>, '<<ft.u_lote>>', '<<ft.u_tlote>>', '<<ft.u_tlote2>>', '<<ALLTRIM(ft.u_ltstamp)>>'
						,<<ft.u_slote>>, <<ft.u_slote2>>, <<ft.u_nslote>>, <<ft.u_nslote2>>, '<<ALLTRIM(ft.u_ltstamp2)>>', <<ft.u_lote2>>, '<<ALLTRIM(m_chinis)>>', CONVERT(varchar,getdate(),102)
						, '<<ALLTRIM(time())>>', '<<ALLTRIM(m_chinis)>>', CONVERT(varchar,getdate(),102), '<<ALLTRIM(time())>>', '<<ALLTRIM(ft.u_nratend)>>','<<ALLTRIM(ft.u_hclstamp)>>', '<<ALLTRIM(uf_gerais_getDate(ft.datatransporte,"SQL"))>>','<<ALLTRIM(ft.localcarga)>>'
						,<<ft.id_tesouraria_conta>>
					)
				ENDTEXT
				lcExecuteSQL = lcExecuteSQL + CHR(13) + lcSQL
							
				SELECT fi
				GO TOP 
				SCAN
					lcSQL = ''
					TEXT TO lcSql NOSHOW TEXTMERGE 
							insert into fi (
								fistamp, nmdoc, fno, ref, design, qtt, tiliquido, etiliquido
								,unidade, unidad2, iva, ivaincl, codigo, tabiva, ndoc, armazem
								,fnoft, ndocft, ftanoft, ftregstamp, lobs2, litem2, litem, lobs3
								,rdata,	cpoc, composto, lrecno, lordem, fmarcada, partes, altura
								,largura, oref, lote, usr1, usr2, usr3,	usr4, usr5
								,ftstamp, cor, tam, stipo, fifref, tipodoc, familia, bistamp
								,stns, ficcusto, fincusto, ofistamp, pv, pvmoeda, epv, tmoeda
								,eaquisicao, custo, ecusto, slvu, eslvu, sltt,esltt,slvumoeda
								,slttmoeda, nccod, epromo, fivendedor, fivendnm, desconto, desc2, desc3
								,desc4, desc5, desc6, u_descval, iectin, eiectin, vbase, evbase
								,tliquido, num1, pcp, epcp, pvori,epvori, zona, morada
								,[local], codpost, telefone, email, tkhposlstamp, u_generico, u_cnp, u_psicont
								,u_bencont, u_psico, u_benzo, u_ettent1, u_ettent2, u_epref, pic, pvpmaxre
								,opcao, u_stock, u_epvp, u_ip, u_comp, u_diploma, ousrinis, ousrdata
								,ousrhora, usrinis, usrdata, usrhora, marcada, u_genalt, u_txcomp, amostra
								,u_refvale, EVALDESC, VALDESC, u_codemb, refentidade, campanhas, id_Dispensa_Eletronica_D
								,pvp4_fee
							)
							Values (
								'<<ALLTRIM(fi.FiStamp)>>', '<<fi.nmDoc>>', <<fi.fno>>, '<<ALLTRIM(fi.ref)>>', '<<ALLTRIM(fi.design)>>', <<fi.qtt>>, <<fi.tiliquido>>, <<fi.etiliquido>>
								,'<<ALLTRIM(fi.unidade)>>', '<<ALLTRIM(fi.unidad2)>>', <<fi.iva>>, <<IIF(fi.ivaincl,1,0)>>,	'<<ALLTRIM(fi.codigo)>>', <<fi.tabiva>>, <<fi.Ndoc>>, <<fi.armazem>>
								,<<fi.fnoft>>, <<fi.ndocft>>, YEAR('<<uf_gerais_getDate(lcFData,"SQL")>>'), '<<ALLTRIM(fi.ftregstamp)>>', '<<ALLTRIM(fi.lobs2)>>', '<<ALLTRIM(fi.litem2)>>', '<<ALLTRIM(fi.litem)>>', '<<ALLTRIM(fi.lobs3)>>'
								,'<<uf_gerais_getDate(lcFData,"SQL")>>', <<fi.cpoc>>, <<IIF(fi.composto,1,0)>>, '<<ALLTRIM(fi.lrecno)>>', <<fi.lordem>>, <<IIF(fi.fmarcada,1,0)>>, <<fi.partes>>, <<fi.altura>>
								,<<fi.largura>>, '<<ALLTRIM(fi.oref)>>', '<<ALLTRIM(fi.lote)>>', '<<ALLTRIM(fi.usr1)>>', '<<ALLTRIM(fi.usr2)>>', '<<ALLTRIM(fi.usr3)>>', '<<ALLTRIM(fi.usr4)>>', '<<ALLTRIM(fi.usr5)>>'
								,'<<ALLTRIM(fi.FtStamp)>>','<<ALLTRIM(fi.cor)>>', '<<ALLTRIM(fi.tam)>>', 1, '<<ALLTRIM(fi.fifref)>>', <<fi.TipoDoc>>, '<<ALLTRIM(fi.familia)>>', '<<ALLTRIM(fi.bistamp)>>'
								,<<IIF(fi.stns,1,0)>>, '<<ALLTRIM(fi.ficcusto)>>', '<<ALLTRIM(fi.fincusto)>>', '<<ALLTRIM(fi.ofistamp)>>', <<fi.pv>>, <<fi.pvmoeda>>, <<fi.epv>>, <<fi.tmoeda>>
								,<<fi.eaquisicao>>, <<fi.custo>>, <<fi.ecusto>>, <<fi.slvu>>, <<fi.eslvu>>,	<<fi.sltt>>, <<fi.esltt>>, <<fi.slvumoeda>>
								,<<fi.slttmoeda>>, '<<ALLTRIM(fi.nccod)>>', <<IIF(fi.epromo,1,0)>>, <<ft.vendedor>>, '<<ALLTRIM(ft.vendnm)>>', <<fi.desconto>>, <<fi.desc2>>, <<fi.desc3>>
								,<<fi.desc4>>, <<fi.desc5>>, <<fi.desc6>>, <<fi.u_descval>>, <<fi.iectin>>, <<fi.eiectin>>,	<<fi.vbase>>, <<fi.evbase>>
								,<<fi.tliquido>>, <<fi.num1>>, <<fi.pcp>>, <<fi.epcp>>, <<fi.pv>>, <<fi.epv>>, '<<ALLTRIM(fi.zona)>>', '<<ALLTRIM(fi.morada)>>'
								,'<<ALLTRIM(fi.local)>>', '<<ALLTRIM(fi.codpost)>>', '<<ALLTRIM(fi.telefone)>>', '<<ALLTRIM(fi.email)>>', '<<ALLTRIM(fi.tkhposlstamp)>>', <<IIF(fi.u_generico,1,0)>>, '<<ALLTRIM(fi.u_cnp)>>', <<fi.u_psicont>>
								, <<fi.u_bencont>>, <<IIF(fi.u_psico,1,0)>>, <<IIF(fi.u_benzo,1,0)>>, <<fi.u_ettent1>>, <<fi.u_ettent2>>, <<fi.u_epref>>, <<fi.pic>>, <<fi.pvpmaxre>>
								,'<<ALLTRIM(fi.opcao)>>', <<fi.u_stock>>, <<fi.u_epvp>>, <<IIF(fi.u_ip,1,0)>>, <<IIF(fi.u_comp,1,0)>>, '<<ALLTRIM(fi.u_diploma)>>', '<<ALLTRIM(m_chinis)>>', CONVERT(varchar,getdate(),102)
								,'<<ALLTRIM(time())>>', '<<ALLTRIM(m_chinis)>>', CONVERT(varchar,getdate(),102), '<<ALLTRIM(time())>>', 0, <<IIF(fi.u_genalt,1,0)>>, <<fi.u_txcomp>>, <<IIF(fi.amostra, 1, 0)>>
								,'<<ALLTRIM(fi.u_refvale)>>', <<IIF(fi.qtt == 0,0,(fi.u_descval / fi.qtt))>>, <<IIF(fi.qtt == 0,0,(fi.u_descval / fi.qtt) * 200.482)>>, '<<ALLTRIM(fi.u_codemb)>>', '<<ALLTRIM(fi.refentidade)>>', '<<ALLTRIM(fi.campanhas)>>', '<<ALLTRIM(fi.id_validacaoDem)>>'
								,<<fi.pvp4_fee>>
							)
						ENDTEXT
						
						lcExecuteSQL  = lcExecuteSQL + CHR(13) + lcSQL
				
					SELECT fi 
				ENDSCAN
				
				SELECT uCrsDocAssociados 
				GO top
				SCAN
					IF uCrsDocAssociados.emitir = .t. AND uCrsDocAssociados.nr_org=lcClno AND uCrsDocAssociados.valor_ret<>0 &&AND uCrsDocAssociados.id_org=lcClestab
						lcSQL = ''
						TEXT TO lcSql NOSHOW TEXTMERGE 
							UPDATE cl_fatc_org SET emitidoale=1 WHERE id='<<ALLTRIM(uCrsDocAssociados.id)>>'
						ENDTEXT
						lcExecuteSQL  = lcExecuteSQL + CHR(13) + lcSQL
					ENDIF 
				ENDSCAN 
				
	*!*				IF lcTotRecebido > 0 then
	*!*					lcSQL = ''
	*!*					TEXT TO lcSql NOSHOW TEXTMERGE 
	*!*						UPDATE cc SET ecredf=<<lcTotRecebido>>, credf=<<lcTotRecebido*200.482>> WHERE ftstamp='<<ALLTRIM(lcFtStamp)>>'
	*!*					ENDTEXT
	*!*					lcExecuteSQL  = lcExecuteSQL + CHR(13) + lcSQL
	*!*				ENDIF 

				
											
				IF !uf_gerais_actGrelha("","",lcExecuteSQL)
					uf_perguntalt_chama("OCORREU UMA ANOMALIA A GRAVAR O DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				ELSE
					&& Tudo Ok
					**lcValidaInsercaoDocumento = .t.
				ENDIF
				SELECT ucrs_ft_aux 
				lcCont2 = lcCont2 + 1	
			ENDSCAN 
			uf_perguntalt_chama("PROCESSO CONCLU�DO COM SUCESSO.","OK","",64)
			regua(2)
		ENDIF 	
		uf_PESQDOCASSOCIADOS_actualizar()
	ELSE
		uf_perguntalt_chama("PROCESSAMENTO CANCELADO.","OK","",64)
	ENDIF
		
ENDFUNC 


FUNCTION uf_PESQDOCASSOCIADOS_sel
    **IF uCrsDocAssociados.emitido=.f.
        IF uCrsDocAssociados.emitir=.t. then
            replace uCrsDocAssociados.emitir with .f.
        ELSE
            replace uCrsDocAssociados.emitir with .t.
        ENDIF 
        SELECT uCrsDocAssociados
        TABLEUPDATE(.t.)
        PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.gridPesq.refresh
        PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.gridPesq.setfocus
    **ELSE
       ** uf_perguntalt_chama("N?O PODE SELECIONAR UMA LINHA J? EMITIDA.","OK","",64)
    **ENDIF 
ENDFUNC 


** funcao para chmar report de forma a ser poss�vel exportar para excel no m�dulo da AFP associados
FUNCTION uf_imprimirGerais_ExportarDoc
	
	uf_gerais_TextoRodape("CL_FATC_ORG")
	public lcdo, lcAo , lcAno, lcMes
	uf_tecladoalpha_Chama("lcDo","Preencha o n�mero da entidade",.f.,.f.,0)
	uf_tecladoalpha_Chama("lcAno","Preencha o ano pretendido",.f.,.f.,0)
	uf_tecladoalpha_Chama("lcMes","Preencha o m�s pretendido",.f.,.f.,0)
	lcAo = lcDo 
	
	** valida��es
	IF EMPTY(lcDo)
		uf_perguntalt_chama("Tem de fornecer o n� de uma entidade. Por favor verfique.","OK","",16)
		return .f.
	ENDIF 
	
	lcReport = ''
	TEXT TO lcReport TEXTMERGE NOSHOW	
		&nome_org=<<>>&nr_org=<<alltrim(lcDo)>>&id_org=<<>>&nome_cl=<<>>&nr_cl=0&id_cl=<<>>&ano=<<alltrim(lcAno)>>&mes=<<alltrim(lcmes)>>&data_tratamento=0&imprimir=<<>>&nrdoc=<<>>
	ENDTEXT		
	
	uf_gerais_chamaReport("relatorio_Resumo_Faturas_Entidades", ALLTRIM(lcReport))
	
ENDFUNC

FUNCTION uf_PESQDOCASSOCIADOS_importdoccompart
    LPARAMETERS uv_tipoDoc

    IF EMPTY(uv_tipoDoc)
        uf_perguntalt_chama("N�o pode utilizar esta fun��o sem os par�metros preenchidos!","OK","",48)
        RETURN .F.
    ENDIF

    uv_pergunta = ""

    DO CASE 
        CASE uv_tipoDoc = 1
            uv_pergunta = "DESEJA IMPORTAR AS FATURAS EXTERNAS �S ENTIDADES?"
        CASE uv_tipoDoc = 2
            uv_pergunta = "DESEJA IMPORTAR AS NOTAS DE CR�DITO EXTERNAS �S ENTIDADES?"
    ENDCASE

	IF uf_perguntalt_chama(ALLTRIM(uv_pergunta),"Sim","N�o", 64) 
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			exec up_pesqdocassociados_pesqDocCompart <<ASTR(uv_tipoDoc)>>
		ENDTEXT 
		uf_gerais_actGrelha("","ucrsDocsExternos",lcSQL)
		IF RECCOUNT("ucrsDocsExternos") > 0
			LOCAL lcIdCl, lcNrclorg, lcAno, lcMes, lcAnotratamento, lcMestratamento, lcValor, lcValorret, lcValorpagar, lcCont, lcCont2, lnId 
			STORE 0 TO lcIdCl, lcNrclorg, lcAno, lcMes, lcAnotratamento, lcMestratamento, lcValor, lcValorret, lcValorpagar
			lcCont = 0
			lcCont2 = 1
			
			SELECT ucrsDocsExternos


			lcCont = RECCOUNT("ucrsDocsExternos")
			regua(0,lcCont,"A importar Documentos...")
			GO TOP
			SCAN
				regua(1,lcCont2,"A importar Documentos...")
				lcIdCl = ucrsDocsExternos.Id_Cl 
				lcNrclorg = ucrsDocsExternos.Nr_cl_org 
				lcAno = ucrsDocsExternos.Ano 
				lcMes = ucrsDocsExternos.Mes 
				lcAnotratamento = ucrsDocsExternos.Ano_tratamento 
				lcMestratamento = ucrsDocsExternos.Mes_tratamento 
				lcValor = ucrsDocsExternos.Valor 
				lcValorret = ucrsDocsExternos.Valor_ret 
				lcValorpagar = ucrsDocsExternos.Valor_pagar 
				lcStamp = ALLTRIM(ucrsDocsExternos.stamp)
				uv_docNr = ALLTRIM(ucrsDocsExternos.docNr)
				
				TEXT TO lcSQL NOSHOW TEXTMERGE 
					select 
						cfo.id  
						, cl.nome as nome_cl
						, Entidades.nome as nome_org
					from 
						cl_fatc_org cfo
						left join B_utentes as cl (nolock) on cl.id = cfo.id_cl
						left join B_utentes as Entidades (nolock) on Entidades.no = cfo.nr_cl_org
					where 
						id_cl='<<lcIdCl>>' 
						and nr_cl_org = <<lcNrclorg>> 
						and ano = <<lcAno>>
						and mes = <<lcMes>>
						and ano_tratamento = <<lcAnotratamento>>
						and mes_tratamento = <<lcMestratamento>>
						and cfo.tipoDoc = <<ASTR(uv_tipoDoc)>>
						and cfo.docNr = '<<ALLTRIM(uv_docNr)>>'
				ENDTEXT 
				
				uf_gerais_actGrelha("","ucrsDocLancado",lcSQL)
				IF RECCOUNT("ucrsDocLancado")>0 then
					IF uf_perguntalt_chama("Os valores da Farm�cia "+ALLTRIM(LEFT(ucrsDocLancado.nome_cl,25))+" para a entidade "+ALLTRIM(LEFT(ucrsDocLancado.nome_org,25))+" de "+ALLTRIM(STR(lcMes))+"/"+ALLTRIM(STR(lcAno))+" j� foram lan�ados."+CHR(13)+"Deseja substituir os mesmos?","Sim","N�o")
						TEXT TO lcSQL NOSHOW TEXTMERGE 
							update
								cl_fatc_org 
							set
								valor = <<lcValor>>
								,valor_ret = <<lcValorret>>
								,valor_pagar = <<lcValorpagar>>
								,obs = 'valor retificado via importa��o a ' + CONVERT(varchar, getdate(), 120)
								,data_alt = getdate()
							where 
								id='<<ALLTRIM(ucrsDocLancado.id)>>' 
						ENDTEXT 
						uf_gerais_actGrelha("","",lcSQL)
					ELSE
						TEXT TO lcSQL NOSHOW TEXTMERGE 
							update
								cl_fatc_org 
							set
								obs = 'Atualiza��o cancelada pelo operador <<ALLTRIM(m_chinis)>> em ' + CONVERT(varchar, getdate(), 120)
								,data_alt = getdate()
							where 
								id='<<ALLTRIM(ucrsDocLancado.id)>>' 
						ENDTEXT 
						uf_gerais_actGrelha("","",lcSQL)
					ENDIF 
				ELSE
				
					lnId = uf_gerais_stampid()
					
					TEXT TO lcSQL NOSHOW TEXTMERGE 
						INSERT INTO cl_fatc_org 
						(	id
							,id_cl
							,nr_cl_org
							,ano
							,mes
							,ano_tratamento
							,mes_tratamento
							,nrDoc
							,valor
							,valor_ret
							,valor_pagar
							,valor_regularizado
							,recebido
							,imprimir
							,obs
							,data
							,data_alt
							,fno_cl
							,data_fno_cl
							,ousrinis
							,usrinis
							,tipoDoc
							,docNr)
						SELECT 
							'<<lnId>>'
							,'<<lcIdcl>>'
							,'<<lcNrclorg>>'
							,'<<LcAno>>'
							,'<<Lcmes>>'
							,'<<lcAnotratamento>>'
							,'<<lcMestratamento>>'
							,(select ISNULL(MAX(nrDoc),0)+1 from cl_fatc_org)
							,'<<lcValor>>'
							,'<<lcValorret>>'
							,'<<lcValorpagar>>'
							,'0'
							,0
							,0
							,''
							,getdate()
							,getdate()
							,''
							,'19000101'
							,'ADM'
							,'ADM'
							,<<ASTR(uv_tipoDoc)>>
							,'<<ALLTRIM(uv_docNr)>>'	
					ENDTEXT 
					**_cliptext=lcSQL
					uf_gerais_actGrelha("","",lcSQL)
				ENDIF 
				fecha("ucrsDocLancado")
				SELECT ucrsDocsExternos
				
				TEXT TO lcSQL NOSHOW TEXTMERGE 
					update
						ext_esb_doc
					set
						export=1
					from
						ext_esb_doc (nolock)
						left join B_utentes as cl (nolock) on cl.ncont = ext_esb_doc .vatNr_ext_pharmacy and cl.no>200
						left join B_utentes as Entidades (nolock) on Entidades.ncont = ext_esb_doc .vatNr_ext_entity and Entidades.no<198
					where 
						cl.ncont = ext_esb_doc .vatNr_ext_pharmacy
						and Entidades.ncont = ext_esb_doc .vatNr_ext_entity
						and export=0
						and ext_esb_doc.stamp = '<<lcStamp>>'
				ENDTEXT 
				uf_gerais_actGrelha("","",lcSQL)
					
				lcCont2 = lcCont2 + 1
			ENDSCAN 			
			uf_perguntalt_chama("IMPORTA��O CONCLU�DA","OK","", 64) 
			regua(2)
			
			TEXT TO lcSQL NOSHOW TEXTMERGE 
		
				SELECT
					top <<lcCont>>
					cfo.id ,cfo.ano ,cfo.mes ,cfo.ano_tratamento ,cfo.mes_tratamento
					,cfo.nrDoc ,cfo.valor ,cfo.valor_ret ,cfo.valor_pagar ,cfo.valor_regularizado
					,cfo.recebido ,cfo.imprimir ,cfo.obs ,cfo.data ,cfo.data_alt
					,cfo.id_cl, cl.no as nr_cl, cl.estab as dep_cl, cl.nome as nome_cl
					,ncont_cl = cl.ncont, morada_cl = cl.morada, codpost_cl = cl.codpost
					,Entidades.no as nr_org, Entidades.id as id_org, Entidades.nome as nome_org
					,Entidades.morada as morada_org, Entidades.codpost as codpost_org, Entidades.ncont as ncont_org, Entidades.nome2 as abrev_org, Entidades.local as local_org
					,referencia = upper(left(datename(month, DateAdd(month , mes-1 , 0)),3)) +  + '/' + convert(varchar,ano)
					,cfo.fno_cl, cfo.data_fno_cl
					,cfo.ousrinis
					,cfo.usrinis
					,nrDocImp = 0
					,cfo.emitido
					,cfo.emitidoe
					,cfo.emitidoale
					,CAST(0 AS bit) as emitir
					,cfo.docNr
				from
					cl_fatc_org cfo (nolock)
					left join B_utentes as cl (nolock) on cl.id = cfo.id_cl
					left join B_utentes as Entidades (nolock) on Entidades.no = cfo.nr_cl_org
				order by
				cfo.data_alt desc		
			ENDTEXT 
 			IF !uf_gerais_actGrelha("PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.GridPesq","uCrsDocAssociados",lcSql)
				uf_perguntalt_chama("N�O FOI POSSIVEL ACTUALIZAR A LISTAGEM DE DOCUMENTOS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				RETURN .f.
			ENDIF
			uf_PESQDOCASSOCIADOS_calcvalorestotal()
			PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.gridPesq.refresh	
		ELSE
			uf_perguntalt_chama("DESCULPE MAS N�O ACHEI DOCUMENTOS PARA IMPORTAR","OK","", 64) 
		ENDIF 
		fecha("ucrsDocsExternos")
	ELSE
		uf_perguntalt_chama("OPERA��O CANCELADA?","OK","", 64) 
	ENDIF 
ENDFUNC 

	
	
FUNCTION uf_PESQDOCASSOCIADOS_ProcuraReceituarioMesAno
	local  numero_id_cls
	IF (PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.id_cl_novo.readonly == .f.)
		IF (!EMPTY(PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.id_cl_novo.Value) AND !EMPTY(PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.ano_tratamento_novo.Value) AND !EMPTY(PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.mes_tratamento_novo.Value))
			TEXT TO lcSQL NOSHOW TEXTMERGE 
				SELECT 
					COUNT(id_cl)  AS numeroId_cl
				FROM cl_fatc_org
				WHERE id_cl ='<<Alltrim(PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.id_cl_novo.Value)>>'  and ano_tratamento=<<PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.ano_tratamento_novo.Value>>
						and mes_tratamento =<<PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.mes_tratamento_novo.Value>>
			ENDTEXT 
			If !uf_gerais_actGrelha("", "numero_id_cls", lcSQL )
				uf_perguntalt_chama("N�O FOI POSS�VEL CONTAR RECEITUARIO PARA ESTE MES. POR FAVOR CONTACTE O SUPORTE","OK","",16)
				RETURN .f.
			ELSE 
				IF numero_id_cls.numeroId_cl > 0 
					IF !(uf_perguntalt_chama("J� existe receitu�rio lan�ado para essa Farm�cia."+ CHR(13) + "Pretende adicionar? " ,"Sim","N�o"))
						myPesqdocassociadosIntroducao	= .f.
						myPesqdocassociadosAlteracao	= .f.

						uf_PESQDOCASSOCIADOS_alternaMenu()
						uf_PESQDOCASSOCIADOS_actualizar()		
						RETURN .f.
					ENDIF 
				ENDIF 
			ENDIF 
		ENDIF 
	ENDIF 
ENDFUNC 

PROCEDURE uf_PESQDOCASSOCIADOS_importCartao

	LOCAL uv_noAssoc

	uv_noAssoc = PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.nr_org.value
	
	uf_importCartao_chama(uv_noAssoc)

ENDPROC