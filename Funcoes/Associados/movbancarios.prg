**
FUNCTION uf_MOVBANCARIOS_chama
	LPARAMETERS lcOrigem
	LOCAL lcNo, lcValor 
	STORE 0 TO lcNo, lcValor 
	
	IF TYPE("MOVBANCARIOS")=="U"
	
		lcSQL = ''
		TEXT TO  LcSQL NOSHOW TEXTMERGE 
			select id, nome, no, dep, conta, tipo, valor, obs, processado, convert(datetime,data) as data, ousrinis from tesouraria_mov WHERE 1=0
		ENDTEXT
		IF !uf_gerais_actGrelha("","ucrsMovBanc",lcSql)
			uf_perguntalt_chama("Ocorreu uma anomalia a configurar Registos Bancarios.","OK","",16)
			Return .f.
		ENDIF
		
		** cursor com a informa��o das linhas que est�o pendentes de pagamento
		SELECT .f. as sel, (uCrsDocAssociados.valor_pagar - uCrsDocAssociados.valor_regularizado) as valor_regularizar, * FROM uCrsDocAssociados WHERE uCrsDocAssociados.recebido = .f. INTO CURSOR ucrsMovBancDocEntidades READWRITE
		
		TEXT TO lcSQL NOSHOW TEXTMERGE
			exec up_pesqdocassociados_movclientes '', 0, '', ''
			,<<IIF(EMPTY(PESQDOCASSOCIADOS.Pageframe1.Page2.nr_cl.value),0,(PESQDOCASSOCIADOS.Pageframe1.Page2.nr_cl.value))>>
			,0
			,''
			,0
			,<<IIF(EMPTY(PESQDOCASSOCIADOS.Pageframe1.Page2.ano.Value),0,PESQDOCASSOCIADOS.Pageframe1.Page2.ano.Value)>>
			,<<IIF(EMPTY(PESQDOCASSOCIADOS.Pageframe1.Page2.mes.Value),0,PESQDOCASSOCIADOS.Pageframe1.Page2.mes.Value)>>
		ENDTEXT
		
		IF !uf_gerais_actGrelha("","ucrsMovBancDocAssociados",lcSql)
			uf_perguntalt_chama("Ocorreu uma anomalia a verificar os movimentos.","OK","",16)
			Return .f.
		ENDIF
		
		DO CASE 
			CASE lcOrigem == "PESQDOCASSOCIADOS_CLIENTE"

				IF EMPTY(PESQDOCASSOCIADOS.Pageframe1.Page2.nr_cl.value)
					uf_perguntalt_chama("Ainda n�o selecionou o cliente.","OK","",16)
					RETURN .f.
				ENDIF
								
				lcNo = PESQDOCASSOCIADOS.Pageframe1.Page2.nr_cl.value
				lcValor = IIF(EMPTY(PESQDOCASSOCIADOS.Pageframe1.Page2.TotalEntClientes.value),0,PESQDOCASSOCIADOS.Pageframe1.Page2.TotalEntClientes.value)
				lcANo = IIF(EMPTY(PESQDOCASSOCIADOS.Pageframe1.Page2.ano.value), 0, PESQDOCASSOCIADOS.Pageframe1.Page2.ano.value)
				lcMes =  IIF(EMPTY(PESQDOCASSOCIADOS.Pageframe1.Page2.mes.value), 0, PESQDOCASSOCIADOS.Pageframe1.Page2.mes.value)
				
				lcSQL = ''
				TEXT TO  lcSQL NOSHOW TEXTMERGE 
					select nome, no, estab from b_utentes WHERE b_utentes.no = <<lcNo>>
				ENDTEXT
				IF !uf_gerais_actGrelha("","ucrsDadosCliente",lcSql)
					uf_perguntalt_chama("Ocorreu um erro a verificar dados do Cliente.","OK","",16)
					Return .f.
				ENDIF
						
				SELECT ucrsDadosCliente
				SELECT ucrsMovBanc
				APPEND BLANK
				Replace ucrsMovBanc.nome 		WITH ucrsDadosCliente.nome
				Replace ucrsMovBanc.no 			WITH ucrsDadosCliente.no
				Replace ucrsMovBanc.dep 		WITH ucrsDadosCliente.estab
				Replace ucrsMovBanc.tipo 		WITH "Debito"
				Replace ucrsMovBanc.valor 		WITH IIF(EMPTY(lcValor),0,lcValor)
				Replace ucrsMovBanc.data 		WITH DATE()	
				Replace ucrsMovBanc.obs 		WITH "Transfer�ncia para o Cliente:" + ALLTRIM(ucrsDadosCliente.nome) + " referente ao ano: " + ASTR(lcANo) + ", m�s: " + ASTR(lcMes) + "."

			CASE lcOrigem == "PESQDOCASSOCIADOS_ENTIDADE"
				
				IF EMPTY(PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.nr_org.value)
					uf_perguntalt_chama("Ainda n�o selecionou a Entidade.","OK","",16)
					RETURN .f.
				ENDIF
								
				lcNo = PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.nr_org.value
				lcANo = IIF(EMPTY(PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.ano.value), 0, PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.ano.value)
				lcMes =  IIF(EMPTY(PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.mes.value), 0, PESQDOCASSOCIADOS.Pageframe1.lancamentoreceituario.mes.value)
				
				lcSQL = ''
				TEXT TO  lcSQL NOSHOW TEXTMERGE 
					select nome, no, estab from b_utentes WHERE b_utentes.no = <<lcNo>>
				ENDTEXT
				IF !uf_gerais_actGrelha("","ucrsDadosEntidade",lcSql)
					uf_perguntalt_chama("Ocorreu uma anomalia ao verificar dados da Entidade. Por favor contacte o Suporte.","OK","",16)
					Return .f.
				ENDIF
				
				SELECT ucrsDadosEntidade
				SELECT ucrsMovBanc
				APPEND BLANK
				Replace ucrsMovBanc.nome 	WITH ucrsDadosEntidade.nome
				Replace ucrsMovBanc.no 		WITH ucrsDadosEntidade.no
				Replace ucrsMovBanc.dep 	WITH ucrsDadosEntidade.estab
				Replace ucrsMovBanc.tipo 	WITH "Credito"
				Replace ucrsMovBanc.valor 	WITH 0
				Replace ucrsMovBanc.data 	WITH DATE()	
				**Replace ucrsMovBanc.obs 	WITH "Recebimento da Entidade:" + ALLTRIM(ucrsDadosEntidade.nome) + " referente ao ano: " + ASTR(lcANo) + ", m�s: " + ASTR(lcMes) + "."
				Replace ucrsMovBanc.obs 	WITH "Ref. Resumo Fac. A/ " + ASTR(lcMes) + "/" + ASTR(lcANo) 

			OTHERWISE 
				**
		ENDCASE
			
		**	
		DO FORM MOVBANCARIOS
		
		 MOVBANCARIOS.Pageframe1.activepage = IIF(lcOrigem == "PESQDOCASSOCIADOS_CLIENTE",1,2)

		&&Configura menu Principal
		WITH MOVBANCARIOS.menu1
			&&.adicionaOpcao("actualizar","Pesquisar",myPath + "\imagens\icons\lupa_w.png","uf_MOVBANCARIOS_actualizar with 'PESQDOCASSOCIADOS_ENTIDADE'" , "")
			.adicionaOpcao("opcoes", "Op��es", myPath + "\imagens\icons\opcoes_w.png", "","")
			.adicionaOpcao("selTodos","Sel. Todos",myPath + "\imagens\icons\unchecked_w.png","uf_MOVBANCARIOS_selTodos","")
		ENDWITH 
		
		&& Configura menu Op��es
		
		IF lcOrigem == "PESQDOCASSOCIADOS_ENTIDADE"
			MOVBANCARIOS.menu_opcoes.adicionaOpcao("imprimir", "Imprimir Conta Corrente", "", "uf_MOVBANCARIOS_imprimirCcEntidades")			
		ELSE
			MOVBANCARIOS.menu_opcoes.adicionaOpcao("imprimir", "Imprimir Conta Corrente", "", "uf_MOVBANCARIOS_imprimirCcAssociados")
		ENDIF
	ELSE
		MOVBANCARIOS.show
	ENDIF
	
ENDFUNC 


** Atualizar Dados
	FUNCTION uf_MOVBANCARIOS_actualizar
		LPARAMETERS lcOrigem
		
		uf_MOVBANCARIOS_sair()
		uf_PESQDOCASSOCIADOS_actualizar()

	ENDFUNC 


** Grava Registo de Trsnfer�ncias
FUNCTION uf_MOVBANCARIOS_gravar
	LOCAL lcSQL, lcPainel 
	STORE '' TO lcPainel 
	
	IF !uf_MOVBANCARIOS_validacoes()
		RETURN .t.
	ENDIF 
	
	SELECT ucrsMovBanc
	GO TOP 
	
	lcSQL = ""
	TEXT TO lcSQL NOSHOW textmerge
		INSERT INTO tesouraria_mov
		(	
			id
			,nome
			,no
			,dep
			,conta
			,tipo
			,valor
			,obs
			,processado
			,data
			,ousrinis
		) 
		VALUES
		(
			(select isnull(max(id),0) + 1 from tesouraria_mov)
			,'<<Alltrim(ucrsMovBanc.nome)>>'
			,<<ucrsMovBanc.no>>
			,<<ucrsMovBanc.dep>>
			,'<<Alltrim(ucrsMovBanc.conta)>>'
			,'<<Alltrim(ucrsMovBanc.tipo)>>'
			,<<ucrsMovBanc.valor>>
			,'<<ucrsMovBanc.obs>>'
			,1
			,'<<uf_gerais_getdate(ucrsMovBanc.data,"SQL")>>'
			,'<<m_chinis>>'
		)	
					
	ENDTEXT
	IF !uf_gerais_actgrelha("", "", lcSql)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA A GUARDAR O MOVIMENTO BANC�RIO. POR FAVOR CONTATE O SUPORTE", "OK",16)
		RETURN .f.
	ENDIF
	
	&&  atualiza o valor que j� foi pago na tabela de movimentos
	&& Movimentos Entidades
	SELECT * FROM uCrsMovBancDocEntidades WHERE uCrsMovBancDocEntidades.sel = .t. INTO CURSOR uCrsMovBancDocEntidadesAux READWRITE 
	
	SELECT uCrsMovBancDocEntidades 
	GO TOP 
	SCAN FOR ucrsMovBancDocEntidades.sel = .t.
		lcPainel = 'PESQDOCASSOCIADOS_ENTIDADE'
		
		lcSQL = ""
		TEXT TO lcSQL NOSHOW textmerge
			UPDATE 
				 cl_fatc_org	
			SET 
				valor_regularizado = <<(ucrsMovBancDocEntidades.valor_regularizar + ucrsMovBancDocEntidades.valor_regularizado)>>
				,recebido = <<IIF((ucrsMovBancDocEntidades.valor_regularizar + ucrsMovBancDocEntidades.valor_regularizado) == ucrsMovBancDocEntidades.valor_pagar, 1,0)>>
			where 
				cl_fatc_org.id = '<<ucrsMovBancDocEntidades.id>>'		
		ENDTEXT
		
		IF !uf_gerais_actgrelha("", "", lcSql)
			uf_perguntalt_chama("OCORREU UMA ANOMALIA A GUARDAR O MOVIMENTO BANC�RIO. POR FAVOR CONTATE O SUPORTE", "OK",16)
			RETURN .f.
		ENDIF
	ENDSCAN

	&& Movimentos Associados	
	SELECT * FROM uCrsMovBancDocAssociados WHERE uCrsMovBancDocAssociados.sel = .t. INTO CURSOR uCrsMovBancDocAssociadosAux READWRITE 
	
	SELECT uCrsMovBancDocAssociados
	GO TOP 
	SCAN FOR ucrsMovBancDocAssociados.sel = .t.
		lcPainel = 'PESQDOCASSOCIADOS_CLIENTE'

		lcSQL = ""
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			UPDATE 
				cl_fatc_org
			SET 
				valor_pago = <<(ucrsMovBancDocAssociados.valor_regularizado + ucrsMovBancDocAssociados.valor_regularizar)>>
			where 
				cl_fatc_org.id = '<<ucrsMovBancDocAssociados.cfo_id>>'
		ENDTEXT 

		IF !uf_gerais_actgrelha("", "", lcSql)
			uf_perguntalt_chama("OCORREU UMA ANOMALIA A GUARDAR O MOVIMENTO BANC�RIO. POR FAVOR CONTATE O SUPORTE", "OK",16)
			RETURN .f.
		ENDIF
	ENDSCAN
	
	&& 
	regua(2)
	uf_perguntalt_chama("Documento gravado com sucesso.","OK","",64)
	
	IF USED("uCrsMovBancDocEntidades") && recebimentos para entidades
		IF !uf_perguntalt_chama("Pretende imprimir recibo do referente ao lan�amento deste pagamento?", "Sim", "N�o")
			IF lcPainel = 'PESQDOCASSOCIADOS_ENTIDADE'
				SELECT uCrsMovBancDocEntidades
				GO TOP 
				MOVBANCARIOS.Pageframe1.Page2.GridPesq.refresh
			ELSE
				SELECT ucrsMovBancDocAssociados
				GO TOP 
				MOVBANCARIOS.Pageframe1.Page1.GridPesq.refresh
			ENDIF
			
			uf_MOVBANCARIOS_actualizar()
			
			&&uf_MOVBANCARIOS_calcvalores()
			RETURN .F.
		ENDIF
	
		&& cria cursor com a informa��o do ultimo movimento para usar na impress�o 
		SELECT uCrsMovBancDocEntidades
		GO TOP 
		
		lcSQL = ''
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			SELECT TOP 1 * FROM tesouraria_mov WHERE no = <<uCrsMovBancDocEntidades.nr_org>> order BY tesouraria_mov.id DESC 
		ENDTEXT 
		IF !uf_gerais_actgrelha("","uCrsAuxMovimento",lcSQL)
			uf_perguntalt_chama("Ocorreu uma anomalia ao gravar o movimento, por favor contacte o Suporte. Obrigado.","OK","",64)
			RETURN .f.
		ENDIF
	
		uf_imprimirgerais_Chama('RECIBOENTASSOCIADOS')
			
		uf_MOVBANCARIOS_calcvalores()
		
	ELSE
		&& recebimentos para farmacias
		uf_MOVBANCARIOS_actualizar()
		
		&&uf_MOVBANCARIOS_calcvalores()
	ENDIF	
	&& voltar para o painel anterior e atualizar
	&&uf_MOVBANCARIOS_actualizar()
ENDFUNC

**
FUNCTION uf_MOVBANCARIOS_validacoes
	SELECT ucrsMovBanc
	GO Top
	LOCATE FOR EMPTY(Alltrim(ucrsMovBanc.conta))	
	IF FOUND()
		uf_perguntalt_chama("O campo Conta � obrigat�rio.","OK","",64)
		Return .f.
	ENDIF 
	
	
	SELECT ucrsMovBanc
	GO Top
	LOCATE FOR EMPTY(ucrsMovBanc.valor)
	IF FOUND()
		uf_perguntalt_chama("O campo Valor � obrigat�rio.","OK","",64)
		Return .f.
	ENDIF 
	
	SELECT ucrsMovBanc
	GO Top
	LOCATE FOR YEAR(ucrsMovBanc.data) = 1900
	IF FOUND()
		uf_perguntalt_chama("Data invalida.","OK","",64)
		Return .f.
	ENDIF 
	
	
	**
	RETURN .t.
	
ENDFUNC 

**Funcao para seleccao de todos os registos
FUNCTION uf_MOVBANCARIOS_selTodos
	
	IF MOVBANCARIOS.Pageframe1.activepage = 2 
			LOCAL lcPos
			
			SELECT ucrsMovBancDocEntidades 
			lcPos = RECNO("ucrsMovBancDocEntidades")
			
			IF MOVBANCARIOS.menu1.selTodos.lbl.caption == "Sel. Todos"
				SELECT ucrsMovBancDocEntidades
				replace ALL ucrsMovBancDocEntidades.sel WITH .t.

				MOVBANCARIOS.menu1.selTodos.config("De-Sel. Todos", myPath + "\imagens\icons\checked_w.png", "uf_MOVBANCARIOS_selTodos")
			ELSE	
				SELECT ucrsMovBancDocEntidades 
				replace ALL ucrsMovBancDocEntidades.sel WITH .f.

				MOVBANCARIOS.menu1.selTodos.config("Sel. Todos", myPath + "\imagens\icons\unchecked_w.png", "uf_MOVBANCARIOS_selTodos")
			ENDIF
			
			uf_MOVBANCARIOS_calcvalores()
			
			SELECT ucrsMovBancDocEntidades
			TRY
				GO lcPos
			CATCH
			ENDTRY
			
	ELSE
			LOCAL lcPos
			
			SELECT ucrsMovBancDocAssociados
			lcPos = RECNO("ucrsMovBancDocAssociados")
			
			IF MOVBANCARIOS.menu1.selTodos.lbl.caption == "Sel. Todos"
				SELECT ucrsMovBancDocAssociados
				replace ALL ucrsMovBancDocAssociados.sel WITH .t.

				MOVBANCARIOS.menu1.selTodos.config("De-Sel. Todos", myPath + "\imagens\icons\checked_w.png", "uf_MOVBANCARIOS_selTodos")
			ELSE	
				SELECT ucrsMovBancDocAssociados
				replace ALL ucrsMovBancDocAssociados.sel WITH .f.

				MOVBANCARIOS.menu1.selTodos.config("Sel. Todos", myPath + "\imagens\icons\unchecked_w.png", "uf_MOVBANCARIOS_selTodos")
			ENDIF
			
			uf_MOVBANCARIOS_calcvalores()
			
			SELECT ucrsMovBancDocAssociados
			TRY
				GO lcPos
			CATCH
			ENDTRY
	
		
	ENDIF
ENDFUNC

** Funcao para calculo de Valores
FUNCTION uf_MOVBANCARIOS_calcvalores
	LOCAL lcValorMovimentoEnt, lcValorMovimentoAss, lcPosuCrs
	STORE 0 TO lcValorMovimentoEnt, lcValorMovimentoAss

	IF MOVBANCARIOS.Pageframe1.activepage = 2 
		lcPosuCrs = RECNO("ucrsMovBancDocEntidades")

		SELECT ucrsMovBancDocEntidades
		GO Top
		CALCULATE SUM(ucrsMovBancDocEntidades.valor_regularizar) FOR ucrsMovBancDocEntidades.sel = .t. TO lcValorMovimentoEnt
		
		MOVBANCARIOS.Pageframe1.page2.soma_regularizar.value = lcValorMovimentoEnt
		MOVBANCARIOS.Pageframe1.page2.soma_regularizar.refresh
		
		SELECT ucrsMovBancDocEntidades
		TRY 
			GO lcPosuCrs
		CATCH
			GO bottom 
		ENDTRY

	ELSE
		lcPosuCrs = RECNO("ucrsMovBancDocAssociados")
		SELECT ucrsMovBancDocAssociados
		GO Top
		CALCULATE SUM(ucrsMovBancDocAssociados.valor_regularizar) FOR ucrsMovBancDocAssociados.sel = .t. TO lcValorMovimentoAss
		
		MOVBANCARIOS.Pageframe1.page1.soma_regularizar.value = lcValorMovimentoAss
		MOVBANCARIOS.Pageframe1.page1.soma_regularizar.refresh
		
		SELECT ucrsMovBancDocAssociados
		TRY 
			GO lcPosuCrs
		CATCH
			GO bottom 
		ENDTRY
		
	ENDIF		
	
ENDFUNC 


** Relatorio com a informa��o de "conta corrente" das farm�cias
FUNCTION uf_MOVBANCARIOS_imprimirCcAssociados
	
	**Trata parametros
	LOCAL lcNo
	lcNo = movbancarios.no.value		

	** Construir string com parametros
	lcSql = "&no_cl=" 		+ ALLTRIM(STR(lcNo)) +;
			"&dataIni="		+ uf_gerais_getDate(movbancarios.txtDe.value, 'DATA') +;
			"&dataFim="		+ uf_gerais_getDate(movbancarios.txtA.value, 'DATA') +;
			"&site=" 		+ mysite

	** chamar report pelo nome 
	uf_gerais_chamaReport("relatorio_associados_ccAssociado", lcSql)

ENDFUNC 


** Relatorio com a informa��o de "conta corrente" das farm�cias
FUNCTION uf_MOVBANCARIOS_imprimirCcEntidades

	** Trata parametros
	LOCAL lcNo
	lcNo = movbancarios.no.value		

	** Construir string com parametros
	lcSql = "&nr_cl_org=" 	+ ALLTRIM(STR(lcNo)) +;
			"&dataIni="		+ uf_gerais_getDate(movbancarios.txtDe.value, 'DATA') +;
			"&dataFim="		+ uf_gerais_getDate(movbancarios.txtA.value, 'DATA') +;
			"&site=" 		+ mysite

	** chamar report pelo nome 
	uf_gerais_chamaReport("relatorio_associados_ccEntidade", lcSql)

ENDFUNC 


** Fecha o painel
FUNCTION uf_MOVBANCARIOS_sair
	
	&& fecha painel
	MOVBANCARIOS.hide
	MOVBANCARIOS.release

ENDFUNC


** funcao que corre no fecho do painel de impress�o p atualizar dados da grelha
FUNCTION uf_movBancarios_actDados

	&& atualiza os dados da grelha 
	IF RECCOUNT("uCrsMovBancDocEntidadesAux") > 0
		SELECT uCrsMovBancDocEntidadesAux 
		GO TOP 
		SCAN 
			DELETE FROM uCrsMovBancDocEntidades WHERE uCrsMovBancDocEntidades.id = uCrsMovBancDocEntidadesAux.id 
		ENDSCAN 
		
		IF USED("uCrsMovBancDocEntidadesAux")
			fecha("uCrsMovBancDocEntidadesAux")
		ENDIF
		
		SELECT uCrsMovBancDocEntidades
		GO TOP 
		MOVBANCARIOS.Pageframe1.Page2.GridPesq.refresh
	ELSE
		IF RECCOUNT("uCrsMovBancDocAssociadosAux") > 0
			SELECT uCrsMovBancDocAssociadosAux
			GO TOP 
			SCAN 
				DELETE FROM uCrsMovBancDocAssociados WHERE uCrsMovBancDocAssociados.cfo_id = uCrsMovBancDocAssociadosAux.cfo_id 
			ENDSCAN 
			
			IF USED("uCrsMovBancDocAssociadosAux")
				fecha("uCrsMovBancDocAssociadosAux")
			ENDIF

			SELECT ucrsMovBancDocAssociados
			GO TOP 
			MOVBANCARIOS.Pageframe1.Page1.GridPesq.refresh	
		ENDIF
	ENDIF
	**
ENDFUNC  