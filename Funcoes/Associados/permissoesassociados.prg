FUNCTION uf_permissoesassociados_chama

    IF !uf_permissoesassociados_carregaCursores()
        RETURN .F.
    ENDIF

	IF WEXIST("permissoesassociados")
		permissoesassociados.show
	ELSE
		DO FORM permissoesassociados
	ENDIF

    PUBLIC  permsAssociadosEditing

    permsAssociadosEditing = .F.

    IF WEXIST("ERROSFATURA")

        PERMISSOESASSOCIADOS.no.value = ERROSFATURA.no.value
        PERMISSOESASSOCIADOS.nome.value = ERROSFATURA.nome.value
        PERMISSOESASSOCIADOS.noEnt.value = ERROSFATURA.noEnt.value
        PERMISSOESASSOCIADOS.entidade.value = ERROSFATURA.entidade.value

        IF !EMPTY(PERMISSOESASSOCIADOS.no.value) OR !EMPTY(PERMISSOESASSOCIADOS.noEnt.value)
            uf_permissoesassociados_carregaCursores()
        ENDIF

    ENDIF

ENDFUNC

FUNCTION uf_permissoesassociados_sair

    IF !permsAssociadosEditing

	    permissoesassociados.release

    ELSE

        IF uf_perguntalt_chama("Vai cancelar a edi��o. Pretende continuar?","Sim","N�o")
            uf_permissoesassociados_mudaModoEcra()
            uf_permissoesassociados_carregaCursores()
        ENDIF

    ENDIF

ENDFUNC

FUNCTION uf_permissoesassociados_carregaCursores

    uv_idFarm = 0
    uv_idEnt = 0
    uv_bloqueadas = "0"

    regua(1,6,"A carregar dados...")

    IF WEXIST("permissoesassociados")

      loGrid = permissoesassociados.grid1
      lcCursor = "uc_perms"

	   lnColumns = loGrid.ColumnCount
	
	   IF lnColumns > 0

		   DIMENSION laControlSource[lnColumns]
		   FOR lnColumn = 1 TO lnColumns
			   laControlSource[lnColumn] = loGrid.Columns[lnColumn].ControlSource
		   ENDFOR

	   ENDIF

      loGrid.RecordSource = ""


        IF TYPE("PERMISSOESASSOCIADOS.no") <> "U"
            IF !EMPTY(PERMISSOESASSOCIADOS.no.value)
                uv_idFarm = ALLTRIM(PERMISSOESASSOCIADOS.no.value)
            ENDIF
        ENDIF

        IF TYPE("PERMISSOESASSOCIADOS.noEnt") <> "U"
            IF !EMPTY(PERMISSOESASSOCIADOS.noEnt.value)
                uv_idEnt = ALLTRIM(PERMISSOESASSOCIADOS.noEnt.value)
            ENDIF
        ENDIF

        IF TYPE("PERMISSOESASSOCIADOS.chkBloqueadas") <> "U"
            uv_bloqueadas = IIF(ALLTRIM(PERMISSOESASSOCIADOS.chkBloqueadas.tag) = 'true', "1", "0")
        ENDIF

        DO CASE
            CASE !EMPTY(uv_idFarm) AND EMPTY(uv_idEnt)
                WITH PERMISSOESASSOCIADOS
                    .GRID1.nomeScreenEnt.visible = .T.
                    .GRID1.idScreenEnt.visible = .T.
                    .GRID1.nomeScreenEnt.WIDTH = 380
                    .GRID1.nomeScreenFarm.visible = .F.
                    .GRID1.idScreenFarm.visible = .F.
                    .GRID1.nomeScreenFarm.WIDTH = 230
                    .chkSelTodosFT.left = .GRID1.nomeScreenEnt.width + .GRID1.idScreenEnt.width + 25
                    .chkSelTodosNC.left = .chkSelTodosFT.left + 55
                    .chkSelTodosND.left = .chkSelTodosNC.left + 55
                ENDWITH

            CASE EMPTY(uv_idFarm) AND !EMPTY(uv_idEnt)
                WITH PERMISSOESASSOCIADOS
                    .GRID1.nomeScreenEnt.visible = .F.
                    .GRID1.idScreenEnt.visible = .F.
                    .GRID1.nomeScreenEnt.WIDTH = 230
                    .GRID1.nomeScreenFarm.visible = .T.
                    .GRID1.idScreenFarm.visible = .T.
                    .GRID1.nomeScreenFarm.WIDTH = 380
                    .chkSelTodosFT.left = .GRID1.nomeScreenFarm.width + .GRID1.idScreenFarm.width + 25
                    .chkSelTodosNC.left = .chkSelTodosFT.left + 55
                    .chkSelTodosND.left = .chkSelTodosNC.left + 55
                ENDWITH

            CASE !EMPTY(uv_idFarm) AND !EMPTY(uv_idEnt)
                WITH PERMISSOESASSOCIADOS
                    .GRID1.nomeScreenEnt.visible = .T.
                    .GRID1.idScreenEnt.visible = .T.
                    .GRID1.nomeScreenEnt.WIDTH = 230
                    .GRID1.nomeScreenFarm.visible = .T.
                    .GRID1.idScreenFarm.visible = .T.
                    .GRID1.nomeScreenFarm.WIDTH = 230
                    .chkSelTodosFT.left = .GRID1.nomeScreenFarm.width + .GRID1.idScreenFarm.width + .GRID1.nomeScreenFarm.width + .GRID1.idScreenFarm.width + 30
                    .chkSelTodosNC.left = .chkSelTodosFT.left + 55
                    .chkSelTodosND.left = .chkSelTodosNC.left + 55
                ENDWITH

        ENDCASE

        PERMISSOESASSOCIADOS.chkSelTodosFT.click(.T.)
        PERMISSOESASSOCIADOS.chkSelTodosNC.click(.T.)
        PERMISSOESASSOCIADOS.chkSelTodosND.click(.T.)

    ENDIF

    TEXT TO msel TEXTMERGE NOSHOW
        exec up_associados_getPerms <<uv_idFarm>>, <<uv_idEnt>>, <<uv_bloqueadas>>
    ENDTEXT

    IF !uf_gerais_actgrelha("", "uc_perms", msel)
        uf_perguntalt_chama('Erro a carregar dados. Por favor contacte o suporte.', "Ok", "", 48)
        RETURN .F.
    ENDIF

    SELECT uc_perms
    GO TOP

   IF WEXIST("PERMISSOESASSOCIADOS")

	   loGrid.RecordSource = lcCursor
	
	   FOR lnColumn = 1 TO lnColumns
   		loGrid.Columns[lnColumn].ControlSource = laControlSource[lnColumn]
	   ENDFOR

       loGrid.setFocus()

   ENDIF


   regua(2)

ENDFUNC

FUNCTION uf_permissoesassociados_gravar

    IF !permsAssociadosEditing
        RETURN .F.
    ENDIF

    IF !uf_perguntalt_chama("Tem a certesza que quer gravar?","Sim","N�o")
        RETURN .F.
    ENDIF

    uv_altered = .F.

    SELECT uc_perms
    GO TOP

    SCAN FOR !EMPTY(uc_perms.stamp)

        TEXT TO msel TEXTMERGE NOSHOW
            SELECT blockInvoice as ft, blockCreditNote as nc, blockDebitNote as nd FROM ext_esb_doc_permissions(nolock) WHERE stamp = '<<ALLTRIM(uc_perms.stamp)>>'
        ENDTEXT

        IF !uf_gerais_actgrelha("", "uc_permsTmp", msel)
            uf_perguntalt_chama('Ocorreu um erro ao registar as altera��es. Por favor contacte o suporte.', "Ok", "", 48)
            RETURN .F.
        ENDIF

        IF !uc_perms.ft AND !uc_perms.nc AND !uc_perms.nd
            IF !uf_gerais_actgrelha("", "", "DELETE FROM ext_esb_doc_permissions WHERE STAMP = '" + uc_perms.stamp + "'")
                uf_perguntalt_chama('Ocorreu um erro ao registar as altera��es. Por favor contacte o suporte.', "Ok", "", 48)
                RETURN .F.
            ENDIF

            uv_altered = .T.
        ELSE

            uv_update = ""

            IF uc_perms.ft <> uc_permsTmp.ft
                uv_update = "blockInvoice = " + IIF(uc_perms.ft, "1", "0")
            ENDIF
            IF uc_perms.nc <> uc_permsTmp.nc
                uv_update = uv_update + IIF(!EMPTY(uv_update), ",", "") + "blockCreditNote = " + IIF(uc_perms.nc, "1", "0")
            ENDIF
            IF uc_perms.nd <> uc_permsTmp.nd
                uv_update = uv_update + IIF(!EMPTY(uv_update), ",", "") + "blockDebitNote = " + IIF(uc_perms.nd, "1", "0")
            ENDIF
            
            IF !EMPTY(uv_update)

                TEXT TO msel TEXTMERGE NOSHOW

                    UPDATE 
                        ext_esb_doc_permissions 
                    SET 
                        <<ALLTRIM(uv_update)>>
                        , usrinis = '<<ALLTRIM(m_chinis)>>' 
                        , usrdata = CONVERT(varchar,GETDATE(), 112)
                        , usrhora = LEFT(CONVERT(TIME,GETDATE()), 8)
                    WHERE 
                        stamp = '<<uc_perms.stamp>>'

                ENDTEXT

                IF !uf_gerais_actgrelha("", "", msel)
                    uf_perguntalt_chama('Ocorreu um erro ao registar as altera��es. Por favor contacte o suporte.', "Ok", "", 48)
                    RETURN .F.
                ENDIF

                uv_altered = .T.

            ENDIF

        ENDIF       

        FECHA("uc_permsTmp") 

        SELECT uc_perms
    ENDSCAN

    SELECT uc_perms
    GO TOP
    SCAN FOR EMPTY(uc_perms.stamp)

        IF uc_perms.ft OR uc_perms.nc OR uc_perms.nd

            uv_stamp = uf_gerais_stamp()

            TEXT TO msel TEXTMERGE NOSHOW 
                INSERT INTO ext_esb_doc_permissions
                (
                    stamp,
                    codeInfarmed,
                    codeOrg,
                    blockInvoice,
                    blockCreditNote,
                    blockDebitNote,
                    ousrinis,
                    ousrdata,
                    ousrhora,
                    usrinis,
                    usrdata,
                    usrhora
                )
                VALUES
                (
                    '<<ALLTRIM(uv_stamp)>>',
                    '<<ASTR(uc_perms.idFarm)>>',
                    '<<RIGHT('000' + ASTR(uc_perms.idEnt), 3)>>',
                    <<IIF(uc_perms.ft, "1", "0")>>,
                    <<IIF(uc_perms.nc, "1", "0")>>,
                    <<IIF(uc_perms.nd, "1", "0")>>,
                    '<<ALLTRIM(m_chinis)>>',
                    CONVERT(varchar,GETDATE(), 112),
                    LEFT(CONVERT(TIME,GETDATE()), 8),
                    '<<ALLTRIM(m_chinis)>>',
                    CONVERT(varchar,GETDATE(), 112),
                    LEFT(CONVERT(TIME,GETDATE()), 8)
                )
            ENDTEXT

            IF !uf_gerais_actgrelha("", "", msel)
                uf_perguntalt_chama('Ocorreu um erro ao registar as altera��es. Por favor contacte o suporte.', "Ok", "", 48)
                RETURN .F.
            ENDIF

            uv_altered = .T.

            SELECT uc_perms
            REPLACE uc_perms.stamp WITH ALLTRIM(uv_stamp)

        ENDIF

    ENDSCAN

    IF uv_altered

        uf_perguntalt_chama('Dados atualizados com sucesso.', "Ok", "", 64)
        uf_permissoesassociados_mudaModoEcra()
        uf_permissoesassociados_carregaCursores()

    ELSE

        uf_perguntalt_chama('N�o alterou nenhum registo!', "Ok", "", 48)
        RETURN .F.

    ENDIF

    RETURN .T.

ENDFUNC

FUNCTION uf_permissoesassociados_mudaModoEcra

    IF TYPE("permsAssociadosEditing") == "C"
        RETURN .F.
    ENDIF
    
    IF !permsAssociadosEditing

        WITH PERMISSOESASSOCIADOS

            .no.enabled = .F.
            .noEnt.enabled = .F.
            .nome.enabled = .F.
            .entidade.enabled = .F.
            .chkBloqueadas.enabled = .F.
            .grid1.ft.readOnly = .F.
            .grid1.nc.readOnly = .F.
            .grid1.nd.readOnly = .F.
            .chkSelTodosFT.enabled = .T.
            .chkSelTodosNC.enabled = .T.
            .chkSelTodosND.enabled = .T.

            .menu1.estado("actualizar, editar", "HIDE", "Gravar", .T., "Cancelar", .T.)

        ENDWITH

        permsAssociadosEditing = .T.

    ELSE

        WITH PERMISSOESASSOCIADOS

            .no.enabled = .T.
            .noEnt.enabled = .T.
            .nome.enabled = .T.
            .entidade.enabled = .T.
            .chkBloqueadas.enabled = .T.
            .grid1.ft.readOnly = .T.
            .grid1.nc.readOnly = .T.
            .grid1.nd.readOnly = .T.
            .chkSelTodosFT.enabled = .F.
            .chkSelTodosNC.enabled = .F.
            .chkSelTodosND.enabled = .F.

            .menu1.estado("actualizar, editar", "SHOW", "Gravar", .F., "Sair", .T.)

        ENDWITH

        permsAssociadosEditing = .F.

    ENDIF


ENDFUNC

