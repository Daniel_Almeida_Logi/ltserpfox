PROCEDURE uf_importCartao_chama
	LPARAMETERS uv_numEnt
	
	IF !uf_importCartao_criarCurs()
		uf_perguntalt_chama('Erro a carregar dados. Por favor contacte o suporte.', "Ok", "", 48)
		RETURN .F.
	ENDIF
	
	IF !WEXIST("IMPORTCARTAO")
		DO FORM IMPORTCARTAO WITH uv_numEnt
	ELSE
		IMPORTCARTAO.show()
	ENDIF

ENDPROC

PROCEDURE uf_importCartao_sair

	IF WEXIST("IMPORTCARTAO")
		IMPORTCARTAO.hide()
		IMPORTCARTAO.release()
	ENDIF

ENDPROC

FUNCTION uf_importCartao_criarCurs

	LOCAL uv_sql

	TEXT TO uv_sql TEXTMERGE NOSHOW
		select * from ext_cards_assoc(nolock) where 1=0
	ENDTEXT
	
	IF !uf_gerais_actGrelha("", "uc_cards_assoc", uv_sql)
		RETURN .F.
	ENDIF
	
	RETURN .T.
	
ENDFUNC

FUNCTION uf_importCartao_cartao

	IF EMPTY(IMPORTCARTAO.id_org.value)
		uf_perguntalt_chama('Tem de selecionar uma Entidade para poder importar.', "Ok", "", 48)
		RETURN .F.				
	ENDIF

	LOCAL uv_file, uv_noAssoc, uv_stamporg, uv_newStamp

	uv_noAssoc = IMPORTCARTAO.id_org.value
	uv_stamporg = uf_gerais_getUmValor("cptorg", "cptorgStamp", "u_no = " + ALLTRIM(uv_noAssoc))
	uv_file = uf_gerais_getFile(ALLTRIM(uf_gerais_getUmValor("B_terminal", "pathReport", "terminal = '" + ALLTRIM(myTerm) + "'")), 'xls')

	IF EMPTY(uv_file)
		RETURN .F.
	ENDIF

	IF ALLTRIM(UPPER(JUSTEXT(uv_file))) <> 'XLS'
		uf_perguntalt_chama('O ficheiro selecionado n�o � suportado.', "Ok", "", 48)
		RETURN .F.		
	ENDIF

	regua(1,1, "A importar ficheiro...")

	IF !UF_GERAIS_xls2Cursor(uv_file, "", "uc_cartAssocImport", .T.)
		uf_perguntalt_chama('Erro ao importar o ficheiro.', "Ok", "", 48)
		regua(2)
		RETURN .F.
	ENDIF

	SELECT uc_cartAssocImport
	GO TOP 

	IF RECCOUNT("uc_cartAssocImport") = 0
		uf_perguntalt_chama('O ficheiro importado est� vazio.', "Ok", "", 48)
		regua(2)
		RETURN .F.
	ENDIF

	LOCAL uv_camposEmFalta
	STORE '' TO uv_camposEmFalta

	SELECT uc_cartAssocImport

	IF TYPE("uc_cartAssocImport.ID_CARTAO") == "U"
		uv_camposEmFalta = 'ID_CARTAO'
	ENDIF

	IF TYPE("uc_cartAssocImport.VALIDADE") == "U"
		uv_camposEmFalta = uv_camposEmFalta + IIF(EMPTY(uv_camposEmFalta), '', ', ') + 'VALIDADE'
	ENDIF

	IF TYPE("uc_cartAssocImport.ENTIDADE") == "U"
		uv_camposEmFalta = uv_camposEmFalta + IIF(EMPTY(uv_camposEmFalta), '', ', ') + 'ENTIDADE'
	ENDIF

	IF !EMPTY(uv_camposEmFalta)
		uv_msg = IIF(AT(uv_camposEmFalta,',') <> 0, 'Os campos ', 'O campo ') + ALLTRIM(uv_camposEmFalta) + IIF(AT(uv_camposEmFalta,',') <> 0, ' n�o est�o presentes no documento selecionado!', ' n�o est� presente no documento selecionado!')
		uf_perguntalt_chama(ALLTRIM(uv_msg), "Ok", "", 48)
		RETURN .F.
	ENDIF

	regua(0, RECCOUNT("uc_cartAssocImport"), "A importar cart�es...")

	SELECT uc_cartAssocImport
	SET FILTER TO ENTIDADE = &uv_noAssoc.
	
	loGrid = IMPORTCARTAO.GRID1
	lcCursor = "uc_cards_assoc"
	lnColumns = loGrid.ColumnCount

	
	uf_importCartao_limparRegistos()
	
	
	SELECT uc_cartAssocImport
	GO TOP

	SCAN

		regua(1, recno(), "A importar cart�es...")

		uv_newStamp = uf_gerais_stamp()

		SELECT uc_cards_assoc
		APPEND BLANK

		REPLACE uc_cards_assoc.stamp WITH uv_newStamp
		REPLACE	uc_cards_assoc.cardID WITH IIF(TYPE("uc_cartAssocImport.id_cartao") = 'N', ASTR(uc_cartAssocImport.id_cartao), ALLTRIM(uc_cartAssocImport.id_cartao))
		REPLACE	uc_cards_assoc.expiryDate WITH uc_cartAssocImport.validade
		REPLACE	uc_cards_assoc.ousrinis WITH m_chinis
		REPLACE	uc_cards_assoc.cptorgStamp WITH ALLTRIM(uv_stamporg)
		REPLACE	uc_cards_assoc.ousrdata WITH DATE()

		SELECT uc_cartAssocImport
	ENDSCAN
	
	regua(2)

	SELECT uc_cards_assoc
	GO TOP
	
	IMPORTCARTAO.refresh()

	IMPORTCARTAO.grid1.AutoFit()
	IMPORTCARTAO.grid1.setFocus()

	uf_perguntalt_chama("Importado com sucesso.","Ok","",64)

	RETURN .T.

ENDFUNC

FUNCTION uf_importCartao_gravar

	IF EMPTY(IMPORTCARTAO.id_org.value)
		uf_perguntalt_chama('Tem de selecionar uma Entidade para poder gravar.', "Ok", "", 48)
		RETURN .F.				
	ENDIF

	LOCAL uv_count, uv_noAssoc, uv_stamporg, uv_insert, uv_sql, i, j, uv_insertCab, uv_addCab
	STORE '' to uv_insert, uv_insertCab

	SELECT uc_cards_assoc
	COUNT TO uv_count

	STORE .T. TO uv_addCab

	IF uv_count = 0
		uf_perguntalt_chama('N�o importou nenhum registo.', "Ok", "", 48)
		RETURN .F.				
	ENDIF

	i = 0
	j = 0
	regua(0, uv_count, "A gravar...")

	uv_noAssoc = IMPORTCARTAO.id_org.value
	uv_stamporg = uf_gerais_getUmValor("cptorg", "cptorgStamp", "u_no = " + ALLTRIM(uv_noAssoc))

	IF !uf_gerais_actGrelha("", "", "delete from ext_cards_assoc where cptorgStamp = '" + uv_stamporg + "'")
		uf_perguntalt_chama('Erro a limpar tabela.', "Ok", "", 48)
		regua(2)
		RETURN .F.				
	ENDIF

	uv_insertCab = 'INSERT INTO ext_cards_assoc (stamp, cardID, expiryDate, ousrinis, cptorgStamp, ousrdata) VALUES '

	SELECT uc_cards_assoc
	GO TOP

	SCAN

		regua(1, i, "A gravar...")

		IF uv_addCab

			uv_insert = uv_insert + IIF(!EMPTY(uv_insert), chr(13) , '') + ALLTRIM(uv_insertCab)
			uv_addCab = .F.

		ENDIF

		uv_insert = uv_insert + IIF(!EMPTY(uv_insert), chr(13) + IIF( j = 0, '', ','), '') + "(" +;
					"'" + ALLTRIM(uc_cards_assoc.stamp) + "', " +;
					"'" + ALLTRIM(uc_cards_assoc.cardID) + "', " +;
					"'" + uf_gerais_getDate(uc_cards_assoc.expiryDate, "SQL") + "', " +;
					"'" + ALLTRIM(uc_cards_assoc.ousrinis) + "', " +;
					"'" + ALLTRIM(uc_cards_assoc.cptorgStamp) + "', " +;
					"'" + uf_gerais_getDate(uc_cards_assoc.ousrdata, "SQL") + "'" +;
					")"

		i = i + 1

		j = j + 1

		IF j = 500

			j = 0
			uv_addCab = .T.

		ENDIF

		SELECT uc_cards_assoc
	ENDSCAN

	TEXT TO uv_sql TEXTMERGE NOSHOW

		<<ALLTRIM(uv_insert)>>

	ENDTEXT	


	IF !uf_gerais_actGrelha("", "", uv_sql)
		uf_perguntalt_chama('Erro ao gravar os registos. Por favor contacte o suporte.', "Ok", "", 48)
		regua(2)
		RETURN .F.				
	ENDIF

	regua(2)
	uf_perguntalt_chama("Gravado com sucesso.","Ok","",64)
	RETURN .T.

ENDFUNC

FUNCTION uf_importCartao_limparRegistos

	loGrid = IMPORTCARTAO.GRID1
	lcCursor = "uc_cards_assoc"
	lnColumns = loGrid.ColumnCount

	IF lnColumns > 0

		DIMENSION laControlSource[lnColumns]
		FOR lnColumn = 1 TO lnColumns
			laControlSource[lnColumn] = loGrid.Columns[lnColumn].ControlSource
		ENDFOR

	ENDIF

	loGrid.RecordSource = ""	
	
	SELECT uc_cards_assoc
	DELETE FOR 1=1
	
	loGrid.RecordSource = lcCursor
	
	FOR lnColumn = 1 TO lnColumns
		loGrid.Columns[lnColumn].ControlSource = laControlSource[lnColumn]
	ENDFOR

ENDFUNC