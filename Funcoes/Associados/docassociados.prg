DO PESQDOCASSOCIADOS

** Funcao chamar Painel Associados
FUNCTION uf_DOCASSOCIADOS_chama
	LPARAMETERS Lcid
	PUBLIC myDocAssociadosIntroducao, myDocAssociadosAlteracao 
	
	**Valida Licenciamento
	IF (uf_ligacoes_addConnection('ASS') == .f.)
		RETURN .F.
	ENDIF

	IF TYPE("DOCASSOCIADOS") == "U"
				
		uf_docassociados_criacursor(Lcid)
	
		DO FORM DOCASSOCIADOS
		
		&&Configura Menu Barra Lateral
		WITH DOCASSOCIADOS.menu1
			 .adicionaOpcao("aplicacoes", "Aplica��es", myPath + "\imagens\icons\aplicacoes_w.png", "uf_gerais_menuAplicacoes With 'DOCASSOCIADOS'","O")	
			 .adicionaOpcao("opcoes","Op��es", myPath + "\imagens\icons\opcoes_w.png","","O")
			 .adicionaOpcao("pesquisar","Pesquisar",myPath + "\imagens\icons\lupa_w.png","uf_PESQDOCASSOCIADOS_chama with 'DOCASSOCIADOS'","P")
			 .adicionaOpcao("novo","Novo",myPath + "\imagens\icons\mais_w.png","uf_DOCASSOCIADOS_novoDoc","N")
			 .adicionaOpcao("editar","Editar",myPath + "\imagens\icons\lapis_w.png","uf_DOCASSOCIADOS_alterarDoc","E")
			 .adicionaOpcao("anterior","Anterior",myPath + "\imagens\icons\ponta_seta_left_w.png","uf_DOCASSOCIADOS_registoanterior","19")
			 .adicionaOpcao("seguinte","Seguinte",myPath + "\imagens\icons\ponta_seta_rigth_w.png","uf_DOCASSOCIADOS_registoseguinte","04")
       		 .adicionaOpcao("novaLinha","Nova Linha",myPath + "\imagens\icons\mais_w.png","uf_DOCASSOCIADOS_adicionaLinha","N")
			 .adicionaOpcao("eliminarLinha","Eliminar Linha",myPath + "\imagens\icons\cruz_w.png","uf_DOCASSOCIADOS_eliminaLinha","E")
		ENDWITH 

		&&Configura Menu Aplica��es
		DOCASSOCIADOS.menu_aplicacoes.adicionaOpcao("calculadora", "Calculadora", "", "uf_calculadora_chama")				

		&&Configura Menu Op��es
		WITH DOCASSOCIADOS.menu_opcoes
			 .adicionaOpcao("imprimir","Imprimir","","uf_imprimirgerais_Chama with 'ASSOCIADOS'")
			 .adicionaOpcao("eliminar","Eliminar","","uf_DOCASSOCIADOS_eliminarDoc")
		ENDWITH
	
	ELSE
		DOCASSOCIADOS.show()
	ENDIF			
	
	IF !EMPTY(LcId)
		TEXT TO lcSQL TEXTMERGE NOSHOW
	    	 SET language portuguese
	    	 
	    	 Select 
	    	 	cfo.id, cfo.id_cl
				,cfo.ano, cfo.mes, cfo.nrDoc
				,cfo.valor, cfo.valor_ret, cfo.valor_pagar
				,cfo.data, cfo.data_alt
	    	 	,Clientes.no, Clientes.nome, Clientes.estab, Clientes.id as IdCliente
				,Entidades.no, Entidades.nome,Entidades.morada, Entidades.codpost, Entidades.ncont, Entidades.nome2
				,referencia = upper(left(datename(month, DateAdd(month , mes-1 , 0)),3)) +  + '/' + convert(varchar,ano)
	    	 from
	    	 	cl_fatc_org cfo (nolock)
	    	 	left join B_utentes as Clientes on Clientes.id = cfo.id_cl
				left join B_utentes as Entidades on Entidades.no = cfo.nr_cl_org
	       	 WHERE 
	       	 	cfo.id = <<IIF(EMPTY(Lcid),'NULL',"'"+Lcid+"'")>>
	    	 
	    	 SET language english	    	  
	   	ENDTEXT  
 	
	   	IF !uf_gerais_actGrelha("", "uCrsDocAssociados", lcSQL)
	   		uf_perguntalt_chama("OCORREU UMA ANOMALIA NA PROCURA DO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		   	RETURN .f.
	   	ENDIF

	ENDIF
	
ENDFUNC


** Funcao criar cursores
FUNCTION uf_docassociados_criacursor
	LPARAMETERS Lcid

	TEXT TO lcSQL TEXTMERGE NOSHOW
    	 Select 
    	 	cl_fatc_org.id, cl_fatc_org.id_cl, cl_fatc_org.nr_cl_org, cl_fatc_org.ano
    	 	, cl_fatc_org.mes, cl_fatc_org.nrDoc, cl_fatc_org.valor, cl_fatc_org.valor_ret
    	 	, cl_fatc_org.valor_pagar, cl_fatc_org.data, cl_fatc_org.data_alt
    	 	, b_utentes.no, b_utentes.nome, b_utentes.estab, b_utentes.id as IdCliente
    	 from
    	 	cl_fatc_org (nolock)
    	 	left join B_utentes as Clientes on Clientes.id = cl_fatc_org.id_cl
			left join B_utentes as Entidades on Entidades.no = cl_fatc_org.nr_cl_org
       	 WHERE 
       	 	cl_fatc_org.id = <<IIF(EMPTY(Lcid),'NULL',"'"+Lcid+"'")>>
	ENDTEXT  
 	
   	IF !uf_gerais_actGrelha("", "uCrsDocAssociados", lcSQL)
   		uf_perguntalt_chama("OCORREU UMA ANOMALIA NA PROCURA DO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
	   	RETURN .f.
   	ENDIF

	**SELECT * FROM uCrsDocAssociados WHERE uCrsDocAssociados.id = lcid INTO CURSOR uCrsDocAssociadoslinhas READWRITE
	
ENDFUNC


** Funcao alternar menu dependendo do estado do documento
FUNCTION uf_DOCASSOCIADOS_alternaMenu
	** Em modo de Leitura
	IF myDocAssociadosIntroducao == .f. AND myDocAssociadosAlteracao == .f.
	
		&&Configura Menu Barra Lateral
		DOCASSOCIADOS.menu1.estado("aplicacoes, opcoes, pesquisar, novo, editar, anterior, seguinte", "SHOW", "Gravar", .f., "Sair", .t.)
		DOCASSOCIADOS.menu1.estado("novaLinha, eliminarLinha", "HIDE")
		
		&&Configura Menu Op��es
		DOCASSOCIADOS.menu_opcoes.estado("eliminar, imprimir", "SHOW")
	
	ELSE
		
		SELECT uCrsDocAssociados 
		IF EMPTY(uCrsDocAssociados.nrdoc)

			&&Configura Menu Barra Lateral
			DOCASSOCIADOS.Menu1.estado("aplicacoes, pesquisar, novo, editar, anterior, seguinte ", "HIDE", "Gravar", .t., "Cancelar", .t.)
			
			&&Configura Menu Op��es
			DOCASSOCIADOS.menu_opcoes.estado("eliminar, imprimir", "HIDE")
		ELSE
			&&Configura Menu Barra Lateral
			DOCASSOCIADOS.Menu1.estado("aplicacoes, pesquisar, novo, editar, anterior, seguinte ", "HIDE", "Gravar", .t., "Cancelar", .t.)
			
			&&Configura Menu Op��es
			DOCASSOCIADOS.menu_opcoes.estado("eliminar, imprimir", "HIDE")
			
		ENDIF
	ENDIF
ENDFUNC

