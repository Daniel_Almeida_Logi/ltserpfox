**
DO pesqconvencoes

**
FUNCTION uf_convencoes_chama
	LPARAMETERS lcId, lcOrigem
	
	**
	IF !uf_gerais_validaPermUser(m.ch_userno, m.ch_grupo, 'Gest�o de Conven��es')
		uf_perguntalt_chama("O seu perfil n�o permite aceder � Gest�o de Conven��es.", "OK", "", 64)
		RETURN	.f.
	ENDIF
	
	**	
	IF EMPTY(lcId)
		lcId = 0
	ENDIF 
	
	PUBLIC myConvencoesIntroducao, myConvencoesAlteracao 
	
	IF TYPE("CONVENCOES") == "U"
	
		uf_convencoes_cursoresIniciais(lcID)
	
		DO FORM CONVENCOES WITH lcOrigem

	ELSE
		
		**
		uf_convencoes_actualizaDados(lcID)
		
		CONVENCOES.show
	ENDIF
		
		
	uf_convencoes_alternaMenu()
ENDFUNC 

**
FUNCTION uf_convencoes_alternaMenu
	
	WITH CONVENCOES.menu1	

		
		DO CASE 
			CASE EMPTY(CONVENCOES.Origem) && Painel de Conven��es
				IF myConvencoesIntroducao == .t. OR myConvencoesAlteracao == .t.
					
					.estado("eliminarLinha, pesquisarStock", "SHOW", "Gravar", .t., "Cancelar", .t.)
					.estado("opcoes, pesquisar, novo,eliminarConvencao, editar", "HIDE")
					
					WITH convencoes
						.local.disabledBackcolor = RGB(191,223,223)
						.convencao.readonly = .f.
						.Entidade.disabledBackcolor = RGB(191,223,223)
						.Entidadeno.disabledBackcolor = RGB(191,223,223)	
					ENDWITH 
					
					
					CONVENCOES.ChkvDtCartao.enable(.t.)
					CONVENCOES.chkinactivo.enable(.t.)
					
					
					IF CONVENCOES.PageFrame1.ActivePage == 1
						.pesquisarStock.config("Nova Linha",myPath + "\imagens\icons\lupa_w.png","uf_convencoes_pesquisarServicos","P")
						.eliminarLinha.config("Eliminar Linha", myPath + "\imagens\icons\cruz_w.png", "uf_convencoes_eliminarLinha","E")
					ELSE
						.pesquisarStock.config("Novo Esp.",myPath + "\imagens\icons\lupa_w.png","uf_convencoes_pesquisarUs","P")
						.eliminarLinha.config("Eliminar Esp.", myPath + "\imagens\icons\cruz_w.png", "uf_convencoes_eliminarUs","E")
					ENDIF 
					
				ELSE 
					.estado("opcoes, pesquisar, novo,eliminarConvencao,  editar", "SHOW", "Gravar", .f., "Sair", .t.)
					.estado("eliminarLinha, pesquisarStock", "HIDE")
					
					
					CONVENCOES.ChkvDtCartao.enable(.f.)
					CONVENCOES.chkinactivo.enable(.f.)
					
					WITH convencoes
						.local.disabledBackcolor = RGB(255,255,255)
						.convencao.readonly = .t.
						.Entidade.disabledBackcolor = RGB(255,255,255)
						.Entidadeno.disabledBackcolor = RGB(255,255,255)	
					ENDWITH 

				ENDIF 
				
				
			CASE CONVENCOES.Origem == "CONFIGHONORARIOS" && Painel de Importa��o de Conven��es
				.estado("opcoes, pesquisar, novo,eliminarConvencao,  editar, eliminarLinha, pesquisarStock", "HIDE", "Importar", .t., "Sair", .t.)
				WITH convencoes
					.local.disabledBackcolor = RGB(255,255,255)
					.convencao.readonly = .t.
					.Entidade.disabledBackcolor = RGB(255,255,255)
					.Entidadeno.disabledBackcolor = RGB(255,255,255)	
				ENDWITH 
				
				
			

				
		ENDCASE 
	ENDWITH
	
	
*!*		WITH convencoes.PageFrame1.Page1.gridpesq
*!*			FOR i=1 TO .columnCount
*!*				
*!*				IF EMPTY(convencoes.Origem )	
*!*					.Columns(i).visible =  .f.			
*!*				ELSE
*!*					
*!*					IF convencoes.Origem != "CONFIGHONORARIOS" AND .Columns(i).name == "sel"
*!*						.Columns(i).visible =  .f.	
*!*					ENDIF 
*!*				ENDIF 			
*!*			ENDFOR
*!*		ENDWITH 
	
	CONVENCOES.refresh

ENDFUNC


**
FUNCTION uf_convencoes_cursoresIniciais
	LPARAMETERS lcId
	
	LOCAL lcSql 
	lcSql = ""
	
	&& Cursor de Dependentes
	TEXT TO lcSql NOSHOW textmerge
		exec up_convencoes_cab <<lcId>>
	ENDTEXT
	IF !uf_gerais_actgrelha("", "uCrsConvencoesCab", lcSql)
		RETURN .f.
	ENDIF
	
	&& Cursor de Dependentes
	TEXT TO lcSql NOSHOW textmerge
		exec up_convencoes_lin <<lcId>>
	ENDTEXT
	IF !uf_gerais_actgrelha("", "uCrsConvencoesLin", lcSql)
		RETURN .f.
	ENDIF
	
	
	&& Cursor de utilizadores
	TEXT TO lcSql NOSHOW textmerge
		exec up_convencoes_us <<lcId>>
	ENDTEXT
	IF !uf_gerais_actgrelha("", "uCrsConvencoesUs", lcSql)
		RETURN .f.
	ENDIF
ENDFUNC 


**
FUNCTION uf_convencoes_actualizaDados
	LPARAMETERS lcId
	
	IF EMPTY(lcId)
		lcId = 0
	ENDIF 
	
	LOCAL lcSql 
	lcSql = ""
	
	&& Cursor de Dependentes
	TEXT TO lcSql NOSHOW textmerge
		exec up_convencoes_cab <<lcId>>
	ENDTEXT
	IF !uf_gerais_actgrelha("", "uCrsConvencoesCab", lcSql)
		RETURN .f.
	ENDIF
	
	&& Cursor de Dependentes
	TEXT TO lcSql NOSHOW textmerge
		exec up_convencoes_lin <<lcId>>
	ENDTEXT
	IF !uf_gerais_actgrelha("convencoes.PageFrame1.Page1.gridpesq", "uCrsConvencoesLin", lcSql)
		RETURN .f.
	ENDIF
	
	&& Cursor de utilizadores
	TEXT TO lcSql NOSHOW textmerge
		exec up_convencoes_us <<lcId>>
	ENDTEXT
	IF !uf_gerais_actgrelha("convencoes.PageFrame1.Page2.gridpesq", "uCrsConvencoesUs", lcSql)
		RETURN .f.
	ENDIF
ENDFUNC 


**
FUNCTION uf_convencoes_novo
	myConvencoesIntroducao = .t.
	myConvencoesAlteracao = .f.
	
	Select uCrsConvencoesLin
	SET FILTER TO 
	
	SELECT ucrsConvencoesCab
	GO Top
	SCAN
		DELETE 
	ENDSCAN 
	SELECT ucrsConvencoesLin
	GO Top
	SCAN
		DELETE 
	ENDSCAN 
	SELECT uCrsConvencoesUs
	GO Top
	SCAN
		DELETE 
	ENDSCAN 
	
	
	
	TEXT TO lcSQL TEXTMERGE NOSHOW 
		SELECT ISNULL(MAX(id),0) as maxid FROM cpt_conv
	ENDTEXT 
	IF !uf_gerais_actgrelha("", "uCrsConvencoesMaxID", lcSql)
		RETURN .f.
	ENDIF
		
	
	SELECT ucrsConvencoesCab
	APPEND BLANK
	Replace ucrsConvencoesCab.site WITH mysite 
	Replace uCrsConvencoesCab.id WITH uCrsConvencoesMaxID.maxid + 1
	uf_convencoes_alternaMenu()
ENDFUNC 


** 
FUNCTION uf_convencoes_eliminarConv

	IF !uf_perguntalt_chama("Vai eliminar a Conven��o. Pretende continuar?", "Sim", "N�o", 32)
		RETURN .f.
	ENDIF

	TEXT TO lcSQL TEXTMERGE NOSHOW 
		delete from cpt_conv WHERE cpt_conv.id = <<uCrsConvencoesCab.id>>
		delete from cpt_val_cli WHERE cpt_val_cli.id_cpt_conv = <<uCrsConvencoesCab.id>>
		delete from cpt_conv_us WHERE cpt_conv_us.id_cpt_conv = <<uCrsConvencoesCab.id>>
	ENDTEXT 
	IF !uf_gerais_actgrelha("", "", lcSql)
		RETURN .f.
	ENDIF
	
	uf_convencoes_chama(0)
	
ENDFUNC 

**
FUNCTION uf_convencoes_editar
	convencoes.PageFrame1.Page1.gridpesq.refresh

	myConvencoesIntroducao = .f.
	myConvencoesAlteracao = .t.
	uf_convencoes_alternaMenu()
ENDFUNC 


**
FUNCTION uf_convencoes_eliminarlinha
	SELECT ucrsConvencoesLin
	DELETE 
	
	SELECT ucrsConvencoesLin
	TRY
		SKIP -1
	CATCH 
		SKIP 1
	ENDTRY
	
	convencoes.PageFrame1.Page1.gridpesq.refresh
	convencoes.PageFrame1.Page1.gridpesq.setfocus
		
ENDFUNC 


**
FUNCTION uf_convencoes_pesquisarUs
	uf_pesqutilizadores_chama("CONVENCOES")
ENDFUNC 



**
FUNCTION uf_convencoes_eliminarUs
	SELECT ucrsConvencoesUs
	DELETE 
	
	SELECT ucrsConvencoesUs
	TRY
		SKIP -1
	CATCH 
		SKIP 1
	ENDTRY
	
	convencoes.PageFrame1.Page2.gridpesq.refresh
	convencoes.PageFrame1.Page2.gridpesq.setfocus
ENDFUNC 


**
Function uf_convencoes_pesquisarServicos
	uf_pesqstocks_chama('CONVENCAO')
ENDFUNC 


**
FUNCTION uf_convencoes_gravar
	LOCAL lcSQL 
	
	
	
	IF CONVENCOES.Origem == "CONFIGHONORARIOS"
	
		uf_CONFIGHONORARIOS_importaConvencaoSel()

	ELSE
		
		
		Select uCrsConvencoesLin
		SET FILTER TO 
		Select uCrsConvencoesLin
		GO Top
		
		IF !uf_convencoes_regrasGravar()
			RETURN .f.
		ENDIF 
		
		
		IF myConvencoesIntroducao = .t.
		
			lcSqlInsercaoCab = uf_convencoes_GravaCpt_conv()
			lcSqlInsercaoCab = uf_gerais_trataPlicasSQL(lcSqlInsercaoCab) 

			lcSqlInsercaoLin = uf_convencoes_GravaCpt_val_cli()
			lcSqlInsercaoLin = uf_gerais_trataPlicasSQL(lcSqlInsercaoLin) 
			
			lcSqlInsercaoUs = uf_convencoes_GravaCptConv_US()
			lcSqlInsercaoUs = uf_gerais_trataPlicasSQL(lcSqlInsercaoUs) 
				
			IF !EMPTY(lcSqlInsercaoCab) And !EMPTY(lcSqlInsercaoLin) 

				lcSql = ''
*!*					TEXT TO lcSQL NOSHOW TEXTMERGE
*!*						exec up_gerais_execSql '<<lcSqlInsercaoCab>>', '<<lcSqlInsercaoLin>>', '<<lcSqlInsercaoUs>>', '', '', ''
*!*					ENDTEXT 
				lcSQLInsertDoc = lcSqlInsercaoCab + CHR(13) + lcSqlInsercaoLin + CHR(13) + lcSqlInsercaoUs
				TEXT TO lcSQL NOSHOW TEXTMERGE
					exec up_gerais_execSql 'Conven��es - Insert', 1,'<<lcSQLInsertDoc>>', '', '', '' , '', ''
				ENDTEXT 

				If !uf_gerais_actGrelha("","",lcSQL)
					uf_perguntalt_chama("OCORREU UM ERRO A GRAVAR A INFORMA��O DA CONVEN��O. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
					Return .f.
				Endif

			ENDIF 

		ELSE && ALtera��o
			
			lcSqlAtualizacaoCab = uf_convencoes_GravaCpt_conv()
			lcSqlAtualizacaoCab = uf_gerais_trataPlicasSQL(lcSqlAtualizacaoCab) 

			lcSqlActualizacaoLin = uf_convencoes_GravaCpt_val_cli()
			lcSqlActualizacaoLin = uf_gerais_trataPlicasSQL(lcSqlActualizacaoLin ) 
		
			lcSqlActualizacaoUs = uf_convencoes_GravaCptConv_US()
			lcSqlActualizacaoUs = uf_gerais_trataPlicasSQL(lcSqlActualizacaoUs) 
						
			IF !EMPTY(lcSqlAtualizacaoCab) And !EMPTY(lcSqlActualizacaoLin) 

				lcSql = ''
					TEXT TO lcSQL NOSHOW TEXTMERGE
						exec up_gerais_execSql 'Convencoes - update', 1,'<<lcSqlAtualizacaoCab>>', '<<lcSqlActualizacaoLin>>', '<<lcSqlActualizacaoUs>>', '', '', ''
					ENDTEXT 
*!*					lcSQLInsertDoc = lcSqlInsercaoCab + CHR(13) + lcSqlInsercaoLin + CHR(13) + lcSqlInsercaoUs
*!*					TEXT TO lcSQL NOSHOW TEXTMERGE
*!*						exec up_gerais_execSql 'Conven��es - Insert', 1,'<<lcSQLInsertDoc>>', '', '', '' , '', ''
*!*					ENDTEXT 
	*!*	_CLIPTEXT = lcSQL
	*!*	MESSAGEBOX(lcSQL)
				If !uf_gerais_actGrelha("","",lcSQL)
					uf_perguntalt_chama("OCORREU UM ERRO A GRAVAR A INFORMA��O DA CONVEN��O. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
					Return .f.
				Endif

			ENDIF 
		
		
		
		ENDIF 	
		
		myConvencoesIntroducao = .f.
		myConvencoesAlteracao = .f.
		
		uf_convencoes_alternaMenu()

		SELECT uCrsConvencoesLin
		GO Top
	ENDIF 
ENDFUNC 


**
FUNCTION uf_convencoes_regrasGravar
	Local lcRegistosLin
	lcRegistosLin = 0

	SELECT uCrsConvencoesCab
	IF EMPTY(uCrsConvencoesCab.site) OR EMPTY(uCrsConvencoesCab.descr) OR EMPTY(uCrsConvencoesCab.entidade)
		uf_perguntalt_chama("Existem campo obrigat�rios por preencher.","OK","",64)
		RETURN .f.
	ENDIF 
	
	SELECT ucrsConvencoesLin
	COUNT TO lcRegistosLin
	IF lcRegistosLin == 0
		uf_perguntalt_chama("N�o � possivel gravar uma conven��o sem linhas.","OK","",64)
		RETURN .f.
	ENDIF 
	
	RETURN .t.
ENDFUNC 



**
FUNCTION uf_convencoes_GravaCpt_conv
	LOCAL lcExecuteSQL, lcSQL, lcCabId 
	lcCabId = 0
	lcExecuteSQL = ""
	Select uCrsConvencoesCab
	lcCabId = uCrsConvencoesCab.id
	
	IF myConvencoesIntroducao = .t.
		lcSQL = ""
		TEXT TO lcSQL NOSHOW textmerge
			INSERT INTO cpt_conv(
				id
				,descr
				,id_entidade
				,site
				,inativo
				,data_cri
				,data_alt
				,id_us
				,id_us_alt
				,vDtCartao
			)values (
				<<uCrsConvencoesCab.id>>
				,'<<ALLTRIM(uCrsConvencoesCab.descr)>>'
				,<<uCrsConvencoesCab.EntidadeNo>>
				,'<<ALLTRIM(uCrsConvencoesCab.site)>>'
				,<<IIF(EMPTY(uCrsConvencoesCab.inativo),0,1)>>
				,dateadd(HOUR, <<difhoraria>>, getdate())
				,dateadd(HOUR, <<difhoraria>>, getdate())
				,<<ch_userno>>
				,<<ch_userno>>
				,<<IIF(EMPTY(uCrsConvencoesCab.vDtCartao),0,1)>>
			)
		ENDTEXT	
	ELSE && Altera��o
	
		SELECT uCrsConvencoesCab
		lcSQL = ""
		TEXT TO lcSQL NOSHOW textmerge
			UPDATE 
				cpt_conv
			SET 
				descr = '<<ALLTRIM(uCrsConvencoesCab.descr)>>'
				,id_entidade  =<<uCrsConvencoesCab.EntidadeNo>>
				,site = '<<ALLTRIM(uCrsConvencoesCab.site)>>'
				,inativo = <<IIF(EMPTY(uCrsConvencoesCab.inativo),0,1)>>
				,data_alt = dateadd(HOUR, <<difhoraria>>, getdate())
				,id_us_alt = <<ch_userno>>
				,vDtCartao = <<IIF(EMPTY(uCrsConvencoesCab.vDtCartao),0,1)>>
			Where
				Id = <<lcCabId>>
		ENDTEXT	
	ENDIF 
	lcExecuteSQL  = lcExecuteSQL + CHR(13) + CHR(13) + lcSQL
		
	RETURN lcExecuteSQL
ENDFUNC


**
FUNCTION uf_convencoes_GravaCpt_val_cli
	LOCAL lcExecuteSQL, lcSQL
	lcExecuteSQL = ""

	IF myConvencoesIntroducao = .t.	
		Select uCrsConvencoesLin
		GO TOP
		SCAN
			lcSQL = ""
			TEXT TO lcSQL NOSHOW textmerge
				INSERT INTO cpt_val_cli (
					id
					,id_entidade
					,ref
					,compart
					,maximo
					,tipoCompart
					,obs
					,inativo
					,refEntidade
					,designEntidade
					,site
					,id_cpt_conv
					,pvp
					
				)values (
					'<<ALLTRIM(uCrsConvencoesLin.id)>>'
					,<<uCrsConvencoesCab.entidadeno>>
					,'<<ALLTRIM(uCrsConvencoesLin.ref)>>'
					,<<uCrsConvencoesLin.compart>>
					,<<uCrsConvencoesLin.maximo>>
					,'<<ALLTRIM(uCrsConvencoesLin.tipoCompart)>>'
					,'<<ALLTRIM(uCrsConvencoesLin.obs)>>'
					,<<IIF(uCrsConvencoesLin.inativo,1,0)>>
					,'<<ALLTRIM(uCrsConvencoesLin.refEntidade)>>'
					,'<<strtran(ALLTRIM(uCrsConvencoesLin.designEntidade),chr(39),'')>>'
					,'<<ALLTRIM(uCrsConvencoesCab.site)>>'
					,<<uCrsConvencoesCab.id>>
					,<<uCrsConvencoesLin.pvp>>
				)
			ENDTEXT
			lcExecuteSQL  = lcExecuteSQL + CHR(13) + CHR(13) + lcSQL
		ENDSCAN

	ELSE && Altera��o
	
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			DELETE from cpt_val_cli WHERE id_cpt_conv = <<uCrsConvencoesCab.id>>
		ENDTEXT 
		
		lcExecuteSQL  = lcExecuteSQL + CHR(13) + CHR(13) + lcSQL
		
		Select uCrsConvencoesLin
		GO TOP
		SCAN
			lcSQL = ""
			TEXT TO lcSQL NOSHOW TEXTMERGE 
				INSERT INTO cpt_val_cli (
					id
					,id_entidade
					,ref
					,compart
					,maximo
					,tipoCompart
					,obs
					,inativo
					,refEntidade
					,designEntidade
					,site
					,id_cpt_conv
					,pvp
				)values (
					'<<ALLTRIM(uCrsConvencoesLin.id)>>'
					,<<uCrsConvencoesCab.entidadeno>>
					,'<<ALLTRIM(uCrsConvencoesLin.ref)>>'
					,<<uCrsConvencoesLin.compart>>
					,<<uCrsConvencoesLin.maximo>>
					,'<<ALLTRIM(uCrsConvencoesLin.tipoCompart)>>'
					,'<<ALLTRIM(uCrsConvencoesLin.obs)>>'
					,<<IIF(uCrsConvencoesLin.inativo,1,0)>>
					,'<<ALLTRIM(uCrsConvencoesLin.refEntidade)>>'
					,'<<ALLTRIM(uCrsConvencoesLin.designEntidade)>>'
					,'<<ALLTRIM(uCrsConvencoesCab.site)>>'
					,<<uCrsConvencoesCab.id>>
					,<<uCrsConvencoesLin.pvp>>
				)
			ENDTEXT
			lcExecuteSQL  = lcExecuteSQL + CHR(13) + CHR(13) + lcSQL
		ENDSCAN
		
	ENDIF 
	
	
	RETURN lcExecuteSQL
ENDFUNC


**
FUNCTION uf_convencoes_GravaCptConv_US
	LOCAL lcExecuteSQL, lcSQL,lcId
	lcExecuteSQL = ""

	SELECT uCrsConvencoesCab
	lcId = uCrsConvencoesCab.id

	IF myConvencoesIntroducao = .t.	
		Select uCrsConvencoesUs
		GO TOP
		SCAN
			lcSQL = ""
			TEXT TO lcSQL NOSHOW textmerge
				INSERT INTO cpt_conv_us(
					id_cpt_conv
					,id_us
				)values (
					<<lcId>>
					,<<uCrsConvencoesUs.no>>
					
				)
			ENDTEXT
			lcExecuteSQL  = lcExecuteSQL + CHR(13) + CHR(13) + lcSQL
		ENDSCAN

	ELSE && Altera��o
	
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			DELETE from cpt_conv_us WHERE id_cpt_conv = <<lcId >>
		ENDTEXT 
		
		lcExecuteSQL  = lcExecuteSQL + CHR(13) + CHR(13) + lcSQL
		
		Select uCrsConvencoesUs
		GO TOP
		SCAN
			lcSQL = ""
			TEXT TO lcSQL NOSHOW TEXTMERGE 
				INSERT INTO cpt_conv_us(
					id_cpt_conv
					,id_us
				)values (
					<<lcId>>
					,<<uCrsConvencoesUs.no>>
				)
			ENDTEXT
			lcExecuteSQL  = lcExecuteSQL + CHR(13) + CHR(13) + lcSQL
		ENDSCAN
		
	ENDIF 
	
	
	RETURN lcExecuteSQL
ENDFUNC



**
FUNCTION uf_convencoes_SelTipoCompart
	LPARAMETERS lcObj

	IF myConvencoesIntroducao == .f. AND myConvencoesAlteracao == .f.
		RETURN .f.
	ENDIF


	LOCAL lcSQL
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_utentes_TiposCompart
	ENDTEXT
	If !uf_gerais_actGrelha("", "ucrsTempTipoCompart", lcSql)
		uf_perguntalt_chama("N�o foi possivel encontrar regitos.", "", "OK", 16)
		RETURN .f.
	ENDIF
	uf_valorescombo_chama(lcObj, 1, "ucrsTempTipoCompart", 2, "tipocompart", "tipocompart")
		

ENDFUNC


**
FUNCTION uf_CONVENCOES_Filtro
	LOCAL lcFiltro

	STORE '' TO lcFiltro
	
	Select uCrsConvencoesLin
	SET FILTER TO 
	
	** Construir express�o do filtro
	&& Especialista
	IF !EMPTY(ALLTRIM(UPPER(CONVENCOES.especialidade.value))) 
	
		IF !EMPTY(ALLTRIM(lcFiltro))
			lcFiltro = lcFiltro + [AND '] + ALLTRIM(UPPER(CONVENCOES.especialidade.value)) + [' $ ALLTRIM(UPPER(especialidade))]
		ELSE
			lcFiltro = ['] + ALLTRIM(UPPER(CONVENCOES.especialidade.value)) + [' $ ALLTRIM(UPPER(especialidade))]
		ENDIF
	
	ENDIF
	

	** aplicar filtro
	SELECT uCrsConvencoesLin
	GO top
	IF !EMPTY(ALLTRIM(lcFiltro))
		SELECT uCrsConvencoesLin
		SET FILTER TO &lcFiltro
	ELSE
		SELECT uCrsConvencoesLin
		SET FILTER TO 
	ENDIF

	SELECT uCrsConvencoesLin
	GO top

	convencoes.PageFrame1.Page1.gridpesq.refresh

ENDFUNC 



** Cancelar
FUNCTION uf_CONVENCOES_sair

	* Verifica se o Documento esta em Modo de Edicao ou Introducao
	If myConvencoesIntroducao == .t. OR myConvencoesAlteracao == .t.
		
		IF myConvencoesIntroducao 
			
			&& navega para ultimo registo
			myConvencoesIntroducao = .f.
			myConvencoesAlteracao = .f.

			uf_convencoes_chama()
		
		ELSE && Altera��o
		
			myConvencoesIntroducao = .f.
			myConvencoesAlteracao = .f.

			uf_convencoes_chama(uCrsConvencoesCab.id)
			
		ENDIF
	
		uf_convencoes_alternaMenu()
		
	ELSE
		uf_CONVENCOES_exit()
	Endif
ENDFUNC 


**
FUNCTION uf_CONVENCOES_exit
	CONVENCOES.hide
	CONVENCOES.release
ENDFUNC

