
**
FUNCTION uf_pesqconvencoes_chama
	LPARAMETERS lcOrigem
	
	**
	IF !uf_gerais_validaPermUser(m.ch_userno, m.ch_grupo, 'Gest�o de Conven��es')
		uf_perguntalt_chama("O seu perfil n�o permite aceder � Gest�o de Conven��es.", "OK", "", 64)
		RETURN	.f.
	ENDIF
	
	
	IF TYPE("PESQCONVENCOES") == "U"
	
		IF !USED("uCrsPesqConvencoes")
			uf_pesqconvencoes_cursoresIniciais()
		ENDIF 
		
		DO FORM PESQCONVENCOES
				
			&& Configura menu principal
		WITH PESQCONVENCOES.menu1
			.adicionaOpcao("opcoes","Op��es",myPath + "\imagens\icons\opcoes_w.png","","O")
			.adicionaOpcao("actualizar", "Pesquisar", myPath + "\imagens\icons\actualizar_w.png", "uf_pesqconvencoes_actualizar","A")
			.adicionaOpcao("novaConvecao","Novo",myPath + "\imagens\icons\mais_w.png","uf_pesqconvencoes_novaConvencao","N")
		ENDWITH
		
	ELSE
		PESQCONVENCOES.show
	ENDIF
	
	IF !EMPTY(lcOrigem)
		PESQCONVENCOES.origem = lcOrigem
	ENDIF 
ENDFUNC 


** 
FUNCTION uf_pesqconvencoes_cursoresIniciais
	LOCAL lcSql 
	lcSql = ""
	
	&& Cursor de Dependentes
	TEXT TO lcSql NOSHOW textmerge
		 exec up_convencoes_pesquisa 30,'', '', '', 0
	ENDTEXT
	IF !uf_gerais_actgrelha("", "uCrsPesqConvencoes", lcSql)
		RETURN .f.
	ENDIF


ENDFUNC 

**
FUNCTION uf_pesqconvencoes_actualizar
	LOCAL lcSql, lcId
	lcSql = ""
	
	IF EMPTY(ALLTRIM(pesqconvencoes.id.value))
		lcId = "0"
	ELSE 
		lcId = pesqconvencoes.id.value
	ENDIF 
	
	&& Cursor de Dependentes
	TEXT TO lcSql NOSHOW textmerge
		 exec up_convencoes_pesquisa <<ALLTRIM(pesqconvencoes.ultimos.value)>>,'<<ALLTRIM(pesqconvencoes.local.value)>>', '<<ALLTRIM(pesqconvencoes.convencao.value)>>', '<<ALLTRIM(pesqconvencoes.entidade.value)>>', <<lcId>>
	ENDTEXT

	IF !uf_gerais_actgrelha("PESQCONVENCOES.gridPesq", "uCrsPesqConvencoes", lcSql)
		RETURN .f.
	ENDIF
	
ENDFUNC 

**
FUNCTION uf_pesqconvencoes_novaConvencao
	uf_convencoes_chama(0,"")
	uf_convencoes_novo()
	uf_pesqconvencoes_sair()
ENDFUNC 

**
FUNCTION uf_pesqconvencoes_sel
	DO CASE 
		CASE EMPTY(pesqconvencoes.origem)
			SELECT uCrsPesqConvencoes
			uf_convencoes_chama(uCrsPesqConvencoes.id,"")
			uf_pesqconvencoes_sair()
		CASE UPPER(ALLTRIM(pesqconvencoes.origem)) == "CONFIGHONORARIOS"
			SELECT uCrsPesqConvencoes
			uf_convencoes_chama(uCrsPesqConvencoes.id,"CONFIGHONORARIOS")
			
			IF TYPE("PESQCONVENCOES") != "U"
				uf_pesqconvencoes_sair()
			ENDIF 
		CASE UPPER(ALLTRIM(pesqconvencoes.origem)) == "CONFIGHONORARIOSLINHA"
	
			SELECT uCrsPesqConvencoes
			SELECT ucrsConfgHonorariosUs
			Replace ucrsConfgHonorariosUs.id_cpt_conv WITH uCrsPesqConvencoes.id
			Replace ucrsConfgHonorariosUs.convencao WITH uCrsPesqConvencoes.descr
	
			IF TYPE("PESQCONVENCOES") != "U"
				uf_pesqconvencoes_sair()
			ENDIF 
			
	ENDCASE 
ENDFUNC 


**
FUNCTION uf_pesqconvencoes_sair
	**
	PESQCONVENCOES.hide
	PESQCONVENCOES.release
ENDFUNC
