**
PROCEDURE uf_pesqlote_chama
 LPARAMETERS lclote, lcref, lcecra, lctipodoc
 IF TYPE("PESQLOTE")<>"U"
    pesqlote.show
 ELSE
    IF  .NOT. USED("ucrsPesqLote")
       lcsql = ""
       TEXT TO lcsql TEXTMERGE NOSHOW
				exec up_ga_loteDetalhePrecos '<<lcLote>>', '<<lcRef>>', <<IIF(EMPTY(lcTipoDoc),0,lcTipoDoc)>>
       ENDTEXT
       uf_gerais_actgrelha("", "ucrsPesqLote", lcsql)
    ENDIF
    DO FORM PESQLOTE
 ENDIF
 pesqlote.local = lcecra
 pesqlote.lote.value = lclote
 pesqlote.ref = lcref
 pesqlote.tipodoc = IIF(EMPTY(lctipodoc), 0, lctipodoc)
 uf_pesqlote_carregadados()
ENDPROC
**
PROCEDURE uf_pesqlote_carregadados
 lcsql = ""
 TEXT TO lcsql TEXTMERGE NOSHOW
	 	exec up_ga_loteDetalhePrecos '<<ALLTRIM(PESQLOTE.lote.value)>>','<<PESQLOTE.ref>>',0
 ENDTEXT
 uf_gerais_actgrelha("PESQLOTE.gridPesq", "ucrsPesqLote", lcsql)
 IF pesqlote.local=="FACTURACAO"
    IF USED("ucrsQtAplicadaLoteAtual")
       fecha("ucrsQtAplicadaLoteAtual")
    ENDIF
    SELECT SUM(qtt) AS qtt, ref, lote FROM fi WHERE  NOT EMPTY(lote) GROUP BY ref, lote INTO CURSOR ucrsQtAplicadaLoteAtual READWRITE
    UPDATE ucrsPesqLote FROM ucrsPesqLote INNER JOIN ucrsQtAplicadaLoteAtual ON ucrspesqlote.ref=ucrsqtaplicadaloteatual.ref .AND. ucrspesqlote.lote=ucrsqtaplicadaloteatual.lote SET ucrspesqlote.stock = ucrspesqlote.stock-ucrsqtaplicadaloteatual.qtt
 ENDIF
ENDPROC
**
FUNCTION uf_pesqlote_escolha
 DO CASE
    CASE pesqlote.local=="FACTURACAO"
       IF pesqlote.tipodoc<>3
          SELECT ucrspesqlote
          IF  .NOT. uf_facturacao_atribuilotealteraqtt(ucrspesqlote.lote)
             RETURN .F.
          ENDIF
       ENDIF
       IF ucrspesqlote.stock<=0
          uf_perguntalt_chama("N�o existe stock suficiente no Lote.", "OK", "", 32)
          RETURN .F.
       ENDIF
       SELECT ucrspesqlote
       SELECT fi
       REPLACE fi.amostra WITH .T.
       REPLACE fi.lote WITH ucrspesqlote.lote
       uf_facturacao_dividelote(fi.ref, fi.qtt, fi.fistamp)
       uf_atendimento_fiiliq(.T.)
       uf_atendimento_acttotaisft()
    CASE pesqlote.local=="CONFENCOMENDAS"
       SELECT ucrspesqlote
       SELECT ucrsfn
       SELECT ucrsfn
       REPLACE ucrsfn.lote WITH ucrspesqlote.lote
    CASE pesqlote.local=="CONFAVIAMENTOS"
       SELECT ucrspesqlote
       SELECT ucrsfn
       IF ucrspesqlote.stock<ucrsfn.qttrec
          uf_perguntalt_chama("N�o existe stock suficiente no Lote.", "OK", "", 32)
       ELSE
          SELECT ucrsfn
          REPLACE ucrsfn.lote WITH ucrspesqlote.lote
       ENDIF
    OTHERWISE
 ENDCASE
 uf_pesqlote_sair()
ENDFUNC
**
PROCEDURE uf_pesqlote_sair
 IF USED("ucrsPesqLote")
    fecha("ucrsPesqLote")
 ENDIF
 pesqlote.hide
 pesqlote.release
ENDPROC
**
