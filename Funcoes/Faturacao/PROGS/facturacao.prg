***
*** ReFox XII  #PT125680  Jose Costa  Lts, Lda. [VFP90]
***
Do pesqfact
Do importarfact
Do reimppos
Do dadoscliente
Do mvr
Do pesqlote
Do FATURACAOPAGEXTERNO

**
Define Class pbordofact As Custom
	**
	Procedure proc_clicklote
		If myftintroducao==.T. .Or. myftalteracao==.T.
			Select ft
			Select fi
			If fi.usalote==.T.
				uf_pesqlote_chama(fi.lote, fi.ref, "FACTURACAO", ft.tipodoc)
			Endif
		Endif
	Endproc
	**
	Procedure proc_gotfocuslote
		With facturacao.pageframe1.page1.griddoc
			For i = 1 To .ColumnCount
				If Upper(.Columns(i).ControlSource)=="FI.LOTE"
				Endif
			Endfor
		Endwith
	Endproc
	**
	Procedure proc_escolhediploma
		Local lcvalida
		Public myftintroducao, myftalteracao
		If myftintroducao==.T. .Or. myftalteracao==.T.
			Store .T. To lcvalida
			Select fi
			If  .Not. Empty(fi.u_diploma)
				Replace fi.u_diploma With "" In fi
				uf_atendimento_actvaloresfi()
				uf_atendimento_acttotaisft()
				For i = 1 To facturacao.pageframe1.page1.griddoc.ColumnCount
					If Upper(facturacao.pageframe1.page1.griddoc.Columns(i).ControlSource)=="FI.U_COMP"
						facturacao.pageframe1.page1.griddoc.Columns(i).SetFocus
						Exit
					Endif
				Endfor
			Else
				Select ft2
				If  .Not. Empty(Alltrim(ft2.u_codigo))
					If uf_gerais_actgrelha("", "uCrsPlaDip", [select diploma from cptpla (nolock) where codigo=']+Alltrim(ft2.u_codigo)+['])
						If Reccount("uCrsPlaDip")=0
							Store .F. To lcvalida
						Endif
						If ucrspladip.diploma<>"1"
							Store .F. To lcvalida
						Endif
						fecha("uCrsPlaDip")
						If lcvalida==.T.
							mydiplomaauto = .F.
							uf_diplomas_chama()
						Endif
					Endif
				Endif
				uf_atendimento_actvaloresfi()
				uf_atendimento_acttotaisft()
			Endif
		Endif
	Endproc
	**
	Procedure proc_activaeventoslinhasfact
		uf_facturacao_eventosgriddoc()
	Endproc
	**
	Procedure proc_validaexistenciavalesdesconto
		Public myvaledescval
		Local lcpos, lcvalida, lcolddescval, ti
		Store .T. To lcvalida
		Store 0 To lcolddescval, ti
		Select fi
		lcolddescval = fi.u_descval
		If myvaledescval==.T.
			With facturacao.pageframe1.page1.griddoc
				For i = 1 To .ColumnCount
					If Upper(.Columns(i).ControlSource)=="FI.U_DESCVAL"
						.Columns(i).text1.Value = Round(lcolddescval, 2)
					Endif
				Endfor
			Endwith
			uf_perguntalt_chama("DESCULPE, N�O � POSS�VEL APLICAR DESCONTOS EM VALOR MANUALMENTE, SE EXISTIREM VALES DE DESCONTO. POR FAVOR VERIFIQUE.", "OK", "", 16)
		Endif
	Endproc
	**
	Procedure proc_painelpvps
		DoDefault()
		If myftintroducao==.T. .Or. myftalteracao==.T.
			Select fi
			uf_atendimento_alterapvp(fi.fistamp)
		Endif
	Endproc
	**
	Procedure proc_validaalteracaototallinha
		Local lcprecouni
		Store 0 To lcprecouni
		Select fi
		If fi.qtt==0
			Replace fi.qtt With 1
		Endif
		Select fi
		If fi.qtt<>0
			lcprecouni = fi.etiliquido
			If fi.desconto>0 .And. fi.desconto<100
				lcprecouni = lcprecouni/((100-fi.desconto)/100)
			Endif
			If fi.desc2>0 .And. fi.desc2<100
				lcprecouni = lcprecouni/((100-fi.desc2)/100)
			Endif
			If fi.desc3>0 .And. fi.desc3<100
				lcprecouni = lcprecouni/((100-fi.desc3)/100)
			Endif
			If fi.desc4>0 .And. fi.desc4<100
				lcprecouni = lcprecouni/((100-fi.desc4)/100)
			Endif
			lcprecouni = lcprecouni/fi.qtt
			If  .Not. lcprecouni=0
				Replace fi.epv With Round(lcprecouni, 2)
			Endif
			Select td
			If td.tipodoc=3
				If fi.etiliquido>0
					Replace fi.etiliquido With fi.etiliquido*-1
				Endif
			Endif
			If  .Not. Empty(Alltrim(fi.ref))
				uf_atendimento_actref()
			Endif
		Endif
		If  .Not. Empty(Alltrim(fi.ref))
			uf_atendimento_actvaloresfi()
		Endif
		uf_atendimento_acttotaisft()
		uf_facturacao_desactivaeventoslinhasfact()
	Endproc
	**
	Procedure proc_apagarautocompletereffi
		Lparameters nkeycode, nshiftaltctrl
		mykeypressed = nkeycode
	Endproc
	**
	Procedure proc_autocompletereffi
		Lparameters nkeycode
		facturacao.eventoref = .T.
		uf_facturacao_autocompletereffi(nkeycode)
	Endproc
	**
	Procedure proc_pesquisarartigosreffi
		Local lcsql, lcref
		Store '' To lcsql, lcref
		uf_pesqstocks_chama("FACTURACAO", facturacao.valorparapesquisa)
		uf_facturacao_desactivaeventoslinhasfact()
	Endproc
	**
	Procedure proc_apagarautocompletedesignfi
		Lparameters nkeycode, nshiftaltctrl
		mykeypressed = nkeycode
	Endproc
	**
	Procedure proc_autocompletedesignfi
		Lparameters nkeycode
		uf_facturacao_autocompletedesign(nkeycode)
	Endproc
	**
	Procedure proc_pesquisarartigosdesignfi
		uf_facturacao_pesquisarartigosdesignfi()
		uf_facturacao_desactivaeventoslinhasfact()
	Endproc
	**
	Procedure proc_eventoreffi
		uf_facturacao_eventoreffi()
		facturacao.eventoref = .F.
		uf_facturacao_desactivaeventoslinhasfact()
	Endproc
	**
	Procedure proc_actualizatotaisfi
		Local lcfistamp

		Select fi
		If fi.u_epvp < 0 And td.codigoDoc <> 98
			uf_perguntalt_chama("N�o pode introduzir um PVP negativo.", "OK", "", 48)
			Select fi
			Replace fi.u_epvp With fi.epvori
		Endif

		If uf_gerais_getparameter("ADM0000000211", "BOOL")
			uf_facturacao_atribuilotealteraqtt()
		Endif
		uf_atendimento_fiiliq(.T.)
		uf_atendimento_actvaloresfi()
		uf_atendimento_acttotaisft()
		uf_facturacao_desactivaeventoslinhasfact()

	Endproc
	**
	Procedure proc_actualizatotaisfiQTT
		Local lcfistamp

		If Used("FI2")
			Select FI2

			If !Empty(ft2.u_codigo)
				Select fi
				If !uf_faturacao_checkQttMaxPla(ft2.u_codigo, fi.qtt)
					Replace fi.qtt With 1
					Return .F.
				Endif
			Endif
		Endif

		**Comentado pois ter� de ser feito para o Ticket SD-41008
		**SELECT FI
		**IF uf_gerais_getUmValor("fprod", "dispositivo_seguranca", "cnp = '" + ALLTRIM(fi.ref) + "'")
		**
		**  uf_perguntalt_chama("N�o pode alterar a quantidade de produtos marcados com Dispositivo de Seguran�a.", "OK", "", 16)
		**
		**  SELECT FI
		**  REPLACE fi.qtt WITH uf_gerais_getUmValor("st", "qttEmbal", "ref = '" + ALLTRIM(fi.ref) + "' and site_nr = " +  ASTR(mySite_nr))
		**ENDIF

		If uf_gerais_getparameter("ADM0000000211", "BOOL")
			uf_facturacao_atribuilotealteraqtt()
		Endif
		uf_atendimento_fiiliq(.T.)
		uf_atendimento_actvaloresfi()
		uf_atendimento_acttotaisft()
		uf_facturacao_desactivaeventoslinhasfact()
	Endproc
	**
	Procedure proc_actualizatotaisfiivaincl
		uf_atendimento_fiiliq(.T.)
		uf_atendimento_actvaloresfi()
		uf_atendimento_acttotaisft()
		uf_facturacao_desactivaeventoslinhasfact()
	Endproc
	**
	Procedure proc_clickccusto
		If myftintroducao==.T. .Or. myftalteracao==.T.
			uf_valorescombo_chama("FI.FICCUSTO", 2, "ucrsCCusto", 2, "DESCRICAO", "DESCRICAO")
		Endif
	Endproc
	**
	Function proc_navegast
		Select fi
		If  .Not. Empty(fi.ref)
			TEXT TO lcsql TEXTMERGE NOSHOW
				Select ref FROM ST (nolock) WHERE REF = '<<Alltrim(FI.REF)>>'
			ENDTEXT
			If  .Not. uf_gerais_actgrelha("", "ucrsExisteRef", lcsql)
				uf_perguntalt_chama("OCORREU UMA ANOMALIA NA VERIFICA��O DOS DADOS DA REFER�NCIA, POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
				Return .F.
			Endif
			If Reccount("ucrsExisteRef")>0
				If  .Not. (Type("gestao_stocks")=="U")
					If  .Not. (gestao_stocks.Caption)=="Stocks e Servi�os"
						uf_perguntalt_chama("O ECR� DE STOCKS ENCONTRA-SE EM MODO DE ALTERA��O / INTRODU��O! TERMINE ESTA OPERA��O E TENTE NOVAMENTE.", "OK", "", 64)
						Return .F.
					Endif
				Endif
				uf_stocks_chama(Alltrim(fi.ref))
			Endif
			If Used("ucrsExisteRef")
				fecha("ucrsExisteRef")
			Endif
		Endif
	Endfunc
	**
	Procedure proc_clickiva
		Public mylccodiva
		Local lctaxaiva, lctabiva
		If myftalteracao==.T. .Or. myftintroducao==.T.
			uf_valorescombo_chama("mylcCodIva", 0, "select sel = convert(bit,0), codigo = CONVERT(int,codigo), taxa from taxasiva (nolock) where ativo=1", 1, "codigo", "taxa")
			If  .Not. Empty(mylccodiva)
				lctaxaiva = uf_gerais_gettabelaiva(mylccodiva, "TAXA")
				lctabiva = mylccodiva
				Select fi
				Replace fi.tabiva With lctabiva
				Replace fi.iva With lctaxaiva
				uf_atendimento_fiiliq(.T.)
				uf_atendimento_acttotaisft()
			Endif
		Endif
	Endproc
	**
	Function proc_codmotiseimp
		Select fi
		If fi.iva=0 .And.  .Not. Empty(fi.ref)
			Local lcsql
			lcsql = ""
			TEXT TO lcsql TEXTMERGE NOSHOW
				select code_motive, campo from b_multidata (nolock) where tipo = 'motiseimp' AND pais='<<ALLTRIM(ucrse1.pais)>>' ORDER BY campo
			ENDTEXT
			If  .Not. uf_gerais_actgrelha("", "ucrsTempMotiseimp", lcsql)
				uf_perguntalt_chama("N�o foi possivel encontrar regitos.", "", "OK", 16)
				Return .F.
			Endif
			Public codmotiseimplin
			codmotiseimplin = ''
			uf_valorescombo_chama("codmotiseimplin", 0, "ucrsTempMotiseimp", 2, "campo", "code_motive, campo ", .F., .F., .T.)
			fecha("ucrsTempMotiseimp")
			If Len(codmotiseimplin)<>0
				lcsql = ""
				TEXT TO lcsql TEXTMERGE NOSHOW
					select code_motive, campo from b_multidata (nolock) where tipo = 'motiseimp' AND campo='<<ALLTRIM(codmotiseimplin)>>' AND pais='<<ALLTRIM(ucrse1.pais)>>' ORDER BY campo
				ENDTEXT
				If  .Not. uf_gerais_actgrelha("", "ucrsTempMotiseimp", lcsql)
					uf_perguntalt_chama("N�o foi possivel encontrar regitos.", "", "OK", 16)
					Return .F.
				Endif
				Local lcmot, lccodmot
				Store '' To lcmot, lccodmot
				Select ucrstempmotiseimp
				lcmot = Alltrim(ucrstempmotiseimp.campo)
				lccodmot = Alltrim(ucrstempmotiseimp.code_motive)
				Select fi
				Replace fi.codmotiseimp With lccodmot
				Replace fi.motiseimp With lcmot
			Else
				Select fi
				Replace fi.codmotiseimp With ''
				Replace fi.motiseimp With ''
			Endif
		Endif
	Endfunc
	**
	Function proc_validaalteracaoarmazem
		Select fi
		If fi.armazem==0
			uf_perguntalt_chama("ARMAZ�M INVALIDO.", "OK", "", 16)
			Return .F.
		Endif
		Select fi
		TEXT TO lcsql TEXTMERGE NOSHOW
			exec up_gerais_armazensDisponiveis '<<ALLTRIM(mysite)>>', '<<fi.ref>>'
		ENDTEXT
		If  .Not. uf_gerais_actgrelha("", "uCrsArmazensDisp", lcsql)
			uf_perguntalt_chama("N�O FOI POSSIVEL VERIFICAR OS ARMAZ�NS DISPONIVEIS. POR FAVOR CONTACTE O SUPORTE", "OK", "", 16)
			Return .F.
		Endif
		Select ucrsarmazensdisp
		Locate For ucrsarmazensdisp.armazem==fi.armazem
		If  .Not. Found()
			uf_perguntalt_chama("Armaz�m inv�lido para Empresa Atual. Por favor verifique.", "OK", "", 48)
			Select fi
			Replace fi.armazem With myarmazem
			If fi.usalote==.T.
				Replace fi.lote With ""
			Endif
		Else
			If fi.usalote==.T.
				uf_facturacao_atribuilote()
			Endif
		Endif
		If Used("uCrsArmazensDisp")
			fecha("uCrsArmazensDisp")
		Endif
	Endfunc
	**
	Procedure proc_ordenapordesigndoc
		uf_facturacao_ordenapordesigndoc()
	Endproc
	**
Enddefine
**
Define Class MYCOMBOFICCUSTO As ComboBox
	**
	Procedure LostFocus
		** Este procedimento est� vazio! **
	Endproc
	**
Enddefine
**
Define Class commandButtonDestinos As CommandButton
	**
	Procedure Click
		If myftalteracao==.F. .And. myftintroducao==.F.
			uf_origensdestinos_chama('FTDESTINOS')
		Endif
	Endproc
	**
Enddefine
**
Define Class commandButtonOrigens As CommandButton
	**
	Procedure Click
		If myftalteracao==.F. .And. myftintroducao==.F.
			uf_origensdestinos_chama('FTORIGENS')
		Endif
	Endproc
	**
Enddefine
**
Define Class commandButtonMV As CommandButton
	**
	Function Click
		If myftalteracao==.T. .Or. myftintroducao==.T.
			Select ft
			If  .Not. (Alltrim(ft.nmdoc)=='Inser��o de Receita')
				uf_perguntalt_chama("S� � POSS�VEL UTILIZAR ESTA FUNCIONALIDADE NAS 'INSER��ES DE RECEITA'.", "OK", "", 48)
				Return .F.
			Endif
			Select ft2
			If Empty(ft2.u_codigo)
				uf_perguntalt_chama("S� � POSS�VEL UTILIZAR ESTA FUNCIONALIDADE EM DOCUMENTOS COM RECEITA.", "OK", "", 48)
				Return .F.
			Endif
			Select fi
			If Empty(fi.ref)
				uf_perguntalt_chama("DEVE SELECIONAR PRIMEIRO A LINHA DA RECEITA QUE PRETENDE ALTERAR. DEVE EXISTIR REFER�NCIA.", "OK", "", 48)
				Return .F.
			Endif
			Select fi
			lcsql = ""
			TEXT TO lcsql TEXTMERGE NOSHOW
				exec up_stocks_pesquisaNoDic '<<Alltrim(fi.ref)>>', '', '',0
			ENDTEXT
			If uf_gerais_actgrelha("", "uCrsExFprod", lcsql)
				If  .Not. Reccount("uCrsExFprod")>0
					uf_perguntalt_chama("S� PODE MANIPULAR O VALOR DE COMPARTICIPA��O EM PRODUTOS EXISTENTES NO DICION�RIO.", "OK", "", 48)
					fecha("uCrsExFprod")
					Return
				Endif
				fecha("uCrsExFprod")
			Endif
			uf_mvr_chama()
		Endif
	Endfunc
	**
ENDDEFINE




Define Class commandButtonAlt As CommandButton
	**
	Function Click
		If myftalteracao==.T. .Or. myftintroducao==.T.
			uf_atendimento_validaDadosPrincipioActivo()
		Endif
	Endfunc
	**
Enddefine



**
Function uf_facturacao_chama
	Lparameters tcstamp
	uf_chk_nova_versao()
	If (uf_ligacoes_addconnection('FACT')==.F.)
		Return .F.
	Endif

	Release myinserereceitacorrigida
	Release myCorriguirReceita
	Release myOldStamp
	If uf_gerais_validapermuser(ch_userno, ch_grupo, 'Gest�o de Factura��o')
		If  .Not. (Type('ATENDIMENTO')=="U") .And.  .Not. factcompartseg
			uf_perguntalt_chama("N�O PODE ABRIR O ECR� DE FACTURA��O ENQUANTO O ECR� DE ATENDIMENTO ESTIVER ABERTO.", "OK", "", 48)
			Return .F.
		Endif
		If  .Not. (Type('FACTURACAO_ENTIDADES')=="U")
			uf_perguntalt_chama("N�O PODE ABRIR O ECR� DE FACTURA��O ENQUANTO O ECR� DE FACTURA��O �S ENTIDADES ESTIVER ABERTO.", "OK", "", 48)
			Return .F.
		Endif
	Else
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ACEDER AO PAINEL GEST�O DE FACTURA��O.", "OK", "", 48)
		Return .F.
	Endif
	If Type("ATENDIMENTO")<>"U" .And.  .Not. factcompartseg
		Return .F.
	Endif
	If Type("PESQUTENTES")<>"U"
		uf_pesqutentes_sair()
	Endif
	regua(0, 8, "A carregar painel...")
	regua(1, 1, "A carregar painel...")
	Local lcnat
	If Empty(tcstamp)
		lcnat = Alltrim(uf_gerais_devolveultregisto(Alltrim(Str(mytermno))))
		uf_gerais_actgrelha("", "uCrsTempStamp", "select top 1 ftstamp from ft (nolock) where u_nratend='"+lcnat+Iif(mytermno<10, '0'+Alltrim(Str(mytermno)), Alltrim(Str(mytermno)))+"' order by ftano desc")
		Select ucrstempstamp
		tcstamp = Alltrim(ucrstempstamp.ftstamp)
		If Used("uCrsTempStamp")
			fecha("uCrsTempStamp")
		Endif
	Endif
	If  .Not. Used("UcrsConfigModPag")
		TEXT TO lcsql TEXTMERGE NOSHOW
			select * from B_modoPag
		ENDTEXT
		If  .Not. uf_gerais_actgrelha("", "UcrsConfigModPag", lcsql)
			uf_perguntalt_chama("N�o foi poss�vel verificar as configura��es de Modo de Pagamento. Contacte o suporte.", "", "OK", 32)
			Return .F.
		Endif
	Endif
	Public myftintroducao, myftalteracao, myresizefacturacao, myfactagravar
	If myftintroducao==.T. .Or. myftalteracao==.T.
		uf_perguntalt_chama("O ECR� DE FACTURA��O ENCONTRA-SE EM MODO DE INTRODU��O OU EDI��O.", "OK", "", 48)
		regua(2)
		If  .Not. (Type("FACTURACAO")=="U")
			facturacao.Show
		Endif
		Return .F.
	Endif
	regua(1, 2, "A carregar painel...")
	TEXT TO lcsql TEXTMERGE NOSHOW
		SELECT TOP 1 nmdoc as nomedoc FROM ft (nolock) WHERE ftstamp='<<ALLTRIM(tcStamp)>>'
	ENDTEXT
	If uf_gerais_actgrelha("", "uCrsNomeDoc", lcsql)
		If Reccount()>0
			If  .Not. (uf_gerais_validapermuser(ch_userno, ch_grupo, Alltrim(ucrsnomedoc.nomedoc)+' - Visualizar'))
				If Type("FACTURACAO")<>"U"
					uf_perguntalt_chama("O SEU PERFIL N�O PERMITE VISUALIZAR DOCUMENTOS DO TIPO: "+Upper(Alltrim(ucrsnomedoc.nomedoc))+".", "OK", "", 48)
				Endif
				fecha("uCrsNomeDoc")
				tcstamp = ""
			Endif
		Endif
		fecha("uCrsNomeDoc")
	Endif
	If uf_gerais_getparameter_site('ADM0000000073', 'BOOL', mysite)
		If Used("fimancompart")
			fecha("fimancompart")
		Endif
		TEXT TO lcsql TEXTMERGE NOSHOW
			select
				fistamp, ref, design, cast(00000000.00 as numeric(10,2)) as valor, cast(000.00 as numeric(5,2)) as perc, cast(00000000.00 as numeric(10,2)) as valortot
			from
				fi
			where
				0=1
		ENDTEXT
		uf_gerais_actgrelha("", "fimancompart", lcsql)
	Endif
	regua(1, 3, "A carregar painel...")
	uf_facturacao_criacursores(tcstamp)
	regua(1, 4, "A carregar painel...")
	uf_facturacao_carregadocumentos()

	If Type("FACTURACAO")=="U"
		If (uf_gerais_addconnection('FACT')==.F.)
			regua(2)
			Return .F.
		Endif
		regua(1, 5, "A carregar painel...")
		Do Form facturacao
		facturacao.menu1.adicionaopcao("opcoes", "Op��es", mypath+"\imagens\icons\opcoes_w.png", "", "")
		facturacao.menu1.adicionaopcao("pesquisar", "Voltar", mypath+"\imagens\icons\lupa_w.png", "uf_pesqFact_chama with 'FACTURACAO'", "")
		If  uf_gerais_validapermuser(ch_userno, ch_grupo,'Gest�o Fatura��o - Criar Documentos')
			facturacao.menu1.adicionaopcao("novo", "Novo", mypath+"\imagens\icons\mais_w.png", "uf_Facturacao_novo", "")
		Endif
		If  uf_gerais_validapermuser(ch_userno, ch_grupo,'Gest�o Fatura��o - Alterar Documentos')
			facturacao.menu1.adicionaopcao("editar", "Editar", mypath+"\imagens\icons\lapis_w.png", "uf_facturacao_alterarDoc", "")
		Endif
		facturacao.menu1.adicionaopcao("anterior", "Anterior", mypath+"\imagens\icons\ponta_seta_left_w.png", "uf_facturacao_registoanterior", "")
		facturacao.menu1.adicionaopcao("seguinte", "Seguinte", mypath+"\imagens\icons\ponta_seta_rigth_w.png", "uf_facturacao_registoseguinte", "")
		facturacao.menu1.adicionaopcao("impDoc", "Importar", mypath+"\imagens\icons\importar.png", "uf_facturacao_importDocChama", "")
		facturacao.menu1.adicionaopcao("dem", "DEM", mypath+"\imagens\icons\detalhe_micro.png", "uf_facturacao_DEM", "")
		facturacao.menu1.adicionaopcao("novaLinha", "Nova Linha", mypath+"\imagens\icons\mais_w.png", "uf_facturacao_addLinha", "")
		facturacao.menu1.adicionaopcao("eliminarLinha", "Eliminar Linha", mypath+"\imagens\icons\eliminar_w.png", "uf_facturacao_remLinha", "")
		facturacao.menu1.adicionaopcao("pesquisarStock", "Pesquisar", mypath+"\imagens\icons\lupa_w.png", "uf_pesqstocks_chama with 'FACTURACAO'", "")
		facturacao.menu1.adicionaopcao("enviarncSNS", "Enviar Doc.", mypath+"\imagens\icons\pasta_w.png", "uf_facturacaoentidades_envioNCSNS", "")
		facturacao.menu1.adicionaopcao("enviarDocCert", "Enviar ESPAP", mypath+"\imagens\icons\pasta_w.png", "uf_faturacao_enviarDocCertif", "")
		facturacao.menu_opcoes.adicionaopcao("imprimir", "Imprimir", "", "uf_imprimirgerais_Chama with 'FACTURACAO'")
		facturacao.menu_opcoes.adicionaopcao("pos", "Imprimir POS", "", "uf_reimppos_chama with 'FT'")
		facturacao.menu_opcoes.adicionaopcao("corrigirReceita", "Corrigir Receita", "", "uf_facturacao_corrigirReceitaAuto")
		facturacao.menu_opcoes.adicionaopcao("dadosReceita", "Dados Receita", "", "uf_facturacao_corrigirReceita")
		facturacao.menu_opcoes.adicionaopcao("consultarReceita", "Consultar Receita", "", "uf_facturacao_consultarReceita")
		facturacao.menu_opcoes.adicionaopcao("reinserirReceita", "Reinserir Receita", "", "uf_facturacao_reinserirReceita")
		facturacao.menu_opcoes.adicionaopcao("cliente", "Dados Cliente", "", "uf_dadoscliente_Chama")
		facturacao.menu_opcoes.adicionaopcao("enviaDT", "Enviar Doc. Transp.", "", "uf_facturacao_enviaDT")
		facturacao.menu_opcoes.adicionaopcao("eliminar", "Eliminar", "", "uf_facturacao_eliminarDoc")
		facturacao.menu_opcoes.adicionaopcao("ultimo", "�ltimo", "", "uf_Facturacao_ultimoRegisto")
		facturacao.menu_opcoes.adicionaopcao("talaonnordisk", "Reimp. Tal�o Controlo", "", "uf_talao_novonordisk")
		facturacao.menu_opcoes.adicionaopcao("fichaUtente", "Utente", "", "uf_facturacao_fichaUtente")
		facturacao.menu_opcoes.adicionaopcao("alterameiospagamento", "Meios Pag.", "", "uf_Facturacao_alterameiospagamento")
		facturacao.menu_opcoes.adicionaopcao("anexos", "Anexos", "", "uf_anexardoc_chama with 'FT', ALLTRIM(ft.ftstamp), ALLTRIM(ft.nmdoc) + ' '+ ALLTRIM(STR(ft.fno)), ALLTRIM(STR(ft.fno))")
		facturacao.menu_opcoes.adicionaopcao("exportFTEntidadeADSE", "Ft.ADSE", "", "uf_FACTENTIDADESCLINICA_EXPORT_FT WITH 'ADSE'")
		facturacao.menu_opcoes.adicionaopcao("exportFTEntidadeADM", "Ft.ADM", "", "uf_FACTENTIDADESCLINICA_EXPORT_FT WITH 'ADM'")
		facturacao.menu_opcoes.adicionaopcao("impResumo", "Impress�o Resumos", "", "uf_resumos_chama")
		If uf_gerais_getUmValor("b_modoPag", "count(*)", "pagamentoExterno = 1") > 0
			facturacao.menu_opcoes.adicionaopcao("pagExterno", "Pagamentos Externos", "", "uf_faturacaoPagExterno_chama WITH ft.u_nrAtend, ft.fdata")
		Endif
		If emdesenvolvimento == .T.
			facturacao.menu_opcoes.adicionaopcao("libertaReceita", "Liberta Receita DEM", "", "uf_facturacao_administrador_receitasanular")
		Endif
		If  uf_gerais_validapermuser(ch_userno, ch_grupo,'Registo de Vacina��o')
			facturacao.menu_opcoes.adicionaopcao("regVacinacao", "Reg.Vacina��o/Injet�vel", "", "uf_facturacao_OP_vacinacao")
		Endif
		If uf_gerais_getparameter_site('ADM0000000143', 'BOOL', mysite)
			facturacao.menu_opcoes.adicionaopcao("sinc_RNUNovo", "Sinc RNU", "", "uf_utentes_sincronizarRNU WITH ft2.u_nbenef, ft.ncont ,'FACTURACAO'")
		Endif
		facturacao.menu_opcoes.adicionaopcao("regSinave", "Registo Sinave", "", "uf_facturacao_chamaRegSinave")
		If  .Not. (uf_gerais_validapermuser(ch_userno, ch_grupo, 'Importa��o Hist�rico de Vendas - Fatura��o'))
			facturacao.menu_opcoes.adicionaopcao("impHistVendas", "Imp. Hist. Vendas", "", "uf_facturacao_importHistVendas")
		Endif
		fecha("ucrTempLCefrID")
		If(Used("ft"))
			Local lcNmdoc, lcNrelegibilidade
			Select ft
			lcNmdoc = Alltrim(ft.nmdoc)

			Select ft2
			lcNrelegibilidade= Alltrim(ft2.nrelegibilidade)

			uf_gerais_get_efrId(ft.NO)

			If  lcNmdoc =='Ft. Seguradoras'  And !Empty(ucrTempLCefrID.fatEletronica) And !Empty(lcNrelegibilidade)
				facturacao.menu_opcoes.adicionaopcao("reenviarfatura", "Reenviar Fatura", "", "uf_facturacao_reenviarFatura")
			Endif

		Endif


		facturacao.menu1.gravar.img1.Picture = mypath+"\imagens\icons\guardar_w.png"
		facturacao.Refresh
	Else
		regua(1, 5, "A carregar painel...")
		facturacao.Show
	Endif
	If Type("Facturacao.oCust")<>"O"
		facturacao.AddObject("oCust", "pbordofact")
		facturacao.KeyPreview = .T.
	Endif
	regua(1, 6, "A carregar painel...")
	If  .Not. Empty(Alltrim(tcstamp))
		If Used("TD")
			fecha("TD")
		Endif
		Select * From uCrsGeralTD Where uCrsGeralTD.ndoc==ft.ndoc And Alltrim(uCrsGeralTD.site)==Alltrim(mysite) Into Cursor td Readwrite
		Select ft
		myftstamp = ft.ftstamp
		Select fi
		uf_facturacao_controlawklinhas()
		Select ft
		If Empty(ft.u_ltstamp)
			If ft.u_lote<>0
				facturacao.containercab.lote.FontStrikethru = .T.
			Else
				facturacao.containercab.lote.FontStrikethru = .F.
			Endif
			If ft.u_nslote<>0
				facturacao.containercab.nslote.FontStrikethru = .T.
			Else
				facturacao.containercab.nslote.FontStrikethru = .F.
			Endif
		Else
			facturacao.containercab.lote.FontStrikethru = .F.
			facturacao.containercab.nslote.FontStrikethru = .F.
		Endif
		Select ft
		If Empty(ft.u_ltstamp2)
			If ft.u_lote2<>0
				facturacao.containercab.lote2.FontStrikethru = .T.
			Else
				facturacao.containercab.lote2.FontStrikethru = .F.
			Endif
			If ft.u_nslote2<>0
				facturacao.containercab.nslote2.FontStrikethru = .T.
			Else
				facturacao.containercab.nslote2.FontStrikethru = .F.
			Endif
		Else
			facturacao.containercab.lote2.FontStrikethru = .F.
			facturacao.containercab.nslote2.FontStrikethru = .F.
		Endif
		Select fi
		Index On lordem Tag lordem
		If  .Not. Empty(ucrspagcentral.evdinheiro) .Or.  .Not. Empty(ucrspagcentral.epaga1) .Or.  .Not. Empty(ucrspagcentral.epaga2) .Or.  .Not. Empty(ucrspagcentral.echtotal) .Or.  .Not. Empty(ucrspagcentral.epaga3) .Or.  .Not. Empty(ucrspagcentral.epaga4) .Or.  .Not. Empty(ucrspagcentral.epaga5) .Or.  .Not. Empty(ucrspagcentral.epaga6)
			facturacao.pageframe1.page2.movcaixa.Tag = "true"
			facturacao.pageframe1.page2.movcaixa.Picture = mypath+"\imagens\icons\checked_b_24.bmp"
		Else
			facturacao.pageframe1.page2.movcaixa.Tag = "false"
			facturacao.pageframe1.page2.movcaixa.Picture = mypath+"\imagens\icons\unchecked_b_24.bmp"
		Endif
	Endif
	uf_facturacao_alertapic()
	uf_facturacao_demcor()
	regua(1, 7, "A carregar painel...")
	If myresizefacturacao==.F.
		uf_facturacao_expandegridlinhas()
	Endif
	regua(1, 8, "A carregar painel...")
	uf_facturacao_controlaobjectos()
	uf_facturacao_moveCursorsTopo()
	regua(2)
Endfunc

Procedure uf_facturacao_moveCursorsTopo

	If(Used("FT"))
		Select ft
		Go Top
	Endif
	If(Used("FT2"))
		Select ft2
		Go Top
	Endif
	If(Used("FI"))
		Select fi
		Go Top
	Endif

Endfunc
**
Procedure uf_facturacao_moverlinhas
	Lparameters lcbool
	uf_atendimento_moverlinhas(lcbool)
	facturacao.pageframe1.page1.griddoc.Refresh
Endproc
**
Procedure uf_facturacao_op_vacinacao
	uf_vacinacao_chama()
Endproc
**
Function uf_facturacao_criacursores
	Lparameters lcstamp
	If Empty(Alltrim(lcstamp))
		uf_gerais_actgrelha("", "FT", "set fmtonly on exec up_touch_ft '' set fmtonly off")
		uf_gerais_actgrelha("", "FT2", "set fmtonly on SELECT respostaDt = convert(varchar(254),''),* FROM FT2 (nolock) WHERE 1=0 set fmtonly off")
		uf_gerais_actgrelha("", "ucrsPagCentral", "Select top 0 * from B_pagCentral (nolock)")
		lcsql = ""
		TEXT TO lcsql TEXTMERGE NOSHOW
			exec up_facturacao_linhas 1, 0, ''
		ENDTEXT
		If Type("FACTURACAO")=="U"
			uf_gerais_actgrelha("", "FI", lcsql)
		Else
			uf_gerais_actgrelha("FACTURACAO.Pageframe1.Page1.GridDoc", "FI", lcsql)
		Endif
		uf_gerais_actgrelha("", "DADOSPSICO", "set fmtonly on select top 1 *,cabvendasordem=0 from B_dadosPsico(nolock) set fmtonly off")
		TEXT TO lcsql TEXTMERGE NOSHOW
		select
			*
		from
			FI2 (nolock)
		WHERE
			0=1
		ENDTEXT
		uf_gerais_actgrelha("", "FI2", lcsql)

	Else
		uf_gerais_actgrelha("", "FT", "exec up_touch_ft '"+Alltrim(lcstamp)+"'")
		uf_gerais_actgrelha("", "FTQRCODE", "exec up_print_qrcode_a4 '"+Alltrim(lcstamp)+"'")
		uf_gerais_actgrelha("", "fi_nnordisk", "select cast('' as varchar(15)) as ref ,* from fi_nnordisk where fistamp in (select fistamp from fi where ftstamp='"+Alltrim(lcstamp)+"')")
		If ch_userno=1
			uf_gerais_actgrelha("", "FT2", "SELECT respostaDt = isnull((select top 1 DescrResposta from b_dt_resposta WHERE stampDoc = '"+Alltrim(lcstamp)+"' order by DataEnvio),''),* FROM FT2 (nolock) WHERE ft2stamp='"+Alltrim(lcstamp)+"'")
		Else
			uf_gerais_actgrelha("", "FT2", "exec up_facturacao_ft2 '"+Alltrim(lcstamp)+"'")
		Endif
		Select ft2
		*!*	    IF  .NOT. EMPTY(ft2.stamporigproc)
		uf_gerais_actgrelha("", "curFTSeg", "select ft.nome, ft.no, b_utentes.tlmvl , ft.telefone from ft inner join b_utentes on ft.no=b_utentes.no and ft.estab=b_utentes.estab where ftstamp = '"+Alltrim(ft2.stamporigproc)+"'")
		*!*	    ENDIF
		Select ft2
		If  .Not. Empty(Alltrim(ft2.nrelegibilidade))
			TEXT TO lcsql TEXTMERGE NOSHOW
				SELECT( case when (select count(token) from ft_compart (nolock) where returnContributionCorrelationId='<<ALLTRIM(ft2.nrelegibilidade)>>' and type=2)>0
					then 'Elegibilidade eletr�nica'
					else 'Elegibilidade manual'
				end ) as texto
			ENDTEXT
		Else
			TEXT TO lcsql TEXTMERGE NOSHOW
				SELECT( case when (select count(token) from ft_compart (nolock) where returnContributionCorrelationId='<<ALLTRIM(ft2.nrelegibilidade)>>' and type=9999)>0
					then 'Elegibilidade eletr�nica'
					else 'Elegibilidade manual'
				end ) as texto
			ENDTEXT
		Endif
		uf_gerais_actgrelha("", "ucrsElegib", lcsql)
		Select ft
		TEXT TO lcsql TEXTMERGE NOSHOW
			Select * from B_pagCentral (nolock) WHERE nrAtend = '<<ALLTRIM(Ft.u_nratend)>>'
		ENDTEXT
		uf_gerais_actgrelha("", "ucrsPagCentral", lcsql)
		lcsql = ""
		TEXT TO lcsql TEXTMERGE NOSHOW
			exec up_facturacao_linhas 0, <<mysite_nr>>, '<<ALLTRIM(lcStamp)>>'
		ENDTEXT
		If Type("FACTURACAO")=="U"
			uf_gerais_actgrelha("", "FI", lcsql)
		Else
			uf_gerais_actgrelha("FACTURACAO.Pageframe1.Page1.GridDoc", "FI", lcsql)
		Endif
		uf_gerais_actgrelha("", "DADOSPSICO", "select top 1 *,cabvendasordem=0 from B_dadosPsico(nolock) where ftstamp='"+Alltrim(lcstamp)+"'")
		TEXT TO lcsql TEXTMERGE NOSHOW
			select
				*
			from
				FI2 (nolock)
			WHERE
				FI2.ftstamp =  '<<ALLTRIM(lcStamp)>>'
		ENDTEXT
		uf_gerais_actgrelha("", "FI2", lcsql)
	Endif
	uf_gerais_actgrelha("", "uCrsCmbCCusto", "Select RTRIM(LTRIM(CCUSTO)) as CCUSTO FROM CCT (NOLOCK) WHERE INACTIVO = 0 ORDER BY RTRIM(LTRIM(CCUSTO))")
	lcsql = ""
	TEXT TO lcsql TEXTMERGE NOSHOW
		Select
			'' descricao
		union all
		Select
			descricao
		from
			cu (nolock)
		order by
			descricao
	ENDTEXT
	If  .Not. uf_gerais_actgrelha("", "ucrsCCusto", lcsql)
		uf_perguntalt_chama("N�O FOI POSS�VEL VERIFICAR A LISTAGEM DE CENTROS DE CUSTO. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
		Return .F.
	Endif
	lcsql = ""
	If Right(Alltrim(ch_pack), 4)=="2010"
		TEXT TO lcsql TEXTMERGE NOSHOW
			SELECT 'PTE ou EURO' as MOEDA UNION ALL SELECT MOEDA FROM CB (nolock)
		ENDTEXT
	Else
		TEXT TO lcsql TEXTMERGE NOSHOW
			SELECT 'EURO' as MOEDA UNION ALL SELECT MOEDA FROM CB (nolock)
		ENDTEXT
	Endif
	If  .Not. uf_gerais_actgrelha("", "ucrsMoedaDoc", lcsql)
		uf_perguntalt_chama("N�O FOI POSS�VEL DEFINIR A MOEDA DO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
		Return .F.
	Endif
	TEXT TO lcsql TEXTMERGE NOSHOW
		SELECT
			RTRIM(DESCRICAO) descricao
		FROM
			TP (nolock)
		WHERE
			tipo = 1
		ORDER BY
			DESCRICAO
	ENDTEXT
	If  .Not. uf_gerais_actgrelha("", "ucrsCondPag", lcsql)
		uf_perguntalt_chama("N�O FOI POSS�VEL DEFINIR A MOEDA DO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
		Return .F.
	Endif
	TEXT TO lcsql TEXTMERGE NOSHOW
		SELECT
			modofact
		From
			b_utentes (nolock)
			inner join ft (nolock) on ft.no = b_utentes.no and ft.estab = b_utentes.estab
		WHERE
			ftstamp='<<ALLTRIM(lcStamp)>>'
	ENDTEXT
	If  .Not. uf_gerais_actgrelha("", "ucrsModoFact", lcsql)
		uf_perguntalt_chama("N�O FOI POSS�VEL DEFINIR O MODO DE FATURACAO DO CLIENTE. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
		Return .F.
	Endif
	If  .Not. (uf_gerais_actgrelha("", "UCRSATENDST", [exec up_touch_ActRef 'xxxxxxxxxxx',0,0]))
		uf_perguntalt_chama("OCORREU UM ERRO A CRIAR O CURSOR [UCRSATENDST]-fact.", "OK", "", 16)
		Return .F.
	Endif
	Select ucrse1
	If Alltrim(Upper(ucrse1.tipoempresa))=="FARMACIA"
		If  .Not. Used("ucrsHistPic")
			TEXT TO lcsql TEXTMERGE NOSHOW
				exec up_facturacao_picData
			ENDTEXT
			If  .Not. uf_gerais_actgrelha("", "ucrsHistPic", lcsql)
				uf_perguntalt_chama("N�O FOI POSS�VEL DEFINIR INFORMA��O DO PIC DO DOCUMENTO.", "OK", "", 16)
				Return .F.
			Endif
		Endif
	Endif
	If Used("ucrsDadosDemPresc")
		fecha("ucrsDadosDemPresc")
	Endif
	lcsql = ""
	Select ft2
	TEXT TO lcsql TEXTMERGE NOSHOW
		exec up_dem_informacaoMedico '<<ALLTRIM(ft2.token)>>'
	ENDTEXT
	If  .Not. uf_gerais_actgrelha("", "ucrsDadosDemPresc", lcsql)
		uf_perguntalt_chama("N�O FOI POSS�VEL ENCONTRAR DOCUMENTOS DE FACTURA��O.", "OK", "", 16)
		Return .F.
	Endif
	If  .Not. Used("fi_trans_info")
		lcsql = ''
		TEXT TO lcsql TEXTMERGE NOSHOW
			set fmtonly on
			select
				*
				,CAST('' as varchar(30)) as verify_i
				,CAST('' as varchar(100)) as verify_s
				, CAST('' as varchar(100)) as tcode
				, CAST('' as varchar(254)) as verifymsg
				, CAST('' as varchar(100)) as verif_reason
				,CAST('' as varchar(30)) as dispense_i
				, CAST('' as varchar(100)) as dispense_s
				, CAST('' as varchar(254)) as dispensemsg
				, CAST('' as varchar(100)) as disp_reason
				,CAST('' as varchar(30)) as undo_i
				, CAST('' as varchar(100)) as undo_s
				, CAST('' as varchar(254)) as undodispensemsg
				, CAST('' as varchar(100)) as undo_reason
				, CAST('' as varchar(10)) as tipo
				, CAST(0 as bit) as conferido
				, CAST(0 as bit) as deleted
			from
				fi_trans_info (nolock)
			set fmtonly off
		ENDTEXT
		If  .Not. uf_gerais_actgrelha("", "fi_trans_info", lcsql)
			uf_perguntalt_chama("Ocorreu uma anomalia ao criar o cursor [fi_trans_info]. Por favor reinicie o Software.", "OK", "", 16)
			Return .F.
		Endif
	Else
		Delete From fi_trans_info
	Endif
	If  .Not. Used("mixed_bulk_pend")
		lcsql = ''
		TEXT TO lcsql TEXTMERGE NOSHOW
			set fmtonly on
			select
				*
			from
				mixed_bulk_pend (nolock)
			set fmtonly off
		ENDTEXT
		If  .Not. uf_gerais_actgrelha("", "mixed_bulk_pend", lcsql)
			uf_perguntalt_chama("Ocorreu uma anomalia ao criar o cursor [mixed_bulk_pend]. Por favor reinicie o Software.", "OK", "", 16)
			Return .F.
		Endif
	Else
		Delete From mixed_bulk_pend
	Endif
	If  .Not. Used("nrserie_com_results")
		If  .Not. uf_gerais_actgrelha("", 'nrserie_com_results', 'set fmtonly on select * from nrserie_com_results (nolock) set fmtonly off')
			uf_perguntalt_chama("Ocorreu uma anomalia ao criar o cursor [nrserie_com_results]. Por favor reinicie o Software.", "OK", "", 16)
			Return .F.
		Endif
	Else
		Delete From nrserie_com_results
	Endif
	TEXT TO lcsql TEXTMERGE NOSHOW
		select  codigo from  cptpla(nolock) WHERE combinado  = 1
	ENDTEXT
	If  .Not. uf_gerais_actgrelha("", "uCrsPlanosCombinados", lcsql)
		uf_perguntalt_chama("Ocorreu uma anomalia ao criar o cursor [uCrsPlanosCombinados]. Por favor reinicie o Software.", "OK", "", 16)
	Endif
Endfunc
**
Function uf_facturacao_carregadocumentos
	Lparameters tcbool
	lcsql = ""
	TEXT TO lcsql TEXTMERGE NOSHOW
		exec up_perfis_validaAcessoDocs_utilizados '<<ch_userno>>', '<<ALLTRIM(ch_grupo)>>', 'Introduzir', 'FT', 0, '<<ALLTRIM(mysite)>>'
	ENDTEXT
	If  .Not. uf_gerais_actgrelha("", "ucrsComboDocFac", lcsql)
		uf_perguntalt_chama("N�O FOI POSS�VEL ENCONTRAR DOCUMENTOS DE FACTURA��O.", "OK", "", 16)
		Return .F.
	Endif
Endfunc
**
Procedure uf_facturacao_controlaobjectos
	Do Case
		Case myftintroducao==.T.
			facturacao.Caption = "Fatura��o - INTRODU��O"
		Case myftalteracao==.T.
			facturacao.Caption = "Fatura��o - ALTERA��O"
		Otherwise
			facturacao.Caption = "Fatura��o"
	Endcase
	Select ft
	If Empty(ft.nmdoc)
		facturacao.containercab.NO.ReadOnly = .T.
		facturacao.containercab.Nome.ReadOnly = .T.
		facturacao.pageframe1.page1.containerlinhas.btnaddlinha.Enable(.F.)
		facturacao.pageframe1.page1.containerlinhas.btndellinha.Enable(.F.)
		facturacao.menu1.estado("impDoc, dem", "HIDE")
		facturacao.menu_opcoes.estado("exportFTEntidadeADSE, exportFTEntidadeADM, fichaUtente", "HIDE")
	Else

		facturacao.containercab.Nome.ReadOnly = .T.
		facturacao.containercab.datareceita.Visible = .T.
		facturacao.containercab.ldatareceita.Visible = .T.
		If Alltrim(Upper(facturacao.containercab.Nome.Value))=="ADSE"
			facturacao.menu_opcoes.estado("exportFTEntidadeADSE", "SHOW")
		Else
			facturacao.menu_opcoes.estado("exportFTEntidadeADSE", "HIDE")
		Endif
		If Used("fi_nnordisk")
			If Reccount("fi_nnordisk")>0
				facturacao.menu_opcoes.estado("talaonnordisk", "SHOW")
			Else
				facturacao.menu_opcoes.estado("talaonnordisk", "HIDE")
			Endif
		Else
			facturacao.menu_opcoes.estado("talaonnordisk", "HIDE")
		Endif
		If Like('ADM*', Alltrim(Upper(facturacao.containercab.Nome.Value)))
			facturacao.menu_opcoes.estado("exportFTEntidadeADM", "SHOW")
		Else
			facturacao.menu_opcoes.estado("exportFTEntidadeADM", "HIDE")
		Endif
	Endif
	If myftintroducao==.T. .Or. myftalteracao==.T.
		facturacao.pageframe1.page2.txtu_nbenef.ReadOnly = .F.
		facturacao.pageframe1.page2.txtu_nbenef2.ReadOnly = .F.
		facturacao.pageframe1.page2.txtncartaocompart.ReadOnly = .F.
		facturacao.pageframe1.page2.txtValidadeLote.ReadOnly = .F.
		**facturacao.pageframe1.page2.txtcodemb.readonly = .F.
		facturacao.pageframe1.page2.cmbccusto.ReadOnly = .F.
		facturacao.containercab.nmdoc.Enabled = .T.
		facturacao.containercab.docdata.Enabled = .T.
		facturacao.containercab.receita.ReadOnly = .F.
		facturacao.containercab.nmdoc.SetFocus
		facturacao.pageframe1.page1.containerlinhas.btnaddlinha.Enable(.T.)
		facturacao.pageframe1.page1.containerlinhas.btndellinha.Enable(.T.)
		facturacao.menu1.estado("impDoc, dem", "SHOW")
		facturacao.menu_opcoes.estado("fichaUtente", "SHOW")
		facturacao.containercab.tpdesc.Enabled = .T.
		facturacao.containercab.moeda.Enabled = .T.
		facturacao.pageframe1.page1.containertotais.descfin.ReadOnly = .F.
		facturacao.pageframe1.page1.containertotais.valordescfin.ReadOnly = .F.
		If Alltrim(Upper(facturacao.containercab.nmdoc.Value))=="VENDA MANUAL"
			facturacao.pageframe1.page1.containertotais.docorig.ReadOnly = .F.
		Else
			facturacao.pageframe1.page1.containertotais.docorig.ReadOnly = .T.
		Endif
		If Used("TD")
			Select td
			If td.u_tipodoc==9 .Or. td.u_tipodoc==8 .Or. td.u_tipodoc==3
				facturacao.containercab.chksuspensa.Enable(.T.)
				facturacao.containercab.chksuspensa.Visible = .T.
			Else
				facturacao.containercab.chksuspensa.Visible = .F.
			Endif
		Endif
		facturacao.pageframe1.page3.chkvddom.Enable(.T.)
		facturacao.pageframe1.page4.editobs.ReadOnly = .F.
		facturacao.pageframe1.page2.txtexcecaolinha.ReadOnly = .T.
		facturacao.pageframe1.page2.txtexcecaolinha.Enabled = .T.
		facturacao.pageframe1.page2.btnexcecao.Enable(.T.)
		facturacao.pageframe1.page5.txtreceita.ReadOnly = .F.
		facturacao.pageframe1.page5.txtmedico.ReadOnly = .F.
		facturacao.pageframe1.page5.txtnomed.ReadOnly = .F.
		facturacao.pageframe1.page5.txtmoradad.ReadOnly = .F.
		facturacao.pageframe1.page5.txtcodpostd.ReadOnly = .F.
		facturacao.pageframe1.page5.txtnomea.ReadOnly = .F.
		facturacao.pageframe1.page5.txtmoradaa.ReadOnly = .F.
		facturacao.pageframe1.page5.txtcodposta.ReadOnly = .F.
		facturacao.pageframe1.page5.txtContactoCli.ReadOnly = .F.
		facturacao.pageframe1.page5.txtbi.ReadOnly = .F.
		facturacao.pageframe1.page5.txtdata.ReadOnly = .F.
		facturacao.pageframe1.page5.txtidade.ReadOnly = .F.
		facturacao.pageframe1.page5.btnimportdadoscl.Enabled = .T.
		facturacao.menu1.estado("pesquisar,anterior,seguinte,enviarncSNS,enviarDocCert", "HIDE", "GRAVAR", .T., "CANCELAR", .T.)
		If  uf_gerais_validapermuser(ch_userno, ch_grupo,'Gest�o Fatura��o - Criar Documentos')
			facturacao.menu1.estado("novo", "HIDE", "GRAVAR", .T., "CANCELAR", .T.)
		Endif
		If  uf_gerais_validapermuser(ch_userno, ch_grupo,'Gest�o Fatura��o - Alterar Documentos')
			facturacao.menu1.estado("editar", "HIDE", "GRAVAR", .T., "CANCELAR", .T.)
		Endif

		facturacao.menu1.estado("impDoc, dem,novaLinha, eliminarLinha, pesquisarStock", "SHOW")
		facturacao.menu_opcoes.estado("cliente, fichaUtente", "SHOW")
		facturacao.menu_opcoes.estado("imprimir, pos, dadosReceita, corrigirReceita, ultimo, eliminar,alterameiospagamento, enviaDT, regSinave", "HIDE")
	Endif
	If myftalteracao==.T.
		facturacao.containercab.nmdoc.Enabled = .F.
	Endif
	If myftintroducao==.F. .And. myftalteracao==.F.
		facturacao.pageframe1.page2.txtu_nbenef.ReadOnly = .T.
		facturacao.pageframe1.page2.txtu_nbenef2.ReadOnly = .T.
		facturacao.pageframe1.page2.txtncartaocompart.ReadOnly = .T.
		facturacao.pageframe1.page2.txtValidadeLote.ReadOnly = .T.
		facturacao.pageframe1.page2.txtcodemb.ReadOnly = .T.
		facturacao.pageframe1.page2.cmbccusto.ReadOnly = .T.
		facturacao.containercab.Nome.ReadOnly = .T.
		facturacao.containercab.nmdoc.Enabled = .F.
		facturacao.containercab.docdata.Enabled = .F.
		facturacao.containercab.receita.ReadOnly = .T.
		facturacao.pageframe1.page1.containerlinhas.btnaddlinha.Enable(.F.)
		facturacao.pageframe1.page1.containerlinhas.btndellinha.Enable(.F.)
		facturacao.containercab.tpdesc.Enabled = .F.
		facturacao.containercab.moeda.Enabled = .F.
		facturacao.pageframe1.page1.containertotais.descfin.ReadOnly = .T.
		facturacao.pageframe1.page1.containertotais.valordescfin.ReadOnly = .T.
		If Alltrim(Upper(facturacao.containercab.nmdoc.Value))=="VENDA MANUAL"
			facturacao.pageframe1.page1.containertotais.docorig.ReadOnly = .T.
		Else
			facturacao.pageframe1.page1.containertotais.docorig.ReadOnly = .T.
		Endif
		If Used("TD")
			Select td
			If td.u_tipodoc==9 .Or. td.u_tipodoc==8 .Or. td.u_tipodoc==3
				facturacao.containercab.chksuspensa.Enable(.F.)
				facturacao.containercab.chksuspensa.Visible = .T.
			Else
				facturacao.containercab.chksuspensa.Visible = .F.
			Endif
		Endif
		facturacao.pageframe1.page3.chkvddom.Enable(.F.)
		facturacao.pageframe1.page4.editobs.ReadOnly = .T.
		facturacao.pageframe1.page2.txtexcecaolinha.ReadOnly = .T.
		facturacao.pageframe1.page2.txtexcecaolinha.Enabled = .F.
		facturacao.pageframe1.page2.btnexcecao.Enable(.F.)
		facturacao.pageframe1.page5.txtreceita.ReadOnly = .T.
		facturacao.pageframe1.page5.txtmedico.ReadOnly = .T.
		facturacao.pageframe1.page5.txtnomed.ReadOnly = .T.
		facturacao.pageframe1.page5.txtmoradad.ReadOnly = .T.
		facturacao.pageframe1.page5.txtcodpostd.ReadOnly = .T.
		facturacao.pageframe1.page5.txtnomea.ReadOnly = .T.
		facturacao.pageframe1.page5.txtmoradaa.ReadOnly = .T.
		facturacao.pageframe1.page5.txtcodposta.ReadOnly = .T.
		facturacao.pageframe1.page5.txtContactoCli.ReadOnly = .T.
		facturacao.pageframe1.page5.txtbi.ReadOnly = .T.
		facturacao.pageframe1.page5.txtdata.ReadOnly = .T.
		facturacao.pageframe1.page5.txtidade.ReadOnly = .T.
		facturacao.pageframe1.page5.btnimportdadoscl.Enabled = .F.
		facturacao.menu1.estado("opcoes,pesquisar,anterior,seguinte", "SHOW", "GRAVAR", .F., "Menu Inicial", .T.)
		If  uf_gerais_validapermuser(ch_userno, ch_grupo,'Gest�o Fatura��o - Criar Documentos')
			facturacao.menu1.estado("novo", "SHOW", "GRAVAR", .F., "Menu Inicial", .T.)
		Endif
		If  uf_gerais_validapermuser(ch_userno, ch_grupo,'Gest�o Fatura��o - Alterar Documentos')
			facturacao.menu1.estado("editar", "SHOW", "GRAVAR", .F., "Menu Inicial", .T.)
		Endif

		facturacao.menu1.estado("impDoc,dem, novaLinha, eliminarLinha, pesquisarStock", "HIDE")
		facturacao.menu_opcoes.estado("cliente, enviaDT, fichaUtente", "HIDE")
		facturacao.menu_opcoes.estado("imprimir, pos, dadosReceita, corrigirReceita, ultimo, eliminar, alterameiospagamento, regSinave", "SHOW")
		If myftintroducao==.F. And myftalteracao==.F. And uf_facturacao_deveenviarsns()
			facturacao.menu1.estado("enviarncSNS", "SHOW")
		Else
			facturacao.menu1.estado("enviarncSNS", "HIDE")
		Endif
		If myftintroducao==.F. And myftalteracao==.F. And uf_faturacao_mostraEnvioCert()
			facturacao.menu1.estado("enviarDocCert", "SHOW")
		Else
			facturacao.menu1.estado("enviarDocCert", "HIDE")
		Endif
	Endif
	uf_excecaomed_preenchecamposlivres()
	If Used("ucrsWkLinhas")
		With facturacao.pageframe1.page1.griddoc
			If myftintroducao==.T. .Or. myftalteracao==.T.
				.ReadOnly = .F.
				Select ucrswklinhas
				Goto Top
				Scan
					For i = 1 To .ColumnCount
						If Upper(Alltrim(ucrswklinhas.wkfield))==Upper(Alltrim(.Columns(i).ControlSource))
							If ucrswklinhas.wkreadonly==.T.
								.Columns(i).ReadOnly = .T.
							Endif
							If Upper(Alltrim(ucrswklinhas.wkfield))=="FI.TABIVA"
								.Columns(i).Enabled = .T.
							Endif
						Endif
					Endfor
				Endscan
			Else
				.ReadOnly = .T.
			Endif
		Endwith
	Endif
	If Upper(mypaisconfsoftw)=='ANGOLA'
		facturacao.containercab.lcondpag.Visible = .F.
		facturacao.containercab.lote.Visible = .F.
		facturacao.containercab.label10.Visible = .F.
		facturacao.containercab.nslote.Visible = .F.
		facturacao.containercab.label5.Visible = .F.
		facturacao.containercab.lote2.Visible = .F.
		facturacao.containercab.label12.Visible = .F.
		facturacao.containercab.nslote2.Visible = .F.
		facturacao.containercab.label11.Visible = .F.
	Endif
	facturacao.Refresh
Endproc
**
Function uf_facturacao_validaentidadenotacreditodebitooeletronica
	Lparameters lcno
	Local lctemfacturacaoeletronica
	lctemfacturacaoeletronica = .F.
	If Used("uCrsTempEntidadeFactEl")
		fecha("uCrsTempEntidadeFactEl")
	Endif
	lcsql = ""
	TEXT TO lcsql TEXTMERGE NOSHOW
		select 'electronica' = count(*) from cptorg(nolock) where e_ncnd = 1 and u_no = <<lcNo>>
	ENDTEXT
	If  .Not. uf_gerais_actgrelha("", "uCrsTempEntidadeFactEl", lcsql)
		uf_perguntalt_chama("N�O FOI POSS�VEL INFORMA��O SOBRE A ENTIDADE", "OK", "", 16)
		Return .F.
	Endif
	If (ucrstempentidadefactel.electronica>0)
		lctemfacturacaoeletronica = .T.
	Endif
	If Used("uCrsTempEntidadeFactEl")
		fecha("uCrsTempEntidadeFactEl")
	Endif
	Return lctemfacturacaoeletronica
Endfunc
**
Function uf_facturacao_deveenviarsns
	If Used("TD")
		Local lcFNo
		Select ft
		lcFNo = ft.NO
		Select td
		**IF ((LEFT(UPPER(ALLTRIM(ft.nmdoc)), 15)='NOTA DE CR�DITO' .OR. LEFT(UPPER(ALLTRIM(ft.nmdoc)), 12)='NOTA CR�DITO' .OR. LEFT(UPPER(ALLTRIM(ft.nmdoc)), 14)='NOTA DE D�BITO' .OR. LEFT(UPPER(ALLTRIM(ft.nmdoc)), 11)='NOTA D�BITO' .OR. LEFT(UPPER(ALLTRIM(ft.nmdoc)), 13)='N.CR�DITO SNS' .OR. LEFT(UPPER(ALLTRIM(ft.nmdoc)), 12)='N.D�BITO SNS' .OR. LEFT(UPPER(ALLTRIM(ft.nmdoc)), 14)='NOTA DE D�BITO') .AND. uf_facturacao_validaentidadenotacreditodebitooeletronica(ft.no))
		If td.envioeletronico = .T. And uf_facturacao_validaentidadenotacreditodebitooeletronica(lcFNo)
			Return .T.
		Else
			Return .F.
		Endif
	Else
		Return .F.
	Endif
Endfunc
**
Procedure uf_facturacao_ultimoregisto
	Local lcnat
	lcnat = Alltrim(uf_gerais_devolveultregisto(Alltrim(Str(mytermno))))
	uf_gerais_actgrelha("", "uCrsTempStamp", "select top 1 ftstamp from ft (nolock) where site = '<<ALLTRIM(mysite)>>' and u_nratend='"+lcnat+Iif(mytermno<10, '0'+Alltrim(Str(mytermno)), Alltrim(Str(mytermno)))+"'")
	Select ucrstempstamp
	uf_facturacao_chama(Alltrim(ucrstempstamp.ftstamp))
	If Used("uCrsTempStamp")
		fecha("uCrsTempStamp")
	Endif
Endproc
**
Procedure uf_facturacao_novo
	Lparameters lcnaopedetipodoc

	Local lcsql
	Store '' To lcsql
	uf_chk_nova_versao()
	myftintroducao = .T.
	myftalteracao = .F.

	Release myinserereceitacorrigida
	Release myCorriguirReceita
	Release myOldStamp

	If Used("Ft")
		Select ft
		Delete All
	Endif
	Select ft
	Append Blank
	Local lcmoedadefault
	Store 'EURO' To lcmoedadefault
	lcmoedadefault = uf_gerais_getparameter("ADM0000000260", "text")
	If Empty(lcmoedadefault)
		lcmoedadefault = 'EURO'
	Endif
	Select ft
	Replace ft.moeda With lcmoedadefault
	If Used("Ft2")
		Select ft2
		Delete All
	Endif
	Select ft2
	Append Blank
	If Used("ucrsPagCentral")
		Select ucrspagcentral
		Delete All
	Endif
	Select ucrspagcentral
	Append Blank
	If Used("FI")
		Select fi
		Delete All
	Endif
	If Used("FI2")
		Select FI2
		Delete All
	Endif
	If Used("dadosPsico")
		Select dadospsico
		Delete All
	Endif
	uf_facturacao_controlaobjectos()
	uf_facturacao_demcor()
	facturacao.Refresh
	Select ucrse1
	If Upper(Alltrim(ucrse1.tipoempresa))=="CLINICA"
		facturacao.pageframe1.page2.movcaixa.Tag = "true"
		facturacao.pageframe1.page2.movcaixa.Picture = mypath+"\imagens\icons\checked_b_24.bmp"
	Else
		facturacao.pageframe1.page2.movcaixa.Tag = "false"
		facturacao.pageframe1.page2.movcaixa.Picture = mypath+"\imagens\icons\unchecked_b_24.bmp"
	Endif
	If Empty(lcnaopedetipodoc)
		facturacao.containercab.nmdoc.Click
	Endif
Endproc
**
Procedure uf_facturacao_novo_escolhe
	Lparameters lndocumento
	Local lcsql, lcdocumento
	Store '' To lcsql
	lcdocumento = Alltrim(lndocumento)
	uf_chk_nova_versao()
	myftintroducao = .T.
	myftalteracao = .F.
	Release myinserereceitacorrigida
	If Used("Ft")
		Select ft
		Delete All
	Endif
	Select ft
	Append Blank
	Local lcmoedadefault
	Store 'EURO' To lcmoedadefault
	lcmoedadefault = uf_gerais_getparameter("ADM0000000260", "text")
	If Empty(lcmoedadefault)
		lcmoedadefault = 'EURO'
	Endif
	Select ft
	Replace ft.moeda With lcmoedadefault
	If Used("Ft2")
		Select ft2
		Delete All
	Endif
	Select ft2
	Append Blank
	If Used("ucrsPagCentral")
		Select ucrspagcentral
		Delete All
	Endif
	Select ucrspagcentral
	Append Blank
	If Used("FI")
		Select fi
		Delete All
	Endif
	If Used("FI2")
		Select FI2
		Delete All
	Endif
	If Used("dadosPsico")
		Select dadospsico
		Delete All
	Endif
	uf_facturacao_controlaobjectos()
	uf_facturacao_demcor()
	Select ucrse1
	If Upper(Alltrim(ucrse1.tipoempresa))=="CLINICA"
		facturacao.pageframe1.page2.movcaixa.Tag = "true"
		facturacao.pageframe1.page2.movcaixa.Picture = mypath+"\imagens\icons\checked_b_24.bmp"
	Else
		facturacao.pageframe1.page2.movcaixa.Tag = "false"
		facturacao.pageframe1.page2.movcaixa.Picture = mypath+"\imagens\icons\unchecked_b_24.bmp"
	Endif
	Select ft
	Replace ft.nmdoc With Alltrim(lcdocumento)
	facturacao.Refresh
	uf_facturacao_valoresdefeitofact()
	uf_facturacao_controlanumerofact(Year(Datetime()+(difhoraria*3600)))
	uf_atendimento_fiiliq()
	uf_atendimento_acttotaisft()
	uf_facturacao_controlawklinhas()
	uf_facturacao_controlaobjectos()
	uf_facturacao_crialinhadefault()
	facturacao.Refresh
Endproc
**
Function uf_facturacao_alterardoc
	uf_chk_nova_versao()
	If  .Not. Used("TD")
		uf_perguntalt_chama("O CURSOR TD N�O EST� CARREGADO.", "OK", "", 48)
		Return .F.
	Else
		Select td
		If td.u_integra==.T.
			uf_perguntalt_chama("ESTA S�RIE DE DOCUMENTO EST� CONFIGURADA COMO SENDO DE INTEGRA��O. N�O � POSS�VEL ALTERAR DOCUMENTOS DESTE TIPO.", "OK", "", 48)
			Return .F.
		Endif
	Endif
	If uf_gerais_getparameter_site('ADM0000000006', 'BOOL')
		If  .Not. Empty(Alltrim(uf_gerais_getparameter_site('ADM0000000007', 'text'))) .And. Dtoc(ft.fdata, 1)<uf_gerais_getparameter_site('ADM0000000007', 'text')
			uf_perguntalt_chama("N�o pode alterar/eliminar um documento com data inferior � data fechada para altera��es/elimina��es.", "OK", "", 64)
			Return .F.
		Endif
	Endif
	Select ft
	If  .Not. Empty(ft.nmdoc)
		Select ft
		If  .Not. (uf_gerais_validapermuser(ch_userno, ch_grupo, Alltrim(ft.nmdoc)+' - Alterar'))
			uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ALTERAR ESTE TIPO DE DOCUMENTO.", "OK", "", 48)
			Return .F.
		Endif
		Select ft
		If  .Not. Empty(ft.exportado)
			If uf_gerais_validapermuser(ch_userno, ch_grupo, 'Altera��o registos exportados')==.T.
				uf_perguntalt_chama("O seu perfil n�o permite editar registos exportados para a contabilidade.", "OK", "", 32)
				Return .F.
			Endif
			If uf_perguntalt_chama("Documento j� exportado para a Contabilidade. Pretende anular a exporta��o?", "Sim", "N�o", 48)
				TEXT TO lcsql TEXTMERGE NOSHOW
					UPDATE Ft SET exportado = 0 WHERE ftstamp = '<<ALLTRIM(ft.ftstamp)>>'
				ENDTEXT
				If  .Not. uf_gerais_actgrelha("", "", lcsql)
					uf_perguntalt_chama("N�o foi possivel anular a exporta��o do documento", "OK", "", 16)
					Return .F.
				Else
					Select ft
					Replace ft.exportado With .F.
					uf_perguntalt_chama("Exporta��o anulada com sucesso.", "OK", "", 64)
				Endif
			Else
				Return .F.
			Endif
		Endif
		nratendimento = ft.u_nratend
		myftintroducao = .F.
		myftalteracao = .T.
		uf_facturacao_controlawklinhas()
		Select td
		If Len(Alltrim(td.tiposaft))>1
			For Each lccoluna In facturacao.pageframe1.page1.griddoc.Columns
				For Each lcobj In lccoluna.Controls
					Unbindevents(lcobj)
					Try
						lccoluna.Enabled = .F.
					Catch
					Endtry
				Endfor
			Endfor
			facturacao.pageframe1.page1.containerlinhas.btnaddlinha.Enable(.F.)
			facturacao.pageframe1.page1.containerlinhas.btndellinha.Enable(.F.)
			facturacao.menu1.estado("impDoc, dem", "HIDE")
			facturacao.menu_opcoes.estado("fichaUtente", "HIDE")
		Endif
		facturacao.pageframe1.ActivePage = facturacao.pageframe1.ActivePage
		fecha("uCrsFtOrig")
		Select ft.fno, ft.etotal, ft.fdata, ft.totqtt, edescc, ft2.motiseimp, ft.ncont, ft.Nome, ft.NO, fi.epv, fi.etiliquido, fi.iva, fi.tabiva, fi.desconto, fi.qtt, fi.fistamp From ft INNER Join fi On fi.ftstamp=ft.ftstamp INNER Join ft2 On ft2.ft2stamp=ft.ftstamp Into Cursor uCrsFtOrig Readwrite
	Endif
	myresizefacturacao = .F.
	uf_facturacao_expandegridlinhas()
	uf_facturacao_controlaobjectos()
Endfunc
**
Function uf_facturacao_eliminardoc
	Local lcvalida, lcsql
	If  .Not. Used("TD")
		uf_perguntalt_chama("N�o foi encontrada informa��o relativa ao tipo do documento."+Chr(13)+Chr(13)+"Por favor atualize o painel de fatura��o e tente novamente.", "OK", "", 64)
		Return .F.
	Endif
	If uf_gerais_getparameter_site('ADM0000000006', 'BOOL')
		If  .Not. Empty(Alltrim(uf_gerais_getparameter_site('ADM0000000007', 'text'))) .And. Dtoc(ft.fdata, 1)<uf_gerais_getparameter_site('ADM0000000007', 'text')
			uf_perguntalt_chama("N�o pode alterar/eliminar um documento com data inferior � data fechada para altera��es/elimina��es.", "OK", "", 64)
			Return .F.
		Endif
	Endif
	Select td
	If  .Not. Empty(Alltrim(td.tiposaft))
		uf_perguntalt_chama("N�O PODE APAGAR UM DOCUMENTO QUE FOI ASSINADO DIGITALMENTE DE ACORDO COM AS REGRAS DE CERTIFICA��O DE SOFTWARE.", "OK", "", 64)
		Return .F.
	Else
		Select ft
		If  .Not. Empty(ft.nmdoc)
			If  .Not. (uf_gerais_validapermuser(ch_userno, ch_grupo, Alltrim(ft.nmdoc)+' - Eliminar'))
				uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ELIMINAR ESTE TIPO DE DOCUMENTO.", "OK", "", 48)
				Return .F.
			Endif
			If uf_perguntalt_chama('Quer mesmo Eliminar o documento?'+Chr(13)+Chr(13)+'N�o ser� possivel recuperar os Dados.', "Sim", "N�o")
				lcsql = ""
				If  .Not. Empty(ft.u_ltstamp)
					Select ft2
					If  .Not. Empty(ft2.token) .And. Empty(ft2.u_docorig)
						uf_atendimento_receitasanular(ft2.u_receita, ft2.token)
					Endif
					Select ft2
					If !Empty(ft2.token_efectivacao_compl)
						uf_compart_anula(ft2.token_efectivacao_compl, ft2.ft2stamp)
					Endif
					Select ft
					TEXT TO lcsql TEXTMERGE NOSHOW
						DELETE FROM ctltrct WHERE ctltrctstamp='<<Alltrim(Ft.u_ltstamp)>>'
					ENDTEXT
					If  .Not. uf_gerais_actgrelha("", "", lcsql)
						uf_perguntalt_chama("OCORREU UM ERRO A ELIMINAR A RECEITA. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
						Return .F.
					Endif
				Endif
				lcsql = ""
				If  .Not. Empty(ft.u_ltstamp2)
					TEXT TO lcsql TEXTMERGE NOSHOW
						DELETE FROM ctltrct WHERE ctltrctstamp='<<Alltrim(ft.u_ltstamp2)>>'
					ENDTEXT
					If  .Not. uf_gerais_actgrelha("", "", lcsql)
						uf_perguntalt_chama("OCORREU UM ERRO A ELIMINAR A RECEITA. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
						Return .F.
					Endif
				Endif

				Select ft
				lcsql = ""
				TEXT TO lcsql TEXTMERGE NOSHOW
					exec up_facturacao_EliminaDoc '<<Alltrim(Ft.Ftstamp)>>'
				ENDTEXT
				If  .Not. uf_gerais_actgrelha("", "", lcsql)
					uf_perguntalt_chama("N�O FOI POSS�VEL ELIMINAR O DOCUMENTO. POR FAVOR CONTACTE O SUPORTE. COD. DELETEFT", "OK", "", 16)
					Return .F.
				Endif
				If Used("uCrsPesqFact")
					Select ucrspesqfact
					Locate For Alltrim(ucrspesqfact.cabstamp)==Alltrim(ft.ftstamp)
					Delete
					Goto Top
				Endif
				uf_facturacao_ultimoregisto()
			Endif
		Endif
	Endif
Endfunc
**
Function uf_facturacao_actualiza
	If  .Not. Used("Ft")
		Return .F.
	Endif
	Select ft
	Goto Top
	If  .Not. Empty(ft.ftstamp)
		uf_facturacao_chama(ft.ftstamp)
	Endif
Endfunc
**
Procedure uf_facturacao_registoanterior
	Select ft
	Local lcstampftactual, lcnovostamp
	Store '' To lcnovostamp
	lcstampftactual = Alltrim(ft.ftstamp)
	Local lcnext
	Store .F. To lcnext
	If (Used("ucrsPesqFact"))
		Select ucrspesqfact
		Goto Top
		Scan
			If Alltrim(lcstampftactual)==Alltrim(ucrspesqfact.cabstamp)
				lcnext = .T.
			Endif
			If lcnext=.F.
				lcnovostamp = Alltrim(ucrspesqfact.cabstamp)
				lcnext = .F.
			Endif
		Endscan
		If Empty(lcnovostamp)
			Select ucrspesqfact
			Goto Bottom
			lcnovostamp = ucrspesqfact.cabstamp
		Endif
	Endif
	If  .Not. Empty(Alltrim(lcnovostamp))
		uf_facturacao_chama(lcnovostamp)
	Endif
Endproc
**
Procedure uf_facturacao_registoseguinte
	Select ft
	Local lcstampftactual, lcnovostamp
	Store '' To lcnovostamp
	lcstampftactual = Alltrim(ft.ftstamp)
	Local lcnext
	Store .F. To lcnext
	If (Used("ucrsPesqFact"))
		Select ucrspesqfact
		Goto Top
		Scan
			If lcnext=.T.
				lcnovostamp = Alltrim(ucrspesqfact.cabstamp)
				lcnext = .F.
			Endif
			If Alltrim(lcstampftactual)==Alltrim(ucrspesqfact.cabstamp)
				lcnext = .T.
			Endif
		Endscan
		If Empty(lcnovostamp)
			Select ucrspesqfact
			Goto Top
			lcnovostamp = ucrspesqfact.cabstamp
		Endif
	Endif
	If  .Not. Empty(Alltrim(lcnovostamp))
		uf_facturacao_chama(lcnovostamp)
	Endif
Endproc
**
Procedure uf_facturacao_painel_excecao
	Local lcdem, lclote
	lcdem = .F.
	lclote = "0"
	If (Used("ft"))
		Select ft
		lclote = ft.u_tlote
		If (Empty(lclote))
			lclote = "0"
		Endif
		If (Val(lclote)>90)
			lcdem = .T.
		Endif
	Endif
	uf_atendimento_painel_excecao(lcdem)
Endproc
**
Procedure uf_facturacao_apagalinhadoc
	Local lcpromo, lcfipos, lclordem, uv_curFistamp
	Store 0 To lcfipos

	Select fi
	If fi.epromo
		lcpromo = .T.
	Endif
	facturacao.LockScreen = .T.
	Select fi
	uv_curFistamp = Alltrim(fi.fistamp)
	lclordem = fi.lordem
	Select Top 1 fistamp, lordem From fi Where fi.lordem>lclordem Order By fi.lordem Into Cursor ucrsNovoStamp Readwrite
	Local lcfistampant
	Select ucrsNovoStamp
	lcfistampant = Alltrim(ucrsNovoStamp.fistamp)


	If Used("FI2")
		Select FI2
		Go Top

		Delete From FI2 Where  uf_gerais_compStr(FI2.fistamp, uv_curFistamp)
	Endif


	If Used("fi_trans_info")
		Select fi_trans_info
		Go Top
		Locate For uf_gerais_compStr(fi_trans_info.recStamp, uv_curFistamp)
		If Found()
			Select fi_trans_info
			Delete
		Endif
	Endif
	Select fi
	Delete

	If lcpromo
		uf_atendimento_transformavaledc(.T., .T.)
	Else
		uf_atendimento_transformavaledc(.T., .T., .T.)
	Endif

	uf_atendimento_acttotaisft()

	If Reccount("ucrsNovoStamp")>0
		Select ucrsNovoStamp
		lcstampnovaposicao = ucrsNovoStamp.fistamp
		Select fi
		Locate For fi.fistamp=lcstampnovaposicao
		If Found()
		Else
			Select fi
			Goto Top
		Endif
	Else
		Select fi
		Goto Bottom
	Endif
	facturacao.pageframe1.page1.griddoc.Refresh
	facturacao.pageframe1.page1.griddoc.SetFocus
	facturacao.LockScreen = .F.
Endproc
**
Function uf_facturacao_importdocchama
	Select ft
	If Empty(Alltrim(ft.nmdoc))
		uf_perguntalt_chama("Para poder efectuar a importa��o seleccione primeiro o tipo de documento que pretende criar.", "OK", "", 64)
		Return .F.
	Endif
	If Empty(ft.NO)
		uf_perguntalt_chama("Para poder efectuar a importa��o seleccione primeiro o utente.", "OK", "", 64)
		Return .F.
	Endif
	uf_importarfact_chama(ft.NO, ft.tipodoc)
Endfunc
**
Procedure uf_facturacao_aplicadesccomercial
	If myftintroducao .Or. myftalteracao
		Local lcpos2, lcdesc, lcvalida
		Store 0 To lcpos2, lcdesc
		Store .F. To lcvalida
		Select fi
		lcpos2 = Recno("FI")
		Goto Top
		Scan
			If  .Not. Empty(fi.desconto)
				lcvalida = .T.
			Endif
		Endscan
		If lcvalida==.T.
			If  .Not. uf_perguntalt_chama("ATEN��O: ESTE DOCUMENTO J� TEM DESCONTOS PREENCHIDOS, PRETENDE ALTERAR?", "Sim", "N�o")
				Select fi
				If lcpos2>0
					Try
						Goto lcpos2
					Catch
					Endtry
				Endif
				Return
			Endif
		Endif
		lcdesc = Val(Inputbox('Qual o Desconto Pretendido:', '', '0.00'))
		Select fi
		Goto Top
		Scan
			Replace fi.desconto With Round(lcdesc, 2)
			uf_atendimento_fiiliq(.T.)
			uf_atendimento_acttotaisft()
		Endscan
		Select fi
		If lcpos2>0
			Try
				Goto lcpos2
			Catch
			Endtry
		Endif
	Endif
Endproc
**
Procedure uf_facturacao_expandegridlinhas
	Public myresizefacturacao
	If myresizefacturacao==.T.
		facturacao.pageframe1.page1.griddoc.Height = facturacao.pageframe1.page1.griddoc.Height-60
		facturacao.pageframe1.page1.containertotais.Visible = .T.
		facturacao.pageframe1.page1.containerlinhas.chkmostratotais.config(mypath+"\imagens\icons\pagedown_B.png", "")
		myresizefacturacao = .F.
	Else
		If facturacao.pageframe1.Height>facturacao.pageframe1.page1.griddoc.Height+60
			facturacao.pageframe1.page1.griddoc.Height = facturacao.pageframe1.page1.griddoc.Height+60
			facturacao.pageframe1.page1.containertotais.Visible = .F.
			facturacao.pageframe1.page1.containerlinhas.chkmostratotais.config(mypath+"\imagens\icons\pageup_B.png", "")
			myresizefacturacao = .T.
		Endif
	Endif
	facturacao.pageframe1.page1.containerlinhas.Top = facturacao.pageframe1.page1.griddoc.Height+30
Endproc
**
Procedure uf_facturacao_sair
	If myftalteracao==.T. .Or. myftintroducao==.T.
		If uf_perguntalt_chama('Quer mesmo cancelar o documento de Factura��o?'+Chr(13)+Chr(13)+'Ir� perder as �ltimas altera��es feitas', "Sim", "N�o")
			myftintroducao = .F.
			myftalteracao = .F.
			uf_facturacao_ultimoregisto()
			uf_facturacao_controlaobjectos()
			If (Used("mixed_bulk_pend"))
				Delete From mixed_bulk_pend
			Endif
			If (Used("fi_trans_info"))
				Delete From fi_trans_info
			Endif
			Release myinserereceitacorrigida
			If Wexist("facturacao")
				facturacao.Refresh
			Endif
		Endif
	Else
		uf_facturacao_exit()
	Endif
Endproc
**
Procedure uf_facturacao_apagaregistosft
	Select ft
	Scan
		Delete
	Endscan
Endproc
**
Procedure uf_facturacao_apagaregistoslinhasfi
	Select fi
	Scan
		Delete
	Endscan
Endproc
**
Procedure uf_facturacao_wkcamposespecificos
	If Used("ucrsWkLinhasAux")
		Select ucrse1
		If Alltrim(Upper(ucrse1.tipoempresa))=="FARMACIA"
			Select ft
			If  .Not. Empty(ft.u_ltstamp) .Or.  .Not. Empty(ft.u_ltstamp2) .Or. (td.u_tipodoc==3 .And. (myftintroducao==.T. .Or. myftalteracao==.T.)) .Or. (td.u_tipodoc==9 .And. (myftintroducao==.T. .Or. myftalteracao==.T.)) .Or. (td.u_tipodoc==8 .And. (myftintroducao==.T. .Or. myftalteracao==.T.)) .Or. (td.u_tipodoc==7 .And. (myftintroducao==.T. .Or. myftalteracao==.T.)) .Or. (td.u_tipodoc==2 .And. (myftintroducao==.T. .Or. myftalteracao==.T.))
				Select ucrswklinhasaux
				Goto Top
				Select ucrswklinhasaux
				Locate For Upper(Alltrim(ucrswklinhasaux.wkfield))=="FI.REF"
				If Found()
					lcordem = ucrswklinhasaux.ordem
					Update ucrswklinhasaux Set ordem = ordem+1 Where ordem>lcordem
					Select ucrswklinhasaux
					Append Blank
					Replace ucrswklinhasaux.wkreadonly With .T.
					Replace ucrswklinhasaux.wkfield With "FI.CNPEM"
					Replace ucrswklinhasaux.wktitle With "CNPEM"
					Replace ucrswklinhasaux.ordem With lcordem+1
					Replace ucrswklinhasaux.largura With 90
				Endif
			Endif
		Endif
	Endif
Endproc
**
Function uf_facturacao_controlawklinhas
	Local ti, i, lcvalorquabraold, lcvalorccantes
	Store '' To lcvalorquabraold, lcvalorccantes
	Select ft
	Goto Top
	lcsql = ""
	TEXT TO lcsql TEXTMERGE NOSHOW
		exec up_facturacao_wk '<<Alltrim(Ft.nmdoc)>>'
	ENDTEXT
	facturacao.pageframe1.page1.griddoc.ColumnCount = 0
	Select fi
	Goto Top
	facturacao.pageframe1.page1.griddoc.RecordSource = "FI"
	If  .Not. uf_gerais_actgrelha("", "ucrsWkLinhasAux", lcsql)
		uf_perguntalt_chama("N�O FOI POSSIVEL IDENTIFICAR A CONFIGURA��O WORKFLOW ASSOCIADO AO DOCUMENTO.", "OK", "", 16)
		Return .F.
	ENDIF
	

	
	uf_facturacao_wkcamposespecificos()
	Select ucrswklinhasaux
	Goto Top
	Select * From ucrswklinhasaux Order By ordem Into Cursor ucrswklinhas Readwrite
	If Used("ucrsWkLinhasAux")
		fecha("ucrsWkLinhasAux")
	Endif
	Local lctabivaplinha
	i = 1
	Select ucrswklinhas
	Goto Top
	Scan
		If Left(Upper(Alltrim(ucrswklinhas.wkfield)), 2)=="FI" .And. Upper(Alltrim(ucrswklinhas.wkfield))<>'FI.U_ROBOT'  .And. Upper(Alltrim(ucrswklinhas.wkfield))<>'FI.NCINTEG'
			If Type(ucrswklinhas.wkfield)=='C' .Or. Type(ucrswklinhas.wkfield)=='N' .Or. Type(ucrswklinhas.wkfield)=='L'
				With facturacao.pageframe1.page1.griddoc
					.AddColumn
					.RowHeight = 30
					.Columns(i).ControlSource = Alltrim(ucrswklinhas.wkfield)
					.Columns(i).FontName = "Verdana"
					.Columns(i).FontSize = 9
					.Columns(i).header1.FontName = "Verdana"
					.Columns(i).ReadOnly = ucrswklinhas.wkreadonly
					.Columns(i).text1.ReadOnly = ucrswklinhas.wkreadonly
					If Upper(Alltrim(ucrswklinhas.wkfield))=="FI.LOTE"
						.Columns(i).DynamicBackColor = "IIF(fi.usalote and EMPTY(fi.lote),rgb[232,76,61],IIF(fi.alertaLote,rgb[232,76,61],IIF(recno()%2!=0,Rgb[255,255,255],rgb[240,240,240])))"
					Else
						.Columns(i).DynamicBackColor = "IIF(recno()%2!=0,Rgb[255,255,255],rgb[240,240,240])"
					Endif
					If Upper(Alltrim(ucrswklinhas.wkfield))=="FI.U_EPVP"
						.Columns(i).DynamicBackColor = "IIF(fi.AlertaPic, rgb[232,76,61],IIF(recno()%2!=0,Rgb[255,255,255],rgb[240,240,240]))"
						.Columns(i).DynamicForeColor = "IIF(fi.AlertaPic, rgb[255,255,255],rgb[0,0,0])"
					Else
						.Columns(i).DynamicBackColor = "IIF(recno()%2!=0,Rgb[255,255,255],rgb[240,240,240])"
					Endif
					.Columns(i).DynamicFontBold = "FI.PESQUISA"
					If ucrswklinhas.largura<>0
						.Columns(i).Width = ucrswklinhas.largura
					Endif
					If  .Not. Empty(ucrswklinhas.mascara)
						.Columns(i).InputMask = Alltrim(ucrswklinhas.mascara)
						.Columns(i).text1.InputMask = Alltrim(ucrswklinhas.mascara)
					Endif
					.Columns(i).header1.Caption = Alltrim(ucrswklinhas.wktitle)
					.Columns(i).header1.Alignment = 2
					.Columns(i).text1.BorderStyle = 0
					.Columns(i).text1.ForeColor = Rgb(0, 0, 0)
					.Columns(i).text1.SelectedForeColor = Rgb(0, 0, 0)
					.Columns(i).text1.ControlSource = Alltrim(ucrswklinhas.wkfield)
					.Columns(i).Format = "RTK"
					.Columns(i).text1.Format = "RTK"
					.Columns(i).text1.HideSelection = .T.
					.Columns(i).ColumnOrder = ucrswklinhas.ordem
					If Type(ucrswklinhas.wkfield)=='L'
						.Columns(i).RemoveObject("Text1")
						.Columns(i).AddObject("Check1", "Checkbox")
						.Columns(i).check1.Caption = ""
						.Columns(i).Alignment = 2
						.Columns(i).check1.Alignment = 2
						.Columns(i).Sparse = .F.
						.Columns(i).Visible = .T.
						.Columns(i).check1.SpecialEffect = 1
						.Columns(i).check1.ColorSource = 0
						.Columns(i).check1.Style = 1
						.Columns(i).check1.Picture = mypath+"\imagens\icons\unchecked_b_24.bmp"
						.Columns(i).check1.DownPicture = mypath+"\imagens\icons\checked_b_24.bmp"
						.Columns(i).check1.SpecialEffect = 1
						.Columns(i).check1.Themes = .F.
					Endif
				Endwith
			Else
				uf_perguntalt_chama("N�O FOI POSSIVEL APRESENTAR O CAMPO: "+Alltrim(ucrswklinhas.wkfield)+".", "OK", "", 48)
			Endif
		ELSE
	
			If Upper(Alltrim(ucrswklinhas.wkfield))=="WKORIGENS"
				With facturacao.pageframe1.page1.griddoc
					.AddColumn
					If ucrswklinhas.largura<>0
						.Columns(i).Width = ucrswklinhas.largura
					Endif
					.Columns(i).header1.Caption = Alltrim(ucrswklinhas.wktitle)
					.Columns(i).header1.Alignment = 2
					.Columns(i).AddObject('ORIGENS', 'CommandButtonOrigens')
					.Columns(i).origens.Visible = .T.
					.Columns(i).Sparse = .F.
					.Columns(i).Bound = .F.
					.Columns(i).origens.Enabled = .T.
					.Columns(i).CurrentControl = 'ORIGENS'
					.Columns(i).origens.SpecialEffect = 1
					.Columns(i).origens.Picture = mypath+"\imagens\icons\left_b_24.bmp"
					.Columns(i).origens.PicturePosition = 14
					.Columns(i).ColumnOrder = ucrswklinhas.ordem
				Endwith
			Endif
			If Upper(Alltrim(ucrswklinhas.wkfield))=="WKDESTINOS"
				With facturacao.pageframe1.page1.griddoc
					.AddColumn
					If ucrswklinhas.largura<>0
						.Columns(i).Width = ucrswklinhas.largura
					Endif
					.Columns(i).header1.Caption = Alltrim(ucrswklinhas.wktitle)
					.Columns(i).header1.Alignment = 2
					.Columns(i).AddObject('DESTINOS', 'CommandButtonDestinos')
					.Columns(i).destinos.Visible = .T.
					.Columns(i).Sparse = .F.
					.Columns(i).Bound = .F.
					.Columns(i).destinos.Enabled = .T.
					.Columns(i).CurrentControl = 'DESTINOS'
					.Columns(i).destinos.SpecialEffect = 1
					.Columns(i).destinos.Picture = mypath+"\imagens\icons\right_b_24.bmp"
					.Columns(i).destinos.PicturePosition = 14
					.Columns(i).ColumnOrder = ucrswklinhas.ordem
				Endwith
			Endif
			If Upper(Alltrim(ucrswklinhas.wkfield))=="FI.U_ROBOT"
				With facturacao.pageframe1.page1.griddoc
					.AddColumn
					If ucrswklinhas.largura<>0
						.Columns(i).Width = ucrswklinhas.largura
					Endif
					.Columns(i).header1.Caption = Alltrim(ucrswklinhas.wktitle)
					.Columns(i).header1.Alignment = 2
					.Columns(i).header1.FontName = "Verdana"
					.Columns(i).FontName = "Verdana"
					.Columns(i).AddObject('MANIPULAVALORES', 'commandButtonMV')
					.Columns(i).manipulavalores.Visible = .T.
					.Columns(i).Sparse = .F.
					.Columns(i).Bound = .F.
					.Columns(i).manipulavalores.Enabled = .T.
					.Columns(i).CurrentControl = 'MANIPULAVALORES'
					.Columns(i).manipulavalores.Caption = 'M.V.'
					.Columns(i).ColumnOrder = ucrswklinhas.ordem
				Endwith
			ENDIF
			If Upper(Alltrim(ucrswklinhas.wkfield))=="FI.NCINTEG"
			
				With facturacao.pageframe1.page1.griddoc
					.AddColumn
					If ucrswklinhas.largura<>0
						.Columns(i).Width = ucrswklinhas.largura
					Endif
					.Columns(i).header1.Caption = Alltrim(ucrswklinhas.wktitle)
					.Columns(i).header1.Alignment = 2
					.Columns(i).header1.FontName = "Verdana"
					.Columns(i).FontName = "Verdana"
					.Columns(i).AddObject('ALTERNATIVOS', 'commandButtonAlt')
					.Columns(i).alternativos.Visible = .T.
					.Columns(i).Sparse = .F.
					.Columns(i).Bound = .F.
					.Columns(i).alternativos.Enabled = .T.
					.Columns(i).CurrentControl = 'ALTERNATIVOS'
					.Columns(i).alternativos.Caption = 'ALT.'
					.Columns(i).ColumnOrder = ucrswklinhas.ordem
				Endwith
			Endif
		Endif
		With facturacao.pageframe1.page1.griddoc
			If Upper(Alltrim(ucrswklinhas.wkfield))=="FI.LOTE"
				.Columns(i).Visible = .T.
			Endif
		Endwith
		i = i+1
	Endscan
	uf_facturacao_controlaeventoslinhas()
	facturacao.Refresh
Endfunc
**
Function uf_facturacao_guardadadospsico
	Local lcposfi
	Local lcvalpsigrava
	lcvalpsigrava = .F.
	lcposfi = Recno("FI")
	Select fi
	Goto Top
	Scan
		If fi.u_psico
			lcvalpsigrava = .T.
			Exit
		Endif
	Endscan
	If (lcvalpsigrava==.T.)
		lcsql = ''
		TEXT TO lcsql TEXTMERGE NOSHOW
			SELECT * FROM dispensa_eletronica_cc (nolock) WHERE doc_id = '<<ALLTRIM(dadosPsico.u_ndutavi)>>'
		ENDTEXT
		If  .Not. uf_gerais_actgrelha("", "uCrsAuxUtente", lcsql)
			uf_perguntalt_chama("Ocorreu uma anomalia ao verificar a informa��o do utentes. Por favor contacte o Suporte.", "OK", "", 16)
			Return .F.
		Else
			If Reccount("uCrsAuxUtente")==0
				lcsql = ''
				TEXT TO lcsql TEXTMERGE NOSHOW
					insert into dispensa_eletronica_cc (id, id_emp, nome, morada, nif, nrSNS, sexo, nascimento, validade_fim, publicKey, site, machine, doc_id, tipoDoc)
               		values (LEFT(NEWID(),20), '<<ALLTRIM(uCrsE1.id_lt)>>', '<<ALLTRIM(ft.nome)>>', '<<ALLTRIM(ft.morada)>>', '<<ALLTRIM(ft.ncont)>>', '<<ALLTRIM(ft.nbenef)>>', '', '<<uf_gerais_getdate(ft.nascimento,"SQL")>>', '<<uf_gerais_getdate(dadosPsico.u_ddutavi, "SQL")>>', '','<<ALLTRIM(mySite)>>', '','<<ALLTRIM(dadosPsico.u_ndutavi)>>', '<<ALLTRIM(dadosPsico.codigoDocSPMS)>>')
				ENDTEXT
				If  .Not. uf_gerais_actgrelha("", "uCrsAuxInsertUtente", lcsql)
					uf_perguntalt_chama("Ocorreu uma anomalia ao inserir a informa��o detalhada do utente. Por favor contacte o Suporte.", "OK", "", 16)
					Return .F.
				Endif
			Else
				lcsql = ''
				TEXT TO lcsql TEXTMERGE NOSHOW

					update dispensa_eletronica_cc
					SET validade_fim = '<<uf_gerais_getdate(dadosPsico.u_ddutavi, "SQL")>>',nome = '<<ALLTRIM(dadosPsico.u_nmutavi)>>', morada = '<<ALLTRIM(dadosPsico.u_moutavi)>>'
					,doc_id = '<<ALLTRIM(dadosPsico.u_ndutavi)>>', nascimento ='<<uf_gerais_getdate(ft.nascimento,"SQL")>>', tipoDoc = '<<ALLTRIM(dadosPsico.codigoDocSPMS)>>'
					where doc_id = '<<ALLTRIM(dadosPsico.u_ndutavi)>>'

				ENDTEXT
				If  .Not. uf_gerais_actgrelha("", "uCrsAuxInsertUtente", lcsql)
					uf_perguntalt_chama("Ocorreu uma anomalia ao atualizar a informa��o detalhada do utente. Por favor contacte o Suporte.", "OK", "", 16)
					Return .F.
				Endif
			Endif
		Endif
	Endif
	Select fi
	If lcposfi>0
		Try
			Goto lcposfi
		Catch
		Endtry
	Endif
	Return .T.
Endfunc
**

Function uf_faturacao_libertaPagCentral
	Lparameters llatend
	Local lcsql


	If !Empty(llatend)
		lcsql = ''
		TEXT TO lcsql TEXTMERGE NOSHOW
				DELETE FROM B_pagCentral WHERE nrAtend = '<<ALLTRIM(llatend)>>' AND nrAtend!=''	AND convert(date,getdate()) = convert(date,oData)
		ENDTEXT
		If  .Not. uf_gerais_actgrelha("", "", lcsql)
			uf_perguntalt_chama("Ocorreu uma anomalia a remover o pagamento. Por favor contacte o Suporte.", "OK", "", 16)
			Return .F.
		Endif
	Endif

Endfunc

Function uf_facturacao_gravar
	Lparameters llatend
	uf_chk_nova_versao()
	Local lcvalida, lcsqlft2, lcsqlft, lcsqlfi, lcsqlinsertdoc, lcvalidainsercaodocumento, lcsqlupdatedoc, lcsqlcert, lctotprod, lctotserv, lctpempr, lcsqlcompartexterna
	Store .F. To lcvalida, lcvalidainsercaodocumento
	Store "" To lcsqlft2, lcsqlft, lcsqlfi, lcsqlfi2, lcsqlinsertdoc, lcsqlupdatedoc, lcsqlcert, lcsqlcompartexterna




	Local lcPlanoTemp, lcFtNdoc
	lcPlanoTemp = ""
	lcFtNdoc = 0

	If(Used("ft2"))
		Select ft2
		Go Top
		lcPlanoTemp = Alltrim(ft2.u_codigo)
	Endif


	If(Used("ft"))
		Select ft
		Go Top
		lcFtNdoc = ft.ndoc
	Endif


	If !(Used("TD"))
		lcsql = ''
		TEXT TO lcsql TEXTMERGE NOSHOW
				SELECT * FROM td WHERE td.ndoc = <<ft.ndoc>>
		ENDTEXT
		If  .Not. uf_gerais_actgrelha("", "TD", lcsql)
			uf_perguntalt_chama("OCORREU UMA ANOMALIA A GRAVAR O DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
			Return .F.
		Endif
	Endif




	Local lcValidaLoteVet
	Store '' To lcValidaLoteVet

	lcValidaLoteVet = uf_atendimento_validaLotesVet()

	If !Empty(lcValidaLoteVet)
		uf_perguntalt_chama("O produto " + Alltrim(lcValidaLoteVet) + " necessita ter os campos Lote e Validade preenchidos. Por favor valide.", "OK", "", 16)
		Return .F.
	Endif




	** N�o pode ser chamado nas reinsercoes de receita, visto que ir� calcular o pref � data actual
	If(uf_facturacao_validaSeDeveRecalcularValores(.F.,lcPlanoTemp,lcFtNdoc ) )
		uf_atendimento_actvaloresfi()
		uf_atendimento_acttotaisft()
	Endif

	If Wexist("facturacao")
		facturacao.Refresh
	Endif

	lctotprod = 0
	lctotserv = 0
	lctpempr = ''
	TEXT TO lcsql TEXTMERGE NOSHOW
		select top 1 tipoempresa from empresa (nolock)
	ENDTEXT
	uf_gerais_actgrelha("", "uCrstpempr", lcsql)
	lctpempr = Alltrim(ucrstpempr.tipoempresa)
	Select ucrse1
	If Len(Alltrim(ucrse1.motivo_isencao_iva))=0
		uf_perguntalt_chama("N�O EXISTE MOTIVO DE ISEN��O DE IVA POR DEFEITO PREENCHIDO NA FICHA DA EMPRESA. POR FAVOR ATUALIZE PARA PODER FINALIZAR O ATENDIMENTO", "OK", "", 48)
		Return .F.
	Endif
	If uf_gerais_getparameter_site('ADM0000000006', 'BOOL')
		If  .Not. Empty(Alltrim(uf_gerais_getparameter_site('ADM0000000007', 'text'))) .And. Dtoc(ft.fdata, 1)<uf_gerais_getparameter_site('ADM0000000007', 'text')
			uf_perguntalt_chama("N�o pode inserir um documento com data inferior � data fechada para altera��es/elimina��es.", "OK", "", 64)
			Return .F.
		Endif
	Endif

	Select ft

	If !uf_faturacao_valida_obito(.T.)
		Return .F.
	Endif

	uv_valPlf = .T.

	Select fi
	Go Top
	Locate For !Empty(fi.bistamp)
	If Found()
		uv_valPlf = .F.
	Endif

	If uf_gerais_getparameter_site('ADM0000000079', 'BOOL', mysite) And td.u_tipodoc = 9 And !Wexist("ATENDIMENTO") And uv_valPlf
		If !uf_pesqdocumentosclientes_validaPlfCl(ft.NO, ft.estab, ft.etotal, .T.)
			Return .F.
		Endif
	Endif

	If(Used("ft2"))
		If(!uf_comunicasinave_validaNumeroReceitaSinave(Alltrim(ft2.u_receita),Alltrim(ft2.u_codigo)))
			uf_perguntalt_chama("Estrutura de receita sinave inv�lida", "OK", "", 32)
			Return .F.
		Endif
	Endif



	If uf_gerais_getparameter("ADM0000000348", "BOOL")
		Select * From fi With (Buffering = .T.) Into Cursor uc_tmpFI

		Select uc_tmpFI
		Go Top
		Scan For !Empty(uc_tmpFI.ref) And uc_tmpFI.qtt <> 0 And uc_tmpFI.u_epvp = 0 And !uc_tmpFI.u_comp

			If !uf_gerais_getUmValor("st", "qlook", "ref = '" + uc_tmpFI.ref + "' and inactivo = 0 and site_nr = " + ASTR(mySite_nr))
				uf_perguntalt_chama("Existem artigos/servi�os com o PVP a 0. Assim n�o pode continuar.", "OK", "", 48)
				Return .F.
			Endif

			Select uc_tmpFI
		Endscan

		fecha("uc_tmpFI")
	Endif

	*!*	 if uf_atendimento_validaBasComComplementar()
	*!*	    uf_perguntalt_chama("N�o pode efetuar vendas com a Portaria n.� 66/2023, de 6 de mar�o. (BAS) e complementariadade", "OK", "", 16)
	*!*	    RETURN .F.
	*!*	 ENDIF

	If !Wexist("ATENDIMENTO")
		Select ft2
		Go Top
		** N�o validar as linhas no PEMH
		If !uf_gerais_compStr(uf_gerais_retornaTipoDispensaEletronica(ft2.u_receita),"2") And !uf_facturacao_linhasdem()
			Return .F.
		Endif

	Endif

	Select ft2
	If !Empty(ft2.u_codigo)

		Select fi
		Go Top
		Scan For !Empty(fi.ref) And fi.qtt <> 0

			If !uf_faturacao_checkLenEmb(Alltrim(ft2.u_codigo), Alltrim(fi.U_CODEMB))
				Return .F.
			Endif

		Endscan
	Endif


	&&valida se mcdt guia ou prestacao
	Local lcCodigoTemp

	Select ft2
	Go Top
	lcCodigoTemp = Alltrim(ft2.u_codigo)

	fecha("uCrsTempComp")

	If  .Not. uf_gerais_actgrelha("", 'uCrsTempComp', [exec up_receituario_dadosDoPlano ']+lcCodigoTemp+['])
		uf_perguntalt_chama("OCORREU UMA ANOMALIA A APLICAR O PLANO.", "OK", "", 16)
		Return .F.
	Endif





	If(Used("uCrsTempComp")  And  !Empty(uCrsTempComp.pergDem))
		Select fi
		Go Top
		uf_atendimento_perguntaSeReceitaRSP(fi.lordem)
	Endif



	fecha("uCrsTempComp")


	** uf_documentos_removelinhasvazias()
	If uf_gerais_getparameter_site('ADM0000000054', 'BOOL', mysite)=.T. .And. Alltrim(ch_grupo)<>'Administrador' .And. Alltrim(ch_grupo)<>'Supervisores' .And. (Alltrim(td.tiposaft)='FT' .Or. Alltrim(td.tiposaft)='NC' .Or. Alltrim(td.tiposaft)='ND')
		If  .Not. uf_gerais_valida_password_admin('FATURA��O', 'ALTERA��O DOC. PARA CR�DITO')
			Return .F.
		Endif
	Endif
	Select td
	If td.lancasl=.T. .And.  .Not. uf_faturacao_valida_artigo_sem_stock()
		Return .F.
	Endif
	lcmoedadefault = uf_gerais_getparameter("ADM0000000260", "text")
	If Empty(lcmoedadefault)
		If (Alltrim(ft.ncont)="999999990" .Or. Empty(ft.ncont)) .And. (Alltrim(ft.nmdoc)=='Factura' .Or. Alltrim(ft.nmdoc)='Venda a Dinheiro' .Or. Alltrim(ft.nmdoc)='Fatura Recibo') .And. Upper(Alltrim(ucrse1.pais))=='PORTUGAL'
			Select fi
			Goto Top
			Scan
				If  .Not. Empty(fi.ref) .And. fi.stns=.T.
					lctotserv = lctotserv+fi.etiliquido
				Endif
				If  .Not. Empty(fi.ref) .And. fi.stns=.F.
					lctotprod = lctotprod+fi.etiliquido
				Endif
			Endscan
			If lctpempr='FARMACIA'
				If lctotserv>100
					uf_perguntalt_chama("N�o pode efetuar documentos com valor de servi�os superior a 100� sem preencher o NIF do cliente. Por favor corrija.", "OK", "", 16)
					Return .F.
				Endif
				If lctotprod>1000
					uf_perguntalt_chama("N�o pode efetuar documentos com valor de artigos superior a 1000� sem preencher o NIF do cliente. Por favor corrija.", "OK", "", 16)
					Return .F.
				Endif
			Else
				If lctotserv+lctotprod>100
					uf_perguntalt_chama("N�o pode efetuar documentos com valor de superior a 100� sem preencher o NIF do cliente. Por favor corrija.", "OK", "", 16)
					Return .F.
				Endif
			Endif
		Endif
	Endif


	If  .Not. llatend
		facturacao.containercab.lote.SetFocus
	Endif
	If Upper(mypaisconfsoftw)=='ANGOLA'
		Local valmanintro
		Store .F. To valmanintro
		If Alltrim(ft.nmdoc)='Nt. Cr�d. Segur.' .Or. Alltrim(ft.nmdoc)='Nota de Cr�dito'
			Select fi
			Goto Top
			Scan
				If  .Not. Empty(fi.ref) .And. Empty(fi.ofistamp)
					valmanintro = .T.
				Endif
			Endscan
		Endif
		If valmanintro=.T.
			Public cvalnrdoc
			Store '' To cvalnrdoc
			uf_tecladoalpha_chama("cvalnrdoc", "Introduza o n� do documento origem:", .F., .F., 0)
			If Empty(cvalnrdoc)
				uf_perguntalt_chama("TEM DE INTRODUZIR OBRIGAT�RIAMENTE O N� DE DOCUMENTO DE ORIGEM! VOLTE A EFETUAR A DEVOLU��O.", "OK", "", 64)
				Return (.F.)
			Endif
			Select ft2
			Replace ft2.u_docorig With Alltrim(cvalnrdoc)
			uf_atendimento_adicionalinhafi(.T., .T.)
			Select fi
			Replace fi.lordem With 100
			Replace fi.Design With 'Anula��o Ft. Seguradoras Nr. '+Alltrim(cvalnrdoc)+' '
			If Alltrim(ft.nmdoc)='Nt. Cr�d. Segur.'
				Public cvalnrdoc
				Store '' To cvalnrdoc
				uf_tecladoalpha_chama("cvalnrdoc", "Introduza o Nome do Segurado", .F., .F., 0)
				If Empty(cvalnrdoc)
					uf_perguntalt_chama("TEM DE INTRODUZIR OBRIGAT�RIAMENTE O NOME DO SEGURADO! VOLTE A GRAVAR A DEVOLU��O.", "OK", "", 64)
					Return (.F.)
				Endif
				Select ft2
				Replace ft2.contacto With Alltrim(cvalnrdoc)
				uf_tecladoalpha_chama("cvalnrdoc", "Introduza o Nr. do Cart�o da Seguradora", .F., .F., 0)

				If Empty(cvalnrdoc)
					uf_perguntalt_chama("TEM DE INTRODUZIR OBRIGAT�RIAMENTE O N� DO CART�O! VOLTE A GRAVAR A DEVOLU��O.", "OK", "", 64)
					Return (.F.)
				Endif

				Select ft2
				If !Empty(ft2.u_codigo)
					uv_regex = uf_gerais_getUmValor("cptpla", "regexCartao", "codigo = '" + Alltrim(ft2.u_codigo) + "'")

					If !Empty(uv_regex)

						If !uf_gerais_checkRegex(Alltrim(uv_regex), Alltrim(cvalnrdoc))

							uv_sample = uf_gerais_getUmValor("cptpla", "regexCartaoSample", "codigo = '" + Alltrim(ft2.u_codigo) + "'")
							uf_perguntalt_chama("Para esta comparticipa��o o N� do Cart�o ter� de ter o seguinte formato: " + Chr(13) + Alltrim(uv_sample), "OK", "", 48)

							Return .F.

						Endif

					Endif

				Endif

				Select ft2
				Replace ft2.c2codpost With Alltrim(cvalnrdoc)
				uf_tecladoalpha_chama("cvalnrdoc", "Introduza o contacto do segurado", .F., .F., 0)
				Select ft2
				Replace ft2.telefone With Alltrim(cvalnrdoc)
			Endif
		Endif
	Endif



	myfactagravar = .T.
	Local lcvalpsigrava
	Store .F. To lcvalpsigrava
	If  .Not. Used("TD")
		Select ft
		Select * From uCrsGeralTD Where Upper(Alltrim(uCrsGeralTD.nmdoc))==Upper(Alltrim(facturacao.containercab.nmdoc.Value)) And Alltrim(uCrsGeralTD.site)==Alltrim(mysite) Into Cursor td Readwrite
	Endif
	If Used("td")
		Select td
		If td.u_integra==.T.
			uf_perguntalt_chama("ESTA S�RIE DE DOCUMENTO EST� CONFIGURADA COMO SENDO DE INTEGRA��O. N�O � POSS�VEL CRIAR DOCUMENTOS DESTE TIPO.", "OK", "", 48)
			Return .F.
		Endif
	Else
		uf_perguntalt_chama("O CURSOR TD N�O EST� CARREGADO.", "OK", "", 48)
		Return .F.
	Endif
	If (uf_facturacao_guardadadospsico()==.F.)
		Return .F.
	Endif
	If uf_facturacao_validacamposobrigatorios()==.F.
		Return .F.
	Endif



	Local lcNrCartao
	Store '' To lcNrCartao
	lcsql = ''
	TEXT TO lcsql TEXTMERGE NOSHOW
 	SELECT nrcartao FROM b_fidel WHERE clno=<<ft.no>> and clestab=<<ft.estab>>
	ENDTEXT
	uf_gerais_actgrelha("", "ucrsverifcartao", lcsql)
	If Len(Alltrim(ucrsverifcartao.nrcartao))>0
		lcNrCartao = Alltrim(ucrsverifcartao.nrcartao)
	Endif
	fecha("ucrsverifcartao")
	Select ft
	If ft.NO>200 And lcNrCartao <> ''
		uf_facturacao_calcular_valor_cartao()
	Endif






	&& vai ser aqui a valida��o
	If myftintroducao=.T.

		lcvalida = uf_facturacao_regrasgravacao(llatend)

		If lcvalida
			If !uf_faturacao_verificaCaixas()
				Return .F.
			Endif
		Else
			Return .F.
		Endif

		** vendas a dinheiro no bakcoffice
		If  .Not. (Type("FACTURACAO")=="U")
			If facturacao.pageframe1.page2.movcaixa.Tag=="true"
				Select td
				If td.u_tipodoc==8
					If Empty(mycxstamp)
						uf_gestaocaixas_chama(.T.)
						If(Used("FT"))
							Select ft
							uf_faturacao_libertaPagCentral(ft.u_nratend)
						Endif
						Return .F.
					Endif
					uf_mrpagamento_chama("FACTURACAO")
					Public mymrpagamentocancelou, mymrpagamentopagou
					If Empty(mymrpagamentopagou)
						If(Used("FT"))
							Select ft
							uf_faturacao_libertaPagCentral(ft.u_nratend)
						Endif
						Return .F.
					Endif
				Endif
			Endif
		Endif




		uf_pagamento_sincronizaFiComFi2()




		Select fi
		Goto Top
		Scan
			If  .Not. Empty(Alltrim(td.tiposaft)) .And. fi.iva==0 .And. Empty(ft2.motiseimp)
				Do Case
					Case ft.pais==3
						Update ft2 Set motiseimp = "Isento Artigo 14. do CIVA (ou similar)"
					Case ft.pais==2
						Update ft2 Set motiseimp = "Isento Artigo 14. do RITI (ou similar)"
					Otherwise
						If Empty(ucrse1.motivo_isencao_iva)
							Update ft2 Set motiseimp = "Isento Artigo 9. do CIVA (ou similar)"
						Else
							Update ft2 Set motiseimp = Alltrim(ucrse1.motivo_isencao_iva)
						Endif
				Endcase
			Endif
		Endscan
		Select fi
		Scan For fi.amostra==.T.
			uf_gerais_registaocorrencia('Vendas', 'Altera��o de PVP na Venda (Back)', 2, Alltrim(fi.ref)+' PVP: '+ASTR(fi.epvori, 8, 2), Alltrim(fi.ref)+' PVP: '+ASTR(fi.u_epvp, 8, 2), Alltrim(fi.fistamp), '', '', '')
		Endscan
		If lcvalida==.T.

			regua(0, 100, "A PROCESSAR A GRAVA��O DO DOCUMENTO...", .F.)
			Select ft
			Set Hours To 24
			atime = Ttoc(Datetime()+(difhoraria*3600), 2)
			ASTR = Left(atime, 8)
			myhora = ASTR
			myinvoicedata = uf_gerais_getdate(ft.fdata, "SQL")
			If  .Not. Used("uCrsValIva")
				Create Cursor uCrsValIva (lceivain1 NUMERIC(19, 2), lceivain2 NUMERIC(19, 2), lceivain3 NUMERIC(19, 2), lceivain4 NUMERIC(19, 2), lceivain5 NUMERIC(19, 2), lceivain6 NUMERIC(19, 2), lceivain7 NUMERIC(19, 2), lceivain8 NUMERIC(19, 2), lceivain9 NUMERIC(19, 2), lceivain10 NUMERIC(19, 2), lceivain11 NUMERIC(19, 2), lceivain12 NUMERIC(19, 2), lceivain13 NUMERIC(19, 2), lceivav1 NUMERIC(19, 2), lceivav2 NUMERIC(19, 2), lceivav3 NUMERIC(19, 2), lceivav4 NUMERIC(19, 2), lceivav5 NUMERIC(19, 2), lceivav6 NUMERIC(19, 2), lceivav7 NUMERIC(19, 2), lceivav8 NUMERIC(19, 2), lceivav9 NUMERIC(19, 2), lceivav10 NUMERIC(19, 2), lceivav11 NUMERIC(19, 2), lceivav12 NUMERIC(19, 2), lceivav13 NUMERIC(19, 2))
			Else
				fecha("uCrsValIva")
				Create Cursor uCrsValIva (lceivain1 NUMERIC(19, 2), lceivain2 NUMERIC(19, 2), lceivain3 NUMERIC(19, 2), lceivain4 NUMERIC(19, 2), lceivain5 NUMERIC(19, 2), lceivain6 NUMERIC(19, 2), lceivain7 NUMERIC(19, 2), lceivain8 NUMERIC(19, 2), lceivain9 NUMERIC(19, 2), lceivain10 NUMERIC(19, 2), lceivain11 NUMERIC(19, 2), lceivain12 NUMERIC(19, 2), lceivain13 NUMERIC(19, 2), lceivav1 NUMERIC(19, 2), lceivav2 NUMERIC(19, 2), lceivav3 NUMERIC(19, 2), lceivav4 NUMERIC(19, 2), lceivav5 NUMERIC(19, 2), lceivav6 NUMERIC(19, 2), lceivav7 NUMERIC(19, 2), lceivav8 NUMERIC(19, 2), lceivav9 NUMERIC(19, 2), lceivav10 NUMERIC(19, 2), lceivav11 NUMERIC(19, 2), lceivav12 NUMERIC(19, 2), lceivav13 NUMERIC(19, 2))
			Endif
			Select uCrsValIva
			Append Blank
			Replace uCrsValIva.lceivain1 With ft.eivain1, uCrsValIva.lceivain2 With ft.eivain2, uCrsValIva.lceivain3 With ft.eivain3, uCrsValIva.lceivain4 With ft.eivain4, uCrsValIva.lceivain5 With ft.eivain5, uCrsValIva.lceivain6 With ft.eivain6, uCrsValIva.lceivain7 With ft.eivain7, uCrsValIva.lceivain8 With ft.eivain8, uCrsValIva.lceivain9 With ft.eivain9, uCrsValIva.lceivain10 With ft.eivain10, uCrsValIva.lceivain11 With ft.eivain11, uCrsValIva.lceivain12 With ft.eivain12, uCrsValIva.lceivain13 With ft.eivain13, uCrsValIva.lceivav1 With ft.eivav1, uCrsValIva.lceivav2 With ft.eivav2, uCrsValIva.lceivav3 With ft.eivav3, uCrsValIva.lceivav4 With ft.eivav4, uCrsValIva.lceivav5 With ft.eivav5, uCrsValIva.lceivav6 With ft.eivav6, uCrsValIva.lceivav7 With ft.eivav7, uCrsValIva.lceivav8 With ft.eivav8, uCrsValIva.lceivav9 With ft.eivav9, uCrsValIva.lceivav10 With ft.eivav10, uCrsValIva.lceivav11 With ft.eivav11, uCrsValIva.lceivav12 With ft.eivav12, uCrsValIva.lceivav13 With ft.eivav13
			lcsqlft2 = uf_pagamento_gravarvendaft2(ft.ftstamp, ft.ndoc, ft2.u_receita, ft2.u_codigo, ft2.u_codigo2, ft2.u_design, ft2.u_design2, ft2.u_abrev, ft2.u_abrev2, myhora, ft2.codacesso, ft2.coddiropcao, ft2.token, ft2.token_efectivacao_compl)
			lcsqlft2 = uf_gerais_trataplicassql(lcsqlft2)
			TEXT TO lcsqlftdeclare TEXTMERGE NOSHOW
				DECLARE @fno as numeric(9,0)
				DECLARE @tabNewDocNr TABLE (newDocNr NUMERIC(10))
			ENDTEXT
			lcsqlft = uf_pagamento_gravarvendaft(ft.ftstamp, ft.nmdoc, ft.ndoc, ft.tipodoc, ft.NO, ft.Nome, ft.estab, ft.totqtt, ft.qtt1, ft.etotal, ft.ettiliq, ft.Custo, ft.ettiva, ft.edescc, ft.pdata, myhora, ft.cobrado, uf_gerais_getdate(ft.cdata, "SQL"), ft.ncont, uf_gerais_getdate(ft.bidata, "SQL"))
			lcsqlft = uf_gerais_trataplicassql(lcsqlftdeclare+lcsqlft)
			lcsqlfi = uf_pagamento_gravarvendafi(ft.ftstamp, ft.nmdoc, ft.ndoc, ft.tipodoc, ft.fno, myhora, .T.)
			lcsqlfi = uf_gerais_trataplicassql(lcsqlfi)

			Local lcndoc
			lcndoc = -1
			If ( .Not. Type("ft.numinternodoc")=="U")
				lcndoc = ft.numinternodoc
			Endif
			If ( .Not. Type("ft.ndoc")=="U")
				lcndoc = ft.ndoc
			Endif
			lcsqlcompartexterna = uf_facturacao_actualizacacompartexterna()
			lcsqlcompartexterna = uf_gerais_trataplicassql(lcsqlcompartexterna)


			If Type("FACTURACAO")<>"U"
				If Alltrim(Upper(facturacao.containercab.nmdoc.Value))<>"VENDA MANUAL"
					lcsqlcert = uf_pagamento_gravarcert(ft.ftstamp, myinvoicedata, myhora, lcndoc, Round(ft.etotal+ft.efinv, 2))
					lcsqlcert = uf_gerais_trataplicassql(lcsqlcert)
				Endif
			Endif
			lcsqlinsertdoc = lcsqlft2+lcsqlft+lcsqlfi+lcsqlcert+lcsqlcompartexterna

			If  .Not. Empty(lcsqlinsertdoc)
				lcsql = ''
				TEXT TO lcsql TEXTMERGE NOSHOW
					exec up_gerais_execSql 'Documentos - Insert', 1,'<<lcSQLInsertDoc>>', '', '', '' , '', ''
				ENDTEXT
				If  .Not. uf_gerais_actgrelha("", "", lcsql)
					uf_perguntalt_chama("OCORREU UMA ANOMALIA A GRAVAR O DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
					lcvalidainsercaodocumento = .F.
				Else
					lcvalidainsercaodocumento = .T.
				Endif
			Endif

			Select ft
			Try
				TEXT TO lcSql NOSHOW TEXTMERGE
				exec up_sem_picagem '<<ft.u_nratend>>', '<<mysite>>'
				ENDTEXT
				If !uf_gerais_actgrelha("","",lcsql)
					uf_perguntalt_chama("OCORREU UM ERRO A GUARDAR PRODUTOS SEM PICAGEM MVO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				Endif
			Catch
				uf_perguntalt_chama("GIULIANA A SP DEU ERRO!!","OK","",16)
			Endtry

			If (Type("myinserereceitacorrigida") <> "U" And myinserereceitacorrigida) Or (Type("myCorriguirReceita") <> 'U' And myCorriguirReceita)

				If !Used("ft2") Or !Used("ft")
					uf_perguntalt_chama("Ocorreu uma anomalia a validar os dados para os dados dos cart�es.", "OK", "", 16)
				Endif

				Local lcValorCartao
				Store '' To lcValorCartao
				If(Type("facturacao.pageframe1.page2.txtu_nbenef2")!='U')
					lcValorCartao = facturacao.pageframe1.page2.txtu_nbenef2.Value
				Endif

				If Used("ucrsTrackId")
					Select ucrsTrackId
					If Alltrim(ucrsTrackId.Id) != Alltrim(lcValorCartao)
						uf_facturacaoChamaValidaCartao(lcValorCartao)
						uf_faturacao_getCartoes(ft2.u_nbenef2,ft2.u_abrev,nratendimento)
					Else
						Select ft
						uf_faturacao_insertExtCards(nratendimento)
					Endif
				Endif


				uf_faturacao_deleteCursorTrackId()


			Endif



			If lcvalidainsercaodocumento==.T.
				Select ft
				lcsql = ''
				TEXT TO lcsql TEXTMERGE NOSHOW
					insert into fi2 (fistamp, ftstamp, motiseimp, codmotiseimp)
					select
						fistamp, ftstamp
						,(case when iva=0 then (select motivo_isencao_iva from empresa where no=<<mysite_nr>>) else '' end)
						,(case when iva=0 then (select codmotiseimp from empresa where no=<<mysite_nr>>) else '' end)
					from fi (nolock)
					where ftstamp='<<ALLTRIM(ft.ftstamp)>>' and fistamp not in (select fistamp from fi2 (nolock) where ftstamp='<<ALLTRIM(ft.ftstamp)>>')
				ENDTEXT
				uf_gerais_actgrelha("", "", lcsql)
				If Used("FI2")
					uv_update = ''
					Select FI2
					Goto Top
					Scan  For !Empty(FI2.fnstamp) Or  !Empty(FI2.isDem)
						TEXT TO lcsql TEXTMERGE NOSHOW
	                        UPDATE fi2 SET fnstamp = '<<ALLTRIM(fi2.fnstamp)>>', isDem = <<IIF(fi2.isDem,1,0)>>, iDValidacaoDem= '<<ALLTRIM(fi2.iDValidacaoDem)>>' WHERE fi2.fistamp = '<<ALLTRIM(fi2.fistamp)>>'
						ENDTEXT
						uv_update = uv_update+Iif( .Not. Empty(uv_update), Chr(13)+Chr(9), '')+lcsql
						Select FI2
					Endscan


					If  .Not. Empty(uv_update)
						uf_gerais_actgrelha("", "", uv_update)
					Endif
				Endif
				If uf_gerais_getparameter("ADM0000000142", "BOOL")
					uf_reservas_abater()
				Endif
				Select fi
				Goto Top
				Scan
					If Alltrim(fi.ref)=='V999999' .And.  .Not. Empty(fi.bistamp)
						Local lcnrvale
						Store '' To lcnrvale
						lcsql = ""
						TEXT TO lcsql TEXTMERGE NOSHOW
							select valeNr from bi2 where bi2stamp='<<ALLTRIM(fi.bistamp)>>'
						ENDTEXT
						uf_gerais_actgrelha("", "uCrsnrvale", lcsql)
						If  .Not. Empty(ucrsnrvale.valenr)
							lcnrvale = Alltrim(ucrsnrvale.valenr)
							lcsql = ""
							TEXT TO lcsql TEXTMERGE NOSHOW
								update B_fidelvale set abatido=1 where ref='<<ALLTRIM(lcnrvale)>>'
							ENDTEXT
							uf_gerais_actgrelha("", "uCrsnrvale", lcsql)
						Endif
						fecha("uCrsnrvale")
					Endif
					If  .Not. Empty(fi.bistamp)
						lcsql = ""
						TEXT TO lcsql TEXTMERGE NOSHOW
							select nmdos from bi where bistamp='<<ALLTRIM(fi.bistamp)>>'
						ENDTEXT
						uf_gerais_actgrelha("", "uCrsnmdocbo", lcsql)
						If Alltrim(ucrsnmdocbo.nmdos)=='Encomenda de Cliente'
							Select fi
							lcsql = ""
							TEXT TO lcsql TEXTMERGE NOSHOW
								update st set cativado=	isnull((select sum(bi.qtt) from bi (nolock)
												INNER JOIN ts (nolock) ON bi.ndos = ts.ndos
												inner join bo (nolock) on bo.bostamp=bi.bostamp
												where ts.cativast=1 and bi.fechada=0 and bo.fechada=0 and bi.armazem=st.site_nr and bi.ref=st.ref),0)
								from st (nolock) where ref = '<<ALLTRIM(fi.ref)>>' and site_nr = <<mysite_nr>>
							ENDTEXT
							uf_gerais_actgrelha("", "uCrsnmdocbo", lcsql)
						Endif
						fecha("uCrsnmdocbo")
					Endif
				Endscan
				lcvalpsigrava = .F.
				Select ft
				Select fi
				Goto Top
				Scan
					If fi.u_psico
						lcvalpsigrava = .T.
						Exit
					Endif
				Endscan
				If lcvalpsigrava
					Select ft
					uf_pagamento_gravardadospsico(ft.ftstamp, 0)
				Endif

				Select ft
				If  .Not. ft.cobrado
					Select td
					If  .Not. td.tipodoc==3 .And.  .Not. td.u_tipodoc==13 .And.  .Not. td.tipodoc==2 .And.  .Not. td.u_tipodoc==5 .And.  .Not. td.u_tipodoc==1 .And.  .Not. Empty(Alltrim(ft2.u_codigo))
						Select ft2
						For i = 1 To 3
							If uf_receituario_verificalote("BACK", Alltrim(ft2.u_codigo), Alltrim(ft2.u_codigo2), Alltrim(ft.ftstamp), .F., Alltrim(ft2.u_receita), Alltrim(ft2.token))
								i = 3
							Else
								If i==3
									uf_perguntalt_chama("Ocorreu um problema a registar a venda nos lotes."+Chr(13)+"Por favor contacte o suporte.", "OK", "", 16)
								Endif
							Endif
						Endfor
					Endif
				Endif
				Select td
				If  .Not. td.tipodoc==3 .And.  .Not. td.u_tipodoc==1
					uf_pagamento_actdataultvendacl()
				Endif
				Select ft
				uf_gerais_gravaultregisto(Str(mytermno), Left(nratendimento, 8))
				If td.tipodoc==1
					uf_factentidadesclinica_actualizamarcacoes()
				Endif
				If myftintroducao
					If myarquivodigital
						Select ft
						uf_arquivodigital_taloeslt(ft.ftstamp)
					Endif
					If myarquivodigitalpdf
						Select ft
						uf_arquivodigital_emitirpdfstalao('FACTURACAO', '')
					Endif
				Endif
				regua(2)
			Else
				regua(2)
				Local lcmensagem, lcorigem
				lcmensagem = "Erro de Inser��o na tabela FT; Nmdoc: "+Alltrim(ft.nmdoc)+"; Fno: "+Alltrim(Str(ft.fno))+"; Ftstamp: "+Alltrim(ft.ftstamp)+"; User:"+Alltrim(Str(ch_userno))
				lcorigem = "Painel Factura��o: uf_gravarFactura"
				lcsql = ""
				TEXT TO lcsql TEXTMERGE NOSHOW
					Insert Into B_elog (tipo, status, mensagem, origem)
					Values 	('E','A','<<Alltrim(lcmensagem)>>', '<<Alltrim(lcorigem)>>')

				ENDTEXT
				uf_gerais_actgrelha("", "", lcsql)
				Return .F.
			Endif
		Else
			Return .F.
		Endif
	Endif



	If myftalteracao=.T.

		lcvalida = uf_facturacao_regrasgravacao()


		If lcvalida==.T.

			If !uf_faturacao_verificaCaixas()
				Return .F.
			Endif

			regua(0, 100, "A PROCESSAR A GRAVA��O DO DOCUMENTO...", .F.)
			lcsqlft2 = uf_facturacao_actualizaft2()
			lcsqlft2 = uf_gerais_trataplicassql(lcsqlft2)
			lcsqlft = uf_facturacao_actualizaft()
			lcsqlft = uf_gerais_trataplicassql(lcsqlft)
			lcsqlfi = uf_facturacao_actualizafi()
			lcsqlfi = uf_gerais_trataplicassql(lcsqlfi)
			lcsqlupdatedoc = lcsqlft2+lcsqlft+lcsqlfi
			If  .Not. Empty(lcsqlupdatedoc)
				lcsql = ''
				TEXT TO lcsql TEXTMERGE NOSHOW
					exec up_gerais_execSql 'Backoffice FT - Update', 1,'<<lcSQLUpdateDoc>>', '', '', '' , '', ''
				ENDTEXT
				If  .Not. uf_gerais_actgrelha("", "", lcsql)
					uf_perguntalt_chama("OCORREU UM ERRO A GRAVAR O DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
					Return .F.
				Else
					lcvalidainsercaodocumento = .T.
				Endif
			Endif
			If lcvalidainsercaodocumento==.T.
				If uf_gerais_getparameter("ADM0000000142", "BOOL")
					uf_reservas_abater()
				Endif
				lcvalpsigrava = .F.
				Select fi
				Goto Top
				Scan
					If fi.u_psico
						lcvalpsigrava = .T.
						Exit
					Endif
				Endscan
				If lcvalpsigrava
					uf_facturacao_actualizavendapsico()
				Endif

				Select ft
				If  .Not. ft.cobrado
					Select td
					If  .Not. td.tipodoc==3 .And.  .Not. td.u_tipodoc==13 .And.  .Not. td.u_tipodoc==5 .And.  .Not. td.u_tipodoc==1 .And.  .Not. Empty(Alltrim(ft2.u_codigo))
						For i = 1 To 3
							If uf_receituario_verificalote("BACK", ft2.u_codigo, ft2.u_codigo2, ft.ftstamp, .F., Alltrim(ft2.u_receita), Alltrim(ft2.token))
								i = 3
							Else
								If i==3
									uf_perguntalt_chama("Ocorreu um problema a registar a venda nos lotes."+Chr(13)+"Por favor contacte o suporte.", "OK", "", 16)
								Endif
							Endif
						Endfor
					Endif
				Endif
				regua(2)
			Else
				regua(2)
				Local lcmensagem, lcorigem
				lcmensagem = "Erro na Actualiza��o da tabela FT; Nmdoc: "+Alltrim(ft.nmdoc)+"; Fno: "+Alltrim(Str(ft.fno))+"; Ftstamp: "+Alltrim(ft.ftstamp)+"; User:"+Alltrim(Str(ch_userno))
				lcorigem = "Painel Factura��o: uf_gravarFactura"
				lcsql = ""
				TEXT TO lcsql TEXTMERGE NOSHOW
					Insert Into B_elog (tipo, status, mensagem, origem)
					Values 	('E','A','<<Alltrim(lcmensagem)>>', '<<Alltrim(lcorigem)>>')
				ENDTEXT
				uf_gerais_actgrelha("", "", lcsql)
				Return .F.
			Endif
		Else
			Return .F.
		Endif
	Endif
	Select fi
	Goto Top
	Locate For  .Not. Empty(id_ext_tarv_prescricao)
	If Found()
		uf_perguntalt_chama("A dispensa TARV foi comunicada com sucesso.", "OK", "", 32)
	Endif


	If myftintroducao=.T.
		Select td
		If td.imp_automatica==.T.
			uf_imprimirgerais_chama('FACTURACAO', .T.)
		Endif
	Endif
	Store .F. To myftintroducao, myftalteracao
	Select td
	If td.u_tipodoc<>3
		uf_facturacao_eventosftaposgravar()
	Endif
	uf_facturacao_enviadt()
	If  .Not. Empty(uf_gerais_getparameter_site('ADM0000000100', 'text'))
		Local lcexporta
		lcexporta = .F.
		Select fi
		Goto Top
		Scan
			If  .Not. Empty(fi.bistamp)
				TEXT TO lcsql TEXTMERGE NOSHOW
					select nmdos from bi (nolock) where bistamp='<<ALLTRIM(fi.bistamp)>>'
				ENDTEXT
				uf_gerais_actgrelha("", "ucrsbifi", lcsql)
				Select ucrsbifi
				If Alltrim(ucrsbifi.nmdos)=='Encomenda de Cliente'
					lcexporta = .T.
				Endif
				fecha("ucrsbifi")
			Endif
		Endscan
		If lcexporta=.T.
			Public ftseguradora
			ftseguradora = 'HIST'
			uf_imprimirgerais_chama('FACTURACAO')
			imprimirgerais.Hide
			imprimirgerais.pageframe1.page1.chkparapdf.Value = 1
			uf_imprimirgerais_gravar()
			uf_imprimirgerais_sair()
			lcnomeficheiropdf = Alltrim(Str(ft.fno))+'_'+Alltrim(Str(ft.ndoc))+'_'+Alltrim(Str(Year(ft.fdata)))+Alltrim(Str(Month(ft.fdata)))+Alltrim(Str(Day(ft.fdata)))+'.pdf'
			TEXT TO lcsql TEXTMERGE NOSHOW
				insert into anexos (anexosstamp, regstamp, tabela, filename, descr, keyword, protected, ousrdata, ousrinis, usrdata, usrinis, tipo, validade)
				select left(newid(),21), '<<ALLTRIM(ft.ftstamp)>>', 'FT', '<<ALLTRIM(lcNomeFicheiroPDF)>>', 'Fatura <<ALLTRIM(lcNomeFicheiroPDF)>>', 'Fatura <<ALLTRIM(lcNomeFicheiroPDF)>>', 0, GETDATE(), 'ousrinis', getdate(), '', 'Fatura', '19000101'
			ENDTEXT
			uf_gerais_actgrelha("", "", lcsql)
		Endif
	Endif
	If  .Not. llatend
		uf_facturacao_chama(Alltrim(ft.ftstamp))
	Endif


	If td.u_tipodoc==3
		If  .Not. Empty(Alltrim(ft.u_ltstamp)) .Or.  .Not. Empty(Alltrim(ft.u_ltstamp2))
			If uf_perguntalt_chama("Pretende imprimir a receita?", "Sim", "N�o")
				If ucrse1.ordemimpressaoreceita==.F.
					If  .Not. Empty(Alltrim(ft.u_ltstamp))
						**IF uf_gerais_getparameter_site('ADM0000000136', 'BOOL', mysite)
						If uf_gerais_getUmValor("B_terminal", "usaDriverWin", "terminal = '" + Alltrim(myTerm) + "'")
							uf_imprimirpos_imprectalaocomum_talaoverso(Alltrim(ft.ftstamp), 1, .F.)
						Else
							uf_imprimirpos_imprec(Alltrim(ft.ftstamp), 1, "REIMP")
						Endif
					Endif
					If  .Not. Empty(Alltrim(ft.u_ltstamp2))
						**IF uf_gerais_getparameter_site('ADM0000000136', 'BOOL', mysite)
						If uf_gerais_getUmValor("B_terminal", "usaDriverWin", "terminal = '" + Alltrim(myTerm) + "'")
							uf_imprimirpos_imprectalaocomum_talaoverso(Alltrim(ft.ftstamp), 2, .F.)
						Else
							uf_imprimirpos_imprec(Alltrim(ft.ftstamp), 2, "REIMP")
						Endif
					Endif
				Else
					If  .Not. Empty(Alltrim(ft.u_ltstamp2))
						**IF uf_gerais_getparameter_site('ADM0000000136', 'BOOL', mysite)
						If uf_gerais_getUmValor("B_terminal", "usaDriverWin", "terminal = '" + Alltrim(myTerm) + "'")
							uf_imprimirpos_imprectalaocomum_talaoverso(Alltrim(ft.ftstamp), 2, .F.)
						Else
							uf_imprimirpos_imprec(Alltrim(ft.ftstamp), 2, "REIMP")
						Endif
					Endif
					If  .Not. Empty(Alltrim(ft.u_ltstamp))
						**IF uf_gerais_getparameter_site('ADM0000000136', 'BOOL', mysite)
						If uf_gerais_getUmValor("B_terminal", "usaDriverWin", "terminal = '" + Alltrim(myTerm) + "'")
							uf_imprimirpos_imprectalaocomum_talaoverso(Alltrim(ft.ftstamp), 1, .F.)
						Else
							uf_imprimirpos_imprec(Alltrim(ft.ftstamp), 1, "REIMP")
						Endif
					Endif
				Endif
			Endif
		Endif
	Endif



	uf_atendimento_mostraSinave("FACTURACAO")

	If Used("uCrsVerificaRespostaDEM")
		Select ucrsverificarespostadem
		Delete All
	Endif
	If Used("uCrsVerificaRespostaDEMTotal")
		Select ft2
		Delete From uCrsVerificaRespostaDEMTotal Where Alltrim(receita_nr)=Alltrim(ft2.u_receita) .And.  .Not. Empty(Alltrim(ft2.u_receita))
	Endif
	If Used("ucrsDemEfetivada")
		Select ucrsdemefetivada
		Delete All
	Endif
	If Used("uCrsVerificaRespostaDEMTotalAux")
		Select ucrsverificarespostademtotalaux
		Delete All
	Endif
	If Used("uCrsVerificaRespostaDEMAuxiliar")
		Select ucrsverificarespostademauxiliar
		Delete All
	Endif
	If Used("uCrsVerificaRespostaDEM_nd")
		Select ucrsverificarespostadem_nd
		Delete All
	Endif
	If Used("uCrsDEMndisp")
		Select ucrsdemndisp
		Delete All
	Endif
	If Used("uCrsVerificaRespostaDEMAux")
		Select ucrsverificarespostademaux
		Delete All
	Endif
	If Used("uCrsVerificaRespostaDEM_tot")
		Select ucrsverificarespostadem_tot
		Delete All
	Endif
	If Used("ucrsReceitaEfetivar")
		Select ucrsreceitaefetivar
		Delete All
	Endif
	If Used("ucrsDadosValidacaoEfetivarDEM")
		Select ucrsdadosvalidacaoefetivardem
		Delete All
	Endif
	If Used("ucrsReceitaEfetivarAp")
		Select ucrsreceitaefetivarap
		Delete All
	Endif
	If Used("ucrsReceitaValidar")
		Select ucrsreceitavalidar
		Delete All
	Endif
	If Used("ucrsReceitaValidarAux")
		Select ucrsreceitavalidaraux
		Delete All
	Endif
	If Used("ucrsReceitaValidarLinhas")
		Select ucrsreceitavalidarlinhas
		Delete All
	Endif
	If Used("ucrsDadosValidacaoDEM")
		Select ucrsdadosvalidacaodem
		Delete All
	Endif
	If Used("ucrsDadosValidacaoDEMLinhas")
		Select ucrsdadosvalidacaodemlinhas
		Delete All
	Endif
	If Used("uCrsreceitas")
		Select ucrsreceitas
		Delete All
	Endif
	If Used("ucrsReceitaValidar")
		Select ucrsreceitavalidar
		Delete All
	Endif
	If Used("uCrsReceitaValidarAp")
		Select ucrsreceitavalidarap
		Delete All
	Endif
	Return .T.
Endfunc
**
Function uf_facturacao_enviadt
	If  .Not. Used("TD")
		Return .F.
	Endif
	Select ucrse1
	Select td
	If Upper(Alltrim(td.tiposaft))=="GT" And Upper(Alltrim(ucrse1.pais))=="PORTUGAL"
		If  .Not. uf_perguntalt_chama("Pretende fazer a comunica��o de trasporte � Autoridade Tribut�ria e Aduaneira (AT)?", "Sim", "N�o", 64)
			Return .F.
		Endif
		If Empty(uf_gerais_getparameter("ADM0000000205", "TEXT")) .Or. Empty(uf_gerais_getparameter("ADM0000000206", "TEXT"))
			uf_perguntalt_chama("Dados de configura��o para envio de Guias de Transporte n�o definidas. Contacte o suporte.", "OK", "", 32)
			Return .F.
		Endif
		Select ft2
		If Empty(Alltrim(ft2.subproc))
			regua(0, 15, "A enviar Documento para a AT...")
			regua(1, 1, "A enviar Documento para a AT...")
			If  .Not. uf_gerais_verifyinternet()
				uf_perguntalt_chama("ESTA FUNCIONALIDADE REQUER LIGA��O � INTERNET."+Chr(13)+"Por favor valide.", "OK", "", 64)
				regua(2)
				Return .F.
			Endif
			regua(1, 2, "A enviar Documento para a AT...")
			facturacao.stamptmrdt = uf_gerais_stamp()
			Select ft
			lcsql = ''
			TEXT TO lcsql TEXTMERGE NOSHOW
				INSERT INTO b_DT_resposta
				(
					stamp
					,stampDoc
				)
				values
				(
					'<<ALLTRIM(facturacao.stampTmrDT)>>'
					,'<<ALLTRIM(ft.ftstamp)>>'
				)
			ENDTEXT
			If  .Not. uf_gerais_actgrelha("", "", lcsql)
				regua(2)
			Endif
			regua(1, 3, "A enviar Documento para a AT...")
			If Directory(Alltrim(uf_gerais_getparameter("ADM0000000185", "TEXT"))+'\DT')
				Select ft
				lcwspath = Alltrim(uf_gerais_getparameter("ADM0000000185", "TEXT"))+'\DT\ATDocTransporte.jar "'+Alltrim(facturacao.stamptmrdt)+'" "'+Alltrim(sql_db)+'" "'+Alltrim(ft.ftstamp)+'" "'+Alltrim(mysite)+'"'
				owsshell = Createobject("WScript.Shell")
				owsshell.Run(lcwspath, 1, .F.)
				regua(1, 4, "A enviar Documento para a AT...")
				facturacao.tmrdt.Enabled = .T.
			Endif
		Endif
	Endif
Endfunc
**
Function uf_facturacao_tmrdt
	facturacao.ntmrdt = facturacao.ntmrdt+1
	If facturacao.ntmrdt>10
		regua(2)
		uf_perguntalt_chama("N�o foi poss�vel obter resposta do WebService. Por favor volte a tentar mais tarde.", "OK", "", 16)
		facturacao.tmrdt.Enabled = .F.
		facturacao.ntmrdt = 0
		facturacao.stamptmrdt = ''
		Return .F.
	Endif
	regua(1, facturacao.ntmrdt+2, "A enviar Documento para a AT...")
	TEXT TO lcsql TEXTMERGE NOSHOW
		SELECT
			codResposta
			,descrResposta
			,ATDocCode
		from
			b_DT_resposta (nolock)
		where
			stamp = '<<ALLTRIM(facturacao.stampTmrDT)>>'
	ENDTEXT
	If  .Not. uf_gerais_actgrelha("", "uCrsVerificaResposta", lcsql)
		regua(2)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA A VERIFICAR A RESPOSTA DO WEB SERVICE. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
		facturacao.tmrdt.Enabled = .F.
		facturacao.ntmrdt = 0
		facturacao.stamptmrdt = ''
		Return .F.
	Else
		Select ucrsverificaresposta
		If Reccount("uCrsVerificaResposta")>0 .And.  .Not. Empty(Alltrim(ucrsverificaresposta.codresposta))
			Select ucrsverificaresposta
			Goto Top
			If Alltrim(ucrsverificaresposta.codresposta)<>"0" .And. Alltrim(ucrsverificaresposta.codresposta)<>"-100"
				regua(2)
				uf_perguntalt_chama("N�o foi possivel enviar o documento."+Chr(13)+"Motivo: "+Upper(Alltrim(ucrsverificaresposta.descrresposta)), "OK", "", 64)
				facturacao.tmrdt.Enabled = .F.
				facturacao.ntmrdt = 0
				facturacao.stamptmrdt = ''
				Return .F.
			Endif
			If Alltrim(ucrsverificaresposta.codresposta)=="-100"
				regua(2)
				uf_perguntalt_chama(Upper(Alltrim(ucrsverificaresposta.descrresposta)), "OK", "", 64)
				Select ft2
				lcsql = ''
				TEXT TO lcsql TEXTMERGE NOSHOW
					UPDATE ft2
					SET subproc = '-100'
					WHERE ft2.ft2stamp = '<<ALLTRIM(ft2.ft2stamp)>>'
				ENDTEXT
				uf_gerais_actgrelha("", "", lcsql)
				Select ft2
				Replace ft2.subproc With '-100'
				facturacao.tmrdt.Enabled = .F.
				facturacao.ntmrdt = 0
				facturacao.stamptmrdt = ''
				Return .F.
			Endif
			Select ucrsverificaresposta
			Select ft2
			lcsql = ''
			TEXT TO lcsql TEXTMERGE NOSHOW
				UPDATE ft2
				SET subproc = '<<ALLTRIM(uCrsVerificaResposta.ATDocCode)>>'
				WHERE ft2.ft2stamp = '<<ALLTRIM(ft2.ft2stamp)>>'
			ENDTEXT
			uf_gerais_actgrelha("", "", lcsql)
			Select ft2
			Replace ft2.subproc With Alltrim(ucrsverificaresposta.atdoccode)
			regua(2)
			uf_perguntalt_chama("DOCUMENTO ENVIADO COM SUCESSO.", "OK", "", 64)
			facturacao.tmrdt.Enabled = .F.
			facturacao.ntmrdt = 0
			facturacao.stamptmrdt = ''
		Endif
	Endif
Endfunc
**
Procedure uf_facturacao_eventosftaposgravar
	uf_facturacao_actualizaquantidademovimentadafact()
	Select fi
	Locate For  .Not. Empty(fi.bistamp)
	If Found()
		uf_fechoautomaticodoc_chama("FT")
	Endif
Endproc
**
Function uf_facturacao_actualizaquantidademovimentadafact
	Local lcsql
	Select fi
	Goto Top
	Scan
		Select td
		If  .Not. Empty(fi.ofistamp) And td.codigoDoc <> 21
			lcsql = ""
			TEXT TO lcsql TEXTMERGE NOSHOW
				UPDATE
					Fi
				SET
					Pbruto = <<Fi.qtt>>
				Where
					Fistamp = '<<Alltrim(Fi.ofistamp)>>'
			ENDTEXT
			If  .Not. uf_gerais_actgrelha("", "", lcsql)
				uf_perguntalt_chama("N�O FOI POSS�VEL ACTUALIZAR A QUANTIDADE MOVIMENTADA DO DOCUMENTO DE ORIGEM.", "OK", "", 16)
				Return .F.
			Endif
		Endif
		If  .Not. Empty(fi.id_ext_tarv_prescricao)
			lcsql = ""
			TEXT TO lcsql TEXTMERGE NOSHOW
				UPDATE
					ext_tarv_prescricao
				SET
					qt_dispensada = qt_dispensada + <<Fi.qtt>>
				Where
					id = '<<Alltrim(fi.id_ext_tarv_prescricao)>>'
			ENDTEXT
			If  .Not. uf_gerais_actgrelha("", "", lcsql)
				uf_perguntalt_chama("N�O FOI POSS�VEL ACTUALIZAR A QUANTIDADE MOVIMENTADA DO DOCUMENTO DE ORIGEM.", "OK", "", 16)
				Return .F.
			Endif
		Endif
	Endscan
	Select td
	If td.u_tipodoc==7
		If (Used("uCrsAtendCl"))
			Select ucrsatendcl
			If Alltrim(ucrsatendcl.modofact)==("Factura��o Acordo")
				Select fi
				Goto Top
				Scan
					If  .Not. Empty(fi.ofistamp)
						lcsql = ""
						TEXT TO lcsql TEXTMERGE NOSHOW
							UPDATE
								fi
							set
								fi.pbruto = 0
							where
								fistamp = (select FRFI.ofistamp from fi FRFI inner join fi NCFI on FRFI.fistamp = NCFI.fistamp where NCFI.fistamp = '<<Alltrim(Fi.ofistamp)>>')
							AND
								fi.nmdoc = 'Factura Acordo'
						ENDTEXT
						If  .Not. uf_gerais_actgrelha("", "", lcsql)
							uf_perguntalt_chama("N�O FOI POSS�VEL ACTUALIZAR A QUANTIDADE MOVIMENTADA DO DOCUMENTO DE ORIGEM. [F.A]", "OK", "", 16)
							Return .F.
						Endif
					Endif
				Endscan
			Endif
		Endif
	Endif
Endfunc
**
Function u_facturacao_seltipodocfact
	Public myftalteracao, myftintroducao
	Local lccntlin
	Store 0 To lccntlin
	Select fi
	Calculate Cnt(fi.qtt) To lccntlin For (fi.qtt<>0 .Or.  .Not. Empty(fi.ref) .Or.  .Not. Empty(fi.Design) .Or. fi.etiliquido<>0)
	If lccntlin>0
		If uf_perguntalt_chama("ATEN��O: SE ALTERAR O TIPO DE DOCUMENTO VAI PERDER TODAS AS ALTERA��ES �S LINHAS. PRETENDE CONTINUAR?", "Sim", "N�o")
			Select fi
			Goto Top
			Scan
				Delete
			Endscan
			Select FI2
			Go Top
			Scan
				Delete
			Endscan

			If Used("fi_trans_info")
				Select fi_trans_info
				Go Top
				Scan
					Delete
				Endscan
			Endif

		Else
			Select fi
			Goto Top
			facturacao.containercab.nmdoc.Value = Alltrim(fi.nmdoc)
			Return .F.
		Endif
	Endif
	uf_facturacao_limpanomefact()
	uf_facturacao_valoresdefeitofact()
	uf_facturacao_controlanumerofact(Year(Datetime()+(difhoraria*3600)))
	uf_atendimento_fiiliq()
	uf_atendimento_acttotaisft()
	uf_facturacao_controlawklinhas()
	uf_facturacao_controlaobjectos()
	uf_facturacao_crialinhadefault()


	Select td
	If(Inlist(Upper(Alltrim(td.tiposaft)),'FR'))
		facturacao.pageframe1.page2.movcaixa.Tag = "true"
		facturacao.pageframe1.page2.movcaixa.Picture = mypath+"\imagens\icons\checked_b_24.bmp"
	Endif
Endfunc
**
Procedure uf_facturacao_crialinhadefault
	Local lcnumlinhas
	Store 0 To lcnumlinhas
	Select fi
	Scan
		lcnumlinhas = lcnumlinhas+1
	Endscan
	If lcnumlinhas==0
		uf_atendimento_adicionalinhafi(.T., .T.)
	Endif
Endproc
**
Function uf_facturacao_limpanomefact
	If  .Not. Used("Ft")
		Return .F.
	Endif
	Select ft
	Replace ft.Nome With '', ft.nome2 With '', ft.NO With 0, ft.estab With 0, ft.morada With '', ft.Local With '', ft.codpost With '', ft.ncont With '', ft.tipo With '', ft.telefone With ''
Endfunc
**
Procedure uf_facturacao_valoresdefeitofact
	Local lcsql
	Local lcstamp
	lcstamp = uf_gerais_stamp()
	Select ft
	Replace ft.ftstamp With lcstamp, ft.fdata With Datetime()+(difhoraria*3600), ft.bidata With Datetime()+(difhoraria*3600), ft.ftano With Year(Datetime()+(difhoraria*3600)), ft.memissao With uf_gerais_getparameter("ADM0000000260", "text"), ft.pdata With Datetime()+(difhoraria*3600), ft.cdata With Datetime()+(difhoraria*3600), ft.nmdoc With Alltrim(facturacao.containercab.nmdoc.Value)
	Replace ft.cambiofixo With .F.
	If Used("TD")
		fecha("TD")
	Endif
	Select ft
	Select * From uCrsGeralTD Where Upper(Alltrim(uCrsGeralTD.nmdoc))==Upper(Alltrim(facturacao.containercab.nmdoc.Value)) And Alltrim(uCrsGeralTD.site)==Alltrim(mysite) And inativo=.F. Into Cursor td Readwrite
	Select td
	Replace ft.nmdoc With Alltrim(td.nmdoc), ft.ndoc With td.ndoc, ft.tipodoc With td.tipodoc
	If td.u_tipodoc==1 .Or. td.u_tipodoc==2 .Or. td.u_tipodoc==5 .Or. td.u_tipodoc==6 .Or. td.u_tipodoc==7 .Or. td.u_tipodoc==8 .Or. td.u_tipodoc==9 .Or. td.u_tipodoc==11 .Or. td.u_tipodoc==12
		Replace ft.localtesouraria With "Conta Geral Recebimento"
		Replace ft.id_tesouraria_conta With 99
	Else
		Replace ft.localtesouraria With ""
		Replace ft.id_tesouraria_conta With 0
	Endif
	Select ft
	Replace ft.ivatx1 With uf_gerais_gettabelaiva(1, "TAXA"), ft.ivatx2 With uf_gerais_gettabelaiva(2, "TAXA"), ft.ivatx3 With uf_gerais_gettabelaiva(3, "TAXA"), ft.ivatx4 With uf_gerais_gettabelaiva(4, "TAXA"), ft.ivatx5 With uf_gerais_gettabelaiva(5, "TAXA"), ft.ivatx6 With uf_gerais_gettabelaiva(6, "TAXA"), ft.ivatx7 With uf_gerais_gettabelaiva(7, "TAXA"), ft.ivatx8 With uf_gerais_gettabelaiva(8, "TAXA"), ft.ivatx9 With uf_gerais_gettabelaiva(9, "TAXA"), ft.ivatx10 With uf_gerais_gettabelaiva(10, "TAXA"), ft.ivatx11 With uf_gerais_gettabelaiva(11, "TAXA"), ft.ivatx12 With uf_gerais_gettabelaiva(12, "TAXA"), ft.ivatx13 With uf_gerais_gettabelaiva(13, "TAXA")
	Select ft2
	Replace ft2.ft2stamp With ft.ftstamp
	stamplindem = ''
	fecha("uc_rnuRecms")
Endproc
**
Procedure uf_facturacao_controlanumerofact
	Lparameters lcano

	**LOCAL lcsql
	**lcsql = ""
	**TEXT TO lcsql TEXTMERGE NOSHOW
	**	exec up_facturacao_numDoc <<Ft.ndoc>>, <<lcAno>>, '<<ALLTRIM(mySite)>>'
	**ENDTEXT
	**IF  .NOT. uf_gerais_actgrelha("", "uc_numdocFact", lcsql)
	**   uf_perguntalt_chama("OCORREU UMA ANOMALIA NO C�LCULO DO N�MERO DO DOCUMENTO.", "OK", "", 16)
	**   RETURN
	**ENDIF

	Local uv_newDocNr
	Store 0 To uv_newDocNr

	uv_newDocNr = uf_gerais_newDocNr("FT", ft.ndoc)

	If uv_newDocNr = 0
		uf_perguntalt_chama("Erro a gerar novo n�mero de documento!" + Chr(13) + "Por favor contacte o suporte.", "OK", "", 48)
		Return .F.
	Endif

	Select ft
	Replace ft.fno With uv_newDocNr

	**IF USED("uc_numdocFact")
	**   fecha("uc_numdocFact")
	**ENDIF
Endproc
**
Procedure uf_facturacao_nomeinteractivechange
	Public mykeypressed
	Store 0 To lctamanhoantigo, lcnovotamanho
	lcvalorantigo = facturacao.containercab.Nome.Value
	lctamanhoantigo = Len(Alltrim(facturacao.containercab.Nome.Value))
	If Alltrim(facturacao.containercab.Nome.Value)==''
		lcsql = "SELECT TOP 1 convert(nvarchar(250),NOME) as NOME FROM b_utentes (nolock) WHERE NOME LIKE '"+Alltrim(lcvalorantigo)+"' and b_utentes.inactivo = 0 ORDER BY NOME ASC"
	Else
		lcsql = "SELECT TOP 1 convert(nvarchar(250),NOME) as NOME FROM b_utentes (nolock) WHERE NOME LIKE '"+Alltrim(lcvalorantigo)+"%' and b_utentes.inactivo = 0 ORDER BY NOME ASC"
	Endif
	If uf_gerais_actgrelha("", "uCrsTempSQLAutoComp", lcsql)
		If Reccount("uCrsTempSQLAutoComp")>0
			Select ucrstempsqlautocomp
			lcnovovalor = Alltrim(ucrstempsqlautocomp.Nome)
			lcnovotamanho = Len(Alltrim(ucrstempsqlautocomp.Nome))
			If mykeypressed<>127 .And. mykeypressed<>7 .And. mykeypressed<>32
				facturacao.containercab.Nome.Value = Alltrim(ucrstempsqlautocomp.Nome)
				lc_diftamanhos = Abs(lcnovotamanho-lctamanhoantigo)
				If  .Not. Empty(lcnovovalor)
					facturacao.containercab.Nome.SelStart = lctamanhoantigo
					facturacao.containercab.Nome.SelLength = lc_diftamanhos
				Endif
			Endif
		Endif
	Endif
Endproc
**
Function uf_facturacao_sugerecriarcliente
	Local lcnome, lcsql, lcmoeda, lcvalidacl
	Store .F. To lcvalidacl
	If  .Not. Used("FT")
		Return .F.
	Endif
	Select ft
	lcnome = Upper(Alltrim(ft.Nome))
	If Empty(lcnome)
		uf_facturacao_limpanomefact()
	Else
		lcsql = ""
		TEXT TO lcsql TEXTMERGE NOSHOW
			exec up_facturacao_existeCl '<<Alltrim(lcNome)>>'
		ENDTEXT
		If uf_gerais_actgrelha("", "uCrsAtendCL", lcsql)
			If Reccount("uCrsAtendCL")=0
				If uf_perguntalt_chama('O CLIENTE N�O EXISTE... QUER INTRODUZIR CRIAR UM NOVO CLIENTE?', "Sim", "N�o")
					If  .Not. Type("UTENTES")=="U" .And. (myclintroducao .Or. myclalteracao)
						uf_perguntalt_chama("N�O � POSS�VEL REALIZAR ESTA A��O PORQUE O ECR� DE CLIENTES EST� EM MODO DE EDI��O. POR FAVOR TERMINE ESTA OPERA��O E TENTE NOVAMENTE", "OK", "", 48)
					Else
						uf_utentes_novo()
						utentes.AlwaysOnTop = .T.
						Select cl
						Replace cl.Nome With lcnome
						utentes.Refresh
					Endif
					uf_facturacao_limpanomefact()
				Else
					uf_facturacao_limpanomefact()
				Endif
			Endif
		Endif
	Endif
Endfunc
**
Procedure uf_facturacao_escolhecliente
	If myftintroducao .Or. myftalteracao
		uf_pesqutentes_chama("FACTURACAO")
		uf_faturacao_valida_obito()
	Endif


Endproc
**
Procedure uf_facturacao_controlaeventoslinhas
	For i = 1 To facturacao.pageframe1.page1.griddoc.ColumnCount
		With facturacao.pageframe1.page1.griddoc
			If Upper(.Columns(i).ControlSource)=="FI.REF"
				Bindevent(.Columns(i).text1, "InteractiveChange", facturacao.ocust, "proc_ActivaEventosLinhasFact")
				Bindevent(.Columns(i).text1, "dblClick", facturacao.ocust, "proc_navegast")
				Bindevent(.Columns(i).text1, "KeyPress", facturacao.ocust, "proc_ApagarAutocompleteRefFi")
				Bindevent(.Columns(i).text1, "InteractiveChange", facturacao.ocust, "proc_autocompleteRefFi")
				Bindevent(.Columns(i).text1, "RightClick", facturacao.ocust, "proc_pesquisarArtigosRefFi")
			Endif
			If Upper(.Columns(i).ControlSource)=="FI.DESIGN"
				Bindevent(.Columns(i).text1, "KeyPress", facturacao.ocust, "proc_ApagarAutocompleteDesignFi")
				Bindevent(.Columns(i).text1, "InteractiveChange", facturacao.ocust, "proc_autocompleteDesignFi")
				Bindevent(.Columns(i).text1, "InteractiveChange", facturacao.ocust, "proc_ActivaEventosLinhasFact")
				Bindevent(.Columns(i).header1, "Click", facturacao.ocust, "proc_OrdenaPorDesignDoc")
			Endif
			If Upper(.Columns(i).ControlSource)=="FI.U_DIPLOMA"
				Bindevent(.Columns(i).text1, "RightClick", facturacao.ocust, "proc_EscolheDiploma")
				Bindevent(.Columns(i).text1, "DblClick", facturacao.ocust, "proc_EscolheDiploma")
			Endif
			If Upper(.Columns(i).ControlSource)=="FI.LOTE"
				Bindevent(.Columns(i).text1, "dblClick", facturacao.ocust, "proc_clickLOTE")
				Bindevent(.Columns(i).text1, "gotFocus", facturacao.ocust, "proc_gotFocusLOTE")
			Endif
			If Upper(.Columns(i).ControlSource)=="FI.IVA"
				Bindevent(.Columns(i).text1, "Click", facturacao.ocust, "proc_clickIVA")
			Endif
			If Upper(.Columns(i).ControlSource)=="FI.QTT"
				Bindevent(.Columns(i).text1, "InteractiveChange", facturacao.ocust, "proc_ActivaEventosLinhasFact")
			Endif
			If Upper(.Columns(i).ControlSource)=="FI.DESCONTO"
				Bindevent(.Columns(i).text1, "InteractiveChange", facturacao.ocust, "proc_ActivaEventosLinhasFact")
			Endif
			If Upper(.Columns(i).ControlSource)=="FI.U_DESCVAL"
				Bindevent(.Columns(i).text1, "InteractiveChange", facturacao.ocust, "proc_ActivaEventosLinhasFact")
			Endif
			If Upper(.Columns(i).ControlSource)=="FI.DESC2"
				Bindevent(.Columns(i).text1, "InteractiveChange", facturacao.ocust, "proc_ActivaEventosLinhasFact")
			Endif
			If Upper(.Columns(i).ControlSource)=="FI.DESC3"
				Bindevent(.Columns(i).text1, "InteractiveChange", facturacao.ocust, "proc_ActivaEventosLinhasFact")
			Endif
			If Upper(.Columns(i).ControlSource)=="FI.DESC4"
				Bindevent(.Columns(i).text1, "InteractiveChange", facturacao.ocust, "proc_ActivaEventosLinhasFact")
			Endif
			If Upper(.Columns(i).ControlSource)=="FI.EPV"
				Bindevent(.Columns(i).text1, "InteractiveChange", facturacao.ocust, "proc_ActivaEventosLinhasFact")
			Endif
			If Upper(.Columns(i).ControlSource)=="FI.ETILIQUIDO"
				Bindevent(.Columns(i).text1, "InteractiveChange", facturacao.ocust, "proc_ActivaEventosLinhasFact")
			Endif
			If Upper(.Columns(i).ControlSource)=="FI.ARMAZEM"
				Bindevent(.Columns(i).text1, "InteractiveChange", facturacao.ocust, "proc_ActivaEventosLinhasFact")
			Endif
			If Upper(.Columns(i).ControlSource)=="FI.U_EPVP"
				Bindevent(.Columns(i).text1, "DblClick", facturacao.ocust, "proc_PainelPVPS")
				Bindevent(.Columns(i).text1, "RightClick", facturacao.ocust, "proc_PainelPVPS")
			Endif
			If Upper(.Columns(i).ControlSource)=="FI.IVAINCL"
				Bindevent(.Columns(i).check1, "Click", facturacao.ocust, "proc_actualizaTotaisFIIvaIncl")
			Endif
			If Upper(.Columns(i).ControlSource)=="FI.FICCUSTO"
				Bindevent(.Columns(i).text1, "Click", facturacao.ocust, "proc_clickCCUSTO")
			Endif
			If Upper(.Columns(i).ControlSource)=="FI.CODMOTISEIMP"
				Bindevent(.Columns(i).text1, "Click", facturacao.ocust, "proc_codmotiseimp")
			Endif
		Endwith
	Endfor
Endproc
**
Procedure uf_facturacao_eventosgriddoc
	With facturacao.pageframe1.page1.griddoc
		For i = 1 To .ColumnCount
			If Upper(.Columns(i).ControlSource)=="FI.REF"
				Bindevent(.Columns(i).text1, "LostFocus", facturacao.ocust, "proc_eventoRefFi")
			Endif
			If Upper(.Columns(i).ControlSource)=="FI.DESIGN"
				Bindevent(.Columns(i).text1, "LostFocus", facturacao.ocust, "proc_pesquisarArtigosDesignFi")
			Endif
			If Upper(.Columns(i).ControlSource)=="FI.QTT"
				Bindevent(.Columns(i).text1, "LostFocus", facturacao.ocust, "proc_actualizaTotaisFIQTT")
			Endif
			If Upper(.Columns(i).ControlSource)=="FI.U_DESCVAL"
				Bindevent(.Columns(i).text1, "LostFocus", facturacao.ocust, "proc_actualizaTotaisFI")
			Endif
			If Upper(.Columns(i).ControlSource)=="FI.U_DESCVAL"
				Bindevent(.Columns(i).text1, "InteractiveChange", facturacao.ocust, "proc_validaExistenciaValesDesconto")
			Endif
			If Upper(.Columns(i).ControlSource)=="FI.DESCONTO"
				Bindevent(.Columns(i).text1, "LostFocus", facturacao.ocust, "proc_actualizaTotaisFI")
			Endif
			If Upper(.Columns(i).ControlSource)=="FI.DESC2"
				Bindevent(.Columns(i).text1, "LostFocus", facturacao.ocust, "proc_actualizaTotaisFI")
			Endif
			If Upper(.Columns(i).ControlSource)=="FI.DESC3"
				Bindevent(.Columns(i).text1, "LostFocus", facturacao.ocust, "proc_actualizaTotaisFI")
			Endif
			If Upper(.Columns(i).ControlSource)=="FI.DESC4"
				Bindevent(.Columns(i).text1, "LostFocus", facturacao.ocust, "proc_actualizaTotaisFI")
			Endif
			If Upper(.Columns(i).ControlSource)=="FI.EPV"
				Bindevent(.Columns(i).text1, "LostFocus", facturacao.ocust, "proc_actualizaTotaisFI")
			Endif
			If Upper(.Columns(i).ControlSource)=="FI.U_EPVP"
				Bindevent(.Columns(i).text1, "LostFocus", facturacao.ocust, "proc_actualizaTotaisFI")
			Endif
			If Upper(.Columns(i).ControlSource)=="FI.IVAINCL"
				Bindevent(.Columns(i).check1, "Click", facturacao.ocust, "proc_actualizaTotaisFIIvaIncl")
			Endif
			If Upper(.Columns(i).ControlSource)=="FI.ETILIQUIDO"
				Bindevent(.Columns(i).text1, "LostFocus", facturacao.ocust, "proc_validaAlteracaoTotalLinha")
			Endif
			If Upper(.Columns(i).ControlSource)=="FI.ARMAZEM"
				Bindevent(.Columns(i).text1, "LostFocus", facturacao.ocust, "proc_validaAlteracaoArmazem")
			Endif
		Endfor
	Endwith
Endproc
**
Procedure uf_facturacao_desactivaeventoslinhasfact
	With facturacao.pageframe1.page1.griddoc
		For i = 1 To .ColumnCount
			If Upper(.Columns(i).ControlSource)=="FI.REF"
				Unbindevents(.Columns(i).text1, "LostFocus", facturacao.ocust, "proc_eventoRefFi")
			Endif
			If Upper(.Columns(i).ControlSource)=="FI.DESIGN"
				Unbindevents(.Columns(i).text1, "LostFocus", facturacao.ocust, "proc_pesquisarArtigosDesignFi")
			Endif
			If Upper(.Columns(i).ControlSource)=="FI.QTT"
				Unbindevents(.Columns(i).text1, "LostFocus", facturacao.ocust, "proc_actualizaTotaisFIQTT")
			Endif
			If Upper(.Columns(i).ControlSource)=="FI.U_DESCVAL"
				Unbindevents(.Columns(i).text1, "LostFocus", facturacao.ocust, "proc_actualizaTotaisFI")
			Endif
			If Upper(.Columns(i).ControlSource)=="FI.U_DESCVAL"
				Unbindevents(.Columns(i).text1, "InteractiveChange", facturacao.ocust, "proc_validaExistenciaValesDesconto")
			Endif
			If Upper(.Columns(i).ControlSource)=="FI.DESCONTO"
				Unbindevents(.Columns(i).text1, "LostFocus", facturacao.ocust, "proc_actualizaTotaisFI")
			Endif
			If Upper(.Columns(i).ControlSource)=="FI.DESC2"
				Unbindevents(.Columns(i).text1, "LostFocus", facturacao.ocust, "proc_actualizaTotaisFI")
			Endif
			If Upper(.Columns(i).ControlSource)=="FI.DESC3"
				Unbindevents(.Columns(i).text1, "LostFocus", facturacao.ocust, "proc_actualizaTotaisFI")
			Endif
			If Upper(.Columns(i).ControlSource)=="FI.DESC4"
				Unbindevents(.Columns(i).text1, "LostFocus", facturacao.ocust, "proc_actualizaTotaisFI")
			Endif
			If Upper(.Columns(i).ControlSource)=="FI.EPV"
				Unbindevents(.Columns(i).text1, "LostFocus", facturacao.ocust, "proc_actualizaTotaisFI")
			Endif
			If Upper(.Columns(i).ControlSource)=="FI.ETILIQUIDO"
				Unbindevents(.Columns(i).text1, "LostFocus", facturacao.ocust, "proc_validaAlteracaoTotalLinha")
			Endif
			If Upper(.Columns(i).ControlSource)=="FI.ARMAZEM"
				Bindevent(.Columns(i).text1, "LostFocus", facturacao.ocust, "proc_validaAlteracaoArmazem")
			Endif
		Endfor
	Endwith
Endproc
**
Function uf_facturacao_dados
	If Empty(uf_gerais_retorna_campos_datamatrix(Alltrim(lcautorizationcode)))
		uf_perguntalt_chama("Codigo de autoriza��o / �nico da embalagem inv�lido, por favor, coloque manualmente.", "OK", "", 16)
		Insert Into ucrsTempAuthCodeEmb Values (1, "")
		Return .T.
	Else
		Local lcpacksn
		Select ucrdatamatrixtemp
		Goto Top
		lcpacksn = Alltrim(ucrdatamatrixtemp.sn)
		fecha("ucrDataMatrixTemp")
		Insert Into ucrsTempAuthCodeEmb Values (2, lcpacksn)
		Return .T.
	Endif
Endfunc
**
Function uf_facturacao_eventoreffi
	Lparameters lclote
	Select fi
	Replace fi.ref With Strtran(fi.ref, Chr(39), ' ')
	If Empty(fi.ref)
		uf_facturacao_controlaeventoslinhas()
		Return .F.
	Endif

	lcsql = ""
	TEXT TO lcsql TEXTMERGE NOSHOW
		exec up_stocks_procuraRef '<<Alltrim(FI.REF)>>',<<mySite_nr>>
	ENDTEXT
	If  .Not. uf_gerais_actgrelha("", "ucrsCodAlternativo", lcsql)
		uf_perguntalt_chama("N�O FOI POSS�VEL VERIFICAR O C�DIGO ALTERNATIVO DO ARTIGO. POR FAVOR VERIFIQUE.", "OK", "", 16)
		Return .F.
	Endif
	If  .Not. Empty(ucrscodalternativo.rateamento) .And. uf_gerais_getparameter("ADM0000000211", "BOOL")==.T.
		uf_perguntalt_chama("O PRODUTO: ["+Alltrim(fi.ref)+"]. Est� marcado como de rateamento!", "OK", "", 64)
	Endif

	** quando o cliente coloca a ref de vale e depois altera por cima
	** � necessario colocar a false para o produto nao ficar marcado como vale
	If !uf_gerais_compStr(fi.ref, "V000001")
		Replace fi.epromo With .F.
	Endif

	If Alltrim(Upper(fi.ref))<>Alltrim(Upper(ucrscodalternativo.ref))
		Replace fi.ref With ucrscodalternativo.ref
	Endif
	Local lcpos
	Store 0 To lcpos
	Select fi
	lcpos = Recno("fi")
	If Used("uCrsAtendST")
		Select ucrsatendst
		Delete For Alltrim(ucrsatendst.ststamp)==Alltrim(fi.fistamp)
	Endif


	uf_atendimento_actref()
	Select fi
	Try
		Goto lcpos
	Catch

	Endtry
	If  .Not. Empty(ucrscodalternativo.cnpem)
		Select fi
		Replace fi.cnpem With Alltrim(ucrscodalternativo.cnpem)
	Endif
	If Used("ucrsCodAlternativo")
		fecha("ucrsCodAlternativo")
	Endif
	If fi.usalote==.T.
		uf_facturacao_atribuilote()
	Endif
	uf_atendimento_actvaloresfi()
	uf_atendimento_acttotaisft()
	Local lcobriga, lccod
	Store .F. To lcobriga
	Select ft2
	lccod = Upper(Alltrim(ft2.u_codigo))
	If lccod=="AS" .Or. lccod=="WG" .Or. lccod=="HP" .Or. lccod=="WJ"
		lcobriga = .T.
	Endif
	If lcobriga==.T.
		If Upper(Alltrim(lccod))=="WJ"
			uf_tecladoalpha_chama("FI.U_CODEMB", "Introduza o C�digo da Embalagem:", .F., .F., 2)
			uf_atendimento_preenche_codunicoembalagemfi()
			If !uf_faturacao_checkLenEmb(Alltrim(lccod), Alltrim(fi.U_CODEMB))
				Replace fi.U_CODEMB With ''
			Endif
		Else
			Select fi
			Select ucrsatendst
			Locate For ucrsatendst.ststamp=fi.fistamp
			If Found()
				If ucrsatendst.coduniemb==.T. .And. Empty(fi.U_CODEMB)
					uf_tecladoalpha_chama("FI.U_CODEMB", "Introduza o C�digo da Embalagem:", .F., .F., 2)
					uf_atendimento_preenche_codunicoembalagemfi()
					If !uf_faturacao_checkLenEmb(Alltrim(lccod), Alltrim(fi.U_CODEMB))
						Replace fi.U_CODEMB With ''
					Endif

				Endif
			Endif
		Endif
	Endif
	uf_facturacao_controlaeventoslinhas()

	Select fi
	If !Empty(fi.ref)

		Select fi_trans_info
		Locate For uf_gerais_compStr(fi_trans_info.recStamp, fi.fistamp)

		If !Found()

			Select fi
			lcsql = ""
			TEXT TO lcsql TEXTMERGE NOSHOW
         SELECT dispositivo_seguranca FROM fprod (nolock) WHERE fprod.cnp='<<ALLTRIM(fi.ref)>>'
			ENDTEXT
			If  .Not. uf_gerais_actgrelha("", "uCrsDadosCaixa", lcsql)
				uf_perguntalt_chama("N�o foi possivel encontrar registos.", "", "OK", 16)
				Return .F.
			Else
				If ucrsdadoscaixa.dispositivo_seguranca
					Select fi
					uf_insert_fi_trans_info_seminfo(fi.ref, fi.fistamp)
				Endif
				fecha("uCrsDadosCaixa")
			Endif

		Endif
	Endif

Endfunc
**
Procedure uf_facturacao_autocompletereffi
	Lparameters nkeycode
	Store 0 To lctamanhoantigo, lcnovotamanho
	With facturacao.pageframe1.page1.griddoc
		For i = 1 To .ColumnCount
			If Upper(.Columns(i).ControlSource)=="FI.REF"
				.Columns(i).text1.Value = Strtran(.Columns(i).text1.Value, Chr(39), ' ')
				facturacao.valorparapesquisa = Alltrim(.Columns(i).text1.Value)
				lctamanhoantigo = Len(Alltrim(.Columns(i).text1.Value))
				lcsql = ""
				TEXT TO lcsql TEXTMERGE NOSHOW
					SELECT TOP 1 convert(nvarchar,REF) as REF
					FROM
						ST (nolock)
					WHERE
						st.site_nr = <<mySite_nr>>
						AND ST.REF LIKE '<<Alltrim(facturacao.valorparapesquisa)>>%'
					ORDER BY
						REF ASC
				ENDTEXT
				uf_gerais_actgrelha("", "tempSQLAutoComp", lcsql)
				If Reccount("tempSQLAutoComp")>0
					Select tempsqlautocomp
					lcnovovalor = Alltrim(tempsqlautocomp.ref)
					lcnovotamanho = Len(Alltrim(tempsqlautocomp.ref))
					If mykeypressed<>127
						.Columns(i).text1.Value = Alltrim(tempsqlautocomp.ref)
						lc_diftamanhos = Abs(lcnovotamanho-lctamanhoantigo)
						If  .Not. Empty(lcnovovalor)
							.Columns(i).text1.SelStart = lctamanhoantigo
							.Columns(i).text1.SelLength = lc_diftamanhos
						Endif
					Endif
				Endif
			Endif
		Endfor
	Endwith
Endproc
**
Procedure uf_facturacao_autocompletedesign
	Lparameters nkeycode
	Store 0 To lctamanhoantigo, lcnovotamanho
	With facturacao.pageframe1.page1.griddoc
		For i = 1 To .ColumnCount
			If Upper(.Columns(i).ControlSource)=="FI.DESIGN"
				If mykeypressed=39
					.Columns(i).text1.Value = Strtran(.Columns(i).text1.Value, Chr(39), '')
				Endif
				If mykeypressed==32
					facturacao.valorparapesquisa = .Columns(i).text1.Value
					lctamanhoantigo = Len(Alltrim(.Columns(i).text1.Value))+1
					lcsql = ""
					TEXT TO lcsql TEXTMERGE NOSHOW
						SELECT TOP 1 convert(nvarchar,DESIGN) as DESIGN
						FROM
							ST (nolock)
						WHERE
							st.site_nr = <<mySite_nr>>
							AND ST.REF LIKE '<<Alltrim(facturacao.valorparapesquisa)>> %'
						ORDER BY
							REF ASC
					ENDTEXT
				Else
					facturacao.valorparapesquisa = .Columns(i).text1.Value
					lctamanhoantigo = Len(Alltrim(.Columns(i).text1.Value))
					lcsql = ""
					TEXT TO lcsql TEXTMERGE NOSHOW

						SELECT
							TOP 1 convert(nvarchar,DESIGN) as DESIGN
						FROM
							ST (nolock)
						WHERE
							st.site_nr = <<mySite_nr>>
							AND st.DESIGN LIKE '<<Alltrim(facturacao.valorparapesquisa)>>%'
						ORDER BY
							st.DESIGN ASC
					ENDTEXT
				Endif
				uf_gerais_actgrelha("", "tempSQLAutoComp", lcsql)
				If Reccount("tempSQLAutoComp")>0
					Select tempsqlautocomp
					lcnovovalor = Alltrim(tempsqlautocomp.Design)
					lcnovotamanho = Len(Alltrim(tempsqlautocomp.Design))
					If mykeypressed<>127 .And. mykeypressed<>7
						.Columns(i).text1.Value = Alltrim(tempsqlautocomp.Design)
						lc_diftamanhos = Abs(lcnovotamanho-lctamanhoantigo)
						If  .Not. Empty(lcnovovalor)
							.Columns(i).text1.SelStart = lctamanhoantigo
							.Columns(i).text1.SelLength = lc_diftamanhos
						Endif
					Endif
				Endif
			Endif
		Endfor
	Endwith
Endproc
**
Function uf_facturacao_pesquisarartigosdesignfi
	If myfactagravar
		myfactagravar = .F.
		Return .F.
	Endif
	ti = facturacao.pageframe1.page1.griddoc.ColumnCount
	For i = 1 To ti
		If Upper(facturacao.pageframe1.page1.griddoc.Columns(i).ControlSource)=="FI.DESIGN"
			Unbindevents(facturacao.pageframe1.page1.griddoc.Columns(i).text1, "LostFocus", facturacao.ocust, "proc_pesquisarArtigosDesignFi")
		Endif
	Endfor
	TEXT TO lcsql TEXTMERGE NOSHOW
		Select
			Count(ref) as valor
			,REF
		From
			ST (nolock)
		Where
			st.site_nr = <<mySite_nr>>
			and st.DESIGN LIKE '<<uf_gerais_trataPlicasSQL(Alltrim(facturacao.valorparapesquisa))>>%'
		Group By
			REF
	ENDTEXT
	If  .Not. uf_gerais_actgrelha("", "ucrsArtigos", lcsql)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA NA PROCURA DO ARTIGO. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
		Return .F.
	Endif
	If Reccount("ucrsArtigos")>0
		uf_pesqstocks_chama("FACTURACAO", facturacao.valorparapesquisa)
		fecha("ucrsArtigos")
	Endif
Endfunc
**
Function uf_facturacao_validacamposobrigatorios
	Local lc_numregistos, lcmotisenvaoiva
	Store .F. To lcmotisenvaoiva
	lcmotisencaoiva = uf_gerais_getparameter("ADM0000000261", "Bool")
	Select ft
	Goto Top
	If Empty(ft.nmdoc)
		uf_perguntalt_chama("EXISTEM CAMPOS OBRIGAT�RIOS POR PREENCHER. POR FAVOR VERIFIQUE. CAMPO: DOCUMENTO.", "OK", "", 48)
		Return .F.
	Endif
	If Empty(ft.Nome)
		uf_perguntalt_chama("EXISTEM CAMPOS OBRIGAT�RIOS POR PREENCHER. POR FAVOR VERIFIQUE. CAMPO: NOME.", "OK", "", 48)
		Return .F.
	Endif
	If Empty(ft.NO) .Or. ft.NO==0
		uf_perguntalt_chama("EXISTEM CAMPOS OBRIGAT�RIOS POR PREENCHER. POR FAVOR VERIFIQUE. CAMPO: N� Cliente.", "OK", "", 48)
		Return .F.
	Endif
	If Empty(ft.fno)
		uf_perguntalt_chama("EXISTEM CAMPOS OBRIGAT�RIOS POR PREENCHER. POR FAVOR VERIFIQUE. CAMPO: N�MERO DOCUMENTO.", "OK", "", 48)
		Return .F.
	Endif
	If Empty(ft.fdata)
		uf_perguntalt_chama("EXISTEM CAMPOS OBRIGAT�RIOS POR PREENCHER. POR FAVOR VERIFIQUE. CAMPO: DATA DOCUMENTO.", "OK", "", 48)
		Return .F.
	Endif
	If Empty(ft.moeda)
		Local lcmoedadefault
		Store 'EURO' To lcmoedadefault
		lcmoedadefault = uf_gerais_getparameter("ADM0000000260", "text")
		If Empty(lcmoedadefault)
			lcmoedadefault = 'EURO'
		Endif
		Select ft
		Replace ft.moeda With lcmoedadefault
	Endif
	If Empty(ft.moeda)
		uf_perguntalt_chama("EXISTEM CAMPOS OBRIGAT�RIOS POR PREENCHER. POR FAVOR VERIFIQUE. CAMPO: MOEDA", "OK", "", 48)
		Return .F.
	Endif
	If Reccount("Fi")==0
		uf_perguntalt_chama("N�O � POSSIVEL GRAVAR DOCUMENTOS SEM LINHAS. POR FAVOR VERIFIQUE.", "OK", "", 48)
		Return .F.
	Endif
	If lcmotisencaoiva==.T. .And. Upper(mypaisconfsoftw)<>'ANGOLA'
		If  .Not. Empty(Alltrim(td.tiposaft)) .And. Empty(ft2.motiseimp) .And. Empty(ucrse1.motivo_isencao_iva)
			Select fi
			Goto Top
			Scan
				If fi.iva==0
					uf_perguntalt_chama("Existem produtos com Iva a 0% mas n�o definiu o Motivo de Isen��o. Por favor verifique.", "OK", "", 48)
					Return .F.
				Endif
			Endscan
		Endif
	Endif


	Local lcBoolParam
	lcBoolParam = uf_gerais_getparameter("ADM0000000368","BOOL")

	Select fi
	Goto Top
	Scan
		Local lcOrefValido
		lcOrefValido = .T.

		** valida se deve validar o fi.oref
		If(lcBoolParam And  Empty(fi.oref)) Or (!lcBoolParam)
			lcOrefValido= .F.
		Endif



		If Empty(fi.ref) .And.  !lcOrefValido .And.  .Not. Empty(fi.Design) .And. fi.qtt<>0
			uf_perguntalt_chama("Existem linhas sem refer�ncia preenchida. Por favor verifique.", "OK", "", 48)
			Return .F.
		Endif
	Endscan
	If Alltrim(Upper(Alltrim(ft.nmdoc)))=="VENDA MANUAL"
		Select ft2
		If Empty(Alltrim(ft2.u_docorig))
			uf_perguntalt_chama("EXISTEM CAMPOS OBRIGAT�RIOS POR PREENCHER. POR FAVOR VERIFIQUE. CAMPO: DOCUMENTO DE ORIGEM PARA VENDAS MANUAIS.", "OK", "", 48)
			Return .F.
		Endif
		If  .Not. (Like('*/*', Alltrim(ft2.u_docorig))) .Or. (Empty(Strextract(Alltrim(ft2.u_docorig), '/'))) .Or. (Occurs('/', Alltrim(ft2.u_docorig))>1)
			uf_perguntalt_chama("FORMATO DO N� DO DOCUMENTO ORIGINAL INV�LIDO. POR FAVOR PREENCHA A S�RIE/N� DOCUMENTO."+Chr(13)+"EX: ABC/00001", "OK", "", 48)
			Return .F.
		Endif
	Endif
	If uf_gerais_getparameter("ADM0000000211", "BOOL") .And. uf_gerais_getparameter("ADM0000000215", "BOOL")==.F.
		If td.lancasl==.T.
			Select fi
			Goto Top
			Scan
				If  .Not. Empty(fi.usalote) .And. Empty(Alltrim(fi.lote)) .And.  .Not. Empty(fi.ref)
					Select fi
					facturacao.pageframe1.page1.textpesq.Value = Alltrim(fi.ref)
					uf_facturacao_criacursorprocura()
					uf_perguntalt_chama("Existem linhas em que o Lote n�o foi identificado."+Chr(13)+"Por favor verifique.", "OK", "", 48)
					Return .F.
				Endif
			Endscan
			Select fi
			Goto Top
		Endif
	Endif
	Select td
	**IF td.u_tipodoc==3
	**   SELECT ft2
	**   IF UPPER(ALLTRIM(ft2.u_abrev))=="SNS" .OR. UPPER(ALLTRIM(ft2.u_abrev2))=="SNS"
	**      SELECT ft
	**      IF YEAR(ft.cdata)==1900
	**         uf_perguntalt_chama("O campo Dt. Rec. � de preenchimento obrigat�rio para planos do SNS.", "OK", "", 48)
	**         RETURN .F.
	**      ENDIF
	**   ENDIF
	**ENDIF
	If Used("ucrsProdutosInactivosDoc")
		fecha("ucrsProdutosInactivosDoc")
	Endif
	TEXT TO lcsql TEXTMERGE NOSHOW
		select ref as refinativa from st where inactivo = 1 AND site_nr = <<mysite_nr>>
	ENDTEXT
	If  .Not. uf_gerais_actgrelha("", "ucrsRefsInativas", lcsql)
		uf_perguntalt_chama("N�O FOI POSSIVEL VERIFICAR A LISTA DE REFER�NCIAS INATIVAS. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
		Return .F.
	Endif
	Select Distinct refinativa From fi Left Join ucrsRefsInativas On fi.ref=ucrsRefsInativas.refinativa Where refinativa Is Not Null  Into Cursor ucrsProdutosInactivosDoc Readwrite
	Local lcrefs
	If Reccount("ucrsProdutosInactivosDoc")<>0
		lcrefs = ""
		Select ucrsProdutosInactivosDoc
		Goto Top
		Scan
			If Empty(lcrefs)
				lcrefs = Alltrim(ucrsProdutosInactivosDoc.refinativa)
			Else
				lcrefs = lcrefs+", "+Alltrim(ucrsProdutosInactivosDoc.refinativa)
			Endif
		Endscan
		uf_perguntalt_chama("Existem produtos inativos no documento: Ex. "+lcrefs+Chr(13)+Chr(13)+"N�o � possivel gravar.", "OK", "", 16)
		Return .F.
	Endif
	Return .T.
Endfunc
**
Function uf_facturacao_regrasgravacao
	Lparameters llatend
	Local lcsql, lctab2, lcref, lnmaxemb, lnmaxuni, lnalias, lccodigo, lnlin, lctab, validado, lccntlin, lccntlin2
	Store "" To lcsql, lcref
	Store 0 To lnmaxemb, lnmaxuni, lnlin, lccntlin, lccntlin2
	Store .F. To validado
	If uf_gerais_getparameter("ADM0000000255", "BOOL") .And. Empty(ft.ncont)
		uf_perguntalt_chama("O N�mero de Contribuinte � obrigat�rio.", "OK", "", 64)
		Return .F.
	Endif
	Select ucrse1
	If  .Not. emdesenvolvimento .And. Alltrim(Upper(ucrse1.tipoempresa))<>"CLINICA"
		Select td
		If td.u_tipodoc=2 .Or. td.u_tipodoc=1
			uf_perguntalt_chama("N�O � POSS�VEL A INTRODU��O DESTE TIPO DE DOCUMENTO MANUALMENTE NO BACKOFFICE. POR FAVOR VERIFIQUE.", "OK", "", 64)
			Return .F.
		Endif
	Endif
	Select ft
	If  .Not. Empty(ft.exportado) .And. Alltrim(Upper(ft.nmdoc))<>'INSER��O DE RECEITA'
		uf_perguntalt_chama("O Documento j� foi exportado. As altera��es podem n�o ser refletidas no destino.", "OK", "", 64)
	Endif


	Local lccorrigecompartexterna
	lccorrigecompartexterna = .F.
	If (Vartype(myinserereceitacorrigida)<>'U')
		lccorrigecompartexterna = myinserereceitacorrigida
	Endif

	*!*	 IF uf_gerais_validaSeVendaMultiplaMCDTeAlerta()
	*!*	    RETURN .F.
	*!*	 ENDIF
	*!*

	If  myftintroducao And Used("FT2") And  !Empty(Alltrim(ft2.u_receita))
		uf_atendimento_validaSeEpisodioPEHMFacturado("7532622",Alltrim(ft2.u_receita))
	Endif




	If Alltrim(ft2.u_codigo)=='RS' .And. Empty(lccorrigecompartexterna)
		uf_perguntalt_chama("O PLANO DE COMPARTICIPA��O S� PODE SER APLICADO NO ATENDIMENTO.", "OK", "", 48)
		Return .F.
	Endif
	If  .Not. Empty(ft2.u_codigo) .Or.  .Not. Empty(ft2.u_codigo2)
		TEXT TO lcsql TEXTMERGE NOSHOW
			select e_compart, reinsereReceita, permiteBackoffice from cptpla(nolock) where codigo='<<ALLTRIM(ft2.u_codigo)>>' or codigo='<<ALLTRIM(ft2.u_codigo2)>>' AND inactivo = 0
		ENDTEXT
		If  .Not. uf_gerais_actgrelha("", "uCrsCompart", lcsql)
			uf_perguntalt_chama("N�o foi poss�vel verificar o plano do documento. Por favor contacte o Suporte.", "OK", "", 16)
			Return .F.
		Endif

		If Reccount("uCrsCompart")>0
			Select ucrscompart
			Goto Top
			Scan
				If Empty(ucrscompart.permiteBackoffice)
					uf_perguntalt_chama("OS PLANOS DE COMPARTICIPA��O S� PODEM SER APLICADOS NO ATENDIMENTO.", "OK", "", 48)
					Return .F.
				Endif
				If(Empty(ucrscompart.reinsereReceita) And   !Empty(lccorrigecompartexterna))
					uf_perguntalt_chama("N�o � permitida a reinser��o de receitas para este plano de comparticipa��o.", "OK", "", 48)
					Return .F.
				Endif

			Endscan
		Endif
	Endif

	Select ft
	If ft.NO==200
		If uf_gerais_getparameter("ADM0000000122", "BOOL")
			uf_perguntalt_chama("O SISTEMA APENAS PERMITE VENDER A CLIENTES COM FICHA. DEVE ESCOLHER UM CLIENTE ANTES DE PODER CONTINUAR.", "OK", "", 64)
			Return .F.
		Endif
	Endif
	If  .Not. llatend
		Select ft
		If  .Not. (ft.NO==200)
			If uf_gerais_getparameter("ADM0000000128", "BOOL")
				If facturacao.nocreditcl
					uf_perguntalt_chama("O CLIENTE SELECCIONADO TEM A FACTURA��O CANCELADA. ASSIM N�O PODE CONTINUAR.", "OK", "", 64)
					Return .F.
				Endif
			Endif
		Endif
	Endif
	Local lcficombi
	Store .F. To lcficombi
	If Upper(Alltrim(ucrse1.pais))<>'PORTUGAL'
		Select * From fi Where  Not Empty(fi.bistamp) Into Cursor ucrsfibi Readwrite
		If Reccount("ucrsfibi")>0
			lcficombi = .T.
		Endif
		fecha("ucrsfibi")
	Endif
	lcsql = ""
	TEXT TO lcsql TEXTMERGE NOSHOW
		select eplafond, esaldo, no_ext from b_utentes where no = <<ft.no>> and estab = <<ft.estab>>
	ENDTEXT
	If  .Not. uf_gerais_actgrelha("", "uCrsClplafond", lcsql)
		uf_perguntalt_chama("N�o foi poss�vel verificar o plafond do cliente. Por favor contacte o Suporte.", "OK", "", 16)
		Return .F.
	Endif

	Local lcEsaldo
	lcEsaldo = uf_gerais_getUmValor("cc", "ISNULL(sum(edeb - edebf) - sum(ecred - ecredf),0)", "cc.no = " +Str(ft.NO )+ " and estab ="  +Str(ft.estab))

	If !Empty(lcEsaldo )
		Select uCrsClplafond
		Goto Top
		Replace uCrsClplafond.esaldo With lcEsaldo
	Endif

	Local lcplafondcl, lcsaldocl, lcno_ext
	Store 0 To lcplafondcl, lcsaldocl
	Store '' To lcno_ext
	Select uCrsClplafond
	Goto Top
	lcplafondcl = uCrsClplafond.eplafond
	lcsaldocl = uCrsClplafond.esaldo
	lcno_ext = uCrsClplafond.no_ext
	If Upper(Alltrim(ucrse1.pais))<>'PORTUGAL' .And. Empty(lcno_ext) .And. td.u_tipodoc==9 .And. lcficombi=.F.
		uf_perguntalt_chama("N�o pode emitir documentos a cr�dito a clientes que n�o tenham n�mero externo!", "OK", "", 64)
		Return .F.
	Endif

	Select td
	Goto Top
	If ucrse1.controla_plafond==.T.

		If td.u_tipodoc==9
			Select ft
			If ft.NO<>200 .And. ucrse1.plafond_cliente>0

				If !uf_gerais_varificaCreditoEPalavrapasse(ft.NO, ft.estab)

					Return .F.
				Endif

				If lcplafondcl==0

					*!*	             IF (ft.etotal+ucrsclplafond.esaldo)>ucrse1.plafond_cliente
					*!*	                IF UPPER(ALLTRIM(ucrse1.pais))=='PORTUGAL'
					*!*	                   IF uf_perguntalt_chama("O plafond do cliente foi ultrapassado."+CHR(13)+"Pretende continuar com o atendimento?", "Sim", "N�o", 64)
					*!*	                      PUBLIC cval
					*!*	                      STORE '' TO cval
					*!*	                      uf_tecladoalpha_chama("cval", "INTRODUZA PASSWORD PARA PLAFOND:", .T., .F., 0)
					*!*	                      IF EMPTY(cval)
					*!*	                         uf_perguntalt_chama("Deve introduzir a password de Supervisor para permitir ultrapassar o plafond.", "OK", "", 64)
					*!*	                         RETURN .F.
					*!*	                      ELSE
					*!*	                         IF  .NOT. (UPPER(ALLTRIM(cval))==UPPER(ALLTRIM(uf_gerais_getparameter('ADM0000000187', 'TEXT'))))
					*!*	                            uf_perguntalt_chama("A password de Supervisor n�o est� correcta.", "OK", "", 64)
					*!*	                            RETURN .F.
					*!*	                         ENDIF
					*!*	                      ENDIF
					*!*	                   ELSE
					*!*	                      RETURN .F.
					*!*	                   ENDIF
					*!*	                ELSE
					*!*	                   IF  .NOT. EMPTY(lcno_ext)
					*!*	                      uf_perguntalt_chama("O plafond do cliente foi ultrapassado."+CHR(13)+"N�o pode concluir o documento.", "OK", "", 64)
					*!*	                      RETURN .F.
					*!*	                   ENDIF
					*!*	                ENDIF
					*!*	             ENDIF
				Else
					If (ft.etotal+uCrsClplafond.esaldo)>lcplafondcl
						If Upper(Alltrim(ucrse1.pais))=='PORTUGAL'
							If uf_perguntalt_chama("O plafond do cliente foi ultrapassado."+Chr(13)+"Pretende continuar com o atendimento?", "Sim", "N�o", 64)
								Public cval
								Store '' To cval
								If uf_gerais_getparameter('ADM0000000187', 'BOOL')
									uf_tecladoalpha_chama("cval", "INTRODUZA PASSWORD PARA PLAFOND:", .T., .F., 0)
									If Empty(cval)
										uf_perguntalt_chama("Deve introduzir a password de Supervisor para permitir ultrapassar o plafond.", "OK", "", 64)
										Return .F.
									Else
										If  .Not. (Upper(Alltrim(cval))==Upper(Alltrim(uf_gerais_getparameter('ADM0000000187', 'TEXT'))))
											uf_perguntalt_chama("A password de Supervisor n�o est� correcta.", "OK", "", 64)
											Return .F.
										Endif
									Endif
								Endif
							Else
								Return .F.
							Endif
						Else
							If  .Not. Empty(lcno_ext)
								uf_perguntalt_chama("O plafond do cliente foi ultrapassado."+Chr(13)+"N�o pode concluir o documento.", "OK", "", 64)
								Return .F.
							Endif
						Endif
					Endif
				Endif
			Else
				Select ft
				Goto Top
				If ft.NO<>200

					If !uf_gerais_varificaCreditoEPalavrapasse(ft.NO, ft.estab)

						Return .F.
					Endif

					If (ft.etotal+uCrsClplafond.esaldo)>lcplafondcl
						If Upper(Alltrim(ucrse1.pais))=='PORTUGAL'
							If uf_perguntalt_chama("O plafond do cliente foi ultrapassado."+Chr(13)+"Pretende continuar com o atendimento?", "Sim", "N�o", 64)
								Public cval
								Store '' To cval
								If uf_gerais_getparameter('ADM0000000187', 'BOOL')
									uf_tecladoalpha_chama("cval", "INTRODUZA PASSWORD PARA PLAFOND:", .T., .F., 0)
									If Empty(cval)
										uf_perguntalt_chama("Deve introduzir a password de Supervisor para permitir ultrapassar o plafond.", "OK", "", 64)
										Return .F.
									Else
										If  .Not. (Upper(Alltrim(cval))==Upper(Alltrim(uf_gerais_getparameter('ADM0000000187', 'TEXT'))))
											uf_perguntalt_chama("A password de Supervisor n�o est� correcta.", "OK", "", 64)
											Return .F.
										Endif
									Endif
								Endif
							Else
								Return .F.
							Endif
						Else
							If  .Not. Empty(lcno_ext)
								uf_perguntalt_chama("O plafond do cliente foi ultrapassado."+Chr(13)+"N�o pode concluir o documento.", "OK", "", 64)
								Return .F.
							Endif
						Endif
					Endif
				Endif
			Endif
		Endif
	Endif

	If myftintroducao
		Select td
		If td.u_tipodoc<>3
			Select ft
			lcsql = ""
			TEXT TO lcsql TEXTMERGE NOSHOW
				SELECT	case when max(fdata) > '<<uf_gerais_getDate(ft.fdata,"SQL")>>' then 1 else 0 end as valor
				FROM	ft (nolock)
				WHERE	ndoc = <<Ft.Ndoc>> And YEAR(fdata) = YEAR('<<uf_gerais_getDate(Ft.fdata,"SQL")>>')
			ENDTEXT
			If  .Not. uf_gerais_actgrelha("", "uCrsMaxNoValida", lcsql)
				uf_perguntalt_chama("N�O FOI POSSIVEL VALIDAR A NUMERA��O DO �LTIMO DOCUMENTO.", "OK", "", 16)
				Return .F.
			Else
				If Reccount()>0
					If ucrsmaxnovalida.valor==1
						uf_perguntalt_chama("J� EXISTE UM DOCUMENTO DESTE TIPO COM DATA SUPERIOR. POR FAVOR VERIFIQUE.", "OK", "", 64)
						fecha("uCrsMaxNoValida")
						Return .F.
					Endif
				Endif
				fecha("uCrsMaxNoValida")
			Endif
		Endif
	Endif

	Select fi
	Calculate Cnt() To lccntlin For fi.tabiva=0
	If lccntlin>0
		uf_perguntalt_chama("Existem linhas que n�o t�m a Tabela de Iva definida. Por favor verifique.", "OK", "", 64)
		Return .F.
	Endif
	lccntlin = 0
	If  .Not. uf_gerais_actgrelha("", "", "SELECT dbo.up_GerarHash('')")
		uf_perguntalt_chama("FUN��O DE GEST�O DE ASSINATURAS DIGITAIS N�O DISPON�VEL, POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
		Return .F.
	Endif
	If myftintroducao==.T.
		Select ft
		If  .Not. Empty(Alltrim(td.tiposaft))
			lcsql = ""
			TEXT TO lcsql TEXTMERGE NOSHOW
				exec up_cert_verificaDocAnterior <<ft.fno>>,<<ft.ndoc>>,'<<uf_gerais_getDate(ft.fdata,"SQL")>>','<<ALLTRIM(td.tiposaft)>>'
			ENDTEXT
			If  .Not. uf_gerais_actgrelha("", "uCrstemp", lcsql)
				uf_perguntalt_chama("O DOCUMENTO ANTERIOR N�O EST� ASSINADO DIGITALMENTE. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
				Return .F.
			Else
				Select ucrstemp
				If ucrstemp.Count==0
					uf_perguntalt_chama("O DOCUMENTO ANTERIOR N�O EST� ASSINADO DIGITALMENTE. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
					fecha("uCrstemp")
					Return .F.
				Endif
			Endif
			If Used("uCrstemp")
				fecha("uCrstemp")
			Endif
		Endif
	Endif
	If myftalteracao
		If  .Not. Empty(Alltrim(td.tiposaft))
			If  .Not. Used("uCrsFtOrig")
				uf_perguntalt_chama("OCORREU UM ERRO A VALIDAR AS ALTERA��ES AO DOCUMENTO.", "OK", "", 16)
				Return .F.
			Endif
			If Reccount("uCrsFtOrig")>0
				Select ft
				Goto Top
				Select uCrsFtOrig
				Goto Top
				If (ft.etotal<>uCrsFtOrig.etotal) .Or. (ft.fno<>uCrsFtOrig.fno) .Or.  .Not. (uf_gerais_getdate(ft.fdata, "SQL")==uf_gerais_getdate(uCrsFtOrig.fdata, "SQL")) .Or. (ft.totqtt<>uCrsFtOrig.totqtt) .Or.  .Not. (Alltrim(ft.ncont)==Alltrim(uCrsFtOrig.ncont)) .Or.  .Not. (Alltrim(ft.Nome)==Alltrim(uCrsFtOrig.Nome)) .Or. (ft.NO<>uCrsFtOrig.NO)
					uf_perguntalt_chama("DE ACORDO COM AS REGRAS DE CERTIFICA��O DE SOFTWARES DE FACTURA��O N�O PODE GRAVAR AS ALTERA��ES.", "OK", "", 64)
					Return .F.
				Endif
				Select ft2
				Goto Top
				Select uCrsFtOrig
				Goto Top
				If  .Not. (Alltrim(ft2.motiseimp)==Alltrim(uCrsFtOrig.motiseimp))
					uf_perguntalt_chama("DE ACORDO COM AS REGRAS DE CERTIFICA��O DE SOFTWARES DE FACTURA��O N�O PODE GRAVAR AS ALTERA��ES.", "OK", "", 64)
					Return .F.
				Endif
				Select fi
				Goto Top
				Scan
					Select uCrsFtOrig
					Goto Top
					Scan For Alltrim(uCrsFtOrig.fistamp)==Alltrim(fi.fistamp)
						If (fi.epv<>uCrsFtOrig.epv) .Or. (fi.etiliquido<>uCrsFtOrig.etiliquido) .Or. (fi.iva<>uCrsFtOrig.iva) .Or. (fi.tabiva<>uCrsFtOrig.tabiva) .Or. (fi.desconto<>uCrsFtOrig.desconto) .Or. (fi.qtt<>uCrsFtOrig.qtt)
							lcvalidaaltfi = .T.
							uf_perguntalt_chama("DE ACORDO COM AS REGRAS DE CERTIFICA��O DE SOFTWARES DE FACTURA��O N�O PODE GRAVAR AS ALTERA��ES.", "OK", "", 64)
							Return .F.
						Endif
						Select uCrsFtOrig
					Endscan
					Select fi
				Endscan
			Endif
		Endif
	Endif
	lcmoedadefault = uf_gerais_getparameter("ADM0000000260", "text")
	If Empty(lcmoedadefault)
		If  .Not. Empty(Alltrim(td.tiposaft))
			If ft.etotal>=1000
				If Empty(Alltrim(ft.ncont)) .Or. Alltrim(ft.ncont)=='999999990' .Or. Empty(Alltrim(ft.morada)) .Or. Empty(Alltrim(ft.Nome)) .And. Upper(Alltrim(ucrse1.pais))=='PORTUGAL'
					uf_perguntalt_chama("Em documentos de fatura��o superiores a 1000� � obrigat�rio indicar o NIF, Nome e Morada do Utente.", "OK", "", 48)
					Return .F.
				Endif
			Endif
		Endif
	Endif

	Select ft2
	lccodigo = ft2.u_codigo
	Select fi
	Calculate Cnt() To lccntlin For ( .Not. Empty(fi.ref) .Or.  .Not. Empty(fi.Design)) .And. fi.qtt<0
	If lccntlin>0
		uf_perguntalt_chama("N�o pode colocar quantidades a negativo.", "OK", "", 64)
		Return .F.
	Endif
	Select fi
	Goto Top
	Calculate Cnt() To lccntlin For ( .Not. Empty(fi.ref) .Or.  .Not. Empty(fi.Design)) .And. fi.qtt>0
	If lccntlin=0
		uf_perguntalt_chama("N�O PODE GRAVAR UMA VENDA SEM PRODUTOS."+Chr(10)+Chr(10)+"PELO MENOS UMA LINHA COM VALOR DEVE EXISTIR [QUANTIDADE>0].", "OK", "", 64)
		Return .F.
	Endif
	lccntlin = 0
	Select td
	If td.lancacc==.T.
		If ft.NO==200
			uf_perguntalt_chama("N�O PODE VENDER A CR�DITO SEM PRIMEIRO ESCOLHER UM CLIENTE COM FICHA.", "OK", "", 64)
			Return .F.
		Endif
		If td.u_tipodoc==4
			If Upper(Alltrim(ucrsmodofact.modofact))=='FACTURA��O ACORDO'
				facturacao.modofactcl = ucrsmodofact.modofact
			Endif
			If Upper(Alltrim(facturacao.modofactcl))<>'FACTURA��O ACORDO'
				uf_perguntalt_chama("S� PODE CRIAR DOCUMENTOS DESTE TIPO PARA CLIENTES COM O MODO DE FACTURA��O IGUAL A 'FACTURA��O ACORDO'.", "OK", "", 64)
				Return .F.
			Endif
		Endif
	Endif
	Select fi
	Calculate Cnt() To lccntlin For fi.etiliquido=0
	If lccntlin>0 .And.  .Not. llatend
		If  .Not. uf_perguntalt_chama("Existem linhas com total a Zero. Pretende continuar com a grava��o?", "SIM", "N�O", 32)
			Return .F.
		Endif
	Endif

	lccntlin = 0
	Select ft
	If Empty(Alltrim(ft.u_nratend)) .And. Type("ATENDIMENTO")=="U"
		uf_atendimento_geranratendimento()
	Else
		If Vartype(nratendimento)=="U"
			uf_atendimento_geranratendimento()
		Else
			Replace ft.u_nratend With Alltrim(nratendimento)
		Endif
	Endif
	Select ft
	If Empty(ft.site)
		Replace site With Alltrim(mysite) In ft
	Endif
	If Empty(ft.pnome)
		Replace pnome With Alltrim(myTerm) In ft
		Replace pno With mytermno In ft
	Endif
	If Empty(ft.vendedor)
		Replace vendedor With ch_vendedor
		Replace vendnm With Alltrim(ch_vendnm)
	Endif
	Select td
	If td.u_tipodoc==5 .Or. td.u_tipodoc==13
		Return .T.
	Endif

	Select td
	If td.tipodoc==3
		Select ucrslojas
		lcinfarmed = ucrslojas.infarmed
		Select ft
		If ft.NO<199 .And.  .Not. Empty(lcinfarmed) .And. Upper(Alltrim(ucrse1.pais))=='PORTUGAL'
			Select fi
			Locate For Empty(Alltrim(fi.ofistamp)) .And. ( .Not. Empty(Alltrim(fi.ref)) .Or.  .Not. Empty(fi.etiliquido))
			If Found()
				uf_perguntalt_chama("Existem linhas sem liga��o ao documento de origem. Verifique pf.", "OK", "", 64)
				Return .F.
			Endif
		Endif
		Return .T.
	Endif

	If uf_gerais_getparameter("ADM0000000211", "BOOL") .And. Alltrim(ucrse1.tipoempresa)<>'FARMACIA'
		If uf_gerais_getparameter("ADM0000000215", "BOOL")==.F.
			If  .Not. uf_facturacao_validastocklotes()
				Return .F.
			Endif
		Endif
		Select fi
		Goto Top
		Locate For ( .Not. Empty(fi.u_psico) .Or.  .Not. Empty(fi.u_benzo)) .And.  .Not. Empty(Alltrim(fi.ref))
		If Found()
			Select td
			If td.lancasl
				uf_pagamento_contadorpsicobenzo()
			Endif
		Endif
		Return .T.
	Endif

	If  .Not. Empty(lccodigo)
		TEXT TO lcsql TEXTMERGE NOSHOW
			exec up_receituario_dadosDoPlano '<<lcCodigo>>'
		ENDTEXT
		uf_gerais_actgrelha("", "uCrsCodPla", lcsql)
		If Reccount('uCrsCodPla')=0
			uf_perguntalt_chama("ESTE C�DIGO N�O EXISTE NA TABELA DE PLANOS DE COMPARTICIPA��O. RECTIFIQUE O MESMO.", "OK", "", 64)
			Return .F.
		Endif
	Endif
	Local lctiporeceita
	lctiporeceita = "RM"
	If myftalteracao
		Select fi
		Goto Top
		Scan
			lcref = fi.ref
			If (Alltrim(fi.tipor)=="RS" .Or. Alltrim(fi.tipor)=="RSP")
				lctiporeceita = "RSP"
			Endif
			If fi.u_comp
				TEXT TO lcsql TEXTMERGE NOSHOW
					Select
						protocolo
					FROM
						fprod (nolock)
					WHERE
						cnp='<<ALLTRIM(lcRef)>>'
				ENDTEXT
				If uf_gerais_actgrelha("", "uCrsFprod", lcsql)
					If Reccount("uCrsFprod")>0
						If ucrsfprod.protocolo==.T.
							lccntlin = lccntlin+1
						Else
							lccntlin2 = lccntlin2+1
						Endif
					Endif
					fecha('uCrsFProd')
				Else
					uf_perguntalt_chama("ATEN��O, OCORREU UM ERRO A VALIDAR CARACTERISTICAS DO PRODUTO. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
					Return .F.
				Endif
			Endif
		Endscan
		If (Used("FIinsercaoReceita"))
			If (Reccount("FIinsercaoReceita")>0)
				Select fiinsercaoreceita
				Goto Top
				Scan
					lcref = fiinsercaoreceita.ref
					If (Alltrim(fiinsercaoreceita.tipor)=="RS" .Or. Alltrim(fiinsercaoreceita.tipor)=="RSP")
						lctiporeceita = "RSP"
					Endif
					If fiinsercaoreceita.u_comp
						TEXT TO lcsql TEXTMERGE NOSHOW
							Select
								protocolo
							FROM
								fprod (nolock)
							WHERE
								cnp='<<ALLTRIM(lcRef)>>'
						ENDTEXT
						If uf_gerais_actgrelha("", "uCrsFprod", lcsql)
							If Reccount("uCrsFprod")>0
								If ucrsfprod.protocolo==.T.
									lccntlin = lccntlin+1
								Else
									lccntlin2 = lccntlin2+1
								Endif
							Endif
							fecha('uCrsFProd')
						Else
							uf_perguntalt_chama("ATEN��O, OCORREU UM ERRO A VALIDAR CARACTERISTICAS DO PRODUTO. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
							Return .F.
						Endif
					Endif
				Endscan
			Endif
		Endif
	Endif

	If myftintroducao
		Select fi
		Goto Top
		Scan
			Select ucrsatendst
			Locate For Upper(Alltrim(ucrsatendst.ref))==Upper(Alltrim(fi.ref))
			If (Alltrim(fi.tipor)=="RS" .Or. Alltrim(fi.tipor)=="RSP")
				lctiporeceita = "RSP"
			Endif
			If ( .Not. Empty(ucrsatendst.ref))
				If ucrsatendst.protocolo=.T.
					lccntlin = lccntlin+1
				Else
					lccntlin2 = lccntlin2+1
				Endif
			Endif
		Endscan
		If (Used("FIinsercaoReceita"))
			If (Reccount("FIinsercaoReceita")>0)
				Select fiinsercaoreceita
				Goto Top
				Scan
					Select ucrsatendst
					Locate For Upper(Alltrim(ucrsatendst.ref))==Upper(Alltrim(fiinsercaoreceita.ref))
					If (Alltrim(fiinsercaoreceita.tipor)=="RS" .Or. Alltrim(fiinsercaoreceita.tipor)=="RSP")
						lctiporeceita = "RSP"
					Endif
					If ( .Not. Empty(ucrsatendst.ref))
						If ucrsatendst.protocolo=.T.
							lccntlin = lccntlin+1
						Else
							lccntlin2 = lccntlin2+1
						Endif
					Endif
				Endscan
			Endif
		Endif
	Endif

	If (lccntlin>0 .And. lccntlin2>0) .And. Alltrim(lctiporeceita)<>"RSP"
		uf_perguntalt_chama("ATEN��O: NA MESMA VENDA N�O PODE VENDER PRODUTOS PROTOCOLO E N�O PROTOCOLO."+Chr(10)+Chr(10)+"POR FAVOR RECTIFIQUE A VENDA.", "OK", "", 48)
		Return .F.
	Endif
	If  .Not. Empty(lccodigo)
		Select fi
		Goto Top
		Scan
			If Upper(Alltrim(fi.u_diploma))=="CFT 2.10"
				Replace fi.u_diploma With ""
			Endif
			If fi.u_comp
				validado = .T.
			Endif
			If ucrscodpla.diploma="0" .And.  .Not. Empty(fi.u_diploma)
				uf_perguntalt_chama("ATEN��O: O PLANO '["+Upper(Alltrim(lccodigo))+"] "+Upper(Alltrim(ucrscodpla.Design))+"' N�O PERMITE DIPLOMAS ASSOCIADOS."+Chr(10)+"RECTIFIQUE A VENDAS ANTES DE CONTINUAR.", "OK", "", 64)
				Return .F.
			Endif
		Endscan
		If validado=.F.
			uf_perguntalt_chama("ATEN��O: TEM UM PLANO ASSOCIADO � VENDA, MAS N�O TEM PRODUTOS MARCADOS PARA COPARTICIPA��O."+Chr(10)+Chr(10)+"POR FAVOR RECTIFIQUE ANTES DE CONTINUAR.", "OK", "", 64)
			Return .F.
		Endif
	Else
		Select fi
		Goto Top
		Scan
			If fi.u_comp=.T.
				uf_perguntalt_chama("ATEN��O: ESTA VENDA N�O TEM PLANO, MAS TEM PRODUTOS MARCADOS PARA COMPARTICIPA��O."+Chr(10)+Chr(10)+"POR FAVOR RECTIFIQUE ANTES DE CONTINUAR.", "OK", "", 64)
				Return .F.
			Endif
		Endscan
	Endif
	If lccntlin>0 .And. lccntlin2=0
		If Used("uCrsCodPla")
			If  .Not. ucrscodpla.protocolo .And. lctiporeceita<>'RSP'
				uf_perguntalt_chama("ATEN��O: SE A VENDA CONTEM PRODUTOS PROTOCOLO, O PLANO TAMB�M TEM DE SER PARA PROTOCOLO."+Chr(10)+Chr(10)+"POR FAVOR RECTIFIQUE ANTES DE CONTINUAR.", "OK", "", 64)
				Return .F.
			Else
				Select ft
				Replace ft.segmento With "protocolo"
			Endif
		Endif
	Endif
	Store 0 To lccntlin, lccntlin2
	If  .Not. Empty(lccodigo)
		lnmaxemb = Iif(Reccount('uCrsCodPla')>0, ucrscodpla.maxembmed, 0)
		lnmaxuni = Iif(Reccount('uCrsCodPla')>0, ucrscodpla.maxembuni, 0)
		Create Cursor uCrsFi (ref C(18), qtt N(12, 3), u_ettent1 N(15, 3), u_ettent2 N(15, 3))
		Select fi
		Goto Top
		Scan
			lcref = Iif( .Not. Empty(fi.ref), fi.ref, fi.oref)
			If  .Not. Empty(lcref) .And. fi.partes==0 .And. Empty(fi.id_validacaodem)
				Insert Into uCrsFi (ref, qtt, u_ettent1, u_ettent2) Values (lcref, fi.qtt, fi.u_ettent1, fi.u_ettent2)
			Endif
		Endscan
		If Reccount('uCrsFi')>0
			Select ref, Sum(qtt) As ct From uCrsFi Where (u_ettent1+u_ettent2)>0 Group By ref Into Cursor uCrsGrpFi Readwrite
			Select uCrsGrpFi
			Calculate Sum(ct) To lnlin
		Else
			lnlin = 0
		Endif
		fecha('uCrsFi')
		If Used("uCrsVerificaRespostaDEM")
			If Alltrim(ucrsverificarespostadem.receita_tipo)<>'RSP'
				lcaux = .T.
			Else
				lcaux = .F.
			Endif
		Else
			lcaux = .T.
		Endif
		If lnlin>ucrscodpla.maxembrct .And.  .Not. Empty(lcaux)
			If  .Not. uf_perguntalt_chama("O n� m�ximo de embalagens do plano foi excedido. "+Chr(13)+Chr(10)+Chr(13)+Chr(10)+"N.� de Embalagens: "+ASTR(lnlin)+"; M�ximo do Plano: "+ASTR(ucrscodpla.maxembrct)+"."+Chr(13)+Chr(10)+Chr(13)+Chr(10)+"Se continuar o verso de receita ser� impresso em tal�o.", "Continuar", "Corrigir", 64)
				Return .F.
			Endif
		Endif
		If Used("uCrsGrpFi")
			Select uCrsGrpFi
			Scan
				If myftalteracao
					uf_gerais_actgrelha("", "uCrstipemb", [select tipembdescr, psico from fprod (nolock) where cnp=']+Alltrim(uCrsGrpFi.ref)+['])
					If Reccount("uCrstipemb")>0
						Select ucrstipemb
						Scan
							If ucrstipemb.tipembdescr='Embalagem N�o Unit�ria' .And. ucrstipemb.psico=.F. .And. uCrsGrpFi.ct>lnmaxemb .And. lnmaxemb>0
								uf_perguntalt_chama("O PLANO DE COMPARTICIPA��O '"+Upper(lccodigo)+"' N�O PERMITE PARA O ARTIGO '"+Alltrim(uCrsGrpFi.ref)+"' UM N�MERO DE EMBALAGENS SUPERIOR A "+ASTR(lnmaxemb)+". NO ENTANTO NESTA VENDA EXISTEM "+ASTR(uCrsGrpFi.ct)+" REFERENTES A ESTE PRODUTO.", "OK", "", 64)
								fecha('uCrsGrpFi')
								Select fi
								Return .F.
							Endif
							If (ucrstipemb.tipembdescr='Embalagem Unit�ria' .Or. ucrstipemb.psico=.T.) .And. uCrsGrpFi.ct>lnmaxuni .And. lnmaxemb>0
								uf_perguntalt_chama("O PLANO DE COMPARTICIPA��O '"+lccodigo+"' N�O PERMITE PARA O ARTIGO '"+Alltrim(uCrsGrpFi.ref)+"' UM N�MERO DE EMBALAGENS SUPERIOR A "+ASTR(lnmaxuni)+". NO ENTANTO NESTA VENDA EXISTEM "+ASTR(uCrsGrpFi.ct)+" REFERENTES A ESTE PRODUTO.", "OK", "", 64)
								fecha('uCrsGrpFi')
								Select fi
								Return .F.
							Endif
						Endscan
					Endif
					If Used("uCrstipemb")
						fecha("uCrstipemb")
					Endif
				Endif
				If myftintroducao
					Select ucrsatendst
					Locate For Alltrim(Upper(ucrsatendst.ref))==Upper(Alltrim(uCrsGrpFi.ref))
					If ucrsatendst.tipembdescr='Embalagem N�o Unit�ria' .And. ucrsatendst.psico=.F. .And. uCrsGrpFi.ct>lnmaxemb .And. lnmaxemb>0
						uf_perguntalt_chama("O PLANO DE COMPARTICIPA��O '"+Upper(lccodigo)+"' N�O PERMITE PARA O ARTIGO '"+Alltrim(uCrsGrpFi.ref)+"' UM N�MERO DE EMBALAGENS SUPERIOR A "+ASTR(lnmaxemb)+". NO ENTANTO NESTA VENDA EXISTEM "+ASTR(uCrsGrpFi.ct)+" REFERENTES A ESTE PRODUTO.", "OK", "", 64)
						fecha('uCrsGrpFi')
						Select fi
						Return .F.
					Endif
					If (ucrsatendst.tipembdescr='Embalagem Unit�ria' .Or. ucrsatendst.psico=.T.) .And. uCrsGrpFi.ct>lnmaxuni .And. lnmaxemb>0
						uf_perguntalt_chama("O PLANO DE COMPARTICIPA��O '"+lccodigo+"' N�O PERMITE PARA O ARTIGO '"+Alltrim(uCrsGrpFi.ref)+"' UM N�MERO DE EMBALAGENS SUPERIOR A "+ASTR(lnmaxuni)+". NO ENTANTO NESTA VENDA EXISTEM "+ASTR(uCrsGrpFi.ct)+" REFERENTES A ESTE PRODUTO.", "OK", "", 64)
						fecha('uCrsGrpFi')
						Select fi
						Return .F.
					Endif
				Endif
			Endscan
			fecha('uCrsGrpFi')
		Endif
	Endif
	Public lcnb
	Local lcnb2
	Store '' To lcnb, lcnb2
	If Type("recreserva")=='U'
		If  .Not. Empty(lccodigo) .And. ( .Not. ft.cobrado .Or. (ft.cobrado .And. ft2.u_vdsusprg))


			** validar se campo do beneficiario no formulario se encontra preenchido
			Select ft2
			Select ucrscodpla
			If(!Used("ucrsTrackId") And  ucrscodpla.beneficiario And !Empty(Alltrim(ft2.u_nbenef2)) )
				Select ft2
				uf_facturacaoChamaValidaCartao(Alltrim(ft2.u_nbenef2))
			Endif



			If ucrscodpla.beneficiario .And. Empty(ft2.u_nbenef2)
				**lcnb = INPUTBOX("QUAL O N� DE BENEFICI�RIO PARA ENTIDADE "+ALLTRIM(ucrscodpla.cptorgabrev), "Falta N� de Benefici�rio.", "")

				lcnb = ''
				uf_tecladoalpha_chama("lcnb", "QUAL O N� DE BENEFICI�RIO PARA ENTIDADE "+Alltrim(ucrscodpla.cptorgabrev), .F., .F., 0)
				If !Empty(ucrscodpla.regexCartao)
					If !uf_gerais_checkRegex(Alltrim(ucrscodpla.regexCartao), Alltrim(lcnb))
						uf_perguntalt_chama("Para esta comparticipa��o o N� do Cart�o ter� de ter o seguinte formato: " + Chr(13) + Alltrim(ucrscodpla.regexCartaoSample), "OK", "", 48)
						Return .F.
					Endif

				Endif
				If Empty(lcnb)
					uf_perguntalt_chama("TEM QUE INTRODUZIR O N� DE BENEFICI�RIO.", "OK", "", 64)
					Return .F.
				Else

					uf_facturacaoChamaValidaCartao(Alltrim(lcnb))

					fecha("ucrsTrackId")

					Select ft2
					Replace ft2.u_nbenef2 With lcnb
					Replace ft2.c2codpost With lcnb
					lcnb = ""
				Endif
			Endif
		Endif
		If Empty(ft2.u_receita) .Or. (Len(Alltrim(ft2.u_receita))<>13 .And. Len(Alltrim(ft2.u_receita))<>19)
			TEXT TO lcsql TEXTMERGE NOSHOW
				exec up_receituario_dadosDoPlano '<<lcCodigo>>'
			ENDTEXT
			uf_gerais_actgrelha("", "uCrsCodPla", lcsql)
			If  .Not. Empty(ucrscodpla.obriga_nrreceita)
				Select ft2
				lcnb = ''
				**lcnb = INPUTBOX("Qual o n�mero de receita?", "O n�mero de receita tem de ter 13 ou 19 caracteres", "")
				uf_tecladoalpha_chama("lcnb", "Introduza o Nr. da Receita:", .F., .F., 0)
				If Empty(lcnb) .Or. Len(Alltrim(lcnb))<>19
					If ucrscodpla.obriga_nrreceita==2 Or  ucrscodpla.obriga_nrreceita==4
						uf_perguntalt_chama("Tem que introduzir um n�mero de receita v�lido(19 caracteres).", "OK", "", 48)
						Return .F.
					Endif
				Else
					Select ft2
					Replace ft2.u_receita With lcnb
					lcnb = ""
					If  .Not. llatend
						facturacao.containercab.receita.Refresh
					Endif

					*!*	            IF uf_gerais_validaSeVendaMultiplaMCDTeAlerta()
					*!*	                RETURN .F.
					*!*	            ENDIF

				Endif
			Endif
		Endif
		If Empty(ft2.u_entpresc)
			Local lcobrigalocalpresc
			If uf_gerais_getparameter("ADM0000000001", "BOOL")
				lcobrigalocalpresc = .T.
			Endif
			If lcobrigalocalpresc
				Select ft2
				lcnb = ''
				lcnb = Inputbox("Qual o c�digo do local de prescri��o?", "Falta C�digo do Local de Prescri��o.", "")
				If Empty(lcnb)
					uf_perguntalt_chama("Tem que introduzir o c�digo do local de prescri��o.", "OK", "", 48)
					Return .F.
				Else
					Select ft2
					Replace ft2.u_entpresc With lcnb
					lcnb = ""
				Endif
			Endif
		Endif
		If Empty(ft2.u_nopresc)
			Local lcobrigacodpresc
			If uf_gerais_getparameter("ADM0000000002", "BOOL")
				lcobrigacodpresc = .T.
			Endif
			If lcobrigacodpresc
				Select ft2
				lcnb = ''
				lcnb = Inputbox("Qual o c�digo do prescritor?", "Falta C�digo do Prescritor.", "")
				If Empty(lcnb)
					uf_perguntalt_chama("Tem que introduzir o c�digo do prescritor.", "OK", "", 48)
					Return .F.
				Else
					Select ft2
					Replace ft2.u_nopresc With lcnb
					lnnb = ""
				Endif
			Endif
		Endif
	Endif
	If Used("uCrsCodPla")
		fecha('uCrsCodPla')
	Endif
	Local llretval, lnpsi, lntot, lnben
	llretval = .T.
	lnpsi = 0
	lntot = 0
	lnben = 0
	lncntlin = 0

	Select fi
	Goto Top
	Scan
		If fi.u_psico
			If (fi.qtt>4)
				uf_perguntalt_chama("N�O PODE TER UMA QUANTIDADE NA LINHA SUPERIOR A 4 PARA O PRODUTO: "+Alltrim(fi.Design), "OK", "", 64)
				Return .F.
			Endif
			lncntlin = lncntlin+1
			lnpsi = lnpsi+1
		Endif
		If fi.u_benzo
			lnben = lnben+1
		Endif
		If  .Not. fi.epromo
			lntot = lntot+1
		Endif
	Endscan

	lncntlin = 0
	llretval =  .Not. (lnpsi>0 .And. lnpsi<>lntot)
	Select ft2
	If Empty(ft2.token)
		If  .Not. llretval
			uf_perguntalt_chama("N�O PODEM EXISTIR PSICOTR�PICOS E N�O PSICOTR�PICOS NA MESMA VENDA.", "OK", "", 64)
			Return .F.
		Endif
	Endif

	If lnpsi>0 .Or. lnben>0
		Select td
		If td.lancasl
			uf_pagamento_contadorpsicobenzo()
		Endif
	Endif

	If td.lancasl
		If lnpsi>0
			Select dadospsico
			Goto Top
			Select ft2
			**IF  .NOT. EMPTY(ALLTRIM(ft2.u_codigo))
			**   IF uf_gerais_getdate(dadospsico.u_recdata, "SQL")=="19000101"
			**      uf_perguntalt_chama("O CAMPO: DATA DA RECEITA, NOS DADOS PSICOTR�PICOS � DE PREENCHIMENTO OBRIGAT�RIO PARA VENDAS COM RECEITA.", "OK", "", 64)
			**      facturacao.lbl5.click()
			**      RETURN .F.
			**   ENDIF
			**ENDIF
			If Empty(ft2.u_nbenef2) And (!Empty(ft.u_lote2) Or !Empty(ft.u_nslote2))
				uf_perguntalt_chama("O CAMPO N� BENEFICI�RIO � DE PREENCHIMENTO OBRIGAT�RIO PARA VENDAS COM RECEITA.", "OK", "", 64)
				facturacao.lbl2.Click()
				Return .F.
			Endif
			If Empty(Alltrim(dadospsico.u_cputavi)) .Or. Empty(Alltrim(dadospsico.u_cputdisp)) .Or. uf_gerais_getdate(dadospsico.u_ddutavi, "SQL")=="19000101" .Or. (dadospsico.u_idutavi>130 .Or. dadospsico.u_idutavi<1) .Or. Empty(Alltrim(dadospsico.u_medico)) .Or. Empty(Alltrim(dadospsico.u_moutavi)) .Or. Empty(Alltrim(dadospsico.u_moutdisp)) .Or. Empty(Alltrim(dadospsico.u_ndutavi)) .Or. Empty(Alltrim(dadospsico.u_nmutavi)) .Or. Empty(Alltrim(dadospsico.u_nmutdisp))
				uf_perguntalt_chama("N�O PREENCHEU TODOS OS CAMPOS RELATIVOS AOS DADOS DE PSICOTR�PICOS.", "OK", "", 64)
				facturacao.lbl5.Click()
				Return .F.
			Endif
		Endif
	Endif
	If  .Not. Used("uCrsAtendCLPato")
		Select ft
		lcsql = ''
		TEXT TO lcsql TEXTMERGE NOSHOW
			declare @clstamp char(25)
			select @clstamp=utstamp from b_utentes where no=<<ft.no>> and estab=<<ft.estab>>
			exec up_clientes_patologias @clstamp
		ENDTEXT
		uf_gerais_actgrelha("", "uCrsAtendCLPato", lcsql)
	Endif
	Local lcvalrefexiste
	Store .F. To lcvalrefexiste
	Select ucrsatendst
	Goto Top
	Scan
		lcvalrefexiste = .F.
		Select fi
		Goto Top
		Scan
			If Alltrim(ucrsatendst.ststamp)==Alltrim(fi.fistamp)
				lcvalrefexiste = .T.
			Endif
		Endscan
		If  .Not. lcvalrefexiste
			Select ucrsatendst
			Delete
		Endif
	Endscan
	Select ucrsatendst
	Goto Top
	Select fi
	Goto Top
	Locate For  .Not. Empty(fi.dem)
	If Found()
		Select ft2
		If Empty(ft2.u_codigo)
			uf_perguntalt_chama("O preenchimento do Plano � obrigat�rio para linhas de Dispensa Eletr�nica de Medicamentos.", "OK", "", 32)
			Return .F.
		Endif
	Endif

	If  .Not. llatend
		If  .Not. Empty(ft2.token)
			uf_atendimento_validacaoreceitasdem(.T., ft2.u_receita, ft2.codacesso, ft2.coddiropcao, ft2.token)
		Endif
		If Type("ATENDIMENTO")=="U" And !myftalteracao
			If Type("TokenCompart") = "U"
				Public TokenCompart
			Endif

			TokenCompart = ""
			If !uf_faturacao_compart_valida()
				Return .F.
			Endif

			Local lcPlano, lcFtNdoc
			lcPlano = ""
			lcFtNdoc = 0

			If(Used("ft2"))
				Select ft2
				lcPlano = ft2.u_codigo
			Endif

			If(Used("ft"))
				Select ft
				Go Top
				lcFtNdoc = ft.ndoc
			Endif


			&&manter os valores da receita original na reinsercao
			** TODO receitas com BAS

			If(uf_facturacao_validaSeDeveRecalcularValores(lccorrigecompartexterna,lcPlano, lcFtNdoc ))
				uf_atendimento_actvaloresfi()
				uf_atendimento_acttotaisft()
			Endif

			**deve recalcular totais nas eletronicas

			If uf_gerais_getUmValor("cptpla", "e_compart", "codigo = '" + Alltrim(lcPlano) + "'")
				uf_atendimento_acttotaisft()
			Endif



			Select * From fi With (Buffering = .T.) Into Cursor uc_tmpFI

			Select uc_tmpFI
			Go Top
			Scan
				If uc_tmpFI.qtt>0 And uc_tmpFI.etiliquido<0 And uc_tmpFI.u_descval>0
					uf_perguntalt_chama("N�O PODE EFETUAR DESCONTOS SUPERIORES AO TOTAL DA LINHA! POR FAVOR RETIFIQUE.", "OK", "", 48)
					Return .F.
				Endif
			Endscan

		Endif
		If  .Not. uf_infovenda_valida()
			Return .F.
		Endif
	Endif
	If Type("ATENDIMENTO") == "U" And !myftalteracao

		If !Empty(TokenCompart) And !uf_pagamento_validaSeEfectivado(TokenCompart)
			lcNewTokenCompart = uf_gerais_stamp()
			If !uf_compart_efetiva(TokenCompart, lcNewTokenCompart)
				Return .F.
			Else
				Select ft2
				Replace ft2.token_efectivacao_compl With Alltrim(lcNewTokenCompart)
			Endif
		Endif

	Endif
	Select fi
	Goto Top
	Locate For  .Not. Empty(fi.dem)
	If Found()
		Local lcvalidaefetivacao
		Store .F. To lcvalidaefetivacao
		If (Used("ucrsDemEfetivada"))
			Select ucrsdemefetivada
			Goto Top
			Locate For Alltrim(ucrsdemefetivada.token)==Alltrim(fi.token)
			If Found()
				lcvalidaefetivacao = .T.
			Else
				lcvalidaefetivacao = .F.
			Endif
		Else
			lcvalidaefetivacao = .F.
		Endif

		If (lcvalidaefetivacao==.F.)
			If Empty(uf_atendimento_receitasefetivar(.T., ft2.u_receita, ft2.codacesso, ft2.coddiropcao, ft2.token))
				Return .F.
			Endif
		Endif
	Endif

	Return .T.
Endfunc


Procedure uf_facturacaoChamaValidaCartao
	Lparameters lcnb
	Local lcNrAtend
	Select ft
	lcNrAtend = Alltrim(ft.u_nratend)
	If(Empty(lcNrAtend) And Vartype(nratendimento)!="U")
		lcNrAtend = Alltrim(nratendimento)
	Endif

	If !Used("ucrscodpla")
		If !Used("ft2")
			Return .F.
		Endif
		Select ft2
		Go Top
		uf_faturacao_criaCursorDadosPlano(Alltrim(ft2.u_codigo))
	Endif

	Select ucrscodpla
	uf_gerais_validaCartaoEntidadeCompart(Alltrim(ucrscodpla.codigo),ucrscodpla.entidade,Alltrim(lcnb),ucrscodpla.e_compart, Alltrim(ucrscodpla.Design),ucrscodpla.valida_card, Alltrim(lcNrAtend))

Endfunc



Function uf_facturacao_validaSeDeveRecalcularValores
	Lparameters lccorrigecompartexterna, lcPlano, lcFtNoc

	&& n�o recalcula valores numa altera��o
	If !Empty(myftalteracao)
		Return .F.
	Endif



	&&sen�o for documento certificado pela AT n�o necessita de recalcular
	If !Empty(lcFtNoc) And Empty(uf_gerais_getUmValor("td", "tiposaft", "ndoc = '" + Alltrim(Str(lcFtNoc)) + "'"))
		Return .F.
	Endif


	If(!Empty(lccorrigecompartexterna) And Alltrim(lcPlano)=='RS')
		Return .F.
	Endif

	If (Type("myinserereceitacorrigida") != "U" And !Empty(myinserereceitacorrigida))
		Return .F.
	Endif


	Return .T.

Endfunc





**
Procedure uf_facturacao_importardadospsicocl
	Select dadospsico
	Goto Top
	Replace dadospsico.u_cputavi 		With Alltrim(ft.codpost)
	Replace dadospsico.u_cputdisp 		With ft.codpost
	Replace dadospsico.u_ddutavi 		With ft.bidata
	Replace dadospsico.u_moutavi 		With Alltrim(ft.morada)
	Replace dadospsico.u_moutdisp 		With Alltrim(ft.morada)
	Replace dadospsico.u_ndutavi 		With ft.bino
	Replace dadospsico.u_nmutavi 		With Alltrim(ft.Nome)
	Replace dadospsico.u_nmutdisp 		With Alltrim(ft.Nome)
	Replace dadospsico.codigodocspms 	With uf_gerais_getUmValor("b_utentes", "codigoDocSPMS", "no = "+ASTR(ft.NO)+" AND estab = "+ASTR(ft.estab))
	Replace dadospsico.nascimento 		With ft.nascimento
	Replace dadospsico.u_idutavi 		With uf_gerais_getIdade(ft.nascimento)
	If Used("uCrsVerificaRespostaDem")
		Select ucrsverificarespostadem
		Replace dadospsico.u_recdata With ucrsverificarespostadem.receita_data
		Replace dadospsico.u_medico With Alltrim(ucrsverificarespostadem.medico_codigo)
	Endif
	facturacao.Refresh
Endproc
**
Function uf_facturacao_actualizaft
	Local lcsql, lcexecutesql
	Store "" To lcexecutesql, lcsql
	Select ft
	Set Hours To 24
	atime = Ttoc(Datetime()+(difhoraria*3600), 2)
	ASTR = Left(atime, 8)
	myhora = ASTR
	myinvoicedata = uf_gerais_getdate(ft.fdata, "SQL")
	TEXT TO lcsql TEXTMERGE NOSHOW
		Update	Ft
		Set		pais = <<Ft.pais>>,nmdoc = '<<Alltrim(Ft.NmDoc)>>', fno =<<Ft.fno>>, [no] = <<Ft.no>>,
				nome = '<<Alltrim(Ft.nome)>>' , morada = '<<Alltrim(ft.morada)>>',
				[local] = '<<Alltrim(ft.local)>>', codpost = '<<Alltrim(ft.codpost)>>', ncont = '<<Alltrim(ft.ncont)>>',
				bino = '<<Alltrim(ft.bino)>>', bidata = '<<IIF(empty(ft.bidata),'19000101',uf_gerais_getDate(ft.bidata,"SQL"))>>',
				bilocal = '<<Alltrim(ft.bilocal)>>', telefone = '<<Alltrim(ft.telefone)>>',
				zona = '<<Alltrim(ft.zona)>>',
            --vendedor =<<ch_vendedor>>, vendnm = '<<Alltrim(ch_vendnm)>>',
            fdata = '<<myInvoiceData>>',
				ftano = year('<<myInvoiceData>>'),
				pdata = '<<Iif(td.u_tipodoc=9,uf_gerais_getDate(Ft.pdata,"SQL"),uf_gerais_getDate(datetime(),"SQL"))>>',
				carga = '<<ft.carga>>', descar = '<<ft.descar>>', saida = Left('<<Ft.saida>>',5),
				ivatx1 = <<ft.ivatx1>>, ivatx2 = <<ft.ivatx2>>, ivatx3 = <<ft.ivatx3>>,
				fin = <<ft.fin>>, ndoc = <<Ft.ndoc>>,
				moeda = '<<ft.moeda>>',fref = '<<ft.fref>>',
				ccusto = '<<ft.ccusto>>', ncusto = '<<ft.ncusto>>',
				facturada = <<IIF(ft.facturada,1,0)>>, fnoft = <<ft.fnoft>>,
				nmdocft = '<<ft.nmdocft>>',estab = <<Ft.estab>>, cdata = '<<uf_gerais_getDate(ft.cdata,"SQL")>>',
				ivatx4 = <<ft.ivatx4>>,
				segmento = '<<ft.segmento>>',totqtt = <<Ft.TotQtt>>, qtt1 = <<Ft.Qtt1>>, qtt2 = <<ft.qtt2>>,
				tipo = '<<ft.tipo>>',
				cobrado = <<IIF(Ft.cobrado,1,0)>>,cobranca = '<<ft.cobranca>>',
				tipodoc = <<Ft.TipoDoc>>, chora = Left('<<Ft.chora>>',4),
				ivatx5 =  <<ft.ivatx5>>, ivatx6 = <<ft.ivatx6>>, ivatx7 = <<ft.ivatx7>>,
				ivatx8 = <<ft.ivatx8>>, ivatx9 = <<ft.ivatx9>>,
				ivatx10 = <<ft.ivatx10>>, ivatx11 = <<ft.ivatx11>>,
				ivatx12 = <<ft.ivatx12>>, ivatx13 = <<ft.ivatx13>>,
				cambiofixo = <<IIF(ft.cambiofixo,1,0)>>, memissao = '<<ft.memissao>>',
				cobrador = '<<ft.cobrador>>', rota = '<<ft.rota>>', multi = <<IIF(ft.multi,1,0)>>,
				cheque = <<IIF(ft.cheque,1,0)>>,clbanco = '<<ft.clbanco>>', clcheque = '<<ft.clcheque>>',
				chtotal = <<IIF(td.u_tipodoc!=9,ft.chtotal,0)>>,echtotal = <<IIF(td.u_tipodoc!=9,ft.echtotal,0)>>,
				custo = <<Ft.ecusto*200.482>>,
				eivain1 = <<Ft.EivaIn1>>, eivain2 = <<Ft.EivaIn2>>,
				eivain3 = <<Ft.EivaIn3>>, eivav1 = <<Ft.EivaV1>>, eivav2 = <<Ft.EivaV2>>, eivav3 = <<Ft.EivaV3>>,
				ettiliq  =<<Ft.EttIliq>>, edescc = <<Ft.edescc>>, ettiva = <<Ft.EttIva>>, etotal = <<Ft.Etotal>>,
				eivain4 = <<Ft.EivaIn4>>, eivav4 = <<Ft.EivaV4>>, efinv = <<ft.efinv>>,ecusto = <<Ft.ecusto>>,
				eivain5 = <<Ft.EivaIn5>>, eivav5 = <<Ft.EivaV5>>, edebreg = <<ft.edebreg>>,
				eivain6 = <<ft.eivain6>>, eivav6 = <<ft.eivav6>>,eivain7 = <<ft.eivain7>>,eivav7 = <<ft.eivav7>>,
				eivain8 = <<ft.eivain8>>,eivav8 = <<ft.eivav8>>, eivain9 = <<ft.eivain9>>,eivav9 = <<ft.eivav9>>,
				eivain10 = <<ft.eivain10>>,eivav10 = <<ft.eivav10>>, eivain11 = <<ft.eivain11>>,eivav11 = <<ft.eivav11>>,
				eivain12 = <<ft.eivain12>>,eivav12 = <<ft.eivav12>>, eivain13 = <<ft.eivain13>>,eivav13 = <<ft.eivav13>>,
				total = <<Ft.Etotal*200.482>>,totalmoeda = <<ft.totalmoeda>>,ivain1 = <<Ft.EivaIn1*200.482>>,
				ivain2 = <<Ft.EivaIn2*200.482>>, ivain3 = <<Ft.EivaIn3*200.482>>,
				ivain4 = <<Ft.EivaIn4*200.482>>, ivain5 = <<Ft.EivaIn5*200.482>>,
				ivain6 = <<ft.ivain6>>,
				ivain7 = <<ft.ivain7>>,ivain8 = <<ft.ivain8>>,
				ivain9 = <<ft.ivain9>>,ivav1 = <<Ft.EivaV1*200.482>>,
				ivav2 = <<Ft.EivaV2*200.482>>,ivav3 = <<Ft.EivaV3*200.482>>,
				ivav4 = <<Ft.EivaV4*200.482>>,ivav5 = <<Ft.EivaV5*200.482>>,ivav6 = <<ft.ivav6>>,
				ivav7 = <<ft.ivav7>>,ivav8 =  <<ft.ivav8>>,
				ivav9 = <<ft.ivav9>>, ttiliq = <<Ft.EttIliq*200.482>>,
				ttiva = <<Ft.EttIva*200.482>>,descc = <<ft.descc>>, debreg = <<ft.debreg>>,
				debregm = <<ft.debregm>>, intid = '<<ft.intid>>', nome2 = '<<ft.nome2>>',
				tpstamp = '<<ft.tpstamp>>', tpdesc = '<<ft.tpdesc>>', erdtotal = <<ft.erdtotal>>, rdtotal = <<ft.rdtotal>>,
				rdtotalm = <<ft.rdtotalm>>,
				cambio = <<IIF(td.u_tipodoc=9,1,0)>>, [site] = '<<ft.site>>', pnome = '<<ft.pnome>>',
				pno = <<ft.pno>>, cxstamp = '<<ft.cxstamp>>', cxusername = '<<ft.cxusername>>', ssstamp = '<<ft.ssstamp>>', ssusername = '<<ft.ssusername>>',
				anulado = <<IIF(ft.anulado,1,0)>>,
				virs = <<ft.virs>>, evirs = <<ft.evirs>>, valorm2 = <<ft.valorm2>>,
				--u_lote = '<<ft.u_lote>>', u_tlote = '<<ft.u_tlote>>', u_tlote2 = '<<ft.u_tlote2>>', u_ltstamp = '<<ft.u_ltstamp>>', u_slote = <<ft.u_slote>>,
				--u_slote2 = <<ft.u_slote2>>, u_nslote = <<ft.u_nslote>>, u_nslote2 = <<ft.u_nslote2>>, u_ltstamp2 = '<<ft.u_ltstamp2>>',	u_lote2 = <<ft.u_lote2>>,
				usrinis = '<<m_chinis>>', usrdata = CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), usrhora = '<<myHora>>', u_nratend = '<<ft.u_nratend>>'
				,ft.datatransporte = '<<uf_gerais_getDate(ft.datatransporte,"SQL")>>'
				,ft.localcarga = '<<ALLTRIM(ft.localcarga)>>'
				,ft.id_tesouraria_conta = <<ft.id_tesouraria_conta>>
			Where
				Ftstamp = '<<Alltrim(Ft.Ftstamp)>>'
	ENDTEXT
	lcexecutesql = lcexecutesql+Chr(13)+Chr(13)+lcsql
	Return lcexecutesql
Endfunc
**
Function uf_facturacao_actualizaft2
	Local lcsql, lcexecutesql
	Store "" To lcexecutesql, lcsql
	TEXT TO lcsql TEXTMERGE NOSHOW
		Update	Ft2
		Set
				contacto = '<<ft2.contacto>>'
				,u_viatura  = '<<ALLTRIM(ft2.u_viatura)>>'
				,morada = '<<ft2.morada>>'
				,codpost = '<<ft2.codpost>>'
				,local = '<<ft2.local>>'
				,telefone = '<<ft2.telefone>>'
				,email = '<<ft2.email>>'
				,horaentrega = '<<ft2.horaentrega>>'
				,u_vddom = <<IIF(ft2.u_vddom,1,0)>>
				,obsdoc = '<<ALLTRIM(ft2.obsdoc)>>'
				,obsInt = '<<ALLTRIM(ft2.obsInt)>>'
				,obsCl = '<<ALLTRIM(ft2.obsCl)>>'
				,u_receita = '<<Alltrim(FT2.u_receita)>>'
				,u_entpresc = '<<Alltrim(ft2.u_entpresc)>>'
				,u_nopresc = '<<Alltrim(ft2.u_nopresc)>>'
				,u_codigo = '<<Alltrim(Ft2.u_codigo)>>'
				,u_codigo2 = '<<Alltrim(Ft2.u_codigo2)>>'
				,u_nbenef = '<<Alltrim(ft2.u_nbenef)>>'
				,u_nbenef2 = '<<Alltrim(ft2.u_nbenef2)>>'
				,u_abrev = '<<Alltrim(Ft2.u_abrev)>>'
				,u_abrev2 = '<<Alltrim(Ft2.u_abrev2)>>'
				,c2no = <<ft2.c2no>>
				,c2estab = <<ft2.c2estab>>
				,subproc = '<<ft2.subproc>>'
				,u_vdsusprg = <<IIF(ft2.u_vdsusprg,1,0)>>
				,u_design = '<<Alltrim(Ft2.u_design)>>'
				,u_design2 = '<<Alltrim(Ft2.u_design2)>>'
				,c2nome = '<<ft2.c2nome>>'
				,c2pais = <<ft2.c2pais>>
				,evdinheiro = <<Iif(td.u_tipodoc!=9,ft2.evdinheiro,0)>>
				,epaga1 = <<Iif(td.u_tipodoc!=9,ft2.epaga1,0)>>
				,epaga2 = <<Iif(td.u_tipodoc!=9,ft2.epaga2,0)>>
				,u_docOrig = '<<ALLTRIM(ft2.u_docorig)>>'
				,etroco = <<IIF(td.u_tipodoc!=9,ft2.etroco,0)>>
				,motiseimp = '<<ft2.motiseimp>>'
				,u_acertovs = <<ft2.u_acertovs>>
				,c2codpost = '<<ft2.c2codpost>>'
				,eacerto = <<ft2.eacerto>>
				,vdollocal = 'Caixa'
				,vdlocal = 'C'
				,REEXGIVA = <<IIF(ft2.REEXGIVA,1,0)>>
				,formapag = '<<ft2.formapag>>'
				,modop1 = '<<ft2.modop1>>'
				,modop2 = '<<ft2.modop2>>'
				,usrinis =  '<<m_chinis>>'
				,usrdata =  CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
				,usrhora = '<<myHora>>'
            ,xpdCodpost = '<<ALLTRIM(ft2.xpdCodPost)>>'
            ,xpdLocalidade = '<<ALLTRIM(ft2.xpdLocalidade)>>'
		Where	Ft2.Ft2Stamp = '<<Alltrim(Ft.Ftstamp)>>'
	ENDTEXT
	lcexecutesql = lcexecutesql+Chr(13)+Chr(13)+lcsql
	Return lcexecutesql
Endfunc
**
Function uf_facturacao_actualizafi
	Local lcsql, lcexecutesql
	Store "" To lcexecutesql, lcsql
	lcsql = ""
	TEXT TO lcsql TEXTMERGE NOSHOW
		SELECT	FISTAMP
		FROM	FI (nolock)
		WHERE	FTSTAMP = '<<ALLTRIM(Ft.Ftstamp)>>'
	ENDTEXT
	If  .Not. uf_gerais_actgrelha("", "uc_StampsAnterioresFi", lcsql)
		uf_perguntalt_chama("OCORREU UM ERRO A ACTUALIZAR AS LINHAS DO DOCUMENTO.", "OK", "", 16)
		Return
	Endif
	Select fistamp From uc_StampsAnterioresFi Where fistamp Not In(Select fistamp From fi) Into Cursor lcCursorDeleteFI
	Select lcCursorDeleteFI
	Goto Top
	Scan
		lcsql = ""
		TEXT TO lcsql TEXTMERGE NOSHOW
			DELETE
			FROM	FI
			WHERE	FISTAMP = '<<ALLTRIM(lcCursorDeleteFI.fistamp)>>'

			DELETE FROM fi_comp WHERE id_fi = '<<ALLTRIM(lcCursorDeleteFI.fistamp)>>'

		ENDTEXT
		lcexecutesql = lcexecutesql+Chr(13)+Chr(13)+lcsql
	Endscan
	Select fi
	Goto Top
	Scan
		lcsql = ""
		TEXT TO lcsql TEXTMERGE NOSHOW
			IF exists(Select FISTAMP From FI (nolock) Where Rtrim(Ltrim(FISTAMP)) = '<<FI.FISTAMP>>')
			BEGIN
				Update Fi
				Set	nmdoc =  '<<Alltrim(Fi.nmdoc)>>',fno =  <<Fi.Fno>>,ref =  '<<Alltrim(fi.ref)>>',design = '<<alltrim(fi.design)>>'
					,qtt =  <<fi.qtt>>,tiliquido = <<fi.tiliquido>>,etiliquido = <<fi.etiliquido>>,unidade = '<<fi.unidade>>'
					,unidad2 = '<<fi.unidad2>>',iva = <<fi.iva>>,ivaincl = <<IIF(fi.ivaincl,1,0)>>,codigo = '<<Alltrim(fi.codigo)>>'
					,tabiva = <<fi.tabiva>>,ndoc = <<Fi.ndoc>>,armazem = <<fi.armazem>>,fnoft = <<fi.fnoft>>,ndocft = <<fi.ndocft>>
					,ftanoft = <<fi.ftanoft>>,ftregstamp = '<<Alltrim(fi.ftregstamp)>>',lobs2 = '<<Alltrim(fi.lobs2)>>'
					,litem2 = '<<Alltrim(fi.litem2)>>',litem = '<<Alltrim(fi.litem)>>',lobs3 = '<<Alltrim(fi.lobs3)>>',rdata = '<<myInvoiceData>>'
					,cpoc = <<fi.cpoc>>,composto = <<IIF(fi.composto,1,0)>>,lrecno = '<<fi.lrecno>>',lordem = <<fi.lordem>>,fmarcada = <<IIF(fi.fmarcada,1,0)>>
					,partes = <<fi.partes>>,altura = <<fi.altura>>,largura = <<fi.largura>>,oref = '<<Alltrim(fi.oref)>>',lote = '<<Alltrim(fi.lote)>>'
					,usr1 = '<<Alltrim(fi.usr1)>>',usr2 = '<<Alltrim(fi.usr2)>>',usr3 = '<<Alltrim(fi.usr3)>>',usr4 = '<<Alltrim(fi.usr4)>>'
					,usr5 = '<<Alltrim(fi.usr5)>>',ftstamp = '<<Alltrim(Ft.Ftstamp)>>', cor = '<<Alltrim(fi.cor)>>',tam = '<<Alltrim(fi.tam)>>', stipo = 1, fifref = '<<Alltrim(fi.fifref)>>'
					,tipodoc = <<Fi.TipoDoc>>, familia = '<<Alltrim(fi.familia)>>', pbruto = <<fi.pbruto>>, bistamp = '<<Alltrim(fi.bistamp)>>',stns = <<IIF(fi.stns,1,0)>>, ficcusto = '<<fi.ficcusto>>'
					,fincusto = '<<Alltrim(fi.fincusto)>>', ofistamp = '<<Alltrim(fi.ofistamp)>>',pv = <<fi.pv>>, pvmoeda = <<fi.pvmoeda>>, epv = <<fi.epv>>, tmoeda = <<fi.tmoeda>>
					,eaquisicao = <<fi.eaquisicao>>, custo = <<fi.custo>>, ecusto = <<fi.ecusto>>, slvu = <<round(fi.pv/(fi.iva/100+1),2)>>,
					eslvu = <<Round(fi.epv/(fi.iva/100+1),2)>>, sltt = ABS(<<fi.tiliquido/(fi.iva/100+1)>>),  esltt = ABS(<<fi.etiliquido/(fi.iva/100+1)>>), slvumoeda = <<fi.slvumoeda>>, 	slttmoeda = <<fi.slttmoeda>>,
					nccod = '<<Alltrim(fi.nccod)>>', epromo = <<IIF(fi.epromo,1,0)>>, fivendedor = <<ch_vendedor>>,  fivendnm = '<<alltrim(ch_vendnm)>>',
					desconto = <<fi.desconto>>, desc2 = <<fi.desc2>>,  desc3 = <<fi.desc3>>, desc4 = <<fi.desc4>>, desc5 = <<fi.desc5>>, desc6 = <<fi.desc6>>,
					iectin = <<fi.iectin>>, eiectin = <<fi.eiectin>>, vbase = <<fi.vbase>>, evbase = <<fi.evbase>>,	tliquido = <<fi.tliquido>>, num1 = <<fi.num1>>
					,pcp = <<fi.pcp>>, epcp = <<fi.epcp>>, rvpstamp = '<<Alltrim(fi.rvpstamp)>>', pvori = <<fi.pv>>, 	epvori = <<fi.epv>>, szzstamp = '<<Alltrim(fi.szzstamp)>>'
					,zona = '<<Alltrim(fi.zona)>>', morada = '<<Alltrim(fi.morada)>>', [local] = '<<Alltrim(fi.local)>>', codpost = '<<Alltrim(fi.codpost)>>',
					telefone = '<<Alltrim(fi.telefone)>>', email = '<<Alltrim(fi.email)>>', tkhposlstamp = '<<Alltrim(fi.tkhposlstamp)>>',u_generico = <<IIF(fi.u_generico,1,0)>>
					,u_cnp = '<<Alltrim(fi.u_cnp)>>', u_psicont = <<fi.u_psicont>>, u_bencont = <<fi.u_bencont>>, u_psico = <<IIF(fi.u_psico,1,0)>>
					,u_benzo = <<IIF(fi.u_benzo,1,0)>>, u_ettent1 = <<fi.u_ettent1>>, u_ettent2 = <<fi.u_ettent2>>
					,u_epref = <<fi.u_epref>>,pic = <<fi.pic>>,pvpmaxre = <<fi.pvpmaxre>>,  u_stock = <<fi.u_stock>>, u_epvp = <<fi.u_epvp>>, u_ip = <<IIF(fi.u_ip,1,0)>>,
					u_comp = <<IIF(fi.u_comp,1,0)>>, 	u_diploma = '<<Alltrim(fi.u_diploma)>>', usrinis = '<<m_chinis>>', usrdata = CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102),
					usrhora = '<<myHora>>', marcada =  <<IIF(fi.marcada,1,0)>>,  u_genalt =  <<IIF(fi.u_genalt,1,0)>>,  u_txcomp =  <<fi.u_txcomp>>, amostra = <<IIF(fi.amostra, 1, 0)>>,
					u_descval = <<fi.u_descval>>,u_codemb = '<<fi.u_codemb>>'
				Where
					Fistamp = '<<Alltrim(Fi.Fistamp)>>'

				update fi_comp SET comp_tipo = <<fi.comp_tipo>>,comp = <<fi.comp>>,comp_diploma = <<fi.comp_diploma>>,comp_tipo_2 = <<fi.comp_tipo_2>>,comp_2 = <<fi.comp_2>>,comp_diploma_2 = <<fi.comp_diploma_2>> where id_fi = '<<Alltrim(Fi.Fistamp)>>'

			END
			ELSE
			BEGIN
				insert into fi (
						fistamp, nmdoc, fno, ref, design, qtt, tiliquido, etiliquido, unidade, unidad2, iva, ivaincl, codigo, tabiva, ndoc, armazem, fnoft, ndocft, ftanoft, ftregstamp
						,lobs2,litem2,litem,lobs3,rdata,cpoc,composto,lrecno,lordem,fmarcada,partes,altura,largura,oref,lote,usr1,usr2,usr3,usr4,usr5,ftstamp,cor,tam,stipo,fifref
						,tipodoc,familia,pbruto,bistamp,stns,ficcusto,fincusto,ofistamp,pv,pvmoeda,epv,tmoeda,eaquisicao,custo,ecusto,slvu,eslvu,sltt,esltt,slvumoeda,slttmoeda,nccod,epromo,
						fivendedor,fivendnm,desconto,desc2,desc3,desc4,desc5,desc6,iectin,eiectin,vbase,evbase,	tliquido,num1,pcp,epcp,rvpstamp,pvori,epvori,szzstamp,zona,morada,[local],
						codpost,telefone,email,tkhposlstamp, u_generico, u_cnp, u_psicont, u_bencont, u_psico, u_benzo, u_ettent1,u_ettent2,u_epref,pic,pvpmaxre, u_stock, u_epvp, u_ip,
						u_comp, u_diploma, ousrinis, ousrdata,ousrhora, usrinis, usrdata, usrhora,marcada, u_genalt, u_txcomp, amostra, u_descval,EVALDESC,VALDESC,u_codemb
					)Values (
						'<<Fi.Fistamp>>','<<Fi.nmdoc>>', <<Fi.Fno>>, '<<fi.ref>>', '<<alltrim(fi.design)>>', <<fi.qtt>>, <<fi.tiliquido>>,
						<<fi.etiliquido>>, '<<fi.unidade>>', '<<fi.unidad2>>', <<fi.iva>>, <<IIF(fi.ivaincl,1,0)>>,	'<<fi.codigo>>', <<fi.tabiva>>, <<Fi.ndoc>>, <<fi.armazem>>, <<fi.fnoft>>, <<fi.ndocft>>,
						<<fi.ftanoft>>, '<<fi.ftregstamp>>','<<fi.lobs2>>', '<<fi.litem2>>','<<fi.litem>>', '<<fi.lobs3>>', CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102),
						<<fi.cpoc>>, <<IIF(fi.composto,1,0)>>,'<<fi.lrecno>>',<<fi.lordem>>	,<<IIF(fi.fmarcada,1,0)>>, <<fi.partes>>, <<fi.altura>>, <<fi.largura>>,
						'<<fi.oref>>', '<<fi.lote>>', '<<fi.usr1>>', '<<fi.usr2>>', '<<fi.usr3>>','<<fi.usr4>>', '<<fi.usr5>>','<<Ft.Ftstamp>>','<<fi.cor>>','<<fi.tam>>',1
						,'<<fi.fifref>>',<<Fi.TipoDoc>>	,'<<fi.familia>>',<<fi.pbruto>>,'<<fi.bistamp>>',<<IIF(fi.stns,1,0)>>, '<<fi.ficcusto>>', '<<fi.fincusto>>',
						'<<fi.ofistamp>>', <<fi.pv>>, <<fi.pvmoeda>>, <<fi.epv>>,<<fi.tmoeda>>,<<fi.eaquisicao>>, <<fi.custo>>, <<fi.ecusto>>, <<round(fi.pv/(fi.iva/100+1),2)>>, <<Round(fi.epv/(fi.iva/100+1),2)>>,
						<<fi.tiliquido/(fi.iva/100+1)>>, <<fi.etiliquido/(fi.iva/100+1)>>, <<fi.slvumoeda>>, <<fi.slttmoeda>>,'<<fi.nccod>>',<<IIF(fi.epromo,1,0)>>,<<ch_vendedor>>, '<<alltrim(ch_vendnm)>>', <<fi.desconto>>,
						<<fi.desc2>>, <<fi.desc3>>, <<fi.desc4>>, <<fi.desc5>>, <<fi.desc6>>, <<fi.iectin>>,<<fi.eiectin>>,<<fi.vbase>>, <<fi.evbase>>,<<fi.tliquido>>, <<fi.num1>>,<<fi.pcp>>, <<fi.epcp>>,'<<fi.rvpstamp>>',
						<<fi.pv>>, <<fi.epv>>,'<<fi.szzstamp>>', '<<fi.zona>>', '<<fi.morada>>', '<<fi.local>>', '<<fi.codpost>>','<<fi.telefone>>', '<<fi.email>>','<<fi.tkhposlstamp>>',
						<<IIF(fi.u_generico,1,0)>>, '<<fi.u_cnp>>',<<fi.u_psicont>>, <<fi.u_bencont>>, <<IIF(fi.u_psico,1,0)>>, <<IIF(fi.u_benzo,1,0)>>, <<fi.u_ettent1>>,<<fi.u_ettent2>>,<<fi.u_epref>>
						,<<fi.pic>>,<<fi.pvpmaxre>>,<<fi.u_stock>>, <<fi.u_epvp>>, <<IIF(fi.u_ip,1,0)>>,<<IIF(fi.u_comp,1,0)>>, '<<fi.u_diploma>>', '<<m_chinis>>', CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102),
						'<<fi.ousrhora>>', '<<m_chinis>>', CONVERT(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), '<<myHora>>',0, <<IIF(fi.u_genalt,1,0)>>, <<fi.u_txcomp>>, <<IIF(fi.amostra, 1, 0)>>, <<fi.u_descval>>
						,<<IIF(fi.qtt == 0,0,(fi.u_descval / fi.qtt))>>,<<IIF(fi.qtt == 0,0,(fi.u_descval / fi.qtt) * 200.482)>>,'<<fi.u_codemb>>'
				)
				IF <<fi.comp_tipo>> != 0 OR <<fi.comp_tipo_2>> != 0
				BEGIN
					INSERT INTO fi_comp (id_fi,comp_tipo,comp,comp_diploma,comp_tipo_2,comp_2,comp_diploma_2) VALUES ('<<Fi.Fistamp>>',<<fi.comp_tipo>>,<<fi.comp>>,<<fi.comp_diploma>>,<<fi.comp_tipo_2>>,<<fi.comp_2>>,<<fi.comp_diploma_2>>)
				END
			END
		ENDTEXT
		lcexecutesql = lcexecutesql+Chr(13)+Chr(13)+lcsql
	Endscan

	Return lcexecutesql
Endfunc
**
Function uf_facturacao_actualizavendapsico
	If  .Not. Used("dadosPsico")
		Return .T.
	Endif
	Set Point To "."
	Select dadospsico
	If uf_gerais_actgrelha("", "checkExistDp", 'select ftstamp from B_dadospsico dadospsico (nolock) where dadospsico.ftstamp=?dadosPsico.ftstamp')
		If Reccount()>0
			TEXT TO lcsql TEXTMERGE NOSHOW
				UPDATE B_dadospsico
				SET u_recdata	= '<<IIF(empty(dadosPsico.u_recdata),'19000101',uf_gerais_getDate(dadosPsico.u_recdata,"SQL"))>>',
					u_medico	= '<<ALLTRIM(dadosPsico.u_medico)>>',
					u_nmutdisp	= '<<ALLTRIM(dadospsico.u_nmutdisp)>>',
					u_moutdisp	= '<<ALLTRIM(dadospsico.u_moutdisp)>>',
					u_cputdisp	= '<<ALLTRIM(dadospsico.u_cputdisp)>>',
					u_nmutavi	= '<<ALLTRIM(dadospsico.u_nmutavi)>>',
					u_moutavi	= '<<ALLTRIM(dadospsico.u_moutavi)>>',
					u_cputavi	= '<<dadosPsico.u_cputavi>>',
					u_ndutavi	= '<<ALLTRIM(dadospsico.u_ndutavi)>>',
					u_ddutavi	= '<<IIF(empty(dadosPsico.u_ddutavi),'19000101',uf_gerais_getDate(dadosPsico.u_ddutavi,"SQL"))>>',
					u_idutavi	= <<dadospsico.u_idutavi>>,
					u_dirtec	= '<<uCrsE1.u_dirtec>>',
					nascimento 	= '<<dadospsico.nascimento>>',
                    codigoDocSPMS = '<<ALLTRIM(dadospsico.codigoDocSPMS)>>'
				where ftstamp	= '<<ALLTRIM(ft.ftstamp)>>'
			ENDTEXT
			If  .Not. uf_gerais_actgrelha("", "", lcsql)
				uf_perguntalt_chama("N�O FOI POSS�VEL ACTUALIZAR OS DADOS DE PSICOTR�PICOS.", "OK", "", 48)
			Endif
		Endif
		fecha("checkExistDp")
	Else
		uf_perguntalt_chama("OCORREU UM ERRO A PROCURAR REGISTO DOS DADOS DE PSICOTR�PICOS.", "OK", "", 16)
	Endif
Endfunc
**
Procedure uf_facturacao_criacursorprocura
	Select fi
	Goto Top
	Select fistamp From fi Where (Like('*'+Alltrim(Upper(facturacao.pageframe1.page1.textpesq.Value))+'*', Alltrim(Upper(fi.ref))) Or Like('*'+Alltrim(Upper(facturacao.pageframe1.page1.textpesq.Value))+'*', Alltrim(Upper(fi.Design))) Or Like('*'+Alltrim(Upper(facturacao.pageframe1.page1.textpesq.Value))+'*', Alltrim(Upper(fi.lote)))) And  Not Empty(Alltrim(Upper(facturacao.pageframe1.page1.textpesq.Value))) Into Cursor ucrsTempPesq
	Select ucrsTempPesq
	Select fi
	Goto Top
	Scan
		If (Like('*'+Alltrim(Upper(facturacao.pageframe1.page1.textpesq.Value))+'*', Alltrim(Upper(fi.ref))) .Or. Like('*'+Alltrim(Upper(facturacao.pageframe1.page1.textpesq.Value))+'*', Alltrim(Upper(fi.Design))) .Or. Like('*'+Alltrim(Upper(facturacao.pageframe1.page1.textpesq.Value))+'*', Alltrim(Upper(fi.lote)))) .And.  .Not. Empty(Alltrim(Upper(facturacao.pageframe1.page1.textpesq.Value)))
			Replace fi.pesquisa With .T.
		Else
			Replace fi.pesquisa With .F.
		Endif
	Endscan
	uf_facturacao_procuralinhaseguinte()
Endproc
**
Function uf_facturacao_procuralinhaseguinte
	If  .Not. Used("ucrsTempPesq")
		Return .F.
	Endif
	lcfistamp = ucrsTempPesq.fistamp
	Select ucrsTempPesq
	Try
		Goto Recno()+1
	Catch
		Goto Top
	Endtry
	Select ucrsTempPesq
	Select fi
	Scan
		If fi.fistamp==ucrsTempPesq.fistamp
			Exit
		Endif
	Endscan
	facturacao.pageframe1.page1.griddoc.SetFocus
Endfunc
**
Function uf_facturacao_datavencimentocliente
	If Type("GETDATE")<>"U"
		Return .F.
	Endif
	If  .Not. Empty(ft.fdata)
		Select ft
		Goto Top
		lcsql = ''
		TEXT TO lcsql TEXTMERGE NOSHOW
			exec up_facturacao_CondPagCliente2 '<<Ft.no)>>', '<<Ft.estab)>>', '<<uf_gerais_getDate(Ft.fdata,"SQL")>>'
		ENDTEXT

		If uf_gerais_actgrelha("", "uc_DataVencimento", lcsql)

			If Reccount("uc_DataVencimento")>0
				Select ft
				Goto Top
				Replace ft.tpstamp With uc_datavencimento.tpstamp, ft.tpdesc With uc_datavencimento.condpag, ft.pdata With uc_datavencimento.datavencimento
			Else
				Select ft
				Goto Top
				Replace ft.pdata With ft.fdata, ft.tpstamp With '', ft.tpdesc With ''
			Endif
		Else
			Select ft
			Goto Top
			Replace ft.pdata With ft.fdata, ft.tpstamp With '', ft.tpdesc With ''
		Endif
	Endif
	facturacao.Refresh
Endfunc
**
Function uf_facturacao_aplicaplanocomp
	Lparameters lndemplano, uv_fromAtend
	If myftintroducao==.T. .Or. myftalteracao==.T.
		Select ft
		Replace ft.cdata With ft.bidata
		Local lcinclinativos
		lcinclinativos = .F.

		If(!Used("td"))
			Return .F.
		Endif


		Select td
		If td.u_tipodoc==3
			lcinclinativos = .T.
		Else
			lcinclinativos = .F.
		Endif
		If Empty(lndemplano)
			uf_selplanocomp_chama("ft2", "u_codigo|u_design|u_abrev|u_codigo2|u_design2|u_abrev2", "codigo|design|cptorgabrev|codigo2|design2|cptorgabrev2", lcinclinativos)
		Endif
		If ( .Not. Empty(ft2.u_codigo))


			If (uf_atendimento_validatiposnsextporplano(Alltrim(ft2.u_codigo)))
				uf_perguntalt_chama("Este plano s� pode ser aplicado no atendimento", "OK", "", 48)
				Return .T.
			Endif

			If !uv_fromAtend

				Select fi
				Go Top

				Scan For !Empty(fi.ref)

					If !uf_faturacao_checkQttMaxPla(ft2.u_codigo, fi.qtt)
						Select ft2
						Replace ft2.u_codigo With ''
						Replace ft2.u_design With ''
						Return .T.
					Endif

					Select fi
				Endscan

				Select fi
				Go Top

			Else
			Endif

			If ft.cobrado
				facturacao.containercab.chksuspensa.Click(.T.)
			Endif

		Endif
		If Empty(ft2.u_codigo)
			Select fi
			Goto Top
			Scan
				Replace fi.u_comp With .F.
				uf_atendimento_actvaloresfi()
			Endscan
			Select fi
			Goto Top
		Else
			uf_atendimento_comparticipaprod()
		Endif
		Select ft2
		uv_cod = ft2.u_codigo
		uv_compartmanual = .F.
		Select fi
		Goto Top
		Scan
			uf_atendimento_actvaloresfi()
		Endscan
		Select fi
		Goto Top
		uf_atendimento_acttotaisft()
		If td.u_tipodoc==3
			uf_facturacao_calccompdata()
		Endif
		Select fi
		Goto Top
		Scan
			Local lcobriga, lccod
			Store .F. To lcobriga
			lccod = Upper(Alltrim(ft2.u_codigo))
			If lccod=="AS" .Or. lccod=="WG" .Or. lccod=="HP" .Or. lccod=="WJ"
				lcobriga = .T.
			Endif
			If lcobriga==.T.
				If Upper(Alltrim(lccod))=="WJ"
					uf_perguntalt_chama("Este documento tem produtos que obrigam � coloca��o do c�digo de embalagem. Por favor verifique.", "OK", "", 48)
				Else
					Select fi
					Select ucrsatendst
					Locate For ucrsatendst.ststamp=fi.fistamp
					If Found()
						If ucrsatendst.coduniemb==.T. .And. Empty(fi.U_CODEMB)
							uf_perguntalt_chama("Este documento tem produtos que obrigam � coloca��o do c�digo de embalagem. Por favor verifique.", "OK", "", 48)
						Endif
					Endif
				Endif
			Endif
		Endscan
		facturacao.containercab.Refresh()
	Else
		Return .F.
	Endif
Endfunc
**
Procedure uf_facturacao_actccusto
	If Used("fi") .And. Used("ft") .And. (myftintroducao .Or. myftalteracao)
		Select ft
		Replace ficcusto With ft.ccusto All In fi
		Goto Top
		uf_perguntalt_chama("LINHAS ACTUALIZADAS COM SUCESSO.", "OK", "", 64)
	Endif
Endproc
**
Function uf_facturacao_importclvd
	If  .Not. Used("ft2")
		Return .F.
	Else
		Select ft2
		If Empty(Alltrim(ft2.ft2stamp))
			Return .F.
		Endif
	Endif
	If  .Not. Used("TD")
		Return .F.
	Else
		If td.u_tipodoc==3
			Return .F.
		Endif
	Endif
	If  .Not. myftalteracao .And.  .Not. myftintroducao
		Return .F.
	Endif
	Select ft2
	If  .Not. ft2.u_vddom
		Return .F.
	Endif
	Select ft
	Select ft2
	Replace ft2.morada With ft.morada, ft2.codpost With ft.codpost, ft2.Local With ft.Local, ft2.telefone With ft.telefone
	facturacao.Refresh
	uf_perguntalt_chama("DADOS IMPORTADOS COM SUCESSO.", "OK", "", 64)
Endfunc
**
Procedure uf_facturacao_confcamposvd
	Select ft2
	If ft2.u_vddom
		facturacao.pageframe1.page3.txtcontacto.ReadOnly = .F.
		facturacao.pageframe1.page3.txtmorada.ReadOnly = .F.
		facturacao.pageframe1.page3.txtcodpost.ReadOnly = .F.
		facturacao.pageframe1.page3.txtlocal.ReadOnly = .F.
		facturacao.pageframe1.page3.txttelefone.ReadOnly = .F.
		facturacao.pageframe1.page3.txtemail.ReadOnly = .F.
		facturacao.pageframe1.page3.txtu_viatura.ReadOnly = .F.
		facturacao.pageframe1.page3.txthoraentrega.ReadOnly = .F.
	Else
		facturacao.pageframe1.page3.txtcontacto.ReadOnly = .T.
		facturacao.pageframe1.page3.txtmorada.ReadOnly = .T.
		facturacao.pageframe1.page3.txtcodpost.ReadOnly = .T.
		facturacao.pageframe1.page3.txtlocal.ReadOnly = .T.
		facturacao.pageframe1.page3.txttelefone.ReadOnly = .T.
		facturacao.pageframe1.page3.txtemail.ReadOnly = .T.
		facturacao.pageframe1.page3.txtu_viatura.ReadOnly = .T.
		facturacao.pageframe1.page3.txthoraentrega.ReadOnly = .T.
	Endif
	facturacao.Refresh
Endproc
**
Procedure uf_facturacao_exit
	If Used("uCrsAtendST")
		fecha("uCrsAtendST")
	Endif
	If Used("uCrsAtendCLPato")
		fecha("uCrsAtendCLPato")
	Endif
	If Used("uCrsAtendCL")
		fecha("uCrsAtendCL")
	Endif
	If Used("dadosPsico")
		fecha("dadosPsico")
	Endif
	If Used("uCrsFt")
		fecha("uCrsFt")
	Endif
	If Used("ucrsDadosDemPresc")
		fecha("ucrsDadosDemPresc")
	Endif
	If Used("uCrsFt2")
		fecha("uCrsFt2")
	Endif
	If Used("uCrsFi")
		fecha("uCrsFi")
	Endif
	If Used("uCrsVerificaRespostaDEM")
		fecha("uCrsVerificaRespostaDEM")
	Endif
	If Used("uCrsVerificaRespostaDEMTotal")
		fecha("uCrsVerificaRespostaDEMTotal")
	Endif
	If Used("ucrsvaliva")
		fecha("ucrsvaliva")
	Endif
	If Used("uCrsPlanosCombinados")
		fecha("uCrsPlanosCombinados")
	Endif
	If Used("fi2")
		fecha("fi2")
	Endif
	If Used("ft")
		fecha("ft")
	Endif
	If Used("ft2")
		fecha("ft2")
	Endif
	fecha("uc_rnuRecms")
	If Used("fi")
		If Type("FACTURACAO")<>"U" And Type("FACTURACAO.PAGEFRAME1.PAGE1.GRIDDOC") <> "U"
			facturacao.pageframe1.page1.griddoc.SetFocus
		Endif
		fecha("fi")
	Endif
	fecha("uCrsProdInteraccoes")
	fecha("uCrsInteraccoes")
	If Used("uCrsExcecaoPanel")
		fecha("uCrsExcecaoPanel")
	Endif
	If  .Not. (Type("excecaoMed")=="U")
		uf_excecaomed_sair()
	Endif
	If (Used("mixed_bulk_pend"))
		fecha("mixed_bulk_pend")
	Endif
	If (Used("fi_trans_info"))
		fecha("fi_trans_info")
	Endif
	If (Used("ucrsComboDocFac"))
		fecha("ucrsComboDocFac")
	Endif

	fecha("ucrTempLCefrID")

	myftintroducao = .F.
	myftalteracao = .F.
	Release myinserereceitacorrigida
	Release myCorriguirReceita
	Release myOldStamp
	If Type("FACTURACAO")<>"U"
		facturacao.Hide
		facturacao.Release
	Endif

Endproc
**
Function uf_facturacao_corrigirreceita
	If  .Not. Used("FT")
		Return .F.
	Endif
	Select ft
	If  .Not. Empty(Alltrim(ft.u_ltstamp)) .Or.  .Not. Empty(Alltrim(ft.u_ltstamp2))
		uf_corrigirreceita_chama(Alltrim(ft.u_ltstamp), Alltrim(ft.u_ltstamp2), ft.fdata)
	Else
		uf_perguntalt_chama("ESTE DOCUMENTO N�O CONT�M RECEITA. POR FAVOR VERIFIQUE", "OK", "", 64)
	Endif
Endfunc
**
Function uf_facturacao_validasepermitecorrigirreceita
	Lparameters lcftstamp
	Local lccorrigedem
	Store .T. To lccorrigedem
	If (Empty(lcftstamp) .Or.  .Not. Used("FT") .Or.  .Not. Used("FT2"))
		Return .F.
	Endif
	Select u_tlote, u_tlote2, ft2.u_abrev, ft2.u_abrev2, ft2.u_codigo, ft2.u_codigo2 From ft Left Join ft2 On ft.ftstamp=ft2.ft2stamp Into Cursor ucrsFtJoinTemp
	Select ucrsFtJoinTemp
	Goto Top
	Scan

		uv_ecompart = .F.

		If !Empty(ucrsFtJoinTemp.u_codigo)
			uv_ecompart = uf_gerais_getUmValor("cptpla", "e_compart", "codigo = '" + Alltrim(ucrsFtJoinTemp.u_codigo) + "'")
		Endif

		If !uv_ecompart And !Empty(ucrsFtJoinTemp.u_codigo2)
			uv_ecompart = uf_gerais_getUmValor("cptpla", "e_compart", "codigo = '" + Alltrim(ucrsFtJoinTemp.u_codigo2) + "'")
		Endif

		If uv_ecompart Or ((Inlist(Alltrim(u_tlote), "96", "97", "98", "99") .Or. Inlist(Alltrim(u_tlote2), "96", "97", "98", "99")) .And. (Alltrim(u_abrev)=="SNS" .Or. Alltrim(u_abrev2)=="SNS"))
			lccorrigedem = .F.
		Endif


		Local uv_permiteCorrigir
		uv_permiteCorrigir = .F.

		If !Empty(ucrsFtJoinTemp.u_codigo)
			uv_permiteCorrigir = uf_gerais_getUmValor("cptpla", "permiteCorrigir", "codigo = '" + Alltrim(ucrsFtJoinTemp.u_codigo) + "'")
		Endif


		If !Empty(ucrsFtJoinTemp.u_codigo2)
			uv_permiteCorrigir  = uf_gerais_getUmValor("cptpla", "permiteCorrigir", "codigo = '" + Alltrim(ucrsFtJoinTemp.u_codigo) + "'")
		Endif


		If(!uv_permiteCorrigir)
			lccorrigedem = .F.
		Endif


	Endscan
	If (Used("ucrsFtJoinTemp"))
		fecha("ucrsFtJoinTemp")
	Endif
	Return lccorrigedem
Endfunc
**
Function uf_facturacao_corrigirreceitaauto

	Local lcqttoriginal, lctokenoriginal, lcNrAtend, lcNewDocNr, lcOldStamp
	Store '' To  lcNrAtend
	If  .Not. Used("FT") And .Not. Used("FT2")
		Return .F.
	Endif

	Select ft
	lcNrAtend = ft.u_nratend

	Select ft2
	uf_faturacao_getCartoes(ft2.u_nbenef2,ft2.u_abrev,lcNrAtend)

	Select ft
	If ( .Not. uf_facturacao_validasepermitecorrigirreceita(Alltrim(ft.ftstamp)))
		uf_perguntalt_chama("Funcionalidade n�o aplic�vel a receitas DEM. "+Chr(13)+"Se pretendido anule e consulte novamente a receita.", "OK", "", 64)
		Return .F.
	Endif
	If  .Not. Empty(Alltrim(ft.u_ltstamp)) .Or.  .Not. Empty(Alltrim(ft.u_ltstamp2))
		If uf_gerais_validapermuser(ch_userno, ch_grupo, 'Inser��o de Receita - Introduzir')
			If uf_perguntalt_chama("Vai eliminar a receita do documento actual e introduzir uma nova. Depois de continuar n�o ser� poss�vel recuperar esta receita."+Chr(13)+Chr(13)+"Tem a certeza que pretende continuar?", "Sim", "N�o")
				If  .Not. Empty(ft.u_ltstamp)
					If  .Not. uf_corrigirreceita_eliminarcontadorespar(1, .T.)
						Return .F.
					Endif
				Endif
				If  .Not. Empty(ft.u_ltstamp2)
					If  .Not. uf_corrigirreceita_eliminarcontadorespar(2, .T.)
						Return .F.
					Endif
				Endif
				*******************NOVO N� RECEITA*******************************
				TEXT TO lcsql TEXTMERGE NOSHOW
			EXEC up_get_newDocNr 'FT', 75, '<<alltrim(mySite)>>', 0, 1, '<<alltrim(ft.ftstamp)>>'
				ENDTEXT
				If  .Not. uf_gerais_actgrelha("", "ucrsMeuSel", lcsql)
					Messagebox("N�o foi poss�vel atribuir um novo id � receita. Por favor contacte o suporte.", 16, "Logitools Software")
					Return .F.
				Endif
				lcNewDocNr = ucrsMeuSel.newDocNr
				lcOldStamp = ft.ftstamp

				Select * From ft Into Cursor uCrsFtCorrigeReceita Readwrite
				Select * From ft2 Into Cursor uCrsFt2CorrigeReceita Readwrite
				Select * From fi Into Cursor uCrsFiCorrigeReceita Readwrite


				Select uCrsFt2CorrigeReceita
				lctokenoriginal = Alltrim(uCrsFt2CorrigeReceita.token)
				uf_facturacao_novo(.T.)
				facturacao.containercab.nmdoc.Value = 'Inser��o de Receita'
				facturacao.containercab.nmdoc.LostFocus()
				Select * From uCrsFiCorrigeReceita Into Cursor fi Readwrite

				uf_facturacao_controlawklinhas()
				Select ft
				Local lcofistamp, lcftstamp
				Store '' To lcofistamp, lcftstamp
				lcftsamp = ft.ftstamp
				Local lcsta



				Select fi
				Goto Top
				Scan
					lcqttoriginal = fi.qtt
					If  .Not. Empty(fi.ref)
						uf_atendimento_actref()
					Endif
					lcstamp = uf_gerais_stamp()
					Select fi
					lcofistamp = fi.fistamp
					Replace fi.fistamp With lcstamp
					Replace fi.ofistamp With lcofistamp
					Replace fi.nmdoc With ft.nmdoc
					Replace fi.fno With lcNewDocNr
					Replace fi.ndoc With ft.ndoc
					Replace fi.u_psicont With 0
					Replace fi.u_bencont With 0
					Replace fi.qtt With lcqttoriginal
				Endscan
				Select fi
				Goto Top
				Select * From uCrsFtCorrigeReceita Into Cursor ft Readwrite
				Select ft
				Replace ft.nmdoc With fi.nmdoc
				Replace ft.tipodoc With 4
				Replace ft.fno With lcNewDocNr
				Replace ft.ftstamp With lcftsamp
				Replace ft.ndoc With fi.ndoc
				Replace ft.site With mysite
				Replace ft.pnome With myTerm
				Replace ft.pno With mytermno
				Replace ft.cxstamp With ''
				Replace ft.cxusername With ''
				Replace ft.ssstamp With ''
				Replace ft.ssusername With ''
				Replace ft.u_nratend With ''
				Replace ft.ftid With 0
				Replace ft.u_lote With 0
				Replace ft.u_lote2 With 0
				Replace ft.u_ltstamp With ''
				Replace ft.u_ltstamp2 With ''
				Replace ft.u_nslote With 0
				Replace ft.u_nslote2 With 0
				Replace ft.u_slote With 0
				Replace ft.u_slote2 With 0
				Replace ft.u_tlote With ''
				Replace ft.u_tlote2 With ''
				Select * From uCrsFt2CorrigeReceita Into Cursor ft2 Readwrite
				Select ft2
				Replace ft2.ft2stamp With lcftsamp
				Select fi
				Goto Top
				Scan
					uf_atendimento_actvaloresfi()
				Endscan
				uf_atendimento_acttotaisft()
				If Used("uCrsFtCorrigeReceita")
					fecha("uCrsFtCorrigeReceita")
				Endif
				If Used("uCrsFt2CorrigeReceita")
					fecha("uCrsFt2CorrigeReceita")
				Endif
				If Used("uCrsFiCorrigeReceita")
					fecha("uCrsFiCorrigeReceita")
				Endif
				If Used("uCrsFi2CorrigeReceita")
					fecha("uCrsFi2CorrigeReceita")
				Endif
				Select ft2
				If  .Not. Empty(ft2.token)
					TEXT TO lcsql TEXTMERGE NOSHOW
						select
							Dispensa_Eletronica_D.medicamento_cod
							,Dispensa_Eletronica_D.medicamento_cnpem
							,Dispensa_Eletronica_DD.ref
						From
							Dispensa_Eletronica_D (nolock)
							inner join Dispensa_Eletronica_DD (nolock) on Dispensa_Eletronica_D.id = Dispensa_Eletronica_DD.id
						Where
							Dispensa_Eletronica_D.token = '<<ALLTRIM(lcTokenOriginal)>>'
							and Dispensa_Eletronica_DD.token = '<<ALLTRIM(lcTokenOriginal)>>'
					ENDTEXT
					uf_gerais_actgrelha("", "uCrsDadosRelacionarCNPEM", lcsql)
					uf_atendimento_dem(.T., ft2.u_receita, ft2.u_nbenef, ft2.codacesso, ft2.coddiropcao, .T.)
					Local lcnrtentativas
					lcnrtentativas = 0
					Do While .T.
						Wait Window "" Timeout 3
						lcnrtentativas = lcnrtentativas+1
						TEXT TO lcsql TEXTMERGE NOSHOW
							exec up_dem_dadosReceita '<<ALLTRIM(facturacao.stamptmrConsultaDEM)>>'
						ENDTEXT
						uf_gerais_actgrelha("", "uCrsVerificaRespostaDEM", lcsql)
						If Reccount("uCrsVerificaRespostaDEM")>0
							If Alltrim(ucrsverificarespostadem.resultado_consulta_cod)<>"100003010001"
								regua(2)
								facturacao.stamptmrconsultadem = ''
								uf_perguntalt_chama("Resposta do Sistema Central de Prescri��es: "+Alltrim(ucrsverificarespostadem.resultado_consulta_descr), "OK", "", 16)
								Return .F.
							Endif
							Select ucrsverificarespostadem
							Goto Top
							Update ft2 Set token = ucrsverificarespostadem.token
							Update fi Set token = ucrsverificarespostadem.token, dem = .T.
							If Used("ucrsFiAuxTemp")
								fecha("ucrsFiAuxTemp")
							Endif
							Select * From fi Into Cursor ucrsFiAuxTemp Readwrite
							Select * From ucrsverificarespostadem Into Cursor uCrsVerificaRespostaDEM_Aux Readwrite
							Select uCrsVerificaRespostaDEM_Aux
							Goto Top
							Scan For  .Not. Empty(Alltrim(uCrsVerificaRespostaDEM_Aux.medicamento_cod)) .Or.  .Not. Empty(Alltrim(ASTR(uCrsVerificaRespostaDEM_Aux.medicamento_cnpem)))
								If Empty(Alltrim(uCrsVerificaRespostaDEM_Aux.medicamento_cod))
									Select ucrsFiAuxTemp
									Goto Top
									Scan For Empty(Alltrim(ucrsFiAuxTemp.id_validacaodem)) .And.  .Not. Empty(Alltrim(ucrsFiAuxTemp.ref))
										Select ucrsdadosrelacionarcnpem
										Goto Top
										Locate For Alltrim(uCrsVerificaRespostaDEM_Aux.medicamento_cnpem)==Alltrim(ucrsdadosrelacionarcnpem.medicamento_cnpem) .And. Alltrim(ucrsFiAuxTemp.ref)==Alltrim(ucrsdadosrelacionarcnpem.ref)
										If Found()
											Select uCrsVerificaRespostaDEM_Aux
											Select ucrsFiAuxTemp
											Replace ucrsFiAuxTemp.id_validacaodem With uCrsVerificaRespostaDEM_Aux.Id
										Endif
									Endscan
									Update fi From fi INNER Join ucrsFiAuxTemp On fi.fistamp=ucrsFiAuxTemp.fistamp Set fi.id_validacaodem = ucrsFiAuxTemp.id_validacaodem Where fi.token=Alltrim(uCrsVerificaRespostaDEM_Aux.token) .And.  .Not. Empty(Alltrim(ucrsFiAuxTemp.id_validacaodem))
								Else
									Select ucrsFiAuxTemp
									Goto Top
									Locate For Alltrim(ucrsFiAuxTemp.ref)=Alltrim(uCrsVerificaRespostaDEM_Aux.medicamento_cod) .And.  .Not. Empty(Alltrim(ucrsFiAuxTemp.ref)) .And. Empty(Alltrim(ucrsFiAuxTemp.id_validacaodem))
									If Found()
										Select ucrsFiAuxTemp
										Replace ucrsFiAuxTemp.id_validacaodem With uCrsVerificaRespostaDEM_Aux.Id
									Endif
									Update fi From fi INNER Join ucrsFiAuxTemp On fi.fistamp=ucrsFiAuxTemp.fistamp Set fi.id_validacaodem = ucrsFiAuxTemp.id_validacaodem Where fi.token=Alltrim(uCrsVerificaRespostaDEM_Aux.token) .And.  .Not. Empty(Alltrim(ucrsFiAuxTemp.id_validacaodem))
								Endif
							Endscan
							Exit
						Endif
						If lcnrtentativas>10
							Exit
						Endif
					Enddo
					regua(2)
				Endif
			Endif
		Else
			uf_perguntalt_chama("O SEU PERFIL N�O PERMITE INTRODUZIR RECEITAS.", "OK", "", 48)
			Return .F.
		Endif
	Else
		uf_perguntalt_chama("ESTE DOCUMENTO N�O CONT�M RECEITA. POR FAVOR VERIFIQUE", "OK", "", 64)
	Endif

	Public myCorriguirReceita, myOldStamp
	myOldStamp = lcOldStamp
	myCorriguirReceita	= .T.


Endfunc
**
Function uf_facturacao_consultarreceita
	Local lccodfarm, lcdbserver, lcdataini, lcdatafim, lcreceita, lctoken, lcnomejar, lctestjar
	Store '' To lccodfarm, lcdbserver, lcdataini, lcdatafim, lcreceita, lctoken, lcnomejar, lctestjar
	Select ucrse1
	If Alltrim(ucrse1.tipoempresa)<>"FARMACIA"
		uf_perguntalt_chama("FUNCIONALIDADE DISPON�VEL APENAS PARA CLIENTES DO TIPO FARM�CIA.", "OK", "", 64)
		Return .T.
	Endif
	If uf_gerais_verifyinternet()==.F.
		uf_perguntalt_chama("ESTA FUNCIONALIDADE REQUER LIGA��O � INTERNET."+Chr(13)+"POR FAVOR VERIFIQUE A SUA LIGA��O.", "OK", "", 64)
		Return .F.
	Endif
	Select ucrse1
	If Empty(Alltrim(ucrse1.u_codfarm))
		uf_perguntalt_chama("C�digo de Farm�cia n�o configurado. Contacte o suporte.", "OK", "", 32)
		Return .F.
	Endif
	Select ucrse1
	lccodfarm = Alltrim(ucrse1.u_codfarm)
	Select ft2
	If  .Not. Empty(ft2.u_receita)
		lcreceita = Alltrim(ft2.u_receita)
	Else
		uf_perguntalt_chama("Para utilizar este funcionalidade, o campo n� de receita deve estar preenchido.", "OK", "", 32)
		Return .F.
	Endif
	If Empty(ft2.token)
		uf_perguntalt_chama("N�o � poss�vel efetuar a opera��o para a receita seleccionada. N�o existe token de verifica��o. Por favor contacte o Suporte.", "OK", "", 32)
		Return .F.
	Else
		lctoken = Alltrim(ft2.token)
	Endif
	lcdataini = ''
	lcdatafim = ''
	regua(0, 5, "A obter dados...")
	regua(1, 1, "A obter dados...")
	*!*	 IF DIRECTORY(ALLTRIM(uf_gerais_getparameter("ADM0000000185", "TEXT"))+'\DEM')
	*!*	    lcnomejar = "LtsDispensaEletronica.jar"
	*!*
	*!*	    IF uf_gerais_getparameter("ADM0000000227", "BOOL")==.F.
	*!*	       lctestjar = "0"
	*!*	    ELSE
	*!*	       lctestjar = "1"
	*!*	    ENDIF
	*!*	    IF  .NOT. FILE(ALLTRIM(uf_gerais_getparameter("ADM0000000185", "TEXT"))+'\DEM\'+ALLTRIM(lcnomejar))
	*!*	       uf_perguntalt_chama("Ficheiro configura��o n�o encontrado. Por favor contacte o Suporte", "OK", "", 64)
	*!*	       RETURN .F.
	*!*	    ENDIF
	*!*	    IF  .NOT. USED("uCrsServerName")
	*!*	       TEXT TO lcsql TEXTMERGE NOSHOW
	*!*					Select @@SERVERNAME as dbServer
	*!*	       ENDTEXT
	*!*	       IF  .NOT. uf_gerais_actgrelha("", "ucrsServerName", lcsql)
	*!*	          uf_perguntalt_chama("N�o foi possivel proceder ao servi�o de Efetiva��o da Dispensa. Contacte o suporte.", "OK", "", 32)
	*!*	          regua(2)
	*!*	          RETURN .F.
	*!*	       ENDIF
	*!*	    ENDIF
	*!*	    SELECT ucrsservername
	*!*	    lcdbserver = ALLTRIM(ucrsservername.dbserver)
	*!*	    lcwspath = ALLTRIM(uf_gerais_getparameter("ADM0000000185", "TEXT"))+'\DEM\'+ALLTRIM(lcnomejar)+' consultaDispensas '+' --codFarmacia='+ALLTRIM(lccodfarm)+' --dataIni='+lcdataini+' --nrReceita='+ALLTRIM(lcreceita)+' --dbServer='+ALLTRIM(lcdbserver)+' --dbName='+UPPER(ALLTRIM(sql_db))+' --site='+UPPER(ALLTRIM(STRTRAN(mysite, ' ', '_')))+' "--token='+ALLTRIM(lctoken)+'"'+' --test='+lctestjar
	*!*	    IF uf_gerais_getparameter("ADM0000000227", "BOOL")==.T.
	*!*	       _CLIPTEXT = lcwspath
	*!*	       uf_perguntalt_chama("Webservice de teste...", "OK", "", 64)
	*!*	    ENDIF
	*!*	    lcwspath = "javaw -jar "+lcwspath
	*!*	    owsshell = CREATEOBJECT("WScript.Shell")
	*!*	    owsshell.run(lcwspath, 1, .T.)
	*!*	    regua(2)
	*!*	    lcsql = ""
	TEXT TO lcsql TEXTMERGE NOSHOW
			exec up_dem_consultaDispensa '<<lcToken>>'
	ENDTEXT
	If  .Not. uf_gerais_actgrelha("", "uCrsAuxConsulta", lcsql)
		regua(2)
		uf_perguntalt_chama("N�o foi poss�vel verificar a informa��o retornada pela SPMS. Por favor contacte o Suporte.", "OK", "", 32)
		Return .F.
	Else
		regua(2)
		uf_consultadispensa_chama()
	Endif
	*!* ENDIF
	If Used("uCrsAuxToken")
		fecha("uCrsAuxToken")
	Endif
	If Used("uCrsAuxConsulta")
		fecha("uCrsAuxConsulta")
	Endif

	regua(2)
Endfunc
**
Function uf_facturacao_atribuilote
	Local lcref
	If Used("TD")
		Select td
		If td.tipodoc==3 .Or. td.tipodoc==4
			Return .F.
		Endif
	Endif
	If  .Not. uf_gerais_getparameter("ADM0000000211", "BOOL")
		Return .F.
	Endif
	Select fi
	If fi.usalote==.F.
		Return .F.
	Endif
	uf_facturacao_dividelote(fi.ref, fi.qtt, fi.fistamp)
	Select ft
	lcsql = ""
	TEXT TO lcsql TEXTMERGE NOSHOW
		exec up_ga_loteDetalhePrecos '<<ALLTRIM(fi.lote)>>','<<ALLTRIM(fi.ref)>>', <<IIF(EMPTY(ft.tipoDoc),0,ft.tipoDoc)>>
	ENDTEXT
	If  .Not. uf_gerais_actgrelha("", "ucrsPesqLote", lcsql)
		Messagebox("N�o foi possivel verificar os pre�os do Lote. Por favor contacte o suporte.", 16, "Logitools Software")
		Return .F.
	Endif
	If Reccount("ucrsPesqLote")>0
		Select ucrspesqlote
		Goto Top
		Select fi
		Replace fi.amostra With .T.
		Replace fi.epv With ucrspesqlote.epv1
		Replace fi.u_epvp With ucrspesqlote.epv1
		Replace fi.ecusto With ucrspesqlote.pct
		Replace fi.lote With ucrspesqlote.lote
	Endif
	If Reccount("ucrsPesqLote")>1
		Select fi
		Replace fi.alertalote With .T.
	Else
		Select fi
		Replace fi.alertalote With .F.
	Endif
Endfunc
**
Function uf_facturacao_beforrowcolumnchange
	If myftintroducao==.F. .And. myftalteracao==.F.
		Return .F.
	Endif
	facturacao.oldqtt = fi.qtt
Endfunc
**
Function uf_facturacao_atribuilotealteraqtt
	Lparameters lclote
	Local j, i, k, lcref, lcstock
	Store 0 To lcstock
	If  .Not. uf_gerais_getparameter("ADM0000000211", "BOOL")
		Return .F.
	Endif
	If myftintroducao==.F. .And. myftalteracao==.F.
		Return .F.
	Endif
	If Empty(lclote)
		lclote = fi.lote
	Endif
	Select fi
	If fi.usalote==.F. .Or. ft.tipodoc==3 .Or. ft.tipodoc==4
		Return .F.
	Endif
	uf_facturacao_dividelote(fi.ref, fi.qtt, fi.fistamp)
Endfunc
**
Function uf_facturacao_desdobraqttemlotes
	Local i, j, k, lcref, lclordem, lcepv, lcbistamp, lcqtto, lcqttaplicadalote
	Store 0 To k
	Select fi
	Goto Top
	Scan For  .Not. Empty(fi.ref)
		lcref = fi.ref
		lclordem = fi.lordem
		If j==.F.
			lcqtto = fi.qtt
		Endif
		lcbistamp = fi.bistamp
		TEXT TO lcsql TEXTMERGE NOSHOW
			Select usalote from st (nolock) WHERE ref ='<<Fi.ref>>'
		ENDTEXT
		If  .Not. uf_gerais_actgrelha("", "ucrsDadosRefLote", lcsql)
			Messagebox("N�o foi poss�vel verificar defini��es da refer�ncia a importar. Por favor contacte o suporte.", 16, "Logitools Software")
			Return .F.
		Endif
		If ucrsdadosreflote.usalote==.T. .And. Empty(fi.lote)
			TEXT TO lcsql TEXTMERGE NOSHOW
				Select
					0 as qttnova
					,ref
					,lote
					,stock = isnull(sum(case when cm < 50 then qtt else -qtt end),0)
					,ousrdata = MIN(ousrdata)
					,ousrhora = MIN(ousrhora)
				from
					sl (nolock)
				where
					ref = '<<ALLTRIM(Fi.ref)>>'
					and armazem = <<Fi.armazem>>
				Group by
					ref
					,lote
				Having
					isnull(sum(case when cm < 50 then qtt else -qtt end),0) > 0
				Order by
					MIN(ousrdata)
					,MIN(ousrhora)

			ENDTEXT
			If  .Not. uf_gerais_actgrelha("", "ucrsDadosRefLote", lcsql)
				Messagebox("N�o foi poss�vel verificar defini��es dos lotes a atribuir. Por favor contacte o suporte.", 16, "Logitools Software")
				Return .F.
			Endif
			k = 0
			For i = 1 To fi.qtt
				Select ucrsdadosreflote
				Goto Top
				Scan
					If ucrsdadosreflote.novaqtt<ucrsdadosreflote.stock .And. ucrsdadosreflote.stock>0
						k = k+1
						Replace ucrsdadosreflote.novaqtt With ucrsdadosreflote.novaqtt+1
						Exit
					Endif
				Endscan
			Endfor
			j = .F.
			Select ucrsdadosreflote
			Goto Top
			Scan For ucrsdadosreflote.novaqtt<>0
				TEXT TO lcsql TEXTMERGE NOSHOW
					exec up_ga_loteUltimoPreco '<<ALLTRIM(ucrsDadosRefLote.lote)>>'
				ENDTEXT
				If  .Not. uf_gerais_actgrelha("", "ucrsUltimoPrecoLote", lcsql)
					Messagebox("N�o foi poss�vel verificar defini��es dos lotes a atribuir. Por favor contacte o suporte.", 16, "Logitools Software")
					Return .F.
				Endif
				If j==.F.
					Select fi
					Replace fi.qtt With ucrsdadosreflote.novaqtt
					Replace fi.lote With ucrsdadosreflote.lote
					Replace fi.u_epvp With ucrsultimoprecolote.epv1
					Replace fi.epv With ucrsultimoprecolote.epv1
					Replace fi.etiliquido With ucrsultimoprecolote.epv1*ucrsdadosreflote.novaqtt
					Replace fi.tiliquido With ucrsultimoprecolote.epv1*ucrsdadosreflote.novaqtt*200.482
					j = .T.
				Else
					uf_atendimento_adicionalinhafi(.T., .T.)
					Replace fi.ref With lcref
					uf_facturacao_eventoreffi(.T.)
					Replace fi.qtt With ucrsdadosreflote.novaqtt
					Replace fi.lordem With lclordem+1
					Replace fi.lote With ucrsdadosreflote.lote
					Replace fi.bistamp With lcbistamp
					Replace fi.u_epvp With ucrsultimoprecolote.epv1
					Replace fi.epv With ucrsultimoprecolote.epv1
					Replace fi.etiliquido With ucrsultimoprecolote.epv1*ucrsdadosreflote.novaqtt
					Replace fi.tiliquido With ucrsultimoprecolote.epv1*ucrsdadosreflote.novaqtt*200.482
				Endif
				uf_atendimento_fiiliq(.T.)
			Endscan
		Endif
		If k<lcqtto
			If j==.F.
				Replace fi.lote With ""
			Else
				uf_atendimento_adicionalinhafi(.T., .T.)
				Replace fi.ref With lcref
				uf_facturacao_eventoreffi(.T.)
				Replace fi.qtt With lcqtto-k
				Replace fi.lordem With lclordem+1
				Replace fi.lote With ""
				Replace fi.bistamp With lcbistamp
			Endif
			uf_atendimento_fiiliq(.T.)
		Endif
		uf_atendimento_acttotaisft()
	Endscan
Endfunc
**
Procedure uf_facturacao_atribuilotesdocumento
	facturacao.LockScreen = .T.
	Select * From fi Into Cursor ucrsFiaux2 Readwrite
	Select ucrsFiaux2
	Goto Top
	Scan For ucrsFiaux2.usalote==.T. .And.  .Not. Empty(ucrsFiaux2.ref)
		Select fi
		Locate For Alltrim(fi.fistamp)==Alltrim(ucrsFiaux2.fistamp)
		If Found()
			If uf_gerais_getparameter("ADM0000000211", "BOOL")
				uf_facturacao_atribuilotealteraqtt()
			Endif
			uf_atendimento_fiiliq(.T.)
			uf_atendimento_actvaloresfi()
			uf_atendimento_acttotaisft()
		Endif
	Endscan
	facturacao.LockScreen = .F.
Endproc
**
Function uf_facturacao_dividelote
	Lparameters lcref, lcqtt, lcfistamp
	Local lcsql, i, j, lcnovostamp, lcarmazem
	j = 0
	lcnovostamp = ""
	lcarmazem = Iif(Empty(facturacao.containercab.armazem.Value), myarmazem, facturacao.containercab.armazem.Value)
	Select * From fi Where fi.fistamp=lcfistamp Into Cursor ucrsFiAux Readwrite
	lcsql = ''
	TEXT TO lcsql TEXTMERGE NOSHOW
		Select
			ref
			,lote
			,armazem
			,stock = isnull(sum(case when cm < 50 then qtt else -qtt end),0)
			,ousrdata = MIN(ousrdata)
			,ousrhora = MIN(ousrhora)
			,qttAtribuir = 0
		from
			sl (nolock)
		where
			ref = '<<ALLTRIM(lcRef)>>'
			and sl.armazem = <<lcArmazem>>
		Group by
			ref
			,lote
			,armazem
		Having
			isnull(sum(case when cm < 50 then qtt else -qtt end),0) > 0
		Order by
			MIN(ousrdata)
			,MIN(ousrhora)
	ENDTEXT
	If  .Not. uf_gerais_actgrelha("", "ucrsDivideLote", lcsql)
		Messagebox("N�o foi possivel verificar o Lote a atribuir. Por favor contacte o suporte.", 16, "Logitools Software")
		Return .F.
	Endif
	Select Sum(qtt) As qtt, ref, lote From fi Where ref=lcref And  Not Empty(lote) And fi.fistamp<>lcfistamp Group By ref, lote Into Cursor ucrsQtAplicadaLoteAtual Readwrite
	If Reccount("ucrsDivideLote")>0
		Select ucrsdividelote
		Goto Top
		Scan
			Select ucrsQtAplicadaLoteAtual
			Goto Top
			Scan For ucrsQtAplicadaLoteAtual.lote==ucrsdividelote.lote .And. ucrsQtAplicadaLoteAtual.ref==ucrsdividelote.ref
				Replace ucrsdividelote.stock With ucrsdividelote.stock-ucrsQtAplicadaLoteAtual.qtt
			Endscan
		Endscan
	Endif
	For i = 1 To lcqtt
		Select ucrsdividelote
		Goto Top
		Scan For ucrsdividelote.qttatribuir<ucrsdividelote.stock
			Replace ucrsdividelote.qttatribuir With ucrsdividelote.qttatribuir+1
			Exit
		Endscan
	Endfor
	Select ucrsdividelote
	Calculate Sum(qttatribuir) To lcqttaplicada
	lcqtnaoaplicada = lcqtt-lcqttaplicada
	If lcqtnaoaplicada>0
		Select ucrsdividelote
		Append Blank
		Replace ucrsdividelote.ref With lcref
		Replace ucrsdividelote.qttatribuir With lcqtnaoaplicada
		Replace ucrsdividelote.lote With ''
	Endif
	Select ucrsdividelote
	Goto Top
	Scan For ucrsdividelote.qttatribuir>0
		j = j+1
		If j=1
			Replace fi.qtt With ucrsdividelote.qttatribuir
			Replace fi.lote With ucrsdividelote.lote
			Replace fi.armazem With ucrsdividelote.armazem
		Else
			lcnovostamp = uf_gerais_stamp()
			Select fi
			Append From Dbf('ucrsFiAux')
			Select fi
			Replace fi.fistamp With lcnovostamp
			Replace fi.qtt With ucrsdividelote.qttatribuir
			Replace fi.lote With ucrsdividelote.lote
			Replace fi.armazem With ucrsdividelote.armazem
			Replace fi.lordem With fi.lordem
		Endif
		uf_atendimento_fiiliq(.T.)
	Endscan
Endfunc
**
Function uf_facturacao_validastocklotes
	Local lcsql, lcref
	If td.lancasl==.F.
		Return .T.
	Endif
	Select fi
	Goto Top
	Scan For fi.usalote==.T. .And.  .Not. Empty(fi.ref)
		lcref = fi.ref
		lclote = fi.lote
		Select Sum(qtt) As qtt, ref, lote From fi Where ref=lcref And  Not Empty(lote) Group By ref, lote Into Cursor ucrsQtAplicadaLoteAtual Readwrite
		lcsql = ""
		TEXT TO lcsql TEXTMERGE NOSHOW
			Select
				0 as qttnova
				,ref
				,lote
				,stock = isnull(sum(case when cm < 50 then qtt else -qtt end),0)
				,ousrdata = MIN(ousrdata)
				,ousrhora = MIN(ousrhora)
			from
				sl (nolock)
			where
				ref = '<<ALLTRIM(fi.ref)>>'
				AND lote = '<<ALLTRIM(lcLote)>>'
			Group by
				ref
				,lote
			Order by
				MIN(ousrdata)
				,MIN(ousrhora)
		ENDTEXT
		If  .Not. uf_gerais_actgrelha("", "ucrsStocksLote", lcsql)
			Messagebox("N�o foi possivel verificar o Lote a atribuir. Por favor contacte o suporte.", 16, "Logitools Software")
			Return .F.
		Endif
		Select ucrsstockslote
		Goto Top
		lcstock = ucrsstockslote.stock
		If lcstock<fi.qtt
			uf_perguntalt_chama("N�o existe stock suficiente no Lote ["+Alltrim(lclote)+"]."+Chr(13)+Chr(13)+"Stock: "+Alltrim(Str(lcstock, 9, 0))+Chr(13)+Chr(13)+"Selecione lote diferente.", "OK", "", 32)
			Return .F.
		Endif
		If Reccount("ucrsStocksLote")>0
			Select ucrsstockslote
			Goto Top
			Scan
				Select ucrsQtAplicadaLoteAtual
				Goto Top
				Scan For ucrsQtAplicadaLoteAtual.lote==ucrsstockslote.lote .And. ucrsQtAplicadaLoteAtual.ref==ucrsstockslote.ref
					Replace ucrsstockslote.qttnova With ucrsstockslote.qttnova+ucrsQtAplicadaLoteAtual.qtt
				Endscan
			Endscan
		Endif
		Select ucrsstockslote
		Calculate Sum(qttnova) To lcsomaqtt
		Select ucrsstockslote
		Calculate Sum(stock) To lcsomastock
		If lcsomaqtt>lcsomastock
			uf_perguntalt_chama("N�o existe stock suficiente no Lote ["+Alltrim(lclote)+"]."+Chr(13)+Chr(13)+"Stock: "+Alltrim(Str(lcstock, 9, 0))+Chr(13)+Chr(13)+"Selecione lote diferente.", "OK", "", 32)
			Return .F.
		Endif
	Endscan
	Return .T.
Endfunc
**
Procedure uf_facturacao_confencomendasaviamentos
	If Empty(Alltrim(ft.nmdoc))
		uf_perguntalt_chama("DEVE SELECIONAR UM DOCUMENTO PARA PROSSEGUIR COM A CONFER�NCIA. POR FAVOR VERIFIQUE!", "OK", "", 64)
	Else
		uf_confencomendas_chama(.T.)
	Endif
Endproc
**
Procedure uf_facturacao_tabelafixaivaclientes
	Lparameters lctabiva
	Local lctaxaiva, lcsql
	lctaxaiva = 0
	Select ft
	Replace ft.tabiva With lctabiva
	Select fi
	Goto Top
	Scan
		uf_atendimento_fiiliq(.T.)
	Endscan
	uf_atendimento_acttotaisft()
Endproc
**
Function uf_facturacao_importhistvendas
	Local lcfilename, lcfileext
	lcfilename = Getfile()
	Local lcsql
	Store '' To lcsql
	Local lcmes, lcano
	Local lcrowdata, lcrowano, lcrowmesx, lcrowdia, lcrowhora
	If Empty(lcfilename)
		uf_perguntalt_chama("Deve especificar o ficheiro de importa��o. Por favor verifique.", "OK", "", 64)
		Return .F.
	Endif
	lclocal = '"'+Alltrim(lcfilename)+'"'
	If  .Not. File(lclocal)
		uf_perguntalt_chama("A localiza��o especificada para importa��o do ficheiro � inv�lida. Por favor verifique.", "OK", "", 64)
		Return .F.
	Endif
	If Used("uCrsImportHistVendas")
		fecha("uCrsImportHistVendas")
	Endif
	If Used("uCrsTempFile")
		fecha("uCrsTempFile")
	Endif
	If Used("ucrsTrocaIdLt")
		fecha("ucrsTrocaIdLt")
	Endif
	lcano = Year(Datetime()+(difhoraria*3600))
	lcmes = Month(Datetime()+(difhoraria*3600))
	Public mylocallt
	lcfileext = Upper(Right(Alltrim(lcfilename), 3))
	Do Case
		Case lcfileext=="XLS"
			uf_gerais_xls2cursor(lcfilename, "SellOut", "uCrsImportHistVendas")
			Create Cursor uCrsTempFile (ano i, mes i, dia i, hora C(8), ref C(18), pvp N(11, 3), qt i, site C(20), Design C(60), lab C(150), familia C(18), faminome C(60), marca C(20), tabiva N(2, 0), taxaiva N(5, 2), site_nr i, pvporig N(11, 3) Default 0, stock i)
			columndata = (Getwordnum(ucrsimporthistvendas.data_hora_venda, 1, " "))
			columnhora = (Getwordnum(ucrsimporthistvendas.data_hora_venda, 2, " "))
			columnano = (Getwordnum(columndata, 1, "/"))
			columnmes = (Getwordnum(columndata, 2, "/"))
			columndia = (Getwordnum(columndata, 3, "/"))
			Select ucrsimporthistvendas
			If Type('columnAno')=="U" .Or. Type('columnMes')=="U" .Or. Type('columnDia')=="U" .Or. Type('columnHora')=="U" .Or. Type('uCrsImportHistVendas.cnp')=="U" .Or. Type('uCrsImportHistVendas.pvp')=="U" .Or. Type('uCrsImportHistVendas.qtd_venda')=="U" .Or. Type('uCrsImportHistVendas.cod_farmacia')=="U"
				uf_perguntalt_chama("O nome das colunas no ficheiro n�o � o correto. Por favor valide.", "OK", "", 64)
				Return .F.
			Endif
			Select ucrsimporthistvendas
			Goto Top
			Scan For  .Not. Empty(ucrsimporthistvendas.cnp) .And.  .Not. Isnull(ucrsimporthistvendas.cnp) .And.  .Not. Empty(ucrsimporthistvendas.qtd_venda) .And.  .Not. Isnull(ucrsimporthistvendas.qtd_venda)
				Select uCrsTempFile
				Append Blank
				lcrowdata = (Getwordnum(ucrsimporthistvendas.data_hora_venda, 1, " "))
				lcrowhora = (Getwordnum(ucrsimporthistvendas.data_hora_venda, 2, " "))
				lcrowano = (Getwordnum(lcrowdata, 1, "/"))
				lcrowmes = (Getwordnum(lcrowdata, 2, "/"))
				lcrowdia = (Getwordnum(lcrowdata, 3, "/"))
				If Type('lcRowAno')=="N"
					Replace uCrsTempFile.ano With lcrowano
				Else
					Replace uCrsTempFile.ano With Val(lcrowano)
				Endif
				If Type('lcRowMes')=="N"
					Replace uCrsTempFile.mes With lcrowmes
				Else
					Replace uCrsTempFile.mes With Val(lcrowmes)
				Endif
				If Type('lcRowDia')=="N"
					Replace uCrsTempFile.dia With Iif(Isnull(lcrowdia), 0, lcrowdia)
				Else
					Replace uCrsTempFile.dia With Val(Iif(Isnull(lcrowdia), '0', lcrowdia))
				Endif
				Replace uCrsTempFile.hora With lcrowhora
				If Type('uCrsImportHistVendas.cnp')=="N"
					Replace uCrsTempFile.ref With ASTR(ucrsimporthistvendas.cnp)
				Else
					Replace uCrsTempFile.ref With ucrsimporthistvendas.cnp
				Endif
				If Type('uCrsImportHistVendas.pvp')=="N"
					Replace uCrsTempFile.pvp With Iif(Isnull(ucrsimporthistvendas.pvp), 0, ucrsimporthistvendas.pvp)
				Else
					Replace uCrsTempFile.pvp With Val(Iif(Isnull(ucrsimporthistvendas.pvp), '0', ucrsimporthistvendas.pvp))
				Endif
				If Type('uCrsImportHistVendas.qtd_venda')=="N"
					Replace uCrsTempFile.qt With ucrsimporthistvendas.qtd_venda
				Else
					Replace uCrsTempFile.qt With Val(ucrsimporthistvendas.qtd_venda)
				Endif
				Replace uCrsTempFile.site With ucrsimporthistvendas.cod_farmacia
				If Type('uCrsImportHistVendas.stock_data')=="N"
					Replace uCrsTempFile.stock With ucrsimporthistvendas.stock_data
				Else
					Replace uCrsTempFile.stock With Val(ucrsimporthistvendas.stock_data)
				Endif
				Select ucrsimporthistvendas
			Endscan
		Case lcfileext=="TXT"
			If  .Not. uf_perguntalt_chama("Aten��o: O ficheiro a importar deve, obrigat�riamente, ter sido gerado no m�s atual. Pretende continuar?", "SIM", "N�O", 64)
				Return .F.
			Endif
			TEXT TO lcsql TEXTMERGE NOSHOW
				select distinct Local=site from empresa
			ENDTEXT
			If  .Not. uf_gerais_actgrelha("", "ucrsTrocaIdLt", lcsql)
				uf_perguntalt_chama("N�o foi poss�vel listar os ID's dos Locais. Por favor contacte o suporte.", "", "OK", 16)
				Return .F.
			Endif
			uf_valorescombo_chama("myLocalLt", 0, "ucrsTrocaIdLt", 2, "Local", "Local", .F., .F., .T., .T.)
			If Empty(mylocallt)
				uf_perguntalt_chama("N�o pode continuar sem escolher o ID do local.", "", "OK", 48)
				Return .F.
			Endif
			Create Cursor ucrsimporthistvendas (ref C(18), nom C(120), fap C(120), localizacao C(100), sac C(10), stm C(10), smi C(10), qte C(10), duv C(20), duc C(20), pvp C(13), pcu C(20), iva C(20), cat C(100), ct1 C(120), gen C(10), gpr C(10), cla C(10), tpr C(10), car C(20), gam C(10), v_0 C(20), v_1 C(20), v_2 C(20), v_3 C(20), v_4 C(20), v_5 C(20), v_6 C(20), v_7 C(20), v_8 C(20), v_9 C(20), v_10 C(20), v_11 C(20), v_12 C(20), v_13 C(20), v_14 C(20), v_15 C(20), v_16 C(20), v_17 C(20), v_18 C(20), v_19 C(20), v_20 C(20), v_21 C(20), v_22 C(20), v_23 C(20), dtval C(20), fpd C(120), lad C(120), prateleira C(60), gama C(20), grupohomogeneo C(120), inactivo C(10), pvp5 C(20), Design C(60), lab C(150), familia C(18), faminome C(60), marca C(20), tabiva N(2, 0), taxaiva N(5, 2), site_nr i, site C(60), pvporig N(11, 3) Default 0)
			Select ucrsimporthistvendas
			Append From &lclocal Type Delimited With Tab
			Select * From ucrsimporthistvendas Where Alltrim(ref)<>'CPR' And localizacao<>"ARMAZ�M" And Alltrim(localizacao)<>"Reserva de Produtos" Order By ref Into Cursor uCrsTempFile Readwrite
		Otherwise
			uf_perguntalt_chama("Tipo de Ficheiro inv�lido. Deve indicar um ficheiro xls ou txt.", "OK", "", 32)
			Return .F.
	Endcase
	Local lcsql2, nrregistos, lccount, lcok
	Store '' To lcsql2
	Store 0 To nrregistos, lccount
	Store .F. To lcok
	Local txtano, txtmes, lcvarqtt, lcqt, lcpvp
	Store 0 To lcqt, lcpvp
	txtano = lcano
	txtmes = lcmes
	If Used("uCrsTempFile")
		nrregistos = Reccount("uCrsTempFile")
		If nrregistos>0
			regua(0, nrregistos, "A processar importa��o do ficheiro...")
			If  .Not. uf_facturacao_importhistvendascriast(lcfileext)
				uf_perguntalt_chama("N�o foi poss�vel criar as fichas dos artigos. Por favor contacte o suporte", "OK", "", 16)
				regua(2)
				Return .F.
			Endif
			regua(1, 1, "A Eliminar registos duplicados...")
			If lcfileext=="TXT"
				Select ref, Count(ref), Design From uCrsTempFile Group By ref, Design Having Count(ref)>1 Into Cursor uCrsTempFileAux Readwrite
				Select * From uCrsTempFile Where ref Not In(Select ref From uCrsTempFileAux) Into Cursor uCrsTempFileAux1 Readwrite
				If Used("uCrsTempFile")
					fecha("uCrsTempFile")
				Endif
				Select * From uCrsTempFileAux1 Into Cursor uCrsTempFile Readwrite
				If Used("uCrsTempFileAux1")
					fecha("uCrsTempFileAux1")
				Endif
			Endif
			Select uCrsTempFile
			Goto Top
			Scan For  .Not. Empty(uCrsTempFile.ref)
				regua(1, Recno("uCrsTempFile"), "A Eliminar registos duplicados: "+uCrsTempFile.ref)
				txtano = lcano
				txtmes = lcmes
				Do Case
					Case lcfileext=="TXT"
						For i = 1 To 12
							lccount = lccount+1
							TEXT TO lcsql TEXTMERGE NOSHOW
								DELETE hist_vendas where ano = <<txtAno>> and mes = <<txtMes>> and dia = 0 and ref = '<<alltrim(uCrsTempFile.ref)>>' AND site = '<<ALLTRIM(myLocalLt)>>'
							ENDTEXT
							If i<12
								lcsql2 = lcsql2+lcsql+Chr(10)+Chr(13)
							Endif
							If txtmes==1
								txtmes = 12
								txtano = lcano-1
							Else
								txtmes = txtmes-1
							Endif
						Endfor
					Case lcfileext=="XLS"
				Endcase
				If lccount>=100
					lccount = 0
					lcsql2 = lcsql2+lcsql
					If  .Not. uf_gerais_actgrelha("", "", lcsql2)
						uf_perguntalt_chama("N�o foi poss�vel importar o ficheiro corretamente. Por favor valide a integridade do mesmo.", "OK", "", 16)
						regua(2)
						Return .F.
					Endif
					lcsql2 = ""
				Else
					If  .Not. Empty(lcsql)
						lcsql2 = lcsql2+lcsql+Chr(10)+Chr(13)
					Endif
				Endif
				Select uCrsTempFile
			Endscan
			If  .Not. Empty(lcsql2)
				If  .Not. uf_gerais_actgrelha("", "", lcsql2)
					uf_perguntalt_chama("N�o foi poss�vel importar o ficheiro corretamente. Por favor valide a integridade do mesmo.", "OK", "", 16)
					regua(2)
					Return .F.
				Endif
				lcsql2 = ""
			Endif
			regua(1, 1, "A inserir registos...")
			lcsql = ''
			Select uCrsTempFile
			Goto Top
			Scan For  .Not. Empty(uCrsTempFile.ref)
				regua(1, Recno("uCrsTempFile"), "A inserir registos: "+uCrsTempFile.ref)
				txtano = lcano
				txtmes = lcmes
				Do Case
					Case lcfileext=="TXT"
						For i = 1 To 12
							lcvarqtt = "ucrsTempFile.V_"+Alltrim(Str(i-1))
							lcqt = Val(&lcvarqtt)
							lcpvp = Val(Strtran(uCrsTempFile.pvp, ',', '.'))
							If lcqt>0
								lccount = lccount+1
								If lcsql2==""
									TEXT TO lcsql TEXTMERGE NOSHOW
										insert into hist_vendas (ano,mes,dia,ref,pvp,qt,site)
										values
											(<<txtAno>>, <<txtMes>>, 0, '<<alltrim(uCrsTempFile.ref)>>', <<lcPvp>>, <<lcQt>>, '<<ALLTRIM(myLocalLt)>>')
									ENDTEXT
								Else
									TEXT TO lcsql TEXTMERGE NOSHOW
										(<<txtAno>>, <<txtMes>>, 0, '<<alltrim(uCrsTempFile.ref)>>', <<lcPvp>>, <<lcQt>>, '<<ALLTRIM(myLocalLt)>>')
									ENDTEXT
								Endif
								If  .Not. Empty(lcsql2)
									lcsql2 = lcsql2+","+lcsql+Chr(10)+Chr(13)
								Else
									lcsql2 = lcsql2+lcsql+Chr(10)+Chr(13)
								Endif
							Endif
							If txtmes==1
								txtmes = 12
								txtano = lcano-1
							Else
								txtmes = txtmes-1
							Endif
						Endfor
					Case lcfileext=="XLS"
						lccount = lccount+1
						TEXT TO lcsql TEXTMERGE NOSHOW

								IF exists (Select ref From hist_vendas (nolock) where ano = <<uCrsTempFile.ano>> and mes = <<uCrsTempFile.mes>> and dia = <<uCrsTempFile.dia>> and hora = '<<alltrim(uCrsTempFile.hora)>>' and ref = '<<alltrim(uCrsTempFile.ref)>>' and site = '<<alltrim(uCrsTempFile.site)>>')
								BEGIN
									UPDATE hist_vendas SET hist_vendas.qt = hist_vendas.qt + <<uCrsTempFile.qt>>, pvp = <<uCrsTempFile.pvp>>, hist_vendas.stock = <<uCrsTempFile.stock>> where ano = <<uCrsTempFile.ano>> and mes = <<uCrsTempFile.mes>> and dia = <<uCrsTempFile.dia>> and hora = '<<alltrim(uCrsTempFile.hora)>>' and ref = '<<alltrim(uCrsTempFile.ref)>>' and site = '<<alltrim(uCrsTempFile.site)>>'
								END else begin

									insert into hist_vendas (ano,mes,dia,hora,ref,pvp,qt,site,stock)
									values
										(<<uCrsTempFile.ano>>, <<uCrsTempFile.mes>>, <<uCrsTempFile.dia>>, '<<alltrim(uCrsTempFile.hora)>>', '<<alltrim(uCrsTempFile.ref)>>', <<uCrsTempFile.pvp>>, <<uCrsTempFile.qt>>, '<<alltrim(uCrsTempFile.site)>>', <<uCrsTempFile.stock>>)
								END

						ENDTEXT
						lcsql2 = lcsql2+lcsql+Chr(10)+Chr(13)
				Endcase
				If lccount>=50
					If  .Not. uf_gerais_actgrelha("", "", lcsql2)
						uf_perguntalt_chama("N�o foi poss�vel importar o ficheiro corretamente. Por favor valide a integridade do mesmo.", "OK", "", 16)
						regua(2)
						Return .F.
					Else
						lcok = .T.
					Endif
					lccount = 0
					lcsql2 = ""
				Endif
				Select uCrsTempFile
			Endscan
			If  .Not. Empty(lcsql2)
				If  .Not. uf_gerais_actgrelha("", "", lcsql2)
					uf_perguntalt_chama("N�o foi poss�vel importar o ficheiro corretamente. Por favor valide a integridade do mesmo e tente novamente.", "OK", "", 16)
					regua(2)
					Return .F.
				Else
					lcok = .T.
				Endif
			Endif
			regua(2)
		Endif
	Endif
	If Used("uCrsImportHistVendas")
		fecha("uCrsImportHistVendas")
	Endif
	If Used("uCrsTempFile")
		fecha("uCrsTempFile")
	Endif
	If Used("uCrsTempFileAux")
		fecha("uCrsTempFileAux")
	Endif
	If lcok
		uf_perguntalt_chama("Hist�rico importado com sucesso.", "OK", "", 64)
	Endif
	Release mylocallt
Endfunc
**
Function uf_facturacao_importhistvendascriast
	Lparameters tcfileext
	If  .Not. Used("ucrsTempFile")
		Return .F.
	Endif
	Local lctotalregistos, lccontador, lcsqlfinal
	Store 0 To lccontador
	Store "" To lcsqlfinal
	lctotalregistos = Reccount("ucrsTempFile")
	regua(0, lctotalregistos, "A verificar dados do dicionario...")
	If  .Not. Used("ucrsDadosFprod")
		TEXT TO lcsqlx TEXTMERGE NOSHOW
			select
				cnp
				,design
				,titaimdescr
				,u_marca
				,u_familia
				,u_tabiva
				,faminome = ISNULL(stfami.nome,'')
				,taxaiva = ISNULL(taxasiva.taxa,0)
				,pvporig
			from
				fprod (nolock)
				left join stfami (nolock) on fprod.u_familia = stfami.ref
				left join taxasiva (nolock) on taxasiva.codigo = fprod.u_tabiva
		ENDTEXT
		If  .Not. uf_gerais_actgrelha("", "ucrsDadosFprod", lcsqlx)
			uf_perguntalt_chama("N�o foi poss�vel verificar dados adicionais do produto no dicion�rio", "OK", "", 16)
			regua(2)
			Return .F.
		Endif
	Endif
	regua(1, 2, "A verificar dados do dicionario...")
	Update uCrsTempFile From uCrsTempFile INNER Join ucrsDadosFprod On Alltrim(ucrsDadosFprod.cnp)==Alltrim(uCrsTempFile.ref) Set uCrsTempFile.Design = Alltrim(ucrsDadosFprod.Design), uCrsTempFile.lab = Alltrim(ucrsDadosFprod.titaimdescr), uCrsTempFile.familia = Alltrim(ucrsDadosFprod.u_familia), uCrsTempFile.faminome = Alltrim(ucrsDadosFprod.faminome), uCrsTempFile.marca = Alltrim(ucrsDadosFprod.u_marca), uCrsTempFile.tabiva = ucrsDadosFprod.u_tabiva, uCrsTempFile.taxaiva = ucrsDadosFprod.taxaiva, uCrsTempFile.pvporig = ucrsDadosFprod.pvporig
	If (tcfileext=="XLS")
		Update uCrsTempFile From uCrsTempFile INNER Join ucrslojas On Alltrim(ucrslojas.site)==Alltrim(uCrsTempFile.site) Set uCrsTempFile.site_nr = ucrslojas.NO
	Else
		Update uCrsTempFile From uCrsTempFile INNER Join ucrslojas On Alltrim(ucrslojas.site)==Alltrim(mylocallt) Set uCrsTempFile.site_nr = ucrslojas.NO
	Endif
	regua(1, 3, "A verificar dados do dicionario...")
	Select uCrsTempFile
	Goto Top
	Scan For  .Not. Empty(uCrsTempFile.ref)
		lccontador = lccontador+1
		lcref = Strtran(Alltrim(uCrsTempFile.ref), Chr(39), '')
		lcdesign = Strtran(Alltrim(uCrsTempFile.Design), Chr(39), '')
		lctabiva = Iif(Empty(uCrsTempFile.tabiva), 4, uCrsTempFile.tabiva)
		lcivataxa = Iif(Empty(uCrsTempFile.taxaiva), 0, uCrsTempFile.taxaiva)
		If Type("ucrsTempFile.pvp")=='N'
			lcpvp = Iif(Empty(uCrsTempFile.pvp), uCrsTempFile.pvporig, uCrsTempFile.pvp)
		Else
			lcpvp = Iif(Empty(uCrsTempFile.pvp), uCrsTempFile.pvporig, Val(Strtran(uCrsTempFile.pvp, ',', '.')))
		Endif
		lcpct = Round(Iif(Empty(lcpvp), 0, lcpvp-(lcpvp*lcivataxa/100)), 2)
		lcstamp = uf_gerais_stamp()
		TEXT TO lcsqlx TEXTMERGE NOSHOW
			if not exists (select ref from st (nolock) where st.ref = '<<lcRef>>' AND st.site_nr = <<ucrsTempFile.site_nr>>)
			BEGIN
				INSERT INTO st (
					ststamp,ref,design,epv1,epcusto,epcpond,epcult
					,ivaincl,st.iva1incl,st.iva2incl,st.iva3incl,st.iva4incl,st.iva5incl
					,st.tabiva,stmax,ptoenc,familia,faminome,usr1
					,ousrdata,ousrhora,ousrinis,usrdata,usrhora,usrinis
					,marg1,marg2,marg3,marg4,u_lab, site_nr, u_tipoetiq
				)
				Select
					ststamp = '<<ALLTRIM(lcStamp)>>'
					,ref = '<<lcRef>>'
					,design = '<<lcDesign>>'
					,epv1 = <<lcPvp>>
					,epcusto = <<lcPCT>>
					,epcpond = 0
					,epcult = <<lcPCT>>
					,ivaincl = 1
					,iva1incl = 1
					,iva2incl = 1
					,iva3incl = 1
					,iva4incl = 1
					,iva5incl = 1
					,tabiva = <<lcTabIva>>
					,stmax = 0
					,ptoenc = 0
					,familia = case when '<<ALLTRIM(ucrsTempFile.familia)>>' = '' then '99' else '<<ALLTRIM(ucrsTempFile.familia)>>' end
					,faminome = case when '<<ALLTRIM(ucrsTempFile.faminome)>>' = '' then 'Outros' else '<<ALLTRIM(ucrsTempFile.faminome)>>' end
					,usr1 = '<<ALLTRIM(ucrsTempFile.marca)>>'
					,ousrdata = convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
					,ousrhora = convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108)
					,ousrinis = '<<m_chinis>>'
					,usrdata = convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
					,usrhora = convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108)
					,usrinis = '<<m_chinis>>'
					-- Margem Comercial
					,marg1	= case when <<lcPvp>> > 0 AND <<lcPCT>> >0 then ROUND( ((<<lcPvp>> / (<<lcIvaTaxa>>/100+1) / <<lcPCT>>) -1) * 100 ,2)
									when <<lcPvp>> >0 AND <<lcPCT>>=0 then 100
									ELSE 0 end
					-- Desconto Comercial
					,marg2	= case when <<lcPCT>> >0 and <<lcPCT>> >0
									then ROUND( ((<<lcPCT>> / <<lcPCT>>) -1) * 100 ,2)
									ELSE 0 end
					-- Margem Bruta
					,marg3	= case when <<lcPvp>> >0 and <<lcPCT>> >0
									then ROUND( ((<<lcPvp>> / (<<lcIvaTaxa>>/100+1) / <<lcPCT>>) -1) * 100 ,2)
									ELSE 0 end
					-- Benificio Bruto
					,marg4	= case when <<lcPvp>> >0 and <<lcPCT>> >0 then ROUND( (<<lcPvp>> / (<<lcIvaTaxa>>/100+1)) - <<lcPCT>> ,2)
									ELSE 0 end
			        ,u_lab = '<<ALLTRIM(ucrsTempFile.lab)>>'
			        ,site_nr = <<ucrsTempFile.site_nr>>
			        ,u_tipoetiq = 1
			END
		ENDTEXT
		lcsqlfinal = lcsqlfinal+Chr(10)+Chr(13)+lcsqlx
		If lccontador>200
			regua(1, Recno("ucrsTempFile"), "A atualizar/criar dados dos produtos: "+ASTR(Recno("ucrsTempFile"))+" de "+ASTR(lctotalregistos))
			If  .Not. Empty(lcsqlfinal)
				If  .Not. uf_gerais_actgrelha("", "", lcsqlfinal)
					uf_perguntalt_chama("Ocorreu um erro a inserir o artigo. Por favor contacte o suporte", "OK", "", 16)
					regua(2)
					Return .F.
				Endif
			Endif
			lccontador = 0
			lcsqlfinal = ""
		Endif
	Endscan
	If  .Not. Empty(lcsqlfinal)
		If  .Not. uf_gerais_actgrelha("", "", lcsqlfinal)
			uf_perguntalt_chama("Ocorreu um erro a inserir o artigo. Por favor contacte o suporte", "OK", "", 16)
			regua(2)
			Return .F.
		Endif
	Endif
	Select uCrsTempFile
Endfunc
**
Procedure uf_facturacao_alterameiospagamento
	Select ft
	uf_alterameiospag_chama(ft.u_nratend, ft.ftano)
Endproc
**
Procedure uf_facturacao_fichautente
	Select ft
	If  .Not. Empty(ft.NO)
		uf_utentes_chama(ft.NO)
	Endif
Endproc
**
Procedure uf_facturacao_dem
	uf_chk_nova_versao()
	uf_dadosdem_chama(.T.)
Endproc
**
Function uf_facturacao_addlinha
	uf_chk_nova_versao()
	If myftalteracao .And.  .Not. Empty(Alltrim(td.tiposaft))
		Return .F.
	Else
		If facturacao.eventoref==.F.
			uf_atendimento_adicionalinhafi(.T., .T.)
			Select fi
			Goto Bottom
			facturacao.pageframe1.page1.griddoc.SetFocus
		Endif
	Endif

	uf_facturacao_controlaobjectos()

	If facturacao.eventoref==.F.
		Select fi
		Goto Bottom
		facturacao.pageframe1.page1.griddoc.SetFocus
	Endif

Endfunc
**
Function uf_facturacao_remlinha
	If myftalteracao .And.  .Not. Empty(Alltrim(td.tiposaft))
		Return .F.
	Else
		facturacao.pageframe1.page1.griddoc.SetFocus
		uf_facturacao_apagalinhadoc()
		Select fi
		lnrecno = Recno("FI")
		Count For  .Not. Deleted() To lnreccount
		Try
			Goto lnrecno
		Catch
			Goto Bottom
		Endtry
		If lnreccount<=0
			facturacao.pageframe1.page1.containerlinhas.btnaddlinha.Click()
		Endif
	Endif
Endfunc
**
Function uf_facturacao_alertapic
	Lparameters lcfistamp
	If  .Not. Used("uCrsHistPic")
		Return .F.
	Endif
	Select ft
	If Empty(lcfistamp)
		Select fi.fistamp From fi INNER Join ucrsHistPic On Alltrim(fi.ref)==Alltrim(ucrsHistPic.ref) Where fi.u_epvp=ucrsHistPic.preco And ft.cdata>=ucrsHistPic.Data Into Cursor uCrsLinhasPICsValidos Readwrite
		If(Type("fi.alertapic") <> "U")
			Update fi From fi Left Join uCrsLinhasPICsValidos On fi.fistamp=uCrsLinhasPICsValidos.fistamp Set alertapic = Iif(Isnull(uCrsLinhasPICsValidos.fistamp), .T., .F.) Where Alltrim(fi.familia)=='1' .Or. Alltrim(fi.familia)=='2'
		Endif
	Else
		Select fi
		If Alltrim(fi.familia)=='1' .Or. Alltrim(fi.familia)=='2'
			Select fi.fistamp From fi INNER Join ucrsHistPic On Alltrim(fi.ref)=Alltrim(ucrsHistPic.ref) Where Alltrim(fi.fistamp)=Alltrim(lcfistamp) And fi.u_epvp=ucrsHistPic.preco And ft.fdata>=ucrsHistPic.Data Into Cursor uCrsLinhasPICsValidos Readwrite
			Select uCrsLinhasPICsValidos
			Count To lcregistos

			If(Type("fi.alertapic") <> "U")
				If lcregistos==0
					Update fi Set alertapic = .T. Where Alltrim(fi.fistamp)=Alltrim(lcfistamp)
				Else
					Update fi Set alertapic = .F. Where Alltrim(fi.fistamp)=Alltrim(lcfistamp)
				Endif
			Endif
			Select fi
			Locate For Alltrim(lcfistamp)==Alltrim(fi.fistamp)
		Endif
	Endif
Endfunc
**
Procedure uf_facturacao_demcor
	Select ft
	Select ft2
	If  .Not. Empty(Alltrim(ft2.token))
		If  .Not. Empty(Alltrim(ft.u_tlote2))
			facturacao.containercab.lote2.DisabledBackColor = Rgb(243, 181, 24)
			facturacao.containercab.lote2.DisabledForeColor = Rgb(255, 255, 255)
		Else
			If  .Not. Empty(Alltrim(ft.u_tlote))
				facturacao.containercab.lote.DisabledBackColor = Rgb(243, 181, 24)
				facturacao.containercab.lote.DisabledForeColor = Rgb(255, 255, 255)
			Endif
		Endif
	Else
		facturacao.containercab.lote.DisabledBackColor = Rgb(255, 255, 255)
		facturacao.containercab.lote.DisabledForeColor = Rgb(0, 0, 0)
		facturacao.containercab.lote2.DisabledBackColor = Rgb(255, 255, 255)
		facturacao.containercab.lote2.DisabledForeColor = Rgb(0, 0, 0)
	Endif
Endproc
**
Procedure uf_facturacao_ordenapordesigndoc
	If myftintroducao==.T. .Or. myftalteracao==.T.
		If facturacao.orderdesign==.F.
			Select fi
			Index On Upper(Design) Tag Design
			facturacao.orderdesign = .T.
		Else
			Select fi
			Index On Upper(Design) Tag Design Descending
			facturacao.orderdesign = .F.
		Endif
		lcordemdaslinhas = 10000
		Select fi
		Goto Top
		Scan
			Replace fi.lordem With lcordemdaslinhas
			lcordemdaslinhas = lcordemdaslinhas+100
		Endscan
		Select fi
		Goto Top
		facturacao.Refresh
	Endif
Endproc
**
Function uf_facturacao_calccompdata
	If Reccount("fi")=0
		Return .F.
	Endif
	If uf_gerais_getdate(.F., "MES")<>uf_gerais_getdate(ft.cdata, "MES")
		If uf_perguntalt_chama("A data da Receita refere-se a um m�s diferente do m�s atual. Pretende usar os pre�os em vigor � data seleccionada ?", "Sim", "N�o")
			Select fi
			Goto Top
			Scan
				lcsql = ''
				TEXT TO lcsql TEXTMERGE NOSHOW
						exec up_facturacao_calcCompData '<<ALLTRIM(fi.ref)>>', '<<uf_gerais_getDate(Ft.cdata,"SQL")>>'
				ENDTEXT
				uf_gerais_actgrelha("", "uCrsAuxPrecos", lcsql)
				If Reccount("uCrsAuxPrecos")>0
					Select ucrsauxprecos
					Replace fi.u_epvp With Iif(ucrsauxprecos.preco_id1>0, ucrsauxprecos.preco_id1, fi.u_epvp)
					Replace fi.u_epref With Iif(ucrsauxprecos.preco_id101>0, ucrsauxprecos.preco_id101, fi.u_epref)
					Replace fi.pvpmaxre With Iif(ucrsauxprecos.preco_id301>0, ucrsauxprecos.preco_id301, fi.pvpmaxre)
					Local tccodigo, tcref, tcdiploma
					Store '' To tccodigo, tccodigo2, tcref, tcdiploma
					tccodigo = Upper(Alltrim(ft2.u_codigo))
					tcref = Alltrim(fi.ref)
					tcdiploma = Alltrim(fi.u_diploma)



					**compart bonus
					uf_atendimento_devolveDadosBonusCompart()
					Local lcIdExt, lcTipoBonusId
					Store "" To lcTipoBonusId, lcIdExt
					Select ucrsFiBonusTemp
					lcTipoBonusId = Alltrim(ucrsFiBonusTemp.bonusTipo)
					lcIdExt = Alltrim(ucrsFiBonusTemp.idBonus)
					fecha("ucrsFiBonusTemp")



					uf_receituario_makeprecos(tccodigo, tcref, tcref, "fi", tcdiploma, .F., .T., .T.,lcTipoBonusId ,lcIdExt )
					Select fi
					Replace fi.u_comp With .T.
					If  .Not. (fi.opcao=='I')
						Replace fi.opcao With Iif(fi.u_epvp>fi.pvpmaxre, 'S', 'N')
					Endif
					Local lcorigem
					Store '' To lcorigem
					Select ft
					lcorigem = 'Doc.: '+ASTR(ft.fno)+' Ref.: '
					Select fi
					lcorigem = lcorigem+Alltrim(fi.ref)
					Select ft2
					lcorigem = lcorigem+' Plano: '+Alltrim(tccodigo)
					Select fi
					uf_gerais_registaocorrencia('RECEITU�RIO', 'Manipula��o de Comparticipa��o Autom�tica', 1, Alltrim(lcorigem), '', Alltrim(fi.fistamp))
					uf_atendimento_fiiliq(.T.)
					uf_atendimento_acttotaisft()
				Else
					uf_perguntalt_chama("N�o existem valores para a data seleccionada. Por favor contacte o Suporte. Obrigado.", "OK", "", 48)
				Endif
				Select fi
			Endscan
		Else
		Endif
	Endif
	If Used("uCrsAuxPrecos")
		fecha("uCrsAuxPrecos")
	Endif
Endfunc
**
Function uf_facturacao_reinserirreceita
	Local lcqttoriginal, lctokenoriginal, lcNrAtend
	If  .Not. Used("FT")
		Return .F.
	Endif
	Select ft

	If  .Not. Empty(Alltrim(ft.u_ltstamp)) .Or.  .Not. Empty(Alltrim(ft.u_ltstamp2))
		If uf_gerais_validapermuser(ch_userno, ch_grupo, 'Inser��o de Receita - Introduzir')
			If uf_perguntalt_chama("ATEN��O: Vai reinserir uma receita j� faturada num m�s anterior."+Chr(13)+Chr(13)+"Tem a certeza que pretende continuar?", "Sim", "N�o")

				lcNrAtend = ft.u_nratend

				Select * From ft Into Cursor uCrsFtCorrigeReceita Readwrite
				Select * From ft2 Into Cursor uCrsFt2CorrigeReceita Readwrite
				Select * From fi Into Cursor uCrsFiCorrigeReceita Readwrite
				Select * From FI2 Into Cursor uCrsFi2CorrigeReceita Readwrite
				uf_facturacao_novo(.T.)
				facturacao.containercab.nmdoc.Value = 'Inser��o de Receita'
				facturacao.containercab.nmdoc.LostFocus()
				Select * From uCrsFiCorrigeReceita Into Cursor fi Readwrite
				Select * From uCrsFi2CorrigeReceita Into Cursor FI2 Readwrite

				uf_faturacao_getCartoes(uCrsFt2CorrigeReceita.u_nbenef2,uCrsFt2CorrigeReceita.u_abrev,lcNrAtend)

				uf_facturacao_controlawklinhas()
				Select ft
				Local lcofistamp, lcftstamp
				Store '' To lcofistamp, lcftstamp
				lcftsamp = ft.ftstamp
				Local lcstamp
				Select fi
				Goto Top
				Scan
					lcqttoriginal = fi.qtt
					lcstamp = uf_gerais_stamp()
					Select fi
					lcofistamp = fi.fistamp
					If(Used("fi2"))
						Select FI2
						Go Top
						Locate For Alltrim(FI2.fistamp)  = Alltrim(lcofistamp)
						If Found()
							Replace FI2.fistamp With Alltrim(lcstamp)
						Endif
					Endif

					Select  fi
					Replace fi.fistamp With lcstamp
					Replace fi.ofistamp With lcofistamp
					Replace fi.nmdoc With ft.nmdoc
					Replace fi.fno With ft.fno
					Replace fi.ndoc With ft.ndoc
					Replace fi.u_psicont With 0
					Replace fi.u_bencont With 0
					Replace fi.qtt With lcqttoriginal
				Endscan




				Select fi
				Goto Top
				Select * From uCrsFtCorrigeReceita Into Cursor ft Readwrite
				Select ft
				Replace ft.nmdoc With fi.nmdoc
				Replace ft.tipodoc With 4
				Replace ft.fno With fi.fno
				Replace ft.ftstamp With lcftsamp
				Replace ft.ndoc With fi.ndoc
				Replace ft.site With mysite
				Replace ft.pnome With myTerm
				Replace ft.pno With mytermno
				Replace ft.cxstamp With ''
				Replace ft.cxusername With ''
				Replace ft.ssstamp With ''
				Replace ft.ssusername With ''
				Replace ft.u_nratend With ''
				Replace ft.ftid With 0
				Replace ft.u_lote With 0
				Replace ft.u_lote2 With 0
				Replace ft.u_ltstamp With ''
				Replace ft.u_ltstamp2 With ''
				Replace ft.u_nslote With 0
				Replace ft.u_nslote2 With 0
				Replace ft.u_slote With 0
				Replace ft.u_slote2 With 0
				Replace ft.u_tlote With ''
				Replace ft.u_tlote2 With ''
				Replace ft.fdata With Datetime()+(difhoraria*3600)
				Select * From uCrsFt2CorrigeReceita Into Cursor ft2 Readwrite
				Select ft2
				Replace ft2.ft2stamp With lcftsamp
				Select ft2
				Update ft2 Set ft2.u_docorig = 'reinsercao receita'
				Public myinserereceitacorrigida
				myinserereceitacorrigida = .T.
				uf_atendimento_acttotaisft()
				If Used("uCrsFtCorrigeReceita")
					fecha("uCrsFtCorrigeReceita")
				Endif
				If Used("uCrsFt2CorrigeReceita")
					fecha("uCrsFt2CorrigeReceita")
				Endif
				If Used("uCrsFiCorrigeReceita")
					fecha("uCrsFiCorrigeReceita")
				Endif
				If Used("uCrsFi2CorrigeReceita")
					fecha("uCrsFi2CorrigeReceita")
				Endif
			Endif
		Else
			uf_perguntalt_chama("O SEU PERFIL N�O PERMITE INTRODUZIR RECEITAS.", "OK", "", 48)
			Return .F.
		Endif
	Else
		uf_perguntalt_chama("ESTE DOCUMENTO N�O CONT�M RECEITA. POR FAVOR VERIFIQUE", "OK", "", 64)
	Endif
Endfunc
**
Procedure uf_faturacao_calcidade
	Local lcidade
	Store 0 To lcidade
	If uf_gerais_getdate(ft.nascimento, "SQL")<>'19000101'
		lcidade = Str(uf_gerais_getIdade(ft.nascimento))
	Else
		lcidade = Str(lcidade)
	Endif
	Select ft
	Goto Top
	If Val(lcidade)<=1 .And. uf_gerais_getdate(ft.nascimento, "SQL")<>'19000101'
		Select nascimento, Date(), Iif(Year(nascimento)=Year(Date()), Month(Date())-Month(nascimento)+1, (12*(Year(Date())-Year(nascimento)-1))+Month(Date())) As meses From ft Into Cursor uCrstempIdadeMeses Readwrite
		facturacao.pageframe1.page5.txtidade.Value = ASTR(uCrstempIdadeMeses.meses)+" Meses"
		fecha("uCrstempidademeses")
	Else
		facturacao.pageframe1.page5.txtidade.Value = Alltrim(lcidade)+" Anos"
	Endif
	facturacao.Refresh
Endproc
**
Procedure uf_facturacao_imprimedocpdf
	Release ftseguradora
	Public ftseguradora
	ftseguradora = 'DOC'
	uf_imprimirgerais_chama('FACTURACAO')
	imprimirgerais.Hide
	imprimirgerais.pageframe1.page1.chkparapdf.Value = 1
	uf_imprimirgerais_gravar()
	uf_imprimirgerais_sair()
	Release ftseguradora
Endproc
**
Function uf_facturacaoentidades_envioncsns
	Local lcPathJar , lcstampligacao, lcstampfaturasns, lcidcliente, lcano, lcmes, lcresultadofatura, lcdataaux, lcnrentidade
	Store '' To lcPathJar  , lcstampligacao, lcstampfaturasns, lcidcliente, lcresultadofatura
	Store 0 To lcano, lcmes
	If Alltrim(ucrse1.tipoempresa)<>"FARMACIA"
		uf_perguntalt_chama("FUNCIONALIDADE APENAS DISPON�VEL PARA CLIENTES DO TIPO FARM�CIA. OBRIGADO.", "Ok", "", 16)
		Return .F.
	Endif
	If uf_gerais_verifyinternet()==.F.
		uf_perguntalt_chama("ESTA FUNCIONALIDADE REQUER LIGA��O � INTERNET."+Chr(13)+"POR FAVOR VERIFIQUE A SUA LIGA��O.", "OK", "", 16)
		Return .F.
	Endif
	Select ucrse1
	If Reccount("uCrsE1")>0
		If Empty(ucrse1.ncont)
			uf_perguntalt_chama("O NIF DA FARM�CIA N�O EST� CORRETAMENTE PREENCHIDO. POR FAVOR VERIFIQUE.", "Ok", "", 16)
			Return .F.
		Endif
	Endif
	Select ucrse1
	If Empty(Alltrim(ucrse1.u_codfarm))
		uf_perguntalt_chama("O C�DIGO INFARMED DA FARM�CIA N�O EST� CORRETAMENTE PREENCHIDO. POR FAVOR VERIFIQUE.", "OK", "", 16)
		Return .F.
	Endif
	Select ucrse1
	If Empty(Alltrim(ucrse1.id_lt))
		uf_perguntalt_chama("O C�DIGO DE CLIENTE LOGITOOLS N�O EST� CORRETAMENTE PREENCHIDO. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
		Return .F.
	Endif
	lcidcliente = Alltrim(ucrse1.id_lt)
	lcsql = ''
	lcsql = ''
	TEXT TO lcsql TEXTMERGE NOSHOW
		select TOP 1 ftstamp FROM [faturacao_eletronica_med] WHERE aceite = 1 AND ftstamp = '<<ALLTRIM(ft.ftstamp)>>'
	ENDTEXT
	If  .Not. uf_gerais_actgrelha("", "uCrsAuxFTStAMP", lcsql)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VERIFICAR A INFORMA��O DO DOCUMENTO . POR FAVOR CONTACTE O SUPORTE.", "Ok", "", 16)
		regua(2)
		Return .F.
	Else
		If Reccount("uCrsAuxFTStAMP")>0
			uf_perguntalt_chama("DOCUMENTO J� ENVIADO. POR FAVOR VERIFIQUE.", "Ok", "", 16)
			regua(2)
			Return .F.
		Else
			regua(0, 4, "A enviar Fatura Eletronicamente...")
			regua(1, 1, "A configurar software para envio...")

			Local lcDigitalCert, lcNomeFicheirolt, lcPathFicheiroLt
			lcDigitalCert  = .F.


			** guardar data e n� de atendimento para o nome do ficheiro pdf **
			Select ft
			Go Top
			lcNomeFicheirolt = Alltrim(ucrse1.id_lt) +"_"+ Alltrim(ASTR(ft.ftstamp))
			lcPathFicheiroLt = Alltrim(uf_gerais_getparameter("ADM0000000015","TEXT")) + "\Cache\" + Alltrim(lcNomeFicheirolt) + ".pdf"

			Select ft
			lcDigitalCert = uf_facturacaoentidades_validaSeEntidadeCertificavel(ft.NO)

			uf_facturacao_imprimedocpdf()

			If(!Empty(lcDigitalCert))
				regua(0, 4, "A certificar o Documento")
				If(!uf_facturacao_certficarDocumento('', Alltrim(lcPathFicheiroLt), Alltrim(lcNomeFicheirolt)+".pdf",.F.))
					regua(2)
					Return .F.
				Endif
			Endif

			Local lcAbrev

			Select ft2
			lcAbrev = ft2.id_efr_externa

			Select ft
			lcPathJar = uf_facturacaoentidades_devolveNomeJavaFacturacaoEletronica(ft.NO, ft.estab, lcAbrev )

			If  .Not. File(Alltrim(uf_gerais_getparameter("ADM0000000185", "TEXT"))+ lcPathJar)
				uf_perguntalt_chama("FICHEIRO DE CONFIGURA��O N�O ENCONTRADO. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
				regua(2)
				Return .F.
			Endif
			If  .Not. Used("uCrsServerName")
				lcsql = ''
				TEXT TO lcsql TEXTMERGE NOSHOW
					Select @@SERVERNAME as dbServer
				ENDTEXT
				If  .Not. uf_gerais_actgrelha("", "ucrsServerName", lcsql)
					uf_perguntalt_chama("OCORREU UMA ANOMALIA. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
					regua(2)
					Return .F.
				Endif
			Endif
			Select ucrsservername
			lcdbserver = Alltrim(ucrsservername.dbserver)
			regua(1, 2, "A recolher informa��o para envio...")
			lcstampligacao = uf_gerais_stamp()
			lcano = Alltrim(Str(Year(ft.fdata)))
			lcmes = Alltrim(Str(Month(ft.fdata)))
			lcresultadofatura = Alltrim(Str(0))
			regua(1, 3, "A enviar informa��o...")
			If !File(Alltrim(uf_gerais_getparameter("ADM0000000185","TEXT")) + lcPathJar)
				uf_perguntalt_chama("FICHEIRO DE CONFIGURA��O N�O ENCONTRADO OU EM DESENVOLVIMENTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				regua(2)
				Return .F.
			Endif
			lcwspath = Alltrim(uf_gerais_getparameter("ADM0000000185", "TEXT"))+ lcPathJar + " "+Alltrim(lcstampligacao)+" "+Alltrim(ft.ftstamp)+" "+lcidcliente+" "+lcano+" "+lcmes+" "+'"'+Alltrim(mysite)+'"'+" "+lcresultadofatura+" 0"
			lcwspath = "javaw -jar "+lcwspath
			owsshell = Createobject("WScript.Shell")
			owsshell.Run(lcwspath, 1, .T.)
			regua(1, 4, "A verificar resposta dos Servi�os Partilhados...")
			lcsql = ''
			TEXT TO lcsql TEXTMERGE NOSHOW
				select TOP 1 * FROM faturacao_eletronica_med (nolock) WHERE ftstamp = '<<ALLTRIM(ft.ftstamp)>>'  ORDER BY data desc
			ENDTEXT
			If  .Not. uf_gerais_actgrelha("", "uCrsAuxResposta", lcsql)
				uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VERIFICAR A RESPOSTA DOS SERVI�OS PARTILHADOS. POR FAVOR CONTACTE O SUPORTE.", "Ok", "", 16)
				regua(2)
				Return .F.
			Else


				If Reccount("uCrsAuxResposta")>0
					If ucrsauxresposta.aceite=.T.
						regua(2)
						uf_perguntalt_chama("ENVIO DO DOCUMENTO EFETUADO COM SUCESSO.", "Ok", "", 16)
						Return .T.
					Else
						lcsql = ''
						fecha("uCrsAuxRespostaDet")
						TEXT TO lcsql TEXTMERGE NOSHOW
					select top 1 LEFT(response_descr,255) as resposta FROM faturacao_eletronica_med_d (nolock) WHERE id_faturacao_eletronica_med = '<<uCrsAuxResposta.id>>'  and response_code!='E002'
						ENDTEXT
						If  .Not. uf_gerais_actgrelha("", "uCrsAuxRespostaDet", lcsql)
							uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VERIFICAR A RESPOSTA DOS SERVI�OS PARTILHADOS. POR FAVOR CONTACTE O SUPORTE.", "Ok", "", 16)
							regua(2)
							Return .F.
						Else
							Local lcResp
							Select uCrsAuxRespostaDet
							Go Top
							lcResp=Alltrim(uCrsAuxRespostaDet.resposta)

							If(Empty(lcResp))
								lcResp = "OCORREU UMA ANOMALIA AO VERIFICAR A RESPOSTA DOS SERVI�OS PARTILHADOS. POR FAVOR CONTACTE O SUPORTE."
							Endif


							uf_perguntalt_chama(lcResp, "Ok", "", 16)
							fecha("uCrsAuxRespostaDet")

							regua(2)
							Return .F.
						Endif
					Endif
				Else
					regua(2)
					uf_perguntalt_chama("OCORREU UMA ANOMALIA AO ENVIAR O DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.", "Ok", "", 16)
					fecha("uCrsAuxRespostaDet")
					Return .T.
				Endif
			Endif
			fecha("uCrsAuxRespostaDet")
			regua(2)
		Endif
	Endif
	If Used("uCrsAuxFT")
		fecha("uCrsAuxFT")
	Endif
	If Used("uCrsAuxFTStAMP")
		fecha("uCrsAuxFTStAMP")
	Endif
	If Used("uCrsAuxResposta")
		fecha("uCrsAuxResposta")
	Endif
	If Used("uCrsAuxRespostaDet")
		fecha("uCrsAuxRespostaDet")
	Endif
Endfunc
**
Function uf_factura��o_validanumdoc
	Local ucrsverificafno
	If (Empty(facturacao.containercab.nmdoc.Value) .Or. facturacao.containercab.numdoc.Value==0)
		uf_facturacao_controlanumerofact(Year(Datetime()+(difhoraria*3600)))
		Return .F.
	Endif
	lcsql = ''
	TEXT TO lcsql TEXTMERGE NOSHOW
		SELECT  * FROM ft (nolock) WHERE ft.ftano = '<<YEAR(FACTURACAO.ContainerCab.docdata.value)>>'  AND  ft.nmdoc='<<FACTURACAO.ContainerCab.nmdoc.value>>' and  ft.fno=<<FACTURACAO.ContainerCab.numdoc.value>>
	ENDTEXT
	If  .Not. uf_gerais_actgrelha("", "uCrsVerificaFno", lcsql)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VERIFICAR O N�MERO DO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.", "Ok", "", 16)
		regua(2)
		Return .F.
	Else
		If Reccount("uCrsVerificaFno")>0
			uf_facturacao_controlanumerofact(Year(Datetime()+(difhoraria*3600)))
			uf_perguntalt_chama("Esse N�mero de Documento j� esta registado por favor verifique o N�mero ou edite o documento j� guardado. ", "Ok", "", 16)
			regua(2)
			Return .F.
		Endif
	Endif
Endfunc
**
Function uf_facturacao_mudafno
	Parameter lcFNo
	Local lcexecutesql, lcupdateft, lcupdateft, lcupdatefi
	Store "" To lcexecutesql, lcupdateft, lcupdateft2, lcupdatefi
	lcexecutesql = ''
	TEXT TO lcupdateft TEXTMERGE NOSHOW
		UPDATE ft SET fno =<<lcFno>> WHERE ftstamp = '<<FT.ftstamp>>'
	ENDTEXT
	TEXT TO lcupdatefi TEXTMERGE NOSHOW
		UPDATE fi SET fno =<<lcFno>> WHERE ftstamp= '<<FT.ftstamp>>'
	ENDTEXT
	lcexecutesql = lcupdateft+Chr(13)+lcupdatefi
	Return lcexecutesql
Endfunc
**
Function uf_faturacao_valida_artigo_sem_stock
	Local lcstklin, lcpassavalidacao
	Store 0 To lcstklin, lcstkst, lcstns
	Store .T. To lcpassavalidacao
	Store .F. To lcstns
	If  .Not. uf_gerais_getparameter_site('ADM0000000086', 'BOOL', mysite) .And. myftintroducao=.T.
		Select fi
		Goto Top
		Select ref, Sum(qtt) As qtt From fi Where  Not Empty(ref) And Empty(ofistamp) And partes=0 Group By ref Into Cursor ucrstotqtts Readwrite
		Select ucrstotqtts
		Goto Top
		Scan
			lcstklin = ucrstotqtts.qtt
			Local lcintbit, lcdecbit, lcconversao
			TEXT TO lcsql TEXTMERGE NOSHOW
				select stock, stns from st (nolock) where ref='<<ALLTRIM(ucrstotqtts.ref)>>' and site_nr=<<mysite_nr>>
			ENDTEXT
			If  .Not. uf_gerais_actgrelha("", "uCrsQttST", lcsql)
				uf_perguntalt_chama("OCORREU UMA ANOMALIA A GUARDAR O NOVO N�MERO DE RECEITA", "OK", 16)
				Return .F.
			Endif
			Select ucrsqttst
			lcstkst = ucrsqttst.stock
			lcstns = ucrsqttst.stns
			fecha("uCrsQttST")
			Select ucrstotqtts
			If lcstklin>lcstkst .And. lcstns=.F.
				uf_perguntalt_chama("O ARTIGO "+Alltrim(ucrstotqtts.ref)+" N�O TEM STOCK SUFICIENTE PARA FINALIZAR A VENDA. POR FAVOR RETIFIQUE!", "OK", "", 48)
				lcpassavalidacao = .F.
			Endif
		Endscan
		fecha("ucrstotqtts")
	Endif
	If lcpassavalidacao
		Return .T.
	Else
		Select fi
		Goto Top
		Return .F.
	Endif
Endfunc
**
Function uf_faturacao_export_pdf_auto
	lcsql = ""
	TEXT TO lcsql TEXTMERGE NOSHOW
		Select Top 1 nomeficheiro, tabela, temduplicados, numduplicados
		From b_impressoes (nolock)
		Where tabela='FT' and documento='<<alltrim(ft.nmdoc)>>' and idupordefeito=1
	ENDTEXT
	If  .Not. uf_gerais_actgrelha("", "templcSQL", lcsql)
		uf_perguntalt_chama("N�O FOI POSS�VEL ENCONTRAR A CONFIGURA��O DE IMPRESS�ES. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
		Return
	Endif
	If Reccount("templcSQL")>0
		lcnomeficheiro = Alltrim(templcsql.nomeficheiro)
	Else
		Return .F.
	Endif
	lcreport = Alltrim(mypath)+"\analises\"+Alltrim(lcnomeficheiro)
	Select ft
	lcnomeficheiropdf = Alltrim(Str(ft.fno))+'_'+Alltrim(Str(ft.ndoc))+'_'+Alltrim(Str(Year(ft.fdata)))+Alltrim(Str(Month(ft.fdata)))+Alltrim(Str(Day(ft.fdata)))+'_'+Alltrim(ft.nmdoc)+'.pdf'
	uf_guarda_pdf_folder(Alltrim(lcreport), Strtran(Strtran(Alltrim(lcnomeficheiropdf), '/', ''), ' ', ''), 'FI', 'c:\temp')
Endfunc
**
Function uf_facturacao_info_emb
	Return .T.
Endfunc
**
Function uf_facturacao_scanner_lin
	Lparameters uv_scanned

	If !myftalteracao And !myftintroducao
		Return .F.
	Endif
	If Empty(uv_scanned)
		Return .F.
	Endif

	uv_scanned= Strtran(Alltrim(uv_scanned), Chr(39), '')

	If Len(Alltrim(uv_scanned))>20

		Local lcDtValID, lcRefID, lcrefpaisid, lceanid, lcloteid, lcSn, lcValido
		Store '' To lcDtValID, lcRefID, lcrefpaisid, lceanid, lcloteid, lcSn
		Store .F. To lcValido

		If uf_gerais_retorna_campos_datamatrix_sql(uv_scanned)

			Select ucrdatamatrixtemp
			If ucrdatamatrixtemp.valido

				lcDtValID = uf_gerais_retornaApenasAlphaNumericos(ucrdatamatrixtemp.Data)
				lcRefID  = uf_gerais_retornaApenasAlphaNumericos(ucrdatamatrixtemp.ref)
				lcrefpaisid = ucrdatamatrixtemp.countryCode
				lceanid = ucrdatamatrixtemp.pc
				lcloteid = ucrdatamatrixtemp.lote
				lcSn = ucrdatamatrixtemp.sn
				lcValido = .T.

				Select fi_trans_info
				Go Top

				Locate For uf_gerais_compStr(lceanid, fi_trans_info.productcode) And uf_gerais_compStr(lcSn, fi_trans_info.packserialnumber)

				If Found()

					uf_perguntalt_chama("Este pack j� foi conferido.", "OK", "", 64)
					Return .F.

				Endif

			Endif

		Endif

		If Empty(lcRefID)
			**uf_gerais_notif("Medicamento conferido." + chr(13) + "Datamatrix n�o desativado.", 3000)
			**uf_gerais_registaerrolog("Erro ao ler o datamatrix no uf_atendimento_scanner:  " +  uv_scanned, "datamatrix")

			lcRefID = uf_gerais_getFromString(uv_scanned, '714', 7, .T.)
			If Len(lcRefID) <> 7
				uf_gerais_registaerrolog("Artigo n�o encontrado: " +  uv_dMatrix, "datamatrixCnp")
				uf_perguntalt_chama("ERRO NA LEITURA OU C�DIGO DE BARRAS INV�LIDO.", "OK", "", 64)
				Return .F.
			Endif

		Endif

		If !Empty(lcRefID) And Not Isnull(lcRefID)

			uv_filtro = ""

			If !Empty(lcloteid) And Not Isnull(lcloteid)
				uv_filtro = " AND ALLTRIM(fi.lote) == ALLTRIM(lcloteid)"
			Endif

			Local lcPosFiAux

			Select fi
			lcPosFiAux = Recno("fi")
			Go Top
			Locate For Alltrim(fi.ref) == Alltrim(lcRefID) &uv_filtro. &&AND !uf_gerais_getUmValor("fprod", "dispositivo_seguranca", "cnp = '" + ALLTRIM(fi.ref) + "'") Comentado pois ter� de ser feito para o Ticket SD-41008

			If Found() And !lcValido

				Select fi
				Replace fi.qtt With fi.qtt + 1

			Else

				Select fi
				Try
					Goto lcPosFiAux
				Catch
					Goto Bottom
				Endtry

				Select fi
				Go Bottom
				If !Empty(fi.ref) Or !Empty(fi.Design)
					uf_facturacao_addlinha()
				Endif

				Select fi
				Replace fi.ref With lcRefID
				uf_facturacao_eventoreffi()

				Select fi
				If Empty(fi.ref) .And. Empty(fi.Design) .And. fi.qtt=0
					uf_facturacao_remlinha()
				Endif

				If !uf_embal_conferereferencia(uv_scanned, .T.)
					uf_facturacao_remlinha()
				Endif

				If !Empty(lcloteid) And Not Isnull(lcloteid)
					Select fi
					Replace fi.lote With lcloteid
				Endif

				If !Empty(lcDtValID) And Not Isnull(lcDtValID)
					uf_gerais_posicionaFi2()
					If Alltrim(Substr(lcDtValID, 5, 2))<>'00' And !Empty(Alltrim(Substr(lcDtValID, 5, 2)))
						Select FI2
						Replace FI2.dataValidade With Date(2000+Val(Substr(lcDtValID, 1, 2)), Val(Substr(lcDtValID, 3, 2)), Val(Substr(lcDtValID, 5, 2)))
					Else
						Select FI2
						Replace FI2.dataValidade With Gomonth(Date(2000+Val(Substr(lcDtValID, 1, 2)), Val(Substr(lcDtValID, 3, 2)), 1), 1)-1
					Endif
				Endif

			Endif

			uf_atendimento_actvaloresfi()
			uf_atendimento_acttotaisft()
			facturacao.Refresh

		Else
			uf_perguntalt_chama("N�o foi possivel validar o datamatrix", "", "OK", 16)
			Return .F.
		Endif

	Else

		If uf_gerais_getUmValor("st", "count(*)", "ref = '"+Alltrim(uv_scanned)+"' and site_nr = "+ASTR(mySite_nr))=0
			uv_altref = uf_gerais_getUmValor("bc", "ref", "codigo = '"+Alltrim(uv_scanned)+"' and site_nr = "+ASTR(mySite_nr))
			If  .Not. Empty(uv_altref)
				uv_scanned = Alltrim(uv_altref)
			Endif
		Endif

		Local lcPosFiAux


		Select fi
		lcPosFiAux = Recno("fi")
		Goto Top
		Locate For Alltrim(fi.ref)==Alltrim(uv_scanned)
		If Found() &&AND !uf_gerais_getUmValor("fprod", "dispositivo_seguranca", "cnp = '" + ALLTRIM(fi.ref) + "'") Comentado pois ter� de ser feito para o Ticket SD-41008
			Select fi
			Replace fi.qtt With (fi.qtt+1)
		Else

			Select fi
			Try
				Goto lcPosFiAux
			Catch
				Goto Bottom
			Endtry

			If !Empty(fi.ref) Or !Empty(fi.Design)
				uf_facturacao_addlinha()
			Endif
			Select fi
			Replace fi.ref With uv_scanned
			uf_facturacao_eventoreffi()
			Select fi
			If Empty(fi.ref) .And. Empty(fi.Design) .And. fi.qtt=0
				uf_facturacao_remlinha()
			Endif
		Endif
		uf_atendimento_actvaloresfi()
		uf_atendimento_acttotaisft()
		facturacao.Refresh
	Endif

Endfunc
**
Function uf_faturacao_scanner
	If Empty(Alltrim(facturacao.txtscanner.Value))
		Return .F.
	Endif
	Local lcref
	Store "" To lcref
	lcref = Upper(Alltrim(facturacao.txtscanner.Value))
	Do Case
		Case Len(lcref)>=13
			TEXT TO lcsql TEXTMERGE NOSHOW
				select TOP 1 ft2stamp from ft2 where ft2.u_receita = '<<ALLTRIM(lcRef)>>' ORDER BY ft2.ousrdata DESC, ft2.ousrhora desc
			ENDTEXT
			If  .Not. uf_gerais_actgrelha("", "ucrsProcuraReceita", lcsql)
				uf_perguntalt_chama("Ocorreu um erro na procura da receita introduzida. Por favor contacte o suporte", "OK", "", 16)
				Return .F.
			Endif
			If Reccount("ucrsProcuraReceita")==0
				facturacao.txtscanner.Value = ""
				Return .F.
			Else
				Select ucrsprocurareceita
				If  .Not. Empty(ucrsprocurareceita.ft2stamp)
					uf_facturacao_chama(ucrsprocurareceita.ft2stamp)
				Endif
			Endif
	Endcase
Endfunc
**
Function uf_facturacao_retornaftstamporiginal
	Local lcsql
	lcsql = ""
	Local lcposfi, lcftstamp, lcofistamp
	Store "" To lcofistamp, lcftstamp
	If ( .Not. Used("fi"))
		Return ""
	Endif
	Select fi
	lcposfi = Recno("FI")
	Select fi
	Goto Top
	Scan For  .Not. Empty(Alltrim(fi.ofistamp))
		lcofistamp = Alltrim(fi.ofistamp)
	Endscan
	fecha("ucrsFtStampOrginal")
	If ( .Not. Empty(Alltrim(lcofistamp)))
		TEXT TO lcsql TEXTMERGE NOSHOW
			select TOP 1 ftstamp as ftstamp  from fi(nolock) where fi.fistamp= '<<ALLTRIM(lcOFiStamp)>>' ORDER BY fi.rdata desc
		ENDTEXT
		If  .Not. uf_gerais_actgrelha("", "ucrsFtStampOrginal", lcsql)
			uf_perguntalt_chama("Ocorreu um erro na procura da de venda original. Por favor contacte o suporte", "OK", "", 16)
			Return ""
		Endif
	Endif
	If (Used("ucrsFtStampOrginal"))
		lcftstamp = Alltrim(ucrsftstamporginal.ftstamp)
	Endif
	fecha("ucrsFtStampOrginal")
	Select fi
	Try
		Goto lcposfi
	Catch
		Goto Top
	Endtry
	Return lcftstamp
Endfunc
**
Function uf_facturacao_actualizacacompartexterna
	Local lcsql
	lcsql = ""

	Local lnposft2, lcposfi, lcoftstamp, lcofistamp, lctokenexterno, lcftstamp
	Store "" To lcoftstamp, lcofistamp, lctokenexterno, lcftstamp
	Select ft2
	lnposft2 = Recno("FT2")
	Select fi
	lcposfi = Recno("FI")
	If (Vartype(myinserereceitacorrigida)=='U')
		Return ""
	Endif
	If (Empty(myinserereceitacorrigida))
		Return ""
	Endif
	If ( .Not. Used("FT2"))
		Return ""
	Endif
	If ( .Not. Used("fi"))
		Return ""
	Endif
	Select ft2
	If Type("ft2.token_efectivacao_compl")=='U'
		Return ""
	Endif
	Select ft2
	Goto Top
	If (Empty(Alltrim(ft2.token_efectivacao_compl)))
		Return ""
	Endif
	Select ft2
	Goto Top
	If ( .Not. Empty(Alltrim(ft2.token_efectivacao_compl)))
		lctokenexterno = Alltrim(ft2.token_efectivacao_compl)
		lcftstamp = Alltrim(ft2.ft2stamp)
	Endif
	lcoftstamp = uf_facturacao_retornaftstamporiginal()
	If (Empty(Alltrim(lcoftstamp)))
		Return ""
	Endif

	lcsql = uf_pagamento_actualizacompartexterna(lcftstamp, lcoftstamp, lctokenexterno)
	Select fi
	Try
		Goto lcposfi
	Catch
		Goto Top
	Endtry
	Select ft2
	Try
		Goto lnposft2
	Catch
		Goto Top
	Endtry
	Return lcsql
Endfunc


Function uf_facturacao_calcular_valor_cartao
	Local lcClno, lcEstabno, lcCampID, lcValidaClPValor, lcposfi, lcRefFi, lcValCartao, lcValCartaoLimite
	Store '' To lcCampID, lcRefFi
	Store .F. To lcValidaClPValor
	Store 0 To lcValCartao, lcValCartaoLimite
	Local lcDescontouValor
	Store .F. To lcDescontouValor

	uf_facturacao_carregacampanhasut()
	uf_facturacao_carregacampanhasst()

	Select fi
	lcposfi = Recno("fi")
	Go Top
	Scan
		If Alltrim(fi.ref) != 'V000001' And !Empty(fi.ofistamp) And fi.tipodoc=1
			Replace fi.valcartao With 0
		Endif
		If Alltrim(fi.ref) == 'V000001'
			lcDescontouValor = .T.
		Endif
	Endscan

	*!*		SELECT uCrsCabVendas
	*!*		GO TOP
	*!*		SELECT * FROM uCrsCabVendas INTO CURSOR uCrsCabVendascamp readwrite

	Select ft
	uf_PAGAMENTO_retornaClienteADescontarValorCartao(ft.NO, ft.estab, ft.ftstamp)

	lcClno = ucrClNumberCardTemp.NO
	lcEstabno = ucrClNumberCardTemp.estab


	Select ucrsValcartaoList
	If Reccount("ucrsValcartaoList")>0
		Go Top
		Scan
			lcValidaClPValor = .F.
			lcCampID = "(" + Alltrim(Str(ucrsValcartaoList.Id)) + ")"
			lcValCartao = ucrsValcartaoList.valcartao/100
			lcValCartaoLimite = ucrsValcartaoList.vallimite
			Select uCrsCampanhasListaUtValor
			Locate For uCrsCampanhasListaUtValor.NO = lcClno  And uCrsCampanhasListaUtValor.estab = lcEstabno And At(lcCampID, uCrsCampanhasListaUtValor.campanhas)>0
			If Found()
				lcValidaClPValor = .T.
			Endif
			If lcValidaClPValor = .T.
				Select ref From uCrsCampanhasListaStValor Into Cursor uCrsCampanhasListaStValorAux Where At(lcCampID, uCrsCampanhasListaStValor.campanhas)>0 Readwrite
				Select fi
				Go Top
				Scan
					If !Empty(fi.ref) And Alltrim(fi.ref) != 'V000001' And fi.valcartao=0 And (fi.u_epvp < lcValCartaoLimite Or lcValCartaoLimite =0)
						Local lcFistampfaminome, lcFamiNomeSel, lctipolincab, lcReceitaTipo
						lcFamiNomeSel = ''
						lctipolincab = ''
						lcReceitaTipo = ''
						lcFistampfaminome = Alltrim(fi.ref)
						Select ucrsatendst
						Locate For Alltrim(ucrsatendst.ref) == Alltrim(lcFistampfaminome)
						If Found()
							lcFamiNomeSel  = Alltrim(ucrsatendst.faminome)
						Endif
						Select fi
						*!*							IF ALLTRIM(lcFamiNomeSel) == 'MSRM' AND uf_gerais_getParameter_site('ADM0000000135','BOOL')
						*!*								LOCAL lcLordemFiS, lcNrReceitaCamp
						*!*								lcLordemFiS = fi.lordem
						*!*								SELECT uCrsCabVendascamp
						*!*								LOCATE FOR  LEFT(ALLTRIM(STR(uCrsCabVendascamp.lordem)),2)==LEFT(ALLTRIM(STR(lcLordemFiS)),2)
						*!*								IF FOUND()
						*!*									lcReceitaTipo = ALLTRIM(uCrsCabVendascamp.receita_tipo)
						*!*									lcNrReceitaCamp = ALLTRIM(STREXTRACT(uCrsCabVendascamp.design, ':', ' -', 1, 0))
						*!*								ENDIF
						*!*							ENDIF

						If !uf_gerais_getparameter_site('ADM0000000135','BOOL') Or (uf_gerais_getparameter_site('ADM0000000135','BOOL') And lcReceitaTipo <> 'SR' And Alltrim(lcFamiNomeSel) == 'MSRM')

							If !uf_gerais_getparameter('ADM0000000319','BOOL')
								If !uf_gerais_getparameter('ADM0000000320','BOOL')
									lcRefFi = Alltrim(fi.ref)
									Select uCrsCampanhasListaStValorAux
									Locate For Alltrim(uCrsCampanhasListaStValorAux.ref) == Alltrim(lcRefFi)
									If Found()
										Select fi
										Replace fi.valcartao With fi.valcartao + (fi.etiliquido * lcValCartao)
									Endif
								Else
									If (Empty(fi.campanhas) And fi.desconto=0 And fi.u_descval=0) Or lcDescontouValor = .T.
										lcRefFi = Alltrim(fi.ref)
										Select uCrsCampanhasListaStValorAux
										Locate For Alltrim(uCrsCampanhasListaStValorAux.ref) == Alltrim(lcRefFi)
										If Found()
											Select fi
											Replace fi.valcartao With fi.valcartao + (fi.etiliquido * lcValCartao)
										Endif
									Endif
								Endif
							Else
								If !uf_gerais_getparameter('ADM0000000320','BOOL')
									Local lcFiLordemPontos
									lcFiLordemPontos = fi.lordem
									**SELECT uCrsCabVendascamp
									**GO TOP
									**SELECT design FROM uCrsCabVendascamp WHERE LEFT(ALLTRIM(STR(uCrsCabVendascamp.lordem)),2)==LEFT(ALLTRIM(STR(lcFiLordemPontos )),2) INTO CURSOR UcrsChkSusp
									**SELECT UcrsChkSusp
									**IF AT('*SUSPENSA*', UcrsChkSusp.design)=0
									Select fi
									lcRefFi = Alltrim(fi.ref)
									Select uCrsCampanhasListaStValorAux
									Locate For Alltrim(uCrsCampanhasListaStValorAux.ref) == Alltrim(lcRefFi)
									If Found()
										Select fi
										Replace fi.valcartao With fi.valcartao + (fi.etiliquido * lcValCartao)
									Endif
									**ENDIF
									**fecha("UcrsChkSusp")
								Else
									If Empty(fi.campanhas)
										Local lcFiLordemPontos
										lcFiLordemPontos = fi.lordem
										**SELECT uCrsCabVendascamp
										**GO TOP
										**SELECT design FROM uCrsCabVendascamp WHERE LEFT(ALLTRIM(STR(uCrsCabVendascamp.lordem)),2)==LEFT(ALLTRIM(STR(lcFiLordemPontos )),2) INTO CURSOR UcrsChkSusp
										**SELECT UcrsChkSusp
										**IF AT('*SUSPENSA*', UcrsChkSusp.design)=0
										Select fi
										If (Empty(fi.campanhas) And fi.desconto=0 And fi.u_descval=0) Or lcDescontouValor = .T.
											lcRefFi = Alltrim(fi.ref)
											Select uCrsCampanhasListaStValorAux
											Locate For Alltrim(uCrsCampanhasListaStValorAux.ref) == Alltrim(lcRefFi)
											If Found()
												Select fi
												Replace fi.valcartao With fi.valcartao + (fi.etiliquido * lcValCartao)
											Endif
										Endif
										**ENDIF
										**fecha("UcrsChkSusp")
									Endif
								Endif
							Endif
						Endif
						Select fi
					Endif
				Endscan
			Endif
		Endscan
		Select ucrsValcartaoList
	Endif

	*!*		IF USED("uCrsCabVendascamp")
	*!*			fecha("uCrsCabVendascamp")
	*!*		ENDIF

	If(Used("ucrClNumberCardTemp"))
		fecha("ucrClNumberCardTemp")
	Endif


	fecha("uCrsCampanhasListaStValorAux")

	Select fi
	Try
		Select fi
		Go lcposfi
	Catch
		Go Bottom
	Endtry

Endfunc


Function uf_facturacao_carregacampanhasut
	lcsql = ''
	TEXT TO lcsql TEXTMERGE NOSHOW
		exec up_campanhas_listar '<<ALLTRIM(mysite)>>'
	ENDTEXT
	If  .Not. uf_gerais_actgrelha("", "ucrsCampanhasList", lcsql)
		uf_perguntalt_chama("Ocorreu um problema ao verificar as campanhas.", "OK", "", 16)
		Return .F.
	Endif
	lcsql = ''
	TEXT TO lcsql TEXTMERGE NOSHOW
		exec up_valcartao_listar '<<ALLTRIM(mysite)>>'
	ENDTEXT
	If  .Not. uf_gerais_actgrelha("", "ucrsValcartaoList", lcsql)
		uf_perguntalt_chama("Ocorreu um problema ao verificar as campanhas de valor em cart�o.", "OK", "", 16)
		Return .F.
	Endif
	lcsql = ''
	TEXT TO lcsql TEXTMERGE NOSHOW
		select no, estab, cast(campanhas as varchar(4000)) as campanhas from campanhas_ut_lista (nolock) where campanhas<>''
	ENDTEXT
	If  .Not. uf_gerais_actgrelha("", "uCrsCampanhasListaUt", lcsql)
		uf_perguntalt_chama("Ocorreu uma anomalia a verificar utentes associados �s campanhas.", "OK", "", 16)
		Return .F.
	Endif
	Select * From uCrsCampanhasListaUt Into Cursor uCrsCampanhasListaUtValor Readwrite
	lcsql = ''
	TEXT TO lcsql TEXTMERGE NOSHOW
		Select
			id = '(' + RTRIM(LTRIM(STR(id))) + ')'
		from
			campanhas (nolock)
		where
			descv = 0
			AND desconto = 0
			AND valorValeDireto = 0
			and campanhas.id not in (select distinct campanhas_id from campanhas_pOfertas (nolock))
			and convert(date,getdate()) between campanhas.dataInicio and campanhas.dataFim
	ENDTEXT
	If  .Not. uf_gerais_actgrelha("", "uCrsCampanhasListaUtSoPontos", lcsql)
		uf_perguntalt_chama("Ocorreu uma anomalia a verificar utentes associados �s campanhas.", "OK", "", 16)
		Return .F.
	Endif
Endfunc

Function uf_facturacao_carregacampanhasst
	Local lcsql
	TEXT TO lcsql TEXTMERGE NOSHOW
		exec up_campanhas_configSt '<<ALLTRIM(mysite)>>'
	ENDTEXT
	If  .Not. uf_gerais_actgrelha("", "ucrsCampanhasConfigSt", lcsql)
		uf_perguntalt_chama("Ocorreu um problema ao verificar defini��o das campanhas.", "OK", "", 16)
		Return .F.
	Endif
	TEXT TO lcsql TEXTMERGE NOSHOW
		select ref, campanhas from campanhas_st_lista (nolock)
	ENDTEXT
	If  .Not. uf_gerais_actgrelha("", "uCrsCampanhasListaSt", lcsql)
		uf_perguntalt_chama("Ocorreu uma anomalia a verificar produtos associados �s campanhas.", "OK", "", 16)
		Return .F.
	Endif
	Select * From uCrsCampanhasListaSt Into Cursor uCrsCampanhasListaStValor Readwrite
Endfunc

**
Function uf_faturacao_selclientepsico
	Lparameters lcno, lcestab
	If Empty(lcno) .Or.  .Not. Type("lcNO")=="N"
		lcno = 200
	Endif
	If Empty(lcestab) .Or.  .Not. Type("lcEstab")=="N"
		lcestab = 0
	Endif
	If  .Not. uf_gerais_actgrelha("", 'uCrsAtendCLPsico', 'Exec up_touch_dadosCliente '+ASTR(lcno)+','+ASTR(lcestab)+'')
		uf_perguntalt_chama("Ocorreu uma anomalia a obter os dados do Cliente.", "OK", "", 16)
		Return .F.
	Else
		If Reccount("uCrsAtendCLPSICO")>0

			Select uCrsAtendCLPSICO
			Go Top

			Local lcidade
			lcidade = 0
			If uf_gerais_getdate(uCrsAtendCLPSICO.nascimento, "SQL")<>'19000101'

				lcidade =uf_gerais_getIdade(uCrsAtendCLPSICO.nascimento)
			Else
				lcidade = lcidade
			Endif

			Select dadospsico
			Go Top


			Replace dadospsico.u_nmutavi	With Alltrim(uCrsAtendCLPSICO.Nome)
			Replace dadospsico.u_moutavi	With Alltrim(uCrsAtendCLPSICO.morada)
			Replace dadospsico.u_cputavi	With Alltrim(uCrsAtendCLPSICO.codpost)
			Replace dadospsico.u_ndutavi	With Alltrim(uCrsAtendCLPSICO.bino)
			Replace dadospsico.u_ddutavi	With uCrsAtendCLPSICO.u_ddutavi
			Replace dadospsico.u_idutavi 	With lcidade
			Replace dadospsico.nascimento 	With uCrsAtendCLPSICO.nascimento


		Endif
	Endif
	If Used("uCrsAtendCLPsico")
		fecha("uCrsAtendCLPsico")
	Endif
	facturacao.Refresh()
Endfunc
**
Function uf_facturacao_alteraIdadePsico
	Local lcidade
	lcidade = 0
	lcidade =uf_gerais_getIdade(dadospsico.nascimento)

	Select dadospsico
	Go Top
	Replace dadospsico.u_idutavi 	With lcidade

	facturacao.Refresh()
Endfunc
**
Function uf_faturacao_checkLenEmb
	Lparameters uv_plano, uv_codEmb

	If Empty(uv_plano)
		Return .F.
	Endif

	TEXT TO msel TEXTMERGE NOSHOW
        select maxlenEmb, minlenEmb from cptpla(nolock) where codigo = '<<ALLTRIM(uv_plano)>>' AND inactivo = 0
	ENDTEXT

	If !uf_gerais_actgrelha("", "uc_lenEmb", msel)
		uf_perguntalt_chama("Ocorreu uma anomalia a verificar o plano de comparticipa��o.", "OK", "", 16)
		Return .F.
	Endif

	Select uc_lenEmb
	If Reccount("uc_lenEmb") = 0
		uf_perguntalt_chama("Ocorreu uma anomalia a verificar o plano de comparticipa��o.", "OK", "", 16)
		Return .F.
	Endif

	If uc_lenEmb.maxlenEmb <> 0 And uc_lenEmb.minlenEmb <> 0 And uc_lenEmb.minlenEmb = uc_lenEmb.maxlenEmb And (Len(uv_codEmb) > uc_lenEmb.maxlenEmb Or Len(uv_codEmb) < uc_lenEmb.minlenEmb)

		uf_perguntalt_chama("O C�digo de Embalagem tem de conter " + ASTR(uc_lenEmb.maxlenEmb) + " caracteres.", "OK", "", 16)
		Return .F.

	Endif

	If uc_lenEmb.maxlenEmb <> 0 And Len(uv_codEmb) > uc_lenEmb.maxlenEmb

		uf_perguntalt_chama("O C�digo de Embalagem n�o pode conter mais que " + ASTR(uc_lenEmb.maxlenEmb) + " caracteres.", "OK", "", 16)
		Return .F.

	Endif

	If uc_lenEmb.minlenEmb <> 0 And Len(uv_codEmb) < uc_lenEmb.minlenEmb

		uf_perguntalt_chama("O C�digo de Embalagem tem que conter pelo menos " + ASTR(uc_lenEmb.minlenEmb) + " caracteres.", "OK", "", 16)
		Return .F.

	Endif

	Return .T.

Endfunc

Function uf_faturacao_checkQttMaxPla
	Lparameters uv_plano, uv_qtt

	If Empty(uv_plano)
		Return .F.
	Endif

	uv_qttMax = uf_gerais_getUmValor("cptpla", "qttMaxLin", "codigo = '" + Alltrim(uv_plano) + "'")

	If uv_qttMax <> 0 And uv_qtt > uv_qttMax
		uf_perguntalt_chama("N�o � permitida a venda de mais do que uma quantidade para este plano. Tem de inserir um c�digo embalagem por cada linha", "OK", "", 16)
		Return .F.
	Endif

	Return .T.

Endfunc

Function uf_faturacao_compart_valida

	Local lctemcompart, lcstampcompart, lcstamplin, lcNrCartao, lcentidade, lcstampcab, lclinabrev, lclinabrev2, lccodacesso, lcprogramid, lcnrreceita
	Store '' To lcnrreceita
	Store .F. To lctemcompart, lcCompartCovid

	Select u_codigo, u_codigo2 From ft2 With (Buffering = .T.) Into Cursor uc_comparts

	uv_ecompart = .F.
	uv_codCompart = ""

	If !Empty(uc_comparts.u_codigo)
		uv_ecompart = uf_gerais_getUmValor("cptpla", "e_compart", "codigo = '" + Alltrim(uc_comparts.u_codigo) + "'")
		uv_codCompart = uc_comparts.u_codigo
	Endif

	If !uv_ecompart And !Empty(uc_comparts.u_codigo2)
		uv_ecompart = uf_gerais_getUmValor("cptpla", "e_compart", "codigo = '" + Alltrim(uc_comparts.u_codigo2) + "'")
		uv_codCompart = uc_comparts.u_codigo2
	Endif




	If uv_ecompart

		Select ft2
		If Empty(ft2.c2codpost)
			uf_perguntalt_chama("ATEN��O!"+Chr(13)+"TEM DE INSERIR O N� DO CART�O NA RECEITA COMPARTICIPADA.", "OK", "", 48)
			Return .F.
		Endif

		lcstampcompart = uf_gerais_stamp()
		If  .Not. Used("uCrsLinPresc")
			Create Cursor uCrsLinPresc (fistamp C(30), ncartao C(20), entidade N(6), ftstamp C(25), abrev C(25), abrev2 C(25), codacesso C(10), programid C(30), nrreceita C(30))
		Else
			Select uCrsLinPresc
			Delete All
		Endif

		Select fi
		lcstamplin = fi.fistamp
		lcNrCartao = Alltrim(ft2.c2codpost)
		lcentidade = uf_gerais_getUmValor("cptpla", "CONVERT(INT,REPLACE(cptorgstamp, 'ADM', ''))", "codigo = '" + Alltrim(uv_codCompart) + "'")
		lcAbrev = ft2.u_abrev
		lcabrev2 = ft2.u_abrev2
		lccodacesso = Alltrim(Str(Rand()*1000000))
		lcprogramid = Alltrim(uf_gerais_getUmValor("cptpla", "codExt", "codigo = '" + Alltrim(uv_codCompart) + "'"))
		lcnrreceita = Alltrim(ft2.u_receita)
		lcftstamp = Alltrim(ft2.ft2stamp)

		uf_geraCursorReceitaValidar()

		If (uf_faturacao_validasevalidacompartexterna())

			uf_faturacao_compart_insert_ft_compart(Alltrim(lcstampcompart), Alltrim(lcftstamp), lcentidade, Alltrim(lcNrCartao), 1, Alltrim(lccodacesso), Alltrim(lcAbrev), Alltrim(lcprogramid), Alltrim(lcnrreceita))

			uf_faturacao_compart_insert_fi_compart(Alltrim(lcstampcompart), Alltrim(lcAbrev), Alltrim(lcabrev2), Alltrim(lcNrCartao), Alltrim(lccodacesso), Alltrim(lcnrreceita), Alltrim(lcftstamp))

			If !uf_faturacao_presc_com(Alltrim(lcstampcompart))
				Return .F.
			Endif

		Endif
	Endif
	Return .T.
Endfunc

Function uf_faturacao_validasevalidacompartexterna

	uv_found = .F.

	Select fi
	Goto Top
	Scan For !Empty(fi.ofistamp)
		uv_found = .T.
		lcsql = ""
		TEXT TO lcsql TEXTMERGE NOSHOW
         select u_comp from fi (nolock) where fistamp='<<ALLTRIM(fi.ofistamp)>>'
		ENDTEXT
		If uf_gerais_actgrelha("", 'uCrsVerifCompLin', lcsql)
			Select uCrsVerifCompLin
			If uCrsVerifCompLin.u_comp = .F.
				Return .T.
			Endif
		Else
			uf_perguntalt_chama("Ocorreu uma anomalia a verificar a comparticipa��o da linha.", "OK", "", 16)
		Endif

		Select fi
	Endscan

	If uv_found
		Return .F.
	Else
		Return .T.
	Endif

Endfunc

Function uf_faturacao_presc_com
	Lparameters lntokencom
	Local lctokencom, lcstampficompart, lcreturncontributedvalue, lcreturncontributedpercentage
	Store '&' To lcadd, lcstampficompart
	Store 0 To lcreturncontributedvalue, lcreturncontributedpercentage
	lctokencom = lntokencom
	lcnomejar = "ltsCompartCli.jar"
	lcwsdir = Alltrim(uf_gerais_getparameter("ADM0000000185", "TEXT"))+'\ltsCompart'
	regua(1, 2, "A efetuar a valida��o da comparticipa��o por favor aguarde.")
	lcwspath = Alltrim(uf_gerais_getparameter("ADM0000000185", "TEXT"))+'\ltsCompart\'+Alltrim(lcnomejar)+' "--TOKEN='+Alltrim(lctokencom)+'"'+' "--IDCL='+Alltrim(Upper(sql_db))+'"'
	lcwspath = "cmd /c cd /d "+lcwsdir+" "+lcadd+lcadd+" javaw -jar "+lcwspath
	owsshell = Createobject("WScript.Shell")
	owsshell.Run(lcwspath, 0, .T.)
	lcsql = ''
	TEXT TO lcsql TEXTMERGE NOSHOW
		select top 1
			token
			, ftstamp
			, code
			,cast(message as varchar(240)) as message
			, ai
			, version
			, ousrdata
		from
			ft_compart_result (nolock)
		where
			token = '<<ALLTRIM(lcTokenCom)>>'
		order by
			code desc
	ENDTEXT
	If  .Not. uf_gerais_actgrelha("", "uCrsAuxCompart", lcsql)
		uf_perguntalt_chama("N�o foi possivel validar a comparticipa��o. Por favor contacte o suporte.", "OK", "", 32)
		regua(2)
		Return .F.
	Else

		Select ucrsauxcompart
		If ucrsauxcompart.Code<>200

			uv_message = "N�o foi possivel validar as linhas de comparticipa��o."

			If !Empty(ucrsauxcompart.Message)
				uv_message = uv_message + Chr(13) + "ERRO: " + Alltrim(ucrsauxcompart.Message)
			Endif

			uf_perguntalt_chama(Alltrim(uv_message), "OK", "", 32)
			regua(2)
			Return .F.

		Else
			lcsql = ''
			TEXT TO lcsql TEXTMERGE NOSHOW
				select * from fi_compart (nolock) where token = '<<ALLTRIM(lcTokenCom)>>' order by ousrdata
			ENDTEXT
			If  .Not. uf_gerais_actgrelha("", "uCrsAuxCompartFi", lcsql)
				uf_perguntalt_chama("N�o foi possivel validar as linhas comparticipa��o. Por favor contacte o suporte.", "OK", "", 32)
				regua(2)
				Return .F.
			Else
				Select ucrsauxcompartfi
				Goto Top
				Scan
					lcstampficompart = Alltrim(ucrsauxcompartfi.fistamp)
					lcreturncontributedvalue = ucrsauxcompartfi.returncontributedvalue
					lcreturncontributedpercentage = ucrsauxcompartfi.returncontributedpercentage
					Select fi
					Goto Top
					Scan
						If lcstampficompart=Alltrim(fi.fistamp)
							Do Case
								Case fi.Comp=0 .And. fi.comp_2=0
									Replace fi.Comp With lcreturncontributedpercentage
									Replace fi.comp_diploma With lcreturncontributedpercentage
									Replace fi.u_ettent1 With lcreturncontributedvalue
									Replace fi.u_txcomp With 100-lcreturncontributedpercentage
								Case fi.Comp=0 .And. fi.comp_2<>0 .And. lcreturncontributedpercentage=100
									Replace fi.Comp With 100-fi.comp_2
									Replace fi.comp_diploma With 100-fi.comp_2
									Replace fi.u_ettent1 With lcreturncontributedvalue
									Replace fi.u_txcomp With 100-lcreturncontributedpercentage
								Case fi.Comp=0 .And. fi.comp_2<>0 .And. lcreturncontributedpercentage<>100
									Replace fi.Comp With (100-fi.comp_2)*(lcreturncontributedpercentage/100)
									Replace fi.comp_diploma With (100-fi.comp_2)*(lcreturncontributedpercentage/100)
									Replace fi.u_ettent1 With lcreturncontributedvalue
									Replace fi.u_txcomp With 100-(fi.comp_2+fi.Comp)
								Otherwise
							Endcase
							If At("Vale", Alltrim(fi.Design))=0
								If fi.desconto=0
									Replace fi.epv With (fi.u_epvp-((fi.u_ettent1+fi.u_ettent2)/fi.qtt))
									If fi.u_descval=0
										Replace fi.etiliquido With ((fi.u_epvp*fi.qtt)-(fi.u_ettent1+fi.u_ettent2))
									Else
										Replace fi.etiliquido With Round((fi.epv*fi.qtt)-fi.u_descval,2)
									Endif
								Else
									If lcreturncontributedpercentage=100
										Replace fi.etiliquido With 0
										Replace fi.epv With 0
									Else
										**REPLACE fi.etiliquido WITH (((fi.u_epvp*fi.qtt)-(fi.u_ettent1))-(((fi.u_epvp*fi.qtt)-(fi.u_ettent1))*(fi.desconto/100)))-fi.u_ettent2
										Replace fi.epv With (fi.u_epvp-((fi.u_ettent1+fi.u_ettent2)/fi.qtt))
										Replace fi.etiliquido With Round((fi.epv*fi.qtt)-((fi.epv*fi.qtt)*(fi.desconto/100)),2)
									Endif
								Endif
							Endif
						Endif
					Endscan
					Select ucrsauxcompartfi
				Endscan
				**uf_atendimento_infototais()
			Endif
			TokenCompart = Alltrim(lctokencom)

		Endif
	Endif
	regua(2)
	fecha("uCrsAuxCompartFi")
	fecha("uCrsAuxCompart")
Endfunc

Function uf_faturacao_compart_insert_fi_compart
	Lparameters lntoken, lnabrev, lnabrev2, lnncartaoaux, lncodacesso, lnReceita, lnftstamp
	Local lctoken, lcvalsns, lclordem, lcabrevaux, lcabrevaux2, lcncartaoaux, lccodacesso, lcreceita
	lctoken = lntoken
	lcvalsns = 0
	lcabrevaux = lnabrev
	lcabrevaux2 = lnabrev2
	lclordem = ''
	lcncartaoaux = lnncartaoaux
	lccodacesso = lncodacesso
	lcreceita = lnReceita

	Select fi
	Goto Top
	Scan For !Empty(fi.ref)
		lcvalsns = 0
		If Alltrim(lcabrevaux)='SNS'
			lcvalsns = fi.u_ettent1
			If fi.comp_2<>0
				Replace fi.comp_2 With 0
				Replace fi.comp_diploma2 With 0
				Replace fi.u_ettent2 With 0
				Replace fi.u_txcomp With fi.comp_diploma
				Replace fi.etiliquido With ((fi.u_epvp*fi.qtt)-(fi.u_ettent1+fi.u_ettent2))
				Replace fi.epv With (fi.u_epvp-(fi.u_ettent1+fi.u_ettent2))
			Endif
		Endif
		If Alltrim(lcabrevaux2)='SNS'
			lcvalsns = fi.u_ettent2
			If fi.Comp<>0
				Replace fi.Comp With 0
				Replace fi.comp_diploma With 0
				Replace fi.u_ettent1 With 0
				Replace fi.u_txcomp With fi.comp_diploma_2
				If At("Vale", Alltrim(fi.Design))=0
					If fi.desconto=0
						If fi.u_descval=0
							Replace fi.etiliquido With ((fi.u_epvp*fi.qtt)-(fi.u_ettent1+fi.u_ettent2))
						Else
							Replace fi.etiliquido With Round((fi.epv*fi.qtt)-fi.u_descval,2)
						Endif
					Else
						Replace fi.etiliquido With Round((fi.epv*fi.qtt)-((fi.epv*fi.qtt)*(fi.desconto/100)),2)
					Endif
					Replace fi.epv With (fi.u_epvp-(fi.u_ettent1+fi.u_ettent2))
				Endif
			Endif
		Endif
		TEXT TO lcsql TEXTMERGE NOSHOW
      insert into fi_compart (
         token
         ,fistamp
         ,ftstamp
         ,hasPrescription
         ,lineNumber
         ,pvpValue
         , prescriptionExceptions
         , prescriptionDispatch
         , prescriptionPVPValue
         , ref
         , quantity
         , snsValue
         , uncontributedValue
         , ousrdata
         , codacesso
         , codCNP
         , percIva
         , ivaincl
         , nrreceita
      )values(
         '<<ALLTRIM(lctoken)>>'
         ,'<<ALLTRIM(fi.fistamp)>>'
         ,'<<ALLTRIM(lnftstamp)>>'
         ,1
         ,<<fi.lordem>>
         ,<<fi.epvori>>
         ,'<<ALLTRIM(fi.lobs3)>>'
         ,''
         ,<<fi.epv>>
         ,'<<ALLTRIM(fi.ref)>>'
         ,<<fi.qtt>>
         ,<<lcValSNS>>
         ,<<((fi.u_epvp*fi.qtt)-(fi.u_ettent1+fi.u_ettent2))>>
         ,dateadd(HOUR, <<difhoraria>>, getdate())
         ,'<<ALLTRIM(lcCodacesso)>>'
         ,(select codCNP from st (nolock) where ref='<<ALLTRIM(fi.ref)>>' and site_nr=<<mysite_nr>>)
         ,<<fi.iva>>
         ,<<IIF(fi.ivaincl,1,0)>>
         ,'<<ALLTRIM(lnReceita)>>'
      )
		ENDTEXT
		If  .Not. uf_gerais_actgrelha("", "", lcsql)
			uf_perguntalt_chama("N�o foi possivel proceder inserir linhas na comparticipa��o. Contacte o suporte.", "OK", "", 32)
			Return .F.
		Endif
		Select fi
	Endscan
Endfunc

Function uf_faturacao_compart_insert_ft_compart
	Lparameters lntoken, lnftstamp, lnefrid, lncardno, lntype, lncodacesso, lnabrev, lnprogramid, lnnrreceita
	Local lctoken, lcftstamp, lcefrid, lccardno, lntype, lccodacesso, lcabrevinsurance, lccodextinsurance, lcAbrev, lcprogramid, lcnrreceita
	lctoken = lntoken
	lcftstamp = lnftstamp
	lcefrid = lnefrid
	lccardno = lncardno
	lctype = lntype
	lccodacesso = lncodacesso
	lcAbrev = lnabrev
	lcnrreceita = lnnrreceita
	If (Empty(lnprogramid))
		lcprogramid = ""
	Else
		lcprogramid = lnprogramid
	Endif
	TEXT TO lcsql TEXTMERGE NOSHOW
		select abrev, codExt, efrID from cptorg (nolock) where abrev='<<lcAbrev>>'
	ENDTEXT
	uf_gerais_actgrelha("", "ucrsCompartInf", lcsql)
	lcabrevinsurance = Alltrim(ucrscompartinf.abrev)
	lccodextinsurance = Alltrim(ucrscompartinf.codext)
	lcefrid = Alltrim(ucrscompartinf.efrid)
	fecha("ucrsCompartInf")
	TEXT TO lcsql TEXTMERGE NOSHOW
		insert into ft_compart (
			token
			,siteno
			,efrid
			,ftstamp
			,cardnumber
			,type
			,contributionCorrelationId
			, ousrdata
			, codacesso
			, currency
			, abrevInsurance
			, codExtInsurance
			, ousrUser
			, programId
			, nrreceita
		)values(
			'<<ALLTRIM(lctoken)>>'
			,<<uCrsE1.Siteno>>
			,'<<ALLTRIM(lcEfrID)>>'
			,'<<ALLTRIM(lcFtstamp)>>'
			,'<<ALLTRIM(lcCardNo)>>'
			,<<lcType>>
			,'<<ALLTRIM(lcCardNo)>>'
			, dateadd(HOUR, <<difhoraria>>, getdate())
			,'<<ALLTRIM(lcCodacesso)>>'
			,'<<ALLTRIM(uf_gerais_getParameter("ADM0000000260","text"))>>'
			, '<<ALLTRIM(lcabrevInsurance)>>'
			, '<<ALLTRIM(lccodExtInsurance)>>'
			, '<<ALLTRIM(m_chinis)>>'
			, '<<ALLTRIM(lcProgramId)>>'
			, '<<ALLTRIM(lcnrreceita)>>'
		)
	ENDTEXT
	If  .Not. uf_gerais_actgrelha("", "", lcsql)
		uf_perguntalt_chama("N�o foi possivel proceder inserir linhas na comparticipa��o. Contacte o suporte.", "OK", "", 32)
		Return .F.
	Endif
Endfunc

Function uf_facturacao_linhasdem

	Select fi
	Count For fi.dem To uv_count

	If uv_count <> 0

		uv_refs = ""

		Select * From fi With (Buffering = .T.) Where !Empty(ref) And !Empty(qtt) And !dem   Into Cursor uc_tmpFI

		Select uc_tmpFI
		Go Top
		Scan

			uv_refs = Alltrim(uv_refs) + Iif(!Empty(uv_refs), ";" + Chr(13), '') + Alltrim(uc_tmpFI.ref)

		Endscan

		If !Empty(uv_refs)
			uv_msg = 'Os seguintes productos foram adicionados manualmente a vendas de dispensa eletr�nica: ' + Chr(13) + Alltrim(uv_refs)
			uf_perguntalt_chama(uv_msg, "OK", "", 48)
			Return .F.
		Endif

	Endif

	Return .T.

Endfunc

Function uf_facturacao_chamaRegSinave

	Select ft
	If Empty(ft.Nome) Or Empty(ft.NO) Or Empty(ft2.u_codigo) Or (Empty(ft2.u_receita) Or Len(Alltrim(ft2.u_receita)) < 19)
		Return .F.
	Endif

	If !Empty(ft.u_lote) And Empty(ft.u_ltstamp)
		uf_perguntalt_chama("N�o pode fazer registo Sinave para uma receita anulada!", "OK", "", 32)
		Return .F.
	Endif

	Public myRefCovid, myChamaSinave, myNrRecSinave
	myRefCovid = '1888889'
	myChamaSinave = 'N�o chama'
	myNrRecSinave = ''

	lcsql = ""
	TEXT TO lcsql TEXTMERGE NOSHOW
            exec up_facturacao_existeCl '<<Alltrim(ft.nome)>>', '<<ASTR(ft.no)>>', '<<ft.estab>>'
	ENDTEXT
	If !uf_gerais_actgrelha("", "uCrsAtendCL", lcsql)
		uf_perguntalt_chama("N�o foi poss�vel carregar os dados do Utente!", "OK", "", 32)
		Return .F.
	Endif

	uf_comunicasinave_chama(ft.NO,ft2.u_codigo, ft.ftstamp, ft.u_nratend, ft2.u_receita)

Endfunc


Procedure uf_facturacao_reenviarFatura

	uf_gerais_get_efrId(ft.NO)

	If !Used("ucrTempLCefrID")
		uf_perguntalt_chama("N�O FOI POSS�VEL OBTER O ID DA SEGURADORA. POR FAVOR CONTACTE O SUPORTE. OBRIGADO.","OK","",16)
	Else
		Local lcFs2stamp,lcNrelegibilidade, lcStamporigproc, lcefrid, lcAbrev
		Select ft2
		lcFs2stamp = Alltrim(ft2.ft2stamp)
		lcNrelegibilidade= Alltrim(ft2.nrelegibilidade)
		lcStamporigproc= Alltrim(ft2.stamporigproc)

		If(Empty(lcNrelegibilidade))
			uf_perguntalt_chama("N�O FOI POSS�VEL OBTER O ID DA ELEGIBILIDADE.","OK","",16)
			regua(2)
			fecha("ucrTempLCefrID")
			fecha("docdescr")
			Return .F.
		Endif


		Select ucrTempLCefrID
		lcefrid = Alltrim(ucrTempLCefrID.efrid)
		lcAbrev= Alltrim(ucrTempLCefrID.abrev)

		TEXT TO lcSQLdescr NOSHOW TEXTMERGE
                select td.tiposaft+ ' '+ltrim(rtrim(str(ft.ndoc)))+ltrim(rtrim(str(ft.ftano)))+'/'+ltrim(rtrim(str(fno))) as docdescr from ft (nolock)
                inner join td (nolock) on ft.ndoc=td.ndoc where ftstamp='<<ALLTRIM(ft.FtStamp)>>'
		ENDTEXT

		If !uf_gerais_actgrelha("","docdescr",lcSQLdescr)
			uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VALIDAR O N� DO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE. OBRIGADO.","OK","",16)
		Endif

		Local lcResult
		lcResult= uf_compart_envia(lcStamporigproc, Alltrim(docdescr.docdescr), Alltrim(lcefrid), lcNrelegibilidade,lcFs2stamp,lcAbrev)

		If lcResult
			uf_perguntalt_chama("Enviado com Sucesso!","OK","",64)
			regua(2)
		Endif
	Endif

	regua(2)
	fecha("ucrTempLCefrID")
	fecha("docdescr")
Endproc

&&Metodo que se liga � multicert para o documento ser assinado
Function uf_facturacao_certficarDocumento
	Lparameters uv_pathDestino, uv_docOrigem, uv_ficheiroDestino, uv_showMsg

	If !Empty(uv_docOrigem)
		If Empty(Justpath(uv_docOrigem))
			Return .F.
		Endif
	Endif

	If Empty(uf_gerais_getparameter_site('ADM0000000169', 'bool', mysite)) Or  Empty(uf_gerais_getparameter_site('ADM0000000169', 'text', mysite))
		uf_perguntalt_chama("Entidade de certifica��o de documentos n�o configurada, por favor contacte o suporte.", "OK", "", 16)
		Return .F.
	Endif

	regua(1, Recno(), "A Assinar Documento(s)")

	If(Empty(uv_docOrigem))
		uf_facturacao_imprimedocpdf()
	Endif

	Local lcId,lcftstamp, lcPathOrigem, lcFileOrigem, lcPathDestino, lcFileDestino, lcIdPlanoCert, lcDescPlanoCert
	Store "" To   lcId,lcftstamp, lcPathOrigem, lcFileOrigem, lcPathDestino, lcFileDestino


	lcDescPlanoCert = Alltrim(uf_gerais_getparameter_site('ADM0000000175', 'text', mysite))
	lcIdPlanoCert   = uf_gerais_getparameter_site('ADM0000000175', 'NUM', mysite)

	Select ucrse1
	lcId =  Alltrim(ucrse1.id_lt)

	Select ft
	lcftstamp=  Alltrim(ft.ftstamp)


	lcPathOrigem = Iif( !Empty(uv_docOrigem), Justpath(uv_docOrigem) + "\", Alltrim(uf_gerais_getparameter("ADM0000000015","TEXT") + "\Cache\PDF\") )
	lcFileOrigem = Iif( !Empty(uv_docOrigem), Alltrim(Justfname(uv_docOrigem)), Alltrim(lcId) +"_"+ Alltrim(lcftstamp)+'.pdf')

	lcPathDestino = lcPathOrigem
	lcFileDestino = Iif( !Empty(uv_ficheiroDestino), Alltrim(uv_ficheiroDestino),  Juststem(Alltrim(lcFileOrigem))+"_signed.pdf")

	Local lcPersonReceiverToken, lcExternalReference,lcfilename , lctoken, lcDocumentsToken, lcCertificateId, lcCertificateSignStatus
	Local lcNmdoc, lcndoc, lcno, lcFNo, lcestab, lcano, lcftstamp, lcsql, lcServiceTeste, lctype, lcTypeDesc

	Store "" To lcCertificateId
	Store 0 To lcCertificateSignStatus

	lcPersonReceiverToken 		=  uf_gerais_stamp()
	lctoken 					=  uf_gerais_stamp()
	lcDocumentsToken 			=  uf_gerais_stamp()

	If(Used("ft"))
		Select ft
		lcNmdoc   = Alltrim(ft.nmdoc)
		lcndoc	  = ft.ndoc
		lcno	  = ft.NO
		lcFNo 	  = ft.fno
		lcestab	  = ft.estab
		lcano 	  = ft.ftano
		lcftstamp = Alltrim(ft.ftstamp)
		lcStampCertificate = ft2.stampCertificate
	Endif

	lcExternalReference	 = Alltrim(Str(lcndoc))+ '_' + Alltrim(Str(lcno)) + '_' + Alltrim(Str(lcano)) + Sys(2)
	lcfilename 			 = Alltrim(Str(lcndoc))+ '_' + Alltrim(Str(lcno)) + '_' + Alltrim(Str(lcano)) + '.PDF'

	If !Empty(lcStampCertificate)

		If !uf_gerais_actgrelha('', 'uc_getCertificate', "EXEC up_facturacao_getCertificate '" + Alltrim(lcStampCertificate) + "'")
			uf_perguntalt_chama("N�o foi poss�vel verificar os dados de certifica��o. Por favor contacte o suporte.", "OK", "", 16)
			regua(2)
			Return .F.
		Endif

		If Reccount("uc_getCertificate") <> 0

			lcCertificateId = uc_getCertificate.Id
			lcCertificateSignStatus = uc_getCertificate.signStatusId
			lcExternalReference = uc_getCertificate.externalReference
			lcfilename = uc_getCertificate.Name

		Endif

		fecha("uc_getCertificate")

	Endif

	If uf_gerais_getparameter("ADM0000000227", "BOOL")==.F.
		lcServiceTeste = 0
	Else
		lcServiceTeste = 1
	Endif

	If Empty(lcCertificateId) Or lcCertificateSignStatus <> 200

		lctype 		= 1
		lcTypeDesc  = "DOCCERT"

	Else

		lctype 		= 4
		lcTypeDesc  = "GETDOC"

	Endif




	TEXT TO lcSql TEXTMERGE NOSHOW
			exec up_gerais_treatmentDocCertificate '<<ALLTRIM(mysite)>>', '<<ALLTRIM(lcPersonReceiverToken)>>', '<<ALLTRIM(lcToken )>>',
			 	'<<ALLTRIM(lcDocumentsToken)>>', <<lcNo>>, <<lcEstab>>, '<<ALLTRIM(lcExternalReference)>>', '<<ALLTRIM(lcFileName )>>', <<lcServiceTeste>>,
			 	<<lcType>>,'<<lcTypeDesc>>', '<<IIF(EMPTY(lcCertificateId), lcPathOrigem, '')>>', '<<IIF(EMPTY(lcCertificateId), lcFileOrigem, '')>>', '<<lcPathDestino>>', '<<lcFileDestino>>', '<<ALLTRIM(lcCertificateId)>>', <<IIF(!EMPTY(lcCertificateId), '1', '0')>>,
			 	<<lcIdPlanoCert>>, '<<ALLTRIM(lcDescPlanoCert)>>'
	ENDTEXT


	If  .Not. uf_gerais_actgrelha("", "ucrsTreatmentTemp", lcsql )
		uf_perguntalt_chama("N�O FOI POSSIVEL GERAR OS DADOS PARA CERTIFICA��O", "OK", "", 16)
		regua(2)
		Return .F.
	Endif

	If(ucrsTreatmentTemp.resultInserts = .F.)
		uf_perguntalt_chama("N�O FOI POSSIVEL GERAR OS DADOS PARA CERTIFICA��O", "OK", "", 16)
		regua(2)
		Return .F.
	Endif

	fecha("ucrsTreatmentTemp")

	Local lcDirectoria
	lcDirectoria =	Alltrim(lcPathDestino)

	If !uf_gerais_createDir(lcDirectoria)
		regua(2)
		uf_perguntalt_chama("Ocorreu uma anomalia ao Verificar a pasta para guardar o documento certificado", "OK", "", 64)
		Return .F.
	Endif

	Local lcnomejar, lcwspath, lcwsdir, lcadd

	lcnomejar = "ltsDocsCertificateCli.jar"
	lcwspath  = Alltrim(uf_gerais_getparameter("ADM0000000185","TEXT")) + '\ltsDocsCertificateCli\'+Alltrim(lcnomejar)+' ' +' --TOKEN='+Alltrim(lctoken) +' --IDCL='+Alltrim(ucrse1.id_lt)
	lcwsdir	  =  Alltrim(uf_gerais_getparameter("ADM0000000185","TEXT")) + '\ltsDocsCertificateCli\'
	lcadd  	  = "&"

	lcwspath  = "cmd /c cd /d "+lcwsdir+" "+lcadd+lcadd+" javaw -jar " +lcwspath
	owsshell  = Createobject("WScript.Shell")
	owsshell.Run(lcwspath, 0, .T.)

	If lcServiceTeste = 1
		_Cliptext = lcwspath
	Endif

	fecha("uc_docsCertificateResponse")

	If !uf_gerais_actgrelha("","uc_docsCertificateResponse", "exec up_gerais_getTreatmentDocCertificate '" + lctoken + "','" + lcExternalReference + "'")
		regua(2)
		uf_perguntalt_chama("N�O FOI POSSIVEL GERAR OS DADOS PARA CERTIFICA��O", "OK", "", 16)
		Return .F.
	Endif

	Local lcDocStampFinal
	lcDocStampFinal = ""
	Select uc_docsCertificateResponse
	Go Top
	lcDocStampFinal = Alltrim(uc_docsCertificateResponse.stamp)

	If (uc_docsCertificateResponse.estado= 200)

		If Empty(lcCertificateId)

			TEXT TO lcsql TEXTMERGE NOSHOW
				UPDATE ft2 SET stampCertificate = '<<ALLTRIM(lcDocStampFinal)>>' where ft2.ft2stamp = '<<ALLTRIM(lcFtstamp)>>'
			ENDTEXT

			If  .Not. uf_gerais_actgrelha("", "", lcsql)
				uf_perguntalt_chama("Ocorreu uma anomalia a guardar informa��es Certifica��o", "OK", "", 64)
				regua(2)
				Return .F.
			Endif

			If Used("FT2")
				Replace ft2.stampCertificate With Alltrim(lcDocStampFinal)
			Endif

		Endif

	Else

		uf_perguntalt_chama(Alltrim(Left(uc_docsCertificateResponse.erroMessage,254)), "OK", "", 16)
		fecha("uc_docsCertificateResponse")
		regua(2)
		Return .F.
	Endif

	fecha("uc_docsCertificateResponse")

	If !Empty(uv_pathDestino) And File(Alltrim(lcPathDestino) + Alltrim(lcFileDestino))
		oFSO = Createobject("Scripting.FileSystemObject")
		oFile = oFSO.Getfile(Alltrim(lcPathDestino) + Alltrim(lcFileDestino))
		oFile.Copy(Alltrim(uv_pathDestino))
	Endif

	regua(2)

	If uv_showMsg
		uf_perguntalt_chama("Certifica��o do Documento(s) com Sucesso", "OK", "", 64)
	Endif

	Return .T.

Endfunc

Function uf_faturacao_mostraEnvioCert

	If !Used("TD") Or !Used("FT") Or !Used("FI")
		Return .F.
	Endif

	Select td

	If !Inlist(td.tiposaft, 'FT', 'FR', 'NC', 'ND')
		Return .F.
	Endif

	Select ft

	If !uf_gerais_getUmValor("b_utentes", "entidadePublica", "no = " +  ASTR(ft.NO) + " AND estab = " + ASTR(ft.estab))
		Return .F.
	Endif

	Select td

	If Inlist(Alltrim(td.tiposaft), 'NC', 'ND')

		Select * From fi With (Buffering = .T.) Into Cursor uc_tmpFI

		Select uc_tmpFI
		Count For !Empty(ref) And Empty(ofistamp) To uv_count

		If uv_count <> 0
			Return .F.
		Endif


	Endif

	Return .T.

Endfunc

Function uf_faturacao_enviarDocCertif

	*!*	   uf_imprimirgerais_chama('FACTURACAO')
	*!*	   imprimirgerais.hide
	*!*	   imprimirgerais.pageframe1.page1.chkparapdf.value = 1

	*!*	   select UCRSIMPIDUSDEFAULT

	*!*	   uv_impressao = Alltrim(imprimirGerais.pageframe1.page1.impressao.value)

	*!*	   uv_nomeFicheiro = uf_gerais_getUmValor("b_impressoes", "nomeficheiro", "nomeImpressao = '" + uv_impressao + "'")

	*!*	   uv_report = Alltrim(myPath)+"\analises\" + Alltrim(uv_nomeFicheiro )

	*!*	   lcNomeFicheiroPDF = uf_gerais_removeCharEspecial(ALLTRIM(STRTRAN(STRTRAN(ALLTRIM(ft.nmdoc), ' ', '_'), '/',''))+'_'+ALLTRIM(STR(ft.fno)) + '_' + ALLTRIM(ft.ftstamp)+'.pdf')
	*!*	   lcExpFolder = ALLTRIM(myPath) + "\Cache\PDF\"

	*!*	   uf_imprimirgerais_gravar(ALLTRIM(lcExpFolder) + ALLTRIM(lcNomeFicheiroPDF))

	*!*	   uf_imprimirgerais_sair()

	*!*	   uf_facturacao_certficarDocumento(ALLTRIM(uf_gerais_getUmValor("B_Terminal", "LTRIM(RTRIM(pathExport))", "terminal = '" + alltrim(myTerm) + "'")) + "\"+ALLTRIM(JUSTSTEM(lcNomeFicheiroPDF))+"_signed.pdf", ALLTRIM(lcExpFolder) + ALLTRIM(lcNomeFicheiroPDF), ALLTRIM(JUSTSTEM(lcNomeFicheiroPDF))+"_signed.pdf")

	If !Used("FT") Or !Used("FT2")
		Return .F.
	Endif

	If !Used("UCRSE1")
		Return .F.
	Endif

	If !uf_gerais_actgrelha("", "uc_dadosEmpresa", "select codpost, morada, local, concelho from empresa(nolock) where site = '" + Alltrim(mysite) + "'")
		uf_perguntalt_chama("N�o foi poss�vel carregar os dados da Empresa. Por favor contacte o Suporte.", "OK", "", 16)
		Return .F.
	Endif

	uv_camposFalta = ""

	Select uc_dadosEmpresa

	If Empty(uc_dadosEmpresa.codpost)
		uv_camposFalta = "C�digo Postal"
	Endif
	If Empty(uc_dadosEmpresa.morada)
		uv_camposFalta = uv_camposFalta + Iif(!Empty(uv_camposFalta), ";" + Chr(13), '') + "Morada"
	Endif
	If Empty(uc_dadosEmpresa.Local)
		uv_camposFalta = uv_camposFalta + Iif(!Empty(uv_camposFalta), ";" + Chr(13), '') + "Local"
	Endif
	If Empty(uc_dadosEmpresa.concelho)
		uv_camposFalta = uv_camposFalta + Iif(!Empty(uv_camposFalta), ";" + Chr(13), '') + "Concelho"
	Endif

	If !Empty(uv_camposFalta)

		uf_perguntalt_chama("POR FAVOR PREENCHA OS SEGUINTES CAMPOS NA FICHA DA EMPRESA:" + Chr(13) + Alltrim(uv_camposFalta) + ".", "OK", "", 16)
		Return .F.

	Endif


	If Empty(uf_gerais_getparameter_site('ADM0000000169', 'bool', mysite)) Or  Empty(uf_gerais_getparameter_site('ADM0000000169', 'text', mysite))
		uf_perguntalt_chama("Entidade de certifica��o de documentos n�o configurada, por favor contacte o suporte.", "OK", "", 16)
		Return .F.
	Endif


	If uf_gerais_getparameter("ADM0000000227", "BOOL")==.F.
		lcServiceTeste = 0
	Else
		lcServiceTeste = 1
	Endif

	Select ft2

	If !Empty(ft2.idCertXML)

		regua(1, 2, "A verificar estado do Documento...")

		uv_tokenGet = uf_gerais_stamp()
		uv_externalReference = uf_gerais_getUmValor("docsCertificateResponse", "(select externalReference from docsCertificateRequest(nolock) where token = docsCertificateResponse.token)", "id = '" + Alltrim(ft2.idCertXML) + "'")

		If Empty(uv_externalReference)
			uf_perguntalt_chama("ERRO A LOCALIZAR EXTERNALREFERENCE. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
			regua(2)
			Return .F.
		Endif

		regua(1, 15, "A verificar estado do Documento...")

		TEXT TO lcSql TEXTMERGE NOSHOW

         Declare @token varchar(36) = '<<ALLTRIM(uv_tokenGet)>>'
         Declare @externalReference varchar(50) = '<<ALLTRIM(uv_externalReference)>>'

         EXEC up_gerais_createResquestCertXML @token, '', '', @externalReference, '', '<<ALLTRIM(ft.ftstamp)>>', <<lcServiceTeste>>, '<<ALLTRIM(ft2.idCertXML)>>', 'CHECK'

		ENDTEXT

		If !uf_gerais_actgrelha("","",lcsql)
			uf_perguntalt_chama("ERRO NO PEDIDO. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
			regua(2)
			Return .F.
		Endif

		regua(1, 30, "A verificar estado do Documento...")

		Local lcnomejar, lcwspath, lcwsdir, lcadd

		lcnomejar = "ltsDocsCertificateCli.jar"
		lcwspath  = Alltrim(uf_gerais_getparameter("ADM0000000185","TEXT")) + '\ltsDocsCertificateCli\'+Alltrim(lcnomejar)+' ' +' --TOKEN='+Alltrim(uv_tokenGet) +' --IDCL='+Alltrim(ucrse1.id_lt)
		lcwsdir	  =  Alltrim(uf_gerais_getparameter("ADM0000000185","TEXT")) + '\ltsDocsCertificateCli\'
		lcadd  	  = "&"

		lcwspath  = "cmd /c cd /d "+lcwsdir+" "+lcadd+lcadd+" javaw -jar " +lcwspath
		owsshell  = Createobject("WScript.Shell")
		owsshell.Run(lcwspath, 0, .T.)

		If lcServiceTeste = 1
			_Cliptext = lcwspath
		Endif

		regua(1, 45, "A verificar estado do Documento...")

		If !uf_gerais_actgrelha("","uc_docsCertificateResponse", "exec up_gerais_getTreatmentDocCertificate '" + Alltrim(uv_tokenGet) + "','', 1")
			uf_perguntalt_chama("N�O FOI POSSIVEL GERAR OS DADOS PARA CERTIFICA��O", "OK", "", 16)
			regua(2)
			Return .F.
		Endif

		Select uc_docsCertificateResponse

		If uc_docsCertificateResponse.estado = 200

			uf_perguntalt_chama(Alltrim(uc_docsCertificateResponse.mensagem), "OK", "", 64)

		Else

			uf_perguntalt_chama(Alltrim(Left(uc_docsCertificateResponse.erroMessage,254)), "OK", "", 16)
			fecha("uc_docsCertificateResponse")
			regua(2)
			Return .F.

		Endif

		regua(2)
		Return .T.

	Endif


	regua(1, 2, "A Assinar Documento(s)")


	Select ft
	lcNmdoc   = Alltrim(ft.nmdoc)
	lcndoc	  = ft.ndoc
	lcno	  = ft.NO
	lcFNo 	  = ft.fno
	lcestab	  = ft.estab
	lcano 	  = ft.ftano
	lcftstamp = Alltrim(ft.ftstamp)

	lcExternalReference	 = ASTR(lcndoc)+ '_' + ASTR(lcno) + '_' + ASTR(lcFNo) + "_" + ASTR(lcano) + Sys(2)

	uv_file = ""

	If !Empty(ft2.idCreateXML)

		If !uf_gerais_actgrelha("", "uc_createdXML", "exec up_gerais_getFileXML '" + ft2.idCreateXML + "'")
			uf_perguntalt_chama("N�O FOI POSSIVEL VERIFICAR OS DADOS DE CRIA��O DO FICHEIRO.", "OK", "", 16)
			Return .F.
		Endif

		Select uc_createdXML

		uv_file = Alltrim(uc_createdXML.signedContent)
		lcExternalReference = Alltrim(uc_createdXML.externalReference)

	Endif

	uv_createFile = .T.

	If !Empty(uv_file) And File(uv_file)
		uv_createFile = .F.
	Endif

	uv_tokenCertificar = uf_gerais_stamp()
	uv_documentsToken = uf_gerais_stamp()
	uv_personReceiverToken = uf_gerais_stamp()

	regua(1, 15, "A Assinar Documento(s)")

	If uv_createFile

		uv_oriOperationId = ""
		uv_typeOp = 2

		If !Empty(uv_file)
			uv_oriOperationId = Alltrim(ft2.idCreateXML)
			uv_typeOp = 4
		Endif

		TEXT TO lcSql TEXTMERGE NOSHOW

         Declare @token varchar(36) = '<<ALLTRIM(uv_tokenCertificar)>>'
         Declare @documentsToken varchar(36) = '<<ALLTRIM(uv_documentsToken)>>'
         Declare @personReceiverToken varchar(36) = '<<ALLTRIM(uv_personReceiverToken)>>'
         Declare @externalReference varchar(50) = '<<ALLTRIM(lcExternalReference)>>'
         Declare @path varchar(254) = '<<ALLTRIM(myPath) + "\CACHE\XML">>'

         EXEC up_gerais_createResquestCertXML @token, @documentsToken, @personReceiverToken, @externalReference, @path, '<<ft.ftstamp>>', <<lcServiceTeste>>, '<<ALLTRIM(uv_oriOperationId)>>', 'CREATE', <<ASTR(uv_typeOp)>>

		ENDTEXT

		If !uf_gerais_actgrelha("","",lcsql)
			_Cliptext = lcsql
			uf_perguntalt_chama("ERRO NO PEDIDO. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
			regua(2)
			Return .F.
		Endif

		Local lcnomejar, lcwspath, lcwsdir, lcadd

		lcnomejar = "ltsDocsCertificateCli.jar"
		lcwspath  = Alltrim(uf_gerais_getparameter("ADM0000000185","TEXT")) + '\ltsDocsCertificateCli\'+Alltrim(lcnomejar)+' ' +' --TOKEN='+Alltrim(uv_tokenCertificar) +' --IDCL='+Alltrim(ucrse1.id_lt)
		lcwsdir	  =  Alltrim(uf_gerais_getparameter("ADM0000000185","TEXT")) + '\ltsDocsCertificateCli\'
		lcadd  	  = "&"

		lcwspath  = "cmd /c cd /d "+lcwsdir+" "+lcadd+lcadd+" javaw -jar " +lcwspath
		owsshell  = Createobject("WScript.Shell")
		owsshell.Run(lcwspath, 0, .T.)

		If lcServiceTeste = 1
			_Cliptext = lcwspath
		Endif

		If !uf_gerais_actgrelha("","uc_docsCertificateResponse", "exec up_gerais_getTreatmentDocCertificate '" + Alltrim(uv_tokenCertificar) + "','', 1")
			regua(2)
			uf_perguntalt_chama("N�O FOI POSSIVEL GERAR OS DADOS PARA CERTIFICA��O", "OK", "", 16)
			Return .F.
		Endif

		Local lcDocStampFinal
		lcDocStampFinal = ""
		Select uc_docsCertificateResponse
		Go Top
		lcDocStampFinal = Alltrim(uc_docsCertificateResponse.stamp)

		If uc_docsCertificateResponse.estado = 200

			Select uc_docsCertificateResponse

			If !uf_gerais_actgrelha("","","update ft2 set idCreateXML = '" + Alltrim(uc_docsCertificateResponse.Id) + "' where ft2stamp = '" + ft.ftstamp + "'")
				uf_perguntalt_chama("ERRO A ATUALIZAR DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
				regua(2)
				Return .F.
			Endif

			Select ft2
			Replace ft2.idCreateXML With Alltrim(uc_docsCertificateResponse.Id)

		Else

			uf_perguntalt_chama(Alltrim(uc_docsCertificateResponse.erroMessage), "OK", "", 16)
			fecha("uc_docsCertificateResponse")
			regua(2)
			Return .F.

		Endif

	Endif

	regua(1, 30, "A Assinar Documento(s)")

	uv_tokenEnviar = uf_gerais_stamp()
	uv_documentsToken = uf_gerais_stamp()
	uv_personReceiverToken = uf_gerais_stamp()

	TEXT TO lcSql TEXTMERGE NOSHOW

      Declare @token varchar(36) = '<<ALLTRIM(uv_tokenEnviar)>>'
      Declare @documentsToken varchar(36) = '<<ALLTRIM(uv_documentsToken)>>'
      Declare @personReceiverToken varchar(36) = '<<ALLTRIM(uv_personReceiverToken)>>'
      Declare @externalReference varchar(50) = '<<ALLTRIM(lcExternalReference)>>'
      Declare @path varchar(254) = '<<ALLTRIM(myPath) + "\CACHE\XML">>'

      EXEC up_gerais_createResquestCertXML @token, @documentsToken, @personReceiverToken, @externalReference, @path, '<<ft.ftstamp>>', <<lcServiceTeste>>, '', 'SEND'

	ENDTEXT

	If !uf_gerais_actgrelha("","",lcsql)
		_Cliptext = lcsql
		uf_perguntalt_chama("ERRO NO PEDIDO. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
		regua(2)
		Return .F.
	Endif

	regua(1, 45, "A Assinar Documento(s)")


	Local lcnomejar, lcwspath, lcwsdir, lcadd

	lcnomejar = "ltsDocsCertificateCli.jar"
	lcwspath  = Alltrim(uf_gerais_getparameter("ADM0000000185","TEXT")) + '\ltsDocsCertificateCli\'+Alltrim(lcnomejar)+' ' +' --TOKEN='+Alltrim(uv_tokenEnviar) +' --IDCL='+Alltrim(ucrse1.id_lt)
	lcwsdir	  =  Alltrim(uf_gerais_getparameter("ADM0000000185","TEXT")) + '\ltsDocsCertificateCli\'
	lcadd  	  = "&"

	lcwspath  = "cmd /c cd /d "+lcwsdir+" "+lcadd+lcadd+" javaw -jar " +lcwspath
	owsshell  = Createobject("WScript.Shell")
	owsshell.Run(lcwspath, 0, .T.)

	If lcServiceTeste = 1
		_Cliptext = lcwspath
	Endif

	regua(1, 60, "A Assinar Documento(s)")

	If !uf_gerais_actgrelha("","uc_docsCertificateResponse", "exec up_gerais_getTreatmentDocCertificate '" + Alltrim(uv_tokenEnviar) + "','" + lcExternalReference + "',1")
		regua(2)
		uf_perguntalt_chama("N�O FOI POSSIVEL GERAR OS DADOS PARA CERTIFICA��O", "OK", "", 16)
		Return .F.
	Endif

	Select uc_docsCertificateResponse

	If (uc_docsCertificateResponse.estado= 200)

		Select uc_docsCertificateResponse

		If !uf_gerais_actgrelha("","","update ft2 set idCertXML = '" + Alltrim(uc_docsCertificateResponse.Id) + "' where ft2stamp = '" + ft.ftstamp + "'")
			uf_perguntalt_chama("ERRO A ATUALIZAR DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
			regua(2)
			Return .F.
		Endif

		Select ft2
		Replace ft2.idCertXML With Alltrim(uc_docsCertificateResponse.Id)

		uf_perguntalt_chama(Alltrim(uc_docsCertificateResponse.mensagem), "OK", "", 64)

	Else

		uf_perguntalt_chama(Alltrim(uc_docsCertificateResponse.erroMessage), "OK", "", 16)
		fecha("uc_docsCertificateResponse")
		regua(2)
		Return .F.

	Endif

	regua(2)

Endfunc





Function uf_facturacao_administrador_receitasanular()

	If !uf_perguntalt_chama("Pretende Libertar a Receita ?", "Sim", "N�o")
		Return .F.
	Endif

	If(!Used("FT2"))
		uf_perguntalt_chama("Cabe�alho de documento FT2 n�o encontrado", "OK", "", 16)
		Return .F.
	Endif


	If(!Used("FT"))
		uf_perguntalt_chama("Cabe�alho de documento FT n�o encontrado", "OK", "", 16)
		Return .F.
	Endif

	Local lctoken, lcreceita
	Store "" To lctoken, lcreceita

	Select ft2
	Go Top
	lctoken =  Alltrim(ft2.token)
	lcreceita    =  Alltrim(ft2.u_receita)

	If(Empty(lctoken))
		uf_perguntalt_chama("Token de receita n�o encontrado", "OK", "", 16)
		Return .F.
	Endif

	If(Empty(lcreceita))
		uf_perguntalt_chama("Numero de receita n�o encontrado", "OK", "", 16)
		Return .F.
	Endif

	If(!uf_atendimento_receitasanular(lcreceita,lctoken))
		uf_perguntalt_chama("Erro ao libertar a receita centralmente", "OK", "", 16)
		Return .F.
	Else
		uf_perguntalt_chama("Receita foi libertada centralmente.", "OK", "", 64)
		Return .F.
	Endif


Endfunc


Function uf_faturacao_tipoEstadoInfoEmbal

	If !Used("fi_trans_info")
		Return .F.
	Endif

	Select fi_trans_info

	If Reccount("fi_trans_info") = 0
		Return .F.
	Endif

	Select td
	If Inlist(td.tipodoc, 3)

		Select fi_trans_info
		Replace fi_trans_info.tipo With 'E' For 1=1

	Else

		Select fi_trans_info
		Replace fi_trans_info.tipo With 'S' For 1=1

	Endif

	Return .T.

Endfunc

Function uf_faturacao_validaRefQttVazia

	Local lcResp
	lcResp = .T.

	If(uf_gerais_getparameter("ADM0000000368","BOOL"))
		Return lcResp
	Endif

	If(Used("fi"))
		Local lcposfi
		Select fi
		lcposfi = Recno("fi")

		Select fi
		Go Top
		Scan
			If(fi.qtt==0 And Empty(Alltrim(fi.ref)))
				lcResp = .F.
			Endif
			Select fi
		Endscan


		Select fi
		Try
			Go lcposfi
		Catch
			Go Top
		Endtry

	Endif


	Return lcResp

Endfunc

Function uf_faturacao_getCartoes
	Lparameters lcNbef, lcAbrev, lcNrAtend

	Local  lcbenef, lcSQLCard, lcSQLCptorg
	Store '' To lcbenef, lcSQLCard, lcSQLCptorg

	If (!Empty(lcNbef) And !Empty(lcAbrev) And !Empty(lcNrAtend))

		TEXT TO lcSQLCptorg TEXTMERGE NOSHOW
	  		SELECT REPLACE(cptorgStamp,'ADM','') AS cptorgStamp FROM cptorg(nolock) WHERE abrev = '<<ALLTRIM(lcabrev)>>'
		ENDTEXT
		If !uf_gerais_actgrelha("", "ucrsEntity", lcSQLCptorg)
			uf_perguntalt_chama("Erro ao ler dados da entidade", "", "OK", 16)
			Return .F.
		Endif

		Local lcEntityId
		lcEntityId   = ''
		Select ucrsEntity
		Go Top
		lcEntityId = Alltrim(ucrsEntity.cptorgStamp)


		TEXT TO lcSQLCard TEXTMERGE NOSHOW
			SELECT TOP 1 convert(varchar(254),trackingId) as  trackingId,id,status,entityId,ousrdata,obs FROM ext_esb_associatecard (nolock)
			WHERE atendStamp = '<<ALLTRIM(lcnratend)>>' AND ID = '<<ALLTRIM(lcNbef)>>'
				  AND dbo.[uf_StripNonNumerics](LEFT(entityId , case when CHARINDEX('.', entityId ) > 0 then CHARINDEX('.', entityId )  - 1 else len(entityId ) end))= convert(int,'<<ALLTRIM(ucrsEntity.cptorgStamp)>>' )
			ORDER BY ousrdata DESC
		ENDTEXT
		If !uf_gerais_actgrelha("", "ucrsTrackId", lcSQLCard)
			uf_perguntalt_chama("Erro ao ler trackID", "", "OK", 16)
			Return .F.
		Endif

		fecha("ucrsEntity")

		Return .T.
	Endif

	Return .T.

Endfunc

Function uf_faturacao_deleteCursorTrackId
	Lparameters lcNrCartao

	If !Used("ucrsTrackId")
		Return .F.
	Endif

	Delete From ucrsTrackId

	Return .T.
Endfunc

Function uf_faturacao_insertExtCards
	Lparameters lcNrAtend

	Local lcSQLInsert, lctoken
	Store '' To lcSQLInsert, lctoken

	If !Used("ucrsTrackId")
		Return .F.
	Endif
	If Reccount("ucrsTrackId")<1
		Return .F.
	Endif

	lctoken = uf_gerais_stamp()

	If  Empty(lcNrAtend) Or Empty(lctoken)
		uf_perguntalt_chama("Erro ao obter valores do n�mero atendimento/Token", "", "OK", 16)
		Return .F.
	Endif

	Select ucrsTrackId
	Do Case
		Case ucrsTrackId.Status == .T.

			TEXT TO lcSQLInsert TEXTMERGE NOSHOW
				INSERT INTO ext_esb_associatecard(token, id, status, trackingId, entityId, atendStamp, ousrdata, obs)
				VALUES('<<ALLTRIM(lcToken)>>','<<ALLTRIM(ucrsTrackId.id)>>',<<IIF(ucrsTrackId.status,1,0)>>,'<<ALLTRIM(ucrsTrackId.trackingId)>>','<<ALLTRIM(ucrsTrackId.entityId)>>','<<ALLTRIM(lcNratend)>>',getdate(),'<<ALLTRIM(ucrsTrackId.OBS)>>')
			ENDTEXT

			If !uf_gerais_actgrelha("", "", lcSQLInsert)
				uf_perguntalt_chama("Erro ao inserir na ext_esb_associatecard", "", "OK", 16)
				Return .F.
			Endif

		Otherwise
			Return .T.
	Endcase

Endfunc

Function uf_faturacao_verificaCaixas

	If !Used("TD")
		Return .F.
	Endif

	If td.mvoVerificaEstado

		If  .Not. uf_valida_info_caixas_verificacao()
			Return .F.
		Endif

		uf_faturacao_tipoEstadoInfoEmbal()

		If !uf_atendimento_valida_estado_datamatrix(.T.)
			Return .F.
		Endif

	Endif

	If !Used("fi_trans_info") Or Reccount("fi_trans_info") = 0
		Return .T.
	Endif

	If td.mvoAlteraEstado

		Local uv_alteraEstado
		Store .T. To uv_alteraEstado

		If Used("TD") And td.mvoOpcional
			If td.tipodoc = 3
				If !uf_perguntalt_chama("Diretiva de Medicamentos Falsificados." + Chr(13) + "Quer ativar as caixas conferidas?","Sim","N�o")
					uv_alteraEstado = .F.
				Endif
			Else
				If !uf_perguntalt_chama("Diretiva de Medicamentos Falsificados." + Chr(13) + "Quer inativar as caixas conferidas?","Sim","N�o")
					uv_alteraEstado = .F.
				Endif
			Endif
		Endif

		If uv_alteraEstado

			Select fi_trans_info
			Go Top

			Select * From fi_trans_info Where fi_trans_info.tipo='S' Into Cursor fi_trans_info_s Readwrite

			If Reccount("fi_trans_info_s")>0
				uf_Atendimento_DispensePackVerify()
			Endif

			fecha("fi_trans_info_s")

			Select fi_trans_info
			Go Top

			Select * From fi_trans_info Where fi_trans_info.tipo='E' Into Cursor fi_trans_info_e Readwrite

			If Reccount("fi_trans_info_e")>0
				uf_Atendimento_UndoDispenseSinglePack()
			Endif

			fecha("fi_trans_info_e")

			Select fi_trans_info
			Go Top

			uf_valida_info_caixas_dispensa(.T.)

		Endif

	Endif

	If Used("mixed_bulk_pend")
		If Reccount("mixed_bulk_pend")>0
			lcSqlmixed_bulk_pend = uf_PAGAMENTO_GravarReg_mixed_bulk_pend()
			uf_gerais_actgrelha("","",lcSqlmixed_bulk_pend )
		Endif
		fecha("mixed_bulk_pend")
	Endif

	Return .T.

Endfunc

Function uf_faturacao_criaCursorDadosPlano
	Lparameters lccodigo

	If  .Not. Empty(lccodigo)
		TEXT TO lcsql TEXTMERGE NOSHOW
				exec up_receituario_dadosDoPlano '<<lcCodigo>>'
		ENDTEXT
		uf_gerais_actgrelha("", "uCrsCodPla", lcsql)
		If Reccount('uCrsCodPla')=0
			uf_perguntalt_chama("ESTE C�DIGO N�O EXISTE NA TABELA DE PLANOS DE COMPARTICIPA��O. RECTIFIQUE O MESMO.", "OK", "", 64)
			Return .F.
		Endif
	Endif

Endfunc

Function uf_faturacao_valida_obito
	Lparameters uv_pergunta

	If !Used("FT")
		Return .F.
	Endif

	If uf_gerais_getUmValor("b_utentes", "obitoRNU", "no = " + ASTR(ft.NO) + " and estab = " + ASTR(ft.estab))
		If uv_pergunta
			If !uf_perguntalt_chama("Aten��o: Este utente tem o campo �bito ativo na ficha, o que poder� gerar receitu�rio devolvido." + Chr(13) + "Quer continuar?", "Sim", "N�o")
				Return .F.
			Endif
		Else
			uf_perguntalt_chama("Aten��o: Este utente tem o campo �bito ativo na ficha, o que poder� gerar receitu�rio devolvido.", "OK", "", 48)
		Endif
	Endif

	Return .T.

Endfunc

Function uf_faturacao_altercaoBloco

	Local lcCampoEscolhido, lcResultadoEscolhido, lcCursor
	Store '' To lcCampoEscolhido, lcResultadoEscolhido, lcCursor

	lcCampoEscolhido = uf_faturacao_altercaoBloco_escolhe()

	If(!Empty(lcCampoEscolhido))
		lcResultadoEscolhido = uf_faturacao_altercaoBloco_escolheResultado()
		If(!Empty(lcResultadoEscolhido) Or (Vartype(lcResultadoEscolhido) == "N" And lcResultadoEscolhido >=0))
			If Used("FT")
				Select ft
				lcCursor = uf_gerais_cursorWkCols(ft.nmdoc)
				If(!Empty(lcCursor))
					Select &lcCursor.
					Go Top
					Scan
						Do Case
							Case Alltrim(lcCampoEscolhido) = 'Desconto P'
								Replace &lcCursor..desconto  With lcResultadoEscolhido
								uf_atendimento_fiiliq(.T.)
								uf_atendimento_acttotaisft()
							Case Alltrim(lcCampoEscolhido) = 'Desconto V'
								Replace &lcCursor..u_descval With lcResultadoEscolhido
								uf_atendimento_fiiliq(.T.)
								uf_atendimento_actvaloresfi()
							Case Alltrim(lcCampoEscolhido) = 'Iva'
								Replace &lcCursor..iva 		 With lcResultadoEscolhido
								Replace &lcCursor..tabiva 	 With myEscolhaIva
							Case Alltrim(lcEscolha) = 'Armazem'
								Replace &lcCursor..armazem	 With lcResultadoEscolhido
								If Wexist("facturacao")
									facturacao.containercab.armazem.Value = lcResultadoEscolhido
								Endif
						Endcase
					Endscan
				Endif
			Endif
		Endif
	Endif

	uf_faturacao_altercaoBloco_realeseVariaveis()


	&& TODO receitas com BAS
	If(uf_facturacao_validaSeDeveRecalcularValores(.F.,"",.F.) )
		uf_atendimento_actvaloresfi()
		uf_atendimento_acttotaisft()
	Endif

	Return .T.
Endfunc

Function uf_faturacao_altercaoBloco_escolheResultado

	Local lcResultado, lcSQLArmazem
	Store '' To lcResultado, lcSQLArmazem

	Do Case
		Case Alltrim(lcEscolha) = 'Desconto P'
			Public myDescLinhas
			Store 0 To myDescLinhas
			uf_tecladonumerico_chama("myDescLinhas", "Introduza o Desconto (%):", 0, .T., .F., 6, 2)
			lcResultado = myDescLinhas

		Case Alltrim(lcEscolha) = 'Desconto V'
			Public myDescLinhasValor
			Store 0 To myDescLinhasValor
			uf_tecladonumerico_chama("myDescLinhasValor", "Introduza o Desconto Valor:", 0, .T., .F., 6, 2)
			lcResultado = myDescLinhasValor

		Case Alltrim(lcEscolha) = 'Iva'
			Public myEscolhaIva
			Local lctaxaiva, lctabiva, lcteste
			uf_valorescombo_chama("myEscolhaIva", 0, "select sel = convert(bit,0), codigo = CONVERT(int,codigo), taxa from taxasiva (nolock) where ativo=1", 1, "codigo", "taxa")
			If(Empty(myEscolhaIva))
				lcResultado = ""
			Else
				lctaxaiva = uf_gerais_gettabelaiva(myEscolhaIva,"TAXA")
				lcResultado = lctaxaiva
			Endif

		Case Alltrim(lcEscolha) = 'Armazem'
			Public myArmazemSel
			TEXT TO lcSQLArmazem TEXTMERGE NOSHOW
				select distinct no as armazem from empresa
			ENDTEXT
			uf_gerais_actgrelha("", "ucrsarmSel", lcSQLArmazem)
			uf_valorescombo_chama("myArmazemSel", 0, "ucrsarmSel", 2, "Armazem", "Armazem")
			lcResultado = myArmazemSel
	Endcase

	Return lcResultado

Endfunc

Function uf_faturacao_altercaoBloco_escolhe

	Local lcSQLEscolhe
	Store '' To lcSQLEscolhe
	Public lcEscolha
	lcEscolha = ""

	TEXT TO lcSQLEscolhe TEXTMERGE NOSHOW
		select campo from b_multidata WHERE tipo='alt_grup_faturacao' order by ordem asc
	ENDTEXT
	uf_gerais_actgrelha("", "ucrsComboDoc", lcSQLEscolhe)

	If Used("ucrsComboDoc")

		Select ucrsComboDoc
		Go Top
		uf_valorescombo_chama("lcEscolha", 0, "ucrsComboDoc", 2, "Campo", "Campo")
	Endif

	Return lcEscolha

Endfunc

Function uf_faturacao_altercaoBloco_realeseVariaveis

	If Type("myDescLinhas")<>"U"
		Release myDescLinhas
	Endif
	If Type("myEscolhaIva")<>"U"
		Release myEscolhaIva
	Endif
	If Type("myDescLinhasValor")<>"U"
		Release myDescLinhasValor
	Endif
	If Type("myArmazemSel")<>"U"
		Release myArmazemSel
	Endif

Endfunc
