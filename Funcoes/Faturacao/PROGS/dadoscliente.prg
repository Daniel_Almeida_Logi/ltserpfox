**
FUNCTION uf_dadoscliente_chama
 IF USED("FT")
    LOCAL lcsql
    SELECT ft
    GOTO TOP
    CREATE CURSOR uc_dadosCliente (nome VARCHAR(55), morada VARCHAR(55), local VARCHAR(43), codpost VARCHAR(45), ncont VARCHAR(20), telefone VARCHAR(60), ccusto VARCHAR(20))
    SELECT uc_dadoscliente
    APPEND BLANK
    REPLACE nome WITH ft.nome, morada WITH ft.morada, local WITH ft.local, codpost WITH ft.codpost, ncont WITH ft.ncont, telefone WITH ft.telefone, ccusto WITH ft.ccusto
 ELSE
    RETURN .F.
 ENDIF
 IF TYPE("dadoscliente")=="U"
    DO FORM dadoscliente
 ELSE
    dadoscliente.show
 ENDIF
ENDFUNC
**
PROCEDURE uf_dadoscliente_init
 IF TYPE("dadoscliente.menu1.rechama")=="U"
    dadoscliente.menu1.adicionaopcao("rechama", "Rechamar Dados", mypath+"\imagens\icons\actualizar_w.png", "uf_dadoscliente_rechamarDadosCliente", "F1")
 ENDIF
 dadoscliente.menu1.estado("", "", "ACTUALIZAR", .T.)
 dadoscliente.txtnomedocumento.value = ft.nmdoc
 dadoscliente.txtnumdocumento.value = ft.fno
ENDPROC
**
FUNCTION uf_dadoscliente_rechamardadoscliente
 LOCAL lcsql
 lcsql = "Select nome, morada, local, codpost, ncont, telefone,ccusto From b_utentes (nolock) Where NO = "+STR(ft.no)+" AND ESTAB = "+STR(ft.estab)
 IF  .NOT. uf_gerais_actgrelha("", "uc_dadosCliente", lcsql)
    uf_perguntalt_chama("N�O FOI POSS�VEL VERIFICAR OS DADOS DO CLIENTE. POR FAVOR CONTACTE A ASSIST�NCIA T�CNICA", "OK", "", 16)
    RETURN .F.
 ENDIF
 dadoscliente.refresh
ENDFUNC
**
PROCEDURE uf_dadoscliente_gravar
 SELECT uc_dadoscliente
 SELECT ft
 REPLACE ft.nome WITH uc_dadoscliente.nome
 REPLACE ft.morada WITH uc_dadoscliente.morada
 REPLACE ft.local WITH uc_dadoscliente.local
 REPLACE ft.codpost WITH uc_dadoscliente.codpost
 REPLACE ft.ncont WITH uc_dadoscliente.ncont
 REPLACE ft.telefone WITH uc_dadoscliente.telefone
 REPLACE ft.ccusto WITH uc_dadoscliente.ccusto
 uf_perguntalt_chama("DADOS ACTUALIZADOS COM SUCESSO.", "OK", "", 64)
 facturacao.refresh
 uf_dadoscliente_sair()
ENDPROC
**
PROCEDURE uf_dadoscliente_sair
 IF USED("uc_dadosCliente")
    fecha("uc_dadosCliente")
 ENDIF
 dadoscliente.hide
 dadoscliente.release
ENDPROC
**
