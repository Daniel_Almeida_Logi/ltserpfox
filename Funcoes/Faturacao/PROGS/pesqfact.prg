**
Function uf_pesqfact_chama
	Lparameters lcecra
	If  .Not. uf_gerais_validapermuser(ch_userno, ch_grupo, 'Gest�o de Factura��o')
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ACEDER AO PAINEL GEST�O DE FACTURA��O.", "OK", "", 48)
		Return .F.
	Endif
	If Type("PESQFACT")<>"U"
		pesqfact.Show
		Update ucrsPesqFact Set ucrsPesqFact.escolha = .F.
		Select ucrsPesqFact
		Goto Top
	Else
		lcsql = ""
		TEXT TO lcsql TEXTMERGE NOSHOW
			exec up_perfis_validaAcessoDocs '<<ch_userno>>', '<<ALLTRIM(ch_grupo)>>', 'Visualizar', 'FT', 0, '<<mysite>>'
		ENDTEXT
		If  .Not. uf_gerais_actgrelha("", "ucrsComboFactPesq", lcsql)
			uf_perguntalt_chama("N�O FOI POSS�VEL ENCONTRAR DOCUMENTOS.", "OK", "", 16)
			Return .F.
		Endif
		If  .Not. Used("ucrsPesqFact")
			uf_pesqfact_carregadados(.T.)
		Else
			Select ucrsPesqFact
			Goto Top
			Scan
				Replace ucrsPesqFact.escolha With .F.
			Endscan
			Select ucrsPesqFact
			Goto Top
		Endif
		Do Form pesqfact
		If Type("lcEcra")=="C"
			pesqfact.ecra = Upper(Alltrim(lcecra))
		Endif
	Endif
Endfunc
**
Procedure uf_pesqfact_limpardados
	pesqfact.numdoc.Value = ''
	pesqfact.produto.Value = ''
	pesqfact.dataini.Value = ''
	pesqfact.datafim.Value = ''
	pesqfact.Nome.Value = ''
	pesqfact.no.Value = ''
	pesqfact.estab.Value = ''
	pesqfact.ultimos.Value = ''
	pesqfact.documento.Value = ''
	pesqfact.txtnatend.Value = ''
	pesqfact.txtnrec.Value = ''
	pesqfact.obs.Value = ''
	uf_pesqfact_carregadados(.T.)
Endproc
**
Function uf_pesqfact_carregadados
	Lparameters tcbool, tcreport
	Local lcsql

	Do Case
		Case !Type("PESQFACT") == "U"
			If !uf_gerais_validapermuser(ch_userno, ch_grupo,'Relat�rios - Painel')
				uf_perguntalt_chama("O SEU PERFIL N�O TEM PERMISS�ES PARA ACEDER A ESTA FUNCIONALIDADE.","OK","",48)
				Return .F.
			Endif
	Endcase

	If tcbool==.T.
		lcsql = ""
		TEXT TO lcsql TEXTMERGE NOSHOW
			exec up_facturacao_pesquisarDocumentos 0, '', 0, 0, -1,  '30000101', '30000101', '', 0, '', '', '', '', '', ''
		ENDTEXT
		If Type("PESQFACT")=="U"
			If  .Not. uf_gerais_actgrelha("", "ucrsPesqFact", lcsql)
				uf_perguntalt_chama("N�O FOI POSSIVEL ACTUALIZAR A LISTAGEM DE DOCUMENTOS. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
				Return .F.
			Endif
		Endif
	Else
		regua(0, 0, "A processar as Listagem de Documentos...")
		lctop = Iif(Int(Val(pesqfact.ultimos.Value))==0, 100000, Int(Val(pesqfact.ultimos.Value)))
		If  .Not. Empty(pesqfact.dataini.Value)
			lcdataini = uf_gerais_getdate(pesqfact.dataini.Value, "SQL")
		Else
			lcdataini = '19000101'
		Endif
		If  .Not. Empty(pesqfact.datafim.Value)
			lcdatafim = uf_gerais_getdate(pesqfact.datafim.Value, "SQL")
		Else
			If  .Not. Empty(pesqfact.dataini.Value)
				lcdatafim = lcdataini
			Else
				lcdatafim = '30001231'
			Endif
		Endif
		If Empty(tcreport)
			lcsql = ""
			TEXT TO lcsql TEXTMERGE NOSHOW
				exec up_facturacao_pesquisarDocumentos <<lcTop>>, '<<Alltrim(PESQFACT.NOME.VALUE)>>', <<IIF(EMPTY(Alltrim(PESQFACT.NUMDOC.VALUE)),0,Alltrim(LEFT(PESQFACT.NUMDOC.VALUE,10)))>>,
						<<IIF(EMPTY(Alltrim(PESQFACT.NO.VALUE)),0,Alltrim(PESQFACT.NO.VALUE))>>, <<IIF(EMPTY(Alltrim(PESQFACT.ESTAB.VALUE)),-1,Alltrim(LEFT(PESQFACT.ESTAB.VALUE,3)))>>,  '<<lcDataIni>>',  '<<lcDataFim>>',
						'<<Alltrim(PESQFACT.documento.Value)>>', <<ch_userno>>,  '<<Alltrim(ch_grupo)>>', '<<Alltrim(PESQFACT.produto.VALUE)>>', '<<Alltrim(PESQFACT.txtNAtend.VALUE)>>', '<<Alltrim(PESQFACT.txtNRec.VALUE)>>',
						'<<Alltrim(PESQFACT.loja.Value)>>', '<<ASTR(PESQFACT.nif.value)>>', -1 ,'<<ALLTRIM(PESQFACT.op.value)>>','<<ALLTRIM(PESQFACT.obs.VALUE)>>'
			ENDTEXT
			uf_gerais_actgrelha("PESQFACT.GridPesq", "ucrsPesqFact", lcsql)
			regua(1, 3, "A PESQUISAR...", .F.)
			pesqfact.gridpesq.Refresh
			pesqfact.lblregistos.Caption = Alltrim(Str(Reccount("ucrsPesqFact")))+" Resultados"
		Else
			TEXT TO lcreport TEXTMERGE NOSHOW
				&topo=<<lcTop>>&entidade=<<Alltrim(PESQFACT.NOME.VALUE)>>&numdoc=<<IIF(EMPTY(Alltrim(PESQFACT.NUMDOC.VALUE)),0,Alltrim(PESQFACT.NUMDOC.VALUE))>>&no=<<IIF(EMPTY(Alltrim(PESQFACT.NO.VALUE)),0,Alltrim(PESQFACT.NO.VALUE))>>&estab= <<IIF(EMPTY(Alltrim(PESQFACT.ESTAB.VALUE)),-1,Alltrim(PESQFACT.ESTAB.VALUE))>>&dataIni=<<uf_gerais_getdate(lcDataIni)>>&dataFim=<<uf_gerais_getdate(lcDataFim)>>&doc=<<Alltrim(PESQFACT.documento.Value)>>&user=<<ch_userno>>&group=<<Alltrim(ch_grupo)>>&design=<<Alltrim(PESQFACT.produto.VALUE)>>&atend=<<Alltrim(PESQFACT.txtNAtend.VALUE)>>&receita=<<Alltrim(PESQFACT.txtNRec.VALUE)>>&site=<<Alltrim(PESQFACT.loja.Value)>>&ncont=<<ASTR(PESQFACT.nif.value)>>&exportado=-1&obs=<<IIF(EMPTY(Alltrim(PESQFACT.obs.VALUE)),'',Alltrim(PESQFACT.obs.VALUE))>>
			ENDTEXT
			Select ucrsPesqFact
			Goto Top
			If Reccount("ucrsPesqFact")>0
				uf_gerais_chamareport("listagem_pesquisa_facturacao", Alltrim(lcreport))
			Else
				uf_perguntalt_chama("N�o existem dados para Impress�o. Por favor verifique. Obrigado.", "OK", "", 16)
			Endif
		Endif
		regua(2)
	Endif
Endfunc
**
Procedure uf_pesqfact_sel
	If Empty(pesqfact.ecra)
		uf_pesqfact_sair()
	Endif
	If Used("ucrsPesqFact")
		Select ucrsPesqFact
		Do Case
			Case pesqfact.ecra=="FACTURACAO"
				uf_facturacao_chama(Alltrim(ucrsPesqFact.cabstamp))
			Case pesqfact.ecra=="VACINACAO"
				If Used("uCrsVacinacao")
					Select ucrsvacinacao
					Replace nrvenda With Alltrim(ucrsPesqFact.documento)+" - "+astr(ucrsPesqFact.numdoc)
					vacinacao.Refresh
				Endif
		Endcase
		uf_pesqfact_sair()
	Endif
Endproc
**
Procedure uf_pesqfact_novo
	uf_facturacao_chama('')
	uf_facturacao_novo()
	uf_pesqfact_sair()
Endproc
**
Procedure uf_pesqfact_novo_escolhe
	Lparameters lndocumento
	Local lcdocumento
	lcdocumento = Alltrim(lndocumento)
	uf_facturacao_chama('')
	uf_facturacao_novo_escolhe(lcdocumento)
	uf_pesqfact_sair()
Endproc
**
Procedure uf_pesqfact_seltodos
	Local lcpos
	Select ucrsPesqFact
	lcpos = Recno("ucrsPesqFact")
	If pesqfact.menu1.seltodos.lbl.Caption=="Sel. Todos"
		Select ucrsPesqFact
		Replace ucrsPesqFact.escolha With .T. All
		pesqfact.menu1.seltodos.config("De-Sel. Todos", mypath+"\imagens\icons\checked_w.png", "uf_PESQFACT_selTodos")
	Else
		Select ucrsPesqFact
		Replace ucrsPesqFact.escolha With .F. All
		pesqfact.menu1.seltodos.config("Sel. Todos", mypath+"\imagens\icons\unchecked_w.png", "uf_PESQFACT_selTodos")
	Endif
	Select ucrsPesqFact
	Try
		Goto lcpos
	Catch
	Endtry
Endproc
**
Procedure uf_pesqfact_imprimirseq
	Select ucrsPesqFact
	Goto Top
	Scan
		lcsql = ''
		TEXT TO lcsql TEXTMERGE NOSHOW
				exec up_touch_ft '<<ALLTRIM(ucrsPesqFact.cabstamp)>>'
		ENDTEXT
		uf_gerais_actgrelha("", "FT", lcsql)
		lcsql = ''
		TEXT TO lcsql TEXTMERGE NOSHOW
				SELECT respostaDt = isnull((select top 1 DescrResposta from b_dt_resposta WHERE stampDoc = '<<ALLTRIM(ucrsPesqFact.cabstamp)>>' order by DataEnvio),''),* FROM FT2 (nolock) WHERE ft2stamp='<<ALLTRIM(ucrsPesqFact.cabstamp)>>'
		ENDTEXT
		uf_gerais_actgrelha("", "FT2", lcsql)
		lcsql = ''
		TEXT TO lcsql TEXTMERGE NOSHOW
				exec up_facturacao_linhas 0, <<mysite_nr>>, '<<ALLTRIM(ucrsPesqFact.cabstamp)>>'
		ENDTEXT
		uf_gerais_actgrelha("", "FI", lcsql)
		Select ft
		Select ft2
		Select fi
		Goto Top
		If Used("TD")
			fecha("TD")
		Endif
		Select * From uCrsGeralTD Where uCrsGeralTD.ndoc==ft.ndoc And Alltrim(uCrsGeralTD.site)==Alltrim(mysite) Into Cursor TD Readwrite
		Select ft
		Select ft2
		Select fi
		uf_imprimirgerais_chama('FACTURACAO', .T., .F., .F.)
	Endscan
Endproc
**
Function uf_pesqfact_gerarseq
	Public lcexportsyntstamp
	Local lccont2, lccharsep, lcvar, lcnrlin, lcStampBulk
	lcStampBulk = uf_gerais_stamp()
	lccont2 = 1
	lcnrlin = 0
	Store "" To lcexportsyntstamp, lccharsep, lcvar
	Select ucrsPesqFact
	Goto Top
	Scan
		If ucrsPesqFact.escolha=.T.
			lcnrlin = lcnrlin+1
		Endif
	Endscan
	If lcnrlin=0
		uf_perguntalt_chama("N�O SELECIONOU NENHUM DOCUMENTO PARA EXPORTAR."+Chr(13)+"ATEN��O QUE TEM DE SELECIONAR 'PARA EXPORTA��O' NO TOPO DIREITO DA GRELHA DOS DOCUMENTOS.", "OK", "", 48)
		Return .F.
	Endif
	If uf_perguntalt_chama("Deseja criar uma configura��o nova ou utilizar uma existente?"+Chr(13)+"Escolha a op��o pretendida?", "Nova", "Existente")
		lcsql = ""
		TEXT TO lcsql TEXTMERGE NOSHOW
				select
					sel = CAST(0 as Bit)
					,design = 'Data'
					,campo='fdata'
					,ordem = 0
				union
				select
					sel = CAST(0 as Bit)
					,design = 'Documento'
					,campo='nmdoc'
					,ordem = 0
				union
				select
					sel = CAST(0 as Bit)
					,design = 'N� Documento'
					,campo='fno'
					,ordem = 0
				union
				select
					sel = CAST(0 as Bit)
					,design = 'Nome Cliente'
					,campo='nome'
					,ordem = 0
				union
				select
					sel = CAST(0 as Bit)
					,design = 'Nr. Cliente'
					,campo='no'
					,ordem = 0
				union
				select
					sel = CAST(0 as Bit)
					,design = 'Estab. Cliente'
					,campo='estab'
					,ordem = 0
		ENDTEXT
		uf_gerais_actgrelha("", "ucrsEXPORTSYNT", lcsql)
		If uf_exportsynt_chama()
		Else
			Return .F.
		Endif
	Else
		TEXT TO lcsql TEXTMERGE NOSHOW
				select codigo = nome, exportsyntstamp from exportsynt (nolock) where [table]='pesqfact'
		ENDTEXT
		uf_gerais_actgrelha("", "curexportsynt", lcsql)
		Select curexportsynt
		If uf_valorescombo_chama("lcexportsyntstamp", 0, "curexportsynt ", 2, "exportsyntstamp ", "codigo")
			TEXT TO lcsql TEXTMERGE NOSHOW
					SELECT * from exportsynt WHERE exportsyntstamp='<<ALLTRIM(lcexportsyntstamp)>>'
			ENDTEXT
			uf_gerais_actgrelha("", "ucrsEXPORTSYNT", lcsql)
			TEXT TO lcsql TEXTMERGE NOSHOW
					SELECT * from exportsynt_linhas WHERE exportsyntstamp='<<ALLTRIM(lcexportsyntstamp)>>'
			ENDTEXT
			uf_gerais_actgrelha("", "ucrsEXPORTSYNT_linhas", lcsql)
		Else
			Return (.F.)
		Endif
	Endif
	If  .Not. Used("ucrsEXPORTSYNT")
		uf_perguntalt_chama("N�O SELECIONOU NENHUMA CONFIGURA��O.", "OK", "", 48)
		Return .F.
	Endif
	If Reccount("ucrsEXPORTSYNT_linhas")=0
		uf_perguntalt_chama("N�O SELECIONOU NENHUMA CONFIGURA��O.", "OK", "", 48)
		Return .F.
	Endif
	Select ucrsexportsynt
	lccharsep = Alltrim(ucrsexportsynt.sepchar)
	uv_path = Alltrim(uf_gerais_getumvalor("B_Terminal", "LTRIM(RTRIM(pathExport))", "terminal = '"+Alltrim(myterm)+"'"))
	If Empty(uv_path)
		uf_perguntalt_chama('Aten��o!'+Chr(13)+'N�o preencheu o par�metro "Caminho Export.".'+Chr(13)+'Ter� de preencher o mesmo para continuar.', "Ok", "", 48)
		Return .F.
	Endif
	If  .Not. uf_gerais_createdir(uv_path)
		Return .F.
	Endif
	myfilepathseq = uv_path+'\'
	Select * From ucrsPesqFact Where ucrsPesqFact.escolha=.T. Into Cursor ucrsPesqFactAux Readwrite
	Select ucrsPesqFactAux
	regua(0, Reccount("ucrsPesqFactAux"), "A Exportar Documentos...")
	Select ucrsPesqFactAux
	Goto Top
	Scan
		regua(1, lccont2, "A Exportar Documento: "+astr(lccont2))
		lcsql = ''
		TEXT TO lcsql TEXTMERGE NOSHOW
				exec up_touch_ft '<<ALLTRIM(ucrsPesqFactAux.cabstamp)>>'
		ENDTEXT
		uf_gerais_actgrelha("", "FT", lcsql)
		lcsql = ''
		TEXT TO lcsql TEXTMERGE NOSHOW
				SELECT respostaDt = isnull((select top 1 DescrResposta from b_dt_resposta WHERE stampDoc = '<<ALLTRIM(ucrsPesqFactAux.cabstamp)>>' order by DataEnvio),''),* FROM FT2 (nolock) WHERE ft2stamp='<<ALLTRIM(ucrsPesqFactAux.cabstamp)>>'
		ENDTEXT
		uf_gerais_actgrelha("", "FT2", lcsql)
		lcsql = ''
		TEXT TO lcsql TEXTMERGE NOSHOW
				exec up_facturacao_linhas 0, <<mysite_nr>>, '<<ALLTRIM(ucrsPesqFactAux.cabstamp)>>'
		ENDTEXT
		uf_gerais_actgrelha("", "FI", lcsql)

		TEXT TO lcSQL TEXTMERGE NOSHOW
        exec up_print_qrcode_a4 '<<ALLTRIM(ucrsPesqFactAux.cabstamp)>>'
		ENDTEXT
		uf_gerais_actgrelha("", "FTQRCODE", lcsql)

		Select ft
		Select ft2
		Select FTQRCODE
		Select fi
		Goto Top
		If Used("TD")
			fecha("TD")
		Endif
		Select * From uCrsGeralTD Where uCrsGeralTD.ndoc==ft.ndoc And Alltrim(uCrsGeralTD.site)==Alltrim(mysite) Into Cursor TD Readwrite
		myfilenameseq = ""
		Select * From ucrsEXPORTSYNT_linhas Order By ordem Into Cursor ucrsEXPORTSYNT_linhas_aux Readwrite
		Select ucrsEXPORTSYNT_linhas_aux
		Goto Top
		Scan
			lcvar = Alltrim(ucrsEXPORTSYNT_linhas_aux.campo)
			Select ft
			For lncnt = 1 To Fcount()
				lcfield = Field(lncnt)
				If Upper(Alltrim(lcvar))==Upper(Alltrim(lcfield))
					lufval = &lcfield
					lctype = Vartype(lufval)
					If Vartype(lufval)='C'
						lufval = &lcfield
					Endif
					If Vartype(lufval)='N'
						lufval = Str(&lcfield)
					Endif
					If Vartype(lufval)='D'
						lufval = Dtos(&lcfield)
					Endif
					If Vartype(lufval)='T'
						lufval = Left(Ttoc(&lcfield),8)
					Endif
					myfilenameseq = myfilenameseq+Alltrim(lufval)+lccharsep
				Endif
			Endfor
			Select ucrsEXPORTSYNT_linhas_aux
		Endscan
		myfilenameseq = Left(Alltrim(myfilenameseq), Len(Alltrim(myfilenameseq))-1)
		myfilenameseq = Strtran(Alltrim(myfilenameseq), '.', '')
		Select ft
		Select ft2
		Select fi
		myfiletableseq = "FI"
		uf_imprimirgerais_chama('FACTURACAO', .T., .F., .F.)
		lccont2 = lccont2+1
	Endscan
	uf_perguntalt_chama("PROCESSO CONCLU�DO COM SUCESSO. POR FAVOR VERIFIQUE OS FICHEIROS NA DIRECTORIA DEFINIDA."+Chr(10)+Chr(10)+"NOTA: A GERA��O DOS FICHEIROS PODER� AINDA ESTAR EM CURSO.", "OK", "", 64)
	regua(2)
	fecha("ucrsPesqFactAux")
	RELEASE lcexportsyntstamp
Endfunc
**
Function uf_PESQFACT_GerarSeq_LTS
	Public lcexportsyntstamp
	Local lccont2, lccharsep, lcvar, lcnrlin, lcStampBulk 
	lcStampBulk = uf_gerais_stamp()
	lccont2 = 1
	lcnrlin = 0
	Store "" To lcexportsyntstamp, lccharsep, lcvar
	Select ucrsPesqFact
	Goto Top
	Scan
		If ucrsPesqFact.escolha=.T.
			lcnrlin = lcnrlin+1
		Endif
	Endscan
	If lcnrlin=0
		uf_perguntalt_chama("N�O SELECIONOU NENHUM DOCUMENTO PARA EXPORTAR."+Chr(13)+"ATEN��O QUE TEM DE SELECIONAR 'PARA EXPORTA��O' NO TOPO DIREITO DA GRELHA DOS DOCUMENTOS.", "OK", "", 48)
		Return .F.
	Endif
	If uf_perguntalt_chama("Deseja criar uma configura��o nova ou utilizar uma existente?"+Chr(13)+"Escolha a op��o pretendida?", "Nova", "Existente")
		lcsql = ""
		TEXT TO lcsql TEXTMERGE NOSHOW
				select
					sel = CAST(0 as Bit)
					,design = 'Data'
					,campo='fdata'
					,ordem = 0
				union
				select
					sel = CAST(0 as Bit)
					,design = 'Documento'
					,campo='nmdoc'
					,ordem = 0
				union
				select
					sel = CAST(0 as Bit)
					,design = 'N� Documento'
					,campo='fno'
					,ordem = 0
				union
				select
					sel = CAST(0 as Bit)
					,design = 'Nome Cliente'
					,campo='nome'
					,ordem = 0
				union
				select
					sel = CAST(0 as Bit)
					,design = 'Nr. Cliente'
					,campo='no'
					,ordem = 0
				union
				select
					sel = CAST(0 as Bit)
					,design = 'Estab. Cliente'
					,campo='estab'
					,ordem = 0
		ENDTEXT
		uf_gerais_actgrelha("", "ucrsEXPORTSYNT", lcsql)
		If uf_exportsynt_chama()
		Else
			Return .F.
		Endif
	Else
		TEXT TO lcsql TEXTMERGE NOSHOW
				select codigo = nome, exportsyntstamp from exportsynt (nolock) where [table]='pesqfact'
		ENDTEXT
		uf_gerais_actgrelha("", "curexportsynt", lcsql)
		Select curexportsynt
		If uf_valorescombo_chama("lcexportsyntstamp", 0, "curexportsynt ", 2, "exportsyntstamp ", "codigo")
			TEXT TO lcsql TEXTMERGE NOSHOW
					SELECT * from exportsynt WHERE exportsyntstamp='<<ALLTRIM(lcexportsyntstamp)>>'
			ENDTEXT
			uf_gerais_actgrelha("", "ucrsEXPORTSYNT", lcsql)
			TEXT TO lcsql TEXTMERGE NOSHOW
					SELECT * from exportsynt_linhas WHERE exportsyntstamp='<<ALLTRIM(lcexportsyntstamp)>>'
			ENDTEXT
			uf_gerais_actgrelha("", "ucrsEXPORTSYNT_linhas", lcsql)
		Else
			Return (.F.)
		Endif
	Endif
	If  .Not. Used("ucrsEXPORTSYNT")
		uf_perguntalt_chama("N�O SELECIONOU NENHUMA CONFIGURA��O.", "OK", "", 48)
		Return .F.
	Endif
	If Reccount("ucrsEXPORTSYNT_linhas")=0
		uf_perguntalt_chama("N�O SELECIONOU NENHUMA CONFIGURA��O.", "OK", "", 48)
		Return .F.
	Endif
	Select ucrsexportsynt
	lccharsep = Alltrim(ucrsexportsynt.sepchar)
	uv_path =ALLTRIM(uf_gerais_getParameter('ADM0000000015','TEXT')) + "\Cache\" + lcStampBulk
	If Empty(uv_path)
		uf_perguntalt_chama('Aten��o!'+Chr(13)+'N�o preencheu o par�metro "Caminho Export.".'+Chr(13)+'Ter� de preencher o mesmo para continuar.', "Ok", "", 48)
		Return .F.
	Endif
	If  .Not. uf_gerais_createdir(uv_path)
		Return .F.
	Endif
	myfilepathseq = uv_path+"\"
	Select * From ucrsPesqFact Where ucrsPesqFact.escolha=.T. Into Cursor ucrsPesqFactAux Readwrite
	Select ucrsPesqFactAux
	regua(0, Reccount("ucrsPesqFactAux"), "A Exportar Documentos...")
	Select ucrsPesqFactAux
	Goto Top
	Scan
		regua(1, lccont2, "A Exportar Documento: "+astr(lccont2))
		lcsql = ''
		TEXT TO lcsql TEXTMERGE NOSHOW
				exec up_touch_ft '<<ALLTRIM(ucrsPesqFactAux.cabstamp)>>'
		ENDTEXT
		uf_gerais_actgrelha("", "FT", lcsql)
		lcsql = ''
		TEXT TO lcsql TEXTMERGE NOSHOW
				SELECT respostaDt = isnull((select top 1 DescrResposta from b_dt_resposta WHERE stampDoc = '<<ALLTRIM(ucrsPesqFactAux.cabstamp)>>' order by DataEnvio),''),* FROM FT2 (nolock) WHERE ft2stamp='<<ALLTRIM(ucrsPesqFactAux.cabstamp)>>'
		ENDTEXT
		uf_gerais_actgrelha("", "FT2", lcsql)
		lcsql = ''
		TEXT TO lcsql TEXTMERGE NOSHOW
				exec up_facturacao_linhas 0, <<mysite_nr>>, '<<ALLTRIM(ucrsPesqFactAux.cabstamp)>>'
		ENDTEXT
		uf_gerais_actgrelha("", "FI", lcsql)

		TEXT TO lcSQL TEXTMERGE NOSHOW
        exec up_print_qrcode_a4 '<<ALLTRIM(ucrsPesqFactAux.cabstamp)>>'
		ENDTEXT
		uf_gerais_actgrelha("", "FTQRCODE", lcsql)

		Select ft
		Select ft2
		Select FTQRCODE
		Select fi
		Goto Top
		If Used("TD")
			fecha("TD")
		Endif
		Select * From uCrsGeralTD Where uCrsGeralTD.ndoc==ft.ndoc And Alltrim(uCrsGeralTD.site)==Alltrim(mysite) Into Cursor TD Readwrite
		myfilenameseq = ""
		Select * From ucrsEXPORTSYNT_linhas Order By ordem Into Cursor ucrsEXPORTSYNT_linhas_aux Readwrite
		Select ucrsEXPORTSYNT_linhas_aux
		Goto Top
		Scan
			lcvar = Alltrim(ucrsEXPORTSYNT_linhas_aux.campo)
			Select ft
			For lncnt = 1 To Fcount()
				lcfield = Field(lncnt)
				If Upper(Alltrim(lcvar))==Upper(Alltrim(lcfield))
					lufval = &lcfield
					lctype = Vartype(lufval)
					If Vartype(lufval)='C'
						lufval = &lcfield
					Endif
					If Vartype(lufval)='N'
						lufval = Str(&lcfield)
					Endif
					If Vartype(lufval)='D'
						lufval = Dtos(&lcfield)
					Endif
					If Vartype(lufval)='T'
						lufval = Left(Ttoc(&lcfield),8)
					Endif
					myfilenameseq = myfilenameseq+Alltrim(lufval)+lccharsep
				Endif
			Endfor
			Select ucrsEXPORTSYNT_linhas_aux
		Endscan
		myfilenameseq = uf_gerais_removeCharEspecial(Left(Alltrim(myfilenameseq), Len(Alltrim(myfilenameseq))-1))
		myfilenameseq = uf_gerais_removeCharEspecial(Strtran(Alltrim(myfilenameseq), '.', ''))
		Select ft
		Select ft2
		Select fi
		myfiletableseq = "FI"
		uf_imprimirgerais_chama('FACTURACAO', .T., .F., .F.)
		lccont2 = lccont2+1
		regua(1, lccont2, "A enviar documento para FTP: "+astr(lccont2))

		*uf_send_file_ftp("94.46.165.207", "afp", "yfV9xKSRn$im", "public_html/assets/media/docs/original/", myfilepathseq+STRTRAN(STRTRAN(ALLTRIM(myfilenameseq), '/', ''), ' ', '')+".PDF", STRTRAN(STRTRAN(ALLTRIM(myfilenameseq), '/', ''), ' ', '')+".PDF")
		lcsql = ""
		TEXT TO lcsql TEXTMERGE NOSHOW
			exec up_pathFtp '<<ft.ftstamp>>'
		ENDTEXT
		If  .Not. uf_gerais_actgrelha("", "ucrsPath", lcsql)
			uf_perguntalt_chama("N�O FOI POSS�VEL GRAVAR O DOCUMENTO. POR FAVOR CONTACTE O SUPORTE", "OK", "", 16)
			Return .F.
		Endif

		uf_send_file_ftp("ftp.ua878528.serversignin.com", "logitools_plus@ua878528.serversignin.com", "qblBG-FUnn0p", Alltrim(ucrsPath.Path), myfilepathseq+Strtran(Strtran(Alltrim(myfilenameseq), '/', ''), ' ', '')+".PDF", Alltrim(ucrsPath.Nome)+".PDF")
		fecha("ucrsPath")
		regua(1, lccont2, "A registar o documento na BD: "+astr(lccont2))


		*!*	    PUBLIC server, port, db, uid, pw, stringsend
		*!*	    server = "94.46.165.207"
		*!*	    port = "3306"
		*!*	    db = "afp_ws_db"
		*!*	    uid = "afp_sa"
		*!*	    pw = "csgmNh)ueC[D"
		*!*	    mysql = SQLSTRINGCONNECT('Driver={MySQL ODBC 5.3 Unicode Driver};Server='+server+';Port='+port+';Database='+db+';uid='+uid+';Pwd='+pw+';', .T.)
		*!*	    IF VARTYPE(mysql)=='L' .OR. m.mysql<0
		*!*	       MESSAGEBOX('MySQL Connection Failed. Logging will be disabled for this session.', 16, 'MySQL Error', 3500)
		*!*	    ENDIF
		*!*	    TEXT TO lcsql TEXTMERGE NOSHOW
		*!*					call  afp_ws_db.importInvoices(<<ALLTRIM(STR(ft.no))>>,'<<ALLTRIM(ft.nmdoc)>> nr <<ALLTRIM(STR(ft.fno))>> de <<ALLTRIM(STR(MONTH(ft.fdata))))>>-<<ALLTRIM(STR(YEAR(ft.fdata)))>>','<<STRTRAN(STRTRAN(ALLTRIM(myFileNameSeq),'/',''),' ','')+".PDF">>',@out);
		*!*	    ENDTEXT
		*!*	    IF VARTYPE(mysql)<>'L' .AND. m.mysql>=0
		*!*	       SQLEXEC(mysql, lcsql, "")
		*!*	    ENDIF
		*!*	    TEXT TO lcsql TEXTMERGE NOSHOW
		*!*					select * from afp_ws_db.afp_docs where name='<<ALLTRIM(ft.nmdoc)>> nr <<ALLTRIM(STR(ft.fno))>> de <<ALLTRIM(STR(MONTH(ft.fdata))))>>-<<ALLTRIM(STR(YEAR(ft.fdata)))>>'
		*!*	    ENDTEXT
		*!*	    IF VARTYPE(mysql)<>'L' .AND. m.mysql>=0
		*!*	       SQLEXEC(mysql, lcsql, "UCRSRETURNVAL")
		*!*	    ENDIF
		*!*	    IF  .NOT. USED("UCRSRETURNVAL")
		*!*	       uf_perguntalt_chama("O SEGUINTE FICHEIRO N�O FICOU REGISTADO NA BD MYSQL - "+STRTRAN(STRTRAN(ALLTRIM(myfilenameseq), '/', ''), ' ', '')+".PDF", "OK", "", 48)
		*!*	    ENDIF
		*!*	    fecha("UCRSRETURNVAL")
	Endscan
	uf_perguntalt_chama("PROCESSO CONCLU�DO COM SUCESSO. POR FAVOR VERIFIQUE OS FICHEIROS NA DIRECTORIA DEFINIDA."+Chr(10)+Chr(10)+"NOTA: A GERA��O DOS FICHEIROS PODER� AINDA ESTAR EM CURSO.", "OK", "", 64)
	regua(2)
	fecha("ucrsPesqFactAux")
	RELEASE lcexportsyntstamp
Endfunc
**
Procedure uf_pesqfact_sair
	If Type("PESQFACT")<>"U"
		pesqfact.Hide
		pesqfact.Release
	Endif
Endproc
**
Procedure uf_pesqfact_seltodos
	Select ucrsPesqFact
	Goto Top
	Scan
		Replace ucrsPesqFact.escolha With .T.
	Endscan
	Select ucrsPesqFact
	Goto Top
	pesqfact.gridpesq.Refresh
Endproc
**
Procedure uf_pesqfact_ftporreceber
	Local lcsql
	lcsql = ''

	fecha("ucrsftporrec")


	TEXT TO lcsql TEXTMERGE NOSHOW
	exec up_relatorio_encomendas_em_faltaProcessar '','1900-01-01', '3000-01-01'
	ENDTEXT
	uf_gerais_actgrelha("", "ucrsftporrec", lcsql)



	Select ucrsftporrec
	uf_browlist_chama("ucrsftporrec")

	fecha("ucrsftporrec")

Endproc
**


Function uf_pesqfact_scanner

	uv_scan = Alltrim(pesqfact.txtScanner.Value)

	If Empty(uv_scan)
		pesqfact.numdoc.SetFocus()
		Return .F.
	Endif

	Do Case
		Case !Empty(uv_scan) And At(" ", Alltrim(uv_scan)) = 0 And Len(Alltrim(uv_scan)) > 20
			uv_ref = uf_gerais_getRefFromQR(Alltrim(uv_scan))

			If !Empty(uv_ref)

				pesqfact.produto.SetFocus()
				pesqfact.produto.Value = Alltrim(uv_ref)
				pesqfact.menu1.actualizar.Click

			Endif
		Case !Empty(uv_scan) And Len(Alltrim(uv_scan)) = 19

			pesqfact.txtnrec.SetFocus()
			pesqfact.txtnrec.Value = Alltrim(uv_scan)
			pesqfact.menu1.actualizar.Click

		Case !Empty(uv_scan) And uf_gerais_getumvalor("st", "count(*)", "ref = '" + Alltrim(uv_scan) + "'") <> 0

			pesqfact.produto.SetFocus()
			pesqfact.produto.Value = Alltrim(uv_scan)
			pesqfact.menu1.actualizar.Click

		Case !Empty(uv_scan)

			uv_ftstamp = uf_gerais_getFtFromQR(Alltrim(uv_scan))

			If !Empty(uv_ftstamp)

				pesqfact.txtnatend.SetFocus()
				pesqfact.txtnatend.Value = Alltrim(uf_gerais_getumvalor("ft", "u_nratend", "ftstamp = '" + Alltrim(uv_ftstamp) + "'"))
				pesqfact.menu1.actualizar.Click

			Else

				pesqfact.txtnatend.SetFocus()
				pesqfact.txtnatend.Value = Alltrim(uv_scan)
				pesqfact.menu1.actualizar.Click

			Endif

	Endcase

Endfunc

Function uf_pesfact_enviarEmail_bulk
	Public lcexportsyntstamp
	Local lccont2, lccharsep, lcvar, lcnrlin, lcStampBulk
	lcStampBulk = uf_gerais_stamp()
	lccont2 = 1
	lcnrlin = 0
	Store "" To lcexportsyntstamp, lccharsep, lcvar
	Select ucrsPesqFact
	Goto Top
	Scan
		If ucrsPesqFact.escolha=.T.
			lcnrlin = lcnrlin+1
		Endif
	Endscan
	If lcnrlin=0
		uf_perguntalt_chama("N�O SELECIONOU NENHUM DOCUMENTO PARA E	ENVIAR."+Chr(13)+"ATEN��O QUE TEM DE SELECIONAR 'PARA EXPORTA��O' NO TOPO DIREITO DA GRELHA DOS DOCUMENTOS.", "OK", "", 48)
		Return .F.
	Endif
	If uf_perguntalt_chama("Deseja criar uma configura��o nova ou utilizar uma existente?"+Chr(13)+"Escolha a op��o pretendida?", "Nova", "Existente")
		lcsql = ""
		TEXT TO lcsql TEXTMERGE NOSHOW
				select
					sel = CAST(0 as Bit)
					,design = 'Data'
					,campo='fdata'
					,ordem = 0
				union
				select
					sel = CAST(0 as Bit)
					,design = 'Documento'
					,campo='nmdoc'
					,ordem = 0
				union
				select
					sel = CAST(0 as Bit)
					,design = 'N� Documento'
					,campo='fno'
					,ordem = 0
				union
				select
					sel = CAST(0 as Bit)
					,design = 'Nome Cliente'
					,campo='nome'
					,ordem = 0
				union
				select
					sel = CAST(0 as Bit)
					,design = 'Nr. Cliente'
					,campo='no'
					,ordem = 0
				union
				select
					sel = CAST(0 as Bit)
					,design = 'Estab. Cliente'
					,campo='estab'
					,ordem = 0
		ENDTEXT
		uf_gerais_actgrelha("", "ucrsEXPORTSYNT", lcsql)
		If uf_exportsynt_chama()
		Else
			Return .F.
		Endif
	Else
		TEXT TO lcsql TEXTMERGE NOSHOW
				select codigo = nome, exportsyntstamp from exportsynt (nolock) where [table]='pesqfact'
		ENDTEXT
		uf_gerais_actgrelha("", "curexportsynt", lcsql)
		Select curexportsynt
		If uf_valorescombo_chama("lcexportsyntstamp", 0, "curexportsynt ", 2, "exportsyntstamp ", "codigo")
			TEXT TO lcsql TEXTMERGE NOSHOW
					SELECT * from exportsynt WHERE exportsyntstamp='<<ALLTRIM(lcexportsyntstamp)>>'
			ENDTEXT
			uf_gerais_actgrelha("", "ucrsEXPORTSYNT", lcsql)
			TEXT TO lcsql TEXTMERGE NOSHOW
					SELECT * from exportsynt_linhas WHERE exportsyntstamp='<<ALLTRIM(lcexportsyntstamp)>>'
			ENDTEXT
			uf_gerais_actgrelha("", "ucrsEXPORTSYNT_linhas", lcsql)
		Else
			Return (.F.)
		Endif
	Endif
	If  .Not. Used("ucrsEXPORTSYNT")
		uf_perguntalt_chama("N�O SELECIONOU NENHUMA CONFIGURA��O.", "OK", "", 48)
		Return .F.
	Endif
	If Reccount("ucrsEXPORTSYNT_linhas")=0
		uf_perguntalt_chama("N�O SELECIONOU NENHUMA CONFIGURA��O.", "OK", "", 48)
		Return .F.
	Endif
	Select ucrsexportsynt
	lccharsep = Alltrim(ucrsexportsynt.sepchar)
	uv_path =ALLTRIM(uf_gerais_getParameter('ADM0000000015','TEXT')) + "\Cache\" + lcStampBulk
	If Empty(uv_path)
		uf_perguntalt_chama('Aten��o!'+Chr(13)+'N�o preencheu o par�metro "Caminho Export.".'+Chr(13)+'Ter� de preencher o mesmo para continuar.', "Ok", "", 48)
		Return .F.
	Endif
	If  .Not. uf_gerais_createdir(uv_path)
		Return .F.
	Endif
	myfilepathseq = uv_path+"\"
	
	****CHAMA ECRA PARA PREENCHER ASSUNTO E CORPO DOS EMAILS*************
	LOCAL lcAssunto, lcCorpoEmail
	uf_imprimirgeraisBulk_chama()
	
	
	Select * From ucrsPesqFact Where ucrsPesqFact.escolha=.T. Into Cursor ucrsPesqFactAux Readwrite
	Select ucrsPesqFactAux
	regua(0, Reccount("ucrsPesqFactAux"), "A Exportar Documentos...")
	Select ucrsPesqFactAux
	Goto Top
	Scan
		regua(1, lccont2, "A Exportar Documento: "+astr(lccont2))
		lcsql = ''
		TEXT TO lcsql TEXTMERGE NOSHOW
				exec up_touch_ft '<<ALLTRIM(ucrsPesqFactAux.cabstamp)>>'
		ENDTEXT
		uf_gerais_actgrelha("", "FT", lcsql)
		lcsql = ''
		TEXT TO lcsql TEXTMERGE NOSHOW
				SELECT respostaDt = isnull((select top 1 DescrResposta from b_dt_resposta WHERE stampDoc = '<<ALLTRIM(ucrsPesqFactAux.cabstamp)>>' order by DataEnvio),''),* FROM FT2 (nolock) WHERE ft2stamp='<<ALLTRIM(ucrsPesqFactAux.cabstamp)>>'
		ENDTEXT
		uf_gerais_actgrelha("", "FT2", lcsql)
		lcsql = ''
		TEXT TO lcsql TEXTMERGE NOSHOW
				exec up_facturacao_linhas 0, <<mysite_nr>>, '<<ALLTRIM(ucrsPesqFactAux.cabstamp)>>'
		ENDTEXT
		uf_gerais_actgrelha("", "FI", lcsql)

		TEXT TO lcSQL TEXTMERGE NOSHOW
        	exec up_print_qrcode_a4 '<<ALLTRIM(ucrsPesqFactAux.cabstamp)>>'
		ENDTEXT
		uf_gerais_actgrelha("", "FTQRCODE", lcsql)

		Select ft
		Select ft2
		Select FTQRCODE
		Select fi
		Goto Top
		If Used("TD")
			fecha("TD")
		ENDIF
		STORE "" TO lcCorpoEmail , lcAssunto 
		IF USED("uCrsEmailData")
			Select uCrsEmailData
			GO TOP 
			TEXT TO  lcCorpoEmail TEXTMERGE NOSHOW 
				<<uCrsEmailData.body>>
			ENDTEXT 
			lcAssunto = uCrsEmailData.subject 
		ENDIF
	
		Select * From uCrsGeralTD Where uCrsGeralTD.ndoc==ft.ndoc And Alltrim(uCrsGeralTD.site)==Alltrim(mysite) Into Cursor TD Readwrite
		myfilenameseq = ""
		Select * From ucrsEXPORTSYNT_linhas Order By ordem Into Cursor ucrsEXPORTSYNT_linhas_aux Readwrite
		Select ucrsEXPORTSYNT_linhas_aux
		Goto Top
		Scan
			lcvar = Alltrim(ucrsEXPORTSYNT_linhas_aux.campo)
			Select ft
			For lncnt = 1 To Fcount()
				lcfield = Field(lncnt)
				If Upper(Alltrim(lcvar))==Upper(Alltrim(lcfield))
					lufval = &lcfield
					lctype = Vartype(lufval)
					If Vartype(lufval)='C'
						lufval = &lcfield
					Endif
					If Vartype(lufval)='N'
						lufval = Str(&lcfield)
					Endif
					If Vartype(lufval)='D'
						lufval = Dtos(&lcfield)
					Endif
					If Vartype(lufval)='T'
						lufval = Left(Ttoc(&lcfield),8)
					Endif
					myfilenameseq = myfilenameseq+Alltrim(lufval)+lccharsep
				Endif
			Endfor
			Select ucrsEXPORTSYNT_linhas_aux
		Endscan
		myfilenameseq = uf_gerais_removeCharEspecial(Left(Alltrim(myfilenameseq), Len(Alltrim(myfilenameseq))-1))
		myfilenameseq = uf_gerais_removeCharEspecial(Strtran(Alltrim(myfilenameseq), '.', ''))
		Select ft
		Select ft2
		Select fi
		myfiletableseq = "FI"
		uf_imprimirgerais_chama('FACTURACAO', .T., .F., .F.)
		lccont2 = lccont2 + 1
		regua(1, lccont2, "A enviar documento por email: "+astr(lccont2))

		IF !USED("uc_CursorAttachment")
			CREATE CURSOR uc_CursorAttachment(;
			    to C(100), ;
			    email C(100), ;
			    subject C(253), ;
			    body M, ;
			    attachment C(253) ;
			)

		ENDIF 
		LOCAL lcEmailBulk, lcPathBulk
		lcEmailBulk = Alltrim(uf_gerais_getumvalor("b_utentes", "LTRIM(RTRIM(email))", "no= '"+STR(ft.no) +"' and  estab= '"+STR(ft.estab)+"'"))
		lcPathBulk = myfilepathseq+Strtran(Strtran(Alltrim(myfilenameseq), '/', ''), ' ', '')+".PDF"
		Select uc_CursorAttachment
		GO BOTTOM 
		APPEND BLANK
		REPLACE uc_CursorAttachment.to 			WITH lcEmailBulk
		REPLACE uc_CursorAttachment.email 		WITH lcEmailBulk
		REPLACE uc_CursorAttachment.subject 	WITH lcAssunto 
		REPLACE uc_CursorAttachment.body 		WITH lcCorpoEmail
		REPLACE uc_CursorAttachment.attachment 	WITH lcPathBulk
		

	ENDSCAN
	uf_gerais_sendmail_bulk("uc_CursorAttachment", .T.)
	regua(2)
	fecha("ucrsPesqFactAux")
	RELEASE lcexportsyntstamp
Endfunc
