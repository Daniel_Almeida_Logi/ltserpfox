**
FUNCTION uf_reimppos_chama
 LPARAMETERS lcpainel
 lcsql = ""
 TEXT TO lcsql TEXTMERGE NOSHOW
		 select nmdoc
		 from td (nolock)
		 where u_tipodoc!=1 and u_tipodoc!=5
		 
		 union all 
		 
		 SELECT 'Recibo Normal' as nmdoc
		 
		 order by nmdoc
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "ucrsComboDocPOS", lcsql)
    uf_perguntalt_chama("N�O FOI POSS�VEL ENCONTRAR DOCUMENTOS.", "OK", "", 16)
    RETURN .F.
 ENDIF
 uf_reimppos_carregadados(.T., lcpainel)
 IF TYPE("REIMPPOS")=="U"
    DO FORM REIMPPOS
 ELSE
    reimppos.show
 ENDIF
 IF TYPE("reimppos.menu1.actualizar")=="U"
    reimppos.menu1.adicionaopcao("opcoes", "Op��es", mypath+"\imagens\icons\opcoes_w.png", "", "")
    reimppos.menu1.adicionaopcao("actualizar", "Actualizar", mypath+"\imagens\icons\actualizar_w.png", "uf_REIMPPOS_CarregaDados", "")
    reimppos.menu1.adicionaopcao("talao", "Tal�o", mypath+"\imagens\icons\imprimir_w.png", "uf_imprimirPOS_ValImpTalao", "")
    reimppos.menu1.adicionaopcao("verso1", "Verso 1", mypath+"\imagens\icons\imprimir_w.png", "uf_imprimirpos_ValimpRec with 1", "")
    reimppos.menu1.adicionaopcao("verso2", "Verso 2", mypath+"\imagens\icons\imprimir_w.png", "uf_imprimirpos_ValimpRec with 2", "")
    reimppos.menu1.adicionaopcao("atendimento", "Atendimento", mypath+"\imagens\icons\imprimir_w.png", "uf_imprimirPOS_reImpAtend", "")
    reimppos.menu1.adicionaopcao("navegar", "Navegar", mypath+"\imagens\icons\doc_seta_w.png", "uf_reimppos_Navegar", "F7")
    **reimppos.menu_opcoes.adicionaopcao("prevVerso1", "Prev. Verso 1", "", "uf_imprimirpos_ValPrevRec WITH 1")
    **reimppos.menu_opcoes.adicionaopcao("prevVerso2", "Prev. Verso 2", "", "uf_imprimirpos_ValPrevRec WITH 2")
    reimppos.menu_opcoes.adicionaopcao("prevVerso1", "Prev. Verso 1", "", "uf_imprimirpos_ValimpRec WITH 1, .T.")
    reimppos.menu_opcoes.adicionaopcao("prevVerso2", "Prev. Verso 2", "", "uf_imprimirpos_ValimpRec WITH 2, .T.")
    reimppos.menu_opcoes.adicionaopcao("psico", "Psico", "", "uf_imprimirpos_ValImpPrevPsico with .f.")
    reimppos.menu_opcoes.adicionaopcao("prevPsico", "Prev. Psico", "", "uf_imprimirpos_ValImpPrevPsico with .t.")
    **reimppos.menu_opcoes.adicionaopcao("prevTalao", "Prev. Tal�o", "", "uf_imprimirPOS_ValPrevTalao")
    reimppos.menu_opcoes.adicionaopcao("prevTalao"	, "Prev. Tal�o"	, "", "uf_imprimirPOS_ValImpTalao WITH .T.")
    reimppos.menu_opcoes.adicionaopcao("talaoOferta", "Tal�o Oferta", "", "uf_imprimirPOS_reImpTalaoOf")
    IF (VARTYPE(mytpastamp)<>"U")
       reimppos.menu_opcoes.adicionaopcao("ultimoReciboTpa", "Reimp. Tal�o TPA", "", "uf_gestaocaixas_mensagemMenu with 1")
    ENDIF
 ENDIF
 reimppos.txtdataini.value = uf_gerais_getdate((DATETIME()+((difhoraria-24)*3600)))
 reimppos.txtdatafim.value = uf_gerais_getdate(DATETIME()+(difhoraria*3600))
 reimppos.lblregistos.caption = ALLTRIM(STR(RECCOUNT("ucrsPesqVendas")))+" Resultados"
 IF UPPER(ALLTRIM(lcpainel))=="FT"
    SELECT ft
    IF  .NOT. EMPTY(ft.fno)
       reimppos.txtnvenda.value = astr(ft.fno)
    ENDIF
    IF  .NOT. EMPTY(ft.no)
       reimppos.txtno.value = astr(ft.no)
    ENDIF
    IF  .NOT. EMPTY(ft.estab)
       reimppos.txtestab.value = astr(ft.estab)
    ENDIF
    IF  .NOT. EMPTY(ft.nmdoc)
       reimppos.cmbdocumento.value = ALLTRIM(ft.nmdoc)
    ENDIF
    IF  .NOT. EMPTY(ft2.u_receita)
       reimppos.txtreceita.value = ALLTRIM(ft2.u_receita)
    ENDIF
    IF  .NOT. EMPTY(ft.fdata)
       reimppos.txtdataini.value = uf_gerais_getdate(ft.fdata)
       reimppos.txtdatafim.value = uf_gerais_getdate(ft.fdata)
    ENDIF
 ENDIF
 reimppos.grdvendas.setfocus
 reimppos.txtnatend.setfocus
 reimppos.refresh
 uf_reimppos_escondecolunas()
ENDFUNC
**
PROCEDURE uf_reimppos_escondecolunas
 IF (uf_gerais_getparameter('ADM0000000281', 'BOOL'))
    IF (VARTYPE(reimppos.grdvendas)<>"U")
       IF (VARTYPE(reimppos.grdvendas.vendnm)<>"U")
          reimppos.grdvendas.vendnm.visible = .F.
       ENDIF
    ENDIF
 ENDIF
ENDPROC
**
FUNCTION uf_reimppos_carregadados
 LPARAMETERS tcbool, lcpainel
 LOCAL lcsql, lcnvenda, lcno, lcestab, lcnmdoc, lcrec, lcdataini, lcdatafim, lcnatend
 STORE 0 TO lcnvenda, lcno, lcnatend
 STORE -1 TO lcestab
 STORE '' TO lcsql, lcnmdoc, lcrec
 lcdataini = uf_gerais_getdate(DATE()-1, "SQL")
 lcdatafim = uf_gerais_getdate(DATE(), "SQL")
 regua(0, 5, "A pesquisar as vendas...")
 regua(1, 1, "A verificar par�metros...")
 IF tcbool==.T.
    IF UPPER(ALLTRIM(lcpainel))=="FT"
       SELECT ft
       IF  .NOT. EMPTY(ft.fno)
          lcnvenda = astr(ft.fno)
       ENDIF
       IF  .NOT. EMPTY(ft.no)
          lcno = astr(ft.no)
       ENDIF
       IF  .NOT. EMPTY(ft.estab)
          lcestab = astr(ft.estab)
       ENDIF
       IF  .NOT. EMPTY(ft.nmdoc)
          lcnmdoc = ALLTRIM(ft.nmdoc)
       ENDIF
       IF  .NOT. EMPTY(ft2.u_receita)
          lcrec = ALLTRIM(ft2.u_receita)
       ENDIF
       IF  .NOT. EMPTY(ft.fdata)
          lcdataini = uf_gerais_getdate(ft.fdata, "SQL")
          lcdatafim = uf_gerais_getdate(ft.fdata, "SQL")
       ENDIF
    ENDIF
    lcsql = ""
    TEXT TO lcsql TEXTMERGE NOSHOW
			exec up_touch_reImprimir '',<<lcNVenda>>,'',<<lcNo>>,<<lcEstab>>,'<<uf_gerais_getDate(lcDataIni,"SQL")>>','<<uf_gerais_getDate(lcDataFim,"SQL")>>','<<lcNmDoc>>','<<lcRec>>', '', '<<mySite>>'
    ENDTEXT
    IF TYPE("REIMPPOS")=="U"
       regua(1, 2, "A pesquisar os dados...")
       IF  .NOT. uf_gerais_actgrelha("", "ucrsPesqVendas", lcsql)
          uf_perguntalt_chama("N�O FOI POSSIVEL ACTUALIZAR A LISTA DE VENDAS. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
          RETURN .F.
       ENDIF
    ELSE
       regua(1, 3, "A pesquisar os dados...")
       uf_gerais_actgrelha("REIMPPOS.grdVendas", "ucrsPesqVendas", lcsql)
    ENDIF
 ELSE
    IF  .NOT. EMPTY(reimppos.txtnvenda.value)
       lcnvenda = reimppos.txtnvenda.value
    ENDIF
    IF  .NOT. EMPTY(reimppos.txtnatend.value)
       lcnatend = reimppos.txtnatend.value
    ENDIF
    IF  .NOT. EMPTY(reimppos.txtno.value)
       lcno = reimppos.txtno.value
    ENDIF
    IF  .NOT. EMPTY(reimppos.txtestab.value)
       lcestab = reimppos.txtestab.value
    ENDIF
    lcdataini = uf_gerais_getdate(reimppos.txtdataini.value, "SQL")
    lcdatafim = uf_gerais_getdate(reimppos.txtdatafim.value, "SQL")
    lcsql = ""
    TEXT TO lcsql TEXTMERGE NOSHOW
			exec up_touch_reImprimir '<<ALLTRIM(REIMPPOS.txtProduto.value)>>', '<<lcNVenda>>', '<<ALLTRIM(REIMPPOS.txtNome.value)>>',<<lcNo>>,<<lcEstab>>,'<<uf_gerais_getDate(lcDataIni,"SQL")>>','<<uf_gerais_getDate(lcDataFim,"SQL")>>','<<ALLTRIM(REIMPPOS.cmbDocumento.value)>>','<<ALLTRIM(REIMPPOS.txtReceita.value)>>', '<<lcNAtend>>', '<<mySite>>'
    ENDTEXT
    regua(1, 4, "A pesquisar os dados...")
    uf_gerais_actgrelha("REIMPPOS.grdVendas", "ucrsPesqVendas", lcsql)
    reimppos.lblregistos.caption = ALLTRIM(STR(RECCOUNT("ucrsPesqVendas")))+" Resultados"
    reimppos.refresh
 ENDIF
 regua(1, 5, "Pesquisa Terminada")
 regua(2)
ENDFUNC
**
PROCEDURE uf_reimppos_actualizadadoslinha
 SELECT ucrspesqvendas
 reimppos.txtplano.value = ucrspesqvendas.plano
 reimppos.txtl.value = ucrspesqvendas.u_lote
 reimppos.txtnr.value = ucrspesqvendas.u_nslote
 reimppos.txtl2.value = ucrspesqvendas.u_lote2
 reimppos.txtnr2.value = ucrspesqvendas.u_nslote2
ENDPROC
**
PROCEDURE uf_reimppos_navegar
 LOCAL lcvalida
 STORE .F. TO lcvalida
 PUBLIC myftintroducao, myftalteracao
 FOR EACH mf IN _SCREEN.forms
    IF UPPER(ALLTRIM(mf.name))=="ATENDIMENTO"
       lcvalida = .T.
       EXIT
    ENDIF
 ENDFOR
 IF  .NOT. lcvalida
    IF myftintroducao==.T. .OR. myftalteracao==.T.
       uf_perguntalt_chama("O ECR� DE FACTURA��O ENCONTRA-SE EM MODO DE INTRODU��O OU EDI��O.", "OK", "", 48)
    ELSE
       SELECT ucrspesqvendas
       IF  .NOT. EMPTY(ucrspesqvendas.ftstamp)
          uf_facturacao_chama(ALLTRIM(ucrspesqvendas.ftstamp))
       ENDIF
    ENDIF
 ELSE
    uf_perguntalt_chama("N�O PODE ACEDER AO ECRA DE FACTURA��O COM O ECRA DE ATENDIMENTO ABERTO.", "OK", "", 48)
 ENDIF
ENDPROC
**
PROCEDURE uf_reimppos_sair
 IF USED("uCrsPesqVendas")
    fecha("uCrsPesqVendas")
 ENDIF
 IF USED("uCrsFt")
    fecha("uCrsFt")
 ENDIF
 IF USED("uCrsFt2")
    fecha("uCrsFt2")
 ENDIF
 IF USED("uCrsFi")
    fecha("uCrsFi")
 ENDIF
 reimppos.hide
 reimppos.release
 RELEASE reimppos
ENDPROC
**


FUNCTION uf_reimppos_scanner

	uv_scan = ALLTRIM(reimppos.txtScanner.value)

    IF EMPTY(uv_scan)
        reimppos.txtnatend.setFocus()
        RETURN .F.
    ENDIF

    DO CASE
        CASE !EMPTY(uv_scan) AND AT(" ", ALLTRIM(uv_scan)) = 0 AND LEN(ALLTRIM(uv_scan)) > 20
            uv_ref = uf_gerais_getRefFromQR(ALLTRIM(uv_scan))

            IF !EMPTY(uv_ref)

                reimppos.txtProduto.setFocus()
                reimppos.txtProduto.value = ALLTRIM(uv_ref)
                reimppos.menu1.actualizar.click			

            ENDIF
        CASE !EMPTY(uv_scan) AND LEN(ALLTRIM(uv_scan)) = 19

            reimppos.txtReceita.setFocus()
            reimppos.txtReceita.value = ALLTRIM(uv_scan)
            reimppos.menu1.actualizar.click		            

        CASE !EMPTY(uv_scan)

            reimppos.txtNAtend.setFocus()
            reimppos.txtNAtend.value = ALLTRIM(uv_scan)
            reimppos.menu1.actualizar.click		

    ENDCASE

ENDFUNC