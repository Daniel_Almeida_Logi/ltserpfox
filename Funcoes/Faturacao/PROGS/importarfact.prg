 PUBLIC myentidadefact
 
**
FUNCTION uf_importarfact_chama
 LPARAMETERS lcno, lctipodoc
 LOCAL lcsql, lcefarmacia
 STORE .F. TO lcefarmacia

 PUBLIC myOrderImportarFact
 
 IF USED("ucrsImportDocsEntidades")
    fecha("ucrsImportDocsEntidades")
 ENDIF
 myentidadefact = .F.
 lcsql = ""
 SELECT ft
 TEXT TO lcsql TEXTMERGE NOSHOW
		exec up_perfis_validaAcessoDocs_Importados '<<ch_userno>>', '<<ALLTRIM(ch_grupo)>>', 'Visualizar', 'BO,FT,FO', 0, '<<ALLTRIM(mySite)>>', '<<ALLTRIM(ft.nmdoc)>>', 'FT'
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "uCrsListaDocs", lcsql)
    uf_perguntalt_chama("OCORREU UMA ANOMALIA NA VERIFICA��O DA LISTAGEM DE DOCUMENTOS. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
    RETURN .F.
 ENDIF
 SELECT ucrslistadocs
 IF RECCOUNT("uCrsListaDocs")=0
    TEXT TO lcsql TEXTMERGE NOSHOW
			exec up_perfis_validaAcessoDocs <<ch_userno>>,'<<ch_grupo>>','visualizar','BO,FT,FO', 0, '<<ALLTRIM(mysite)>>'
    ENDTEXT
    IF  .NOT. uf_gerais_actgrelha("", "uCrsListaDocs", lcsql)
       uf_perguntalt_chama("OCORREU UMA ANOMALIA NA VERIFICA��O DA LISTAGEM DE DOCUMENTOS. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
       RETURN .F.
    ENDIF
 ENDIF
 IF  .NOT. USED("uCrsListaDocs")
    uf_perguntalt_chama("OCORREU UMA ANOMALIA NA VERIFICA��O DA LISTAGEM DE DOCUMENTOS. POR FAVOR CAONTACTE O SUPORTE.", "OK", "", 16)
    RETURN .F.
 ELSE
    IF uf_gerais_getparameter("ADM0000000142", "BOOL")
       SELECT ucrslistadocs
       APPEND BLANK
       REPLACE nomedoc WITH 'Reserva de Cliente'
    ENDIF
    lcsql = ""
 ENDIF
 SELECT ucrse1
 IF UPPER(ALLTRIM(ucrse1.tipoempresa))=='FARMACIA'
    lcefarmacia = .T.
 ENDIF
 IF lcno<199 .AND. (lctipodoc==3 .OR. lctipodoc==2) .AND. lcefarmacia==.T.
    myentidadefact = .T.
    SELECT ucrslojas
    LOCATE FOR ALLTRIM(UPPER(ucrslojas.site))==ALLTRIM(UPPER(mysite))
    IF FOUND()
       SELECT ucrslistadocs
       GOTO TOP
       SCAN FOR ucrslistadocs.ndoc<>ucrslojas.serie_ent .AND. ucrslistadocs.ndoc<>ucrslojas.serie_entsns  AND !like('Factura SNS*',ucrslistadocs.resumo)
          DELETE
       ENDSCAN
    ENDIF
 ENDIF
 IF  .NOT. USED("ucrsImportDocs")
    CREATE CURSOR ucrsImportDocs (qttot N(9), operador C(70), hora C(20), tipo C(254), estado C(20), cabstamp C(25), linstamp C(25), copiar N(1), tipodoc C(254), documento C(254), data D, valor N(9, 2))
 ENDIF
 IF  .NOT. USED("ucrsImportDocsEntidades")
    CREATE CURSOR ucrsImportDocsEntidades (qttot N(9), operador C(70), hora C(20), tipo C(254), estado C(20), cabstamp C(25), linstamp C(25), copiar N(1), tipodoc C(254), documento C(254), data D, valor N(9, 2), descricaocred C(60), valorcred N(9, 2), tabiva N(5, 0))
 ENDIF
 IF  .NOT. USED("ucrsImportLinhasDocs")
    CREATE CURSOR ucrsImportLinhasDocs (stamp C(32), copiar N(1), ref C(18), design C(60), epv N(9, 2), qtt N(9, 0), etiliquido N(9, 2), qttmov N(9, 0), cabstamp C(25), tipo C(254), documento C(254))
 ENDIF
 SELECT ucrslistadocs
 GOTO TOP
 DO FORM IMPORTARFACT WITH IIF((lcno<199 AND UPPER(mypaisconfsoftw)=='PORTUGAL') .AND. (lctipodoc==3 .OR. lctipodoc==2) .AND. lcefarmacia==.T., .T., .F.)
ENDFUNC
**
PROCEDURE uf_importarfact_filtradocs
 SELECT ucrslistadocs
 IF  .NOT. EMPTY(ALLTRIM(importarfact.text1.value))
    SET FILTER TO LIKE('*'+ALLTRIM(UPPER(importarfact.text1.value))+'*', ALLTRIM(UPPER(ucrslistadocs.nomedoc)))
 ELSE
    SET FILTER TO
 ENDIF
 importarfact.gridlistadocs.refresh
ENDPROC
**
FUNCTION uf_importarfact_actualizadocs
 LOCAL lcdataini, lcdatafim, lcno, lcserie, lcsite, lcprod, lnEstab 
 STORE 0 TO lcno
 STORE -1 TO lnEstab

 IF USED("FT")
    SELECT ft
    lcno = ft.no
    lnEstab = ft.estab
 ENDIF
 lcdataini = uf_gerais_getdate(importarfact.datainicio.value, "SQL")
 lcdatafim = uf_gerais_getdate(importarfact.datafim.value, "SQL")
 lcsite = importarfact.local.value
 lcprod = ''
 IF VARTYPE(importarfact.pageframe1)<>'U' .AND. VARTYPE(importarfact.pageframe1.page1)<>'U'
    lcprod = ALLTRIM(importarfact.pageframe1.page1.txtprod.value)
 ENDIF
 SELECT ucrslistadocs
 lcsql = ""
 TEXT TO lcsql TEXTMERGE NOSHOW
		exec up_facturacao_ImportActualizaDocs
			<<lcNo>>			
			,'<<Alltrim(importarfact.txtDocs.value)>>'
			,<<IMPORTARFACT.Forn>>
			,'<<Alltrim(lcDataIni)>>'
			,'<<Alltrim(lcDataFim)>>'
			,<<IMPORTARFACT.Aberto>>
			,'<<lcSite>>'
			,'<<Alltrim(lcProd)>>'
			,<<lnEstab>> 
 ENDTEXT
 uf_gerais_actgrelha("IMPORTARFACT.pageframe1.page1.gridPesq", "ucrsImportDocs", lcsql)
 IF importarfact.pageframe1.activepage==3 .OR. importarfact.pageframe1.activepage==4
    uf_importarfact_actualizalinhasdocs()
 ENDIF
 IF  .NOT. USED("ucrsImportDocs")
    RETURN .F.
 ENDIF
 SELECT ucrslistadocs
 importarfact.pageframe1.page1.refresh
ENDFUNC
**
FUNCTION uf_importarimportarfact_formatanomerectificacao
 LPARAMETERS lcnome
 LOCAL lcpos, lctexto
 STORE 0 TO lcpos
 STORE '' TO lctexto
 lcnome = ALLTRIM(lcnome)
 lcpos = AT("Tipo", lcnome)
 IF (lcpos>0)
    lctexto = " "+SUBSTR(lcnome, lcpos, lcpos+6)
    RETURN lctexto
 ENDIF
 lcpos = AT("Remunera��o", lcnome)
 IF (lcpos>0)
    lctexto = " R.E."
 ENDIF
 RETURN lctexto
ENDFUNC
**
PROCEDURE uf_importarfact_actualizalinhasdocs
 LOCAL lcsql, lcsite
 LOCAL lcsite, lcoperador, lcop, lchora, lctipo, lcestado, lccabstamp, lcestado, lcdocumento, lctipodoc, lcdata, lctipodocabrev
 uv_cabs = ''
 SELECT cabstamp FROM ucrsImportDocs WITH (BUFFERING=.T.) WHERE copiar=1 INTO CURSOR uc_cabSel
 SELECT uc_cabsel
 GOTO TOP
 SCAN
    uv_cabs = uv_cabs+IIF( .NOT. EMPTY(uv_cabs), ';', '')+uc_cabsel.cabstamp
    SELECT uc_cabsel
 ENDSCAN
 lcsql = ""
 lcsite = importarfact.local.value
 TEXT TO lcsql TEXTMERGE NOSHOW
		exec up_facturacao_ImportActualizaLinhas <<IMPORTARFACT.qttMaiorZero>>, <<IMPORTARFACT.qttMovMenorQtt>>, '<<Alltrim(uv_cabs)>>', '<<lcSite>>'
 ENDTEXT
 uf_gerais_actgrelha("IMPORTARFACT.pageframe1.page1.grid1", "ucrsImportLinhasDocs", lcsql)
 SELECT ucrsimportdocsentidades
 GOTO TOP
 SCAN
    DELETE
 ENDSCAN
 SELECT ucrslistadocs
 lcserie = ucrslistadocs.ndoc
 SELECT * FROM ucrsImportDocs WITH (BUFFERING=.T.) INTO CURSOR uc_cabSel READWRITE
 SELECT ucrsimportlinhasdocs
 GOTO TOP
 SCAN
    lctipodocabrev = uf_importarimportarfact_formatanomerectificacao(ucrsimportlinhasdocs.design)
    SELECT uc_cabsel
    LOCATE FOR ALLTRIM(uc_cabsel.cabstamp)==ALLTRIM(ucrsimportlinhasdocs.cabstamp)
    SELECT ucrsimportdocsentidades
    APPEND BLANK
    REPLACE ucrsimportdocsentidades.qttot WITH ucrsimportlinhasdocs.qtt
    REPLACE ucrsimportdocsentidades.operador WITH uc_cabsel.operador
    REPLACE ucrsimportdocsentidades.hora WITH uc_cabsel.hora
    REPLACE ucrsimportdocsentidades.tipo WITH uc_cabsel.tipo
    REPLACE ucrsimportdocsentidades.estado WITH uc_cabsel.estado
    REPLACE ucrsimportdocsentidades.linstamp WITH ucrsimportlinhasdocs.fistamp
    REPLACE ucrsimportdocsentidades.copiar WITH ucrsimportlinhasdocs.copiar
    REPLACE ucrsimportdocsentidades.tipodoc WITH ucrsimportlinhasdocs.design
    REPLACE ucrsimportdocsentidades.documento WITH ucrsimportlinhasdocs.documento
    REPLACE ucrsimportdocsentidades.data WITH uc_cabsel.data
    REPLACE ucrsimportdocsentidades.valor WITH ucrsimportlinhasdocs.etiliquido
    REPLACE ucrsimportdocsentidades.descricaocred WITH "Retifica��o � Fatura N� "+ALLTRIM(uc_cabsel.documento)+" S�rie "+astr(lcserie)+lctipodocabrev+" ("+uf_gerais_getdate(uc_cabsel.data)+")"
    REPLACE ucrsimportdocsentidades.valorcred WITH 0
    REPLACE ucrsimportdocsentidades.tabiva WITH ucrsimportlinhasdocs.tabiva
    REPLACE ucrsimportdocsentidades.cabstamp WITH ucrsimportlinhasdocs.cabstamp
 ENDSCAN
 SELECT ucrsimportdocsentidades
 GOTO TOP
 importarfact.pageframe1.page1.grident.refresh
 IF USED("uc_linSelFac")
    SELECT uc_linselfac
    GOTO TOP
    SCAN
       SELECT ucrsimportlinhasdocs
       LOCATE FOR ALLTRIM(ucrsimportlinhasdocs.stamp)=ALLTRIM(uc_linselfac.stamp)
       IF FOUND()
          REPLACE ucrsimportlinhasdocs.copiar WITH 1
       ELSE
          SELECT uc_linselfac
          DELETE
       ENDIF
       SELECT uc_linselfac
    ENDSCAN
 ENDIF
 IF USED("uc_linSelEnt")
    SELECT uc_linselent
    GOTO TOP
    SCAN
       SELECT ucrsimportdocsentidades
       LOCATE FOR ALLTRIM(ucrsimportdocsentidades.linstamp)=ALLTRIM(uc_linselent.stamp)
       IF FOUND()
          REPLACE ucrsimportdocsentidades.copiar WITH 1
       ELSE
          SELECT uc_linselent
          DELETE
       ENDIF
       SELECT uc_linselent
    ENDSCAN
 ENDIF
 SELECT ucrsimportdocs
ENDPROC
**
FUNCTION uf_importarfact_importarlinhas

 IF  .NOT. USED("ucrsImportDocs") .OR.  .NOT. USED("ucrsImportLinhasDocs")
    RETURN .F.
 ENDIF
 regua(0, 1, "A importar linhas....")
 LOCAL lcvalida
 STORE .F. TO lcvalida
 importarfact.lordem = uf_importarfact_maxlordem()
 LOCAL lcvalida
 STORE .F. TO lcvalida
 uv_mudoucab = .T.
 uv_cabstamp = ""
  	
 SELECT ucrsimportlinhasdocs
 SCAN FOR ucrsimportlinhasdocs.copiar==1
    IF EMPTY(uv_cabstamp) .OR. (ALLTRIM(uv_cabstamp)<>ALLTRIM(ucrsimportlinhasdocs.cabstamp))
       uv_mudoucab = .T.
       uv_cabstamp = ucrsimportlinhasdocs.cabstamp
    ELSE
       uv_mudoucab = .F.
    ENDIF
    IF importarfact.incluinome==1 .AND. uv_mudoucab
       SELECT * FROM ucrsImportDocs WITH (BUFFERING=.T.) WHERE ALLTRIM(ucrsimportdocs.cabstamp)=ALLTRIM(ucrsimportlinhasdocs.cabstamp) INTO CURSOR uc_cabSel READWRITE
       SELECT fi
       APPEND BLANK
       REPLACE fi.lordem WITH importarfact.lordem
       IF UPPER(ALLTRIM(ucrse1.pais)) == 'PORTUGAL'
	       IF ALLTRIM(uc_cabsel.tipodoc)=='Venda a Dinheiro'
	          REPLACE fi.design WITH 'Fatura'+' Nr. '+ALLTRIM(uc_cabsel.documento)+'/3 de '+uf_gerais_getdate(uc_cabsel.data)
	       ELSE
	          REPLACE fi.design WITH ALLTRIM(uc_cabsel.tipodoc)+' Nr. '+ALLTRIM(uc_cabsel.documento)+' de '+uf_gerais_getdate(uc_cabsel.data)
	       ENDIF
	   ELSE
	   		SELECT ucrsimportlinhasdocs 
	   		select * from ucrsimportlinhasdocs WITH (BUFFERING=.T.) where copiar=0 INTO CURSOR ucrslinnsel READWRITE
			sele ucrslinnsel 
			IF reccount("ucrslinnsel")=0 
				SELECT fi
		   		REPLACE fi.design WITH 'Anula��o de ' + alltrim(uc_cabsel.tiposaft) + ' ' + alltrim(uc_cabsel.ndoc) +  '/' + alltrim(uc_cabsel.documento) +' de '+uf_gerais_getdate(uc_cabsel.data)
		   	ELSE
		   		SELECT fi
		   		REPLACE fi.design WITH 'Retifica��o de ' + alltrim(uc_cabsel.tiposaft) + ' ' + alltrim(uc_cabsel.ndoc) +  '/' + alltrim(uc_cabsel.documento)+' de '+uf_gerais_getdate(uc_cabsel.data)
		   	ENDIF 
		   	fecha("ucrslinnsel")
		   	SELECT ucrsimportlinhasdocs 
	   ENDIF 
       lcstamp = uf_gerais_stamp()
       SELECT fi
       REPLACE fi.fistamp WITH lcstamp
       REPLACE fi.nmdoc WITH ALLTRIM(ft.nmdoc)
       REPLACE fi.ndoc WITH ft.ndoc
       REPLACE fi.fno WITH ft.fno
       REPLACE fi.iva WITH uf_gerais_gettabelaiva(1, "TAXA")
       REPLACE fi.tabiva WITH 1
       REPLACE fi.armazem WITH myarmazem
    ENDIF

    IF ALLTRIM(ucrsimportlinhasdocs.tipo)="FT"   
       uf_importarfact_crialinhas_individuaisftft(ucrsimportlinhasdocs.stamp)
    ENDIF
    IF ALLTRIM(ucrsimportlinhasdocs.tipo)="BO"   
       uf_importarfact_crialinhas_individuaisboft(ucrsimportlinhasdocs.stamp)
    ENDIF
    IF ALLTRIM(ucrsimportlinhasdocs.tipo)="FO"   
       uf_importarfact_crialinhas_individuaisfoft(ucrsimportlinhasdocs.stamp)
    ENDIF
    IF ucrsimportdocs.tipo="TARV"
       uf_importarfact_crialinhas_individuaistarvft(ALLTRIM(ucrsimportlinhasdocs.stamp), ALLTRIM(ucrsimportdocs.cabstamp))
    ENDIF
    lcvalida = .T.
    SELECT ucrsimportlinhasdocs
 ENDSCAN
 regua(2)

  
  
 IF lcvalida 
    SELECT FI
    GO TOP
    SCAN

        uf_atendimento_actvaloresfi(.T.)

        SELECT FI
    ENDSCAN

   

    
    
    SELECT FI
    GO TOP

    uf_atendimento_acttotaisft()   
    uf_importarfact_sair()
    uf_facturacao_alertapic()
	
	

    facturacao.refresh    
 
 ENDIF
ENDFUNC
**
FUNCTION uf_importarfact_seltodos
 LPARAMETERS lcbool
 DO CASE
    CASE importarfact.pageframe1.activepage=1
       SELECT ucrsimportdocs
       GOTO TOP
       SCAN
          IF lcbool
             REPLACE ucrsimportdocs.copiar WITH 1
          ELSE
             REPLACE ucrsimportdocs.copiar WITH 0
          ENDIF
       ENDSCAN
       GOTO TOP
    CASE importarfact.pageframe1.activepage=3
       SELECT ucrsimportlinhasdocs
       GOTO TOP
       SCAN
          IF lcbool
             REPLACE ucrsimportlinhasdocs.copiar WITH 1
          ELSE
             REPLACE ucrsimportlinhasdocs.copiar WITH 0
          ENDIF
       ENDSCAN
       GOTO TOP
    OTHERWISE
       RETURN .F.
 ENDCASE
 IF lcbool
    importarfact.menu1.seltodos.config("Des. Todos", mypath+"\imagens\icons\checked_w.png", "uf_ImportarFact_SelTodos with .f.", "T")
 ELSE
    importarfact.menu1.seltodos.config("Sel. Todos", mypath+"\imagens\icons\unchecked_w.png", "uf_ImportarFact_SelTodos with .t.", "T")
 ENDIF
ENDFUNC
**
FUNCTION uf_importarfact_importar

 LOCAL lcorigem, lcdestino, lcvalida
 LOCAL lcstamp
 IF (myentidadefact)
    uf_perguntalt_chama("Por favor escolha uma linha da fatura", "OK", "", 16)
    RETURN .F.
 ENDIF
 SELECT fi
 COUNT TO lcnrregistosfi
 IF lcnrregistosfi==1 .AND. EMPTY(fi.ref) .AND. EMPTY(fi.design) .AND. EMPTY(fi.qtt)
    DELETE FROM Fi WHERE EMPTY(fi.ref) .AND. EMPTY(fi.design) .AND. EMPTY(fi.qtt)
 ENDIF
 STORE .F. TO lcvalida
 SELECT ucrsimportdocs
 GOTO TOP
 SCAN
    IF ucrsimportdocs.copiar==1
       importarfact.lordem = uf_importarfact_maxlordem()
       IF importarfact.incluinome==1
          SELECT fi
          APPEND BLANK
          REPLACE fi.lordem WITH importarfact.lordem
          IF UPPER(ALLTRIM(ucrse1.pais)) == 'PORTUGAL'
	          IF ALLTRIM(ucrsimportdocs.tipodoc)=='Venda a Dinheiro'
	             REPLACE fi.design WITH 'Fatura'+' Nr. '+ALLTRIM(ucrsimportdocs.documento)+'/3 de '+uf_gerais_getdate(ucrsimportdocs.data)
	          ELSE
	             REPLACE fi.design WITH ALLTRIM(ucrsimportdocs.tipodoc)+' Nr. '+ALLTRIM(ucrsimportdocs.documento)+' de '+uf_gerais_getdate(ucrsimportdocs.data)
	          ENDIF
	      ELSE
	      	REPLACE fi.design WITH 'Anula��o ou retifica��o de ' + ALLTRIM(uc_cabsel.tipodoc)+' Nr. '+ALLTRIM(uc_cabsel.documento)+' de '+uf_gerais_getdate(uc_cabsel.data)
	      ENDIF 
          lcstamp = uf_gerais_stamp()
          SELECT fi
          REPLACE fi.fistamp WITH lcstamp
          REPLACE fi.nmdoc WITH ALLTRIM(ft.nmdoc)
          REPLACE fi.ndoc WITH ft.ndoc
          REPLACE fi.fno WITH ft.fno
          REPLACE fi.iva WITH uf_gerais_gettabelaiva(1, "TAXA")
          REPLACE fi.tabiva WITH 1
          REPLACE fi.armazem WITH myarmazem
       ENDIF
       DO CASE
          CASE ucrsimportdocs.tipo="FT"
             uf_importarfact_crialinhas_compftft(ucrsimportdocs.cabstamp)
             lcvalida = .T.
          CASE ucrsimportdocs.tipo=="BO"
             uf_importarfact_crialinhas_compboft(ucrsimportdocs.cabstamp)
             lcvalida = .T.
          CASE ucrsimportdocs.tipo=="TARV"
             uf_importarfact_crialinhas_comptarvft(ucrsimportdocs.cabstamp)
             lcvalida = .T.
       ENDCASE
       
    ENDIF
 ENDSCAN
  
	
	LOCAL lcIdEfrExterna
	
	SELECT ucrsImportDocs	
	lcIdEfrExterna= ucrsImportDocs.id_efr_externa 
	
	SELECT ft2 	
	Replace ft2.id_efr_externa WITH lcIdEfrExterna
  
 IF lcvalida==.T.
    SELECT fi
    GOTO TOP
    SCAN
       uf_atendimento_fiiliq(.T.)
       IF uf_gerais_getparameter("ADM0000000211", "BOOL")==.F.
          uf_atendimento_actvaloresfi()
       ENDIF
       SELECT fi
    ENDSCAN
    
    IF uf_gerais_getparameter("ADM0000000211", "BOOL")==.T.
       uf_facturacao_atribuilotesdocumento()
    ENDIF



    uf_importarfact_sair()
 ENDIF
ENDFUNC
**
FUNCTION uf_importarfact_validadadosentidadeimportar
 IF ( .NOT. USED("ucrsImportDocsEntidades"))
    RETURN .F.
 ENDIF
 SELECT ucrsimportdocsentidades
 GOTO TOP
 LOCATE FOR ucrsimportdocsentidades.copiar==1
 IF  .NOT. FOUND()
    RETURN .F.
 ENDIF
 SELECT ucrsimportdocsentidades
 GOTO TOP
 LOCATE FOR ucrsimportdocsentidades.copiar==1 .AND. ucrsimportdocsentidades.valorcred=0
 IF FOUND()
    RETURN .F.
 ENDIF
 RETURN .T.
ENDFUNC
**
FUNCTION uf_importarfact_importarfacturasentidades
 LOCAL lcorigem, lcdestino, lcvalida
 STORE .F. TO lcvalida
 LOCAL lcstamp
 SELECT fi
 COUNT TO lcnrregistosfi
 IF lcnrregistosfi==1 .AND. EMPTY(fi.ref) .AND. EMPTY(fi.design) .AND. EMPTY(fi.qtt)
    DELETE FROM Fi WHERE EMPTY(fi.ref) .AND. EMPTY(fi.design) .AND. EMPTY(fi.qtt)
 ENDIF
 IF ( .NOT. uf_importarfact_validadadosentidadeimportar())
    uf_perguntalt_chama("Selecione as linhas a importar e preencha o respetivo valor a creditar/debitar na coluna 'Valor C'", "OK", "", 16)
    RETURN .F.
 ENDIF
 uv_credtodeb = .F.
 IF INLIST(ALLTRIM(ucrsimportdocs.tipodoc), 'Nota de Cr�dito', 'N.Cr�dito SNS Comp', 'N.Cr�dito SNS Fee') .AND. INLIST(ALLTRIM(ft.nmdoc), 'Nota de D�bito', 'N.D�bito SNS Fee', 'N.D�bito SNS Comp')
    uv_credtodeb = .T.
 ENDIF
 IF uv_credtodeb
    SELECT fi
    APPEND BLANK
    REPLACE fi.lordem WITH importarfact.lordem
    REPLACE fi.design WITH ALLTRIM(ucrsimportdocs.tipodoc)+' Nr. '+ALLTRIM(ucrsimportdocs.documento)+' de '+uf_gerais_getdate(ucrsimportdocs.data)
    lcstamp = uf_gerais_stamp()
    SELECT fi
    REPLACE fi.fistamp WITH lcstamp
    REPLACE fi.nmdoc WITH ALLTRIM(ft.nmdoc)
    REPLACE fi.ndoc WITH ft.ndoc
    REPLACE fi.fno WITH ft.fno
    REPLACE fi.iva WITH uf_gerais_gettabelaiva(1, "TAXA")
    REPLACE fi.tabiva WITH 1
    REPLACE fi.armazem WITH myarmazem
 ENDIF
 SELECT ucrsimportdocsentidades
 GOTO TOP
 SCAN FOR ucrsimportdocsentidades.copiar==1 .AND. ucrsimportdocsentidades.valorcred<>0
    importarfact.lordem = uf_importarfact_maxlordem()
    SELECT fi
    APPEND BLANK
    REPLACE fi.lordem WITH importarfact.lordem
    lcstamp = uf_gerais_stamp()
    SELECT fi
    REPLACE fi.fistamp WITH lcstamp
    REPLACE fi.ftstamp WITH ft.ftstamp
    REPLACE fi.nmdoc WITH ALLTRIM(ft.nmdoc)
    REPLACE fi.ndoc WITH ft.ndoc
    REPLACE fi.fno WITH ft.fno
    REPLACE fi.ref WITH "9999998"
    REPLACE fi.qtt WITH 1
    REPLACE fi.design WITH IIF( .NOT. uv_credtodeb, ALLTRIM(ucrsimportdocsentidades.descricaocred), ALLTRIM(ucrsimportdocsentidades.tipodoc))
    REPLACE fi.etiliquido WITH IIF( .NOT. uv_credtodeb, ucrsimportdocsentidades.valorcred, ABS(ucrsimportdocsentidades.valorcred))*200.482
    REPLACE fi.tiliquido WITH IIF( .NOT. uv_credtodeb, ucrsimportdocsentidades.valorcred, ABS(ucrsimportdocsentidades.valorcred))*200.482
    REPLACE fi.epv WITH IIF( .NOT. uv_credtodeb, ucrsimportdocsentidades.valorcred, ABS(ucrsimportdocsentidades.valorcred))
    REPLACE fi.u_epvp WITH IIF( .NOT. uv_credtodeb, ucrsimportdocsentidades.valorcred, ABS(ucrsimportdocsentidades.valorcred))
    REPLACE fi.ofistamp WITH ALLTRIM(ucrsimportdocsentidades.linstamp)
    REPLACE fi.iva WITH uf_gerais_gettabelaiva(ucrsimportdocsentidades.tabiva, "TAXA")
    REPLACE fi.tabiva WITH ucrsimportdocsentidades.tabiva
    REPLACE fi.ivaincl WITH .T.
    REPLACE fi.amostra WITH .T.
    REPLACE fi.armazem WITH myarmazem
    IF ft.tipodoc==3 .AND. fi.etiliquido>0 
       REPLACE fi.etiliquido WITH fi.etiliquido*-1
       REPLACE fi.tiliquido WITH fi.tiliquido*-1
    ENDIF
    IF (ft.tipodoc==1 OR ft.tipodoc==2) .AND. fi.etiliquido<0   
    	SELECT fi
        REPLACE fi.etiliquido WITH fi.etiliquido*-1        
        REPLACE fi.tiliquido WITH fi.tiliquido*-1
        REPLACE fi.epv WITH fi.epv*-1
        REPLACE fi.u_epvp WITH fi.u_epvp*-1
    ENDIF
    REPLACE fi.ivaincl WITH .T.
    uf_atendimento_fiiliq(.T.) 
    
    uf_atendimento_actvaloresfi()  
    uf_atendimento_acttotaisft()
    
    SELECT ucrsimportdocsentidades
 ENDSCAN
 uf_importarfact_sair()
ENDFUNC
**
PROCEDURE uf_importarfact_validavalorcredito
 SELECT ucrsimportdocsentidades
 IF ucrsimportdocsentidades.valorcred>ABS(ucrsimportdocsentidades.valor)
    SELECT ucrsimportdocsentidades
    REPLACE ucrsimportdocsentidades.valorcred WITH 0
    uf_perguntalt_chama("O valor a creditar/debitar n�o pode ser superior ao valor da Fatura.", "OK", "", 32)
    RETURN .F.
 ENDIF
 
 IF ucrsimportdocsentidades.valorcred < 0
    SELECT ucrsimportdocsentidades
    REPLACE ucrsimportdocsentidades.valorcred WITH 0
    uf_perguntalt_chama("O valor a creditar/debitar n�o pode ser negativo.", "OK", "", 32)
    RETURN .F.
 ENDIF

ENDPROC
**
PROCEDURE uf_importarfact_crialinhas_compftft
 LPARAMETERS cabstamp
 LOCAL lcsql
 lcsql = ""
 TEXT TO lcsql TEXTMERGE NOSHOW
		Select	
			qttmov = fi.Pbruto
			,fi.fistamp
		From 	
			FI (nolock) 
		Where 
			FTSTAMP = '<<Alltrim(cabstamp)>>'
		Order by
			lordem	
 ENDTEXT
 IF uf_gerais_actgrelha("", "uc_LinhasFt", lcsql) .AND. RECCOUNT("uc_LinhasFt")>0
    SELECT uc_linhasft
    GOTO TOP
    SCAN
       uf_importarfact_crialinhas_individuaisftft(uc_linhasft.fistamp)
    ENDSCAN
 ENDIF
ENDPROC
**
FUNCTION uf_importarfact_crialinhas_compboft
 LPARAMETERS cabstamp
 LOCAL lcsql
 lcsql = ""
 TEXT TO lcsql TEXTMERGE NOSHOW
		Select 
			bistamp
		From 
			BI (nolock) 
		Where 
			BOSTAMP = '<<Alltrim(cabstamp)>>'
		order by
			lordem
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "uc_ImportDossier", lcsql)
    uf_perguntalt_chama("DESCULPE, N�O FOI POSS�VEL IDENTIFICAR AS DEFNI��ES DAS LINHAS A IMPORTAR. POR FAVOR CONTACTE A ASSIST�NCIA T�CNICA.", "OK", "", 16)
    RETURN .F.
 ENDIF
 SELECT uc_importdossier
 GOTO TOP
 SCAN
    uf_importarfact_crialinhas_individuaisboft(uc_importdossier.bistamp)
 ENDSCAN
ENDFUNC
**
FUNCTION uf_importarfact_crialinhas_comptarvft
 LPARAMETERS cabstamp
 LOCAL lcsql
 lcsql = ""
 TEXT TO lcsql TEXTMERGE NOSHOW
		Select 
			id
		From 
			ext_tarv_prescricao (nolock) 
		Where 
			id_ext_tarv = '<<Alltrim(cabstamp)>>'
		order by
			id
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "uc_ImportTarv", lcsql)
    uf_perguntalt_chama("DESCULPE, N�O FOI POSS�VEL IDENTIFICAR AS DEFNI��ES DAS LINHAS A IMPORTAR. POR FAVOR CONTACTE A ASSIST�NCIA T�CNICA.", "OK", "", 16)
    RETURN .F.
 ELSE
    SELECT uc_importtarv
    GOTO TOP
    SCAN
       uf_importarfact_crialinhas_individuaistarvft(ALLTRIM(uc_importtarv.id), ALLTRIM(cabstamp))
    ENDSCAN
    fecha("uc_ImportTarv")
 ENDIF
ENDFUNC
**
PROCEDURE uf_importarfact_crialinhas_individuaisftft
 LPARAMETERS linstamp
 LOCAL novaqtt
 lcsql = ""
 TEXT TO lcsql TEXTMERGE NOSHOW
		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#DadosProduto'))
		    DROP TABLE #DadosProduto
			
		select 
			st.ref
			,st.usalote
			,pic = isnull(fprod.pvporig,0)
         ,fprod.dispositivo_seguranca
		into
			#DadosProduto
		from
			st (nolock)
			left join fprod (nolock) on fprod.cnp = st.ref
		where
			 st.site_nr = <<mysite_nr>>

		SELECT
			usalote = isnull(#DadosProduto.usalote,CONVERT(bit,0))
			,pic = isnull(#DadosProduto.pic,0)
			,fi.Pbruto as qttmov
			,fi.*
			,fi2.dataValidade
         ,#DadosProduto.dispositivo_seguranca
		From
			FI (nolock) 
			left join #DadosProduto on fi.ref = #DadosProduto.ref
			inner join fi2 (nolock) on fi2.fistamp = fi.fistamp 
		Where 	
			fi.fistamp = '<<Alltrim(linstamp)>>'
		
		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#DadosProduto'))
		DROP TABLE #DadosProduto
 ENDTEXT
 IF uf_gerais_actgrelha("", "tempSQL_FNCMP", lcsql) .AND. RECCOUNT("tempSQL_FNCMP")>0
    LOCAL lcstamp
    SELECT tempsql_fncmp
    SCAN
       novaqtt = tempsql_fncmp.qtt-tempsql_fncmp.qttmov
       SELECT fi
       APPEND BLANK
       lcstamp = uf_gerais_stamp()
       SELECT fi
       REPLACE fi.fistamp WITH lcstamp
       REPLACE fi.design WITH tempsql_fncmp.design
       REPLACE fi.nmdoc WITH ALLTRIM(ft.nmdoc)
       REPLACE fi.ndoc WITH ft.ndoc
       REPLACE fi.fno WITH ft.fno
       REPLACE fi.lordem WITH importarfact.lordem
       REPLACE fi.ficcusto WITH tempsql_fncmp.ficcusto
       SELECT ucrslistadocs
       IF importarfact.movstock=1
          SELECT fi
          REPLACE fi.ref WITH IIF(EMPTY(ALLTRIM(tempsql_fncmp.ref)), ALLTRIM(tempsql_fncmp.oref), ALLTRIM(tempsql_fncmp.ref))
       ENDIF
       IF novaqtt>0
          SELECT fi
          REPLACE fi.qtt WITH novaqtt
          REPLACE fi.epv WITH tempsql_fncmp.epv         
          IF tempsql_fncmp.qtt<>tempsql_fncmp.qttmov
             SELECT fi
             REPLACE fi.etiliquido WITH tempsql_fncmp.etiliquido &&* novaqtt
             REPLACE fi.tiliquido WITH tempsql_fncmp.tiliquido &&* novaqtt*200.248
          ELSE
             SELECT fi
             REPLACE fi.etiliquido WITH tempsql_fncmp.etiliquido
             REPLACE fi.tiliquido WITH tempsql_fncmp.etiliquido*200.248
          ENDIF
       ELSE
          SELECT fi
          REPLACE fi.qtt WITH 0
          REPLACE fi.etiliquido WITH 0
       ENDIF
       SELECT fi
       REPLACE fi.iva WITH tempsql_fncmp.iva
       REPLACE fi.tabiva WITH IIF(tempsql_fncmp.tabiva=0, uf_gerais_getumvalor("taxasIva", "codigo", "taxa = 0", "codigo asc"), tempsql_fncmp.tabiva)
       REPLACE fi.armazem WITH tempsql_fncmp.armazem
       SELECT fi
       REPLACE fi.unidade WITH tempsql_fncmp.unidade
       REPLACE fi.unidad2 WITH tempsql_fncmp.unidad2
       REPLACE fi.iva WITH tempsql_fncmp.iva
       REPLACE fi.ivaincl WITH tempsql_fncmp.ivaincl
       REPLACE fi.codigo WITH tempsql_fncmp.codigo
       REPLACE fi.fnoft WITH tempsql_fncmp.fnoft
       REPLACE fi.ndocft WITH tempsql_fncmp.ndocft
       REPLACE fi.ftanoft WITH tempsql_fncmp.ftanoft
       REPLACE fi.lobs2 WITH tempsql_fncmp.lobs2
       REPLACE fi.litem2 WITH tempsql_fncmp.litem2
       REPLACE fi.litem WITH tempsql_fncmp.litem
       REPLACE fi.lobs3 WITH tempsql_fncmp.lobs3
       REPLACE fi.cpoc WITH tempsql_fncmp.cpoc
       REPLACE fi.composto WITH tempsql_fncmp.composto
       REPLACE fi.lrecno WITH tempsql_fncmp.lrecno
       REPLACE fi.partes WITH tempsql_fncmp.partes
       REPLACE fi.altura WITH tempsql_fncmp.altura
       REPLACE fi.largura WITH tempsql_fncmp.largura
       REPLACE fi.oref WITH IIF(EMPTY(ALLTRIM(tempsql_fncmp.ref)), ALLTRIM(tempsql_fncmp.oref), ALLTRIM(tempsql_fncmp.ref))
       REPLACE fi.lote WITH tempsql_fncmp.lote
       REPLACE fi.usr1 WITH tempsql_fncmp.usr1
       REPLACE fi.usr2 WITH tempsql_fncmp.usr2
       REPLACE fi.usr3 WITH tempsql_fncmp.usr3
       REPLACE fi.usr4 WITH tempsql_fncmp.usr4
       REPLACE fi.usr5 WITH tempsql_fncmp.usr5
       REPLACE fi.cor WITH tempsql_fncmp.cor
       REPLACE fi.tam WITH tempsql_fncmp.tam
       REPLACE fi.stipo WITH tempsql_fncmp.stipo
       REPLACE fi.fifref WITH tempsql_fncmp.fifref
       REPLACE fi.familia WITH tempsql_fncmp.familia
       REPLACE fi.pbruto WITH tempsql_fncmp.pbruto
       REPLACE fi.bistamp WITH tempsql_fncmp.bistamp
       REPLACE fi.stns WITH tempsql_fncmp.stns
       REPLACE fi.fincusto WITH tempsql_fncmp.fincusto
       REPLACE fi.ofistamp WITH tempsql_fncmp.fistamp
       REPLACE fi.pv WITH tempsql_fncmp.pv
       REPLACE fi.pvmoeda WITH tempsql_fncmp.pvmoeda
       REPLACE fi.epv WITH tempsql_fncmp.epv
       REPLACE fi.tmoeda WITH tempsql_fncmp.tmoeda
       REPLACE fi.eaquisicao WITH tempsql_fncmp.eaquisicao
       lcsql = ""
       TEXT TO lcsql TEXTMERGE NOSHOW
                Select PCPOND,EPCPOND, epcult From ST (nolock) Where (Ref = '<<Alltrim(tempSQL_FNCMP.ref)>>' Or ref = '<<Alltrim(tempSQL_FNCMP.oref)>>') and site_nr=<<mysite_nr>>
       ENDTEXT
       IF uf_gerais_actgrelha("", "uc_PRECOCUSTO", lcsql)
          SELECT fi
          REPLACE fi.custo WITH uc_precocusto.epcult*200.482
          REPLACE fi.ecusto WITH uc_precocusto.epcult
       ENDIF
       SELECT fi
       REPLACE fi.slvu WITH tempsql_fncmp.slvu
       REPLACE fi.eslvu WITH tempsql_fncmp.eslvu
       REPLACE fi.sltt WITH tempsql_fncmp.sltt
       REPLACE fi.esltt WITH tempsql_fncmp.esltt
       REPLACE fi.slvumoeda WITH tempsql_fncmp.slvumoeda
       REPLACE fi.slttmoeda WITH tempsql_fncmp.slttmoeda
       REPLACE fi.nccod WITH tempsql_fncmp.nccod
       REPLACE fi.ncinteg WITH tempsql_fncmp.ncinteg
       REPLACE fi.epromo WITH tempsql_fncmp.epromo
       REPLACE fi.desconto WITH tempsql_fncmp.desconto
       REPLACE fi.desc2 WITH tempsql_fncmp.desc2
       REPLACE fi.desc3 WITH tempsql_fncmp.desc3
       REPLACE fi.desc4 WITH tempsql_fncmp.desc4
       REPLACE fi.desc5 WITH tempsql_fncmp.desc5
       REPLACE fi.desc6 WITH tempsql_fncmp.desc6
       REPLACE fi.iectin WITH tempsql_fncmp.iectin
       REPLACE fi.eiectin WITH tempsql_fncmp.eiectin
       REPLACE fi.vbase WITH tempsql_fncmp.vbase
       REPLACE fi.evbase WITH tempsql_fncmp.evbase
       REPLACE fi.tliquido WITH tempsql_fncmp.tliquido
       REPLACE fi.num1 WITH tempsql_fncmp.num1
       REPLACE fi.pcp WITH tempsql_fncmp.pcp
       REPLACE fi.epcp WITH tempsql_fncmp.epcp
       REPLACE fi.pvori WITH tempsql_fncmp.pvori
       REPLACE fi.epvori WITH tempsql_fncmp.epvori
       REPLACE fi.szzstamp WITH tempsql_fncmp.szzstamp
       REPLACE fi.zona WITH tempsql_fncmp.zona
       REPLACE fi.morada WITH tempsql_fncmp.morada
       REPLACE fi.local WITH tempsql_fncmp.local
       REPLACE fi.codpost WITH tempsql_fncmp.codpost
       REPLACE fi.telefone WITH tempsql_fncmp.telefone
       REPLACE fi.email WITH tempsql_fncmp.email
       REPLACE fi.tkhposlstamp WITH tempsql_fncmp.tkhposlstamp
       REPLACE fi.marcada WITH tempsql_fncmp.marcada
       REPLACE fi.amostra WITH tempsql_fncmp.amostra
       REPLACE fi.u_epref WITH tempsql_fncmp.u_epref
       REPLACE fi.pic WITH tempsql_fncmp.pic
       REPLACE fi.u_stock WITH tempsql_fncmp.u_stock
       REPLACE fi.u_ip WITH tempsql_fncmp.u_ip
       REPLACE fi.u_epvp WITH tempsql_fncmp.u_epvp
       REPLACE fi.u_genalt WITH tempsql_fncmp.u_genalt
       REPLACE fi.u_generico WITH tempsql_fncmp.u_generico
       REPLACE fi.u_ettent1 WITH tempsql_fncmp.u_ettent1
       REPLACE fi.u_ettent2 WITH tempsql_fncmp.u_ettent2
       REPLACE fi.u_psicont WITH tempsql_fncmp.u_psicont
       REPLACE fi.u_bencont WITH tempsql_fncmp.u_bencont
       REPLACE fi.u_psico WITH tempsql_fncmp.u_psico
       REPLACE fi.u_benzo WITH tempsql_fncmp.u_benzo
       REPLACE fi.u_txcomp WITH tempsql_fncmp.u_txcomp
       IF tempsql_fncmp.u_txcomp <> 0
         REPLACE fi.forceCalcCompart WITH .T.
       ENDIF
       REPLACE fi.u_cnp WITH tempsql_fncmp.u_cnp
       REPLACE fi.u_comp WITH tempsql_fncmp.u_comp
       REPLACE fi.u_nbenef WITH tempsql_fncmp.u_nbenef
       REPLACE fi.u_nbenef2 WITH tempsql_fncmp.u_nbenef2
       REPLACE fi.u_diploma WITH tempsql_fncmp.u_diploma
       REPLACE fi.u_refvale WITH tempsql_fncmp.u_refvale
       REPLACE fi.u_robot WITH tempsql_fncmp.u_robot
       REPLACE fi.u_descval WITH tempsql_fncmp.u_descval
       REPLACE fi.usalote WITH tempsql_fncmp.usalote
       REPLACE fi.pic WITH tempsql_fncmp.pic
       SELECT ft
       IF ft.tipodoc==3 .AND. fi.etiliquido>0            
          SELECT fi
          REPLACE fi.etiliquido WITH fi.etiliquido*-1
          REPLACE fi.tiliquido WITH fi.tiliquido*-1
       ENDIF
		
		SELECT fi2
		LOCATE FOR fi2.fistamp = fi.fistamp
		Replace fi2.dataValidade WITH tempsql_fncmp.dataValidade
		

       IF USED("fi_trans_info") AND tempSQL_FNCMP.dispositivo_seguranca

         SELECT fi
         uf_insert_fi_trans_info_seminfo(fi.ref, fi.fistamp)

       ENDIF


       SELECT tempsql_fncmp
       importarfact.lordem = importarfact.lordem+1
    ENDSCAN
 ENDIF
 
ENDPROC
**
FUNCTION uf_importarfact_crialinhas_individuaisboft
 LPARAMETERS linstamp
 LOCAL novaqtt, lcarmazem, lcstamp
 lcarmazem = 1
 lcsql = ""
 TEXT TO lcsql TEXTMERGE NOSHOW
		Select 	
			bi.*
			,bi.Pbruto as qttmov
			,usalote = isnull(st.usalote,CONVERT(bit,0))
			,epv1 = isnull(st.epv1,0)
			,pic = isnull(fprod.pvporig,0)
			,psico = ISNULL(psico, convert(BIT,0))
			,benzo = ISNULL(benzo, convert(BIT,0))
         ,fprod.dispositivo_seguranca
		From 
			BI (nolock)		
			LEFT JOIN st (nolock) ON bi.ref = st.ref
			left join fprod (nolock) on fprod.cnp = st.ref
		Where
			Bi.bistamp = '<<Alltrim(linstamp)>>'
			and st.site_nr = <<mysite_nr>>
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "tempSQL_BICMP", lcsql)
    uf_perguntalt_chama("N�O FOI POSS�VEL IDENTIFICAR AS DEFINI��ES DAS LINHAS A IMPORTAR. POR FAVOR CONTACTE A ASSIST�NCIA T�CNICA.", "OK", "", 16)
    RETURN .F.
 ENDIF
 SELECT tempsql_bicmp
 SCAN
    IF tempsql_bicmp.ndos<>43
       novaqtt = tempsql_bicmp.qtt-tempsql_bicmp.qttmov
    ELSE
       novaqtt = tempsql_bicmp.qtrec-tempsql_bicmp.qtt2
    ENDIF
    SELECT fi
    APPEND BLANK
    lcstamp = uf_gerais_stamp()
    SELECT fi
    REPLACE fi.fistamp WITH lcstamp
    REPLACE fi.nmdoc WITH ALLTRIM(ft.nmdoc)
    REPLACE fi.ndoc WITH ft.ndoc
    REPLACE fi.fno WITH ft.fno
    REPLACE fi.ficcusto WITH tempsql_bicmp.ccusto
    IF TYPE("IMPORTARFACT")<>'U'
       IF importarfact.movstock=1
          SELECT ucrslistadocs
          SELECT fi
          REPLACE fi.ref WITH ALLTRIM(tempsql_bicmp.ref)
       ENDIF
    ELSE
       SELECT ucrslistadocs
       SELECT fi
       REPLACE fi.ref WITH ALLTRIM(tempsql_bicmp.ref)
    ENDIF
    IF novaqtt>0
       SELECT fi
       REPLACE fi.qtt WITH novaqtt
       REPLACE fi.epv WITH tempsql_bicmp.edebito
       REPLACE fi.u_epvp WITH tempsql_bicmp.edebito
    ELSE
       SELECT fi
       REPLACE fi.qtt WITH 0
       REPLACE fi.etiliquido WITH 0
       REPLACE fi.tiliquido WITH 0
    ENDIF
    SELECT fi
    REPLACE fi.design WITH tempsql_bicmp.design
    REPLACE fi.iva WITH tempsql_bicmp.iva
    REPLACE fi.tabiva WITH IIF(tempsql_bicmp.tabiva=0, uf_gerais_getumvalor("taxasIva", "codigo", "taxa = 0", "codigo asc"), tempsql_bicmp.tabiva)
    lcarmazem = tempsql_bicmp.armazem
    SELECT fi
    REPLACE fi.armazem WITH lcarmazem
    REPLACE fi.pv WITH tempsql_bicmp.debito
    REPLACE fi.fno WITH ft.fno
    REPLACE fi.nmdoc WITH ALLTRIM(ft.nmdoc)
    REPLACE fi.ndoc WITH ft.ndoc
    REPLACE fi.rdata WITH DATE()
    REPLACE fi.bistamp WITH tempsql_bicmp.bistamp
    SELECT fi
    REPLACE fi.unidade WITH tempsql_bicmp.unidade
    REPLACE fi.ivaincl WITH tempsql_bicmp.ivaincl
    REPLACE fi.codigo WITH tempsql_bicmp.codigo
    REPLACE fi.cpoc WITH tempsql_bicmp.cpoc
    REPLACE fi.composto WITH tempsql_bicmp.composto
    REPLACE fi.lrecno WITH tempsql_bicmp.lrecno
    REPLACE fi.partes WITH tempsql_bicmp.partes
    REPLACE fi.oref WITH tempsql_bicmp.ref
    REPLACE fi.lote WITH tempsql_bicmp.lote
    REPLACE fi.usr1 WITH tempsql_bicmp.usr1
    REPLACE fi.usr2 WITH tempsql_bicmp.usr2
    REPLACE fi.usr3 WITH tempsql_bicmp.usr3
    REPLACE fi.usr4 WITH tempsql_bicmp.usr4
    REPLACE fi.usr5 WITH tempsql_bicmp.usr5
    REPLACE fi.stipo WITH tempsql_bicmp.stipo
    REPLACE fi.familia WITH tempsql_bicmp.familia
    REPLACE fi.amostra WITH .T.
    SELECT fi
    REPLACE fi.bistamp WITH tempsql_bicmp.bistamp
    REPLACE fi.stns WITH tempsql_bicmp.stns
    SELECT fi
    REPLACE fi.pv WITH tempsql_bicmp.edebito*200.248
    REPLACE fi.epv WITH tempsql_bicmp.edebito
    REPLACE fi.u_epvp WITH tempsql_bicmp.edebito
    IF  .NOT. EMPTY(ALLTRIM(tempsql_bicmp.ref))
       lcsql = ""
       TEXT TO lcsql TEXTMERGE NOSHOW
                Select PCPOND,EPCPOND From ST (nolock) Where (Ref = '<<Alltrim(tempSQL_BICMP.ref)>>' ) 
       ENDTEXT
       IF uf_gerais_actgrelha("", "uc_PRECOCUSTO", lcsql)
          SELECT fi
          REPLACE fi.custo WITH uc_precocusto.pcpond
          REPLACE fi.ecusto WITH uc_precocusto.epcpond
       ENDIF
    ENDIF
    SELECT fi
    REPLACE fi.slvu WITH tempsql_bicmp.slvu
    REPLACE fi.eslvu WITH tempsql_bicmp.eslvu
    REPLACE fi.sltt WITH tempsql_bicmp.sltt
    REPLACE fi.esltt WITH tempsql_bicmp.esltt
    REPLACE fi.slvumoeda WITH tempsql_bicmp.slvumoeda
    REPLACE fi.slttmoeda WITH tempsql_bicmp.slttmoeda
    SELECT fi
    REPLACE fi.desconto WITH tempsql_bicmp.desconto
    REPLACE fi.desc2 WITH tempsql_bicmp.desc2
    REPLACE fi.desc3 WITH tempsql_bicmp.desc3
    REPLACE fi.desc4 WITH tempsql_bicmp.desc4
    REPLACE fi.num1 WITH tempsql_bicmp.num1
    REPLACE fi.morada WITH tempsql_bicmp.morada
    REPLACE fi.local WITH tempsql_bicmp.local
    REPLACE fi.codpost WITH tempsql_bicmp.codpost
    SELECT tempsql_bicmp
    IF TYPE("IMPORTARFACT")<>'U'
       SELECT fi
       REPLACE fi.lordem WITH importarfact.lordem
    ELSE
       SELECT fi
       REPLACE fi.lordem WITH tempsql_bicmp.lordem
    ENDIF
    SELECT fi
    REPLACE fi.usalote WITH tempsql_bicmp.usalote
    REPLACE fi.pic WITH tempsql_bicmp.pic
    REPLACE fi.u_psico WITH tempsql_bicmp.psico
    REPLACE fi.u_benzo WITH tempsql_bicmp.benzo
    REPLACE fi.campanhas WITH tempsql_bicmp.lobs

    IF USED("fi_trans_info") AND tempsql_bicmp.dispositivo_seguranca

      SELECT fi
      uf_insert_fi_trans_info_seminfo(fi.ref, fi.fistamp)

    ENDIF

    IF TYPE("IMPORTARFACT")<>'U'
       importarfact.lordem = importarfact.lordem+1
    ENDIF
 ENDSCAN
 SELECT fi
 GOTO TOP
ENDFUNC
**
PROCEDURE uf_importarfact_crialinhas_individuaistarvft
 LPARAMETERS linstamp, cabstamp
 LOCAL novaqtt
 lcsql = ""
 TEXT TO lcsql TEXTMERGE NOSHOW
		SELECT
			t.id
			,st.ref
			,st.usalote
			,pic = isnull(fprod.pvporig,0)
			,qttmov = t.qt_dispensada
			,qtt = t.qt
			,st.epcusto
			,epv = st.epv1
			,design = st.design
			,tabiva = st.tabiva
			,iva = (select taxa from taxasiva (nolock) where codigo=st.tabiva)
			,st.ivaincl
			,st.cpoc
			,st.usr1
			,st.familia
			,st.stns
			,st.EPCPOND
			,psico = ISNULL(fprod.psico,CONVERT(bit,0))
			,benzo = ISNULL(fprod.benzo,CONVERT(bit,0))
			,pref = ISNULL(fprod.pref,0)
			,st.stock
			,generico = ISNULL(fprod.generico,CONVERT(bit,0))
			,st.usalote
			,cnpem = ISNULL(fprod.cnpem,'')
         ,fprod.dispositivo_seguranca
		From
			ext_tarv_prescricao t (nolock)
			inner join st (nolock) on t.ref = st.ref
			left join fprod (nolock) on fprod.cnp = st.ref
		Where
			t.id_ext_tarv = '<<Alltrim(cabstamp)>>'
			and t.id = '<<Alltrim(linstamp)>>'
			and st.site_nr = <<mysite_nr>>
			and t.qt > t.qt_dispensada
 ENDTEXT
 IF uf_gerais_actgrelha("", "tempSQL_tarv_lin", lcsql) .AND. RECCOUNT("tempSQL_tarv_lin")>0
    LOCAL lcstamp
    SELECT tempsql_tarv_lin
    SCAN
       novaqtt = tempsql_tarv_lin.qtt-tempsql_tarv_lin.qttmov
       IF importarfact.qttmaior==0 .AND. novaqtt<=0
       ELSE
          SELECT fi
          APPEND BLANK
          lcstamp = uf_gerais_stamp()
          SELECT fi
          REPLACE fi.fistamp WITH lcstamp
          REPLACE fi.nmdoc WITH ALLTRIM(ft.nmdoc)
          REPLACE fi.ndoc WITH ft.ndoc
          REPLACE fi.fno WITH ft.fno
          REPLACE fi.lordem WITH importarfact.lordem
          REPLACE fi.id_ext_tarv_prescricao WITH ALLTRIM(tempsql_tarv_lin.id)
          SELECT fi
          REPLACE fi.ficcusto WITH ft.ccusto
          SELECT fi
          REPLACE fi.ecusto WITH tempsql_tarv_lin.epcusto
          SELECT fi
          REPLACE fi.eslvu WITH tempsql_tarv_lin.epcusto
          REPLACE fi.esltt WITH tempsql_tarv_lin.epcusto*novaqtt
          REPLACE fi.slvumoeda WITH tempsql_tarv_lin.epcusto
          REPLACE fi.slttmoeda WITH tempsql_tarv_lin.epcusto*novaqtt
          REPLACE fi.pvmoeda WITH tempsql_tarv_lin.epv
          SELECT ucrslistadocs
          SELECT fi
          REPLACE fi.ref WITH ALLTRIM(tempsql_tarv_lin.ref)
          REPLACE fi.codigo WITH ALLTRIM(tempsql_tarv_lin.ref)
          REPLACE fi.cnpem WITH ALLTRIM(tempsql_tarv_lin.cnpem)
          IF novaqtt>0
             SELECT fi
             REPLACE fi.qtt WITH novaqtt
             REPLACE fi.epv WITH tempsql_tarv_lin.epv
             REPLACE fi.etiliquido WITH tempsql_tarv_lin.epv*novaqtt
          ELSE
             SELECT fi
             REPLACE fi.qtt WITH 0
             REPLACE fi.etiliquido WITH 0
          ENDIF
          SELECT fi
          REPLACE fi.design WITH tempsql_tarv_lin.design
          REPLACE fi.iva WITH tempsql_tarv_lin.iva
          REPLACE fi.tabiva WITH tempsql_tarv_lin.tabiva
          REPLACE fi.ivaincl WITH tempsql_tarv_lin.ivaincl
          REPLACE fi.armazem WITH myarmazem
          SELECT fi
          REPLACE fi.ftanoft WITH ft.ftano
          REPLACE fi.cpoc WITH tempsql_tarv_lin.cpoc
          REPLACE fi.lrecno WITH lcstamp
          REPLACE fi.usr1 WITH tempsql_tarv_lin.usr1
          REPLACE fi.stipo WITH 1
          REPLACE fi.familia WITH tempsql_tarv_lin.familia
          REPLACE fi.stns WITH tempsql_tarv_lin.stns
          SELECT fi
          REPLACE fi.epcp WITH tempsql_tarv_lin.epcpond
          REPLACE fi.epvori WITH fi.etiliquido
          SELECT fi
          REPLACE fi.u_epref WITH tempsql_tarv_lin.pref
          REPLACE fi.pic WITH tempsql_tarv_lin.pic
          REPLACE fi.u_stock WITH tempsql_tarv_lin.stock
          REPLACE fi.u_epvp WITH tempsql_tarv_lin.epv
          REPLACE fi.u_generico WITH tempsql_tarv_lin.generico
          REPLACE fi.u_psico WITH tempsql_tarv_lin.psico
          REPLACE fi.u_benzo WITH tempsql_tarv_lin.benzo
          REPLACE fi.usalote WITH tempsql_tarv_lin.usalote
          SELECT ft
          IF ft.tipodoc==3 .AND. fi.etiliquido>0       
             SELECT fi
             REPLACE fi.etiliquido WITH fi.etiliquido*-1
             REPLACE fi.tiliquido WITH fi.tiliquido*-1
          ENDIF
          IF (ft.tipodoc==1 OR ft.tipodoc==2) .AND. fi.etiliquido<0       
             SELECT fi
             REPLACE fi.etiliquido WITH fi.etiliquido*-1
             REPLACE fi.tiliquido WITH fi.tiliquido*-1
          ENDIF

         IF USED("fi_trans_info") AND tempsql_tarv_lin.dispositivo_seguranca

            SELECT fi
            uf_insert_fi_trans_info_seminfo(fi.ref, fi.fistamp)

         ENDIF

          SELECT tempsql_tarv_lin
          importarfact.lordem = importarfact.lordem+1
       ENDIF
    ENDSCAN
    IF USED("tempSQL_tarv_lin")
       fecha("tempSQL_tarv_lin")
    ENDIF
 ENDIF
ENDPROC
**
FUNCTION uf_importarfact_maxlordem
 LOCAL lccont
 lccont = 0
 SELECT fi
 SCAN
    IF fi.lordem>lccont
       lccont = fi.lordem
    ENDIF
 ENDSCAN
 RETURN lccont+1
ENDFUNC
**
FUNCTION uf_importarfact_actualizadefinicaodoc
 LOCAL lcsql
 IF  .NOT. USED("ucrsImportDocs")
    RETURN .F.
 ENDIF
 SELECT ucrslistadocs
 lcsql = ""
 IF ALLTRIM(UPPER(ucrslistadocs.tabela))=="FT"
    TEXT TO lcsql TEXTMERGE NOSHOW
			UPDATE TD Set u_copref = <<IIF(uCrsListaDocs.u_copref,1,0)>> WHERE nmdoc = '<<Alltrim(uCrsListaDocs.nomedoc)>>'
    ENDTEXT
 ENDIF
 IF ALLTRIM(UPPER(ucrslistadocs.tabela))=="BO"
    TEXT TO lcsql TEXTMERGE NOSHOW
			UPDATE TS Set u_copref = <<IIF(uCrsListaDocs.u_copref,1,0)>> WHERE nmdos ='<<Alltrim(uCrsListaDocs.nomedoc)>>'
    ENDTEXT
 ENDIF
 IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
    uf_perguntalt_chama("OCORREU UMA ANOMALIA A ACTUALIZAR AS DEFINI��ES DO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
    RETURN .F.
 ENDIF
ENDFUNC
**
PROCEDURE uf_importarfact_gravar
 
  	IF USED("ucrsImportDocs")
	  	
	  	LOCAL lcIdEfrExterna
	    
		SELECT ucrsImportDocs
		lcIdEfrExterna= ucrsImportDocs.id_efr_externa 
		
		SELECT ft2
		Replace ft2.id_efr_externa WITH lcIdEfrExterna
  	
  	endif
  	
  	
 IF  .NOT. up_isentidade
    uf_importarfact_importarlinhas()
 ELSE
    importarfact.text1.setfocus
    uf_importarfact_importarfacturasentidades()
 ENDIF
 
ENDPROC
**
PROCEDURE uf_importarfact_sair
 IF USED("ucrsImportDocs")
    fecha("ucrsImportDocs")
 ENDIF
 IF USED("ucrsImportLinhasDocs")
    fecha("ucrsImportLinhasDocs")
 ENDIF
 IF USED("fi")
    SELECT fi
    GOTO TOP
 ENDIF
 RELEASE myentidadefact
 RELEASE myOrderImportarFact
 importarfact.hide
 importarfact.release
ENDPROC
**
PROCEDURE uf_importarfact_redrawgrids
 WITH importarfact.pageframe1
    IF up_isentidade
       .page1.gridpesq.width = (.width/3)+40
       .page1.grident.width = .width-.page1.gridpesq.width
       .page1.grident.left = .page1.gridpesq.left+.page1.gridpesq.width+5
       .page1.grident.top = .page1.gridpesq.top
       .page1.grident.height = .page1.gridpesq.height
       .page1.check6.left = .page1.gridpesq.left+.page1.gridpesq.width+17
    ELSE
       .page1.grid1.width = (.width/2)-10
       .page1.gridpesq.width = (.width/2)-10
       .page1.grid1.left = .page1.gridpesq.left+.page1.gridpesq.width+5
       .page1.check6.left = .page1.grid1.left+15
    ENDIF
 ENDWITH
ENDPROC
**
PROCEDURE uf_importarfact_seldocs
 uf_importarfact_actualizalinhasdocs()
 SELECT ucrsimportdocs
 IF ucrsimportdocs.copiar=1
    IF up_isentidade   
       uv_curcabstamp = ucrsimportdocs.cabstamp
       IF  .NOT. USED("uc_linSelEnt")
          CREATE CURSOR uc_linSelEnt (stamp C(254))
       ENDIF
       SELECT linstamp AS stamp FROM ucrsImportDocsEntidades WITH (BUFFERING=.T.) WHERE ALLTRIM(ucrsimportdocsentidades.cabstamp)=ALLTRIM(uv_curcabstamp) INTO CURSOR uc_cabSel
       SELECT uc_cabsel
       GOTO TOP
       SCAN
          SELECT uc_linselent
          LOCATE FOR ALLTRIM(uc_cabsel.stamp)==ALLTRIM(uc_linselent.stamp)
          IF  .NOT. FOUND()
             SELECT uc_linselent
             APPEND BLANK
             REPLACE uc_linselent.stamp WITH uc_cabsel.stamp
          ENDIF
          SELECT uc_linselent
       ENDSCAN
       UPDATE ucrsImportDocsEntidades SET copiar = 1 WHERE ALLTRIM(ucrsimportdocsentidades.cabstamp)==ALLTRIM(uv_curcabstamp)
       SELECT ucrsimportdocsentidades
       COUNT FOR  .NOT. DELETED() TO uv_count
       SELECT uc_linselent
       COUNT FOR  .NOT. DELETED() TO uv_countsel
       IF uv_countsel==uv_count
          IF  .NOT. importarfact.seltodos
             importarfact.pageframe1.page1.check6.click(.T.)
          ENDIF
       ELSE
          IF importarfact.seltodos
             importarfact.pageframe1.page1.check6.click(.T.)
          ENDIF
       ENDIF
    ELSE    
       uv_curcabstamp = ucrsimportdocs.cabstamp
       IF  .NOT. USED("uc_linSelFac")
          CREATE CURSOR uc_linSelFac (stamp C(254))
       ENDIF
       SELECT stamp FROM ucrsImportLinhasDocs WITH (BUFFERING=.T.) WHERE ALLTRIM(ucrsimportlinhasdocs.cabstamp)=ALLTRIM(uv_curcabstamp) INTO CURSOR uc_cabSel    
       
       SELECT uc_cabsel
       GOTO TOP
       SCAN
          SELECT uc_linselfac
          LOCATE FOR ALLTRIM(uc_cabsel.stamp)==ALLTRIM(uc_linselfac.stamp)
          IF  .NOT. FOUND()
             SELECT uc_linselfac
             APPEND BLANK
             REPLACE uc_linselfac.stamp WITH uc_cabsel.stamp
          ENDIF
          SELECT uc_linselfac
       ENDSCAN   
       UPDATE ucrsImportLinhasDocs SET copiar = 1 WHERE ALLTRIM(ucrsimportlinhasdocs.cabstamp)==ALLTRIM(uv_curcabstamp)
       SELECT ucrsimportlinhasdocs
       COUNT FOR  .NOT. DELETED() TO uv_count
       SELECT uc_linselfac
       COUNT FOR  .NOT. DELETED() TO uv_countsel
       IF uv_countsel==uv_count
          IF  .NOT. importarfact.seltodos
             importarfact.pageframe1.page1.check6.click(.T.)
          ENDIF
       ELSE
          IF importarfact.seltodos
             importarfact.pageframe1.page1.check6.click(.T.)
          ENDIF
       ENDIF
    ENDIF
 ENDIF
 IF up_isentidade
    SELECT ucrsimportdocsentidades
    COUNT FOR  .NOT. DELETED() TO uv_count
    IF EMPTY(uv_count)
       IF importarfact.seltodos
          importarfact.pageframe1.page1.check6.click(.T.)
       ENDIF
    ENDIF
    importarfact.pageframe1.page1.grident.refresh()
 ELSE
    SELECT ucrsimportlinhasdocs
    COUNT FOR  .NOT. DELETED() TO uv_count
    IF EMPTY(uv_count)
       IF importarfact.seltodos
          importarfact.pageframe1.page1.check6.click(.T.)
       ENDIF
    ENDIF
    importarfact.pageframe1.page1.grid1.refresh()
 ENDIF
ENDPROC
**
PROCEDURE uf_importarfact_sellin
 IF up_isentidade
    IF  .NOT. USED("uc_linSelEnt")
       CREATE CURSOR uc_linSelEnt (stamp C(254))
    ENDIF
    IF ucrsimportdocsentidades.copiar=1
       SELECT uc_linselent
       APPEND BLANK
       REPLACE uc_linselent.stamp WITH ucrsimportdocsentidades.linstamp
       IF  .NOT. importarfact.seltodos
          SELECT * FROM ucrsImportDocsEntidades WITH (BUFFERING=.T.) INTO CURSOR uc_sels
          SELECT uc_sels
          COUNT FOR  .NOT. DELETED() .AND. uc_sels.copiar=0 TO uv_count
          IF uv_count=0
             importarfact.pageframe1.page1.check6.click(.T.)
          ENDIF
       ENDIF
    ELSE
       SELECT uc_linselent
       DELETE FOR ALLTRIM(uc_linselent.stamp)==ALLTRIM(ucrsimportdocsentidades.linstamp)
       IF importarfact.seltodos
          importarfact.pageframe1.page1.check6.click(.T.)
       ENDIF
    ENDIF
 ELSE
    IF  .NOT. USED("uc_linSelFac")
       CREATE CURSOR uc_linSelFac (stamp C(254))
    ENDIF
    IF ucrsimportlinhasdocs.copiar=1
       SELECT uc_linselfac
       APPEND BLANK
       REPLACE uc_linselfac.stamp WITH ucrsimportlinhasdocs.stamp
       IF  .NOT. importarfact.seltodos
          SELECT * FROM ucrsImportLinhasDocs WITH (BUFFERING=.T.) INTO CURSOR uc_sels
          SELECT uc_sels
          COUNT FOR  .NOT. DELETED() .AND. uc_sels.copiar=0 TO uv_count
          IF uv_count=0
             importarfact.pageframe1.page1.check6.click(.T.)
          ENDIF
       ENDIF
    ELSE
       SELECT uc_linselfac
       DELETE FOR ALLTRIM(uc_linselfac.stamp)==ALLTRIM(ucrsimportlinhasdocs.stamp)
       IF importarfact.seltodos
          importarfact.pageframe1.page1.check6.click(.T.)
       ENDIF
    ENDIF
 ENDIF
ENDPROC
**
PROCEDURE uf_importarfact_chksellin
 LPARAMETERS uv_progclick
 IF  .NOT. USED("uc_linSelFac")
    CREATE CURSOR uc_linSelFac (stamp C(254))
 ENDIF
 IF importarfact.seltodos
    importarfact.seltodos = .F.
    IF  .NOT. uv_progclick
       IF up_isentidade
          DELETE FROM uc_linSelEnt
          SELECT ucrsimportdocsentidades
          GOTO TOP
          REPLACE ucrsimportdocsentidades.copiar WITH 0 FOR 1=1
          SELECT ucrsimportdocsentidades
          GOTO TOP
       ELSE
          DELETE FROM uc_linSelFac
          SELECT ucrsimportlinhasdocs
          GOTO TOP
          REPLACE ucrsimportlinhasdocs.copiar WITH 0 FOR 1=1
          SELECT ucrsimportlinhasdocs
          GOTO TOP
       ENDIF
    ENDIF
 ELSE
    importarfact.seltodos = .T.
    IF  .NOT. uv_progclick
       IF up_isentidade
          importarfact.pageframe1.page1.gridpesq.setfocus()
          SELECT ucrsimportdocsentidades
          GOTO TOP
          SCAN
             REPLACE ucrsimportdocsentidades.copiar WITH 1
             SELECT ucrsimportdocsentidades
          ENDSCAN
          INSERT INTO uc_linSelEnt SELECT linstamp FROM ucrsImportDocsEntidades WHERE copiar=1 .AND.  .NOT. DELETED()
          SELECT ucrsimportdocsentidades
          GOTO TOP
       ELSE
          importarfact.pageframe1.page1.gridpesq.setfocus()
          SELECT ucrsimportlinhasdocs
          GOTO TOP
          SCAN
             REPLACE ucrsimportlinhasdocs.copiar WITH 1
             SELECT ucrsimportlinhasdocs
          ENDSCAN
          INSERT INTO uc_linSelFac SELECT stamp FROM ucrsImportLinhasDocs WHERE copiar=1 .AND.  .NOT. DELETED()
          SELECT ucrsimportlinhasdocs
          GOTO TOP
       ENDIF
    ENDIF
 ENDIF
 importarfact.pageframe1.page1.grid1.refresh()
ENDPROC
**
PROCEDURE uf_importarfact_crialinhas_individuaisfoft
 LPARAMETERS linstamp
 LOCAL novaqtt
 lcsql = ""
 TEXT TO lcsql TEXTMERGE NOSHOW
		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#DadosProduto'))
		DROP TABLE #DadosProduto
			
		select 
			st.ref
			,st.usalote
			,pic = isnull(fprod.pvporig,0)
			,u_psico = fprod.psico
			,u_benzo = fprod.benzo
			,u_cnp = fprod.cnp
         ,fprod.dispositivo_seguranca
		into
			#DadosProduto
		from
			st (nolock)
			left join fprod (nolock) on fprod.cnp = st.ref
		where
			 st.site_nr = <<mysite_nr>>

		SELECT
			usalote = isnull(#DadosProduto.usalote,CONVERT(bit,0))
			,pic = isnull(#DadosProduto.pic,0)
			,fn.Pbruto as qttmov
			,ISNULL(#DadosProduto.u_psico,0) as u_psico
			,ISNULL(#DadosProduto.u_benzo,0) as u_benzo
			,ISNULL(#DadosProduto.u_cnp, '') as u_cnp
			,fn.*
         ,#DadosProduto.dispositivo_seguranca
		From
			FN (nolock) 
			left join #DadosProduto on fn.ref = #DadosProduto.ref
		Where 	
			fn.fnstamp = '<<Alltrim(linstamp)>>'
		
		IF EXISTS(SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb.dbo.#DadosProduto'))
		DROP TABLE #DadosProduto
 ENDTEXT
 IF uf_gerais_actgrelha("", "tempSQL_FNCMP", lcsql) .AND. RECCOUNT("tempSQL_FNCMP")>0
    LOCAL lcstamp
    SELECT tempsql_fncmp
    SCAN
       novaqtt = tempsql_fncmp.qtt-tempsql_fncmp.qttmov
       SELECT fi
       APPEND BLANK
       lcstamp = uf_gerais_stamp()
       SELECT fi
       REPLACE fi.fistamp WITH lcstamp
       REPLACE fi.design WITH tempsql_fncmp.design
       REPLACE fi.nmdoc WITH ALLTRIM(ft.nmdoc)
       REPLACE fi.ndoc WITH ft.ndoc
       REPLACE fi.fno WITH ft.fno
       REPLACE fi.lordem WITH importarfact.lordem
       REPLACE fi.ficcusto WITH tempsql_fncmp.fnccusto
       SELECT ucrslistadocs
       IF importarfact.movstock=1
          SELECT fi
          REPLACE fi.ref WITH IIF(EMPTY(ALLTRIM(tempsql_fncmp.ref)), ALLTRIM(tempsql_fncmp.oref), ALLTRIM(tempsql_fncmp.ref))
       ENDIF
       IF novaqtt>0
          SELECT fi
          REPLACE fi.qtt WITH novaqtt
          REPLACE fi.epv WITH tempsql_fncmp.epv
          IF tempsql_fncmp.qtt<>tempsql_fncmp.qttmov
             SELECT fi
             REPLACE fi.etiliquido WITH tempsql_fncmp.epv*novaqtt
             REPLACE fi.tiliquido WITH tempsql_fncmp.epv*novaqtt*200.248
          ELSE
             SELECT fi
             REPLACE fi.etiliquido WITH tempsql_fncmp.etiliquido
             REPLACE fi.tiliquido WITH tempsql_fncmp.etiliquido*200.248
          ENDIF
       ELSE
          SELECT fi
          REPLACE fi.qtt WITH 0
          REPLACE fi.etiliquido WITH 0
       ENDIF
       SELECT fi
       REPLACE fi.iva WITH tempsql_fncmp.iva
       REPLACE fi.tabiva WITH IIF(tempsql_fncmp.tabiva=0, uf_gerais_getumvalor("taxasIva", "codigo", "taxa = 0", "codigo asc"), tempsql_fncmp.tabiva)
       REPLACE fi.armazem WITH tempsql_fncmp.armazem
       SELECT fi
       REPLACE fi.unidade WITH tempsql_fncmp.unidade
       REPLACE fi.iva WITH tempsql_fncmp.iva
       REPLACE fi.ivaincl WITH tempsql_fncmp.ivaincl
       REPLACE fi.codigo WITH tempsql_fncmp.codigo
       REPLACE fi.cpoc WITH tempsql_fncmp.cpoc
       REPLACE fi.composto WITH tempsql_fncmp.composto
       REPLACE fi.oref WITH IIF(EMPTY(ALLTRIM(tempsql_fncmp.ref)), ALLTRIM(tempsql_fncmp.oref), ALLTRIM(tempsql_fncmp.ref))
       REPLACE fi.lote WITH tempsql_fncmp.lote
       REPLACE fi.usr1 WITH tempsql_fncmp.usr1
       REPLACE fi.usr2 WITH tempsql_fncmp.usr2
       REPLACE fi.usr3 WITH tempsql_fncmp.usr3
       REPLACE fi.usr4 WITH tempsql_fncmp.usr4
       REPLACE fi.usr5 WITH tempsql_fncmp.usr5
       REPLACE fi.cor WITH tempsql_fncmp.cor
       REPLACE fi.tam WITH tempsql_fncmp.tam
       REPLACE fi.fifref WITH tempsql_fncmp.fnfref
       REPLACE fi.familia WITH tempsql_fncmp.familia
       REPLACE fi.pbruto WITH tempsql_fncmp.pbruto
       REPLACE fi.stns WITH tempsql_fncmp.stns
       REPLACE fi.pv WITH tempsql_fncmp.pv
       REPLACE fi.epv WITH tempsql_fncmp.epv
       REPLACE fi.tmoeda WITH tempsql_fncmp.tmoeda
       lcsql = ""
       TEXT TO lcsql TEXTMERGE NOSHOW
                Select PCPOND,EPCPOND, epcult From ST (nolock) Where (Ref = '<<Alltrim(tempSQL_FNCMP.ref)>>' Or ref = '<<Alltrim(tempSQL_FNCMP.oref)>>') and site_nr=<<mysite_nr>>
       ENDTEXT
       IF uf_gerais_actgrelha("", "uc_PRECOCUSTO", lcsql)
          SELECT fi
          REPLACE fi.custo WITH uc_precocusto.epcult*200.482
          REPLACE fi.ecusto WITH uc_precocusto.epcult
       ENDIF
       SELECT fi
       REPLACE fi.slvu WITH tempsql_fncmp.slvu
       REPLACE fi.eslvu WITH tempsql_fncmp.eslvu
       REPLACE fi.sltt WITH tempsql_fncmp.sltt
       REPLACE fi.esltt WITH tempsql_fncmp.esltt
       REPLACE fi.slvumoeda WITH tempsql_fncmp.slvumoeda
       REPLACE fi.desconto WITH tempsql_fncmp.desconto
       REPLACE fi.desc2 WITH tempsql_fncmp.desc2
       REPLACE fi.desc3 WITH tempsql_fncmp.desc3
       REPLACE fi.desc4 WITH tempsql_fncmp.desc4
       REPLACE fi.num1 WITH tempsql_fncmp.num1
       REPLACE fi.marcada WITH tempsql_fncmp.marcada
       REPLACE fi.amostra WITH .T.
       REPLACE fi.pic WITH tempsql_fncmp.pic
       REPLACE fi.u_psico WITH tempsql_fncmp.u_psico
       REPLACE fi.u_benzo WITH tempsql_fncmp.u_benzo
       REPLACE fi.u_cnp WITH tempsql_fncmp.u_cnp
       REPLACE fi.usalote WITH tempsql_fncmp.usalote
       REPLACE fi.pic WITH tempsql_fncmp.pic
       SELECT ft
       IF ft.tipodoc==3 .AND. fi.etiliquido>0     
          SELECT fi
          REPLACE fi.etiliquido WITH fi.etiliquido*-1
          REPLACE fi.tiliquido WITH fi.tiliquido*-1
       ENDIF
       IF (ft.tipodoc==1 OR ft.tipodoc==2) .AND. fi.etiliquido<0     
          SELECT fi
          REPLACE fi.etiliquido WITH fi.etiliquido*-1
          REPLACE fi.tiliquido WITH fi.tiliquido*-1
       ENDIF
       SELECT fi2
       APPEND BLANK
       REPLACE fi2.fistamp WITH lcstamp
       REPLACE fi2.fnstamp WITH tempsql_fncmp.fnstamp

       IF USED("fi_trans_info") AND tempSQL_FNCMP.dispositivo_seguranca

         SELECT fi
         uf_insert_fi_trans_info_seminfo(fi.ref, fi.fistamp)

       ENDIF

       SELECT tempsql_fncmp
       importarfact.lordem = importarfact.lordem+1
    ENDSCAN
 ENDIF
 
ENDPROC
**
PROCEDURE uf_importarfact_setmovstock
 importarfact.movstock = 1
ENDPROC
**
PROCEDURE uf_importarfact_chksel
 LPARAMETERS uv_progclick
 IF  .NOT. USED("uc_docSel")
    CREATE CURSOR uc_docSel (stamp C(254))
 ENDIF
 IF importarfact.seltodosdocs
    importarfact.seltodosdocs = .F.
    IF  .NOT. uv_progclick
       DELETE FROM uc_docSel
       SELECT ucrsimportdocs
       GOTO TOP
       REPLACE ucrsimportdocs.copiar WITH 0 FOR 1=1
       IF importarfact.seltodos
          importarfact.pageframe1.page1.check6.click(.T.)
       ENDIF
       SELECT ucrsimportdocs
       GOTO TOP
    ENDIF
 ELSE
    importarfact.seltodosdocs = .T.
    IF  .NOT. uv_progclick
       importarfact.pageframe1.page1.grid1.setfocus()
       SELECT ucrsimportdocs
       REPLACE ucrsimportdocs.copiar WITH 1 FOR 1=1
       INSERT INTO uc_docSel SELECT cabstamp FROM ucrsImportDocs WHERE copiar=1 .AND.  .NOT. DELETED()
       SELECT ucrsimportdocs
       GOTO TOP
    ENDIF
 ENDIF
 uf_importarfact_actualizalinhasdocs()
 IF importarfact.seltodosdocs .AND.  .NOT. importarfact.seltodos
    importarfact.pageframe1.page1.check6.click(.F.)
 ENDIF
 importarfact.pageframe1.page1.gridpesq.refresh()
ENDPROC
**

FUNCTION uf_importarfact_scanner

    importarfact.txtscanner.Value = strtran(importarfact.txtscanner.Value, chr(39), '')

    If Empty(alltrim(importarfact.txtscanner.value))
		RETURN .F.
	ENDIF

    lcRef	= UPPER(alltrim(importarfact.txtscanner.value))

    IF !EMPTY(lcRef) AND AT(" ", ALLTRIM(lcRef)) = 0 AND LEN(ALLTRIM(lcRef)) > 20
        uv_ref = uf_gerais_getRefFromQR(ALLTRIM(lcRef))

        IF !EMPTY(uv_ref)

            importarfact.Pageframe1.Page1.txtProd.value = ALLTRIM(uv_ref)
        
            uf_importarfact_actualizadocs()	

        ENDIF

    ELSE

        importarfact.Pageframe1.Page1.txtProd.value = ALLTRIM(lcRef)

        uf_importarfact_actualizadocs()	

    ENDIF

ENDFUNC