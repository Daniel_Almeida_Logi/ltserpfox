**
PROCEDURE uf_mvr_chama
 IF TYPE("MVR")<>"U"
    mvr.show
 ELSE
    DO FORM MVR
 ENDIF
ENDPROC
**
PROCEDURE uf_mvr_gravar
 uf_mvr_calculacompartaltir()
ENDPROC
**
PROCEDURE uf_mvr_init
 mvr.menu1.estado("", "", "Alterar", .T.)
 SELECT ft2
 mvr.txtcod1.value = ft2.u_codigo
 mvr.txtentidade1.value = ft2.u_design
 mvr.txtcod2.value = ft2.u_codigo2
 mvr.txtentidade2.value = ft2.u_design2
 SELECT fi
 mvr.txtnmdoc.value = ALLTRIM(fi.nmdoc)
 mvr.txtfno.value = fi.fno
 mvr.txtref.value = fi.ref
 mvr.txtdesign.value = fi.design
 mvr.txtopvp.value = ROUND(fi.u_epvp, 2)
 IF  .NOT. EMPTY(fi.u_epref)
    mvr.txtopref.value = ROUND(fi.u_epref, 2)
 ELSE
    mvr.txtopref.value = 0.00
 ENDIF
 mvr.txtoepv.value = ROUND(fi.epv, 2)
 mvr.txtoettent1.value = ROUND(fi.u_ettent1, 2)
 mvr.txtoettent2.value = ROUND(fi.u_ettent2, 2)
 mvr.txtoqt.value = fi.qtt
 mvr.txtodiploma.value = fi.u_diploma
 mvr.txtodiploma.enabled = .F.
 mvr.txtpvp.value = ROUND(fi.u_epvp, 2)
 mvr.txtepv.value = ROUND(fi.epv, 2)
 mvr.txtpref.value = ROUND(fi.u_epref, 2)
 mvr.txtcomp.value = 0
 mvr.txtcomp2.value = 0
 mvr.txtocomp.value = 0
 mvr.txtocomp2.value = 0
 SELECT ft2
 IF EMPTY(ft2.u_codigo2)
    mvr.txtcomp2.readonly = .T.
 ENDIF
 mvr.txtettent1.value = ROUND(fi.u_ettent1, 2)
 mvr.txtettent2.value = ROUND(fi.u_ettent2, 2)
 mvr.txtqt.value = fi.qtt
 mvr.txtdiploma.value = fi.u_diploma
 mvr.txtdiploma.enabled = .F.
 mvr.txtotop5.value = ROUND(fi.pvpmaxre, 2)
 mvr.txttop5.value = ROUND(fi.pvpmaxre, 2)
 uf_mvr_calculapercentcompir()
ENDPROC
**
PROCEDURE uf_mvr_actpvpepv
 IF mvr.txtcomp.value==0 .AND. mvr.txtcomp2.value==0
    uf_perguntalt_chama("Ambas as taxas n�o podem ser 0%.", "OK", "", 48)
    mvr.txtcomp.value = mvr.txtocomp.value
    mvr.txtcomp2.value = mvr.txtocomp2.value
    uf_mvr_calculacompartaltir(.T.)
    RETURN
 ENDIF
 IF (mvr.txtcomp.value<0) .OR. (mvr.txtcomp2.value<0)
    uf_perguntalt_chama("A taxa de comparticipa��o n�o pode ser negativa.", "OK", "", 48)
    mvr.txtcomp.value = mvr.txtocomp.value
    mvr.txtcomp2.value = mvr.txtocomp2.value
    uf_mvr_calculacompartaltir(.T.)
    RETURN
 ENDIF
 IF (mvr.txtcomp.value>100) .OR. (mvr.txtcomp2.value>100)
    uf_perguntalt_chama("A taxa de comparticipa��o n�o pode ser superior a 100.", "OK", "", 48)
    mvr.txtcomp.value = mvr.txtocomp.value
    mvr.txtcomp2.value = mvr.txtocomp2.value
    uf_mvr_calculacompartaltir(.T.)
    RETURN
 ENDIF
 IF mvr.txtpref.value<0
    uf_perguntalt_chama("O pre�o de refer�ncia n�o pode ser negativo.", "OK", "", 48)
    mvr.txtpref.value = mvr.txtopref.value
    uf_mvr_calculacompartaltir(.T.)
    RETURN
 ENDIF
 uf_mvr_calculacompartaltir(.T.)
ENDPROC
**
PROCEDURE uf_mvr_calculapercentcompir
 LOCAL lcsql, tccodigo, tccodigo2, tcref, tcdiploma, lcchoosepvp
 STORE '' TO lcsql, tccodigo, tccodigo2, tcref, tcdiploma
 LOCAL lccomp1, lccomp2
 STORE .F. TO lctemval1, lctemval2
 tccodigo = ALLTRIM(mvr.txtcod1.value)
 tccodigo2 = ALLTRIM(mvr.txtcod2.value)
 tcref = ALLTRIM(mvr.txtref.value)
 tcdiploma = ALLTRIM(mvr.txtdiploma.value)
 lcchoosepvp = mvr.txtpvp.value
 IF EMPTY(tcdiploma)
    tcdiploma = 'xxx'
 ENDIF
 TEXT TO lcsql TEXTMERGE NOSHOW
		exec up_receituario_CalcCompart '<<tcDiploma>>', <<lcChoosePvp>>, '<<tcRef>>', '<<tcCodigo>>',<<mysite_nr>>
 ENDTEXT
 IF  .NOT. uf_gerais_actgrelha("", "uCrsCptVal", lcsql)
 ELSE
    IF RECCOUNT("uCrsCptVal")>0
       lctemval1 = .T.
       IF  .NOT. EMPTY(ucrscptval.percentagem)
          mvr.txtcomp.value = ROUND(ucrscptval.percentagem, 0)
          mvr.txtocomp.value = ROUND(ucrscptval.percentagem, 0)
       ENDIF
    ENDIF
 ENDIF
 uf_gerais_actgrelha("", 'uCrsCptCompl', [select compl from cptpla (nolock) where codigo=']+ALLTRIM(tccodigo)+['])
 IF USED("uCrsCptCompl")
    SELECT ucrscptcompl
    IF RECCOUNT('uCrsCptCompl')>0
       TEXT TO lcsql TEXTMERGE NOSHOW
				exec up_receituario_CalcCompart '<<ALLTRIM(tcDiploma)>>', <<lcChoosePvp>>, '<<ALLTRIM(tcRef)>>', '<<ALLTRIM(uCrsCptCompl.compl)>>',<<mysite_nr>>
       ENDTEXT
       uf_gerais_actgrelha("", 'uCrsCptVal2', lcsql)
       SELECT ucrscptval2
       IF RECCOUNT('uCrsCptVal2')>0
          lctemval2 = .T.
          IF  .NOT. EMPTY(ucrscptval2.percentagem)
             mvr.txtcomp2.value = ROUND(ucrscptval2.percentagem, 0)
             mvr.txtocomp2.value = ROUND(ucrscptval2.percentagem, 0)
          ENDIF
       ENDIF
    ENDIF
    fecha('uCrsCptCompl')
 ENDIF
 IF USED("uCrsCptVal")
    fecha("uCrsCptVal")
 ENDIF
 IF USED("uCrsCptVal2")
    fecha("uCrsCptVal2")
 ENDIF
ENDPROC
**
FUNCTION uf_mvr_calculacompartaltir
 LPARAMETERS tcbool
 IF EMPTY(mvr.txtcomp.value) .AND. EMPTY(mvr.txtcomp2.value)
    uf_perguntalt_chama("N�o pode comparticipar com ambas a 0%.", "OK", "", 48)
    RETURN .F.
 ENDIF
 LOCAL tccodigo, tccodigo2, tcref, tcdiploma
 STORE '' TO tccodigo, tccodigo2, tcref, tcdiploma
 tccodigo = ALLTRIM(mvr.txtcod1.value)
 tccodigo2 = ALLTRIM(mvr.txtcod2.value)
 tcref = ALLTRIM(mvr.txtref.value)
 tcdiploma = ALLTRIM(mvr.txtdiploma.value)
 uf_receituario_makeprecos(tccodigo, tcref, tcref, "fi", tcdiploma, .F., .T., .f., .f.,.f.)
 IF  .NOT. tcbool
    SELECT fi
    REPLACE fi.u_comp WITH .T.
    REPLACE fi.pvpmaxre WITH ROUND(mvr.txttop5.value, 2)
    IF  .NOT. (fi.opcao=='I')
       REPLACE fi.opcao WITH IIF(fi.u_epvp>fi.pvpmaxre, 'S', 'N')
    ENDIF
    LOCAL lcorigem
    SELECT ft
    lcorigem = 'Doc.: '+astr(ft.fno)+' Ref.: '
    SELECT fi
    lcorigem = lcorigem+ALLTRIM(fi.ref)
    SELECT ft2
    lcorigem = lcorigem+' Plano: '+ALLTRIM(tccodigo)
    IF  .NOT. EMPTY(tccodigo2)
       lcorigem = lcorigem+' | '+ALLTRIM(tccodigo2)
    ENDIF
    SELECT fi
    uf_gerais_registaocorrencia('Lotes', 'Manipula��o de Comparticipa��o', 1, ALLTRIM(lcorigem), '', ALLTRIM(fi.fistamp))
    uf_atendimento_fiiliq(.T.)
    SELECT fi
    IF fi.epv<>0
       IF fi.u_epvp=0
          REPLACE fi.u_epvp WITH fi.epv
       ENDIF
       IF fi.qtt>0
          SELECT fi
          REPLACE fi.u_txcomp WITH ROUND((fi.etiliquido*100)/(fi.u_epvp*fi.qtt), 2)
       ENDIF
    ELSE
       REPLACE fi.u_txcomp WITH 0
    ENDIF
    uf_atendimento_acttotaisft()
    uf_mvr_sair()
 ENDIF
ENDFUNC
**
PROCEDURE uf_mvr_sair
 mvr.hide
 mvr.release
ENDPROC
**
