PROCEDURE uf_faturacaoPagExterno_chama
   LPARAMETERS uv_nrAtend, uv_dtIni

	IF !uf_faturacaoPagExterno_criarCursores()
		RETURN .F.
	ENDIF


	IF !WEXIST("FATURACAOPAGEXTERNO")
	
		DO FORM FATURACAOPAGEXTERNO WITH uv_nrAtend, uv_dtIni
		
	ELSE
	
		FATURACAOPAGEXTERNO.show()
	
	ENDIF


ENDPROC

PROCEDURE uf_faturacaoPagExterno_sair

	IF WEXIST("FATURACAOPAGEXTERNO")
	
		FATURACAOPAGEXTERNO.hide()
		FATURACAOPAGEXTERNO.release()
	
	ENDIF

ENDPROC

FUNCTION uf_faturacaoPagExterno_criarCursores

	LOCAL uv_sql

	TEXT TO uv_sql TEXTMERGE NOSHOW

		exec up_faturacao_getDocsPagExt
			0,
			'',
			0,
			'',
			'19000101',
			'1900101',
			'',
			0,
			0,
			'',
			0

	ENDTEXT

	IF !uf_gerais_actGrelha("", "uc_ftPagExt", uv_sql)
		RETURN .F.
	ENDIF

	TEXT TO uv_sql TEXTMERGE NOSHOW

		exec up_faturacao_getPagExternoAtendimento '', 0

	ENDTEXT

	IF !uf_gerais_actGrelha("", "uc_pagExtAtendimento", uv_sql)
		RETURN .F.
	ENDIF

	RETURN .T.

ENDFUNC

FUNCTION uf_faturacaoPagExterno_getPagExtAtendimento
	LPARAMETERS uv_nrAtend

	IF EMPTY(uv_nrAtend)
		RETURN .F.
	ENDIF

	LOCAL uv_sql, loGrid, lcCursor, uv_porPagar

	loGrid = FATURACAOPAGEXTERNO.grdPag
	lcCursor = "uc_pagExtAtendimento"

	lnColumns = loGrid.ColumnCount

	IF lnColumns > 0

		DIMENSION laControlSource[lnColumns]
		FOR lnColumn = 1 TO lnColumns
			laControlSource[lnColumn] = loGrid.Columns[lnColumn].ControlSource
		ENDFOR
	ENDIF

	loGrid.RecordSource = ""

	uv_porPagar = IIF(FATURACAOPAGEXTERNO.porPagar, '1', '0')

	TEXT TO uv_sql TEXTMERGE NOSHOW

		exec up_faturacao_getPagExternoAtendimento '<<ALLTRIM(uv_nrAtend)>>', <<uv_porPagar>>

	ENDTEXT

	IF !uf_gerais_actGrelha("", "uc_pagExtAtendimento", uv_sql)
		RETURN .F.
	ENDIF

	loGrid.RecordSource = lcCursor

	FOR lnColumn = 1 TO lnColumns
		loGrid.Columns[lnColumn].ControlSource = laControlSource[lnColumn]
	ENDFOR

	FATURACAOPAGEXTERNO.refresh()

	RETURN .T.

ENDFUNC

FUNCTION uf_faturacaoPagExterno_atualizar

	IF !WEXIST("FATURACAOPAGEXTERNO")
		RETURN .F.
	ENDIF

	LOCAL uv_top, uv_nmdoc, uv_fno, uv_nrAtend, uv_dataIni, uv_dataFim, uv_nome, uv_no, uv_estab, loGrid, lcCursor, lnColumns, uv_sql, uv_porPagar

	loGrid = FATURACAOPAGEXTERNO.grdFT
	lcCursor = "uc_ftPagExt"

	lnColumns = loGrid.ColumnCount

	IF lnColumns > 0

		DIMENSION laControlSource[lnColumns]
		FOR lnColumn = 1 TO lnColumns
			laControlSource[lnColumn] = loGrid.Columns[lnColumn].ControlSource
		ENDFOR
	ENDIF

	loGrid.RecordSource = ""

	WITH FATURACAOPAGEXTERNO

		uv_top = .ultimos.value
		uv_nmdoc = .documento.value
		uv_fno = .numdoc.value
		uv_nrAtend = .txtNAtend.value
		uv_dataini = .dataIni.value
		uv_dataFim = .dataFim.value
		uv_nome = .nome.value
		uv_no = .no.value
		uv_estab = .estab.value
		uv_porPagar = IIF(.porPagar, '1', '0')

	ENDWITH

	TEXT TO uv_sql TEXTMERGE NOSHOW

		exec up_faturacao_getDocsPagExt
			<<IIF(EMPTY(uv_top), 0, uv_top)>>,
			'<<uv_nmdoc>>',
			<<IIF(EMPTY(uv_fno), 0, uv_fno)>>,
			'<<ALLTRIM(uv_nrAtend)>>',
			'<<uf_gerais_getdate(ALLTRIM(uv_dataini),"SQL")>>',
			'<<uf_gerais_getdate(ALLTRIM(uv_dataFim),"SQL")>>',
			'<<uv_nome>>',
			<<IIF(EMPTY(uv_no), 0, uv_no)>>,
			<<IIF(EMPTY(uv_estab), 0, uv_estab)>>,
			'<<ALLTRIM(mySite)>>',
			<<uv_porPagar>>

	ENDTEXT

	IF !uf_gerais_actGrelha("", "uc_ftPagExt", uv_sql)
		RETURN .F.
	ENDIF

	loGrid.RecordSource = lcCursor
        
	FOR lnColumn = 1 TO lnColumns
		loGrid.Columns[lnColumn].ControlSource = laControlSource[lnColumn]
	ENDFOR

	SELECT uc_ftPagExt
	GO TOP

	uf_faturacaoPagExterno_getPagExtAtendimento(uc_ftPagExt.nrAtend)

	FATURACAOPAGEXTERNO.refresh()

	RETURN .T.

ENDFUNC

FUNCTION uf_faturacaoPagExterno_consultar

	IF !USED("uc_pagExtAtendimento") OR !USED("uc_ftPagExt")
		RETURN .F.
	ENDIF

	LOCAL uv_hasConsult, uv_count, uv_rec

	uv_hasConsult = .F.


	uv_rec = 1

	regua(0, uv_count, "A consultar Pagamento...")

	SELECT uc_pagExtAtendimento
	SCAN 

		regua(1, uv_rec, "A consultar Pagamento...")

		uv_hasConsult = .T.

		IF !uf_faturacaoPagExterno_consultPagExterno(uc_pagExtAtendimento.operationID, uc_pagExtAtendimento.typePayment, uc_pagExtAtendimento.typePaymentDesc, uc_pagExtAtendimento.receiverID, uc_pagExtAtendimento.receiverName, uc_pagExtAtendimento.nrAtend)
			regua(2)
			RETURN .F.
		ENDIF

		uv_rec = uv_rec + 1

		SELECT uc_pagExtAtendimento
	ENDSCAN

	SELECT uc_ftPagExt

	uf_faturacaoPagExterno_getPagExtAtendimento(uc_ftPagExt.nrAtend)

	regua(2)

	uf_perguntalt_chama("Pagamento consultado com sucesso.", "OK", "", 64)

	

ENDFUNC

FUNCTION uf_faturacaoPagExterno_consultPagExterno
	LPARAMETERS uv_operationID, uv_typePayment, uv_typePaymentDesc, uv_receiverID, uv_receiverName, uv_nrAtend

	IF EMPTY(uv_operationID) OR EMPTY(uv_typePayment) OR EMPTY(uv_typePaymentDesc) OR EMPTY(uv_receiverID) OR EMPTY(uv_receiverName)
		RETURN .F.
	ENDIF

	LOCAL uv_userName, uv_userPass, uv_entityType, uv_teste, uv_token, uv_sql

	uv_userName = uf_gerais_getParameter_site("ADM0000000187", "TEXT", mySite)
	uv_userPass = uf_gerais_getParameter_site("ADM0000000188", "TEXT", mySite)
	uv_entityType = uf_gerais_getParameter_site("ADM0000000189", "TEXT", mySite)

	uv_token = uf_gerais_stamp()

   	IF !uf_gerais_getparameter("ADM0000000227", "BOOL")
    	uv_teste = 0
 	ELSE
    	uv_teste = 1
 	ENDIF

	TEXT TO uv_sql TEXTMERGE NOSHOW

		EXEC up_vendas_requestPagExterno
		'<<ALLTRIM(uv_token)>>',
		'<<ALLTRIM(uv_userName)>>',
		'<<ALLTRIM(uv_userPass)>>',
		2,
		<<ASTR(uv_typePayment)>>,
		'<<ALLTRIM(uv_typePaymentDesc)>>',
		'<<ALLTRIM(uv_receiverID)>>',
		'<<ALLTRIM(uv_receiverName)>>',
		'<<ALLTRIM(uv_entityType)>>',
		0,
		'',
		'',
		'',
		'<<ALLTRIM(mySite)>>',
		<<ASTR(uv_teste)>>,
		0,
		'<<ALLTRIM(m_chinis)>>',
		'<<ALLTRIM(uv_operationID)>>',
		'<<ALLTRIM(uv_nrAtend)>>'

	ENDTEXT

	IF !uf_gerais_actGrelha("", "", uv_sql)
		RETURN .F.
	ENDIF

	LOCAL lcNomeJar, LcWsPath, lcWsDir, lcAdd  
   
	lcNomeJar = "ltsPaymentMethods.jar"
	LcWsPath  = ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\ltsPaymentMethods\'+ALLTRIM(lcNomeJar)+' ' +' --TOKEN='+ALLTRIM(uv_token) +' --IDCL='+ALLTRIM(uCrsE1.id_lt)
	lcWsDir	  =  ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\ltsPaymentMethods\'
	lcAdd  	  = "&"
				
	LcWsPath  = "cmd /c cd /d "+lcWsDir+" "+lcAdd+lcAdd+" javaw -jar " +LcWsPath 
	owsshell  = CREATEOBJECT("WScript.Shell")
	owsshell.run(lcwspath, 0, .T.) 
	
	IF uv_teste = 1
		_CLIPTEXT = lcwspath
	ENDIF

	IF !uf_gerais_actGrelha("","uc_pagExternoResp", "SELECT * FROM paymentMethodResponse(nolock) where token = '" + ALLTRIM(uv_token) + "'") 
		RETURN .F.	
	ENDIF

	select uc_pagExternoResp
	
	IF uc_pagExternoResp.statusID <> 200

		uf_perguntalt_chama(ALLTRIM(LEFT(uc_pagExternoResp.statusDesc,254)), "OK", "", 16)
		RETURN .F.

	ENDIF

	RETURN .T.

ENDFUNC

FUNCTION uf_faturacaoPagExterno_reenviar

	IF !USED("uc_pagExtAtendimento") OR !USED("uc_ftPagExt")
		RETURN .F.
	ENDIF

	SELECT uc_pagExtAtendimento
		
	IF EMPTY(uc_pagExtAtendimento.operationID)
		RETURN .F.
	ENDIF

	LOCAL uv_sql, uv_msgErro

	DO CASE
		CASE uc_pagExtAtendimento.statePaymentID = 1

			uf_perguntalt_chama("Este pedido j� se encontra pago.", "OK", "", 16)
			RETURN .F.			

		CASE uc_pagExtAtendimento.statePaymentID = 4

			uf_perguntalt_chama("Este pedido foi cancelado pelo operador.", "OK", "", 16)
			RETURN .F.			

		CASE uc_pagExtAtendimento.statePaymentID = 0 AND EMPTY(uc_pagExtAtendimento.statePaymentIdDesc)

			uf_perguntalt_chama("Por favor consulte o estado deste pedido antes de reenviar.", "OK", "", 16)
			RETURN .F.			

		OTHERWISE
	ENDCASE

	IF INLIST(uc_pagExtAtendimento.statePaymentID, 1, 4)
		uf_perguntalt_chama("Este pedido ", "OK", "", 16)
		RETURN .F.
	ENDIF

	SELECT uc_ftPagExt

	IF !uf_faturacaoPagExterno_reenviarPagExterno(ALLTRIM(uc_pagExtAtendimento.token), uc_ftPagExt.no, uc_ftPagExt.estab, uc_ftPagExt.nrAtend, ALLTRIM(uc_pagExtAtendimento.operationId), uc_pagExtAtendimento.ousrdata)
		uf_perguntalt_chama("N�o foi poss�vel reenviar o pedido.", "OK", "", 16)
		RETURN .F.
	ENDIF

	SELECT uc_pagExtAtendimento

	TEXT TO uv_sql TEXTMERGE NOSHOW

		UPDATE 
			b_PagCentral_ext 
		SET 
			statePaymentID = -4, 
			statePaymentIdDesc = 'Cancelada pelo Operador', 
			usrdata = GETDATE(), 
			usrinis = '<<ALLTRIM(m_chinis)>>'
		WHERE
			operationID = '<<uc_pagExtAtendimento.operationID>>'
         	and ousrdata <= DATEADD(DAY, 60, '<<uf_gerais_getDate(uc_pagExtAtendimento.ousrdata, "SQL")>>')

	ENDTEXT

	IF !uf_gerais_actGrelha("", "", uv_sql)
		uf_perguntalt_chama("Erro a atualizar o pedido. Por favor contacte o suporte.", "OK", "", 16)
		RETURN .F.
	ENDIF

	SELECT uc_ftPagExt

	uf_faturacaoPagExterno_getPagExtAtendimento(uc_ftPagExt.nrAtend)

	RETURN .T.

ENDFUNC

FUNCTION uf_faturacaoPagExterno_reenviarPagExterno
	LPARAMETERS uv_tokenOri, uv_no, uv_estab, uv_nrAtend, uv_operationID, uv_data

	IF EMPTY(uv_tokenOri) OR EMPTY(uv_no) OR TYPE("uv_estab") <> 'N' OR EMPTY(uv_nrAtend) OR EMPTY(uv_operationID) OR TYPE("uv_data") <> 'T' 
		RETURN .F.
	ENDIF

	LOCAL uv_curPagExt

	TEXT TO uv_sql TEXTMERGE NOSHOW
		SELECT * FROM b_PagCentral_ext(nolock) WHERE 
			operationID = '<<ALLTRIM(uv_operationID)>>'
			and token <> '<<ALLTRIM(uv_tokenOri)>>'
			and ousrdata <= DATEADD(DAY, 60, '<<uf_gerais_getDate(uv_data, "SQL")>>')
	ENDTEXT


	IF !uf_gerais_actGrelha("", "uc_pagExtSoma", uv_sql)
		RETURN .F.
	ENDIF

	SELECT uc_pagExtAtendimento

	uf_pagExterno_chama(ALLTRIM(uc_pagExtAtendimento.modoPag), 0, 0, ALLTRIM(uc_pagExtAtendimento.email), ALLTRIM(uc_pagExtAtendimento.phone), ALLTRIM(uc_pagExtAtendimento.description), ASTR(uc_pagExtAtendimento.timeLimitDays))

	uv_curPagExt = 'uc_dadosPagExterno' + ALLTRIM(uc_pagExtAtendimento.modoPag)

	IF !USED(uv_curPagExt)
		RETURN .F.
	ENDIF

	CREATE CURSOR uc_pagsExterno(modoPag C(10), tlmvl C(50), email C(254), duracao C(50), descricao M, valor N(14,2), tipoPagExt N(3), tipoPagExtDesc C(100), receiverID C(100), receiverName C(100), no N(9), estab N(5), nrAtend C(20))

	SELECT uc_pagsExterno
	APPEND BLANK

	REPLACE uc_pagsExterno.modoPag WITH ALLTRIM(uc_pagExtAtendimento.modoPag)
	REPLACE uc_pagsExterno.tlmvl WITH ALLTRIM(&uv_curPagExt..tlmvl)
	REPLACE uc_pagsExterno.email WITH ALLTRIM(&uv_curPagExt..email)
	REPLACE uc_pagsExterno.duracao WITH ALLTRIM(&uv_curPagExt..duracao)
	REPLACE uc_pagsExterno.descricao WITH ALLTRIM(&uv_curPagExt..descricao)
	REPLACE uc_pagsExterno.valor WITH uc_pagExtAtendimento.valor
	REPLACE uc_pagsExterno.tipoPagExt WITH uc_pagExtAtendimento.typePayment
	REPLACE uc_pagsExterno.tipoPagExtDesc WITH uc_pagExtAtendimento.typePaymentDesc
	REPLACE uc_pagsExterno.receiverID WITH uc_pagExtAtendimento.receiverID
	REPLACE uc_pagsExterno.receiverName WITH uc_pagExtAtendimento.receiverName
	REPLACE uc_pagsExterno.no WITH uv_no
	REPLACE uc_pagsExterno.estab WITH uv_estab
	REPLACE uc_pagsExterno.nrAtend WITH ALLTRIM(uv_nrAtend)

	LOCAL uv_userName, uv_userPass, uv_entityType, uv_teste, uv_token, uv_newOperationID

	uv_userName = uf_gerais_getParameter_site("ADM0000000187", "TEXT", mySite)
	uv_userPass = uf_gerais_getParameter_site("ADM0000000188", "TEXT", mySite)
	uv_entityType = uf_gerais_getParameter_site("ADM0000000189", "TEXT", mySite)

	IF EMPTY(uv_userName) OR EMPTY(uv_userPass) OR EMPTY(uv_entityType)
		RETURN .F.
	ENDIF

	IF !uf_gerais_getparameter("ADM0000000227", "BOOL")
			uv_teste = 0
		ELSE
			uv_teste = 1
		ENDIF   

	SELECT uc_pagsExterno
	GO TOP

	uv_token = uf_gerais_stamp()

	TEXT TO uv_sql TEXTMERGE NOSHOW

		EXEC up_vendas_requestPagExterno
			'<<ALLTRIM(uv_token)>>',
			'<<ALLTRIM(uv_userName)>>',
			'<<ALLTRIM(uv_userPass)>>',
			1,
			<<ASTR(uc_pagsExterno.tipoPagExt)>>,
			'<<ALLTRIM(uc_pagsExterno.tipoPagExtDesc)>>',
			'<<ALLTRIM(uc_pagsExterno.receiverID)>>',
			'<<ALLTRIM(uc_pagsExterno.receiverName)>>',
			'<<ALLTRIM(uv_entityType)>>',
			<<ASTR(uc_pagsExterno.valor,14,2)>>,
			'<<ALLTRIM(uc_pagsExterno.email)>>',
			'<<ALLTRIM(uc_pagsExterno.tlmvl)>>',
			'<<ALLTRIM(uc_pagsExterno.descricao)>>',
			'<<ALLTRIM(mySite)>>',
			<<ASTR(uv_teste)>>,
			<<ASTR(VAL(uc_pagsExterno.duracao))>>,
			'<<ALLTRIM(m_chinis)>>',
			'',
			'<<ALLTRIM(uc_pagsExterno.nrAtend)>>'
	ENDTEXT

	IF !uf_gerais_actGrelha("","",uv_sql)
		RETURN .F.
	ENDIF

	LOCAL lcNomeJar, LcWsPath, lcWsDir, lcAdd  

	lcNomeJar = "ltsPaymentMethods.jar"
	LcWsPath  = ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\ltsPaymentMethods\'+ALLTRIM(lcNomeJar)+' ' +' --TOKEN='+ALLTRIM(uv_token) +' --IDCL='+ALLTRIM(uCrsE1.id_lt)
	lcWsDir	  =  ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\ltsPaymentMethods\'
	lcAdd  	  = "&"
				
	LcWsPath  = "cmd /c cd /d "+lcWsDir+" "+lcAdd+lcAdd+" javaw -jar " +LcWsPath 
	owsshell  = CREATEOBJECT("WScript.Shell")
	owsshell.run(lcwspath, 0, .T.) 
	
	IF uv_teste = 1
		_CLIPTEXT = lcwspath
	ENDIF

	IF !uf_gerais_actGrelha("","uc_pagExternoResp", "SELECT * FROM paymentMethodResponse(nolock) where token = '" + ALLTRIM(uv_token) + "'") 
		RETURN .F.	
	ENDIF

	select uc_pagExternoResp
	
	IF uc_pagExternoResp.statusID <> 200

		uf_perguntalt_chama(ALLTRIM(LEFT(uc_pagExternoResp.statusDesc,254)), "OK", "", 16)
		RETURN .F.

	ENDIF

	IF EMPTY(uc_pagExternoResp.operationID)
		RETURN .F.
	ENDIF

	uv_newOperationID = uc_pagExternoResp.operationID

uf_gerais_regista_logs_passos("B_PAGCENTRAL_ext",nrAtend,"b_pag4","insert antes uf_faturacaoPagExterno_reenviarPagExterno")

	TEXT TO uv_sql TEXTMERGE NOSHOW

		INSERT INTO b_pagCentral_ext (token,nrAtend,operationId,typePayment,typePaymentDesc,status,statusDesc,
						statePaymentID,statePaymentIDDesc,receiverid,receivername,site,ousrinis,ousrdata,usrinis,usrdata, limitDate)
				VALUES (
						'<<ALLTRIM(uv_token)>>',
						'<<ALLTRIM(uc_pagsExterno.nrAtend)>>',
						'<<ALLTRIM(uv_newOperationID)>>',
						<<ASTR(uc_pagsExterno.tipoPagExt)>>,
						'<<ALLTRIM(uc_pagsExterno.tipoPagExtDesc)>>',
						'',
						'',
						0,
						'',
						'<<ALLTRIM(uc_pagsExterno.receiverID)>>',
						'<<ALLTRIM(uc_pagsExterno.receiverName)>>',
						'<<ALLTRIM(mySite)>>',
						'<<ALLTRIM(m_chinis)>>',
						GETDATE(),
						'<<ALLTRIM(m_chinis)>>',
						GETDATE(),
						'<<IIF(VAL(uc_pagsExterno.duracao) = 0, "19000101", uf_gerais_getDate((DATE() + VAL(uc_pagsExterno.duracao)+ 1),"SQL")))>>'
					)

	ENDTEXT

	IF !uf_gerais_actGrelha("","",uv_sql)
		RETURN .F.
	ENDIF   

uf_gerais_regista_logs_passos("B_PAGCENTRAL_ext",nrAtend,"b_pag4","insert depois uf_faturacaoPagExterno_reenviarPagExterno")

	SELECT uc_pagExtSoma
	GO TOP
	
uf_gerais_regista_logs_passos("B_PAGCENTRAL_ext",nrAtend,"b_pag4","insert 2 antes uf_faturacaoPagExterno_reenviarPagExterno")

	SCAN

		uv_token = uf_gerais_stamp()

		TEXT TO uv_sql TEXTMERGE NOSHOW

			INSERT INTO b_pagCentral_ext (token,nrAtend,operationId,typePayment,typePaymentDesc,status,statusDesc,
					statePaymentID,statePaymentIDDesc,receiverid,receivername,site,ousrinis,ousrdata,usrinis,usrdata, limitDate)
			VALUES (
					'<<ALLTRIM(uv_token)>>',
					'<<ALLTRIM(uc_pagExtSoma.nrAtend)>>',
					'<<ALLTRIM(uv_newOperationID)>>',
					<<ASTR(uc_pagsExterno.tipoPagExt)>>,
					'<<ALLTRIM(uc_pagsExterno.tipoPagExtDesc)>>',
					'',
					'',
					0,
					'',
					'<<ALLTRIM(uc_pagsExterno.receiverID)>>',
					'<<ALLTRIM(uc_pagsExterno.receiverName)>>',
					'<<ALLTRIM(mySite)>>',
					'<<ALLTRIM(m_chinis)>>',
					GETDATE(),
					'<<ALLTRIM(m_chinis)>>',
					GETDATE(),
					'<<IIF(VAL(uc_pagsExterno.duracao) = 0, "19000101", uf_gerais_getDate((DATE() + VAL(uc_pagsExterno.duracao)+ 1),"SQL")))>>'
				)

		ENDTEXT

		IF !uf_gerais_actGrelha("","",uv_sql)
			RETURN .F.
		ENDIF   

		SELECT uc_pagExtSoma
	ENDSCAN

uf_gerais_regista_logs_passos("B_PAGCENTRAL_ext",nrAtend,"b_pag4","insert 2 depois uf_faturacaoPagExterno_reenviarPagExterno")


	FECHA("uc_pagsExterno")
	FECHA(uv_curPagExt)
	FECHA("uc_modosPagExterno")

	RETURN .T.

ENDFUNC

FUNCTION uf_faturacaoPagExterno_consultarTodos

	LOCAL uv_curReg

	FATURACAOPAGEXTERNO.lockScreen = .T.

	SELECT uc_ftPagExt
	GO TOP

	uv_curReg = 1
	regua(0, RECCOUNT("uc_ftPagExt"), "A consultar pagamentos...")

	SCAN

		regua(1, uv_curReg, "A consultar pagamentos...")

		uf_faturacaoPagExterno_getPagExtAtendimento(uc_ftPagExt.nrAtend)

		SELECT uc_pagExtAtendimento
		GO TOP
		SCAN

			IF !uf_faturacaoPagExterno_consultPagExterno(uc_pagExtAtendimento.operationID, uc_pagExtAtendimento.typePayment, uc_pagExtAtendimento.typePaymentDesc, uc_pagExtAtendimento.receiverID, uc_pagExtAtendimento.receiverName)
				regua(2)
				FATURACAOPAGEXTERNO.lockScreen = .F.
				RETURN .F.
			ENDIF

			SELECT uc_pagExtAtendimento
		ENDSCAN

		uv_curReg = uv_curReg+ 1

		SELECT uc_ftPagExt
	ENDSCAN

	SELECT uc_ftPagExt
	GO TOP

	uf_faturacaoPagExterno_getPagExtAtendimento(uc_ftPagExt.nrAtend)

	regua(2)

	uf_perguntalt_chama("Pagamentos consultados com sucesso.", "OK", "", 64)

	FATURACAOPAGEXTERNO.lockScreen = .F.

ENDFUNC