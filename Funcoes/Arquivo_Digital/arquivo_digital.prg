**
FUNCTION uf_arquivodigital_chama
	PUBLIC myFilePathDataMatrix
	myFilePathDataMatrix = ""
	
	LOCAL lcCaminhoArquivo 
	lcCaminhoArquivo = ALLTRIM(mypath)+ "\FicheirosArquivo"
		
	IF !DIRECTORY(lcCaminhoArquivo)
		uf_perguntalt_chama("DIRECTORIO DE ARQUIVO DIGITAL N�O ENCONTRADO. CONTACTE O SUPORTE.","OK","",32)
		RETURN .f.
	ENDIF 
		
	regua(0,6,"A carregar painel...")
	regua(1,1,"A carregar painel...")
	**Valida Licenciamento	
	IF (uf_gerais_addConnection('ARQ_DIGITA') != .f.)
				
		&& CONTROLA PERFIS DE ACESSO
		IF uf_gerais_validaPermUser(ch_userno,ch_grupo,'Arquivo Digital')
			IF !myArquivoDigital AND !myArquivoDigitalPdf
				uf_perguntalt_chama("PARA ADQUIRIR ESTE M�DULO POR FAVOR ENTRE EM CONTACTO COM SUPORTE! PE�A J� A SUA DEMONSTRA��O.","OK","",64)
				regua(2)
				RETURN .f.
			ENDIF
			
			IF TYPE("PESQUTENTES") != "U"
				uf_pesqutentes_sair()
			ENDIF
			IF !(TYPE("ATENDIMENTO") == "U")
				uf_perguntalt_chama("N�O PODE ABRIR O ECR� DE ARQUIVO DIGITAL TAL�ES ENQUANTO O ECR� DE ATENDIMENTO ESTIVER ABERTO.","OK","",48)
				regua(2)
				RETURN .f.
			ENDIF
			IF !(TYPE("FACTURACAO") == "U")
				uf_perguntalt_chama("N�O PODE ABRIR O ECR� DE ARQUIVO DIGITAL TAL�ES ENQUANTO O ECR� DE FACTURA��O ESTIVER ABERTO.","OK","",48)
				regua(2)
				RETURN .f.
			ENDIF
			IF !(TYPE("IDENTIFICACAO") == "U")
				uf_perguntalt_chama("N�O PODE ABRIR O ECR� DE ARQUIVO DIGITAL TAL�ES ENQUANTO O ECR� DE IDENTIFICA��O PARA O ATENDIMENTO ESTIVER ABERTO.","OK","",48)
				regua(2)
				RETURN .f.
			ENDIF
		ELSE
			uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ACEDER AO PAINEL ARQUIVO DIGITAL.", "OK","",48)
			regua(2)
			RETURN .f.
		ENDIF
	ENDIF
	
	regua(1,2,"A carregar painel...")
	
	IF TYPE("ARQUIVO_DIGITAL") != "U"
		regua(1,6,"A carregar painel...")
		ARQUIVO_DIGITAL.show
	ELSE
		regua(1,3,"A carregar painel...")
		IF !USED("ucrsComboFactPesq")
			lcSQL = ""
			TEXT TO lcSQL TEXTMERGE NOSHOW
				exec up_perfis_validaAcessoDocs '<<ch_userno>>', '<<ALLTRIM(ch_grupo)>>', 'Visualizar', 'FT', 0
			ENDTEXT
			
			If !uf_gerais_actGrelha("","ucrsComboFactPesq",lcSQL)
				UF_PERGUNTALT_CHAMA("N�O FOI POSS�VEL ENCONTRAR DOCUMENTOS.","OK","",16)
				RETURN .f.
			ELSE
				SELECT ucrsComboFactPesq
				SCAN
					IF ucrsComboFactPesq.u_integra
						DELETE
					ENDIF
				ENDSCAN 
				GO TOP
			ENDIF
		ENDIF
		
		regua(1,4,"A carregar painel...")
		
		IF !USED("uc_ListagemDocsArqDigTalLt")
			uf_gerais_actGrelha("","uc_ListagemDocsArqDigTalLt","set fmtonly on exec up_facturacao_pesquisarDocumentosAdd 30, '', 0, 0, -1, '19000101',  '30001231', '', 1, 'Administrador','','' set fmtonly off")
		ELSE
			SELECT uc_ListagemDocsArqDigTalLt
			GO TOP
			SCAN
				REPLACE uc_ListagemDocsArqDigTalLt.escolha WITH .f.
			ENDSCAN
			SELECT uc_ListagemDocsArqDigTalLt
			GO TOP
		ENDIF
		
		regua(1,5,"A carregar painel...")
		
		IF USED("uCrsListFiles")
			fecha("uCrsListFiles")
		ENDIF
		CREATE CURSOR uCrsListFiles(filename c(254), filesize c(10), created d)
		
		fso=createobject("scripting.filesystemobject")
		fld=fso.getfolder(lcCaminhoArquivo)
		
		for each fil in fld.files
			SELECT uCrsListFiles
			APPEND BLANK
			replace ;
				uCrsListFiles.filename	WITH fil.name ;
				uCrsListFiles.filesize	WITH ALLTRIM(str(fil.size * 0.0009765625))+'KB' ; &&converte bytes em kilobytes
				uCrsListFiles.created 	WITH  fil.DateCreated
		NEXT

		SELECT uCrsListFiles
		index on created TAG created desc
		GO TOP
		
		regua(1,6,"A carregar painel...")
		
		DO FORM ARQUIVO_DIGITAL
		
		ARQUIVO_DIGITAL.menu1.adicionaOpcao("actualizar", "Actualizar", myPath + "\imagens\icons\actualizar_w.png", "uf_arquivodigital_pesquisa", "A")
		ARQUIVO_DIGITAL.menu1.adicionaOpcao("selTodos", "Sel. Todos", myPath + "\imagens\icons\unchecked_w.png", "uf_arquivodital_selTodos with .t.","T")
		ARQUIVO_DIGITAL.menu1.adicionaOpcao("talao", "Tal�o", myPath + "\imagens\icons\imprimir_w.png", "uf_arquivodigital_ImprimeTalaoPos", "I")
		ARQUIVO_DIGITAL.menu1.adicionaOpcao("prevTalao","Prev. Tal�o",myPath + "\imagens\icons\prever_imp_w.png","uf_arquivodigital_ValPrevTalao", "P")
		ARQUIVO_DIGITAL.menu1.adicionaOpcao("expTalao","PDF Tal�o",myPath + "\imagens\icons\exportar_w.png","uf_arquivodigital_EmitirPdfsTalao with 'ARQDIGITAL', ''", "E")
		ARQUIVO_DIGITAL.menu1.adicionaOpcao("prevA4","Prev. A4",myPath + "\imagens\icons\prever_imp_w.png","uf_arquivodigital_ValPrevA4", "V")
		ARQUIVO_DIGITAL.menu1.adicionaOpcao("expA4","PDF A4",myPath + "\imagens\icons\exportar_w.png","uf_arquivodigital_EmitirPdfA4", "X")
		
		ARQUIVO_DIGITAL.menu1.adicionaOpcao("listFiles","Lista Fx",myPath + "\imagens\icons\doc_linhas_w.png","uf_arquivodigital_listFiles", "L")
		ARQUIVO_DIGITAL.menu1.adicionaOpcao("verPDF","Ver PDF",myPath + "\imagens\icons\rec_linhas_w.png","uf_arquivodigital_verPDF", "D")
		ARQUIVO_DIGITAL.menu1.adicionaOpcao("expFaturacao","Exp. Fat. PDF",myPath + "\imagens\icons\exportar_w.png","uf_arquivodigital_expFaturacao", "F")
		
		ARQUIVO_DIGITAL.expFaturacao.menu1.adicionaOpcao("expTalao","PDF Tal�o",myPath + "\imagens\icons\exportar_w.png","uf_arquivodigital_expFaturacaoTalao", "O")
		ARQUIVO_DIGITAL.expFaturacao.menu1.adicionaOpcao("expA4","PDF A4",myPath + "\imagens\icons\exportar_w.png","uf_arquivodigital_expFaturacaoA4", "Q")
		
		ARQUIVO_DIGITAL.menu1.estado("listFiles,verPDF,expFaturacao","HIDE")
	ENDIF 
	regua(2)
ENDFUNC 

FUNCTION uf_arquivodigital_listFiles
	ARQUIVO_DIGITAL.pageframe1.page2.pgFrPDF.activepage = 1
	ARQUIVO_DIGITAL.pageframe1.page2.lblTitle.caption = 'Ficheiros Armazenados'
ENDFUNC 

FUNCTION uf_arquivodigital_verPDF

	LOCAL lcCaminhoArquivo 
	lcCaminhoArquivo = ALLTRIM(mypath)+ "\FicheirosArquivo"

	ARQUIVO_DIGITAL.pageframe1.page2.pgFrPDF.activepage = 2
	ARQUIVO_DIGITAL.pageframe1.page2.lblTitle.caption = 'Ficheiro: ' + + ALLTRIM(uCrsListFiles.filename)
	
	regua(0,2,"A carregar PDF...")
	regua(1,1,"A carregar PDF...")
	* Get PDF file name
	SELECT uCrsListFiles
	LOCAL lcFx
	lcFx = ALLTRIM(lcCaminhoArquivo) + "\" + ALLTRIM(uCrsListFiles.filename)

	* Display the name in the textbox
	IF !EMPTY(lcFx)
		* Display PDF
		ARQUIVO_DIGITAL.ShowPdf(lcFx)
	ENDIF
	regua(1,2,"A carregar PDF...")
	regua(2)
ENDFUNC 

FUNCTION uf_arquivodigital_EmitirPdfsTalao &&uf_EmitirPdfsArqDigLtTalao
	LPARAMETERS lcPainel, lcStamp
	
	LOCAL lcCaminhoArquivo 
	lcCaminhoArquivo = ALLTRIM(mypath)+ "\FicheirosArquivo"
	
	IF TYPE("lcPainel") != "C"
		lcPainel = ''
	ENDIF
	IF TYPE("lcStamp") != "C"
		lcStamp = ''
	ENDIF
		
	** IMPRESSORA EXISTE
	DIMENSION lcPrinters[1,1]
	APRINTERS(lcPrinters)
	LOCAL lcExistePrinter
	
	FOR lnIndex = 1 TO ALEN(lcPrinters,1)
		IF upper(alltrim(lcPrinters(lnIndex,1))) == "PDFCREATOR"
			lcExistePrinter = .t.
		ENDIF
	ENDFOR
	IF !(lcExistePrinter)
		uf_perguntalt_chama("PARA PODER EMITIR PDF'S, DEVE TER O PDFCREATOR INSTALADO","OK","",48)
		RETURN .f.
	ENDIF
	&& DIRECTORIO PREENCHIDO?
	LOCAL lcPath
	IF TYPE("ARQUIVO_DIGITAL") != "U"
		lcPath = ALLTRIM(ARQUIVO_DIGITAL.pageframe1.page1.txtDir.value)
	ELSE
		lcPath = lcCaminhoArquivo
	ENDIF
	
	IF (ALLTRIM(lcPath))==""
		uf_perguntalt_chama("PARA PODER EMITIR PDF'S, DEVE PRIMEIRO DEFINIR UM DIRECT�RIO.","OK","",64)
		RETURN .f.
	ENDIF
	
	** EMITIR PDF EM TAL�O **
	DO CASE 
		CASE lcPainel == 'ARQDIGITAL'
			If uf_perguntalt_chama("TEM A CERTEZA QUE QUER EXPORTAR OS PDF'S PARA "+ALLTRIM(lcPath)+"?","Sim","N�o")
				&&r�gua
				LOCAL lcCont, lcCont2
				lcCont = 0
				lcCont2 = 1
				select uc_ListagemDocsArqDigTalLt
				calculate count() for uc_ListagemDocsArqDigTalLt.escolha==.t. to lcCont
				regua(0,lcCont,"A Exportar Documentos...")
				
				lcOldPrinter = SET("printer",2)
				
				uf_arquivodigital_preparaPDFCreator()
				
				SELECT uc_ListagemDocsArqDigTalLt
				GO TOP
				SCAN FOR uc_ListagemDocsArqDigTalLt.escolha == .t.
					regua(1,lcCont2,"A Exportar Documentos...")
					
					IF !EMPTY(uc_listagemDocsArqDigTalLt.cabstamp)
						uf_gerais_actGrelha("","uCrsFt",[select td.u_tipodoc,td.tiposaft, B_utentes.pais, adc.* from B_arquivoTalaoCab adc (nolock) inner join td (nolock) on td.ndoc=adc.ndoc inner join B_utentes (nolock) on B_utentes.no =adc.no and B_utentes.estab = adc.estab where adc.ftstamp=']+alltrim(uc_listagemDocsArqDigTalLt.cabstamp)+['])
						Select uCrsFt
						uf_gerais_actGrelha("","uCrsFt2",[select * from B_arquivoTalaoCab (nolock) where ftstamp=']+alltrim(uc_listagemDocsArqDigTalLt.cabstamp)+['])
						Select uCrsFt2
						uf_gerais_actGrelha("","uCrsFi",[select * from B_arquivoTalaoLin atl (nolock) where epromo=0 and ftstamp=']+alltrim(uc_listagemDocsArqDigTalLt.cabstamp)+[' order by lordem])
						Select uCrsFi
						
						uf_imprimirpos_prevTalao(uc_listagemDocsArqDigTalLt.cabstamp,"ARQDIGITAL",.t.)
					ENDIF
					
					lcCont2 = lcCont2 + 1
					
					SELECT uc_listagemDocsArqDigTalLt
				ENDSCAN
				GO TOP
				
				Set Printer To  Name (lcoldPrinter) 	
				uf_perguntalt_chama("PROCESSO CONCLU�DO COM SUCESSO. POR FAVOR VERIFIQUE OS FICHEIROS NA DIRECTORIA DEFINIDA."+CHR(10)+CHR(10)+"NOTA: A GERA��O DOS FICHEIROS PODER� AINDA ESTAR EM CURSO.","OK","",64)
				regua(2)
			ENDIF
		CASE lcPainel == 'PAGAMENTO'
			IF USED("uCrsCabVendas")
				SELECT uCrsCabVendas
				LOCAL lcPosCab
				lcPosCab = RECNO("uCrsCabVendas")
				
				lcOldPrinter = SET("printer",2)
				uf_arquivodigital_preparaPDFCreator()
				uf_imprimirpos_prevTalao(uCrsCabVendas.stamp,"ARQDIGITAL",.t.)
				Set Printer To  Name (lcoldPrinter) 
				
				SELECT uCrsCabVendas
				TRY
					GO lcPosCab	
				CATCH
				ENDTRY
			ENDIF
		CASE lcPainel == 'FACTURACAO'
			IF USED("FT")
				SELECT FT
				lcOldPrinter = SET("printer",2)
				uf_arquivodigital_preparaPDFCreator()
				uf_imprimirpos_prevTalao(FT.ftstamp,"CALCCURSOR",.t.)
				Set Printer To Name (lcoldPrinter) 
			ENDIF
	ENDCASE
	*************************
ENDFUNC


**
FUNCTION uf_arquivodigital_preparaPDFCreator
	TRY
		PUBLIC oPDFC
		oPDFC  = Createobject("PDFCreator.clsPDFCreator","pdfcreator")
		oPDFC.cStart("/NoProcessingAtStartup")
		**oPDFC.cVisible = .f.
		oPDFC.cOption("UseAutosave") = 1
		oPDFC.cOption("UseAutosaveDirectory") = 1
		oPDFC.cOption("AutosaveFormat") = 0         
		oPDFC.cOption("DisableUpdateCheck") = 0         
		DefaultPrinter = oPDFC.cDefaultprinter
		oPDFC.cDefaultprinter = "PDFCreator"
		oPDFC.cClearCache
	CATCH
	ENDTRY
ENDFUNC


**
FUNCTION uf_arquivodigital_preparaPDFCreatorToSave
	LOCAL lcCaminhoArquivo 
	lcCaminhoArquivo = ALLTRIM(mypath)+ "\FicheirosArquivo"

	LOCAL lcNomeFicheirolt
	SELECT uCrsFt
	lcNomeFicheirolt = 	astr(YEAR(uCrsFt.fdata)) + ;
						IIF(LEN(astr(month(uCrsFt.fdata)))=1,'0'+astr(month(uCrsFt.fdata)),astr(month(uCrsFt.fdata))) + ;
						IIF(LEN(astr(day(uCrsFt.fdata)))=1,'0'+astr(day(uCrsFt.fdata)),astr(day(uCrsFt.fdata))) + ;
						strtran(astr(seconds()),'.','') + ;
						"-" + ALLTRIM(uCrsFt.u_nratend)
	IF TYPE("ARQUIVO_DIGITAL") != "U"
		lcNomeFicheirolt = ALLTRIM(lcNomeFicheirolt) + "-A"
	ELSE
		lcNomeFicheirolt = ALLTRIM(lcNomeFicheirolt) + "-F"
	ENDIF
	
	ReadyState = 0
	oPDFC.cOption("AutosaveFilename")  = LTRIM(lcNomeFicheiroLt)
	IF TYPE("ARQUIVO_DIGITAL") != "U"
		IF ARQUIVO_DIGITAL.expFaturacao.visible == .t.
			oPDFC.cOption("AutosaveDirectory") = ALLTRIM(ARQUIVO_DIGITAL.expFaturacao.txtDir.value)
		ELSE
			oPDFC.cOption("AutosaveDirectory") = ALLTRIM(ARQUIVO_DIGITAL.pageframe1.page1.txtDir.value)
		ENDIF
	ELSE
		oPDFC.cOption("AutosaveDirectory") = lcCaminhoArquivo
	ENDIF
	oPDFC.cprinterstop=.F.
ENDFUNC


*** Cria os Pdfs no Direct�rio definido por defeito no driver da Impressora de Pdf (PDFCreator)
FUNCTION uf_arquivodigital_EmitirPdfA4 &&uf_EmitirPdfsArqDigLt

	LOCAL i, lcNomeFicheiro 
	i=0
	lcNomeFicheiro = ""
	
	** IMPRESSORA EXISTE
	DIMENSION lcPrinters[1,1]
	APRINTERS(lcPrinters)

	LOCAL lcExistePrinter
	FOR lnIndex = 1 TO ALEN(lcPrinters,1)
		if upper(alltrim(lcPrinters(lnIndex,1))) == "PDFCREATOR"
			lcExistePrinter = .t.
		endif
	ENDFOR
	IF !(lcExistePrinter)
		uf_perguntalt_chama("PARA PODER EMITIR PDF'S, DEVE TER O PDFCREATOR INSTALADO","OK","",48)
		RETURN .f.
	ENDIF
	
	&& DIRECTORIO PREENCHIDO?
	IF(ALLTRIM(ARQUIVO_DIGITAL.pageframe1.page1.txtDir.value))==""
		uf_perguntalt_chama("PARA PODER EMITIR PDF'S, DEVE PRIMEIRO DEFINIR UM DIRECT�RIO.","OK","",64)
		RETURN .f.
	ENDIF
	
	If uf_perguntalt_chama("TEM A CERTEZA QUE QUER EXPORTAR OS PDF'S PARA "+ALLTRIM(ARQUIVO_DIGITAL.pageframe1.page1.txtDir.value)+"?","Sim","N�o")
		
		&&r�gua
		LOCAL lcCont, lcCont2
		lcCont = 0
		lcCont2 = 1
		select uc_ListagemDocsArqDigTalLt
		calculate count() for uc_ListagemDocsArqDigTalLt.escolha==.t. to lcCont
		regua(0,lcCont,"A Exportar Documentos...")
		
		*****************************
		* PDF CREATOR API - parte 1 *
		*****************************
		
		lcOldPrinter = SET("printer",2) &&Impressora por defeito
		
		uf_arquivodigital_preparaPDFCreator()
		
		&&Percorre o cursor e imprime
		SELECT uc_ListagemDocsArqDigTalLt
		GO TOP
		SCAN
			IF uc_ListagemDocsArqDigTalLt.Escolha
				regua(1,lcCont2,"A Exportar Documentos...")
				IF USED("FT")
					fecha("FT")
				ENDIF 
				IF USED("FT2")
					fecha("FT2")
				ENDIF 
				IF USED("FI")
					fecha("FI")
				ENDIF 
				
				uf_arquivodigital_imprimeFactpdf(uc_ListagemDocsArqDigTalLt.cabstamp, .f.)
				lcCont2 = lcCont2 + 1
			ENDIF

			SELECT uc_ListagemDocsArqDigTalLt
		ENDSCAN

		&& Repoe impressora por defeito
		Set Printer To  Name (lcoldPrinter) 	
		uf_perguntalt_chama("PROCESSO CONCLU�DO COM SUCESSO. POR FAVOR VERIFIQUE OS FICHEIROS NA DIRECTORIA DEFINIDA."+CHR(10)+CHR(10)+"NOTA: A GERA��O DOS FICHEIROS PODER� AINDA ESTAR EM CURSO.","OK","",64)
		
		regua(2)
	ENDIF && Fim da pergunta
ENDFUNC


**
FUNCTION uf_arquivodigital_ValPrevTalao
	IF USED("uc_listagemDocsArqDigTalLt")
		SELECT uc_listagemDocsArqDigTalLt
		IF !EMPTY(uc_listagemDocsArqDigTalLt.cabstamp)
			uf_gerais_actGrelha("","uCrsFt",[select td.u_tipodoc,td.tiposaft, B_utentes.pais, adc.* from B_arquivoTalaoCab adc (nolock) inner join td (nolock) on td.ndoc=adc.ndoc inner join B_utentes (nolock) on B_utentes.no =adc.no and B_utentes.estab = adc.estab where adc.ftstamp=']+alltrim(uc_listagemDocsArqDigTalLt.cabstamp)+['])
			Select uCrsFt
			uf_gerais_actGrelha("","uCrsFt2",[select * from B_arquivoTalaoCab (nolock) where ftstamp=']+alltrim(uc_listagemDocsArqDigTalLt.cabstamp)+['])
			Select uCrsFt2
			uf_gerais_actGrelha("","uCrsFi",[select * from B_arquivoTalaoLin atl (nolock) where epromo=0 and ftstamp=']+alltrim(uc_listagemDocsArqDigTalLt.cabstamp)+[' order by lordem])
			Select uCrsFi
			
			uf_imprimirpos_prevTalao(uc_listagemDocsArqDigTalLt.cabstamp,"ARQDIGITAL",.f.)
		ENDIF
	ENDIF
ENDFUNC


**
FUNCTION uf_arquivodigital_ValPrevA4
	IF USED("uc_listagemDocsArqDigTalLt")
		SELECT uc_listagemDocsArqDigTalLt
		IF !EMPTY(uc_listagemDocsArqDigTalLt.cabstamp)
			IF USED("FT")
				fecha("FT")
			ENDIF 
			IF USED("FT2")
				fecha("FT2")
			ENDIF 
			IF USED("FI")
				fecha("FI")
			ENDIF	
			uf_arquivodigital_imprimeFactpdf(uc_listagemDocsArqDigTalLt.cabstamp,.t.)
		ENDIF
	ENDIF
ENDFUNC


**
FUNCTION uf_arquivodigital_ImprimeTalaoPos
	
	LOCAL lcCont
	STORE 0 TO lcCont
	
	SELECT uc_ListagemDocsArqDigTalLt
	GO TOP
	SCAN
		IF uc_ListagemDocsArqDigTalLt.Escolha
			uf_arquivodigital_imprimeTalao(uc_ListagemDocsArqDigTalLt.cabstamp)
			lcCont = lcCont + 1
		ENDIF
		
		SELECT uc_ListagemDocsArqDigTalLt
	ENDSCAN
	
	IF lcCont > 0
		uf_perguntalt_chama("PROCESSO CONCLU�DO COM SUCESSO.","OK","",64)
	ELSE
		uf_perguntalt_chama("N�o seleccionou nada para imprimir.","OK","",64)
	ENDIF
ENDFUNC

Function uf_arquivodigital_imprimeTalao
	Lparameters tcStamp
	
	** CRIAR CURSORES COM DADOS DAS VENDAS **
	uf_gerais_actGrelha("","uCrsFt",[select td.u_tipodoc,td.tiposaft, B_utentes.pais, atc.* from B_arquivoTalaoCab atc (nolock) inner join td (nolock) on td.ndoc=atc.ndoc inner join B_utentes (nolock) on B_utentes.no =adc.no and B_utentes.estab = adc.estab where atc.ftstamp=']+tcStamp+['])
	SELECT uCrsFt
	uf_gerais_actGrelha("","uCrsFi",[select * from B_arquivoTalaoLin (nolock) where epromo=0 and ftstamp=']+tcStamp+[' order by lordem])
	SELECT uCrsFi
	uf_gerais_actGrelha("",[uCrsFt2],[exec up_touch_ft2 ']+tcStamp+['])
	SELECT uCrsFt2

	** APENAS IMPRIMIR TAL�ES DE FRONT OFFICE **
	SELECT uCrsFt
	IF uCrsFt.u_tipodoc!=8 And uCrsFt.u_tipodoc!=9 And uCrsFt.u_tipodoc!=10 And uCrsFt.u_tipodoc!=2
		Fecha("uCrsFt")
		Fecha("uCrsFi")
		fecha("uCrsFt2")
		
		uf_perguntalt_chama("N�o � poss�vel imprimir esse tipo de documento.","Ok","",64)
		
		RETURN .f.
	ENDIF
	*******************************************
	
	uf_imprimirPOS_ImpTalao(tcStamp,'ARQDIGITAL',.f., 0)
ENDFUNC

FUNCTION uf_arquivodital_selTodos
	LPARAMETERS lcOpcao
	
	Select uc_ListagemDocsArqDigTalLt
	SCAN
		Replace uc_ListagemDocsArqDigTalLt.escolha WITH lcOpcao
	Endscan
	Select uc_ListagemDocsArqDigTalLt
	Go top 
	
	IF lcOpcao
		ARQUIVO_DIGITAL.menu1.selTodos.config("Sel. Todos", myPath + "\imagens\icons\checked_w.png", "uf_arquivodital_selTodos with .F.", "T")
	ELSE
		ARQUIVO_DIGITAL.menu1.selTodos.config("Sel. Todos", myPath + "\imagens\icons\unchecked_w.png", "uf_arquivodital_selTodos with .T.", "T")
	ENDIF 
ENDFUNC

FUNCTION uf_arquivoDigital_TaloesLt &&uf_arquivoDigitalTaloesLt
	LPARAMETERS lnstamp
	
	LOCAL lcSQL, lcBool1, lcBool2, lcTran
	STORE .f. TO lcBool1, lcBool2
	STORE "T"+Sys(2015) TO lcTran

	**Inicio da Transac��o
	IF uf_gerais_actGrelha("","",[BEGIN TRANSACTION])
		LOCAL lcValida, lcValida2
		STORE .t. TO lcValida, lcValida2
		
		&&utilizar cursores
		IF USED("uCrsFt")
			SELECT uCrsFt
			LOCATE FOR ALLTRIM(uCrsFt.ftstamp) == ALLTRIM(lnstamp)
			IF !FOUND()
				lcValida = .f.
			ENDIF 
		ELSE
			lcValida = .f.
		ENDIF
		IF USED("uCrsFt2")
			SELECT uCrsFt2
			LOCATE FOR ALLTRIM(uCrsFt2.ft2stamp) == ALLTRIM(lnstamp)
			IF !FOUND()
				lcValida = .f.
			ENDIF 
		ELSE
		lcValida = .f.
		ENDIF
		
		IF !lcValida &&n�o encontrou cursores
			**Instru��es SQL de Insert Cab
			lcSQL = ""
			TEXT TO lcSQL TEXTMERGE NOSHOW
				Insert	B_ArquivoTalaoCab (
						ftstamp, nmdoc, fno, no, nome, nome2,
						Ft.morada, Ft.local, Ft.codpost, ncont,
						vendedor, vendnm, fdata, ftano,
						pdata, saida, ivatx1, ivatx2, ivatx3, ivatx4,
						ivatx5, ivatx6, ivatx7, ivatx8, ivatx9, final,
						ndoc, estab, eivain1, eivain2, eivain3, eivain4,
						eivain5, eivain6, eivain7, eivain8, eivain9,
						eivav1, eivav2, eivav3, eivav4, eivav5, eivav6,
						eivav7, eivav8, eivav9, ettiliq, edescc, ettiva,
						etotal, efinv, site, pnome, pno, Ft.ousrdata,
						Ft.ousrhora, Ft.ousrinis, u_lote2, u_ltstamp2,
						u_nslote2, u_nslote, u_slote2, u_lote, u_tlote,
						u_tlote2, u_slote, u_nratend, echtotal,
					--	--Ft2
						evdinheiro, epaga1, epaga2,
						u_codigo, u_codigo2, u_abrev, u_abrev2, u_design, u_design2,
						u_receita, u_nbenef, u_nbenef2,
						ft2_contacto,ft2_morada,ft2_codpost,ft2_local,ft2_telefone,ft2_email,ft2_horaentrega,ft2_u_viatura,ft2_u_vddom,
						u_docorig
				)
				Select	ftstamp, nmdoc, fno, no, nome, nome2,
						Ft.morada, Ft.local, Ft.codpost, ncont,
						vendedor, vendnm, fdata, ftano,
						pdata, saida, ivatx1, ivatx2, ivatx3, ivatx4,
						ivatx5, ivatx6, ivatx7, ivatx8, ivatx9, final,
						ndoc, estab, eivain1, eivain2, eivain3, eivain4,
						eivain5, eivain6, eivain7, eivain8, eivain9,
						eivav1, eivav2, eivav3, eivav4, eivav5, eivav6,
						eivav7, eivav8, eivav9, ettiliq, edescc, ettiva,
						etotal, efinv, site, pnome, pno, Ft.ousrdata,
						Ft.ousrhora, Ft.ousrinis, u_lote2, u_ltstamp2,
						u_nslote2, u_nslote, u_slote2, u_lote, u_tlote,
						u_tlote2, u_slote, u_nratend, echtotal,
				--		--Ft2
						evdinheiro, epaga1, epaga2,
						u_codigo, u_codigo2, u_abrev, u_abrev2, u_design, u_design2,
						u_receita, u_nbenef, u_nbenef2,
						ft2.contacto,ft2.morada,ft2.codpost,ft2.local,ft2.telefone,ft2.email,ft2.horaentrega,ft2.u_viatura,ft2.u_vddom,
						ft2.u_docOrig
				From Ft (nolock)
				Inner Join Ft2 (nolock) On Ft.Ftstamp = Ft2.Ft2stamp
				Where	Ft.Ftstamp = '<<ALLTRIM(lnstamp)>>'
			ENDTEXT	
		ELSE
			lcSQL = ""
			TEXT TO lcSQL TEXTMERGE NOSHOW
				Insert	B_ArquivoTalaoCab (
						ftstamp, nmdoc, fno, no, nome, nome2,
						Ft.morada, Ft.local, Ft.codpost, ncont,
						vendedor, vendnm, fdata, ftano, 
						pdata, saida, ivatx1, ivatx2, ivatx3, ivatx4,
						ivatx5, ivatx6, ivatx7, ivatx8, ivatx9,
						ndoc, estab, eivain1, eivain2, eivain3, eivain4,
						eivain5, eivain6, eivain7, eivain8, eivain9,
						eivav1, eivav2, eivav3, eivav4, eivav5, eivav6,
						eivav7, eivav8, eivav9, ettiliq, edescc, ettiva,
						etotal, efinv, site, pnome, pno, Ft.ousrdata,
						Ft.ousrhora, Ft.ousrinis, u_lote2, u_ltstamp2,
						u_nslote2, u_nslote, u_slote2, u_lote, u_tlote,
						u_tlote2, u_slote, u_nratend, echtotal,
					--	--Ft2
						evdinheiro, epaga1, epaga2,
						u_codigo, u_codigo2, u_abrev, u_abrev2, u_design, u_design2,
						u_receita, u_nbenef, u_nbenef2,
						ft2_contacto,ft2_morada,ft2_codpost,ft2_local,ft2_telefone,ft2_email,ft2_horaentrega,ft2_u_viatura,ft2_u_vddom,
						u_docorig
				)
				values(
						'<<uCrsFt.ftstamp>>', '<<uCrsFt.nmdoc>>', <<uCrsFt.fno>>, <<uCrsFt.no>>, '<<uCrsFt.nome>>', '<<uCrsFt.nome2>>',
						'<<uCrsFt.morada>>', '<<uCrsFt.local>>', '<<uCrsFt.codpost>>', '<<uCrsFt.ncont>>',
						<<uCrsFt.vendedor>>, '<<uCrsFt.vendnm>>', '<<uf_gerais_getDate(uCrsFt.fdata,"SQL")>>', <<uCrsFt.ftano>>,
						'<<uf_gerais_getDate(uCrsFt.pdata,"SQL")>>', '<<uCrsFt.saida>>', <<uCrsFt.ivatx1>>, <<uCrsFt.ivatx2>>, <<uCrsFt.ivatx3>>, <<uCrsFt.ivatx4>>,
						<<uCrsFt.ivatx5>>, <<uCrsFt.ivatx6>>, <<uCrsFt.ivatx7>>, <<uCrsFt.ivatx8>>, <<uCrsFt.ivatx9>>,
						<<uCrsFt.ndoc>>, <<uCrsFt.estab>>, <<uCrsFt.eivain1>>, <<uCrsFt.eivain2>>, <<uCrsFt.eivain3>>, <<uCrsFt.eivain4>>,
						<<uCrsFt.eivain5>>, <<uCrsFt.eivain6>>, <<uCrsFt.eivain7>>, <<uCrsFt.eivain8>>, <<uCrsFt.eivain9>>,
						<<uCrsFt.eivav1>>, <<uCrsFt.eivav2>>, <<uCrsFt.eivav3>>, <<uCrsFt.eivav4>>, <<uCrsFt.eivav5>>, <<uCrsFt.eivav6>>,
						<<uCrsFt.eivav7>>, <<uCrsFt.eivav8>>, <<uCrsFt.eivav9>>, <<uCrsFt.ettiliq>>, <<uCrsFt.edescc>>, <<uCrsFt.ettiva>>,
						<<uCrsFt.etotal>>, <<uCrsFt.efinv>>, '<<ALLTRIM(mySite)>>', '<<uCrsFt.pnome>>', <<uCrsFt.pno>>, '<<uf_gerais_getDate(uCrsFt.ousrdata,"SQL")>>',
						'<<uCrsFt.ousrhora>>', '<<uCrsFt.ousrinis>>', <<uCrsFt.u_lote2>>, '<<uCrsFt.u_ltstamp2>>',
						<<uCrsFt.u_nslote2>>, <<uCrsFt.u_nslote>>, <<uCrsFt.u_slote2>>, <<uCrsFt.u_lote>>, '<<uCrsFt.u_tlote>>',
						'<<uCrsFt.u_tlote2>>', <<uCrsFt.u_slote>>, '<<uCrsFt.u_nratend>>', <<uCrsFt.echtotal>>,
				--		--Ft2
						<<uCrsFt2.evdinheiro>>, <<uCrsFt2.epaga1>>, <<uCrsFt2.epaga2>>,
						'<<uCrsFt2.u_codigo>>', '<<uCrsFt2.u_codigo2>>', '<<uCrsFt2.u_abrev>>', '<<uCrsFt2.u_abrev2>>', '<<uCrsFt2.u_design>>', '<<uCrsFt2.u_design2>>',
						'<<uCrsFt2.u_receita>>', '<<uCrsFt2.u_nbenef>>', '<<uCrsFt2.u_nbenef2>>',
						'<<uCrsFt2.contacto>>','<<uCrsFt2.morada>>','<<uCrsFt2.codpost>>','<<uCrsFt2.local>>','<<uCrsFt2.telefone>>','<<uCrsFt2.email>>','<<uCrsFt2.horaentrega>>','<<uCrsFt2.u_viatura>>','<<IIF(uCrsFt2.u_vddom,1,0)>>',
						'<<uCrsFt2.u_docOrig>>'
				)
			ENDTEXT
		ENDIF

		IF uf_gerais_actGrelha("","",lcSQL)
			STORE .t. TO lcBool1
		ENDIF
		
		IF USED("uCrsFi")
			SELECT uCrsFi
			LOCATE FOR ALLTRIM(uCrsFi.ftstamp) == ALLTRIM(lnstamp)
			IF !FOUND()
				lcValida2 = .f.
			ENDIF 
		ELSE
			lcValida2 = .f.
		ENDIF
		
		IF !lcValida2 &&n�o encontrou cursor
			**Instru��es SQL de Insert Lin
			lcSQL = ""
			TEXT TO lcSQL TEXTMERGE NOSHOW
				Insert	B_ArquivoTalaoLin (
						ftstamp, fistamp, nmdoc, fno, ref,
						design, qtt, etiliquido, unidade, unidad2,
						iva, ivaincl, codigo, tabiva, ndoc, armazem,
						litem2, litem, lordem, altura, largura, oref,
						epv, desconto, desc2, desc3, desc4, desc5, desc6,
						u_epref, u_stock, u_ip, u_epvp, u_genalt, u_generico,
						u_ettent1, u_ettent2, u_psicont, u_bencont, u_psico,
						u_benzo, u_txcomp, u_cnp, u_comp, u_nbenef, u_nbenef2,
						u_diploma, u_refvale, u_robot, epromo, u_descval,
						lobs2
				)
				Select	ftstamp, fistamp, nmdoc, fno, ref,
						design, qtt, etiliquido, unidade, unidad2,
						iva, ivaincl, codigo, tabiva, ndoc, armazem,
						litem2, litem, lordem, altura, largura, oref,
						epv, desconto, desc2, desc3, desc4, desc5, desc6,
						u_epref, u_stock, u_ip, u_epvp, u_genalt, u_generico,
						u_ettent1, u_ettent2, u_psicont, u_bencont, u_psico,
						u_benzo, u_txcomp, u_cnp, u_comp, u_nbenef, u_nbenef2,
						u_diploma, u_refvale, u_robot, epromo, u_descval,
						lobs2
				From 	Fi (nolock)
				Where	Fi.Ftstamp = '<<ALLTRIM(lnstamp)>>'
			ENDTEXT	
		ELSE
			lcSQL = ""
			
			SELECT uCrsFi
			SCAN FOR ALLTRIM(uCrsFi.ftstamp) == ALLTRIM(lnstamp)
				TEXT TO lcSQL TEXTMERGE NOSHOW
					
					<<lcSQL>>
					
					Insert	B_ArquivoTalaoLin (
							ftstamp, fistamp, nmdoc, fno, ref,
							design, qtt, etiliquido, unidade, unidad2,
							iva, ivaincl, codigo, tabiva, ndoc, armazem,
							litem2, litem, lordem, altura, largura, oref,
							epv, desconto, desc2, desc3, desc4, desc5, desc6,
							u_epref, u_stock, u_ip, u_epvp, u_genalt, u_generico,
							u_ettent1, u_ettent2, u_psicont, u_bencont, u_psico,
							u_benzo, u_txcomp, u_cnp, u_comp, u_nbenef, u_nbenef2,
							u_diploma, u_refvale, u_robot, epromo, u_descval,
							lobs2
					)
					values(
							'<<ALLTRIM(lnstamp)>>', '<<ALLTRIM(uCrsFi.fistamp)>>', '<<ALLTRIM(uCrsFi.nmdoc)>>', <<uCrsFi.fno>>, '<<uCrsFi.ref>>',
							'<<ALLTRIM(uCrsFi.design)>>', <<uCrsFi.qtt>>, <<uCrsFi.etiliquido>>, '<<uCrsFi.unidade>>', '<<uCrsFi.unidad2>>',
							<<uCrsFi.iva>>, <<IIF(uCrsFi.ivaincl,1,0)>>, '<<uCrsFi.codigo>>', <<uCrsFi.tabiva>>, <<uCrsFi.ndoc>>, <<uCrsFi.armazem>>,
							'<<uCrsFi.litem2>>', '<<uCrsFi.litem>>', <<uCrsFi.lordem>>, <<uCrsFi.altura>>, <<uCrsFi.largura>>, '<<uCrsFi.oref>>',
							<<uCrsFi.epv>>, <<uCrsFi.desconto>>, <<uCrsFi.desc2>>, <<uCrsFi.desc3>>, <<uCrsFi.desc4>>, <<uCrsFi.desc5>>, <<uCrsFi.desc6>>,
							<<uCrsFi.u_epref>>, <<uCrsFi.u_stock>>, <<IIF(uCrsFi.u_ip,1,0)>>, <<uCrsFi.u_epvp>>, <<IIF(uCrsFi.u_genalt,1,0)>>, <<IIF(uCrsFi.u_generico,1,0)>>,
							<<uCrsFi.u_ettent1>>, <<uCrsFi.u_ettent2>>, <<uCrsFi.u_psicont>>, <<uCrsFi.u_bencont>>, <<IIF(uCrsFi.u_psico,1,0)>>,
							<<IIF(uCrsFi.u_benzo,1,0)>>, <<uCrsFi.u_txcomp>>, '<<uCrsFi.u_cnp>>', <<IIF(uCrsFi.u_comp,1,0)>>, '<<uCrsFi.u_nbenef>>', '<<uCrsFi.u_nbenef2>>',
							'<<uCrsFi.u_diploma>>', '<<uCrsFi.u_refvale>>', '<<uCrsFi.u_robot>>', <<IIF(uCrsFi.epromo,1,0)>>, <<uCrsFi.u_descval>>,
							'<<ALLTRIM(uCrsFi.lobs2)>>'
					)
				ENDTEXT	
			ENDSCAN
		ENDIF

*!*	_CLIPTEXT = lcSQL
*!*	MESSAGEBOX(lcSQL)

		IF uf_gerais_actGrelha("","",lcSQL)
			STORE .t. TO lcBool2
		ENDIF	

		** Controla coerencia dos inserts
		IF lcBool1 AND lcBool2
			uf_gerais_actGrelha("","",[COMMIT TRANSACTION])
		Else
			uf_gerais_actGrelha("","",[ROLLBACK TRANSACTION])
						
			** Regista Problema na Tabela de Logs
			Local lcmensagem, lcorigem
			lcmensagem = "Erro na Cria��o de Arquivos Digitais Taloes; Ftstamp: " + Alltrim(lnstamp)  + "; User:" + Alltrim(Str(ch_userno))
			lcorigem = "Painel Factura��o: uf_gravarFactura ; Painel de Atentendimento"
			lcSQL = ""
			TEXT TO lcSQL TEXTMERGE NOSHOW
				Insert Into B_elog (tipo, status, mensagem, origem, usr, terminal, versao)
				Values 	('E','A','<<Alltrim(lcmensagem)>>', '<<Alltrim(lcorigem)>>', '<<Alltrim(Str(ch_userno))>>', '<<myTermNo>>', '<<uf_gerais_getParameter("ADM0000000156","TEXT")>>')
			ENDTEXT
			uf_gerais_actGrelha("","",lcSQL)
						
			RETURN .f.
		ENDIF
	ENDIF
ENDFUNC

FUNCTION uf_arquivodigital_pesquisa
	regua(0,1,"A processar as Listagem de Documentos...")
	
	IF arquivo_digital.pgFrPesq.activepage == 1	
		**Trata parametros
		If Empty(ARQUIVO_DIGITAL.pgFrPesq.page1.ultimos.Value)
			ARQUIVO_DIGITAL.pgFrPesq.page1.ultimos.Value = 30
		ENDIF
			
		If !empty(ARQUIVO_DIGITAL.pgFrPesq.page1.DataIni.value)
			lcDataIni = uf_gerais_getdate(ARQUIVO_DIGITAL.pgFrPesq.page1.dataIni.value,"SQL")
		Else
			lcDataIni ='19000101'
		EndIf
		If !empty(ARQUIVO_DIGITAL.pgFrPesq.page1.DataFim.value)
			lcDataFim = uf_gerais_getdate(ARQUIVO_DIGITAL.pgFrPesq.page1.dataFim.value,"SQL")
		Else
			If !empty(ARQUIVO_DIGITAL.pgFrPesq.page1.DataIni.value)
				lcDataFim = lcDataIni
			Else
				lcDataFim='30001231'
			EndIf
		ENDIF
		
		lcSQL = ""
		TEXT TO lcSql NOSHOW TEXTMERGE 
			exec up_facturacao_pesquisarDocumentosAdd <<ARQUIVO_DIGITAL.pgFrPesq.page1.ultimos.Value>>, '<<Alltrim(ARQUIVO_DIGITAL.pgFrPesq.page1.NOME.VALUE)>>', <<IIF(EMPTY(Alltrim(ARQUIVO_DIGITAL.pgFrPesq.page1.NUMDOC.VALUE)),0,Alltrim(ARQUIVO_DIGITAL.pgFrPesq.page1.NUMDOC.VALUE))>>,
					<<IIF(EMPTY(Alltrim(ARQUIVO_DIGITAL.pgFrPesq.page1.NO.VALUE)),0,Alltrim(ARQUIVO_DIGITAL.pgFrPesq.page1.NO.VALUE))>>, <<IIF(EMPTY(Alltrim(ARQUIVO_DIGITAL.pgFrPesq.page1.ESTAB.VALUE)),-1,Alltrim(ARQUIVO_DIGITAL.pgFrPesq.page1.ESTAB.VALUE))>>,  '<<lcDataIni>>',  '<<lcDataFim>>',
					'<<Alltrim(ARQUIVO_DIGITAL.pgFrPesq.page1.documento.Value)>>', <<ch_userno>>,  '<<Alltrim(ch_grupo)>>', '<<Alltrim(ARQUIVO_DIGITAL.pgFrPesq.page1.produto.Value)>>', '<<Alltrim(ARQUIVO_DIGITAL.pgFrPesq.page1.loja.Value)>>'
		ENDTEXT
		uf_gerais_actGrelha("ARQUIVO_DIGITAL.pageframe1.page1.grdPesq","uc_ListagemDocsArqDigTalLt",lcSQL)
	ELSE
		If !empty(ARQUIVO_DIGITAL.pgFrPesq.page2.DataIni.value)
			lcDataIni = uf_gerais_getdate(ARQUIVO_DIGITAL.pgFrPesq.page2.dataIni.value,"SQL")
		Else
			lcDataIni ='19000101'
		EndIf
		If !empty(ARQUIVO_DIGITAL.pgFrPesq.page2.DataFim.value)
			lcDataFim = uf_gerais_getdate(ARQUIVO_DIGITAL.pgFrPesq.page2.dataFim.value,"SQL")
		Else
			If !empty(ARQUIVO_DIGITAL.pgFrPesq.page2.DataIni.value)
				lcDataFim = lcDataIni
			Else
				lcDataFim='30001231'
			EndIf
		ENDIF
		
		SELECT uCrsListFiles
		set filter to Like ('*'+alltrim(ARQUIVO_DIGITAL.pgFrPesq.page2.nome.value)+'*',uCrsListFiles.filename) and uCrsListFiles.created >= ctod(uf_gerais_getdate(arquivo_digital.pgFrPesq.page2.dataini.value)) and uCrsListFiles.created <= ctod(uf_gerais_getdate(arquivo_digital.pgFrPesq.page2.datafim.value))
		go top
	ENDIF
	regua(2)
	ARQUIVO_DIGITAL.Refresh
ENDFUNC

Function uf_arquivo_digital_sair	
	IF USED("uc_ListagemDocsArqDigTalLt")
		Fecha("uc_ListagemDocsArqDigTalLt")
	ENDIF
	IF USED("uCrsFt")
		Fecha("uCrsFt")
	ENDIF
	IF USED("uCrsFt2")
		Fecha("uCrsFt2")
	ENDIF
	IF USED("uCrsFi")
		Fecha("uCrsFi")
	ENDIF
	IF USED("FT")
		Fecha("FT")
	ENDIF
	IF USED("FT2")
		Fecha("FT2")
	ENDIF
	IF USED("FI")
		Fecha("FI")
	ENDIF
	IF USED("uCrsListFiles")
		fecha("uCrsListFiles")
	ENDIF
	
	ARQUIVO_DIGITAL.RELEASE
	RELEASE ARQUIVO_DIGITAL
ENDFUNC


**
FUNCTION uf_arquivodigital_imprimeFactpdf 
	LPARAMETERS lnstamp, lcBool, lnEnvioFtANF

	PUBLIC myIsentoIva
	STORE .f. TO myIsentoIva

	LOCAL lcimpressao, lcDefaultPrinter, lcnomeFicheiro, LcSQL

	SELECT uCrsE1
	
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_gerais_iduCert  '<<Alltrim(lnstamp)>>'
 	ENDTEXT
 	IF !uf_gerais_actGrelha("","ucrsHash",lcSQL)
		uf_perguntalt_chama("OCORREU UM ERRO A CALCULAR A INFORMA��O PARA A IMPRESS�O.COD.1","OK","",16)
 		RETURN .f.
 	ENDIF

	IF !RECCOUNT("ucrsHash")>0
		SELECT ucrsHash
		APPEND Blank
		replace ucrsHash.temcert	WITH .f.
		replace uCrsHash.parte1		WITH ''
		replace uCrsHash.parte1		WITH ''
	ENDIF
	
	*** Obtem Dados da Tabela de Registo Digital
	IF !USED("FT") OR !USED("FT2") OR !USED("FI")
		lcSQL = ""
		TEXT TO lcSQL TEXTMERGE NOSHOW
			Select 	*
			From 	B_ArquivoTalaoCab (nolock)
			Where 	ftstamp='<<Alltrim(lnstamp)>>'
		ENDTEXT
		IF !uf_gerais_actGrelha("","FT",lcSQL)
			uf_perguntalt_chama("OCORREU UM ERRO A CALCULAR A INFORMA��O PARA A IMPRESS�O. COD.2","OK","",16)
			RETURN .f.
		ENDIF 
		
		lcSQL = ""
		TEXT TO lcSQL TEXTMERGE NOSHOW
			Select  u_docorig, obsdoc=''
			From 	B_ArquivoTalaoCab (nolock)
			Where 	ftstamp='<<Alltrim(lnstamp)>>'
		ENDTEXT
		
		If !uf_gerais_actGrelha("","FT2",lcSQL)
			uf_perguntalt_chama("OCORREU UM ERRO A CALCULAR A INFORMA��O PARA A IMPRESS�O. COD.2","OK","",16)
			RETURN .f.
		Endif
		
		lcSQL = ""
		TEXT TO lcSQL TEXTMERGE NOSHOW
			Select 	*
			From 	B_ArquivoTalaoLin (nolock) 
			Where 	ftstamp='<<Alltrim(lnstamp)>>' and epromo=0
			order by lordem
		ENDTEXT
		If !uf_gerais_actGrelha("","FI",lcSQL)
			uf_perguntalt_chama("OCORREU UM ERRO A CALCULAR A INFORMA��O PARA A IMPRESS�O. COD.3","OK","",16)
			RETURN .f.
		ENDIF
	ENDIF
			
	Select FT
	GO TOP
	SELECT FT2
	GO TOP
	Select Fi
	
	**** OBTEM O TIPO DE DOCUMENTO DE FACTURACAO ********
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
			Select *
			from td (nolock)
			where nmdoc='<<Alltrim(FT.nmdoc)>>'
	ENDTEXT
	If !uf_gerais_actGrelha("","TD",lcSQL)
		uf_perguntalt_chama("OCORREU UM ERRO A CALCULAR INFORMA��O PARA A IMPRESS�O. COD.4","OK","",16)
		RETURN .f.
	Endif
	**
	
	** guardar data e n� de atendimento para o nome do ficheiro pdf **
	SELECT Ft
	GO TOP
	lcNomeFicheirolt = astr(YEAR(Ft.fdata)) + IIF(LEN(astr(month(Ft.fdata)))=1,'0'+astr(month(Ft.fdata)),astr(month(Ft.fdata))) + IIF(LEN(astr(day(Ft.fdata)))=1,'0'+astr(day(Ft.fdata)),astr(day(Ft.fdata))) + strtran(astr(seconds()),'.','') + "-" + ALLTRIM(Ft.u_nratend) + "-A"
	**
	
	** Veriricar se existem produtos isentos de iva **
	SELECT fi
	GO top
	SCAN FOR fi.iva==0
		myIsentoIva = .t.
	ENDSCAN
	SELECT fi 
	GO TOP
	**

	** Guardar report a imprimir - ficheiro **
	** Obtem dados a partir da tabela de impressoes **
	lcSQL = ""
	Select FT
	GO TOP
	TEXT TO lcSQL TEXTMERGE NOSHOW
		Select nomeficheiro as nomefich From B_impressoes (nolock) Where documento = '<<Alltrim(FT.nmdoc)>>'
	ENDTEXT
	If !uf_gerais_actGrelha("","uc_impressoesArqTalaoDig",lcSQL)
		uf_perguntalt_chama("OCORREU UM ERRO A CALCULAR INFORMA��O PARA A IMPRESS�O. Cod.5","OK","",16)
		RETURN .f.
	ELSE
		IF !RECCOUNT("uc_impressoesArqTalaoDig")>0
			RETURN .f.
		ENDIF
	ENDIF
	
	IF EMPTY(ALLTRIM(uc_impressoesArqTalaoDig.nomefich))
		RETURN .f.
	ELSE
		lcnomeFicheiro = ALLTRIM(uc_impressoesArqTalaoDig.nomefich)
		fecha("uc_impressoesArqTalaoDig")
	ENDIF
	
	IF FILE(ALLTRIM(myPath)+"\analises\" + ALLTRIM(lcnomeFicheiro))
		lcreport = Alltrim(uCrsPath.textvalue)+"\analises\" + Alltrim(lcnomeFicheiro)
	ELSE
		uf_perguntalt_chama("N�o foi encontrado IDU de pr� visualiza��o. Por favor contacte o suporte.","OK","",16)
		RETURN .f.
	ENDIF
	**	

	
	***** Se for uma factura a entidades ***		
	If (td.u_tipodoc)=1
		Select Fi
		
		LOCAL lnTot,pvp1,pvp2,pvp3,pvp4,pvp5,pvp6,pvp7,pvp8,pvp9, compUtente1,compUtente2,compUtente3,compUtente4, compUtente5,compUtente6,compUtente7,compUtente8,compUtente9
		Local compOrg1,compOrg2,compOrg3,CompOrg4,compOrg5,compOrg6,compOrg7,CompOrg8, CompOrg9, mes
		
		CALCULATE SUM(Fi.etiliquido) TO lnTot
		inTot = uf_gerais_extenso(lnTot)
		
		******* Calcula Totais ************
		Store 0 To pvp1,pvp2,pvp3,pvp4,pvp5,pvp6,pvp7,pvp8,pvp9,compUtente1,compUtente1,compUtente2,compUtente3,compUtente4, compUtente5,compUtente6,compUtente7,compUtente8,compUtente9
		Store 0 To compOrg1,compOrg2,compOrg3,CompOrg4,compOrg5,compOrg6,compOrg7,CompOrg8, CompOrg9
 			
		Select Fi
		Go top
		Scan  
			Do case
				CASE fi.tabiva==1
					pvp1 = pvp1 + Fi.altura
					compUtente1 = compUtente1 + Fi.largura
					compOrg1 = compOrg1 +  Fi.ETILIQUIDO
				Case Fi.tabiva == 2 
					pvp2 = pvp2 + Fi.altura
					compUtente2 = compUtente2 + Fi.largura
					compOrg2 = compOrg3 +  Fi.ETILIQUIDO
				Case Fi.tabiva == 3
					pvp3 = pvp3 + Fi.altura
					compUtente3 = compUtente3 + Fi.largura
					compOrg3 = compOrg3 +  Fi.ETILIQUIDO
				Case Fi.tabiva == 4
					pvp4 = pvp4 + Fi.altura
					compUtente4 = compUtente4 + Fi.largura
					compOrg4 = compOrg4 +  Fi.ETILIQUIDO
				Case fi.tabiva == 5
					pvp5 = pvp5 + fi.altura
					compUtente5 = compUtente5 + fi.largura
					compOrg5 = compOrg5 +  FI.ETILIQUIDO
				Case fi.tabiva == 6 
					pvp6 = pvp6 + fi.altura
					compUtente6 = compUtente6 + fi.largura
					compOrg6 = compOrg6 +  FI.ETILIQUIDO
				Case fi.tabiva == 7
					pvp7 = pvp7 + fi.altura
					compUtente7 = compUtente7 + fi.largura
					compOrg7 = compOrg7 +  FI.ETILIQUIDO
				Case fi.tabiva == 8
					pvp8 = pvp8 + fi.altura
					compUtente8 = compUtente8 + fi.largura
					compOrg8 = compOrg8 +  FI.ETILIQUIDO
				Case fi.tabiva == 9
					pvp9 = pvp9 + fi.altura
					compUtente9 = compUtente9 + fi.largura
					compOrg9 = compOrg9 +  FI.ETILIQUIDO
			Endcase
			Select Fi
		Endscan 
		*************************
		**** Calcula o Mes **********
		myMesDocumento = ""
		Select FT
		GO top
		myMesDocumento = uf_Gerais_CalculaMes(Month(FT.fdata))
		****************************
		Select Fi
		
		lcSQL=""
		TEXT TO lcSQL TEXTMERGE NOSHOW
		 	SELECT	design,Sum(largura) as largura,SUm(altura) as altura,
		 			SUM(CASE WHEN ISNUMERIC(litem2) = 1 THEN convert(numeric,litem2) ELSE 0 END) as litem2,
					SUM(CASE WHEN ISNUMERIC(litem) = 1 THEN convert(numeric,litem) ELSE 0 END) as litem,
		 			Sum(etiliquido) as etiliquido
			FROM	<<IIF(ARQUIVO_DIGITAL.expFaturacao.visible==.t.,"FI","B_ArquivoTalaoLin")>> (nolock)
			WHERE ftstamp = '<<Alltrim(lnstamp)>>'
			GROUP BY DESIGN
		ENDTEXT
		
		IF !uf_gerais_actGrelha("","tempFi",lcSQL)
			uf_perguntalt_chama("OCORREU UM ERRO A CALCULAR INFORMACAO PARA A IMPRESSAO. Cod.6","OK","",16)
 			RETURN .f.
 		ENDIF

		Select FT
		GO TOP
		SELECT FT2
		GO TOP
		Select Fi
		GO TOP
	ENDIF

	Select ft 
	&& condi��o invalida por causa da multiloja
	IF (ft.ndoc == 72 OR ft.ndoc == 77) AND ft.no < 199
		uf_imprimirgerais_dataMatrixCriaImagem(ft.ndoc)
	ENDIF 
		
	Select Fi
	IF (TD.u_tipodoc)==1
		Select tempFi
	ENDIF

	*****************************
	* PDF CREATOR API - parte 2 *
	*****************************
	uf_gerais_actGrelha("","ucrsTextoRodape",[exec up_cert_TextoRodape '] + ALLTRIM(td.tiposaft) + ['])
	
	IF lcBool
		Select Fi
		GO TOP
		SELECT FI
		IF (TD.u_tipodoc)==1
			Select tempFi
			GO TOP
			Select tempFi
		ENDIF
		
		SET REPORTBEHAVIOR 90
		REPORT FORM Alltrim(lcreport) TO PRINTER PREVIEW
	ELSE 			
		*Imprimir para PDF
		ReadyState = 0
		oPDFC.cOption("AutosaveFilename")  = LTRIM(lcNomeFicheiroLt)
		IF ARQUIVO_DIGITAL.expFaturacao.visible == .t.
			oPDFC.cOption("AutosaveDirectory") = ALLTRIM(ARQUIVO_DIGITAL.expFaturacao.txtDir.value)
		ELSE
			oPDFC.cOption("AutosaveDirectory") = ALLTRIM(ARQUIVO_DIGITAL.pageframe1.page1.txtDir.value)
		ENDIF
		oPDFC.cprinterstop=.F.
		
		Select Fi
		GO TOP
		SELECT FI
		IF (TD.u_tipodoc)==1
			Select tempFi
			GO TOP
			Select tempFi
		ENDIF
		
		REPORT FORM Alltrim(lcreport) TO PRINTER NOCONSOLE

		*Fecha tramento de impress�o
		INKEY(1)
		*oPDFC.cDefaultprinter = DefaultPrinter
		*oPDFC.cClearCache
		*Release oPDFC
		**********
	ENDIF
	
	IF USED("uCrsHash")
		fecha("uCrsHash")
	ENDIF
	
	IF USED("FT")
		fecha("FT")
	ENDIF
	IF USED("FT2")
		fecha("FT2")
	ENDIF
	IF USED("FI")
		fecha("FI")
	ENDIF
	
	SELECT uc_ListagemDocsArqDigTalLt
ENDFUNC


**
FUNCTION uf_arquivodigital_ExpFactAutEntidadesA4
	LPARAMETERS lnstamp
	
	LOCAL lcCaminhoArquivo 
	lcCaminhoArquivo = ALLTRIM(mypath)+ "\FicheirosArquivo"
	
	PUBLIC myIsentoIva
	STORE .f. TO myIsentoIva

	Local lcimpressao, lcDefaultPrinter, lcnomeFicheiro, LcSQL

	SELECT uCrsE1
	
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_gerais_iduCert  '<<Alltrim(lnstamp)>>'
 	ENDTEXT
 	IF !uf_gerais_actGrelha("","ucrsHash",lcSQL)
		uf_perguntalt_chama("OCORREU UM ERRO A CALCULAR A INFORMA��O PARA A GRAVA��O EM PDF.COD.1","OK","",16)
 		RETURN .f.
 	ENDIF

	IF !RECCOUNT("ucrsHash")>0
		SELECT ucrsHash
		APPEND Blank
		replace ucrsHash.temcert	WITH .f.
		replace uCrsHash.parte1		WITH ''
		replace uCrsHash.parte2		WITH ''
	ENDIF
	
	*** Obtem Dados da Tabela de Fatura��o
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		Select 	*
		From 	FT (nolock)
		Where 	ftstamp='<<Alltrim(lnstamp)>>'
	ENDTEXT
	If !uf_gerais_actGrelha("","FT",lcSQL)
		uf_perguntalt_chama("OCORREU UM ERRO A CALCULAR A INFORMA��O PARA A GRAVA��O EM PDF. COD.2","OK","",16)
		RETURN .f.
	Endif
	
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		Select  *
		From 	FT2 (nolock)
		Where 	ft2stamp='<<Alltrim(lnstamp)>>'
	ENDTEXT
	
	If !uf_gerais_actGrelha("","FT2",lcSQL)
		uf_perguntalt_chama("OCORREU UM ERRO A CALCULAR A INFORMA��O PARA A GRAVA��O EM PDF. COD.2","OK","",16)
		RETURN .f.
	Endif
	
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		Select 	*
		From 	FI (nolock) 
		Where 	ftstamp='<<Alltrim(lnstamp)>>'
		order by lordem
	ENDTEXT
	If !uf_gerais_actGrelha("","FI",lcSQL)
		uf_perguntalt_chama("OCORREU UM ERRO A CALCULAR A INFORMA��O PARA A GRAVA��O EM PDF. COD.3","OK","",16)
		RETURN .f.
	Endif
			
	Select FT
	GO TOP
	SELECT FT2
	GO TOP
	Select Fi
	
	**** OBTEM O TIPO DE DOCUMENTO DE FACTURACAO ********
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
			Select *
			from td (nolock)
			where nmdoc='<<Alltrim(FT.nmdoc)>>'
	ENDTEXT
	If !uf_gerais_actGrelha("","TD",lcSQL)
		uf_perguntalt_chama("OCORREU UM ERRO A CALCULAR INFORMA��O PARA A GRAVA��O EM PDF. COD.4","OK","",16)
		RETURN .f.
	Endif
	******************************************************
	
	** guardar data e n� de atendimento para o nome do ficheiro pdf **
	SELECT FT
	GO TOP
	lcNomeFicheirolt = astr(YEAR(Ft.fdata)) + IIF(LEN(astr(month(Ft.fdata)))=1,'0'+astr(month(Ft.fdata)),astr(month(Ft.fdata))) + astr(day(Ft.fdata)) + strtran(astr(seconds()),'.','') + "-" + ALLTRIM(Ft.u_nratend) + "-F"
	*******************************************************************	
	
	** Veriricar se existem produtos isentos de iva **
	SELECT fi
	GO top
	SCAN FOR fi.iva==0
		myIsentoIva = .t.
	ENDSCAN
	SELECT fi 
	GO TOP
	**************************************************
	
	** Guardar report a imprimir - ficheiro **
	** Obtem dados a partir da tabela de impressoes **
	lcSQL = ""
	Select FT
	GO TOP
	TEXT TO lcSQL TEXTMERGE NOSHOW
		Select nomeficheiro as nomefich From B_impressoes (nolock) Where documento = '<<Alltrim(FT.nmdoc)>>'
	ENDTEXT
	If !uf_gerais_actGrelha("","uc_impressoesArqTalaoDig",lcSQL)
		uf_perguntalt_chama("OCORREU UM ERRO A CALCULAR INFORMA��O PARA A GRAVA��O EM PDF. Cod.5","OK","",16)
		RETURN .f.
	ELSE
		IF !RECCOUNT("uc_impressoesArqTalaoDig")>0
			RETURN .f.
		ENDIF
	ENDIF
	
	IF EMPTY(ALLTRIM(uc_impressoesArqTalaoDig.nomefich))
		RETURN .f.
	ELSE
		lcnomeFicheiro = ALLTRIM(uc_impressoesArqTalaoDig.nomefich)
		fecha("uc_impressoesArqTalaoDig")
	ENDIF
	
	IF FILE(ALLTRIM(myPath)+"\analises\" + ALLTRIM(lcnomeFicheiro))
		lcreport = Alltrim(uCrsPath.textvalue)+"\analises\" + Alltrim(lcnomeFicheiro)
	ELSE
		uf_perguntalt_chama("N�o foi encontrado IDU de grava��o em PDF. Por favor contacte o suporte.","OK","",16)
		RETURN .f.
	ENDIF
	
	
	* Se for uma factura a entidades 
	If (td.u_tipodoc)=1
		Select Fi
		
		LOCAL lnTot,pvp1,pvp2,pvp3,pvp4,pvp5,pvp6,pvp7,pvp8,pvp9, compUtente1,compUtente2,compUtente3,compUtente4, compUtente5,compUtente6,compUtente7,compUtente8,compUtente9
		Local compOrg1,compOrg2,compOrg3,CompOrg4,compOrg5,compOrg6,compOrg7,CompOrg8, CompOrg9, mes
		
		CALCULATE SUM(Fi.etiliquido) TO lnTot
		inTot = uf_gerais_extenso(lnTot)
		
		******* Calcula Totais ************
		Store 0 To pvp1,pvp2,pvp3,pvp4,pvp5,pvp6,pvp7,pvp8,pvp9,compUtente1,compUtente1,compUtente2,compUtente3,compUtente4, compUtente5,compUtente6,compUtente7,compUtente8,compUtente9
		Store 0 To compOrg1,compOrg2,compOrg3,CompOrg4,compOrg5,compOrg6,compOrg7,CompOrg8, CompOrg9
 			
		Select Fi
		Go top
		Scan  
			Do case
				CASE fi.tabiva==1
					pvp1 = pvp1 + Fi.altura
					compUtente1 = compUtente1 + Fi.largura
					compOrg1 = compOrg1 +  Fi.ETILIQUIDO
				Case Fi.tabiva == 2 
					pvp2 = pvp2 + Fi.altura
					compUtente2 = compUtente2 + Fi.largura
					compOrg2 = compOrg3 +  Fi.ETILIQUIDO
				Case Fi.tabiva == 3
					pvp3 = pvp3 + Fi.altura
					compUtente3 = compUtente3 + Fi.largura
					compOrg3 = compOrg3 +  Fi.ETILIQUIDO
				Case Fi.tabiva == 4
					pvp4 = pvp4 + Fi.altura
					compUtente4 = compUtente4 + Fi.largura
					compOrg4 = compOrg4 +  Fi.ETILIQUIDO
				Case fi.tabiva == 5
					pvp5 = pvp5 + fi.altura
					compUtente5 = compUtente5 + fi.largura
					compOrg5 = compOrg5 +  FI.ETILIQUIDO
				Case fi.tabiva == 6 
					pvp6 = pvp6 + fi.altura
					compUtente6 = compUtente6 + fi.largura
					compOrg6 = compOrg6 +  FI.ETILIQUIDO
				Case fi.tabiva == 7
					pvp7 = pvp7 + fi.altura
					compUtente7 = compUtente7 + fi.largura
					compOrg7 = compOrg7 +  FI.ETILIQUIDO
				Case fi.tabiva == 8
					pvp8 = pvp8 + fi.altura
					compUtente8 = compUtente8 + fi.largura
					compOrg8 = compOrg8 +  FI.ETILIQUIDO
				Case fi.tabiva == 9
					pvp9 = pvp9 + fi.altura
					compUtente9 = compUtente9 + fi.largura
					compOrg9 = compOrg9 +  FI.ETILIQUIDO
			Endcase
			Select Fi
		Endscan 
		*************************
		**** Calcula o Mes **********
		myMesDocumento = ""
		Select FT
		GO top
		myMesDocumento = uf_Gerais_CalculaMes(Month(FT.fdata))
		****************************
		Select Fi
		
		lcSQL=""
		TEXT TO lcSQL TEXTMERGE NOSHOW
		 	SELECT	design,Sum(largura) as largura,SUm(altura) as altura,
		 			SUM(CASE WHEN ISNUMERIC(litem2) = 1 THEN convert(numeric,litem2) ELSE 0 END) as litem2,
					SUM(CASE WHEN ISNUMERIC(litem) = 1 THEN convert(numeric,litem) ELSE 0 END) as litem,
		 			Sum(etiliquido) as etiliquido
			FROM	FI (nolock)
			WHERE ftstamp = '<<Alltrim(lnstamp)>>'
			GROUP BY DESIGN
		 Endtext
		
		IF !uf_gerais_actGrelha("","tempFi",lcSQL)
			uf_perguntalt_chama("OCORREU UM ERRO A CALCULAR INFORMACAO PARA A GRAVA��O EM PDF. Cod.6","OK","",16)
 			RETURN .f.
 		ENDIF
				
		Select FT
		GO TOP
		SELECT FT2
		GO TOP
		Select Fi
		GO TOP
		

	ENDIF

	Select ft 
	IF (ft.ndoc == 72 OR ft.ndoc == 77) AND ft.no < 199
		uf_imprimirgerais_dataMatrixCriaImagem(ft.ndoc)
	ENDIF 
		
	*****************************
	* PDF CREATOR API - parte 2 *
	*****************************
	uf_gerais_actGrelha("","ucrsTextoRodape",[exec up_cert_TextoRodape '] + ALLTRIM(td.tiposaft) + ['])
	
	LOCAL lcOldPrinter
	lcOldPrinter = SET("printer",2) &&Impressora por defeito
	
	uf_arquivodigital_preparaPDFCreator() 
		
	*Imprimir para PDF
	ReadyState = 0
	oPDFC.cOption("AutosaveFilename")  = LTRIM(lcNomeFicheiroLt)
	oPDFC.cOption("AutosaveDirectory") = lcCaminhoArquivo
	oPDFC.cprinterstop=.F.
	
	SELECT tempFi
	GO TOP
	SELECT tempFi
	GO Top
	IF !EMPTY(lcreport)	
		REPORT FORM Alltrim(lcreport) TO PRINTER NOCONSOLE
	ENDIF 

	*Fecha tramento de impress�o
	INKEY(1)
	
	&& Repoe impressora por defeito
	Set Printer To  Name (lcoldPrinter)
	
	IF USED("uCrsHash")
		fecha("uCrsHash")
	ENDIF
	
	IF USED("FT")
		fecha("FT")
	ENDIF
	IF USED("FT2")
		fecha("FT2")
	ENDIF
	IF USED("FI")
		fecha("FI")
	ENDIF
ENDFUNC

**
FUNCTION uf_arquivodigital_expFaturacao
	** Colocar container visivel
	ARQUIVO_DIGITAL.expFaturacao.visible = .t.
	
	** Colocar sombra
	uf_arquivodigital_sombra(.t.)
ENDFUNC

**
FUNCTION uf_arquivodigital_sombra
	LPARAMETERS tcBool
	
	IF tcBool
		** remover foco da grelha
		*receituario.crack.setfocus
		
		** Colocar sombra
		ARQUIVO_DIGITAL.sombra.width	= ARQUIVO_DIGITAL.width
		ARQUIVO_DIGITAL.sombra.height	= ARQUIVO_DIGITAL.height
		ARQUIVO_DIGITAL.sombra.left 	= 0
		ARQUIVO_DIGITAL.sombra.top 		= 0
		
		ARQUIVO_DIGITAL.sombra.visible 	= .t.
	ELSE
		** remover sombra
		ARQUIVO_DIGITAL.sombra.visible = .f.
	ENDIF
ENDFUNC

FUNCTION uf_arquivodigital_fecharexpFaturacao
	ARQUIVO_DIGITAL.expFaturacao.visible = .f.
	uf_arquivodigital_sombra(.f.)
ENDFUNC

FUNCTION uf_arquivodigital_expFaturacaoComum
	** IMPRESSORA EXISTE
	DIMENSION lcPrinters[1,1]
	APRINTERS(lcPrinters)
	LOCAL lcExistePrinter
	
	FOR lnIndex = 1 TO ALEN(lcPrinters,1)
		IF upper(alltrim(lcPrinters(lnIndex,1))) == "PDFCREATOR"
			lcExistePrinter = .t.
		ENDIF
	ENDFOR
	IF !(lcExistePrinter)
		uf_perguntalt_chama("PARA PODER EMITIR PDF'S, DEVE TER O PDFCREATOR INSTALADO","OK","",48)
		RETURN .f.
	ENDIF
	&& DIRECTORIO PREENCHIDO?
	LOCAL lcPath
	lcPath = ALLTRIM(ARQUIVO_DIGITAL.expFaturacao.txtDir.value)
	
	IF (ALLTRIM(lcPath))==""
		uf_perguntalt_chama("PARA PODER EMITIR PDF'S, DEVE PRIMEIRO DEFINIR UM DIRECT�RIO.","OK","",64)
		RETURN .f.
	ENDIF
	
	LOCAL lcSQL
	lcSQL = ''
	TEXT TO lcSQL TEXTMERGE noshow
		select 
			ftstamp 
		from 
			ft (nolock)
		where 
			fdata between '<<uf_gerais_getdate(ARQUIVO_DIGITAL.expFaturacao.dataIni.value,"SQL")>>' and '<<uf_gerais_getdate(ARQUIVO_DIGITAL.expFaturacao.dataFim.value,"SQL")>>'
	ENDTEXT 
	uf_gerais_actGrelha("","uCrsListaDocsExport",lcSQL)
ENDFUNC 

FUNCTION uf_arquivodigital_expFaturacaoTalao
	uf_arquivodigital_expFaturacaoComum()
	
	If uf_perguntalt_chama("TEM A CERTEZA QUE QUER EXPORTAR OS PDF'S PARA "+ALLTRIM(ARQUIVO_DIGITAL.expFaturacao.txtDir.value)+"?" + CHR(13) + CHR(13) + "Dependendo da per�odo seleccionado o processo poder� ser demorado.","Sim","N�o")
		&&r�gua
		LOCAL lcCont2
		lcCont2 = 1
		select uCrsListaDocsExport
		regua(0,RECCOUNT("uCrsListaDocsExport"),"A Exportar Documentos...")
		
		lcOldPrinter = SET("printer",2)
		
		uf_arquivodigital_preparaPDFCreator()
		
		SELECT uCrsListaDocsExport
		GO TOP
		SCAN
			regua(1,lcCont2,"A Exportar Documento: " + astr(lcCont2))
			
			IF !EMPTY(uCrsListaDocsExport.ftstamp)
				&& for�ar c�lculo dos cursores
				IF USED("uCrsFt")
					fecha("uCrsFt")
				ENDIF 
				IF USED("uCrsFt2")
					fecha("uCrsFt2")
				ENDIF 
				IF USED("uCrsFi")
					fecha("uCrsFi")
				ENDIF 
				SELECT uCrsListaDocsExport
				uf_imprimirpos_prevTalao(uCrsListaDocsExport.ftstamp,"ARQDIGITAL",.t.)
			ENDIF
			
			lcCont2 = lcCont2 + 1
			
			SELECT uCrsListaDocsExport
		ENDSCAN
		GO TOP
		
		Set Printer To  Name (lcoldPrinter) 	
		uf_perguntalt_chama("PROCESSO CONCLU�DO COM SUCESSO. POR FAVOR VERIFIQUE OS FICHEIROS NA DIRECTORIA DEFINIDA."+CHR(10)+CHR(10)+"NOTA: A GERA��O DOS FICHEIROS PODER� AINDA ESTAR EM CURSO.","OK","",64)
		regua(2)
	ENDIF
	
	fecha("uCrsListaDocsExport")
ENDFUNC 

FUNCTION uf_arquivodigital_expFaturacaoA4
	uf_arquivodigital_expFaturacaoComum()
	
	If uf_perguntalt_chama("TEM A CERTEZA QUE QUER EXPORTAR OS PDF'S PARA "+ALLTRIM(ARQUIVO_DIGITAL.expFaturacao.txtDir.value)+"?" + CHR(13) + CHR(13) +"Dependendo da per�odo seleccionado o processo poder� ser demorado.","Sim","N�o")
		
		&&r�gua
		LOCAL lcCont2
		lcCont2 = 1
		select uCrsListaDocsExport
		regua(0,RECCOUNT("uCrsListaDocsExport"),"A Exportar Documentos...")
		
		lcOldPrinter = SET("printer",2)
		
		uf_arquivodigital_preparaPDFCreator()
		
		&&Percorre o cursor e imprime
		SELECT uCrsListaDocsExport
		GO TOP
		SCAN
			regua(1,lcCont2,"A Exportar Documento: " + astr(lcCont2))
			
			uf_gerais_actGrelha("",[FT],[exec up_touch_ft ']+ALLTRIM(uCrsListaDocsExport.ftstamp)+['])
			Select FT
			uf_gerais_actGrelha("",[FT2],[exec up_touch_ft2 ']+ALLTRIM(uCrsListaDocsExport.ftstamp)+['])
			Select FT2
			uf_gerais_actGrelha("",[FI],[select * from fi (nolock) where epromo=0 and ftstamp=']+ALLTRIM(uCrsListaDocsExport.ftstamp)+[' order by lordem])
			Select FI
			
			uf_arquivodigital_imprimeFactpdf(uCrsListaDocsExport.ftstamp,.f.)
			lcCont2 = lcCont2 + 1

			SELECT uCrsListaDocsExport
		ENDSCAN

		&& Repoe impressora por defeito
		Set Printer To  Name (lcoldPrinter) 	
		uf_perguntalt_chama("PROCESSO CONCLU�DO COM SUCESSO. POR FAVOR VERIFIQUE OS FICHEIROS NA DIRECTORIA DEFINIDA."+CHR(10)+CHR(10)+"NOTA: A GERA��O DOS FICHEIROS PODER� AINDA ESTAR EM CURSO.","OK","",64)
		
		regua(2)
	ENDIF && Fim da pergunta
	
	fecha("uCrsListaDocsExport")
ENDFUNC