**
PROCEDURE uf_chavelocal_chama
 IF TYPE("CHAVELOCAL")=="U"
    DO FORM CHAVELOCAL
 ELSE
    chavelocal.show
 ENDIF
 uf_chavelocal_getkey()
ENDPROC
**

FUNCTION uf_chavelocal_getkey
	select ucrsempresas	
	LOCATE FOR ALLTRIM(ucrsempresas.nome)==ALLTRIM(login.ctndados.txtempresa.value)
	IF FOUND()
		IF EMPTY(ALLTRIM(ucrsempresas.cs))
			lcconstr = "DSN="+ALLTRIM(ucrsempresas.odbc)+";UID=logiserv;PWD=ls2k12...;DATABASE="+ALLTRIM(ucrsempresas.nomebd)+";App=LT - "+myusername+" - "+myclientname
		ELSE
			lcconstr = ALLTRIM(ucrsempresas.cs)
		ENDIF
	ELSE
		RETURN .F.
	ENDIF
 
	PUBLIC gnconnhandle
	gnconnhandle = SQLSTRINGCONNECT(lcconstr)
	IF gnconnhandle>0
		PUBLIC sql_db
		LOCAL LcValString 
		sql_db = ALLTRIM(ucrsempresas.nomebd)
		TEXT TO lcsql TEXTMERGE NOSHOW
			select masterkey from empresa
		ENDTEXT
		SQLEXEC(gnconnhandle, lcsql, "uCrsMK")
		select uCrsMK
		LcValString = ALLTRIM(uCrsMK.masterkey)
		LcValString = uf_chavelocal_decrypt(LcValString )
		LcValString = ALLTRIM(uf_chavelocal_randomString(1,1)) + '|' + LcValString + ALLTRIM(uf_chavelocal_randomString(1,1))
		LcValString = uf_chavelocal_encrypt(LcValString )
		chavelocal.Edit1.value=alltrim(LcValString )
	ELSE
		MESSAGEBOX("Connection Failed", 16, "ODBC Problem")
		RETURN .F.
	ENDIF 
	 
ENDFUNC 

PROCEDURE uf_chavelocal_sair
 chavelocal.release
 RELEASE chavelocal
ENDPROC

FUNCTION uf_chavelocal_validade
	LOCAL lcOldKey, lcNewKey, lcOldKeyTime, lcOldKeyID, lcNewKeyID , lcNewKeyID 
	lcOldKey = ALLTRIM(chavelocal.Edit1.value)
	lcNewKey = ALLTRIM(chavelocal.Edit2.value)
	lcOldKey = uf_chavelocal_decrypt(lcOldKey)
	lcNewKey = uf_chavelocal_decrypt(lcNewKey)

	lcOldKeyTime=alltrim(substr(lcOldKey , 8,20))
	lcOldKeyID=right(lcOldKey ,1)

	lcNewKeyTime=substr(lcNewKey ,2,20 )
	lcNewKeyID = right(alltrim(lcNewKey ), 1)
	IF (ctot(lcNewKeyTime)-ctot(lcOldKeyTime))<3600 AND ALLTRIM(lcNewKeyID) == ALLTRIM(lcOldKeyID)

		LOCAL lcMasterKeySTR, lcMasterKey, lcNomeBD   
		lcMasterKeySTR = uf_chavelocal_randomString(1,5) + ALLTRIM(ttoc(datetime()))+'|01'+uf_chavelocal_randomString(1,4)
		lcMasterKey  = uf_chavelocal_encrypt(lcMasterKeySTR)
		
		select ucrsempresas	
		LOCATE FOR ALLTRIM(ucrsempresas.nome)==ALLTRIM(login.ctndados.txtempresa.value)
		IF FOUND()
			IF EMPTY(ALLTRIM(ucrsempresas.cs))
				lcNomeBD = ALLTRIM(ucrsempresas.nomebd)
				lcconstr = "DSN="+ALLTRIM(ucrsempresas.odbc)+";UID=logiserv;PWD=ls2k12...;DATABASE="+ALLTRIM(ucrsempresas.nomebd)+";App=LT - "+myusername+" - "+myclientname
			ELSE
				lcconstr = ALLTRIM(ucrsempresas.cs)
			ENDIF
		ELSE
			RETURN .F.
		ENDIF
	 
		PUBLIC gnconnhandle
		gnconnhandle = SQLSTRINGCONNECT(lcconstr)
		IF gnconnhandle>0
			PUBLIC sql_db
			LOCAL LcValString 
			sql_db = ALLTRIM(ucrsempresas.nomebd)
			TEXT TO lcsql TEXTMERGE NOSHOW
				update empresa set masterkey='<<ALLTRIM(lcMasterKey)>>' WHERE id_lt = '<<ALLTRIM(lcNomeBD)>>'
			ENDTEXT
			SQLEXEC(gnconnhandle, lcsql, "uCrsMK")
			MESSAGEBOX("Chave Validada", 64, "Software Logitools")
		ELSE
			MESSAGEBOX("Connection Failed", 16, "ODBC Problem")
			RETURN .F.
		ENDIF 
		
	ELSE
		MESSAGEBOX("Chave Inv�lida", 16, "Software Logitools")
        QUIT
	ENDIF 
	
ENDFUNC 

FUNCTION uf_chavelocal_encrypt
	LPARAMETERS lcString
	LOCAL loCrypt, lcTxt1, loSbEncrypted, lcDecryptedText, lcRetString

	loCrypt = CreateObject('Chilkat_9_5_0.Crypt2')

	lnSuccess = loCrypt .UnlockComponent("LGTLSP.CB1052019_eQLZAyAR9RkB")
		IF (lnSuccess <> 1) THEN
		  	? loMailman.LastErrorText
		    RELEASE loMailman
		   CANCEL
		ENDIF

	loCrypt.CryptAlgorithm = "aes"
	loCrypt.CipherMode = "cbc"
	loCrypt.KeyLength = 128

	loCrypt.SetEncodedKey("L0g1T00lsFarm@L0g1Lts2k16..LTS","hex")
	loCrypt.SetEncodedIV("L0g1T00lsFarm@L0g1Lts2k16..LTS","hex")

	loCrypt.EncodingMode = "hex"
	lcTxt1 = ALLTRIM(lcString)
	**lcTxt2 = "-" + CHR(13) + CHR(10)
	**lcTxt3 = "Done." + CHR(13) + CHR(10)


	loSbEncrypted = CreateObject('Chilkat_9_5_0.StringBuilder')


	loSbEncrypted.Append(loCrypt.EncryptStringENC(lcTxt1))

	lcRetString=loSbEncrypted.GetAsString()

	RELEASE loCrypt
	RELEASE loSbEncrypted
	
	RETURN lcRetString
ENDFUNC 

FUNCTION uf_chavelocal_decrypt
	LPARAMETERS lcString
	LOCAL loCrypt, lcTxt1, loSbEncrypted, lcDecryptedText, lcRetString

	loCrypt = CreateObject('Chilkat_9_5_0.Crypt2')

	lnSuccess = loCrypt .UnlockComponent("LGTLSP.CB1052019_eQLZAyAR9RkB")
		IF (lnSuccess <> 1) THEN
		  	? loMailman.LastErrorText
		    RELEASE loMailman
		   CANCEL
		ENDIF

	loCrypt.CryptAlgorithm = "aes"
	loCrypt.CipherMode = "cbc"
	loCrypt.KeyLength = 128

	loCrypt.SetEncodedKey("L0g1T00lsFarm@L0g1Lts2k16..LTS","hex")
	loCrypt.SetEncodedIV("L0g1T00lsFarm@L0g1Lts2k16..LTS","hex")

	loCrypt.EncodingMode = "hex"
	lcTxt1 = ALLTRIM(lcString)

	lcDecryptedText = loCrypt.DecryptStringENC(lcTxt1)

	lcRetString = lcDecryptedText

	RELEASE loCrypt
	RELEASE loSbEncrypted
	
	RETURN lcRetString
ENDFUNC

FUNCTION uf_chavelocal_randomString
	LPARAMETERS lcRandIni, lcRandFim
	LOCAL lcRandomString
	STORE '' TO lcRandomString
	
	lcPWchars = "ABCDEFGHJKLMNPQRSTUVWXYZ1234567890" 
	lnPWLen = LEN(lcPWchars) 
	
	=RAND(-1) && Seed the RAND() function 
	
	FOR lnii = lcRandIni TO lcRandFim 
	    lcRandomString = lcRandomString + SUBSTR(lcPWchars, INT(RAND()*lnPWLen)+1, 1) 
	ENDFOR 
	
	RETURN lcRandomString

ENDFUNC 	