DECLARE Long FindWindow In Win32API String, String
uf_integracaoPhc_bringToFront()

FUNCTION uf_integracaoPhc_bringToFront
	lcscreenCaption = [Logitools Software]
	nHWND = FindWindow(Null,lcscreenCaption)

	If nHWND >0
	    =uf_integracaoPhc_ForceForegroundWindow(nHWND)
	ENDIF
ENDFUNC


FUNCTION uf_integracaoPhc_ForceForegroundWindow(lnHWND)

    LOCAL nForeThread, nAppThread
    
    =uf_integracaoPhc_Decl()
    
    nForeThread = GetWindowThreadProcessId(GetForegroundWindow(), 0)
    nAppThread = GetCurrentThreadId()

    IF nForeThread != nAppThread
        AttachThreadInput(nForeThread, nAppThread, .T.)
        BringWindowToTop(lnHWND)
        ShowWindow(lnHWND,3)
        AttachThreadInput(nForeThread, nAppThread, .F.)
    ELSE
        BringWindowToTop(lnHWND)
        ShowWindow(lnHWND,3)
    ENDIF
    
ENDFUNC

FUNCTION uf_integracaoPhc_Decl
*!* DECLARE INTEGER SetForegroundWindow IN user32 INTEGER hwnd  
    DECLARE Long BringWindowToTop In Win32API Long

    DECLARE Long ShowWindow In Win32API Long, Long

    DECLARE INTEGER GetCurrentThreadId; 
        IN kernel32
     
    DECLARE INTEGER GetWindowThreadProcessId IN user32; 
        INTEGER   hWnd,; 
        INTEGER @ lpdwProcId  
     
    DECLARE INTEGER GetCurrentThreadId; 
        IN kernel32  
        
    DECLARE INTEGER AttachThreadInput IN user32 ;
        INTEGER idAttach, ;
        INTEGER idAttachTo, ;
        INTEGER fAttach

    DECLARE INTEGER GetForegroundWindow IN user32  
    
ENDFUNC