*** 
*** ReFox XII  #PT125680  Jose Costa  LTS Lda. [VFP90]
***
 LPARAMETERS lcmodo, lcuser, lcpass, lccomando
 DO aptec
 DO login
 DO painelcentral
 uf_logitools_arranca(lcuser, lcpass, lccomando)
 
**
DEFINE CLASS ScreenHook AS Session
**
   PROCEDURE myresize
    IF TYPE("painelcentral")<>"U"
       painelcentral.resize()
    ENDIF
   ENDPROC
**
ENDDEFINE
**
PROCEDURE uf_logitools_arranca
 LPARAMETERS lcuser, lcpass, lccomando
 TRY
    IF TYPE("lcModo")<>"C"
       lcmodo = ""
    ENDIF
    IF TYPE("lcUser")<>"C"
       lcuser = ""
    ENDIF
    IF TYPE("lcPass")<>"C"
       lcpass = ""
    ENDIF
    IF TYPE("lcComando")<>"C"
       lccomando = ""
    ENDIF
    SET RESOURCE OFF
    SET DELETED ON
    SET TALK OFF
    SET NOTIFY OFF
    SET SYSMENU OFF
    SET ESCAPE OFF
    SET SAFETY OFF
    SET CONSOLE OFF
    SET DATE TO YMD
    SET POINT TO "."
    ON KEY LABEL CTRL+F2 uf_aptec_chama()
    ON SHUTDOWN uf_logitools_sair()
    _SCREEN.windowstate = 2
    _SCREEN.minwidth = 600
    _SCREEN.minheight = 600
    _SCREEN.icon = "logitools_icon.ico"
    _SCREEN.controlbox = .F.
    PUBLIC osh
    osh = NEWOBJECT("ScreenHook")
    BINDEVENT(_SCREEN, "Resize", osh, "myResize")
    LOCAL lcexe, lcvar
    PUBLIC dircliente, myusername, mypath, mycomputername, emdesenvolvimento, myclientname, mysessionname
    STORE '' TO lcvar, dircliente
    IF EMPTY(mypath)
       mypath = ""
    ENDIF
    STORE "logitools.exe" TO lcexe
    myusername = SYS(0)
    myusername = ALLTRIM(SUBSTR(myusername, AT("#", myusername)+1))
    myclientname = GETENV("CLIENTNAME")
    mysessionname = GETENV("SessionName")
    CREATE CURSOR uCrsDirectorios (directorio C (125))
    IF  .NOT. FILE('config.ini')
       MESSAGEBOX('O ficheiro de configura��o de direct�rios n�o existe!', 16, 'Logitools Software')
       RETURN
    ENDIF
    SET MEMOWIDTH TO 125
    lcficheiroini = UPPER(FILETOSTR('config.ini'))
    FOR lclinha = 1 TO (MEMLINES(lcficheiroini))
       lcstringlinha = MLINE(lcficheiroini, lclinha)
       IF UPPER('Directorio=')$lcstringlinha
          SELECT ucrsdirectorios
          INSERT INTO uCrsDirectorios (directorio) VALUES (ALLTRIM(STREXTRACT(lcstringlinha, UPPER('Directorio='))))
       ENDIF
    ENDFOR
    SELECT ucrsdirectorios
    DELETE FOR ALLTRIM(ucrsdirectorios.directorio)==""
    IF RECCOUNT("uCrsDirectorios")==0
       MESSAGEBOX("N�o existem direct�rios configurados!", 16, "LOGITOOLS SOFTWARE")
       QUIT
    ENDIF
    dircliente = 'Y:\'
    DO CASE
       CASE UPPER(ALLTRIM(lcmodo))=="RECUPERA"
          uf_painelcentral_chama(lcuser, lcpass, lccomando, .F.)
       CASE UPPER(ALLTRIM(lcmodo))=="AUTO"
          uf_painelcentral_chama(lcuser, lcpass, lccomando, .T.)
       OTHERWISE
          uf_painelcentral_chama(lcuser, lcpass, lccomando, .F.)
    ENDCASE
    READ EVENTS
 CATCH TO oerr
    IF emdesenvolvimento==.T.
       uf_aptec_chama()
       aptec.edit2.value = ''
       aptec.edit2.value = aptec.edit2.value+CHR(13)+': Nested Catch! (Unhandled: Throw oErr Object Up) '
       aptec.edit2.value = aptec.edit2.value+CHR(13)+'  Inner Exception Object: '
       aptec.edit2.value = aptec.edit2.value+CHR(13)+'  Error: '+STR(oerr.errorno)
       aptec.edit2.value = aptec.edit2.value+CHR(13)+'  LineNo: '+STR(oerr.lineno)
       aptec.edit2.value = aptec.edit2.value+CHR(13)+'  Message: '+oerr.message
       aptec.edit2.value = aptec.edit2.value+CHR(13)+'  Procedure: '+oerr.procedure
       aptec.edit2.value = aptec.edit2.value+CHR(13)+'  Details: '+oerr.details
       aptec.edit2.value = aptec.edit2.value+CHR(13)+'  StackLevel: '+STR(oerr.stacklevel)
       aptec.edit2.value = aptec.edit2.value+CHR(13)+'  LineContents: '+oerr.linecontents
       aptec.edit2.value = aptec.edit2.value+CHR(13)+'  UserValue: '+oerr.uservalue
    ELSE
       uf_perguntalt_chama("Foi detectada uma anomalia."+CHR(13)+"Assim que poss�vel reinicie o software."+CHR(13)+CHR(13)+"Ser� enviado para o suporte o registo da anomalia.", "OK", "", 16)
       LOCAL lcbody, lcfooter, lcsubject, lcto, lcattach, lcpath, lcappprtscreen
       lcbody = ''
       lcfooter = ''
       lcsubject = ''
       lcto = ''
       lcattach = ''
       lcpath = ''
       lcappprtscreen = ''
       lcbody = lcbody+" <br> "+': Nested Catch! (Unhandled: Throw oErr Object Up) '
       lcbody = lcbody+" <br> "+'  Inner Exception Object: '
       lcbody = lcbody+" <br> "+'  Error: '+STRTRAN(STR(oerr.errorno), CHR(10), '')
       lcbody = lcbody+" <br> "+'  LineNo: '+STRTRAN(STR(oerr.lineno), CHR(10), '')
       lcbody = lcbody+" <br> "+'  Message: '+STRTRAN(oerr.message, CHR(10), '')
       lcbody = lcbody+" <br> "+'  Procedure: '+STRTRAN(oerr.procedure, CHR(10), '')
       lcbody = lcbody+" <br> "+'  Details: '+STRTRAN(oerr.details, CHR(10), '')
       lcbody = lcbody+" <br> "+'  StackLevel: '+STRTRAN(STR(oerr.stacklevel), CHR(10), '')
       lcbody = lcbody+" <br> "+'  LineContents: '+STRTRAN(oerr.linecontents, CHR(10), '')
       lcbody = lcbody+" <br> "+'  UserValue: '+STRTRAN(oerr.uservalue, CHR(10), '')
       lcbody = lcbody+" <br> "+'  Operador: '+astr(ch_vendnm)
       TRY
          lcfooter = ALLTRIM(uf_gerais_getparameter("ADM0000000157", "TEXT"))+" - "+astr(myterm)
       CATCH
          lcfooter = ''
       ENDTRY
       lcsubject = 'E FARM:'+ALLTRIM(uf_gerais_getparameter("ADM0000000156", "TEXT"))+' CL:'+ALLTRIM(uf_gerais_getparameter("ADM0000000157", "TEXT"))
       lcto = IIF( .NOT. EMPTY(ALLTRIM(uf_gerais_getparameter("ADM0000000210", "TEXT"))), ALLTRIM(uf_gerais_getparameter("ADM0000000210", "TEXT")), 'suporte@logitools.pt')
       lcpath = ALLTRIM(uf_gerais_getparameter("ADM0000000015", "TEXT"))+"\imagens\anomalia.png"
       lcappprtscreen = ALLTRIM(uf_gerais_getparameter("ADM0000000015", "TEXT"))+"\libs\prtscreen.app"
       DO &lcappprtscreen
       LOCAL lobmp AS XFCBITMAP
       WITH _SCREEN.system.drawing
          lobmp = .bitmap.fromscreen(_SCREEN)
          lobmp.save(lcpath, .imaging.imageformat.png)
       ENDWITH
       lcattach = '"'+lcpath+'"'
       IF DIRECTORY(ALLTRIM(uf_gerais_getparameter("ADM0000000185", "TEXT"))+'\SendMail')
          lcwspath = ALLTRIM(uf_gerais_getparameter("ADM0000000185", "TEXT"))+'\SendMail\SendMail.jar "'+ALLTRIM(lcbody)+'" "'+ALLTRIM(lcfooter)+'" "'+ALLTRIM(lcsubject)+'" "'+ALLTRIM(lcto)+'" '+ALLTRIM(lcattach)
          lcwspath = "javaw -jar "+lcwspath
          owsshell = CREATEOBJECT("WScript.Shell")
          owsshell.run(lcwspath, 1, .F.)
       ELSE
          uf_perguntalt_chama("N�o foi poss�vel enviar o email. Por favor contacte o suporte.", "OK", "", 16)
       ENDIF
    ENDIF
    uf_startup_startterminal()
    uf_logitools_arranca("RECUPERA")
    READ EVENTS
 FINALLY
    uf_startup_startterminal()
    uf_logitools_arranca("RECUPERA")
    READ EVENTS
 ENDTRY
ENDPROC
**
PROCEDURE uf_logitools_sair
 TRY
    SQLDISCONNECT(gnconnhandle)
 CATCH
 ENDTRY
 QUIT
ENDPROC
**
PROCEDURE uf_logitools_addglowobj
 LPARAMETERS lcobj
 lcobj2 = lcobj
 ALINES(arobj, lcobj, ".")
 lcobj = ""
 FOR i = 1 TO ALEN(arobj)-1
    lcobj = lcobj+arobj(i)+"."
 ENDFOR
 IF TYPE(lcobj+"glow")=="U"
    &lcobj.ADDOBJECT("glow","shape")
    &lcobj.glow.BACKSTYLE = 0 
    &lcobj.glow.BORDERCOLOR = RGB(99,138,148) 
    &lcobj.glow.BORDERWIDTH = 2
 ENDIF
 &lcobj.glow.VISIBLE = .t.
 &lcobj.glow.LEFT = &lcobj2..LEFT - 1
 &lcobj.glow.TOP = &lcobj2..TOP - 1
 &lcobj.glow.HEIGHT = &lcobj2..HEIGHT + 2
 &lcobj.glow.WIDTH = &lcobj2..WIDTH + 2
 &lcobj.glow.ZORDER(1)
ENDPROC
**
PROCEDURE uf_logitools_removeglowobj
 LPARAMETERS lcobj
 ALINES(arobj, lcobj, ".")
 lcobj = ""
 FOR i = 1 TO ALEN(arobj)-1
    lcobj = lcobj+arobj(i)+"."
 ENDFOR
 &lcobj.glow.VISIBLE = .f.
ENDPROC
**
PROCEDURE uf_exe_sessionid
 LOCAL lolocator, lowmi, loprocesses, loprocess
 lolocator = CREATEOBJECT('WBEMScripting.SWBEMLocator')
 lowmi = lolocator.connectserver()
 lowmi.security_.impersonationlevel = 3
 DECLARE INTEGER GetCurrentProcessId IN win32api INTEGER
 lcpid = getcurrentprocessid(0)
 loprocesses = lowmi.execquery('SELECT SessionID FROM Win32_Process where ProcessId = '+STR(lcpid)+'')
 IF loprocesses.count>0
    FOR EACH loprocess IN loprocesses
       MESSAGEBOX(loprocess.sessionid)
    ENDFOR
 ENDIF
ENDPROC
**

