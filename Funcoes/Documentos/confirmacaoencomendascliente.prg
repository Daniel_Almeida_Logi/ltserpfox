**
FUNCTION uf_confirmacaoencomendascliente_chama
	
	IF TYPE("confirmacaoencomendascliente") == "U"
			
		&& Calcula Encomendas de Cliente abertas e com quantidade por satisfazer do grupo de cliente selecionado
		text to lcSQL noshow textmerge 
			exec up_keiretsu_ConfirmacaoEntregaStock '',0,'','', 1
		endtext 
		IF !uf_gerais_actgrelha("", "ucrsEncomendasClienteSatisfazer", lcSql)
			MESSAGEBOX("N�o foi possivel verificar as quantidades a satisfazer das Encomendas de Cliente.",16,"LOGITOOLS SOFTWARE")
			RETURN .f.
		ENDIF
		
		DO FORM confirmacaoencomendascliente
		
		confirmacaoencomendascliente.menu1.adicionaOpcao("actualizar", "Actualizar", myPath + "\imagens\icons\actualizar_w.png", "uf_confirmacaoencomendascliente_ConfirmacaoEntrega","A")		
		confirmacaoencomendascliente.menu1.adicionaOpcao("selTodos", "Sel. Todos", myPath + "\imagens\icons\unchecked_w.png", "uf_confirmacaoencomendascliente_SelTodos","T")
		confirmacaoencomendascliente.menu1.adicionaOpcao("subir", "Subir", myPath + "\imagens\icons\ponta_seta_up.png", "uf_gerais_MovePage with .t.,'confirmacaoencomendascliente.gridPesq','ucrsEncomendasClienteSatisfazer'","05")
		confirmacaoencomendascliente.menu1.adicionaOpcao("descer", "Descer", myPath + "\imagens\icons\ponta_seta_down.png", "uf_gerais_MovePage with .f.,'confirmacaoencomendascliente.gridPesq','ucrsEncomendasClienteSatisfazer'","24")
		confirmacaoencomendascliente.menu1.estado("", "SHOW", "Aplicar", .t., "Sair", .t.)
	ELSE
		confirmacaoencomendascliente.show()
	ENDIF
	
	
ENDFUNC


** Calcula a quantidade a distribuir
FUNCTION uf_confirmacaoencomendascliente_ConfirmacaoEntrega
	** As encomendas a considerar s�o as do grupo de clientes especificado e com estado aberto

	&& Calcula Encomendas de Cliente abertas e com quantidade por satisfazer do grupo de cliente selecionado
	text to lcSQL noshow textmerge 
		exec up_keiretsu_ConfirmacaoEntregaStock 
		'<<IIF(EMPTY(confirmacaoencomendascliente.tipoUtentes.value),'',confirmacaoencomendascliente.tipoUtentes.value)>>'
		,<<IIF(EMPTY(confirmacaoencomendascliente.armazem.value),0,confirmacaoencomendascliente.armazem.value)>>
		,'<<ALLTRIM(confirmacaoencomendascliente.lab.value)>>'
		,'<<ALLTRIM(confirmacaoencomendascliente.cliente.value)>>'
		,<<mysite_nr>>
	ENDTEXT

	IF !uf_gerais_actgrelha("confirmacaoencomendascliente.gridPesq", "ucrsEncomendasClienteSatisfazer", lcSql)
		MESSAGEBOX("N�o foi possivel verificar as quantidades a satisfazer das Encomendas de Cliente.",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF
	
	&& Calcula Stock para o armazem Selecionado
	text to lcSQL noshow textmerge 
		exec up_keiretsu_Stock_Distribuir <<IIF(EMPTY(confirmacaoencomendascliente.armazem.value),0,confirmacaoencomendascliente.armazem.value)>>
	endtext 
	IF !uf_gerais_actgrelha("", "ucrsConfirmacaoEntregaStockArmazem", lcSql)
		MESSAGEBOX("N�o foi possivel verificar as quantidades a satisfazer das Encomendas de Cliente.",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF

	DELETE ucrsConfirmacaoEntregaStockArmazem;
	FROM;
		ucrsConfirmacaoEntregaStockArmazem;
		left join ucrsEncomendasClienteSatisfazer on ucrsConfirmacaoEntregaStockArmazem.ref = ucrsEncomendasClienteSatisfazer.ref;
	Where;
		ucrsEncomendasClienteSatisfazer.ref is null
	
	regua(0,RECCOUNT("ucrsEncomendasClienteSatisfazer"),"A CALCULAR DISTRIBUI��O...",.f.)
	
	Select ucrsConfirmacaoEntregaStockArmazem
	GO Top
	SCAN	
			
		For i = 1 to ucrsConfirmacaoEntregaStockArmazem.stock - ucrsConfirmacaoEntregaStockArmazem.stockConfirmacao
		
			lcJaAplicou = .F.
			
			Select ucrsEncomendasClienteSatisfazer
			GO Top
			SCAN FOR ALLTRIM(ucrsConfirmacaoEntregaStockArmazem.ref) == ALLTRIM(ucrsEncomendasClienteSatisfazer.ref);
				AND (ucrsEncomendasClienteSatisfazer.qttatribuida + ucrsEncomendasClienteSatisfazer.qttrec) < ucrsEncomendasClienteSatisfazer.qtt
						
				IF lcJaAplicou == .F.
					
					Replace ucrsEncomendasClienteSatisfazer.qttatribuida WIth ucrsEncomendasClienteSatisfazer.qttatribuida+ 1
				
					lcJaAplicou = .T.
				ENDIF			
			ENDSCAN
			
		ENDFOR 
		regua[1,recno(),"PRODUTO: "+ALLTRIM(ucrsConfirmacaoEntregaStockArmazem.ref),.f.]
	ENDSCAN
	regua(2)
	confirmacaoencomendascliente.gridPesq.refresh
ENDFUNC


** 
FUNCTION uf_confirmacaoencomendascliente_SelTodos

	IF EMPTY(confirmacaoencomendascliente.menu1.seltodos.tag) OR confirmacaoencomendascliente.menu1.seltodos.tag == "false"
		&& aplica sele��o
		REPLACE ALL sel WITH .t. IN ucrsEncomendasClienteSatisfazer
		
		&& altera o botao
		confirmacaoencomendascliente.menu1.selTodos.tag = "true"
		confirmacaoencomendascliente.menu1.selTodos.config("Sel. Todos", myPath + "\imagens\icons\checked_w.png", "uf_confirmacaoencomendascliente_SelTodos","T")
	ELSE
		&& aplica sele��o
		REPLACE ALL sel WITH .f. IN ucrsEncomendasClienteSatisfazer
		
		&& altera o botao
		confirmacaoencomendascliente.menu1.selTodos.tag = "false"
		confirmacaoencomendascliente.menu1.selTodos.config("Sel. Todos", myPath + "\imagens\icons\unchecked_w.png", "uf_confirmacaoencomendascliente_SelTodos","T")
		
	ENDIF
		
	SELECT ucrsEncomendasClienteSatisfazer
	GO top

ENDFUNC 


**
FUNCTION uf_confirmacaoencomendascliente_gravar

	&&
	confirmacaoencomendascliente.refresh
	
	
	IF USED("ucrsEncomendasClienteSatisfazerAux")
		fecha("ucrsEncomendasClienteSatisfazerAux")
	ENDIF 
	
	Select ucrsEncomendasClienteSatisfazer
	GO Top
	
	Select * FROM ucrsEncomendasClienteSatisfazer WHERE ucrsEncomendasClienteSatisfazer.qttatribuida != 0 AND ucrsEncomendasClienteSatisfazer.sel == .t. INTO CURSOR ucrsEncomendasClienteSatisfazerAux READWRITE
	
	regua(0,RECCOUNT("ucrsEncomendasClienteSatisfazerAux"),"A Actualizar Confirm. Entrega Cliente...")
	
	Select ucrsEncomendasClienteSatisfazerAux
	GO Top
	SCAN 

		regua(1,RECCOUNT("ucrsEncomendasClienteSatisfazerAux"),"A Actualizar Confirm. Entrega Cliente...")
		
		Select ucrsEncomendasClienteSatisfazerAux

		** Caso n�o exista documento de Confirma��o cria 
		uf_condicoesComerciais_insereConferenciaEncClientes(ucrsEncomendasClienteSatisfazerAux.bostamp)

		Select ucrsEncomendasClienteSatisfazerAux
		** 
		lcSQL = ""
		text to lcSQL noshow textmerge 
			update 
				bi 
			set 
				qtrec = qtrec + <<ucrsEncomendasClienteSatisfazerAux.qttatribuida>> 
				,armazem = <<ucrsEncomendasClienteSatisfazerAux.armazem>> 
			where 
				obistamp = '<<ucrsEncomendasClienteSatisfazerAux.bistamp>>' 
				and ref = '<<Alltrim(ucrsEncomendasClienteSatisfazerAux.ref)>>'
		ENDTEXT

		IF !uf_gerais_actgrelha("", "", lcSql)
			MESSAGEBOX("N�o foi possivel atualizar a quantidade rececionada na Confirma��o de Entrega Cliente. Contacte o suporte.",16,"LOGITOOLS SOFTWARE")
			RETURN .f.
		ENDIF
		
		Select ucrsEncomendasClienteSatisfazerAux
	ENDSCAN

	regua(2)
	
	IF USED("ucrsEncomendasClienteSatisfazerAux")
		fecha("ucrsEncomendasClienteSatisfazerAux")
	ENDIF 
	
	**
	uf_confirmacaoencomendascliente_ConfirmacaoEntrega()
	**
	
	&&
	confirmacaoencomendascliente.refresh
	**
	uf_perguntalt_chama("CONFIRMA��ES ACTUALIZADAS COM SUCESSO.","OK","",64)
ENDFUNC 



**
FUNCTION uf_confirmacaoencomendascliente_sair
	confirmacaoencomendascliente.hide
	confirmacaoencomendascliente.release
ENDFUNC

