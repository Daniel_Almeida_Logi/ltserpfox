**
FUNCTION uf_dadosentidade_chama
	IF !USED("cabDoc")
		RETURN .f.
	ENDIF 

	Select cabDoc
	Go Top
	Select nome, morada, local, codpost, ncont, telefone,ccusto From cabdoc Into Cursor ucrsDadosEntidades readwrite
	**
	
	IF TYPE("DADOSENTIDADE")=="U"
		DO FORM DADOSENTIDADE
	ELSE
		DADOSENTIDADE.show 
	ENDIF
ENDFUNC


** Actualiza os Dados com base no fornecedor
FUNCTION uf_dadosentidade_rechamarDados	
	Do CASE 
		Case Alltrim(ucrsConfigDoc.bdempresas) = "FL"
			lcSQL = "Select nome, morada, local, codpost, ncont, telefone,ccusto From FL (nolock) Where nome = '" + Alltrim(cabDoc.Nome) + "' AND NO = " + Str(cabDoc.no) + " AND ESTAB = " + Str(cabDoc.estab)
		
		Case Alltrim(ucrsConfigDoc.bdempresas) = "CL"
			lcSQL = "Select nome, morada, local, codpost, ncont, telefone,ccusto From b_utentes (nolock) Where nome = '" + Alltrim(cabDoc.Nome) + "' AND NO = " + Str(cabDoc.no) + " AND ESTAB = " + Str(cabDoc.estab)
			
		Case Alltrim(ucrsConfigDoc.bdempresas) = "AG"
			lcSQL = "Select nome, morada, local, codpost, ncont, telefone,'' as ccusto From AG (nolock) Where nome = '" + Alltrim(cabDoc.Nome) + "' AND NO = " + Str(cabDoc.no)
			
		Otherwise
			Return .F.
	ENDCASE  
			
	IF !uf_gerais_actGrelha("","ucrsDadosEntidades",lcSQL)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA A VERIFICAR DADOS DA ENTIDADE. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
	ENDIF 

	DADOSENTIDADE.refresh
ENDFUNC


**
FUNCTION uf_dadosentidade_gravar
	
	Select cabDoc
	Replace cabDoc.Nome	WITH ucrsDadosEntidades.Nome
	Replace cabDoc.Morada WITH ucrsDadosEntidades.Morada
	Replace cabDoc.Local WITH ucrsDadosEntidades.Local
	Replace cabDoc.codpost With	ucrsDadosEntidades.codpost
	Replace cabDoc.ncont WITH ucrsDadosEntidades.ncont
	REPLACE cabdoc.telefone WITH ucrsDadosEntidades.telefone
	REPLACE cabdoc.ccusto WITH ucrsDadosEntidades.ccusto
	
	uf_perguntalt_chama("DADOS ACTUALIZADOS COM SUCESSO.","OK","",64)
	uf_dadosentidade_sair()
ENDFUNC 


**
FUNCTION uf_dadosentidade_sair
	**Fecha Cursores
	DADOSENTIDADE.hide
	DADOSENTIDADE.release
	
	RELEASE DADOSENTIDADE
ENDFUNC


**
FUNCTION uf_dadosentidade_CarregaMenu
	PUBLIC mydocAlteracao, mydocIntroducao
	
	DADOSENTIDADE.menu1.adicionaOpcao("rechamar","RE-CHAMAR OS DADOS",myPath + "\imagens\icons\actualizar_w.png","uf_dadosentidade_rechamarDados","R")
	DADOSENTIDADE.menu1.estado("", "SHOW", "Actualizar", .t., "Sair", .t.)
	DADOSENTIDADE.refresh
ENDFUNC
