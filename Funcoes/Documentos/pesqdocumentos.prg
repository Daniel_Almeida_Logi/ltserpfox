**
FUNCTION uf_pesqDocumentos_Chama
	LPARAMETERS lcOrigem, lcNome
	
	PUBLIC lcorigempesq
	STORE '' TO lcorigempesq
	
	** Controle de Permiss�es	
	IF !uf_gerais_validaPermUser(ch_userno, ch_grupo, 'Gest�o de Documentos - Fornecedores')
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ACEDER AO PAINEL Gest�o de Documentos - Fornecedores.","OK","",48)
		RETURN .f.
	ENDIF

	
	IF !USED("uCrsPesqDocs")
		uf_pesqDocumentos_actPesquisa(.t.)
	ELSE
		UPDATE ucrsPesqDocs SET ucrsPesqDocs.escolha = 0, ucrsPesqDocs.sel = .f.
		SELECT ucrsPesqDocs
		GO TOP
	ENDIF
	
	IF TYPE("PESQDOCUMENTOS")=="U"
		DO FORM PESQDOCUMENTOS WITH lcOrigem, lcNome
		PESQDOCUMENTOS.show
		IF !EMPTY(lcOrigem)
			IF lcOrigem = "PCENTRAL"
				uf_pesqDocumentos_actPesquisa(.F.)
			ENDIF 
		ENDIF 
	ELSE
		PESQDOCUMENTOS.show
	ENDIF

    uf_pesqDocumentos_refreshDatEntrega()

ENDFUNC 


** tcBool = caso venha a .t. cria apenas o cursor
FUNCTION uf_pesqDocumentos_actPesquisa
	LPARAMETERS tcBool
	LOCAL lcTop 
	
	LOCAL lcSQL, lcDataIni, lcDataFim
	STORE "" TO lcSql, lcDataIni, lcDataFim
	
	IF tcBool
		lcSQL = ""
		TEXT TO lcSql NOSHOW TEXTMERGE
			exec up_Documentos_pesquisarDocumentos_fornecedores 30, '', '', 0, -1,  '19000101', '19000101', '', 0, '', '','','',-1,''
		ENDTEXT
		
		if type("PESQDOCUMENTOS")=="U"
			IF !uf_gerais_actGrelha("",[ucrsPesqDocs],lcSql)
				uf_perguntalt_chama("N�O FOI POSSIVEL ACTUALIZAR A LISTAGEM DE DOCUMENTOS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				RETURN .f.
			ENDIF
		ENDIF
		
	ELSE &&Refresca valores da Grid
	
		regua(0,0,"A processar as Listagem de Documentos...")
		
		**Trata parametros
		lcTop = IIF(INT(VAL(PESQDOCUMENTOS.ultimos.value))== 0,100000,INT(VAL(PESQDOCUMENTOS.ultimos.value)))
					
		If !empty(PESQDOCUMENTOS.DataIni.value)
			lcDataIni = uf_gerais_getdate(PESQDOCUMENTOS.dataIni.value,"SQL")
		Else
			lcDataIni ='19000101'
		EndIf

		If !empty(PESQDOCUMENTOS.DataFim.value)
			lcDataFim = uf_gerais_getdate(PESQDOCUMENTOS.DataFim.value,"SQL")
		Else
			If !empty(PESQDOCUMENTOS.DataIni.value)
				lcDataFim = lcDataIni
			Else
				lcDataFim='30001231'
			EndIf
		ENDIF
		
		lcSQL = ""
		TEXT TO lcSql NOSHOW TEXTMERGE 
			exec up_Documentos_pesquisarDocumentos_fornecedores <<lcTop>>, '<<Alltrim(PESQDOCUMENTOS.NOME.VALUE)>>', '<<Alltrim(PESQDOCUMENTOS.NUMDOC.VALUE)>>',
					<<IIF(EMPTY(Alltrim(PESQDOCUMENTOS.NO.VALUE)),0,Alltrim(PESQDOCUMENTOS.NO.VALUE))>>, <<IIF(EMPTY(Alltrim(PESQDOCUMENTOS.ESTAB.VALUE)),-1,Alltrim(PESQDOCUMENTOS.ESTAB.VALUE))>>,  '<<lcDataIni>>',  '<<lcDataFim>>',
					'<<Alltrim(PESQDOCUMENTOS.documento.Value)>>', <<ch_userno>>,  '<<Alltrim(ch_grupo)>>', '<<Alltrim(PESQDOCUMENTOS.produto.VALUE)>>','<<Alltrim(PESQDOCUMENTOS.estado.VALUE)>>','<<Alltrim(PESQDOCUMENTOS.loja.VALUE)>>',-1,''
		ENDTEXT
		uf_gerais_actGrelha("PESQDOCUMENTOS.GridPesq", "ucrsPesqDocs",lcSQL)
      		
		regua(2)
		
		IF UPPER(myPaisConfSoftw) == 'ANGOLA'
			FOR i=1 TO PESQDOCUMENTOS.GridPesq.columnCount
				IF Upper(PESQDOCUMENTOS.GridPesq.Columns(i).ControlSource)=="UCRSPESQDOCS.DATA"
					PESQDOCUMENTOS.GridPesq.Columns(i).dynamicBackColor = "iIF(ALLTRIM(ucrspesqdocs.documento)=='Encomenda de Cliente' ,IIF(ALLTRIM(ucrspesqdocs.cor)=='R',rgb[255,0,0],IIF(ALLTRIM(ucrspesqdocs.cor)=='G',rgb[0,255,0],rgb[255,255,0])),rgb[255,255,255])"
				ENDIF 
			ENDFOR
		ENDIF 
		
		PESQDOCUMENTOS.GridPesq.Refresh

		PESQDOCUMENTOS.lblRegistos.caption = ALLTRIM(STR(RECCOUNT("ucrsPesqDocs"))) + " Resultados"
	ENDIF

    uf_pesqDocumentos_refreshDatEntrega()

ENDFUNC


**
FUNCTION uf_pesqDocumentos_sair
	IF type("PESQDOCUMENTOS")!="U"

        IF PESQDOCUMENTOS.origem == "IMPORTDOCASSOCIADO" AND pesqdocumentos.menu1.multiSel.tag = 'true'

            SELECT ucrsPesqDocs
            GO TOP

            SCAN FOR ucrsPesqDocs.sel           


				SELECT ucrsImportDocs
				Replace ucrsImportDocs.encomenda WITH ALLTRIM(ucrsImportDocs.encomenda) + IIF(EMPTY(ucrsImportDocs.encomenda), "", ",") + ALLTRIM(ucrsPesqDocs.Numdoc)

                IF NOT USED("uc_encAssociada")

                    CREATE CURSOR uc_encAssociada (importStamp c(25), encStamp c(25)) 

                ENDIF

                SELECT uc_encAssociada
                APPEND BLANK
                
                REPLACE uc_encAssociada.importStamp     WITH ucrsImportDocs.cabStamp,;
                        uc_encAssociada.encStamp        WITH ucrsPesqDocs.cabStamp


                SELECT ucrsPesqDocs
            ENDSCAN

        ENDIF

		PESQDOCUMENTOS.hide
		PESQDOCUMENTOS.release
	ENDIF 
ENDFUNC


**
FUNCTION uf_pesqDocumentos_CarregaMenu

	** Configura Menu barra Lateral
	WITH PESQDOCUMENTOS.MENU1
		**.adicionaopcao("aplicacoes", "Aplica��es" , mypath + "\imagens\icons\aplicacoes_w.png", "uf_gerais_menuAplicacoes With 'pesqdocumentos'","O" )
		.adicionaOpcao("opcoes", "Op��es", myPath + "\imagens\icons\opcoes_w.png", "uf_gerais_menuOpcoes With 'pesqdocumentos'","O")
		.adicionaOpcao("actualizar", "Pesquisar", myPath + "\imagens\icons\lupa_w.png", "uf_pesqDocumentos_actPesquisa with .F.","A")
		.adicionaOpcao("novo","Novo",myPath + "\imagens\icons\mais_w.png","uf_PesqDocumentos_novo","N")
				
		IF !uf_gerais_validaPermUser(ch_userno,ch_grupo,'Documentos - Permite fechar de seguida') == .t.
			.adicionaOpcao("multiSel", "Multi Sel.", myPath + "\imagens\icons\unchecked_w.png", "uf_pesqdocs_multiSel","")
			.adicionaOpcao("seltodos", "Sel. Todos", myPath + "\imagens\icons\unchecked_w.png", "uf_pesqdocs_SelTodos","")
		ENDIF 
        IF PESQDOCUMENTOS.origem == "IMPORTDOCASSOCIADO" AND TYPE("pesqdocumentos.menu1.multiSel") = "U"
			.adicionaOpcao("multiSel", "Multi Sel.", myPath + "\imagens\icons\unchecked_w.png", "uf_pesqdocs_multiSel","")
			.adicionaOpcao("seltodos", "Sel. Todos", myPath + "\imagens\icons\unchecked_w.png", "uf_pesqdocs_SelTodos","")
        ENDIF
		.adicionaOpcao("subir", "Subir", myPath + "\imagens\icons\pg_up_b.png", "uf_gerais_MovePage with .t.,'pesqDocumentos.gridpesq','ucrsPesqDocs'","05")
		.adicionaOpcao("descer", "Descer", myPath + "\imagens\icons\pg_down_b.png", "uf_gerais_MovePage with .f.,'pesqDocumentos.gridpesq','ucrsPesqDocs'","24")	
		**.adicionaOpcao("processar", "Processar", myPath + "\imagens\icons\doc_fact_w.png", "uf_pesqDocumentos_processar with ucrsPesqDocs.cabstamp","F")	
	ENDWITH 

	** Configura Menu Aplica��es
**	WITH PESQDOCUMENTOS.menu_aplicacoes
**		.adicionaOpcao("prepararEnc","Preparar Encomenda","","uf_prepararEncomenda_chama")	
**		.adicionaOpcao("conferenciaFact","Confer�ncia Facturas","","uf_conffact_chama with 'DOC'")
**		.adicionaOpcao("pagamentos", "Pagamentos", "", "uf_forn_pagamentos")
**		.adicionaOpcao("arquivodigital", "Arquivo Digital", "", "uf_arquivodigital_chama")
**	ENDWITH 
	
	** Configura Menu op��es
	WITH PESQDOCUMENTOS.menu_opcoes
		.adicionaOpcao("Imp", "Imprimir Dados", myPath + "\imagens\icons\imprimir_w.png", "uf_PesqDocumentos_imprimir")
		IF !uf_gerais_validaPermUser(ch_userno,ch_grupo,'Documentos - Permite fechar de seguida') == .t.
			.adicionaOpcao("Fech", "Fechar documentos", myPath + "\imagens\icons\imprimir_w.png", "uf_PesqDocumentos_fechar")
		ENDIF 
	&&  .adicionaOpcao("tecladoVirtual", "Teclado", myPath + "\imagens\icons\teclado_w.png", "uf_uf_PesqDocumentos_tecladoVirtual","T")
	
		IF uf_gerais_getParameter("ADM0000000213","BOOL") == .t. AND uf_gerais_getParameter("ADM0000000214","BOOL") == .f.
				.adicionaOpcao("evovendas","Importar Evovendas","","uf_evovendas_chama")
		ENDIF

		.adicionaOpcao("prepararEnc","Preparar Encomenda","","uf_prepararEncomenda_chama")	
		.adicionaOpcao("conferenciaFact","Confer�ncia Facturas","","uf_conffact_chama with 'DOC'")
		.adicionaOpcao("pagamentos", "Pagamentos", "", "uf_forn_pagamentos")
		.adicionaOpcao("arquivodigital", "Arquivo Digital", "", "uf_arquivodigital_chama")
		.adicionaOpcao("editSeries", "Editar S�ries", "", "uf_editSeries_chama WITH 'TS'")

	ENDWITH 
	
	IF !uf_gerais_validaPermUser(ch_userno,ch_grupo,'Documentos - Permite fechar de seguida') == .t.
		PESQDOCUMENTOS.menu1.estado("seltodos", "HIDE")
	ENDIF 
	
	IF UPPER(myPaisConfSoftw) == 'ANGOLA'
		PESQDOCUMENTOS.GridPesq.Envio.visible=.t.
		PESQDOCUMENTOS.GridPesq.Info.visible=.t.
		PESQDOCUMENTOS.GridPesq.codext.visible=.t.
		PESQDOCUMENTOS.GridPesq.Status.visible=.t.
		PESQDOCUMENTOS.GridPesq.Pagamento.visible=.t.
	ELSE
		PESQDOCUMENTOS.GridPesq.Envio.visible=.f.
		PESQDOCUMENTOS.GridPesq.Info.visible=.f.
		PESQDOCUMENTOS.GridPesq.codext.visible=.f.
		PESQDOCUMENTOS.GridPesq.Status.visible=.f.
		PESQDOCUMENTOS.GridPesq.Pagamento.visible=.f.
	ENDIF 
	
	PESQDOCUMENTOS.refresh
ENDFUNC


**
FUNCTION uf_PesqDocumentos_novo
	IF !uf_gerais_validaPermUser(ch_userno, ch_grupo, 'Documentos Fornecedores - Criar')
		uf_perguntalt_chama("O seu perfil n�o permite Criar Documentos Fornecedor.","OK","",32)
		Return .f.
	ENDIF
	uf_documentos_chama('')
	uf_documentos_novodoc(.f., 'FORNECEDORES')
	uf_PesqDocumentos_sair()
ENDFUNC 


FUNCTION uf_PesqDocumentos_novo_escolhe
	LPARAMETERS tcDocumento
	uf_documentos_chama('')
	IF ALLTRIM(tcDocumento)='RECECAO'
		uf_documentos_novodoc()
		uf_documentos_chamaImportacao_enc()
	ELSE 
		uf_documentos_novoDoc_escolhe(tcDocumento)
	ENDIF 
	uf_PesqDocumentos_sair()
ENDFUNC 

**
FUNCTION uf_pesqDocumentos_Escolhe

	IF EMPTY(PESQDOCUMENTOS.origem)
		SELECT ucrsPesqDocs
		uf_documentos_Chama(ALLTRIM(ucrsPesqDocs.cabstamp))
		uf_pesqDocumentos_sair()
	ELSE
		DO CASE 
			CASE PESQDOCUMENTOS.origem == "IMPORTDOCASSOCIADO"
				
				SELECT ucrsImportDocs
				Replace ucrsImportDocs.encomenda WITH ucrsPesqDocs.Numdoc

                IF NOT USED("uc_encAssociada")

                    CREATE CURSOR uc_encAssociada (importStamp c(25), encStamp c(25)) 

                ENDIF

                SELECT uc_encAssociada
                APPEND BLANK
                
                REPLACE uc_encAssociada.importStamp     WITH ucrsImportDocs.cabStamp,;
                        uc_encAssociada.encStamp        WITH ucrsPesqDocs.cabStamp
				
				uf_pesqDocumentos_sair()
            CASE ALLTRIM(UPPER(PESQDOCUMENTOS.origem)) = "VFATURARESUMO"

                uf_pesqDocumentos_gravar()

		ENDCASE 
	
	ENDIF 
ENDFUNC


** 
FUNCTION uf_pesqDocumentos_gravar
	DO CASE 
		CASE PESQDOCUMENTOS.origem = "VFATURARESUMO"
			uf_pagForn_importaMovimentosResumo()
		OTHERWISE 
			**
	ENDCASE 
ENDFUNC 

**
FUNCTION uf_PesqDocumentos_imprimir

 	DO CASE
		CASE !TYPE("PESQDOCUMENTOS") == "U" 
			IF !uf_gerais_validaPermUser(ch_userno, ch_grupo,'Relat�rios - Painel')
				uf_perguntalt_chama("O SEU PERFIL N�O TEM PERMISS�ES PARA ACEDER A ESTA FUNCIONALIDADE.","OK","",48)
				RETURN .f.
			ENDIF
	ENDCASE

	IF RECCOUNT("ucrsPesqDocs") > 0
		
		**Trata parametros
		If Empty(PESQDOCUMENTOS.ultimos.Value)
			PESQDOCUMENTOS.ultimos.Value = 30
		ENDIF

		lcDataIni = PESQDOCUMENTOS.dataIni.value

		lcDataFim = PESQDOCUMENTOS.DataFim.value
		
		
		If !empty(PESQDOCUMENTOS.DataIni.value)
			lcDataIni = PESQDOCUMENTOS.dataIni.value
		Else
			lcDataIni ='1900.01.01'
		EndIf
		If !empty(PESQDOCUMENTOS.DataFim.value)
			lcDataFim = PESQDOCUMENTOS.DataFim.value
		Else
			If !empty(PESQDOCUMENTOS.DataIni.value)
				lcDataFim = lcDataIni
			Else
				lcDataFim='3000.12.31'
			EndIf
		ENDIF

		parametros  = ''
		TEXT TO parametros TEXTMERGE NOSHOW
			&topo=<<PESQDOCUMENTOS.ultimos.Value>>&entidade=<<uf_gerais_trataCarateresReservadosURL(Alltrim(PESQDOCUMENTOS.NOME.VALUE))>>&numdoc=<<Alltrim(PESQDOCUMENTOS.NUMDOC.VALUE)>>&no=<<IIF(EMPTY(Alltrim(PESQDOCUMENTOS.NO.VALUE)),0,Alltrim(PESQDOCUMENTOS.NO.VALUE))>>&estab=<<IIF(EMPTY(Alltrim(PESQDOCUMENTOS.ESTAB.VALUE)),-1,Alltrim(PESQDOCUMENTOS.ESTAB.VALUE))>>&dataIni=<<uf_gerais_getdate(lcDataIni, "DATA")>>&dataFim=<<uf_gerais_getdate(lcDataFim, "DATA")>>&doc=<<Alltrim(PESQDOCUMENTOS.documento.Value)>>&user=<<ch_userno>>&group=<<Alltrim(ch_grupo)>>&design=<<Alltrim(PESQDOCUMENTOS.produto.VALUE)>>&fechada=<<ALLTRIM(Alltrim(PESQDOCUMENTOS.estado.VALUE))>>&site=<<ALLTRIM(Alltrim(PESQDOCUMENTOS.loja.VALUE))>>&exportado=-1&designProd=&docNew=01.01.1900&docUpdate=01.01.1900
		ENDTEXT 
		
		SELECT ucrsPesqDocs
		GO TOP 
		
		IF RECCOUNT("ucrsPesqDocs") > 0
			uf_gerais_chamaReport("listagem_pesquisa_documentos", parametros)	
		ELSE
			uf_perguntalt_chama("N�o existem dados para Impress�o. Por favor verifique. Obrigado.","OK","",16)
		ENDIF

	ENDIF 
ENDFUNC 


**
FUNCTION uf_PESQDOCUMENTOS_seltodos
	LOCAL lcPos
		
		SELECT ucrsPesqDocs
		lcPos = RECNO("ucrsPesqDocs")
		
	IF PESQDOCUMENTOS.menu1.selTodos.lbl.caption == "Sel. Todos"
		SELECT ucrsPesqDocs
		replace ALL ucrsPesqDocs.sel WITH .t.

		PESQDOCUMENTOS.menu1.selTodos.config("De-Sel. Todos", myPath + "\imagens\icons\checked_w.png", "uf_PESQDOCUMENTOS_seltodos")
	ELSE	
		SELECT ucrsPesqDocs
		replace ALL ucrsPesqDocs.sel WITH .f.

		PESQDOCUMENTOS.menu1.selTodos.config("Sel. Todos", myPath + "\imagens\icons\unchecked_w.png", "uf_PESQDOCUMENTOS_seltodos")
	ENDIF
	
	SELECT ucrsPesqDocs
	TRY
		GO lcPos
	CATCH
	ENDTRY

ENDFUNC 

FUNCTION uf_pesqdocs_multiSel
	IF pesqdocumentos.menu1.multiSel.tag == 'false'
	   pesqdocumentos.menu1.multiSel.tag = 'true'
		
	   && altera o botao
	   pesqdocumentos.menu1.multiSel.config("Multi Sel.", myPath + "\imagens\icons\checked_w.png", " uf_pesqdocs_multiSel","")
	   PESQDOCUMENTOS.menu1.estado("seltodos", "SHOW")

	ELSE
	   && aplica sele��o
	   pesqdocumentos.menu1.multiSel.tag = 'false'
		
	   && altera o botao
       pesqdocumentos.menu1.multiSel.config("Multi Sel.", myPath + "\imagens\icons\unchecked_w.png", " uf_pesqdocs_multiSel","")
		PESQDOCUMENTOS.menu1.estado("seltodos", "HIDE")
	ENDIF
ENDFUNC 


FUNCTION uf_PesqDocumentos_fechar
	IF uf_perguntalt_chama("DESEJA FECHAR TODOS OS DOCUMENTOS SELECIONADOS?","Sim","N�o", 32)
		SELECT ucrsPesqDocs
		GO TOP 
		SCAN FOR ucrsPesqDocs.sel=.t.
            IF ALLTRIM(ucrsPesqDocs.tipodoc) == 'BO' AND (ALLTRIM(ucrsPesqDocs.documento) == 'Encomenda a Fornecedor' OR ALLTRIM(ucrsPesqDocs.documento) == 'Encomenda Via Verde')

                IF uf_gerais_getUmValor("bo","ndos","bostamp = '" + ucrsPesqDocs.cabstamp + "'") = 2

                    TEXT TO msel TEXTMERGE NOSHOW

                        SELECT DISTINCT
                            ref, armazem, sum(qtt) as totqtt
                        FROM
                            bi(nolock)
                        WHERE
                            bostamp = '<<ALLTRIM(ucrsPesqDocs.cabstamp)>>'
                            and (select stns from st(nolock) where bi.ref = st.ref and st.site_nr = <<ASTR(mySite_Nr)>>) = 0
                        group by 
                            ref, armazem

                    ENDTEXT

                    uf_gerais_actGrelha("", "uc_updateST",msel)

                    select uc_updateST
                    GO TOP

                    SCAN FOR uc_updateST.totqtt <> 0

                        **IF !uf_gerais_actGrelha("", "","update st set qttfor = qttfor - " + ASTR(uc_updateST.totqtt) + " where ref = '" + ALLTRIM(uc_updateST.ref) + "' and site_nr = " + ASTR(mySite_Nr))  
                            **   uf_perguntalt_chama("OCORREU UM PROBLEMA A FECHAR O DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.","OK","",48)
                        **ENDIF
                        
                        IF !EMPTY(uc_updateST.ref) 
                            lcSQL = ""
                            TEXT TO lcSQL TEXTMERGE NOSHOW
                                exec up_stocks_Corrige_enc_pend_cl <<uc_updateST.armazem>>, '<<ALLTRIM(uc_updateST.ref)>>'
                            ENDTEXT 
                            uf_gerais_actGrelha("", "", lcSQL)
                        ENDIF 

                        select uc_updateST
                    ENDSCAN

                    FECHA("uc_updateST")

                ENDIF


                lcSQL = ""
                TEXT TO lcSql NOSHOW TEXTMERGE 
                    UPDATE bo SET fechada=1 WHERE bostamp='<<ALLTRIM(ucrsPesqDocs.cabstamp)>>'	
                    UPDATE bi SET fechada=1 WHERE bostamp='<<ALLTRIM(ucrsPesqDocs.cabstamp)>>'	
                ENDTEXT
                uf_gerais_actGrelha("", "",lcSQL)
            ELSE
                IF ALLTRIM(ucrsPesqDocs.tipodoc) == 'BO'
                    lcSQL = ""
                    TEXT TO lcSql NOSHOW TEXTMERGE 
                        UPDATE bo SET fechada=1 WHERE bostamp='<<ALLTRIM(ucrsPesqDocs.cabstamp)>>'	
                        UPDATE bi SET fechada=1 WHERE bostamp='<<ALLTRIM(ucrsPesqDocs.cabstamp)>>'	
                    ENDTEXT
                    uf_gerais_actGrelha("", "",lcSQL)
                ELSE
                    lcSQL = ""
                    TEXT TO lcSql NOSHOW TEXTMERGE 
                         UPDATE fo SET u_status='F' WHERE fostamp='<<ALLTRIM(ucrsPesqDocs.cabstamp)>>'	
                    ENDTEXT
                    uf_gerais_actGrelha("", "",lcSQL)
                ENDIF
            ENDIF 
            SELECT ucrsPesqDocs
            
            ** registar ocorrencia **
            LOCAL lcStamp
            lcStamp = uf_gerais_stamp()
            lcSQL = ''
            TEXT TO lcSql NOSHOW textmerge
                INSERT INTO B_ocorrencias(
                    stamp, tipo, grau, descr,
                    oValor, dValor,
                    usr, date)
                values
                    ('<<lcStamp>>', 'Fecho manual documentos', 3, 'Doc. <<ALLTRIM(ucrsPesqDocs.documento)>> <<ALLTRIM(ucrsPesqDocs.numdoc)>>',
                    '', '',
                    <<ch_userno>>, dateadd(HOUR, <<difhoraria>>, getdate()))
            ENDTEXT
            IF !uf_gerais_actgrelha("", "", lcSql)
                uf_perguntalt_chama("OCORREU UM PROBLEMA A REGISTAR OCORR�NCIA. POR FAVOR CONTACTE O SUPORTE.","OK","",48)
            ENDIF
		ENDSCAN 
		SELECT ucrsPesqDocs
		GO TOP 
		IF !uf_gerais_validaPermUser(ch_userno,ch_grupo,'Documentos - Permite fechar de seguida') == .t.
			pesqdocumentos.menu1.multiSel.tag = 'false'
			pesqdocumentos.menu1.multiSel.config("Multi Sel.", myPath + "\imagens\icons\unchecked_w.png", " uf_pesqdocs_multiSel","")
		ENDIF 
		uf_pesqDocumentos_actPesquisa()
	ELSE
		uf_perguntalt_chama("OPERA��O CANCELADA.","OK","", 64)
	ENDIF 
ENDFUNC 

FUNCTION uf_pesqdocs_SelTodos
	
	IF EMPTY(PESQDOCUMENTOS.menu1.seltodos.tag) OR PESQDOCUMENTOS.menu1.seltodos.tag == "false"
		&& aplica sele��o
		REPLACE ALL sel WITH .t. IN ucrsPesqDocs
		
		&& altera o botao
		PESQDOCUMENTOS.menu1.selTodos.tag = "true"
		PESQDOCUMENTOS.menu1.selTodos.config("Sel. Todos", myPath + "\imagens\icons\checked_w.png", "uf_pesqdocs_SelTodos","T")
		
		&& actualiza o ecra
		*pesqstocks.refresh()
	ELSE
		&& aplica sele��o
		REPLACE ALL sel WITH .f. IN ucrsPesqDocs
		
		&& altera o botao
		PESQDOCUMENTOS.menu1.selTodos.tag = "false"
		PESQDOCUMENTOS.menu1.selTodos.config("Sel. Todos", myPath + "\imagens\icons\unchecked_w.png", "uf_pesqdocs_SelTodos","T")
		
		&& actualiza o ecra		
		*pesqstocks.refresh()
	ENDIF
	
	uf_encautomaticas_ActualizaQtValorEnc()
		
	SELECT ucrsPesqDocs
	GO TOP 
	
	PESQDOCUMENTOS.GridPesq.Refresh
ENDFUNC 

FUNCTION uf_pesqDocumentos_processar

	LPARAMETERS LcStampProc

	IF ALLTRIM(ucrsPesqDocscl.documento)<>'Encomenda de Cliente'
		uf_perguntalt_chama("ESTA FUNCIONALIDADE EST� RESTRITA �S ENCOMENDAS DE CLIENTE.","OK","",48)
	ELSE
		LOCAL lcNmdos, LcObrano, LcDataobra, LcCodext
		Text To lcSQL Textmerge noshow
			select status, bo.nmdos, bo.obrano, CONVERT(varchar, bo.dataobra, 104) as dataobra, codext,pagamento from bo2 (nolock)
			inner join bo (nolock) on bo.bostamp=bo2.bo2stamp 
			where bo2.bo2stamp='<<ALLTRIM(LcStampProc)>>'
		ENDTEXT
		uf_gerais_actGrelha("","uCrsstatus",lcSQL)
		lcNmdos = ALLTRIM(uCrsstatus.nmdos)
		LcObrano = uCrsstatus.obrano
		LcDataobra = uCrsstatus.dataobra
		LcCodext = ALLTRIM(codext )
		DO CASE 
			CASE (UPPER(ALLTRIM(uCrsstatus.status))=='PENDENTE' OR UPPER(ALLTRIM(uCrsstatus.status))=='EM PROCESSAMENTO') AND UPPER(ALLTRIM(uCrsstatus.pagamento))=='MULTIBANCO'
				uf_facturacao_chama('')
				uf_facturacao_novo('FACTURA')
				Text To lcSQL Textmerge noshow
					select * from td (nolock) where nmdoc='Fatura Recibo' and site='<<mysite>>'
				ENDTEXT
				uf_gerais_actGrelha("","uCrsNmdocVD",lcSQL)
				IF RECCOUNT("uCrsNmdocVD")>0
					Facturacao.containercab.nmdoc.VALUE='Fatura Recibo'
				ELSE
					Facturacao.containercab.nmdoc.VALUE='Venda a Dinheiro'
				ENDIF 
				fecha("uCrsNmdocVD")
			CASE (UPPER(ALLTRIM(uCrsstatus.status))=='PENDENTE' OR UPPER(ALLTRIM(uCrsstatus.status))=='EM PROCESSAMENTO') AND UPPER(ALLTRIM(uCrsstatus.pagamento))=='PAG.ENTREGA'
				uf_facturacao_chama('')
				uf_facturacao_novo('FACTURA')
				Facturacao.containercab.nmdoc.VALUE='Factura'
			OTHERWISE
				uf_perguntalt_chama("N�O PODE SER EMITIDO UM DOCUMENTO DE FATURA��O APARTIR DESTE DOCUMENTO.","OK","",48)
				RETURN .f.
		ENDCASE  
		fecha("uCrsstatus")
		uf_facturacao_limpaNomeFact()
		uf_facturacao_valoresDefeitoFact()
		uf_facturacao_controlaNumeroFact(year(datetime()+(difhoraria*3600)))
		uf_atendimento_fiiliq() &&actualiza totais das linhas
		uf_atendimento_actTotaisFt() &&actualiza totais do cabe�alho
		uf_facturacao_criaLinhaDefault()
		uf_atendimento_selCliente(ucrsPesqDocs.no, ucrsPesqDocs.estab,"FACTURACAO")
		LcSql = ""
		Text To lcSQL Textmerge noshow
			exec up_perfis_validaAcessoDocs <<ch_userno>>,'<<ch_grupo>>','visualizar','FT', 0, '<<ALLTRIM(mysite)>>'
		ENDTEXT
		uf_gerais_actGrelha("","uCrsListaDocs",lcSQL)
		
		Select FI
		Append Blank
		REPLACE Fi.LORDEM	With	10
		IF EMPTY(LcCodext)
			REPLACE Fi.design WITH ALLTRIM(lcNmdos) + ' Nr. ' + ALLTRIM(STR(LcObrano)) + ' de ' + LcDataobra 
		ELSE 
			REPLACE Fi.design WITH ALLTRIM(lcNmdos) + ' Nr. ' + ALLTRIM(LcCodext) + ' de ' + LcDataobra 
		ENDIF 

		*VALORES POR DEFEITO
		** FISTAMP
		lcStamp = uf_gerais_stamp()

		SELECT FI
		
		REPLACE FI.FISTAMP 	With	lcStamp
		REPLACE FI.NMDOC 	With	Alltrim(Ft.Nmdoc)
		Replace Fi.ndoc		With	Ft.ndoc
		REPLACE FI.FNO 		With	Ft.Fno

		**IVA
		REPLACE FI.IVA With uf_gerais_getTabelaIVA(1, "TAXA")
		REPLACE FI.TABIVA With 1
		
		**ARMAZEM (Variavel publica criada no startup)
		REPLACE FI.ARMAZEM With myarmazem
		
		uf_importarFact_crialinhas_compBOFT(ALLTRIM(LcStampProc))
		
		SELECT fi
		GO TOP
		SCAN

			uf_atendimento_fiiliq(.t.)
			
			IF uf_gerais_getParameter("ADM0000000211","BOOL") == .f.
				uf_atendimento_actValoresFi()
			ENDIF
		
			SELECT fi
		ENDSCAN


		**					
		IF uf_gerais_getParameter("ADM0000000211","BOOL") == .t. && Atribui automaticamente os Lotes
			UF_FACTURACAO_AtribuiLotesDocumento()
		ENDIF
		uf_atendimento_actValoresFi()
		uf_atendimento_actTotaisFt()
		
		SELECT fi
		GO TOP
	
	ENDIF 
		
ENDFUNC 


PROCEDURE uf_pesqDocumentos_refreshDatEntrega

    IF USED("ucrspesqdocs") AND WEXIST("PESQDOCUMENTOS")

        SELECT ucrspesqdocs

        WITH PESQDOCUMENTOS.GridPesq
        
            FOR i=1 TO .columnCount
                DO CASE					
                    CASE ALLTRIM(Upper(.Columns(i).name))=="U_DATAENTR"
                        .Columns(i).dynamicBackColor = "IIF(DTOC(ucrspesqdocs.u_dataentr, '1') < DTOC(DATETIME(), '1') AND DTOC(ucrspesqdocs.u_dataentr, '1') <> '19000101',RGB(255,0,0), RGB(255,255,255))"
                        
                    CASE ALLTRIM(UPPER(.Columns(i).name)) == "DATAENTRB2B"	
                        .Columns(i).dynamicBackColor = "IIF(EMPTY(ucrsPesqDocs.dataEntrB2B), RGB(255,255,255), IIF(CTOD(ucrsPesqDocs.dataEntrB2B) >= DATE(), RGB(255,255,255), RGB(255,0,0)))"
                ENDCASE

            ENDFOR

        ENDWITH

    ENDIF

ENDPROC

FUNCTION uf_pesqDocumentos_scanner

    uv_scan = ALLTRIM(PESQDOCUMENTOS.txtScanner.value)

    IF EMPTY(uv_scan)
        PESQDOCUMENTOS.numDoc.setFocus()
        RETURN .F.
    ENDIF

    IF !EMPTY(uv_scan) AND AT(" ", ALLTRIM(uv_scan)) = 0 AND LEN(ALLTRIM(uv_scan)) > 20
        uv_ref = uf_gerais_getRefFromQR(ALLTRIM(uv_scan))

        IF !EMPTY(uv_ref)

            PESQDOCUMENTOS.produto.setFocus()
            PESQDOCUMENTOS.produto.value = ALLTRIM(uv_ref)
            PESQDOCUMENTOS.menu1.actualizar.click			

        ENDIF
    ENDIF

ENDFUNC