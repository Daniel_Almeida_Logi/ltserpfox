**
FUNCTION uf_pesqdocumentosclientes_Chama
	LPARAMETERS lcOrigem, lcNome
	
	PUBLIC lcorigempesq
	STORE '' TO lcorigempesq
	
	** Controle de Permiss�es	
	IF !uf_gerais_validaPermUser(ch_userno, ch_grupo, 'Gest�o de Documentos - Clientes')
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ACEDER AO PAINEL Gest�o de Documentos - Clientes","OK","",48)
		RETURN .f.
	ENDIF
	
	IF !USED("uCrsPesqDocsCL")
		uf_pesqdocumentosclientes_actPesquisa(.t.)
	ELSE
		UPDATE ucrsPesqDocsCL SET ucrsPesqDocsCL.escolha = 0, ucrsPesqDocsCL.sel = .f.
		SELECT ucrsPesqDocsCL
		GO TOP
	ENDIF
	
	IF TYPE("PESQDOCUMENTOSCLIENTES")=="U"
		DO FORM PESQDOCUMENTOSCLIENTES WITH lcOrigem, lcNome
		PESQDOCUMENTOSCLIENTES.show
		IF !EMPTY(lcOrigem)
			IF lcOrigem = "PCENTRAL"
				uf_pesqdocumentosclientes_actPesquisa(.F.)
			ENDIF 
		ENDIF 
	ELSE
		PESQDOCUMENTOSCLIENTES.show
	ENDIF
ENDFUNC 


** tcBool = caso venha a .t. cria apenas o cursor
FUNCTION uf_pesqdocumentosclientes_actPesquisa
	LPARAMETERS tcBool
	LOCAL lcTop 
	
	LOCAL lcSQL, lcDataIni, lcDataFim
	STORE "" TO lcSql, lcDataIni, lcDataFim
	
	IF tcBool
		lcSQL = ""
		TEXT TO lcSql NOSHOW TEXTMERGE
			exec up_Documentos_pesquisarDocumentos_clientes 30, '', '', 0, -1,  '19000101', '19000101', '', 0, '', '','','',-1,''
		ENDTEXT
		
		if type("PESQDOCUMENTOSCLIENTES")=="U"
			IF !uf_gerais_actGrelha("",[ucrsPesqDocscl],lcSql)
				uf_perguntalt_chama("N�O FOI POSSIVEL ACTUALIZAR A LISTAGEM DE DOCUMENTOS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				RETURN .f.
			ENDIF
		ENDIF
		
	ELSE &&Refresca valores da Grid
	
		regua(0,0,"A processar as Listagem de Documentos...")
		
		**Trata parametros
		lcTop = IIF(INT(VAL(PESQDOCUMENTOSCLIENTES.ultimos.value))== 0,100000,INT(VAL(PESQDOCUMENTOSCLIENTES.ultimos.value)))
					
		If !empty(PESQDOCUMENTOSCLIENTES.DataIni.value)
			lcDataIni = uf_gerais_getdate(PESQDOCUMENTOSCLIENTES.dataIni.value,"SQL")
		Else
			lcDataIni ='19000101'
		EndIf
		If !empty(PESQDOCUMENTOSCLIENTES.DataFim.value)
			lcDataFim = uf_gerais_getdate(PESQDOCUMENTOSCLIENTES.DataFim.value,"SQL")
		Else
			If !empty(PESQDOCUMENTOSCLIENTES.DataIni.value)
				lcDataFim = lcDataIni
			Else
				lcDataFim='30001231'
			EndIf
		ENDIF
		
		lcSQL = ""
		TEXT TO lcSql NOSHOW TEXTMERGE 
			exec up_Documentos_pesquisarDocumentos_clientes <<lcTop>>, '<<Alltrim(PESQDOCUMENTOSCLIENTES.NOME.VALUE)>>', '<<Alltrim(PESQDOCUMENTOSCLIENTES.NUMDOC.VALUE)>>',
					<<IIF(EMPTY(Alltrim(PESQDOCUMENTOSCLIENTES.NO.VALUE)),0,Alltrim(PESQDOCUMENTOSCLIENTES.NO.VALUE))>>, <<IIF(EMPTY(Alltrim(PESQDOCUMENTOSCLIENTES.ESTAB.VALUE)),-1,Alltrim(PESQDOCUMENTOSCLIENTES.ESTAB.VALUE))>>,  '<<lcDataIni>>',  '<<lcDataFim>>',
					'<<Alltrim(PESQDOCUMENTOSCLIENTES.documento.Value)>>', <<ch_userno>>,  '<<Alltrim(ch_grupo)>>', '<<Alltrim(PESQDOCUMENTOSCLIENTES.produto.VALUE)>>','<<Alltrim(PESQDOCUMENTOSCLIENTES.estado.VALUE)>>','<<Alltrim(PESQDOCUMENTOSCLIENTES.loja.VALUE)>>',-1,''
		ENDTEXT
		
		uf_gerais_actGrelha("PESQDOCUMENTOSCLIENTES.GridPesq", "ucrsPesqDocscl",lcSQL)
		
		regua(2)

		WITH PESQDOCUMENTOSCLIENTES.GridPesq
		
			FOR i=1 TO .columnCount
				DO CASE
					CASE ALLTRIM(Upper(.Columns(i).ControlSource))=="UCRSPESQDOCSCL.DATA"
						.Columns(i).dynamicBackColor = "iIF(ALLTRIM(ucrsPesqDocsCL.documento)=='Encomenda de Cliente' ,IIF(ALLTRIM(ucrsPesqDocsCL.cor)=='R',rgb[255,0,0],IIF(ALLTRIM(ucrsPesqDocsCL.cor)=='G',rgb[0,255,0],rgb[255,255,0])),rgb[255,255,255])"
					
					CASE ALLTRIM(Upper(.Columns(i).name))=="U_DATAENTR"
						.Columns(i).dynamicBackColor = "IIF(DTOC(ucrspesqdocscl.u_dataentr, '1') < DTOC(DATETIME(), '1') AND DTOC(UCRSPESQDOCSCL.U_DATAENTR, '1') <> '19000101',RGB(255,0,0), RGB(255,255,255))"
				ENDCASE

			ENDFOR

		ENDWITH
		
		PESQDOCUMENTOSCLIENTES.GridPesq.Refresh

		PESQDOCUMENTOSCLIENTES.lblRegistos.caption = ALLTRIM(STR(RECCOUNT("ucrsPesqDocsCL"))) + " Resultados"
	ENDIF
ENDFUNC


**
FUNCTION uf_pesqdocumentosclientes_sair
	IF type("PESQDOCUMENTOSCLIENTES")!="U"
		PESQDOCUMENTOSCLIENTES.hide
		PESQDOCUMENTOSCLIENTES.release
	ENDIF 
ENDFUNC


**
FUNCTION uf_pesqdocumentosclientes_CarregaMenu

	** Configura Menu barra Lateral
	WITH PESQDOCUMENTOSCLIENTES.MENU1
		**.adicionaopcao("aplicacoes", "Aplica��es" , mypath + "\imagens\icons\aplicacoes_w.png", "uf_gerais_menuAplicacoes With 'pesqdocumentosclientes'","O" )
		.adicionaOpcao("opcoes", "Op��es", myPath + "\imagens\icons\opcoes_w.png", "uf_gerais_menuOpcoes With 'pesqdocumentos'","O")
		.adicionaOpcao("actualizar", "Pesquisar", myPath + "\imagens\icons\lupa_w.png", "uf_pesqdocumentosclientes_actPesquisa with .F.","A")
		.adicionaOpcao("novo","Novo",myPath + "\imagens\icons\mais_w.png","uf_pesqdocumentosclientes_novo","N")
		IF !uf_gerais_validaPermUser(ch_userno,ch_grupo,'Documentos - Permite fechar de seguida') == .t.
			.adicionaOpcao("multiSel", "Multi Sel.", myPath + "\imagens\icons\unchecked_w.png", "uf_pesqdocscl_multiSel","")
			.adicionaOpcao("seltodos", "Sel. Todos", myPath + "\imagens\icons\unchecked_w.png", "uf_pesqdocscl_SelTodos","")
		ENDIF 
		.adicionaOpcao("subir", "Subir", myPath + "\imagens\icons\pg_up_b.png", "uf_gerais_MovePage with .t.,'pesqdocumentosclientes.gridpesq','ucrsPesqDocsCL'","05")
		.adicionaOpcao("descer", "Descer", myPath + "\imagens\icons\pg_down_b.png", "uf_gerais_MovePage with .f.,'pesqdocumentosclientes.gridpesq','ucrsPesqDocsCL'","24")	
		.adicionaOpcao("processar", "Processar", myPath + "\imagens\icons\doc_fact_w.png", "uf_pesqdocumentosclientes_processar with ucrsPesqDocsCL.cabstamp","F")	
	ENDWITH 

		** Configura Menu Aplica��es
	**	WITH PESQDOCUMENTOSCLIENTES.menu_aplicacoes
*!*			.adicionaOpcao("prepararEnc","Preparar Encomenda","","uf_prepararEncomenda_chama")	
*!*			.adicionaOpcao("conferenciaFact","Confer�ncia Facturas","","uf_conffact_chama with 'DOC'")
*!*			.adicionaOpcao("pagamentos", "Pagamentos", "", "uf_forn_pagamentos")
			**PESQDOCUMENTOSCLIENTES.menu_aplicacoes.adicionaOpcao("arquivodigital", "Arquivo Digital", "", "uf_arquivodigital_chama")
	**	ENDWITH 
	
	** Configura Menu op��es
	WITH PESQDOCUMENTOSCLIENTES.menu_opcoes
		.adicionaOpcao("Imp", "Imprimir Dados", myPath + "\imagens\icons\imprimir_w.png", "uf_pesqdocumentosclientes_imprimir")
		IF !uf_gerais_validaPermUser(ch_userno,ch_grupo,'Documentos - Permite fechar de seguida') == .t.
			.adicionaOpcao("Fech", "Fechar documentos", myPath + "\imagens\icons\imprimir_w.png", "uf_pesqdocumentosclientes_fechar")
		ENDIF 
	&&  .adicionaOpcao("tecladoVirtual", "Teclado", myPath + "\imagens\icons\teclado_w.png", "uf_uf_PesqDocumentos_tecladoVirtual","T")
	
		IF uf_gerais_getParameter("ADM0000000213","BOOL") == .t. AND uf_gerais_getParameter("ADM0000000214","BOOL") == .f.
				.adicionaOpcao("evovendas","Importar Evovendas","","uf_evovendas_chama")
		ENDIF

        .adicionaOpcao("arquivodigital", "Arquivo Digital", "", "uf_arquivodigital_chama")

		IF uf_gerais_getparameter_site('ADM0000000199', 'BOOL', mysite)
		    .adicionaOpcao("sincEncCentral", "Sincronizar Encomendas", "", "uf_pesqdocumentosclientes_syncStatusEnc")
		ENDIF

		.adicionaOpcao("editSeries", "Editar S�ries", "", "uf_editSeries_chama WITH 'TS'")

	ENDWITH 
	
	pesqdocumentosclientes.menu1.estado("processar", "HIDE")
	
	IF !uf_gerais_validaPermUser(ch_userno,ch_grupo,'Documentos - Permite fechar de seguida') == .t.
		PESQDOCUMENTOSCLIENTES.menu1.estado("seltodos", "HIDE")
	ENDIF 
	
	**IF UPPER(myPaisConfSoftw) == 'ANGOLA'
		PESQDOCUMENTOSCLIENTES.GridPesq.Envio.visible=.t.
		PESQDOCUMENTOSCLIENTES.GridPesq.Info.visible=.t.
		PESQDOCUMENTOSCLIENTES.GridPesq.codext.visible=.t.
		PESQDOCUMENTOSCLIENTES.GridPesq.Status.visible=.t.
		PESQDOCUMENTOSCLIENTES.GridPesq.Pagamento.visible=.t.
	**ELSE
	**	PESQDOCUMENTOSCLIENTES.GridPesq.Envio.visible=.f.
	**	PESQDOCUMENTOSCLIENTES.GridPesq.Info.visible=.f.
	**	PESQDOCUMENTOSCLIENTES.GridPesq.codext.visible=.f.
	**	PESQDOCUMENTOSCLIENTES.GridPesq.Status.visible=.f.
	**	PESQDOCUMENTOSCLIENTES.GridPesq.Pagamento.visible=.f.
	**ENDIF 
	
	PESQDOCUMENTOSCLIENTES.refresh
ENDFUNC


**
FUNCTION uf_pesqdocumentosclientes_novo
	uf_documentos_chama('')
	uf_documentos_novodoc(.f., 'CLIENTES')
	uf_pesqdocumentosclientes_sair()
ENDFUNC 


FUNCTION uf_pesqdocumentosclientes_novo_escolhe
	LPARAMETERS tcDocumento
	uf_documentos_chama('')
	IF ALLTRIM(tcDocumento)='RECECAO'
		uf_documentos_novodoc()
		uf_documentos_chamaImportacao_enc()
	ELSE 
		uf_documentos_novoDoc_escolhe(tcDocumento)
	ENDIF 
	uf_pesqdocumentosclientes_sair()
ENDFUNC 

**
FUNCTION uf_pesqdocumentosclientes_Escolhe

	IF EMPTY(PESQDOCUMENTOSCLIENTES.origem)
		SELECT ucrsPesqDocsCL
		uf_documentos_Chama(ALLTRIM(ucrsPesqDocsCL.cabstamp))
		uf_pesqdocumentosclientes_sair()
	ELSE
		DO CASE 
			CASE PESQDOCUMENTOSCLIENTES.origem == "IMPORTDOCASSOCIADO"
				
				SELECT ucrsImportDocs
				Replace ucrsImportDocs.encomenda WITH ucrsPesqDocsCL.Numdoc
				
				uf_pesqdocumentosclientes_sair()
		ENDCASE 
	
	ENDIF 
ENDFUNC


** 
FUNCTION uf_pesqdocumentosclientes_gravar
	DO CASE 
		CASE PESQDOCUMENTOSCLIENTES.origem = "VFATURARESUMO"
			uf_pagForn_importaMovimentosResumo()
		OTHERWISE 
			**
	ENDCASE 
ENDFUNC 

**
FUNCTION uf_pesqdocumentosclientes_imprimir

 DO CASE
	CASE !TYPE("PESQDOCUMENTOSCLIENTES") == "U" 
		IF !uf_gerais_validaPermUser(ch_userno, ch_grupo,'Relat�rios - Painel')
			uf_perguntalt_chama("O SEU PERFIL N�O TEM PERMISS�ES PARA ACEDER A ESTA FUNCIONALIDADE.","OK","",48)
			RETURN .f.
		ENDIF
 ENDCASE

	IF RECCOUNT("ucrsPesqDocsCL") > 0
		
		**Trata parametros
		If Empty(PESQDOCUMENTOSCLIENTES.ultimos.Value)
			PESQDOCUMENTOSCLIENTES.ultimos.Value = 30
		ENDIF

		lcDataIni = PESQDOCUMENTOSCLIENTES.dataIni.value

		lcDataFim = PESQDOCUMENTOSCLIENTES.DataFim.value
		
		
		If !empty(PESQDOCUMENTOSCLIENTES.DataIni.value)
			lcDataIni = PESQDOCUMENTOSCLIENTES.dataIni.value
		Else
			lcDataIni ='1900.01.01'
		EndIf
		If !empty(PESQDOCUMENTOSCLIENTES.DataFim.value)
			lcDataFim = PESQDOCUMENTOSCLIENTES.DataFim.value
		Else
			If !empty(PESQDOCUMENTOSCLIENTES.DataIni.value)
				lcDataFim = lcDataIni
			Else
				lcDataFim='3000.12.31'
			EndIf
		ENDIF

		parametros  = ''
		TEXT TO parametros TEXTMERGE NOSHOW
			&topo=<<PESQDOCUMENTOSCLIENTES.ultimos.Value>>&entidade=<<uf_gerais_trataCarateresReservadosURL(Alltrim(PESQDOCUMENTOSCLIENTES.NOME.VALUE))>>&numdoc=<<Alltrim(PESQDOCUMENTOSCLIENTES.NUMDOC.VALUE)>>&no=<<IIF(EMPTY(Alltrim(PESQDOCUMENTOSCLIENTES.NO.VALUE)),0,Alltrim(PESQDOCUMENTOSCLIENTES.NO.VALUE))>>&estab=<<IIF(EMPTY(Alltrim(PESQDOCUMENTOSCLIENTES.ESTAB.VALUE)),-1,Alltrim(PESQDOCUMENTOSCLIENTES.ESTAB.VALUE))>>&dataIni=<<uf_gerais_getdate(lcDataIni, "DATA")>>&dataFim=<<uf_gerais_getdate(lcDataFim, "DATA")>>&doc=<<Alltrim(PESQDOCUMENTOSCLIENTES.documento.Value)>>&user=<<ch_userno>>&group=<<Alltrim(ch_grupo)>>&design=<<Alltrim(PESQDOCUMENTOSCLIENTES.produto.VALUE)>>&fechada=<<ALLTRIM(Alltrim(PESQDOCUMENTOSCLIENTES.estado.VALUE))>>&site=<<ALLTRIM(Alltrim(PESQDOCUMENTOSCLIENTES.loja.VALUE))>>&exportado=-1&designProd=&docNew=01.01.1900&docUpdate=01.01.1900
		ENDTEXT 
		
		SELECT ucrsPesqDocsCL
		GO TOP 
		
		IF RECCOUNT("ucrsPesqDocsCL") > 0
			uf_gerais_chamaReport("listagem_pesquisa_documentos", parametros)	
		ELSE
			uf_perguntalt_chama("N�o existem dados para Impress�o. Por favor verifique. Obrigado.","OK","",16)
		ENDIF

	ENDIF 
ENDFUNC 


**
FUNCTION uf_PESQDOCUMENTOSCLIENTES_seltodos
	LOCAL lcPos
		
		SELECT ucrsPesqDocsCL
		lcPos = RECNO("ucrsPesqDocsCL")
		
	IF PESQDOCUMENTOSCLIENTES.menu1.selTodos.lbl.caption == "Sel. Todos"
		SELECT ucrsPesqDocsCL
		replace ALL ucrsPesqDocsCL.sel WITH .t.

		PESQDOCUMENTOSCLIENTES.menu1.selTodos.config("De-Sel. Todos", myPath + "\imagens\icons\checked_w.png", "uf_PESQDOCUMENTOSCLIENTES_seltodos")
	ELSE	
		SELECT ucrsPesqDocsCL
		replace ALL ucrsPesqDocsCL.sel WITH .f.

		PESQDOCUMENTOSCLIENTES.menu1.selTodos.config("Sel. Todos", myPath + "\imagens\icons\unchecked_w.png", "uf_PESQDOCUMENTOSCLIENTES_seltodos")
	ENDIF
	
	SELECT ucrsPesqDocsCL
	TRY
		GO lcPos
	CATCH
	ENDTRY

ENDFUNC 

FUNCTION uf_pesqdocscl_multiSel
	IF PESQDOCUMENTOSCLIENTES.menu1.multiSel.tag == 'false'
	   PESQDOCUMENTOSCLIENTES.menu1.multiSel.tag = 'true'
		
	   && altera o botao
	   PESQDOCUMENTOSCLIENTES.menu1.multiSel.config("Multi Sel.", myPath + "\imagens\icons\checked_w.png", " uf_pesqdocscl_multiSel","")
	   PESQDOCUMENTOSCLIENTES.menu1.estado("seltodos", "SHOW")

	ELSE
	   && aplica sele��o
	   PESQDOCUMENTOSCLIENTES.menu1.multiSel.tag = 'false'
		
	   && altera o botao
       PESQDOCUMENTOSCLIENTES.menu1.multiSel.config("Multi Sel.", myPath + "\imagens\icons\unchecked_w.png", " uf_pesqdocscl_multiSel","")
		PESQDOCUMENTOSCLIENTES.menu1.estado("seltodos", "HIDE")
	ENDIF
ENDFUNC 


FUNCTION uf_pesqdocumentosclientes_fechar
	IF uf_perguntalt_chama("DESEJA FECHAR TODOS OS DOCUMENTOS SELECIONADOS?","Sim","N�o", 32)
		SELECT ucrsPesqDocsCL
		GO TOP 
		SCAN 
			IF ucrsPesqDocsCL.sel=.t. AND ALLTRIM(ucrsPesqDocsCL.pagamento) != 'Pag.Entrega'
				IF ALLTRIM(ucrsPesqDocsCL.tipodoc) == 'BO'

               IF uf_gerais_getUmValor("bo","ndos","bostamp = '" + ucrsPesqDocsCL.cabstamp + "'") = 5

                  TEXT TO msel TEXTMERGE NOSHOW

                     SELECT DISTINCT
                        ref,
                        sum(qtt) as totqtt
                     FROM
                        bi(nolock)
                     WHERE
                        bostamp = '<<ALLTRIM(ucrsPesqDocsCL.cabstamp)>>'
						      and (select stns from st(nolock) where bi.ref = st.ref and st.site_nr = <<ASTR(mySite_Nr)>>) = 0
                     GROUP BY
                        ref

                  ENDTEXT

                  uf_gerais_actGrelha("", "uc_updateST",msel)

                  select uc_updateST
                  GO TOP

                  SCAN FOR uc_updateST.totqtt <> 0

                     IF !uf_gerais_actGrelha("", "","update st set qttcli = qttcli - " + ASTR(uc_updateST.totqtt) + " where ref = '" + ALLTRIM(uc_updateST.ref) + "' and site_nr = " + ASTR(mySite_Nr))  
                        uf_perguntalt_chama("OCORREU UM PROBLEMA A FECHAR O DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.","OK","",48)
                     ENDIF

                     select uc_updateST
                  ENDSCAN

                  FECHA("uc_updateST")

               ENDIF


					lcSQL = ""
					TEXT TO lcSql NOSHOW TEXTMERGE 
						UPDATE bo SET fechada=1 WHERE bostamp='<<ALLTRIM(ucrsPesqDocsCL.cabstamp)>>'	
					ENDTEXT
					uf_gerais_actGrelha("", "",lcSQL)

				ELSE
					lcSQL = ""
					TEXT TO lcSql NOSHOW TEXTMERGE 
						UPDATE fo SET u_status='F' WHERE fostamp='<<ALLTRIM(ucrsPesqDocsCL.cabstamp)>>'	
					ENDTEXT
					uf_gerais_actGrelha("", "",lcSQL)
				ENDIF 
				SELECT ucrsPesqDocsCL
				
				** registar ocorrencia **
				LOCAL lcStamp
				lcStamp = uf_gerais_stamp()
				lcSQL = ''
				TEXT TO lcSql NOSHOW textmerge
					INSERT INTO B_ocorrencias(
						stamp, tipo, grau, descr,
						oValor, dValor,
						usr, date)
					values
						('<<lcStamp>>', 'Fecho manual documentos', 3, 'Doc. <<ALLTRIM(ucrsPesqDocsCL.documento)>> <<ALLTRIM(ucrsPesqDocsCL.numdoc)>>',
						'', '',
						<<ch_userno>>, dateadd(HOUR, <<difhoraria>>, getdate()))
				ENDTEXT
				IF !uf_gerais_actgrelha("", "", lcSql)
					uf_perguntalt_chama("OCORREU UM PROBLEMA A REGISTAR OCORR�NCIA. POR FAVOR CONTACTE O SUPORTE.","OK","",48)
				ENDIF
			ENDIF 
		ENDSCAN 
		SELECT ucrsPesqDocsCL
		GO TOP 
		IF !uf_gerais_validaPermUser(ch_userno,ch_grupo,'Documentos - Permite fechar de seguida') == .t.
			pesqdocumentosclientes.menu1.multiSel.tag = 'false'
			pesqdocumentosclientes.menu1.multiSel.config("Multi Sel.", myPath + "\imagens\icons\unchecked_w.png", " uf_pesqdocs_multiSel","")
		ENDIF 
		uf_pesqdocumentosclientes_actPesquisa()
	ELSE
		uf_perguntalt_chama("OPERA��O CANCELADA.","OK","", 64)
	ENDIF 
ENDFUNC 

FUNCTION uf_pesqdocscl_SelTodos
	
	IF EMPTY(PESQDOCUMENTOSCLIENTES.menu1.seltodos.tag) OR PESQDOCUMENTOSCLIENTES.menu1.seltodos.tag == "false"
		&& aplica sele��o
		REPLACE ALL sel WITH .t. IN ucrsPesqDocsCL
		
		&& altera o botao
		PESQDOCUMENTOSCLIENTES.menu1.selTodos.tag = "true"
		PESQDOCUMENTOSCLIENTES.menu1.selTodos.config("Sel. Todos", myPath + "\imagens\icons\checked_w.png", "uf_pesqdocscl_SelTodos","T")
		
		&& actualiza o ecra
		*pesqstocks.refresh()
	ELSE
		&& aplica sele��o
		REPLACE ALL sel WITH .f. IN ucrsPesqDocsCL
		
		&& altera o botao
		PESQDOCUMENTOSCLIENTES.menu1.selTodos.tag = "false"
		PESQDOCUMENTOSCLIENTES.menu1.selTodos.config("Sel. Todos", myPath + "\imagens\icons\unchecked_w.png", "uf_pesqdocscl_SelTodos","T")
		
		&& actualiza o ecra		
		*pesqstocks.refresh()
	ENDIF
	
	uf_encautomaticas_ActualizaQtValorEnc()
		
	SELECT ucrsPesqDocsCL
	GO TOP 
	
	PESQDOCUMENTOSCLIENTES.GridPesq.Refresh
ENDFUNC 

FUNCTION uf_pesqdocumentosclientes_processar

	LPARAMETERS LcStampProc

	LOCAL uv_campo, uv_oriSite, uv_modoPag

    uv_oriSite = uf_gerais_getUmValor("bo", "site", "bostamp = '" + ALLTRIM(LcStampProc) + "'")
	
	IF EMPTY(uv_oriSite) OR ALLTRIM(uf_gerais_getUmValor("bo", "nmdos", "bostamp = '" + ALLTRIM(LcStampProc) + "'")) <>'Encomenda de Cliente'
		uf_perguntalt_chama("ESTA FUNCIONALIDADE EST� RESTRITA �S ENCOMENDAS DE CLIENTE.","OK","",48)
	ELSE

		IF (ALLTRIM(uv_oriSite) != ALLTRIM(mysite))
			uf_perguntalt_chama("N�O � PERMITIDO PROCESSAR ENCOMENDAS EM LOJAS DIFERENTE DA DE ORIGEM","OK","",48)
			RETURN .F.
		ENDIF

		LOCAL lcNmdos, LcObrano, LcDataobra, LcCodext, LcTotValeDesc, uv_updateStatus
		STORE 0 TO LcTotValeDesc

		Text To uv_sql Textmerge noshow
			select status, bo.nmdos, bo.obrano, CONVERT(varchar, bo.dataobra, 104) as dataobra, 
			codext,pagamento, bo.no, bo.estab, bo.etotal, bo2.modo_envio ,
			bo.usrhora, bo.usrdata, bo.usrinis
			from bo2 (nolock)
			inner join bo (nolock) on bo.bostamp=bo2.bo2stamp 
			where bo2.bo2stamp='<<ALLTRIM(LcStampProc)>>'
		ENDTEXT

		IF uf_gerais_getparameter_site('ADM0000000199', 'BOOL', mysite)

			IF !uf_gerais_chkconexao()
				uf_perguntalt_chama("N�o existe liga��o ao servidor Central. Por favor volte a tentar mais tarde.","OK","",16)
				RETURN .F.
			ENDIF

			IF !uf_gerais_runSQLCentral(uv_sql, "uCrsstatus")
				uf_perguntalt_chama("N�o foi poss�vel verificar o documento no servidor Central.","OK","",16)
				RETURN .F.
			ENDIF

			IF RECCOUNT("uCrsstatus") = 0
				uf_perguntalt_chama("N�o foi poss�vel verificar o documento no servidor Central.","OK","",16)
				RETURN .F.
			ENDIF

			IF !uf_gerais_actGrelha("", "uCrsstatusLocal", uv_sql)
				uf_perguntalt_chama("N�o foi poss�vel verificar o documento no servido Local.","OK","",16)
				RETURN .F.
			ENDIF

			SELECT uCrsstatus
			SELECT uCrsstatusLocal

			IF !uf_gerais_compStr(uCrsstatusLocal.status, uCrsstatus.status)

				IF uCrsstatusLocal.usrdata > uCrsstatus.usrdata OR (uCrsstatusLocal.usrdata = uCrsstatus.usrdata AND uCrsstatusLocal.usrhora > uCrsstatus.usrhora)

					TEXT TO uv_updateStatus TEXTMERGE NOSHOW
						UPDATE bo2 SET 
							status = '<<ALLTRIM(uCrsstatusLocal.status)>>', 
							pagamento = '<<ALLTRIM(uCrsstatusLocal.pagamento)>>',
							modo_envio = '<<ALLTRIM(uCrsstatusLocal.modo_envio)>>',
							usrdata = '<<uf_gerais_getDate(uCrsstatusLocal.usrdata, "SQL")>>', 
							usrhora = '<<uCrsstatusLocal.usrhora>>',
							usrinis = '<<ALLTRIM(uCrsstatusLocal.usrinis)>>'
						WHERE
							bo2stamp = '<<ALLTRIM(LcStampProc)>>'

						UPDATE bo SET 
							usrdata = '<<uf_gerais_getDate(uCrsstatusLocal.usrdata, "SQL")>>', 
							usrhora = '<<uCrsstatusLocal.usrhora>>',
							usrinis = '<<uCrsstatusLocal.usrinis>>'
						WHERE
							bostamp = '<<ALLTRIM(LcStampProc)>>'

					ENDTEXT

					IF !uf_gerais_runSQLCentral(uv_updateStatus)
						uf_perguntalt_chama("Erro a atualizar a Encomenda na Central.","OK","",16)
						RETURN .F.
					ENDIF

					SELECT uCrsstatus
					REPLACE uCrsstatus.status WITH ALLTRIM(uCrsstatusLocal.status),;
							uCrsstatus.pagamento WITH ALLTRIM(uCrsstatusLocal.pagamento),;
							uCrsstatus.modo_envio WITH ALLTRIM(uCrsstatusLocal.modo_envio)
					

				ENDIF

				IF uCrsstatus.usrdata > uCrsstatusLocal.usrdata OR (uCrsstatus.usrdata = uCrsstatusLocal.usrdata AND uCrsstatus.usrhora > uCrsstatusLocal.usrhora)

					TEXT TO uv_updateStatus TEXTMERGE NOSHOW
						UPDATE bo2 SET 
							status = '<<ALLTRIM(uCrsstatus.status)>>', 
							pagamento = '<<ALLTRIM(uCrsstatus.pagamento)>>',
							modo_envio = '<<ALLTRIM(uCrsstatus.modo_envio)>>',
							usrdata = '<<uf_gerais_getDate(uCrsstatus.usrdata, "SQL")>>', 
							usrhora = '<<uCrsstatus.usrhora>>',
							usrinis = '<<ALLTRIM(uCrsstatus.usrinis)>>'
						WHERE
							bo2stamp = '<<ALLTRIM(LcStampProc)>>'

						UPDATE bo SET 
							usrdata = '<<uf_gerais_getDate(uCrsstatus.usrdata, "SQL")>>', 
							usrhora = '<<uCrsstatus.usrhora>>',
							usrinis = '<<uCrsstatus.usrinis>>'
						WHERE
							bostamp = '<<ALLTRIM(LcStampProc)>>'

					ENDTEXT

					IF !uf_gerais_actGrelha("", "", uv_updateStatus)
						uf_perguntalt_chama("Erro a atualizar a Encomenda Localmente.","OK","",16)
						RETURN .F.
					ENDIF

					SELECT uCrsstatusLocal
					REPLACE uCrsstatusLocal.status WITH ALLTRIM(uCrsstatus.status),;
					REPLACE uCrsstatusLocal.pagamento WITH ALLTRIM(uCrsstatus.pagamento),;
					REPLACE uCrsstatusLocal.modo_envio WITH ALLTRIM(uCrsstatus.modo_envio),;

					IF USED("ucrsPesqDocsCL")

						SELECT ucrsPesqDocsCL
						REPLACE ucrsPesqDocsCL.status WITH ALLTRIM(uCrsstatus.status),;
								ucrsPesqDocsCL.pagamento WITH ALLTRIM(uCrsstatus.pagamento),;
								ucrsPesqDocsCL.modo_envio WITH ALLTRIM(uCrsstatus.modo_envio)

					ENDIF

				ENDIF

			ENDIF
		
		ELSE

			uf_gerais_actGrelha("","uCrsstatus",uv_sql)

		ENDIF

		select uCrsstatus
		
		lcNmdos = ALLTRIM(uCrsstatus.nmdos)
		LcObrano = uCrsstatus.obrano
		LcDataobra = uCrsstatus.dataobra
		LcCodext = ALLTRIM(uCrsstatus.codext )
		**Alterar na versao 23_02 usar tabela de controlo de estados

		TEXT TO uv_sql TEXTMERGE NOSHOW
			exec up_vendas_online_checkStatusChange '<<ALLTRIM(uCrsstatus.modo_envio)>>', '<<ALLTRIM(uCrsstatus.pagamento)>>', 'Logitools', '<<ALLTRIM(uCrsstatus.status)>>', 'Faturada', '<<ALLTRIM(mysite)>>'
		ENDTEXT

		IF !uf_gerais_ActGrelha("","uc_checkStatusChange", uv_sql)
			uf_perguntalt_chama("Erro a verificar a altera��o de estado." + CHR(13) + "Por favor contacte o suporte.","OK","",48)
			RETURN .f.
		ENDIF

		SELECT uc_checkStatusChange

		IF !uc_checkStatusChange.statChange
			uf_perguntalt_chama("N�O PODE SER EMITIDO UM DOCUMENTO DE FATURA��O APARTIR DESTE DOCUMENTO.","OK","",48)
			RETURN .f.
		ENDIF

		DO CASE 
			CASE UPPER(ALLTRIM(uCrsstatus.pagamento))=='PAG.ENTREGA'

				uf_facturacao_chama('')
				uf_facturacao_novo('FACTURA')
				Facturacao.containercab.nmdoc.VALUE='Factura'

			CASE UPPER(ALLTRIM(uCrsstatus.pagamento))=='MULTIBANCO'

				uf_facturacao_chama('')
				uf_facturacao_novo('FACTURA')

				IF uf_gerais_getUmValor("td", "count(*)", "nmdoc='Fatura Recibo' and site='" + mysite + "'") <> 0
					Facturacao.containercab.nmdoc.VALUE='Fatura Recibo'
				ELSE
					Facturacao.containercab.nmdoc.VALUE='Venda a Dinheiro'
				ENDIF 

			OTHERWISE
				uf_perguntalt_chama("N�O PODE SER EMITIDO UM DOCUMENTO DE FATURA��O APARTIR DESTE DOCUMENTO.","OK","",48)
				RETURN .f.
		ENDCASE

		uv_modoPag = uCrsstatus.pagamento
		
		fecha("uCrsstatus")
		
		uf_facturacao_limpaNomeFact()
		uf_facturacao_valoresDefeitoFact()
		uf_facturacao_controlaNumeroFact(year(datetime()+(difhoraria*3600)))
		uf_atendimento_fiiliq() &&actualiza totais das linhas
		uf_atendimento_actTotaisFt() &&actualiza totais do cabe�alho
		uf_facturacao_criaLinhaDefault()
		uf_atendimento_selCliente(ucrsPesqDocsCL.no, ucrsPesqDocsCL.estab,"FACTURACAO")
		LcSql = ""
		Text To lcSQL Textmerge noshow
			exec up_perfis_validaAcessoDocs <<ch_userno>>,'<<ch_grupo>>','visualizar','FT', 0, '<<ALLTRIM(mysite)>>'
		ENDTEXT
		uf_gerais_actGrelha("","uCrsListaDocs",lcSQL)
		
		Select FI
		Append Blank
		REPLACE Fi.LORDEM	With	10
		IF EMPTY(LcCodext)
			REPLACE Fi.design WITH ALLTRIM(lcNmdos) + ' Nr. ' + ALLTRIM(STR(LcObrano)) + ' de ' + LcDataobra 
		ELSE 
			REPLACE Fi.design WITH ALLTRIM(lcNmdos) + ' Nr. ' + ALLTRIM(LcCodext) + ' de ' + LcDataobra 
		ENDIF 

		*VALORES POR DEFEITO
		** FISTAMP
		lcStamp = uf_gerais_stamp()

		SELECT FI
		
		REPLACE FI.FISTAMP 	With	lcStamp
		REPLACE FI.NMDOC 	With	Alltrim(Ft.Nmdoc)
		Replace Fi.ndoc		With	Ft.ndoc
		REPLACE FI.FNO 		With	Ft.Fno

		**IVA
		REPLACE FI.IVA With uf_gerais_getTabelaIVA(1, "TAXA")
		REPLACE FI.TABIVA With 1
		
		**ARMAZEM (Variavel publica criada no startup)
		REPLACE FI.ARMAZEM With myarmazem
		
		uf_importarFact_crialinhas_compBOFT(ALLTRIM(LcStampProc))
		
		SELECT fi
		GO TOP
		SCAN
			replace fi.ivaincl WITH .t.
			uf_atendimento_fiiliq(.t.)
			
			IF uf_gerais_getParameter("ADM0000000211","BOOL") == .f.
				uf_atendimento_actValoresFi()
			ENDIF
		
			SELECT fi
			IF ALLTRIM(fi.ref)=='V999999' AND !EMPTY(fi.bistamp)
				LOCAL lcnrvale
				STORE '' TO lcnrvale
				LcSql = ""
				Text To lcSQL Textmerge noshow
					select valeNr from bi2 where bi2stamp='<<ALLTRIM(fi.bistamp)>>'
				ENDTEXT
				uf_gerais_actGrelha("","uCrsnrvale",lcSQL)
				IF !EMPTY(uCrsnrvale.valenr)
					replace fi.design WITH ALLTRIM(fi.design) + ' - Vale Nr. ' + ALLTRIM(uCrsnrvale.valenr)
				ENDIF 				
				fecha("uCrsnrvale")
			ENDIF 
			
		ENDSCAN
		
		
		SELECT fi
		GO TOP

		**					
		IF uf_gerais_getParameter("ADM0000000211","BOOL") == .t. && Atribui automaticamente os Lotes
			UF_FACTURACAO_AtribuiLotesDocumento()
		ENDIF
		uf_atendimento_actValoresFi()
		uf_atendimento_actTotaisFt()
		
		SELECT fi
		GO TOP
		
		SELECT ft
		IF (ALLTRIM(ft.nmdoc)=='Fatura Recibo' OR ALLTRIM(ft.nmdoc)=='Venda a Dinheiro') AND UPPER(ALLTRIM(uv_modoPag))=='MULTIBANCO'
			facturacao.pageframe1.page2.movcaixa.Tag = "true"
			facturacao.pageframe1.page2.movcaixa.Picture = myPath + "\imagens\icons\checked_b_24.bmp"	

			uv_campo = uf_gerais_getUmValor("b_modoPag_moeda", "REPLACE(campoFact, 'ft2.', '')", "codigoEXT = 'RB'", " ref asc")

			IF !EMPTY(uv_campo)

				replace ucrsPagCentral.&uv_campo. WITH ft.etotal

			ENDIF

			*replace ucrsPagCentral.epaga3 WITH ft.etotal	
		ENDIF 
		
		SELECT fi
		GO TOP 
		SCAN 
			IF ALLTRIM(fi.ref)=='V000001'
				**uf_atendimento_actref()
				LcTotValeDesc = LcTotValeDesc + etiliquido
				SELECT fi
				replace fi.ref WITH '' 
				replace fi.qtt WITH 0 
				replace fi.etiliquido WITH 0
				replace fi.epv WITH 0 
				replace fi.ecusto WITH 0 
				replace fi.epromo WITH .f.
				replace fi.u_epvp WITH 0
				replace fi.design WITH 'Valor utilizado em cart�o: ' + ALLTRIM(STR(LcTotValeDesc)) + ' AOA'
				replace fi.valcartao WITH LcTotValeDesc
				**uf_atendimento_actValoresFi()
				uf_atendimento_actTotaisFt()
			ENDIF 
		ENDSCAN 
		
		SELECT fi
		GO TOP 
		
		IF LcTotValeDesc < 0 AND uf_gerais_getParameter_site('ADM0000000153', 'BOOL', mySite)
			SELECT ft2
			replace ft2.valcartaoutilizado WITH LcTotValeDesc 
			SELECT ft
			IF ALLTRIM(ft.nmdoc)=='Fatura Recibo' OR ALLTRIM(ft.nmdoc)=='Venda a Dinheiro'

				

				uv_campo = uf_gerais_getUmValor("b_modoPag_moeda", "REPLACE(campoFact, 'ft2.', '')", "valCartao = 1", " ref asc")

				IF !EMPTY(uv_campo)

					**replace ucrsPagCentral.epaga3 WITH ucrsPagCentral.epaga3+	LcTotValeDesc 
					**replace ucrsPagCentral.epaga4 WITH LcTotValeDesc * (-1)

					replace ucrsPagCentral.&uv_campo. WITH ucrsPagCentral.&uv_campo. + ABS(LcTotValeDesc)

				ENDIF
			ENDIF
		ENDIF 
	
	ENDIF 
		
ENDFUNC 


FUNCTION uf_pesqdocumentosclientes_scanner

    uv_scan = ALLTRIM(PESQDOCUMENTOSCLIENTES.txtScanner.value)

    IF EMPTY(uv_scan)
        PESQDOCUMENTOSCLIENTES.numDoc.setFocus()
        RETURN .F.
    ENDIF

    IF !EMPTY(uv_scan) AND AT(" ", ALLTRIM(uv_scan)) = 0 AND LEN(ALLTRIM(uv_scan)) > 20
        uv_ref = uf_gerais_getRefFromQR(ALLTRIM(uv_scan))

        IF !EMPTY(uv_ref)

            PESQDOCUMENTOSCLIENTES.produto.setFocus()
            PESQDOCUMENTOSCLIENTES.produto.value = ALLTRIM(uv_ref)
            PESQDOCUMENTOSCLIENTES.menu1.actualizar.click			

        ENDIF
    ENDIF

ENDFUNC

FUNCTION uf_pesqdocumentosclientes_validaPlfCl
	LPARAMETERS uv_no, uv_estab, uv_totDoc, uv_chamaJava

	IF EMPTY(uv_no)
		RETURN .F.
	ENDIF

	uv_clExt = uf_gerais_getUmValor("b_utentes", "ISNULL(no_ext, '')", "no = " + ASTR(uv_no) + " AND estab = " + ASTR(uv_estab))

	IF EMPTY(uv_clExt)
		uf_perguntalt_chama("O CLIENTE N�O TEM PLAFOND.","OK","",48)
		RETURN .F.
	ENDIF

	IF uv_chamaJava
		IF !uf_pesqutentes_actplafondcentral(uv_clExt, 'BO', .T.)
			RETURN .F.
		ENDIF

	ENDIF

	TEXT TO msel TEXTMERGE NOSHOW
		SELECT esaldo, eplafond FROM b_utentes(nolock) WHERE no = <<ASTR(uv_no)>> AND estab = <<ASTR(uv_estab)>>
	ENDTEXT

	IF !uf_gerais_actGrelha("", "uc_clPlafond", msel)
		uf_perguntalt_chama("ERRO A VERIFICAR SALDO DO CLIENTE.","OK","",48)
		RETURN .F.
	ENDIF

	SELECT uc_clPlafond

	uv_saldo = uc_clPlafond.esaldo
	uv_plafond = uc_clPlafond.eplafond

	FECHA("uc_clPlafond")

	IF uv_totDoc + uv_saldo > uv_plafond
		uf_perguntalt_chama("O CLIENTE N�O TEM PLAFOND O SUFICIENTE.","OK","",48)
		RETURN .F.
	ENDIF

	RETURN .T.

ENDFUNC

FUNCTION uf_pesqdocumentosclientes_syncStatusEnc

	IF !USED("ucrsPesqDocsCL")
		RETURN .F.
	ENDIF

	IF !uf_gerais_chkconexao()
		uf_perguntalt_chama("N�o existe liga��o ao servidor Central. Por favor volte a tentar mais tarde.","OK","",48)
		RETURN .F.
	ENDIF

	LOCAL uv_bostamps, uv_token, uv_central
	uv_bostamps = ''

	REGUA(0,3, "A sincronizar Encomendas...")

	REGUA(1,1, "A sincronizar Encomendas...")

	SELECT ucrsPesqDocsCL
	GO TOP

	SCAN FOR uf_gerais_compStr("Encomenda de Cliente", ucrsPesqDocsCL.documento)

		uv_bostamps = uv_bostamps + IIF(EMPTY(uv_bostamps), '', ',') + ucrsPesqDocsCL.cabstamp

	ENDSCAN

	REGUA(1,2, "A sincronizar Encomendas...")

	IF !EMPTY(uv_bostamps)

		uv_token = uf_gerais_stamp()
		uv_central = uf_gerais_getparameter_site('ADM0000000090', 'TEXT', mysite)

		uf_gerais_insert_tempFilters(uv_token, ALLTRIM(uv_bostamps), 0, 'BOSTAMP', mySite, ',', 50)

		TEXT TO msel TEXTMERGE NOSHOW

			EXEC up_documentos_syncEncSiteCentral '<<ALLTRIM(uv_token)>>', '<<ALLTRIM(uv_central)>>', '<<ALLTRIM(m_chinis)>>'

		ENDTEXT

		REGUA(1,3, "A sincronizar Encomendas...")

		IF !uf_gerais_actGrelha("", "", msel)
			REGUA(2)
			uf_perguntalt_chama("N�o foi poss�vel sincronizar os documentos com a central.","OK","",48)
			RETURN .F.
		ENDIF

		uf_pesqdocumentosclientes_actPesquisa()

		REGUA(2)	
		uf_perguntalt_chama("Encomendas sincronizadas com sucesso.","OK","",64)

	ELSE

		REGUA(2)	

	ENDIF

	

ENDFUNC