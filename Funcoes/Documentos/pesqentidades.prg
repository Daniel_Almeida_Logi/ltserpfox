**
** lcOrigem
**
FUNCTION uf_pesqEntidades_Chama
	LPARAMETERS lcOrigem
	
	PUBLIC myOrigemPPE
	myOrigemPPE = lcOrigem
	
	LOCAL lcSQL
	lcSql = ''

	uf_pesqEntidades_CarregaDados(.t.)
	
	IF TYPE('PESQENTIDADES')=="U"
		DO FORM PESQENTIDADES
		PESQENTIDADES.show
	ELSE
		PESQENTIDADES.show
	ENDIF
ENDFUNC



**
** tcBool	: caso venha a .t. cria apenas o cursor
**
FUNCTION uf_pesqEntidades_CarregaDados
	LPARAMETERS tcBool
	
	LOCAL lcSQL
	lcSql = ''
	
	IF tcBool == .t.
		TEXT TO lcSql NOSHOW TEXTMERGE
			SET FMTONLY on
			exec up_documentos_pesquisaEntidades '', 0, '', '', ''
			SET FMTONLY off
		ENDTEXT
		
		IF type("PESQENTIDADES")=="U"
			IF !uf_gerais_actgrelha("", [ucrsPesqEnt], lcSql)
				uf_perguntalt_chama("N�O FOI POSSIVEL ACTUALIZAR A LISTAGEM. POR FAVOR CONTACTE O SUPORTE.","OK","", 16)
				RETURN .f.
			ENDIF
		ELSE
*			uf_gerais_actGrelha("PESQENTIDADES.GridPesq","ucrsPesqEnt",lcSQL)
			PESQENTIDADES.lblRegistos.caption = ALLTRIM(STR(RECCOUNT("uCrsPesqEnt"))) + " Resultados"
		ENDIF
		
	ELSE && Refresca valores da Grid
	
		regua(0,0,"A processar as Listagem de Documentos...")
		
		TEXT TO lcSQL NOSHOW TEXTMERGE
			exec up_documentos_pesquisaEntidades '<<ALLTRIM(PESQENTIDADES.txtNome.value)>>', <<IIF(EMPTY(ALLTRIM(PESQENTIDADES.txtNo.value)), 0, ALLTRIM(PESQENTIDADES.txtNo.value))>>, '<<ALLTRIM(PESQENTIDADES.txtTipo.value)>>', '<<ALLTRIM(PESQENTIDADES.txtTelef.value)>>', '<<ALLTRIM(PESQENTIDADES.txtNcont.value)>>'
		ENDTEXT

		uf_gerais_actGrelha("PESQENTIDADES.grdEntidades", "uCrsPesqEnt", lcSQL)
		
		regua(2)

		PESQENTIDADES.lblRegistos.caption = ALLTRIM(STR(RECCOUNT("uCrsPesqEnt"))) + " Resultados"

		PESQENTIDADES.Refresh
	ENDIF	
ENDFUNC



**
FUNCTION uf_pesqEntidades_Sel

	DO CASE
		CASE myOrigemPPE = "DOCUMENTOS"
			Select ucrsPesqEnt
			
			Select cabdoc
			replace cabdoc.nome		WITH ucrsPesqEnt.nome
			replace cabdoc.no		WITH ucrsPesqEnt.no
			replace cabdoc.estab	WITH 0
						
         	REPLACE cabdoc.xpdmorada WITH "[" + ASTR(uCrsPesqEnt.NO) + "]" +  ALLTRIM(uCrsPesqEnt.morada),;  
                    cabdoc.xpdcodpost WITH ALLTRIM(uCrsPesqEnt.codPost),; 
                    cabdoc.xpdLocalidade WITH  ALLTRIM(uCrsPesqEnt.local)

**			IF TYPE("upv_armazemDestino") <> "U" AND UPPER(ALLTRIM(cabdoc.doc)) == "TRF ENTRE ARMAZ�NS"
**
**				upv_armazemDestino = uf_gerais_getUmValor("empresa_arm", "armazem", "empresa_no = " + ASTR(ucrsPesqEnt.no), "armazem asc")
**
**				IF USED("BI")
**					SELECT BI
**					UPDATE BI SET ar2mazem = upv_armazemDestino
**				ENDIF
**
**			ENDIF

			DOCUMENTOS.ContainerCab.refresh
		
		CASE myOrigemPPE = "SELFORN_ENCAUTOMATICAS"
			Select ucrsPesqEnt
			
			Select crsenc
			
			replace ALL crsenc.nome		WITH ucrsPesqEnt.nome
			replace ALL crsenc.no		WITH ucrsPesqEnt.no
			replace ALL crsenc.estab	WITH 0
						
		OTHERWISE
			RETURN .f.
			
	ENDCASE
	
	uf_pesqEntidades_sair()
ENDFUNC



**
FUNCTION uf_pesqEntidades_sair
	** Fecha Cursores
	IF USED("ucrsPesqEnt")
		fecha("ucrsPesqEnt")
	ENDIF
	
	RELEASE myOrigemPPE
	
	PESQENTIDADES.hide
	PESQENTIDADES.release
ENDFUNC


**
FUNCTION uf_pesqEntidades_CarregaMenu
	PESQENTIDADES.menu1.adicionaOpcao("actualizar","Actualizar",myPath + "\imagens\icons\actualizar_w.png","uf_pesqEntidades_CarregaDados","A")
	PESQENTIDADES.menu1.adicionaOpcao("tecladoVirtual","Teclado",myPath + "\imagens\icons\teclado_w.png","uf_pesqEntidades_tecladoVirtual","T")

	PESQENTIDADES.refresh
ENDFUNC


**
FUNCTION uf_pesqEntidades_tecladoVirtual
	PESQENTIDADES.tecladoVirtual1.show("PESQENTIDADES.grdEntidades", 161, "PESQENTIDADES.shape7", 161)
ENDFUNC
