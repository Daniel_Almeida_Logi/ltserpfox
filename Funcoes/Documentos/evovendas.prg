**
FUNCTION uf_evovendas_chama

	Public myEVOVENDASIntroducao, myEVOVENDASAlteracao

	IF !USED("ucrsHistoricoEvovendas")
		TEXT TO lcSQL TEXTMERGE NOSHOW
			select
				bostamp
				,data =LTRIM(STR(YEAR(dataobra))) +'.'
					+ CASE WHEN LEN(LTRIM(STR(MONTH(dataobra))))=1 then '0' + LTRIM(STR(MONTH(dataobra))) else LTRIM(STR(MONTH(dataobra))) end
					+'.'+CASE WHEN LEN(LTRIM(STR(DAY(dataobra))))=1 then '0' + LTRIM(STR(DAY(dataobra))) else LTRIM(STR(DAY(dataobra))) end
				,documento = nmdos
				,numero = obrano
			from
				bo
			where 
				obs = 'DOCUMENTO GERADO POR IMPORTA��O EVOVENDAS'
			order by
				dataobra desc
		ENDTEXT
		IF !uf_gerais_actgrelha("", "ucrsHistoricoEvovendas", lcSql)
			MESSAGEBOX("N�o foi possivel verificar o hist�rico de importa��o Evovendas. Contacte o suporte.",16,"LOGITOOLS SOFTWARE")
			RETURN .f.
		ENDIF
	ENDIF

	IF TYPE("EVOVENDAS") == "U"
		DO FORM EVOVENDAS
	ELSE
		EVOVENDAS.show()
	ENDIF
	
	uf_EVOVENDAS_ActHistorico()
	
ENDFUNC


**
FUNCTION uf_EVOVENDAS_ActHistorico
	TEXT TO lcSQL TEXTMERGE NOSHOW
		select 
			bostamp,
			data = LTRIM(STR(YEAR(dataobra))) +'.'
				+ CASE WHEN LEN(LTRIM(STR(MONTH(dataobra))))=1 then '0' + LTRIM(STR(MONTH(dataobra))) else LTRIM(STR(MONTH(dataobra))) end
				+'.'+CASE WHEN LEN(LTRIM(STR(DAY(dataobra))))=1 then '0' + LTRIM(STR(DAY(dataobra))) else LTRIM(STR(DAY(dataobra))) end
			,documento = nmdos
			,numero = obrano 
		from 
			bo 
		where 
			obs = 'DOCUMENTO GERADO POR IMPORTA��O EVOVENDAS'
		order by  
			dataobra desc
	ENDTEXT 
	
	IF !uf_gerais_actgrelha("EVOVENDAS.GridPesq", "ucrsHistoricoEvovendas", lcSql)
		MESSAGEBOX("N�o foi possivel verificar o hist�rico de importa��o Evovendas. Contacte o suporte.",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF	
ENDFUNC


**
FUNCTION uf_EVOVENDAS_init
	
	&& Configura menu principal
	WITH EVOVENDAS.menu1
		.adicionaOpcao("opcoes","Op��es",myPath + "\imagens\icons\opcoes_w.png","","O")
		.adicionaOpcao("aplicar","Importar",myPath + "\imagens\icons\ferramentas_w.png","uf_evovendas_importEvovendas", "A")
	ENDWITH
	WITH EVOVENDAS.menu_opcoes		
		.adicionaOpcao("AtualizaStocks","Atualiza Stocks S2000","","uf_evovendas_ActualizaStocksSifarma2000")
	ENDWITH
ENDFUNC


**
**123
Function uf_evovendas_importEvovendas
	local lcLocal, lcMeses, lcValidaInsertACerto, lcData  
	store .f. to lcValidaInsertACerto
		
	IF EMPTY(evovendas.local.value) or EMPTY(evovendas.meses.value)
		uf_perguntalt_chama("Deve especificar o ficheiro de importa��o e o n�mero de meses a importar. Por favor verifique.","OK","",64)
		return .f.
	ENDIF

	IF EMPTY(evovendas.software.value)
		uf_perguntalt_chama("Deve especificar o Software de origem. Por favor verifique.","OK","",64)
		return .f.
	ENDIF

	** Valida extensoes dos ficheiros
	IF ALLTRIM(evovendas.software.value) == "SoftReis - SPharm v2.44.0" OR ALLTRIM(evovendas.software.value) == "SoftReis - (Exporta��o s/MSOffice)" && xls
		IF UPPER(RIGHT(ALLTRIM(evovendas.local.value),3)) != "XLS"
			uf_perguntalt_chama("Tipo de Ficheiro inv�lido. Deve indicar um ficheiro xls.","OK","",32)
			return .f.
		ENDIF
	ENDIF
	IF ALLTRIM(evovendas.software.value) == "SIFARMA 2000" && xls
		IF UPPER(RIGHT(ALLTRIM(evovendas.local.value),3)) != "TXT"
			uf_perguntalt_chama("Tipo de Ficheiro inv�lido. Deve indicar um ficheiro txt.","OK","",32)
			return .f.
		ENDIF
	ENDIF

	lcLocal = ["] + ALLTRIM(evovendas.local.value) + ["]
	lcMeses = evovendas.meses.value
	lcData = uf_gerais_getdate(evovendas.data.value,"SQL")

	IF !FILE(lcLocal)
		uf_perguntalt_chama("A localiza��o especificada para importa��o do ficheiro � inv�lida. Por favor verifique.","OK","",64)
		return .f.
	Endif

	** Verifica se existem documentos de acerto para o periodo especificado
	IF uf_evovendas_verificaSobreposicao() == .f. 
		return .f.
	ENDIF


	DO CASE
		&& Sifarma 2000
		CASE ALLTRIM(evovendas.software.value) == "SIFARMA 2000"
			CREATE cursor ucrsTempFileAux (CPR c(254),NOM c(254),FAP c(254),LOCALIZACAO c(254),SAC c(254);
			,STM c(254),SMI c(254),QTE c(254),DUV c(254),DUC c(254),PVP c(254),PCU c(254),IVA c(254),CAT c(254),CT1 c(254),GEN c(254);
			,GPR c(254),CLA c(254),TPR c(254),CAR c(254),GAM c(254),V_0 c(254),V_1 c(254),V_2 c(254),V_3 c(254),V_4 c(254),V_5 c(254);
			,V_6 c(254),V_7	c(254),V_8 c(254),V_9 c(254),V_10 c(254),V_11 c(254),V_12 c(254);
			,V_13 c(254), V_14 c(254), V_15 c(254), V_16 c(254), V_17 c(254), V_18 c(254) ,V_19 c(254);
			,V_20 c(254),V_21 c(254),V_22 c(254),V_23 c(254),DTVAL c(254),FPD c(254),LAD c(254)	,PRATELEIRA c(254);
			,GAMA c(254),GRUPOHOMOGENEO c(254),INACTIVO c(254),PVP5 c(254);
			,lab c(150), familia c(18), faminome c(60), marca c(20), tabiva n(2,0), taxaiva n(5,2))

			SELECT ucrsTempFileAux
			APPEND FROM &lcLocal TYPE DELIMITED WITH TAB

			Select * from ucrsTempFileAux WHERE ALLTRIM(ucrsTempFileAux.CPR) != 'CPR' AND LOCALIZACAO != "ARMAZ�M" AND ALLTRIM(LOCALIZACAO) != "Reserva de Produtos" ORDER BY CPR into CURSOR ucrsTempFile READWRITE
			
			** Actualizar cursor com dados da Fprod
			uf_evovendas_actualizaDadosFprod()

			regua(0,lcMeses+1,"A processar importa��o do ficheiro evovendas...")
			regua(1,1,"A processar importa��o do ficheiro evovendas...")
			lcData = uf_gerais_getDate(evovendas.data.value,"SQL")

			FOR i=IIF(EVOVENDAS.CHECK1.tag == "true",1,0) TO lcMeses-1

				regua(1,i+1,"A processar importa��o do ficheiro evovendas... M�s: " + ALLTRIM(STR(i+1)))

				lcValidaInsertACerto  = uf_evovendas_CriaAcertoStockEvovendasMes(i, lcData)

				IF lcValidaInsertACerto == .f.
					EXIT
				ENDIF
			ENDFOR
	
		&& SoftReis
		CASE ALLTRIM(evovendas.software.value) == "SoftReis - SPharm v2.44.0"
			CREATE cursor ucrsTempFile (CPR	 c(18), NOM	 c(200),FAP c(254), LOCALIZACAO c(254), SAC n(10);
			,STM c(254), SMI c(254), QTE c(254), DUV c(254), DUC c(254), PVP c(254), PCU c(254), IVA c(254), CAT c(254), CT1 c(254), GEN c(254);
			,GPR c(254), CLA c(254), TPR c(254), CAR c(254), GAM c(254);
			,JAN_1 n(10), FEV_1 n(10), MAR_1 n(10), ABR_1 n(10), MAI_1 n(10), JUN_1 n(10), JUL_1 n(10), AGO_1 n(10), SET_1 n(10), OUT_1 n(10), NOV_1 n(10), DEZ_1 n(10);
			,JAN_2 n(10), FEV_2 n(10), MAR_2 n(10), ABR_2 n(10), MAI_2 n(10), JUN_2 n(10), JUL_2 n(10), AGO_2 n(10), SET_2 n(10), OUT_2 n(10), NOV_2 n(10), DEZ_2 n(10);
			,DTVAL c(254), FPD c(254), LAD c(254) ,PRATELEIRA c(254), GAMA c(254), GRUPOHOMOGENEO c(254), INACTIVO c(254), PVP5 c(254);
			,lab c(150), familia c(18), faminome c(60), marca c(20), tabiva n(2,0), taxaiva n(5,2))


			SELECT ucrsTempFile
			
			UF_GERAIS_xls2Cursor(evovendas.local.value,"xxMapaEvolucaoVendasXLS","ucrsEvovendasSoftReis")

			IF USED("ucrsEvovendasSoftReis")
				IF RECCOUNT("ucrsEvovendasSoftReis") > 0
					Select ucrsEvovendasSoftReis
					GO TOP
					SCAN
						IF !ISNULL(ucrsEvovendasSoftReis.codigo)

							select ucrsTempFile 
							APPEND BLANK

							Replace ucrsTempFile.cpr WITH ALLTRIM(STR(ucrsEvovendasSoftReis.codigo))
							Replace ucrsTempFile.NOM WITH ucrsEvovendasSoftReis.nome_comercial
							&&Replace ucrsTempFile.SAC WITH ALLTRIM(STR(ucrsEvovendasSoftReis.Exist�ncia))
							Replace ucrsTempFile.SAC WITH ucrsEvovendasSoftReis.Exist�ncia
							Replace ucrsTempFile.PCU WITH ALLTRIM(STR(ucrsEvovendasSoftReis.P_u_c_,9,2))
							Replace ucrsTempFile.PVP WITH ALLTRIM(STR(ucrsEvovendasSoftReis.P_v_p_,9,2))

							Select ucrsEvovendasSoftReis
							FOR i=1 TO FCOUNT('ucrsEvovendasSoftReis')

						      	DO CASE

						      		CASE UPPER(ALLTRIM(LEFT(FIELD(i),3))) == "JAN" AND VAL(RIGHT(FIELD(i),4)) = YEAR(DATE())
						      			lcNomeColuna = "ucrsEvovendasSoftReis." + ALLTRIM(FIELD(i))
						      			IF TYPE("ucrsTempFile.JAN_1")=='N'
											Replace ucrsTempFile.JAN_1 WITH &lcNomeColuna
										ELSE
											Replace ucrsTempFile.JAN_1 WITH VAL(&lcNomeColuna)
										ENDIF

						      		CASE ALLTRIM(LEFT(FIELD(i),3)) == "FEV" AND VAL(RIGHT(FIELD(i),4)) = YEAR(DATE())	
					      				lcNomeColuna = "ucrsEvovendasSoftReis." + ALLTRIM(FIELD(i))
					      				IF TYPE("ucrsTempFile.FEV_1")=='N'
											Replace ucrsTempFile.FEV_1 WITH &lcNomeColuna
										ELSE
											Replace ucrsTempFile.FEV_1 WITH VAL(&lcNomeColuna)
										ENDIF
						      		
						      		CASE ALLTRIM(LEFT(FIELD(i),3)) == "MAR" AND VAL(RIGHT(FIELD(i),4)) = YEAR(DATE())
							      		lcNomeColuna = "ucrsEvovendasSoftReis." + ALLTRIM(FIELD(i))
							      		IF TYPE("ucrsTempFile.MAR_1")=='N'
											Replace ucrsTempFile.MAR_1 WITH &lcNomeColuna
										ELSE
											Replace ucrsTempFile.MAR_1 WITH VAL(&lcNomeColuna)
										ENDIF
										
						      		CASE ALLTRIM(LEFT(FIELD(i),3)) == "ABR" AND VAL(RIGHT(FIELD(i),4)) = YEAR(DATE())
							      		lcNomeColuna = "ucrsEvovendasSoftReis." + ALLTRIM(FIELD(i))
							      		IF TYPE("ucrsTempFile.ABR_1")=='N'
											Replace ucrsTempFile.ABR_1 WITH &lcNomeColuna
										ELSE
											Replace ucrsTempFile.ABR_1 WITH VAL(&lcNomeColuna)
										ENDIF

						      		CASE ALLTRIM(LEFT(FIELD(i),3)) == "MAI" AND VAL(RIGHT(FIELD(i),4)) = YEAR(DATE())
										lcNomeColuna = "ucrsEvovendasSoftReis." + ALLTRIM(FIELD(i))
						      			IF TYPE("ucrsTempFile.MAI_1")=='N'
											Replace ucrsTempFile.MAI_1 WITH &lcNomeColuna
										ELSE
											Replace ucrsTempFile.MAI_1 WITH VAL(&lcNomeColuna)
										ENDIF				      		

						      		CASE ALLTRIM(LEFT(FIELD(i),3)) == "JUN" AND VAL(RIGHT(FIELD(i),4)) = YEAR(DATE())
						      			lcNomeColuna = "ucrsEvovendasSoftReis." + ALLTRIM(FIELD(i))
										IF TYPE("ucrsTempFile.JUN_1")=='N'
											Replace ucrsTempFile.JUN_1 WITH &lcNomeColuna
										ELSE
											Replace ucrsTempFile.JUN_1 WITH VAL(&lcNomeColuna)
										ENDIF
										
						      		CASE ALLTRIM(LEFT(FIELD(i),3)) == "JUL" AND VAL(RIGHT(FIELD(i),4)) = YEAR(DATE())
										lcNomeColuna = "ucrsEvovendasSoftReis." + ALLTRIM(FIELD(i))
							      		IF TYPE("ucrsTempFile.JUL_1")=='N'
											Replace ucrsTempFile.JUL_1 WITH &lcNomeColuna
										ELSE
											Replace ucrsTempFile.JUL_1 WITH VAL(&lcNomeColuna)
										ENDIF

						      		CASE ALLTRIM(LEFT(FIELD(i),3)) == "AGO" AND VAL(RIGHT(FIELD(i),4)) = YEAR(DATE())
							      		lcNomeColuna = "ucrsEvovendasSoftReis." + ALLTRIM(FIELD(i))
							      		IF TYPE("ucrsTempFile.AGO_1")=='N'
											Replace ucrsTempFile.AGO_1 WITH &lcNomeColuna
										ELSE
											Replace ucrsTempFile.AGO_1 WITH VAL(&lcNomeColuna)
										ENDIF

						      		CASE ALLTRIM(LEFT(FIELD(i),3)) == "SET" AND VAL(RIGHT(FIELD(i),4)) = YEAR(DATE())
						      			lcNomeColuna = "ucrsEvovendasSoftReis." + ALLTRIM(FIELD(i))
						      			IF TYPE("ucrsTempFile.SET_1")=='N'
											Replace ucrsTempFile.SET_1 WITH &lcNomeColuna
										ELSE
											Replace ucrsTempFile.SET_1 WITH VAL(&lcNomeColuna)
										ENDIF
										
						      		CASE ALLTRIM(LEFT(FIELD(i),3)) == "OUT" AND VAL(RIGHT(FIELD(i),4)) = YEAR(DATE())      			
							      		lcNomeColuna = "ucrsEvovendasSoftReis." + ALLTRIM(FIELD(i))
						      			IF TYPE("ucrsTempFile.OUT_1")=='N'
											Replace ucrsTempFile.OUT_1 WITH &lcNomeColuna
										ELSE
											Replace ucrsTempFile.OUT_1 WITH VAL(&lcNomeColuna)
										ENDIF
										
						      		CASE ALLTRIM(LEFT(FIELD(i),3)) == "NOV" AND VAL(RIGHT(FIELD(i),4)) = YEAR(DATE())
										lcNomeColuna = "ucrsEvovendasSoftReis." + ALLTRIM(FIELD(i))
						      			IF TYPE("ucrsTempFile.NOV_1")=='N'
											Replace ucrsTempFile.NOV_1 WITH &lcNomeColuna
										ELSE
											Replace ucrsTempFile.NOV_1 WITH VAL(&lcNomeColuna)
										ENDIF
										
						      		CASE ALLTRIM(LEFT(FIELD(i),3)) == "DEZ" AND VAL(RIGHT(FIELD(i),4)) = YEAR(DATE())
						   				lcNomeColuna = "ucrsEvovendasSoftReis." + ALLTRIM(FIELD(i))
						      			IF TYPE("ucrsTempFile.DEZ_1")=='N'
											Replace ucrsTempFile.DEZ_1 WITH &lcNomeColuna
										ELSE
											Replace ucrsTempFile.DEZ_1 WITH VAL(&lcNomeColuna)
										ENDIF
										
									** Ano Anterior
									
									CASE ALLTRIM(LEFT(FIELD(i),3)) == "JAN" AND VAL(RIGHT(FIELD(i),4)) != YEAR(DATE())
						      			lcNomeColuna = "ucrsEvovendasSoftReis." + ALLTRIM(FIELD(i))
							      		IF TYPE("ucrsTempFile.JAN_2")=='N'
											Replace ucrsTempFile.JAN_2 WITH &lcNomeColuna
										ELSE
											Replace ucrsTempFile.JAN_2 WITH VAL(&lcNomeColuna)
										ENDIF
						      			
						      		CASE ALLTRIM(LEFT(FIELD(i),3)) == "FEV" AND VAL(RIGHT(FIELD(i),4)) != YEAR(DATE())
						      			lcNomeColuna = "ucrsEvovendasSoftReis." + ALLTRIM(FIELD(i))
							      		IF TYPE("ucrsTempFile.FEV_2")=='N'
											Replace ucrsTempFile.FEV_2 WITH &lcNomeColuna
										ELSE
											Replace ucrsTempFile.FEV_2 WITH VAL(&lcNomeColuna)
										ENDIF
						      		
						      		CASE ALLTRIM(LEFT(FIELD(i),3)) == "MAR" AND VAL(RIGHT(FIELD(i),4)) != YEAR(DATE())
							      		lcNomeColuna = "ucrsEvovendasSoftReis." + ALLTRIM(FIELD(i))
							      		IF TYPE("ucrsTempFile.MAR_2")=='N'
											Replace ucrsTempFile.MAR_2 WITH &lcNomeColuna
										ELSE
											Replace ucrsTempFile.MAR_2 WITH VAL(&lcNomeColuna)
										ENDIF
						      			
						      		CASE ALLTRIM(LEFT(FIELD(i),3)) == "ABR" AND VAL(RIGHT(FIELD(i),4)) != YEAR(DATE())
								  		lcNomeColuna = "ucrsEvovendasSoftReis." + ALLTRIM(FIELD(i))
							      		IF TYPE("ucrsTempFile.ABR_2")=='N'
											Replace ucrsTempFile.ABR_2 WITH &lcNomeColuna
										ELSE
											Replace ucrsTempFile.ABR_2 WITH VAL(&lcNomeColuna)
										ENDIF
						      			
						      		CASE ALLTRIM(LEFT(FIELD(i),3)) == "MAI" AND VAL(RIGHT(FIELD(i),4)) != YEAR(DATE())
										lcNomeColuna = "ucrsEvovendasSoftReis." + ALLTRIM(FIELD(i))
							      		IF TYPE("ucrsTempFile.MAI_2")=='N'
											Replace ucrsTempFile.MAI_2 WITH &lcNomeColuna
										ELSE
											Replace ucrsTempFile.MAI_2 WITH VAL(&lcNomeColuna)
										ENDIF
						      			
						      		CASE ALLTRIM(LEFT(FIELD(i),3)) == "JUN" AND VAL(RIGHT(FIELD(i),4)) != YEAR(DATE())
						      			lcNomeColuna = "ucrsEvovendasSoftReis." + ALLTRIM(FIELD(i))
							      		&&Replace ucrsTempFile.JUN_2 WITH ALLTRIM(STR(&lcNomeColuna,9,0))	
							      		IF TYPE("ucrsTempFile.JUN_2")=='N'
											Replace ucrsTempFile.JUN_2 WITH &lcNomeColuna
										ELSE
											Replace ucrsTempFile.JUN_2 WITH VAL(&lcNomeColuna)
										ENDIF

						      		CASE ALLTRIM(LEFT(FIELD(i),3)) == "JUL" AND VAL(RIGHT(FIELD(i),4)) != YEAR(DATE())
										lcNomeColuna = "ucrsEvovendasSoftReis." + ALLTRIM(FIELD(i))
							      		IF TYPE("ucrsTempFile.JUL_2")=='N'
											Replace ucrsTempFile.JUL_2 WITH &lcNomeColuna
										ELSE
											Replace ucrsTempFile.JUL_2 WITH VAL(&lcNomeColuna)
										ENDIF
						      			
						      		CASE ALLTRIM(LEFT(FIELD(i),3)) == "AGO" AND VAL(RIGHT(FIELD(i),4)) != YEAR(DATE())
										lcNomeColuna = "ucrsEvovendasSoftReis." + ALLTRIM(FIELD(i))
							      		IF TYPE("ucrsTempFile.AGO_2")=='N'
											Replace ucrsTempFile.AGO_2 WITH &lcNomeColuna
										ELSE
											Replace ucrsTempFile.AGO_2 WITH VAL(&lcNomeColuna)
										ENDIF
										
						      		CASE ALLTRIM(LEFT(FIELD(i),3)) == "SET" AND VAL(RIGHT(FIELD(i),4)) != YEAR(DATE())
							      		lcNomeColuna = "ucrsEvovendasSoftReis." + ALLTRIM(FIELD(i))
					      				IF TYPE("ucrsTempFile.SET_2")=='N'
											Replace ucrsTempFile.SET_2 WITH &lcNomeColuna
										ELSE
											Replace ucrsTempFile.SET_2 WITH VAL(&lcNomeColuna)
										ENDIF
										
						      		CASE ALLTRIM(LEFT(FIELD(i),3)) == "OUT" AND VAL(RIGHT(FIELD(i),4)) != YEAR(DATE())
							      		lcNomeColuna = "ucrsEvovendasSoftReis." + ALLTRIM(FIELD(i))
					      				IF TYPE("ucrsTempFile.OUT_2")=='N'
											Replace ucrsTempFile.OUT_2 WITH &lcNomeColuna
										ELSE
											Replace ucrsTempFile.OUT_2 WITH VAL(&lcNomeColuna)
										ENDIF
										
						      		CASE ALLTRIM(LEFT(FIELD(i),3)) == "NOV" AND VAL(RIGHT(FIELD(i),4)) != YEAR(DATE())
										lcNomeColuna = "ucrsEvovendasSoftReis." + ALLTRIM(FIELD(i))
					      				IF TYPE("ucrsTempFile.NOV_2")=='N'
											Replace ucrsTempFile.NOV_2 WITH &lcNomeColuna
										ELSE
											Replace ucrsTempFile.NOV_2 WITH VAL(&lcNomeColuna)
										ENDIF
										
						      		CASE ALLTRIM(LEFT(FIELD(i),3)) == "DEZ" AND VAL(RIGHT(FIELD(i),4)) != YEAR(DATE())
							      		lcNomeColuna = "ucrsEvovendasSoftReis." + ALLTRIM(FIELD(i))
					      				IF TYPE("ucrsTempFile.DEZ_2")=='N'
											Replace ucrsTempFile.DEZ_2 WITH &lcNomeColuna
										ELSE
											Replace ucrsTempFile.DEZ_2 WITH VAL(&lcNomeColuna)
										ENDIF

								ENDCASE

							NEXT i

						ENDIF

						Select ucrsEvovendasSoftReis
					ENDSCAN

					regua(0, lcMeses+1, "A processar importa��o do ficheiro evovendas...")
					regua(1, 24, "A processar importa��o do ficheiro evovendas...")

					lcMeses = evovendas.meses.value

					** Actualizar cursor com dados da Fprod
					uf_evovendas_actualizaDadosFprod()

					**

					FOR i=1 TO 12 && ano atual
						IF DATE(YEAR(DATE()),i,DAY(DATE())) >= GOMONTH(DATE(),-lcMeses)
							lcMesAbrev = uf_gerais_getMonthToMonthAbrev(i)
							regua(1, i+1 ,"A processar importa��o do ficheiro evovendas... " + lcMesAbrev + STR(YEAR(DATE())))
							lcValidaInsertACerto = uf_evovendas_CriaAcertoStockEvovendasMesExcel(lcMesAbrev + "_1",DATE(YEAR(DATE()),i,1))
						ENDIF
					ENDFOR

					FOR i=1 TO 12 && ano anterior
						IF DATE(YEAR(DATE())-1,i,DAY(DATE())) >= GOMONTH(DATE(),-lcMeses)
							lcMesAbrev = uf_gerais_getMonthToMonthAbrev(i)
							regua(1, 13+i ,"A processar importa��o do ficheiro evovendas... " + lcMesAbrev + STR(YEAR(DATE())))
							lcValidaInsertACerto = uf_evovendas_CriaAcertoStockEvovendasMesExcel(lcMesAbrev + "_2",DATE(YEAR(DATE())-1,i,1))
						ENDIF
					ENDFOR

				ENDIF
			ENDIF

		&& SoftReis s/MSOffice
		CASE ALLTRIM(evovendas.software.value) == "SoftReis - (Exporta��o s/MSOffice)"
			CREATE cursor ucrsTempFile (CPR	c(18), NOM c(200), FAP c(50), SAC n(10);
			,STM n(10), SMI n(10), PVP c(25), PCU c(25), IVA c(25);
			,JAN_1 n(10), FEV_1 n(10), MAR_1 n(10), ABR_1 n(10), MAI_1 n(10), JUN_1 n(10), JUL_1 n(10), AGO_1 n(10), SET_1 n(10), OUT_1 n(10), NOV_1 n(10), DEZ_1 n(10);
			,JAN_2 n(10), FEV_2 n(10), MAR_2 n(10), ABR_2 n(10), MAI_2 n(10), JUN_2 n(10), JUL_2 n(10), AGO_2 n(10), SET_2 n(10), OUT_2 n(10), NOV_2 n(10), DEZ_2 n(10);
			,lab c(150), familia c(18), faminome c(60), marca c(20), tabiva n(2,0), taxaiva n(5,2))

			SELECT ucrsTempFile

			UF_GERAIS_xls2Cursor(evovendas.local.value,"Mapa de Evolu��o de Vendas","ucrsEvovendasSoftReis")

			LOCAL lcUltMes, lcMesAbrev
			IF USED("ucrsEvovendasSoftReis")
				IF RECCOUNT("ucrsEvovendasSoftReis") > 0
					SELECT ucrsEvovendasSoftReis
					GO TOP
					SCAN
						IF !ISNULL(ucrsEvovendasSoftReis.Texto8)
							SELECT ucrsTempFile
							APPEND BLANK

							Replace ucrsTempFile.cpr WITH astr(ucrsEvovendasSoftReis.Texto8)
							Replace ucrsTempFile.NOM WITH IIF(ISNULL(ucrsEvovendasSoftReis.Text151), "", ucrsEvovendasSoftReis.Text151)
							Replace ucrsTempFile.SAC WITH IIF(ISNULL(ucrsEvovendasSoftReis.Text422), 0, ucrsEvovendasSoftReis.Text422)
							Replace ucrsTempFile.PVP WITH ALLTRIM(STR(ucrsEvovendasSoftReis.Text349,9,2))

							** Percorrer as �ltimas 12 colunas correspondentes aos �ltimos 12 meses
							SELECT ucrsEvovendasSoftReis
							i = 19
							lcUltMes = VAL(right(left(lcData,6),2))
							trocaAno = .f.
							
							DO WHILE i >= 8

								SELECT ucrsEvovendasSoftReis
								lcColunaOrigem = "ucrsEvovendasSoftReis." + ALLTRIM(FIELD(i))

								lcMesAbrev = uf_gerais_getMonthToMonthAbrev(lcUltMes)
								IF trocaAno && Ano anterior
									lcColunaDestino = "ucrsTempFile." + lcMesAbrev + "_2"
								ELSE && Ano atual
									lcColunaDestino = "ucrsTempFile." + lcMesAbrev + "_1"
								ENDIF

								SELECT ucrsTempFile
								Replace &lcColunaDestino WITH IIF(ISNULL(&lcColunaOrigem), 0, &lcColunaOrigem)

								** se o m�s for janeiro ent�o este � o �ltimo m�s do ano atual, pr�ximo registo passa para o ano anterior
								SELECT ucrsEvovendasSoftReis
								IF lcUltMes == 1
									trocaAno = .t.
									lcUltMes = 12
								ELSE
									lcUltMes = lcUltMes - 1
								ENDIF

								i = i-1
								MESSAGE(i)
							ENDDO
						ENDIF

						SELECT ucrsEvovendasSoftReis
					ENDSCAN

*!*	SELECT ucrsTempFile
*!*	BROWSE

					** Actualizar cursor com dados da Fprod
					uf_evovendas_actualizaDadosFprod()

					regua(0, lcMeses+1, "A processar importa��o do ficheiro evovendas...")
					regua(1, 24, "A processar importa��o do ficheiro evovendas...")

					lcMeses = evovendas.meses.value

					FOR i=1 TO 12 && ano anterior
						IF DATE(YEAR(DATE())-1,i,DAY(DATE())) >= GOMONTH(DATE(),-lcMeses)
							lcMesAbrev = UPPER(uf_gerais_getMonthToMonthAbrev(i))
							regua(1, 13+i, "A processar importa��o do ficheiro evovendas... " + lcMesAbrev + STR(YEAR(DATE())-1))
							lcValidaInsertACerto = uf_evovendas_CriaAcertoStockEvovendasMesExcel(lcMesAbrev + "_2",DATE(YEAR(DATE())-1,i,1))
						ENDIF
					ENDFOR

					FOR i=1 TO 12 && ano atual
						IF DATE(YEAR(DATE()),i,DAY(DATE())) >= GOMONTH(DATE(),-lcMeses)
							lcMesAbrev = UPPER(uf_gerais_getMonthToMonthAbrev(i))
							regua(1, i+1, "A processar importa��o do ficheiro evovendas... " + lcMesAbrev + STR(YEAR(DATE())))
							lcValidaInsertACerto = uf_evovendas_CriaAcertoStockEvovendasMesExcel(lcMesAbrev + "_1",DATE(YEAR(DATE()),i,1))
						ENDIF
					ENDFOR
				ENDIF
			ENDIF

		** 4DigitalCare
		CASE ALLTRIM(evovendas.software.value) == "4DigitalCare"

			CREATE cursor ucrsTempFile (CPR	c(18), NOM c(200), FAP c(50), SAC n(10);
			,STM n(10), SMI n(10), PVP c(50), PCU c(50), IVA c(50);
			,JAN_1 n(10), FEV_1 n(10), MAR_1 n(10), ABR_1 n(10), MAI_1 n(10), JUN_1 n(10), JUL_1 n(10), AGO_1 n(10), SET_1 n(10), OUT_1 n(10), NOV_1 n(10), DEZ_1 n(10);
			,JAN_2 n(10), FEV_2 n(10), MAR_2 n(10), ABR_2 n(10), MAI_2 n(10), JUN_2 n(10), JUL_2 n(10), AGO_2 n(10), SET_2 n(10), OUT_2 n(10), NOV_2 n(10), DEZ_2 n(10);
			,lab c(150), familia c(18), faminome c(60), marca c(20), tabiva n(2,0), taxaiva n(5,2))

			&&,JAN_1 c(50), FEV_1 c(50), MAR_1 c(50), ABR_1 c(50), MAI_1 c(50), JUN_1 c(50), JUL_1 c(50), AGO_1 c(50), SET_1 c(50), OUT_1 c(50), NOV_1 c(50), DEZ_1 c(50);
			&&,JAN_2 c(50), FEV_2 c(50), MAR_2 c(50), ABR_2 c(50), MAI_2 c(50), JUN_2 c(50), JUL_2 c(50), AGO_2 c(50), SET_2 c(50), OUT_2 c(50), NOV_2 c(50), DEZ_2 c(50);
			
			SELECT ucrsTempFile

			UF_GERAIS_xls2Cursor(evovendas.local.value,"Sheet","ucrsEvovendas4DigitalCare")

			IF USED("ucrsEvovendas4DigitalCare")
				IF RECCOUNT("ucrsEvovendas4DigitalCare") > 0

					LOCAL lcDataIni, lcDataFim, anoVendas, trocaAno
					trocaAno = .f.
					lcDataFim = uf_gerais_getDate(evovendas.data.value,"DATE")
					lcMeses = evovendas.meses.value
					lcDataIni = gomonth(CTOD(lcDataFim),-lcMeses)
					lcDataIni = uf_gerais_getDate(DTOS(lcDataIni),"DATE")
					anoVendas = YEAR(CTOD(lcDataFim))

					Select ucrsEvovendas4DigitalCare
					GO TOP
					SCAN
						IF !ISNULL(ucrsEvovendas4DigitalCare.codigo)
							IF uf_gerais_isNumeric(ALLTRIM(ucrsEvovendas4DigitalCare.codigo))
								SELECT ucrsTempFile
								APPEND BLANK

								Replace ucrsTempFile.cpr WITH ALLTRIM(ucrsEvovendas4DigitalCare.Codigo)
								Replace ucrsTempFile.NOM WITH IIF(ISNULL(ucrsEvovendas4DigitalCare.Designa��o), "", ucrsEvovendas4DigitalCare.Designa��o)
&&								Replace ucrsTempFile.SAC WITH IIF(ISNULL(ucrsEvovendas4DigitalCare.Exist_), "0", ALLTRIM(STR(ucrsEvovendas4DigitalCare.Exist_)))
								Replace ucrsTempFile.SAC WITH IIF(ISNULL(ucrsEvovendas4DigitalCare.Exist_), 0, ucrsEvovendas4DigitalCare.Exist_)
								Replace ucrsTempFile.PCU WITH IIF(ISNULL(ucrsEvovendas4DigitalCare.Pre�o), "0", ALLTRIM(STR(ucrsEvovendas4DigitalCare.Pre�o,9,2))) 
								** Validar necessidade do PVP
								Replace ucrsTempFile.PVP WITH '0'

								** Percorrer as �ltimas 12 colunas correspondentes aos �ltimos 12 meses
								SELECT ucrsEvovendas4DigitalCare
								i = 19
								trocaAno = .f.
								DO WHILE i >= 8

									SELECT ucrsEvovendas4DigitalCare
									lcColunaOrigem = "ucrsEvovendas4DigitalCare." + ALLTRIM(FIELD(i))

									IF trocaAno && Ano anterior
										lcColunaDestino = "ucrsTempFile." + LEFT(FIELD(i),3) + "_2"
									ELSE && Ano atual
										lcColunaDestino = "ucrsTempFile." + LEFT(FIELD(i),3) + "_1"
									ENDIF

									SELECT ucrsTempFile
									&&Replace &lcColunaDestino WITH IIF(ISNULL(&lcColunaOrigem), '0', ALLTRIM(STR(&lcColunaOrigem,9,0)))
									Replace &lcColunaDestino WITH IIF(ISNULL(&lcColunaOrigem), 0, &lcColunaOrigem)

									** se o m�s for janeiro ent�o este � o �ltimo m�s do ano atual, pr�ximo registo passa para o ano anterior
									SELECT ucrsEvovendas4DigitalCare
									IF UPPER(LEFT(FIELD(i),3)) == "JAN"
										trocaAno = .t.
									ENDIF
									i = i-1
								ENDDO
							ENDIF
						ENDIF

						SELECT ucrsEvovendas4DigitalCare
					ENDSCAN

					** Actualizar cursor com dados da Fprod
					uf_evovendas_actualizaDadosFprod()

					regua(0, lcMeses+1, "A processar importa��o do ficheiro evovendas...")
					regua(1, 24, "A processar importa��o do ficheiro evovendas...")

					lcMeses = evovendas.meses.value

					FOR i=1 TO 12 && ano anterior
						IF DATE(YEAR(DATE())-1,i,DAY(DATE())) >= GOMONTH(DATE(),-lcMeses)
							lcMesAbrev = UPPER(uf_gerais_getMonthToMonthAbrev(i))
							regua(1, 13+i, "A processar importa��o do ficheiro evovendas... " + lcMesAbrev + STR(YEAR(DATE())-1))
							lcValidaInsertACerto = uf_evovendas_CriaAcertoStockEvovendasMesExcel(lcMesAbrev + "_2",DATE(YEAR(DATE())-1,i,1))
						ENDIF
					ENDFOR

					FOR i=1 TO 12 && ano atual
					
						IF DATE(YEAR(DATE()),i,DAY(DATE())) >= GOMONTH(DATE(),-lcMeses)
							lcMesAbrev = UPPER(uf_gerais_getMonthToMonthAbrev(i))
							regua(1, i+1, "A processar importa��o do ficheiro evovendas... " + lcMesAbrev + STR(YEAR(DATE())))
							lcValidaInsertACerto = uf_evovendas_CriaAcertoStockEvovendasMesExcel(lcMesAbrev + "_1",DATE(YEAR(DATE()),i,1))
						ENDIF
					ENDFOR
				ENDIF
			ENDIF
	ENDCASE

	IF lcValidaInsertACerto
		** Update directo na ST
		uf_evovendas_ActualizaStockAposMovimentosSaida()
		**
		uf_EVOVENDAS_ActHistorico()

		regua(2)

		uf_perguntalt_chama("Processo conclu�do com sucesso.", "OK", "", 64)	
	ELSE
		regua(2)		
	ENDIF
ENDFUNC


**
FUNCTION uf_evovendas_actualizaDadosFprod
	
	regua(0,3,"A verificar dados do dicionario...")
	regua(1,1,"A verificar dados do dicionario...")
	
	IF !USED("ucrsDadosFprod")
		** Carrega Informa��o da Fprod
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			select 
				cnp
				,design
				,titaimdescr
				,u_marca
				,u_familia
				,u_tabiva
				,faminome = ISNULL(stfami.nome,'') 
				,taxaiva = ISNULL(taxasiva.taxa,0)
				,fprod.pvporig
			from 
				fprod (nolock)
				left join stfami (nolock) on fprod.u_familia = stfami.ref
				left join taxasiva (nolock) on taxasiva.codigo = fprod.u_tabiva
			--order by cnp
		ENDTEXT 
		
		IF !uf_gerais_actGrelha("","ucrsDadosFprod",lcSql)
			uf_perguntalt_chama("N�O FOI POSSIVER VERIFICAR DADOS DO DICIONARIO.","OK","",16)

			regua(2)
			RETURN .f.
		ENDIF
	ENDIF

	IF !USED("ucrsTempFile")
		regua(2)
		RETURN .f.		
	ENDIF 

	regua(1,2,"A verificar dados do dicionario...")

	UPDATE ucrsTempFile;
    SET ;
    	ucrsTempFile.NOM = ALLTRIM(ucrsDadosFprod.design);
   	 	,ucrsTempFile.lab = ALLTRIM(ucrsDadosFprod.titaimdescr);
   	 	,ucrsTempFile.familia = ALLTRIM(ucrsDadosFprod.u_familia);
		,ucrsTempFile.faminome = ALLTRIM(ucrsDadosFprod.faminome);
		,ucrsTempFile.marca = ALLTRIM(ucrsDadosFprod.u_marca);
		,ucrsTempFile.tabiva = ucrsDadosFprod.u_tabiva;
		,ucrsTempFile.taxaiva = ucrsDadosFprod.taxaiva;
    FROM ucrsTempFile;
    inner join ucrsDadosFprod on ALLTRIM(ucrsDadosFprod.cnp) == ALLTRIM(ucrsTempFile.CPR)

	regua(1,3,"A verificar dados do dicionario...")
	regua(2)
	
	SELECT ucrsTempFile
	uf_evovendas_ActualizaST()
ENDFUNC 


** Elimina documentos anteriores
FUNCTION uf_evovendas_verificaSobreposicao
	LOCAL lcSQL

	lcMeses = evovendas.meses.value
	lcData = uf_gerais_getDate(evovendas.data.value,"SQL")

	&& verifica se tem documentos
	lcSQL = ''	
	TEXT TO lcSql NOSHOW textmerge
		SELECT bostamp FROM bo WHERE ndos = 6 and obs = 'DOCUMENTO GERADO POR IMPORTA��O EVOVENDAS' and dataobra between DATEADD(MM,-<<lcMeses>>,'<<lcData>>') and '<<lcData>>'
	ENDTEXT
	IF !uf_gerais_actGrelha("","uCrsAuxBo",lcSQL)
		uf_perguntalt_chama("N�O FOI POSSIVER VERIFICAR EXISTENCIA DE DOCUMENTOS DE CONSUMO INTERNO. CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.	
	ENDIF
	
	
	&& Elimina documentos
	IF !EMPTY("uCrsAuxBo")
		regua(0,reccount("uCrsAuxBo"),"A eliminar documentos anteriores.")
		SELECT uCrsAuxBo
		GO TOP
		SCAN 
			regua(1,RECNO("uCrsAuxBo"),"A eliminar documentos anteriores.")
			lcSQL = ''	
			TEXT TO lcSql NOSHOW textmerge
				delete from bi WHERE bostamp = '<<uCrsAuxBo.bostamp>>'
				delete from bi2 WHERE bostamp = '<<uCrsAuxBo.bostamp>>'
				delete from bo2 WHERE bo2stamp = '<<uCrsAuxBo.bostamp>>'
				delete from bo WHERE bostamp = '<<uCrsAuxBo.bostamp>>'
			ENDTEXT
			IF !uf_gerais_actGrelha("","uCrsDadosEvoSobreposicoes",lcSql)
				uf_perguntalt_chama("N�O FOI POSSIVER VERIFICAR EXISTENCIA DE DOCUMENTOS DE CONSUMO INTERNO. CONTACTE O SUPORTE.","OK","",16)
				regua(2)
				RETURN .f.
			ENDIF
		ENDSCAN
		regua(2)
	ENDIF

	return .t.
ENDFUNC


**
FUNCTION uf_evovendas_ActualizaST
	Local lcValidaSt, lcSql, lcStamp, lcSqlFinal
	LOCAL lcContador, lcContadorRegua, lcTotalRegistos
	STORE 0 TO lcContador, lcContadorRegua

	Store .f. to lcValidaSt
	STORE '' TO lcSql, lcStamp, lcSqlFinal

	SET POINT TO "."

	lcTotalRegistos = reccount("ucrsTempFile")
	regua(0,reccount("ucrsTempFile"),"A verificar existencia de produtos...")
	regua(1,1,"A verificar existencia de produtos...")
	
	&& CRIAR FICHA DO PRODUTO
	Select ucrsTempFile
	Go TOP
	Scan for !empty(ucrsTempFile.CPR)

		lcContadorRegua = lcContadorRegua + 1
		lcContador = lcContador + 1

		lcRef = strtran(ALLTRIM(ucrsTempFile.CPR),chr(39),'')
		lcDesign = strtran(ALLTRIM(ucrsTempFile.NOM),chr(39),'')
		lcPVP =  STRTRAN(IIF(empty(ucrsTempFile.PVP),'0',ucrsTempFile.PVP), ',', '.')
		lcPCU =  STRTRAN(IIF(empty(ucrsTempFile.PCU),'0',ucrsTempFile.PCU), ',', '.')
		lcIVA =  STRTRAN(IIF(empty(ucrsTempFile.IVA),'0',ucrsTempFile.IVA), ',', '.')
	
		lcTabIva = iif(EMPTY(ucrsTempFile.tabiva), 4, ucrsTempFile.tabiva)
		lcTabIvaTaxa = IIF(EMPTY(ucrsTempFile.taxaiva), 0, ucrsTempFile.taxaiva)
	
		&&lcSAC = STRTRAN(IIF(empty(ucrsTempFile.SAC),'0',ucrsTempFile.SAC), ',', '.')
		&&lcSTMax = STRTRAN(IIF(empty(ucrsTempFile.STM),'0',ucrsTempFile.STM), ',', '.')
		&&lcSTMin = STRTRAN(IIF(empty(ucrsTempFile.SMI),'0',ucrsTempFile.SMI), ',', '.')
		
		lcSAC = IIF(empty(ucrsTempFile.SAC),0,ucrsTempFile.SAC)
		lcSTMax = IIF(empty(ucrsTempFile.STM),'0',ucrsTempFile.STM)
		lcSTMin = IIF(empty(ucrsTempFile.SMI),'0',ucrsTempFile.SMI)
		lcStamp = uf_gerais_stamp()
				
		TEXT TO lcSql NOSHOW TEXTMERGE
			if not exists (select ref from st where st.ref = '<<ALLTRIM(lcRef)>>')
			begin 
				INSERT INTO st
				(
					ststamp,ref,design,epv1,epcusto,epcpond,epcult
					,ivaincl,st.iva1incl,st.iva2incl,st.iva3incl,st.iva4incl,st.iva5incl
					,st.tabiva,stmax,ptoenc,familia,faminome,usr1
					,ousrdata,ousrhora,ousrinis,usrdata,usrhora,usrinis
					,marg1,marg2,marg3,marg4,u_lab, site_nr				 
				)
				Select 
					ststamp = '<<ALLTRIM(lcStamp)>>'
					,ref = '<<ALLTRIM(lcRef)>>'
					,design = '<<LEFT(ALLTRIM(lcDesign),60)>>'
					,epv1 = <<lcPVP>>
					,epcusto = <<lcPCU>>
					,epcpond = <<lcPCU>>
					,epcult = <<lcPCU>>
					,ivaincl = 1
					,iva1incl = 1
					,iva2incl = 1 
					,iva3incl = 1
					,iva4incl = 1
					,iva5incl = 1
					,tabiva = <<lcTabIva>>
					,stmax = <<lcSTMax>>
					,ptoenc = <<lcSTMin>>
					,familia = '<<ALLTRIM(ucrsTempFile.familia)>>'
					,faminome = '<<ALLTRIM(ucrsTempFile.faminome)>>'
					,usr1 = '<<ALLTRIM(ucrsTempFile.marca)>>'
					,ousrdata = convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
					,ousrhora = convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108)
					,ousrinis = '<<m_chinis>>'
					,usrdata = convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
					,usrhora = convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108)
					,usrinis = '<<m_chinis>>'	
					,marg1    = ISNULL(case when <<lcPVP>> > 0 AND ROUND(<<lcPCU>>,2)>0
			                    then ROUND( ((<<lcPVP>> / (<<lcTabIvaTaxa>>/100+1) / round(<<lcPCU>>,2)) -1) * 100 ,2)
			                    ELSE 0 end,0)
			       	,marg2    = ISNULL(case when ROUND(<<lcPCU>>,2)>0 and ROUND(<<lcPCU>>,2)>0
			                    then ROUND( ((round(<<lcPCU>>,2) / round(<<lcPCU>>,2)) -1) * 100 ,2)
			                    ELSE 0 end,0)
					,marg3    = ISNULL(case when <<lcPVP>>>0 and ROUND(<<lcPCU>>,2)>0
			                    then ROUND( ((<<lcPVP>> / (<<lcTabIvaTaxa>>/100+1) / round(<<lcPCU>>,2)) -1) * 100 ,2)
			                    ELSE 0 end,0)
			     	,marg4    = ISNULL(case when <<lcPVP>> > 0
			                    then ROUND( (<<lcPVP>> / (<<lcTabIvaTaxa>>/100+1)) - round(<<lcPCU>>,2) ,2)
			                    ELSE 0 end,0)
			        ,u_lab 	 = '<<ALLTRIM(ucrsTempFile.lab)>>'
			        ,site_nr = <<mysite_nr>>
			END 
			ELSE
			BEGIN 
				update 
					ST
				set 
					design = '<<LEFT(ALLTRIM(lcDesign),60)>>'
					,epv1 = <<lcPVP>>
					,epcusto = <<lcPCU>>
					,epcpond = <<lcPCU>>
					,epcult = <<lcPCU>>
					,st.tabiva = <<lcTabIva>>
					,stmax = <<lcSTMax>>
					,ptoenc = <<lcSTMin>>
					,usrdata = convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
					,usrhora  =convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108)
					,usrinis = '<<m_chinis>>'
					,marg1    = ISNULL(case when epv1>0 AND ROUND(epcusto,2)>0
			                    then ROUND( ((st.epv1 / (taxasiva.taxa/100+1) / round(st.epcusto,2)) -1) * 100 ,2)
			                    ELSE 0 end,0)
			       	,marg2    = ISNULL(case when ROUND(st.epcusto,2)>0 and ROUND(st.epcpond,2)>0
			                    then ROUND( ((round(st.epcusto,2) / round(st.epcpond,2)) -1) * 100 ,2)
			                    ELSE 0 end,0)
					,marg3    = ISNULL(case when st.epv1>0 and ROUND(st.epcpond,2)>0
			                    then ROUND( ((st.epv1 / (taxasiva.taxa/100+1) / round(st.epcpond,2)) -1) * 100 ,2)
			                    ELSE 0 end,0)
			     	,marg4    = ISNULL(case when st.epv1>0
			                    then ROUND( (st.epv1 / (taxasiva.taxa/100+1)) - round(st.<<ALLTRIM(uf_gerais_getGrossProfitAtribute())>>,2) ,2)
			                    ELSE 0 end,0)
			        ,familia = '<<ALLTRIM(ucrsTempFile.familia)>>'
					,faminome = '<<ALLTRIM(ucrsTempFile.faminome)>>'
					,usr1 = '<<ALLTRIM(ucrsTempFile.marca)>>'
			        ,u_lab 	 = '<<ALLTRIM(ucrsTempFile.lab)>>'
				From
					st
					left join taxasiva on taxasiva.codigo = st.tabiva
				Where 
					st.ref = '<<ALLTRIM(lcRef)>>'
					and st.site_nr = <<mysite_nr>>
			END
		ENDTEXT
	
		lcSqlFinal  = lcSqlFinal  + CHR(10) + CHR(13) + lcSQL 
	
		IF lcContador > 100
			regua(1,lcContadorRegua ,"A atualizar dados produto: " + ASTR(lcContadorRegua) + " de " + ASTR(lcTotalRegistos))
			
			IF !EMPTY(lcSqlFinal)
				IF !uf_gerais_actgrelha("", "", lcSqlFinal)
					uf_perguntalt_chama("OCORREU UM ERRO AO INSERIR O ARTIGO! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
					regua(2)
					return .f.
				ENDIF
			ENDIF 
			lcContador = 0
			lcSqlFinal  = ""
		ENDIF

	ENDSCAN

	IF !EMPTY(lcSqlFinal)
		IF !uf_gerais_actgrelha("", "", lcSqlFinal)
			uf_perguntalt_chama("OCORREU UM ERRO AO INSERIR O ARTIGO! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			regua(2)
			return .f.
		ENDIF
	ENDIF 
		
	lcStampOcorr = uf_gerais_stamp()
	uf_evovendas_actualizaOcorrencias()
	
	regua(2)
	return .t.
ENDFUNC




**
FUNCTION uf_evovendas_ActualizaStockAposMovimentosSaida
	Local lcValidaSt, lcSql, lcStamp, lcSqlFinal  
	LOCAL lcContador, lcContadorRegua,lcTotalRegistos 
	STORE 0 TO lcContador, lcContadorRegua 
	
	
	Store .f. to lcValidaSt
	STORE '' TO lcSql, lcStamp, lcSqlFinal  

	SET POINT TO "."
	lcTotalRegistos = reccount("ucrsTempFile")
	regua(0,reccount("ucrsTempFile"),"A calcular Stock...")
	regua(1,1,"A calcular Stock...")
	
	&& CRIAR FICHA DO PRODUTO
	Select ucrsTempFile
	Go Top
	Scan for !empty(ucrsTempFile.CPR)
		
		lcContadorRegua = lcContadorRegua +1
		lcContador = lcContador + 1 
			
		lcRef = strtran(ALLTRIM(ucrsTempFile.CPR),chr(39),'')
		lcSAC = IIF(empty(ucrsTempFile.SAC),0,ucrsTempFile.SAC)
				
		TEXT TO lcSql NOSHOW TEXTMERGE
			update ST set stock = <<lcSAC>> From st Where st.ref = '<<ALLTRIM(lcRef)>>'
			update sa set sa.stock = <<lcSAC>> from sa WHERE sa.ref = '<<ALLTRIM(lcRef)>>'
		ENDTEXT
	
		lcSqlFinal  = lcSqlFinal  + CHR(10) + CHR(13) + lcSQL 
	
		IF lcContador > 100

			regua(1,lcContadorRegua ,"A calcular Stock..." + ASTR(lcContadorRegua) + " de " + ASTR(lcTotalRegistos))
	
			IF !EMPTY(lcSqlFinal)
				IF !uf_gerais_actgrelha("", "", lcSqlFinal)
					uf_perguntalt_chama("OCORREU UM ERRO AO INSERIR O ARTIGO! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
					regua(2)
					return .f.
				ENDIF
			ENDIF 
			lcContador = 0
			lcSqlFinal  = ""
		ENDIF

	ENDSCAN

	IF !EMPTY(lcSqlFinal)
		IF !uf_gerais_actgrelha("", "", lcSqlFinal)
			uf_perguntalt_chama("OCORREU UM ERRO AO INSERIR O ARTIGO! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			regua(2)
			return .f.
		ENDIF
	ENDIF 
		
	regua(2)
	return .t.
ENDFUNC


**
Function uf_evovendas_actualizaOcorrencias
	
	lcStampOcorr = uf_gerais_stamp()
	
	TEXT TO lcSql NOSHOW TEXTMERGE
		insert into B_ocorrencias 
		(
			Stamp, linkStamp, tipo, Grau, descr, oValor,dValor, usr, date, terminal, site
		)
		values 
		(
			'<<ALLTRIM(lcStampOcorr)>>'
			,''
			,'Stocks'
			,3
			,'Importa��o Ficheiro Evovendas'
			,''
			,''
			,<<ch_userno>>
			,dateadd(HOUR, <<difhoraria>>, getdate())
			,<<mytermno>>
			,'<<mysite>>'
		)
	ENDTEXT

	IF !uf_gerais_actgrelha("", "", lcSql)
		uf_perguntalt_chama("OCORREU UM ERRO AO ATUALIZAR OCORRENCIAS! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	ENDIF
ENDFUNC



Function uf_evovendas_CriaAcertoStockEvovendasMes
	LPARAMETERS lcMes, lcData

	LOCAL lcSQLFinal, lcContador
	lcSQLFinal = ""
	lcContador = 0

	Local lcValorIva1, lcValorIva2, lcValorIva3, lcValorIva4, lcValorIva5, lcValorIva6, lcValorIva7, lcValorIva8, lcValorIva9;
	,lcTotalSemIvaDi1, lcTotalSemIvaDi2, lcTotalSemIvaDi3, lcTotalSemIvaDi4, lcTotalSemIvaDi5, lcTotalSemIvaDi6, lcTotalSemIvaDi7;
	,lcTotalSemIvaDi8, lcTotalSemIvaDi9, lcCusto, lcQtSum, lcTotalIva, lcEstadoDocBO, lcdoccont,lcTotalSemIvaDi,lctotalComIva,lctotalSemIva,LCTOTALFINAL

	STORE 0 TO lcValorIva1,lcValorIva2,lcValorIva3,lcValorIva4,lcValorIva5,lcValorIva6,lcValorIva7,lcValorIva8,lcValorIva9;
	,lcTotalSemIvaDi1, lcTotalSemIvaDi2, lcTotalSemIvaDi3, lcTotalSemIvaDi4, lcTotalSemIvaDi5, lcTotalSemIvaDi6;
	,lcTotalSemIvaDi7, lcTotalSemIvaDi8, lcTotalSemIvaDi9, lcCusto, lcQtSum, lcTotalIva, lcdoccont, lcEstadoDocBO, lcTotalSemIvaDi1,lcTotalSemIvaDi;
	,lctotalComIva,lctotalSemIva,LCTOTALFINAL

	** Guardar n�mero do documento **
	IF uf_gerais_actGrelha("",[uCrsNrMax],[select ISNULL((MAX(convert(numeric,obrano))),0) as nr from bo (nolock) where bo.ndos=6])
		IF RECCOUNT()>0
			lcNrDoc = uCrsNrMax.nr + 1
		ENDIF
		fecha("uCrsNrMax")
	ELSE
		uf_perguntalt_chama("O DOCUMENTO [CONSUMO INTERNO] N�O EST� A SER ENCONTRADO. O DOCUMENTO N�O SER� CRIADO.","OK","",16)
		RETURN .f.
	ENDIF

	lcbostamp = uf_gerais_stamp()
	lcDocCode = 6
	lcDocNome = 'Consumo Interno'
	lcNome = "Pessoal"
	lcNo = 1
	lcEstab = 0
	lcMorada = ""
	lcLocal = ""
	lcCodPost = ""
	lcNcont = ""
	lcMoeda = "EURO"


	** TOTAIS
	SELECT 	ucrsTempFile
	GO TOP
	SCAN FOR !EMPTY(ucrsTempFile.CPR)

		lcVarQtt = "ucrsTempFile.V_" + ALLTRIM(STR(lcMes))
		lcQt = VAL(&lcVarQtt)
		lcQtAltern = lcQt
		lcTotalLiq = 0
		lcRef = ""
		lcTabIva = 1
		lcRef = ALLTRIM(ucrsTempFile.CPR)
		lcDesign = strtran(ALLTRIM(ucrsTempFile.FAP),chr(39),'')
		lcIva = VAL(ucrsTempFile.iva)
		lcPCU = VAL(ucrsTempFile.PCU)
		lcTotalLinha = VAL(ucrsTempFile.PCU)*lcQt

		lctotalComIva=lctotalComIva+ (lcTotalLinha *(lcIva/100+1))
		lctotalSemIva=lctotalSemIva+ (lcTotalLinha )
	ENDSCAN


	TEXT TO lcSql NOSHOW textmerge
		Insert into bo (bostamp, ndos, nmdos, obrano, dataobra,
				nome, nome2, [no], estab, morada, [local], codpost, ncont,
				etotaldeb, dataopen, boano, ecusto, etotal, moeda, memissao, origem, site,
				vendedor, vendnm,
				sqtt14, ebo_1tvall, ebo_2tvall, ebo_totp1, ebo_totp2,
				ebo11_bins, ebo12_bins, ebo21_bins, ebo22_bins, ebo31_bins, ebo32_bins, ebo41_bins, ebo42_bins, ebo51_bins, ebo52_bins,
				ebo11_iva, ebo12_iva, ebo21_iva, ebo22_iva, ebo31_iva, ebo32_iva, ebo41_iva, ebo42_iva, ebo51_iva, ebo52_iva,
				ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora, obs, fechada
				,pnome, pno)
		Values ('<<ALLTRIM(lcBostamp)>>', <<lcDocCode>>, '<<lcDocNome>>', <<lcNrDoc>>, STR(YEAR(DATEADD(MONTH,-<<lcMes>>,'<<lcData>>'))) +'.'+ CASE WHEN LEN(LTRIM(STR(MONTH(DATEADD(MONTH,-<<lcMes>>,'<<lcData>>')))))=1 then '0' + LTRIM(STR(MONTH(DATEADD(MONTH,-<<lcMes>>,'<<lcData>>')))) else LTRIM(STR(MONTH(DATEADD(MONTH,-<<lcMes>>,'<<lcData>>')))) end + '.01',
				'<<ALLTRIM(lcNome)>>', '', <<lcNo>>, <<lcEstab>>, '<<ALLTRIM(lcMorada)>>', '<<ALLTRIM(lcLocal)>>', '<<ALLTRIM(lcCodPost)>>', '<<ALLTRIM(lcNcont)>>',
				<<ROUND(lctotalSemIva,2)>>, 
				convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), 
				YEAR(DATEADD(MONTH,-<<lcMes>>,'<<lcData>>')), 
				<<ROUND(lctotalSemIva,2)>>, 
				<<ROUND(lctotalSemIva,2)>>, 
				'<<ALLTRIM(lcMoeda)>>', 
				'EURO', 'BO', '<<ALLTRIM(mySite)>>',
				<<ch_vendedor>>, '<<ALLTRIM(ch_vendnm)>>', 
				<<lcQtSum>>, 
				<<ROUND(lctotalSemIva,2)>>, <<ROUND(lctotalSemIva,2)>>, <<ROUND(lctotalSemIva,2)>>,<<ROUND(lctotalSemIva,2)>>,
				<<lcTotalSemIvaDi1>>,<<lcTotalSemIvaDi1>>, <<lcTotalSemIvaDi2>>, <<lcTotalSemIvaDi2>>, <<lcTotalSemIvaDi3>>, <<lcTotalSemIvaDi3>>,
				<<lcTotalSemIvaDi4>>, <<lcTotalSemIvaDi4>>, <<lcTotalSemIvaDi5>>, <<lcTotalSemIvaDi5>>, 
				<<lcValorIva1>>, <<lcValorIva1>>, <<lcValorIva2>>, <<lcValorIva2>>, <<lcValorIva3>>, <<lcValorIva3>>,
				<<lcValorIva4>>, <<lcValorIva4>>, <<lcValorIva5>>, <<lcValorIva5>>,
				'ADM', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8), 'ADM', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8),
				'DOCUMENTO GERADO POR IMPORTA��O EVOVENDAS',1
				,'<<myTerm>>','<<myTermNo>>'
		)
	ENDTEXT

*!*	_cliptext = lcSQL
*!*	messagebox(lcSQL)

	IF !uf_gerais_actGrelha("","",lcSql)
		uf_perguntalt_chama("OCORREU UM ERRO A GERAR DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.", "OK", "", 16)
		RETURN .f.
	ENDIF

	TEXT TO lcSql NOSHOW textmerge
		Insert into bo2 (bo2stamp, autotipo, pdtipo, etotalciva, armazem, ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora)
		Values ('<<ALLTRIM(lcBostamp)>>', 1, 1, <<lcTotalFinal>>, <<myArmazem>>, 'ADM', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8), 'ADM', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8))
	ENDTEXT

	IF !uf_gerais_actGrelha("","",lcSql)
		uf_perguntalt_chama("OCORREU UM ERRO A GERAR DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	ENDIF

	** INSERE LINHAS
	SELECT ucrsTempFile
	GO TOP
	SCAN FOR !EMPTY(ucrsTempFile.CPR)

		lcContador = lcContador + 1

		lcBiStamp = uf_gerais_stamp()
		** Contador para as linhas
		lcOrdem = 10000

		** Quantidade
		lcVarQtt = "ucrsTempFile.V_" + ALLTRIM(STR(lcMes))
		lcQt = VAL(&lcVarQtt)
		lcQtAltern = lcQt
		lcTotalLiq = 0
		lcRef = ""
		lcTabIva = 1
		lcRef = ALLTRIM(ucrsTempFile.CPR)
		lcDesign = strtran(ALLTRIM(ucrsTempFile.NOM),chr(39),'')
		lcIva = VAL(ucrsTempFile.iva)
		lcPCU = VAL(ucrsTempFile.PCU)
		lcTotalLinha = VAL(ucrsTempFile.PCU)*lcQt

		IF lcQt > 0
			** Preencher Linhas da Bi **
			TEXT TO lcSql NOSHOW textmerge
				Insert into bi
				(bistamp, bostamp, nmdos, obrano, ref, codigo,
				design, qtt, qtt2, uni2qtt,
				u_stockact, iva, tabiva, armazem, stipo, ndos, cpoc, familia,
				no, nome, local, morada, codpost,
				epu, edebito, eprorc, epcusto, ettdeb, ecustoind, edebitoori, u_upc,
				rdata, dataobra, dataopen, resfor, lordem,
				lobs,
				ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora
				,u_mquebra
				)
				Values ('<<ALLTRIM(lcBiStamp)>>', '<<ALLTRIM(lcBoStamp)>>', '<<Alltrim(lcDocNome)>>', <<lcNrDoc>>, '<<ALLTRIM(lcRef)>>', '',
						'<<LEFT(ALLTRIM(lcDesign),60)>>', <<lcQt>>, 0, <<lcQtAltern>>,
						0, <<lcIva>>, <<lcTabIva>>, <<myarmazem>>, 4, <<lcDocCode>>,0, '',
						<<lcNo>>, '<<ALLTRIM(lcNome)>>', '<<ALLTRIM(lcLocal)>>', '<<ALLTRIM(lcMorada)>>', '<<ALLTRIM(lcCodPost)>>',
						<<lcPCU>>,<<lcPCU>>,<<lcPCU>>,<<lcPCU>>,<<lcTotalLinha>>,<<lcPCU>>,<<lcPCU>>,<<lcPCU>>,
						STR(YEAR(DATEADD(MONTH,-<<lcMes>>,'<<lcData>>'))) +'.'+ CASE WHEN LEN(LTRIM(STR(MONTH(DATEADD(MONTH,-<<lcMes>>,'<<lcData>>')))))=1 then '0' + LTRIM(STR(MONTH(DATEADD(MONTH,-<<lcMes>>,'<<lcData>>')))) else LTRIM(STR(MONTH(DATEADD(MONTH,-<<lcMes>>,'<<lcData>>')))) end + '.01',
						STR(YEAR(DATEADD(MONTH,-<<lcMes>>,'<<lcData>>'))) +'.'+ CASE WHEN LEN(LTRIM(STR(MONTH(DATEADD(MONTH,-<<lcMes>>,'<<lcData>>')))))=1 then '0' + LTRIM(STR(MONTH(DATEADD(MONTH,-<<lcMes>>,'<<lcData>>')))) else LTRIM(STR(MONTH(DATEADD(MONTH,-<<lcMes>>,'<<lcData>>')))) end + '.01'
						, convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), 0, <<lcOrdem>>,
						'', 
						'ADM', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8), 'ADM', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8) 
						,'Hist�rico'
				)
			Endtext

			** Preencher Linhas da Bi2 **
			TEXT TO lcSqlBi2 NOSHOW textmerge
				Insert into bi2
						(bi2stamp, bostamp, morada, local, codpost)
				Values ('<<ALLTRIM(lcBiStamp)>>', '<<ALLTRIM(lcBoStamp)>>', '<<ALLTRIM(lcMorada)>>', '<<ALLTRIM(lcLocal)>>', '<<ALLTRIM(lcCodPost)>>')
			ENDTEXT


			lcSQLFinal = lcSQLFinal + CHR(13) + CHR(10) + lcSql + CHR(13) + CHR(10) + lcSqlBi2 

			IF lcContador > 50
				lcContador = 0		

				IF !uf_gerais_actGrelha("","",lcSQLFinal)
					uf_perguntalt_chama("OCORREU UM ERRO A GERAR DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
					RETURN .f.
				ENDIF
				
				lcSQLFinal = ""
			ENDIF 
		Endif 

		SELECT uCrsTempFile
	ENDSCAN

	IF !EMPTY(lcSQLFinal)
		IF !uf_gerais_actGrelha("","",lcSQLFinal)
			uf_perguntalt_chama("OCORREU UM ERRO A GERAR DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			RETURN .f.
		Endif
	ENDIF
	
	RETURN .t.
Endfunc


FUNCTION uf_evovendas_CriaAcertoStockEvovendasMesExcel
	LPARAMETERS lcVarNomeQtt, lcData

	LOCAL lcSQLFinal, lcContador 
	lcSQLFinal = ""
	lcContador = 0

	LOCAL lcVarQtt
	lcVarQtt = ""
	lcVarQtt = "ucrsTempFile." + ALLTRIM(lcVarNomeQtt)

	Local lcValorIva1, lcValorIva2, lcValorIva3, lcValorIva4, lcValorIva5, lcValorIva6, lcValorIva7, lcValorIva8, lcValorIva9;
	,lcTotalSemIvaDi1, lcTotalSemIvaDi2, lcTotalSemIvaDi3, lcTotalSemIvaDi4, lcTotalSemIvaDi5, lcTotalSemIvaDi6, lcTotalSemIvaDi7;
	,lcTotalSemIvaDi8, lcTotalSemIvaDi9, lcCusto, lcQtSum, lcTotalIva, lcEstadoDocBO, lcdoccont,lcTotalSemIvaDi,lctotalComIva,lctotalSemIva,LCTOTALFINAL

	STORE 0 TO lcValorIva1,lcValorIva2,lcValorIva3,lcValorIva4,lcValorIva5,lcValorIva6,lcValorIva7,lcValorIva8,lcValorIva9;
	,lcTotalSemIvaDi1, lcTotalSemIvaDi2, lcTotalSemIvaDi3, lcTotalSemIvaDi4, lcTotalSemIvaDi5, lcTotalSemIvaDi6;
	,lcTotalSemIvaDi7, lcTotalSemIvaDi8, lcTotalSemIvaDi9, lcCusto, lcQtSum, lcTotalIva, lcdoccont, lcEstadoDocBO, lcTotalSemIvaDi1,lcTotalSemIvaDi;
	,lctotalComIva,lctotalSemIva,LCTOTALFINAL

	** Guardar n�mero do documento **
	IF uf_gerais_actGrelha("",[uCrsNrMax],[select ISNULL((MAX(convert(numeric,obrano))),0) as nr from bo (nolock) where bo.ndos=6])
		IF RECCOUNT()>0
			lcNrDoc = uCrsNrMax.nr + 1
		ENDIF
		fecha("uCrsNrMax")
	ELSE
		uf_perguntalt_chama("O DOCUMENTO [CONSUMO INTERNO] N�O EST� A SER ENCONTRADO. O DOCUMENTO N�O SER� CRIADO.","OK","",16)
		RETURN .f.
	ENDIF

	lcbostamp = uf_gerais_stamp()
	lcDocCode = 6
	lcDocNome = 'Consumo Interno'
	lcNome = "Pessoal"
	lcNo = 1
	lcEstab = 0
	lcMorada = ""
	lcLocal = ""
	lcCodPost = ""
	lcNcont = ""
	lcMoeda = "EURO"

	** TOTAIS
	SELECT ucrsTempFile
	Go TOP
	SCAN FOR !EMPTY(ucrsTempFile.CPR)

		&&lcQt = VAL(&lcVarQtt)
		lcQt = &lcVarQtt
		lcQtSum = lcQtSum + lcQt
		lcQtAltern = lcQt 
		lcTotalLiq = 0
		lcRef = ""
		lcTabIva = 1
		lcRef = ALLTRIM(ucrsTempFile.CPR)
		lcDesign = strtran(ALLTRIM(ucrsTempFile.FAP),chr(39),'')
		lcIva = VAL(ucrsTempFile.iva)
		lcPCU = VAL(ucrsTempFile.PCU)
		lcTotalLinha = VAL(ucrsTempFile.PCU)*lcQt

		lctotalComIva=lctotalComIva+ (lcTotalLinha * (lcIva/100+1))
		lctotalSemIva=lctotalSemIva+ (lcTotalLinha )

		select ucrsTempFile
	ENDSCAN


	&& Se a quantidade total das Linhas for Zero, n�o cria o documento
	IF lcQtSum == 0
		RETURN .t.
	ENDIF 

	TEXT TO lcSql NOSHOW textmerge
		IF exists(select obs from bo where dataobra = '<<Alltrim(uf_gerais_getDate(lcData,"SQL"))>>' and obs = 'DOCUMENTO GERADO POR IMPORTA��O EVOVENDAS')
		begin
			delete from bo where dataobra = '<<Alltrim(uf_gerais_getDate(lcData,"SQL"))>>' and obs = 'DOCUMENTO GERADO POR IMPORTA��O EVOVENDAS'
			delete from bo2 WHERE bo2stamp in (select bostamp from bo WHERE dataobra = '<<Alltrim(uf_gerais_getDate(lcData,"SQL"))>>' and obs = 'DOCUMENTO GERADO POR IMPORTA��O EVOVENDAS')
			delete from bi WHERE bostamp in (select bostamp from bo WHERE dataobra = '<<Alltrim(uf_gerais_getDate(lcData,"SQL"))>>' and obs = 'DOCUMENTO GERADO POR IMPORTA��O EVOVENDAS')
			delete from bi2 WHERE bostamp in (select bostamp from bo WHERE dataobra = '<<Alltrim(uf_gerais_getDate(lcData,"SQL"))>>' and obs = 'DOCUMENTO GERADO POR IMPORTA��O EVOVENDAS')
		end


		Insert into bo (bostamp, ndos, nmdos, obrano, dataobra,
				nome, nome2, [no], estab, morada, [local], codpost, ncont,
				etotaldeb, dataopen, boano, ecusto, etotal, moeda, memissao, origem, site,
				vendedor, vendnm,
				sqtt14, ebo_1tvall, ebo_2tvall, ebo_totp1, ebo_totp2,
				ebo11_bins, ebo12_bins, ebo21_bins, ebo22_bins, ebo31_bins, ebo32_bins, ebo41_bins, ebo42_bins, ebo51_bins, ebo52_bins,
				ebo11_iva, ebo12_iva, ebo21_iva, ebo22_iva, ebo31_iva, ebo32_iva, ebo41_iva, ebo42_iva, ebo51_iva, ebo52_iva,
				ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora, obs, fechada
				,pnome, pno)
		Values ('<<ALLTRIM(lcBostamp)>>', <<lcDocCode>>, '<<lcDocNome>>', <<lcNrDoc>>, '<<Alltrim(uf_gerais_getDate(lcData,"SQL"))>>',
				'<<ALLTRIM(lcNome)>>', '', <<lcNo>>, <<lcEstab>>, '<<ALLTRIM(lcMorada)>>', '<<ALLTRIM(lcLocal)>>', '<<ALLTRIM(lcCodPost)>>', '<<ALLTRIM(lcNcont)>>',
				<<ROUND(lctotalSemIva,2)>>,
				convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102),
				YEAR('<<Alltrim(uf_gerais_getDate(lcData,"SQL"))>>'),
				<<ROUND(lctotalSemIva,2)>>,
				<<ROUND(lctotalSemIva,2)>>,
				'<<ALLTRIM(lcMoeda)>>',
				'EURO', 'BO', '<<ALLTRIM(mySite)>>',
				<<ch_vendedor>>, '<<ALLTRIM(ch_vendnm)>>',
				<<lcQtSum>>,
				<<ROUND(lctotalSemIva,2)>>, <<ROUND(lctotalSemIva,2)>>, <<ROUND(lctotalSemIva,2)>>,<<ROUND(lctotalSemIva,2)>>,
				<<lcTotalSemIvaDi1>>,<<lcTotalSemIvaDi1>>, <<lcTotalSemIvaDi2>>, <<lcTotalSemIvaDi2>>, <<lcTotalSemIvaDi3>>, <<lcTotalSemIvaDi3>>,
				<<lcTotalSemIvaDi4>>, <<lcTotalSemIvaDi4>>, <<lcTotalSemIvaDi5>>, <<lcTotalSemIvaDi5>>,
				<<lcValorIva1>>, <<lcValorIva1>>, <<lcValorIva2>>, <<lcValorIva2>>, <<lcValorIva3>>, <<lcValorIva3>>,
				<<lcValorIva4>>, <<lcValorIva4>>, <<lcValorIva5>>, <<lcValorIva5>>,
				'ADM', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8), 'ADM', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8),
				'DOCUMENTO GERADO POR IMPORTA��O EVOVENDAS',1
				,'<<myTerm>>','<<myTermNo>>'
		)
	ENDTEXT



*!*		_cliptext = lcSQL
*!*		messagebox(lcSQL)

	IF !uf_gerais_actGrelha("","",lcSql)
		uf_perguntalt_chama("OCORREU UM ERRO A GERAR DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	ENDIF

	TEXT TO lcSql NOSHOW textmerge
		Insert into bo2 (bo2stamp, autotipo, pdtipo, etotalciva, armazem, ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora)
		Values ('<<ALLTRIM(lcBostamp)>>', 1, 1, <<lcTotalFinal>>, <<myArmazem>>, 'ADM', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8), 'ADM', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8))
	ENDTEXT

*!*	_cliptext = lcSQL
*!*	messagebox(lcSQL)

	IF !uf_gerais_actGrelha("","",lcSql)
		uf_perguntalt_chama("OCORREU UM ERRO A GERAR DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	ENDIF

	lcSql = ""
	
	** INSERE LINHAS
	SELECT ucrsTempFile 
	GO TOP
	SCAN FOR !EMPTY(ucrsTempFile.CPR) 

		lcContador = lcContador +1
		lcBiStamp = uf_gerais_stamp()					
		** Contador para as linhas
		lcOrdem = 10000

		**Quantidade
		&&lcQt = VAL(&lcVarQtt)
		lcQt = &lcVarQtt
		lcQtAltern = lcQt
		lcTotalLiq = 0
		lcRef = ""
		lcTabIva = 1
		lcRef = ALLTRIM(ucrsTempFile.CPR)
		lcDesign = strtran(ALLTRIM(ucrsTempFile.NOM),chr(39),'')
		lcIva = VAL(ucrsTempFile.iva)
		lcPCU = VAL(ucrsTempFile.PCU)
		lcTotalLinha = VAL(ucrsTempFile.PCU)*lcQt

		IF lcQt > 0
			** Preencher Linhas da Bi **
			TEXT TO lcSql NOSHOW textmerge
				Insert into bi
				(bistamp, bostamp, nmdos, obrano, ref, codigo,
				design, qtt, qtt2, uni2qtt,
				u_stockact, iva, tabiva, armazem, stipo, ndos, cpoc, familia,
				no, nome, local, morada, codpost,
				epu, edebito, eprorc, epcusto, ettdeb, ecustoind, edebitoori, u_upc,
				rdata, dataobra, dataopen, resfor, lordem,
				lobs,
				ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora
				,u_mquebra
				)
				Values ('<<ALLTRIM(lcBiStamp)>>', '<<ALLTRIM(lcBoStamp)>>', '<<Alltrim(lcDocNome)>>', <<lcNrDoc>>, '<<ALLTRIM(lcRef)>>', '',
						'<<LEFT(ALLTRIM(lcDesign),60)>>', <<lcQt>>, 0, <<lcQtAltern>>,
						0, <<lcIva>>, <<lcTabIva>>, <<myarmazem>>, 4, <<lcDocCode>>,0, '',
						<<lcNo>>, '<<ALLTRIM(lcNome)>>', '<<ALLTRIM(lcLocal)>>', '<<ALLTRIM(lcMorada)>>', '<<ALLTRIM(lcCodPost)>>',
						<<lcPCU>>,<<lcPCU>>,<<lcPCU>>,<<lcPCU>>,<<lcTotalLinha>>,<<lcPCU>>,<<lcPCU>>,<<lcPCU>>,
						'<<Alltrim(uf_gerais_getDate(lcData,"SQL"))>>'
						,'<<Alltrim(uf_gerais_getDate(lcData,"SQL"))>>'
						,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
						,0
						,<<lcOrdem>>,
						'', 
						'ADM', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8), 'ADM', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8) 
						,'Hist�rico'
				)
				
				Insert into bi2
						(bi2stamp, bostamp, morada, local, codpost)
				Values ('<<ALLTRIM(lcBiStamp)>>', '<<ALLTRIM(lcBoStamp)>>', '<<ALLTRIM(lcMorada)>>', '<<ALLTRIM(lcLocal)>>', '<<ALLTRIM(lcCodPost)>>')
			ENDTEXT
			
			lcSQLFinal = lcSQLFinal + CHR(13) + CHR(10) + lcSql
		ENDIF 
	

		IF lcContador > 50
			IF !EMPTY(lcSQLFinal)

*!*	**
*!*	_CLIPTEXT = lcSQLFinal
*!*	MESSAGEBOX(lcSQLFinal)

				lcContador = 0

				IF !uf_gerais_actGrelha("","",lcSQLFinal)
					uf_perguntalt_chama("OCORREU UM ERRO A GERAR DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
					RETURN .f.
				ENDIF
				
				lcSQLFinal = ""
			ENDIF 
		ENDIF 
	ENDSCAN


	IF !EMPTY(lcSQLFinal)
		IF !uf_gerais_actGrelha("","",lcSQLFinal)
			uf_perguntalt_chama("OCORREU UM ERRO A GERAR DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			RETURN .f.
		Endif		
	ENDIF
	
	RETURN .t.
ENDFUNC



**
**
FUNCTION uf_evovendas_ActualizaStocksSifarma2000
	LOCAL lcLocal

	lcLocal = ["] + ALLTRIM(evovendas.local.value) + ["]
	IF !FILE(lcLocal)
		uf_perguntalt_chama("A localiza��o especificada para importa��o do ficheiro � inv�lida. Por favor verifique.","OK","",64)
		return .f.
	Endif
		
	IF ALLTRIM(evovendas.software.value) == "SIFARMA 2000"

		CREATE cursor ucrsTempFileAux (CPR	 c(254),NOM	 c(254),FAP c(254),LOCALIZACAO	 c(254),SAC c(254);
		,STM c(254),SMI c(254),QTE c(254),DUV c(254),DUC c(254),PVP c(254),PCU c(254),IVA c(254),CAT c(254),CT1 c(254),GEN c(254);
		,GPR c(254),CLA c(254),TPR c(254),CAR c(254),GAM	c(254),V_0 c(254),V_1 c(254),V_2 c(254),V_3 c(254),V_4 c(254),V_5 c(254);
		,V_6 c(254),V_7	c(254),V_8	c(254),V_9	c(254),V_10 c(254),V_11 c(254),V_12 c(254);
		,V_13 c(254), V_14 c(254), V_15 c(254), V_16 c(254), V_17 c(254), V_18 c(254) ,V_19 c(254);
		,V_20 c(254),V_21 c(254),V_22 c(254),V_23 c(254),DTVAL c(254),FPD c(254),LAD c(254)	,PRATELEIRA c(254),	GAMA c(254),GRUPOHOMOGENEO c(254),INACTIVO c(254),PVP5 c(254))

		SELECT ucrsTempFileAux 
		APPEND FROM &lcLocal TYPE DELIMITED WITH TAB

		Select * from ucrsTempFileAux WHERE ALLTRIM(ucrsTempFileAux.CPR) != 'CPR' AND LOCALIZACAO != "ARMAZ�M" AND ALLTRIM(LOCALIZACAO) != "Reserva de Produtos" ORDER BY CPR into CURSOR ucrsTempFile READWRITE

		uf_evovendas_ActualizaST()
		uf_perguntalt_chama("STOCKS ATUALIZADOS COM SUCESSO!","OK","",64)	
	ELSE
		uf_perguntalt_chama("Op��o disponivel apenas para o software SIFARMA 2000.","OK","",64)
	ENDIF 

ENDFUNC 




**
FUNCTION uf_EVOVENDAS_sair
	EVOVENDAS.hide
	EVOVENDAS.release
ENDFUNC
