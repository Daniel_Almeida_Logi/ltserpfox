**
FUNCTION uf_valpvpalt_chama
	SELECT crsRes
	GO TOP
	
	IF TYPE("valpvpalt") == "U"
		DO FORM valpvpalt
	ELSE
		valpvpalt.show()
	ENDIF
ENDFUNC

**
FUNCTION uf_valpvpalt_sair
	valpvpalt.hide
	valpvpalt.release
ENDFUNC
