FUNCTION uf_encautomaticas_chama
	LPARAMETERS nTipoEnc, nSavedEncStamp, nconjunta

	PUBLIC myGetEncStamp, MyTipoEncAuto, myOrderEncAutomaticas, myControlaEventoQt, myResizeEncAutomaticas, MyEncConjunta, myEncGrupo , myAlterSt
	MyEncConjunta = nconjunta
	STORE .f. TO myAlterSt
	
	IF TYPE("PREPARARENCOMENDA")!="U"	
		uf_prepararencomenda_sair()
	ENDIF 

	IF !EMPTY(nSavedEncStamp)
		myGetEncStamp = nSavedEncStamp
	ELSE
		STORE '' TO myGetEncStamp
	ENDIF

	IF !EMPTY(nTipoEnc)
		MyTipoEncAuto = nTipoEnc
	ELSE
		MyTipoEncAuto = ''
	ENDIF

	IF TYPE("ENCAUTOMATICAS")=="U"
		LOCAL lcSQL

		IF !USED("crsenc") AND MyTipoEncAuto <> "STOCKS"
			IF TYPE("ATENDIMENTO")=="U"
				TEXT TO lcSQL NOSHOW TEXTMERGE
					exec up_enc_autoref 0, '', '', <<mysite_nr>>
				ENDTEXT

			ELSE 
				SELECT ref, qtt, lordem FROM fi where !empty(fi.ref) INTO CURSOR ucrsfienc READWRITE 
				LOCAL lcListRef, lcListRef1
				STORE '' TO lcListRef, lcListRef1
				SELECT ucrsfienc 
				GO TOP 
				SCAN 
					lcListRef = ALLTRIM(lcListRef)+ALLTRIM(ucrsfienc.ref)+","
				ENDSCAN 
				**fecha("ucrsfienc")
				lcListRef = SUBSTR(lcListRef,1,LEN(lcListRef)-1)

				TEXT TO lcSQL NOSHOW TEXTMERGE
					exec up_enc_autoref_atendimento <<myArmazem>>, '<<ALLTRIM(lcListRef)>>', '', <<mysite_nr>>, 0
				ENDTEXT
	
			ENDIF 
			IF !uf_gerais_actgrelha("", "crsenc", lcSql)
				uf_perguntalt_chama("N�O FOI POSSIVEL DETERMINAR OS DADOS PARA GERAR ENCOMENDAS AUTOM�TICAS. POR FAVOR CONTACTE O SUPORTE.","OK","", 16)
				RETURN .f.
			ENDIF
			IF TYPE("ATENDIMENTO")!="U"
				LOCAL lcrefat, lcqttat, lclordemat
				STORE '' TO lcrefat
				STORE 0 TO lcqttat, lclordemat
				SELECT ucrsfienc 
				GO TOP 
				SCAN 
					lcrefat=ALLTRIM(ucrsfienc.ref)
					lcqttat=ucrsfienc.qtt
					lclordemat = ucrsfienc.lordem
					SELECT crsenc
					GO TOP 
					SCAN 
						IF ALLTRIM(crsenc.ref) = ALLTRIM(lcrefat)
							IF crsenc.lordem=0
								replace crsenc.eoq WITH lcqttat
								replace crsenc.lordem WITH lclordemat
							ELSE
								replace crsenc.eoq WITH crsenc.eoq+lcqttat
							ENDIF 
						ENDIF 
					ENDSCAN 
					
				ENDSCAN 
				fecha("ucrsfienc")
				SELECT crsenc
				GO TOP 
				
			ENDIF 
        ELSE
       	
         	IF USED("ST")
         
	            SELECT ST

	            TEXT TO lcSQL NOSHOW TEXTMERGE
	                exec up_enc_autoref_atendimento <<myArmazem>>, '<<ALLTRIM(st.ref)>>', '', <<mysite_nr>>, 0
	            ENDTEXT
	        
	            IF !uf_gerais_actgrelha("", "crsenc", lcSql)
	                uf_perguntalt_chama("N�O FOI POSSIVEL DETERMINAR OS DADOS PARA GERAR ENCOMENDAS AUTOM�TICAS. POR FAVOR CONTACTE O SUPORTE.","OK","", 16)
	                RETURN .f.
	            ENDIF

	            SELECT crsenc

	            go top

	            replace crsenc.EOQ with 1

			ENDIF

		ENDIF
		
		IF ALLTRIM(uCrsE1.tipoempresa) == "PARAFARMACIA"
			SELECT crsenc
			SET FILTER TO !ALLTRIM(crsenc.familia) == "1" AND !ALLTRIM(crsenc.familia) == "58" 
		ENDIF
		


		IF !USED("ucrsFornceEncAut")
			lcSQL = ""
			TEXT TO lcSQL NOSHOW TEXTMERGE
				SELECT
					nome	= ''
					,no		= 0
					,estab	= 0
					,nome2 	= ''
				
				union all

				Select 
					nome
					,no
					,estab
					,nome2
				from 
					fl (nolock)
				order by
					nome
			ENDTEXT
			IF !uf_gerais_actgrelha("", "ucrsFornceEncAut", lcSql)
				RETURN .f.
				uf_perguntalt_chama("OCORREU UMA ANOMALIA NA VERIFICA��O DE FORNECEDORES. POR FAVOR CONTACTE O SUPORTE.","OK","", 16)
			ENDIF
		ENDIF

		&& Cursor informacao bonus para o B2B
		lcSQL = ''
		TEXT TO lcSQL TEXTMERGE NOSHOW 
			exec up_stocks_verBonus 0, '999999999'
		ENDTEXT

		IF !uf_gerais_actGrelha("", "uCrsBonusResumoB2B", lcSQL)
			MESSAGEBOX("DESCULPE, MAS N�O FOI POSS�VEL CRIAR OS BONUS PARA O B2B. POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
			RETURN .f.
		ENDIF 

		SELECT crsenc
		GO TOP

		**
		DO FORM ENCAUTOMATICAS

		&&Centra
		ENCAUTOMATICAS.autocenter = .t.
		ENCAUTOMATICAS.windowstate = 2
		
		ENCAUTOMATICAS.show

	ELSE
		**
		ENCAUTOMATICAS.show
	ENDIF
	
*!*		ENCAUTOMATICAS.Detalhest1.Pageframe1.PAGE9.chart1.left = ENCAUTOMATICAS.Detalhest1.Pageframe1.PAGE9.chart1.left-150
*!*		ENCAUTOMATICAS.Detalhest1.Pageframe1.PAGE9.label1.left = ENCAUTOMATICAS.Detalhest1.Pageframe1.PAGE9.label1.left-150
*!*		ENCAUTOMATICAS.Detalhest1.Pageframe1.PAGE9.label6.left = ENCAUTOMATICAS.Detalhest1.Pageframe1.PAGE9.label6.left-150
*!*		ENCAUTOMATICAS.Detalhest1.Pageframe1.PAGE9.label8.left = ENCAUTOMATICAS.Detalhest1.Pageframe1.PAGE9.label8.left-150
*!*		ENCAUTOMATICAS.Detalhest1.Pageframe1.PAGE9.vendasultmes.left = ENCAUTOMATICAS.Detalhest1.Pageframe1.PAGE9.vendasultmes.left-150
*!*		ENCAUTOMATICAS.Detalhest1.Pageframe1.PAGE9.vendasultmesgh.left = ENCAUTOMATICAS.Detalhest1.Pageframe1.PAGE9.vendasultmesgh.left-150
*!*		**ENCAUTOMATICAS.Detalhest1.Pageframe1.PAGE9.btnimg5.left = ENCAUTOMATICAS.Detalhest1.Pageframe1.PAGE9.btnimg5.left-150
*!*		**ENCAUTOMATICAS.Detalhest1.Pageframe1.PAGE9.btnimg6.left = ENCAUTOMATICAS.Detalhest1.Pageframe1.PAGE9.btnimg6.left-150
*!*		ENCAUTOMATICAS.Detalhest1.Pageframe1.PAGE9.btnimg5.visible=.f.
*!*		ENCAUTOMATICAS.Detalhest1.Pageframe1.PAGE9.btnimg6.visible=.f.
*!*		ENCAUTOMATICAS.Detalhest1.Pageframe1.PAGE9.btnimg1.left = ENCAUTOMATICAS.Detalhest1.Pageframe1.PAGE9.btnimg1.left-165
	
*!*		IF ENCAUTOMATICAS.Detalhest1.Pageframe1.PAGE9.grdbonusresumo.left = 722
*!*			ENCAUTOMATICAS.Detalhest1.Pageframe1.PAGE9.grdbonusresumo.left = ENCAUTOMATICAS.Detalhest1.Pageframe1.PAGE9.grdbonusresumo.left-150
*!*			ENCAUTOMATICAS.Detalhest1.Pageframe1.PAGE9.LblLeg1.left = ENCAUTOMATICAS.Detalhest1.Pageframe1.PAGE9.LblLeg1.left-180
*!*			ENCAUTOMATICAS.Detalhest1.Pageframe1.PAGE9.LblLeg2.left = ENCAUTOMATICAS.Detalhest1.Pageframe1.PAGE9.LblLeg2.left-180
*!*		ENDIF 
	
*!*		WITH ENCAUTOMATICAS.GridPesq
*!*			.documento.DisabledBackColor = RGB(255,255,255)
*!*			.numDoc.DisabledBackColor = RGB(255,255,255)
*!*			.nome.DisabledBackColor = RGB(255,255,255)
*!*			.data.DisabledBackColor = RGB(255,255,255)
*!*			.estado.DisabledBackColor = IIF(ALLTRIM(DOCUMENTOS.containerCab.estado.value) == "A",Rgb[232,76,61],RGB(255,255,255))
*!*			.estado.ForeColor = IIF(ALLTRIM(DOCUMENTOS.containerCab.estado.value) == "A",RGB(255,255,255),RGB(0,0,0))
*!*			.estado.FontBold = IIF(ALLTRIM(DOCUMENTOS.containerCab.estado.value) == "A",.t.,.f.)
*!*		ENDWITH

	** calcula totais
	uf_encautomaticas_ActualizaQtValorEnc()
	
	uf_encautomaticas_format_rodapebonus()
	
	uf_encautomaticas_configgrdenc()
ENDFUNC


**
FUNCTION uf_encautomaticas_CarregaMenu
	&& configura menu
	ENCAUTOMATICAS.menu1.adicionaOpcao("opcoes", "Op��es", myPath + "\imagens\icons\opcoes_w.png", "","O")
	**ENCAUTOMATICAS.menu1.adicionaOpcao("consultab2b", "Consulta B2B", myPath + "\imagens\icons\detalhe_micro.png", "uf_encautomaticas_consultab2b_singleline","B")
	**ENCAUTOMATICAS.menu1.adicionaOpcao("atualizar", "Pesquisar", myPath + "\imagens\icons\actualizar_w.png", "uf_encautomaticas_aplicaFiltros","A")
	ENCAUTOMATICAS.menu1.adicionaOpcao("selTodos", "Sel. Todos", myPath + "\imagens\icons\unchecked_w.png", "uf_encautomaticas_SelTodos","T")
	ENCAUTOMATICAS.menu1.adicionaOpcao("selFornecedor", "Fornecedor", myPath + "\imagens\icons\forn_lupa_w.png", "uf_encautomaticas_AplicarFornecedor","F")	
	ENCAUTOMATICAS.menu1.adicionaOpcao("encTodos", "Enc. Todos", myPath + "\imagens\icons\unchecked_w.png", "uf_encautomaticas_EncTodos","E")
	ENCAUTOMATICAS.menu1.adicionaOpcao("guardarEnc", "Guardar Prep.Enc.", myPath + "\imagens\icons\doc_ef_w.png", "uf_encautomaticas_GuardaEstadoEncomenda","U")
	**ENCAUTOMATICAS.menu1.adicionaOpcao("infoFornec", "Consulta Forn.", myPath + "\imagens\icons\detalhe_micro.png", "uf_stocks_consultarProd with 'Encomendas'","F")
**	ENCAUTOMATICAS.menu1.adicionaOpcao("subir", "Subir", myPath + "\imagens\icons\ponta_seta_up.png", "uf_gerais_MovePage with .t.,'ENCAUTOMATICAS.GridPesq','crsenc'","05")
**	ENCAUTOMATICAS.menu1.adicionaOpcao("descer", "Descer", myPath + "\imagens\icons\ponta_seta_down.png", "uf_gerais_MovePage with .f.,'ENCAUTOMATICAS.GridPesq','crsenc'","24")
**	ENCAUTOMATICAS.menu1.adicionaOpcao("getinfolinb2b","Consulta Forn.",myPath + "\imagens\icons\actualizar_w.png","uf_encautomaticas_consultab2b_infolinha","")
	
	&&Sub-Menu
	ENCAUTOMATICAS.menu_opcoes.adicionaOpcao("encAberto", "Encomendas em Aberto", "", "uf_encautomaticas_EncomendaAberto")
	ENCAUTOMATICAS.menu_opcoes.adicionaOpcao("multiloja", IIF(MyEncConjunta == .f.,"Ver detalhe Multi-Loja","Ver detalhe Loja �nica"), "", "uf_encautomaticas_MultiLoja")
	ENCAUTOMATICAS.menu_opcoes.adicionaOpcao("verFicha", "Ver Ficha Produto", "", "uf_encautomaticas_chamaSt")
	ENCAUTOMATICAS.menu_opcoes.adicionaOpcao("infoFornec", "Consulta Forn.", "", "uf_stocks_consultarProd with 'Encomendas'")
	IF TYPE("MyTipoEncAuto") <> 'U' AND uf_gerais_compStr("MANUAL", MyTipoEncAuto)
		ENCAUTOMATICAS.menu_opcoes.adicionaOpcao("importXls", "Importar XLS", "", "uf_encautomaticas_importXls")
	ENDIF

	encautomaticas.menu1.estado("", "", "Gravar", .t., "Sair", .t.)
ENDFUNC


**
FUNCTION uf_encautomaticas_MultiLoja
	PUBLIC MyEncConjunta
	
	IF MyEncConjunta == .f.
		MyEncConjunta = .t.
		ENCAUTOMATICAS.menu_opcoes.multiloja.config("Ver detalhe Loja �nica","","uf_encautomaticas_MultiLoja")
	ELSE
		MyEncConjunta = .f.
		ENCAUTOMATICAS.menu_opcoes.multiloja.config("Ver detalhe Multi-Loja","","uf_encautomaticas_MultiLoja")
	ENDIF
	
	ENCAUTOMATICAS.detalhest1.myActivateResumoRef = ""
	ENCAUTOMATICAS.detalhest1.myActivate5baratos = ""
	ENCAUTOMATICAS.detalhest1.myActivatebonusref = ""
	ENCAUTOMATICAS.detalhest1.myActivateesgotadosref = ""
	ENCAUTOMATICAS.detalhest1.myActivatehistprecoscompras = ""
	ENCAUTOMATICAS.detalhest1.myActivatesaidasref = ""
	ENCAUTOMATICAS.detalhest1.myActivatehistprecosref = ""
		
	ENCAUTOMATICAS.detalhest1.refresh
	ENCAUTOMATICAS.refresh
ENDFUNC 


**
FUNCTION uf_encautomaticas_criaCursorProcura
	SELECT crsenc
	GO TOP

	SELECT ref FROM crsenc WHERE (LIKE ('*'+ALLTRIM(UPPER(ENCAUTOMATICAS.textPesq.value))+'*',ALLTRIM(UPPER(crsenc.ref))) OR ;
			LIKE ('*'+ALLTRIM(UPPER(ENCAUTOMATICAS.textPesq.value))+'*',ALLTRIM(UPPER(crsenc.design)))) AND ;
			!EMPTY(ALLTRIM(UPPER(ENCAUTOMATICAS.textPesq.value))) INTO CURSOR ucrsTempPesq

	SELECT ucrsTempPesq

	SELECT crsenc		
	GO TOP
	SCAN
		IF (LIKE ('*'+ALLTRIM(UPPER(ENCAUTOMATICAS.textPesq.value))+'*',ALLTRIM(UPPER(crsenc.ref))) OR ;
			LIKE ('*'+ALLTRIM(UPPER(ENCAUTOMATICAS.textPesq.value))+'*',ALLTRIM(UPPER(crsenc.design)))) AND ;
			!EMPTY(ALLTRIM(UPPER(ENCAUTOMATICAS.textPesq.value)))
			
			replace crsenc.PESQUISA WITH .t.
		ELSE
			replace crsenc.PESQUISA WITH .f.
		ENDIF
	ENDSCAN
	
	uf_encautomaticas_procuraLinhaSeguinte()
ENDFUNC


**
FUNCTION uf_encautomaticas_procuraLinhaSeguinte
	IF !USED("ucrsTempPesq")
		RETURN .f.
	ENDIF
	
	lcref = ucrsTempPesq.ref
	select ucrsTempPesq
	TRY
		goto recno() +1
	CATCH
	 	GO TOP
	ENDTRY
	
	SELECT ucrsTempPesq
	SELECT crsenc
	SCAN
		IF crsenc.ref == ucrsTempPesq.ref

			EXIT
		ENDIF
	ENDSCAN

	ENCAUTOMATICAS.GridPesq.refresh
ENDFUNC


**
FUNCTION uf_encautomaticas_aplicaFiltros
	Local lcFiltro, lcGenerico

	WITH ENCAUTOMATICAS
		** controlo de genericos
		IF !EMPTY(UPPER(ALLTRIM(.Genericos.value)))
			IF UPPER(ALLTRIM(.Genericos.value)) == "GEN�RICOS"
				lcGenerico = 1
			ELSE
				lcGenerico = 2
			ENDIF
		ELSE
			lcGenerico = 0
		ENDIF

		** fonte
		IF UPPER(ALLTRIM(.fonte.value)) == "DICION�RIO / INTERNO"
			lcFiltro = '(ALLTRIM(crsenc.u_fonte) == "D" OR ALLTRIM(crsenc.u_fonte) == "I")'
		ELSE
			IF UPPER(ALLTRIM(.fonte.value)) == "EXTERNO"
				lcFiltro = '(ALLTRIM(crsenc.u_fonte) == "E")'
			ELSE
				lcFiltro = ''
			ENDIF
		ENDIF	

		** familia
		IF !EMPTY(UPPER(ALLTRIM(.familia.value)))
			
			** guardar filtro
			IF !EMPTY(lcFiltro)
			&&lcFiltro = lcFiltro + ' ' + [ AND ALLTRIM(UPPER(crsenc.faminome)) ==  ALLTRIM(UPPER(ENCAUTOMATICAS.familia.value))] 
				UV_FILTRO = ''
				FOR i = 1 TO ALINES(ua_filtrofamilia, ALLTRIM(UPPER(ENCAUTOMATICAS.familia.value)),",")
					IF(!EMPTY(ua_filtrofamilia[i]))
						UV_FILTRO = UV_FILTRO + IIF(!EMPTY(UV_FILTRO), " OR ", " ")+ " ALLTRIM(UPPER(crsenc.faminome)) ==  '"+ua_filtrofamilia[i]+"'" 					
					ENDIF	
				NEXT
				
				IF(!EMPTY(UV_FILTRO))
					lcFiltro = lcFiltro + ' ' + [ AND (]+UV_FILTRO+[)]
				ENDIF			
			ELSE

				&&lcFiltro = [ ALLTRIM(UPPER(crsenc.faminome)) ==  ALLTRIM(UPPER(ENCAUTOMATICAS.familia.value))] 
				UV_FILTRO = ''
				FOR i = 1 TO ALINES(ua_filtrofamilia, ALLTRIM(UPPER(ENCAUTOMATICAS.familia.value)),",")
					IF(!EMPTY(ua_filtrofamilia[i]))
						UV_FILTRO = UV_FILTRO + IIF(!EMPTY(UV_FILTRO), " OR ", " ")+ " ALLTRIM(UPPER(crsenc.faminome)) ==  '"+ua_filtrofamilia[i]+"'" 					
					ENDIF	
				NEXT
				IF(!EMPTY(UV_FILTRO))
					lcFiltro = lcFiltro + ' ' + UV_FILTRO
				ENDIF
				
			ENDIF
		ENDIF
		
		** tipoProduto
		IF !EMPTY(UPPER(ALLTRIM(.tipoProduto.value)))
			** guardar filtro
			IF !EMPTY(lcFiltro)
				lcFiltro = lcFiltro + ' ' + [ AND ALLTRIM(UPPER(crsenc.tipoProduto)) ==  ALLTRIM(UPPER(ENCAUTOMATICAS.tipoProduto.value))] 
			ELSE
				lcFiltro = [ ALLTRIM(UPPER(crsenc.tipoProduto)) ==  ALLTRIM(UPPER(ENCAUTOMATICAS.tipoProduto.value))] 
			ENDIF
		ENDIF

		** generico
		IF !EMPTY(lcGenerico)
			SELECT crsenc
			IF lcGenerico = 1
				IF !EMPTY(lcFiltro)
					lcFiltro = lcFiltro + ' ' + 'AND crsenc.generico == .t.'
				ELSE
					lcFiltro = 'crsenc.generico == .t.'
				ENDIF
			ELSE
				IF !EMPTY(lcFiltro)
					lcFiltro = lcFiltro + ' ' + 'AND crsenc.generico == .f.'
				ELSE
					lcFiltro = 'crsenc.generico == .f.'
				ENDIF
			ENDIF
		ENDIF

		** fornecedor
		If !EMPTY(.no.value)
			** guardar filtro 
			IF !EMPTY(lcFiltro)
				lcFiltro = lcFiltro + ' ' + 'AND (crsenc.fornec = ENCAUTOMATICAS.no.value AND crsenc.fornestab = ENCAUTOMATICAS.estab.value)'
			ELSE
				lcFiltro = '(crsenc.fornec = ENCAUTOMATICAS.no.value AND crsenc.fornestab = ENCAUTOMATICAS.estab.value)'
			ENDIF
		ENDIF
	

		** GRUPO HOMOGENEO
		If !EMPTY(.gh.value)
			** guardar filtro
			IF !EMPTY(lcFiltro)
				lcFiltro = lcFiltro + ' ' + [ AND ALLTRIM(UPPER(crsenc.grphmgcode)) $  ALLTRIM(UPPER(ENCAUTOMATICAS.gh.value))] 
			ELSE
				lcFiltro = [ ALLTRIM(UPPER(crsenc.grphmgcode)) $  ENCAUTOMATICAS.gh.value] 
			ENDIF
		ENDIF

		** LABORAT�RIO
		If !EMPTY(.laboratorio.value)
			** guardar filtro 
			IF !EMPTY(lcFiltro)
				lcFiltro = lcFiltro + ' ' + [ AND ALLTRIM(UPPER(crsenc.u_lab)) $  ALLTRIM(UPPER(ENCAUTOMATICAS.laboratorio.value))]
			ELSE
				lcFiltro = [ ALLTRIM(UPPER(crsenc.u_lab)) $  ALLTRIM(UPPER(ENCAUTOMATICAS.laboratorio.value))] &&'(crsenc.u_lab= ENCAUTOMATICAS.laboratorio.value)'
			ENDIF
		ENDIF

		** MARCA
		If !EMPTY(.marca.value)
			** guardar filtro (fornecedor)
			IF !EMPTY(lcFiltro)
				lcFiltro = lcFiltro + ' ' + [ AND  ALLTRIM(UPPER(crsenc.marca)) $  ALLTRIM(UPPER(ENCAUTOMATICAS.marca.value))] 
			ELSE
				lcFiltro = [ ALLTRIM(UPPER(crsenc.marca)) $  ALLTRIM(UPPER(ENCAUTOMATICAS.marca.value))] 
			ENDIF
		ENDIF

		** Psicotr�picos/Benzodiazepinas
		If !EMPTY(.psicobenzo.value)
			
			** guardar filtro (fornecedor)
			IF !EMPTY(lcFiltro)
				lcFiltro = lcFiltro + ' ' + 'AND (crsenc.psico = .t. OR crsenc.benzo = .t.)'
			ELSE
				lcFiltro = '(crsenc.psico = .t. OR crsenc.benzo = .t.)'
			ENDIF
		ENDIF

		** Produtos com Reservas de Clientes
		If !EMPTY(.reservas.value)
			** guardar filtro (fornecedor)
			IF !EMPTY(lcFiltro)
				IF ALLTRIM(.reservas.value)='Produtos Reservados por Clientes'
					lcFiltro = lcFiltro + ' ' + 'AND (crsenc.qttcli > 0)'
				ELSE
					lcFiltro = lcFiltro + ' ' + 'AND (crsenc.qttcli = 0)'
				ENDIF 
			ELSE
				IF ALLTRIM(.reservas.value)='Produtos Reservados por Clientes'
					lcFiltro = '(crsenc.qttcli > 0)'
				ELSE
					lcFiltro = '(crsenc.qttcli = 0)'
				ENDIF
			ENDIF
		ENDIF
		
		** Tipo Encomenda
		If !EMPTY(.tipoEncomenda.value)
			** guardar filtro (tipoEncomenda)
			IF !EMPTY(lcFiltro)
				IF ALLTRIM(.tipoEncomenda.value)='Esgotados'
					lcFiltro =  lcFiltro + ' ' + [ AND  ALLTRIM(UPPER(crsenc.Esgotado)) $  ALLTRIM(UPPER("S"))] 
				ELSE 
					IF ALLTRIM(.tipoEncomenda.value)='Sem Esgotados' 
						lcFiltro = lcFiltro + ' ' + [ AND  ALLTRIM(UPPER(crsenc.Esgotado)) !=  ALLTRIM(UPPER("S"))]
					ENDIF  
				ENDIF 
			ELSE
				IF ALLTRIM(.tipoEncomenda.value)='Esgotados'
					lcFiltro =  [ ALLTRIM(UPPER(crsenc.Esgotado)) $  ALLTRIM(UPPER("S"))] 
				ELSE  
					IF ALLTRIM(.tipoEncomenda.value)='Sem Esgotados' 
						lcFiltro = [ ALLTRIM(UPPER(crsenc.Esgotado)) !=  ALLTRIM(UPPER("S"))]
					ENDIF  
				ENDIF 
			ENDIF 
		ENDIF
		
		** Produtos de Parafarm�cia
		IF .chkParafarmacia.tag = "true"
			** guardar filtro
			IF !EMPTY(lcFiltro)
				lcFiltro = lcFiltro + ' ' + 'AND (!ALLTRIM(crsenc.familia) == "1") AND (!ALLTRIM(crsenc.familia) == "58") '
			ELSE
				lcFiltro = '(!ALLTRIM(crsenc.familia) == "1" AND !ALLTRIM(crsenc.familia) == "58" )'
			ENDI
		ENDIF	

			
		** manter sempre os j� seleccionados no filtro
*!*			IF !EMPTY(lcFiltro)
*!*				lcFiltro = lcFiltro + ' ' + 'OR crsenc.enc=.t.'
*!*			ENDIF
SET EXACT OFF
		** aplicar filtro		
		SELECT crsenc
		SET FILTER TO &lcFiltro
SET EXACT ON

		SELECT crsenc
		GO TOP
	
		ENCAUTOMATICAS.gridPesq.refresh
	ENDWITH	
	uf_encautomaticas_configgrdenc()
ENDFUNC


**
FUNCTION uf_encautomaticas_ActualizaQtValorEnc
	LOCAL lcPos, lcQt, lcValor, lcQtRef
	STORE 0 TO lcPos, lcQt, lcValor, lcQtRef
	
	IF USED("CRSenc")
		SELECT crsenc
		lcPos = RECNO()

		SELECT crsenc
		CALCULATE SUM(crsenc.eoq) FOR crsenc.enc TO lcQt
		IF TYPE("MyTipoEncAuto") <> "U" AND uf_gerais_compStr("MANUAL", MyTipoEncAuto)
			CALCULATE SUM((crsenc.epcusto - ((crsenc.epcusto * crsenc.descontoForn)/100))*(crsenc.eoq-crsenc.qtbonus)) FOR crsenc.enc TO lcValorPCLForn
		ELSE
			CALCULATE SUM(IIF(crsenc.pclfornec==0,crsenc.epcult,crsenc.pclfornec)*(crsenc.eoq-crsenc.qtbonus)) FOR crsenc.enc TO lcValorPCLForn && Alterado a pedido do Hugo em 22.01.2014
		ENDIF
		CALCULATE SUM((crsenc.epcusto - ((crsenc.epcusto * crsenc.descontoForn)/100))*(crsenc.eoq-crsenc.qtbonus)) FOR crsenc.enc TO lcValor
		CALCULATE count() FOR crsenc.enc AND crsenc.eoq>0 TO lcQtRef
		
		**CALCULATE SUM(crsenc.epcusto*crsenc.eoq) FOR crsenc.enc TO lcValor
		
		ENCAUTOMATICAS.containerSubTotais.qtsum.value = lcQt
*!*			IF uf_gerais_getParameter("ADM0000000213","BOOL") == .t. and uf_gerais_getParameter("ADM0000000214","BOOL") == .f. && Encomendas Keiretsu
		ENCAUTOMATICAS.containerSubTotais.valorsum.value = lcValorPCLForn
		ENCAUTOMATICAS.containerSubTotais.qtref.value = lcQtRef
*!*			ELSE
*!*				ENCAUTOMATICAS.containerSubTotais.valorsum.value = lcValor
*!*			ENDIF		
		ENCAUTOMATICAS.containerSubTotais.qtsum.refresh
		ENCAUTOMATICAS.containerSubTotais.valorsum.refresh
		ENCAUTOMATICAS.containerSubTotais.qtref.refresh
		
		SELECT crsenc
		IF lcPos>0
			try
				GO lcPos
			CATCH
				GO TOP
			ENDTRY
		ENDIF
	ENDIF
ENDFUNC


**
FUNCTION uf_encautomaticas_EncTodos
	IF EMPTY(encautomaticas.menu1.enctodos.tag) OR encautomaticas.menu1.enctodos.tag == "false"
		&& aplica sele��o
		REPLACE ALL enc WITH .t. IN crsenc
		
		&& altera o botao
		encautomaticas.menu1.enctodos.tag = "true"
		encautomaticas.menu1.enctodos.config("Enc. Todos", myPath + "\imagens\icons\checked_w.png", "uf_encautomaticas_EncTodos","E")
		
		&& actualiza o ecra
		*pesqstocks.refresh()
	ELSE
		&& aplica sele��o
		REPLACE ALL enc WITH .f. IN crsenc
		
		&& altera o botao
		encautomaticas.menu1.enctodos.tag = "false"
		encautomaticas.menu1.enctodos.config("Enc. Todos", myPath + "\imagens\icons\unchecked_w.png", "uf_encautomaticas_EncTodos","E")
		
		&& actualiza o ecra		
		*pesqstocks.refresh()
	ENDIF
	
	uf_encautomaticas_ActualizaQtValorEnc()
		
	SELECT crsenc
	GO top
	
	uf_encautomaticas_format_rodapebonus()
ENDFUNC


**
FUNCTION uf_encautomaticas_SelTodos
	IF EMPTY(encautomaticas.menu1.seltodos.tag) OR encautomaticas.menu1.seltodos.tag == "false"
		&& aplica sele��o
		REPLACE ALL sel WITH .t. IN crsenc
		
		&& altera o botao
		encautomaticas.menu1.selTodos.tag = "true"
		encautomaticas.menu1.selTodos.config("Sel. Todos", myPath + "\imagens\icons\checked_w.png", "uf_encautomaticas_SelTodos","T")
		
		&& actualiza o ecra
		*pesqstocks.refresh()
	ELSE
		&& aplica sele��o
		REPLACE ALL sel WITH .f. IN crsenc
		
		&& altera o botao
		encautomaticas.menu1.selTodos.tag = "false"
		encautomaticas.menu1.selTodos.config("Sel. Todos", myPath + "\imagens\icons\unchecked_w.png", "uf_encautomaticas_SelTodos","T")
		
		&& actualiza o ecra		
		*pesqstocks.refresh()
	ENDIF
	
	uf_encautomaticas_ActualizaQtValorEnc()
		
	SELECT crsenc
	GO top
	
	uf_encautomaticas_format_rodapebonus()
ENDFUNC


**
FUNCTION uf_encautomaticas_alteraFornecedorSel
	LPARAMETERS nNome, nNo, nEstab, nNome2
	
	**Alertas
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE
		exec up_enc_dadosAlertasEncAutomaticas '', <<nNo>>, <<nEstab>>, <<mysite_nr>>
	ENDTEXT
	IF !uf_gerais_actGrelha("", "uCrsActualizaFornecedores", lcSQL)
		uf_perguntalt_chama("N�O VERIFICAR INFORMA��O DOS ARTIGOS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	ENDIF

	regua(0,RECCOUNT("crsenc"),"A ACTUALIZAR INFORMA��O DO FORNECEDOR...",.f.)
	SELECT crsenc
	GO TOP
	SCAN FOR crsenc.sel == .t.
	
		regua[1,recno(),"A PRODUTO: "+ALLTRIM(crsenc.ref),.f.]
		
		REPLACE crsenc.fornecedor 	WITH ALLTRIM(nNome)
		Replace crsenc.fornec 		WITH nNo
		Replace crsenc.fornestab 	WITH nEstab
		replace crsenc.fornecabrev 	WITH IIF(EMPTY(nNome2), ALLTRIM(nNome), ALLTRIM(nNome2))
		
		IF nNo == 0
			replace crsenc.enc			WITH .f.
		ELSE
		
*!*				Select uCrsActualizaFornecedores
*!*				LOCATE FOR ALLTRIM(uCrsActualizaFornecedores.ref) == ALLTRIM(crsenc.ref)
*!*				IF FOUND()
*!*		            Replace crsenc.bonusfornec    	WITH uCrsActualizaFornecedores.bonus
*!*		            Replace crsenc.pclfornec    	WITH uCrsActualizaFornecedores.pcl
*!*		            Replace crsenc.esgotado    		WITH uCrsActualizaFornecedores.esgotado
*!*					replace crsenc.alertaStockGH 	WITH uCrsActualizaFornecedores.alertaStockGH
*!*					replace crsenc.alertaPCL 		WITH uCrsActualizaFornecedores.alertaPCL 	
*!*					replace crsenc.alertaBonus 		WITH uCrsActualizaFornecedores.alertaBonus 
*!*				ENDIF 
			
			replace crsenc.enc			WITH .t.
		Endif
	ENDSCAN
	

	** Actualiza Dados
	UPDATE crsenc;
	SET crsenc.bonusfornec = IIF(ALLTRIM(uCrsActualizaFornecedores.bonus)<>'','S','');
		,crsenc.pclfornec = uCrsActualizaFornecedores.pcl;
		,crsenc.esgotado = IIF(UPPER(uCrsActualizaFornecedores.esgotado)='SIM', 'S', '');
		,crsenc.alertaStockGH = uCrsActualizaFornecedores.alertaStockGH;
		,crsenc.alertaPCL = uCrsActualizaFornecedores.alertaPCL;
		,crsenc.alertaBonus = uCrsActualizaFornecedores.alertaBonus;
	From;
		crsenc inner join uCrsActualizaFornecedores on crsenc.ref = uCrsActualizaFornecedores.ref;
	Where;
		crsenc.sel = .t.
		

	regua(2)
	SELECT crsenc
	GO top
	SCAN
		replace crsenc.sel WITH .f.
	ENDSCAN
	
	SELECT crsenc
	GO TOP
ENDFUNC


**
FUNCTION uf_encautomaticas_chamaSt
	If Used("crsenc")
		Select crsenc
		If !Empty(Alltrim(crsenc.ref))
			uf_stocks_chama(crsenc.ref)
		Endif
	ENDIF
ENDFUNC


**
FUNCTION uf_encautomaticas_adicionaProd

    uf_pesqStocks_chama("ENCAUTOMATICAS")

ENDFUNC


**
FUNCTION uf_encautomaticas_EliminaProd
	IF !USED("crsenc")
		RETURN .f.
	ENDIF

	LOCAL lcRef, lcPos
	lcPos = recno()
	
	Select crsenc
	lcRef = crsenc.ref
		
	IF !EMPTY(lcRef)
		Select crsenc
		LOCATE FOR ALLTRIM(crsenc.ref) == ALLTRIM(lcRef)
		DELETE
	ENDIF
	
	select crsenc
	TRY 
		GO lcPos +1
	CATCH
		GO TOP
	ENDTRY
					
	SELECT crsenc	
	ENCAUTOMATICAS.GRIDPESQ.setfocus
	
ENDFUNC


**
FUNCTION uf_encautomaticas_InteractiveChangeFornecedor
	If Used("crsenc") AND USED("ucrsFornceEncAut")
		SELECT ucrsFornceEncAut
		Select crsenc
		Replace crsenc.fornecedor 	WITH ucrsFornceEncAut.nome
		Replace crsenc.fornec 		WITH ucrsFornceEncAut.no
		Replace crsenc.fornestab 	WITH ucrsFornceEncAut.estab
		replace crsenc.fornecabrev 	WITH ucrsFornceEncAut.nome2
	ENDIF
ENDFUNC


**
FUNCTION uf_encautomaticas_GuardaEstadoEncomenda
	
	IF !USED("crsenc")
		RETURN .f.
	ENDIF
	
	SELECT crsenc
	IF RECCOUNT("crsenc") == 0
		uf_perguntalt_chama("N�O PODE GUARDAR UMA ENCOMENDA SEM REGISTOS.", "OK","",48)
		RETURN .f.
	ENDIF
	
	LOCAL lcErro, lcTime, lcDate
	STORE .f. TO lcUpdate, lcErro
	STORE '' TO lcTime, lcDate

	IF EMPTY(myGetEncStamp)
		LOCAL lcStamp
		lcStamp = uf_gerais_stamp()
		myGetEncStamp = ALLTRIM(lcStamp)
	ENDIF

	IF !uf_perguntalt_chama("VAI PROCEDER � GRAVA��O TEMPORARIA DO ESTADO DA ENCOMENDA. PRENDE CONTINUAR?","Sim","N�o")
		RETURN .f.
	ENDIF

	SET HOURS TO 24
	Atime=ttoc(datetime()+(difhoraria*3600),2)
	Astr=left(Atime,8)
	lcTime = Astr
	**lcTime = TIME()
	lcDate = uf_gerais_getDate((datetime()+(difhoraria*3600)),"SQL")

	* a vari�vel guarda o total de registos do cursor tempcursor
	Select crsenc
	mntotal=reccount()

	* inicializa a r�gua apresentando um t�tulo e o n� total de registos
	regua(0,mntotal,"A PROCESSAR...",.f.)

	**Verifica se j� existe uma encomeda com o stamp inserido, se existir apaga para proceder � introdu��o
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		Select 
			contador = COUNT(stamp)
		from 
			B_encAutoSave (nolock)
		Where 
			B_encAutoSave.stamp = '<<ALLTRIM(myGetEncStamp)>>'
	ENDTEXT
	IF !uf_gerais_actgrelha("", "ucrsExisteEncTemp", lcSql)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA NA VERIFICA��O DE EXISTENCIA DE ENCOMENDA TEMPOR�RIA. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	ENDIF

	IF ucrsExisteEncTemp.contador > 0 &&existe vai eliminar registo da tabela temp na BD
		lcSQL = ""
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			DELETE
			from
				B_encAutoSave
			Where
				B_encAutoSave.stamp = '<<ALLTRIM(myGetEncStamp)>>'
		ENDTEXT
		IF !uf_gerais_actgrelha("", "", lcSQL)
			uf_perguntalt_chama("OCORREU UMA ANOMALIA NA VERIFICA��O DE EXISTENCIA DE ENCOMENDA TEMPOR�RIA. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			RETURN .f.
		ENDIF
	ENDIF
	***********************************************************
	
	SELECT crsenc
	GO TOP
	
		SCAN FOR !EMPTY(crsenc.ref)
			** actualizar regua **
			regua[1,recno("crsenc"),"A GUARDAR PRODUTO: "+ALLTRIM(crsenc.design),.f.]
		
			TEXT TO lcSql NOSHOW textmerge
				INSERT 
					INTO B_encAutoSave (
							stamp
							,data 
							,hora 
							,udata 
							,uhora 
							,ousr 
							,usr
							,tipoenc
							,ststamp
							,enc
							,u_fonte
							,ref
							,refb
							,qttb
							,codigo
							,design
							,stock
							,stmin
							,stmax
							,ptoenc
							,eoq
							,qtbonus
							,qttadic
							,qttfor
							,qttacin
							,qttcli
							,fornecedor
							,fornec
							,fornestab
							,sel
							,epcusto
							,epcpond
							,epcult
							,tabiva
							,iva
							,cpoc
							,familia
							,faminome
							,u_lab
							,conversao
							,generico
							,stockGH
							,PCLFornec
							,alertaStockGH
							,alertaPCL
							,alertaBonus
							,bonusFornec
							,esgotado
							,pcustoImp
							,desconto
					)
					values (
						'<<ALLTRIM(myGetEncStamp)>>'
						,'<<lcDate>>'
						,'<<ALLTRIM(lcTime)>>'
						,'<<lcDate>>'
						,'<<ALLTRIM(lcTime)>>'
						,'<<ALLTRIM(m_chinis)>>'
						,'<<ALLTRIM(m_chinis)>>'
						,'<<ALLTRIM(myTipoEncAuto)>>'
						,'<<ALLTRIM(crsenc.ststamp)>>'
						,<<IIF(crsenc.enc==.t.,1,0)>>
						,'<<ALLTRIM(crsenc.u_fonte)>>'
						,'<<ALLTRIM(crsenc.ref)>>'
						,'<<ALLTRIM(crsenc.refb)>>'
						,<<crsenc.qttb>>
						,'<<ALLTRIM(crsenc.codigo)>>'
						,'<<ALLTRIM(crsenc.design)>>'
						,<<crsenc.stock>>
						,<<crsenc.stmin>>
						,<<crsenc.stmax>>
						,<<crsenc.ptoenc>>
						,<<crsenc.eoq>>
						,<<crsenc.qtbonus>>
						,<<crsenc.qttadic>>
						,<<crsenc.qttfor>>
						,<<crsenc.qttacin>>
						,<<crsenc.qttcli>>
						,'<<ALLTRIM(crsenc.fornecedor)>>'
						,<<crsenc.fornec>>
						,<<crsenc.fornestab>>
						,<<IIF(crsenc.sel==.t.,1,0)>>
						,<<crsenc.epcusto>>
						,<<crsenc.epcpond>>
						,<<crsenc.epcult>>
						,<<crsenc.tabiva>>
						,<<crsenc.iva>>
						,<<crsenc.cpoc>>
						,'<<ALLTRIM(crsenc.familia)>>'
						,'<<ALLTRIM(crsenc.faminome)>>'
						,'<<ALLTRIM(crsenc.u_lab)>>'
						,<<crsenc.conversao>>
						,<<IIF(crsenc.generico==.t.,1,0)>>
						,<<crsenc.stockGH>>
						,<<crsenc.PCLFornec>>
						,<<crsenc.alertaStockGH>>
						,<<crsenc.alertaPCL>>
						,<<crsenc.alertaBonus>>
						,'<<ALLTRIM(crsenc.bonusFornec)>>'
						,'<<ALLTRIM(crsenc.esgotado)>>'
						,<<IIF(TYPE("crsenc.pcusto") <> "U", crsenc.pcusto, 0)>>
						,<<crsenc.descontoForn>>
						)
			ENDTEXT
			IF !uf_gerais_actgrelha("", "", lcSql)
				uf_perguntalt_chama("OCORREU UM ERRO A GUARDAR A ENCOMENDA. POR FAVOR CONTACTE O SUPORTE","OK","",16)
				lcErro=.t.
				EXIT
			ENDIF

			SELECT crsenc
		ENDSCAN

	** fechar regua
	REGUA(2)

	IF !lcErro
		uf_perguntalt_chama("ENCOMENDA GUARDADA COM SUCESSO!","OK","", 64)
		uf_encautomaticas_sair()
	ENDIF
ENDFUNC


**
FUNCTION uf_encautomaticas_AplicarFornecedor
	uf_pesqFornecedores_Chama("ENCAUTOMATICAS")
ENDFUNC


**
FUNCTION uf_encautomaticas_EncomendaAberto
	*MSG("AN�LISE DE ENCOMEDAS EM ABERTO...PAINEL DE BORDO")
ENDFUNC


**
FUNCTION uf_encautomaticas_expandeGridLinhas

	If myResizeEncAutomaticas == .f.
		
		ENCAUTOMATICAS.detalhest1.visible = .t.
		ENCAUTOMATICAS.detalhest1.pageFRAME1.page9.CHART1.Visible = .t.
		ENCAUTOMATICAS.detalhest1.pageFRAME1.page9.CHART1.click
		ENCAUTOMATICAS.shape1.visible = .t.
		ENCAUTOMATICAS.GridPesq.height = ENCAUTOMATICAS.GridPesq.height - 258
		ENCAUTOMATICAS.shape3.height = ENCAUTOMATICAS.shape3.height - 258
		ENCAUTOMATICAS.ContainerSubTotais.top = ENCAUTOMATICAS.GridPesq.height +  ENCAUTOMATICAS.GridPesq.top - 1
		ENCAUTOMATICAS.ContainerSubTotais.check1.config(myPath + "\imagens\icons\pagedown_B.png", "")
*!*			ENCAUTOMATICAS.Detalhest1.Pageframe1.PAGE9.chart1.left = ENCAUTOMATICAS.Detalhest1.Pageframe1.PAGE9.chart1.left-150
*!*			ENCAUTOMATICAS.Detalhest1.Pageframe1.PAGE9.label1.left = ENCAUTOMATICAS.Detalhest1.Pageframe1.PAGE9.label1.left-150
		**ENCAUTOMATICAS.Detalhest1.Pageframe1.PAGE9.grdbonusresumo.left = ENCAUTOMATICAS.Detalhest1.Pageframe1.PAGE9.grdbonusresumo.left-150
		
		uf_encautomaticas_format_rodapebonus()

		myResizeEncAutomaticas = .t.
	ELSE
	
		IF ENCAUTOMATICAS.height > ENCAUTOMATICAS.GridPesq.height + 258
	
			ENCAUTOMATICAS.detalhest1.visible		= .f.
			ENCAUTOMATICAS.detalhest1.pageFRAME1.page9.CHART1.Visible = .f.
			ENCAUTOMATICAS.shape1.visible			= .f.
			ENCAUTOMATICAS.GridPesq.height 			= ENCAUTOMATICAS.GridPesq.height + 258
			ENCAUTOMATICAS.shape3.height 			= ENCAUTOMATICAS.shape3.height + 258
			ENCAUTOMATICAS.ContainerSubTotais.top	= ENCAUTOMATICAS.GridPesq.height +  ENCAUTOMATICAS.GridPesq.top - 1
			ENCAUTOMATICAS.ContainerSubTotais.check1.config(myPath + "\imagens\icons\pageup_B.png", "")
*!*				ENCAUTOMATICAS.Detalhest1.Pageframe1.PAGE9.chart1.left = ENCAUTOMATICAS.Detalhest1.Pageframe1.PAGE9.chart1.left-150
*!*				ENCAUTOMATICAS.Detalhest1.Pageframe1.PAGE9.label1.left = ENCAUTOMATICAS.Detalhest1.Pageframe1.PAGE9.label1.left-150
			**ENCAUTOMATICAS.Detalhest1.Pageframe1.PAGE9.grdbonusresumo.left = ENCAUTOMATICAS.Detalhest1.Pageframe1.PAGE9.grdbonusresumo.left-150
			uf_encautomaticas_format_rodapebonus()

			myResizeEncAutomaticas = .f.
		ENDIF
		
	ENDIF
	
ENDFUNC


**
Function uf_encautomaticas_Gravar
	Select crsenc
	SET FILTER TO
		
	IF RECCOUNT("crsenc")=0
		uf_perguntalt_chama("N�O PODE GRAVAR UMA ENCOMENDA SEM REGISTOS.","OK","", 48)
		RETURN .f.
	ENDIF
	
	ENCAUTOMATICAS.containerSubTotais.valorsum.SetFocus
	**SET POINT TO "."

	Select crsenc
	mntotal=reccount()
	* inicializa a r�gua apresentando um t�tulo e o n� total de registos
	regua(0,MNTOTAL,"A PROCESSAR GRAVA��O DA ENCOMENDA AUTOM�TICA...",.f.)
	
	
	** Verificar que nenhum produto foi seleccionado sem fornecedor **
	select fornecedor from crsenc into cursor uForn where enc=.t. and empty(fornecedor)
	Calculate cnt() to lcCntForn For Empty(fornecedor)
	If lcCntForn>0
		If !uf_perguntalt_chama("UM OU MAIS PRODUTOS FORAM SELECCIONADOS SEM FORNECEDOR."+chr(13)+chr(10)+"PRETENDE CONTINUAR?"+CHR(13)+"SE 'SIM' ESTES PRODUTOS N�O SER�O INCLU�DOS NA(S) ENCOMENDA(S)","Sim","N�o")
			regua(2)
			RETURN .f.
		EndIf
	ENDIF
	IF USED("uForn")
		fecha("uForn")
	ENDIF
	*******************************************************************

	** Verificar se existe bonus igual ou superior � EOQ **
	SELECT crsEnc
	GO TOP
	SCAN FOR enc=.t.
		IF crsenc.eoq==0
			uf_perguntalt_chama("ATEN��O: N�O PODE TER PRODUTOS MARCADOS PARA A ENCOMENDA COM QUANTIDADE A ENCOMENDA IGUAL A 0."+CHR(10)+CHR(10)+"PRODUTO ["+ALLTRIM(crsenc.design)+"]","OK","",48)
			regua(2)
			RETURN .f.
		ENDIF
		
		IF crsenc.eoq<=crsenc.qtbonus
			uf_perguntalt_chama("ATEN��O: N�O PODE TER BONUS IGUAL OU SUPERIOR � QUANTIDADE A ENCOMENDAR."+CHR(10)+CHR(10)+"PRODUTO ["+ALLTRIM(crsenc.design)+"]","OK","",48)
			regua(2)
			RETURN .f.
		ENDIF
	ENDSCAN
	SELECT crsenc
	GO TOP
	*******************************************************

	** Verificar que todos os fornecedores escolhidos est�o correctos na ficha **
	SELECT distinct fornecedor as nome, fornec as no, fornestab as estab FROM crsenc INTO CURSOR uForn WHERE enc=.t. AND !EMPTY(fornecedor) readwrite
	IF reccount("uForn")=0
		uf_perguntalt_chama("NENHUM PRODUTO COM FORNECEDOR ACTIVO FOI SELECCIONADO PARA ENCOMENDA","OK","",48)
		regua(2)
		RETURN .f.
	ENDIF
	
	SELECT uForn
	GO TOP
	SCAN
		IF uf_gerais_actgrelha("", "uFornVal", [select top 1 nome, no from fl (nolock) where fl.no=]+ALLTRIM(STR(uForn.no))+[ and fl.estab=]+ALLTRIM(STR(uForn.estab)))
			IF !RECCOUNT("uFornVal")>0
				uf_perguntalt_chama("O FORNECEDOR '" + ALLTRIM(uForn.nome) + "[" + Alltrim(str(uForn.no))+ "]" + "[" + Alltrim(Str(uForn.estab)) + "]' N�O TEM FICHA CRIADA. DEVE CORRIGIR ANTES DE PODER CONTINUAR.","OK","",48)
	
				fecha("uFornVal")
				
				Select crsenc
				Go top
				Scan For fornecedor==uForn.nome And enc==.t.
					Exit
				Endscan
				
				Fecha("uForn")
				
				regua(2)
				RETURN .f.
			ELSE
				replace uForn.nome WITH ALLTRIM(uFornVal.nome) IN uForn
			ENDIF
			fecha("uFornVal")
		ENDIF
		
		SELECT uForn
	ENDSCAN
	****************************************************************************
	
	** eliminar duplicados (pode acontecer quando existem altera��es ao nome do fornecedor na ficha e nao no preferencial **
	SELECT distinct nome, no, estab, .f. as b2b, .f. as envionormal from uForn into cursor uForn2 READWRITE 
	
	** Valida Plafond das Encomendas a Fornecedor **
	LOCAL lcValidaEnc
	lcValidaEnc = uf_documentos_verifyPlafondEncForn(.t., "ENCAUTOMATICAS")
	IF !lcValidaEnc
		uf_perguntalt_chama("N�O PODE ULTRAPASSAR O PLAFOND DAS ENCOMENDAS A FORNCEDOR.","OK","",48)
		regua(2)
		RETURN .f.
	ENDIF
	************************************************

	*************************************************************************************************************
	** Para os artigos escolhidos, realizar os documentos de encomenda (n� de documentos = n� de fornecedores) **
	*************************************************************************************************************
	local myecra, myStampEnc
	store '' to myecra
	
	** Guardar c�digo e nome do documento **
	LOCAL lcDocCode, lcDocNome
	lcDocCode = 2 && c�digo do documento
		
	IF uf_gerais_actgrelha("", "uCrsTsCode", [select nmdos from ts (nolock) where ndos=2])
		IF RECCOUNT("uCrsTsCode")>0
			lcDocNome = uCrsTsCode.nmdos && nome do documento
		ELSE
			uf_perguntalt_chama("O DOCUMENTO [ENCOMENDA A FORNECEDOR] N�O EST� A SER ENCONTRADO. O DOCUMENTO N�O SER� CRIADO.","OK","",48)
			regua(2)
			RETURN .f.
		ENDIF
		fecha("uCrsTsCode")
	ELSE
		uf_perguntalt_chama("O DOCUMENTO [ENCOMENDA A FORNECEDOR] N�O EST� A SER ENCONTRADO. O DOCUMENTO N�O SER� CRIADO.","OK","",48)
		regua(2)
		RETURN .f.
	ENDIF
	
	** verifica se houve altera��o de dados na grelha para atualizar na ST
	IF myAlterSt = .t.
		IF uf_perguntalt_chama("ATUALIZOU DADOS DOS ARTIGOS NA GRELHA DE ENCOMENDAS (STK MAX E/OU PTO ENC). DESEJA ATUALIZAR ESSA INFORMA��O NA FICHA DOS ARTIGOS ALTERADOS?","Sim","N�o")
			uf_encautomaticas_atualizaST()
			uf_perguntalt_chama("Informa��o atualizada.","OK","", 64)
			myAlterSt = .f.
		ENDIF
	ENDIF 

	 

	SELECT crsenc
	GO TOP

	COUNT TO uv_count FOR !EMPTY(crsEnc.fornec) AND crsEnc.enc and crsEnc.descontoForn <> crsEnc.descontoFornOri

	IF uv_count > 0

		LOCAL uv_updateCCFDesc, uv_sqlCCFDesc
		STORE '' TO uv_updateCCFDesc, uv_sqlCCFDesc

		IF uf_perguntalt_chama("Atualizou o Desconto de Fornecedor na grelha de Encomendas." + CHR(13) + "Deseja atualizar os descontos no documento de Condi��es Comerciais de Fornecedor?","Sim","N�o")

			SELECT crsenc
			GO TOP

			SCAN FOR !EMPTY(crsEnc.fornec) AND crsEnc.enc and crsEnc.descontoForn <> crsEnc.descontoFornOri

				TEXT TO uv_sqlCCFDesc TEXTMERGE NOSHOW
					UPDATE bi
					SET
						desconto = <<crsenc.descontoForn>>
					WHERE
						bostamp = (SELECT 
									TOP 1 bostamp 
								FROM 
									bo(nolock) 
									join ts(nolock) on ts.ndos = bo.ndos 
								WHERE 
									ts.codigoDoc = 39 
									AND bo.fechada = 0 
									AND bo.no = <<crsenc.fornec>> 
									AND bo.estab = <<crsenc.fornestab>> 
								ORDER BY
									bo.ousrdata desc, bo.ousrhora desc
								)
						AND ref = '<<ALLTRIM(crsenc.ref)>>'

				ENDTEXT

				uv_updateCCFDesc = uv_updateCCFDesc + IIF(!EMPTY(uv_updateCCFDesc), CHR(13), '') + uv_sqlCCFDesc

				SELECT crsenc
			ENDSCAN

			IF !EMPTY(uv_updateCCFDesc)

				IF !uf_gerais_actGrelha("", "", uv_updateCCFDesc)
					uf_perguntalt_chama("Erro a atualizar descontos de fornecedor." + CHR(13) + "Por favor contacte o suporte.", "OK", "", 48)
					RETURN .F.
				ELSE
					uf_perguntalt_chama("Informa��o atualizada.","OK","", 64)
					UPDATE crsenc SET crsenc.descontoFornOri = crsenc.descontoForn
				ENDIF

			ENDIF

		ENDIF

	ENDIF
	
	LOCAL llErroVV
	STORE .f. TO llErroVV
	** tratar das encomendas VV
	IF TYPE("ATENDIMENTO")!="U"
	
		IF uf_encautomaticas_valida_vv() = .t.

            CREATE CURSOR uc_respVV(erro L, soGrava L, temVV L)
            SELECT uc_respVV
            APPEND BLANK
            REPLACE uc_respVV.erro with .T.
            REPLACE uc_respVV.soGrava with .F.
            REPLACE uc_respVV.temVV with .F.
            
			IF !uf_encautomaticas_envia_vv()
				regua(2)
				RETURN .F.
			ENDIF
			
		ELSE
			llErroVV = .t.
		ENDIF 
	ENDIF 

	
	IF llErroVV = .t.
		regua(2)
		RETURN .f.
	ENDIF 
	
	SELECT crsenc 
	GO TOP 
	DELETE FROM crsenc WHERE vv=.t.
	SELECT crsenc 
	GO TOP 

    SELECT uForn2
    GO TOP
    SCAN

        SELECT crsEnc
        GO TOP
        LOCATE FOR crsEnc.fornec == uForn2.no AND crsEnc.fornestab == uForn2.estab

        IF !FOUND()
            SELECT uForn2
            DELETE
        ENDIF

        SELECT uForn2
    ENDSCAN

    SELECT crsEnc
    
    IF RECCOUNT("crsEnc") > 0
	
        ** Verifica se grava a encomenda apenas ou tamb�m a envia - B2B
        LOCAL lcFornB2B, lcenviaNormal
        STORE .f. TO lcFornB2B
        lcenviaNormal = .t.
        
        SELECT uForn2
        GO TOP 
        SCAN
            TEXT TO lcSQLb2b NOSHOW TEXTMERGE
                select * from fl_site (nolock) where 
                --id_esb<>'' and 
                site_nr=<<mysite_nr>> and nr_fl=<<ALLTRIM(STR(uForn2.no))>> and dep_fl=<<ALLTRIM(STR(uForn2.estab))>> 
                --and usa_esb=1
            ENDTEXT  
            IF uf_gerais_actGrelha("","uFornB2B",lcSQLb2b )
                IF RECCOUNT("uFornB2B")>0
                    SELECT uFornB2B
                    GO TOP 
                    SCAN 
                        IF ALLTRIM(uFornB2B.id_esb)='B2B'
                            lcFornB2B = .t.
                            SELECT uForn2
                            replace uForn2.b2b WITH .t.
                        ENDIF 
                        IF !EMPTY(uFornB2B.idclfl) AND ALLTRIM(uFornB2B.id_esb)<>'B2B'
                            lcenviaNormal= .t.
                            SELECT uForn2
                            replace uForn2.envionormal WITH .t.
                        ENDIF 
                    ENDSCAN 
                ENDIF
                fecha("uFornB2B")
            ELSE
                uf_perguntalt_chama("Erro a consultar as comunica��es do fornecedor.","OK","",48)
                RETURN .f.
            ENDIF
        ENDSCAN 

        LOCAL lcSoGrava
        STORE .t. TO lcSoGrava 

        uv_usouVV = .F.

        IF USED("uc_respVV")

            SELECT uc_respVV

            IF uc_respVV.temVV
                uv_usouVV = .T.
                lcSoGrava = uc_respVV.soGrava
            ENDIF

            FECHA("uc_respVV")
            
        ENDIF    

  
        IF !uv_usouVV 
            If !uf_perguntalt_chama("DESEJA APENAS GRAVAR A(S) ENCOMENDA(S) OU TAMB�M ENVIAR A(S) MESMA(S)?","GRAVAR","GRAVAR/ENVIAR", .F., .F., "Cancelar", "", "", "ponta_seta_rigth_w.png")
          	
              	IF myretornocancelar ==.T.
              		regua(2)
					RETURN .F.
				ENDIF  
         
                lcSoGrava = .f.
            ENDIF 
        ENDIF 
       
        ***************************************
        
        ** PREPARAR DADOS/TOTAIS **
            
        ** Vari�veis para os Cabe�alhos ENCOMENDA A FORNECEDOR **
        LOCAL valorIva1, valorIva2, valorIva3, valorIva4, valorIva5, lcTotalIva, totalComIva, totalSemIva, totalNservico, lcTotalFinal
        LOCAL totalSemIva1, totalSemIva2, totalSemIva3, totalSemIva4, totalSemIva5, lcQtSum
        LOCAL lcSql, lcNrDoc, lcDocCont
        ** vari�veis para as linhas
        LOCAL lcBiStamp, lcCountS, lcOrdem, lcQtAltern, lcRef, lcQt, lcTotalLiq, lcValInsertLin, lcEpv
        ****************************************************
        ** criar documentos **
        
        SELECT uForn2
        GO TOP
        SCAN
            ** inicializar vari�veis **
            STORE 0 TO valorIva1, valorIva2, valorIva3, valorIva4, valorIva5, lcTotalIva, totalComIva, totalSemIva, totalNservico, lcTotalFinal
            STORE 0 TO totalSemIva1, totalSemIva2, totalSemIva3, totalSemIva4, totalSemIva5, lcQtSum
            STORE 0 TO lcNrDoc, lcDocCont
            STORE '' TO lcSql
            ***************************
                
            ** Guardar n�mero do documento **
            **IF uf_gerais_actgrelha("", "uCrsNrMax", [select ISNULL((MAX(convert(numeric,obrano))),0) as nr from bo (nolock) where bo.ndos=2 and YEAR(dataobra) = YEAR(dateadd(HOUR, ]+str(difhoraria)+[, getdate()))])
            **    IF RECCOUNT("uCrsNrMax")>0
            **        lcNrDoc = uCrsNrMax.nr + 1
            **    ENDIF
            **    fecha("uCrsNrMax")
            **ELSE
            **    uf_perguntalt_chama("O DOCUMENTO [ENCOMENDAS A FORNECEDOR] N�O EST� A SER ENCONTRADO. O DOCUMENTO N�O SER� CRIADO.","OK","", 48)
            **    regua(2)
            **    RETURN .f.
            **ENDIF
            *********************************

            ** Guarda Contador por tipo de documento e origem **
            IF uf_gerais_actgrelha("", "uCrsContDoc", [select ISNULL(MAX(bo2.u_doccont),0) as doccont from bo2 (nolock) inner join bo (nolock) on bo.bostamp=bo2.bo2stamp where bo.ndos=2])
                IF RECCOUNT("uCrsContDoc")>0
                    lcDocCont = uCrsContDoc.doccont + 1
                ELSE
                    lcDocCont = 1
                ENDIF
                fecha("uCrsContDoc")
            ENDIF
            ****************************************************

            ** guardar dados do fornecedor **
            SELECT uforn2
            TEXT TO lcSql NOSHOW textmerge
                SELECT 
                    nome
                    ,no
                    ,estab
                    ,tipo
                    ,local
                    ,codpost
                    ,ncont
                    ,morada
                    ,moeda
                    ,ccusto
                from 
                    fl (nolock)
                where 
                    no = <<uForn2.no>> 
                    and estab=<<uForn2.estab>>
            ENDTEXT
            IF !uf_gerais_actgrelha("", "uCrsDadosForn", lcSql)
                uf_perguntalt_chama("OCORREU UM ERRO A LER DADOS DO FORNECEDOR. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
                regua(2)
                RETURN .f.
            ENDIF
            *********************************

            ** Guardar Stamp Para Cabe�alho **
            LOCAL lcStampEnc
            lcStampEnc	= uf_gerais_stamp()

            ** calcular totais **		
            Select crsenc
            GO top
            SCAN FOR crsenc.fornec==uForn2.no AND crsenc.fornestab==uForn2.estab AND crsenc.enc==.t. AND crsenc.eoq>0
                lcQtSum	= lcQtSum + crsenc.eoq
                
                IF uf_gerais_getParameter("ADM0000000213","BOOL") == .t. and uf_gerais_getParameter("ADM0000000214","BOOL") == .f. && Encomendas Keiretsu
                    lcTotalFinal	= (crsenc.pclfornec - ((crsenc.pclfornec * crsenc.descontoForn)/100)) * (crsenc.eoq - crsenc.qtbonus)
                ELSE
                    IF crsenc.epcusto<=0 AND crsenc.epcult>0
                        lcTotalFinal	= (crsenc.epcult - ((crsenc.epcult * crsenc.descontoForn)/100)) * (crsenc.eoq - crsenc.qtbonus)
                    ELSE
                        lcTotalFinal	= (crsenc.epcusto - ((crsenc.epcusto * crsenc.descontoForn)/100)) * (crsenc.eoq - crsenc.qtbonus)
                    ENDIF
                ENDIF

				IF TYPE("MyTipoEncAuto") <> "U" AND uf_gerais_compStr("MANUAL", MyTipoEncAuto)
					lcTotalFinal = (crsenc.pcusto - ((crsenc.pcusto * crsenc.descontoForn)/100)) * (crsenc.eoq - crsenc.qtbonus)
				ENDIF
                
                DO CASE
                    CASE crsenc.tabiva==1
                        valorIva1		= valorIva1+(lcTotalFinal *(crsenc.iva/100))
                        totalSemIva1	= totalSemIva1 + lcTotalFinal
                    CASE crsenc.tabiva==2
                        valorIva2		= valorIva2+(lcTotalFinal *(crsenc.iva/100))
                        totalSemIva2	= totalSemIva2 + lcTotalFinal
                    CASE crsenc.tabiva==3
                        valorIva3		= valorIva3+(lcTotalFinal *(crsenc.iva/100))
                        totalSemIva3	= totalSemIva3 + lcTotalFinal
                    CASE crsenc.tabiva==4
                        valorIva4		= valorIva4+(lcTotalFinal *(crsenc.iva/100))
                        totalSemIva4	= totalSemIva4 + lcTotalFinal
                    CASE crsenc.tabiva==5
                        valorIva5		= valorIva5+(lcTotalFinal *(crsenc.iva/100))
                        totalSemIva5	= totalSemIva5 + lcTotalFinal
                ENDCASE
            
                totalComIva		= totalComIva + (lcTotalFinal *(crsenc.iva/100+1))
                totalSemIva		= totalSemIva + (lcTotalFinal)
                totalNservico	= totalNservico + lcTotalFinal
                
                SELECT crsenc
            ENDSCAN

            lcTotalIva=valorIva1+valorIva2+valorIva3+valorIva4+valorIva5
            ****************************************
            
            LOCAL lcTime 
            SET HOURS TO 24
            Atime=ttoc(datetime()+(difhoraria*3600),2)
            Astr=left(Atime,8)
            lcTime = Astr
            **lcTime = TIME()
            
            LOCAL lcDate 
            lcDate = uf_gerais_getDate((datetime()+(difhoraria*3600)),"SQL")
            


            ** GRAVAR CABE�ALHOS **
            SELECT crsenc
            *GO top
            TEXT TO lcSql NOSHOW textmerge

				DECLARE @obrano NUMERIC(10)
				DECLARE @tabNewDocNr TABLE (newDocNr NUMERIC(10,0))

				INSERT INTO @tabNewDocNr (newDocNr)
				EXEC up_get_newDocNr 'BO', <<lcDocCode>>, '<<mySite>>', 1

				SET @obrano = (SELECT newDocNr FROM @tabNewDocNr)

                Insert into bo
                    (
                    bostamp
                    ,ndos
                    ,nmdos
                    ,obrano
                    ,dataobra
                    ,U_DATAENTR
                    ,nome
                    ,nome2
                    ,[no]
                    ,estab
                    ,morada
                    ,[local]
                    ,codpost
                    ,tipo
                    ,ncont
                    ,etotaldeb
                    ,dataopen
                    ,boano
                    ,ecusto
                    ,etotal
                    ,moeda
                    ,memissao
                    ,origem
                    ,site
                    ,vendedor
                    ,vendnm
                    ,obs
                    ,ccusto
                    ,sqtt14
                    ,ebo_1tvall
                    ,ebo_2tvall
                    ,ebo_totp1
                    ,ebo_totp2
                    ,ebo11_bins
                    ,ebo12_bins
                    ,ebo21_bins
                    ,ebo22_bins
                    ,ebo31_bins
                    ,ebo32_bins
                    ,ebo41_bins
                    ,ebo42_bins
                    ,ebo51_bins
                    ,ebo52_bins
                    ,ebo11_iva
                    , ebo12_iva
                    , ebo21_iva
                    , ebo22_iva
                    , ebo31_iva
                    , ebo32_iva
                    , ebo41_iva
                    , ebo42_iva
                    , ebo51_iva
                    , ebo52_iva
                    ,
                    ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora
                    )
                Values 
                    (
                    '<<ALLTRIM(lcStampEnc)>>', <<lcDocCode>>, '<<lcDocNome>>', @obrano, '<<lcDate>>', '19000101',
                    '<<ALLTRIM(uCrsDadosForn.nome)>>', '', <<uCrsDadosForn.no>>, <<uCrsDadosForn.estab>>, '<<ALLTRIM(uCrsDadosForn.morada)>>',
                    '<<ALLTRIM(uCrsDadosForn.local)>>', '<<ALLTRIM(uCrsDadosForn.codpost)>>', '<<ALLTRIM(uCrsDadosForn.Tipo)>>', '<<ALLTRIM(uCrsDadosForn.Ncont)>>',
                    <<ROUND(totalSemIva,2)>>, '<<lcDate>>', year(dateadd(HOUR, <<difhoraria>>, getdate())), <<ROUND(totalSemIva,2)>>, <<ROUND(totalComIva,2)>>, '<<ALLTRIM(uCrsDadosForn.moeda)>>', 'EURO', 'BO', '<<ALLTRIM(mySite)>>',
                    <<ch_vendedor>>, '<<ALLTRIM(ch_vendnm)>>', 'Documento gerado Via Encomendas Semi-Autom�ticas', '<<uCrsDadosForn.ccusto>>',
                    <<lcQtSum>>, <<ROUND(totalSemIva,2)>>, <<ROUND(totalSemIva,2)>>, <<ROUND(totalSemIva,2)>>, <<ROUND(totalSemIva,2)>>,
                    <<totalSemIva1>>, <<totalSemIva1>>, <<totalSemIva2>>, <<totalSemIva2>>, <<totalSemIva3>>, <<totalSemIva3>>, <<totalSemIva4>>, <<totalSemIva4>>, <<totalSemIva5>>, <<totalSemIva5>>,
                    <<valorIva1>>, <<valorIva1>>, <<valorIva2>>, <<valorIva2>>, <<valorIva3>>, <<valorIva3>>, <<valorIva4>>, <<valorIva4>>, <<valorIva5>>, <<valorIva5>>,
                    '<<m_chinis>>','<<lcDate>>', '<<ALLTRIM(lcTime)>>', '<<m_chinis>>', '<<lcDate>>', '<<ALLTRIM(lcTime)>>'
                    )
                    
        
            ENDTEXT
            IF !uf_gerais_actgrelha("", "", lcSql)
                uf_perguntalt_chama("OCORREU UM ERRO A GERAR DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
                regua(2)
                RETURN .f.
            ENDIF


            lcNrDoc = uf_gerais_getUmValor("bo", "obrano", "bostamp = '" + lcStampEnc + "'")
            
            lcSql=''

            TEXT TO lcSql NOSHOW textmerge
                Insert into bo2
                    (
                    bo2stamp, autotipo, pdtipo, etotalciva, ETOTIVA, u_class, u_doccont, armazem,
                    ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora
                    )
                Values
                    (
                    '<<ALLTRIM(lcStampEnc)>>', 1, 1, <<ROUND(totalComIva,2)>>, <<ROUND(lcTotalIva,2)>>, 'Encomenda Semi-Autom�tica', <<lcDocCont>>, <<myArmazem>>,
                    '<<m_chinis>>', '<<lcDate>>', '<<ALLTRIM(lcTime)>>', '<<m_chinis>>', '<<lcDate>>', '<<ALLTRIM(lcTime)>>'
                    )
            ENDTEXT
            IF !uf_gerais_actgrelha("", "", lcSql)
                uf_perguntalt_chama("OCORREU UM ERRO A GERAR DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
                regua(2)
                RETURN .f.
            ENDIF
            
            
        
            
            &&certific��o documento
    *!*			lcSqlCert = uf_pagamento_gravarCert(lcStampEnc, lcDate, ALLTRIM(lcTime), lcDocCode , ROUND(totalComIva,2),'BO',0)
    *!*			lcSqlCert = uf_gerais_trataPlicasSQL(lcSqlCert)	
    *!*			
    *!*			lcSQL = ''
    *!*			TEXT TO lcSQL NOSHOW TEXTMERGE
    *!*				exec up_gerais_execSql 'Documentos - Insert', 1,'<<lcSqlCert>>', '', '', '' , '', ''
    *!*			ENDTEXT 
    *!*											
    *!*			IF !uf_gerais_actGrelha("","",lcSQL)
    *!*				uf_perguntalt_chama("OCORREU UMA ANOMALIA A GRAVAR O DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
    *!*				RETURN .f.		
    *!*			ENDIF
            
            
            ************************** && FIM DOS TOTAIS

            ** Gravar Linhas **
            STORE "" TO lcBiStamp, lcRef
            STORE 0 TO lcCountS, lcOrdem, lcQtAltern, lcQt, lcTotalLiq, lcEpv
            STORE .t. TO lcValInsertLin

            SELECT crsenc
            GO TOP
            SCAN FOR crsenc.fornec==uForn2.no AND crsenc.fornestab==uForn2.estab AND crsenc.enc==.t. AND crsenc.eoq>0
                ** Guardar Stamp das linhas **
                lcBiStamp = uf_gerais_stamp()				
                
                ** Contador para as linhas
                lcOrdem = lcOrdem + 10000
                
                ** Calcular quantidade, unidade alternativa e total liquido
                lcQt = crsenc.eoq
                
                    ** actualiza unidades alternativas **
                TEXT TO lcSql NOSHOW textmerge
                    exec up_stocks_factorConversaoFl '<<ALLTRIM(crsenc.ref)>>', <<uForn2.no>>, <<mysite_nr>>
                ENDTEXT
                IF uf_gerais_actgrelha("", [uCrsGetConv], lcSql)
                    IF RECCOUNT("uCrsGetConv")>0
                        IF EMPTY(uCrsGetConv.conversao)
                            lcQtAltern = crsenc.eoq
                        ELSE
                            lcQtAltern = INT(crsenc.eoq * uCrsGetConv.conversao)
                        ENDIF
                    ELSE
                        lcQtAltern	= INT((lcQt) * crsenc.conversao)
                    ENDIF
                    fecha("uCrsGetConv")
                ELSE
                    lcQtAltern	= INT((lcQt) * crsenc.conversao)
                ENDIF
                    *************************************
                
                IF uf_gerais_getParameter("ADM0000000213","BOOL") == .t. and uf_gerais_getParameter("ADM0000000214","BOOL") == .f. AND myencGrupo == .t.&& Encomendas Keiretsu
                    lcEpv	= crsenc.pclfornec 
                ELSE
                    IF crsenc.epcusto<=0 AND crsenc.epcult>0
                        lcEpv		= crsenc.epcult
                    ELSE
                        lcEpv		= crsenc.epcusto
                    ENDIF
                ENDIF

				IF TYPE("MyTipoEncAuto") <> "U" AND uf_gerais_compStr("MANUAL", MyTipoEncAuto)
					lcEpv		= crsenc.pcusto
				ENDIF

				lcTotalLiq	= (crsenc.eoq-crsenc.qtbonus) * (lcEpv - ((lcEpv * crsenc.descontoForn)/100))
                    
                ** guardar refer�ncia **
                lcRef = crsenc.ref

                ** Preencher Linhas da Bi **
                TEXT TO lcSql NOSHOW textmerge
                    Insert into bi
                        (
                        bistamp, bostamp, nmdos, obrano, ref, codigo,
                        design, qtt, qtt2, uni2qtt, u_bonus,
                        u_stockact, iva, tabiva, armazem, stipo, ndos, cpoc, familia,
                        no, nome, local, morada, codpost, ccusto,
                        epu, edebito, eprorc, epcusto, ettdeb, ecustoind, edebitoori, u_upc,
                        rdata, dataobra, dataopen, resfor, lordem,
                        lobs,
                        ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora, desconto
                        )
                    Values
                        (
                        '<<ALLTRIM(lcBiStamp)>>', '<<ALLTRIM(lcStampEnc)>>', '<<ALLTRIM(lcDocNome)>>', <<lcNrDoc>>, '<<ALLTRIM(lcRef)>>', '<<ALLTRIM(crsenc.codigo)>>',
                        '<<ALLTRIM(crsenc.design)>>', <<lcQt>>, 0, <<lcQtAltern>>, <<crsenc.qtbonus>>,
                        <<crsenc.stock>>, <<crsenc.iva>>, <<crsenc.tabiva>>, <<myArmazem>>, 4, <<lcDocCode>>, <<crsenc.cpoc>>, '<<crsenc.familia>>',
                        <<uCrsDadosForn.No>>, '<<ALLTRIM(uCrsDadosForn.Nome)>>', '<<ALLTRIM(uCrsDadosForn.Local)>>', '<<ALLTRIM(uCrsDadosForn.Morada)>>', '<<ALLTRIM(uCrsDadosForn.CodPost)>>', '<<uCrsDadosForn.ccusto>>',
                        <<lcEpv>>, <<lcEpv>>, <<lcEpv>>, <<lcEpv>>, <<lcTotalLiq>>, <<lcEpv>>, <<lcEpv>>, <<ROUND(lcTotalLiq / lcQt,2)>>,
                        '<<lcDate>>', '<<lcDate>>', '<<lcDate>>', 1, <<lcOrdem>>,
                        '',
                        '<<m_chinis>>', '<<lcDate>>', '<<ALLTRIM(lcTime)>>', '<<m_chinis>>', '<<lcDate>>', '<<ALLTRIM(lcTime)>>', <<crsenc.descontoForn>>
                        )
                ENDTEXT
                IF !uf_gerais_actGrelha("","",lcSql)
                    lcValInsertLin = .f.
                ENDIF
                
                ** Preencher Linhas da Bi2 **
                TEXT TO lcSql NOSHOW textmerge
                    Insert into bi2
                        (
                        bi2stamp, bostamp, morada, local, codpost
                        )
                    Values
                        (
                        '<<ALLTRIM(lcBiStamp)>>', '<<ALLTRIM(lcStampEnc)>>', '<<ALLTRIM(uCrsDadosForn.Morada)>>', '<<ALLTRIM(uCrsDadosForn.Local)>>', '<<ALLTRIM(uCrsDadosForn.CodPost)>>'
                        )
                ENDTEXT
                IF !uf_gerais_actgrelha("", "", lcSql)
                    lcValInsertLin = .f.
                ENDIF
            
                STORE 0 TO lcQtAltern, lcQt, lcTotalLiq	
                SELECT crsenc
            ENDSCAN
            ******************* && fim das linhas
                
            IF uf_gerais_getParameter("ADM0000000213","BOOL") == .t. && Encomendas Keiretsu
                uf_encautomaticas_estableceRelacoesComEncClientes(ALLTRIM(lcStampEnc))
            ENDIF

            IF USED("uCrsDadosForn")
                fecha("uCrsDadosForn")
            ENDIF
          
            ** envio autom�tico das encomendas
            SELECT uForn2
            IF lcSoGrava = .f. AND uForn2.envionormal = .t.

                uf_encautomaticas_enviarEncomenda(ALLTRIM(lcStampEnc))
            ENDIF 
            SELECT uForn2
            IF lcSoGrava = .f. AND uForn2.b2b=.t.
                uf_gerais_ChamaEsbOrders_PedidoEncomenda(ALLTRIM(lcStampEnc))
            ENDIF 

            SELECT uForn2
        ENDSCAN
        ************************
        
        ** actualiza ultimo registo **
        IF TYPE("lcStampEnc") <> "U"

            uf_gerais_gravaUltRegisto('BO', lcStampEnc)
            
            IF lcValInsertLin == .f.
                regua(2)
                uf_perguntalt_chama("UM OU MAIS PRODUTOS N�O FORAM CORRECTAMENTE GRAVADOS NA ENCOMENDA. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
            ENDIF
            
            ** PARA OS PRODUTOS COM st.EOQ>0 E/OU ESGOTADOS, LIMPAR OS MESMOS DADOS NA ST
            SELECT crsenc
            GO top
            select ref from CRSenc into cursor resetVal where enc=.t. AND CRSenc.qttadic>0
            
            select resetVal
            GO TOP
            SCAN
                uf_gerais_actgrelha("", "", [update st set eoq=0 where st.ref=']+ALLTRIM(resetVal.ref)+['])
            ENDSCAN
            IF USED("resetVal")
                fecha("resetVal")
            ENDIF
            *****************************************************************************
     
            IF TYPE("ATENDIMENTO") == "U"
                uf_documentos_chama(ALLTRIM(lcStampEnc))
            ENDIF 

            
            IF !EMPTY(myGetEncStamp)
                TEXT TO lcSql NOSHOW TEXTMERGE
                    DELETE
                        B_encAutoSave
                    where
                        stamp='<<ALLTRIM(myGetEncStamp)>>'
                ENDTEXT
                IF !uf_gerais_actgrelha("", "", lcSql)
                    uf_perguntalt_chama("OCORREU UM PROBLEMA A LIMPAR ENCOMENDA GUARDADA. POR FAVOR CONTACTE O SUPORTE","OK","", 16)
                ENDIF
            ENDIF

        ENDIF

    ENDIF
	
	
	regua(2)
	

	
	uf_encautomaticas_sair(.t.)
ENDFUNC 


**
FUNCTION uf_encautomaticas_criaDocumentoConferenciaDeEncomendas
	Lparameters lcStampEncForn
	Local lcbostamp 
	
	lcbostamp = uf_gerais_stamp()
	lcndos = 42
	lcnmdos = "Conferencia Enc. Forn."

	**text to lcSQL noshow textmerge 
	**	(select numero = isnull(max(obrano)+1,1) from bo where ndos = 42)
	**endtext 
	**IF !uf_gerais_actgrelha("", "ucrsObranoConfEnc", lcSql)
	**	uf_perguntalt_chama("OCORREU UM ERRO A GERAR DOCUMENTO [CONFERENCIA ENC. FORN.] POR FAVOR CONTACTE O SUPORTE.","OK","",16)
	**	RETURN .F.
	**ENDIF
	**
	**IF RECCOUNT("ucrsObranoConfEnc") == 0
	**	uf_perguntalt_chama("OCORREU UM ERRO A GERAR DOCUMENTO [CONFERENCIA ENC. FORN.] POR FAVOR CONTACTE O SUPORTE.","OK","",16)
	**	return .f.
	**ENDIF
	**
	**
	**Select ucrsObranoConfEnc
	**lcNumero = ucrsObranoConfEnc.numero
	
	** GRAVAR CABE�ALHOS **
	TEXT TO lcSql NOSHOW textmerge

		DECLARE @obrano NUMERIC(10)
		DECLARE @tabNewDocNr TABLE (newDocNr NUMERIC(10,0))

		INSERT INTO @tabNewDocNr (newDocNr)
		EXEC up_get_newDocNr 'BO', 42, '<<mySite>>', 1

		SET @obrano = (SELECT newDocNr FROM @tabNewDocNr)

		insert into bo (
			bostamp, ndos, nmdos, obrano, dataobra, U_DATAENTR, nome
			,nome2, [no], estab, morada, [local], codpost, tipo, ncont
			,etotaldeb, dataopen, boano, ecusto, etotal, moeda, memissao
			,origem, site, vendedor, vendnm, obs, ccusto, sqtt14, ousrinis
			,ousrdata, ousrhora, usrinis, usrdata, usrhora
		)
		select 
			'<<lcbostamp>>',42,'Conferencia Enc. Forn.',@obrano, dataobra, U_DATAENTR, nome
			,nome2, [no], estab, morada, [local], codpost, tipo, ncont
			,etotaldeb, dataopen, boano, ecusto, etotal, moeda, memissao
			,origem, site, vendedor, vendnm, obs, ccusto, sqtt14, ousrinis
			,ousrdata, ousrhora, usrinis, usrdata, usrhora
		from 
			bo (NOLOCK)
		where	
			bostamp = '<<Alltrim(lcStampEncForn)>>'
	ENDTEXT
	IF !uf_gerais_actgrelha("", "", lcSql)
		uf_perguntalt_chama("OCORREU UM ERRO A GERAR DOCUMENTO [CONFERENCIA ENC. FORN.] POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	ENDIF

	lcNumero = uf_gerais_getUmValor("bo", "obrano", "bostamp = '" + lcbostamp + "'")

	lcSql=''
	TEXT TO lcSql NOSHOW textmerge
		insert into bo2 (
			bo2stamp, autotipo, pdtipo, etotalciva, ETOTIVA, u_class, u_doccont, armazem,
			ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora, u_fostamp
		)

		Select '<<lcbostamp>>', autotipo, pdtipo, etotalciva, ETOTIVA, u_class, u_doccont, armazem,
			ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora, bo2stamp
		From 
			bo2 (NOLOCK)
		Where
			bo2stamp = '<<Alltrim(lcStampEncForn)>>'

	ENDTEXT
	IF !uf_gerais_actgrelha("", "", lcSql)
		uf_perguntalt_chama("OCORREU UM ERRO A GERAR DOCUMENTO [CONFERENCIA ENC. FORN.] POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	ENDIF
	
	
	** Preencher Linhas da Bi **
	TEXT TO lcSql NOSHOW textmerge
		insert into bi (	
			bistamp, bostamp, ndos, nmdos, obrano, ref, codigo,
			design, qtt, qtt2, uni2qtt, u_bonus,
			u_stockact, iva, tabiva, armazem, stipo, cpoc, familia,
			no, nome, local, morada, codpost, ccusto,
			epu, edebito, eprorc, epcusto, ettdeb, ecustoind, edebitoori, u_upc,
			rdata, dataobra, dataopen, resfor, lordem,
			lobs,
			ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora,obistamp
		)

		select  'E' + LEFT(bistamp,23), '<<lcbostamp>>', 42, 'Conferencia Enc. Forn.', <<lcNumero>>, ref, codigo,
				design, qtt, qtt2 = 0, uni2qtt, u_bonus,
				u_stockact, iva, tabiva, armazem, stipo, cpoc, familia,
				no, nome, local, morada, codpost, ccusto,
				epu, edebito, eprorc, epcusto, ettdeb, ecustoind, edebitoori, u_upc,
				rdata, dataobra, dataopen, resfor, lordem,
				lobs,
				ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora, bistamp
		From
			bi (NOLOCK)
		Where
			bostamp = '<<Alltrim(lcStampEncForn)>>'
	ENDTEXT
	IF !uf_gerais_actGrelha("","",lcSql)
		uf_perguntalt_chama("OCORREU UM ERRO A GERAR DOCUMENTO [CONFERENCIA ENC. FORN.] POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	ENDIF
	
	** Preencher Linhas da Bi2 **
	TEXT TO lcSql NOSHOW textmerge
		insert into bi2 (	
			bi2stamp, bostamp, morada, local, codpost
		)

		select 'E' + LEFT(bi2stamp,23), '<<lcbostamp>>', morada, local, codpost
		From
			bi2
		Where
			bostamp = '<<Alltrim(lcStampEncForn)>>'
	ENDTEXT
	IF !uf_gerais_actgrelha("", "", lcSql)
		uf_perguntalt_chama("OCORREU UM ERRO A GERAR DOCUMENTO [CONFERENCIA ENC. FORN.] POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	ENDIF

ENDFUNC 


** 
FUNCTION uf_encautomaticas_estableceRelacoesComEncClientes
	Lparameters lcEncFornstamp

	IF empty(lcEncFornstamp)
		return .f.
	ENDIF

	TEXT TO lcSql NOSHOW TEXTMERGE
		select 
			bostamp
		from 
			bo (nolock) 
		where 
			bo.ndos = 41
			and fechada = 0
	ENDTEXT
	IF !uf_gerais_actgrelha("", "ucrsEncClientesK", lcSql)
		uf_perguntalt_chama("OCORREU UM PROBLEMA A VERIFICAR ENCOMENDAS DE CLIENTES. POR FAVOR CONTACTE O SUPORTE","OK","", 16)
		return .f.
	ENDIF
	
	SElect ucrsEncClientesK
	Go Top
	Scan 
		
		TEXT TO lcSql NOSHOW TEXTMERGE
			insert into encKeiretsuLnk  values ('<<ucrsEncClientesK.bostamp>>','<<lcEncFornstamp>>')
		ENDTEXT
		IF !uf_gerais_actgrelha("", "", lcSql)
			uf_perguntalt_chama("OCORREU UM PROBLEMA A GERAR LIGA��O ENTRE ENCOMENDAS DE CLIENTES E ENCOMENDAS A FORNECEDORES. POR FAVOR CONTACTE O SUPORTE","OK","", 16)
			return .f.
		ENDIF

	ENDSCAN

ENDFUNC


**
FUNCTION uf_encautomaticas_DynamicBackColor_Bonus
	LPARAMETERS lcRecno
	
	RETURN IIF(crsenc.alertaBonus == 1,rgb[255,0,0],IIF(lcRecno%2!=0,Rgb[255,255,255],rgb[240,240,240]))
ENDFUNC


**
FUNCTION uf_encautomaticas_DynamicBackColor_Esgotados
	LPARAMETERS lcRecno

	RETURN IIF(crsenc.Esgotado == "SIM",rgb[255,0,0],IIF(lcRecno%2!=0,Rgb[255,255,255],rgb[240,240,240]))
ENDFUNC


**
FUNCTION uf_encautomaticas_DynamicBackColor_PCL
	LPARAMETERS lcRecno
	
	RETURN IIF(crsenc.alertaPCL == 1,rgb[255,0,0],IIF(lcRecno%2!=0,Rgb[255,255,255],rgb[240,240,240]))
ENDFUNC


**
FUNCTION uf_encautomaticas_DynamicBackColor_StockGH
	LPARAMETERS lcRecno
	
	RETURN IIF(crsenc.alertaStockGH == 1,rgb[255,0,0],IIF(lcRecno%2!=0,Rgb[255,255,255],rgb[240,240,240]))
ENDFUNC

**
FUNCTION uf_encautomaticas_DynamicBackColor_Alter_stmax
	LPARAMETERS lcRecno

	RETURN IIF(crsenc.stmax<> crsenc.ostmax ,rgb[255,0,0],Rgb[255,255,255])
ENDFUNC

**
FUNCTION uf_encautomaticas_DynamicBackColor_Alter_ptoenc
	LPARAMETERS lcRecno

	RETURN IIF(crsenc.ptoenc<> crsenc.optoenc ,rgb[255,0,0],Rgb[255,255,255])
ENDFUNC



**
FUNCTION uf_encautomaticas_chamaDocumentos
	LPARAMETERS lcRef, lcDesign, lcDoc
	
	IF !EMPTY(lcRef)
		uf_LISTADOCUMENTOS_chama(lcRef, 0, lcDesign, "ST", lcDoc)
	ENDIF

ENDFUNC

**
FUNCTION uf_encautomaticas_selFornecedorLostFocus
	LPARAMETERS tcNumForn
	
	IF USED("crsenc")
	 
	 	IF EMPTY(tcNumForn) OR VAL(tcNumForn) == 0 
		 	SELECT crsenc
	        Replace  crsenc.fornecedor     	WITH ""
	        Replace  crsenc.fornec         	WITH 0
	        Replace  crsenc.fornestab    	WITH 0
	       	Replace  crsenc.enc    	WITH .f.
	       
			uf_perguntalt_chama("N�o foi possivel encontrar o fornecedor com o n�mero introduzido","OK","", 64)
		ELSE
	 
		 	TEXT TO lcSql NOSHOW TEXTMERGE
				exec up_fornecedores_pesquisar '', <<LEFT(ALLTRIM(tcNumForn),5)>>, 0, '', '', '', 0, '',<<mysite_nr>>
			ENDTEXT
			IF !uf_gerais_actGrelha("",[ucrsPesqEntTemp],lcSql)
				uf_perguntalt_chama("N�o foi possivel encontrar o fornecedor com o n�mero introduzido. Por favor contacte o suporte.","OK","", 64)
				RETURN .f.
			ENDIF
		 
		 	IF RECCOUNT("ucrsPesqEntTemp")>0
		        Select ucrsPesqEntTemp
		        SELECT crsenc
		        Replace  crsenc.fornecedor     	WITH ALLTRIM(ucrsPesqEntTemp.nome)
		        Replace  crsenc.fornec         	WITH ucrsPesqEntTemp.no
		        Replace  crsenc.fornestab    	WITH ucrsPesqEntTemp.estab
		        Replace  crsenc.bonusfornec    	WITH ucrsPesqEntTemp.bonus
		        Replace  crsenc.pclfornec    	WITH ucrsPesqEntTemp.pcl
		        Replace  crsenc.esgotado    	WITH ucrsPesqEntTemp.esgotado
		        replace  crsenc.fornecabrev		WITH IIF(EMPTY(ucrsPesqEntTemp.nome2),ALLTRIM(ucrsPesqEntTemp.nome),ALLTRIM(ucrsPesqEntTemp.nome2))
		
		        SELECT crsenc
		        Replace  crsenc.enc    WITH .t.
			ELSE
				SELECT crsenc
		        Replace  crsenc.fornecedor     	WITH ""
		        Replace  crsenc.fornec         	WITH 0
		        Replace  crsenc.fornestab    	WITH 0
	     	    Replace  crsenc.enc    	WITH .f.
		       
		        uf_perguntalt_chama("N�o foi possivel encontrar o fornecedor com o n�mero introduzido.","OK","", 64)
	        ENDIF 
	       
       	ENDIF 
        
        
        ENCAUTOMATICAS.detalhest1.refresh
        
        ** recalcula totais
		uf_encautomaticas_ActualizaQtValorEnc()
        
    ENDIF	
ENDFUNC


**
FUNCTION uf_encautomaticas_sair
	LPARAMETERS tcBool
	
	
	IF myAlterSt = .t.
		IF uf_perguntalt_chama("ATUALIZOU DADOS DOS ARTIGOS NA GRELHA DE ENCOMENDAS (STK MAX E/OU PTO ENC). DESEJA ATUALIZAR ESSA INFORMA��O NA FICHA DOS ARTIGOS ALTERADOS?","Sim","N�o")
			uf_encautomaticas_atualizaST()
			uf_perguntalt_chama("Informa��o atualizada.","OK","", 64)
			myAlterSt = .f.
		ENDIF
	ENDIF 

	SELECT crsenc
	GO TOP

	COUNT TO uv_count FOR !EMPTY(crsEnc.fornec) AND crsEnc.enc and crsEnc.descontoForn <> crsEnc.descontoFornOri

	IF uv_count > 0

		LOCAL uv_updateCCFDesc, uv_sqlCCFDesc
		STORE '' TO uv_updateCCFDesc, uv_sqlCCFDesc

		IF uf_perguntalt_chama("Atualizou o Desconto de Fornecedor na grelha de Encomendas." + CHR(13) + "Deseja atualizar os descontos no documento de Condi��es Comerciais de Fornecedor?","Sim","N�o")

			SELECT crsenc
			GO TOP

			SCAN FOR !EMPTY(crsEnc.fornec) AND crsEnc.enc and crsEnc.descontoForn <> crsEnc.descontoFornOri

				TEXT TO uv_sqlCCFDesc TEXTMERGE NOSHOW
					UPDATE bi
					SET
						desconto = <<crsenc.descontoForn>>
					WHERE
						bostamp = (SELECT 
									TOP 1 bostamp 
								FROM 
									bo(nolock) 
									join ts(nolock) on ts.ndos = bo.ndos 
								WHERE 
									ts.codigoDoc = 39 
									AND bo.fechada = 0 
									AND bo.no = <<crsenc.fornec>> 
									AND bo.estab = <<crsenc.fornestab>> 
								ORDER BY
									bo.ousrdata desc, bo.ousrhora desc
								)
						AND ref = '<<ALLTRIM(crsenc.ref)>>'

				ENDTEXT

				uv_updateCCFDesc = uv_updateCCFDesc + IIF(!EMPTY(uv_updateCCFDesc), CHR(13), '') + uv_sqlCCFDesc

				SELECT crsenc
			ENDSCAN

			IF !EMPTY(uv_updateCCFDesc)

				IF !uf_gerais_actGrelha("", "", uv_updateCCFDesc)
					uf_perguntalt_chama("Erro a atualizar descontos de fornecedor." + CHR(13) + "Por favor contacte o suporte.", "OK", "", 48)
					RETURN .F.
				ELSE
					uf_perguntalt_chama("Informa��o atualizada.","OK","", 64)
					UPDATE crsenc SET crsenc.descontoFornOri = crsenc.descontoForn
				ENDIF

			ENDIF

		ENDIF

	ENDIF
	
	IF !tcBool
		IF !uf_perguntalt_chama("TEM A CERTEZA QUE DESEJA SAIR DESTE ECR�?"+CHR(13)+CHR(13)+ "Se 'SIM' ir� perder todas as altera��es.","Sim","N�o")
			RETURN .f.
		ENDIF
	ENDIF
	
	IF USED("crsenc")
		fecha("crsenc")
	ENDIF
		
	IF USED("ucrsFornceEncAut")
		fecha("ucrsFornceEncAut")
	ENDIF
	
	IF USED("ucrsConfEmpresas")
		fecha("ucrsConfEmpresas")
	ENDIF
	

	RELEASE myOrderEncAutomaticas
	RELEASE myGetEncStamp
	RELEASE MyTipoEncAuto
	RELEASE myControlaEventoQt
			
	ENCAUTOMATICAS.hide
	ENCAUTOMATICAS.release
ENDFUNC



**
FUNCTION uf_encautomaticas_DuplicarLinha
	SELECT crsenc 
	lcStStamp = crsenc.ststamp 
	
	SELECT TOP 1 * FROM crsenc WHERE ALLTRIM(ststamp) = ALLTRIM(lcStStamp) ORDER BY ststamp INTO CURSOR ucrsDuplicarLinhaTemp READWRITE 
	SELECT crsenc
	APPEND FROM DBF("ucrsDuplicarLinhaTemp")
	
	IF USED("ucrsDuplicarLinhaTemp")
		fecha("ucrsDuplicarLinhaTemp")
	ENDIF 
	

ENDFUNC



FUNCTION uf_encautomaticas_CarregaTipoProduto

    public  myTabTipoProduto
    myTabTipoProduto=""
    
    && Cursor uCrsTempMotives	
	IF USED("uCrsTempTipoProduto")
		fecha("uCrsTempTipoProduto")
	ENDIF 
	CREATE CURSOR uCrsTempMotives(id varchar(254), descr varchar(254))	

    IF uf_gerais_actgrelha("", "uCrsTempTipoProduto", [select distinct design as TipoProduto from b_famFamilias (nolock) where inactivo = 0 order by design] )
        uf_valorescombo_chama("myTabTipoProduto", 0, "uCrsTempTipoProduto", 2, "TipoProduto ", "TipoProduto",.f.,.f.,.t.,.f.)
    ENDIF 
	
	IF USED("uCrsTempTipoProduto")
		fecha("uCrsTempTipoProduto")
	ENDIF
	
    ENCAUTOMATICAS.tipoProduto.Value= myTabTipoProduto

	RELEASE myTabTipoProduto
ENDFUNC 


FUNCTION uf_encautomaticas_CarregaTipoEncomenda()

    public  myTabTipoEncomenda
    myTabTipoEncomenda=""
    
    && Cursor uCrsTempMotives	
	IF USED("uCrsTempTipoProduto")
		fecha("uCrsTempTipoProduto")
	ENDIF 
	CREATE CURSOR uCrsTempTipoEncomenda(descricao varchar(254))	

    IF uf_gerais_actgrelha("", "uCrsTempTipoEncomenda", [SELECT descricao='Esgotados' UNION ALL  SELECT descricao='Sem Esgotados'] )
        uf_valorescombo_chama("myTabTipoEncomenda", 0, "uCrsTempTipoEncomenda", 2, "descricao", "descricao",.f.,.f.,.t.,.f.)
    ENDIF 
	
	IF USED("uCrsTempTipoEncomenda")
		fecha("uCrsTempTipoEncomenda")
	ENDIF
	
	IF (myTabTipoEncomenda == "TODOS")
	    myTabTipoEncomenda=""
	ENDIF 
	
	
    ENCAUTOMATICAS.TipoEncomenda.Value= myTabTipoEncomenda

	RELEASE myTabTipoEncomenda

ENDFUNC

FUNCTION uf_encautomaticas_consultab2b

	** Verificar fornecedores dispon�veis para o B2B
	**IF uf_gerais_actgrelha("", "uFornB2B", [select * from fl_site (nolock) where id_esb='B2B' and site_nr=]mysite_nr[ and nr_fl=]+ALLTRIM(STR(uForn.no))+[ and dep_fl=]+ALLTRIM(STR(uForn.estab)))
	
	TEXT TO lcSql NOSHOW TEXTMERGE
		select fl.no ,fl.no_ext, fl.nome from fl_site (nolock)
		inner join fl (nolock) on fl.no=fl_site.nr_fl and fl.estab=fl_site.dep_fl 
		WHERE id_esb='B2B'
		and site_nr=<<mysite_nr>>
	ENDTEXT
	IF !uf_gerais_actGrelha("","uFornB2B",lcSql)
		uf_perguntalt_chama("N�o foi possivel a listagem de forecedores B2B. Por favor contacte o suporte.","OK","", 64)
		RETURN .f.
	ENDIF
	IF RECCOUNT("uFornB2B")>0
		**SELECT crsenc 
		**GO TOP 
		
		**SELECT distinct ref, design, SUM(eoq) as eoq FROM crsenc GROUP BY ref,design INTO CURSOR ucrsconsultaprod READWRITE 
		
		&&r�gua
		LOCAL lcCont, lcCont2
		lcCont = 0
		lcCont2 = 1
		
*!*			SELECT uFornB2B
*!*			calculate count() to lcCont
*!*			regua(0,lcCont,"A Consultar os fornecedores B2B...")
*!*			SELECT uFornB2B
*!*			GO TOP 
*!*			SCAN 
			regua(0,lcCont2,"A Consultar os fornecedores B2B...")
			LOCAL LcStampPed
			LcStampPed = uf_gerais_stamp()  
			uf_gerais_ChamaEsbOrders_ConsultaProdutos(ALLTRIM(LcStampPed), uFornB2B.no_ext, ALLTRIM(uFornB2B.nome), uFornB2B.no)	
*!*				lcCont2 = lcCont2 + 1
*!*			ENDSCAN 
		regua(2)
	ELSE
		uf_perguntalt_chama("N�O EXISTEM FORNECEDORES DO TIPO B2B.","OK","",48)
		RETURN .f.
	ENDIF 

ENDFUNC 


FUNCTION uf_encautomaticas_consultab2b_singleline
	** Verificar fornecedores dispon�veis para o B2B
	**IF uf_gerais_actgrelha("", "uFornB2B", [select * from fl_site (nolock) where id_esb='B2B' and site_nr=]mysite_nr[ and nr_fl=]+ALLTRIM(STR(uForn.no))+[ and dep_fl=]+ALLTRIM(STR(uForn.estab)))
	
	TEXT TO lcSql NOSHOW TEXTMERGE
		select fl.no ,fl.no_ext, fl.nome from fl_site (nolock)
		inner join fl (nolock) on fl.no=fl_site.nr_fl and fl.estab=fl_site.dep_fl 
		WHERE id_esb='B2B'
		and site_nr=<<mysite_nr>>
	ENDTEXT
	IF !uf_gerais_actGrelha("","uFornB2B",lcSql)
		uf_perguntalt_chama("N�o foi possivel a listagem de forecedores B2B. Por favor contacte o suporte.","OK","", 64)
		RETURN .f.
	ENDIF

	IF RECCOUNT("uFornB2B")>0

        IF USED("crsenc")
            
		    SELECT crsenc 
		
		    IF USED("ucrsconsultaprod")
			    SELECT ucrsconsultaprod 
			    DELETE ALL 
		    ELSE
			    SELECT ref, design, eoq FROM crsenc WHERE 0=1 INTO CURSOR ucrsconsultaprod READWRITE 
		    ENDIF 
		    SELECT ucrsconsultaprod 
		    APPEND BLANK 
		    replace ucrsconsultaprod.ref WITH ALLTRIM(crsenc.ref)
		    replace ucrsconsultaprod.design WITH ALLTRIM(crsenc.design)		
		    replace ucrsconsultaprod.eoq WITH crsenc.eoq

        ENDIF
		
		&&r�gua
		LOCAL lcCont, lcCont2
		lcCont = 0
		lcCont2 = 1
		
*!*			SELECT uFornB2B
*!*			calculate count() to lcCont
*!*			regua(0,lcCont,"A Consultar os fornecedores B2B...")
*!*			SELECT uFornB2B
*!*			GO TOP 
*!*			SCAN 
			regua(0,lcCont2,"A Consultar os fornecedores B2B...")
			LOCAL LcStampPed
			LcStampPed = uf_gerais_stamp()  
			uf_gerais_ChamaEsbOrders_ConsultaProdutos(ALLTRIM(LcStampPed), uFornB2B.no_ext, ALLTRIM(uFornB2B.nome), uFornB2B.no)	
			lcCont2 = lcCont2 + 1
*!*			ENDSCAN 
		regua(2)
	ELSE
		uf_perguntalt_chama("N�O EXISTEM FORNECEDORES DO TIPO B2B.","OK","",48)
		RETURN .f.
	ENDIF 

ENDFUNC 



PROCEDURE uf_encautomaticas_configgrdenc
*!*	 LOCAL ti, i
*!*	 STORE 0 TO ti, i
*!*	 ti = ENCAUTOMATICAS.GridPesq.columncount
*!*	 FOR i = 1 TO ti
*!*	    IF UPPER(ALLTRIM((ENCAUTOMATICAS.GridPesq.columns(i).name)))=="MIN"
*!*	       ENCAUTOMATICAS.GridPesq.columns(i).dynamicbackcolor = "IIF(crsenc.stmax <> crsenc.ostmax, ,rgb[255,0,0],Rgb[255,255,255])"
*!*	    ENDIF
*!*	    IF UPPER(ALLTRIM((ENCAUTOMATICAS.GridPesq.columns(i).name)))=="PE"
*!*	       ENCAUTOMATICAS.GridPesq.columns(i).dynamicbackcolor = "IIF(crsenc.ptoenc <> crsenc.optoenc, ,rgb[255,0,0],Rgb[255,255,255])"
*!*	    ENDIF
*!*	   
*!*	 ENDFOR
*!*	 ENCAUTOMATICAS.GridPesq.refresh
ENDPROC


FUNCTION uf_encautomaticas_valida_vv
	SELECT crsenc 
	GO TOP 
	SCAN 
		IF crsenc.vv = .t.
			LOCAL lcMedNome , lcMedRef 
			lcMedNome = ALLTRIM(crsenc.design)
			lcMedRef = ALLTRIM(crsenc.ref)
			&&valida quantidade
			IF(crsenc.eoq>2)
				uf_perguntalt_chama("Pode encomendar no m�ximo 2 embalagens de " + lcMedNome,"OK","",48)
				RETURN .f.
			ENDIF

			&& Verifica se a venda a que pertence o produto, tem nr receita preenchido
			LOCAL lcFiLordem, lcNrReceita
			STORE 0 TO lcFiLordem
			STORE '' TO lcNrReceita

			lcFiLordem = LEFT(ASTR(crsenc.lordem),2)
			
			SELECT * FROM fi WHERE LEFT(ASTR(fi.lordem),2)=lcFiLordem  and  LEFT(ASTR(fi.design),1)="." INTO CURSOR uCrsTempFiValidaNrReceita READWRITE

			SELECT ucrsTempFiValidaNrReceita 
			GO TOP 
			LOCATE FOR LEFT(ASTR(uCrsTempFiValidaNrReceita.lordem),2) = lcFiLordem 
			IF FOUND()
				IF (LEN(STREXTRACT(uCrsTempFiValidaNrReceita.design, ':', ' -', 1)) != 13;
				 	AND LEN(STREXTRACT(uCrsTempFiValidaNrReceita.design, ':', ' -', 1)) != 19);
				 	OR LEFT(uCrsTempFiValidaNrReceita.design,4) == '.SEM'

					uf_perguntalt_chama("Deve preencher o n�mero de receita para a venda em que est� inserida a refer�ncia "+ ALLTRIM(lcMedRef),"OK","",48)
					RETURN .f.
				ELSE 
					lcNrReceita = STREXTRACT(uCrsTempFiValidaNrReceita.design, ':', ' -', 1)
					REPLACE crsenc.nreceita  WITH lcNrReceita 
				ENDIF
			ENDIF
			IF USED("ucrsTempFiValidaNrReceita")
				fecha("ucrsTempFiValidaNrReceita")
			ENDIF 
		ENDIF 
		SELECT crsenc	
	ENDSCAN 
	RETURN .t.
ENDFUNC 

FUNCTION uf_encautomaticas_envia_vv

	LOCAL lcretorno, lcSoGravaVV
	STORE .t. TO lcSoGravaVV


	SELECT ;
	uf_gerais_stamp() AS ID , nreceita,ref,design,fornecedor,fornestab as estab,fornec as no,pclfornec as ecusto,stock,eoq as qtt,.f. as encfornecedor,vv as encViaVerde,stock as u_stock,tabiva,iva,cpoc,familia,;
	IIF(!uf_gerais_compStr(TYPE("crsenc.descontoForn"),"U"), descontoForn, 0) as descontoForn;
	FROM crsenc ; 
	WHERE enc=.t.  AND vv=.t. INTO CURSOR ucrsEncomendaViaVerde  READWRITE

	CREATE CURSOR ucrsEncomendaAEnviar(;
		stamp   c(50),;
		nrDoc n(9),;
		nreceita c(25),; 
		ref c(18),;
		encViaVerde  l,;
		estab n(5),;
		no n(8),;
		estadoEnviado l,; 
		respErro  c(200);
	)
	


	SELECT ucrsEncomendaViaVerde  
	IF RECCOUNT("ucrsEncomendaViaVerde")>0
	
		** tem um cancelar o cancelar coloca a variavel publica a .f., depois .t. � para gravar, .t. � para gravar/enviar
		PUBLIC myretornocancelar

		lcretorno = uf_perguntalt_chama("DESEJA APENAS GRAVAR A ENCOMENDA VIA VERDE OU TAMB�M ENVIAR A MESMA?","GRAVAR","GRAVAR/ENVIAR", .F., .F., "Cancelar", "", "", "ponta_seta_rigth_w.png")		
		IF myretornocancelar==.T.
			RETURN .F.
		ENDIF

		lcSoGravaVV = myRetornoPergunta
		lcretorno = .f.
		
		SELECT ucrsEncomendaViaVerde 
		GO TOP 
		UPDATE ucrsEncomendaViaVerde SET encfornecedor = .t.
		
		
		select uc_respVV
    	replace uc_respVV.temVV with .T.
					
		
	ENDIF 


	DIMENSION lcSqlInsercaoEnc(3)

	SELECT ucrsEncomendaViaVerde 
	GO TOP
	SCAN
		lcValidaForn=uf_encautomaticas_dadosFornecedoresVV(ucrsEncomendaViaVerde.no,ucrsEncomendaViaVerde.estab, ucrsEncomendaViaVerde.fornecedor)

		IF(lcValidaForn==.f.)
			RETURN .F.
		ENDIF	

        IF USED("uCrsDadosForn")
            SELECT uCrsDadosForn
            IF EMPTY(uCrsDadosForn.via_verde_user) OR EMPTY(uCrsDadosForn.via_verde_pass)
                uf_perguntalt_chama("O FORNECEDOR " + ALLTRIM(uCrsDadosForn.NOME) + " N�O TEM OS DADOS DE VIA VERDE PREENCHIDOS." + CHR(13) + "POR FAVOR PREENCHA E VOLTE A TENTAR.","OK","", 48)
                LOOP
            ENDIF
        ENDIF

		LOCAL lcPosCabViaVerde
		lcPosCabViaVerde = RECNO("ucrsEncomendaViaVerde")
		LOCAL lcStampEnc
		lcStampEnc	= uf_gerais_stamp()
		**IF uf_gerais_actgrelha("", "uCrsNrMax", [select ISNULL((MAX(convert(numeric,obrano))),0) as nr from bo (nolock) where bo.ndos = 3 and YEAR(dataobra) = YEAR(GETDATE())])
		**	IF RECCOUNT("uCrsNrMax")>0
		**		lcNrDoc = uCrsNrMax.nr + 1
		**	ENDIF
		**	fecha("uCrsNrMax")
		**ELSE
		**	uf_perguntalt_chama("O DOCUMENTO [ENCOMENDAS A FORNECEDOR] N�O EST� A SER ENCONTRADO. O DOCUMENTO N�O SER� CRIADO.","OK","", 48)
		**	RETURN .f.
		**ENDIF	

		lcNrDoc = uf_gerais_newDocNr("BO", 3, .T.)

		uf_encautomaticas_Inserecab_VV(lcStampEnc,lcNrDoc,ucrsEncomendaViaVerde.no,ucrsEncomendaViaVerde.estab,ucrsEncomendaViaVerde.id,.t.,"ucrsEncomendaViaVerde",@lcSqlInsercaoEnc)
		lcSqlInsercaoEnc[1] = uf_gerais_trataPlicasSQL(lcSqlInsercaoEnc[1])
		lcSqlInsercaoEnc[2] = uf_gerais_trataPlicasSQL(lcSqlInsercaoEnc[2])
		lcSqlInsercaoEnc[3] = uf_gerais_trataPlicasSQL(lcSqlInsercaoEnc[3])
		lcSql = ''
		TEXT TO lcSQL NOSHOW TEXTMERGE
			exec up_gerais_execSql 'Encomendas At', 1, '<<lcSqlInsercaoEnc[1]>>', '<<lcSqlInsercaoEnc[2]>>', '', '', '', ''
		ENDTEXT 

		IF !uf_gerais_actGrelha("","",lcSQL)
			uf_perguntalt_chama("OCORREU UM ERRO A GRAVAR O CABE�ALHO DO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE."+CHR(10)+"Cod: Enc. At.","OK","",16)
			Return .f.
		ENDIF 	 
		SELECT ucrsEncomendaViaVerde
		TRY
			GO lcPosCabViaVerde 
		CATCH
		ENDTRY
	ENDSCAN
*!*			ELSE
*!*				RETURN .f.
*!*			ENDIF 

	IF lcSoGravaVV = .f.
		uf_encautomaticas_enviaDadosFornecedor_VV()
	ENDIF 
		
	SELECT crsenc 
	GO TOP 
	**DELETE FROM crsenc WHERE sel=.t. AND vv=.t.
	DELETE FROM crsenc WHERE vv=.t.
	SELECT crsenc 
	GO TOP 

	IF USED("ucrsEncomendaAEnviar") AND lcSoGravaVV = .f.
	    SELECT ucrsEncomendaAEnviar
	    GO TOP
	    
	    SCAN

	        IF ucrsEncomendaAEnviar.encViaVerde

	            IF ucrsEncomendaAEnviar.estadoEnviado
	                uf_perguntalt_chama("A Encomenda Via Verde n� "+ALLTRIM(STR(ucrsEncomendaAEnviar.nrDoc))+" foi confirmada pelo fornecedor " +ALLTRIM(uf_gerais_getUmValor("fl","nome","no = "+ astr(ucrsEncomendaAEnviar.no) + " and estab = " + astr(ucrsEncomendaAEnviar.estab))),"OK","", 64)
	            ELSE
	                uf_perguntalt_chama("A Encomenda Via Verde n� "+ALLTRIM(STR(ucrsEncomendaAEnviar.nrDoc))+" n�o foi enviada para o fornecedor " +ALLTRIM(uf_gerais_getUmValor("fl","nome","no = "+ astr(ucrsEncomendaAEnviar.no) + " and estab = " + astr(ucrsEncomendaAEnviar.estab))),"OK","", 64)
	            ENDIF

	        ENDIF

	    ENDSCAN
	ENDIF

	IF USED("uc_respVV")
	    replace uc_respVV.erro with .f.
	    replace uc_respVV.soGrava with lcSoGravaVV
	ENDIF

	RETURN .T.

ENDFUNC 


FUNCTION uf_encautomaticas_Inserelin_vv
	LPARAMETERS lcStampEnc, lcDocNome, lcNrDoc, lcRef, lcDocCode, lcEpv, lcQt, lcTotalLiq, lcCodigo, lcDesign, lcQtAltern, lcStock, lcIva, lcTabIva, lcCpoc, lcFamilia, lcDate, lcTime, lcDesconto
	
	LOCAL lcExecuteSQL
	lcExecuteSQL = ""
	
	lcBiStamp = uf_gerais_stamp()				
			
	** Contador para as linhas
	lcOrdem = 10000
			

	** Preencher Linhas da Bi **
	TEXT TO lcSql NOSHOW textmerge
		Insert into bi
			(
			bistamp, bostamp, nmdos, obrano, ref, codigo,
			design, qtt, qtt2, uni2qtt, u_bonus,
			u_stockact, iva, tabiva, armazem, stipo, ndos, cpoc, familia,
			no, nome, local, morada, codpost, ccusto,
			epu, edebito, eprorc, epcusto, ettdeb, ecustoind, edebitoori, u_upc,
			rdata, dataobra, dataopen, resfor, lordem,lobs,
			ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora, desconto
			)
		Values
			(
			'<<ALLTRIM(lcBiStamp)>>', '<<ALLTRIM(lcStampEnc)>>', '<<ALLTRIM(lcDocNome)>>', <<lcNrDoc>>, '<<ALLTRIM(lcRef)>>', '<<ALLTRIM(lcCodigo)>>',
			'<<ALLTRIM(lcDesign)>>', <<lcQt>>, 0, <<lcQtAltern>>, 0,
			<<lcStock>>, <<lciva>>, <<lctabiva>>, <<myArmazem>>, 4, <<lcDocCode>>, <<lccpoc>>, '<<ALLTRIM(lcfamilia)>>',
			<<uCrsDadosForn.No>>, '<<ALLTRIM(uCrsDadosForn.Nome)>>', '<<ALLTRIM(uCrsDadosForn.Local)>>', '<<ALLTRIM(uCrsDadosForn.Morada)>>', '<<ALLTRIM(uCrsDadosForn.CodPost)>>', '<<uCrsDadosForn.ccusto>>',
			<<ROUND(lcEpv,2)>>, <<ROUND(lcEpv,2)>>, <<ROUND(lcEpv,2)>>, <<ROUND(lcEpv,2)>>, <<ROUND(lcTotalLiq,2)>>, <<ROUND(lcEpv,2)>>, <<ROUND(lcEpv,2)>>, <<ROUND(lcEpv,2)>>,
			'<<lcDate>>', '<<lcDate>>', '<<lcDate>>', 1, <<lcOrdem>>,'',
			'<<m_chinis>>', '<<lcDate>>', '<<ALLTRIM(lcTime)>>' , '<<m_chinis>>', '<<lcDate>>', '<<ALLTRIM(lcTime)>>', <<lcDesconto>>
			)
	ENDTEXT
	lcExecuteSQL  = lcExecuteSQL + CHR(13) + CHR(13) + lcSQL
	
	
	** Preencher Linhas da Bi2 **
	TEXT TO lcSql NOSHOW textmerge
		Insert into bi2
			(
			bi2stamp, bostamp, morada, local, codpost
			)
		Values
			(
			'<<ALLTRIM(lcBiStamp)>>', '<<ALLTRIM(lcStampEnc)>>', '<<ALLTRIM(uCrsDadosForn.Morada)>>', '<<ALLTRIM(uCrsDadosForn.Local)>>', '<<ALLTRIM(uCrsDadosForn.CodPost)>>'
			)
	ENDTEXT
	lcExecuteSQL  = lcExecuteSQL + CHR(13) + CHR(13) + lcSQL
	

	RETURN lcExecuteSQL  

ENDFUNC



FUNCTION uf_encautomaticas_dadosFornecedoresVV
	LPARAMETERS lcNo,lcEstab, lcNome ,lcEsconderMensagem
	LOCAL lcNomeTexto
		
	IF USED("uCrsDadosForn")
		Fecha("uCrsDadosForn")
	ENDIF
	
	TEXT TO lcSQL NOSHOW textmerge
		SELECT 
			fl.nome
			,fl.no
			,fl.estab
			,fl.tipo
			,fl.local
			,fl.codpost
			,fl.ncont
			,fl.morada
			,fl.moeda
			,fl.ccusto
			,fl_site.via_verde_user
            ,fl_site.via_verde_pass
			,fl_site.idclfl
			,fl_site.clpass
		from 
			fl (nolock)
			left join fl_site on fl.no = fl_site.nr_fl and fl.estab = fl_site.dep_fl
		where 
			fl.no = <<lcNo>> 
			and site_nr  = <<mysite_nr>>
			and fl.estab = <<lcEstab>>
	ENDTEXT
	IF !uf_gerais_actgrelha("", "uCrsDadosForn", lcSql)
		uf_perguntalt_chama("OCORREU UM ERRO A LER DADOS DO FORNECEDOR. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	ENDIF
	
	IF(RECCOUNT("uCrsDadosForn")==0)
		IF(EMPTY(ALLTRIM(lcNome)))
			lcNomeTexto=""
		ELSE
			lcNomeTexto=" "+ ALLTRIM(lcNome)+ " "
		ENDIF
		IF(EMPTY(lcEsconderMensagem))
			uf_perguntalt_chama("O FORNECEDOR N�O TEM O ENVIO DE ENCOMENDAS CONFIGURADO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		ENDIF
		RETURN .f.
	ENDIF
	
	RETURN .t.
ENDFUNC


FUNCTION uf_encautomaticas_Inserecab_VV
	LPARAMETERS lcStampEnc, lcNrDoc,lcNo,lcEstab, lcID, lcEncViaVerde, lcEnc, lcSqlArray
	
	LOCAL lcEnvia,lcTime, lcDate 
	LOCAL lcExecuteSQL, lcSqlCert 
	STORE "" TO lcExecuteSQL, lcSqlCert 

	LOCAL valorIva1, valorIva2, valorIva3, valorIva4, valorIva5,valorIva6,valorIva7,valorIva8,valorIva9, lcTotalIva, totalComIva, totalSemIva, totalNservico, lcTotalFinal
	LOCAL totalSemIva1, totalSemIva2, totalSemIva3, totalSemIva4, totalSemIva5,totalSemIva6, totalSemIva7, totalSemIva8, totalSemIva9,  lcQtSum
	LOCAL lcSql, lcDocCont, lcNrAux, lcNrReceita 

	LOCAL lcBiStamp, lcCountS, lcOrdem, lcQtAltern, lcRef, lcQt, lcTotalLiq, lcValInsertLin, lcEpv,lcExecuteSQLin 

	STORE 0 TO valorIva1, valorIva2, valorIva3, valorIva4, valorIva5, valorIva6, valorIva7, valorIva8, valorIva9, lcTotalIva, totalComIva, totalSemIva, totalNservico, lcTotalFinal
	STORE 0 TO totalSemIva1, totalSemIva2, totalSemIva3, totalSemIva4, totalSemIva5, totalSemIva6, totalSemIva7, totalSemIva8, totalSemIva9, lcQtSum
	STORE 0 TO lcDocCont
	STORE '' TO lcSQL,lcExecuteSQLin, lcNrReceita  
	STORE 0 TO lcNrAux
	STORE .f. TO lcEnvia

	LOCAL lcDocCode, lcDocNome
	STORE 0 TO  lcDocCode 

	IF(EMPTY(ALLTRIM(lcID)))
		lcNrAux=1
	ENDIF
	
	lcTime = TIME()
 	lcDate = uf_gerais_getDate(DATE(),"SQL")
	
	SELECT &lcEnc 
  	GO TOP
	SCAN FOR (estab=lcEstab AND no=lcNo AND (ID=lcID OR lcNrAux=1))
		LOCAL totalComIvaLinha
		STORE 0 TO totalComIvaLinha
			
		IF lcEncViaVerde == .t.
			IF uf_gerais_actgrelha("", "uCrsTsCode", [select nmdos from ts (nolock) where ndos = 3])
				IF RECCOUNT("uCrsTsCode")>0
					lcDocNome = uCrsTsCode.nmdos 
					lcDocCode = 3
				ELSE
					uf_perguntalt_chama("O DOCUMENTO [ENCOMENDA A FORNECEDOR] N�O EST� A SER ENCONTRADO. O DOCUMENTO N�O SER� CRIADO.","OK","",48)
					regua(2)
					RETURN .f.
				ENDIF
				fecha("uCrsTsCode")
			ELSE
				uf_perguntalt_chama("O DOCUMENTO [ENCOMENDA A FORNECEDOR] N�O EST� A SER ENCONTRADO. O DOCUMENTO N�O SER� CRIADO.","OK","",48)
				regua(2)
				RETURN .f.
			ENDIF
		ELSE
			IF uf_gerais_actgrelha("", "uCrsTsCode", [select nmdos from ts (nolock) where ndos = 2])
				IF RECCOUNT("uCrsTsCode")>0
					lcDocNome = uCrsTsCode.nmdos 
					lcDocCode = 2
				ELSE
					uf_perguntalt_chama("O DOCUMENTO [ENCOMENDA A FORNECEDOR] N�O EST� A SER ENCONTRADO. O DOCUMENTO N�O SER� CRIADO.","OK","",48)
					regua(2)
					RETURN .f.
				ENDIF
				fecha("uCrsTsCode")
			ELSE
				uf_perguntalt_chama("O DOCUMENTO [ENCOMENDA A FORNECEDOR] N�O EST� A SER ENCONTRADO. O DOCUMENTO N�O SER� CRIADO.","OK","",48)
				regua(2)
				RETURN .f.
			ENDIF
		ENDIF
		** decide se envia encomenda
		lcEnvia=&lcEnc..encfornecedor
		
		lcQtSum	= lcQtSum+&lcEnc..qtt
		lcRef = ALLTRIM(&lcEnc..ref)
		lcTabIva = &lcEnc..tabiva
		lcIva = &lcEnc..iva
		lcTotalFinal = (&lcEnc..ecusto - ((&lcEnc..ecusto - &lcEnc..descontoForn))) * &lcEnc..qtt
		totalComIvaLinha = (&lcEnc..ecusto - ((&lcEnc..ecusto - &lcEnc..descontoForn))) * &lcEnc..qtt
		lcEpv = &lcEnc..ecusto
		lcCodigo = ALLTRIM(&lcEnc..ref)
		lcDesign = ALLTRIM(&lcEnc..design)
		lcQtAltern = 0
		lcStock = &lcEnc..u_stock
		lcCpoc = &lcEnc..cpoc 
		lcFamilia = &lcEnc..familia
		lcNrReceita = &lcEnc..nreceita
			
		DO CASE
			CASE lcTabIva ==1
				valorIva1		= valorIva1+(lcTotalFinal *(lcIva/100))
				totalSemIva1	= totalSemIva1 + lcTotalFinal
			CASE lcTabIva ==2
				valorIva2		= valorIva2+(lcTotalFinal *(lcIva/100))
				totalSemIva2	= totalSemIva2 + lcTotalFinal
			CASE lcTabIva ==3
				valorIva3		= valorIva3+(lcTotalFinal *(lcIva/100))
				totalSemIva3	= totalSemIva3 + lcTotalFinal
			CASE lcTabIva ==4
				valorIva4		= valorIva4+(lcTotalFinal *(lcIva/100))
				totalSemIva4	= totalSemIva4 + lcTotalFinal
			CASE lcTabIva ==5
				valorIva5		= valorIva5+(lcTotalFinal *(lcIva/100))
				totalSemIva5	= totalSemIva5 + lcTotalFinal	
			CASE lcTabIva ==6
				valorIva6		= valorIva6+(lcTotalFinal *(lcIva/100))
				totalSemIva6	= totalSemIva6 + lcTotalFinal
			CASE lcTabIva ==7
				valorIva7		= valorIva7+(lcTotalFinal *(lcIva/100))
				totalSemIva7	= totalSemIva7 + lcTotalFinal
			CASE lcTabIva ==8
				valorIva8		= valorIva8+(lcTotalFinal *(lcIva/100))
				totalSemIva8	= totalSemIva8 + lcTotalFinal
			CASE lcTabIva ==9
				valorIva9		= valorIva9+(lcTotalFinal *(lcIva/100))
				totalSemIva9	= totalSemIva9 + lcTotalFinal
		ENDCASE
		
		totalComIva		= totalComIva + (lcTotalFinal *(lcIva/100+1))
		totalSemIva		= totalSemIva + (lcTotalFinal)
		totalNservico	= totalNservico + lcTotalFinal
		
		lcTotalIva=valorIva1+valorIva2+valorIva3+valorIva4+valorIva5+valorIva6+valorIva7+valorIva8+valorIva9

		lcExecuteSQLin =lcExecuteSQLin   + CHR(10) + CHR(13) + uf_encautomaticas_Inserelin_vv(lcStampEnc, lcDocNome, lcNrDoc, lcRef, lcDocCode, lcEpv, &lcEnc..qtt, totalComIvaLinha , lcCodigo, lcDesign, lcQtAltern, lcStock, lcIva, lcTabIva, lcCpoc, lcFamilia, lcDate, lcTime, &lcEnc..descontoForn)
			
	ENDSCAN

	**guarda dados a enviar
	IF(lcEnvia==.t.)
		INSERT INTO ucrsEncomendaAEnviar(stamp, nrDoc, nreceita, ref, encViaVerde, no, estab);
		VALUES (lcStampEnc,lcNrDoc, lcNrReceita, lcRef,lcEncViaVerde,lcNo,lcEstab)
	ENDIF

	SELECT uCrsDadosForn
			
	TEXT TO lcSQL NOSHOW textmerge
		INSERT INTO bo
			(
			bostamp,ndos,nmdos,obrano,dataobra,U_DATAENTR
			,nome,nome2,[no],estab,morada
			,[local],codpost,tipo,ncont
			,etotaldeb,dataopen,boano,ecusto,etotal,moeda,memissao
			,origem,site
			,vendedor,vendnm,obs,ccusto,sqtt14
			,ebo_1tvall,ebo_2tvall,ebo_totp1,ebo_totp2
			,ebo11_bins,ebo12_bins,ebo21_bins,ebo22_bins,ebo31_bins,ebo32_bins,
			ebo41_bins,ebo42_bins,ebo51_bins,ebo52_bins,ebo61_bins,ebo62_bins					
			,ebo11_iva,ebo12_iva,ebo21_iva,ebo22_iva,ebo31_iva,ebo32_iva
			,ebo41_iva,ebo42_iva,ebo51_iva,ebo52_iva,ebo61_iva,ebo62_iva
			,ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora
			)
		Values  
			(
			'<<ALLTRIM(lcStampEnc)>>', <<lcDocCode>>, '<<ALLTRIM(lcDocNome)>>', <<lcNrDoc>>, '<<lcDate>>', '19000101',
			'<<ALLTRIM(uCrsDadosForn.nome)>>', '', <<uCrsDadosForn.no>>, <<uCrsDadosForn.estab>>, '<<ALLTRIM(uCrsDadosForn.morada)>>',
			'<<ALLTRIM(uCrsDadosForn.local)>>', '<<ALLTRIM(uCrsDadosForn.codpost)>>', '<<ALLTRIM(uCrsDadosForn.Tipo)>>', '<<ALLTRIM(uCrsDadosForn.Ncont)>>',
			<<ROUND(totalSemIva,2)>>, '<<lcDate>>',year(dateadd(HOUR, <<difhoraria>>, getdate())), <<ROUND(totalSemIva,2)>>, <<ROUND(totalComIva,2)>>, '<<ALLTRIM(uCrsDadosForn.moeda)>>', 'EURO', 
			'BO', '<<ALLTRIM(mySite)>>',
			<<ch_vendedor>>, '<<ALLTRIM(ch_vendnm)>>', 'Documento gerado via Enc. Forn. Atendimento', '<<ALLTRIM(uCrsDadosForn.ccusto)>>',<<lcQtSum>>, 
			<<ROUND(totalSemIva,2)>>, <<ROUND(totalSemIva,2)>>, <<ROUND(totalSemIva,2)>>, <<ROUND(totalSemIva,2)>>,
			<<ROUND(totalSemIva1,2)>>, <<ROUND(totalSemIva1,2)>>, <<ROUND(totalSemIva2,2)>>, <<ROUND(totalSemIva2,2)>>, <<ROUND(totalSemIva3,2)>>, <<ROUND(totalSemIva3,2)>>, 
			<<ROUND(totalSemIva4,2)>>, <<ROUND(totalSemIva4,2)>>, <<ROUND(totalSemIva5,2)>>, <<ROUND(totalSemIva5,2)>>,<<ROUND(totalSemIva6,2)>>, <<ROUND(totalSemIva6,2)>>,
			<<ROUND(valorIva1,2)>>, <<ROUND(valorIva1,2)>>, <<ROUND(valorIva2,2)>>, <<ROUND(valorIva2,2)>>, <<ROUND(valorIva3,2)>>, <<ROUND(valorIva3,2)>>, 
			<<ROUND(valorIva4,2)>>, <<ROUND(valorIva4,2)>>, <<ROUND(valorIva5,2)>>, <<ROUND(valorIva5,2)>>,<<ROUND(valorIva6,2)>>, <<ROUND(valorIva6,2)>>,
			'<<m_chinis>>', '<<lcDate>>', '<<ALLTRIM(lcTime)>>', '<<m_chinis>>', '<<ALLTRIM(lcDate)>>', '<<ALLTRIM(lcTime)>>'
			)
	ENDTEXT

	lcExecuteSQL  = lcExecuteSQL + CHR(13) + CHR(13) + lcSQL
	lcSql=''
	TEXT TO lcSql NOSHOW textmerge
		Insert into bo2
			(
			bo2stamp, autotipo, pdtipo, etotalciva, ETOTIVA, u_class, u_doccont, armazem,
			ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora,
			ebo71_bins,ebo72_bins,ebo81_bins,ebo82_bins,ebo91_bins,ebo92_bins,
			ebo71_iva,ebo72_iva,ebo81_iva,ebo82_iva,ebo91_iva,ebo92_iva, nrReceita
			)
		Values
			(
			'<<ALLTRIM(lcStampEnc)>>', 1, 1, <<ROUND(totalComIva,2)>>, <<ROUND(lcTotalIva,2)>>, 'Documento gerado via Enc. Forn. Atendimento', <<lcDocCont>>, <<myArmazem>>,
			'<<m_chinis>>', '<<lcDate>>', '<<ALLTRIM(lcTime)>>', '<<m_chinis>>', '<<lcDate>>', '<<ALLTRIM(lcTime)>>',
			<<ROUND(totalSemIva7,2)>>, <<ROUND(totalSemIva7,2)>>,<<ROUND(totalSemIva8,2)>>, <<ROUND(totalSemIva8,2)>>,<<ROUND(totalSemIva9,2)>>, <<ROUND(totalSemIva9,2)>>,
			<<ROUND(valorIva7,2)>>, <<ROUND(valorIva7,2)>>,<<ROUND(valorIva8,2)>>, <<ROUND(valorIva8,2)>>,<<ROUND(valorIva9,2)>>, <<ROUND(valorIva9,2)>> , '<<ALLTRIM(lcNrReceita)>>'
			) 
			
	ENDTEXT

	lcExecuteSQL  = lcExecuteSQL + CHR(10) + CHR(13) + lcSQL	
	
	&&certifica documento
	lcSqlCert = lcSqlCert  + uf_pagamento_gravarCert(ALLTRIM(lcStampEnc), lcDate, lcTime, lcDocCode, ROUND(totalComIva,2),'BO',0)
	
	&&guarda queries sql no array
	STORE lcExecuteSQL  TO lcSqlArray(1)
	STORE lcExecuteSQLin  TO lcSqlArray(2)
	STORE lcSqlCert TO lcSqlArray(3)
	
ENDFUNC 


FUNCTION uf_encautomaticas_enviaDadosFornecedor_VV()
	LOCAL lcValidaForn
	STORE .f. TO lcValidaForn, lcBreak	
	
	&& Verifica a informa��o do fornecedor
	IF USED("uCrsTipoFl")
		fecha("uCrsTipoFl")
	ENDIF
	
    lcSQL = ''
    TEXT TO lcSql NOSHOW TEXTMERGE
        select tipo, usa_esb, id_esb from fl (nolock) LEFT JOIN fl_site ON fl.no = fl_site.nr_fl AND fl.estab = fl_site.dep_fl where no = <<ucrsEncomendaAEnviar.no>> and estab = <<ucrsEncomendaAEnviar.estab>> and site_nr = <<mysite_nr>>
    ENDTEXT
    IF !uf_gerais_actGrelha("", "uCrsTipoFl",lcSql)
        uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VERIFICAR TIPO DO FORNECEDOR. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
        RETURN .f.
    ENDIF

	SELECT ucrsEncomendaAEnviar
	GO TOP
	SCAN
		if(ucrsEncomendaAEnviar.estadoEnviado==.f.)
			IF ucrsEncomendaAEnviar.encViaVerde== .t.
				lcValidaForn=uf_encomendaMed_dadosFornecedores(ucrsEncomendaAEnviar.no,ucrsEncomendaAEnviar.estab,"")
				IF(lcValidaForn==.f.)
					lcBreak = .t.			
				ENDIF
				
				regua(0,3,"A enviar Encomenda, por favor aguarde.")
				
				LOCAL ViaVerdePath, lcWsPath, lcIDCliente, lcNomeJar, lcStampEnc, lcNrDoc, lcFornNo, lcFornEstab, lcNrReceita, lcEstadoDeEnvio, lcRespErro  
				STORE '' TO ViaVerdePath, lcWsPath, lcIDCliente, lcNomeJar, lcStampEnc, lcNrDoc, lcFornNo, lcFornEstab, lcNrReceita, lcRespErro  
				STORE .f. TO lcEstadoDeEnvio
				
				SELECT uCrsE1
				IF EMPTY(ALLTRIM(uCrsE1.id_lt))
					uf_perguntalt_chama("O C�DIGO DE CLIENTE LOGITOOLS N�O EST� CORRETAMENTE PREENCHIDO. POR FAVOR CONTACTE O SUPORTE.","OK","", 16)
					RETURN .f.
				ENDIF 
					
				lcIDCliente = ALLTRIM(uCrsE1.id_lt)
				
	 				&& valida e configura software p envio encomenda
				lcNomeJar = 'ViaVerdeMed.jar'
				IF !FILE(ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\ViaVerdeMed\' + ALLTRIM(lcNomeJar))
					uf_perguntalt_chama("O SOFTWARE DE ENVIO N�O EST� CONFIGURADO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
					regua(2)
					RETURN .f.
				ENDIF 
				
				&& Define de usa testes ou n�o 
				&& verifica se utiliza webservice de testes
				LOCAL lcDefineWsTest
				STORE 0 TO lcDefineWsTest
				IF uf_gerais_getParameter("ADM0000000227","BOOL") == .f.
					lcDefineWsTest = 0
				ELSE
					lcDefineWsTest = 1
				ENDIF 

				lcStampEnc= ucrsEncomendaAEnviar.stamp
				lcNrDoc= ucrsEncomendaAEnviar.nrDoc
				lcFornNo = ucrsEncomendaAEnviar.no
				lcFornEstab = ucrsEncomendaAEnviar.estab 
				lcNrReceita = ucrsEncomendaAEnviar.nreceita 

				&& Tenta o envio			
				regua(1,2,"A enviar Encomenda, por favor aguarde.")
						
				lcWsPath = ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\ViaVerdeMed\' + ALLTRIM(lcNomeJar) + ' ENVIO ';
					+ ' "--orderStamp=' + ALLTRIM(lcStampEnc) + ["];	
					+ ' "--orderNr=' + ALLTRIM(STR(lcNrDoc)) + ["];
					+ ' "--clID=' + lcIDCliente + ["];
					+ ' "--site=' + UPPER(ALLTRIM(mySite)) + ["];
					+ ' --supplierNr=' + ALLTRIM(STR(lcFornNo));
					+ ' --supplierDep=' + ALLTRIM(STR(lcFornEstab));
					+ ' "--IDReceita=' + ALLTRIM(lcNrReceita) + ["];
					+ ' --test=' + ALLTRIM(STR(lcDefineWsTest))

				&& Envia comando Java		
				lcWsPath = "javaw -jar " + lcWsPath 
	
				&& aguarda que software termine o envio da informa��o 3� PARAMETRO = .T.
				oWSShell = CREATEOBJECT("WScript.Shell")
				oWSShell.Run(lcWsPath, 1, .t.)	
				

				&& consultar info registada na BD para dar indica��o ao utilizador
				lcSQL = ''
				TEXT TO lcSQL TEXTMERGE NOSHOW 
					select TOP 1 *, Aceite AS LOGI1 FROM via_verde_med (nolock) WHERE bostamp = '<<ALLTRIM(lcStampEnc)>>' ORDER BY data desc
				ENDTEXT 

				IF !uf_gerais_actGrelha("","uCrsAuxResposta",lcSQL)
					lcEstadoDeEnvio = .f.	
					lcRespErro  =	"OCORREU UMA ANOMALIA A ATUALIZAR A ENCOMENDA. POR FAVOR CONTACTE O SUPORTE."+CHR(10)+"Cod: Enc. At."
				ELSE
		 			IF RECCOUNT("uCrsAuxResposta") > 0
		 				IF uCrsAuxResposta.aceite == .t.
			 				&& atualiza informa��o de documento enviado					
							lcSQL = ''
							TEXT TO lcSQL TEXTMERGE NOSHOW 
								UPDATE bo SET logi1 = 1 WHERE bostamp = '<<ALLTRIM(uCrsAuxResposta.bostamp)>>'
							ENDTEXT		
							IF !uf_gerais_actGrelha("","",lcSQL)
								lcEstadoDeEnvio = .f.	
								lcRespErro  =	"OCORREU UMA ANOMALIA AO VERIFICAR A RESPOSTA DO FORNECEDOR. POR FAVOR CONTACTE O SUPORTE."		
							ELSE
								if(uCrsAuxResposta.logi1==.t.) 
									lcEstadoDeEnvio = .t.	
								else
									lcEstadoDeEnvio = .f.	
								endif
							ENDIF
							LOCAL lcMostra
							lcMostra = .t.
							

							SELECT uCrsAuxResposta
		                   	If(uf_documentos_fecha_encomendas_semProducto(ALLTRIM(uCrsAuxResposta.estado),ALLTRIM(uCrsAuxResposta.bostamp)))
		                   		regua(2)
		                   		lcMostra = .f.
		                   	ENDIF
							&& output informa��o proveniente do forncedor
							if(!EMPTY(lcMostra))
								lcSQL = ''
								TEXT TO lcSQL TEXTMERGE NOSHOW 
									SELECT vv.ref, bi.design,  vv.qt_pedida, vv.qt_entregue FROM via_verde_med_d vv (nolock)
									INNER JOIN bi (nolock) ON vv.ref = bi.ref 
									WHERE id_via_verde_med = '<<ALLTRIM(uCrsAuxResposta.id)>>' 
									AND bi.bostamp = '<<ALLTRIM(uCrsAuxResposta.bostamp)>>'
								ENDTEXT	
								
								IF !uf_gerais_actGrelha("","uCrsAuxRespostaDetalhe",lcSQL)
									lcEstadoDeEnvio = .f.	
									lcRespErro  =	"OCORREU UMA ANOMALIA AO VERIFICAR A RESPOSTA DO FORNECEDOR. POR FAVOR CONTACTE O SUPORTE."	
								ELSE
								
									IF RECCOUNT(uCrsAuxRespostaDetalhe) = 0
										TEXT TO lcSQL TEXTMERGE NOSHOW 
											SELECT vv.ref, bi.design,  vv.qt_pedida, vv.qt_entregue FROM via_verde_med_d vv (nolock)
											INNER JOIN bi (nolock) ON vv.ref = bi.ref 
											WHERE id_via_verde_med = '<<ALLTRIM(uCrsAuxResposta.id)>>' 
											AND bi.bostamp = '<<ALLTRIM(uCrsAuxResposta.bostamp)>>'
										ENDTEXT	
										
										IF !uf_gerais_actGrelha("","uCrsAuxRespostaDetalhe",lcSQL)
											lcEstadoDeEnvio = .f.	
											lcRespErro  =	"OCORREU UMA ANOMALIA AO VERIFICAR A RESPOSTA DO FORNECEDOR. POR FAVOR CONTACTE O SUPORTE."	
										ELSE
											lcInfForn = "O Fornecedor n�o tem quantidade dispon�vel para satisfazer o seu pedido."
											
											TEXT TO lcSQL TEXTMERGE NOSHOW 
												UPDATE bo SET logi1 = 1, FECHADA = 1 WHERE bostamp = '<<ALLTRIM(uCrsAuxResposta.bostamp)>>'
											ENDTEXT		
											IF !uf_gerais_actGrelha("","",lcSQL)
												lcEstadoDeEnvio = .f.	
												lcRespErro  =	"OCORREU UMA ANOMALIA AO VERIFICAR A RESPOSTA DO FORNECEDOR. POR FAVOR CONTACTE O SUPORTE."		
											ENDIF
										ENDIF
									ENDIF

									&&  Mostra a informa��o com qtt pedida e qtt fornecida pelo forn (n�o foi testado mt bem pq os acessos de testes aos fornecedores estavam em baixo)
									LOCAL lcInfForn
									STORE '' TO lcInfForn
				

									SELECT uCrsAuxRespostaDetalhe
									GO TOP 
									SCAN 
										lcInfForn = lcInfForn + CHR(13) + ALLTRIM(uCrsAuxRespostaDetalhe.ref) + ' ' + ALLTRIM(uCrsAuxRespostaDetalhe.design) + ' Qtt Pedida '  + ALLTRIM(STR(uCrsAuxRespostaDetalhe.qt_pedida))  + ' Qtt Entregue '  + ALLTRIM(STR(uCrsAuxRespostaDetalhe.qt_entregue))
									ENDSCAN 
									lcEstadoDeEnvio = .t.	
									uf_perguntalt_chama(LEFT(lcInfForn,200),"OK","",16)
									regua(2)							
								ENDIF
							ENDIF
					

						ELSE
							lcSQL = ''
							TEXT TO lcSQL TEXTMERGE NOSHOW 
                                select TOP 2 via_verde_med_d.*, via_verde_med.estado 
                                FROM via_verde_med(nolock)
								left outer join  via_verde_med_d (nolock) on via_verde_med.id = via_verde_med_d.id_via_verde_med
								WHERE via_verde_med.id = '<<ALLTRIM(uCrsAuxResposta.id)>>' 
							ENDTEXT		
						
							IF !uf_gerais_actGrelha("","uCrsAuxRespostaDetalhe",lcSQL)
								lcEstadoDeEnvio = .f.	
								lcRespErro  =	"OCORREU UMA ANOMALIA AO VERIFICAR A RESPOSTA DO FORNECEDOR. POR FAVOR CONTACTE O SUPORTE."	
						
							ELSE
								LOCAL lcInfForn
								STORE '' TO lcInfForn
								
								SELECT uCrsAuxRespostaDetalhe
								GO TOP 
								SCAN 
									IF ALLTRIM(uCrsAuxRespostaDetalhe.estado) = "Sem Produto"
										lcInfForn=  lcInfForn + "O Fornecedor n�o tem quantidade dispon�vel para satisfazer o seu pedido."
										TEXT TO lcSQL TEXTMERGE NOSHOW 
											UPDATE bo SET logi1 = 1, FECHADA = 1 WHERE bostamp = '<<ALLTRIM(uCrsAuxResposta.bostamp)>>'
										ENDTEXT		
										IF !uf_gerais_actGrelha("","",lcSQL)
											lcEstadoDeEnvio = .f.	
											lcRespErro  =	"OCORREU UMA ANOMALIA AO VERIFICAR A RESPOSTA DO FORNECEDOR. POR FAVOR CONTACTE O SUPORTE."		
										ENDIF
										lcEstadoDeEnvio = .T.	
									ELSE
										lcInfForn = lcInfForn + CHR(13) + ALLTRIM(uCrsAuxRespostaDetalhe.response_descr)
										lcEstadoDeEnvio = .f.	
									ENDIF
								ENDSCAN 
								uf_perguntalt_chama(LEFT(lcInfForn,100),"OK","",16)
								regua(2)
							ENDIF
					 	ENDIF
					 
						
					ELSE
						lcEstadoDeEnvio = .f.	
						lcRespErro  =	"OCORREU UMA ANOMALIA AO VERIFICAR A RESPOSTA DO FORNECEDOR. POR FAVOR CONTACTE O SUPORTE."	
						regua(2)						
					ENDIF
						
					IF USED("uCrsAuxResposta")
						fecha("uCrsAuxResposta")
					ENDIF
					IF USED("uCrsAuxRespostaDetalhe")
						fecha("uCrsAuxRespostaDetalhe")
					ENDIF
			
				ENDIF		
				regua(2)
					
			ELSE
					&& Encomenda do Tipo Normal - pode ser enviada por postal normal ou por LTS ESB
				IF !EMPTY(uCrsTipoFl.usa_esb) && encomenda ESB
					
					LOCAL  lcRespErro, lcEstadoDeEnvio 
					STORE ''  TO lcRespErro
					STORE .f. TO lcEstadoDeEnvio 
				
					&& id do fornecedor no ESB
					IF EMPTY(uCrsTipoFl.id_esb)
						lcEstadoDeEnvio = .f.	
						lcRespErro  = "Falta preencher o c�digo do fornecedor no ESB. Por favor contacte o suporte."
					ENDIF
					
					LOCAL  lcWsPath, lcIDCliente, lcNomeJar, lcToken
					STORE '' TO lcWsPath, lcIDCliente, lcNomeJar, lcToken
					
					lcToken = uf_gerais_stamp()
					
					SELECT uCrsE1
					IF EMPTY(ALLTRIM(uCrsE1.id_lt))
						lcEstadoDeEnvio = .f.	
						lcRespErro  = "O C�DIGO DE CLIENTE LOGITOOLS N�O EST� CORRETAMENTE PREENCHIDO. POR FAVOR CONTACTE O SUPORTE."
					ENDIF 
					lcIDCliente = ALLTRIM(uCrsE1.id_lt)
					
					lcNomeJar = 'LTSESBOrdersClient.jar'
					IF !FILE(ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\LTSESBOrdersClient\' + ALLTRIM(lcNomeJar))
						lcEstadoDeEnvio = .f.	
						lcRespErro = "O SOFTWARE DE ENVIO LTS ESB N�O EST� CONFIGURADO. POR FAVOR CONTACTE O SUPORTE."
					ENDIF 
					
					regua(1,2,"A enviar Encomenda, por favor aguarde.")
					
					&& Define de usa testes ou n�o 
					&& verifica se utiliza webservice de testes
					LOCAL lcDefineWsTest
					STORE 0 TO lcDefineWsTest
					IF uf_gerais_getParameter("ADM0000000227","BOOL") == .f.
						lcDefineWsTest = 0
					ELSE
						lcDefineWsTest = 1
					ENDIF 
						
					lcWsPath = ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\LTSESBOrdersClient\' + ALLTRIM(lcNomeJar) +  ' --requestType=POST_ORDERS';
						+ ' "--idOrder=' + ALLTRIM(ucrsEncomendaAEnviar.stamp) + ["];
						+ ' "--entity=' + ALLTRIM(uCrsTipoFl.id_esb) + ["];
						+ ' "--idCl=' + lcIDCliente + ["];
						+ ' "--site=' + UPPER(ALLTRIM(mySite)) + ["];
						+ ' --siteNr=' + ALLTRIM(STR(mysite_nr));
						+ ' --test=' + ALLTRIM(STR(lcDefineWsTest));
						+ ' "--Token=' + ALLTRIM(lcToken) + ["]

					lcWsPath = "javaw -jar " + lcWsPath 
						
									
					oWSShell = CREATEOBJECT("WScript.Shell")
					oWSShell.Run(lcWsPath, 1, .t.)
					
					regua(2)
					
					lcSQL = ''
					TEXT TO lcSQL TEXTMERGE NOSHOW 
						SELECT TOP 1 * FROM ext_esb_msgStatus (nolock) WHERE token = '<<lcToken>>'
					ENDTEXT		
					IF !uf_gerais_actGrelha("","uCrsAuxRespostaDetalheESB",lcSQL)
						lcEstadoDeEnvio = .f.	
						lcRespErro = "OCORREU UMA ANOMALIA AO VERIFICAR A RESPOSTA DO FORNECEDOR. POR FAVOR CONTACTE O SUPORTE."
					ELSE
						LOCAL lcInfForn
						STORE '' TO lcInfForn

						IF ALLTRIM(uCrsAuxRespostaDetalheESB.id) == '200.1'
							lcEstadoDeEnvio = .t.				
						ELSE
							lcInfForn = ALLTRIM(uCrsAuxRespostaDetalheESB.descr)
							lcEstadoDeEnvio = .f.	
							lcRespErro = LEFT(lcInfForn,200)
						ENDIF		
					ENDIF
				
					IF USED("uCrsAuxRespostaDetalheESB")
						fecha("uCrsAuxRespostaDetalheESB")
					ENDIF
				
				ELSE
					LOCAL postalPath, lcFornNo, lcFornEstab, lcEstadoDeEnvio, lcRespErro, lcNrDoc, lcStampEnc,lcEstadoDeEnvio 
					STORE '' TO postalPath, lcFornNo, lcFornEstab, lcEstadoDeEnvio, lcRespErro, lcNrDoc, lcStampEnc
					STORE .f. TO lcEstadoDeEnvio 
					postalPath = uf_gerais_getParameter('ADM0000000185', 'TEXT')
					IF !(RIGHT(postalPath,1)=="\")
						postalPath = alltrim(postalPath) + '\'
					ENDIF
					postalPath = postalPath + "Postal\Postal.jar"
					
					
					lcStampEnc= ucrsEncomendaAEnviar.stamp
					lcNrDoc= ucrsEncomendaAEnviar.nrDoc
					lcFornNo = ucrsEncomendaAEnviar.no
					lcFornEstab = ucrsEncomendaAEnviar.estab 
				
					if !file('&postalPath')
						lcRespErro="O POSTAL N�O EST� A SER ENCONTRADO. ASSIM N�O � POSS�VEL ENVIAR A ENCOMENDA. POR FAVOR CONTACTE O SUPORTE."
						lcEstadoDeEnvio = .f.
					ENDIF
					
					lcSQL = ''
					TEXT TO lcSQL TEXTMERGE NOSHOW 
						UPDATE bo SET logi1 = 1 WHERE bostamp = '<<ALLTRIM(lcStampEnc)>>'
					ENDTEXT		
					IF !uf_gerais_actGrelha("","",lcSQL)
						lcEstadoDeEnvio = .f.	
						lcRespErro  =	"OCORREU UMA ANOMALIA AO VERIFICAR A RESPOSTA DO FORNECEDOR. POR FAVOR CONTACTE O SUPORTE."		
					ENDIF
					
					postalPath = "javaw -jar " + postalPath + " " + ALLTRIM(STR(lcFornNo)) + ' ' + ALLTRIM(STR(lcFornEstab)) + ' ' + ALLTRIM(STR(lcNrDoc)) + [ "] + ALLTRIM(lcStampEnc) + [" ] +ALLTRIM(STR(ch_userno)) + [ "] + ALLTRIM(mySite) + [" "] + ALLTRIM(sql_db) +  ["] 
				
					oWSShell = CREATEOBJECT("WScript.Shell")
					oWSShell.Run(postalPath,1,.t.)
					
					lcSQL = ''
					TEXT TO lcSQL TEXTMERGE NOSHOW 
						select TOP 1 logi1 FROM bo(nolock) WHERE logi1 = 1 and bostamp = '<<ALLTRIM(lcStampEnc)>>' ORDER BY ousrdata desc
					ENDTEXT 
					
					IF !uf_gerais_actGrelha("","uCrsAuxResposta",lcSQL)
						lcEstadoDeEnvio = .f.	
						lcRespErro  =	"OCORREU UMA ANOMALIA A ATUALIZAR A ENCOMENDA. POR FAVOR CONTACTE O SUPORTE."+CHR(10)+"Cod: Enc. At."
					ELSE
						if(uCrsAuxResposta.logi1==.t.) 
							lcEstadoDeEnvio = .t.	
						else
							lcEstadoDeEnvio = .f.	
						endif
					ENDIF
					
					fecha("uCrsAuxResposta") 	
					regua(2)
					&&uf_perguntalt_chama("Encomenda criada com sucesso!","OK","",64)
				ENDIF
			ENDIF	
				
			SELECT 	ucrsEncomendaAEnviar
			REPLACE estadoEnviado  WITH  lcEstadoDeEnvio 
			REPLACE respErro       WITH  lcRespErro 
			
	 	ENDIF
	 	 		  	
	ENDSCAN
	
	IF USED("uCrsTipoFl")
		fecha("uCrsTipoFl")
	ENDIF

ENDFUNC


FUNCTION uf_encautomaticas_selViaVerde
	SELECT crsEnc
	
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE noshow
		SELECT COUNT(ref) as viaVerde FROM emb_via_verde (nolock) WHERE  Ref = '<<ALLTRIM(crsEnc.ref)>>' and  tipo='VVM' and (data_fim='19000101' or  data_fim<=GETDATE()) 
	ENDTEXT
	IF !uf_gerais_actGrelha("","uCrsEncomendaViaVerde",lcSQL)
		uf_perguntalt_chama("OCORREU A VERIFICAR O ESTADO VIA VERDE DO MEDICAMENTO! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	ENDIF
	
	IF uCrsEncomendaViaVerde.viaVerde > 0
		RETURN .t.
	ELSE
		uf_perguntalt_chama("ESTE MEDICAMENTO N�O PODE SER ENCOMENDADO POR VIA VERDE.","OK","",16)
		RETURN .f.
	ENDIF	
ENDFUNC


FUNCTION uf_encautomaticas_mostra_totencs
	select Fornecedor, count(ref) as Artigos , sum(eoq) as Quantidades, round(SUM(IIF(crsenc.pclfornec==0,crsenc.epcult,crsenc.pclfornec)*(crsenc.eoq-crsenc.qtbonus)),2) as Valor from crsenc where enc=.t. group by fornecedor into cursor ucrstotenc readwrite
	sele ucrstotenc 
	uf_valorescombo_chama("", 2, "ucrstotenc", 2, "fornecedor", "Fornecedor, Artigos , Quantidades,Valor ",.f.,.f.,.t.,.f.,.t.)
	
	fecha("ucrstotenc")

ENDFUNC 

FUNCTION uf_encautomaticas_atualizaST
	LOCAL lcactstmax, lcactptoenc
	STORE '' TO lcactstmax, lcactptoenc
	SELECT crsenc
	GO TOP 
	SCAN 
		STORE '' TO lcactstmax, lcactptoenc
		IF crsenc.stmax <> crsenc.ostmax
			lcactstmax = 'stmax = ' + ALLTRIM(STR(crsenc.stmax))
		ENDIF 
		IF crsenc.ptoenc <> crsenc.optoenc
			IF len(lcactstmax)=0
				lcactptoenc = 'ptoenc = ' + ALLTRIM(STR(crsenc.ptoenc))
			ELSE
				lcactptoenc = ', ptoenc = ' + ALLTRIM(STR(crsenc.ptoenc))
			ENDIF 
		ENDIF 
		IF len(lcactstmax)>0 OR len(lcactptoenc)>0
			TEXT TO lcSQL TEXTMERGE NOSHOW 
				UPDATE st
				SET <<lcactstmax>> <<lcactptoenc>> 
				WHERE st.ref='<<ALLTRIM(crsenc.ref)>>' and st.site_nr=<<mysite_nr>>
			ENDTEXT				

			IF !(uf_gerais_actGrelha("","", lcSQL))
				uf_perguntalt_chama("Ocorreu uma anomalia ao atualizar a ficha do cliente. Por favor contacte o Suporte.","OK","",16)
			ENDIF
		ENDIF 
	ENDSCAN 
ENDFUNC 


FUNCTION uf_encautomaticas_enviarEncomenda
	LPARAMETERS StampCab
	
	IF USED("CabDOC")
		Fecha("CabDOC")
	ENDIF 
	CREATE CURSOR CABDOC (	CABSTAMP C(40), DOC C(80), NUMINTERNODOC N(9), NUMDOC C(20), NOME C(80), NOME2 C(60), TIPO C(20), NO N(9), ESTAB N(9), ;
							CONDPAG C(254), DATAVENC D, MOEDA C(254), DATADOC D, DATAINTERNA D, DATAREGISTO D, ESTADODOC C(254), DESCFIN N(15,6), ;
							VALORDESCFIN N(9,3), DOCFL C(254), DOCFLNO C(34), NOINTERNO N(9), CONTADORDOC N(9), BASEINC N(15,3), IVA N(15,3), TOTAL N(15,3), ;
							VALORIVA1 N(12,3), VALORIVA2 N(12,3), VALORIVA3 N(12,3), VALORIVA4 N(12,3), VALORIVA5 N(12,3), VALORIVA6 N(12,3), VALORIVA7 N(12,3), VALORIVA8 N(12,3), VALORIVA9 N(12,3), VALORIVA10 N(12,3),VALORIVA11 N(12,3), VALORIVA12 N(12,3),VALORIVA13 N(12,3),;
							TOTALNSERVICO N(9,3), ESTADO C(1), HORAREGISTO C(8), USERREGISTO C(60), DATAALTERA D, HORAALTERA C(8), USERALTERA C(60), ;
							ENCENVIADA N(1), DATAENTREGA D, ARMAZEM N(2), LOGI1 L, PLANO L, ;
							MORADA C(90), LOCAL C(254), CODPOST C(254), NCONT C(30), EMAIL C(45), TELEFONE C(30), ;
							FOID N(9), U_DOCCONT N(9), U_CLASS C(120), OBS C(254), DATAFINAL D,;
							XPDDATA D, XPDHORA C(5), XPDVIATURA C(60), XPDMORADA C(55), XPDCODPOST C(40), XPDTELEFONE C(50), XPDCONTACTO C(50), XPDEMAIL C(45), PAIS N(9,0),CCUSTO C(20),ATDocCode C(20), TABIVA N(5,0), ;
							BLOQPAG L, EXPORTADO L, EAIVAIN n(19,6), EDESCC n(19,6), arredondaPVP L, consignacao L, id_tesouraria_conta i, ousrdata D, ousrHORA C(5), imput_desp N(15,2), NRRECEITA C(50) , CODEXT C(50), SITE C(20), anexos L,;
							u_dataentr D, u_horaentr c(5), entrega C(60), pagamento C(60), momentopagam C(60), estadoenc C(60), pinacesso c(10), pinopcao c(10);
						)
				
	**Limpa Cursores
	IF USED("cabDoc")
		Select CabDoc
		GO TOP 
		SCAN
			Delete
		ENDSCAN
	ENDIF
	
	uf_encautomaticas_preencheCabDoc(ALLTRIM(StampCab))
	
	LOCAL lcSQL
	STORE '' TO lcSQL
	
	&& Verifica a informa��o do fornecedor
	lcSQL = ''
	TEXT TO lcSql NOSHOW TEXTMERGE
		select tipo, usa_esb, id_esb from fl (nolock) LEFT JOIN fl_site ON fl.no = fl_site.nr_fl AND fl.estab = fl_site.dep_fl where no = <<cabDoc.no>> and estab = <<cabDoc.estab>> and site_nr = <<mysite_nr>>
	ENDTEXT
	IF !uf_gerais_actGrelha("", "uCrsTipoFl",lcSql)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VERIFICAR TIPO DO FORNECEDOR. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	ENDIF

	&& Valida Envio de Encomenda para Grupo
	Select uCrsTipoFl
	IF ALLTRIM(UPPER(uCrsTipoFl.tipo)) == 'GRUPO'  && Encomendas de GRUPO
		uf_condicoesComerciais_enviarEncomenda()
	ELSE 
	
		&& Verificar Loja Da Encomenda
		TEXT TO lcSql NOSHOW TEXTMERGE
			select site from bo (nolock) where bostamp = '<<cabDoc.cabstamp>>' 
		ENDTEXT
		IF !uf_gerais_actGrelha("", "uCrsSiteEnc",lcSql)
			uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VERIFICAR O LOCAL DA ENCOMENDA. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			RETURN .f.
		ENDIF
		
		SELECT uCrsSiteEnc
		IF !(ALLTRIM(uCrsSiteEnc.site) == ALLTRIM(mysite))
			If !uf_perguntalt_chama("Aten��o: A encomenda foi feita no local: " + ALLTRIM(uCrsSiteEnc.site) + chr(13) + "Est� a enviar a encomenda a partir do local: " + ALLTRIM(mysite) + CHR(13) + "Vai usar as configura��es do local atual. Pretende continuar?","Sim","N�o")
				RETURN .f.
			EndIf	
		ENDIF 
		IF USED("uCrsSiteEnc")
			fecha("uCrsSiteEnc")
		ENDIF 
		**
	
		&& Prepara o envio da Encomenda
		SELECT cabdoc
		
		&& Encomenda do Tipo Normal - pode ser enviada por postal normal ou por LTS ESB
		IF !EMPTY(uCrsTipoFl.usa_esb) && encomenda ESB e n�o b2b

			&& id do fornecedor no ESB
			IF EMPTY(uCrsTipoFl.id_esb)
				uf_perguntalt_chama("Falta preencher o c�digo do fornecedor no ESB. Por favor contacte o suporte.","OK","", 16)
				RETURN .f.
			ENDIF
			
			&& Guardar caminho do software de envio
			LOCAL  lcWsPath, lcIDCliente, lcNomeJar, lcToken
			STORE '' TO lcWsPath, lcIDCliente, lcNomeJar, lcToken
			
			&& atribui valor ao lcToken que depois � necess�rio para aceder � informa��o que � escrita na BD
			lcToken = uf_gerais_stamp()
			
			&& id do cliente LTS
			SELECT uCrsE1
			IF EMPTY(ALLTRIM(uCrsE1.id_lt))
				uf_perguntalt_chama("O C�DIGO DE CLIENTE LOGITOOLS N�O EST� CORRETAMENTE PREENCHIDO. POR FAVOR CONTACTE O SUPORTE.","OK","", 16)
				RETURN .f.
			ENDIF 
			lcIDCliente = ALLTRIM(uCrsE1.id_lt)
			
 			&& valida e configura software p envio encomenda
			lcNomeJar = 'LTSESBOrdersClient.jar'
			IF !FILE(ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\LTSESBOrdersClient\' + ALLTRIM(lcNomeJar))
				uf_perguntalt_chama("O SOFTWARE DE ENVIO LTS ESB N�O EST� CONFIGURADO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				RETURN .f.
			ENDIF 
			
			regua(1,2,"A enviar Encomenda, por favor aguarde.")
			
			&& Define de usa testes ou n�o 
			&& verifica se utiliza webservice de testes
			LOCAL lcDefineWsTest
			STORE 0 TO lcDefineWsTest
			IF uf_gerais_getParameter("ADM0000000227","BOOL") == .f.
				lcDefineWsTest = 0
			ELSE
				lcDefineWsTest = 1
			ENDIF 
				
			lcWsPath = ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\LTSESBOrdersClient\' + ALLTRIM(lcNomeJar) +  ' --requestType=POST_ORDERS';
				+ ' "--idOrder=' + ALLTRIM(cabdoc.cabstamp) + ["];
				+ ' "--entity=' + ALLTRIM(uCrsTipoFl.id_esb) + ["];
				+ ' "--idCl=' + lcIDCliente + ["];
				+ ' "--site=' + UPPER(ALLTRIM(mySite)) + ["];
				+ ' --siteNr=' + ALLTRIM(STR(mysite_nr));
				+ ' --test=' + ALLTRIM(STR(lcDefineWsTest));
				+ ' "--Token=' + ALLTRIM(lcToken) + ["]

			&& Envia comando Java		
			lcWsPath = "javaw -jar " + lcWsPath 		
							
			&& aguarda que software termine o envio da informa��o 3� PARAMETRO = .T.
			oWSShell = CREATEOBJECT("WScript.Shell")
			oWSShell.Run(lcWsPath, 1, .t.)
			
			regua(2)
			
			&& mostra informa��o retornada pelo fornecedor.
			lcSQL = ''
			TEXT TO lcSQL TEXTMERGE NOSHOW 
				SELECT TOP 1 * FROM ext_esb_msgStatus (nolock) WHERE token = '<<lcToken>>'
			ENDTEXT		
			IF !uf_gerais_actGrelha("","uCrsAuxRespostaDetalheESB",lcSQL)
				uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VERIFICAR A RESPOSTA DO FORNECEDOR. POR FAVOR CONTACTE O SUPORTE.", "Ok", "", 16)
				RETURN .f.
			ELSE
				LOCAL lcInfForn
				STORE '' TO lcInfForn

				IF ALLTRIM(uCrsAuxRespostaDetalheESB.id) == '200.1'
					uf_perguntalt_chama("ENCOMENDA ENVIADA COM SUCESSO.", "Ok", "", 64)					
				ELSE
					lcInfForn = ALLTRIM(uCrsAuxRespostaDetalheESB.descr)
					uf_perguntalt_chama(LEFT(lcInfForn,200),"OK","",16)
				ENDIF		
			ENDIF
		
			IF USED("uCrsAuxRespostaDetalheESB")
				fecha("uCrsAuxRespostaDetalheESB")
			ENDIF
		
		ELSE
			&& Guardar caminho do Postal
			LOCAL postalPath
			postalPath = uf_gerais_getParameter('ADM0000000185', 'TEXT')
			IF !(RIGHT(postalPath,1)=="\")
				postalPath = alltrim(postalPath) + '\'
			ENDIF
			postalPath = postalPath + "Postal\Postal.jar"
					
			** valida exist�ncia do ficheiro
			IF !file('&postalPath')
				uf_perguntalt_chama("O POSTAL N�O EST� A SER ENCONTRADO. ASSIM N�O � POSS�VEL ENVIAR A ENCOMENDA. POR FAVOR CONTACTE O SUPORTE.","OK","", 48)
				RETURN .f.
			ENDIF

			** Criar parametros para Postal 
			SELECT cabdoc
			postalPath = "javaw -jar " + postalPath + " " + ALLTRIM(STR(cabdoc.no)) + ' ' + ALLTRIM(STR(cabdoc.estab)) + ' ' + ALLTRIM(cabdoc.numdoc) + [ "] + ALLTRIM(cabdoc.cabstamp) + [" ] +ALLTRIM(STR(ch_userno)) + [ "] + ALLTRIM(mySite) + [" "] + ALLTRIM(sql_db) +  ["] 

			** Executar Postal (enviar a encomenda)
			oWSShell = CREATEOBJECT("WScript.Shell")
			oWSShell.Run(postalPath,1,.t.)
		ENDIF
	ENDIF
	IF USED("CabDOC")
		Fecha("CabDOC")
	ENDIF 	
	IF USED("uCrsTipoFl")	
		fecha("uCrsTipoFl")
	ENDIF 
ENDFUNC


FUNCTION uf_encautomaticas_preencheCabDoc
	LPARAMETERS tcStamp
	
	IF uf_gerais_actGrelha("", "ltBO", "SELECT * FROM BO (nolock) WHERE BO.BOSTAMP = '" + ALLTRIM(tcStamp) + "'")
		PUBLIC lc
		IF uf_gerais_actGrelha("", "ltBO2", "SELECT * FROM BO2 (nolock) WHERE BO2.BO2STAMP = '" + ALLTRIM(tcstamp) + "'")
			lcNrRegistos = 0
			Select CabDoc
			APPEND BLANK
			IF ltBO.FECHADA = .f.
				lcestadodoc = "A"
			ELSE 
				lcestadodoc = "F"
			ENDIF 
			REPLACE cabDoc.cabstamp			with	ALLTRIM(ltBO.BOSTAMP)
			REPLACE CabDoc.DOC 				With	ALLTRIM(ltBO.NMDOS)
			REPLACE CabDoc.NUMINTERNODOC 	With	ltBO.NDOS
			REPLACE CabDoc.NUMDOC			with	ALLTRIM(STR(ltBO.OBRANO))
			REPLACE CabDoc.NOME				With	ALLTRIM(ltBO.NOME)
			REPLACE cabdoc.nome2			With	ALLTRIM(ltBO.nome2)
			REPLACE CabDoc.NO				With	ltBO.NO
			REPLACE CabDoc.ESTAB			With	ltBO.ESTAB
			REPLACE CabDoc.CONDPAG			With	ltBO.TPDESC
			REPLACE CabDoc.MOEDA			With	ltBO.MOEDA
			REPLACE CabDoc.DATADOC			With	ltBO.DATAOBRA
			REPLACE cabdoc.dataentrega		With	ltBO.u_dataentr
			REPLACE CabDoc.ESTADODOC		With	lcestadodoc 
			REPLACE CabDoc.CONTADORDOC		With	ltBO2.U_DOCCONT
			REPLACE CabDoc.BASEINC			With	ltBO.etotaldeb
			REPLACE CabDoc.IVA				With	ltBO2.ETOTIVA
			REPLACE CabDoc.Total			With	ROUND(ltBO.etotal,2)
			REPLACE CabDoc.valorIva1		With	ltBO.EBO11_IVA
			REPLACE CabDoc.valorIva2		With	ltBO.EBO21_IVA
			REPLACE CabDoc.valorIva3		With	ltBO.EBO31_IVA
			REPLACE CabDoc.valorIva4		With	ltBO.EBO41_IVA
			REPLACE CabDoc.valorIva5		With	ltBO.EBO51_IVA
			REPLACE CabDoc.valorIva6		With	ltBO.EBO61_IVA
			REPLACE CabDoc.valorIva7		With	ltBO2.EBO71_IVA
			REPLACE CabDoc.valorIva8		With	ltBO2.EBO81_IVA
			REPLACE CabDoc.valorIva9		With	ltBO2.EBO91_IVA
			REPLACE CabDoc.valorIva10		With	ltBO2.EBO101_IVA
			REPLACE CabDoc.valorIva11		With	ltBO2.EBO111_IVA
			REPLACE CabDoc.valorIva12		With	ltBO2.EBO121_IVA
			REPLACE CabDoc.valorIva13		With	ltBO2.EBO131_IVA
			REPLACE CabDoc.ousrdata			With	ltBO.ousrdata
			REPLACE CabDoc.ousrhora			With	ltBO.ousrhora
			replace cabdoc.site 			WITH 	ltBO.site
			REPLACE CabDoc.U_CLASS		WITH	ltBO2.U_CLASS
			REPLACE cabDoc.DATAREGISTO	With ltBO.ousrdata
			REPLACE cabDoc.HORAREGISTO	With ltBO.ousrhora
			REPLACE cabDoc.USERREGISTO	With ltBO.ousrinis
			REPLACE cabDoc.DATAALTERA 	With ltBO.usrdata
			REPLACE cabDoc.HORAALTERA	With ltBO.usrhora
			REPLACE cabDoc.USERALTERA	With ltBO.usrinis
			REPLACE cabDoc.Armazem		With ltBO2.ARMAZEM
			REPLACE cabDoc.NRRECEITA    WITH ltBO2.nrReceita
			REPLACE cabDoc.pinacesso    WITH ltBO2.pinacesso
			REPLACE cabDoc.pinopcao    	WITH ltBO2.pinopcao
			REPLACE cabDoc.LOGI1 		With  ltBO.logi1
			REPLACE cabDoc.Morada 		With ltBO.Morada
			REPLACE cabDoc.Local 		With ltBO.Local
			REPLACE cabDoc.codpost		With ltBO.CodPost
			REPLACE cabDoc.ncont 		With ltBO.ncont
			IF ALLTRIM(ltBO.NMDOS)!='Encomenda de Cliente'
				REPLACE cabDoc.obs 			With ltBO.obs
			ELSE
				REPLACE cabDoc.obs 			With ALLTRIM(ltBO.obs) + CHR(13) + 'Zona:' + alltrim(ltBO2.codpost) + CHR(13) + 'Envio:' + alltrim(ltBO2.modo_envio) + CHR(13) + 'Pagamento:' + alltrim(ltBO2.pagamento) + CHR(13) + 'Status:' + alltrim(ltBO2.status) 
			ENDIF 
			REPLACE cabDoc.datafinal 	With ltBO.datafinal
			replace cabdoc.xpddata 		With ltBO2.xpddata
			replace cabdoc.xpdhora 		With ltBO2.xpdhora
			replace cabdoc.xpdviatura 	With ltBO2.xpdviatura
			replace cabdoc.xpdmorada 	With ltBO2.morada
			replace cabdoc.xpdcontacto	With ltBO2.contacto
			replace cabdoc.xpdcodpost	With ltBO2.codpost
			replace cabdoc.xpdemail		With ltBO2.email
			replace cabdoc.xpdtelefone	With ltBO2.telefone
			replace cabdoc.ATDocCode	WITH ltBO2.ATDocCode
			replace cabdoc.tabIva		WITH ltBO.TABIVA
			replace cabdoc.u_dataentr	WITH ltBO.u_dataentr
			SET HOURS TO 24
			replace cabdoc.u_horaentr	WITH SUBSTR(TTOC(ltBO.u_dataentr),10,5)
			REPLACE cabdoc.ccusto		With ltBO.ccusto
			REPLACE cabdoc.plano		WITH .f.
			REPLACE cabdoc.exportado	WITH ltBO.exportado
			REPLACE CabDoc.bloqpag		WITH .f.
			IF uf_gerais_getParameter("ADM0000000299","BOOL")=.t.
				REPLACE cabDoc.arredondaPVP WITH .t. 
			ELSE
				REPLACE cabDoc.arredondaPVP WITH .f.				
			ENDIF 
			replace cabdoc.codext WITH ltBO2.codext
			replace cabdoc.edescc	WITH ROUND(ltBO.edescc,2)
			replace cabdoc.entrega WITH ltBO2.modo_envio
			replace cabdoc.pagamento WITH ltBO2.pagamento
			replace cabdoc.momentopagam WITH ltBO2.momento_pagamento
			replace cabdoc.estadoenc WITH ltBO2.status
			** INFORMA��O LINHAS
			TEXT TO lcSQL NOSHOW TEXTMERGE 
				exec up_Documentos_linhas 'BO','<<Alltrim(tcStamp)>>', <<mysite_nr>>
			ENDTEXT		
							
			IF !uf_gerais_actGrelha("", "BI", lcSQL)
				uf_perguntalt_chama("N�O FOI POSS�VEL DETERMINAR OS DADOS DE LINHAS DO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE!","OK","",16)
				RETURN .f.
			ENDIF
		
			IF USED("BI2")
				Fecha("BI2")
			ENDIF
			
			lcSQL = ''
			IF EMPTY(ALLTRIM(tcStamp))
				TEXT TO lcSQL NOSHOW TEXTMERGE 
					SET FMTONLY on
					SELECT TOP 0 * FROM BI2 (nolock) 
					SET FMTONLY off
				ENDTEXT 
			ELSE
				TEXT TO lcSQL NOSHOW TEXTMERGE 
					SELECT * 
					FROM 
						BI2 (nolock) 
					WHERE 
						BOSTAMP = '<<Alltrim(tcStamp)>>'
				ENDTEXT		
			ENDIF
			IF !uf_gerais_actGrelha("", "BI2", lcSQL)
				uf_perguntalt_chama("N�O FOI POSS�VEL DETERMINAR OS DADOS DE LINHAS DO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE!(2)","OK","",16)
				RETURN .f.
			ENDIF
		
			** Dados dos Totais
			IF EMPTY(ALLTRIM(tcStamp))
				TEXT TO lcSQL NOSHOW TEXTMERGE 
					SELECT TOP 0 * FROM bo_totais
				ENDTEXT 
			ELSE
				TEXT TO lcSQL NOSHOW TEXTMERGE 
					SELECT * FROM bo_totais WHERE STAMP = '<<Alltrim(tcStamp)>>'
				ENDTEXT		
			ENDIF
			IF !uf_gerais_actGrelha("", "ucrsTotaisDocumento", lcSQL)
				uf_perguntalt_chama("N�O FOI POSS�VEL DETERMINAR OS TOTAIS DO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE!","OK","",16)
				RETURN .f.
			ENDIF
		
		ENDIF
		
	ENDIF
		
ENDFUNC


FUNCTION uf_encautomaticas_format_rodapebonus
	**encautomaticas.Detalhest1.Pageframe1.PAGE9.chart1.left=10
	**encautomaticas.Detalhest1.Pageframe1.PAGE9.label1.left=70
	**encautomaticas.Detalhest1.Pageframe1.PAGE9.label6.left=10
	**encautomaticas.Detalhest1.Pageframe1.PAGE9.vendasultmes.left=115
	**encautomaticas.Detalhest1.Pageframe1.PAGE9.label8.left=170
	**encautomaticas.Detalhest1.Pageframe1.PAGE9.vendasultmesgh.left=300
	**encautomaticas.Detalhest1.Pageframe1.PAGE9.grdbonusresumo.left=360
	**encautomaticas.Detalhest1.Pageframe1.PAGE9.btnimg1.left=360
	**encautomaticas.Detalhest1.Pageframe1.PAGE9.btnimg1.visible=.f.
	**encautomaticas.Detalhest1.Pageframe1.PAGE9.lblleg1.left=360
	**encautomaticas.Detalhest1.Pageframe1.PAGE9.lblleg2.left=360
	**encautomaticas.Detalhest1.Pageframe1.PAGE9.grdbonusresumo.width=encautomaticas.width-500
ENDFUNC 

FUNCTION uf_encautomaticas_consultab2b_infolinha
    
	uf_encautomaticas_format_rodapebonus()
	uf_encautomaticas_consultab2b_singleline()

	IF USED("uCrsBonusResumoB2B")
		SELECT uCrsBonusResumoB2B 
		GO TOP 
		SCAN 
			IF ALLTRIM(uCrsBonusResumoB2B.ref) = ALLTRIM(crsenc.ref)
				LOCAL lcEncontra
				STORE .f. TO lcEncontra
				SELECT uCrsBonusResumo
				GO TOP 
				SCAN
					IF ALLTRIM(uCrsBonusResumo.fornecedor) = ALLTRIM(uCrsBonusResumoB2B.fornecedor)
						replace uCrsBonusResumo.ref WITH ALLTRIM(uCrsBonusResumoB2B.ref)
						replace uCrsBonusResumo.design  WITH ALLTRIM(uCrsBonusResumoB2B.design)
						**replace uCrsBonusResumo.bonus WITH ALLTRIM(uCrsBonusResumoB2B.bonus)
						replace uCrsBonusResumo.fornecedor WITH ALLTRIM(uCrsBonusResumoB2B.fornecedor)
						replace uCrsBonusResumo.nrfornecedor WITH uCrsBonusResumoB2B.nrfornecedor
						replace uCrsBonusResumo.ststamp WITH ALLTRIM(uCrsBonusResumoB2B.ststamp)
						replace uCrsBonusResumo.validade WITH uCrsBonusResumoB2B.validade
						replace uCrsBonusResumo.infoconsulta WITH ALLTRIM(uCrsBonusResumoB2B.infoconsulta)
						replace uCrsBonusResumo.disp WITH ALLTRIM(uCrsBonusResumoB2B.disp)
						replace uCrsBonusResumo.pct WITH uCrsBonusResumoB2B.pct
						**replace uCrsBonusResumo.dc WITH uCrsBonusResumoB2B.dc
						replace uCrsBonusResumo.pcl WITH uCrsBonusResumoB2B.pcl
						replace uCrsBonusResumo.dtentrega WITH uCrsBonusResumoB2B.dtentrega
                        replace uCrsBonusResumo.pvp WITH uCrsBonusResumoB2B.pvp
						lcEncontra = .t.
					ENDIF 
				ENDSCAN 
				IF lcEncontra = .f.
					SELECT uCrsBonusResumo
					GO BOTTOM 
					APPEND BLANK 
					replace uCrsBonusResumo.ref WITH ALLTRIM(uCrsBonusResumoB2B.ref)
					replace uCrsBonusResumo.design  WITH ALLTRIM(uCrsBonusResumoB2B.design)
					replace uCrsBonusResumo.bonus WITH ALLTRIM(uCrsBonusResumoB2B.bonus)
					replace uCrsBonusResumo.fornecedor WITH ALLTRIM(uCrsBonusResumoB2B.fornecedor)
					replace uCrsBonusResumo.nrfornecedor WITH uCrsBonusResumoB2B.nrfornecedor
					replace uCrsBonusResumo.ststamp WITH ALLTRIM(uCrsBonusResumoB2B.ststamp)
					replace uCrsBonusResumo.validade WITH uCrsBonusResumoB2B.validade
					replace uCrsBonusResumo.infoconsulta WITH ALLTRIM(uCrsBonusResumoB2B.infoconsulta)
					
					replace uCrsBonusResumo.disp WITH ALLTRIM(uCrsBonusResumoB2B.disp)
					replace uCrsBonusResumo.pct WITH uCrsBonusResumoB2B.pct
					**replace uCrsBonusResumo.dc WITH uCrsBonusResumoB2B.dc
					replace uCrsBonusResumo.pcl WITH uCrsBonusResumoB2B.pcl
					replace uCrsBonusResumo.dtentrega WITH uCrsBonusResumoB2B.dtentrega
                    replace uCrsBonusResumo.pvp WITH uCrsBonusResumoB2B.pvp
				ENDIF 
				
			ENDIF 
			SELECT uCrsBonusResumoB2B
		ENDSCAN 
	ENDIF 
	SELECT uCrsBonusResumo
	GO TOP 
	ENCAUTOMATICAS.Detalhest1.Pageframe1.PAGE9.grdbonusresumo.refresh
	uf_encautomaticas_format_rodapebonus()
	ENCAUTOMATICAS.GridPesq.setfocus

ENDFUNC 

FUNCTION uf_encautomaticas_importXls

	uv_msg = "O ficheiro pode incluir os seguintes campos:"
	uv_msg = uv_msg + chr(13) + CHR(9) + "Ref (Obrigat�rio)"
	uv_msg = uv_msg + chr(13) + CHR(9) + "Qt"
	uv_msg = uv_msg + chr(13) + CHR(9) + "PCusto"
	uv_msg = uv_msg + chr(13) + CHR(9) + "PMax"
	uv_msg = uv_msg + chr(13) + CHR(9) + "PtE"
	uv_msg = uv_msg + chr(13) + CHR(9) + "Forn"
	uv_msg = uv_msg + chr(13) + CHR(9) + "Estab"

	uf_perguntalt_chama(uv_msg ,"OK","",64)


	lcFilePath = uf_gerais_getFile(ALLTRIM(uf_gerais_getUmValor("B_terminal", "pathReport", "terminal = '" + ALLTRIM(myTerm) + "'")), 'xls')

	IF EMPTY(lcFilePath)
		RETURN .F.
	ENDIF

	regua(0,2,"A importar XLS...")

	IF !UF_GERAIS_xls2Cursor(lcFilePath, "", "uc_import", .T.)
		uf_perguntalt_chama("N�o foi poss�vel importar o ficheiro selecionado.","OK","",16)
		regua(2)
		RETURN .f.
	ENDIF




	select uc_import
	IF TYPE("uc_import.ref") == "U"
		regua(2)
		uf_perguntalt_chama("O ficheiro selecionado n�o tem os campos corretos.","OK","",16)
		RETURN .F.
	ENDIF

   SELECT uc_import

	**IF !INLIST(TYPE("uc_import.ref"), "C", "M")
	**	uf_perguntalt_chama("Os dados presentes na coluna Ref n�o est�o definidos como texto." + CHR(13) + "Por favor edite o ficheiro a importar para o formato correto.","OK","",16)
	**	regua(2)
	**	RETURN .F.
	**ENDIF

	LOCAL uv_camposErro
	STORE '' TO uv_camposErro

	IF TYPE("uc_import.qt") <> "N" AND TYPE("uc_import.qt") <> "U"
		uv_camposErro = "Qt"
	ENDIF

	IF TYPE("uc_import.pcusto") <> "N" AND TYPE("uc_import.pcusto") <> "U"
		uv_camposErro = uv_camposErro + IIF(EMPTY(uv_camposErro), '', ',') +  "Pcusto"
	ENDIF

	IF TYPE("uc_import.pmax") <> "N" AND TYPE("uc_import.pmax") <> "U"
		uv_camposErro = uv_camposErro + IIF(EMPTY(uv_camposErro), '', ',') +  "PMax"
	ENDIF

	IF TYPE("uc_import.pte") <> "N" AND TYPE("uc_import.pte") <> "U"
		uv_camposErro = uv_camposErro + IIF(EMPTY(uv_camposErro), '', ',') +  "PtE"
	ENDIF

	IF TYPE("uc_import.forn") <> "N" AND TYPE("uc_import.forn") <> "U"
		uv_camposErro = uv_camposErro + IIF(EMPTY(uv_camposErro), '', ',') +  "Forn"
	ENDIF

	IF TYPE("uc_import.estab") <> "N" AND TYPE("uc_import.estab") <> "U"
		uv_camposErro = uv_camposErro + IIF(EMPTY(uv_camposErro), '', ',') +  "Estab"
	ENDIF

	IF !EMPTY(uv_camposErro)

		LOCAL uv_msgErro

		uv_msgErro = "Os dados presentes " + IIF(AT(',', uv_camposErro) <> 0, 'nas colunas ', 'na coluna ') + ALLTRIM(uv_camposErro) + " n�o est�o definidos como num�rico."

		regua(2)
		uf_perguntalt_chama(ALLTRIM(uv_msgErro) + CHR(13) + "Por favor edite o ficheiro a importar para o formato correto.","OK","",16)
		RETURN .F.
	ENDIF

   LOCAL uv_select
   STORE '' TO uv_select

   uv_select = uv_select + "SELECT "
   uv_select = uv_select + IIF(TYPE("uc_import.ref") == "N", "LEFT(ASTR(ref), 18) as ref, ", "LEFT(ref,18) as ref, ")
   uv_select = uv_select + IIF(TYPE("uc_import.qt") <> "U", "IIF(ISNULL(qt), 0, qt) as qt, ", " 0 as qt, ")
   uv_select = uv_select + IIF(TYPE("uc_import.pcusto") <> "U", "IIF(ISNULL(pcusto), 0, pcusto) as pcusto, ", " 0 as pcusto, ")
   uv_select = uv_select + IIF(TYPE("uc_import.pcusto") <> "U", "IIF(ISNULL(pcusto), .F., .T.) as imp_pcusto, ", " .F. as imp_pcusto, ")
   uv_select = uv_select + IIF(TYPE("uc_import.pmax") <> "U", "IIF(ISNULL(pmax), 0, pmax) as pmax, ", " 0 as pmax, ")
   uv_select = uv_select + IIF(TYPE("uc_import.pmax") <> "U", "IIF(ISNULL(pmax), .F., .T.) as imp_pmax, ", " .F. as imp_pmax, ")
   uv_select = uv_select + IIF(TYPE("uc_import.pte") <> "U", "IIF(ISNULL(pte), 0, pte) as pte, ", " 0 as pte, ")
   uv_select = uv_select + IIF(TYPE("uc_import.pte") <> "U", "IIF(ISNULL(pte), .F., .T.) as imp_pte, ", " .F. as imp_pte, ")
   uv_select = uv_select + IIF(TYPE("uc_import.forn") <> "U", "IIF(ISNULL(forn), 0, forn) as forn, ", " 0 as forn, ")
   uv_select = uv_select + IIF(TYPE("uc_import.estab") <> "U", "IIF(ISNULL(estab), 0, estab) as estab ", " 0 as estab ")
   uv_select = uv_select + "FROM uc_import WITH (BUFFERING = .T.) WHERE !ISNULL(ref) INTO CURSOR uc_importXls"

   &uv_select.

	FECHA("uc_import")
	
    &&Pedir as fichas
    
    LOCAL lcRefTotal, xCount, xCountMax
    lcRefTotal = ""
	xCount = 1
	xCountMax = 200
	
	SELECT uc_importXls
	GO TOP
	SCAN	
		regua(1, RECNO(), "A importar XLS...")
		lbConcatRef  = .f.
		IF uf_gerais_getUmValor("st", "COUNT(*)", "ref = '" + ALLTRIM(uc_importXls.ref) + "' and site_nr = " + ASTR(mySite_nr)) = 0
			IF uf_encautomaticas_criarFichaArtigo(uc_importXls.ref)
				lbConcatRef  = .t.
			ENDIF
		ELSE
			lbConcatRef  = .t.
		ENDIF
		if(lbConcatRef  )
			SELECT uc_importXls
			lcRefTotal =  lcRefTotal  + IIF(EMPTY(lcRefTotal), "", ",") + uc_importXls.ref
		ENDIF
		if(xCount%xCountMax=0)
			uf_gerais_meiaRegua()
			if(!EMPTY(lcRefTotal ))
				uf_encautomaticas_importProd(ALLTRIM(lcRefTotal))		
			ENDIF			
			lcRefTotal = ''			
			regua(2)
		ENDIF
		
		xCount =  xCount + 1	
	ENDSCAN

	if(!EMPTY(lcRefTotal ))
		uf_encautomaticas_importProd(ALLTRIM(lcRefTotal))		
		lcRefTotal = ''			
	ENDIF

	SELECT uc_importXls
	GO TOP

	SCAN


		SELECT crsenc
		GO TOP
		LOCATE FOR  uf_gerais_compStr(crsenc.ref, uc_importXls.ref) 
		
	
		IF uc_importXls.imp_pmax AND uc_importXls.pmax <> crsenc.ostmax
			REPLACE crsenc.stmax WITH uc_importXls.pmax
			myAlterSt = .t.
		ENDIF
		IF uc_importXls.imp_pte AND uc_importXls.pte <> crsenc.optoenc
			REPLACE crsenc.ptoenc WITH uc_importXls.pte
			myAlterSt = .t.
		ENDIF
		REPLACE crsenc.eoq WITH uc_importXls.qt
		IF uc_importXls.imp_pcusto
			REPLACE crsenc.pcusto WITH uc_importXls.pcusto
		ENDIF

		SELECT uc_importXls

		IF !EMPTY(uc_importXls.forn)

			TEXT TO msel TEXTMERGE NOSHOW
				Select nome, nome2 FROM fl(nolock) where no = <<uc_importXls.forn>> AND estab = <<uc_importXls.estab>>
			ENDTEXT

			IF !uf_gerais_actGrelha("", "uc_forn", msel)
				regua(2)
				uf_perguntalt_chama("N�o foi poss�vel importar o fornecedor.","OK","",16)
				RETURN .F.
			ENDIF

			SELECT uc_forn

			REPLACE crsenc.fornecedor 	WITH ALLTRIM(uc_forn.nome)
			REPLACE crsenc.fornec 		WITH uc_importXls.forn
			REPLACE crsenc.fornestab 	WITH uc_importXls.estab
			REPLACE crsenc.fornecabrev 	WITH IIF(EMPTY(uc_forn.nome2), ALLTRIM(uc_forn.nome), ALLTRIM(uc_forn.nome2))
			REPLACE crsenc.enc WITH .T.

			FECHA("uc_forn")

		ENDIF

		SELECT uc_importXls
	ENDSCAN

	SELECT crsenc
	GO TOP

	uf_encautomaticas_ActualizaQtValorEnc()
	ENCAUTOMATICAS.refresh()

	regua(2)

ENDFUNC

FUNCTION uf_encautomaticas_importProd
	LPARAMETERS uv_refTotal



	LOCAL lcIDSedeGrupo
	lcIDSedeGrupo = ""

	IF USED("uCrsConfiguracaoSedeGrupo")
		IF myencGrupo == .t.
			SELECT uCrsConfiguracaoSedeGrupo
			lcIDSedeGrupo = ALLTRIM(uCrsConfiguracaoSedeGrupo.bdname)
		ENDIF
	ENDIF 

	TEXT TO lcSQL NOSHOW TEXTMERGE
		exec up_enc_autorefEncAutomatica <<IIF(MyEncConjunta,0,myArmazem)>>, '<<Alltrim(uv_refTotal)>>','<<ALLTRIM(lcIDSedeGrupo)>>', <<mysite_nr>>
	ENDTEXT 

	IF !uf_gerais_actGrelha("", "uCrsAddProdPea", lcSQL)
		uf_perguntalt_chama("N�o foi poss�vel importar a refer�ncia selecionada.","OK","",16)
		RETURN .F.
	ENDIF

	SELECT uCrsAddProdPea
	GO TOP

	IF RECCOUNT("uCrsAddProdPea") = 0

		RETURN .T.

	ENDIF

	SELECT uCrsAddProdPea
	GO TOP
	SCAN

		Select crsenc
		Append Blank
		replace crsenc.ststamp		With 	Alltrim(uCrsAddProdPea.ststamp)
		replace crsenc.u_fonte		With 	Alltrim(uCrsAddProdPea.u_fonte)
		replace crsenc.ref			With 	Alltrim(uCrsAddProdPea.ref)
		replace crsenc.refb			With 	Alltrim(uCrsAddProdPea.refb)
		replace crsenc.codigo		With 	Alltrim(uCrsAddProdPea.codigo)
		replace crsenc.design		With 	Alltrim(uCrsAddProdPea.design)
		replace crsenc.stock		With 	uCrsAddProdPea.stock
		replace crsenc.stmin		With 	uCrsAddProdPea.stmin
		replace crsenc.stmax		With 	uCrsAddProdPea.stmax
		replace crsenc.ostmax		With 	uCrsAddProdPea.stmax
		replace crsenc.ptoenc		With 	uCrsAddProdPea.ptoenc
		replace crsenc.optoenc		With 	uCrsAddProdPea.ptoenc
		replace crsenc.eoq			With 	uCrsAddProdPea.eoq
		replace crsenc.qtbonus		With 	uCrsAddProdPea.qtbonus
		replace crsenc.qttadic		With 	uCrsAddProdPea.qttadic
		replace crsenc.qttfor		With 	uCrsAddProdPea.qttfor
		replace crsenc.qttacin		With 	uCrsAddProdPea.qttacin
		replace crsenc.qttcli		With 	uCrsAddProdPea.qttcli
		replace crsenc.qttres		With 	uCrsAddProdPea.qttcli
		replace crsenc.fornecedor	With 	Alltrim(uCrsAddProdPea.fornecedor)
		replace crsenc.fornec		With 	uCrsAddProdPea.fornec
		replace crsenc.fornestab	With 	uCrsAddProdPea.fornestab
		replace crsenc.epcusto		with 	uCrsAddProdPea.epcusto
		replace crsenc.epcpond			With 	uCrsAddProdPea.epcpond
		replace crsenc.epcult			With 	uCrsAddProdPea.epcult
		replace crsenc.tabiva			With 	uCrsAddProdPea.tabiva
		replace crsenc.iva				With 	uCrsAddProdPea.iva
		replace crsenc.cpoc				With 	uCrsAddProdPea.cpoc
		replace crsenc.familia			With 	uCrsAddProdPea.familia
		replace crsenc.faminome			With 	alltrim(uCrsAddProdPea.faminome)
		replace crsenc.u_lab			With 	Alltrim(uCrsAddProdPea.u_lab)
		replace crsenc.conversao		With 	uCrsAddProdPea.conversao
		replace crsenc.stockgh			With 	uCrsAddProdPea.stockgh
		replace crsenc.bonusfornec		With 	Alltrim(uCrsAddProdPea.bonusfornec)
		Replace crsenc.pclfornec		WITH 	uCrsAddProdPea.pclfornec
		replace crsenc.esgotado			With 	Alltrim(uCrsAddProdPea.esgotado)
		Replace crsenc.alertaPCL		WITH 	uCrsAddProdPea.alertaPCL
		Replace crsenc.alertaBonus		WITH 	uCrsAddProdPea.alertaBonus
		Replace crsenc.alertaStockGH	WITH 	uCrsAddProdPea.alertaStockGH
		replace crsenc.enc				WITH  	.f.
		replace crsenc.sel				WITH 	.f.
		REPLACE crsenc.pvp 				WITH	uCrsAddProdPea.pvpFicha
		TRY
			replace crsenc.marca		WITH uCrsAddProdPea.marca
		CATCH
		ENDTRY
		IF TYPE("crsenc.pcusto") <> "U"
			REPLACE crsenc.pcusto WITH uCrsAddProdPea.pclFicha
		ENDIF

		SELECT uCrsAddProdPea
	ENDSCAN

	FECHA("uCrsAddProdPea")

ENDFUNC

FUNCTION uf_encautomaticas_ValidaQttEmb
LPARAMETERS lcValue, lcSiteNr, lcRef, lcTpArredondamento
	lcSQL = ""
	
	TEXT TO lcSQL TEXTMERGE NOSHOW
		select * from uf_calcular_qtt_encomendar_emb ('<<lcValue>>', '<<lcRef>>', '<<lcSiteNr>>', '<<lcTpArredondamento>>')
	ENDTEXT

	If !uf_gerais_actGrelha("", "qttEncomendar", lcSql)
		uf_perguntalt_chama("N�O FOI POSS�VEL VERIFICAR AS DEFINI��ES DE DOCUMENTOS DISPON�VEIS. POR FAVOR CONTACTE O SUPORTE","OK","",16)
		RETURN -1
	ENDIF
	
	RETURN qttEncomendar.qtt_encomendar
	
ENDFUNC 




FUNCTION uf_encautomaticas_criarFichaArtigo
	LPARAMETERS uv_ref
	IF !(type("STOCKS")=="U") AND !(STOCKS.caption=="Stocks e Servi�os")
		uf_perguntalt_chama("N�O � POSS�VEL REALIZAR ESTA AC��O PORQUE O ECR� GEST�O DE STOCKS EST� EM MODO DE EDI��O. POR FAVOR TERMINE ESTA OPERA��O E TENTE NOVAMENTE","OK","",48)
		RETURN .F.
	ELSE
		TEXT TO lcSQL NOSHOW TEXTMERGE
			exec up_stocks_criaSt '<<uv_ref>>', <<mysite_nr>>
		ENDTEXT
		IF uf_gerais_actGrelha("", "", lcSQL)
			RETURN .T.
		ELSE
						
			IF uf_perguntalt_chama("A refer�ncia n�o existe. Quer introduzir uma nova refer�ncia?","Sim","N�o")
						
				uf_stocks_chama('')
				uf_stocks_novo()

				select ST
				Replace st.REF With Alltrim(uv_ref)

				STOCKS.refresh
				STOCKS.hide
				STOCKS.show(1)
				
			ELSE

				RETURN .F.

			ENDIF
		ENDIF
	ENDIF

	RETURN .T.

ENDFUNC