**
FUNCTION uf_consolidacaocompras_chama

	IF !USED("ucrsConsolidacaoEmpresa")
		SELECT .f. as sel ,no as site_nr, site FROM ucrsLojas WHERE !EMPTY(ALLTRIM(ucrsLojas.site)) INTO CURSOR ucrsConsolidacaoEmpresa READWRITE
		SELECT ucrsConsolidacaoEmpresa 
		GO Top
	ENDIF
	
	IF !USED("ucrsConsolidacaoMes")
		
		CREATE CURSOR ucrsConsolidacaoMesAux(sel l, mes c(15), mesno n(2), ano n(4))
*!*			lcData = GOMONTH(DATE(), -14)
*!*			DO WHILE lcData < DATE()
*!*				lcData = GOMONTH(lcData , +1)
*!*				SELECT ucrsConsolidacaoMesAux
*!*				APPEND BLANK 
*!*				Replace ucrsConsolidacaoMesAux.mes WITH uf_Gerais_CalculaMes(MONTH(lcData)) + " " + ASTR(YEAR(lcData))
*!*				Replace ucrsConsolidacaoMesAux.mesno WITH MONTH(lcData)
*!*				Replace ucrsConsolidacaoMesAux.ano WITH YEAR(lcData)
*!*			ENDDO
		
		FOR i = 0 TO 13
			IF(i >12)
				uv_data = GOMONTH(GOMONTH(DATE(), -12),(i - 12)*-1)
			ELSE
				uv_data = GOMONTH(DATE(), IIF(I=0,0,I*-1))
			ENDIF
			SELECT ucrsConsolidacaoMesAux
			APPEND BLANK 
			Replace ucrsConsolidacaoMesAux.mes WITH  uf_Gerais_CalculaMes(MONTH(uv_data))+ " " + ASTR(YEAR(uv_data))
			Replace ucrsConsolidacaoMesAux.mesno WITH MONTH(uv_data)
			Replace ucrsConsolidacaoMesAux.ano WITH YEAR(uv_data)
		NEXT
		
		SELECT * FROM ucrsConsolidacaoMesAux  INTO CURSOR ucrsConsolidacaoMes READWRITE
		SELECT ucrsConsolidacaoMes 
		GO Top		
	ENDIF
	IF !USED("ucrsConsolidacaoSt")
		CREATE CURSOR ucrsConsolidacaoSt(ref c(254), design c(100), lab c(150), validado l, pesquisa l)
	ENDIF 
	
	** Calcula no inicio para uso posterior
	IF !USED("ucrsConsolidacaoListaSt")
		lcSQL = ""
		TEXT To lcSQL NOSHOW TEXTMERGE
			select distinct ref,design,u_lab from st (nolock)
		ENDTEXT
		
		If !uf_gerais_actGrelha("", "ucrsConsolidacaoListaSt", lcSql)
			uf_perguntalt_chama("N�O FOI POSS�VEL ENCONTRAR ARTIGOS.","OK","",16)
			RETURN .f.
		ENDIF 
				
	ENDIF 
	
	IF TYPE("consolidacaocompras")=="U"
		DO FORM consolidacaocompras
		consolidacaocompras.show
	ELSE
		consolidacaocompras.show
	ENDIF
ENDFUNC 



**
FUNCTION uf_consolidacaocompras_eliminar
		
	SELECT ucrsConsolidacaoSt
	DELETE 
	TRY
		SELECT ucrsConsolidacaoSt
		SKIP - 1
	CATCH
		SELECT ucrsConsolidacaoSt
		GO Bottom
	ENDTRY
	
	consolidacaocompras.PageFrame1.Page1.GridArtigos.refresh
ENDFUNC 


**
FUNCTION uf_consolidacaocompras_pesquisaST

	uf_pesqstocks_chama('CONSOLIDACAOCOMPRAS')

ENDFUNC 

**
FUNCTION uf_consolidacaocompras_paste

	LOCAL lcPaste
	lcPaste = _cliptext

	IF EMPTY(ALLTRIM(lcPaste))
		uf_perguntalt_chama("N�o existe nada em mem�ria. Verifique se copiou as refer�ncias.","OK","",16)
		RETURN .f.
	ENDIF 
		
	ALINES(aMyarray,lcPaste,CHR(13))
	For i=1 to ALEN(aMyarray)
		
		
		lcRef = aMyarray(i)
		
		SELECT ucrsConsolidacaoSt
		GO Top
		LOCATE FOR ALLTRIM(ucrsConsolidacaoSt.ref) == ALLTRIM(lcRef) && N�o permite repetidos
		IF !FOUND()
			
			SELECT ucrsConsolidacaoSt
			APPEND BLANK
			Replace ucrsConsolidacaoSt.ref WITH ALLTRIM(lcRef)

			SELECT ucrsConsolidacaoListaSt
			LOCATE FOR ALLTRIM(ucrsConsolidacaoListaSt.ref) == ALLTRIM(lcRef)
			IF FOUND()	
				Replace ucrsConsolidacaoSt.design WITH ucrsConsolidacaoListaSt.design
				Replace ucrsConsolidacaoSt.lab WITH ucrsConsolidacaoListaSt.u_lab
				Replace ucrsConsolidacaoSt.validado WITH .t.
				
			ENDIF 	
		ENDIF 
	ENDFOR 
	
	
	consolidacaocompras.PageFrame1.Page1.GridArtigos.refresh
ENDFUNC 


**
FUNCTION uf_consolidacaocompras_limpar
	Delete ucrsConsolidacaoSt From ucrsConsolidacaoSt where ucrsConsolidacaoSt.validado== .f.	
	SELECT ucrsConsolidacaoSt 
	GO Top
	
	consolidacaocompras.PageFrame1.Page1.GridArtigos.refresh
	
ENDFUNC 

**
FUNCTION uf_consolidacaocompras_botaolimparMenu
	Delete ucrsConsolidacaoSt From ucrsConsolidacaoSt
	UPDATE ucrsConsolidacaoMes SET ucrsConsolidacaoMes.Sel = .f.
	UPDATE ucrsConsolidacaoEmpresa SET ucrsConsolidacaoEmpresa.sel = .f.		
	consolidacaocompras.PageFrame1.Page1.GridEmpresa.refresh	
	consolidacaocompras.PageFrame1.Page1.GridMes.refresh	
	consolidacaocompras.PageFrame1.Page1.GridArtigos.refresh	
ENDFUNC 

**
FUNCTION uf_consolidacaocompras_Gravar
	LOCAL lcTotalStock, lcTotalVd, lcTotalExc, lcTotalNec, lcRef, lcSQLExec
	STORE 0 TO lcTotalStock, lcTotalVd, lcTotalExc, lcTotalNec	
	STORE '' TO lcRef, lcSQLExec
	
	consolidacaocompras.menu1.estado("xls", "SHOW", "Executar", .f., "Voltar", .t.)
	consolidacaocompras.menu1.estado("pesquisarST,eliminar,pastST,limpar", "HIDE")		
	consolidacaocompras.PageFrame1.ActivePage = 2
	
	** Cria registos nas Tabelas Temporarias Empresas
	TEXT TO lcSQL NOSHOW TEXTMERGE
		If OBJECT_ID('tempdb.dbo.#temp_consolidacaoEmpresas') IS NOT NULL
			DROP TABLE #temp_consolidacaoEmpresas
		If OBJECT_ID('tempdb.dbo.#temp_consolidacaoMeses') IS NOT NULL
			DROP TABLE #temp_consolidacaoMeses
		If OBJECT_ID('tempdb.dbo.#temp_consolidacaoSt') IS NOT NULL
			DROP TABLE #temp_consolidacaoSt
		create table #temp_consolidacaoEmpresas (site_nr tinyint, site varchar(20))
		create table #temp_consolidacaoMeses (ano int, mes int)
		create table #temp_consolidacaoSt (ref varchar(18))
	ENDTEXT 
	lcSQLExec = lcSQLExec + chr(13) + lcSql
*!*		If !uf_gerais_actGrelha("", "", lcSql)
*!*			uf_perguntalt_chama("Ocorreu um erro ao registar dados para c�lculo.","OK","",16)
*!*			RETURN .f.
*!*		ENDIF 
	
	SELECT ucrsConsolidacaoEmpresa
	Go Top 
	SCAN FOR !EMPTY(ucrsConsolidacaoEmpresa.sel)

		TEXT TO lcSQL NOSHOW TEXTMERGE
			INSERT INTO #temp_consolidacaoEmpresas(site_nr,site)Values (<<ucrsConsolidacaoEmpresa.site_nr>>,'<<ALLTRIM(ucrsConsolidacaoEmpresa.site)>>')
		ENDTEXT 
		lcSQLExec = lcSQLExec + chr(13) + lcSql
*!*			If !uf_gerais_actGrelha("", "", lcSql)
*!*				uf_perguntalt_chama("Ocorreu um erro ao registar dados para c�lculo.","OK","",16)
*!*				RETURN .f.
*!*			ENDIF 
		
	ENDSCAN
	**
	
	SELECT ucrsConsolidacaoMes
	Go Top 
	SCAN FOR !EMPTY(ucrsConsolidacaoMes.sel)

		TEXT TO lcSQL NOSHOW TEXTMERGE
			INSERT INTO #temp_consolidacaoMeses (ano,mes)Values(<<ucrsConsolidacaoMes.ano>>,<<ucrsConsolidacaoMes.mesno>>)
		ENDTEXT 
		lcSQLExec = lcSQLExec + chr(13) + lcSql
*!*			If !uf_gerais_actGrelha("", "", lcSql)
*!*				uf_perguntalt_chama("Ocorreu um erro ao registar dados para c�lculo.","OK","",16)
*!*				RETURN .f.
*!*			ENDIF 
	ENDSCAN
	**

	SELECT ucrsConsolidacaoSt
	Go Top 
	SCAN FOR !EMPTY(ucrsConsolidacaoSt.validado)

		TEXT TO lcSQL NOSHOW TEXTMERGE
			INSERT INTO #temp_consolidacaoSt(ref)Values ('<<ALLTRIM(ucrsConsolidacaoSt.ref)>>')
		ENDTEXT 
		lcSQLExec = lcSQLExec + chr(13) + lcSql
*!*			If !uf_gerais_actGrelha("", "", lcSql)
*!*				uf_perguntalt_chama("Ocorreu um erro ao registar dados para c�lculo.","OK","",16)
*!*				RETURN .f.
*!*			ENDIF 
		
	ENDSCAN
	**
	
	** Excuta SP 
	TEXT TO lcSQL NOSHOW TEXTMERGE
		<<lcSQLExec>>
		exec up_encomendas_consolidacaoCompras 'temp_consolidacaoEmpresas','temp_consolidacaoMeses','temp_consolidacaoSt'
	ENDTEXT 

	If !uf_gerais_actGrelha("", "ucrsDadosConsolidados", lcSql)
		uf_perguntalt_chama("Ocorreu um erro ao registar dados para c�lculo.","OK","",16)
		RETURN .f.
	ENDIF 

	SELECT ucrsDadosConsolidados
	GO top 
	
	**
	consolidacaocompras.PageFrame1.Page2.GridConsolidacao.recordSource = ""
	WITH consolidacaocompras.PageFrame1.Page2.GridConsolidacao
		FOR j=1 TO .columnCount
			.DeleteColumn
		ENDFOR
	ENDWITH 
	
	** Primeria Coluna (Disponibilidades Recursos)
	WITH consolidacaocompras.PageFrame1.Page2.GridConsolidacao
		
		.recordSource = "ucrsDadosConsolidados"
		
		** Precorre todas as colunas do Cursor	
		SELECT ucrsDadosConsolidados
		gnFieldcount = AFIELDS(gaMyArray) 
		FOR nCount = 1 TO gnFieldcount 
		
			
			i = nCount
			lcNomeCampo = proper(gaMyArray(nCount,1))
			lcControlSource = "ucrsDadosConsolidados." + ALLTRIM(lcNomeCampo)

			.AddColumn 
			.RowHeight = uf_gerais_getRowHeight()
			.columns(i).ControlSource = lcControlSource 
			.columns(i).text1.ControlSource = lcControlSource 
			.columns(i).header1.FontName = "Verdana"
			.columns(i).header1.Fontsize = 9
			.columns(i).FontName = "Verdana"
			.columns(i).Fontsize = 9
			.columns(i).ReadOnly = .t.
			.columns(i).text1.ReadOnly = .t.
			.Columns(i).columnorder = i
								
			.Columns(i).width = 50
			
			.Columns(i).InputMask=""
			.Columns(i).text1.InputMask=""
			.columns(i).header1.caption = lcNomeCampo 
			.columns(i).header1.Alignment = 2
			&&.columns(i).header1.WordWrap = .t.
			IF LEFT(ALLTRIM(UPPER(lcNomeCampo)),4) =="DIST"
				.columns(i).header1.FontBold = .t.
			ELSE
				.columns(i).header1.FontBold = .f.
			ENDIF 
			
			.columns(i).text1.BorderStyle =  0
			.columns(i).text1.ForeColor = RGB(0,0,0)
			.columns(i).text1.SelectedForeColor = RGB(0,0,0)
			.columns(i).text1.FontBold = .f.
			
			IF ALLTRIM(UPPER(lcNomeCampo)) == "REF" OR ALLTRIM(UPPER(lcNomeCampo)) == "DESIGN"
				.Columns(i).Format="RTK"
				.Columns(i).text1.Format="RTK"
			ELSE
				.Columns(i).Format="9999999999999"
				.Columns(i).inputMask="9999999999999"
				.Columns(i).text1.Format="9999999999999"
				.Columns(i).text1.inputMask="9999999999999"
			ENDIF 	
			
			IF ALLTRIM(UPPER(lcNomeCampo)) = "PESQUISA"
				.Columns(i).visible =  .f.
			ENDIF 
		ENDFOR
		.autofit()
	ENDWITH
	
	WITH consolidacaocompras.PageFrame1.Page2.GridConsolidacao
		LOCAL lcj, lck
		lcj = 0
		lck = 2
		FOR i=1 TO .columnCount
						
			IF ALLTRIM(UPPER(.columns(i).header1.caption)) == "PESQUISA" OR ALLTRIM(UPPER(.columns(i).header1.caption)) == "REF" OR ALLTRIM(UPPER(.columns(i).header1.caption)) == "DESIGN"
				.Columns(i).dynamicBackColor="IIF(recno()%2!=0,Rgb[255,255,255],rgb[240,240,240])"
			ELSE
				
				lcj = lcj +1

				IF lcj = 6
					IF lck == 2
						lck	= 1
					ELSE
						lck	= 2
					ENDIF 
					lcj = 1
				ENDIF 

				IF LEFT(ALLTRIM(UPPER(.columns(i).header1.caption)),4) == "DIST"
					.Columns(i).dynamicBackColor="IIF(recno()%2!=0,Rgb[255,255,255],Rgb[191,223,223])"	
				ELSE
					IF lck %2!=0
						.Columns(i).dynamicBackColor="IIF(recno()%2!=0,Rgb[255,255,255],rgb[240,240,240])"
					ELSE
						.Columns(i).dynamicBackColor="IIF(recno()%2!=0,Rgb[255,255,255],rgb[210,210,210])"
					ENDIF 
			
				ENDIF
				 
			ENDIF 
			
			.Columns(i).DynamicFontBold = "ucrsDadosConsolidados.PESQUISA"
			
		ENDFOR
		
		.RowHeight 	= uf_gerais_getRowHeight()
		.HeaderHeight 	= uf_gerais_getHeaderHeight()
	ENDWITH 
	
	
	LOCAL lcTotalStock 
	
	** Actualiza Total Por linha 
	SELECT ucrsDadosConsolidados
	GO Top
	SCAN
	

		lcTotalStock = 0
		lcTotalVd = 0
		lcTotalExc = 0
		lcTotalNec = 0
		
		
		SELECT ucrsDadosConsolidados
		gnFieldcount = AFIELDS(gaMyArray) 
		FOR nCount = 1 TO gnFieldcount 
		
			i = nCount
			lcNomeCampo = proper(gaMyArray(nCount,1))
			lcControlSource = "ucrsDadosConsolidados." + ALLTRIM(lcNomeCampo)


			IF LEFT(UPPER(ALLTRIM(lcNomeCampo)),3) == "STK"
				SELECT ucrsDadosConsolidados
				lcTotalStock = lcTotalStock + &lcControlSource
			ENDIF 	
		
			IF LEFT(UPPER(ALLTRIM(lcNomeCampo)),2) == "VD"
				SELECT ucrsDadosConsolidados
				lcTotalVd = lcTotalVd + &lcControlSource
			ENDIF 	
		
			IF LEFT(UPPER(ALLTRIM(lcNomeCampo)),3) == "EXC"
				SELECT ucrsDadosConsolidados
				lcTotalExc = lcTotalExc + &lcControlSource
			ENDIF 	
			
			IF LEFT(UPPER(ALLTRIM(lcNomeCampo)),3) == "NEC"
				SELECT ucrsDadosConsolidados
				lcTotalNec = lcTotalNec + &lcControlSource
			ENDIF 	
		ENDFOR  
	
		Replace ucrsDadosConsolidados.stk_total WITH lcTotalStock		
		Replace ucrsDadosConsolidados.vd_total WITH lcTotalVd		
		Replace ucrsDadosConsolidados.exc_total WITH lcTotalExc		
		Replace ucrsDadosConsolidados.nec_total WITH lcTotalNec	
			
	ENDSCAN 
	

	** Calcula distribui��o 

	IF USED("ucrsDadosConsolidadosOrdenacaoNec")
		fecha("ucrsDadosConsolidadosOrdenacaoNec")
	ENDIF
	CREATE CURSOR ucrsDadosConsolidadosOrdenacaoNec(ref c(18), empresa c(254), vd n(9,0), nec n(9,0), dist n(9,0))
	SELECT ucrsDadosConsolidados
	GO Top
	SCAN
	
		lcRef = ucrsDadosConsolidados.ref

		SELECT ucrsDadosConsolidadosOrdenacaoNec
		GO Top
		SCAN
			DELETE 
		ENDSCAN 
		
		** Acrescenta VD
		SELECT ucrsDadosConsolidados
		gnFieldcount = AFIELDS(gaMyArray) 
		FOR nCount = 1 TO gnFieldcount 
		
			i = nCount
			lcNomeCampo = proper(gaMyArray(nCount,1))
			lcControlSource = "ucrsDadosConsolidados." + ALLTRIM(lcNomeCampo)

			IF LEFT(UPPER(ALLTRIM(lcNomeCampo)),2) == "VD" AND UPPER(ALLTRIM(lcNomeCampo)) != "VD_TOTAL"
				SELECT ucrsDadosConsolidadosOrdenacaoNec
				APPEND BLANK
				Replace ucrsDadosConsolidadosOrdenacaoNec.ref WITH ALLTRIM(lcRef)
				Replace ucrsDadosConsolidadosOrdenacaoNec.empresa WITH ALLTRIM(lcNomeCampo)

				IF LEFT(UPPER(ALLTRIM(lcNomeCampo)),2) == "VD"
					Replace ucrsDadosConsolidadosOrdenacaoNec.vd WITH &lcControlSource
				ENDIF 
			ENDIF 	
		
		ENDFOR  
		
		
		** Nec	
		SELECT ucrsDadosConsolidadosOrdenacaoNec
		GO Top
		SCAN 
			SELECT ucrsDadosConsolidados
			gnFieldcount = AFIELDS(gaMyArray) 
			FOR nCount = 1 TO gnFieldcount 
			
				i = nCount
				lcNomeCampo = proper(gaMyArray(nCount,1))
				lcControlSource = "ucrsDadosConsolidados." + ALLTRIM(lcNomeCampo)

				IF LEFT(UPPER(ALLTRIM(lcNomeCampo)),3) == "NEC" AND UPPER(ALLTRIM(lcNomeCampo)) != "NEC_TOTAL" 
					IF ALLTRIM(UPPER(STRTRAN(ucrsDadosConsolidadosOrdenacaoNec.empresa,"Vd_","") )) == UPPER(ALLTRIM(STRTRAN(lcNomeCampo,"Nec_","")))
						lcNec = &lcNomeCampo
						SELECT ucrsDadosConsolidadosOrdenacaoNec
						Replace ucrsDadosConsolidadosOrdenacaoNec.nec WITH lcNec 
					ENDIF
				ENDIF 	
			ENDFOR 
		 
		ENDSCAN 
		
				
		IF USED("ucrsDadosConsolidadosOrdenacaoNecOrdenado")
			fecha("ucrsDadosConsolidadosOrdenacaoNecOrdenado")
		ENDIF 
		
		SELECT * FROM ucrsDadosConsolidadosOrdenacaoNec ORDER BY vd DESC INTO CURSOR ucrsDadosConsolidadosOrdenacaoNecOrdenado READWRITE 
		
		lcStkTotal = ucrsDadosConsolidados.stk_total

		SELECT ucrsDadosConsolidadosOrdenacaoNecOrdenado 
		GO TOP 
		SCAN
			FOR i=1 TO ucrsDadosConsolidadosOrdenacaoNecOrdenado.nec - ucrsDadosConsolidadosOrdenacaoNecOrdenado.dist
				IF lcStkTotal > 0
					SELECT ucrsDadosConsolidadosOrdenacaoNecOrdenado 
					REPLACE ucrsDadosConsolidadosOrdenacaoNecOrdenado.dist WITH ucrsDadosConsolidadosOrdenacaoNecOrdenado.dist + 1
					lcStkTotal = lcStkTotal - 1
				ENDIF 
			ENDFOR 
		ENDSCAN 
		
		
		SELECT ucrsDadosConsolidadosOrdenacaoNecOrdenado
		GO TOP 
		SCAN FOR ucrsDadosConsolidadosOrdenacaoNecOrdenado.dist > 0
			
			SELECT ucrsDadosConsolidados
			gnFieldcount = AFIELDS(gaMyArray) 
			FOR nCount = 1 TO gnFieldcount 
			
				i = nCount
				lcNomeCampo = proper(gaMyArray(nCount,1))
				lcControlSource = "ucrsDadosConsolidados." + ALLTRIM(lcNomeCampo)

				IF LEFT(UPPER(ALLTRIM(lcNomeCampo)),4) == "DIST" AND UPPER(ALLTRIM(lcNomeCampo)) != "DIST_TOTAL" 
					IF ALLTRIM(UPPER(STRTRAN(ucrsDadosConsolidadosOrdenacaoNecOrdenado.empresa,"Vd_","") )) == UPPER(ALLTRIM(STRTRAN(lcNomeCampo,"Dist_","")))
						SELECT ucrsDadosConsolidados
						Replace &lcNomeCampo WITH ucrsDadosConsolidadosOrdenacaoNecOrdenado.dist
					ENDIF
				ENDIF 	
			ENDFOR  
			
		ENDSCAN 
		 
	ENDSCAN 


	** Actualiza Dist Total 
	SELECT ucrsDadosConsolidados
	GO Top
	SCAN

		lcTotalDist = 0

		SELECT ucrsDadosConsolidados
		gnFieldcount = AFIELDS(gaMyArray) 
		FOR nCount = 1 TO gnFieldcount 
		
			i = nCount
			lcNomeCampo = proper(gaMyArray(nCount,1))
			lcControlSource = "ucrsDadosConsolidados." + ALLTRIM(lcNomeCampo)


			IF LEFT(UPPER(ALLTRIM(lcNomeCampo)),4) == "DIST"
				SELECT ucrsDadosConsolidados
				lcTotalDist = lcTotalDist + &lcControlSource
			ENDIF 	
			
		ENDFOR  
	
		Replace ucrsDadosConsolidados.dist_total WITH lcTotalDist		
	ENDSCAN 
	


	SELECT ucrsDadosConsolidados
	APPEND BLANK
	SELECT ucrsDadosConsolidados
	APPEND BLANK
	Replace ucrsDadosConsolidados.design WITH "Totais"
	
	** Precorre todas as colunas do Cursor	- Actualiza Total Por Coluna
	SELECT ucrsDadosConsolidados
	gnFieldcount = AFIELDS(gaMyArray) 
	FOR nCount = 1 TO gnFieldcount 
	
		i = nCount
		lcNomeCampo = proper(gaMyArray(nCount,1))
		&&lcControlSource = "ucrsDadosConsolidados." + ALLTRIM(lcNomeCampo)
		lcSomatorio = 0
		
		IF UPPER(ALLTRIM(lcNomeCampo)) != "PESQUISA" AND UPPER(ALLTRIM(lcNomeCampo)) != "REF" AND UPPER(ALLTRIM(lcNomeCampo)) != "DESIGN"
			SELECT ucrsDadosConsolidados
			CALCULATE SUM(&lcNomeCampo) TO lcSomatorio

			UPDATE ucrsDadosConsolidados SET &lcNomeCampo = lcSomatorio WHERE design = "Totais"

		ENDIF 	
	ENDFOR  
	
	
	consolidacaocompras.PageFrame1.Page2.GridConsolidacao.refresh
ENDFUNC 


**
FUNCTION uf_consolidacaocompras_ExportXLS
	Local mdir,lcCampos	
	**oShell = CREATEOBJECT("Shell.Application")
	**oFolder = oShell.Application.BrowseForFolder(_screen.HWnd,"Select Folder", 1)
	**IF isnull(oFolder)
	**	RETURN .f.
	**ENDIF
	**
	**mdir = oFolder.self.path

    **uv_path = ALLTRIM(uf_gerais_getUmValor("B_Terminal", "LTRIM(RTRIM(pathExport))", "terminal = '" + alltrim(myTerm) + "' and site = '" + alltrim(mySite) + "'"))
    uv_path = ALLTRIM(uf_gerais_getUmValor("B_Terminal", "LTRIM(RTRIM(pathExport))", "terminal = '" + alltrim(myTerm) + "'"))

    IF EMPTY(uv_path)

		uf_perguntalt_chama('Aten��o!' + chr(13) + 'N�o preencheu o par�metro "Caminho Export.".' + chr(13) +  'Ter� de preencher o mesmo para continuar.', "Ok", "", 48)
		RETURN .f.

    ENDIF

    IF !uf_gerais_createDir(uv_path)
        RETURN .F.
    ENDIF

    mdir = uv_path
	
	IF !EMPTY(mdir)
		lcFile = ALLTRIM(mdir) + "\LogitoolsConsolidacaoCompras.xls"

		IF USED("ucrsDadosConsolidadosXls")
			fecha("ucrsDadosConsolidadosXls ")
		ENDIF 

		SELECT ucrsDadosConsolidados
		gnFieldcount = AFIELDS(gaMyArray) 
		FOR nCount = 1 TO gnFieldcount 
			
			** Comentado para mostrar a ref do produto a pedido de PMC - Lu�s Leal 20-07-2015
			** IF nCount != 1
				IF EMPTY(lcCampos)
					lcCampos = proper(gaMyArray(nCount,1))
				ELSE
					lcCampos = lcCampos + "," + proper(gaMyArray(nCount,1))
				ENDIF 
			** ENDIF 			
		ENDFOR 

		SELECT &lcCampos FROM ucrsDadosConsolidados INTO CURSOR ucrsDadosConsolidadosXls READWRITE 

		SELECT ucrsDadosConsolidadosXls 
		GO TOP 	
		SELECT ucrsDadosConsolidadosXls 
		COPY TO &lcFile TYPE XL5
	ENDIF 
ENDFUNC 



**
FUNCTION uf_consolidacaocompras_sair
	**	
	IF consolidacaocompras.PageFrame1.ActivePage == 2
		
		consolidacaocompras.menu1.estado("pesquisarST,eliminar,pastST,limpar", "SHOW", "Executar", .t., "Sair", .t.)
		consolidacaocompras.menu1.estado("xls", "HIDE")
		consolidacaocompras.PageFrame1.ActivePage = 1
	ELSE
		**
		IF USED("ucrsConsolidacaoEmpresa")
			fecha("ucrsConsolidacaoEmpresa")
		ENDIF 
		
		IF USED("ucrsConsolidacaoMes")
			fecha("ucrsConsolidacaoMes")
		ENDIF
		
		IF USED("ucrsConsolidacaoSt")
			fecha("ucrsConsolidacaoSt")
		ENDIF

		IF USED("ucrsConsolidacaoListaSt")
			fecha("ucrsConsolidacaoListaSt")
		ENDIF 

		consolidacaocompras.hide
		consolidacaocompras.release
	ENDIF 
ENDFUNC

