**
FUNCTION uf_importSellOut_chama

	Public myIMPORTSELLOUTIntroducao, myIMPORTSELLOUTAlteracao

	IF !USED("ucrsCondicoesClientes")
		TEXT TO lcSQL TEXTMERGE NOSHOW
			Select sel = convert(bit,0), nome,bdname,odbc, aplicado = convert(bit,0), sede, no, estab from condComercConfig
		ENDTEXT 
		IF !uf_gerais_actgrelha("", "ucrsCondicoesClientes", lcSql)
			MESSAGEBOX("N�o foi possivel verificar os clientes para aplicar as defini��es. Contacte o suporte.",16,"LOGITOOLS SOFTWARE")
			RETURN .f.
		ENDIF
	ENDIF
	
	IF TYPE("IMPORTSELLOUT") == "U"
		DO FORM IMPORTSELLOUT
	ELSE
		IMPORTSELLOUT.show()
	ENDIF
	
	uf_importSellOut_actualizaClientes()
	
ENDFUNC


**
FUNCTION uf_importSellOut_actualizaClientes

	Select CabDoc						
	TEXT TO lcSQL TEXTMERGE NOSHOW
		Select 
			sel = convert(bit,0)
			,nome
			,bdname
			,odbc
			,aplicado = convert(bit,0)
			,sede
			,no
			,estab
		from 
			condComercConfig
	ENDTEXT 
	IF !uf_gerais_actgrelha("importSellOut.GridPesq", "ucrsCondicoesClientes", lcSql)
		MESSAGEBOX("N�o foi possivel verificar os clientes para aplicar as defini��es. Contacte o suporte.",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF
	
	importSellOut.GridPesq.refresh
ENDFUNC



**
FUNCTION uf_importSellOut_init
	
	&& Configura menu principal
	WITH importSellOut.menu1
		.adicionaOpcao("importar","Importar",myPath + "\imagens\icons\doc_seta_w.png","uf_importSellOut_ImportarSellOut", "A")
		.adicionaOpcao("seltodas","Sel.Todos",myPath + "\imagens\icons\unchecked_w.png","uf_importSellOut_inverteSelecao","T")
		.adicionaOpcao("editar","Editar",myPath + "\imagens\icons\lapis_w.png","uf_importSellOut_editar","E")
	ENDWITH

	uf_importSellOut_alternaMenu()
ENDFUNC


**
FUNCTION uf_importSellOut_alternaMenu
	
	WITH importSellOut.menu1	
		IF myimportSellOutIntroducao == .t. OR myimportSellOutAlteracao == .t.
			.estado("", "SHOW", "Gravar", .t., "Cancelar", .t.)
			.estado("editar,importar", "HIDE")
			
			WITH importSellOut.GridPesq
				For i=1 To .columncount
					IF .Columns(i).name != "sel" AND .Columns(i).name != "aplicado"
						.Columns(i).readonly = .f.
					ENDIF
				ENDFOR
			ENDWITH
		ELSE 
			.estado("importar, editar", "SHOW", "Gravar", .f., "Sair", .t.)
			.estado("", "HIDE")
			
			WITH importSellOut.GridPesq
				For i=1 To .columncount
					IF .Columns(i).name != "sel" AND .Columns(i).name != "aplicado"
						.Columns(i).readonly = .t.
					ENDIF
				ENDFOR
			ENDWITH
		ENDIF 
	ENDWITH

		
	importSellOut.refresh
ENDFUNC


**
FUNCTION uf_importSellOut_inverteSelecao

	IF importSellOut.menu1.seltodas.tag == 'false'
	
		&& aplica sele��o
		importSellOut.menu1.seltodas.tag = 'true'
		
		&& altera o botao
		importSellOut.menu1.seltodas.config("Sel.Todos", myPath + "\imagens\icons\checked_w.png", "uf_importSellOut_inverteSelecao","C")
		
		Select ucrsCondicoesClientes
		Go Top
		Scan
			Replace ucrsCondicoesClientes.sel with .t.
		Endscan
		Select ucrsCondicoesClientes
		Go Top

	ELSE
	
		&& aplica sele��o
		importSellOut.menu1.seltodas.tag = 'false'
		
		&& altera o botao
		importSellOut.menu1.seltodas.config("Sel.Todos", myPath + "\imagens\icons\unchecked_w.png", "uf_importSellOut_inverteSelecao","C")
		
		Select ucrsCondicoesClientes
		Go Top
		Scan
			Replace ucrsCondicoesClientes.sel with .f.
		Endscan
		Select ucrsCondicoesClientes
		Go Top
		
	ENDIF
	

	importSellOut.refresh

ENDFUNC


** 
FUNCTION uf_importSellOut_editar

	IF myimportSellOutIntroducao==.f. AND myimportSellOutAlteracao==.f.
		myimportSellOutAlteracao=.t.
		uf_importSellOut_alternaMenu()			
	ENDIF
	
	importSellOut.refresh

ENDFUNC


**
FUNCTION uf_importSellOut_novaLinha
	
	IF myimportSellOutIntroducao== .f. AND myimportSellOutAlteracao== .f.
		
		uf_perguntalt_chama("Deve colocar o ecr� em edi��o para alterar as definic�es dos clientes!", "", "OK", 64)
		return .f.
	ENDIF
	
	
	SELECT ucrsCondicoesClientes
	APPEND BLANK

	importSellOut.GridPesq.refresh
ENDFUNC


**
FUNCTION uf_importSellOut_apagaLinha

	IF myimportSellOutIntroducao== .f. AND myimportSellOutAlteracao== .f.
		
		uf_perguntalt_chama("Deve colocar o ecr� em edi��o para alterar as definic�es dos clientes!", "", "OK", 64)
		return .f.
	ENDIF
	
	select ucrsCondicoesClientes
	DELETE 
	
	Select ucrsCondicoesClientes
	lcPos = recno()
	select ucrsCondicoesClientes
	TRY
		IF lcPos>1
			go lcPos-1
		ENDIF
	CATCH
		**
	ENDTRY

	importSellOut.GridPesq.refresh
	
	select ucrsCondicoesClientes
	GO BOTTOM 
ENDFUNC



**
FUNCTION uf_importSellOut_eliminaLinha

	SELECT ucrsCondicoesClientes
	APPEND BLANK

	importSellOut.GridPesq.refresh
ENDFUNC


**
FUNCTION  uf_importSellOut_ImportarSellOut

	IF myimportSellOutIntroducao==.t. OR myimportSellOutAlteracao==.t.
		return .f.	
	ENDIF

	regua(0,reccount("ucrsCondicoesClientes"),"A importar SellOut a partir das BDs dos clientes...")
	regua(1,1,"A importar SellOut a partir das BDs dos clientes...")
	
	
	** Verifica se as defini��es de liga��o s�o validas para todos os cliente selecionados
	Select ucrsCondicoesClientes
	Go Top
	Scan for ucrsCondicoesClientes.sel == .t.
		
		** Constroi liga��o � BD do cliente 
		IF !uf_importSellOut_ligacaoBDCliente(ucrsCondicoesClientes.bdname,ucrsCondicoesClientes.odbc)
			uf_perguntalt_chama("N�o foi possivel establecer liga��o com a BD do cliente: " + ALLTRIM(ucrsCondicoesClientes.nome) , "", "OK", 64)
			regua(2)
			return .f.
		ENDIF		
		Select ucrsCondicoesClientes
	ENDSCAN
	
	
	** Obtem informa��o	
	Select ucrsCondicoesClientes
	Go Top
	Scan for ucrsCondicoesClientes.sel == .t.

		regua(1,recno(),"A importar Cliente: " + ALLTRIM(ucrsCondicoesClientes.nome))
		** Constroi liga��o � BD do cliente 
		IF uf_importSellOut_ligacaoBDCliente(ucrsCondicoesClientes.bdname,ucrsCondicoesClientes.odbc)


			lcSQL = ""
			TEXT TO lcSQL NOSHOW TEXTMERGE 
				select * from bo where ndos = 6 AND dataobra between '<<uf_gerais_getDate(importSellOut.dataInicio.value,"SQL")>>' AND '<<uf_gerais_getDate(importSellOut.dataFim.value,"SQL")>>'
			ENDTEXT
			
			uf_importSellOut_sqlExec(lcSQL,"ucrsBOSellOut")
			
			lcSQL = ""
			TEXT TO lcSQL NOSHOW TEXTMERGE 
				select bo2.* from bo inner join bo2 on bostamp = bo2stamp where ndos = 6 AND bo.dataobra between '<<uf_gerais_getDate(importSellOut.dataInicio.value,"SQL")>>' AND '<<uf_gerais_getDate(importSellOut.dataFim.value,"SQL")>>'
			ENDTEXT
			uf_importSellOut_sqlExec(lcSQL,"ucrsBO2SellOut")
			
			
			lcSQL = ""
			TEXT TO lcSQL NOSHOW TEXTMERGE 
				select * from bi where ndos = 6 AND dataobra between '<<uf_gerais_getDate(importSellOut.dataInicio.value,"SQL")>>' AND '<<uf_gerais_getDate(importSellOut.dataFim.value,"SQL")>>'
			ENDTEXT
			uf_importSellOut_sqlExec(lcSQL,"ucrsBiSellOut")
			
			
			lcSQL = ""
			TEXT TO lcSQL NOSHOW TEXTMERGE 
				select bi2.* from bi inner join bi2 on bi2stamp = bistamp where ndos = 6 AND dataobra between '<<uf_gerais_getDate(importSellOut.dataInicio.value,"SQL")>>' AND '<<uf_gerais_getDate(importSellOut.dataFim.value,"SQL")>>'
			ENDTEXT
			uf_importSellOut_sqlExec(lcSQL,"ucrsBi2SellOut")
		
			
			TEXT TO lcSQL TEXTMERGE NOSHOW
				Select * from b_utentes WHERE no = <<ucrsCondicoesClientes.no>> and estab = <<ucrsCondicoesClientes.estab>>
			ENDTEXT 
			IF !uf_gerais_actgrelha("", "ucrsClienteNaSede", lcSql)
				MESSAGEBOX("N�o foi possivel verificar os clientes para aplicar as defini��es. Contacte o suporte.",16,"LOGITOOLS SOFTWARE")
				RETURN .f.
			ENDIF
		
		
			Select ucrsClienteNaSede
			IF RECCOUNT("ucrsClienteNaSede") == 0
				uf_perguntalt_chama("N�o foi possivel verificar a existencia do cliente: " + ALLTRIM(ucrsCondicoesClientes.nome) + " Por favor verifique se o cliente existe.", "", "OK", 64)
				return .f.
			ENDIF
		
			uf_importsellout_disconnect()	
			
			Select ucrsBOSellOut
			IF RECCOUNT("ucrsBOSellOut")> 0
				uf_condicoesComerciais_insereSellOut()
			ENDIF

		ELSE
			uf_perguntalt_chama("N�o foi possivel establecer liga��o com a BD do cliente: " + ALLTRIM(ucrsCondicoesClientes.nome) , "", "OK", 64)
			regua(2)
			return .f.
		ENDIF		
	
		Select ucrsCondicoesClientes
	ENDSCAN
	
	
	regua(2)
	

	uf_perguntalt_chama("Imforma��o de [SellOut] atualizada com sucesso.","","OK")
ENDFUNC 



**
FUNCTION uf_importSellOut_ligacaoBDCliente
	lparameters lcNomeBD, lcOdbc
	LOCAL lcConStr, lcSQL

	STORE "" TO lcConStr, lcSQL
	lcConStr = "DSN=" + ALLTRIM(lcOdbc) + ";UID=logiserv;PWD=ls2k12...;DATABASE="+ALLTRIM(lcNomeBD)+";"

	IF uf_importSellOut_connectDB(lcConStr)
		Return .t.
	ELSE
		RETURN .f.
	ENDIF 

ENDFUNC


**
FUNCTION uf_importSellOut_connectDB
	LPARAMETERS lcConnection_string
	
	PUBLIC gnConnHandle2
	
	*!* Connect to SQL Server
	gnConnHandle2=SQLSTRINGCONN(lcConnection_string)

	IF gnConnHandle2 > 0
	    RETURN .t.
	ELSE
	  MESSAGEBOX("Connection Failed",16,"ODBC Problem")
	  RETURN .f.
	ENDIF

ENDFUNC 

**
FUNCTION uf_importSellOut_sqlExec
	LPARAMETERS lcSQL, lcCursor
	
	IF TYPE("lcCursor") != "C"
		lcCursor = ""
	ENDIF 
	
	TRY
		IF !EMPTY(lcCursor)
			TABLEUPDATE(2,.t.,lcCursor)
		ELSE
			TABLEUPDATE()
		ENDIF
	CATCH
		**
	FINALLY 
		**
	ENDTRY
	 
	IF SQLEXEC(gnConnHandle2,lcSQL,lcCursor) < 0
		RETURN .f.
	ELSE	
		RETURN .t.
	ENDIF 
ENDFUNC


**
FUNCTION uf_importsellout_disconnect
	TRY
		SQLDISCONN(gnConnHandle2)
	CATCH
	ENDTRY
ENDFUNC 


**
FUNCTION uf_importsellout_gravar

	** ELimina defini��es anteriores
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE
		delete from condComercConfig
	ENDTEXT
	IF !uf_gerais_actGrelha("", "",lcSQL)
		MESSAGEBOX("OCORREU UM ERRO A GRAVAR AS DEFINI�OES! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		return .f.
	ENDIF		
	
	SELECT ucrsCondicoesClientes
	Go Top
	SCAN
		lcSQL = ""
		TEXT TO lcSQL NOSHOW TEXTMERGE
			INSERT INTO	condComercConfig (nome,bdname,odbc,no,estab)
			VALUES('<<ucrsCondicoesClientes.nome>>','<<ucrsCondicoesClientes.bdname>>','<<ucrsCondicoesClientes.odbc>>',<<ucrsCondicoesClientes.no>>,<<ucrsCondicoesClientes.estab>>)
		ENDTEXT

		IF !uf_gerais_actGrelha("", "",lcSQL)
			MESSAGEBOX("OCORREU UM ERRO A GRAVAR AS DEFINI�OES! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
			return .f.
		ENDIF		
	ENDSCAN
	
	myIMPORTSELLOUTIntroducao= .f.
	myIMPORTSELLOUTAlteracao= .f.
	uf_IMPORTSELLOUT_alternaMenu()
	
	uf_perguntalt_chama("Defini��es dos clientes gravados com sucesso!", "", "OK", 64)
	
ENDFUNC


**
Function uf_IMPORTSELLOUT_ListaODBC

	&& calcular DataSources
	DECLARE SHORT SQLDataSources IN odbc32;
    INTEGER   EnvironmentHandle,;
    INTEGER   Direction,;
    STRING  @ ServerName,;
    INTEGER   BufferLength1,;
    INTEGER @ NameLength1Ptr,;
    STRING  @ Description,;
    INTEGER   BufferLength2,;
    INTEGER @ NameLength2Ptr

	LOCAL lnRet, lnEnvHandle, lcDriver, lnBufferLen
	lnEnvHandle = VAL(SYS(3053))
	lnBufferLen = 1024
	lcDriver = REPLICATE(CHR(0),lnBufferLen)
	lnRet = SQLDataSources(lnEnvHandle, 2, @lcDriver, lnBufferLen, @lnBufferLen, 0, 0, 0)

	CREATE CURSOR uCrsDSouces (Name CHAR (100))
	lnCount = 1
	DO WHILE lnRet = 0
	  INSERT INTO uCrsDSouces VALUES(LEFT(lcDriver,lnBufferLen))
	  lnBufferLen = 1024
	  lcDriver = REPLICATE(CHR(0),lnBufferLen)
	  lnRet = SQLDataSources(lnEnvHandle, 1, @lcDriver, lnBufferLen, @lnBufferLen, 0, 0, 0)
	ENDDO
	GO TOP
	
ENDFUNC


**
Function uf_condicoesComerciais_insereSellOut
	Local lcBoStamp
	
	Select ucrsBOSellOut
	GO TOP
	SCAN
	
		TEXT TO lcSQL NOSHOW textmerge
			select numero = isnull(max(obrano),0)+1 from bo where ndos = 44
		Endtext
		IF !uf_gerais_actgrelha("","ucrsNumeroSellOut",lcSQL)
			uf_perguntalt_chama("OCORREU UM ERRO A GRAVAR O CABE�ALHO DO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE."+CHR(10)+"Cod: DOC-BO","OK","",16)
			RETURN
		Endif
		
		Select ucrsNumeroSellOut
			
		lcSQL = ""
		TEXT TO lcSQL NOSHOW textmerge
			IF (select COUNT(bostamp) from bo where bostamp = '<<ALLTRIM(ucrsBOSellOut.bostamp)>>')=0
			BEGIN
				Insert into bo
				(	
					bostamp
					,ndos
					,nmdos
					,obrano
					,dataobra
					,nome
					,nome2
					,no
					,estab
					,morada
					,local
					,codpost
					,tipo
					,tpdesc
					,ncont
					,boano
					,etotaldeb
					,dataopen
					,ecusto
					,etotal
					,moeda
					,memissao
					,origem
					,site
					,vendedor
					,vendnm
					,sqtt14
					,ebo_1tvall
					,ebo_2tvall
					,ebo_totp1
					,ebo_totp2
					,ebo11_bins
					,ebo12_bins
					,ebo21_bins
					,ebo22_bins
					,ebo31_bins
					,ebo32_bins
					,ebo41_bins
					,ebo42_bins
					,ebo51_bins
					,ebo52_bins
					,ebo11_iva
					,ebo12_iva
					,ebo21_iva
					,ebo22_iva
					,ebo31_iva
					,ebo32_iva
					,ebo41_iva
					,ebo42_iva
					,ebo51_iva
					,ebo52_iva
					,ocupacao
					,obs
					,ousrinis
					,ousrdata
					,ousrhora
					,usrinis
					,usrdata
					,usrhora
					,datafinal
					,ccusto
					,pnome
					,pno
					
				)Values (
					'<<ALLTRIM(ucrsBOSellOut.bostamp)>>'
					,44
					,'SellOut'
					,<<ucrsNumeroSellOut.numero>>
					,'<<uf_gerais_getDate(ucrsBOSellOut.dataobra,"SQL")>>'
					,'<<ucrsClienteNaSede.nome>>'
					,''
					,<<ucrsClienteNaSede.no>>
					,<<ucrsClienteNaSede.estab>>
					,'<<ALLTRIM(ucrsClienteNaSede.morada)>>'
					,'<<ALLTRIM(ucrsClienteNaSede.local)>>'
					,'<<ALLTRIM(ucrsClienteNaSede.codPost)>>'
					,'<<ALLTRIM(ucrsClienteNaSede.tipo)>>'
					,'<<Alltrim(ucrsClienteNaSede.tpdesc)>>'
					,'<<ALLTRIM(ucrsClienteNaSede.ncont)>>'
					,Year('<<uf_gerais_getDate(ucrsBOSellOut.dataobra,"SQL")>>')
					,<<ucrsBOSellOut.etotaldeb>>
					,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
					,<<ucrsBOSellOut.ecusto>>
					,<<ucrsBOSellOut.etotal>>
					,'<<ucrsBOSellOut.Moeda>>'
					,'EURO'
					,'BO'
					,'<<ALLTRIM(mySite)>>'
					,<<ucrsBOSellOut.vendedor>>
					,'<<ucrsBOSellOut.vendnm>>'
					,<<ucrsBOSellOut.sqtt14>>
					,<<ucrsBOSellOut.ebo_1tvall>>
					,<<ucrsBOSellOut.ebo_2tvall>>
					,<<ucrsBOSellOut.ebo_totp1>>
					,<<ucrsBOSellOut.ebo_totp2>>
					,<<ucrsBOSellOut.ebo11_bins>>
					,<<ucrsBOSellOut.ebo12_bins>>
					,<<ucrsBOSellOut.ebo21_bins>>
					,<<ucrsBOSellOut.ebo22_bins>>
					,<<ucrsBOSellOut.ebo31_bins>>
					,<<ucrsBOSellOut.ebo32_bins>>
					,<<ucrsBOSellOut.ebo41_bins>>
					,<<ucrsBOSellOut.ebo42_bins>>
					,<<ucrsBOSellOut.ebo51_bins>>
					,<<ucrsBOSellOut.ebo52_bins>>
					,<<ucrsBOSellOut.ebo11_iva>>
					,<<ucrsBOSellOut.ebo12_iva>>
					,<<ucrsBOSellOut.ebo21_iva>>
					,<<ucrsBOSellOut.ebo22_iva>>
					,<<ucrsBOSellOut.ebo31_iva>>
					,<<ucrsBOSellOut.ebo32_iva>>
					,<<ucrsBOSellOut.ebo41_iva>>
					,<<ucrsBOSellOut.ebo42_iva>>
					,<<ucrsBOSellOut.ebo51_iva>>
					,<<ucrsBOSellOut.ebo52_iva>>
					,<<ucrsBOSellOut.ocupacao>>
					,'<<ucrsBOSellOut.obs>>'
					,'ADM'
					,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
					,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8)
					,'ADM'
					,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
					,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8)
					,'<<uf_gerais_getDate(ucrsBOSellOut.datafinal,"SQL")>>'
					,'<<ucrsBOSellOut.ccusto>>'
					,'<<ucrsBOSellOut.pnome>>'
					,<<ucrsBOSellOut.pno>>
				)
			END				
		Endtext

		IF !uf_gerais_actGrelha("","",lcSQL)
			uf_perguntalt_chama("OCORREU UM ERRO A GRAVAR O CABE�ALHO DO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE."+CHR(10)+"Cod: DOC-BO","OK","",16)
			RETURN
		Endif
	ENDSCAN
	
	
	Select ucrsBO2SellOut
	GO TOP
	SCAN
	
		Select ucrsBO2SellOut
		TEXT TO lcSql NOSHOW textmerge
		
			IF (select COUNT(bo2stamp) from bo2 where bo2stamp= '<<ALLTRIM(ucrsBO2SellOut.bo2stamp)>>')=0
			BEGIN
				Insert into bo2 (
					bo2stamp
					,autotipo
					,pdtipo
					,etotalciva
					,armazem
					,ousrinis
					,ousrdata
					,ousrhora
					,usrinis
					,usrdata
					,usrhora
					,u_fostamp
				)Values (
					'<<ALLTRIM(ucrsBO2SellOut.bo2stamp)>>'
					,1
					,1
					,<<ucrsBO2SellOut.etotalciva>>
					,<<ucrsBO2SellOut.armazem>>
					,'<<ucrsBO2SellOut.ousrinis>>'
					,'<<uf_gerais_getDate(ucrsBO2SellOut.ousrdata,"SQL")>>'
					,'<<ucrsBO2SellOut.ousrhora>>'
					,'<<ucrsBO2SellOut.usrinis>>'
					,'<<uf_gerais_getDate(ucrsBO2SellOut.usrdata,"SQL")>>'
					,'<<ucrsBO2SellOut.usrhora>>'
					,'<<ucrsBO2SellOut.u_fostamp>>'
				)
			END
		ENDTEXT
		IF !uf_gerais_actGrelha("","",lcSQL)
			uf_perguntalt_chama("OCORREU UM ERRO A GERAR DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			RETURN
		ENDIF
	ENDSCAN	

	Select ucrsBISellOut
	Go Top
	Scan	

			
		TEXT TO lcSql NOSHOW textmerge
			
			IF (select COUNT(bistamp) from bi where bistamp = '<<ucrsBISellOut.bistamp>>')=0
			BEGIN
				Insert into bi
				(
					bistamp
					,bostamp
					,nmdos
					,obrano
					,ref
					,codigo
					,design
					,qtt
					,qtt2
					,uni2qtt
					,u_stockact
					,iva
					,tabiva
					,armazem
					,stipo
					,ndos
					,cpoc
					,familia
					,no
					,nome
					,local
					,morada
					,codpost
					,epu
					,edebito
					,eprorc
					,epcusto
					,ettdeb
					,ecustoind
					,edebitoori
					,u_upc
					,rdata
					,dataobra
					,dataopen
					,lordem
					,lobs
					,ousrinis
					,ousrdata
					,ousrhora
					,usrinis
					,usrdata
					,usrhora
					,obistamp
					,desconto
					,desc2
					,desc3
					,desc4
				)
				Values (
					'<<ucrsBISellOut.bistamp>>'
					,'<<ALLTRIM(ucrsBISellOut.bostamp)>>'
					,'SellOut'
					,<<ucrsNumeroSellOut.numero>>
					,'<<ucrsBISellOut.ref>>'
					,'<<ucrsBISellOut.codigo>>'
					,'<<ucrsBISellOut.design>>'
					,<<ucrsBISellOut.qtt>>
					,<<ucrsBISellOut.qtt2>>
					,<<ucrsBISellOut.uni2qtt>>
					,<<ucrsBISellOut.u_stockact>>
					,<<ucrsBISellOut.iva>>
					,<<ucrsBISellOut.tabiva>>
					,<<ucrsBISellOut.armazem>>
					,<<ucrsBISellOut.stipo>>
					,44
					,<<ucrsBISellOut.cpoc>>
					,'<<ucrsBISellOut.familia>>'
					,<<ucrsClienteNaSede.no>>
					,'<<ucrsClienteNaSede.nome>>'
					,'<<ucrsBISellOut.local>>'
					,'<<ucrsBISellOut.morada>>'
					,'<<ucrsBISellOut.codpost>>'
					,<<ucrsBISellOut.epu>>
					,<<ucrsBISellOut.edebito>>
					,<<ucrsBISellOut.eprorc>>
					,<<ucrsBISellOut.epcusto>>
					,<<ucrsBISellOut.ettdeb>>
					,<<ucrsBISellOut.ecustoind>>
					,<<ucrsBISellOut.edebitoori>>
					,<<ucrsBISellOut.u_upc>>
					,'<<uf_gerais_getDate(ucrsBISellOut.rdata,"SQL")>>'
					,'<<uf_gerais_getDate(ucrsBISellOut.dataobra,"SQL")>>'
					,'<<uf_gerais_getDate(ucrsBISellOut.dataopen,"SQL")>>'
					,<<ucrsBISellOut.lordem>>
					,'<<ucrsBISellOut.lobs>>'
					,'ADM', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8), 'ADM', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8)
					,'<<ucrsBISellOut.obistamp>>'
					,<<ucrsBISellOut.desconto>>
					,<<ucrsBISellOut.desc2>>
					,<<ucrsBISellOut.desc3>>
					,<<ucrsBISellOut.desc4>>
				)
			END
		ENDTEXT
		IF !uf_gerais_actGrelha("","",lcSQL)
			uf_perguntalt_chama("OCORREU UM ERRO A GERAR DOCUMENTO. POR FAVOR CONTACTE O SUPORTE."+CHR(10)+"Cod: DOC-BO","OK","",16)
			RETURN .f.
		Endif
	ENDSCAN
	
	
	Select ucrsBI2SellOut
	Go Top
	SCAN 
		TEXT TO lcSql NOSHOW textmerge
			
			IF (select COUNT(bi2stamp) from bi2 where bi2stamp = '<<ALLTRIM(ucrsBI2SellOut.bi2stamp)>>')=0
			BEGIN
				Insert into bi2(
					bi2stamp
					,bostamp
					,morada
					,local
					,codpost
				)Values (
					'<<ALLTRIM(ucrsBI2SellOut.bi2stamp)>>'
					, '<<ALLTRIM(ucrsBISellOut.bostamp)>>'
					, '<<ALLTRIM(ucrsBI2SellOut.morada)>>'
					, '<<ALLTRIM(ucrsBI2SellOut.local)>>'
					, '<<ALLTRIM(ucrsBI2SellOut.codpost)>>'
				)
			END
		ENDTEXT
		IF !uf_gerais_actGrelha("","",lcSQL)
			uf_perguntalt_chama("OCORREU UM ERRO A GERAR DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			RETURN .f.
		ENDIF
	ENDSCAN
	
	
ENDFUNC


**
FUNCTION uf_IMPORTSELLOUT_sair

	IF myIMPORTSELLOUTIntroducao== .t. OR myIMPORTSELLOUTAlteracao== .t.
		IF uf_perguntalt_chama("Pretende cancelar as altera��es?", "Sim", "N�o", 32)
			
			myIMPORTSELLOUTIntroducao= .f.
			myIMPORTSELLOUTAlteracao= .f.
			uf_IMPORTSELLOUT_alternaMenu()
		ENDIF
	ELSE
		uf_IMPORTSELLOUT_exit()
	ENDIF

ENDFUNC


**
FUNCTION uf_IMPORTSELLOUT_exit
	IMPORTSELLOUT.hide
	IMPORTSELLOUT.release
ENDFUNC


