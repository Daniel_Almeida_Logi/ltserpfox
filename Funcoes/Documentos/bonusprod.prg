**
FUNCTION uf_BONUSPROD_chama
	IF TYPE("BONUSPROD") == "U"
		DO FORM BONUSPROD
	ELSE
		BONUSPROD.show()
	ENDIF
	
	&&menu
	IF TYPE("BONUSPROD.menu1.imprimir") == "U"
		BONUSPROD.menu1.adicionaOpcao("imprimir","Imprimir",myPath + "\imagens\icons\imprimir_w.png","uf_BONUSPROD_imprimir","I")
	ENDIF
ENDFUNC


**
FUNCTION uf_BONUSPROD_imprimir
	IF RECCOUNT("ucrsBonus") > 0
		
		**Trata parametros
		SELECT cabdoc
		lcStamp = ALLTRIM(cabdoc.cabstamp)
		
		IF myTipoDoc == "FO"
			lcTipo = "FO"
		ELSE
			lcTipo = "BO"
		ENDIF 
		
		parametros  = ''
		TEXT TO parametros TEXTMERGE NOSHOW
			&stamp=<<lcStamp>>&tipo=<<lcTipo>>&site=<<ALLTRIM(mysite)>>
		ENDTEXT 

		uf_gerais_chamaReport("listagem_documentos_bonus", parametros)
	ENDIF 
ENDFUNC

**
FUNCTION uf_BONUSPROD_sair
	BONUSPROD.hide
	BONUSPROD.release
	
	IF USED("ucrsBonus")
		fecha("ucrsBonus")
	ENDIF 
ENDFUNC
