**
FUNCTION uf_DESCDOC_chama
	PARAMETERS lcFnstamp
	PUBLIC lcDescperc, lcDescval, lcPclFinal, lcPclInicial, lcQttFn, lcnovo
	STORE 0.00 TO lcDescperc, lcDescval, lcPclFinal
	STORE .f. TO lcnovo
	lcPclFinal = fn.u_upc
	lcPclInicial  = fn.u_upc
	lcQttFn = fn.qtt
	
	IF !USED("ucrsDescontos")

		SELECT fnstamp, qtd, euro, percentagem FROM ucrsfndesclin WHERE fnstamp=ALLTRIM(lcFnstamp) ORDER BY nrlin ASC INTO CURSOR ucrsDescontos READWRITE 
		
		IF RECCOUNT("ucrsDescontos")=0 
			lcnovo = .t.	
	   		**CREATE CURSOR ucrsDescontos (fnstamp c(30), qtd n(6,0), euro n(10,2), percentagem n(6,2))
			SELECT ucrsDescontos 
			APPEND BLANK
			replace ucrsDescontos.fnstamp WITH ALLTRIM(lcFnstamp)
			replace ucrsfndesclin.qtd WITH 0
			replace ucrsfndesclin.euro WITH 0.00
			replace ucrsfndesclin.percentagem WITH 0.00
			APPEND BLANK
			replace ucrsDescontos.fnstamp WITH ALLTRIM(lcFnstamp)
			replace ucrsfndesclin.qtd WITH 0
			replace ucrsfndesclin.euro WITH 0.00
			replace ucrsfndesclin.percentagem WITH 0.00
			APPEND BLANK
			replace ucrsDescontos.fnstamp WITH ALLTRIM(lcFnstamp)
			replace ucrsfndesclin.qtd WITH 0
			replace ucrsfndesclin.euro WITH 0.00
			replace ucrsfndesclin.percentagem WITH 0.00
		ENDIF 
		SELECT ucrsDescontos
		GO TOP 		
		
	ENDIF 
   
	IF TYPE("DESCDOC") == "U"
		DO FORM DESCDOC
	ELSE
		DESCDOC.show()
	ENDIF
	
ENDFUNC

**
FUNCTION uf_DESCDOC_gravar
	LOCAL lcNrlin
	STORE 1 TO lcNrlin

	uf_DESCDOC_recalcula()
	
	IF lcnovo = .t.
		SELECT ucrsDescontos
		GO TOP 
		SCAN 
			SELECT ucrsfndesclin 
			APPEND BLANK 
			replace ucrsfndesclin.fnstamp WITH ucrsDescontos.fnstamp
			replace ucrsfndesclin.nrlin WITH lcNrlin 
			replace ucrsfndesclin.qtd WITH ucrsDescontos.qtd
			replace ucrsfndesclin.euro WITH ucrsDescontos.euro
			replace ucrsfndesclin.percentagem WITH ucrsDescontos.percentagem 
			lcNrlin = lcNrlin +1
			SELECT ucrsDescontos
		ENDSCAN 
	ELSE
		SELECT ucrsDescontos
		GO TOP 
		SCAN 
			SELECT ucrsfndesclin 
			GO TOP 
			SCAN 
				IF  ucrsfndesclin.fnstamp = ucrsDescontos.fnstamp AND ucrsfndesclin.nrlin = lcNrlin
					replace ucrsfndesclin.fnstamp WITH ucrsDescontos.fnstamp
					replace ucrsfndesclin.qtd WITH ucrsDescontos.qtd
					replace ucrsfndesclin.euro WITH ucrsDescontos.euro
					replace ucrsfndesclin.percentagem WITH ucrsDescontos.percentagem 
				ENDIF 
			ENDSCAN 
			lcNrlin = lcNrlin +1
			SELECT ucrsDescontos
		ENDSCAN 
	ENDIF 
	
	
	SELECT fn 
	replace fn.desconto WITH lcDescperc 
	MyDescLinAcum = lcDescperc 
	
	lcFnstamp = fn.fnstamp
	**uf_documentos_recalculaTotaisFN()
	IF lcDescperc < 100
		replace fn.epv WITH (fn.etiliquido/fn.qtt)/(1-(fn.desconto/100))
	ELSE
		replace fn.etiliquido WITH 0
	ENDIF 
	replace fn.descant WITH fn.desconto
	replace fn.pctant WITH fn.epv
	
	replace fn.u_upc WITH lcPclFinal 
				
	uf_documentos_actualizaTotaisCAB()
	DOCUMENTOS.PageFrame1.Page1.detalhest1.refresh
				
	uf_documentos_EventosGridDoc(.t.) &&desactiva()
	documentos.lockscreen = .f.	
		
	DOCUMENTOS.PageFrame1.Page1.gridDoc.setfocus && Corrige bug Documento sem linhas	

	DOCUMENTOS.Pageframe1.Page1.ContainerLinhas.TOTAL.refresh	
	DOCUMENTOS.PageFrame1.Page1.detalhest1.refresh
	
	
	IF  Alltrim(mytipoDoc) == "BO"
		SELECT BI 	
		uf_documentos_editarTemp(bi.bistamp)
	ELSE 
		SELECT FN
 		uf_documentos_editarTemp(fn.fnstamp)
	ENDIF
	
	
	uf_DESCDOC_sair()
	
ENDFUNC

**
FUNCTION uf_DESCDOC_sair

	IF USED("ucrsDescontos")
		fecha("ucrsDescontos")
	ENDIF 
	
	DESCDOC.hide
	DESCDOC.release
	
	RELEASE DESCDOC
	
ENDFUNC

FUNCTION uf_DESCDOC_refresh
	uf_DESCDOC_recalcula()
	descdoc.GridPesq.setfocus
ENDFUNC 

FUNCTION uf_DESCDOC_recalcula
	LOCAL lcPrecoFinal, lcTotQtd, lcDescQtt, lcTotvalor, lcTotafterdescval, lcTotafterdescperc, lcValDescPerc, lcdescbonus, lcPclAntesdescperc
	
	STORE 0 TO lcTotQtd, lcTotvalor, lcTotafterdescval, lcTotafterdescperc, lcValDescPerc, lcdescbonus, lcPclAntesdescperc
	
	IF USED("ucrsDescontostemp")
		fecha("ucrsDescontostemp")
	ENDIF 
	
	SELECT ucrsDescontos 
	SELECT * FROM ucrsDescontos INTO CURSOR ucrsDescontostemp readwrite
	
	SELECT ucrsDescontostemp
	GO TOP 
	SCAN 
		lcTotQtd = lcTotQtd + ucrsDescontostemp.qtd
		lcTotvalor = lcTotvalor + ucrsDescontostemp.euro
	ENDSCAN 
	
	IF lcTotQtd  > 0
		lcDescQtt = ROUND(lcTotQtd/lcQttFn ,2)
		lcDescperc = lcDescQtt * 100
	ELSE
		lcDescQtt = 0
		lcDescperc = 0
	ENDIF 
	lcDescval = ROUND(lcPclInicial * lcDescQtt,2)
	lcdescbonus = lcDescval 
	lcPclFinal = lcPclInicial - lcDescval 
	
	IF lcTotvalor > 0
		lcTotafterdescval = lcPclFinal - lcTotvalor 
		lcDescperc = ROUND(((lcDescval + lcTotvalor ) / lcPclInicial ) * 100,2)
		lcDescval =  lcDescval + lcTotvalor 
		lcPclFinal = lcTotafterdescval 
	ENDIF 

	LOCAL lcNrDesc
	STORE 0 TO lcNrDesc
	SELECT ucrsDescontostemp
	GO TOP 
	SCAN 
		IF ucrsDescontostemp.percentagem > 0
			lcNrDesc = lcNrDesc + 1
		ENDIF 
	ENDSCAN 
	
	lcPclAntesdescperc = lcPclFinal 
	IF lcNrDesc = 1 AND lcTotQtd = 0 AND lcTotvalor = 0 THEN 
		SELECT ucrsDescontostemp
		GO TOP 
		SCAN 
			IF ucrsDescontostemp.percentagem > 0
				lcDescval = ROUND((lcPclAntesdescperc * (ucrsDescontostemp.percentagem / 100)) ,2)
				lcDescperc = ucrsDescontostemp.percentagem
			ENDIF 
		ENDSCAN 
	ELSE 
		SELECT ucrsDescontostemp
		GO TOP 
		SCAN 
			IF ucrsDescontostemp.percentagem > 0
				lcValDescPerc = lcValDescPerc + ROUND((lcPclAntesdescperc * (ucrsDescontostemp.percentagem / 100)) ,2)
				lcPclAntesdescperc = lcPclAntesdescperc - lcValDescPerc 
			ENDIF 
		ENDSCAN 
		IF lcValDescPerc > 0
			lcDescperc = ROUND(((lcdescbonus + lcTotvalor + lcValDescPerc) / lcPclInicial ) * 100,2)
			lcDescval =  lcDescval + lcValDescPerc 
			lcPclFinal = lcPclFinal - 	lcValDescPerc 
		ENDIF 
		IF USED("ucrsDescontostemp")
			fecha("ucrsDescontostemp")
		ENDIF 
	ENDIF 
	
	DESCDOC.refresh
	SELECT ucrsDescontos 
ENDFUNC 

FUNCTION uf_DESCDOC_limpa
	PARAMETERS lcFnstamp
	SELECT fnstamp, qtd, euro, percentagem FROM ucrsfndesclin WHERE fnstamp=ALLTRIM(lcFnstamp) ORDER BY nrlin ASC INTO CURSOR ucrsDescontos READWRITE 
	SELECT ucrsfndesclin 
	GO TOP 
	SCAN 
		IF ALLTRIM(ucrsfndesclin.fnstamp) == ALLTRIM(lcFnstamp)
			 replace ucrsfndesclin.qtd WITH 0
			 replace ucrsfndesclin.euro WITH 0
			 replace ucrsfndesclin.percentagem WITH 0
		ENDIF 
	ENDSCAN 
ENDFUNC 