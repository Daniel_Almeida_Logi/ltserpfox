**
FUNCTION uf_opcoesdiversasDoc_chama
	**
	IF TYPE("OPCOESDIVERSASDOC")=="U"
		DO FORM OPCOESDIVERSASDOC
		OPCOESDIVERSASDOC.show
	ELSE
		OPCOESDIVERSASDOC.show
	ENDIF
ENDFUNC



**
FUNCTION uf_opcoesdiversasDoc_opcoesDiversas
	LPARAMETERS tcOrdem
	
	DO Case
		CASE tcOrdem == 1
			uf_documentos_importarArtigosPorIva()
		CASE tcOrdem == 2
			uf_Documentos_ProdEncNaoRecDoc()
		CASE tcOrdem == 3
			uf_Documentos_ImprimeTalaoPOSReservaIN()
			uf_opcoesdiversasDoc_sair()
		OTHERWISE
			**
	Endcase
ENDFUNC 


**
FUNCTION uf_opcoesdiversasDoc_FiltraOpcoesDiversas
 	SELECT uCrsOpcoesDiversasDoc	
	IF !EMPTY(ALLTRIM(OPCOESDIVERSASDOC.text1.value))
		SET FILTER TO LIKE ('*'+ALLTRIM(UPPER(OPCOESDIVERSASDOC.text1.value))+'*',ALLTRIM(UPPER(uCrsOpcoesDiversasDoc.desc)))
	ELSE
		SET FILTER TO 
	ENDIF
	
	OPCOESDIVERSASDOC.refresh
ENDFUNC


**
FUNCTION uf_opcoesdiversasDoc_sair
	**Fecha Cursores
	OPCOESDIVERSASDOC.hide
	OPCOESDIVERSASDOC.release
	RELEASE OPCOESDIVERSASDOC
ENDFUNC

**
FUNCTION uf_opcoesdiversasDoc_CarregaMenu
	
ENDFUNC