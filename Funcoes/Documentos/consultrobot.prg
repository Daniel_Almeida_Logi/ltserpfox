
***
FUNCTION uf_consultRobot_chama
		
	IF !uf_consultRobot_criarCursor()
        RETURN .F.
    ENDIF
				
	IF TYPE("CONSULTROBOT")=="U"
		DO FORM CONSULTROBOT
	ELSE
		CONSULTROBOT.show
	ENDIF
		
ENDFUNC 

**
PROCEDURE uf_consultRobot_criarCursor

    IF USED("uc_recRoboLin")
        FECHA("uc_recRoboLin")
    ENDIF

	SELECT uCrsRecRoboPend

    uv_nrConf = uCrsRecRoboPend.nrConf

    TEXT TO msel TEXTMERGE NOSHOW
        EXEC up_robot_recPendentes_Lin '<<ALLTRIM(uv_nrConf)>>'
    ENDTEXT

    IF !uf_gerais_actGrelha("", "uc_recRoboLin", msel)
        uf_perguntalt_chama("Erro ao gerar an�lise." + chr(13) + "Por favor contacte o suporte","OK","",16)
        RETURN .F.
    ENDIF

    IF myDocIntroducao == .t. OR myDocAlteracao == .t.

        SELECT uc_recRoboLin
        REPLACE uc_recRoboLin.documento WITH 0 FOR 1=1

        SELECT uc_recRoboLin
        GO TOP
        
        SCAN

            SELECT FN
            SELECT TOP 1 SUM(fn.qtRec) as documento FROM fn WITH (BUFFERING = .T.) WHERE ALLTRIM(UPPER(fn.ref)) = ALLTRIM(UPPER(uc_recRoboLin.ref)) GROUP BY fn.ref ORDER BY documento DESC INTO CURSOR uc_sumLin READWRITE

            SELECT uc_sumLin
            SELECT uc_recRoboLin

            REPLACE uc_recRoboLin.documento     WITH uc_sumLin.documento,;
                    uc_recRoboLin.diff          WITH (uc_recRoboLin.robot - uc_recRoboLin.documento)

            FECHA("uc_sumLin")

            SELECT uc_recRoboLin
        ENDSCAN

        SELECT uc_recRoboLin
        GO TOP

    ENDIF

ENDPROC

FUNCTION uf_consultRobot_sair
	CONSULTROBOT.hide
	CONSULTROBOT.release
ENDFUNC

**
