**
FUNCTION uf_fechoautomaticodoc_chama
	LPARAMETERS lcTipo
	
	LOCAL lcSQL

	IF USED("ucrsFecharDoc")
		SELECT ucrsFecharDoc
		DELETE ALL
	ELSE
		CREATE CURSOR ucrsFecharDoc(fechar l,origem c(2), cabStamp c(254),nomedoc c(254))
	ENDIF

	local lcExistStamp 
	
	DO CASE
		CASE lcTipo == 'FO'

			SELECT FN
			Go TOP
			SCAN 
				lcExistStamp = .f.	
				IF !EMPTY(FN.OFNSTAMP) OR !EMPTY(ALLTRIM(FN.BISTAMP))
					
					TEXT TO lcSQL TEXTMERGE NOSHOW
						exec up_documentos_FechoAutomatico '<<ALLTRIM(FN.OFNSTAMP)>>', '<<ALLTRIM(FN.BISTAMP)>>' 
					ENDTEXT

					IF !uf_gerais_actGrelha("","ucrsTempFechaDoc",lcSQL)
						uf_perguntalt_chama("OCORREU UMA ANOMALIA NA VERIFICA��O DE DOCUMENTOS PARA FECHO AUTOM�TICO. POR FAVOR VERIFIQUE.","OK","",16)
						RETURN .f.
					ENDIF 
					SELECT ucrsFecharDoc
					SCAN 
						IF ALLTRIM(ucrsFecharDoc.cabStamp) == ALLTRIM(ucrsTempFechaDoc.cabStamp)
							lcExistStamp = .t.					
						ENDIF
					ENDSCAN
					IF lcExistStamp = .f.
						SELECT ucrsTempFechaDoc
						IF !EMPTY(ALLTRIM(ucrsTempFechaDoc.cabStamp))
						
							SELECT  ucrsFecharDoc
							APPEND  BLANK
							REPLACE ucrsFecharDoc.origem 	WITH ucrsTempFechaDoc.origem 
							REPLACE ucrsFecharDoc.cabStamp 	WITH ucrsTempFechaDoc.cabStamp	
							REPLACE ucrsFecharDoc.nomedoc 	WITH ucrsTempFechaDoc.Doc
							REPLACE ucrsFecharDoc.fechar 	WITH .t.
							
						ENDIF
					ENDIF 

					** Documentos Externos
					LOCAL lcConnection_stringCC, gnConnHandleCC,lcIDCliente 
					lcIDCliente = ""

					SELECT ucrsE1
					lcIDCliente = ALLTRIM(ucrsE1.id_lt)
					IF !EMPTY(lcIDCliente)

						lcConnection_stringCC = "DSN=MariaDB_MySQL_UNICODE;UID=root;PWD=mdb2016cn;DATABASE=ltsfx;App=LT"
						gnConnHandleCC = SQLSTRINGCONN(lcConnection_stringCC)

						IF gnConnHandleCC > 0	    
							TEXT TO lcSQL TEXTMERGE NOSHOW
								Select 
									'EX' as ORIGEM
									,docs.id as cabstamp
									,CONCAT('Externo: ', CAST(docs.nr AS CHAR)) as Doc
								from 
									docs 
									inner join emp on docs.id_emp = emp.id
									inner join entidades on docs.id_ent = entidades.id
								WHERE
									emp.id_ext = '<<ALLTRIM(lcIDCliente)>>'
									and docs.id = '<<ALLTRIM(FN.OFNSTAMP)>>'
							ENDTEXT
						
							**
							SQLEXEC(gnConnHandleCC, lcSQL ,"uCrsDocsFecharCentralCompras")
						
							SELECT uCrsDocsFecharCentralCompras
							GO TOP
							SCAN 
								lcExistStamp = .f.		
								SELECT ucrsFecharDoc
								SCAN 
									IF ALLTRIM(ucrsFecharDoc.cabStamp) == ALLTRIM(uCrsDocsFecharCentralCompras.cabStamp)
										lcExistStamp = .t.					
									ENDIF
								ENDSCAN
								IF lcExistStamp = .f.
									SELECT ucrsTempFechaDoc
									IF !EMPTY(ALLTRIM(uCrsDocsFecharCentralCompras.cabStamp))
									
										SELECT  ucrsFecharDoc
										APPEND  BLANK
										REPLACE ucrsFecharDoc.origem 	WITH uCrsDocsFecharCentralCompras.origem 
										REPLACE ucrsFecharDoc.cabStamp 	WITH uCrsDocsFecharCentralCompras.cabStamp	
										REPLACE ucrsFecharDoc.nomedoc 	WITH uCrsDocsFecharCentralCompras.Doc
										REPLACE ucrsFecharDoc.fechar 	WITH .t.
										
									ENDIF
								ENDIF 
							ENDSCAN 
						ENDIF
					ENDIF 
					
				ENDIF
			ENDSCAN 
*!*				** Caso seja V/Fatura Resumo atualiza o campo 
*!*				SELECT CabDoc
*!*				IF ALLTRIM(UPPER(CabDoc.doc)) == "V/FACTURA RESUMO"
*!*				
*!*					SELECT ucrsFecharDoc
*!*					GO Top
*!*					SCAN 
*!*						
*!*						TEXT TO lcSQL NOSHOW TEXTMERGE
*!*							UPDATE FO SET bloqpag = 1 WHERE fostamp = '<<ucrsFecharDoc.cabStamp>>'
*!*						ENDTEXT
*!*						
*!*						IF !uf_gerais_actGrelha("","",lcSQL)
*!*							uf_perguntalt_chama("OCORREU UMA ANOMALIA NA MARCA��O DOS DOUMENTOS IMPORTADOS COMO BLOQUEADOS PARA PAGAMENTO. POR FAVOR VERIFIQUE.","OK","",16)
*!*							RETURN .f.
*!*						ENDIF

*!*					ENDSCAN 
*!*				ENDIF 

		CASE lcTipo == 'BO'
			SELECT BI
			Go TOP
			SCAN 
				lcExistStamp = .f.	
				IF !EMPTY(ALLTRIM(BI.OBISTAMP))
					
					TEXT TO lcSQL TEXTMERGE NOSHOW
						exec up_documentos_FechoAutomatico '', '<<ALLTRIM(BI.OBISTAMP)>>' 
					ENDTEXT
					IF !uf_gerais_actGrelha("","ucrsTempFechaDoc",lcSQL)
						uf_perguntalt_chama("OCORREU UMA ANOMALIA NA VERIFICA��O DE DOCUMENTOS PARA FECHO AUTOM�TICO. POR FAVOR VERIFIQUE.","OK","",16)
						RETURN .f.
					ENDIF 
					SELECT ucrsFecharDoc
					SCAN 
						IF ALLTRIM(ucrsFecharDoc.cabStamp) == ALLTRIM(ucrsTempFechaDoc.cabStamp)
							lcExistStamp = .t.					
						ENDIF
					ENDSCAN
					IF lcExistStamp = .f.
						SELECT ucrsTempFechaDoc
						IF !EMPTY(ALLTRIM(ucrsTempFechaDoc.cabStamp))
						
							SELECT  ucrsFecharDoc
							APPEND  BLANK
							REPLACE ucrsFecharDoc.origem 	WITH ucrsTempFechaDoc.origem 
							REPLACE ucrsFecharDoc.cabStamp 	WITH ucrsTempFechaDoc.cabStamp	
							REPLACE ucrsFecharDoc.nomedoc 	WITH ucrsTempFechaDoc.Doc
							REPLACE ucrsFecharDoc.fechar 	WITH .t.
							
						ENDIF
					ENDIF 
				ENDIF
			ENDSCAN 
		CASE lcTipo == 'PAGFORN'
			SELECT PL
			Go TOP
			SCAN 
				lcExistStamp = .f.	
				IF !EMPTY(ALLTRIM(PL.FCSTAMP))
					TEXT TO lcSQL TEXTMERGE NOSHOW
						exec up_documentos_FechoAutomatico '<<ALLTRIM(PL.FCSTAMP)>>', '' 
					ENDTEXT
					IF !uf_gerais_actGrelha("","ucrsTempFechaDoc",lcSQL)
						uf_perguntalt_chama("OCORREU UMA ANOMALIA NA VERIFICA��O DE DOCUMENTOS PARA FECHO AUTOM�TICO. POR FAVOR VERIFIQUE.","OK","",16)
						RETURN .f.
					ENDIF 
					SELECT ucrsFecharDoc
					SCAN 
						IF ALLTRIM(ucrsFecharDoc.cabStamp) == ALLTRIM(ucrsTempFechaDoc.cabStamp)
							lcExistStamp = .t.					
						ENDIF
					ENDSCAN
					IF lcExistStamp = .f.
						SELECT ucrsTempFechaDoc
						IF !EMPTY(ALLTRIM(ucrsTempFechaDoc.cabStamp))
						
							SELECT  ucrsFecharDoc
							APPEND  BLANK
							REPLACE ucrsFecharDoc.origem 	WITH ucrsTempFechaDoc.origem 
							REPLACE ucrsFecharDoc.cabStamp 	WITH ucrsTempFechaDoc.cabStamp	
							REPLACE ucrsFecharDoc.nomedoc 	WITH ucrsTempFechaDoc.Doc
							REPLACE ucrsFecharDoc.fechar 	WITH .t.
							
						ENDIF
					ENDIF 
				ENDIF
			ENDSCAN 
			
		CASE lcTipo == 'FT'
			SELECT FI
			Go TOP
			SCAN 
				lcExistStamp = .f.	
				IF !EMPTY(ALLTRIM(FI.BISTAMP))
					
					TEXT TO lcSQL TEXTMERGE NOSHOW
						exec up_documentos_FechoAutomatico '', '<<ALLTRIM(FI.BISTAMP)>>' 
					ENDTEXT
					IF !uf_gerais_actGrelha("","ucrsTempFechaDoc",lcSQL)
						uf_perguntalt_chama("OCORREU UMA ANOMALIA NA VERIFICA��O DE DOCUMENTOS PARA FECHO AUTOM�TICO. POR FAVOR VERIFIQUE.","OK","",16)
						RETURN .f.
					ENDIF 
					SELECT ucrsFecharDoc
					SCAN 
						IF ALLTRIM(ucrsFecharDoc.cabStamp) == ALLTRIM(ucrsTempFechaDoc.cabStamp)
							lcExistStamp = .t.					
						ENDIF
					ENDSCAN
					IF lcExistStamp = .f.
						SELECT ucrsTempFechaDoc
						IF !EMPTY(ALLTRIM(ucrsTempFechaDoc.cabStamp))
						
							SELECT  ucrsFecharDoc
							APPEND  BLANK
							REPLACE ucrsFecharDoc.origem 	WITH ucrsTempFechaDoc.origem 
							REPLACE ucrsFecharDoc.cabStamp 	WITH ucrsTempFechaDoc.cabStamp	
							REPLACE ucrsFecharDoc.nomedoc 	WITH ucrsTempFechaDoc.Doc
							REPLACE ucrsFecharDoc.fechar 	WITH .t.
							
						ENDIF
					ENDIF 
				ENDIF
			ENDSCAN 
		ENDCASE
*!*		IF USED("CabDoc")	
*!*			SELECT CabDoc
*!*			IF ALLTRIM(UPPER(CabDoc.doc)) == "V/FACTURA RESUMO"	
*!*				RETURN .f.
*!*			ENDIF 
*!*		ENDIF 
	
	Select ucrsFecharDoc
	GO TOP
	Select ucrsFecharDoc
	COUNT TO lcNrRecords
	IF lcNrRecords > 0

*!*	        IF USED("cabdoc")

*!*	            SELECT cabdoc

*!*	            IF ALLTRIM(cabdoc.doc) == "V/Factura Resumo"

*!*	                uv_cabs = ""

*!*	                Select ucrsFecharDoc
*!*			        GO TOP

*!*	                SCAN

*!*	                    uv_cabs = uv_cabs + IIF(!EMPTY(uv_cabs), ',', '') + "'" + ALLTRIM(ucrsFecharDoc.cabStamp) + "'"

*!*	                    Select ucrsFecharDoc
*!*	                ENDSCAN

*!*	                IF !EMPTY(uv_cabs)

*!*	                    TEXT TO uv_sql TEXTMERGE NOSHOW
*!*	                        UPDATE fc SET ecredf = ecred WHERE fcstamp in (<<ALLTRIM(uv_cabs)>>)
*!*	                    ENDTEXT

*!*	                    uf_gerais_actGrelha("","",uv_sql)

*!*	                ENDIF

*!*	            ENDIF

*!*	        ENDIF

		Select ucrsFecharDoc
		GO top
		**
		DO FORM FECHOAUTOMATICODOC WITH lcTipo
	ENDIF

ENDFUNC


**
FUNCTION uf_fechoautomaticodoc_gravar
	LOCAL lcSQL
	IF !USED("ucrsFecharDoc")
		return .f.
	ENDIF
	
	Select ucrsFecharDoc
	SCAN 
		If ucrsFecharDoc.fechar = .t.
		
			If ucrsFecharDoc.origem = "BO"

				uv_estado = 'Faturada_Recibada'

				IF uf_gerais_getParameter_site("ADM0000000186", "BOOL", mySite) AND FECHOAUTOMATICODOC.tipo = "FT"

					Text To lcSQL Textmerge noshow
						select 
							status, bo.nmdos, bo.obrano, CONVERT(varchar, bo.dataobra, 104) as dataobra, codext,pagamento, bo.no, bo.estab, bo.etotal, bo2.modo_envio,
							bo.ousrinis
						from bo2 (nolock)
						inner join bo (nolock) on bo.bostamp=bo2.bo2stamp 
						where bo2.bo2stamp='<<ALLTRIM(ucrsFecharDoc.cabstamp)>>'
					ENDTEXT
		
					uf_gerais_actGrelha("","uCrsstatus",lcSQL)

					IF RECCOUNT("uCrsstatus") > 0

						SELECT uCrsstatus
						GO TOP

						IF ALLTRIM(UPPER(uCrsstatus.ousrinis)) == "ONL"

							TEXT TO uv_sql TEXTMERGE NOSHOW
								exec up_vendas_online_getStatusChange '<<ALLTRIM(uCrsstatus.modo_envio)>>', '<<ALLTRIM(uCrsstatus.pagamento)>>', 'Logitools', '<<ALLTRIM(uCrsstatus.status)>>', 1
							ENDTEXT

							IF !uf_gerais_ActGrelha("","uc_getStatusChange", uv_sql)
								uf_perguntalt_chama("Erro a verificar a altera��o de estado." + CHR(13) + "Por favor contacte o suporte.","OK","",48)
								RETURN .f.
							ENDIF

							uv_estado = ALLTRIM(uc_getStatusChange.status_Fim)

							FECHA("uc_getStatusChange")

						ENDIF

					ENDIF

					FECHA("uCrsstatus")

					SELECT ucrsFecharDoc
				ENDIF
         
				TEXT TO uv_sql TEXTMERGE NOSHOW
					Update 
						BO 
					Set 
						FECHADA = 1,
						DATAFECHO = dateadd(HOUR, <<difhoraria>>, getdate()),
						usrdata = dateadd(HOUR, <<difhoraria>>, getdate()),
						usrhora = convert(varchar,getdate(),8),
						usrinis = '<<ALLTRIM(m_chinis)>>'
					Where 
						BOSTAMP = '<<Alltrim(ucrsFecharDoc.cabstamp)>>'
			
					UPDATE
						BI 
					SET	
						FECHADA = 1,
						usrdata = dateadd(HOUR, <<difhoraria>>, getdate()),
						usrhora = convert(varchar,getdate(),8),
						usrinis = '<<ALLTRIM(m_chinis)>>'
					FROM
						BI
					WHERE
						BOSTAMP ='<<Alltrim(ucrsFecharDoc.cabstamp)>>'
						
					UPDATE bo2 SET 
						status='<<ALLTRIM(uv_estado)>>',
						usrdata = dateadd(HOUR, <<difhoraria>>, getdate()),
						usrhora = convert(varchar,getdate(),8),
						usrinis = '<<ALLTRIM(m_chinis)>>'
					WHERE bo2stamp='<<Alltrim(ucrsFecharDoc.cabstamp)>>'
											
				ENDTEXT 

				IF uf_gerais_getparameter_site('ADM0000000199', 'BOOL', mysite)

					IF !uf_gerais_chkconexao()
						uf_perguntalt_chama("N�o existe liga��o ao servidor Central. Por favor volte a tentar mais tarde.","OK","",16)
						RETURN .F.
					ENDIF

					IF !uf_gerais_runSQLCentral(uv_sql)
						uf_perguntalt_chama("N�o foi poss�vel atualizar o documento no servidor Central.","OK","",16)
						RETURN .F.
					ENDIF

				ENDIF

				IF !uf_gerais_actGrelha("","",uv_sql)
					uf_perguntalt_chama("N�O FOI POSSIVEL PROCEDER AO FECHO DOS DOCUMENTOS.","OK","",16)	
				Endif
				
			ENDIF 
			
			If ucrsFecharDoc.origem = "FO"
				TEXT TO lcSQL TEXTMERGE NOSHOW
					Update 
						FO 
					Set 
						U_STATUS = 'F' 
					Where 
						FOSTAMP = '<<Alltrim(ucrsFecharDoc.cabStamp)>>'
				ENDTEXT
				IF !uf_gerais_actGrelha("","",lcSQL)
					uf_perguntalt_chama("N�O FOI POSSIVEL PROCEDER AO FECHO DOS DOCUMENTOS.","OK","",16)	
				Endif
			Endif
			
			If ucrsFecharDoc.origem = "EX"
			
				** Documentos Externos
				LOCAL lcConnection_stringCC, gnConnHandleCC,lcIDCliente 
				lcIDCliente = ""

				SELECT ucrsE1
				lcIDCliente = ALLTRIM(ucrsE1.id_lt)
				IF !EMPTY(lcIDCliente)

					lcConnection_stringCC = "DSN=MariaDB_MySQL_UNICODE;UID=root;PWD=mdb2016cn;DATABASE=ltsfx;App=LT"
					gnConnHandleCC = SQLSTRINGCONN(lcConnection_stringCC)

					IF gnConnHandleCC > 0	    
					ELSE
					  **uf_perguntalt_chama("N�o foi possivel a liga��o � Central de Compras. Contacte o Suporte.","OK","",16)
					  RETURN .f.
					ENDIF
					
					TEXT TO lcSQL TEXTMERGE NOSHOW
						UPDATe docs	SET estado = 1 WHERE docs.id = '<<Alltrim(ucrsFecharDoc.cabstamp)>>'
					ENDTEXT
					
					**
					SQLEXEC(gnConnHandleCC, lcSQL ,"uCrsDocsFecharCentralCompras")
				ENDIF
			
			ENDIF
			
		Endif
	ENDSCAN
	uf_perguntalt_chama("DOCUMENTO(S) FECHADO(S) COM SUCESSO!","OK","",64)
	uf_fechoautomaticodoc_sair()
ENDFUNC


**
FUNCTION uf_fechoautomaticodoc_sair
	**Fecha Cursores
	FECHOAUTOMATICODOC.hide
	FECHOAUTOMATICODOC.release
	RELEASE FECHOAUTOMATICODOC
ENDFUNC
