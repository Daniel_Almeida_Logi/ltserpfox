**
FUNCTION uf_prepararEncomenda_chama
	PUBLIC myencGrupo, myEncConjunta
	STORE .f. TO myencGrupo
	STORE .f. TO myEncConjunta
	
	IF !uf_gerais_validaPermUser(ch_userno,ch_grupo,'Prepara��o de encomendas')
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ACEDER AO PAINEL DE PREPARA��O DE ENCOMENDAS.","OK","",48)
		RETURN .f.
	ENDIF
	
	IF TYPE("PREPARARENCOMENDA")=="U"
	
		IF !USED("ucrsHoraEnc")
			CREATE CURSOR ucrsHoraEnc (hora c(5))
			SELECT ucrsHoraEnc 
			FOR i = 0 TO 23
				APPEND BLANK
				REPLACE ucrsHoraEnc.hora WITH REPLICATE('0',2-Len(Alltrim(Str(i))))+Alltrim(STR(i))+":00"
				SELECT ucrsHoraEnc 
				APPEND BLANK
				REPLACE ucrsHoraEnc.hora WITH REPLICATE('0',2-Len(Alltrim(Str(i))))+Alltrim(STR(i))+":30"
				IF i=23
					APPEND BLANK
					REPLACE ucrsHoraEnc.hora WITH REPLICATE('0',2-Len(Alltrim(Str(i))))+Alltrim(STR(i))+":59"
				ENDIF 
			ENDFOR
			SELECT ucrsHoraEnc
		ENDIF
		
		IF USED("uCrsEncSave")
			fecha("uCrsEncSave")
		ENDIF
		
		TEXT TO lcSql NOSHOW TEXTMERGE 
			exec up_documentos_EncSave
		ENDTEXT 
		If !uf_gerais_actgrelha("", "uCrsEncSave", lcSql)
			uf_perguntalt_chama("N�o foi possivel determinar Encomendas Pendentes.","OK","",16)
		ENDIF

		IF !USED("uCrsConfiguracaoSedeGrupo")
			TEXT TO lcSql NOSHOW TEXTMERGE 
				select bdName, odbc from condComercConfig where sede = 1
			ENDTEXT 
			If !uf_gerais_actgrelha("", "uCrsConfiguracaoSedeGrupo", lcSql)
				uf_perguntalt_chama("N�o foi possivel determinar a Sede de Grupo.","OK","",16)
			ENDIF
		ENDIF
		
		DO FORM PREPARARENCOMENDA
		PREPARARENCOMENDA.show
	ELSE
		PREPARARENCOMENDA.show
	ENDIF
	
	
	IF !EMPTY(uf_gerais_getParameter("ADM0000000240","NUM")) == .t. && Usa Encomendas de Grupo
		PREPARARENCOMENDA.menu1.encGrupo.tag = IIF(uf_gerais_getParameter("ADM0000000240","BOOL"),'true','false')
		PREPARARENCOMENDA.menu1.estado("encGrupo", "SHOW")
	ELSE
		PREPARARENCOMENDA.menu1.estado("encGrupo", "HIDE")
	ENDIF 
	
		
	IF USED("ucrsConfEmpresas")
		fecha("ucrsConfEmpresas")
	ENDIF

	uf_prepararencomenda_selLojas()

ENDFUNC




FUNCTION uf_prepararencomenda_selLojas



	IF !USED("ucrsConfEmpresas")
		TEXT To lcSQL TEXTMERGE NOSHOW
			exec up_gerais_trocaEmpresa <<ch_userno>>,'<<ALLTRIM(ch_grupo)>>'
		ENDTEXT 
		uf_gerais_actGrelha("","ucrsListSiteAux",lcSQL)
		SELECT .f. as selEmpresa,* FROM ucrsListSiteAux INTO CURSOR ucrsConfEmpresas READWRITE 
		UPDATE ucrsConfEmpresas SET selEmpresa = .T. WHERE siteno = mySite_nr
	ENDIF 
	

	IF( uf_gerais_getParameter("ADM0000000193","BOOL"))
		IF USED("ucrsConfEmpresas")
			UPDATE ucrsConfEmpresas SET selEmpresa = .T. 
		ENDIF 
	ENDIF
	
	uf_prepararencomenda_valida_conjuntaUI(uf_prepararencomenda_valida_conjunta())
			
	
	

ENDFUNC

FUNCTION uf_prepararencomenda_valida_conjuntaUI
 LPARAMETERS lcConjunta
 


 if(!EMPTY(lcConjunta))
 	PREPARARENCOMENDA.menu1.conjunta.tag =  'true' 	
	myEncConjunta = .t.	
		&& altera o botao
	PREPARARENCOMENDA.menu1.conjunta.config("Multiloja", myPath + "\imagens\icons\checked_w.png", "uf_prepararencomenda_conjunta","F1")
 ELSE
 	&& aplica sele��o
	PREPARARENCOMENDA.menu1.conjunta.tag = 'false'		
	myEncConjunta = .f.		
	&& altera o botao
	PREPARARENCOMENDA.menu1.conjunta.config("Loja Atual", myPath + "\imagens\icons\unchecked_w.png", "uf_prepararencomenda_conjunta","F1")		
 ENDIF
 
 
ENDFUNC
 




FUNCTION uf_prepararencomenda_valida_conjunta

	LOCAL lcValidaConjunta 
	lcValidaConjunta  = .t.
	

	if(USED("ucrsConfEmpresas"))
	
	 	fecha("ucrsConfEmpresasTemp ")		
		
		** valida se apenas est� selecionada a actual, n�o � conjunta
		SELECT * FROM  ucrsConfEmpresas WHERE selempresa =.T. INTO CURSOR ucrsConfEmpresasTemp READWRITE
		
		if(RECCOUNT("ucrsConfEmpresasTemp") = 0)
			lcValidaConjunta   = .f.
		endif

		
		if(RECCOUNT("ucrsConfEmpresasTemp") == 1)
		
			SELECT ucrsConfEmpresas
			GO top
			SCAN  
				IF ucrsConfEmpresas.siteno=mySite_nr AND selempresa =.T. then
					lcValidaConjunta   = .f.
				ENDIF 
			ENDSCAN 
		
		ENDIF
				
	ENDIF
	
	fecha("ucrsConfEmpresasTemp")

	
	RETURN lcValidaConjunta  
	

ENDFUNC


FUNCTION uf_prepararencomenda_conjunta

	uf_prepararencomenda_valida_conjuntaUI(uf_prepararencomenda_valida_conjunta())


		IF vartype(ucrsEmpresasSel)=="U" THEN 
			** Listam de Empresas
			IF USED("ucrsConfEmpresas")
				SELECT * FROM ucrsConfEmpresas INTO CURSOR ucrsConfEmpresasAux READWRITE 
			ENDIF 
			
			IF !USED("ucrsConfEmpresas")
				TEXT To lcSQL TEXTMERGE NOSHOW
					exec up_gerais_trocaEmpresa <<ch_userno>>,'<<ALLTRIM(ch_grupo)>>'
				ENDTEXT 
				uf_gerais_actGrelha("","ucrsListSiteAux",lcSQL)
				SELECT .f. as selEmpresa,* FROM ucrsListSiteAux INTO CURSOR ucrsConfEmpresas READWRITE 
			ENDIF 
			
			
			IF USED("ucrsConfEmpresasAux")	
				SELECT ucrsConfEmpresas
				GO TOP
				SCAN 
					SELECT ucrsConfEmpresasAux	
					LOCATE FOR ALLTRIM(UPPER(ucrsConfEmpresas.local)) == ALLTRIM(UPPER(ucrsConfEmpresasAux.local))
					IF FOUND()
						Replace ucrsConfEmpresas.selEmpresa WITH ucrsConfEmpresasAux.selEmpresa
					ENDIF 
				ENDSCAN 
				
			ENDIF 
			
			uf_valorescombo_chama("ucrsConfEmpresas", 6, "ucrsConfEmpresas", 2, "LOCAL", "LOCAL",.t.,.f.,.t.,.t.)
		ELSE
			uf_valorescombo_chama("ucrsConfEmpresas", 6, "ucrsConfEmpresas", 3, "LOCAL", "LOCAL",.t.,.f.,.t.,.t.)
		ENDIF 

	
	
	uf_prepararencomenda_valida_conjuntaUI(uf_prepararencomenda_valida_conjunta())
	
ENDFUNC

**
FUNCTION uf_prepararencomenda_di
	uf_getdate_chama(.f., "PREPARARENCOMENDA.di", 1)
ENDFUNC

**
FUNCTION uf_prepararencomenda_df
	uf_getdate_chama(.f., "PREPARARENCOMENDA.df", 1)
ENDFUNC


**
FUNCTION uf_prepararencomenda_encAutoDelSave
	IF USED("uCrsEncSave")
		IF !uf_perguntalt_chama("ATEN��O: VAI APAGAR A PREPARA��O DE ENCOMENDA." + CHR(10) + "Criada em: " + alltrim(uCrsEncSave.data) + chr(10) + "Alterada em: " + alltrim(uCrsEncSave.udata) + chr(10) + chr(10)+"TEM A CERTEZA QUE PRENTENDE CONTINUAR?","Sim","N�o")
			SELECT uCrsEncSave
			replace uCrsEncSave.del WITH .f.
			PREPARARENCOMENDA.gridPesq.refresh
			RETURN .f.
		ENDIF
		
		SELECT uCrsEncSave
		TEXT TO lcSql NOSHOW textmerge
			DELETE
				B_encAutoSave
			where
				stamp='<<ALLTRIM(uCrsEncSave.stamp)>>'
		ENDTEXT
		IF !uf_gerais_actgrelha("", "", lcSql)
			uf_perguntalt_chama("OCORREU UM PROBLEMA A ELIMINAR A ENCOMENDA GUARDADA. POR FAVOR CONTACTE O SUPROTE.","OK","", 16)
		ELSE
			SELECT uCrsEncSave
			DELETE
			SELECT uCrsEncSave
			GO Top
			PREPARARENCOMENDA.gridPesq.refresh
			
			uf_perguntalt_chama("PREPARA��O ELIMINADA COM SUCESSO.","OK","",64)
		ENDIF
	ENDIF
ENDFUNC


**
FUNCTION uf_prepararencomenda_encAutoGestao
	** ENCOMENDA COM BASE NA GEST�O DE STOCKS **
	Local lcSql
	lcSql=""
	
	regua(0,100,"A PROCESSAR ENCOMENDA COM BASE NA GEST�O DE STOCKS...",.f.)
	
	**
	LOCAL lcIDSedeGrupo
	lcIDSedeGrupo = ""
	
	IF PREPARARENCOMENDA.menu1.encGrupo.tag == "true"
		SELECT uCrsConfiguracaoSedeGrupo
		lcIDSedeGrupo = ALLTRIM(uCrsConfiguracaoSedeGrupo.bdname)
		
		myencGrupo = .t.
	ENDIF
	
	IF PREPARARENCOMENDA.menu1.conjunta.tag == "true"
		LOCAL lcEmpSel
		STORE '' TO lcEmpSel
		SELECT ucrsConfEmpresas
		GO TOP 
		SCAN 
			IF ucrsConfEmpresas.selempresa=.t.
				IF EMPTY(lcEmpSel)
					lcEmpSel = ALLTRIM(STR(ucrsConfEmpresas.armazem1))
				ELSE
					lcEmpSel = lcEmpSel + ',' + ALLTRIM(STR(ucrsConfEmpresas.siteno))
				ENDIF 
			ENDIF 
		ENDSCAN 
		IF EMPTY(lcEmpSel)
			uf_perguntalt_chama("N�O ESCOLHEU NENHUMA EMPRESA PARA EMITIR. POR FAVOR VERIFIQUE.","OK","",48)
			regua(2)
			RETURN .f.
		ENDIF 
		
		Text To lcSql Noshow textmerge
			exec up_enc_auto_multiemp '<<ALLTRIM(lcEmpSel)>>','','<<ALLTRIM(lcIDSedeGrupo)>>',<<mysite_nr>>,<<IIF(myEncConjunta,1,0)>>
		ENDTEXT
	ELSE
		Text To lcSql Noshow textmerge
			exec up_enc_auto <<IIF(PREPARARENCOMENDA.menu1.conjunta.tag == "true",0,myArmazem)>>,'','<<ALLTRIM(lcIDSedeGrupo)>>',<<mysite_nr>>,<<IIF(myEncConjunta,1,0)>>
		ENDTEXT
	ENDIF 
	
*!*		lcSQL = ''
*!*		TEXT TO lcSql NOSHOW TEXTMERGE 
*!*			exec up_enc_auto <<IIF(PREPARARENCOMENDA.menu1.conjunta.tag == "true",0,myArmazem)>>,'','<<ALLTRIM(lcIDSedeGrupo)>>',<<mysite_nr>>,<<IIF(myEncConjunta,1,0)>>
*!*		ENDTEXT
	

	IF !uf_gerais_actgrelha("", "CRSenc", lcSQL)
		uf_perguntalt_chama("OCORREU UM ERRO A GERAR LISTAGEM DE ENCOMENDA. POR FAVOR CONTACTE O SUPORTE.","OK","", 16)
		regua(2)
		RETURN .f.
	ELSE
		IF RECCOUNT()>0
			GO TOP IN CRSenc
		ENDIF
	ENDIF
	
	regua(2)	
	uf_encautomaticas_chama("AUTO",.f.,IIF(PREPARARENCOMENDA.menu1.conjunta.tag="true",.t.,.f.))
ENDFUNC



**
Function uf_prepararencomenda_encAutoReforco
	** ENCOMENDA DE REFOR�O **
	Local lcSql
	lcSql=""
	
	regua(0,100,"A PROCESSAR ENCOMENDA DE REFOR�O...",.f.)
	
	**
	LOCAL lcIDSedeGrupo
	lcIDSedeGrupo = ""
	
	IF PREPARARENCOMENDA.menu1.encGrupo.tag == "true"
		SELECT uCrsConfiguracaoSedeGrupo
		lcIDSedeGrupo = ALLTRIM(uCrsConfiguracaoSedeGrupo.bdname)
		
		myencGrupo = .t.
	ENDIF
	
	IF PREPARARENCOMENDA.menu1.conjunta.tag == "true"
		LOCAL lcEmpSel
		STORE '' TO lcEmpSel
		SELECT ucrsConfEmpresas
		GO TOP 
		SCAN 
			IF ucrsConfEmpresas.selempresa=.t.
				IF EMPTY(lcEmpSel)
					lcEmpSel = ALLTRIM(STR(ucrsConfEmpresas.armazem1))
				ELSE
					lcEmpSel = lcEmpSel + ',' + ALLTRIM(STR(ucrsConfEmpresas.siteno))
				ENDIF 
			ENDIF 
		ENDSCAN 
		IF EMPTY(lcEmpSel)
			uf_perguntalt_chama("N�O ESCOLHEU NENHUMA EMPRESA PARA EMITIR. POR FAVOR VERIFIQUE.","OK","",48)
			regua(2)
			RETURN .f.
		ENDIF 
		
		Text To lcSql Noshow textmerge
			exec up_enc_reforco_multiemp '<<ALLTRIM(lcEmpSel)>>',<<IIF(EMPTY(PREPARARENCOMENDA.t.value), 0,PREPARARENCOMENDA.t.value))>>, '<<uf_gerais_getdate(PREPARARENCOMENDA.di.value, "SQL")>>', '<<uf_gerais_getdate(PREPARARENCOMENDA.df.value, "SQL")>>', '<<Alltrim(PREPARARENCOMENDA.hi.value)>>', '<<Alltrim(PREPARARENCOMENDA.hf.value)>>','<<ALLTRIM(lcIDSedeGrupo)>>',<<mysite_nr>>,<<IIF(myEncConjunta,1,0)>>
		ENDTEXT
	ELSE
		Text To lcSql Noshow textmerge
			exec up_enc_reforco <<IIF(PREPARARENCOMENDA.menu1.conjunta.tag == "true",0,myArmazem)>>,<<IIF(EMPTY(PREPARARENCOMENDA.t.value), 0,PREPARARENCOMENDA.t.value))>>, '<<uf_gerais_getdate(PREPARARENCOMENDA.di.value, "SQL")>>', '<<uf_gerais_getdate(PREPARARENCOMENDA.df.value, "SQL")>>', '<<Alltrim(PREPARARENCOMENDA.hi.value)>>', '<<Alltrim(PREPARARENCOMENDA.hf.value)>>','<<ALLTRIM(lcIDSedeGrupo)>>',<<mysite_nr>>,<<IIF(myEncConjunta,1,0)>>
		ENDTEXT
	ENDIF 
	
	
*!*		
*!*		Text To lcSql Noshow textmerge
*!*			exec up_enc_reforco <<IIF(PREPARARENCOMENDA.menu1.conjunta.tag == "true",0,myArmazem)>>,<<IIF(EMPTY(PREPARARENCOMENDA.t.value), 0,PREPARARENCOMENDA.t.value))>>, '<<uf_gerais_getdate(PREPARARENCOMENDA.di.value, "SQL")>>', '<<uf_gerais_getdate(PREPARARENCOMENDA.df.value, "SQL")>>', '<<Alltrim(PREPARARENCOMENDA.hi.value)>>', '<<Alltrim(PREPARARENCOMENDA.hf.value)>>','<<ALLTRIM(lcIDSedeGrupo)>>',<<mysite_nr>>,<<IIF(myEncConjunta,1,0)>>
*!*		ENDTEXT

	
	If !uf_gerais_actgrelha("", "CRSenc", lcSql)
		uf_perguntalt_chama("OCORREU UM ERRO A GERAR SUGEST�O DE ENCOMENDA."+Chr(13)+Chr(10)+Chr(13)+Chr(10)+"POR FAVOR TENTE REINICIAR O SOFTWARE. SE O PROBLEMA PERSISTIR CONTACTE O SUPORTE.","OK","",16)
		regua(2)
		return .f.
	Else
		If Reccount()>0
			Go top in CRSenc
		ENDIF
	ENDIF

	regua(2)
	
	uf_encautomaticas_chama("REFORCO",.f.,IIF(PREPARARENCOMENDA.menu1.conjunta.tag="true",.t.,.f.))
EndFunc



** ENCOMENDA DE REFOR�O POR FORNECEDOR
FUNCTION  uf_prepararencomenda_encAutoReforcoFornec
	LOCAL lcSql
	lcSql=""
	
	regua(0,100,"A PROCESSAR ENCOMENDA DE REFOR�O POR FORNECEDOR...",.f.)
	
	&&
	IF EMPTY(PREPARARENCOMENDA.no.value)
		uf_perguntalt_chama("DEVE PRIMEIRO SELECCIONAR O FORNECEDOR. POR FAVOR VERIFIQUE.","OK","",48)
		regua(2)
		RETURN .f.
	ENDIF

	&&
	LOCAL lcIDSedeGrupo
	lcIDSedeGrupo = ""
	
	IF PREPARARENCOMENDA.menu1.encGrupo.tag == "true"
		SELECT uCrsConfiguracaoSedeGrupo
		lcIDSedeGrupo = ALLTRIM(uCrsConfiguracaoSedeGrupo.bdname)
		myencGrupo = .t.
	ENDIF
	
	IF PREPARARENCOMENDA.menu1.conjunta.tag == "true"
		LOCAL lcEmpSel
		STORE '' TO lcEmpSel
		SELECT ucrsConfEmpresas
		GO TOP 
		SCAN 
			IF ucrsConfEmpresas.selempresa=.t.
				IF EMPTY(lcEmpSel)
					lcEmpSel = ALLTRIM(STR(ucrsConfEmpresas.armazem1))
				ELSE
					lcEmpSel = lcEmpSel + ',' + ALLTRIM(STR(ucrsConfEmpresas.siteno))
				ENDIF 
			ENDIF 
		ENDSCAN 
		IF EMPTY(lcEmpSel)
			uf_perguntalt_chama("N�O ESCOLHEU NENHUMA EMPRESA PARA EMITIR. POR FAVOR VERIFIQUE.","OK","",48)
			regua(2)
			RETURN .f.
		ENDIF 
		
		Text To lcSql Noshow textmerge
			exec up_enc_reforcoForn_multiemp '<<ALLTRIM(lcEmpSel)>>',<<PREPARARENCOMENDA.no.value>>,'<<ALLTRIM(lcIDSedeGrupo)>>',<<mysite_nr>>,<<IIF(myEncConjunta,1,0)>>
		ENDTEXT
	ELSE
		Text To lcSql Noshow textmerge
			exec up_enc_reforcoForn <<myArmazem>>,<<PREPARARENCOMENDA.no.value>>,'<<ALLTRIM(lcIDSedeGrupo)>>',<<mysite_nr>>,<<IIF(myEncConjunta,1,0)>>
		ENDTEXT
	ENDIF 
*!*		Text To lcSql Noshow textmerge
*!*			exec up_enc_reforcoForn <<IIF(PREPARARENCOMENDA.menu1.conjunta.tag == "true",0,myArmazem)>>,<<PREPARARENCOMENDA.no.value>>,'<<ALLTRIM(lcIDSedeGrupo)>>',<<mysite_nr>>,<<IIF(myEncConjunta,1,0)>>
*!*		ENDTEXT




	If !uf_gerais_actgrelha("", [CRSenc], lcSql)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA A GERAR SUGEST�O DE ENCOMENDA."+Chr(13)+Chr(10)+Chr(13)+Chr(10)+"POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		regua(2)
		return .f.
	ELSE
		If Reccount()>0
			Go top in CRSenc
		Endif
	ENDIF
	regua(2)
	
	
	uf_encautomaticas_chama("FORNECEDOR",.f.,IIF(PREPARARENCOMENDA.menu1.conjunta.tag="true",.t.,.f.))
EndFunc


**
FUNCTION uf_prepararencomenda_encAutoReservas
	** ENCOMENDA DE RESERVAS DE CLIENTES **
	
	Local lcSql
	lcSql=""
	
	regua(0,100,"A PROCESSAR ENCOMENDA DE RESERVAS DE CLIENTES...",.f.)
	
	**
	LOCAL lcIDSedeGrupo
	lcIDSedeGrupo = ""
	
	IF PREPARARENCOMENDA.menu1.encGrupo.tag == "true"
		SELECT uCrsConfiguracaoSedeGrupo
		lcIDSedeGrupo = ALLTRIM(uCrsConfiguracaoSedeGrupo.bdname)
		myencGrupo = .t.
	ENDIF
	
	IF PREPARARENCOMENDA.menu1.conjunta.tag == "true"
		LOCAL lcEmpSel
		STORE '' TO lcEmpSel
		SELECT ucrsConfEmpresas
		GO TOP 
		SCAN 
			IF ucrsConfEmpresas.selempresa=.t.
				IF EMPTY(lcEmpSel)
					lcEmpSel = ALLTRIM(STR(ucrsConfEmpresas.armazem1))
				ELSE
					lcEmpSel = lcEmpSel + ',' + ALLTRIM(STR(ucrsConfEmpresas.siteno))
				ENDIF 
			ENDIF 
		ENDSCAN 
		IF EMPTY(lcEmpSel)
			uf_perguntalt_chama("N�O ESCOLHEU NENHUMA EMPRESA PARA EMITIR. POR FAVOR VERIFIQUE.","OK","",48)
			regua(2)
			RETURN .f.
		ENDIF 
		
	
		
		Text To lcSql Noshow textmerge
				exec up_enc_autoReservas_multiemp '<<ALLTRIM(lcEmpSel)>>','<<ALLTRIM(lcIDSedeGrupo)>>',<<mysite_nr>>,<<IIF(myEncConjunta,1,0)>>
		ENDTEXT
	ELSE
		Text To lcSql Noshow textmerge
			exec up_enc_autoReservas <<IIF(PREPARARENCOMENDA.menu1.conjunta.tag == "true",0,myArmazem)>>,'<<ALLTRIM(lcIDSedeGrupo)>>',<<mysite_nr>>,<<IIF(myEncConjunta,1,0)>>
		ENDTEXT
	ENDIF 
	
*!*		Text To lcSql Noshow textmerge
*!*			exec up_enc_autoReservas <<IIF(PREPARARENCOMENDA.menu1.conjunta.tag == "true",0,myArmazem)>>,'<<ALLTRIM(lcIDSedeGrupo)>>',<<mysite_nr>>,<<IIF(myEncConjunta,1,0)>>
*!*		ENDTEXT


	
	If !uf_gerais_actgrelha("", [CRSenc], lcSql)
		uf_perguntalt_chama("OCORREU UM ERRO A GERAR SUGEST�O DE ENCOMENDA."+Chr(13)+Chr(10)+Chr(13)+Chr(10)+"POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		regua(2)
		return .f.
	ELSE
		If Reccount()>0
			Go top in CRSenc
		Endif
	ENDIF
	
	regua(2)
	uf_encautomaticas_chama("RESERVAS",.f.,IIF(PREPARARENCOMENDA.menu1.conjunta.tag="true",.t.,.f.))


ENDFUNC



**
FUNCTION uf_prepararencomenda_encGuardada
	** ENCOMENDA GUARDADA **
	Local lcSql
	lcSql=""

	IF used("uCrsEncSave")
		SELECT uCrsEncSave
		
		Text To lcSql Noshow textmerge
			exec up_enc_getSave '<<ALLTRIM(uCrsEncSave.stamp)>>', <<mysite_nr>>, <<IIF(uf_gerais_compStr("MANUAL", uCrsEncSave.tipoenc), '1', '0')>>
		Endtext
		IF !uf_gerais_actgrelha("", [CRSenc], lcSql)
			uf_perguntalt_chama("OCORREU UM ERRO A GERAR SUGEST�O DE ENCOMENDA. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			return .f.
		ELSE
			If Reccount()>0
				Go top in CRSenc
			Endif
		ENDIF
		
		uf_encautomaticas_chama(uCrsEncSave.tipoenc, uCrsEncSave.stamp,IIF(PREPARARENCOMENDA.menu1.conjunta.tag="true",.t.,.f.))
	ENDIF
ENDFUNC




**
FUNCTION uf_prepararencomenda_sair
	IF USED("uCrsEncSave")
		RELEASE uCrsEncSave
	ENDIF
	
	
	PREPARARENCOMENDA.hide
	PREPARARENCOMENDA.release
ENDFUNC



**
FUNCTION uf_prepararencomenda_alertaMelhorBB
	LPARAMETERS lcRef

	IF !USED("ucrsListaMelhoresBB")
		**Verificar alterta maior BB
		lcSQL = ""
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			exec up_enc_5maisBBTodos <<mysite_nr>>
		ENDTEXT
		If !uf_gerais_actgrelha("", "ucrsListaMelhoresBB", lcSQL)
			uf_perguntalt_chama("OCORREU UM ERRO NA VERIFICA��O DO BB. AO GERAR LISTAGEM DE ENCOMENDA. POR FAVOR CONTACTE O SUPORTE.","OK","", 16)
			return .f.
		ENDIF
	ENDIF
	
	IF !USED("crsenc")
		RETURN .f.
	ENDIF 
	
	SELECT crsenc
	GO TOP 
	SCAN
		SELECT ucrsListaMelhoresBB
		LOCATE FOR ALLTRIM(crsenc.grphmgcode) == ALLTRIM(ucrsListaMelhoresBB.grphmgcode) 
		IF FOUND()
			IF crsenc.marg4 < ucrsListaMelhoresBB.marg4
				Replace crsenc.alertaStockGH WITH 1
			ELSE	
				Replace crsenc.alertaStockGH WITH 0
			ENDIF
		ENDIF
	ENDSCAN
	
	IF USED("ucrsListaMelhoresBB")
		fecha("ucrsListaMelhoresBB")
	ENDIF 
ENDFUNC


**
FUNCTION uf_prepararencomenda_alertaEsgotados
	LPARAMETERS lcRef
	
	IF !USED("ucrsListaEsgotados")
		**Verificar alterta maior BB
		lcSQL = ""
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			exec up_enc_FonecedorEsgotados
		ENDTEXT
		If !uf_gerais_actgrelha("", "ucrsListaEsgotados", lcSQL)
			uf_perguntalt_chama("OCORREU UM ERRO NA VERIFICA��O DO ULTIMO PCL. DO FORNECEDOR. POR FAVOR CONTACTE O SUPORTE.","OK","", 16)
			return .f.
		ENDIF
	ENDIF
	
	IF !USED("crsenc")
		RETURN .f.
	ENDIF 
	
	SELECT crsenc
	GO TOP 
	SCAN
		SELECT ucrsListaEsgotados
		LOCATE FOR ALLTRIM(crsenc.ref) == ALLTRIM(ucrsListaEsgotados.ref) 
		IF FOUND()
			Replace crsenc.Esgotado WITH 'SIM'
		ELSE	
			Replace crsenc.Esgotado WITH 'N�O'
		ENDIF
	ENDSCAN
	
	IF USED("ucrsListaEsgotados")
		fecha("ucrsListaEsgotados")
	ENDIF 
ENDFUNC


**
FUNCTION uf_prepararencomenda_alertaFornecedoresComBonus
	LPARAMETERS lcRef
	
	IF !USED("ucrsListaFornecedoresComBonus")
		**Verificar alterta maior BB
		lcSQL = ""
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			 exec up_enc_BonusFornecedores
		ENDTEXT
		If !uf_gerais_actgrelha("", "ucrsListaFornecedoresComBonus", lcSQL)
			uf_perguntalt_chama("OCORREU UM ERRO NA VERIFICA��O DOS BONUS DO FORNECEDOR. POR FAVOR CONTACTE O SUPORTE.","OK","", 16)
			return .f.
		ENDIF
	ENDIF
	
	IF !USED("crsenc")
		RETURN .f.
	ENDIF 
	
	SELECT crsenc
	GO TOP 
	SCAN
		SELECT TOP 1 bonus FROM ucrsListaFornecedoresComBonus where crsenc.fornec = ucrsListaFornecedoresComBonus.no and crsenc.fornestab = ucrsListaFornecedoresComBonus.estab and crsenc.ref = ucrsListaFornecedoresComBonus.ref ORDER BY ref INTO CURSOR ucrsTestaBonusActual
		
		SELECT ucrsTestaBonusActual
		SELECT crsenc
		Replace crsenc.bonusfornec WITH ucrsTestaBonusActual.bonus
	
		SELECT COUNT(bonus) as nrBonusOutros FROM ucrsListaFornecedoresComBonus where (crsenc.fornec != ucrsListaFornecedoresComBonus.no or crsenc.fornestab != ucrsListaFornecedoresComBonus.estab) and crsenc.ref = ucrsListaFornecedoresComBonus.ref ORDER BY ref INTO CURSOR ucrsTestaBonusOutrosFornec	
			
		SELECT ucrsTestaBonusOutrosFornec
		IF  EMPTY(ALLTRIM(crsenc.bonusfornec)) AND ucrsTestaBonusOutrosFornec.nrBonusOutros > 0
			SELECT crsenc
			Replace crsenc.alertaBonus WITH 1
		ELSE
			SELECT crsenc
			Replace crsenc.alertaBonus WITH 0			
		ENDIF
	ENDSCAN
	
	IF USED("ucrsListaFornecedoresComBonus")
		fecha("ucrsListaFornecedoresComBonus")
	ENDIF 
	IF USED("ucrsTestaBonusOutrosFornec")
		fecha("ucrsTestaBonusOutrosFornec")
	ENDIF 
ENDFUNC



**
Function uf_prepararencomenda_keiretsu
	Local lcSql
		
	prepararencomenda.tipoutente = ""
	
	** As encomendas a considerar s�o as do grupo de clientes especificado e com estado aberto
	IF !used("ucrsTiposUtentesEncomenda")
	
		Text To lcSql Noshow textmerge
			exec up_utentes_TiposUtentes
		ENDTEXT

		If !uf_gerais_actgrelha("", "ucrsTiposUtentesEncomenda", lcSql)
			uf_perguntalt_chama("OCORREU UM ERRO A VERIFICAR TIPOS DE UTENTES.","OK","",16)
			return .f.
		ENDIF
	ENDIF
	uf_valorescombo_chama("prepararencomenda.tipoutente", 5, "ucrsTiposUtentesEncomenda", 2, "tipo", "tipo", .t.)
	
	
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		select 
			distinct 
			sel	= convert(bit,0)
			,convert(nvarchar(3),armazem) as armazem 
			,armazem as arm2
		from 
			empresa
			inner join empresa_arm on empresa.no = empresa_arm.empresa_no
		order by 
			arm2
	ENDTEXT
	uf_valorescombo_chama("prepararencomenda.armazem", 5, lcSQL, 1, "armazem", "armazem",.f.)

	IF EMPTY(prepararencomenda.armazem)
		uf_perguntalt_chama("ARMAZ�M N�O ESPECIFICADO.","OK","",16)
		return .f.
	ENDIF
	
	IF !uf_perguntalt_chama("A sugest�o de Encomenda vai ser calculada com base em: ";
		+ CHR(13) ;
		+ CHR(13) + " . Tipo Documento: Encomendas de Cliente" ;
		+ CHR(13) + " . Estado Documento: Aberto" ;
		+ CHR(13) + " . Tipo Utente: Sele��o";
		+ CHR(13) + " . Armaz�m: " + LEFT(ALLTRIM(IIF(EMPTY(prepararencomenda.armazem),"",prepararencomenda.armazem)),30);
		+ CHR(13) +"Pretende continuar?","Sim","N�o",32)
		return .f.
	ENDIF
	
	lcSql=""
	regua(0,100,"A PROCESSAR ENCOMENDA KEIRETSU...",.f.)
	
	Text To lcSql NOSHOW TEXTMERGE
		exec up_enc_encomendasClientes '<<IIF(EMPTY(prepararencomenda.tipoutente),'',prepararencomenda.tipoutente)>>', <<IIF(EMPTY(prepararencomenda.armazem),'',prepararencomenda.armazem)>>,<<mysite_nr>>
	ENDTEXT
	
	

	If !uf_gerais_actgrelha("", [CRSenc], lcSql)
		uf_perguntalt_chama("OCORREU UM ERRO A GERAR SUGEST�O DE ENCOMENDA."+Chr(13)+Chr(10)+Chr(13)+Chr(10)+"POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		regua(2)
		return .f.
	ELSE
		If Reccount()>0
			Go top in CRSenc
		Endif
	ENDIF
	
	regua(2)
	uf_encautomaticas_chama("KEIRETSU",.f.,IIF(PREPARARENCOMENDA.menu1.conjunta.tag="true",.t.,.f.))
	
ENDFUNC


**
FUNCTION uf_prepararencomenda_encGrupo
	IF PREPARARENCOMENDA.menu1.encGrupo.tag == 'false'
		&& aplica sele��o
		PREPARARENCOMENDA.menu1.encGrupo.tag = 'true'
				
		&& altera o botao
		PREPARARENCOMENDA.menu1.encGrupo.config("Enc.Grupo", myPath + "\imagens\icons\checked_w.png", "uf_prepararencomenda_encGrupo","F2")
	ELSE
		&& aplica sele��o
		PREPARARENCOMENDA.menu1.encGrupo.tag = 'false'
		
		&& altera o botao
		PREPARARENCOMENDA.menu1.encGrupo.config("Enc.Grupo", myPath + "\imagens\icons\unchecked_w.png", "uf_prepararencomenda_encGrupo","F2")
	ENDIF
ENDFUNC 


**
FUNCTION uf_prepararencomenda_consolidacaoCompras

	**
	uf_prepararencomenda_sair()
	uf_consolidacaocompras_chama()

ENDFUNC 


**
FUNCTION uf_forn_novaComunicacao

ENDFUNC 

**
FUNCTION uf_forn_ApagaComunicacao

ENDFUNC 


**
FUNCTION uf_prepararencomenda_encAutoGestaoSemReservas
	** ENCOMENDA COM BASE NA GEST�O DE STOCKS **
	Local lcSql
	lcSql=""
	
	regua(0,100,"A PROCESSAR ENCOMENDA COM BASE NA GEST�O DE STOCKS...",.f.)
	
	**
	LOCAL lcIDSedeGrupo
	lcIDSedeGrupo = ""
	
	IF PREPARARENCOMENDA.menu1.encGrupo.tag == "true"
		SELECT uCrsConfiguracaoSedeGrupo
		lcIDSedeGrupo = ALLTRIM(uCrsConfiguracaoSedeGrupo.bdname)
		
		myencGrupo = .t.
	ENDIF
	
	IF PREPARARENCOMENDA.menu1.conjunta.tag == "true"
		LOCAL lcEmpSel
		STORE '' TO lcEmpSel
		SELECT ucrsConfEmpresas
		GO TOP 
		SCAN 
			IF ucrsConfEmpresas.selempresa=.t.
				IF EMPTY(lcEmpSel)
					lcEmpSel = ALLTRIM(STR(ucrsConfEmpresas.armazem1))
				ELSE
					lcEmpSel = lcEmpSel + ',' + ALLTRIM(STR(ucrsConfEmpresas.siteno))
				ENDIF 
			ENDIF 
		ENDSCAN 
		IF EMPTY(lcEmpSel)
			uf_perguntalt_chama("N�O ESCOLHEU NENHUMA EMPRESA PARA EMITIR. POR FAVOR VERIFIQUE.","OK","",48)
			regua(2)
			RETURN .f.
		ENDIF 
		
		Text To lcSql Noshow textmerge
			exec up_enc_autoSemReservaClientes_multiemp '<<ALLTRIM(lcEmpSel)>>','','<<ALLTRIM(lcIDSedeGrupo)>>',<<mysite_nr>>,<<IIF(myEncConjunta,1,0)>>
		ENDTEXT
	ELSE
		lcSQL = ''
		TEXT TO lcSql NOSHOW TEXTMERGE 
			exec up_enc_autoSemReservaClientes <<IIF(PREPARARENCOMENDA.menu1.conjunta.tag == "true",0,myArmazem)>>,'','<<ALLTRIM(lcIDSedeGrupo)>>',<<mysite_nr>>,<<IIF(myEncConjunta,1,0)>>
		ENDTEXT
	ENDIF 
*!*		lcSQL = ''
*!*		TEXT TO lcSql NOSHOW TEXTMERGE 
*!*			exec up_enc_autoSemReservaClientes <<IIF(PREPARARENCOMENDA.menu1.conjunta.tag == "true",0,myArmazem)>>,'','<<ALLTRIM(lcIDSedeGrupo)>>',<<mysite_nr>>,<<IIF(myEncConjunta,1,0)>>
*!*		ENDTEXT



	IF !uf_gerais_actgrelha("", "CRSenc", lcSQL)
		uf_perguntalt_chama("OCORREU UM ERRO A GERAR LISTAGEM DE ENCOMENDA. POR FAVOR CONTACTE O SUPORTE.","OK","", 16)
		regua(2)
		RETURN .f.
	ELSE
		IF RECCOUNT()>0
			GO TOP IN CRSenc
		ENDIF
	ENDIF
	
	regua(2)	
	uf_encautomaticas_chama("AUTO",.f.,IIF(PREPARARENCOMENDA.menu1.conjunta.tag="true",.t.,.f.))
ENDFUNC

FUNCTION uf_prepararencomenda_encManual

	FECHA("CRSenc")

	CREATE CURSOR CRSenc (PESQUISA L, STSTAMP C(25), ENC L, U_FONTE C(1), REF C(18), REFB C(18), QTTB N(15,3), STOCKGRUPO N(15,3), CODIGO C(40), DESIGN C(100), STOCK I, STMIN N(15,3),; 
							STMAX N(15,3), OSTMAX N(15,3), PTOENC N(12,3), OPTOENC N(12,3), EOQ I, SUG I, QTBONUS I, QTTADIC N(15,3), QTTFOR N(15,3), QTTACIN N(15,3), QTTCLI N(15,3),; 
							FORNECEDOR C(80), FORNEC N(12,0), FORNESTAB N(5,0), BONUSFORNEC C(20), PCLFORNEC N(15,2), SEL L, EPCUSTO N(20,6), EPCPOND N(20,6), EPCULT N(20,6), TABIVA N(7,2),; 
							IVA N(7,2), CPOC N(8,0), FAMILIA C(18), FAMINOME C(60), U_LAB C(150), MARCA C(200), CONVERSAO N(17,7), ESGOTADO C(1), ORDEM I, GENERICO L, COMPONENTE L,;
							GRPHMGCODE C(6), GRPHMGDESCR C(100), PSICO L, BENZO L, STOCKGH N(20,3), MARG4 N(18,3), ALERTASTOCKGH I, ALERTAPCL I, ALERTABONUS I, MELHORBB C(18), GRPHMGCODE1 C(6),; 
							BBFORNEC N(11,2), MBPCFORNEC N(11,2), QTPREVISTA N(11,2), PRIORITARIO L, VV L, QTTRES N(20,4), M3UM N(20,6), FORNECABREV C(80), PVP N(20,6), TIPOPRODUTO C(254),;
							OBS C(254), U_NOTA1 M, PCUSTO N(20,6), descontoForn N(6,2), descontoFornOri N(6,2))


	uf_encautomaticas_chama("MANUAL",.f.,IIF(PREPARARENCOMENDA.menu1.conjunta.tag="true",.t.,.f.))

ENDFUNC