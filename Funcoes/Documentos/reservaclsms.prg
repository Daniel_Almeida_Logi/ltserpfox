**
FUNCTION uf_reservaclsms_chama
	IF TYPE("reservaclsms") == "U"
		DO FORM reservaclsms
		
	ELSE
		reservaclsms.show()
	ENDIF
ENDFUNC

FUNCTION uf_reservaclsms_validaTlmvl
	LPARAMETERS lcTlmvl
	
	&&valida se o telefone � valido	
	IF(LEN(lcTlmvl)!=9)	
		RETURN .F.
	ENDIF
	RETURN .t.	

ENDFUNC



**
FUNCTION uf_reservaclsms_gravar

	&&grava lista de tlm na tabela b_res_notificacoes para depois o java enviar
	LOCAL lcStamp, lcBIstamp,lcBOstamp, lcNomesClientes
	STORE "" TO lcStamp
	lcStamp = uf_gerais_stamp()
		
	** limpar numeros de telemovel
	uf_reservaclsms_limpaNrTelemovel()
	
	
	SELECT uCrsCliSMSEnviar
	GO TOP
	SCAN FOR uCrsCliSMSEnviar.sel == .t.
		IF(!uf_reservaclsms_validaTlmvl(ALLTRIM(uCrsCliSMSEnviar.tlmvl))) or !uf_reservaclsms_verificaPrefixo(ALLTRIM(uCrsCliSMSEnviar.tlmvl))
			select uCrsCliSMSEnviar
			replace uCrsCliSMSEnviar.sel with .f.
		ENDIF
	ENDSCAN
	
	SELECT uCrsCliSMSEnviar
	GO TOP
	SCAN FOR uCrsCliSMSEnviar.sel == .t.

			TEXT TO lcSQL TEXTMERGE NOSHOW 
				INSERT INTO b_res_enviar(
					stamp
					,bistamp
					,obrano
					,dataobra
					,design
					,ref
					,tlmvl
				)
				values(
					'<<lcStamp>>'
					,'<<uCrsCliSMSEnviar.bistamp>>'
					,<<uCrsCliSMSEnviar.obrano>>
					,'<<uf_gerais_getdate(uCrsCliSMSEnviar.dataobra,"SQL")>>'
					,'<<ALLTRIM(uCrsCliSMSEnviar.design)>>'
					,'<<ALLTRIM(uCrsCliSMSEnviar.ref)>>'
					,'<<ALLTRIM(uCrsCliSMSEnviar.tlmvl)>>'
				)
			ENDTEXT 		
			**marca o cursor orginal como imprimido
		
			UPDATE uCrsListaReservas SET sms=.t. WHERE bistamp = ALLTRIM(uCrsCliSMSEnviar.bistamp)
		
			IF !uf_gerais_actGrelha("","",lcSQL)
				uf_perguntalt_chama("Ocorreu um erro a gravar os tlmvl na BD para envio. Os SMS n�o ser�o enviados.","OK","",48)
				RETURN .f.
			ENDIF		
	ENDSCAN

	&&enviar sms
	uf_param_sms_atualiza()
	
	lcWsPath = ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\ltsSMS\LtsSMS.jar reserva "' + ALLTRIM(lcStamp) + '" ' + ALLTRIM(sql_db)
	oWSShell = CREATEOBJECT("WScript.Shell")
	
	oWSShell.Run(lcWsPath,1,.t.)
 
	uf_perguntalt_chama("SMS enviados. Poder� consultar o hist�rico atrav�s da an�lise de SMS enviados no painel de An�lises.","OK","",64)
	
	
	uf_reservaclsms_sair()
ENDFUNC

FUNCTION uf_reservaclsms_selTodos
	LPARAMETERS lcBool
	
	SELECT uCrsCliSMSEnviar
	IF lcBool
		reservaclsms.menu1.selTodos.config("Des. Todos",myPath + "\imagens\icons\checked_w.png","uf_reservaclsms_selTodos with .f.","T")
		replace ALL uCrsCliSMSEnviar.sel WITH .t.
	ELSE
		reservaclsms.menu1.selTodos.config("Sel. Todos",myPath + "\imagens\icons\unchecked_w.png","uf_reservaclsms_selTodos with .t.","T")
		replace ALL uCrsCliSMSEnviar.sel WITH .f.
	ENDIF 
	GO TOP

	reservaclsms.refresh()
ENDFUNC 

FUNCTION uf_reservaclsms_verificaTlmvl
	IF uf_perguntalt_chama("Vai eliminar da lista todos os n� de telem�vel que n�o comecem por 9, +351 ou 00351." + CHR(13) + "Tem a certeza que pretende continuar?","Sim","N�o")
		&& Prepara Regua
		regua(0,3,"A validar telem�veis vazios, n� de telem�vel que n�o comecem por 9, +351 ou 00351 ...",.F.)
		&& incrementa regua
		regua(0,1,"A validar telem�veis vazios,n� de telem�vel que n�o comecem por 9, +351 ou 00351 ...",.F.)
		&&vazios, 9 digitos e comece por 9		
		SELECT uCrsCliSMSEnviar
		SCAN
		    LOCAL lcTlmvl
		    lcTlmvl = ALLTRIM(uCrsCliSMSEnviar.tlmvl)

		    IF EMPTY(lcTlmvl) OR ;
		        (LEFT(lcTlmvl, 1) != "9" AND LEFT(lcTlmvl, 5) != "00351" AND LEFT(lcTlmvl, 4) != "+351")
		        DELETE
		    ENDIF
		ENDSCAN

		
		&& incrementa regua
		regua[0,2,"A validar telem�veis repetidos...",.F.]	
		&&repetidos
*!*			DO WHILE .t.
*!*				select * from uCrsCliSMSEnviar where tlmvl in (SELECT tlmvl FROM uCrsCliSMSEnviar GROUP BY tlmvl HAVING (COUNT(tlmvl) > 1) ) order by uCrsCliSMSEnviar.tlmvl INTO CURSOR ucrsTlmvlRepetidos READWRITE
*!*				SELECT ucrsTlmvlRepetidos
*!*				IF RECCOUNT("ucrsTlmvlRepetidos") > 0
*!*					SELECT ucrsTlmvlRepetidos
*!*					GO TOP
*!*					SELECT uCrsCliSMSEnviar
*!*					LOCATE FOR ALLTRIM(uCrsCliSMSEnviar.clstamp) == ALLTRIM(ucrsTlmvlRepetidos.clstamp)
*!*					DELETE
*!*				ELSE
*!*					EXIT
*!*				ENDIF
*!*			ENDDO
		
		&& incrementa regua
		regua[0,3,"A actualzar grelha de clientes...",.F.]	
		SELECT uCrsCliSMSEnviar
		GO TOP
		reservaclsms.refresh
		
		&& FECHA REGUA
		regua(2)
	ENDIF 
ENDFUNC 

**
FUNCTION uf_reservaclsms_sair
	reservaclsms.hide
	reservaclsms.release
ENDFUNC

FUNCTION uf_reservaclsms_limpaNrTelemovel
    LOCAL lcNewValue, lcOldValue, lnPos
    
    if(USED("uCrsCliSMSEnviar"))
	    SELECT uCrsCliSMSEnviar
	    SCAN
	        lcOldValue = ALLTRIM(uCrsCliSMSEnviar.tlmvl) 
	        lnPos = AT("9", lcOldValue)  

	        IF lnPos > 1
	            lcNewValue = SUBSTR(lcOldValue, lnPos)  
	            SELECT uCrsCliSMSEnviar
	            REPLACE tlmvl WITH lcNewValue
	        ENDIF
	        
	    ENDSCAN
    ENDIF
    
   RETURN .T.
ENDFUNC

FUNCTION uf_reservaclsms_verificaPrefixo
    LPARAMETERS tcValue
    LOCAL lcPrefix, llResult

    lcPrefix = LEFT(ALLTRIM(tcValue), 2)
    IF INLIST(lcPrefix, "91", "92", "93", "95", "96")
    	RETURN .T.
    ENDIF

    RETURN .F.
ENDFUNC