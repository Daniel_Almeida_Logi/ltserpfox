**
FUNCTION uf_conffact_chama
	LPARAMETERS lcLocal
	
	IF uf_conffact_verificaAcesso() == .f.
		return .f.
	ENDIF
	
	*** Abre o ecr� caso n�o esteja aberto
	IF TYPE("CONFFACT")=="U"
		
		uf_conffact_criaCursores(lcLocal)
		
		DO FORM CONFFACT
		
		If Type("CONFFACT.oCust") # "O"
			CONFFACT.AddObject("oCust","confactCust")
		ENDIF
		
		** controla objectos
		IF !uf_gerais_validaPermUser(ch_userno, ch_grupo, 'Compras - Confer�ncia de Facturas Gest�o') == .t.
			conffact.adoc.readonly = .f.
			conffact.datainicio.enabled = .t.
			conffact.datafim.enabled = .t.
		ELSE
			conffact.adoc.readonly = .t.
			conffact.datainicio.enabled = .f.
			conffact.datafim.enabled = .f.
		ENDIF
		
		IF !(lcLocal == "DOC")
			uf_conffact_CarregaDados()
		ENDIF
		
		uf_conffact_pramGrelhaConfPvp()
		**uf_conffact_AvisoInventario()
		
		CONFFACT.show
	ELSE
		CONFFACT.show
	ENDIF
ENDFUNC



**
FUNCTION uf_conffact_verificaAcesso
	
	** Valida acesso - Conferencia de Facturas **
	IF uf_gerais_validaPermUser(ch_userno, ch_grupo, 'Compras - Confer�ncia de Facturas') == .t. && Parametro de Atribui��o
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ACEDER � CONFER�NCIA DE FACTURAS.","OK","",48)
		RETURN .f.
	ENDIF
	
	** Valida Documentos **
	IF USED("cabDoc")
		IF ALLTRIM(UPPER(cabDoc.doc)) != 'V/FACTURA' 
			uf_perguntalt_chama("ESTA OP��O APENAS EST� DISPONIVEL PARA O DOCUMENTO <V/FACTURA>.","OK","",48)
			RETURN .f.
		ENDIF
		
		IF MyDocIntroducao == .t. OR MyDocAlteracao == .t.
			uf_perguntalt_chama("DEVE GRAVAR O DOCUMENTO ANTES DE PROCEDER � CONFER�NCIA.","OK","",48)
			RETURN .f.
		ENDIF
	ENDIF
	
	
	** validar se o parametro est� activo e se as facturas mexem em stock **
	LOCAL lcValidaParam, lcValidaMovStock
	STORE .f. TO lcValidaParam, lcValidaMovStock
	
	IF uf_gerais_actgrelha("", "uCrsValMovStockFact", [select folansl from cm1 (nolock) where cm=55])
		IF RECCOUNT("uCrsValMovStockFact") > 0
			IF uCrsValMovStockFact.folansl
				lcValidaMovStock = .t.
			ENDIF
		ENDIF
		fecha("uCrsValMovStockFact")
	ENDIF
	IF ALLTRIM(uf_gerais_getParameter("ADM0000000029","TEXT")) != 'Painel Confer�ncia'
		uf_perguntalt_chama("A CONFER�NCIA DE FACTURAS N�O SE ENCONTRA ACTIVA.","OK","", 48)
		RETURN .f.
	ENDIF
	IF lcValidaMovStock
		uf_perguntalt_chama("N�O PODE UTILIZAR A CONFER�NCIA DE FACTURAS ENQUANTO O DOCUMENTO FACTURA ESTIVER CONFIGURADO PARA MEXER EM STOCK. POR FAVOR CONTACTE O SUPORTE.","OK","", 48)
		RETURN .f.
	ENDIF
	************************************************************************

	RETURN .t.
ENDFUNC



**
FUNCTION uf_conffact_criaCursores
	LPARAMETERS lcLocal
	
	IF lcLocal != "DOC"
	
		IF !USED("uCrsConfFac")
			TEXT TO lcSQL NOSHOW TEXTMERGE
				SET FMTONLY on
				exec up_documentos_conferenciaFacturas '19000101', '19000101',  '', '', 0, <<mysite_nr>>
				SET FMTONLY off
			ENDTEXT
			IF !uf_gerais_actgrelha("", "uCrsConfFac", lcSql)
				uf_perguntalt_chama("OCORREU UM ERRO NA VERIFICA��O DE DOCUMENTOS.","OK","",16)
				RETURN .f.
			ENDIF
		ENDIF
	
	ELSE
	
		IF !USED("ucrsConfFactParam")
			CREATE CURSOR ucrsConfFactParam (dinicio d, dfim d, adoc c(20), ref c(18))
			SELECT ucrsConfFactParam 
			APPEND BLANK
		ENDIF
	
		SELECT cabDoc
		SELECT ucrsConfFactParam
		Replace ucrsConfFactParam.dinicio 	WITH cabDoc.datadoc
		Replace ucrsConfFactParam.dfim 		WITH cabDoc.datadoc
		Replace ucrsConfFactParam.adoc 		WITH cabDoc.numdoc
		
		uf_conffact_CarregaDados(.t.)
	ENDIF	
ENDFUNC
  
  

**
FUNCTION uf_conffact_AvisoInventario
	** validar se existe acerto por invent�rio pendente**
	LOCAL lcRef
	STORE '' TO lcRef
	
	SELECT uCrsConfFac
	GO TOP
	SCAN
		lcRef = Iif(Empty(uCrsConfFac.ref), uCrsConfFac.oref, uCrsConfFac.ref)
		
		IF uf_gerais_actgrelha("", "uCrsExisteInvAviso", [select lanca from stil (nolock) inner join stic (nolock) on stic.sticstamp=stil.sticstamp where ref=']+ALLTRIM(lcRef)+[' and stic.data between ']+uCrsConfFac.odata+[' and dateadd(HOUR, ]+str(difhoraria)+[, getdate())])
			IF RECCOUNT("uCrsExisteInvAviso")>0
				If !uCrsExisteInvAviso.lanca
					CONFFACT.ContainerDados.lblAviso.visible = .t.
				ENDIF
			ENDIF
			fecha("uCrsExisteInvAviso")
		ENDIF
		
		SELECT uCrsConfFac
	ENDSCAN

	SELECT uCrsConfFac
	GO TOP
ENDFUNC



**
FUNCTION uf_conffact_CarregaDados
	LPARAMETERS lcBool && Corre antes do painel abrir, chamado do painel de Documentos
	
	LOCAL lcSQL, lcValidaAcesso, lcDataFim, lcDataIni
	
	Store .f. To lcValidaAcesso

	regua(0,100,"A PROCESSAR REGISTOS PENDENTES DE VALIDA��O...",.f.)

	IF !uf_gerais_validaPermUser(ch_userno,ch_grupo,'Compras - Confer�ncia de Facturas Farm�cia')
		lcValidaAcesso=.t.
	ENDIF

	IF lcBool == .f.
		If !empty(CONFFACT.dataInicio.value)
			lcDataIni = CONFFACT.dataInicio.value
		Else
			lcDataIni ='19000101'
		EndIf
		If !empty(CONFFACT.DataFim.value)
			lcDataFim = CONFFACT.DataFim.value
		Else
			If !empty(CONFFACT.dataInicio.value)
				lcDataFim = lcDataIni
			Else
				lcDataFim='30001231'
			EndIf
		ENDIF
		TEXT TO lcSQL NOSHOW TEXTMERGE
			exec up_documentos_conferenciaFacturas '19000101', '20590101',  '<<alltrim(CONFFACT.adoc.value)>>', '<<alltrim(CONFFACT.ref.value)>>', <<Iif(lcValidaAcesso==.t.,1,0)>>, <<mysite_nr>>
		ENDTEXT
		
		uf_gerais_actGrelha("CONFFACT.GridPesq","uCrsConfFac",lcSQL)
		
		uf_conffact_AvisoInventario()	
		
	ELSE
	
		IF !USED("uCrsConfFac")
			SELECT cabDoc		
			TEXT TO lcSQL NOSHOW TEXTMERGE
				exec up_documentos_conferenciaFacturas '<<uf_gerais_getDate(CabDOC.DATADOC,"SQL")>>', '<<uf_gerais_getDate(CabDOC.DATADOC,"SQL")>>',  '<<alltrim(cabDoc.numdoc)>>', '', <<Iif(lcValidaAcesso==.t.,1,0)>>, <<mysite_nr>>
			ENDTEXT
			IF !uf_gerais_actgrelha("", "uCrsConfFac", lcSql)
				uf_perguntalt_chama("OCORREU UMA ANOMALIA NA VERIFICA��O DE DADOS A CONFERIR.","OK","",16)
				RETURN .f.
			ENDIF
		ENDIF
	ENDIF
	
	
	regua(2)
ENDFUNC



**
Function uf_conffact_pramGrelhaConfPvp
	LPARAMETERS tcBool

	LOCAL lcti, lcColumn
	STORE 0 TO lcti
	STORE '' TO lcColumn
	
	
	WITH CONFFACT.GridPesq
	

		**PDU_2ZI0WKUAJ.pageframe1.page1.obj1.themes=.f.
		**PDU_2ZI0WKUAJ.pageframe1.page1.obj1.rowheight = 25

		FOR i=1 TO .ColumnCount
			lcColumn = Upper(Alltrim(.columns(i).header1.caption))
		
			IF tcBool==.f.
				DO CASE
					CASE lcColumn=="QT." Or lcColumn=="BONUS" Or lcColumn=="DC%(1)" Or lcColumn=="DC%(2)" Or lcColumn=="DC%(3)" Or lcColumn=="DC%(4)" Or lcColumn=="TOTAL" Or lcColumn=="FECHAR"
						.columns(i).dynamicBackColor="IIF(recno()%2!=0,Rgb[152,251,152],rgb[152,251,152])"
					OTHERWISE
						.columns(i).dynamicBackColor="IIF(recno()%2!=0,Rgb[255,255,255],rgb[245,245,245])"
				ENDCASE

				If lcColumn=="FACTURA"
					.columns(i).width = 80
				Endif
				If lcColumn=="DESIGNA��O"
					.columns(i).width = 210
				Endif
				If lcColumn=="QT. ENC."
					.columns(i).width = 50
				Endif
				If lcColumn=="PCL ENC."
					.columns(i).width = 55
				Endif
				If lcColumn=="TOTAL ENC."
					.columns(i).width = 70
				Endif
			Endif

			If lcColumn=="QT."
				If tcBool==.f.
					.columns(i).header1.backcolor = 50688
					.columns(i).width = 50
				Endif
				BindEvent(.columns(i).text1,"InteractiveChange",CONFFACT.oCust,"proc_afterQtChangePCF")
				.columns(i).dynamicForeColor="IIF(uCrsConfFac.qtf!=0 and uCrsConfFac.qtf!=uCrsConfFac.qtfac,Rgb[255,0,0],rgb[0,0,0])"
			Endif
		
			If lcColumn=="PCL" 
				If tcBool==.f.
					.columns(i).header1.backcolor = 50688
					.columns(i).width = 55
				Endif
			Endif

			If lcColumn=="BONUS"
				If tcBool==.f.
					.columns(i).header1.backcolor = 50688
					.columns(i).width = 55
				Endif
			Endif
			If lcColumn=="DC%(1)"
				If tcBool==.f.
					.columns(i).header1.backcolor = 50688
					.columns(i).width = 55
				Endif
			Endif
			If lcColumn=="DC%(2)"
				If tcBool==.f.
					.columns(i).header1.backcolor = 50688
					.columns(i).width = 55
				Endif
			Endif
			If lcColumn=="DC%(3)"
				If tcBool==.f.
					.columns(i).header1.backcolor = 50688
					.columns(i).width = 55
				Endif
			Endif
			If lcColumn=="DC%(4)"
				If tcBool==.f.
					.columns(i).header1.backcolor = 50688
					.columns(i).width = 55
				Endif
			ENDIF

			If lcColumn=="TOTAL"
				If tcBool==.f.
					.columns(i).header1.backcolor = 50688
					.columns(i).width = 70
				Endif
				BindEvent(.columns(i).text1,"LostFocus",CONFFACT.oCust,"proc_afterTotalChangePCF")
				.columns(i).dynamicForeColor="IIF(uCrsConfFac.totalf!=0 and uCrsConfFac.totalf!=uCrsConfFac.totalfac,Rgb[255,0,0],rgb[0,0,0])"
						
				****************************************************
				*Valida acesso - Conferencia de Facturas Farmacia  *
				****************************************************
				Local lcValidaAcesso
				Store .f. To lcValidaAcesso
				
				IF !(uf_gerais_validaPermUser(ch_userno,ch_grupo,'Compras - Confer�ncia de Facturas Farm�cia'))				
					.columns(i).readonly = .t.
					lcValidaAcesso = .t.			
				ENDIF
				********************************
			ENDIF

			If lcColumn=="F.F."
				If tcBool==.f.
					.columns(i).header1.backcolor = 50688
					.columns(i).width = 40
					*BindEvent(.columns(i).header1,"DblClick",CONFFACT.oCust,"proc_selTodosConfFacFF")
				Endif
			Endif
			If lcColumn=="P.F."
				If tcBool==.f.
					.columns(i).header1.backcolor = 50688
					.columns(i).width = 40
					*BindEvent(.columns(i).header1,"DblClick",CONFFACT.oCust,"proc_selTodosConfFacPF")
					
					IF lcValidaAcesso==.t.
						.columns(i).readonly = .t.
					ENDIF
				Endif
			ENDIF
					
			If tcBool==.f.
				If lcColumn=="QT. FAC."
					.columns(i).width = 50
				Endif
				If lcColumn=="PCL FAC."
					.columns(i).width = 55
				Endif
				If lcColumn=="TOTAL FAC."
					.columns(i).width = 70
				Endif
			Endif
		EndFor

		.lockcolumns = 2
	ENDWITH 
ENDFUNC




**
Function uf_conffact_FacCorrectaConfFac
	If !Used("uCrsConfFac")
		RETURN .f.
	ENDIF
	
	SELECT uCrsConfFac
	replace uCrsConfFac.qtf			With uCrsConfFac.qtfac
	replace uCrsConfFac.upcf		With uCrsConfFac.upcfac
	replace uCrsConfFac.bonusf		With uCrsConfFac.bonusfac
	replace uCrsConfFac.desc1f		With uCrsConfFac.desc1fac
	replace uCrsConfFac.desc2f		With uCrsConfFac.desc2fac
	replace uCrsConfFac.desc3f		With uCrsConfFac.desc3fac
	replace uCrsConfFac.desc4f		With uCrsConfFac.desc4fac
	replace uCrsConfFac.totalf		With uCrsConfFac.totalfac
	replace uCrsConfFac.fmarcada	With .t.
	
	CONFFACT.gridPesq.refresh
*!*		CONFFACT.containerDados.lblAviso.Caption 	= "Confer�ncia alterada de acordo com a Factura."
*!*		CONFFACT.containerDados.lblAviso.forecolor	= RGB(34,177,76)
*!*		CONFFACT.containerDados.lblAviso.visible	= .t.
*!*		
*!*		IF Type("CONFFACT.timerConffact") # "O"
*!*			CONFFACT.AddObject('timerConffact','timerConffact')
*!*		ENDIF
ENDFUNC



**
Function uf_conffact_encCorrectaConfFac
	If !Used("uCrsConfFac")
		RETURN .f.
	Endif
	
	Select uCrsConfFac
	replace uCrsConfFac.qtf			With uCrsConfFac.qtenc
	replace uCrsConfFac.upcf		With uCrsConfFac.upcenc
	replace uCrsConfFac.bonusf		With uCrsConfFac.bonusenc
	replace uCrsConfFac.desc1f		With uCrsConfFac.desc1enc
	replace uCrsConfFac.desc2f		With uCrsConfFac.desc2enc
	replace uCrsConfFac.desc3f		With uCrsConfFac.desc3enc
	replace uCrsConfFac.desc4f		With uCrsConfFac.desc4enc
	replace uCrsConfFac.totalf		With uCrsConfFac.totalenc
	replace uCrsConfFac.fmarcada	With .t.
	
	CONFFACT.gridPesq.refresh
*!*		CONFFACT.containerDados.lblAviso.Caption = "Confer�ncia alterada de acordo com a Origem."
*!*		CONFFACT.containerDados.lblAviso.forecolor = RGB(34,177,76)
*!*		CONFFACT.containerDados.lblAviso.visible = .t.

*!*		IF Type("CONFFACT.timerConffact") # "O" 
*!*			CONFFACT.AddObject('timerConffact','timerConffact')
*!*		ENDIF
ENDFUNC



**
Function uf_conffact_gravar
	IF !Used("uCrsConfFac")
		RETURN .f.
	ENDIF
	
	**SET Point TO "."
	
	LOCAL lcPos, lcValidaAlt, lcValidaAlt2, lcValidaAccao, lcSql, lcValidaCfsExist, lcValidaSave, lcAviso, lcContaPsi, lcContaBen
	STORE .f. TO lcValidaCfsExist, lcValidaSave
	STORE 0 To lcPos, lcValidaAlt, lcValidaAlt2, lcValidaAccao, lcAviso, lcContaPsi, lcContaBen
	STORE "" TO lcSql
	
	SELECT uCrsConfFac
	lcPos = Recno()
	
	** gravar as altera��es parciais em tabela tempor�ria cfs **
	SELECT uCrsConfFac
	GO TOP
	SCAN FOR uCrsConfFac.marcadaf==.f. AND uCrsConfFac.fmarcada==.t.
		** valida se o registo j� existe na tabela cfs **
		IF Uf_gerais_actgrelha("", "uCrsCheckCfs", [select * from B_confFacSave cfs (nolock) where cfs.fnstamp=']+ALLTRIM(uCrsConfFac.fnstamp)+['])
			IF reccount("uCrsCheckCfs")>0
				lcValidaCfsExist = .t.
			ENDIF
		ENDIF
		
		IF lcValidaCfsExist==.f.
			TEXT TO lcSql NOSHOW textmerge
				INSERT INTO B_confFacSave
					(fnstamp, qtf, upcf, desc1f,
					desc2f, desc3f, desc4f, totalf, pf)
				values
					('<<ALLTRIM(uCrsConfFac.fnstamp)>>', <<uCrsConfFac.qtf>>, <<uCrsConfFac.upcf>>, <<uCrsConfFac.desc1f>>,
					<<uCrsConfFac.desc2f>>, <<uCrsConfFac.desc3f>>, <<uCrsConfFac.desc4f>>, <<uCrsConfFac.totalf>>, 1)
			ENDTEXT
			IF !uf_gerais_actgrelha("", "", lcSql)
				uf_perguntalt_chama("N�O EST� A SER POSS�VEL GUARDAR AS ALTERA��ES. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				RETURN .f.
			ELSE
				lcValidaSave=.T.
			ENDIF
		ELSE
			TEXT TO lcSql NOSHOW textmerge
				UPDATE
					B_confFacSave
				SET
					qtf		= <<uCrsConfFac.qtf>>,
					upcf	= <<uCrsConfFac.upcf>>,
					desc1f	= <<uCrsConfFac.desc1f>>,
					desc2f	= <<uCrsConfFac.desc2f>>,
					desc3f	= <<uCrsConfFac.desc3f>>,
					desc4f	= <<uCrsConfFac.desc4f>>,
					totalf	= <<uCrsConfFac.totalf>>
				Where
					fnstamp='<<uCrsConfFac.fnstamp>>'
			ENDTEXT
			IF !uf_gerais_actgrelha("", "", lcSql)
				uf_perguntalt_chama("N�O EST� A SER POSS�VEL GRAVAR AS ALTERA��ES. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				RETURN .f.
			ELSE
				lcValidaSave=.T.
			ENDIF
		ENDIF
		
		lcValidaCfsExist = .f.

		IF USED("uCrsCheckCfs")
			fecha("uCrsCheckCfs")
		ENDIF
		
		SELECT uCrsConfFac
	ENDSCAN
	
	** elimina os registos sem visto no p.f. **
	SELECT uCrsConfFac
	GO TOP
	SCAN FOR uCrsConfFac.fmarcada=.f. AND uCrsConfFac.marcadaf=.f.
		uf_gerais_actgrelha("", "", [delete B_confFacSave where B_confFacSave.fnstamp=']+ALLTRIM(uCrsConfFac.fnstamp)+['])
		lcValidaSave = .t.
	ENDSCAN
	*******************************************
	
	IF lcValidaSave==.t.
		uf_perguntalt_chama("ALTERA��ES GRAVADAS COM SUCESSO.","OK","",64)
	ENDIF
	********************************************************
	
	** Valida se h� Ajustes a Fazer e Controlo de Contadores Psico/Benzo **
	SELECT uCrsConfFac
	GO TOP
	SCAN
		IF (uCrsConfFac.marcadaf==.t.) AND (uCrsConfFac.qtf!=uCrsConfFac.qtenc or uCrsConfFac.totalf!=uCrsConfFac.totalenc)
			IF uf_gerais_actgrelha("", "uCrsValidaGEx", [select ofnstamp from fn (nolock) inner join fo (nolock) on fo.fostamp=fn.fostamp where fo.doccode=102 and fn.ofnstamp=']+Alltrim(uCrsConfFac.fnstamp)+['])
				IF !RECCOUNT("uCrsValidaGEx")>0
					lcValidaAlt		= 1
					IF (uCrsConfFac.qtf==0 Or uCrsConfFac.totalf==0)
						lcAviso 	= 1
					ENDIF
					
					** controlo contadores de entradas psico/benzo **
					IF EMPTY(uCrsConfFac.ofnstamp)
						IF uCrsConfFac.psico
							** incrementar contador psico
							IF lcContaPsi==0	&& Primeira contagem para as Linhas
								TEXT TO lcSql NOSHOW TEXTMERGE PRETEXT 7
									SELECT
										IsNull(Max(u_psicont),0) ct
									FROM
										fn (nolock)
								ENDTEXT
								IF uf_gerais_actgrelha("", [uCrsContador], lcSql)
									lcContaPsi= uCrsContador.ct + 1
									fecha([uCrsContador])
								ENDIF
							ELSE
								lcContaPsi= lcContaPsi	+ 1
							ENDIF
							Replace uCrsConfFac.u_psicont WITH lcContaPsi IN uCrsConfFac
						ENDIF
						
						IF uCrsConfFac.benzo
							** incrementar contador benzo
							IF lcContaBen==0	&& Primeira contagem para as Linhas
								TEXT TO lcSql NOSHOW TEXTMERGE PRETEXT 7
									SELECT
										IsNull(Max(u_bencont),0) ct
									FROM
										fn (nolock)
								ENDTEXT
								IF uf_gerais_actgrelha("", [uCrsContador], lcSql)
									lcContaBen= uCrsContador.ct + 1
									fecha([uCrsContador])
								ENDIF
							ELSE
								lcContaBen = lcContaBen	+ 1
							ENDIF
							Replace uCrsConfFac.u_bencont WITH lcContaBen IN uCrsConfFac
						ENDIF
					ENDIF
					**************************************************
				ENDIF
				fecha("uCrsValidaGEx")
			ENDIF
		ENDIF
		
		IF (uCrsConfFac.marcadaf==.t.) AND uCrsConfFac.qtf==uCrsConfFac.qtenc and uCrsConfFac.totalf==uCrsConfFac.totalenc
			lcValidaAlt2	= 1
		ENDIF
	ENDSCAN
	************************************************************************
	
	If lcValidaAlt==1
		** Cria novos documentos (Guias) para os produtos com ajustes a fazer **
		If uf_perguntalt_chama("VAI PROCEDER A CRIA��O DOS RESPECTIVOS AJUSTES. PRETENDE CONTINUAR?","Sim","N�o")
			IF lcAviso==1
				IF uf_perguntalt_chama("ATEN��O: ALGUM(S) PRODUTO(S) A CRIAR NAS GUIAS DE ACERTO CONT�M QUANTIDADES OU PRE�OS A 0. PRETENDE MESMO ASSIM CONTINUAR?","Sim","N�o")
					lcValidaAccao=1
					uf_conffact_criarGuiasAcertoPCF()
				ENDIF
			ELSE
				lcValidaAccao=1
				uf_conffact_criarGuiasAcertoPCF()
			ENDIF
		ENDIF
	ENDIF
	
	** Fechar registos em que a recep��o seja considerada correcta **
	IF lcValidaAlt2==1
		SELECT uCrsConfFac
		GO TOP
		SCAN FOR uCrsConfFac.marcadaf==.t. AND uCrsConfFac.qtf==uCrsConfFac.qtenc and uCrsConfFac.totalf==uCrsConfFac.totalenc
			TEXT TO lcSql NOSHOW textmerge
				UPDATE
					FN
				SET
					marcada=1
				where
					fn.fnstamp='<<ALLTRIM(uCrsConfFac.fnstamp)>>' or fn.fnstamp='<<<ALLTRIM(uCrsConfFac.ofnstamp)>>'
			ENDTEXT
			IF !uf_gerais_actgrelha("", "", lcSql)
				uf_perguntalt_chama("OCCORREU UM ERRO A FECHAR UM LITIGIO, POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			ELSE
				lcValidaAccao = 1
			ENDIF
			
			SELECT uCrsConfFac
		ENDSCAN
	ENDIF
	*******************************************************************
	
	** Criar Pedidos de Regulariza��o a Fornecedores **
	IF lcValidaAccao=1
		** pedido de regulariza��o (cr�dito) **
		IF uf_gerais_getParameter("ADM0000000060","BOOL") ==.t.
			uf_conffact_criaPedCredPCF()
		ENDIF
		** pedido de regulariza��o (d�bito) **
		IF uf_gerais_getParameter("ADM0000000075","BOOL") ==.t.
			uf_conffact_criaPedDebPCF()
		ENDIF
	ENDIF
	****************************************
	
	If lcValidaAlt==0 And lcValidaAlt2==0 AND lcValidaSave==.f.
		uf_perguntalt_chama("N�O EXISTE NADA A ALTERAR.","OK","",48)
	ELSE
		If lcValidaAccao==1
			uf_perguntalt_chama("PROCESSO TERMINADO COM SUCESSO.","OK","",64)
		Endif
	Endif
	
	If Used("uCrsStampsGuiasFoPCF")
		Fecha("uCrsStampsGuiasFoPCF")
	Endif

	If Used("uCrsStampsGuiasFoDistinctPCF")
		Fecha("uCrsStampsGuiasFoDistinctPCF")
	ENDIF
ENDFUNC 



**
FUNCTION uf_conffact_criarGuiasAcertoPCF	
	**Set Point To "."

	LOCAL lcSql, lcFoStamp, lcFnStamp, lcNrDoc, lcDocCode, lcDocNome, lcFoTotal, lcCount
	LOCAL lcNrDos, lcDosCode, lcDosNome, lcBoTotal, lcBoStamp, lcBiStamp, lcMotivoQuebra
	STORE 0 TO lcNrDoc, lcNrDDoc, lcFoTotal, lcBoTotal, lcCount
	STORE '' TO lcSql, lcDosNome, lcDocNome, lcFoStamp, lcFnStamp, lcBoStamp, lcBiStamp, lcMotivoQuebra
	
	** Guardar c�digo e nome da guia de acerto **
	lcDocCode = 102 && c�digo do documento
	IF uf_gerais_actgrelha("", "uCrsCmCode", [select cmdesc from cm1 (nolock) where cm=102])
		IF RECCOUNT()>0
			lcDocNome = uCrsCmCode.cmdesc && nome do documento
		ENDIF
		fecha("uCrsCmCode")
	ELSE
		uf_perguntalt_chama("O DOCUMENTO N/GUIA DE ENTRADA N�O EST� A SER ENCONTRADO. O DOCUMENTO DE ACERTO N�O SER� CRIADO.","OK","",16)
		RETURN .f.
	ENDIF
	*********************************************
	
	** Guardar c�digo e nome do doc. Stock Quebra **
	lcDosCode = 18 && c�digo do documento
	IF uf_gerais_actgrelha("", "uCrsNdosCode", [select nmdos from ts (nolock) where ndos=18])
		IF RECCOUNT("uCrsNdosCode")>0
			lcDosNome = uCrsNdosCode.nmdos && nome do documento
		ENDIF
		fecha("uCrsNdosCode")
	ELSE
		uf_perguntalt_chama("O DOCUMENTO STOCK QUEBRAS N�O EST� A SER ENCONTRADO. O DOCUMENTO DE ACERTO N�O SER� CRIADO.","OK","",48)
		RETURN .f.
	ENDIF
	************************************************
	
	** Guardar motivo de quebra **
	IF uf_gerais_actgrelha("", "uCrsMq", [select descr from B_motivosQuebra (nolock) where ref='2'])
		IF RECCOUNT("uCrsMq")>0
			lcMotivoQuebra = uCrsMq.descr
		ENDIF
		fecha("uCrsMq")
	ENDIF
	******************************

	** Determinar armazem para cabe�alho **
	LOCAL lcArmazem
	lcArmazem = 0
	IF uf_gerais_actgrelha("", "uCrsArm", [exec up_gerais_dadosEmpresa ']+ALLTRIM(mySite)+['])
		IF RECCOUNT("uCrsArm")>0
			lcArmazem = uCrsArm.armazem1
		ENDIF
		fecha("uCrsArm")
	ENDIF
	***************************************

	** Guardar dados da Entidade para doc. Stock Quebras **
	LOCAL lcEnome, lcEno, lcEestab, lcElocal, lcEcodPost, lcEmorada, lcEmoeda, lcEtelefone, lcEtipo, lcEncont, lcEemail
	STORE '' TO lcEnome, lcElocal, lcEcodPost, lcEmorada, lcEmoeda, lcEtelefone, lcEtipo, lcEncont, lcEemail
	STORE 0 TO lcEno, lcEestab
	
	IF uf_gerais_actgrelha("", "uCrsDE", [exec up_gerais_dadosEntidade ']+ALLTRIM(mySite)+[']) && aten��o: a entidade deve estar mapeada na tabela de lojas
		IF RECCOUNT("uCrsDE")>0
			lcEnome		= uCrsDE.nome
			lcEno		= uCrsDE.no
			lcEestab	= 0
			lcElocal	= uCrsDE.local
			lcEcodPost	= uCrsDE.codpost
			lcEmorada	= uCrsDE.morada
			lcEmoeda	= 'EURO'
			lcEtelefone	= uCrsDE.telefone
			lcEtipo		= uCrsDE.codigo
			lcEncont	= uCrsDE.ncont
			lcEemail	= uCrsDE.email
		ELSE
			uf_perguntalt_chama("N�O EXISTE ENTIDADE ASSOCIADA � LOJA. ASSIM N�O PODE CONTINUAR.","OK","",48)
			fecha("uCrsDE")
			RETURN .f.
		ENDIF
		fecha("uCrsDE")
	ELSE
		uf_perguntalt_chama("OCORREU UM ERRO A PROCURAR DADOS DA ENTIDADE. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	ENDIF
	******************************************************

	*** Calcular n�mero de fornecedores = n�mero de documentos **
	SELECT uCrsConfFac
	GO TOP
	SCAN FOR uCrsConfFac.marcadaf==.t. AND (uCrsConfFac.qtf!=uCrsConfFac.qtenc Or uCrsConfFac.totalf!=uCrsConfFac.totalenc) && se o produto tiver ajustes a fazer
		IF uf_gerais_actgrelha("", "uCrsValidaGuiaExist2", [select ofnstamp from fn (nolock) inner join fo (nolock) on fo.fostamp=fn.fostamp where fo.doccode=102 and fn.ofnstamp=']+Alltrim(uCrsConfFac.fnstamp)+['])
			IF !RECCOUNT("uCrsValidaGuiaExist2")>0 && se o produto ainda n�o tiver originado nova Guia de Acerto
				** Guardar fornecedores num cursor para divis�o de documentos
				If !Used("uCrsFornGuiasFoPCF")
					Create Cursor uCrsFornGuiasFoPCF (no n(10), estab n(5))
					Create Cursor uCrsFornGuiasFoDistinctPCF (no n(10), estab n(5))
				Endif
				If uf_gerais_actgrelha("", "uCrsGetFornPCF", [select no, estab from fo (nolock) where fostamp=']+Alltrim(uCrsConfFac.fostamp)+['])
					If Reccount("uCrsGetFornPCF")>0
						Insert Into uCrsFornGuiasFoPCF (no, estab) Values (uCrsGetFornPCF.no, uCrsGetFornPCF.estab)
					Endif
					Fecha("uCrsGetFornPCF")
				ENDIF
				******************************************************************
			ENDIF
			fecha("uCrsValidaGuiaExist2")
		ENDIF
		
		SELECT uCrsConfFac
	ENDSCAN

	** Guardar em Cursor apenas os fornecedores �nicos **
	If used("uCrsFornGuiasFoPCF")
		Select distinct no, estab From uCrsFornGuiasFoPCF Into Cursor uCrsFornGuiasFoDistinctPCF
		fecha("uCrsFornGuiasFoPCF")
	Endif
	*****************************************************
	
	IF !USED("uCrsFornGuiasFoDistinctPCF")
		RETURN .f.
	ENDIF
	
	** Calcular Totais para Guia de Acerto **
	* NOTA: os totais antes dos arrendondamentos s�o mantidos iguais aos totais depois dos arredondamentos para efeitos de simplicidade. N�o devem existir arrendondamentos nas Guias
	LOCAL valorIva1, valorIva2, valorIva3, valorIva4, valorIva5, valorIva6, valorIva7, valorIva8, valorIva9
	LOCAL totalIva, totalComIva, totalSemIva, totalNservico, lcValidaNovo, lcTotalFinal
	LOCAL lcNome, lcNo, lcPais, lcEstab, lcLocal, lcCodPost, lcNcont, lcMorada, lcMoeda, lcTipo, lcCcusto
	** variaveis para o doc Stock Quebra **
	LOCAL lcExisteInv, lcControlTotalInv, lcQtSum, lcTotalFinalDi, lcTotalIvaDi, lcRef
	LOCAL lcTotalComIvaDi, lcTotalSemIvaDi, lcTotalSemIvaDi1, lcTotalSemIvaDi2, lcTotalSemIvaDi3, lcTotalSemIvaDi4, lcTotalSemIvaDi5, lcTotalSemIvaDi6, lcTotalSemIvaDi7, lcTotalSemIvaDi8, lcTotalSemIvaDi9
	LOCAL lcValorIvaDi1, lcValorIvaDi2, lcValorIvaDi3, lcValorIvaDi4, lcValorIvaDi5, lcValorIvaDi6, lcValorIvaDi7, lcValorIvaDi8, lcValorIvaDi9
	*****************************************
	
	** Vari�vies para as linhas **
		** Criar Linhas da Guia de Acerto e do documento de quebras caso necess�rio **
	LOCAL lcFnCount, lcPcpG, lcRef, lcQtf, lcQtAltern, lcBonusF, lcStock, lcFamilia, lcUpc
		** variaveis para o documento stock quebra
	LOCAL lcTotalFinalQ, lcUpcQ, lcQftQ, lcQtAlternQ
	******************************
	
	**************************************************
	*** Criar Documentos de Ajuste de Quantidade *****
	**************************************************
	
	SELECT uCrsFornGuiasFoDistinctPCF
	GO TOP
	SCAN
		** Guardar n�mero do documento para a guia de acerto **
		IF uf_gerais_actgrelha("", "uCrsNrMax", [select ISNULL((MAX(convert(numeric,adoc))),0) as nr from fo (nolock) where fo.doccode=102 and ISNUMERIC(adoc)=1 and YEAR(fo.docdata)=YEAR(dateadd(HOUR, ]+str(difhoraria)+[, getdate()))])
			IF RECCOUNT("uCrsNrMax")>0
				lcNrDoc = uCrsNrMax.nr + 1
			ENDIF
			fecha("uCrsNrMax")
		ELSE
			uf_perguntalt_chama("O DOCUMENTO N/GUIA DE ENTRADA N�O EST� A SER ENCONTRADO. O DOCUMENTO DE ACERTO N�O SER� CRIADO.","OK","",16)
			RETURN .f.
		ENDIF
		*******************************************************
		
		** Guardar n�mero do documento para a quebra de stock (contra ajuste) **
		IF uf_gerais_actgrelha("", "uCrsNrDMax", [select ISNULL((MAX(obrano)),0) as nr from bo (nolock) where bo.ndos=18 and YEAR(bo.dataobra)=YEAR(dateadd(HOUR, ]+str(difhoraria)+[, getdate()))])
			IF RECCOUNT("uCrsNrDMax")>0
				lcNrDos = uCrsNrDMax.nr + 1
			ENDIF
			fecha("uCrsNrDMax")
		ELSE
			uf_perguntalt_chama("O DOCUMENTO STOCK QUEBRAS N�O EST� A SER ENCONTRADO. O DOCUMENTO DE ACERTO N�O SER� CRIADO.","OK","",16)
			RETURN .f.
		ENDIF
		*********************************

		** Guarda Contador por tipo de documento e origem **
		LOCAL lcDocCont, lcNdosCont
		STORE 0 TO lcDocCont, lcNdosCont
		
		IF uf_gerais_actgrelha("", "uCrsContDoc", [select ISNULL(MAX(fo2.u_doccont),0) as doccont from fo2 (nolock) inner join fo (nolock) on fo.fostamp=fo2.fo2stamp where fo.doccode=102])
			IF RECCOUNT("uCrsContDoc")>0
				lcDocCont = uCrsContDoc.doccont + 1
			ELSE
				lcDocCont = 1
			ENDIF
			fecha("uCrsContDoc")
		ENDIF
		IF uf_gerais_actgrelha("", "uCrsContNDos", [select ISNULL(MAX(bo2.u_doccont),0) as doccont from bo2 (nolock) inner join bo (nolock) on bo.bostamp=bo2.bo2stamp where bo.ndos=18])
			IF RECCOUNT("uCrsContNDos")>0
				lcNdosCont = uCrsContNDos.doccont + 1
			ELSE
				lcNdosCont = 1
			ENDIF
			fecha("uCrsContNDos")
		ENDIF
		******************************************************

		** guardar stamps para cabe�alhos **
		lcCount		= lcCount + 1
		*lcFoStamp	= u_stamp(lcCount)
		lcFoStamp	= uf_gerais_stamp()
		lcCount		= lcCount + 1
		*lcBoStamp	= u_stamp(lcCount)
		lcBoStamp	= uf_gerais_stamp()
		
		** valores por defeito **
		STORE 0 TO valorIva1, valorIva2, valorIva3, valorIva4, valorIva5, valorIva6, valorIva7, valorIva8, valorIva9
		STORE 0 TO totalIva, totalComIva, totalSemIva, totalNservico, lcTotalFinal
		Store .f. To lcValidaNovo
		STORE "" TO lcNome, lcLocal, lcCodPost, lcNcont, lcMorada, lcMoeda, lcTipo, lcCcusto
		STORE 0 TO lcNo, lcEstab
		STORE 1 TO lcPais
		** valores por defeito para o doc. stock quebra **
		STORE 0 TO lcExisteInv, lcControlTotalInv, lcQtSum, lcTotalFinalDi, lcTotalIvaDi
		STORE 0 TO lcTotalComIvaDi, lcTotalSemIvaDi, lcTotalSemIvaDi1, lcTotalSemIvaDi2, lcTotalSemIvaDi3, lcTotalSemIvaDi4, lcTotalSemIvaDi5, lcTotalSemIvaDi6, lcTotalSemIvaDi7, lcTotalSemIvaDi8, lcTotalSemIvaDi9
		STORE 0 TO lcValorIvaDi1, lcValorIvaDi2, lcValorIvaDi3, lcValorIvaDi4, lcValorIvaDi5, lcValorIvaDi6, lcValorIvaDi7, lcValorIvaDi8, lcValorIvaDi9
		STORE '' TO lcRef
		
		** Guias de acerto de quantidades **
		SELECT uCrsConfFac
		GO TOP
		SCAN FOR uCrsConfFac.no==uCrsFornGuiasFoDistinctPCF.no AND uCrsConfFac.estab==uCrsFornGuiasFoDistinctPCF.estab ;
				AND uCrsConfFac.marcadaf==.t. AND uCrsConfFac.qtf!=uCrsConfFac.qtenc
			IF uf_gerais_actgrelha("", "uCrsValidaGuiaExist2", [select ofnstamp from fn (nolock) inner join fo (nolock) on fo.fostamp=fn.fostamp where fo.doccode=102 and fn.ofnstamp=']+Alltrim(uCrsConfFac.fnstamp)+['])
				IF !RECCOUNT("uCrsValidaGuiaExist2")>0 && se o produto ainda n�o tiver originado nova Guia de Acerto
					
					** ref
					lcRef			= Iif(Empty(uCrsConfFac.ref), uCrsConfFac.oref, uCrsConfFac.ref)
					
					** validar se existe acerto por invent�rio **
					IF !EMPTY(uCrsConfFac.odata) AND (uCrsConfFac.qtf!=uCrsConfFac.qtenc) && s� emite quebras para ajustes de quantidade
						IF uf_gerais_actgrelha("", "uCrsExisteInv", [select ref from stil where ref=']+ALLTRIM(lcRef)+[' and data between ']+uCrsConfFac.odata+[' and dateadd(HOUR, ]+str(difhoraria)+[, getdate())])
							IF RECCOUNT("uCrsExisteInv")>0
								lcExisteInv	= 1
								lcControlTotalInv = 1
							ENDIF
							fecha("uCrsExisteInv")
						ENDIF
					ENDIF
					********************************************
					
					** total qtt apenas para o doc. stock quebras **
					IF lcControlTotalInv == 1
						lcQtSum		= lcQtSum + (uCrsConfFac.qtf-uCrsConfFac.qtenc)
					ENDIF
					************************************************
					
					** Calcular totais para o documento de quebras **
					IF lcControlTotalInv == 1
						lcTotalFinalDi	= uCrsConfFac.upcenc * (uCrsConfFac.qtf - uCrsConfFac.qtenc)
						
						DO CASE 
							CASE uCrsConfFac.tabiva=1
								lcValorIvaDi1	= lcValorIvaDi1+(lcTotalFinalDi*(uCrsConfFac.iva/100))
							CASE uCrsConfFac.tabiva=2
								lcValorIvaDi2	= lcvalorIvaDi2+(lcTotalFinalDi*(uCrsConfFac.iva/100))
							CASE uCrsConfFac.tabiva=3
								lcValorIvaDi3	= lcvalorIvaDi3+(lcTotalFinalDi*(uCrsConfFac.iva/100))
							CASE uCrsConfFac.tabiva=4
								lcValorIvaDi4	= lcvalorIvaDi4+(lcTotalFinalDi*(uCrsConfFac.iva/100))
							CASE uCrsConfFac.tabiva=5
								lcValorIvaDi5	= lcvalorIvaDi5+(lcTotalFinalDi*(uCrsConfFac.iva/100))
							CASE uCrsConfFac.tabiva=6
								lcValorIvaDi6	= lcvalorIvaDi6+(lcTotalFinalDi*(uCrsConfFac.iva/100))
							CASE uCrsConfFac.tabiva=7
								lcValorIvaDi7	= lcvalorIvaDi7+(lcTotalFinalDi*(uCrsConfFac.iva/100))
							CASE uCrsConfFac.tabiva=8
								lcValorIvaDi8	= lcvalorIvaDi8+(lcTotalFinalDi*(uCrsConfFac.iva/100))
							CASE uCrsConfFac.tabiva=9
								lcValorIvaDi9	= lcvalorIvaDi9+(lcTotalFinalDi*(uCrsConfFac.iva/100))
						ENDCASE

						lcTotalComIvaDi	= lcTotalComIvaDi + (lcTotalFinalDi*(uCrsConfFac.iva/100+1))
						lcTotalSemIvaDi	= lcTotalSemIvaDi + lcTotalFinalDi
					ENDIF
					*************************************************
					
					** Calcular totais para o documento de ajuste **
					lcTotalFinal	= uCrsConfFac.upcenc * (uCrsConfFac.qtf - uCrsConfFac.qtenc)
					
					DO CASE
						CASE uCrsConfFac.tabiva=1
							valorIva1=valorIva1+(lcTotalFinal*(uCrsConfFac.iva/100))
						CASE uCrsConfFac.tabiva=2
							valorIva2=valorIva2+(lcTotalFinal*(uCrsConfFac.iva/100))
						CASE uCrsConfFac.tabiva=3
							valorIva3=valorIva3+(lcTotalFinal*(uCrsConfFac.iva/100))
						CASE uCrsConfFac.tabiva=4
							valorIva4=valorIva4+(lcTotalFinal*(uCrsConfFac.iva/100))
						CASE uCrsConfFac.tabiva=5
							valorIva5=valorIva5+(lcTotalFinal*(uCrsConfFac.iva/100))
						CASE uCrsConfFac.tabiva=6
							valorIva6=valorIva6+(lcTotalFinal*(uCrsConfFac.iva/100))
						CASE uCrsConfFac.tabiva=7
							valorIva7=valorIva7+(lcTotalFinal*(uCrsConfFac.iva/100))
						CASE uCrsConfFac.tabiva=8
							valorIva8=valorIva8+(lcTotalFinal*(uCrsConfFac.iva/100))
						CASE uCrsConfFac.tabiva=9
							valorIva9=valorIva9+(lcTotalFinal*(uCrsConfFac.iva/100))
					ENDCASE
														
					totalComIva = totalComIva + (lcTotalFinal*(uCrsConfFac.iva/100+1))
					totalSemIva = totalSemIva + (lcTotalFinal)
					
					If !uCrsConfFac.stns
						totalNservico=totalNservico + lcTotalFinal
					ENDIF
			
					lcValidaNovo=.t.
		
					lcNome		= uCrsConfFac.nome
					lcNo		= uCrsConfFac.no
					lcEstab		= uCrsConfFac.estab
					lcTipo		= uCrsConfFac.tipo
					lcLocal		= uCrsConfFac.local
					lcCodPost	= uCrsConfFac.codpost
					lcNcont		= uCrsConfFac.ncont
					lcMorada	= uCrsConfFac.morada
					lcMoeda		= uCrsConfFac.moeda
					lcPais		= uCrsConfFac.pais
					lcCcusto	= uCrsConfFac.ccusto
				ENDIF
				
				fecha("uCrsValidaGuiaExist2")
			ENDIF
			
			lcControlTotalInv = 0
					
			SELECT uCrsConfFac
		ENDSCAN
	
		LOCAL lcOrdem, lcOrdemQ
		STORE 10000 TO lcOrdem, lcOrdemQ
	
		IF lcValidaNovo && valida que existem produtos a gerar guias
		
			totalIva		= valorIva1 + valorIva2 + valorIva3 + valorIva4 + valorIva5 + valorIva6 + valorIva7 + valorIva8 + valorIva9
			lcTotalIvaDi	= lcValorIvaDi1 + lcValorIvaDi2 + lcValorIvaDi3 + lcValorIvaDi4 + lcValorIvaDi5 + lcValorIvaDi6 + lcValorIvaDi7 + lcValorIvaDi8 + lcValorIvaDi9
			*****************************************

			** Criar Cabe�alho da Guia de Acerto **
			SELECT uCrsConfFac
			
				** o campo fo2.u_ajuste � usado para marcar este documento como de acerto, � utilizado pelas analises
				** 1 = ajuste valor, 2 = ajuste de quantidade
			TEXT TO lcSql NOSHOW TEXTMERGE
				INSERT INTO FO
					(fostamp, docnome, doccode,
					nome, no, estab, adoc,
					morada, local, codpost, ncont,
					data, docdata, foano, pdata,
					moeda, memissao, moeda2, ollocal, telocal, final,
					total, etotal, ivain, eivain,
					ttiva, ettiva, ettiliq,
					ivainsns, eivainsns,
					ivav1, eivav1,	ivav2, eivav2, ivav3, eivav3,
					ivav4, eivav4, ivav5, eivav5, ivav6, eivav6,
					ivav7, eivav7, ivav8, eivav8, ivav9, eivav9,
					epaivav1, epaivav2,	epaivav3, epaivav4, epaivav5,
					epaivav6, epaivav7,	epaivav8, epaivav9,
					epaivain, patotal, epatotal,
					site, pnome, pno, u_status, pais, ccusto,
					ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora )
				Values
					('<<ALLTRIM(lcFoStamp)>>', '<<ALLTRIM(lcDocNome)>>', <<lcDocCode>>,
					'<<ALLTRIM(lcNome)>>', <<lcNo>>, <<lcEstab>>, '<<astr(lcNrDoc)>>',
					'<<ALLTRIM(lcMorada)>>', '<<ALLTRIM(lcLocal)>>', '<<ALLTRIM(lcCodPost)>>', '<<ALLTRIM(lcNcont)>>',
					convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), YEAR(dateadd(HOUR, <<difhoraria>>, getdate())), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102),
					'<<ALLTRIM(lcMoeda)>>', 'EURO', '<<ALLTRIM(lcMoeda)>>', 'Caixa', 'C', 'Documento Criado via Confer�ncia de Facturas',
					<<Round(totalComIva*200.482,2)>>, <<Round(totalComIva,2)>>, <<Round(totalSemIva*200.482,2)>>, <<Round(totalSemIva,2)>>,
					<<Round(totalIva*200.482,2)>>, <<Round(totalIva,2)>>, <<Round(totalSemIva,2)>>,
					<<Round(totalNservico*200.482,2)>>, <<Round(totalNservico,2)>>,
					<<Round(valorIva1*200.482,2)>>, <<Round(valorIva1,2)>>, <<Round(valorIva2*200.482,2)>>, <<Round(valorIva2,2)>>, <<Round(valorIva3*200.482,2)>>, <<Round(valorIva3,2)>>,
					<<Round(valorIva4*200.482,2)>>, <<Round(valorIva4,2)>>, <<Round(valorIva5*200.482,2)>>, <<Round(valorIva5,2)>>, <<Round(valorIva6*200.482,2)>>, <<Round(valorIva6,2)>>,
					<<Round(valorIva7*200.482,2)>>, <<Round(valorIva7,2)>>, <<Round(valorIva8*200.482,2)>>, <<Round(valorIva8,2)>>, <<Round(valorIva9*200.482,2)>>, <<Round(valorIva9,2)>>,
					<<Round(valorIva1,2)>>, <<Round(valorIva2,2)>>, <<Round(valorIva3,2)>>, <<Round(valorIva4,2)>>, <<Round(valorIva5,2)>>,
					<<Round(valorIva6,2)>>, <<Round(valorIva7,2)>>, <<Round(valorIva8,2)>>, <<Round(valorIva9,2)>>,
					<<Round(totalSemIva,2)>>, <<Round(totalComIva*200.482,2)>>, <<Round(totalComIva,2)>>,
					'<<ALLTRIM(mySite)>>', '<<ALLTRIM(myTerm)>>', <<myTermNo>>, 'F', <<lcPais>>, '<<lcCcusto>>',
					'<<m_chinis>>', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8), '<<m_chinis>>', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8) )
			ENDTEXT
			IF !uf_gerais_actgrelha("", "", lcSql)
				uf_perguntalt_chama("OCORREU UM ERRO A CRIAR A GUIA DE ACERTO. POR FAVOR CONTACTE O SUPORTE."+CHR(10)+"Cod: FO","OK","",16)
				RETURN .f.
			ENDIF

			lcSql=''
			TEXT TO lcSql NOSHOW TEXTMERGE
				INSERT INTO fo2
					( fo2stamp,
					ivatx1, ivatx2, ivatx3,
					ivatx4, ivatx5, ivatx6,
					ivatx7, ivatx8, ivatx9,
					u_doccont, u_ajuste, u_armazem,
					ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora )
				values
					( '<<ALLTRIM(lcFoStamp)>>',
					 (select taxa from taxasiva (nolock) where codigo=1), (select taxa from taxasiva (nolock) where codigo=2), (select taxa from taxasiva (nolock) where codigo=3),
					 (select taxa from taxasiva (nolock) where codigo=4), (select taxa from taxasiva (nolock) where codigo=5), (select taxa from taxasiva (nolock) where codigo=6),
					 (select taxa from taxasiva (nolock) where codigo=7), (select taxa from taxasiva (nolock) where codigo=8), (select taxa from taxasiva (nolock) where codigo=9),
					 <<lcDocCont>>, 2, <<lcArmazem>>,
					'<<m_chinis>>', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8), '<<m_chinis>>', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8) )
			ENDTEXT
			IF !uf_gerais_actgrelha("", "", lcSql)
				uf_perguntalt_chama("OCORREU UM ERRO A CRIAR A GUIA DE ACERTO. POR FAVOR CONTACTE O SUPORTE."+CHR(10)+"Cod: FO2","OK","",16)
			ENDIF
			***************************************
			
			** Criar cabe�alho do Dossier Interno Caso seja necess�rio **
			IF lcExisteInv==1

				SELECT uCrsConfFac

					** o campo ocupacao esta com o valor estatico de 4, porqu�?????
				TEXT TO lcSql NOSHOW textmerge
					Insert into bo
						(bostamp, ndos, nmdos, obrano, dataobra,
						nome, nome2, [no], estab, morada, [local], codpost, tipo, ncont,
						etotaldeb, dataopen, boano, ecusto, etotal, moeda, memissao, origem, site,
						vendedor, vendnm, obs, u_mquebra,
						sqtt14, ebo_1tvall, ebo_2tvall, ebo_totp1, ebo_totp2,
						ebo11_bins, ebo12_bins, ebo21_bins, ebo22_bins, ebo31_bins, ebo32_bins,
						ebo41_bins, ebo42_bins, ebo51_bins, ebo52_bins, ebo61_bins, ebo62_bins,
						ebo11_iva, ebo12_iva, ebo21_iva, ebo22_iva, ebo31_iva, ebo32_iva,
						ebo41_iva, ebo42_iva, ebo51_iva, ebo52_iva, ebo61_iva, ebo62_iva,
						ocupacao, fechada, datafinal, 
						ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora)
					Values
						('<<ALLTRIM(lcBostamp)>>', <<lcDosCode>>, '<<lcDosNome>>', <<lcNrDos>>, convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102),
						'<<ALLTRIM(lcEnome)>>', '', <<lcEno>>, <<lcEestab>>, '<<ALLTRIM(lcEmorada)>>', '<<ALLTRIM(lcElocal)>>', '<<ALLTRIM(lcEcodPost)>>', '<<ALLTRIM(lcEtipo)>>', '<<ALLTRIM(lcEncont)>>',
						<<ROUND(lcTotalSemIvaDi,2)>>, convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), year(dateadd(HOUR, <<difhoraria>>, getdate())), <<ROUND(lcTotalSemIvaDi,2)>>, <<ROUND(lcTotalComIvaDi,2)>>, '<<ALLTRIM(lcMoeda)>>', 'EURO', 'BO', '<<ALLTRIM(mySite)>>',
						<<ch_vendedor>>, '<<ALLTRIM(ch_vendnm)>>', 'Documento gerado Via Confer�ncia de Facturas', '<<ALLTRIM(lcMotivoQuebra)>>',
						<<lcQtSum>>, <<ROUND(lcTotalSemIvaDi,2)>>, <<ROUND(lcTotalSemIvaDi,2)>>, <<ROUND(lcTotalSemIvaDi,2)>>, <<ROUND(lcTotalSemIvaDi,2)>>,
						<<lcTotalSemIvaDi1>>, <<lcTotalSemIvaDi1>>, <<lcTotalSemIvaDi2>>, <<lcTotalSemIvaDi2>>, <<lcTotalSemIvaDi3>>, <<lcTotalSemIvaDi3>>,
						<<lcTotalSemIvaDi4>>, <<lcTotalSemIvaDi4>>, <<lcTotalSemIvaDi5>>, <<lcTotalSemIvaDi5>>, <<lcTotalSemIvaDi6>>, <<lcTotalSemIvaDi6>>,
						<<lcValorIvaDi1>>, <<lcValorIvaDi1>>, <<lcValorIvaDi2>>, <<lcValorIvaDi2>>, <<lcValorIvaDi3>>, <<lcValorIvaDi3>>,
						<<lcValorIvaDi4>>, <<lcValorIvaDi4>>, <<lcValorIvaDi5>>, <<lcValorIvaDi5>>, <<lcValorIvaDi6>>, <<lcValorIvaDi6>>,
						4, 1, '19000101',
						'ADM', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8), 'ADM', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8) )
				ENDTEXT
			
				IF !uf_gerais_actgrelha("", "", lcSql)
					uf_perguntalt_chama("OCORREU UM ERRO A GERAR DOCUMENTO STOCK QUEBRAS. POR FAVOR CONTACTE O SUPORTE."+CHR(10)+"Cod: BO2","OK","",16)
					RETURN .f.
				ENDIF
				lcSql=''


				TEXT TO lcSql NOSHOW textmerge
					Insert into bo2
						(bo2stamp, autotipo, pdtipo, etotalciva, etotiva, u_doccont,
						telefone, email, armazem,
						ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora)
					Values
						('<<ALLTRIM(lcBostamp)>>', 1, 1, <<ROUND(lcTotalComIvaDi,2)>>, <<ROUND(lcTotalIvaDi,2)>>, <<lcNdosCont>>,
						'<<lcEtelefone>>', '<<lcEemail>>', <<lcArmazem>>,
						'ADM', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8), 'ADM', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8) )
				ENDTEXT				
				IF !uf_gerais_actgrelha("", "", lcSql)
					uf_perguntalt_chama("OCORREU UM ERRO A GERAR DOCUMENTO STOCK QUEBRAS. POR FAVOR CONTACTE O SUPORTE."+CHR(10)+"Cod: BO2","OK","",16)
				ENDIF
			ENDIF
			************************************************************
		

			** Criar Linhas da Guia de Acerto e do documento de quebras caso necess�rio **
			STORE 0 TO lcFnCount, lcPcpG, lcQtf, lcQtAltern, lcTotalFinal, lcBonusF, lcStock, lcExisteInv, lcUpc
			STORE '' TO lcRef, lcSql, lcFamilia
				** variaveis para o documento stock quebra
			STORE 0 TO lcTotalFinalQ, lcUpcQ, lcQftQ, lcQtAlternQ
				
			
			SELECT uCrsConfFac
			GO TOP
			SCAN FOR uCrsConfFac.no==uCrsFornGuiasFoDistinctPCF.no AND uCrsConfFac.estab==uCrsFornGuiasFoDistinctPCF.estab ;
					AND uCrsConfFac.marcadaf==.t. AND uCrsConfFac.qtf!=uCrsConfFac.qtenc
				IF uf_gerais_actgrelha("", "uCrsValidaGuiaExist3", [select ofnstamp from fn (nolock) inner join fo (nolock) on fo.fostamp=fn.fostamp where fo.doccode=102 and fn.ofnstamp=']+Alltrim(uCrsConfFac.fnstamp)+['])
					IF !RECCOUNT("uCrsValidaGuiaExist3")>0 && se o produto ainda n�o tiver originado nova Guia de Acerto
						
						** ref produto **
						lcRef			= Iif(Empty(uCrsConfFac.ref), uCrsConfFac.oref, uCrsConfFac.ref)
						
						** validar se existe acerto por invent�rio **
						IF !EMPTY(uCrsConfFac.odata) AND uCrsConfFac.qtf!=uCrsConfFac.qtenc && apenas emite quebras qd existe ajuste de quantidade
							IF uf_gerais_actgrelha("", "uCrsExisteInv", [select ref from stil where ref=']+ALLTRIM(lcRef)+[' and data between ']+uCrsConfFac.odata+[' and dateadd(HOUR, ]+str(difhoraria)+[, getdate())])
								IF RECCOUNT("uCrsExisteInv")>0
									lcExisteInv	= 1
								ENDIF
								fecha("uCrsExisteInv")
							ENDIF
						ENDIF
						
						** stamp linha
						lcFnCount		= lcFnCount + 1
						*lcFnStamp		= u_stamp(lcFnCount)
						lcFnStamp		= uf_gerais_stamp()
						lcFnCount		= lcFnCount + 1
						*lcBiStamp		= u_stamp(lcFnCount)
						lcBiStamp		= uf_gerais_stamp()
						
						SELECT uCrsConfFac	
						** qt linha
						lcQtf			= uCrsConfFac.qtf-uCrsConfFac.qtenc
						lcQtAltern		= INT((lcQtf) * uCrsConfFac.conversao)
								
						** quantidade para quebra **
						lcQtfQ			= uCrsConfFac.qtf - uCrsConfFac.qtenc
						lcQtAlternQ		= INT((lcQtf) * uCrsConfFac.conversao)
							
						** total linha
						lcTotalFinal	= uCrsConfFac.upcenc * (uCrsConfFac.qtf - uCrsConfFac.qtenc)
					
						** total para quebra **
						lcTotalFinalQ	= uCrsConfFac.upcenc * (uCrsConfFac.qtf - uCrsConfFac.qtenc)
							
						** upc
						IF lcQtf>0
							lcUpc		= uCrsConfFac.upcenc
						ELSE
							lcUpc		= uCrsConfFac.upcenc * (-1)
						ENDIF
							
						** upc para quebra **
						lcUpcQ			= lcUpc

						** bonus linha
						lcBonus			= 0
							
						** guardar stock actual, familia **
						IF uf_gerais_actgrelha("", "uCrsStockAct", [select stock, familia from st (nolock) where st.ref=']+ALLTRIM(lcRef)+['])
							IF reccount("uCrsStockAct")>0
								lcStock 	= uCrsStockAct.stock
								lcFamilia	= uCrsStockAct.familia
							ENDIF
							fecha("uCrsStockAct")
						ENDIF
							
						** PCT
						lcPcpG			= uCrsConfFac.epvenc
						
						** preencher linhas fn **				
						TEXT TO lcSql NOSHOW TEXTMERGE
							INSERT INTO fn
								(fnstamp, fostamp, ofnstamp, data,
								ref, oref, codigo, design,
								stns, usr1, usr2, usr3, usr4, usr5, usr6,
								docnome, adoc, unidade, qtt, uni2qtt,
								iva, ivaincl, tabiva, armazem, cpoc, lordem,
								epv, pv, etiliquido, tiliquido,
								u_bonus, desconto, desc2, desc3, desc4,
								esltt, sltt, u_upc, eslvu, slvu,
								u_stockAct, u_epv1act, marcada,
								u_psicont, u_bencont,
								ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora )
							Values
								('<<ALLTRIM(lcFnStamp)>>', '<<ALLTRIM(lcFoStamp)>>', '<<ALLTRIM(uCrsConfFac.fnstamp)>>', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102),
								 '<<ALLTRIM(lcRef)>>', '', '<<ALLTRIM(uCrsConfFac.codigo)>>', '<<ALLTRIM(uCrsConfFac.design)>>',
								 <<IIF(uCrsConfFac.stns, 1, 0)>>, '<<ALLTRIM(uCrsConfFac.usr1)>>', '<<ALLTRIM(uCrsConfFac.usr2)>>', '<<ALLTRIM(uCrsConfFac.usr3)>>', '<<ALLTRIM(uCrsConfFac.usr4)>>', '<<ALLTRIM(uCrsConfFac.usr5)>>', '<<ALLTRIM(uCrsConfFac.usr6)>>',
								 '<<ALLTRIM(lcDocNome)>>', '<<astr(lcNrDoc)>>', '<<ALLTRIM(uCrsConfFac.unidade)>>', <<lcQtf>>, <<lcQtAltern>>,
								 <<uCrsConfFac.iva>>, <<IIF(uCrsConfFac.ivaincl,1,0)>>, <<uCrsConfFac.tabiva>>, <<uCrsConfFac.armazem>>, <<uCrsConfFac.cpoc>>, <<lcOrdem>>,
								 <<Round(lcPcpG,2)>>, <<Round(lcPcpG*200.482,2)>>, <<lcTotalFinal>>, <<lcTotalFinal*200.482>>,
								 <<uCrsConfFac.bonusf>>, <<uCrsConfFac.desc1f>>, <<uCrsConfFac.desc2f>>, <<uCrsConfFac.desc3f>>, <<uCrsConfFac.desc4f>>,
								 <<lcTotalFinal>>, <<lcTotalFinal*200.482>>, <<lcUpc>>, <<lcTotalFinal / lcQtf>>, <<(lcTotalFinal / lcQtf) * 200.482>>,
								 <<uCrsConfFac.u_stockact>>, <<uCrsConfFac.u_epv1act>>, 1,
								 <<uCrsConfFac.u_psicont>>, <<uCrsConfFac.u_bencont>>,
								 '<<m_chinis>>', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8), '<<m_chinis>>', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8) )
						ENDTEXT
						IF !uf_gerais_actgrelha("", "", lcSql)
							uf_perguntalt_chama("OCORREU UM ERRO A CRIAR A GUIA DE ACERTO. POR FAVOR CONTACTE O SUPORTE."+CHR(10)+"Cod: FN","OK","",16)
							RETURN
						ENDIF
						
						** incrementar ordem das linhas
						lcOrdem 	= lcOrdem + 1000
						
						IF lcExisteInv==1 && se grava linhas doc. stock quebras
								
							** Preencher Linhas da Bi **
							TEXT TO lcSql NOSHOW textmerge
								Insert into bi
										(bistamp, bostamp, nmdos, obrano, ref, codigo,
										design, qtt, qtt2, uni2qtt,
										u_stockact, iva, tabiva, armazem, stipo, ndos, cpoc, familia,
										no, nome, local, morada, codpost,
										epu, edebito, eprorc, epcusto, ettdeb,
										ecustoind, edebitoori, u_upc,
										rdata, dataobra, dataopen, resfor, lordem,
										lobs, u_mquebra,
										ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora)
								Values ('<<ALLTRIM(lcBiStamp)>>', '<<ALLTRIM(lcBoStamp)>>', '<<ALLTRIM(lcDosNome)>>', <<lcNrDos>>, '<<ALLTRIM(lcRef)>>', '<<ALLTRIM(uCrsConfFac.codigo)>>',
										'<<ALLTRIM(uCrsConfFac.design)>>', <<lcQtfQ>>, 0, <<lcQtAlternQ>>,
										<<lcStock>>, <<uCrsConfFac.iva>>, <<uCrsConfFac.tabiva>>, <<uCrsConfFac.armazem>>, 4, <<lcDosCode>>, <<uCrsConfFac.cpoc>>, '<<ALLTRIM(lcFamilia)>>',
										<<lcEno>>, '<<ALLTRIM(lcEnome)>>', '<<ALLTRIM(lcElocal)>>', '<<ALLTRIM(lcEMorada)>>', '<<ALLTRIM(lcEcodPost)>>',
										<<lcTotalFinalQ / lcQtfQ>>, <<lcTotalFinalQ / lcQtfQ>>, <<lcTotalFinalQ / lcQtfQ>>, <<lcTotalFinalQ / lcQtfQ>>, <<lcTotalFinalQ>>,
										<<lcTotalFinalQ / lcQtfQ>>, <<lcTotalFinalQ / lcQtfQ>>, <<lcUpcQ>>,
										convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), 0, <<lcOrdemQ>>,
										'Referente � Factura N�. '+'<<ALLTRIM(uCrsConfFac.adoc)>>', '<<ALLTRIM(lcMotivoQuebra)>>',
										'ADM', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8), 'ADM', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8)
								)
							ENDTEXT
							IF !uf_gerais_actgrelha("", "", lcSql)
								uf_perguntalt_chama("OCORREU UM ERRO A CRIAR DOCUMENTO DE CONTRA AJUSTE. POR FAVOR CONTACTE O SUPORTE."+CHR(10)+"Cod: BI","OK","",16)
								RETURN .f.
							ENDIF
							
							** Preencher Linhas da Bi2 **
							TEXT TO lcSql NOSHOW textmerge
								Insert into bi2
									(bi2stamp, bostamp, fnstamp, morada, local, codpost)
								Values
									('<<ALLTRIM(lcBiStamp)>>', '<<ALLTRIM(lcBoStamp)>>', '<<ALLTRIM(uCrsConfFac.fnstamp)>>', '<<ALLTRIM(lcEmorada)>>', '<<ALLTRIM(lcElocal)>>', '<<ALLTRIM(lcEcodPost)>>')
							ENDTEXT
							IF !uf_gerais_actgrelha("", "", lcSql)
								uf_perguntalt_chama("OCORREU UM ERRO A CRIAR A GUIA DOCUMENTO DE CONTRA AJUSTE. POR FAVOR CONTACTE O SUPORTE."+CHR(10)+"Cod: BI","OK","",16)
							ENDIF
							
							** incrementar ordem das linhas
							lcOrdemQ	= lcOrdemQ + 1000
						ENDIF
						
						SET HOURS TO 24
						Atime=ttoc(datetime()+(difhoraria*3600),2)
						Astr=left(Atime,8)					
						
						** marcar as linhas como fechadas
						SELECT uCrsConfFac
						TEXT TO lcSql NOSHOW textmerge
							UPDATE
								fn
							SET
								marcada = 1,
								usrdata = convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102),
								usrhora = '<<Astr>>'
							where
								(fn.fnstamp='<<ALLTRIM(lcFnStamp)>>' or fn.fnstamp='<<ALLTRIM(uCrsConfFac.fnstamp)>>' or fnstamp='<<ALLTRIM(uCrsConfFac.ofnstamp)>>')
						ENDTEXT
						IF !uf_gerais_actgrelha("", "", lcSql)
							uf_perguntalt_chama("OCORREU UM ERRO A ACTUALIZAR ESTADO DA LINHA NA FACTURA. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
						ENDIF
						**********************************
						
						** Eliminar Registo pendente da tabela cfs se existir **
						SELECT uCrsConfFac
						IF uf_gerais_actgrelha("", "uCrsCheckCfs", [select * from B_confFacSave cfs (nolock) where cfs.fnstamp=']+ALLTRIM(uCrsConfFac.fnstamp)+['])
							IF reccount("uCrsCheckCfs")>0
								TEXT TO lcSql NOSHOW textmerge
									DELETE
										B_confFacSave
									where
										B_confFacSave.fnstamp='<<ALLTRIM(uCrsConfFac.fnstamp)>>'
								ENDTEXT
								IF !uf_gerais_actgrelha("", "", lcSql)
									***
								ENDIF
							ENDIF
							fecha("uCrsCheckCfs")
						ENDIF
						*******************************************************
						
					ENDIF
					fecha("uCrsValidaGuiaExist3")
				ENDIF

				lcExisteInv = 0
				
				SELECT uCrsConfFac
			ENDSCAN
			************************************
		ENDIF
		
		SELECT uCrsFornGuiasFoDistinctPCF
	ENDSCAN
	*********************************************************


	IF lcOrdem > 10000
		uf_perguntalt_chama("GUIA(s) DE ACERTO DE QUANTIDADES CRIADA(s) COM SUCESSO.","OK","",64)
	
		IF lcOrdemQ > 10000
			uf_perguntalt_chama("DOCUMENTO(s) DE QUEBRAS CRIADO(s) COM SUCESSO.","OK","",64)
		ENDIF
	ENDIF
	*********************************************************


	** valores por defeito **
	STORE 0 TO valorIva1, valorIva2, valorIva3, valorIva4, valorIva5, valorIva6, valorIva7, valorIva8, valorIva9
	STORE 0 TO totalIva, totalComIva, totalSemIva, totalNservico, lcTotalFinal
	Store .f. To lcValidaNovo
	STORE "" TO lcNome, lcLocal, lcCodPost, lcNcont, lcMorada, lcMoeda, lcTipo, lcCcusto
	STORE 0 TO lcNo, lcEstab
	STORE 1 TO lcPais
	STORE '' TO lcRef
	** contador de ref a gravar na guia de ajuste **
	LOCAL lcCiclo, lcCicloDev
	STORE 0 TO lcCiclo, lcCicloDev
	
	
	*********************************************************
	*** Criar Documentos de Ajuste de Valor *****************
	*********************************************************
	
	SELECT uCrsFornGuiasFoDistinctPCF
	GO TOP
	SCAN
		** Guardar n�mero do documento para a guia de acerto **
		IF uf_gerais_actgrelha("", "uCrsNrMax", [select ISNULL((MAX(convert(numeric,adoc))),0) as nr from fo (nolock) where fo.doccode=102 and ISNUMERIC(adoc)=1 and YEAR(fo.docdata)=YEAR(dateadd(HOUR, ]+str(difhoraria)+[, getdate()))])
			IF RECCOUNT("uCrsNrMax")>0
				lcNrDoc = uCrsNrMax.nr + 1
			ENDIF
			fecha("uCrsNrMax")
		ELSE
			uf_perguntalt_chama("O DOCUMENTO N/GUIA DE ENTRADA N�O EST� A SER ENCONTRADO. O DOCUMENTO DE ACERTO N�O SER� CRIADO.","OK","",16)
			RETURN .f.
		ENDIF
		*******************************************************

		** Guarda Contador por tipo de documento e origem **
		LOCAL lcDocCont, lcNdosCont
		STORE 0 TO lcDocCont, lcNdosCont
		
		IF uf_gerais_actgrelha("", "uCrsContDoc", [select ISNULL(MAX(fo2.u_doccont),0) as doccont from fo2 (nolock) inner join fo (nolock) on fo.fostamp=fo2.fo2stamp where fo.doccode=102])
			IF RECCOUNT("uCrsContDoc")>0
				lcDocCont = uCrsContDoc.doccont + 1
			ELSE
				lcDocCont = 1
			ENDIF
			fecha("uCrsContDoc")
		ENDIF
		******************************************************

		** guardar stamps para cabe�alhos **
		lcCount		= lcCount + 1
		*lcFoStamp	= u_stamp(lcCount)
		lcFoStamp	= uf_gerais_stamp()
		
		** valores por defeito **
		STORE 0 TO valorIva1, valorIva2, valorIva3, valorIva4, valorIva5, valorIva6, valorIva7, valorIva8, valorIva9
		STORE 0 TO totalIva, totalComIva, totalSemIva, totalNservico, lcTotalFinal
		Store .f. To lcValidaNovo
		STORE "" TO lcNome, lcLocal, lcCodPost, lcNcont, lcMorada, lcMoeda, lcTipo, lcCcusto
		STORE 0 TO lcNo, lcEstab
		STORE 1 TO lcPais
		STORE '' TO lcRef
		** contador de ref a gravar na guia de ajuste **
		LOCAL lcCiclo, lcCicloDev
		STORE 0 TO lcCiclo, lcCicloDev

		SELECT uCrsConfFac
		GO TOP
		SCAN FOR uCrsConfFac.no==uCrsFornGuiasFoDistinctPCF.no AND uCrsConfFac.estab==uCrsFornGuiasFoDistinctPCF.estab ;
				AND uCrsConfFac.marcadaf==.t. AND uCrsConfFac.upcf!=uCrsConfFac.upcenc AND !(uCrsConfFac.upcf==0) AND !(uCrsConfFac.qtf==0)		

				** aten��o a condi��o abaixo so deve avaliar destinos em que o ajuste seja de valor, isto porque ja pode existir liga��o com ajustes de quantidade (mesmo tipo de documento)
			IF uf_gerais_actgrelha("", "uCrsValidaGuiaExist2", [select ofnstamp from fn (nolock) inner join fo (nolock) on fo.fostamp=fn.fostamp inner join fo2 on fo2.fo2stamp=fo.fostamp where fo.doccode=102 and fo2.u_ajuste=1 and fn.ofnstamp=']+Alltrim(uCrsConfFac.fnstamp)+['])
				IF !RECCOUNT("uCrsValidaGuiaExist2")>0 && se o produto ainda n�o tiver originado nova Guia de Acerto de Valor
					** ref
					lcRef			= Iif(Empty(uCrsConfFac.ref), uCrsConfFac.oref, uCrsConfFac.ref)
									
					** definir se da baixa do produto da encomenda **
					lcCiclo 	= 2
					IF uCrsConfFac.qtf<=0
						lcCicloDev	= 1
					ELSE
						lcCicloDev	= 0
					ENDIF
					
					** Calcular totais para o documento de ajuste **
					FOR i= 1 TO lcCiclo
						IF i==1 AND lcCiclo>1
							lcTotalFinal	= uCrsConfFac.totalenc * (-1)
						ELSE
							IF lcCicloDev == 0
								lcTotalFinal	= uCrsConfFac.totalf
							ELSE
								lcTotalFinal	= uCrsConfFac.totalenc * -(1)
							ENDIF
						ENDIF
					
						DO CASE
							CASE uCrsConfFac.tabiva=1
								valorIva1=valorIva1+(lcTotalFinal*(uCrsConfFac.iva/100))
							CASE uCrsConfFac.tabiva=2
								valorIva2=valorIva2+(lcTotalFinal*(uCrsConfFac.iva/100))
							CASE uCrsConfFac.tabiva=3
								valorIva3=valorIva3+(lcTotalFinal*(uCrsConfFac.iva/100))
							CASE uCrsConfFac.tabiva=4
								valorIva4=valorIva4+(lcTotalFinal*(uCrsConfFac.iva/100))
							CASE uCrsConfFac.tabiva=5
								valorIva5=valorIva5+(lcTotalFinal*(uCrsConfFac.iva/100))
							CASE uCrsConfFac.tabiva=6
								valorIva6=valorIva6+(lcTotalFinal*(uCrsConfFac.iva/100))
							CASE uCrsConfFac.tabiva=7
								valorIva7=valorIva7+(lcTotalFinal*(uCrsConfFac.iva/100))
							CASE uCrsConfFac.tabiva=8
								valorIva8=valorIva8+(lcTotalFinal*(uCrsConfFac.iva/100))
							CASE uCrsConfFac.tabiva=9
								valorIva9=valorIva9+(lcTotalFinal*(uCrsConfFac.iva/100))
						ENDCASE
											
						totalComIva=totalComIva + (lcTotalFinal*(uCrsConfFac.iva/100+1))
						totalSemIva=totalSemIva + (lcTotalFinal)
						
						If !uCrsConfFac.stns
							totalNservico=totalNservico + lcTotalFinal
						Endif
					ENDFOR
								
					lcValidaNovo=.t.
		
					lcNome		= uCrsConfFac.nome
					lcNo		= uCrsConfFac.no
					lcEstab		= uCrsConfFac.estab
					lcTipo		= uCrsConfFac.tipo
					lcLocal		= uCrsConfFac.local
					lcCodPost	= uCrsConfFac.codpost
					lcNcont		= uCrsConfFac.ncont
					lcMorada	= uCrsConfFac.morada
					lcMoeda		= uCrsConfFac.moeda
					lcPais		= uCrsConfFac.pais
					lcCcusto	= uCrsConfFac.ccusto
				ENDIF
				fecha("uCrsValidaGuiaExist2")
			ENDIF

			SELECT uCrsConfFac
		ENDSCAN
		
		IF !lcValidaNovo
			RETURN
		ENDIF

		totalIva		= valorIva1+valorIva2+valorIva3+valorIva4+valorIva5+valorIva6+valorIva7+valorIva8+valorIva9
		*****************************************

		** Criar Cabe�alho da Guia de Acerto **
		SELECT uCrsConfFac
		
			** O campo fo2.u_ajuste � usado para marcar este documento como de acerto, � utilizado pelas analises.
			** 1 = ajuste valor, 2 = ajuste de quantidade
		TEXT TO lcSql NOSHOW TEXTMERGE
			INSERT INTO FO
				(fostamp, docnome, doccode,
				nome, no, estab, adoc,
				morada, local, codpost, ncont,
				data, docdata, foano, pdata,
				moeda, memissao, moeda2, ollocal, telocal, final,
				total, etotal, ivain, eivain,
				ttiva, ettiva, ettiliq,
				ivainsns, eivainsns,
				ivav1, eivav1,	ivav2, eivav2, ivav3, eivav3,
				ivav4, eivav4, ivav5, eivav5, ivav6, eivav6,
				ivav7, eivav7, ivav8, eivav8, ivav9, eivav9,
				epaivav1, epaivav2,	epaivav3, epaivav4, epaivav5,
				epaivav6, epaivav7,	epaivav8, epaivav9,
				epaivain, patotal, epatotal,
				site, pnome, pno, u_status, pais, ccusto,
				ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora )
			Values
				('<<ALLTRIM(lcFoStamp)>>', '<<ALLTRIM(lcDocNome)>>', <<lcDocCode>>,
				'<<ALLTRIM(lcNome)>>', <<lcNo>>, <<lcEstab>>, '<<astr(lcNrDoc)>>',
				'<<ALLTRIM(lcMorada)>>', '<<ALLTRIM(lcLocal)>>', '<<ALLTRIM(lcCodPost)>>', '<<ALLTRIM(lcNcont)>>',
				convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), YEAR(dateadd(HOUR, <<difhoraria>>, getdate())), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102),
				'<<ALLTRIM(lcMoeda)>>', 'EURO', '<<ALLTRIM(lcMoeda)>>', 'Caixa', 'C', 'Documento Criado via Confer�ncia de Facturas',
				<<Round(totalComIva*200.482,2)>>, <<Round(totalComIva,2)>>, <<Round(totalSemIva*200.482,2)>>, <<Round(totalSemIva,2)>>,
				<<Round(totalIva*200.482,2)>>, <<Round(totalIva,2)>>, <<Round(totalSemIva,2)>>,
				<<Round(totalNservico*200.482,2)>>, <<Round(totalNservico,2)>>,
				<<Round(valorIva1*200.482,2)>>, <<Round(valorIva1,2)>>, <<Round(valorIva2*200.482,2)>>, <<Round(valorIva2,2)>>, <<Round(valorIva3*200.482,2)>>, <<Round(valorIva3,2)>>,
				<<Round(valorIva4*200.482,2)>>, <<Round(valorIva4,2)>>, <<Round(valorIva5*200.482,2)>>, <<Round(valorIva5,2)>>, <<Round(valorIva6*200.482,2)>>, <<Round(valorIva6,2)>>,
				<<Round(valorIva7*200.482,2)>>, <<Round(valorIva7,2)>>, <<Round(valorIva8*200.482,2)>>, <<Round(valorIva8,2)>>, <<Round(valorIva9*200.482,2)>>, <<Round(valorIva9,2)>>,
				<<Round(valorIva1,2)>>, <<Round(valorIva2,2)>>, <<Round(valorIva3,2)>>, <<Round(valorIva4,2)>>, <<Round(valorIva5,2)>>,
				<<Round(valorIva6,2)>>, <<Round(valorIva7,2)>>, <<Round(valorIva8,2)>>, <<Round(valorIva9,2)>>,
				<<Round(totalSemIva,2)>>, <<Round(totalComIva*200.482,2)>>, <<Round(totalComIva,2)>>,
				'<<ALLTRIM(mySite)>>', '<<ALLTRIM(myTerm)>>', <<myTermNo>>, 'F', <<lcPais>>, '<<lcCcusto>>',
				'<<m_chinis>>', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8), '<<m_chinis>>', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8) )
		ENDTEXT
		IF !uf_gerais_actgrelha("", "", lcSql)
			uf_perguntalt_chama("OCORREU UM ERRO A CRIAR A GUIA DE ACERTO. POR FAVOR CONTACTE O SUPORTE."+CHR(10)+"Cod: FO","OK","",16)
			RETURN .f.
		ENDIF

		lcSql=''
		TEXT TO lcSql NOSHOW textmerge
			INSERT INTO fo2
				( fo2stamp,
				ivatx1, ivatx2, ivatx3,
				ivatx4, ivatx5, ivatx6,
				ivatx7, ivatx8, ivatx9,
				u_doccont, u_ajuste, u_armazem,
				ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora )
			values
				( '<<ALLTRIM(lcFoStamp)>>',
				 (select taxa from taxasiva (nolock) where codigo=1), (select taxa from taxasiva (nolock) where codigo=2), (select taxa from taxasiva (nolock) where codigo=3),
				 (select taxa from taxasiva (nolock) where codigo=4), (select taxa from taxasiva (nolock) where codigo=5), (select taxa from taxasiva (nolock) where codigo=6),
				 (select taxa from taxasiva (nolock) where codigo=7), (select taxa from taxasiva (nolock) where codigo=8), (select taxa from taxasiva (nolock) where codigo=9),
				 <<lcDocCont>>, 1, <<lcArmazem>>,
				'<<m_chinis>>', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8), '<<m_chinis>>', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8) )
		ENDTEXT
		IF !uf_gerais_actgrelha("", "", lcSql)
			uf_perguntalt_chama("OCORREU UM ERRO A CRIAR A GUIA DE ACERTO. POR FAVOR CONTACTE O SUPORTE."+CHR(10)+"Cod: FO2","OK","",16)
		Endif
		***************************************

		** Criar Linhas da Guia de Acerto **
		LOCAL lcFnCount, lcOrdem, lcPcpG, lcRef, lcQtf, lcQtAltern, lcBonusF, lcStock, lcFamilia, lcUpc
		STORE 0 TO lcFnCount, lcPcpG, lcQtf, lcQtAltern, lcTotalFinal, lcBonusF, lcStock, lcExisteInv, lcUpc

		STORE 10000 TO lcOrdem
		STORE '' TO lcRef, lcSql, lcFamilia
		STORE 0 TO lcCiclo, lcCicloDev
	
		SELECT uCrsConfFac
		GO TOP
		SCAN FOR uCrsConfFac.no==uCrsFornGuiasFoDistinctPCF.no AND uCrsConfFac.estab==uCrsFornGuiasFoDistinctPCF.estab ;
				AND uCrsConfFac.marcadaf==.t. AND uCrsConfFac.upcf!=uCrsConfFac.upcenc AND !(uCrsConfFac.upcf==0) AND !(uCrsConfFac.qtf==0)
		
			IF uf_gerais_actgrelha("", "uCrsValidaGuiaExist3", [select ofnstamp from fn (nolock) inner join fo (nolock) on fo.fostamp=fn.fostamp inner join fo2 on fo2.fo2stamp=fo.fostamp where fo.doccode=102 and fo2.u_ajuste=1 and fn.ofnstamp=']+Alltrim(uCrsConfFac.fnstamp)+['])
				IF !RECCOUNT("uCrsValidaGuiaExist3")>0 && se o produto ainda n�o tiver originado nova Guia de Acerto

					** ref produto **
					lcRef			= Iif(Empty(uCrsConfFac.ref), uCrsConfFac.oref, uCrsConfFac.ref)

					** definir se da baixa do produto da encomenda (ajuste de valor da sempre)**
					lcCiclo 	= 2

					IF uCrsConfFac.qtf<=0
						lcCicloDev	= 1
					ELSE
						lcCicloDev	= 0
					ENDIF
					****************************************************************************

					FOR i = 1 TO lcCiclo
						** stamp linha
						lcFnCount		= lcFnCount + 1
						*lcFnStamp		= u_stamp(lcFnCount)
						lcFnStamp		= uf_gerais_stamp()
						
						SELECT uCrsConfFac
						
						** qt linha
						IF i==1 AND lcCiclo>1
							lcQtf			= uCrsConfFac.qtenc * (-1)
							lcQtAltern		= INT((lcQtf) * uCrsConfFac.conversao) * (-1)
						ELSE
							IF lcCicloDev==0
								lcQtf			= uCrsConfFac.qtenc
								lcQtAltern		= INT((lcQtf) * uCrsConfFac.conversao)
							ELSE
								lcQtf			= uCrsConfFac.qtenc * (-1) &&
								lcQtAltern		= INT((lcQtf) * uCrsConfFac.conversao) * (-1)
							ENDIF
						ENDIF

						** total linha
						IF i==1 AND lcCiclo>1
							lcTotalFinal		= uCrsConfFac.totalenc * (-1)
						ELSE
							IF lcCicloDev==0
								lcTotalFinal	= uCrsConfFac.upcf * uCrsConfFac.qtenc && novo preco, mesma quantidade da recep��o
							ELSE
								lcTotalFinal	= uCrsConfFac.totalenc * (-1)
							ENDIF
						ENDIF
						
						** upc
						IF i==1 AND lcCiclo>1
							lcUpc			= uCrsConfFac.upcenc * (-1)
						ELSE
							IF lcCicloDev==0
								lcUpc		= lcTotalFinal / lcQtf
							ELSE
								lcUpc		= uCrsConfFac.upcenc * (-1)
							ENDIF
						ENDIF
				
						** bonus linha
						IF i==1 AND lcCiclo>1
							lcBonus			= uCrsConfFac.bonusenc
						ELSE
							IF lcCicloDev==0
								IF uCrsConfFac.bonusf > lcQtf
									lcBonus		= 0
								ELSE
									lcBonusF	= uCrsConfFac.bonusenc
								ENDIF
							ELSE
								lcBonus			= uCrsConfFac.bonusenc
							ENDIF
						ENDIF
						
						** guardar stock actual, familia **
						IF uf_gerais_actgrelha("", "uCrsStockAct", [select stock, familia from st (nolock) where st.ref=']+ALLTRIM(lcRef)+['])
							IF reccount("uCrsStockAct")>0
								lcStock 	= uCrsStockAct.stock
								lcFamilia	= uCrsStockAct.familia
							ENDIF
							fecha("uCrsStockAct")
						ENDIF
						
						** PCT
						IF i==1 AND lcCiclo>1
							lcPcpG			= uCrsConfFac.epvenc
						ELSE
							lcPcpG			= uCrsConfFac.epvenc
						ENDIF
		
						** preencher linhas fn **				
						TEXT TO lcSql NOSHOW TEXTMERGE
							INSERT INTO fn
								(fnstamp, fostamp, ofnstamp, data,
								ref, oref, codigo, design,
								stns, usr1, usr2, usr3, usr4, usr5, usr6,
								docnome, adoc, unidade, qtt, uni2qtt,
								iva, ivaincl, tabiva, armazem, cpoc, lordem,
								epv, pv, etiliquido, tiliquido,
								u_bonus, desconto, desc2, desc3, desc4,
								esltt, sltt, u_upc, eslvu, slvu,
								u_stockAct, u_epv1act, marcada,
								u_psicont, u_bencont,
								ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora )
							Values
								('<<ALLTRIM(lcFnStamp)>>', '<<ALLTRIM(lcFoStamp)>>', '<<ALLTRIM(uCrsConfFac.fnstamp)>>', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102),
								 '<<ALLTRIM(lcRef)>>', '', '<<ALLTRIM(uCrsConfFac.codigo)>>', '<<ALLTRIM(uCrsConfFac.design)>>',
								 <<IIF(uCrsConfFac.stns, 1, 0)>>, '<<ALLTRIM(uCrsConfFac.usr1)>>', '<<ALLTRIM(uCrsConfFac.usr2)>>', '<<ALLTRIM(uCrsConfFac.usr3)>>', '<<ALLTRIM(uCrsConfFac.usr4)>>', '<<ALLTRIM(uCrsConfFac.usr5)>>', '<<ALLTRIM(uCrsConfFac.usr6)>>',
								 '<<ALLTRIM(lcDocNome)>>', '<<astr(lcNrDoc)>>', '<<ALLTRIM(uCrsConfFac.unidade)>>', <<lcQtf>>, <<lcQtAltern>>,
								 <<uCrsConfFac.iva>>, <<IIF(uCrsConfFac.ivaincl,1,0)>>, <<uCrsConfFac.tabiva>>, <<uCrsConfFac.armazem>>, <<uCrsConfFac.cpoc>>, <<lcOrdem>>,
								 <<Round(lcPcpG,2)>>, <<Round(lcPcpG*200.482,2)>>, <<lcTotalFinal>>, <<lcTotalFinal*200.482>>,
								 <<uCrsConfFac.bonusf>>, <<uCrsConfFac.desc1f>>, <<uCrsConfFac.desc2f>>, <<uCrsConfFac.desc3f>>, <<uCrsConfFac.desc4f>>,
								 <<lcTotalFinal>>, <<lcTotalFinal*200.482>>, <<lcUpc>>, <<lcTotalFinal / lcQtf>>, <<(lcTotalFinal / lcQtf) * 200.482>>,
								 <<uCrsConfFac.u_stockact>>, <<uCrsConfFac.u_epv1act>>, 1,
								 <<uCrsConfFac.u_psicont>>, <<uCrsConfFac.u_bencont>>,
								 '<<m_chinis>>', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8), '<<m_chinis>>', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8) )
						ENDTEXT
						IF !uf_gerais_actgrelha("", "", lcSql)
							uf_perguntalt_chama("OCORREU UM ERRO A CRIAR A GUIA DE ACERTO. POR FAVOR CONTACTE O SUPORTE."+CHR(10)+"Cod: FN","OK","",16)
							RETURN .f.
						ENDIF
						
						lcOrdem = lcOrdem + 1000
					ENDFOR

					SET HOURS TO 24
					Atime=ttoc(datetime()+(difhoraria*3600),2)
					Astr=left(Atime,8)
					
					&& marcar as linhas como fechadas
					SELECT uCrsConfFac
					TEXT TO lcSql NOSHOW textmerge
						UPDATE
							fn
						SET
							marcada = 1,
							usrdata = convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102),
							usrhora = '<<Astr>>'
						where
							(fn.fnstamp='<<ALLTRIM(lcFnStamp)>>' or fn.fnstamp='<<ALLTRIM(uCrsConfFac.fnstamp)>>' or fnstamp='<<ALLTRIM(uCrsConfFac.ofnstamp)>>')
					ENDTEXT
					IF !uf_gerais_actgrelha("", "", lcSql)
						uf_perguntalt_chama("OCORREU UM ERRO A ACTUALIZAR ESTADO DA LINHA NA FACTURA. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
					ENDIF
					**********************************
			
					** actualizar pcl da ficha do produto **
						** Nota: n�o deve actualizar PCT, este s� deve ser actualizado pela factura **
					IF lcUpc > 0
						IF uf_gerais_actgrelha("", "uCrsChkUpPcl2", [select fo.data, convert(numeric,fo.adoc) as nr from fn (nolock) inner join fo (nolock) on fo.fostamp=fn.fostamp where fo.doccode=102 and ISNUMERIC(fo.adoc)=1 and fn.fnstamp=']+ALLTRIM(uCrsConfFac.ofnstamp)+['])
							IF RECCOUNT("uCrsChkUpPcl2")>0
								IF uf_gerais_actgrelha("", "uCrsChkUpPcl", [select ISNULL((MAX(convert(numeric,fo.adoc))),0) as nr from fo (nolock) inner join fo2 (nolock) on fo2.fo2stamp=fo.fostamp inner join fn (nolock) on fn.fostamp=fo.fostamp where fn.ref=']+ALLTRIM(lcRef)+[' and fo.doccode=102 and ISNUMERIC(fo.adoc)=1 and fo2.u_ajuste=0 and fo.data >= ']+uf_gerais_getdate(uCrsChkUpPcl2.data, "SQL")+['])	
									IF RECCOUNT("uCrsChkUpPcl")>0
										IF uCrsChkUpPcl.nr==uCrsChkUpPcl2.nr && atualizar apenas se a guia for a �ltima
											TEXT TO lcSql NOSHOW textmerge
												UPDATE
													ST
												SET 
													--ST.epcusto=<<Round(lcPcpG,2)>>, st.pcusto=<<Round(lcPcpG,2)*200.482>>,
													st.epcult=<<lcUpc>>, st.pcult=<<lcUpc*200.482>>
												where
													st.ref='<<ALLTRIM(lcRef)>>'
											ENDTEXT
											IF !uf_gerais_actgrelha("", "", lcSql)
												uf_perguntalt_chama("OCORREU UM ERRO A ACTUALIZAR PRE�OS DE CUSTO NO PRODUTO. POR FAVOR CONTACTE O SUPORTE.", "OK","",16)
											ENDIF
											
											** Actualizar Margens **
											uf_documentos_updateMargensFoSt(lcRef)
											************************
										ENDIF
									ENDIF
									fecha("uCrsChkUpPcl2")
								ENDIF
							ENDIF
							fecha("uCrsChkUpPcl")
						ENDIF
					ENDIF
					**********************************************
			
					** Eliminar Registo pendente da tabela cfs se existir **
					SELECT uCrsConfFac
					IF uf_gerais_actgrelha("", "uCrsCheckCfs", [select * from B_confFacSave cfs (nolock) where cfs.fnstamp=']+ALLTRIM(uCrsConfFac.fnstamp)+['])
						IF reccount("uCrsCheckCfs")>0
							TEXT TO lcSql NOSHOW textmerge
								DELETE
									B_confFacSave
								where
									B_confFacSave.fnstamp='<<ALLTRIM(uCrsConfFac.fnstamp)>>'
							ENDTEXT
							IF !uf_gerais_actgrelha("", "", lcSql)
								***
							ENDIF
						ENDIF
						fecha("uCrsCheckCfs")
					ENDIF
					*******************************************************
			
				ENDIF
				fecha("uCrsValidaGuiaExist3")
			ENDIF

			lcExisteInv = 0
			
			SELECT uCrsConfFac
		ENDSCAN
		************************************
	
		SELECT uCrsFornGuiasFoDistinctPCF
	ENDSCAN
	*********************************************************
	
	** actualiza ultimo registo **
	uf_gerais_gravaUltRegisto('bo', lcBoStamp)
	uf_gerais_gravaUltRegisto('fo', lcFoStamp)
			
	IF lcOrdem > 10000
		uf_perguntalt_chama("GUIA(s) DE ACERTO DE VALORES CRIADA COM SUCESSO.","OK","",64)
	ENDIF
	*********************************************************

	** FIM **
	IF USED("uCrsFornGuiasFoDistinctPCF")
		fecha("uCrsFornGuiasFoDistinctPCF")
	ENDIF
ENDFUNC



**
FUNCTION uf_conffact_criaPedDebPCF	
	** verificar se a factura lan�a em stock **
	LOCAL lcFacLs
	STORE .f. TO lcFacLs
	
	SET POINT TO "."
	
	IF uf_gerais_actgrelha("", "uCrsFLS", [select FOLANSL from cm1 (nolock) where cm=55])
		IF RECCOUNT("uCrsFLS")>0
			IF uCrsFLS.folansl==.t.
				lcFacLs = .t.
			ENDIF
		ENDIF
	ENDIF
	*******************************************
	
	
	IF !lcFacLs && se a factura n�o lan�ar em stock
		** Guardar c�digo e nome da guia de acerto **
		LOCAL lcDocCode, lcDocNome
		lcDocCode = 38 && c�digo do documento
		
		IF uf_gerais_actgrelha("", "uCrsTsCode", [select nmdos from ts (nolock) where ndos=38])
			IF RECCOUNT("uCrsTsCode")>0
				lcDocNome = uCrsTsCode.nmdos && nome do documento
			ENDIF
			fecha("uCrsTsCode")
		ELSE
			uf_perguntalt_chama("O DOCUMENTO [PEDIDO DE REGULARIZA��O] N�O EST� A SER ENCONTRADO. O DOCUMENTO N�O SER� CRIADO.","OK","",48)
			RETURN .f.
		ENDIF
		***************************************
	
		** Verificar se existem novos pedidos de regulariza��o (d�bito) a ser lan�ados **
		LOCAL lcValidaNdd
		STORE .f. TO lcValidaNdd
		
		SELECT uCrsConfFac
		GO TOP
		SCAN FOR uCrsConfFac.marcadaf==.t. AND uCrsConfFac.totalf>uCrsConfFac.totalfac
			IF uf_gerais_actgrelha("", "uCrsValidaPcExist", [select fnstamp from bi2 (nolock) inner join bi (nolock) on bi.bistamp=bi2.bi2stamp where bi.ndos=38 and bi2.fnstamp=']+Alltrim(uCrsConfFac.fnstamp)+['])
				IF !RECCOUNT("uCrsValidaPcExist")>0 && se o produto ainda n�o tiver originado um pedido de d�bito
					lcValidaNdd = .t.
				ENDIF
				fecha("uCrsValidaPcExist")
			ENDIF
		ENDSCAN
	
		IF !lcValidaNdd
			RETURN
		ENDIF
		************************************************************
		
		** Determinar armazem para cabe�alho **
		LOCAL lcArmazem
		lcArmazem = 0
		SELECT uCrsE1
		lcArmazem = uCrsE1.armazem1
		***************************************
		
		
		** PREPARAR DADOS/TOTAIS **


		*** Calcular n�mero de fornecedores = n�mero de documentos **
		SELECT uCrsConfFac
		GO top
		SCAN FOR uCrsConfFac.marcadaf==.t. AND uCrsConfFac.totalf>uCrsConfFac.totalfac
			IF uf_gerais_actgrelha("", "uCrsValidaPcExist2", [select fnstamp from bi2 (nolock) inner join bi (nolock) on bi.bistamp=bi2.bi2stamp where bi.ndos=38 and bi2.fnstamp=']+Alltrim(uCrsConfFac.fnstamp)+['])
				IF !RECCOUNT("uCrsValidaPcExist2")>0 && se o produto ainda n�o tiver originado um pedido de d�bito
					** Guardar fornecedores num cursor para divis�o de documentos
					If !Used("uCrsFornPcPCF")
						Create Cursor uCrsFornPcPCF (no n(10), estab n(5))
						Create Cursor uCrsFornPcDistinctPCF (no n(10), estab n(5))
					Endif
					If uf_gerais_actgrelha("", "uCrsGetFornPCF", [select no, estab from fo (nolock) where fostamp=']+Alltrim(uCrsConfFac.fostamp)+['])
						If Reccount("uCrsGetFornPCF")>0
							Insert Into uCrsFornPcPCF (no, estab) Values (uCrsGetFornPCF.no, uCrsGetFornPCF.estab)
						Endif
						Fecha("uCrsGetFornPCF")
					ENDIF
					******************************************************************
				ENDIF
				fecha("uCrsValidaPcExist2")
			ENDIF
			
			SELECT uCrsConfFac
		ENDSCAN
		
		** guardar em cursor apenas os fornecedores unicos **
		If used("uCrsFornPcPCF")
			Select distinct no, estab From uCrsFornPcPCF Into Cursor uCrsFornPcDistinctPCF
			fecha("uCrsFornPcPCF")
		Endif
		*****************************************************

		
		** Vari�veis para os Cabe�alhos PEDIDO DE D�BITO **
		LOCAL valorIva1, valorIva2, valorIva3, valorIva4, valorIva5, lcTotalIva, totalComIva, totalSemIva, totalNservico, lcTotalFinal
		LOCAL totalSemIva1, totalSemIva2, totalSemIva3, totalSemIva4, totalSemIva5, lcQtSum
		LOCAL lcSql, lcBoStamp, lcNrDoc, lcDocCont, lcCount
		STORE 0 TO lcCount
		LOCAL lcNome, lcNo, lcEstab, lcPais, lcLocal, lcCodPost, lcNcont, lcMorada, lcMoeda, lcTipo, lcCcusto
		** vari�veis para as linhas
		LOCAL lcBiStamp, lcCountS, lcOrdem, lcQtAltern, lcRef, lcQt, lcStock, lcFamilia, lcTotalLiq, lcValInsertLin
		****************************************************
		
		SELECT uCrsFornPcDistinctPCF
		GO TOP
		SCAN
			** inicializar vari�veis **
			STORE 0 TO valorIva1, valorIva2, valorIva3, valorIva4, valorIva5, lcTotalIva, totalComIva, totalSemIva, totalNservico, lcTotalFinal
			STORE 0 TO totalSemIva1, totalSemIva2, totalSemIva3, totalSemIva4, totalSemIva5, lcQtSum
			STORE 0 TO lcNrDoc, lcDocCont
			STORE '' TO lcSql, lcBoStamp
			STORE '' TO lcNome, lcLocal, lcCodPost, lcNcont, lcMorada, lcMoeda, lcTipo, lcCcusto
			STORE 0 TO lcNo, lcEstab
			STORE 1 TO lcPais
			***************************
			
			** Guardar n�mero do documento **
			IF uf_gerais_actgrelha("", "uCrsNrMax", [select ISNULL((MAX(convert(numeric,obrano))),0) as nr from bo (nolock) where bo.ndos=38 and YEAR(bo.dataobra)=YEAR(dateadd(HOUR, str(difhoraria), getdate()))])
				IF RECCOUNT("uCrsNrMax")>0
					lcNrDoc = uCrsNrMax.nr + 1
				ENDIF
				fecha("uCrsNrMax")
			ELSE
				uf_perguntalt_chama("O DOCUMENTO [PEDIDO DE REGULARIZA��O] N�O EST� A SER ENCONTRADO. O DOCUMENTO N�O SER� CRIADO.","OK","",48)
				RETURN
			ENDIF
			*********************************

			** Guarda Contador por tipo de documento e origem **
			IF uf_gerais_actgrelha("", "uCrsContDoc", [select ISNULL(MAX(bo2.u_doccont),0) as doccont from bo2 (nolock) inner join bo (nolock) on bo.bostamp=bo2.bo2stamp where bo.ndos=38])
				IF RECCOUNT("uCrsContDoc")>0
					lcDocCont = uCrsContDoc.doccont + 1
				ELSE
					lcDocCont = 1
				ENDIF
				fecha("uCrsContDoc")
			ENDIF
			****************************************************

			** Guardar Stamp Para Cabe�alho **
			lcCount = lcCount + 1
			*lcBoStamp = u_stamp(lcCount)
			lcBoStamp = uf_gerais_stamp()
			
			Select uCrsConfFac
			GO top
			SCAN FOR uCrsConfFac.no==uCrsFornPcDistinctPCF.no AND uCrsConfFac.estab==uCrsFornPcDistinctPCF.estab AND uCrsConfFac.marcadaf==.t. AND uCrsConfFac.totalf>uCrsConfFac.totalfac
				IF uf_gerais_actgrelha("", "uCrsValidaPcExist2", [select fnstamp from bi2 (nolock) inner join bi (nolock) on bi.bistamp=bi2.bi2stamp where bi.ndos=38 and bi2.fnstamp=']+Alltrim(uCrsConfFac.fnstamp)+['])
					IF !RECCOUNT("uCrsValidaPcExist2")>0 && se o produto ainda n�o tiver originado um pedido de cr�dito
						** total quantidades **
						IF uCrsConfFac.qtfac < uCrsConfFac.qtf
							lcQtSum		= lcQtSum + (uCrsConfFac.qtf - uCrsConfFac.qtfac)
						ELSE
							lcQtSum		= lcQtSum + 1
						ENDIF
						** total por linha **
						lcTotalFinal	= uCrsConfFac.totalf-uCrsConfFac.totalfac && uCrsConfFac.upcfac * (uCrsConfFac.qtf - uCrsConfFac.qtfac)
						
						DO CASE
							CASE uCrsConfFac.tabiva=1
								IF uCrsConfFac.ivaincl
									valorIva1=valorIva1+(lcTotalFinal /(uCrsConfFac.iva/100+1))
								ELSE
									valorIva1=valorIva1+(lcTotalFinal *(uCrsConfFac.iva/100))
								ENDIF
							CASE uCrsConfFac.tabiva=2
								IF uCrsConfFac.ivaincl
									valorIva2=valorIva2+(lcTotalFinal /(uCrsConfFac.iva/100+1))
								ELSE
									valorIva2=valorIva2+(lcTotalFinal *(uCrsConfFac.iva/100))
								ENDIF
							CASE uCrsConfFac.tabiva=3
								IF uCrsConfFac.ivaincl
									valorIva3=valorIva3+(lcTotalFinal /(uCrsConfFac.iva/100+1))
								ELSE
									valorIva3=valorIva3+(lcTotalFinal *(uCrsConfFac.iva/100))
								ENDIF
							CASE uCrsConfFac.tabiva=4
								IF uCrsConfFac.ivaincl
									valorIva4=valorIva4+(lcTotalFinal /(uCrsConfFac.iva/100+1))
								ELSE
									valorIva4=valorIva4+(lcTotalFinal *(uCrsConfFac.iva/100))
								ENDIF
							CASE uCrsConfFac.tabiva=5
								IF uCrsConfFac.ivaincl
									valorIva5=valorIva5+(lcTotalFinal /(uCrsConfFac.iva/100+1))
								ELSE
									valorIva5=valorIva5+(lcTotalFinal *(uCrsConfFac.iva/100))
								ENDIF
						ENDCASE
											
						IF uCrsConfFac.ivaincl
							totalComIva=totalComIva + lcTotalFinal
							totalSemIva=totalSemIva + (lcTotalFinal /(uCrsConfFac.iva/100+1))
						ELSE
							totalComIva=totalComIva + (lcTotalFinal *(uCrsConfFac.iva/100+1))
							totalSemIva=totalSemIva + (lcTotalFinal)
						ENDIF

						IF !uCrsConfFac.stns
							IF uCrsConfFac.ivaincl
								totalNservico=totalNservico + ( lcTotalFinal /(uCrsConfFac.iva/100+1) )
							ELSE
								totalNservico=totalNservico + lcTotalFinal
							ENDIF
						ENDIF
						
						lcNome		= uCrsConfFac.nome
						lcNo		= uCrsConfFac.no
						lcEstab		= uCrsConfFac.estab
						lcTipo		= uCrsConfFac.tipo
						lcLocal		= uCrsConfFac.local
						lcCodPost	= uCrsConfFac.codpost
						lcNcont		= uCrsConfFac.ncont
						lcMorada	= uCrsConfFac.morada
						lcMoeda		= uCrsConfFac.moeda
						lcPais		= uCrsConfFac.pais
						lcCcusto	= uCrsConfFac.ccusto
					ENDIF
					
					fecha("uCrsValidaPcExist2")
				ENDIF
				
				STORE 0 TO lcTotalFinal
				SELECT uCrsConfFac
			Endscan
					
			lcTotalIva=valorIva1+valorIva2+valorIva3+valorIva4+valorIva5
			****************************************

			
			** GRAVAR CABE�ALHOS **
			SELECT uCrsConfFac
			*GO top
		
			TEXT TO lcSql NOSHOW textmerge
				Insert into bo
					(bostamp, ndos, nmdos, obrano, dataobra,
					nome, nome2, [no], estab, morada, [local], codpost, tipo, ncont,
					etotaldeb, dataopen, boano, ecusto, etotal, moeda, memissao, origem, site,
					vendedor, vendnm, obs, ccusto,
					sqtt14, ebo_1tvall, ebo_2tvall, ebo_totp1, ebo_totp2,
					ebo11_bins, ebo12_bins, ebo21_bins, ebo22_bins, ebo31_bins, ebo32_bins, ebo41_bins, ebo42_bins, ebo51_bins, ebo52_bins,
					ebo11_iva, ebo12_iva, ebo21_iva, ebo22_iva, ebo31_iva, ebo32_iva, ebo41_iva, ebo42_iva, ebo51_iva, ebo52_iva,
					ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora)
				Values 
					('<<ALLTRIM(lcBostamp)>>', <<lcDocCode>>, '<<lcDocNome>>', <<lcNrDoc>>, convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102),
					'<<ALLTRIM(lcNome)>>', '', <<lcNo>>, <<lcEstab>>, '<<ALLTRIM(lcMorada)>>', '<<ALLTRIM(lcLocal)>>', '<<ALLTRIM(lcCodPost)>>', '<<ALLTRIM(lcTipo)>>', '<<ALLTRIM(lcNcont)>>',
					<<ROUND(totalSemIva,2)*(-1)>>, convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), year(dateadd(HOUR, <<difhoraria>>, getdate())), <<ROUND(totalSemIva,2)*(-1)>>, <<ROUND(totalComIva,2)*(-1)>>, '<<ALLTRIM(lcMoeda)>>', 'EURO', 'BO', '<<ALLTRIM(mySite)>>',
					<<ch_vendedor>>, '<<ALLTRIM(ch_vendnm)>>', 'Documento gerado Via Confer�ncia de Facturas', '<<lcCcusto>>',
					<<lcQtSum*(-1)>>, <<ROUND(totalSemIva,2)*(-1)>>, <<ROUND(totalSemIva,2)*(-1)>>, <<ROUND(totalSemIva,2)*(-1)>>, <<ROUND(totalSemIva,2)*(-1)>>,
					<<totalSemIva1*(-1)>>, <<totalSemIva1*(-1)>>, <<totalSemIva2*(-1)>>, <<totalSemIva2*(-1)>>, <<totalSemIva3*(-1)>>, <<totalSemIva3*(-1)>>, <<totalSemIva4*(-1)>>, <<totalSemIva4*(-1)>>, <<totalSemIva5*(-1)>>, <<totalSemIva5*(-1)>>,
					<<valorIva1*(-1)>>, <<valorIva1*(-1)>>, <<valorIva2*(-1)>>, <<valorIva2*(-1)>>, <<valorIva3*(-1)>>, <<valorIva3*(-1)>>, <<valorIva4*(-1)>>, <<valorIva4*(-1)>>, <<valorIva5*(-1)>>, <<valorIva5*(-1)>>,
					'ADM', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8), 'ADM', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8) )
			ENDTEXT
			IF !uf_gerais_actgrelha("", "", lcSql)
				uf_perguntalt_chama("OCORREU UM ERRO A GERAR DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				RETURN .f.
			ENDIF
			lcSql=''
		
			TEXT TO lcSql NOSHOW textmerge
				Insert into bo2
					(bo2stamp, autotipo, pdtipo, etotalciva, etotiva, u_class, armazem,
					ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora)
				Values
					('<<ALLTRIM(lcBostamp)>>', 1, 1, <<ROUND(totalComIva,2)>>, <<ROUND(lcTotalIva,2)>>, 'Pedido de D�bito', <<lcArmazem>>,
					'ADM', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8), 'ADM', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8) )
			ENDTEXT
			IF !uf_gerais_actgrelha("", "", lcSql)
				uf_perguntalt_chama("OCORREU UM ERRO A GERAR DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				RETURN .f.
			ENDIF
			***********************
			*************************** && FIM DOS TOTAIS
		
			** Gravar Linhas **
			STORE "" TO lcBiStamp, lcRef
			STORE 0 TO lcCountS, lcOrdem, lcQtAltern, lcQt, lcStock, lcFamilia, lcTotalLiq
			STORE .t. TO lcValInsertLin
		
			SELECT uCrsConfFac
			GO TOP
			SCAN FOR uCrsConfFac.no==uCrsFornPcDistinctPCF.no AND uCrsConfFac.estab==uCrsFornPcDistinctPCF.estab AND uCrsConfFac.marcadaf==.t. AND uCrsConfFac.totalf > uCrsConfFac.totalfac
				IF uf_gerais_actgrelha("", "uCrsValidaPcExist2", [select fnstamp from bi2 (nolock) inner join bi (nolock) on bi.bistamp=bi2.bi2stamp where bi.ndos=38 and bi2.fnstamp=']+Alltrim(uCrsConfFac.fnstamp)+['])
					IF !RECCOUNT("uCrsValidaPcExist2")>0
						** Guardar Stamp das linhas **
						lcCountS = lcCountS + 1
						*lcBiStamp = u_stamp(lcCountS)					
						lcBiStamp = uf_gerais_stamp()
						
						SELECT uCrsConfFac
						
						** Contador para as linhas
						lcOrdem = lcOrdem + 10000
						** Calcular quantidade, unidade alternativa e total liquido
						IF uCrsConfFac.qtfac < uCrsConfFac.qtf
							lcQt = uCrsConfFac.qtf-uCrsConfFac.qtfac
						ELSE
							lcQt = 1
						ENDIF
						lcQtAltern	= INT((lcQt) * uCrsConfFac.conversao)
						lcTotalLiq	= uCrsConfFac.totalf-uCrsConfFac.totalfac
						** guardar refer�ncia **
						lcRef = IIF(EMPTY(ALLTRIM(uCrsConfFac.ref)), uCrsConfFac.oref, uCrsConfFac.ref)
						** guardar stock actual, familia **
						IF uf_gerais_actgrelha("", "uCrsStockAct", [select stock, familia from st (nolock) where st.ref=']+ALLTRIM(lcRef)+['])
							IF reccount("uCrsStockAct")>0
								lcStock 	= uCrsStockAct.stock
								lcFamilia	= uCrsStockAct.familia
							ENDIF
							fecha("uCrsStockAct")
						ENDIF
						
						** Preencher Linhas da Bi **
						TEXT TO lcSql NOSHOW textmerge
							Insert into bi
									(bistamp, bostamp, nmdos, obrano, ref, codigo,
									design, qtt, qtt2, uni2qtt,
									u_stockact, iva, tabiva, armazem, stipo, ndos, cpoc, familia,
									no, nome, local, morada, codpost, ccusto,
									epu, edebito, eprorc, epcusto, ettdeb, ecustoind, edebitoori, u_upc,
									rdata, dataobra, dataopen, resfor, lordem,
									lobs,
									ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora
								)
							Values
								('<<ALLTRIM(lcBiStamp)>>', '<<ALLTRIM(lcBoStamp)>>', '<<ALLTRIM(lcDocNome)>>', <<lcNrDoc>>, '<<ALLTRIM(lcRef)>>', '<<ALLTRIM(uCrsConfFac.codigo)>>',
								'<<ALLTRIM(uCrsConfFac.design)>>', <<lcQt*(-1)>>, 0, <<lcQtAltern>>,
								<<lcStock>>, <<uCrsConfFac.iva>>, <<uCrsConfFac.tabiva>>, <<uCrsConfFac.armazem>>, 4, <<lcDocCode>>, <<uCrsConfFac.cpoc>>, '<<lcFamilia>>',
								<<lcNo>>, '<<ALLTRIM(lcNome)>>', '<<ALLTRIM(lcLocal)>>', '<<ALLTRIM(lcMorada)>>', '<<ALLTRIM(lcCodPost)>>', '<<lcCcusto>>',
								<<(lcTotalLiq / lcQt)*(-1)>>, <<(lcTotalLiq / lcQt)*(-1)>>, <<(lcTotalLiq / lcQt)*(-1)>>, <<(lcTotalLiq / lcQt)*(-1)>>, <<lcTotalLiq*(-1)>>, <<(lcTotalLiq / lcQt)*(-1)>>, <<(lcTotalLiq / lcQt)*(-1)>>, <<(lcTotalLiq / lcQt)*(-1)>>,
								convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), 0, <<lcOrdem>>,
								'Referente � Factura N�. '+'<<ALLTRIM(uCrsConfFac.adoc)>>',
								'ADM', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8), 'ADM', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8)
								)
						ENDTEXT
						IF !uf_gerais_actgrelha("", "", lcSql)
							lcValInsertLin = .f.
						ENDIF
						
						** Preencher Linhas da Bi2 **
						TEXT TO lcSql NOSHOW textmerge
							Insert into bi2
								(bi2stamp, bostamp, fnstamp, morada, local, codpost)
							Values
								('<<ALLTRIM(lcBiStamp)>>', '<<ALLTRIM(lcBoStamp)>>', '<<ALLTRIM(uCrsConfFac.fnstamp)>>', '<<ALLTRIM(lcMorada)>>', '<<ALLTRIM(lcLocal)>>', '<<ALLTRIM(lcCodPost)>>')
						ENDTEXT
						IF !uf_gerais_actgrelha("", "", lcSql)
							lcValInsertLin = .f.
						ENDIF
					ENDIF
				ENDIF
				
				STORE 0 TO lcQtAltern, lcQt, lcStock, lcFamilia, lcTotalLiq
				SELECT uCrsConfFac
			ENDSCAN
			******************* && fim das linhas
			
			SELECT uCrsFornPcDistinctPCF
		ENDSCAN
	
		** actualiza ultimo registo **
		IF !EMPTY(lcBoStamp)
			uf_gerais_gravaUltRegisto('bo', lcBoStamp)
		ENDIF
	
		IF !lcValInsertLin
			uf_perguntalt_chama("OCORREU UM ERRO A GRAVAR LINHAS DO PEDIDO DE D�BITO A FORNECEDOR. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		ELSE
			IF lcValidaNdd==.t.
				uf_perguntalt_chama("PEDIDO(s) DE D�BITO CRIADO(s) COM SUCESSO.","OK","",64)
			ENDIF
		ENDIF
		
		IF USED("uCrsFornPcDistinctPCF")
			fecha("uCrsFornPcDistinctPCF")
		ENDIF
	ENDIF && fim do caso em que a factura n�o mexe em stock
ENDFUNC



**
FUNCTION uf_conffact_AfterRowColChange
	IF USED("uCrsConfFac")
		SELECT uCrsConfFac
		CONFFACT.containerDados.lbldocOrig.caption = ALLTRIM(uCrsConfFac.oDocnome)
		
		IF EMPTY(uCrsConfFac.ref)
			CONFFACT.containerDados.ref.value = ALLTRIM(uCrsConfFac.oref)
		ELSE
			CONFFACT.containerDados.ref.value = ALLTRIM(uCrsConfFac.ref)
		ENDIF
		
		conffact.containerDados.refresh
	ENDIF
ENDFUNC



**
FUNCTION uf_conffact_criaPedCredPCF	
	** verificar se a factura lan�a em stock **
	LOCAL lcFacLs
	STORE .f. TO lcFacLs
	
	SET POINT TO "."
	
	IF uf_gerais_actgrelha("", "uCrsFLS", [select FOLANSL from cm1 (nolock) where cm=55])
		IF RECCOUNT("uCrsFLS")>0
			IF uCrsFLS.folansl==.t.
				lcFacLs = .t.
			ENDIF
		ENDIF
	ENDIF
	*******************************************
	
	
	IF !lcFacLs && se a factura n�o lan�ar em stock
		** Guardar c�digo e nome da guia de acerto **
		LOCAL lcDocCode, lcDocNome
		lcDocCode = 38 && c�digo do documento
	
		IF uf_gerais_actgrelha("", "uCrsTsCode", [select nmdos from ts (nolock) where ndos=38])
			IF RECCOUNT("uCrsTsCode")>0
				lcDocNome = uCrsTsCode.nmdos && nome do documento
			ENDIF
			fecha("uCrsTsCode")
		ELSE
			uf_perguntalt_chama("O DOCUMENTO [PEDIDO DE REGULARIZA��O] N�O EST� A SER ENCONTRADO. O DOCUMENTO N�O SER� CRIADO.","OK","",16)
			RETURN .f.
		ENDIF
		***************************************
	
		** Verificar se existem novos pedidos de regulariza��o (cr�dito) a ser lan�ados **
		LOCAL lcValidaNd
		STORE .f. TO lcValidaNd
		
		SELECT uCrsConfFac
		GO TOP
		SCAN FOR uCrsConfFac.marcadaf==.t. AND (uCrsConfFac.totalf<uCrsConfFac.totalfac OR (ROUND(uCrsConfFac.totalf,2)==ROUND(uCrsConfFac.totalfac,2) AND uCrsConfFac.qtf<=0 AND uCrsConfFac.qtfac>0))
			IF uf_gerais_actgrelha("", "uCrsValidaPcExist", [select fnstamp from bi2 (nolock) inner join bi (nolock) on bi.bistamp=bi2.bi2stamp where bi.ndos=38 and bi2.fnstamp=']+Alltrim(uCrsConfFac.fnstamp)+['])
				IF !RECCOUNT("uCrsValidaPcExist")>0 && se o produto ainda n�o tiver originado um pedido de cr�dito
					lcValidaNd = .t.
				ENDIF
				fecha("uCrsValidaPcExist")
			ENDIF
		ENDSCAN
	
		IF !lcValidaNd
			RETURN
		ENDIF
		************************************************************
			
		** Determinar armazem para cabe�alho **
		LOCAL lcArmazem
		lcArmazem = 0
		SELECT uCrsE1
		lcArmazem = uCrsE1.armazem1
		***************************************
		
		
		** PREPARAR DADOS/TOTAIS **


		*** Calcular n�mero de fornecedores = n�mero de documentos **
		SELECT uCrsConfFac
		GO TOP
		SCAN FOR uCrsConfFac.marcadaf==.t. AND (uCrsConfFac.totalf<uCrsConfFac.totalfac OR (ROUND(uCrsConfFac.totalf,2)==ROUND(uCrsConfFac.totalfac,2) AND uCrsConfFac.qtf<=0 AND uCrsConfFac.qtfac>0))
			IF uf_gerais_actgrelha("", "uCrsValidaPcExist2", [select fnstamp from bi2 (nolock) inner join bi (nolock) on bi.bistamp=bi2.bi2stamp where bi.ndos=38 and bi2.fnstamp=']+Alltrim(uCrsConfFac.fnstamp)+['])
				IF !RECCOUNT("uCrsValidaPcExist2")>0 && se o produto ainda n�o tiver originado um pedido de cr�dito
					** Guardar fornecedores num cursor para divis�o de documentos
					If !Used("uCrsFornPcPCF")
						Create Cursor uCrsFornPcPCF (no n(10), estab n(5))
						Create Cursor uCrsFornPcDistinctPCF (no n(10), estab n(5))
					Endif
					If uf_gerais_actgrelha("", "uCrsGetFornPCF", [select no, estab from fo (nolock) where fostamp=']+Alltrim(uCrsConfFac.fostamp)+['])
						If Reccount("uCrsGetFornPCF")>0
							Insert Into uCrsFornPcPCF (no, estab) Values (uCrsGetFornPCF.no, uCrsGetFornPCF.estab)
						Endif
						Fecha("uCrsGetFornPCF")
					ENDIF
					******************************************************************
				ENDIF
				fecha("uCrsValidaPcExist2")
			ENDIF
			
			SELECT uCrsConfFac
		ENDSCAN
		
		** guardar em cursor apenas os fornecedores unicos **
		If used("uCrsFornPcPCF")
			Select distinct no, estab From uCrsFornPcPCF Into Cursor uCrsFornPcDistinctPCF
			fecha("uCrsFornPcPCF")
		Endif
		*****************************************************

		
		** Vari�veis para os Cabe�alhos PEDIDO DE CR�DITO **
		LOCAL valorIva1, valorIva2, valorIva3, valorIva4, valorIva5, lcTotalIva, totalComIva, totalSemIva, totalNservico, lcTotalFinal
		LOCAL totalSemIva1, totalSemIva2, totalSemIva3, totalSemIva4, totalSemIva5, lcQtSum
		LOCAL lcSql, lcBoStamp, lcNrDoc, lcDocCont, lcCount
		STORE 0 TO lcCount
		LOCAL lcNome, lcNo, lcEstab, lcPais, lcLocal, lcCodPost, lcNcont, lcMorada, lcMoeda, lcTipo, lcCcusto
		** vari�veis para as linhas
		LOCAL lcBiStamp, lcCountS, lcOrdem, lcQtAltern, lcRef, lcQt, lcStock, lcFamilia, lcTotalLiq, lcValInsertLin
		****************************************************
		
		SELECT uCrsFornPcDistinctPCF
		GO TOP
		SCAN
			** inicializar vari�veis **
			STORE 0 TO valorIva1, valorIva2, valorIva3, valorIva4, valorIva5, lcTotalIva, totalComIva, totalSemIva, totalNservico, lcTotalFinal
			STORE 0 TO totalSemIva1, totalSemIva2, totalSemIva3, totalSemIva4, totalSemIva5, lcQtSum
			STORE 0 TO lcNrDoc, lcDocCont
			STORE '' TO lcSql, lcBoStamp
			STORE '' TO lcNome, lcLocal, lcCodPost, lcNcont, lcMorada, lcMoeda, lcTipo, lcCcusto
			STORE 0 TO lcNo, lcEstab
			STORE 1 TO lcPais
			***************************
			
			** Guardar n�mero do documento **
			IF uf_gerais_actgrelha("", "uCrsNrMax", [select ISNULL((MAX(convert(numeric,obrano))),0) as nr from bo (nolock) where bo.ndos=38 and YEAR(bo.dataobra)=YEAR(dateadd(HOUR, STR(difhoraria), getdate()))])
				IF RECCOUNT("uCrsNrMax")>0
					lcNrDoc = uCrsNrMax.nr + 1
				ENDIF
				fecha("uCrsNrMax")
			ELSE
				uf_perguntalt_chama("O DOCUMENTO [PEDIDO DE CR�DITO] N�O EST� A SER ENCONTRADO. O DOCUMENTO N�O SER� CRIADO.","OK","",48)
				RETURN .f.
			ENDIF
			*********************************

			** Guarda Contador por tipo de documento e origem **
			IF uf_gerais_actgrelha("", "uCrsContDoc", [select ISNULL(MAX(bo2.u_doccont),0) as doccont from bo2 (nolock) inner join bo (nolock) on bo.bostamp=bo2.bo2stamp where bo.ndos=38])
				IF RECCOUNT("uCrsContDoc")>0
					lcDocCont = uCrsContDoc.doccont + 1
				ELSE
					lcDocCont = 1
				ENDIF
				fecha("uCrsContDoc")
			ENDIF
			****************************************************

			** Guardar Stamp Para Cabe�alho **
			lcCount = lcCount + 1
			*lcBoStamp = u_stamp(lcCount)
			lcBoStamp = uf_gerais_stamp()
			
			Select uCrsConfFac
			GO TOP
			SCAN FOR uCrsConfFac.no==uCrsFornPcDistinctPCF.no AND uCrsConfFac.estab==uCrsFornPcDistinctPCF.estab AND uCrsConfFac.marcadaf==.t. ;
				AND (uCrsConfFac.totalf<uCrsConfFac.totalfac OR (ROUND(uCrsConfFac.totalf,2)==ROUND(uCrsConfFac.totalfac,2) AND uCrsConfFac.qtf<=0 AND uCrsConfFac.qtfac>0))

				IF uf_gerais_actgrelha("", "uCrsValidaPcExist2", [select fnstamp from bi2 (nolock) inner join bi (nolock) on bi.bistamp=bi2.bi2stamp where bi.ndos=38 and bi2.fnstamp=']+Alltrim(uCrsConfFac.fnstamp)+['])
					IF !RECCOUNT("uCrsValidaPcExist2")>0 && se o produto ainda n�o tiver originado um pedido de cr�dito
						IF uCrsConfFac.qtfac > uCrsConfFac.qtf
							IF uCrsConfFac.qtf<0
								lcQtSum		= lcQtSum + (uCrsConfFac.qtfac)
							ELSE
								lcQtSum		= lcQtSum + (uCrsConfFac.qtfac - uCrsConfFac.qtf)
							ENDIF
						ELSE
							lcQtSum		= lcQtSum + 1
						ENDIF
						
						IF uCrsConfFac.qtf <0
							lcTotalFinal	= uCrsConfFac.totalfac
						ELSE
							lcTotalFinal	= uCrsConfFac.totalfac-uCrsConfFac.totalf && uCrsConfFac.upcfac * (uCrsConfFac.qtfac - uCrsConfFac.qtf)
						ENDIF
							
						
						DO CASE
							CASE uCrsConfFac.tabiva=1
								IF uCrsConfFac.ivaincl
									valorIva1=valorIva1+(lcTotalFinal /(uCrsConfFac.iva/100+1))
								ELSE
									valorIva1=valorIva1+(lcTotalFinal *(uCrsConfFac.iva/100))
								ENDIF
							CASE uCrsConfFac.tabiva=2
								IF uCrsConfFac.ivaincl
									valorIva2=valorIva2+(lcTotalFinal /(uCrsConfFac.iva/100+1))
								ELSE
									valorIva2=valorIva2+(lcTotalFinal *(uCrsConfFac.iva/100))
								ENDIF
							CASE uCrsConfFac.tabiva=3
								IF uCrsConfFac.ivaincl
									valorIva3=valorIva3+(lcTotalFinal /(uCrsConfFac.iva/100+1))
								ELSE
									valorIva3=valorIva3+(lcTotalFinal *(uCrsConfFac.iva/100))
								ENDIF
							CASE uCrsConfFac.tabiva=4
								IF uCrsConfFac.ivaincl
									valorIva4=valorIva4+(lcTotalFinal /(uCrsConfFac.iva/100+1))
								ELSE
									valorIva4=valorIva4+(lcTotalFinal *(uCrsConfFac.iva/100))
								ENDIF
							CASE uCrsConfFac.tabiva=5
								IF uCrsConfFac.ivaincl
									valorIva5=valorIva5+(lcTotalFinal /(uCrsConfFac.iva/100+1))
								ELSE
									valorIva5=valorIva5+(lcTotalFinal *(uCrsConfFac.iva/100))
								ENDIF
						ENDCASE
											
						IF uCrsConfFac.ivaincl
							totalComIva=totalComIva + lcTotalFinal
							totalSemIva=totalSemIva + (lcTotalFinal /(uCrsConfFac.iva/100+1))
						ELSE
							totalComIva=totalComIva + (lcTotalFinal *(uCrsConfFac.iva/100+1))
							totalSemIva=totalSemIva + (lcTotalFinal)
						ENDIF

						IF !uCrsConfFac.stns
							IF uCrsConfFac.ivaincl
								totalNservico=totalNservico + ( lcTotalFinal /(uCrsConfFac.iva/100+1) )
							ELSE
								totalNservico=totalNservico + lcTotalFinal
							ENDIF
						ENDIF
						
						lcNome		= uCrsConfFac.nome
						lcNo		= uCrsConfFac.no
						lcEstab		= uCrsConfFac.estab
						lcTipo		= uCrsConfFac.tipo
						lcLocal		= uCrsConfFac.local
						lcCodPost	= uCrsConfFac.codpost
						lcNcont		= uCrsConfFac.ncont
						lcMorada	= uCrsConfFac.morada
						lcMoeda		= uCrsConfFac.moeda
						lcPais		= uCrsConfFac.pais
						lcCcusto	= uCrsConfFac.ccusto
					ENDIF
					fecha("uCrsValidaPcExist2")
				ENDIF
				
				STORE 0 TO lcTotalFinal
				SELECT uCrsConfFac
			ENDSCAN
					
			lcTotalIva=valorIva1+valorIva2+valorIva3+valorIva4+valorIva5
			****************************************

			
			** GRAVAR CABE�ALHOS **
			SELECT uCrsConfFac
			*GO top
		
			TEXT TO lcSql NOSHOW textmerge
				Insert into bo
					(bostamp, ndos, nmdos, obrano, dataobra,
					nome, nome2, [no], estab, morada, [local], codpost, tipo, ncont,
					etotaldeb, dataopen, boano, ecusto, etotal, moeda, memissao, origem, site,
					vendedor, vendnm, obs, ccusto,
					sqtt14, ebo_1tvall, ebo_2tvall, ebo_totp1, ebo_totp2,
					ebo11_bins, ebo12_bins, ebo21_bins, ebo22_bins, ebo31_bins, ebo32_bins, ebo41_bins, ebo42_bins, ebo51_bins, ebo52_bins,
					ebo11_iva, ebo12_iva, ebo21_iva, ebo22_iva, ebo31_iva, ebo32_iva, ebo41_iva, ebo42_iva, ebo51_iva, ebo52_iva,
					ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora)
				Values 
					('<<ALLTRIM(lcBostamp)>>', <<lcDocCode>>, '<<lcDocNome>>', <<lcNrDoc>>, convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102),
					'<<ALLTRIM(lcNome)>>', '', <<lcNo>>, <<lcEstab>>, '<<ALLTRIM(lcMorada)>>', '<<ALLTRIM(lcLocal)>>', '<<ALLTRIM(lcCodPost)>>', '<<ALLTRIM(lcTipo)>>', '<<ALLTRIM(lcNcont)>>',
					<<ROUND(totalSemIva,2)>>, convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), year(dateadd(HOUR, <<difhoraria>>, getdate())), <<ROUND(totalSemIva,2)>>, <<ROUND(totalComIva,2)>>, '<<ALLTRIM(lcMoeda)>>', 'EURO', 'BO', '<<ALLTRIM(mySite)>>',
					<<ch_vendedor>>, '<<ALLTRIM(ch_vendnm)>>', 'Documento gerado Via Confer�ncia de Facturas', '<<lcCcusto>>',
					<<lcQtSum>>, <<ROUND(totalSemIva,2)>>, <<ROUND(totalSemIva,2)>>, <<ROUND(totalSemIva,2)>>, <<ROUND(totalSemIva,2)>>,
					<<totalSemIva1>>, <<totalSemIva1>>, <<totalSemIva2>>, <<totalSemIva2>>, <<totalSemIva3>>, <<totalSemIva3>>, <<totalSemIva4>>, <<totalSemIva4>>, <<totalSemIva5>>, <<totalSemIva5>>,
					<<valorIva1>>, <<valorIva1>>, <<valorIva2>>, <<valorIva2>>, <<valorIva3>>, <<valorIva3>>, <<valorIva4>>, <<valorIva4>>, <<valorIva5>>, <<valorIva5>>,
					'ADM', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8), 'ADM', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8) )
			ENDTEXT
			IF !uf_gerais_actgrelha("", "", lcSql)
				uf_perguntalt_chama("OCORREU UM ERRO A GERAR DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				RETURN .f.
			ENDIF
			lcSql=''
		
			TEXT TO lcSql NOSHOW textmerge
				Insert into bo2 
					(bo2stamp, autotipo, pdtipo, etotalciva, etotiva, u_class, armazem,
					ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora)
				Values
					('<<ALLTRIM(lcBostamp)>>', 1, 1, <<ROUND(totalComIva,2)>>, <<ROUND(lcTotalIva,2)>>, 'Pedido de Cr�dito', <<lcArmazem>>,
					'ADM', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8), 'ADM', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8) )
			ENDTEXT
			IF !uf_gerais_actgrelha("", "", lcSql)
				uf_perguntalt_chama("OCORREU UM ERRO A GERAR DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				RETURN
			ENDIF
			***********************
			*************************** && FIM DOS TOTAIS
		
			** Gravar Linhas **
			STORE "" TO lcBiStamp, lcRef
			STORE 0 TO lcCountS, lcOrdem, lcQtAltern, lcQt, lcStock, lcFamilia, lcTotalLiq
			STORE .t. TO lcValInsertLin
		
			SELECT uCrsConfFac
			GO TOP
			SCAN FOR uCrsConfFac.no==uCrsFornPcDistinctPCF.no AND uCrsConfFac.estab==uCrsFornPcDistinctPCF.estab AND uCrsConfFac.marcadaf==.t. ;
				AND (uCrsConfFac.totalf<uCrsConfFac.totalfac OR (uCrsConfFac.totalf=uCrsConfFac.totalfac AND uCrsConfFac.qtf<=0 AND uCrsConfFac.qtfac>0))
				IF uf_gerais_actgrelha("", "uCrsValidaPcExist2", [select fnstamp from bi2 (nolock) inner join bi (nolock) on bi.bistamp=bi2.bi2stamp where bi.ndos=38 and bi2.fnstamp=']+Alltrim(uCrsConfFac.fnstamp)+['])
					IF !RECCOUNT("uCrsValidaPcExist2")>0
						** Guardar Stamp das linhas **
						lcCountS = lcCountS + 1
						*lcBiStamp = u_stamp(lcCountS)
						lcBiStamp = uf_gerais_stamp()
						
						SELECT uCrsConfFac
						
						** Contador para as linhas
						lcOrdem = lcOrdem + 10000
						
						** Calcular quantidade, unidade alternativa e total liquido
						IF uCrsConfFac.qtfac > uCrsConfFac.qtf
							IF uCrsConfFac.qtf<0 && devolu��es
								lcQt = uCrsConfFac.qtfac
							ELSE
								lcQt = uCrsConfFac.qtfac-uCrsConfFac.qtf
							ENDIF
						ELSE
							lcQt = 1
						ENDIF
						
						lcQtAltern	= INT((lcQt) * uCrsConfFac.conversao)
						
						IF uCrsConfFac.qtf<0 && devolu��es
							lcTotalLiq	= uCrsConfFac.totalfac
						ELSE
							lcTotalLiq	= uCrsConfFac.totalfac-uCrsConfFac.totalf
						ENDIF
						
						** guardar refer�ncia **
						lcRef = IIF(EMPTY(ALLTRIM(uCrsConfFac.ref)), uCrsConfFac.oref, uCrsConfFac.ref)

						** guardar stock actual, familia **
						IF uf_gerais_actgrelha("", "uCrsStockAct", [select stock, familia from st (nolock) where st.ref=']+ALLTRIM(lcRef)+['])
							IF reccount("uCrsStockAct")>0
								lcStock 	= uCrsStockAct.stock
								lcFamilia	= uCrsStockAct.familia
							ENDIF
							fecha("uCrsStockAct")
						ENDIF
						
						** Preencher Linhas da Bi **
						TEXT TO lcSql NOSHOW textmerge
							Insert into bi
								(bistamp, bostamp, nmdos, obrano, ref, codigo,
								design, qtt, qtt2, uni2qtt,
								u_stockact, iva, tabiva, armazem, stipo, ndos, cpoc, familia,
								no, nome, local, morada, codpost, ccusto,
								epu, edebito, eprorc, epcusto, ettdeb, ecustoind, edebitoori, u_upc,
								rdata, dataobra, dataopen, resfor, lordem,
								lobs,
								ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora
								)
							Values
								('<<ALLTRIM(lcBiStamp)>>', '<<ALLTRIM(lcBoStamp)>>', '<<alltrim(lcDocNome)>>', <<lcNrDoc>>, '<<ALLTRIM(lcRef)>>', '<<ALLTRIM(uCrsConfFac.codigo)>>',
								'<<ALLTRIM(uCrsConfFac.design)>>', <<lcQt>>, 0, <<lcQtAltern>>,
								<<lcStock>>, <<uCrsConfFac.iva>>, <<uCrsConfFac.tabiva>>, <<uCrsConfFac.armazem>>, 4, <<lcDocCode>>, <<uCrsConfFac.cpoc>>, '<<lcFamilia>>',
								<<lcNo>>, '<<ALLTRIM(lcNome)>>', '<<ALLTRIM(lcLocal)>>', '<<ALLTRIM(lcMorada)>>', '<<ALLTRIM(lcCodPost)>>', '<<lcCcusto>>',
								<<lcTotalLiq / lcQt>>, <<lcTotalLiq / lcQt>>, <<lcTotalLiq / lcQt>>, <<lcTotalLiq / lcQt>>, <<lcTotalLiq>>, <<lcTotalLiq / lcQt>>, <<lcTotalLiq / lcQt>>, <<lcTotalLiq / lcQt>>,
								convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), 0, <<lcOrdem>>,
								'Referente � Factura N�. '+'<<ALLTRIM(uCrsConfFac.adoc)>>',
								'ADM', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8), 'ADM', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8)
								)
						ENDTEXT
						IF !uf_gerais_actgrelha("", "", lcSql)
							lcValInsertLin = .f.
						ENDIF
						
						** Preencher Linhas da Bi2 **
						TEXT TO lcSql NOSHOW textmerge
							Insert into bi2
								(bi2stamp, bostamp, fnstamp, morada, local, codpost)
							Values
								('<<ALLTRIM(lcBiStamp)>>', '<<ALLTRIM(lcBoStamp)>>', '<<ALLTRIM(uCrsConfFac.fnstamp)>>', '<<ALLTRIM(lcMorada)>>', '<<ALLTRIM(lcLocal)>>', '<<ALLTRIM(lcCodPost)>>')
						ENDTEXT
						IF !uf_gerais_actgrelha("", "", lcSql)
							lcValInsertLin = .f.
						ENDIF
					ENDIF
				ENDIF
				
				STORE 0 TO lcQtAltern, lcQt, lcStock, lcFamilia, lcTotalLiq
				SELECT uCrsConfFac
			ENDSCAN
			******************* && fim das linhas
			
			SELECT uCrsFornPcDistinctPCF
		ENDSCAN
	
		** actualiza ultimo registo **
		IF !EMPTY(lcBoStamp)
			uf_gerais_gravaUltRegisto('bo', lcBoStamp)
		ENDIF
	
		IF !lcValInsertLin
			uf_perguntalt_chama("OCORREU UM ERRO A GRAVAR LINHAS DO PEDIDO DE CR�DITO A FORNECEDOR. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		ELSE
			IF lcValidaNd==.t.
				uf_perguntalt_chama("PEDIDO(s) DE CR�DITO CRIADO(s) COM SUCESSO.","OK","",64)
			ENDIF
		ENDIF
		
		IF USED("uCrsFornPcDistinctPCF")
			fecha("uCrsFornPcDistinctPCF")
		ENDIF
	ENDIF && fim do caso em que a factura n�o mexe em stock
ENDFUNC


**
FUNCTION uf_conffact_CarregaMenu
	CONFFACT.menu1.adicionaOpcao("actualizar","Actualizar",myPath + "\imagens\icons\actualizar_w.png","uf_conffact_CarregaDados","A")
	
	IF uf_gerais_validaPermUser(ch_userno, ch_grupo, 'Compras - Confer�ncia de Facturas Farm�cia')
		CONFFACT.menu1.adicionaOpcao("pfTodos","P.F. Todos",myPath + "\imagens\icons\unchecked_b.png","uf_conffact_pfTodos","P")
	ENDIF
	
	CONFFACT.menu1.adicionaOpcao("ffTodos","F.F. Todos",myPath + "\imagens\icons\unchecked_w.png","uf_conffact_ffTodos","F")
	
	CONFFACT.menu1.estado("actualizar", "SHOW", "Gravar", .t., "Sair", .t.)
	
	CONFFACT.refresh
ENDFUNC



**
FUNCTION uf_conffact_pfTodos

	** remover o focus da grelha para evitar que nem todos as linhas sejam alteradas
	conffact.adoc.setfocus

	IF EMPTY(conffact.menu1.pftodos.tag) OR conffact.menu1.pftodos.tag == "false"
		&& aplica sele��o
		SELECT uCrsConfFac
		REPLACE ALL fmarcada WITH .t.
		
		&& altera o botao
		conffact.menu1.pftodos.tag = "true"
		conffact.menu1.pftodos.config("P.F. Todos", myPath + "\imagens\icons\checked_w.png", "uf_conffact_pfTodos","P")
	ELSE
		&& aplica sele��o
		SELECT uCrsConfFac
		REPLACE ALL fmarcada WITH .f.
		
		&& altera o botao
		conffact.menu1.pftodos.tag = "false"
		conffact.menu1.pftodos.config("P.F. Todos", myPath + "\imagens\icons\unchecked_w.png", "uf_conffact_pfTodos","P")
	ENDIF
	
	SELECT uCrsConfFac
	GO TOP
ENDFUNC



**
FUNCTION uf_conffact_ffTodos
	
	** remover o focus da grelha para evitar que nem todos as linhas sejam alteradas
	conffact.adoc.setfocus
	
	IF EMPTY(conffact.menu1.fftodos.tag) OR conffact.menu1.fftodos.tag == "false"
		&& aplica sele��o
		SELECT uCrsConfFac
		REPLACE ALL marcadaf WITH .t.
		
		&& altera o botao
		conffact.menu1.fftodos.tag = "true"
		conffact.menu1.fftodos.config("F.F. Todos", myPath + "\imagens\icons\checked_w.png", "uf_conffact_ffTodos","F")
	ELSE
		&& aplica sele��o
		SELECT uCrsConfFac
		REPLACE ALL marcadaf WITH .f.
		
		&& altera o botao
		conffact.menu1.fftodos.tag = "false"
		conffact.menu1.fftodos.config("F.F. Todos", myPath + "\imagens\icons\unchecked_w.png", "uf_conffact_ffTodos","F")
	ENDIF
	
	SELECT uCrsConfFac
	GO TOP
ENDFUNC



**
FUNCTION uf_conffact_sair
	IF uf_perguntalt_chama("TEM A CERTEZA QUE DESEJA SAIR DESTE ECR�?","Sim","N�o")
		uf_conffact_exit()
	ENDIF
ENDFUNC


**
FUNCTION uf_conffact_exit
	**Fecha Cursores
	IF USED("uCrsConfFac")
		fecha("uCrsConfFac")
	ENDIF 
	IF USED("ucrsConfFactParam")
		fecha("ucrsConfFactParam")
	ENDIF
	
	**
	CONFFACT.hide
	CONFFACT.release
ENDFUNC


** class **
Define Class confactCust as Custom
	
	Procedure proc_afterQtChangePCF
		WITH CONFFACT.GridPesq
			** activar o evento lostfocus	**	
			FOR i=1 TO .ColumnCount
				lcColumn = Upper(Alltrim(.columns(i).header1.caption))
				If lcColumn=="QT."
					BindEvent(.columns(i).text1,"LostFocus",CONFFACT.oCust,"proc_afterQtChangePCFLost")
				Endif
			ENDFOR
			*******************
		ENDWITH
	ENDPROC
	
	
	PROCEDURE proc_afterQtChangePCFLost
		SELECT uCrsConfFac
		
		If (uCrsConfFac.qtf>0) AND (uCrsConfFac.totalf>0)
			replace uCrsConfFac.totalf	With Round( uCrsConfFac.upcf * uCrsConfFac.qtf  ,2) In uCrsConfFac
		ELSE
			If (uCrsConfFac.qtf>0) AND (uCrsConfFac.upcf>0)
				replace uCrsConfFac.totalf with Round( uCrsConfFac.upcf * uCrsConfFac.qtf  ,2) in uCrsConfFac
			ELSE
				If uCrsConfFac.qtf>0 And uCrsConfFac.totalf=0
					replace uCrsConfFac.totalf with Round( uCrsConfFac.upcenc * uCrsConfFac.qtf ,2) in uCrsConfFac
					replace uCrsConfFac.upcf with Round( uCrsConfFac.upcenc ,2) in uCrsConfFac
				ELSE
					If uCrsConfFac.qtf<0 And (uCrsConfFac.qtf * (-1) <= uCrsConfFac.qtenc) && devolu��es
						IF (uf_gerais_validaPermUser(ch_userno,ch_grupo,'Compras - Confer�ncia de Facturas Farm�cia'))	 && apenas altera valores se tiver permiss�o para tal
							replace uCrsConfFac.upcf		With uCrsConfFac.upcenc In uCrsConfFac
							replace uCrsConfFac.totalf	With uCrsConfFac.totalenc In uCrsConfFac
						ENDIF
					Else
						If uCrsConfFac.qtf<0 And (uCrsConfFac.qtf * (-1) > uCrsConfFac.qtenc) && n�o deve deixar devolver quantidades superiores �s encomendadas
							replace uCrsConfFac.qtf	With 0 In uCrsConfFac
						ENDIF
						
						IF (uf_gerais_validaPermUser(ch_userno,ch_grupo,'Compras - Confer�ncia de Facturas Farm�cia'))	 && apenas altera valores se tiver permiss�o para tal
							replace uCrsConfFac.upcf	With 0 in uCrsConfFac
							replace uCrsConfFac.totalf	With 0 In uCrsConfFac
							replace uCrsConfFac.bonusf	With 0 In uCrsConfFac
							replace uCrsConfFac.desc1f	With 0 in uCrsConfFac
							replace uCrsConfFac.desc2f	With 0 In uCrsConfFac
							replace uCrsConfFac.desc3f	With 0 in uCrsConfFac
							replace uCrsConfFac.desc4f	With 0 In uCrsConfFac
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		WITH CONFFACT.GridPesq
			** desactivar o evento lostfocus **	
			
			FOR i=1 TO .ColumnCount
				lcColumn = Upper(Alltrim(.columns(i).header1.caption))
				If lcColumn == "QT."
					UnBindEvent(.columns(i).text1, "LostFocus", CONFFACT.oCust, "proc_afterQtChangePCFLost")
				ENDIF
			ENDFOR
			*****************************
		ENDWITH
	ENDPROC
		
	PROCEDURE proc_afterTotalChangePCF
		SELECT uCrsConfFac
		
		IF (uCrsConfFac.qtf=0) AND (uCrsConfFac.totalf>0)
			replace uCrsConfFac.qtf with 1 in uCrsConfFac
		Endif
		
		If (uCrsConfFac.qtf>0) AND (uCrsConfFac.totalf>0)
			replace uCrsConfFac.upcf with Round( uCrsConfFac.totalf / uCrsConfFac.qtf  ,2) in uCrsConfFac
		Else
			If uCrsConfFac.totalf=0
				replace uCrsConfFac.upcf With 0
			Endif
		Endif
		
		If uCrsConfFac.qtf<0 && devolu��o
			replace uCrsConfFac.upcf	With uCrsConfFac.upcenc
			replace uCrsConfFac.totalf	With uCrsConfFac.totalenc
		Endif
	Endproc
ENDDEFINE