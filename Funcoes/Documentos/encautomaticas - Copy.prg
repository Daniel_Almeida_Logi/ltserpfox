**
FUNCTION uf_encautomaticas_chama
	LPARAMETERS nTipoEnc, nSavedEncStamp, nconjunta

	PUBLIC myGetEncStamp, MyTipoEncAuto, myOrderEncAutomaticas, myControlaEventoQt, myResizeEncAutomaticas, MyEncConjunta, myEncGrupo 
	MyEncConjunta = nconjunta
	
	IF TYPE("PREPARARENCOMENDA")!="U"	
		uf_prepararencomenda_sair()
	ENDIF 

	IF !EMPTY(nSavedEncStamp)
		myGetEncStamp = nSavedEncStamp
	ELSE
		STORE '' TO myGetEncStamp
	ENDIF

	IF !EMPTY(nTipoEnc)
		MyTipoEncAuto = nTipoEnc
	ELSE
		MyTipoEncAuto = ''
	ENDIF

	IF TYPE("ENCAUTOMATICAS")=="U"
		LOCAL lcSQL
		
		IF !USED("crsenc")
			IF TYPE("ATENDIMENTO")=="U"
				TEXT TO lcSQL NOSHOW TEXTMERGE
					exec up_enc_autoref 0, '', '', <<mysite_nr>>
				ENDTEXT
			ELSE 
				SELECT distinct ref FROM fi where !empty(fi.ref) INTO CURSOR ucrsfienc READWRITE 
				LOCAL lcListRef, lcListRef1
				STORE '' TO lcListRef, lcListRef1
				SELECT ucrsfienc 
				GO TOP 
				SCAN 
					lcListRef = ALLTRIM(lcListRef)+ALLTRIM(ucrsfienc.ref)+","
				ENDSCAN 
				fecha("ucrsfienc")
				lcListRef = SUBSTR(lcListRef,1,LEN(lcListRef)-1)

				TEXT TO lcSQL NOSHOW TEXTMERGE
					exec up_enc_autoref_atendimento <<myArmazem>>, '<<ALLTRIM(lcListRef)>>', '', <<mysite_nr>>, 0
				ENDTEXT
			ENDIF 
			IF !uf_gerais_actgrelha("", "crsenc", lcSql)
				uf_perguntalt_chama("N�O FOI POSSIVEL DETERMINAR OS DADOS PARA GERAR ENCOMENDAS AUTOM�TICAS. POR FAVOR CONTACTE O SUPORTE.","OK","", 16)
				RETURN .f.
			ENDIF
		ENDIF

		IF ALLTRIM(uCrsE1.tipoempresa) == "PARAFARMACIA"
			SELECT crsenc
			SET FILTER TO crsenc.familia != "1"
		ENDIF

		IF !USED("ucrsFornceEncAut")
			lcSQL = ""
			TEXT TO lcSQL NOSHOW TEXTMERGE
				SELECT
					nome	= ''
					,no		= 0
					,estab	= 0
				
				union all

				Select 
					nome
					,no
					,estab
				from 
					fl (nolock)
				order by
					nome
			ENDTEXT
			IF !uf_gerais_actgrelha("", "ucrsFornceEncAut", lcSql)
				RETURN .f.
				uf_perguntalt_chama("OCORREU UMA ANOMALIA NA VERIFICA��O DE FORNECEDORES. POR FAVOR CONTACTE O SUPORTE.","OK","", 16)
			ENDIF
		ENDIF

		SELECT crsenc
		GO TOP

		**
		DO FORM ENCAUTOMATICAS

		&&Centra
		ENCAUTOMATICAS.autocenter = .t.
		ENCAUTOMATICAS.windowstate = 2
		
		ENCAUTOMATICAS.show

	ELSE
		**
		ENCAUTOMATICAS.show
	ENDIF
	
	** calcula totais
	uf_encautomaticas_ActualizaQtValorEnc()
ENDFUNC


**
FUNCTION uf_encautomaticas_CarregaMenu
	&& configura menu
	ENCAUTOMATICAS.menu1.adicionaOpcao("opcoes", "Op��es", myPath + "\imagens\icons\opcoes_w.png", "","O")
	ENCAUTOMATICAS.menu1.adicionaOpcao("atualizar", "Pesquisar", myPath + "\imagens\icons\actualizar_w.png", "uf_encautomaticas_aplicaFiltros","A")
	ENCAUTOMATICAS.menu1.adicionaOpcao("encTodos", "Enc. Todos", myPath + "\imagens\icons\unchecked_w.png", "uf_encautomaticas_EncTodos","E")
	ENCAUTOMATICAS.menu1.adicionaOpcao("selTodos", "Sel. Todos", myPath + "\imagens\icons\unchecked_w.png", "uf_encautomaticas_SelTodos","T")
	ENCAUTOMATICAS.menu1.adicionaOpcao("guardarEnc", "Guardar Enc.", myPath + "\imagens\icons\doc_ef_w.png", "uf_encautomaticas_GuardaEstadoEncomenda","U")
	**ENCAUTOMATICAS.menu1.adicionaOpcao("infoFornec", "Consulta Forn.", myPath + "\imagens\icons\detalhe_micro.png", "uf_stocks_consultarProd with 'Encomendas'","F")
	ENCAUTOMATICAS.menu1.adicionaOpcao("selFornecedor", "Fornecedor", myPath + "\imagens\icons\forn_lupa_w.png", "uf_encautomaticas_AplicarFornecedor","F")	
	ENCAUTOMATICAS.menu1.adicionaOpcao("subir", "Subir", myPath + "\imagens\icons\ponta_seta_up.png", "uf_gerais_MovePage with .t.,'ENCAUTOMATICAS.GridPesq','crsenc'","05")
	ENCAUTOMATICAS.menu1.adicionaOpcao("descer", "Descer", myPath + "\imagens\icons\ponta_seta_down.png", "uf_gerais_MovePage with .f.,'ENCAUTOMATICAS.GridPesq','crsenc'","24")
	&&Sub-Menu
	ENCAUTOMATICAS.menu_opcoes.adicionaOpcao("encAberto", "Encomendas em Aberto", "", "uf_encautomaticas_EncomendaAberto")
	ENCAUTOMATICAS.menu_opcoes.adicionaOpcao("multiloja", IIF(MyEncConjunta == .f.,"Ver detalhe Multi-Loja","Ver detalhe Loja �nica"), "", "uf_encautomaticas_MultiLoja")
	ENCAUTOMATICAS.menu_opcoes.adicionaOpcao("verFicha", "Ver Ficha Produto", "", "uf_encautomaticas_chamaSt")
	ENCAUTOMATICAS.menu_opcoes.adicionaOpcao("infoFornec", "Consulta Forn.", "", "uf_stocks_consultarProd with 'Encomendas'")

	encautomaticas.menu1.estado("", "", "Gravar", .t., "Sair", .t.)
ENDFUNC


**
FUNCTION uf_encautomaticas_MultiLoja
	PUBLIC MyEncConjunta
	
	IF MyEncConjunta == .f.
		MyEncConjunta = .t.
		ENCAUTOMATICAS.menu_opcoes.multiloja.config("Ver detalhe Loja �nica","","uf_encautomaticas_MultiLoja")
	ELSE
		MyEncConjunta = .f.
		ENCAUTOMATICAS.menu_opcoes.multiloja.config("Ver detalhe Multi-Loja","","uf_encautomaticas_MultiLoja")
	ENDIF
	
	ENCAUTOMATICAS.detalhest1.myActivateResumoRef = ""
	ENCAUTOMATICAS.detalhest1.myActivate5baratos = ""
	ENCAUTOMATICAS.detalhest1.myActivatebonusref = ""
	ENCAUTOMATICAS.detalhest1.myActivateesgotadosref = ""
	ENCAUTOMATICAS.detalhest1.myActivatehistprecoscompras = ""
	ENCAUTOMATICAS.detalhest1.myActivatesaidasref = ""
	ENCAUTOMATICAS.detalhest1.myActivatehistprecosref = ""
		
	ENCAUTOMATICAS.detalhest1.refresh
	ENCAUTOMATICAS.refresh
ENDFUNC 


**
FUNCTION uf_encautomaticas_criaCursorProcura
	SELECT crsenc
	GO TOP

	SELECT ref FROM crsenc WHERE (LIKE ('*'+ALLTRIM(UPPER(ENCAUTOMATICAS.textPesq.value))+'*',ALLTRIM(UPPER(crsenc.ref))) OR ;
			LIKE ('*'+ALLTRIM(UPPER(ENCAUTOMATICAS.textPesq.value))+'*',ALLTRIM(UPPER(crsenc.design)))) AND ;
			!EMPTY(ALLTRIM(UPPER(ENCAUTOMATICAS.textPesq.value))) INTO CURSOR ucrsTempPesq

	SELECT ucrsTempPesq

	SELECT crsenc		
	GO TOP
	SCAN
		IF (LIKE ('*'+ALLTRIM(UPPER(ENCAUTOMATICAS.textPesq.value))+'*',ALLTRIM(UPPER(crsenc.ref))) OR ;
			LIKE ('*'+ALLTRIM(UPPER(ENCAUTOMATICAS.textPesq.value))+'*',ALLTRIM(UPPER(crsenc.design)))) AND ;
			!EMPTY(ALLTRIM(UPPER(ENCAUTOMATICAS.textPesq.value)))
			
			replace crsenc.PESQUISA WITH .t.
		ELSE
			replace crsenc.PESQUISA WITH .f.
		ENDIF
	ENDSCAN
	
	uf_encautomaticas_procuraLinhaSeguinte()
ENDFUNC


**
FUNCTION uf_encautomaticas_procuraLinhaSeguinte
	IF !USED("ucrsTempPesq")
		RETURN .f.
	ENDIF
	
	lcref = ucrsTempPesq.ref
	select ucrsTempPesq
	TRY
		goto recno() +1
	CATCH
	 	GO TOP
	ENDTRY
	
	SELECT ucrsTempPesq
	SELECT crsenc
	SCAN
		IF crsenc.ref == ucrsTempPesq.ref

			EXIT
		ENDIF
	ENDSCAN

	ENCAUTOMATICAS.GridPesq.refresh
ENDFUNC


**
FUNCTION uf_encautomaticas_aplicaFiltros
	Local lcFiltro, lcGenerico

	WITH ENCAUTOMATICAS
		** controlo de genericos
		IF !EMPTY(UPPER(ALLTRIM(.Genericos.value)))
			IF UPPER(ALLTRIM(.Genericos.value)) == "GEN�RICOS"
				lcGenerico = 1
			ELSE
				lcGenerico = 2
			ENDIF
		ELSE
			lcGenerico = 0
		ENDIF

		** fonte
		IF UPPER(ALLTRIM(.fonte.value)) == "DICION�RIO / INTERNO"
			lcFiltro = '(ALLTRIM(crsenc.u_fonte) == "D" OR ALLTRIM(crsenc.u_fonte) == "I")'
		ELSE
			IF UPPER(ALLTRIM(.fonte.value)) == "EXTERNO"
				lcFiltro = '(ALLTRIM(crsenc.u_fonte) == "E")'
			ELSE
				lcFiltro = ''
			ENDIF
		ENDIF	

		** familia
		IF !EMPTY(UPPER(ALLTRIM(.familia.value)))
		
			** guardar filtro
			IF !EMPTY(lcFiltro)
				lcFiltro = lcFiltro + ' ' + [ AND ALLTRIM(UPPER(crsenc.faminome)) $  ENCAUTOMATICAS.familia.value] 
			ELSE
				lcFiltro = [ ALLTRIM(UPPER(crsenc.faminome)) $  ALLTRIM(UPPER(ENCAUTOMATICAS.familia.value))] 
			ENDIF
		ENDIF
		
		
		** tipoProduto
		IF !EMPTY(UPPER(ALLTRIM(.tipoProduto.value)))
			** guardar filtro
			IF !EMPTY(lcFiltro)
				lcFiltro = lcFiltro + ' ' + [ AND ALLTRIM(UPPER(crsenc.faminome)) $  ENCAUTOMATICAS.tipoProduto.value] 
			ELSE
				lcFiltro = [ ALLTRIM(UPPER(crsenc.faminome)) $  ALLTRIM(UPPER(ENCAUTOMATICAS.tipoProduto.value))] 
			ENDIF
		ENDIF

		** generico
		IF !EMPTY(lcGenerico)
			SELECT crsenc
			IF lcGenerico = 1
				IF !EMPTY(lcFiltro)
					lcFiltro = lcFiltro + ' ' + 'AND crsenc.generico == .t.'
				ELSE
					lcFiltro = 'crsenc.generico == .t.'
				ENDIF
			ELSE
				IF !EMPTY(lcFiltro)
					lcFiltro = lcFiltro + ' ' + 'AND crsenc.generico == .f.'
				ELSE
					lcFiltro = 'crsenc.generico == .f.'
				ENDIF
			ENDIF
		ENDIF

		** fornecedor
		If !EMPTY(.no.value)
			** guardar filtro 
			IF !EMPTY(lcFiltro)
				lcFiltro = lcFiltro + ' ' + 'AND (crsenc.fornec = ENCAUTOMATICAS.no.value AND crsenc.fornestab = ENCAUTOMATICAS.estab.value)'
			ELSE
				lcFiltro = '(crsenc.fornec = ENCAUTOMATICAS.no.value AND crsenc.fornestab = ENCAUTOMATICAS.estab.value)'
			ENDIF
		ENDIF
	

		** GRUPO HOMOGENEO
		If !EMPTY(.gh.value)
			** guardar filtro
			IF !EMPTY(lcFiltro)
				lcFiltro = lcFiltro + ' ' + [ AND ALLTRIM(UPPER(crsenc.grphmgcode)) $  ALLTRIM(UPPER(ENCAUTOMATICAS.gh.value))] 
			ELSE
				lcFiltro = [ ALLTRIM(UPPER(crsenc.grphmgcode)) $  ENCAUTOMATICAS.gh.value] 
			ENDIF
		ENDIF

		** LABORAT�RIO
		If !EMPTY(.laboratorio.value)
			** guardar filtro 
			IF !EMPTY(lcFiltro)
				lcFiltro = lcFiltro + ' ' + [ AND ALLTRIM(UPPER(crsenc.u_lab)) $  ALLTRIM(UPPER(ENCAUTOMATICAS.laboratorio.value))]
			ELSE
				lcFiltro = [ ALLTRIM(UPPER(crsenc.u_lab)) $  ALLTRIM(UPPER(ENCAUTOMATICAS.laboratorio.value))] &&'(crsenc.u_lab= ENCAUTOMATICAS.laboratorio.value)'
			ENDIF
		ENDIF

		** MARCA
		If !EMPTY(.marca.value)
			** guardar filtro (fornecedor)
			IF !EMPTY(lcFiltro)
				lcFiltro = lcFiltro + ' ' + [ AND  ALLTRIM(UPPER(crsenc.marca)) $  ALLTRIM(UPPER(ENCAUTOMATICAS.marca.value))] 
			ELSE
				lcFiltro = [ ALLTRIM(UPPER(crsenc.marca)) $  ALLTRIM(UPPER(ENCAUTOMATICAS.marca.value))] 
			ENDIF
		ENDIF

		** Psicotr�picos/Benzodiazepinas
		If !EMPTY(.psicobenzo.value)
			
			** guardar filtro (fornecedor)
			IF !EMPTY(lcFiltro)
				lcFiltro = lcFiltro + ' ' + 'AND (crsenc.psico = .t. OR crsenc.benzo = .t.)'
			ELSE
				lcFiltro = '(crsenc.psico = .t. OR crsenc.benzo = .t.)'
			ENDIF
		ENDIF

		** Produtos com Reservas de Clientes
		If !EMPTY(.reservas.value)
			** guardar filtro (fornecedor)
			IF !EMPTY(lcFiltro)
				IF ALLTRIM(.reservas.value)='Produtos Reservados por Clientes'
					lcFiltro = lcFiltro + ' ' + 'AND (crsenc.qttcli > 0)'
				ELSE
					lcFiltro = lcFiltro + ' ' + 'AND (crsenc.qttcli = 0)'
				ENDIF 
			ELSE
				IF ALLTRIM(.reservas.value)='Produtos Reservados por Clientes'
					lcFiltro = '(crsenc.qttcli > 0)'
				ELSE
					lcFiltro = '(crsenc.qttcli = 0)'
				ENDIF
			ENDIF
		ENDIF
		
		** Tipo Encomenda
		If !EMPTY(.tipoEncomenda.value)
			** guardar filtro (tipoEncomenda)
			IF !EMPTY(lcFiltro)
				IF ALLTRIM(.tipoEncomenda.value)='Esgotados'
					lcFiltro =  lcFiltro + ' ' + [ AND  ALLTRIM(UPPER(crsenc.Esgotado)) $  ALLTRIM(UPPER("SIM"))] 
				ELSE  
					lcFiltro = lcFiltro + ' ' + [ AND  ALLTRIM(UPPER(crsenc.Esgotado)) $  ALLTRIM(UPPER("N�O"))] 
				ENDIF 
			ELSE
				IF ALLTRIM(.tipoEncomenda.value)='Esgotados'
					lcFiltro = [ ALLTRIM(UPPER(crsenc.Esgotado)) $  ALLTRIM(UPPER("SIM"))] 
				ELSE  
					lcFiltro = [ ALLTRIM(UPPER(crsenc.Esgotado)) $  ALLTRIM(UPPER("N�O"))] 
				ENDIF 
			ENDIF 
		ENDIF
		
		** Produtos de Parafarm�cia
		IF .chkParafarmacia.tag = "true"
			** guardar filtro
			IF !EMPTY(lcFiltro)
				lcFiltro = lcFiltro + ' ' + 'AND (crsenc.familia != "1")'
			ELSE
				lcFiltro = '(crsenc.familia != "1")'
			ENDIF
		ENDIF	
			
		** manter sempre os j� seleccionados no filtro
*!*			IF !EMPTY(lcFiltro)
*!*				lcFiltro = lcFiltro + ' ' + 'OR crsenc.enc=.t.'
*!*			ENDIF

		** aplicar filtro
		SELECT crsenc
		SET FILTER TO &lcFiltro

		SELECT crsenc
		GO TOP
	
		ENCAUTOMATICAS.gridPesq.refresh
	ENDWITH	
ENDFUNC


**
FUNCTION uf_encautomaticas_ActualizaQtValorEnc
	LOCAL lcPos, lcQt, lcValor
	STORE 0 TO lcPos, lcQt, lcValor
	
	IF USED("CRSenc")
		SELECT crsenc
		lcPos = RECNO()

		SELECT crsenc
		CALCULATE SUM(crsenc.eoq) FOR crsenc.enc TO lcQt
		CALCULATE SUM(IIF(crsenc.pclfornec==0,crsenc.epcult,crsenc.pclfornec)*(crsenc.eoq-crsenc.qtbonus)) FOR crsenc.enc TO lcValorPCLForn && Alterado a pedido do Hugo em 22.01.2014
		CALCULATE SUM(crsenc.epcusto*(crsenc.eoq-crsenc.qtbonus)) FOR crsenc.enc TO lcValor
		
		**CALCULATE SUM(crsenc.epcusto*crsenc.eoq) FOR crsenc.enc TO lcValor
		
		ENCAUTOMATICAS.containerSubTotais.qtsum.value = lcQt
*!*			IF uf_gerais_getParameter("ADM0000000213","BOOL") == .t. and uf_gerais_getParameter("ADM0000000214","BOOL") == .f. && Encomendas Keiretsu
		ENCAUTOMATICAS.containerSubTotais.valorsum.value = lcValorPCLForn
*!*			ELSE
*!*				ENCAUTOMATICAS.containerSubTotais.valorsum.value = lcValor
*!*			ENDIF		
		ENCAUTOMATICAS.containerSubTotais.qtsum.refresh
		ENCAUTOMATICAS.containerSubTotais.valorsum.refresh
		
		SELECT crsenc
		IF lcPos>0
			try
				GO lcPos
			CATCH
				GO TOP
			ENDTRY
		ENDIF
	ENDIF
ENDFUNC


**
FUNCTION uf_encautomaticas_EncTodos
	IF EMPTY(encautomaticas.menu1.enctodos.tag) OR encautomaticas.menu1.enctodos.tag == "false"
		&& aplica sele��o
		REPLACE ALL enc WITH .t. IN crsenc
		
		&& altera o botao
		encautomaticas.menu1.enctodos.tag = "true"
		encautomaticas.menu1.enctodos.config("Enc. Todos", myPath + "\imagens\icons\checked_w.png", "uf_encautomaticas_EncTodos","E")
		
		&& actualiza o ecra
		*pesqstocks.refresh()
	ELSE
		&& aplica sele��o
		REPLACE ALL enc WITH .f. IN crsenc
		
		&& altera o botao
		encautomaticas.menu1.enctodos.tag = "false"
		encautomaticas.menu1.enctodos.config("Enc. Todos", myPath + "\imagens\icons\unchecked_w.png", "uf_encautomaticas_EncTodos","E")
		
		&& actualiza o ecra		
		*pesqstocks.refresh()
	ENDIF
	
	uf_encautomaticas_ActualizaQtValorEnc()
		
	SELECT crsenc
	GO top
ENDFUNC


**
FUNCTION uf_encautomaticas_SelTodos
	IF EMPTY(encautomaticas.menu1.seltodos.tag) OR encautomaticas.menu1.seltodos.tag == "false"
		&& aplica sele��o
		REPLACE ALL sel WITH .t. IN crsenc
		
		&& altera o botao
		encautomaticas.menu1.selTodos.tag = "true"
		encautomaticas.menu1.selTodos.config("Sel. Todos", myPath + "\imagens\icons\checked_w.png", "uf_encautomaticas_SelTodos","T")
		
		&& actualiza o ecra
		*pesqstocks.refresh()
	ELSE
		&& aplica sele��o
		REPLACE ALL sel WITH .f. IN crsenc
		
		&& altera o botao
		encautomaticas.menu1.selTodos.tag = "false"
		encautomaticas.menu1.selTodos.config("Sel. Todos", myPath + "\imagens\icons\unchecked_w.png", "uf_encautomaticas_SelTodos","T")
		
		&& actualiza o ecra		
		*pesqstocks.refresh()
	ENDIF
	
	uf_encautomaticas_ActualizaQtValorEnc()
		
	SELECT crsenc
	GO top
ENDFUNC


**
FUNCTION uf_encautomaticas_alteraFornecedorSel
	LPARAMETERS nNome, nNo, nEstab

	**Alertas
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE
		exec up_enc_dadosAlertasEncAutomaticas '', <<nNo>>, <<nEstab>>, <<mysite_nr>>
	ENDTEXT
	IF !uf_gerais_actGrelha("", "uCrsActualizaFornecedores", lcSQL)
		uf_perguntalt_chama("N�O VERIFICAR INFORMA��O DOS ARTIGOS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	ENDIF

	regua(0,RECCOUNT("crsenc"),"A ACTUALIZAR INFORMA��O DO FORNECEDOR...",.f.)
	SELECT crsenc
	GO TOP
	SCAN FOR crsenc.sel == .t.
	
		regua[1,recno(),"A PRODUTO: "+ALLTRIM(crsenc.ref),.f.]
		
		REPLACE crsenc.fornecedor 	WITH ALLTRIM(nNome)
		Replace crsenc.fornec 		WITH nNo
		Replace crsenc.fornestab 	WITH nEstab
		
		IF nNo == 0
			replace crsenc.enc			WITH .f.
		ELSE
		
*!*				Select uCrsActualizaFornecedores
*!*				LOCATE FOR ALLTRIM(uCrsActualizaFornecedores.ref) == ALLTRIM(crsenc.ref)
*!*				IF FOUND()
*!*		            Replace crsenc.bonusfornec    	WITH uCrsActualizaFornecedores.bonus
*!*		            Replace crsenc.pclfornec    	WITH uCrsActualizaFornecedores.pcl
*!*		            Replace crsenc.esgotado    		WITH uCrsActualizaFornecedores.esgotado
*!*					replace crsenc.alertaStockGH 	WITH uCrsActualizaFornecedores.alertaStockGH
*!*					replace crsenc.alertaPCL 		WITH uCrsActualizaFornecedores.alertaPCL 	
*!*					replace crsenc.alertaBonus 		WITH uCrsActualizaFornecedores.alertaBonus 
*!*				ENDIF 
			
			replace crsenc.enc			WITH .t.
		Endif
	ENDSCAN
	

	** Actualiza Dados
	UPDATE crsenc;
	SET crsenc.bonusfornec = uCrsActualizaFornecedores.bonus;
		,crsenc.pclfornec = uCrsActualizaFornecedores.pcl;
		,crsenc.esgotado = uCrsActualizaFornecedores.esgotado;
		,crsenc.alertaStockGH = uCrsActualizaFornecedores.alertaStockGH;
		,crsenc.alertaPCL = uCrsActualizaFornecedores.alertaPCL;
		,crsenc.alertaBonus = uCrsActualizaFornecedores.alertaBonus;
	From;
		crsenc inner join uCrsActualizaFornecedores on crsenc.ref = uCrsActualizaFornecedores.ref;
	Where;
		crsenc.sel = .t.
		

	regua(2)
	SELECT crsenc
	GO top
	SCAN
		replace crsenc.sel WITH .f.
	ENDSCAN
	
	SELECT crsenc
	GO TOP
ENDFUNC


**
FUNCTION uf_encautomaticas_chamaSt
	If Used("crsenc")
		Select crsenc
		If !Empty(Alltrim(crsenc.ref))
			uf_stocks_chama(crsenc.ref)
		Endif
	ENDIF
ENDFUNC


**
FUNCTION uf_encautomaticas_adicionaProd
	uf_pesqStocks_chama("ENCAUTOMATICAS")
ENDFUNC


**
FUNCTION uf_encautomaticas_EliminaProd
	IF !USED("crsenc")
		RETURN .f.
	ENDIF

	LOCAL lcRef, lcPos
	lcPos = recno()
	
	Select crsenc
	lcRef = crsenc.ref
		
	IF !EMPTY(lcRef)
		Select crsenc
		LOCATE FOR ALLTRIM(crsenc.ref) == ALLTRIM(lcRef)
		DELETE
	ENDIF
	
	select crsenc
	TRY 
		GO lcPos +1
	CATCH
		GO TOP
	ENDTRY
					
	SELECT crsenc	
	ENCAUTOMATICAS.GRIDPESQ.setfocus
	
ENDFUNC


**
FUNCTION uf_encautomaticas_InteractiveChangeFornecedor
	If Used("crsenc") AND USED("ucrsFornceEncAut")
		SELECT ucrsFornceEncAut
		Select crsenc
		Replace crsenc.fornecedor 	WITH ucrsFornceEncAut.nome
		Replace crsenc.fornec 		WITH ucrsFornceEncAut.no
		Replace crsenc.fornestab 	WITH ucrsFornceEncAut.estab
	ENDIF
ENDFUNC


**
FUNCTION uf_encautomaticas_GuardaEstadoEncomenda
	
	IF !USED("crsenc")
		RETURN .f.
	ENDIF
	
	SELECT crsenc
	IF RECCOUNT("crsenc") == 0
		uf_perguntalt_chama("N�O PODE GUARDAR UMA ENCOMENDA SEM REGISTOS.", "OK","",48)
		RETURN .f.
	ENDIF
	
	LOCAL lcErro, lcTime, lcDate
	STORE .f. TO lcUpdate, lcErro
	STORE '' TO lcTime, lcDate

	IF EMPTY(myGetEncStamp)
		LOCAL lcStamp
		lcStamp = uf_gerais_stamp()
		myGetEncStamp = ALLTRIM(lcStamp)
	ENDIF

	IF !uf_perguntalt_chama("VAI PROCEDER � GRAVA��O TEMPORARIA DO ESTADO DA ENCOMENDA. PRENDE CONTINUAR?","Sim","N�o")
		RETURN .f.
	ENDIF

	SET HOURS TO 24
	Atime=ttoc(datetime()+(difhoraria*3600),2)
	Astr=left(Atime,8)
	lcTime = Astr
	**lcTime = TIME()
	lcDate = uf_gerais_getDate((datetime()+(difhoraria*3600)),"SQL")

	* a vari�vel guarda o total de registos do cursor tempcursor
	Select crsenc
	mntotal=reccount()

	* inicializa a r�gua apresentando um t�tulo e o n� total de registos
	regua(0,mntotal,"A PROCESSAR...",.f.)

	**Verifica se j� existe uma encomeda com o stamp inserido, se existir apaga para proceder � introdu��o
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		Select 
			contador = COUNT(stamp)
		from 
			B_encAutoSave (nolock)
		Where 
			B_encAutoSave.stamp = '<<ALLTRIM(myGetEncStamp)>>'
	ENDTEXT
	IF !uf_gerais_actgrelha("", "ucrsExisteEncTemp", lcSql)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA NA VERIFICA��O DE EXISTENCIA DE ENCOMENDA TEMPOR�RIA. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	ENDIF

	IF ucrsExisteEncTemp.contador > 0 &&existe vai eliminar registo da tabela temp na BD
		lcSQL = ""
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			DELETE
			from
				B_encAutoSave
			Where
				B_encAutoSave.stamp = '<<ALLTRIM(myGetEncStamp)>>'
		ENDTEXT
		IF !uf_gerais_actgrelha("", "", lcSQL)
			uf_perguntalt_chama("OCORREU UMA ANOMALIA NA VERIFICA��O DE EXISTENCIA DE ENCOMENDA TEMPOR�RIA. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			RETURN .f.
		ENDIF
	ENDIF
	***********************************************************
	
	SELECT crsenc
	GO TOP
	
		SCAN FOR !EMPTY(crsenc.ref)
			** actualizar regua **
			regua[1,recno("crsenc"),"A GUARDAR PRODUTO: "+ALLTRIM(crsenc.design),.f.]
		
			TEXT TO lcSql NOSHOW textmerge
				INSERT 
					INTO B_encAutoSave (
							stamp
							,data 
							,hora 
							,udata 
							,uhora 
							,ousr 
							,usr
							,tipoenc
							,ststamp
							,enc
							,u_fonte
							,ref
							,refb
							,qttb
							,codigo
							,design
							,stock
							,stmin
							,stmax
							,ptoenc
							,eoq
							,qtbonus
							,qttadic
							,qttfor
							,qttacin
							,qttcli
							,fornecedor
							,fornec
							,fornestab
							,sel
							,epcusto
							,epcpond
							,epcult
							,tabiva
							,iva
							,cpoc
							,familia
							,faminome
							,u_lab
							,conversao
							,generico
							,stockGH
							,PCLFornec
							,alertaStockGH
							,alertaPCL
							,alertaBonus
							,bonusFornec
							,esgotado
					)
					values (
						'<<ALLTRIM(myGetEncStamp)>>'
						,'<<lcDate>>'
						,'<<ALLTRIM(lcTime)>>'
						,'<<lcDate>>'
						,'<<ALLTRIM(lcTime)>>'
						,'<<ALLTRIM(m_chinis)>>'
						,'<<ALLTRIM(m_chinis)>>'
						,'<<ALLTRIM(myTipoEncAuto)>>'
						,'<<ALLTRIM(crsenc.ststamp)>>'
						,<<IIF(crsenc.enc==.t.,1,0)>>
						,'<<ALLTRIM(crsenc.u_fonte)>>'
						,'<<ALLTRIM(crsenc.ref)>>'
						,'<<ALLTRIM(crsenc.refb)>>'
						,<<crsenc.qttb>>
						,'<<ALLTRIM(crsenc.codigo)>>'
						,'<<ALLTRIM(crsenc.design)>>'
						,<<crsenc.stock>>
						,<<crsenc.stmin>>
						,<<crsenc.stmax>>
						,<<crsenc.ptoenc>>
						,<<crsenc.eoq>>
						,<<crsenc.qtbonus>>
						,<<crsenc.qttadic>>
						,<<crsenc.qttfor>>
						,<<crsenc.qttacin>>
						,<<crsenc.qttcli>>
						,'<<ALLTRIM(crsenc.fornecedor)>>'
						,<<crsenc.fornec>>
						,<<crsenc.fornestab>>
						,<<IIF(crsenc.sel==.t.,1,0)>>
						,<<crsenc.epcusto>>
						,<<crsenc.epcpond>>
						,<<crsenc.epcult>>
						,<<crsenc.tabiva>>
						,<<crsenc.iva>>
						,<<crsenc.cpoc>>
						,'<<ALLTRIM(crsenc.familia)>>'
						,'<<ALLTRIM(crsenc.faminome)>>'
						,'<<ALLTRIM(crsenc.u_lab)>>'
						,<<crsenc.conversao>>
						,<<IIF(crsenc.generico==.t.,1,0)>>
						,<<crsenc.stockGH>>
						,<<crsenc.PCLFornec>>
						,<<crsenc.alertaStockGH>>
						,<<crsenc.alertaPCL>>
						,<<crsenc.alertaBonus>>
						,'<<ALLTRIM(crsenc.bonusFornec)>>'
						,'<<ALLTRIM(crsenc.esgotado)>>'
						)
			ENDTEXT
			IF !uf_gerais_actgrelha("", "", lcSql)
				uf_perguntalt_chama("OCORREU UM ERRO A GUARDAR A ENCOMENDA. POR FAVOR CONTACTE O SUPORTE","OK","",16)
				lcErro=.t.
				EXIT
			ENDIF

			SELECT crsenc
		ENDSCAN

	** fechar regua
	REGUA(2)

	IF !lcErro
		uf_perguntalt_chama("ENCOMENDA GUARDADA COM SUCESSO!","OK","", 64)
		uf_encautomaticas_sair()
	ENDIF
ENDFUNC


**
FUNCTION uf_encautomaticas_AplicarFornecedor
	uf_pesqFornecedores_Chama("ENCAUTOMATICAS")
ENDFUNC


**
FUNCTION uf_encautomaticas_EncomendaAberto
	*MSG("AN�LISE DE ENCOMEDAS EM ABERTO...PAINEL DE BORDO")
ENDFUNC


**
FUNCTION uf_encautomaticas_expandeGridLinhas

	If myResizeEncAutomaticas == .f.
		
		ENCAUTOMATICAS.detalhest1.visible = .t.
		ENCAUTOMATICAS.detalhest1.pageFRAME1.page9.CHART1.Visible = .t.
		ENCAUTOMATICAS.detalhest1.pageFRAME1.page9.CHART1.click
		ENCAUTOMATICAS.shape1.visible = .t.
		ENCAUTOMATICAS.GridPesq.height = ENCAUTOMATICAS.GridPesq.height - 258
		ENCAUTOMATICAS.shape3.height = ENCAUTOMATICAS.shape3.height - 258
		ENCAUTOMATICAS.ContainerSubTotais.top = ENCAUTOMATICAS.GridPesq.height +  ENCAUTOMATICAS.GridPesq.top - 1
		ENCAUTOMATICAS.ContainerSubTotais.check1.config(myPath + "\imagens\icons\pagedown_B.png", "")

		myResizeEncAutomaticas = .t.
	ELSE
	
		IF ENCAUTOMATICAS.height > ENCAUTOMATICAS.GridPesq.height + 258
	
			ENCAUTOMATICAS.detalhest1.visible		= .f.
			ENCAUTOMATICAS.detalhest1.pageFRAME1.page9.CHART1.Visible = .f.
			ENCAUTOMATICAS.shape1.visible			= .f.
			ENCAUTOMATICAS.GridPesq.height 			= ENCAUTOMATICAS.GridPesq.height + 258
			ENCAUTOMATICAS.shape3.height 			= ENCAUTOMATICAS.shape3.height + 258
			ENCAUTOMATICAS.ContainerSubTotais.top	= ENCAUTOMATICAS.GridPesq.height +  ENCAUTOMATICAS.GridPesq.top - 1
			ENCAUTOMATICAS.ContainerSubTotais.check1.config(myPath + "\imagens\icons\pageup_B.png", "")

			myResizeEncAutomaticas = .f.
		ENDIF
		
	ENDIF
	
ENDFUNC


**
Function uf_encautomaticas_Gravar
	Select crsenc
	SET FILTER TO
		
	IF RECCOUNT("crsenc")=0
		uf_perguntalt_chama("N�O PODE GRAVAR UMA ENCOMENDA SEM REGISTOS.","OK","", 48)
		RETURN .f.
	ENDIF
	
	ENCAUTOMATICAS.containerSubTotais.valorsum.SetFocus
	**SET POINT TO "."

	Select crsenc
	mntotal=reccount()
	* inicializa a r�gua apresentando um t�tulo e o n� total de registos
	regua(0,MNTOTAL,"A PROCESSAR GRAVA��O DA ENCOMENDA AUTOM�TICA...",.f.)
	
	
	** Verificar que nenhum produto foi seleccionado sem fornecedor **
	select fornecedor from crsenc into cursor uForn where enc=.t. and empty(fornecedor)
	Calculate cnt() to lcCntForn For Empty(fornecedor)
	If lcCntForn>0
		If !uf_perguntalt_chama("UM OU MAIS PRODUTOS FORAM SELECCIONADOS SEM FORNECEDOR."+chr(13)+chr(10)+"PRETENDE CONTINUAR?"+CHR(13)+"SE 'SIM' ESTES PRODUTOS N�O SER�O INCLU�DOS NA(S) ENCOMENDA(S)","Sim","N�o")
			regua(2)
			RETURN .f.
		EndIf
	ENDIF
	IF USED("uForn")
		fecha("uForn")
	ENDIF
	*******************************************************************

	** Verificar se existe bonus igual ou superior � EOQ **
	SELECT crsEnc
	GO TOP
	SCAN FOR enc=.t.
		IF crsenc.eoq==0
			uf_perguntalt_chama("ATEN��O: N�O PODE TER PRODUTOS MARCADOS PARA A ENCOMENDA COM QUANTIDADE A ENCOMENDA IGUAL A 0."+CHR(10)+CHR(10)+"PRODUTO ["+ALLTRIM(crsenc.design)+"]","OK","",48)
			regua(2)
			RETURN .f.
		ENDIF
		
		IF crsenc.eoq<=crsenc.qtbonus
			uf_perguntalt_chama("ATEN��O: N�O PODE TER BONUS IGUAL OU SUPERIOR � QUANTIDADE A ENCOMENDAR."+CHR(10)+CHR(10)+"PRODUTO ["+ALLTRIM(crsenc.design)+"]","OK","",48)
			regua(2)
			RETURN .f.
		ENDIF
	ENDSCAN
	SELECT crsenc
	GO TOP
	*******************************************************

	** Verificar que todos os fornecedores escolhidos est�o correctos na ficha **
	SELECT distinct fornecedor as nome, fornec as no, fornestab as estab FROM crsenc INTO CURSOR uForn WHERE enc=.t. AND !EMPTY(fornecedor) readwrite
	IF reccount("uForn")=0
		uf_perguntalt_chama("NENHUM PRODUTO COM FORNECEDOR ACTIVO FOI SELECCIONADO PARA ENCOMENDA","OK","",48)
		regua(2)
		RETURN .f.
	ENDIF
	
	SELECT uForn
	GO TOP
	SCAN
		IF uf_gerais_actgrelha("", "uFornVal", [select top 1 nome, no from fl (nolock) where fl.no=]+ALLTRIM(STR(uForn.no))+[ and fl.estab=]+ALLTRIM(STR(uForn.estab)))
			IF !RECCOUNT("uFornVal")>0
				uf_perguntalt_chama("O FORNECEDOR '" + ALLTRIM(uForn.nome) + "[" + Alltrim(str(uForn.no))+ "]" + "[" + Alltrim(Str(uForn.estab)) + "]' N�O TEM FICHA CRIADA. DEVE CORRIGIR ANTES DE PODER CONTINUAR.","OK","",48)
	
				fecha("uFornVal")
				
				Select crsenc
				Go top
				Scan For fornecedor==uForn.nome And enc==.t.
					Exit
				Endscan
				
				Fecha("uForn")
				
				regua(2)
				RETURN .f.
			ELSE
				replace uForn.nome WITH ALLTRIM(uFornVal.nome) IN uForn
			ENDIF
			fecha("uFornVal")
		ENDIF
		
		SELECT uForn
	ENDSCAN
	****************************************************************************
	
	** eliminar duplicados (pode acontecer quando existem altera��es ao nome do fornecedor na ficha e nao no preferencial **
	SELECT distinct nome, no, estab from uForn into cursor uForn2
	
	** Valida Plafond das Encomendas a Fornecedor **
	LOCAL lcValidaEnc
	lcValidaEnc = uf_documentos_verifyPlafondEncForn(.t., "ENCAUTOMATICAS")
	IF !lcValidaEnc
		uf_perguntalt_chama("N�O PODE ULTRAPASSAR O PLAFOND DAS ENCOMENDAS A FORNCEDOR.","OK","",48)
		regua(2)
		RETURN .f.
	ENDIF
	************************************************

	*************************************************************************************************************
	** Para os artigos escolhidos, realizar os documentos de encomenda (n� de documentos = n� de fornecedores) **
	*************************************************************************************************************
	local myecra, myStampEnc
	store '' to myecra
	
	** Guardar c�digo e nome do documento **
	LOCAL lcDocCode, lcDocNome
	lcDocCode = 2 && c�digo do documento
		
	IF uf_gerais_actgrelha("", "uCrsTsCode", [select nmdos from ts (nolock) where ndos=2])
		IF RECCOUNT("uCrsTsCode")>0
			lcDocNome = uCrsTsCode.nmdos && nome do documento
		ELSE
			uf_perguntalt_chama("O DOCUMENTO [ENCOMENDA A FORNECEDOR] N�O EST� A SER ENCONTRADO. O DOCUMENTO N�O SER� CRIADO.","OK","",48)
			regua(2)
			RETURN .f.
		ENDIF
		fecha("uCrsTsCode")
	ELSE
		uf_perguntalt_chama("O DOCUMENTO [ENCOMENDA A FORNECEDOR] N�O EST� A SER ENCONTRADO. O DOCUMENTO N�O SER� CRIADO.","OK","",48)
		regua(2)
		RETURN .f.
	ENDIF
	***************************************
	
	** PREPARAR DADOS/TOTAIS **
		
	** Vari�veis para os Cabe�alhos ENCOMENDA A FORNECEDOR **
	LOCAL valorIva1, valorIva2, valorIva3, valorIva4, valorIva5, lcTotalIva, totalComIva, totalSemIva, totalNservico, lcTotalFinal
	LOCAL totalSemIva1, totalSemIva2, totalSemIva3, totalSemIva4, totalSemIva5, lcQtSum
	LOCAL lcSql, lcNrDoc, lcDocCont
	** vari�veis para as linhas
	LOCAL lcBiStamp, lcCountS, lcOrdem, lcQtAltern, lcRef, lcQt, lcTotalLiq, lcValInsertLin, lcEpv
	****************************************************
	** criar documentos **
	
	SELECT uForn2
	GO TOP
	SCAN
		** inicializar vari�veis **
		STORE 0 TO valorIva1, valorIva2, valorIva3, valorIva4, valorIva5, lcTotalIva, totalComIva, totalSemIva, totalNservico, lcTotalFinal
		STORE 0 TO totalSemIva1, totalSemIva2, totalSemIva3, totalSemIva4, totalSemIva5, lcQtSum
		STORE 0 TO lcNrDoc, lcDocCont
		STORE '' TO lcSql
		***************************
			
		** Guardar n�mero do documento **
		IF uf_gerais_actgrelha("", "uCrsNrMax", [select ISNULL((MAX(convert(numeric,obrano))),0) as nr from bo (nolock) where bo.ndos=2 and YEAR(dataobra) = YEAR(dateadd(HOUR, ]+str(difhoraria)+[, getdate()))])
			IF RECCOUNT("uCrsNrMax")>0
				lcNrDoc = uCrsNrMax.nr + 1
			ENDIF
			fecha("uCrsNrMax")
		ELSE
			uf_perguntalt_chama("O DOCUMENTO [ENCOMENDAS A FORNECEDOR] N�O EST� A SER ENCONTRADO. O DOCUMENTO N�O SER� CRIADO.","OK","", 48)
			regua(2)
			RETURN .f.
		ENDIF
		*********************************

		** Guarda Contador por tipo de documento e origem **
		IF uf_gerais_actgrelha("", "uCrsContDoc", [select ISNULL(MAX(bo2.u_doccont),0) as doccont from bo2 (nolock) inner join bo (nolock) on bo.bostamp=bo2.bo2stamp where bo.ndos=2])
			IF RECCOUNT("uCrsContDoc")>0
				lcDocCont = uCrsContDoc.doccont + 1
			ELSE
				lcDocCont = 1
			ENDIF
			fecha("uCrsContDoc")
		ENDIF
		****************************************************

		** guardar dados do fornecedor **
		SELECT uforn2
		TEXT TO lcSql NOSHOW textmerge
			SELECT 
				nome
				,no
				,estab
				,tipo
				,local
				,codpost
				,ncont
				,morada
				,moeda
				,ccusto
			from 
				fl (nolock)
			where 
				no = <<uForn2.no>> 
				and estab=<<uForn2.estab>>
		ENDTEXT
		IF !uf_gerais_actgrelha("", "uCrsDadosForn", lcSql)
			uf_perguntalt_chama("OCORREU UM ERRO A LER DADOS DO FORNECEDOR. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			regua(2)
			RETURN .f.
		ENDIF
		*********************************

		** Guardar Stamp Para Cabe�alho **
		LOCAL lcStampEnc
		lcStampEnc	= uf_gerais_stamp()

		** calcular totais **		
		Select crsenc
		GO top
		SCAN FOR crsenc.fornec==uForn2.no AND crsenc.fornestab==uForn2.estab AND crsenc.enc==.t. AND crsenc.eoq>0
			lcQtSum	= lcQtSum + crsenc.eoq
			
			IF uf_gerais_getParameter("ADM0000000213","BOOL") == .t. and uf_gerais_getParameter("ADM0000000214","BOOL") == .f. && Encomendas Keiretsu
				lcTotalFinal	= crsenc.pclfornec * (crsenc.eoq - crsenc.qtbonus)
			ELSE
				IF crsenc.epcusto<=0 AND crsenc.epcult>0
					lcTotalFinal	= crsenc.epcult * (crsenc.eoq - crsenc.qtbonus)
				ELSE
					lcTotalFinal	= crsenc.epcusto * (crsenc.eoq - crsenc.qtbonus)
				ENDIF
			ENDIF
			
			DO CASE
				CASE crsenc.tabiva==1
					valorIva1		= valorIva1+(lcTotalFinal *(crsenc.iva/100))
					totalSemIva1	= totalSemIva1 + lcTotalFinal
				CASE crsenc.tabiva==2
					valorIva2		= valorIva2+(lcTotalFinal *(crsenc.iva/100))
					totalSemIva2	= totalSemIva2 + lcTotalFinal
				CASE crsenc.tabiva==3
					valorIva3		= valorIva3+(lcTotalFinal *(crsenc.iva/100))
					totalSemIva3	= totalSemIva3 + lcTotalFinal
				CASE crsenc.tabiva==4
					valorIva4		= valorIva4+(lcTotalFinal *(crsenc.iva/100))
					totalSemIva4	= totalSemIva4 + lcTotalFinal
				CASE crsenc.tabiva==5
					valorIva5		= valorIva5+(lcTotalFinal *(crsenc.iva/100))
					totalSemIva5	= totalSemIva5 + lcTotalFinal
			ENDCASE
		
			totalComIva		= totalComIva + (lcTotalFinal *(crsenc.iva/100+1))
			totalSemIva		= totalSemIva + (lcTotalFinal)
			totalNservico	= totalNservico + lcTotalFinal
			
			SELECT crsenc
		ENDSCAN

		lcTotalIva=valorIva1+valorIva2+valorIva3+valorIva4+valorIva5
		****************************************
		
		LOCAL lcTime 
		SET HOURS TO 24
		Atime=ttoc(datetime()+(difhoraria*3600),2)
		Astr=left(Atime,8)
		lcTime = Astr
		**lcTime = TIME()
		
		LOCAL lcDate 
		lcDate = uf_gerais_getDate((datetime()+(difhoraria*3600)),"SQL")
		


		** GRAVAR CABE�ALHOS **
		SELECT crsenc
		*GO top
		TEXT TO lcSql NOSHOW textmerge
			Insert into bo
				(
				bostamp
				,ndos
				,nmdos
				,obrano
				,dataobra
				,U_DATAENTR
				,nome
				,nome2
				,[no]
				,estab
				,morada
				,[local]
				,codpost
				,tipo
				,ncont
				,etotaldeb
				,dataopen
				,boano
				,ecusto
				,etotal
				,moeda
				,memissao
				,origem
				,site
				,vendedor
				,vendnm
				,obs
				,ccusto
				,sqtt14
				,ebo_1tvall
				,ebo_2tvall
				,ebo_totp1
				,ebo_totp2
				,ebo11_bins
				,ebo12_bins
				,ebo21_bins
				,ebo22_bins
				,ebo31_bins
				,ebo32_bins
				,ebo41_bins
				,ebo42_bins
				,ebo51_bins
				,ebo52_bins
				,ebo11_iva
				, ebo12_iva
				, ebo21_iva
				, ebo22_iva
				, ebo31_iva
				, ebo32_iva
				, ebo41_iva
				, ebo42_iva
				, ebo51_iva
				, ebo52_iva
				,
				ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora
				)
			Values 
				(
				'<<ALLTRIM(lcStampEnc)>>', <<lcDocCode>>, '<<lcDocNome>>', <<lcNrDoc>>, '<<lcDate>>', '<<lcDate>>',
				'<<ALLTRIM(uCrsDadosForn.nome)>>', '', <<uCrsDadosForn.no>>, <<uCrsDadosForn.estab>>, '<<ALLTRIM(uCrsDadosForn.morada)>>',
				'<<ALLTRIM(uCrsDadosForn.local)>>', '<<ALLTRIM(uCrsDadosForn.codpost)>>', '<<ALLTRIM(uCrsDadosForn.Tipo)>>', '<<ALLTRIM(uCrsDadosForn.Ncont)>>',
				<<ROUND(totalSemIva,2)>>, '<<lcDate>>', year(dateadd(HOUR, <<difhoraria>>, getdate())), <<ROUND(totalSemIva,2)>>, <<ROUND(totalComIva,2)>>, '<<ALLTRIM(uCrsDadosForn.moeda)>>', 'EURO', 'BO', '<<ALLTRIM(mySite)>>',
				<<ch_vendedor>>, '<<ALLTRIM(ch_vendnm)>>', 'Documento gerado Via Encomendas Semi-Autom�ticas', '<<uCrsDadosForn.ccusto>>',
				<<lcQtSum>>, <<ROUND(totalSemIva,2)>>, <<ROUND(totalSemIva,2)>>, <<ROUND(totalSemIva,2)>>, <<ROUND(totalSemIva,2)>>,
				<<totalSemIva1>>, <<totalSemIva1>>, <<totalSemIva2>>, <<totalSemIva2>>, <<totalSemIva3>>, <<totalSemIva3>>, <<totalSemIva4>>, <<totalSemIva4>>, <<totalSemIva5>>, <<totalSemIva5>>,
				<<valorIva1>>, <<valorIva1>>, <<valorIva2>>, <<valorIva2>>, <<valorIva3>>, <<valorIva3>>, <<valorIva4>>, <<valorIva4>>, <<valorIva5>>, <<valorIva5>>,
				'<<m_chinis>>','<<lcDate>>', '<<ALLTRIM(lcTime)>>', '<<m_chinis>>', '<<lcDate>>', '<<ALLTRIM(lcTime)>>'
				)
				
	
		ENDTEXT
		IF !uf_gerais_actgrelha("", "", lcSql)
			uf_perguntalt_chama("OCORREU UM ERRO A GERAR DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			regua(2)
			RETURN .f.
		ENDIF
		
		
		lcSql=''

		TEXT TO lcSql NOSHOW textmerge
			Insert into bo2
				(
				bo2stamp, autotipo, pdtipo, etotalciva, ETOTIVA, u_class, u_doccont, armazem,
				ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora
				)
			Values
				(
				'<<ALLTRIM(lcStampEnc)>>', 1, 1, <<ROUND(totalComIva,2)>>, <<ROUND(lcTotalIva,2)>>, 'Encomenda Semi-Autom�tica', <<lcDocCont>>, <<myArmazem>>,
				'<<m_chinis>>', '<<lcDate>>', '<<ALLTRIM(lcTime)>>', '<<m_chinis>>', '<<lcDate>>', '<<ALLTRIM(lcTime)>>'
				)
		ENDTEXT
		IF !uf_gerais_actgrelha("", "", lcSql)
			uf_perguntalt_chama("OCORREU UM ERRO A GERAR DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			regua(2)
			RETURN .f.
		ENDIF
		
		
	
		
		&&certific��o documento
*!*			lcSqlCert = uf_pagamento_gravarCert(lcStampEnc, lcDate, ALLTRIM(lcTime), lcDocCode , ROUND(totalComIva,2),'BO',0)
*!*			lcSqlCert = uf_gerais_trataPlicasSQL(lcSqlCert)	
*!*			
*!*			lcSQL = ''
*!*			TEXT TO lcSQL NOSHOW TEXTMERGE
*!*				exec up_gerais_execSql 'Documentos - Insert', 1,'<<lcSqlCert>>', '', '', '' , '', ''
*!*			ENDTEXT 
*!*											
*!*			IF !uf_gerais_actGrelha("","",lcSQL)
*!*				uf_perguntalt_chama("OCORREU UMA ANOMALIA A GRAVAR O DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
*!*				RETURN .f.		
*!*			ENDIF
		
		
		************************** && FIM DOS TOTAIS

		** Gravar Linhas **
		STORE "" TO lcBiStamp, lcRef
		STORE 0 TO lcCountS, lcOrdem, lcQtAltern, lcQt, lcTotalLiq, lcEpv
		STORE .t. TO lcValInsertLin

		SELECT crsenc
		GO TOP
		SCAN FOR crsenc.fornec==uForn2.no AND crsenc.fornestab==uForn2.estab AND crsenc.enc==.t. AND crsenc.eoq>0
			** Guardar Stamp das linhas **
			lcBiStamp = uf_gerais_stamp()				
			
			** Contador para as linhas
			lcOrdem = lcOrdem + 10000
			
			** Calcular quantidade, unidade alternativa e total liquido
			lcQt = crsenc.eoq
			
				** actualiza unidades alternativas **
			TEXT TO lcSql NOSHOW textmerge
				exec up_stocks_factorConversaoFl '<<ALLTRIM(crsenc.ref)>>', <<uForn2.no>>, <<mysite_nr>>
			ENDTEXT
			IF uf_gerais_actgrelha("", [uCrsGetConv], lcSql)
				IF RECCOUNT("uCrsGetConv")>0
					IF EMPTY(uCrsGetConv.conversao)
						lcQtAltern = crsenc.eoq
					ELSE
						lcQtAltern = INT(crsenc.eoq * uCrsGetConv.conversao)
					ENDIF
				ELSE
					lcQtAltern	= INT((lcQt) * crsenc.conversao)
				ENDIF
				fecha("uCrsGetConv")
			ELSE
				lcQtAltern	= INT((lcQt) * crsenc.conversao)
			ENDIF
				*************************************
			
			IF uf_gerais_getParameter("ADM0000000213","BOOL") == .t. and uf_gerais_getParameter("ADM0000000214","BOOL") == .f. AND myencGrupo == .t.&& Encomendas Keiretsu
				lcEpv	= crsenc.pclfornec 
				lcTotalLiq	= (crsenc.eoq-crsenc.qtbonus) * crsenc.pclfornec 
			ELSE
				IF crsenc.epcusto<=0 AND crsenc.epcult>0
					lcEpv		= crsenc.epcult
					lcTotalLiq	= (crsenc.eoq-crsenc.qtbonus) * crsenc.epcult
				ELSE
					lcEpv		= crsenc.epcusto
					lcTotalLiq	= (crsenc.eoq-crsenc.qtbonus) * crsenc.epcusto
				ENDIF
			ENDIF
				
			** guardar refer�ncia **
			lcRef = crsenc.ref

			** Preencher Linhas da Bi **
			TEXT TO lcSql NOSHOW textmerge
				Insert into bi
					(
					bistamp, bostamp, nmdos, obrano, ref, codigo,
					design, qtt, qtt2, uni2qtt, u_bonus,
					u_stockact, iva, tabiva, armazem, stipo, ndos, cpoc, familia,
					no, nome, local, morada, codpost, ccusto,
					epu, edebito, eprorc, epcusto, ettdeb, ecustoind, edebitoori, u_upc,
					rdata, dataobra, dataopen, resfor, lordem,
					lobs,
					ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora
					)
				Values
					(
					'<<ALLTRIM(lcBiStamp)>>', '<<ALLTRIM(lcStampEnc)>>', '<<ALLTRIM(lcDocNome)>>', <<lcNrDoc>>, '<<ALLTRIM(lcRef)>>', '<<ALLTRIM(crsenc.codigo)>>',
					'<<ALLTRIM(crsenc.design)>>', <<lcQt>>, 0, <<lcQtAltern>>, <<crsenc.qtbonus>>,
					<<crsenc.stock>>, <<crsenc.iva>>, <<crsenc.tabiva>>, <<myArmazem>>, 4, <<lcDocCode>>, <<crsenc.cpoc>>, '<<crsenc.familia>>',
					<<uCrsDadosForn.No>>, '<<ALLTRIM(uCrsDadosForn.Nome)>>', '<<ALLTRIM(uCrsDadosForn.Local)>>', '<<ALLTRIM(uCrsDadosForn.Morada)>>', '<<ALLTRIM(uCrsDadosForn.CodPost)>>', '<<uCrsDadosForn.ccusto>>',
					<<lcEpv>>, <<lcEpv>>, <<lcEpv>>, <<lcEpv>>, <<lcTotalLiq>>, <<lcEpv>>, <<lcEpv>>, <<ROUND(lcTotalLiq / lcQt,2)>>,
					'<<lcDate>>', '<<lcDate>>', '<<lcDate>>', 1, <<lcOrdem>>,
					'',
					'<<m_chinis>>', '<<lcDate>>', '<<ALLTRIM(lcTime)>>', '<<m_chinis>>', '<<lcDate>>', '<<ALLTRIM(lcTime)>>'
					)
			ENDTEXT
			IF !uf_gerais_actGrelha("","",lcSql)
				lcValInsertLin = .f.
			ENDIF
			
			** Preencher Linhas da Bi2 **
			TEXT TO lcSql NOSHOW textmerge
				Insert into bi2
					(
					bi2stamp, bostamp, morada, local, codpost
					)
				Values
					(
					'<<ALLTRIM(lcBiStamp)>>', '<<ALLTRIM(lcStampEnc)>>', '<<ALLTRIM(uCrsDadosForn.Morada)>>', '<<ALLTRIM(uCrsDadosForn.Local)>>', '<<ALLTRIM(uCrsDadosForn.CodPost)>>'
					)
			ENDTEXT
			IF !uf_gerais_actgrelha("", "", lcSql)
				lcValInsertLin = .f.
			ENDIF
	
		
			STORE 0 TO lcQtAltern, lcQt, lcTotalLiq	
			SELECT crsenc
		ENDSCAN
		******************* && fim das linhas

		IF uf_gerais_getParameter("ADM0000000213","BOOL") == .t. && Encomendas Keiretsu
			uf_encautomaticas_estableceRelacoesComEncClientes(ALLTRIM(lcStampEnc))
		ENDIF

		IF USED("uCrsDadosForn")
			fecha("uCrsDadosForn")
		ENDIF

		SELECT uForn2
	ENDSCAN
	************************
	
	** actualiza ultimo registo **
	uf_gerais_gravaUltRegisto('BO', lcStampEnc)
	
	IF lcValInsertLin == .f.
		regua(2)
		uf_perguntalt_chama("UM OU MAIS PRODUTOS N�O FORAM CORRECTAMENTE GRAVADOS NA ENCOMENDA. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
	ENDIF
	
	** PARA OS PRODUTOS COM st.EOQ>0 E/OU ESGOTADOS, LIMPAR OS MESMOS DADOS NA ST
	SELECT crsenc
	GO top
	select ref from CRSenc into cursor resetVal where enc=.t. AND CRSenc.qttadic>0
	
	select resetVal
	GO TOP
	SCAN
		uf_gerais_actgrelha("", "", [update st set eoq=0 where st.ref=']+ALLTRIM(resetVal.ref)+['])
	ENDSCAN
	IF USED("resetVal")
		fecha("resetVal")
	ENDIF
	*****************************************************************************
	
	uf_documentos_chama(ALLTRIM(lcStampEnc))
	
	IF !EMPTY(myGetEncStamp)
		TEXT TO lcSql NOSHOW TEXTMERGE
			DELETE
				B_encAutoSave
			where
				stamp='<<ALLTRIM(myGetEncStamp)>>'
		ENDTEXT
		IF !uf_gerais_actgrelha("", "", lcSql)
			uf_perguntalt_chama("OCORREU UM PROBLEMA A LIMPAR ENCOMENDA GUARDADA. POR FAVOR CONTACTE O SUPORTE","OK","", 16)
		ENDIF
	ENDIF
	
	
	
	regua(2)
	
	uf_encautomaticas_sair(.t.)
ENDFUNC 


**
FUNCTION uf_encautomaticas_criaDocumentoConferenciaDeEncomendas
	Lparameters lcStampEncForn
	Local lcbostamp 
	
	lcbostamp = uf_gerais_stamp()
	lcndos = 42
	lcnmdos = "Conferencia Enc. Forn."

	text to lcSQL noshow textmerge 
		(select numero = isnull(max(obrano)+1,1) from bo where ndos = 42)
	endtext 
	IF !uf_gerais_actgrelha("", "ucrsObranoConfEnc", lcSql)
		uf_perguntalt_chama("OCORREU UM ERRO A GERAR DOCUMENTO [CONFERENCIA ENC. FORN.] POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .F.
	ENDIF
	
	IF RECCOUNT("ucrsObranoConfEnc") == 0
		uf_perguntalt_chama("OCORREU UM ERRO A GERAR DOCUMENTO [CONFERENCIA ENC. FORN.] POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		return .f.
	ENDIF
	
	
	Select ucrsObranoConfEnc
	lcNumero = ucrsObranoConfEnc.numero
	
	** GRAVAR CABE�ALHOS **
	TEXT TO lcSql NOSHOW textmerge
		insert into bo (
			bostamp, ndos, nmdos, obrano, dataobra, U_DATAENTR, nome
			,nome2, [no], estab, morada, [local], codpost, tipo, ncont
			,etotaldeb, dataopen, boano, ecusto, etotal, moeda, memissao
			,origem, site, vendedor, vendnm, obs, ccusto, sqtt14, ousrinis
			,ousrdata, ousrhora, usrinis, usrdata, usrhora
		)
		select 
			'<<lcbostamp>>',42,'Conferencia Enc. Forn.',<<lcNumero>>, dataobra, U_DATAENTR, nome
			,nome2, [no], estab, morada, [local], codpost, tipo, ncont
			,etotaldeb, dataopen, boano, ecusto, etotal, moeda, memissao
			,origem, site, vendedor, vendnm, obs, ccusto, sqtt14, ousrinis
			,ousrdata, ousrhora, usrinis, usrdata, usrhora
		from 
			bo (NOLOCK)
		where	
			bostamp = '<<Alltrim(lcStampEncForn)>>'
	ENDTEXT
	IF !uf_gerais_actgrelha("", "", lcSql)
		uf_perguntalt_chama("OCORREU UM ERRO A GERAR DOCUMENTO [CONFERENCIA ENC. FORN.] POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	ENDIF

	lcSql=''
	TEXT TO lcSql NOSHOW textmerge
		insert into bo2 (
			bo2stamp, autotipo, pdtipo, etotalciva, ETOTIVA, u_class, u_doccont, armazem,
			ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora, u_fostamp
		)

		Select '<<lcbostamp>>', autotipo, pdtipo, etotalciva, ETOTIVA, u_class, u_doccont, armazem,
			ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora, bo2stamp
		From 
			bo2 (NOLOCK)
		Where
			bo2stamp = '<<Alltrim(lcStampEncForn)>>'

	ENDTEXT
	IF !uf_gerais_actgrelha("", "", lcSql)
		uf_perguntalt_chama("OCORREU UM ERRO A GERAR DOCUMENTO [CONFERENCIA ENC. FORN.] POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	ENDIF
	
	
	** Preencher Linhas da Bi **
	TEXT TO lcSql NOSHOW textmerge
		insert into bi (	
			bistamp, bostamp, ndos, nmdos, obrano, ref, codigo,
			design, qtt, qtt2, uni2qtt, u_bonus,
			u_stockact, iva, tabiva, armazem, stipo, cpoc, familia,
			no, nome, local, morada, codpost, ccusto,
			epu, edebito, eprorc, epcusto, ettdeb, ecustoind, edebitoori, u_upc,
			rdata, dataobra, dataopen, resfor, lordem,
			lobs,
			ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora,obistamp
		)

		select  'E' + LEFT(bistamp,23), '<<lcbostamp>>', 42, 'Conferencia Enc. Forn.', <<lcNumero>>, ref, codigo,
				design, qtt, qtt2 = 0, uni2qtt, u_bonus,
				u_stockact, iva, tabiva, armazem, stipo, cpoc, familia,
				no, nome, local, morada, codpost, ccusto,
				epu, edebito, eprorc, epcusto, ettdeb, ecustoind, edebitoori, u_upc,
				rdata, dataobra, dataopen, resfor, lordem,
				lobs,
				ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora, bistamp
		From
			bi (NOLOCK)
		Where
			bostamp = '<<Alltrim(lcStampEncForn)>>'
	ENDTEXT
	IF !uf_gerais_actGrelha("","",lcSql)
		uf_perguntalt_chama("OCORREU UM ERRO A GERAR DOCUMENTO [CONFERENCIA ENC. FORN.] POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	ENDIF
	
	** Preencher Linhas da Bi2 **
	TEXT TO lcSql NOSHOW textmerge
		insert into bi2 (	
			bi2stamp, bostamp, morada, local, codpost
		)

		select 'E' + LEFT(bi2stamp,23), '<<lcbostamp>>', morada, local, codpost
		From
			bi2
		Where
			bostamp = '<<Alltrim(lcStampEncForn)>>'
	ENDTEXT
	IF !uf_gerais_actgrelha("", "", lcSql)
		uf_perguntalt_chama("OCORREU UM ERRO A GERAR DOCUMENTO [CONFERENCIA ENC. FORN.] POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	ENDIF

ENDFUNC 


** 
FUNCTION uf_encautomaticas_estableceRelacoesComEncClientes
	Lparameters lcEncFornstamp

	IF empty(lcEncFornstamp)
		return .f.
	ENDIF

	TEXT TO lcSql NOSHOW TEXTMERGE
		select 
			bostamp
		from 
			bo (nolock) 
		where 
			bo.ndos = 41
			and fechada = 0
	ENDTEXT
	IF !uf_gerais_actgrelha("", "ucrsEncClientesK", lcSql)
		uf_perguntalt_chama("OCORREU UM PROBLEMA A VERIFICAR ENCOMENDAS DE CLIENTES. POR FAVOR CONTACTE O SUPORTE","OK","", 16)
		return .f.
	ENDIF
	
	SElect ucrsEncClientesK
	Go Top
	Scan 
		
		TEXT TO lcSql NOSHOW TEXTMERGE
			insert into encKeiretsuLnk  values ('<<ucrsEncClientesK.bostamp>>','<<lcEncFornstamp>>')
		ENDTEXT
		IF !uf_gerais_actgrelha("", "", lcSql)
			uf_perguntalt_chama("OCORREU UM PROBLEMA A GERAR LIGA��O ENTRE ENCOMENDAS DE CLIENTES E ENCOMENDAS A FORNECEDORES. POR FAVOR CONTACTE O SUPORTE","OK","", 16)
			return .f.
		ENDIF

	ENDSCAN

ENDFUNC


**
FUNCTION uf_encautomaticas_DynamicBackColor_Bonus
	LPARAMETERS lcRecno
	
	RETURN IIF(crsenc.alertaBonus == 1,rgb[255,0,0],IIF(lcRecno%2!=0,Rgb[255,255,255],rgb[240,240,240]))
ENDFUNC


**
FUNCTION uf_encautomaticas_DynamicBackColor_Esgotados
	LPARAMETERS lcRecno

	RETURN IIF(crsenc.Esgotado == "SIM",rgb[255,0,0],IIF(lcRecno%2!=0,Rgb[255,255,255],rgb[240,240,240]))
ENDFUNC


**
FUNCTION uf_encautomaticas_DynamicBackColor_PCL
	LPARAMETERS lcRecno
	
	RETURN IIF(crsenc.alertaPCL == 1,rgb[255,0,0],IIF(lcRecno%2!=0,Rgb[255,255,255],rgb[240,240,240]))
ENDFUNC


**
FUNCTION uf_encautomaticas_DynamicBackColor_StockGH
	LPARAMETERS lcRecno
	
	RETURN IIF(crsenc.alertaStockGH == 1,rgb[255,0,0],IIF(lcRecno%2!=0,Rgb[255,255,255],rgb[240,240,240]))
ENDFUNC


**
FUNCTION uf_encautomaticas_chamaDocumentos
	LPARAMETERS lcRef, lcDesign, lcDoc
	
	IF !EMPTY(lcRef)
		uf_LISTADOCUMENTOS_chama(lcRef, 0, lcDesign, "ST", lcDoc)
	ENDIF

ENDFUNC


**
FUNCTION uf_encautomaticas_selFornecedorLostFocus
	LPARAMETERS tcNumForn
	
	IF USED("crsenc")
	 
	 	IF EMPTY(tcNumForn) OR VAL(tcNumForn) == 0 
		 	SELECT crsenc
	        Replace  crsenc.fornecedor     	WITH ""
	        Replace  crsenc.fornec         	WITH 0
	        Replace  crsenc.fornestab    	WITH 0
	       	Replace  crsenc.enc    	WITH .f.
	       
			uf_perguntalt_chama("N�o foi possivel encontrar o fornecedor com o n�mero introduzido","OK","", 64)
		ELSE
	 
		 	TEXT TO lcSql NOSHOW TEXTMERGE
				exec up_fornecedores_pesquisar '', <<LEFT(ALLTRIM(tcNumForn),5)>>, 0, '', '', '', 0, '',<<mysite_nr>>
			ENDTEXT
			IF !uf_gerais_actGrelha("",[ucrsPesqEntTemp],lcSql)
				uf_perguntalt_chama("N�o foi possivel encontrar o fornecedor com o n�mero introduzido. Por favor contacte o suporte.","OK","", 64)
				RETURN .f.
			ENDIF
		 
		 	IF RECCOUNT("ucrsPesqEntTemp")>0
		        Select ucrsPesqEntTemp
		        SELECT crsenc
		        Replace  crsenc.fornecedor     	WITH ALLTRIM(ucrsPesqEntTemp.nome)
		        Replace  crsenc.fornec         	WITH ucrsPesqEntTemp.no
		        Replace  crsenc.fornestab    	WITH ucrsPesqEntTemp.estab
		        Replace  crsenc.bonusfornec    	WITH ucrsPesqEntTemp.bonus
		        Replace  crsenc.pclfornec    	WITH ucrsPesqEntTemp.pcl
		        Replace  crsenc.esgotado    	WITH ucrsPesqEntTemp.esgotado
		
		        SELECT crsenc
		        Replace  crsenc.enc    WITH .t.
			ELSE
				SELECT crsenc
		        Replace  crsenc.fornecedor     	WITH ""
		        Replace  crsenc.fornec         	WITH 0
		        Replace  crsenc.fornestab    	WITH 0
	     	    Replace  crsenc.enc    	WITH .f.
		       
		        uf_perguntalt_chama("N�o foi possivel encontrar o fornecedor com o n�mero introduzido.","OK","", 64)
	        ENDIF 
	       
       	ENDIF 
        
        
        ENCAUTOMATICAS.detalhest1.refresh
        
        ** recalcula totais
		uf_encautomaticas_ActualizaQtValorEnc()
        
    ENDIF	
ENDFUNC


**
FUNCTION uf_encautomaticas_sair
	LPARAMETERS tcBool
	
	IF !tcBool
		IF !uf_perguntalt_chama("TEM A CERTEZA QUE DESEJA SAIR DESTE ECR�?"+CHR(13)+CHR(13)+ "Se 'SIM' ir� perder todas as altera��es.","Sim","N�o")
			RETURN .f.
		ENDIF
	ENDIF
	
	IF USED("crsenc")
		fecha("crsenc")
	ENDIF
		
	IF USED("ucrsFornceEncAut")
		fecha("ucrsFornceEncAut")
	ENDIF
	
	IF USED("ucrsConfEmpresas")
		fecha("ucrsConfEmpresas")
	ENDIF
	

	RELEASE myOrderEncAutomaticas
	RELEASE myGetEncStamp
	RELEASE MyTipoEncAuto
	RELEASE myControlaEventoQt
			
	ENCAUTOMATICAS.hide
	ENCAUTOMATICAS.release
ENDFUNC



**
FUNCTION uf_encautomaticas_DuplicarLinha
	SELECT crsenc 
	lcStStamp = crsenc.ststamp 
	
	SELECT TOP 1 * FROM crsenc WHERE ALLTRIM(ststamp) = ALLTRIM(lcStStamp) ORDER BY ststamp INTO CURSOR ucrsDuplicarLinhaTemp READWRITE 
	SELECT crsenc
	APPEND FROM DBF("ucrsDuplicarLinhaTemp")
	
	IF USED("ucrsDuplicarLinhaTemp")
		fecha("ucrsDuplicarLinhaTemp")
	ENDIF 
	

ENDFUNC



FUNCTION uf_encautomaticas_CarregaTipoProduto

    public  myTabTipoProduto
    myTabTipoProduto=""
    
    && Cursor uCrsTempMotives	
	IF USED("uCrsTempTipoProduto")
		fecha("uCrsTempTipoProduto")
	ENDIF 
	CREATE CURSOR uCrsTempMotives(id varchar(254), descr varchar(254))	

    IF uf_gerais_actgrelha("", "uCrsTempTipoProduto", [select distinct design as TipoProduto from b_famFamilias (nolock) where inactivo = 0 order by design] )
        uf_valorescombo_chama("myTabTipoProduto", 0, "uCrsTempTipoProduto", 2, "TipoProduto ", "TipoProduto",.f.,.f.,.t.,.f.)
    ENDIF 
	
	IF USED("uCrsTempTipoProduto")
		fecha("uCrsTempTipoProduto")
	ENDIF
	
    ENCAUTOMATICAS.tipoProduto.Value= myTabTipoProduto

	RELEASE myTabTipoProduto
ENDFUNC 


FUNCTION uf_encautomaticas_CarregaTipoEncomenda()

    public  myTabTipoEncomenda
    myTabTipoEncomenda=""
    
    && Cursor uCrsTempMotives	
	IF USED("uCrsTempTipoProduto")
		fecha("uCrsTempTipoProduto")
	ENDIF 
	CREATE CURSOR uCrsTempTipoEncomenda(descricao varchar(254))	

    IF uf_gerais_actgrelha("", "uCrsTempTipoEncomenda", [SELECT descricao='Esgotados' UNION ALL  SELECT descricao='Sem Esgotados'] )
        uf_valorescombo_chama("myTabTipoEncomenda", 0, "uCrsTempTipoEncomenda", 2, "descricao", "descricao",.f.,.f.,.t.,.f.)
    ENDIF 
	
	IF USED("uCrsTempTipoEncomenda")
		fecha("uCrsTempTipoEncomenda")
	ENDIF
	
	IF (myTabTipoEncomenda == "TODOS")
	    myTabTipoEncomenda=""
	ENDIF 
	
	
    ENCAUTOMATICAS.TipoEncomenda.Value= myTabTipoEncomenda

	RELEASE myTabTipoEncomenda

ENDFUNC