FUNCTION uf_LOGMSG_chama
	**LPARAMETERS lcStamp
		
	** cria cursor vazio
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_mensagens_pesquisa ''
	ENDTEXT 
	IF !uf_gerais_actgrelha("", "uCrsLOGMSG", lcSql)
		uf_perguntalt_chama("ERRO AO LER A TABELA DE LOGS!","OK","",16)
		RETURN .F.
	ENDIF 
	
	&& Controla Abertura do Painel			
	if type("LOGMSG")=="U"
		DO FORM LOGMSG
	ELSE
		LOGMSG.show
	ENDIF
	
	** carrega dados
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_mensagens_pesquisa ']+m_chinis+['
	ENDTEXT 	
	IF !uf_gerais_actgrelha("LOGMSG.GridPesq", "uCrsLOGMSG", lcSql)
		uf_perguntalt_chama("ERRO AO LER A TABELA DE LOGS!","OK","",16)
		RETURN .F.
	ENDIF 
	
ENDFUNC


**
FUNCTION uf_LOGMSG_carregamenu
	LOGMSG.menu1.adicionaopcao("atualizar", "Atualizar", myPath + "\imagens\icons\actualizar_w.png", "uf_LOGMSG_Pesquisa", "F2")
ENDFUNC


**
FUNCTION uf_LOGMSG_Pesquisa
	LOCAL lcSQL
	
	regua(0,0,"A atualizar...")
	
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_mensagens_pesquisa ']+m_chinis+['
	ENDTEXT

	uf_gerais_actGrelha("LOGMSG.GridPesq", "ucrsLOGMSG", lcSQL)

	regua(2)
	
	LOGMSG.Refresh
	
ENDFUNC

**
FUNCTION uf_LOGMSG_sair
	**Fecha Cursores
	IF USED("uCrsLOGMSG")
		fecha("uCrsLOGMSG")
	ENDIF 
	
	LOGMSG.hide
	LOGMSG.release
ENDFUNC