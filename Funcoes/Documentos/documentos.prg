**Carrega Prgs dependentes
Do PESQDOCUMENTOS
Do PESQDOCUMENTOSCLIENTES
Do PESQENTIDADES
Do IMPORTARDOC
Do DADOSENTIDADE
Do OPCOESDIVERSASDOC
Do FECHOAUTOMATICODOC
Do CONFFACTURAS
Do CONFROBOTICA
Do PREPARARENCOMENDA
Do ENCAUTOMATICAS
Do reservaclsms
Do bonusprod
Do valpvpalt
Do condicoescomerciais
Do EVOVENDAS
Do importSellOut
Do confirmacaoencomendascliente
Do consolidacaocompras
Do logmsg
Do descdoc
Do consultrobot


*** Classes Linhas ***
** Define class para os eventos **
Define Class pbordodoc As Custom

	Procedure up_documentos_seltipoDoc
		uf_documentos_seltipoDoc()
	Endproc

	**
	Procedure proc_ActivaEventosDoc
		uf_documentos_EventosGridDoc()
	Endproc

	Procedure proc_AlteraQTT
		uf_documentos_alteraqtt()
	Endproc

	**
	Procedure proc_clickCCUSTO

		If myDocIntroducao == .T. Or myDocAlteracao == .T.

			lcSQL = ""
			TEXT TO lcSQL TEXTMERGE NOSHOW
                Select
                    '' descricao
                union all
                Select
                    descricao
                from
                    cu (nolock)
                order by
                    descricao
			ENDTEXT

			If !uf_gerais_actGrelha("", "ucrsCCusto", lcSQL)
				uf_perguntalt_chama("N�O FOI POSS�VEL VERIFICAR A LISTAGEM DE CENTROS DE CUSTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				Return .F.
			Endif

			uf_valorescombo_chama(Iif(myTipoDoc == "FO","FN.FNCCUSTO","BI.CCUSTO"), 2, "ucrsCCusto", 2, "DESCRICAO", "DESCRICAO")
		Endif
	Endproc

	**
	Procedure proc_clickLOTE
		Lparameters lcClickColumn
		With DOCUMENTOS.PageFrame1.Page1.gridDoc
			For i=1 To .ColumnCount
				If Upper(.Columns(i).ControlSource)=="BI.LOTE" Or;
						Upper(.Columns(i).ControlSource)=="FN.LOTE"
					lcClickColumn = i
				Endif
			Endfor
		Endwith

		If myTipoDoc = "BO"
			Select Bi

			Do Case
				Case Bi.familia = "97" Or Bi.familia = "98"
					DOCUMENTOS.PageFrame1.Page1.gridDoc.Columns(lcClickColumn).text1.ReadOnly = .F.

				Case Empty(Bi.usaLote)
					DOCUMENTOS.PageFrame1.Page1.gridDoc.Columns(lcClickColumn).text1.ReadOnly = .T.

				Otherwise
					DOCUMENTOS.PageFrame1.Page1.gridDoc.Columns(lcClickColumn).text1.ReadOnly = .F.
			Endcase
		Else

			Select FN

			Do Case
				Case FN.familia = "97" Or FN.familia = "98"
					DOCUMENTOS.PageFrame1.Page1.gridDoc.Columns(lcClickColumn).text1.ReadOnly = .F.

				Case Empty(FN.usaLote)
					DOCUMENTOS.PageFrame1.Page1.gridDoc.Columns(lcClickColumn).text1.ReadOnly = .T.

				Otherwise
					DOCUMENTOS.PageFrame1.Page1.gridDoc.Columns(lcClickColumn).text1.ReadOnly = .F.
			Endcase

		Endif


		If DOCUMENTOS.PageFrame1.Page1.gridDoc.Columns(lcClickColumn).text1.ReadOnly
			Return .T.
		Endif


		uf_documentos_escolheLote()

		**

		DOCUMENTOS.PageFrame1.Page1.gridDoc.SetFocus && Corrige bug Documento sem linhas
		DOCUMENTOS.PageFrame1.Page1.detalhest1.Refresh && Actualiza Detalhes
	Endproc

	**
	Procedure proc_interactiveChangeLOTE
		DOCUMENTOS.interactiveChangeLOTE = .T.
	Endproc

	**
	Procedure proc_lostFocusLOTE
		If !Empty(DOCUMENTOS.interactiveChangeLOTE)

			With DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.Page12.PageFrame1
				If .ActivePage == 1
					.Page1.Container1.txtLoteConf.Value = Iif(myTipoDoc == "FO",FN.LOTE,Bi.LOTE)
					.Page1.Container1.txtLoteConf.Refresh
					.Page1.Container1.txtLote.Refresh
				Else
					.Page2.Container2.txtLoteConf.Value = Iif(myTipoDoc == "FO",FN.LOTE,Bi.LOTE)
					.Page1.Container1.txtLoteConf.Refresh
					.Page1.Container1.txtLote.Refresh
				Endif
			Endwith

			**
			DOCUMENTOS.PageFrame1.Page1.gridDoc.SetFocus && Corrige bug Documento sem linhas
			&& Actualiza Detalhes
			DOCUMENTOS.PageFrame1.Page1.detalhest1.Refresh

			DOCUMENTOS.interactiveChangeLOTE = .F.
		Endif
	Endproc

	**
	Procedure proc_gotFocusLOTE
		With DOCUMENTOS.PageFrame1.Page1.gridDoc
			For i=1 To .ColumnCount
				**EVENTO Lote
				If Upper(.Columns(i).ControlSource)=="BI.LOTE" Or;
						Upper(.Columns(i).ControlSource)=="FN.LOTE"
					If myDocIntroducao == .T. Or myDocAlteracao == .T.
						If myTipoDoc == "FO"
							Select FN
							If Empty(FN.usaLote)
								.Columns(i).ReadOnly = .T.
							Else
								.Columns(i).ReadOnly = .F.
							Endif
						Endif
						If myTipoDoc == "BO"
							If Empty(Bi.usaLote)
								.Columns(i).ReadOnly = .T.
							Else
								.Columns(i).ReadOnly = .F.
							Endif
						Endif
					Else
						.Columns(i).ReadOnly = .T.
					Endif

				Endif
			Endfor
		Endwith
	Endproc

	**
	Procedure proc_clickMQUEBRA
		If myDocIntroducao == .T. Or myDocAlteracao == .T.

			lcSQL = ""
			TEXT TO lcSQL TEXTMERGE NOSHOW
				select descr as motivo from B_motivosQuebra (nolock) ORDER BY descr
			ENDTEXT

			If !uf_gerais_actGrelha("", "ucrsMotivosQuebra", lcSQL)
				uf_perguntalt_chama("N�O FOI POSS�VEL VERIFICAR MOTIVOS DE QUEBRA. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				Return .F.
			Endif
			**
			Select Bi
			uf_valorescombo_chama("BI.U_MQUEBRA", 2, "ucrsMotivosQuebra", 2, "motivo", "motivo")
			If  Alltrim(myTipoDoc) == "BO"
				Select Bi
				uf_documentos_editarTemp(Bi.bistamp)
			Else
				Select FN
				uf_documentos_editarTemp(FN.fnstamp)
			Endif

		Endif
	Endproc

	**
	Procedure proc_gotFocusMQUEBRA
		With DOCUMENTOS.PageFrame1.Page1.gridDoc
			For i=1 To .ColumnCount
				**EVENTO Lote
				If Upper(.Columns(i).ControlSource)=="BI.U_MQUEBRA"
					.Columns(i).ReadOnly = .T.
				Endif
			Endfor
		Endwith
	Endproc

	**
	Procedure proc_clickIva
		Public mylcCodIva
		Local lcTaxaIva, lcTabIva

		If myDocIntroducao == .T. Or myDocAlteracao == .T.

			uf_valorescombo_chama("mylcCodIva", 0, "select sel = convert(bit,0), codigo = codigo,taxa from taxasiva (nolock) where ativo=1", 1, "codigo", "taxa")

			If !Empty(mylcCodIva)
				Do Case
					Case myTipoDoc == "FO"

						lcTaxaIva = uf_gerais_getTabelaIva(mylcCodIva,"TAXA")
						lcTabIva = mylcCodIva
						Select FN
						Replace FN.tabiva With lcTabIva
						Replace FN.IVA With lcTaxaIva

						Select FN
						mySelIvaMan=1
						uf_documentos_recalculaTotaisFN()

					Case myTipoDoc == "BO"

						lcTaxaIva = uf_gerais_getTabelaIva(mylcCodIva,"TAXA")
						lcTabIva = mylcCodIva

						Select Bi
						Replace Bi.tabiva With lcTabIva
						Replace Bi.IVA With lcTaxaIva
						mySelIvaMan=1
						uf_documentos_recalculaTotaisBI()

					Otherwise
						**
				Endcase

				uf_documentos_actualizaTotaisCAB()
			Endif
		Endif

		**
		DOCUMENTOS.PageFrame1.Page1.gridDoc.SetFocus && Corrige bug Documento sem linhas
		DOCUMENTOS.PageFrame1.Page1.ContainerLinhas.Total.Refresh
		&& Actualiza Detalhes
		DOCUMENTOS.PageFrame1.Page1.detalhest1.Refresh

	Endproc

	Procedure proc_clickusr2
		Public mylcCodUSR2
		If myDocIntroducao == .T. Or myDocAlteracao == .T.
			uf_valorescombo_chama("mylcCodUSR2", 0, "select distinct sel = convert(bit,0), codigo = ref, tabela = nome from stfami (nolock) order by ref", 1, "codigo", "codigo, tabela")
			Do Case
				Case myTipoDoc == "BO"
					Select Bi
					Replace Bi.usr2 With mylcCodUSR2

					If Used("ucrsts")
						If ucrsts.tipodos = 4 And Empty(Bi.ref)
							Replace Bi.Design With Alltrim(uf_gerais_getUmvalor("stfami", "nome", "ref = '" + Alltrim(mylcCodUSR2) + "'"))
						Endif
					Endif

				Otherwise
					**
			Endcase
		Endif
		**
		DOCUMENTOS.PageFrame1.Page1.gridDoc.SetFocus && Corrige bug Documento sem linhas

	Endproc

	** evento Inc. Iva
	Procedure proc_actualizaTotaisDOCIvaIncl

		Do Case
			Case myTipoDoc == "FO"
				uf_documentos_recalculaTotaisFN()

			Case myTipoDoc == "BO"
				uf_documentos_recalculaTotaisBI()

			Otherwise
				**
		Endcase

		uf_documentos_actualizaTotaisCAB()

	Endproc

	**
	Procedure proc_navegast
		uf_documentos_navegast()
	Endproc

	**
	Procedure proc_eventoref
		lcRetorno = uf_documentos_eventoref()
		uf_documentos_EventosGridDoc(.T.) &&desactiva

		If !Empty(lcRetorno)
			**
			DOCUMENTOS.PageFrame1.Page1.gridDoc.SetFocus && Corrige bug Documento sem linhas
			DOCUMENTOS.PageFrame1.Page1.ContainerLinhas.Total.Refresh
			&& Actualiza Detalhes
			DOCUMENTOS.PageFrame1.Page1.detalhest1.Refresh
		Endif

		If  Alltrim(myTipoDoc) == "BO"
			Select Bi
			uf_documentos_editarTemp(Bi.bistamp)
		Else
			Select FN
			uf_documentos_editarTemp(FN.fnstamp)
		Endif

	Endproc

	Procedure proc_actualizaqtrec

		uf_actualizaqtrec()

	Endproc

	**
	Procedure proc_actualizaTotaisLinhas
		Local uv_qttEmbEnc

		If myTipoDoc == "BO"

			If Bi.u_bonus > Bi.qtt And Bi.qtt > 0
				uf_perguntalt_chama("A QUANTIDADE DE B�NUS N�O PODE SER SUPERIOR � QUANTIDADE RECECIONADA.","OK","",16)
				Select Bi
				Replace Bi.u_bonus With 0
			Endif


			If uf_gerais_getParameter_Site('ADM0000000218', 'BOOL' , mySite) And Bi.ndos = 2
				Select Bi

				uv_qttEmbEnc = uf_gerais_getUmvalor("st", "isnull(qttEmbEnc,0)", "st.ref = '"+Alltrim(Bi.ref) +"' AND site_nr = "+Str(mySite_nr))

				If uv_qttEmbEnc > 0

					If Bi.qtt%uv_qttEmbEnc <>0
						If !uf_perguntalt_chama("Este produto � vendido em packs. Deseja arredondar por defeito ou por excesso?" ,"Defeito","Excesso")
							If uf_encautomaticas_ValidaQttEmb(Bi.qtt, Bi.ref, mySite_nr, 2) > 0
								Replace Bi.qtt With  uf_encautomaticas_ValidaQttEmb(Bi.qtt, Bi.ref, mySite_nr, 2)
							Endif
						Else
							If uf_encautomaticas_ValidaQttEmb(Bi.qtt, Bi.ref, mySite_nr, 1) > 0
								Replace Bi.qtt With uf_encautomaticas_ValidaQttEmb(Bi.qtt, Bi.ref, mySite_nr, 1)
							Endif
						Endif
					Else
						Select Bi
						Replace Bi.qtt With uf_encautomaticas_ValidaQttEmb(Bi.qtt, Bi.ref, mySite_nr, 1)
					Endif
				Else
					Select Bi
					Replace Bi.qtt With uf_encautomaticas_ValidaQttEmb(Bi.qtt, Bi.ref, mySite_nr, 1)
				Endif
			Else

				Select Bi
				Replace Bi.qtt With Bi.qtt


			Endif
			lcBistamp = Bi.bistamp
			uf_corrigeDesconto("BI", lcBistamp)
			uf_documentos_recalculaTotaisBI()
			**
			If  uf_gerais_getParameter("ADM0000000214","BOOL") == .T.
				uf_condicoesComerciais_FeeGestaoLinha()
			Endif
		Endif
		If myTipoDoc == "FO"

			If FN.u_bonus > FN.qtt
				uf_perguntalt_chama("A QUANTIDADE BONUS N�O PODE SER SUPERIOR � QUANTIDADE RECECIONADA.","OK","",16)
				Replace FN.u_bonus With 0
			Endif

			lcFnstamp = FN.fnstamp
			uf_corrigeDesconto("FN",lcFnstamp)
			uf_documentos_recalculaTotaisFN(.T.)
		Endif

		uf_documentos_actualizaTotaisCAB()

		&& Actualiza Detalhes
		DOCUMENTOS.PageFrame1.Page1.detalhest1.Refresh

		If myTipoDoc == "FO" And !Empty(lcFnstamp)
			Select FN
			Locate For FN.fnstamp = lcFnstamp
		Endif
		If myTipoDoc == "BO" And !Empty(lcBistamp)
			Select Bi
			Locate For Bi.bistamp = lcBistamp
		Endif
		uf_documentos_EventosGridDoc(.T.) &&desactiva()
		DOCUMENTOS.LockScreen = .F.
		**
		DOCUMENTOS.PageFrame1.Page1.gridDoc.SetFocus && Corrige bug Documento sem linhas
		DOCUMENTOS.PageFrame1.Page1.ContainerLinhas.Total.Refresh
		&& Actualiza Detalhes
		DOCUMENTOS.PageFrame1.Page1.detalhest1.Refresh


		If  Alltrim(myTipoDoc) == "BO"
			Select Bi
			uf_documentos_editarTemp(Bi.bistamp)
		Else
			Select FN
			uf_documentos_editarTemp(FN.fnstamp)
		Endif

	Endproc

	Procedure proc_valid_u_dtval
		If myTipoDoc == "FO"
			lcFnstamp = FN.fnstamp
			uf_documentos_valid_u_dtval()
		Endif

		If  Alltrim(myTipoDoc) == "BO"
			Select Bi
			uf_documentos_editarTemp(Bi.bistamp)
		Else
			Select FN
			uf_documentos_editarTemp(FN.fnstamp)
		Endif

	Endproc

	**
	Procedure proc_actualizaUni2Qtt
		uf_documentos_actualizaUni2Qtt()
		uf_documentos_EventosGridDoc(.T.) &&desactiva()

		If  Alltrim(myTipoDoc) == "BO"
			Select Bi
			uf_documentos_editarTemp(Bi.bistamp)
		Else
			Select FN
			uf_documentos_editarTemp(FN.fnstamp)
		Endif
	Endproc

	**
	Procedure proc_calcularPrecoUni
		If myTipoDoc == "FO"
			Select FN
			lcFnstamp = FN.fnstamp
		Endif
		uf_documentos_calcularPrecoUni()

		If myTipoDoc == "BO"
			**
			If  uf_gerais_getParameter("ADM0000000214","BOOL") == .T.
				uf_condicoesComerciais_FeeGestaoLinha()
			Endif
		Endif
		If myTipoDoc == "FO" And !Empty(lcFnstamp)
			Select FN
			Locate For FN.fnstamp = lcFnstamp
		Endif

		DOCUMENTOS.PageFrame1.Page1.detalhest1.Refresh
		DOCUMENTOS.PageFrame1.Page1.gridDoc.SetFocus
		uf_documentos_EventosGridDoc(.T.) &&desactiva()


		If  Alltrim(myTipoDoc) == "BO"
			Select Bi
			uf_documentos_editarTemp(Bi.bistamp)
		Else
			Select FN
			uf_documentos_editarTemp(FN.fnstamp)
		Endif
	Endproc

	**
	Procedure proc_OrdenaPorDesignDoc
		uf_documentos_OrdenaPorDesignDoc()
		uf_documentos_EventosGridDoc(.T.) &&desactiva()
	Endproc

	**
	Procedure proc_ApagarAutocompleteRef
		Lparameters nKeyCode, nShiftAltCtrl
		mykeypressed = nKeyCode
	Endproc

	**
	Procedure proc_autocompleteRef
		Lparameters nKeyCode
		uf_documentos_autocompleteRef(nKeyCode)
	Endproc

	**
	Procedure proc_OrdenaPorDesignDoc
		uf_documentos_OrdenaPorDesignDoc()

		If  Alltrim(myTipoDoc) == "BO"
			Select Bi
			uf_documentos_editarTemp(Bi.bistamp)
		Else
			Select FN
			uf_documentos_editarTemp(FN.fnstamp)
		Endif
	Endproc

	**
	Procedure proc_OrdenaPorRefDoc
		uf_documentos_OrdenaPorRefDoc()
	Endproc

	**
	Procedure proc_ValidaArmazem
		uf_documentos_validaArmazem()
	Endproc

	**
	Procedure proc_FeeKeiretsu
		uf_condicoesComerciais_FeeGestaoLinha()
		uf_documentos_EventosGridDoc(.T.) &&desactiva()
	Endproc

	Procedure proc_clickDESCPANEL
		If Alltrim(cabdoc.Doc) = 'V/Factura Med.' And FN.etiliquido>0
			uf_DESCDOC_chama(Alltrim(FN.fnstamp))
		Endif
	Endproc

	Procedure proc_pressDESCPANEL
		Lparameters nKeyCode, nShiftAltCtrl
		mykeypressed = nKeyCode
		If Alltrim(cabdoc.Doc) = 'V/Factura Med.' And FN.etiliquido>0
			If mykeypressed = 43
				uf_DESCDOC_chama(Alltrim(FN.fnstamp))
				Nodefault
				Select FN
				Replace FN.desconto With MyDescLinAcum
			Endif
		Endif
	Endproc

	Procedure proc_insDESCPANEL
		If Alltrim(cabdoc.Doc) = 'V/Factura Med.'
			uf_DESCDOC_limpa(Alltrim(FN.fnstamp))
		Endif
	Endproc

	Procedure proc_pressVALPANEL
		Lparameters nKeyCode, nShiftAltCtrl
		mykeypressed = nKeyCode
		If Alltrim(cabdoc.Doc) = 'V/Factura Med.'
			If mykeypressed = 46
				Nodefault
			Endif
		Endif


		If  Alltrim(myTipoDoc) == "BO"
			Select Bi
			uf_documentos_editarTemp(Bi.bistamp)
		Else
			Select FN
			uf_documentos_editarTemp(FN.fnstamp)
		Endif

	Endproc

	Procedure proc_actualiza_desp_Linhas
		uf_documentos_recalc_despesa_linhas()
	Endproc

	Procedure proc_selArmazemDestinoTrf

		uf_documentos_selArmazemDestino()

		Select Bi
		**UPDATE BI SET BI.AR2MAZEM = upv_armazemDestino

	Endproc

	Procedure proc_clickPvpValido

		uf_documentos_precosValidos("fn.u_pvp", "fn.u_pvp", .T.)

		uf_documentos_actulizaTotaisLinhas_NoBind()

		With DOCUMENTOS.PageFrame1.Page1.gridDoc
			For i = 1 To .ColumnCount
				If uf_gerais_compstr(.Columns(i).ControlSource, 'FN.U_PVP')
					.Columns(i).DynamicBackColor = "uf_documentos_dynamicBackColorGridAlerta('U_PVP')"
					.Columns(i).DynamicForeColor = "uf_documentos_dynamicForeColorGridAlerta('U_PVP')"
				Endif
			Endfor
		Endwith

		DOCUMENTOS.Refresh

	Endproc

	Procedure proc_lostFocusPvpValido

		uf_documentos_precosValidos("fn.u_pvp", "fn.u_pvp", .F.)

		With DOCUMENTOS.PageFrame1.Page1.gridDoc
			For i = 1 To .ColumnCount
				If uf_gerais_compstr(.Columns(i).ControlSource, 'FN.U_PVP')
					.Columns(i).DynamicBackColor = "uf_documentos_dynamicBackColorGridAlerta('U_PVP')"
					.Columns(i).DynamicForeColor = "uf_documentos_dynamicForeColorGridAlerta('U_PVP')"
				Endif
			Endfor
		Endwith

		DOCUMENTOS.Refresh

	Endproc


	Procedure proc_lostFocusObs
		With DOCUMENTOS.PageFrame1.Page1.gridDoc
			For i = 1 To .ColumnCount
				If uf_gerais_compstr(.Columns(i).ControlSource, 'BI.LOBS')
					uf_documentos_editarTemp(Bi.bistamp)
				Endif
			Endfor
		Endwith

		DOCUMENTOS.Refresh

	Endproc

Enddefine


**
Define Class commandButtonDestinos As CommandButton
	Procedure Click
		**
		Public myDocAlteracao, myDocIntroducao
		If myDocAlteracao == .F. And myDocIntroducao == .F.
			uf_OrigensDestinos_Chama('DOCDESTINOS')
		Endif
	Endproc
Enddefine


**
Define Class commandButtonOrigens As CommandButton
	Procedure Click
		**
		Public myDocAlteracao, myDocIntroducao
		If myDocAlteracao == .F. And myDocIntroducao == .F.
			uf_OrigensDestinos_Chama('DOCORIGENS')
		Endif
	Endproc
Enddefine


**
Function uf_documentos_Chama
	Lparameters tcStamp
	** Verificar se existe nova vers�o para reiniciar o software
	uf_chk_nova_versao()

	If Type("tcStamp") != 'C'
		tcStamp = ''
	Endif

	** Controle de Permiss�es
	*!*		IF !uf_gerais_validaPermUser(ch_userno,ch_grupo,'Gest�o de Documentos - Clientes')
	*!*			uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ACEDER AO PAINEL Gest�o de Documentos - Clientes.","OK","",48)
	*!*			RETURN .f.
	*!*		ENDIF
	*!*
	*!*		IF !uf_gerais_validaPermUser(ch_userno, ch_grupo, 'Gest�o de Documentos - Fornecedores')
	*!*			uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ACEDER AO PAINEL Gest�o de Documentos - Fornecedores.","OK","",48)
	*!*			RETURN .f.
	*!*		ENDIF

	Public myTipoDoc, MYENCCONJUNTA, myDocAGravar, myResizeDocumentos, upv_armazemDestino
	Public myResizeDocumentos, mykeypressed, lcvalorantigo


	If myDocAlteracao == .T. Or myDocIntroducao == .T.
		uf_perguntalt_chama("O ECR� DE DOCUMENTOS ENCONTRA-SE EM MODO DE INTRODU��O OU EDI��O.","OK","",48)
		If !(Type("DOCUMENTOS") == "U")
			DOCUMENTOS.Show
		Endif
		Return .F.
	Endif

	**upv_armazemDestino = uf_gerais_getUmValor("empresa_arm", "armazem", "empresa_no = " + ASTR(mySite_nr), "armazem asc")

	Store "" To myTipoDoc
	Store .F. To myDocAlteracao, myDocIntroducao
	Store .F. To myResizeDocumentos

	Public MyDescLinAcum
	Store 0 To MyDescLinAcum

	Local lcSQL, lcValidaAberto

	regua(0,11,"A carregar painel...")
	regua(1,1,"A carregar painel...")
	**App Gerais (O parametro � irrelevante, tratamento igual para BO ou FO)
	If Empty(tcStamp)
		tcStamp = uf_gerais_devolveUltRegisto('BO')
	Endif

	regua(1,2,"A carregar painel...")
	** Valida acesso ao documento
	If uf_documentos_validaPermissoesAcesso(tcStamp) == .F.
		tcStamp=""
		*RETURN .f.
	Endif

	regua(1,3,"A carregar painel...")
	**Limpa cursor de cabecalho
	uf_documentos_criaCursorCabDoc()

	regua(1,4,"A carregar painel...")
	**carrega documentos disponiveis
	uf_documentos_CarregaDocs()

	&& Abre o ecr� caso n�o esteja aberto
	If Type("DOCUMENTOS")=="U"

		**Valida Licenciamento
		If (uf_gerais_addConnection('DOC') == .F.)
			regua(2)
			Return .F.
		Endif

		regua(1,5,"A carregar painel...")
		Do Form DOCUMENTOS

		regua(1,6,"A carregar painel...")
		** Obtem informa��o sobre o documento escolhido
		If !Empty(tcStamp)
			uf_documentos_preencheCabDoc(tcStamp)

			If Used("ucrsts") And ucrsts.tipodos = 5
				If uf_gerais_getParameter_Site('ADM0000000199', 'BOOL', mySite)

					Select cabdoc
					If !uf_documentos_updateEncCentral(cabdoc.cabstamp)
						uf_perguntalt_chama("N�o foi poss�vel sincronizar o estado da encomenda na Central. Por favor volte a tentar mais tarde.","OK","",16)
					Else

						TEXT TO uv_sql TEXTMERGE NOSHOW
							SELECT status, pagamento, modo_envio, usrhora, usrdata, usrinis FROM bo2(nolock) where bo2stamp = '<<ALLTRIM(cabDoc.cabstamp)>>'
						ENDTEXT

						uf_gerais_actGrelha("", "uc_estadoEncUpdate", uv_sql)

						Select uc_estadoEncUpdate

						Select cabdoc
						Replace cabdoc.estadoenc With Alltrim(uc_estadoEncUpdate.Status),;
							cabdoc.pagamento With Alltrim(uc_estadoEncUpdate.pagamento),;
							cabdoc.entrega With Alltrim(uc_estadoEncUpdate.modo_envio),;
							cabdoc.DATAALTERA With uc_estadoEncUpdate.usrdata,;
							cabdoc.HORAALTERA With uc_estadoEncUpdate.usrhora,;
							cabdoc.USERALTERA With uc_estadoEncUpdate.usrinis

					Endif

				Endif

			Endif

		Endif

		DOCUMENTOS.Menu1.gravar.img1.Picture = myPath + "\imagens\icons\guardar_w.png"

	Else

		regua(1,6,"A carregar painel...")
		** Obtem informa��o sobre o documento escolhido
		If !Empty(tcStamp)
			uf_documentos_preencheCabDoc(tcStamp)

			If Used("ucrsts") And ucrsts.tipodos = 5
				If uf_gerais_getParameter_Site('ADM0000000199', 'BOOL', mySite)

					Select cabdoc
					If !uf_documentos_updateEncCentral(cabdoc.cabstamp)
						uf_perguntalt_chama("N�o foi poss�vel sincronizar o estado da encomenda na Central. Por favor volte a tentar mais tarde.","OK","",16)
					Else

						TEXT TO uv_sql TEXTMERGE NOSHOW
							SELECT status, pagamento, modo_envio, usrhora, usrdata, usrinis FROM bo2(nolock) where bo2stamp = '<<ALLTRIM(cabDoc.cabstamp)>>'
						ENDTEXT

						uf_gerais_actGrelha("", "uc_estadoEncUpdate", uv_sql)

						Select uc_estadoEncUpdate

						Select cabdoc
						Replace cabdoc.estadoenc With Alltrim(uc_estadoEncUpdate.Status),;
							cabdoc.pagamento With Alltrim(uc_estadoEncUpdate.pagamento),;
							cabdoc.entrega With Alltrim(uc_estadoEncUpdate.modo_envio),;
							cabdoc.DATAALTERA With uc_estadoEncUpdate.usrdata,;
							cabdoc.HORAALTERA With uc_estadoEncUpdate.usrhora,;
							cabdoc.USERALTERA With uc_estadoEncUpdate.usrinis

					Endif

				Endif
			Endif

		Endif

		DOCUMENTOS.Show
	Endif

	*** Cria classe para permitir os eventos dinamicos **
	If Type("DOCUMENTOS.oCust") # "O"
		DOCUMENTOS.AddObject("oCust","pbordodoc")
	Endif

	**Preenche Cursor ucrConfigDoc usado em grande parte das fun��es com as configura��es do documento actual
	regua(1,7,"A carregar painel...")
	uf_documentos_ConfiguracoesDoc(.T.)

	regua(1,8,"A carregar painel...")
	uf_documentos_CalculaArredondamentos()

	regua(1,9,"A carregar painel...")
	uf_documentos_CalculaTotais()

	regua(1,10,"A carregar painel...")
	uf_documentos_calculaTotaisRodape()
	regua(1,11,"A carregar painel...")

	uf_documentos_controlaWkLinhas()
	uf_documentosControlaObj()

	regua(2)
Endfunc


Function uf_documentos_abrirConferencia

	If(myUsaServicosRobot== .F. And Used("cabDOC") )
		uf_perguntalt_chama("FUNCIONALIDADE INDISPONIVEL","OK","",16)
		Return .F.
	Endif

	Local lcNumDoc
	Local lcSQL
	Local lcTotal

	Select cabdoc
	Go Top
	lcNumDoc = Alltrim(cabdoc.NUMDOC)

	If(Empty(lcNumDoc))
		uf_perguntalt_chama("POR FAVOR PREENCHA O N�MERO DO DOCUMENTO","OK","",16)
		Return .F.
	Endif


	fecha("ucrsRobotResponsePayload")
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		select COUNT(*) as total  from robot_response_payload(nolock) where deliveryNumber =   '<<lcNumDoc>>'
	ENDTEXT


	If !uf_gerais_actGrelha("", "ucrsRobotResponsePayload", lcSQL)
		uf_perguntalt_chama("N�O FOI POSS�VEL VERIFICAR O DOCUMENTO NO ROBOT. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		Return .F.
	Endif


	lcTotal = 0
	Select ucrsRobotResponsePayload
	Go Top
	lcTotal = ucrsRobotResponsePayload.Total


	fecha("ucrsRobotResponsePayload")


	If(lcTotal >0)

		lcSQL=''
		TEXT TO lcSQL TEXTMERGE NOSHOW
			UPDATE robot_response_payload SET conferido = 0 where deliveryNumber = '<<lcNumDoc>>'
		ENDTEXT
		If !uf_gerais_actGrelha("","",lcSQL)
			uf_perguntalt_chama("N�O FOI POSS�VEL ABRIR A CONFER�NCIA DO DOCUMENTO NO ROBOT. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			Return .F.
		Else
			uf_perguntalt_chama("DOCUMENTO ABERTO PARA CONFER�NCIA.","OK","",64)
		Endif

	Else
		uf_perguntalt_chama("O DOCUMENTO N�O EST� DISPON�VEL PARA CONFER�NCIA","OK","",16)
	Endif




Endfunc


** Carrega Documento com permiss�es na Combobox
** Verifica Permiss�es <NomeDocumento - Introduzir>
Function uf_documentos_CarregaDocs

	If Used("ucrsComboDoc")
		fecha("ucrsComboDoc")
	Endif

	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
			exec up_perfis_validaAcessoDocs_utilizados '<<ch_userno>>', '<<ALLTRIM(ch_grupo)>>', 'Introduzir', 'FO,BO', 0, '<<mySite>>'
	ENDTEXT

	If !uf_gerais_actGrelha("", "ucrsComboDoc", lcSQL)
		uf_perguntalt_chama("N�O FOI POSS�VEL ENCONTRAR DOCUMENTOS.","OK","",16)
		Return .F.
	Endif



	If Reccount("ucrsComboDoc")<5 Then

		fecha("ucrsComboDoc")
		lcSQL = ""
		TEXT TO lcSQL TEXTMERGE NOSHOW
				exec up_perfis_validaAcessoDocs '<<ch_userno>>', '<<ALLTRIM(ch_grupo)>>', 'Introduzir', 'FO,BO', 0, '<<mySite>>'
		ENDTEXT

		If !uf_gerais_actGrelha("", "ucrsComboDoc", lcSQL)
			uf_perguntalt_chama("N�O FOI POSS�VEL ENCONTRAR DOCUMENTOS.","OK","",16)
			Return .F.
		Endif



	Endif

	**Cria Cursor Documento de Fornecedor (combobox)
	If !Used("ucrsComboDocFl")
		Create Cursor ucrsComboDocFl (Doc c(30))
		Select ucrsComboDocFl
		Append Blank
		Replace ucrsComboDocFl.Doc With 'Factura'
		Append Blank
		Replace ucrsComboDocFl.Doc With 'Guia Remessa'
		Append Blank
		Replace ucrsComboDocFl.Doc With 'Pedido de Regulariza��o'
	Endif

	**Cria Cursor Documento de Fornecedor (combobox)
	If !Used("ucrsEstadoDoc")
		Create Cursor ucrsEstadoDoc(ref c(1), Descr c(20))
		Select ucrsEstadoDoc
		Append Blank
		Replace ucrsEstadoDoc.ref With 'F'
		Replace ucrsEstadoDoc.Descr With 'Fechado'
		Append Blank
		Replace ucrsEstadoDoc.ref With 'A'
		Replace ucrsEstadoDoc.Descr With 'Aberto'
	Endif

	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
			SELECT 'EURO' as MOEDA UNION ALL SELECT DISTINCT MOEDA FROM CB (nolock)
	ENDTEXT
	If !uf_gerais_actGrelha("", "ucrsMoedaDoc", lcSQL)
		uf_perguntalt_chama("N�O FOI POSS�VEL DEFINIR A MOEDA DO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		Return .F.
	Endif

	&& cursor tipos de entrega site
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
			select campo from b_multidata where tipo='site_deliveryStatus' order by campo
	ENDTEXT

	If !uf_gerais_actGrelha("", "ucrssite_deliveryStatus", lcSQL)
		uf_perguntalt_chama("N�O FOI POSS�VEL VERIFICAR A LISTAGEM DE CENTROS DE CUSTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		Return .F.
	Endif
	*********************

	&& cursor tipos de tipos de pagamento site
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
			select campo from b_multidata where tipo='site_paymentType' order by campo
	ENDTEXT

	If !uf_gerais_actGrelha("", "ucrssite_paymentType", lcSQL)
		uf_perguntalt_chama("N�O FOI POSS�VEL VERIFICAR A LISTAGEM DE CENTROS DE CUSTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		Return .F.
	Endif
	*********************

	&& cursor tipos de momentos de pagamento site
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
			select campo from b_multidata where tipo='site_paymentMoment' order by campo
	ENDTEXT

	If !uf_gerais_actGrelha("", "ucrssite_paymentMoment", lcSQL)
		uf_perguntalt_chama("N�O FOI POSS�VEL VERIFICAR A LISTAGEM DE CENTROS DE CUSTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		Return .F.
	Endif
	*********************

	&& cursor tipos de modo de envio do site
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
			select campo from b_multidata where tipo='site_shippingmode' order by campo
	ENDTEXT

	If !uf_gerais_actGrelha("", "ucrssite_shippingmode", lcSQL)
		uf_perguntalt_chama("N�O FOI POSS�VEL VERIFICAR A LISTAGEM DE CENTROS DE CUSTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		Return .F.
	Endif
	*********************

	** Cursor de Op��es Diversas
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
			exec up_documentos_OpcoesDiversas
	ENDTEXT
	If !uf_gerais_actGrelha("", "uCrsOpcoesDiversasDoc", lcSQL)
		uf_perguntalt_chama("N�O FOI POSS�VEL VERIFICAR A LISTAGEM DE OP��ES DIVERSAS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		Return .F.
	Endif
	*************************************

	If Used("ucrsComboDoc2")
		fecha("ucrsComboDoc2")
	Endif
	If Vartype(lcorigempesq)!="U"
		Do Case
			Case Alltrim(lcorigempesq) == 'CLIENTES'
				Select nomedoc From ucrsComboDoc Where bdempresas = 'CL' Into Cursor ucrsComboDoc2 Readwrite
			Case Alltrim(lcorigempesq) == 'FORNECEDORES'
				Select nomedoc From ucrsComboDoc Where bdempresas = 'FL' Or bdempresas = 'AG' Into Cursor ucrsComboDoc2 Readwrite
			Otherwise
				Select nomedoc From ucrsComboDoc Into Cursor ucrsComboDoc2 Readwrite
		Endcase
	Else
		Select nomedoc From ucrsComboDoc Into Cursor ucrsComboDoc2 Readwrite
	Endif
	*ENDIF
Endfunc


**
Function uf_documentos_criaCursorCabDoc

	* Cria
	If Used("CabDOC")
		fecha("CabDOC")
	Endif
	Create Cursor cabdoc (	cabstamp c(40), Doc c(80), NUMINTERNODOC N(9), NUMDOC c(20), Nome c(80), NOME2 c(60), TIPO c(20), NO N(9), ESTAB N(9), ;
		CONDPAG c(254), DATAVENC D, MOEDA c(254), DATADOC D, DATAINTERNA D, DATAREGISTO D, ESTADODOC c(254), DESCFIN N(15,6), ;
		VALORDESCFIN N(9,3), DOCFL c(254), DOCFLNO c(34), NOINTERNO N(9), CONTADORDOC N(9), BASEINC N(15,3), IVA N(15,3), Total N(15,3), ;
		VALORIVA1 N(12,3), VALORIVA2 N(12,3), VALORIVA3 N(12,3), VALORIVA4 N(12,3), VALORIVA5 N(12,3), VALORIVA6 N(12,3), VALORIVA7 N(12,3), VALORIVA8 N(12,3), VALORIVA9 N(12,3), VALORIVA10 N(12,3),VALORIVA11 N(12,3), VALORIVA12 N(12,3),VALORIVA13 N(12,3),;
		TOTALNSERVICO N(9,3), ESTADO c(1), HORAREGISTO c(8), USERREGISTO c(60), DATAALTERA D, HORAALTERA c(8), USERALTERA c(60), ;
		ENCENVIADA N(1), DATAENTREGA D, ARMAZEM N(2), LOGI1 L, PLANO L, ;
		MORADA c(90), Local c(254), CODPOST c(254), NCONT c(30), EMAIL c(45), TELEFONE c(30), ;
		FOID N(9), U_DOCCONT N(9), U_CLASS c(120), OBS c(254), DATAFINAL D,;
		XPDDATA D, XPDHORA c(5), XPDVIATURA c(60), XPDMORADA c(55), XPDCODPOST c(40), XPDTELEFONE c(50), XPDCONTACTO c(50), XPDEMAIL c(45), PAIS N(9,0),CCUSTO c(20),ATDocCode c(20), tabiva N(5,0), ;
		BLOQPAG L, EXPORTADO L, EAIVAIN N(19,6), EDESCC N(19,6), arredondaPVP L, consignacao L, id_tesouraria_conta i, ousrdata D, ousrHORA c(5), imput_desp N(15,2), NRRECEITA c(50) , CODEXT c(50), SITE c(20), anexos L,;
		u_dataentr D, u_horaentr c(5), entrega c(60), pagamento c(60), momentopagam c(60), estadoenc c(60), pinacesso c(10), pinopcao c(10), xpdLocalidade c(50), tipodos N(3);
		)

	**Limpa Cursores
	If Used("cabDoc")
		Select cabdoc
		Go Top
		Scan
			Delete
		Endscan
	Endif

	** CURSORES DAS LINHAS
	If !Used("BI")
		TEXT TO lcSQL NOSHOW TEXTMERGE
			exec up_Documentos_linhas 'BO','xxx',0
		ENDTEXT
		If Type("DOCUMENTOS")=="U"
			If !uf_gerais_actGrelha("", "BI", lcSQL)
				uf_perguntalt_chama("N�O FOI POSS�VEL DETERMINAR OS DADOS DE LINHAS DO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE!","OK","",16)
				Return .F.
			Endif
		Else
			If !uf_gerais_actGrelha("DOCUMENTOS.PageFrame1.Page1.griddoc", "BI", lcSQL)
				uf_perguntalt_chama("N�O FOI POSS�VEL DETERMINAR OS DADOS DE LINHAS DO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE!","OK","",16)
				Return .F.
			Endif
		Endif

		Select Bi
		Go Top
		Scan

			Select Bi

			Replace Bi.u_reserva With uf_gerais_getUmvalor("st","(case when st.qttcli = 0 then cast(0 as bit) else cast(1 as bit) end)","st.ref = '" + Bi.ref + "' and site_nr = " + ASTR(mySite_nr))

		Endscan

		Select Bi
		Go Top

		If Used("ucrsBiCompare")
			fecha("ucrsBiCompare")
		Endif
		Select * From Bi Into Cursor ucrsBiCompare Readwrite
	Endif

	If !Used("BI2")
		lcSQL = ''
		TEXT TO lcSQL NOSHOW TEXTMERGE
			SET FMTONLY on
			SELECT TOP 0 * FROM BI2 (nolock)
			SET FMTONLY off
		ENDTEXT
		If !uf_gerais_actGrelha("", "BI2", lcSQL)
			uf_perguntalt_chama("N�O FOI POSS�VEL DETERMINAR OS DADOS DE LINHAS DO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE!(2)","OK","",16)
			Return .F.
		Endif

	Endif

	*!*		IF !USED("BO2")
	*!*			lcSQL = ''
	*!*			TEXT TO lcSQL NOSHOW TEXTMERGE
	*!*				SET FMTONLY on
	*!*				SELECT TOP 0 * FROM BO2 (nolock)
	*!*				SET FMTONLY off
	*!*			ENDTEXT
	*!*			IF !uf_gerais_actGrelha("", "BO2", lcSQL)
	*!*				uf_perguntalt_chama("N�O FOI POSS�VEL DETERMINAR OS DADOS DO DOCUMENTO (BO2). POR FAVOR CONTACTE O SUPORTE!(2)","OK","",16)
	*!*				RETURN .f.
	*!*			ENDIF
	*!*
	*!*		ENDIF

	If !Used("FN")
		lcSQL = ""

		TEXT TO lcSQL NOSHOW TEXTMERGE
			exec up_Documentos_linhas 'FO','xxx',0
		ENDTEXT

		If Type("DOCUMENTOS")=="U"
			If !uf_gerais_actGrelha("", "FN", lcSQL)
				uf_perguntalt_chama("N�O FOI POSS�VEL DETERMINAR OS DADOS DE LINHAS DO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE!","OK","",16)
				Return .F.
			Endif
		Else
			If !uf_gerais_actGrelha("DOCUMENTOS.PageFrame1.Page1.griddoc", "FN", lcSQL)
				uf_perguntalt_chama("N�O FOI POSS�VEL DETERMINAR OS DADOS DE LINHAS DO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE!","OK","",16)
				Return .F.
			Endif
		Endif

		Select FN
		Go Top
		Scan

			Select FN

			Replace FN.u_reserva With uf_gerais_getUmvalor("st","(case when st.qttcli = 0 then cast(0 as bit) else cast(1 as bit) end)","st.ref = '" + FN.ref + "' and site_nr = " + ASTR(mySite_nr))

		Endscan

		Select FN
		Go Top

		If Used("ucrsFnCompare")
			fecha("ucrsFnCompare")
		Endif
		Select * From FN Into Cursor ucrsFnCompare Readwrite

	Endif

	If !Used("ucrsListCondPagForn")
		lcSQL = ""
		TEXT TO lcSQL NOSHOW TEXTMERGE
			exec up_documentos_ConPagFornec
		ENDTEXT
		If !uf_gerais_actGrelha("", "ucrsListCondPagForn", lcSQL)
			uf_perguntalt_chama("N�O FOI POSSIVEL VERIFICAR AS CONDI��ES DE PAGAMENTO DO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			Return .F.
		Endif
	Endif

	** Cursor de descontos de linhas
	If !Used("ucrsfndesclin")
		lcSQL = ""
		TEXT TO lcSQL NOSHOW TEXTMERGE
			SELECT * FROM fndesclin WHERE 0=1
		ENDTEXT
		If !uf_gerais_actGrelha("", "ucrsfndesclin", lcSQL)
			uf_perguntalt_chama("N�O FOI POSSIVEL CRIAR O CURSOR DE DESCONTOS DE LINHAS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			Return .F.
		Endif
	Endif

	If !Used("fi_trans_info")
		lcSQL = ''
		TEXT TO lcsql TEXTMERGE NOSHOW
				set fmtonly on
				select
					*
					,CAST('' as varchar(30)) as verify_i
					,CAST('' as varchar(100)) as verify_s
					, CAST('' as varchar(100)) as tcode
					, CAST('' as varchar(254)) as verifymsg
					, CAST('' as varchar(100)) as verif_reason
					,CAST('' as varchar(30)) as dispense_i
					, CAST('' as varchar(100)) as dispense_s
					, CAST('' as varchar(254)) as dispensemsg
					, CAST('' as varchar(100)) as disp_reason
					,CAST('' as varchar(30)) as undo_i
					, CAST('' as varchar(100)) as undo_s
					, CAST('' as varchar(254)) as undodispensemsg
					, CAST('' as varchar(100)) as undo_reason
					, CAST('' as varchar(10)) as tipo
					, CAST(0 as bit) as conferido
					, CAST(0 as bit) as deleted
				from
					fi_trans_info (nolock)
				set fmtonly off
		ENDTEXT
		If  .Not. uf_gerais_actGrelha("", "fi_trans_info", lcSQL)
			uf_perguntalt_chama("Ocorreu uma anomalia ao criar o cursor [fi_trans_info]. Por favor reinicie o Software.", "OK", "", 16)
			Return .F.
		Endif
	Else
		Delete From fi_trans_info
	Endif

	If !Used("mixed_bulk_pend")
		lcSQL = ''
		TEXT TO lcsql TEXTMERGE NOSHOW
				set fmtonly on
				select
					*
				from
					mixed_bulk_pend (nolock)
				set fmtonly off
		ENDTEXT
		If  .Not. uf_gerais_actGrelha("", "mixed_bulk_pend", lcSQL)
			uf_perguntalt_chama("Ocorreu uma anomalia ao criar o cursor [mixed_bulk_pend]. Por favor reinicie o Software.", "OK", "", 16)
			Return .F.
		Endif
	Else
		Delete From mixed_bulk_pend
	Endif

	If  .Not. Used("nrserie_com_results")
		If  .Not. uf_gerais_actGrelha("", 'nrserie_com_results', 'set fmtonly on select * from nrserie_com_results (nolock) set fmtonly off')
			uf_perguntalt_chama("Ocorreu uma anomalia ao criar o cursor [nrserie_com_results]. Por favor reinicie o Software.", "OK", "", 16)
			Return .F.
		Endif
	Else
		Delete From nrserie_com_results
	Endif

	** Cria Cursores para conferencia de encomenda com robot
	uf_CONFROBOTICA_CriaCursor()

Endfunc


**
Function uf_documentos_preencheCabDoc
	Lparameters tcStamp

	uf_documentos_criaCursorCabDoc()

	** OBTEM INFORMA��O SOBRE O DOCUMENTO ESCOLHIDO
	* Cabe�alho
	If Upper(Alltrim(myTipoDoc)) == "BO"
		**BO

		If uf_gerais_actGrelha("", "ltBO", "SELECT * FROM BO (nolock) WHERE BO.BOSTAMP = '" + Alltrim(tcStamp) + "'")

			Public lc
			**BO2
			If uf_gerais_actGrelha("", "ltBO2", "SELECT * FROM BO2 (nolock) WHERE BO2.BO2STAMP = '" + Alltrim(tcStamp) + "'")
				lcNrRegistos = 0
				Select cabdoc
				Append Blank

				If ltBO.FECHADA = .F.
					lcestadodoc = "A"
				Else
					lcestadodoc = "F"
				Endif

				Replace cabdoc.cabstamp			With	Alltrim(ltBO.BOSTAMP)
				Replace cabdoc.Doc 				With	Alltrim(ltBO.NMDOS)
				Replace cabdoc.NUMINTERNODOC 	With	ltBO.ndos
				Replace cabdoc.NUMDOC			With	Alltrim(Str(ltBO.OBRANO))
				Replace cabdoc.Nome				With	Alltrim(ltBO.Nome)
				Replace cabdoc.NOME2			With	Alltrim(ltBO.NOME2)
				Replace cabdoc.NO				With	ltBO.NO
				Replace cabdoc.ESTAB			With	ltBO.ESTAB
				Replace cabdoc.CONDPAG			With	ltBO.TPDESC
				Replace cabdoc.MOEDA			With	ltBO.MOEDA
				Replace cabdoc.DATADOC			With	ltBO.DATAOBRA
				Replace cabdoc.DATAENTREGA		With	ltBO.u_dataentr
				Replace cabdoc.ESTADODOC		With	lcestadodoc
				Replace cabdoc.CONTADORDOC		With	ltBO2.U_DOCCONT
				Replace cabdoc.BASEINC			With	ltBO.etotaldeb
				Replace cabdoc.IVA				With	ltBO2.ETOTIVA
				Replace cabdoc.Total			With	Round(ltBO.etotal,2)
				Replace cabdoc.VALORIVA1		With	ltBO.EBO11_IVA
				Replace cabdoc.VALORIVA2		With	ltBO.EBO21_IVA
				Replace cabdoc.VALORIVA3		With	ltBO.EBO31_IVA
				Replace cabdoc.VALORIVA4		With	ltBO.EBO41_IVA
				Replace cabdoc.VALORIVA5		With	ltBO.EBO51_IVA
				Replace cabdoc.VALORIVA6		With	ltBO.EBO61_IVA
				Replace cabdoc.VALORIVA7		With	ltBO2.EBO71_IVA
				Replace cabdoc.VALORIVA8		With	ltBO2.EBO81_IVA
				Replace cabdoc.VALORIVA9		With	ltBO2.EBO91_IVA
				Replace cabdoc.VALORIVA10		With	ltBO2.EBO101_IVA
				Replace cabdoc.VALORIVA11		With	ltBO2.EBO111_IVA
				Replace cabdoc.VALORIVA12		With	ltBO2.EBO121_IVA
				Replace cabdoc.VALORIVA13		With	ltBO2.EBO131_IVA
				Replace cabdoc.ousrdata			With	ltBO.ousrdata
				Replace cabdoc.ousrHORA			With	ltBO.ousrHORA
				Replace cabdoc.SITE 			With 	ltBO.SITE
				Replace cabdoc.tipodos			With 	uf_gerais_getUmvalor("ts", "(CASE WHEN tipodos = '' THEN 0 ELSE CONVERT(NUMERIC,tipodos) END)", "ndos = " + ASTR(ltBO.ndos))

				** CAMPO CLASSIFICA��O PARA PEDIDO DES REGULARIZA��O
				Replace cabdoc.U_CLASS		With	ltBO2.U_CLASS

				** DADOS DE REGISTO
				Replace cabdoc.DATAREGISTO	With ltBO.ousrdata
				Replace cabdoc.HORAREGISTO	With ltBO.ousrHORA
				Replace cabdoc.USERREGISTO	With ltBO.ousrinis
				Replace cabdoc.DATAALTERA 	With ltBO.usrdata
				Replace cabdoc.HORAALTERA	With ltBO.usrhora
				Replace cabdoc.USERALTERA	With ltBO.usrinis

				** ARMAZEM
				Replace cabdoc.ARMAZEM		With ltBO2.ARMAZEM
				Replace cabdoc.NRRECEITA    With ltBO2.NRRECEITA
				Replace cabdoc.pinacesso    With ltBO2.pinacesso
				Replace cabdoc.pinopcao    	With ltBO2.pinopcao

				** ENCOMENDA ENVIDADA?
				Replace cabdoc.LOGI1 		With  ltBO.LOGI1

				** dados fornecedor
				Replace cabdoc.MORADA 		With ltBO.MORADA
				Replace cabdoc.Local 		With ltBO.Local
				Replace cabdoc.CODPOST		With ltBO.CODPOST
				Replace cabdoc.NCONT 		With ltBO.NCONT

				** OBSERVA��ES
				If Alltrim(ltBO.NMDOS)!='Encomenda de Cliente'
					Replace cabdoc.OBS 			With ltBO.OBS
				Else
					Replace cabdoc.OBS 			With Alltrim(ltBO.OBS) + Chr(13) + 'Zona:' + Alltrim(ltBO2.CODPOST) + Chr(13) + 'Envio:' + Alltrim(ltBO2.modo_envio) + Chr(13) + 'Pagamento:' + Alltrim(ltBO2.pagamento) + Chr(13) + 'Status:' + Alltrim(ltBO2.Status)
				Endif

				**DATA VALIDADE
				Replace cabdoc.DATAFINAL 	With ltBO.DATAFINAL

				** Dados Transporte
				Replace cabdoc.XPDDATA 		With ltBO2.XPDDATA
				Replace cabdoc.XPDHORA 		With ltBO2.XPDHORA
				Replace cabdoc.XPDVIATURA 	With ltBO2.XPDVIATURA
				Replace cabdoc.XPDMORADA 	With ltBO2.MORADA
				Replace cabdoc.XPDCONTACTO	With ltBO2.contacto
				Replace cabdoc.XPDCODPOST	With ltBO2.CODPOST
				Replace cabdoc.XPDEMAIL		With ltBO2.EMAIL
				Replace cabdoc.XPDTELEFONE	With ltBO2.TELEFONE
				Replace cabdoc.ATDocCode	With ltBO2.ATDocCode
				Replace cabdoc.xpdLocalidade With ltBO2.xpdLocalidade
				Replace cabdoc.tabiva		With ltBO.tabiva
				Replace cabdoc.u_dataentr	With ltBO.u_dataentr
				Set Hours To 24
				Replace cabdoc.u_horaentr	With Ttoc(ltBO.u_dataentr,2)

				**Dados Centro Anal�tico
				Replace cabdoc.CCUSTO		With ltBO.CCUSTO
				Replace cabdoc.PLANO		With .F.
				Replace cabdoc.EXPORTADO	With ltBO.EXPORTADO
				Replace cabdoc.BLOQPAG		With .F.
				If uf_gerais_getParameter("ADM0000000299","BOOL")=.T.
					Replace cabdoc.arredondaPVP With .T.
				Else
					Replace cabdoc.arredondaPVP With .F.
				Endif

				Replace cabdoc.CODEXT With ltBO2.CODEXT

				** Total Valor Descontos Comerciais
				Replace cabdoc.EDESCC	With Round(ltBO.EDESCC,2)

				** verifica se tem anexos
				TEXT TO lcSQL NOSHOW TEXTMERGE
					select case when (select count(anexosstamp) as anexos from anexos (nolock) where tabela='documentos' and regstamp='<<Alltrim(tcStamp)>>')=0 then 0 else 1 end as anexos
				ENDTEXT

				If !uf_gerais_actGrelha("", "ucrsanexosconta", lcSQL)
					uf_perguntalt_chama("N�O FOI POSS�VEL DETERMINAR OS DADOS DE LINHAS DO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE!","OK","",16)
					Return .F.
				Endif
				If ucrsanexosconta.anexos>0
					Replace cabdoc.anexos With .T.
				Else
					Replace cabdoc.anexos With .F.
				Endif
				Replace cabdoc.entrega With ltBO2.modo_envio
				Replace cabdoc.pagamento With ltBO2.pagamento
				Replace cabdoc.momentopagam With ltBO2.momento_pagamento
				Replace cabdoc.estadoenc With ltBO2.Status

				fecha("ucrsanexosconta")

				** INFORMA��O LINHAS
				TEXT TO lcSQL NOSHOW TEXTMERGE
					exec up_Documentos_linhas 'BO','<<Alltrim(tcStamp)>>', <<mysite_nr>>
				ENDTEXT

				If !uf_gerais_actGrelha("DOCUMENTOS.PageFrame1.Page1.griddoc", "BI", lcSQL)
					uf_perguntalt_chama("N�O FOI POSS�VEL DETERMINAR OS DADOS DE LINHAS DO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE!","OK","",16)
					Return .F.
				Endif

				Select Bi
				Go Top
				Scan

					Select Bi

					Replace Bi.u_reserva With uf_gerais_getUmvalor("st","(case when st.qttcli = 0 then cast(0 as bit) else cast(1 as bit) end)","st.ref = '" + Bi.ref + "' and site_nr = " + ASTR(mySite_nr))

				Endscan

				Select Bi
				Go Top

				If Used("ucrsBiCompare")
					fecha("ucrsBiCompare")
				Endif

				Select * From Bi Into Cursor ucrsBiCompare Readwrite

				If Used("BI2")
					fecha("BI2")
				Endif

				lcSQL = ''
				If Empty(Alltrim(tcStamp))
					TEXT TO lcSQL NOSHOW TEXTMERGE
						SET FMTONLY on
						SELECT TOP 0 * FROM BI2 (nolock)
						SET FMTONLY off
					ENDTEXT
				Else
					TEXT TO lcSQL NOSHOW TEXTMERGE
						SELECT *
						FROM
							BI2 (nolock)
						WHERE
							BOSTAMP = '<<Alltrim(tcStamp)>>'
					ENDTEXT
				Endif
				If !uf_gerais_actGrelha("", "BI2", lcSQL)
					uf_perguntalt_chama("N�O FOI POSS�VEL DETERMINAR OS DADOS DE LINHAS DO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE!(2)","OK","",16)
					Return .F.
				Endif

				** Dados dos Totais
				If Empty(Alltrim(tcStamp))
					TEXT TO lcSQL NOSHOW TEXTMERGE
						SELECT TOP 0 * FROM bo_totais
					ENDTEXT
				Else
					TEXT TO lcSQL NOSHOW TEXTMERGE
						SELECT * FROM bo_totais WHERE STAMP = '<<Alltrim(tcStamp)>>'
					ENDTEXT
				Endif
				If !uf_gerais_actGrelha("", "ucrsTotaisDocumento", lcSQL)
					uf_perguntalt_chama("N�O FOI POSS�VEL DETERMINAR OS TOTAIS DO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE!","OK","",16)
					Return .F.
				Endif

			Endif

			** n� da fatura associada � encomenda para a impress�o da etiqueta
			If Alltrim(cabdoc.Doc)=='Encomenda de Cliente'
				Public NrFatEncomenda
				Store '' To NrFatEncomenda
				uf_gerais_actGrelha("", "ltFTENC", "select distinct left('FT'+' '+convert(varchar,ft.ndoc)+'/'+CONVERT(varchar,ft.fno),60) as fno from ft (nolock) inner join fi (nolock) on ft.ftstamp=fi.ftstamp where fi.bistamp in (select bistamp from bi (nolock) where bostamp= '" + Alltrim(tcStamp) + "')")
				Select ltFTENC
				Go Top
				Scan
					If Empty(NrFatEncomenda)
						NrFatEncomenda = Alltrim(ltFTENC.fno)
					Else
						NrFatEncomenda = NrFatEncomenda  + '-' +  Alltrim(ltFTENC.fno)
					Endif
				Endscan
				fecha("ltFTENC")
			Endif

		Endif
	Endif

	&& tabela FO
	If Upper(Alltrim(myTipoDoc)) == "FO"
		If uf_gerais_actGrelha("", "ltFO", "SELECT * FROM FO (nolock) WHERE FO.FOSTAMP = '" + Alltrim(tcStamp) + "'")

			** FO2
			If uf_gerais_actGrelha("", "ltFO2", "SELECT * FROM FO2 (nolock) WHERE FO2.FO2STAMP = '" + Alltrim(tcStamp) + "'")
				Select ltFO
				Select ltFO2

				Select cabdoc
				Go Bottom
				Append Blank

				Replace cabdoc.cabstamp			With ltFO.FOSTAMP
				Replace cabdoc.Doc 				With ltFO.docnome
				Replace cabdoc.NUMINTERNODOC 	With ltFO.doccode
				Replace cabdoc.NUMDOC			With Alltrim(ltFO.Adoc)
				Replace cabdoc.Nome				With ltFO.Nome
				Replace cabdoc.PAIS				With ltFO.PAIS
				Replace cabdoc.NO				With ltFO.NO
				Replace cabdoc.ESTAB			With ltFO.ESTAB
				Replace cabdoc.CONDPAG			With ltFO.TPDESC
				Replace cabdoc.DATAVENC			With ltFO.PDATA
				Replace cabdoc.MOEDA			With ltFO.MOEDA
				Replace cabdoc.DATADOC			With ltFO.Docdata
				Replace cabdoc.DATAINTERNA		With ltFO.Data
				Replace cabdoc.ESTADODOC		With ltFO.U_STATUS
				Replace cabdoc.DESCFIN			With ltFO.FIN
				Replace cabdoc.VALORDESCFIN		With ltFO.EFINV
				Replace cabdoc.DOCFL			With ltFO2.U_DOCFL
				Replace cabdoc.DOCFLNO			With ltFO2.U_DOCFLNO
				Replace cabdoc.CONTADORDOC		With ltFO2.U_DOCCONT
				Replace cabdoc.BASEINC			With ltFO.ETTILIQ
				Replace cabdoc.IVA				With ltFO.ETTIVA
				Replace cabdoc.Total			With Round(ltFO.etotal,2)
				Replace cabdoc.VALORIVA1		With ltFO.EIVAV1
				Replace cabdoc.VALORIVA2		With ltFO.EIVAV2
				Replace cabdoc.VALORIVA3		With ltFO.EIVAV3
				Replace cabdoc.VALORIVA4		With ltFO.EIVAV4
				Replace cabdoc.VALORIVA5		With ltFO.EIVAV5
				Replace cabdoc.VALORIVA6		With ltFO.EIVAV6
				Replace cabdoc.VALORIVA7		With ltFO.EIVAV7
				Replace cabdoc.VALORIVA8		With ltFO.EIVAV8
				Replace cabdoc.VALORIVA9		With ltFO.EIVAV9
				Replace cabdoc.ousrdata			With ltFO.ousrdata
				Replace cabdoc.ousrHORA			With ltFO.ousrHORA
				Replace cabdoc.imput_desp		With ltFO.imput_desp
				Replace cabdoc.SITE 			With ltFO.SITE



				Replace cabdoc.FOID				With ltFO.FOID
				Replace cabdoc.U_DOCCONT		With ltFO2.U_DOCCONT

				**DADOS DE REGISTO
				Replace cabdoc.DATAREGISTO		With ltFO.ousrdata
				Replace cabdoc.HORAREGISTO		With ltFO.ousrHORA
				Replace cabdoc.USERREGISTO		With ltFO.ousrinis

				Replace cabdoc.DATAALTERA		With ltFO.usrdata
				Replace cabdoc.HORAALTERA		With ltFO.usrhora
				Replace cabdoc.USERALTERA		With ltFO.usrinis

				Replace cabdoc.ARMAZEM			With ltFO2.U_ARMAZEM

				Replace cabdoc.MORADA			With ltFO.MORADA
				Replace cabdoc.Local			With ltFO.Local
				Replace cabdoc.CODPOST			With ltFO.CODPOST
				Replace cabdoc.NCONT			With ltFO.NCONT

				**Dados Centro Anal�tico
				Replace cabdoc.CCUSTO			With ltFO.CCUSTO

				Replace cabdoc.PLANO			With ltFO.PLANO
				Replace cabdoc.EXPORTADO		With ltFO.EXPORTADO

				Replace cabdoc.BLOQPAG			With ltFO.BLOQPAG

				** OBSERVA��ES
				Replace cabdoc.OBS 		With ltFO.Final
				Replace cabdoc.tabiva	With ltFO.tabiva

				** Arredondamento da Base de incidencia
				Replace cabdoc.EAIVAIN	With ltFO.EAIVAIN

				** Total Valor Descontos Comerciais
				Replace cabdoc.EDESCC	With Round(ltFO.EDESCC,2)
				**
				If uf_gerais_getParameter("ADM0000000299","BOOL")=.T.
					Replace cabdoc.arredondaPVP With .T.
				Else
					Replace cabdoc.arredondaPVP With .F.
				Endif

				Replace cabdoc.consignacao With ltFO.consignacao
				Replace cabdoc.CODEXT With ''

				Replace cabdoc.id_tesouraria_conta With Iif(ltFO.id_tesouraria_conta == 0, 98, ltFO.id_tesouraria_conta)

				** INFORMA��O LINHAS
				fecha("FN")
				lcSQL = ""
				If Empty(Alltrim(tcStamp))
					TEXT TO lcSQL NOSHOW TEXTMERGE
						exec up_Documentos_linhas 'FO','xxx',0
					ENDTEXT
				Else
					TEXT TO lcSQL NOSHOW TEXTMERGE
						exec up_Documentos_linhas 'FO','<<Alltrim(tcStamp)>>', <<mysite_nr>>
					ENDTEXT
				Endif

				If !uf_gerais_actGrelha("DOCUMENTOS.PageFrame1.Page1.griddoc", "FN", lcSQL)
					uf_perguntalt_chama("N�O FOI POSS�VEL DETERMINAR OS DADOS DE LINHAS DO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE!","OK","",16)
					Return .F.
				Endif


				&& preencher a validade do PVP da ficha nas faturas de armaz�m - igual � valida��o do atendimento
				Select FN
				Go Top
				Scan

					Select FN

					Replace FN.u_reserva With uf_gerais_getUmvalor("st","(case when st.qttcli = 0 then cast(0 as bit) else cast(1 as bit) end)","st.ref = '" + FN.ref + "' and site_nr = " + ASTR(mySite_nr))

					uf_documentos_validaPrecoAtribuiCor()

				Endscan
				Select FN
				Go Top

				If Used("ucrsFnCompare")
					fecha("ucrsFnCompare")
				Endif
				Select * From FN Into Cursor ucrsFnCompare Readwrite

				** selecionar descontos das linhas
				If Alltrim(cabdoc.Doc) == "V/Factura Med."
					If !uf_gerais_actGrelha("", "ucrsfndesclin", "select * from fndesclin where fnstamp in (select fnstamp from fn where fostamp='" + Alltrim(tcStamp) + "')")
						uf_perguntalt_chama("N�O FOI POSS�VEL DETERMINAR OS DADOS DE LINHAS DE DESCONTOS DO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE!","OK","",16)
						Return .F.
					Endif
				Endif

				** Dados dos Totais
				If Empty(Alltrim(tcStamp))

					TEXT TO lcSQL NOSHOW TEXTMERGE
							SELECT TOP 0 *, cast(0 as numeric(2,0)) as tabiva FROM fo_totais(NOLOCK)
					ENDTEXT

				Else

					TEXT TO lcSQL NOSHOW TEXTMERGE
							SELECT *, cast(0 as numeric(2,0)) as tabiva FROM fo_totais(NOLOCK) WHERE STAMP = '<<Alltrim(tcStamp)>>'
					ENDTEXT

				Endif

				If !uf_gerais_actGrelha("", "ucrsTotaisDocumento", lcSQL)
					uf_perguntalt_chama("N�O FOI POSS�VEL DETERMINAR OS TOTAIS DO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE!","OK","",16)
					Return .F.
				Endif

			Endif
		Endif
	Endif

	fecha("ucrsTS")
	fecha("ucrsCM1")

	If !Empty(cabdoc.NUMINTERNODOC)

		If myTipoDoc == "BO"
			uf_gerais_actGrelha("","ucrsTS", "exec up_documentos_getTS " + ASTR(cabdoc.NUMINTERNODOC))
		Endif

		If myTipoDoc == "FO"
			uf_gerais_actGrelha("","ucrsCM1", "exec up_documentos_getCM " + ASTR(cabdoc.NUMINTERNODOC))
		Endif

	Endif

Endfunc


**
Function uf_documentos_ControlaDetalheST

	** Define o cursor que alimenta DetalheST
	With DOCUMENTOS.PageFrame1.Page1.detalhest1

		.cursoractivo = Iif(Upper(Alltrim(myTipoDoc)) == "FO","FN","BI")

		&& Separadores N�o visiveis
		.lbl1.Visible = .F. && Mapa de Pre�os
		.lbl2.Visible = .F. && Hist. Pre�os
		.lbl3.Visible = .F. && Pre�os Compra
		.lbl4.Visible = .F. && B�nus
		.lbl5.Visible = .F. && Esgotados
		.lbl6.Visible = .F. && 5 Mais Baratos
		.lbl7.Visible = .F. && Comp.Vendas
		.lbl8.Visible = .F. && G.H.
		.lbl9.Visible = .F. && Resumo
		.lbl10.Visible = .F. && Totais Doc.
		.lbl11.Visible = .F.  && Outros Dados
		.lbl3.Visible = .F. && Pre�os Compra
		.lbl7.Visible = .F. && Comp.Vendas
		.lbl12.Visible = .F. && Confer�ncia

		&& Seperadores visiveis
		.lbl1.Visible = .T. && Mapa de Pre�os
		.lbl2.Visible = .T. && Hist. Pre�os
		.lbl10.Visible = .T. && Totais Doc.
		.lbl11.Visible = .T.  && Outros Dados
		.lbl9.Visible = .T. && Resumo
		.lbl3.Visible = .T. && Pre�os Compra
		.lbl7.Visible = .T. && Comp.Vendas
		.lbl14.Visible = Iif(Upper(Alltrim(myTipoDoc)) == "BO", .T., .F.)
		.lbl15.Visible = .T. && Origens

		**N�O ESQUECER VERIFICAR PARAMETROS
		If Used("cabdoc")
			If Alltrim(Upper(myTipoDoc)) = 'FO' And Inlist(cabdoc.NUMINTERNODOC, 55, 58, 101)
				.lbl13.Visible = .T. && Confer�ncia	Robot
			Else
				.lbl13.Visible = .F. && Confer�ncia	Robot
			Endif
		Else
			.lbl13.Visible = .F. && Confer�ncia	Robot
		Endif

		&& Trata posicao
		If Used("cabDoc")
			If cabdoc.NUMINTERNODOC == 55 Or cabdoc.NUMINTERNODOC == 101 Or cabdoc.NUMINTERNODOC == 102

				.lbl12.Visible = .T. && Confer�ncia

				&& Trata posicao
				.lbl10.Left = 15 && Totais Doc.

				.lbl12.Left = .lbl10.Left + .lbl10.Width + 15 && Confer�ncia
				.lbl1.Left = .lbl12.Left + .lbl12.Width + 15 && Mapa de Pre�os
				.lbl2.Left = .lbl1.Left + .lbl1.Width + 15 && Hist. Pre�os
				.lbl3.Left = .lbl2.Left + .lbl2.Width + 15 && Pre�os Compra
				.lbl7.Left = .lbl3.Left + .lbl3.Width + 15 && Comp.Vendas
				.lbl9.Left = .lbl7.Left + .lbl7.Width + 15 && Resumo
				.lbl13.Left = .lbl9.Left + .lbl9.Width + 15 && Conf. Rob�tica
				If .lbl13.Visible
					.lbl14.Left = .lbl13.Left + .lbl13.Width + 15 && Transporte
				Else
					.lbl14.Left = .lbl9.Left + .lbl9.Width + 15 && Transporte
				Endif

				If .lbl14.Visible
					.lbl11.Left = .lbl14.Left + .lbl14.Width + 15 && Outros Dados
				Else
					If .lbl13.Visible
						.lbl11.Left = .lbl13.Left + .lbl13.Width + 15 && Outros Dados
					Else
						.lbl11.Left = .lbl9.Left + .lbl9.Width + 15 && Outros Dados
					Endif
				Endif

				If .lbl11.Visible
					.lbl15.Left = .lbl11.Left + .lbl11.Width + 15 && Origens
				Else
					If lbl14.Visible
						.lbl15.Left = .lbl14.Left + .lbl14.Width + 15 && Origens
					Else
						.lbl15.Left = .lbl9.Left + .lbl9.Width + 15 && Origens
					Endif
				Endif



			Else

				.lbl12.Visible = .F. && Confer�ncia

				.lbl10.Left = 15 && Totais Doc.

				.lbl1.Left = .lbl10.Left + .lbl10.Width + 15 && Mapa de Pre�os
				.lbl2.Left = .lbl1.Left + .lbl1.Width + 15 && Hist. Pre�os
				.lbl3.Left = .lbl2.Left + .lbl2.Width + 15 && Pre�os Compra
				.lbl7.Left = .lbl3.Left + .lbl3.Width + 15 && Comp.Vendas
				.lbl9.Left = .lbl7.Left + .lbl7.Width + 15 && Resumo
				.lbl13.Left = .lbl9.Left + .lbl9.Width + 15 && Resumo
				If .lbl13.Visible
					.lbl14.Left = .lbl13.Left + .lbl13.Width + 15 && Transporte
				Else
					.lbl14.Left = .lbl9.Left + .lbl9.Width + 15 && Transporte
				Endif

				If .lbl14.Visible
					.lbl11.Left = .lbl14.Left + .lbl14.Width + 15 && Outros Dados
				Else
					If .lbl13.Visible
						.lbl11.Left = .lbl13.Left + .lbl13.Width + 15 && Outros Dados
					Else
						.lbl11.Left = .lbl9.Left + .lbl9.Width + 15 && Outros Dados
					Endif
				Endif

				If .lbl11.Visible
					.lbl15.Left = .lbl11.Left + .lbl11.Width + 15 && Origens
				Else
					If lbl14.Visible
						.lbl15.Left = .lbl14.Left + .lbl14.Width + 15 && Origens
					Else
						.lbl15.Left = .lbl9.Left + .lbl9.Width + 15 && Origens
					Endif
				Endif

				&&posiciona-se na 1a linha
				.lbl10.Click
			Endif

		Endif


		Do Case
			Case myDocAlteracao == .F. And myDocIntroducao == .F.
				&&posiciona-se na 1a linha
				.lbl10.Click

			Case myDocAlteracao == .T. Or myDocIntroducao == .T.
				&&
		Endcase

		.lbl2.Top = .lbl1.Top
		.lbl3.Top = .lbl1.Top
		.lbl4.Top = .lbl1.Top
		.lbl5.Top = .lbl1.Top
		.lbl6.Top = .lbl1.Top
		.lbl7.Top = .lbl1.Top
		.lbl8.Top = .lbl1.Top
		.lbl9.Top = .lbl1.Top
		.lbl10.Top = .lbl1.Top
		.lbl13.Top = .lbl1.Top
		.lbl14.Top = .lbl1.Top
		.lbl15.Top = .lbl1.Top
	Endwith

	If Used("cabDoc")
		**IF ALLTRIM(cabdoc.doc)='Encomenda a Fornecedor'
		*documentos.Pageframe1.Page1.Detalhest1.PAGEFRAME1.PAGE9.Btnimg1.visible=.t.
		DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.PAGE9.Btnimg6.Visible=.F.
		DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.PAGE9.Btnimg5.Visible=.F.
		**ELSE
		**	documentos.Pageframe1.Page1.Detalhest1.PAGEFRAME1.PAGE9.Btnimg1.visible=.f.
		**	documentos.Pageframe1.Page1.Detalhest1.PAGEFRAME1.PAGE9.Btnimg6.visible=.t.
		**	documentos.Pageframe1.Page1.Detalhest1.PAGEFRAME1.PAGE9.Btnimg5.visible=.t.
		**ENDIF
	Endif

Endfunc

**
Function uf_documentos_validaPermissoesAcesso
	Lparameters tcStamp

	**Valida Permiss�o de acesso ao tipo de documento passado por parametro (uf_validaPermUser)
	TEXT TO lcSQL NOSHOW TEXTMERGE
		exec up_documentos_dadosDoc '<<ALLTRIM(tcStamp)>>'
	ENDTEXT
	If !uf_gerais_actGrelha("", "uCrsNomeDoc", lcSQL)
		Return .F.
	Endif

	If Reccount("uCrsNomeDoc")>0
		If !(uf_gerais_validaPermUser(ch_userno, ch_grupo, Alltrim(uCrsNomeDoc.nomedoc) + ' - Visualizar'))
			If Type("DOCUMENTOS") != "U"
				uf_perguntalt_chama("O SEU PERFIL N�O PERMITE VISUALIZAR ESTE TIPO DE DOCUMENTO.", "OK","",48)
			Endif
			fecha("uCrsNomeDoc")
			Return .F.
		Endif

		**Atribi Tipo de documento � vari�vel p�blica
		Select uCrsNomeDoc
		myTipoDoc = uCrsNomeDoc.tipoDoc

		fecha("uCrsNomeDoc")
	Endif
	*********************************************************
Endfunc


**

Function uf_documentos_expandeGridLinhas

	*PUBLIC myResizeDocumentos &&jg

	If myResizeDocumentos == .T. &&jg

		*IF (DOCUMENTOS.height-DOCUMENTOS.pageframe1.page1.DetalheSt1.top) == 102
		DOCUMENTOS.PageFrame1.Page1.gridDoc.Height = DOCUMENTOS.PageFrame1.Page1.gridDoc.Height - 225
		DOCUMENTOS.PageFrame1.Page1.ContainerLinhas.check1.config(myPath + "\imagens\icons\pagedown_B.png", "")

		*caixinha com o total desce um pouco, fica ao nivel do menu lateral esq.
		DOCUMENTOS.PageFrame1.Page1.ContainerLinhas.Top = DOCUMENTOS.PageFrame1.Page1.gridDoc.Height + 30

		myResizeDocumentos = .F.
	Else
		*IF Documentos.pageframe1.height > Documentos.pageframe1.page1.GridDoc.height + 225 &&jg
		DOCUMENTOS.PageFrame1.Page1.gridDoc.Height = DOCUMENTOS.PageFrame1.Page1.gridDoc.Height + 225
		DOCUMENTOS.PageFrame1.Page1.ContainerLinhas.check1.config(myPath + "\imagens\icons\pageup_B.png", "")

		*caixinha com o total encosta quase � Grid de cima
		DOCUMENTOS.PageFrame1.Page1.ContainerLinhas.Top = DOCUMENTOS.PageFrame1.Page1.gridDoc.Height + 5

		myResizeDocumentos = .T.
		*endif	&&jg
	Endif

	*Documentos.pageframe1.page1.containerLinhas.top = Documentos.pageframe1.page1.GridDoc.height + 5
	DOCUMENTOS.PageFrame1.Page1.detalhest1.Top = DOCUMENTOS.PageFrame1.Page1.gridDoc.Height + 30

	**** colocacao dos botoes acima ou abaixo, mas sempre a 5 pontos da Grid
	**** 2020-08-17, jg, TD-50549
	DOCUMENTOS.PageFrame1.Page1.btnAddLinha.Top = DOCUMENTOS.PageFrame1.Page1.gridDoc.Height + 5
	DOCUMENTOS.PageFrame1.Page1.btnDelLinha.Top = DOCUMENTOS.PageFrame1.Page1.gridDoc.Height + 5
	DOCUMENTOS.PageFrame1.Page1.btn.Top = DOCUMENTOS.PageFrame1.Page1.gridDoc.Height + 5
	DOCUMENTOS.PageFrame1.Page1.Shape1.Top = DOCUMENTOS.PageFrame1.Page1.gridDoc.Height + 5
	DOCUMENTOS.PageFrame1.Page1.BtnEsgot.Top = DOCUMENTOS.PageFrame1.Page1.gridDoc.Height + 5
	DOCUMENTOS.PageFrame1.Page1.ShapeEsgot.Top = DOCUMENTOS.PageFrame1.Page1.gridDoc.Height + 5
	****


Endfunc




** Usado no detalhe (Op��es)
Function uf_documentos_mostraDetalhe
	If (DOCUMENTOS.Height-DOCUMENTOS.PageFrame1.Page1.detalhest1.Top) == 104
		DOCUMENTOS.PageFrame1.Page1.gridDoc.Height = DOCUMENTOS.PageFrame1.Page1.gridDoc.Height - 225
		DOCUMENTOS.PageFrame1.Page1.ContainerLinhas.check1.config(myPath + "\imagens\icons\pagedown_B.png", "")
		DOCUMENTOS.PageFrame1.Page1.ContainerLinhas.Top = DOCUMENTOS.PageFrame1.Page1.gridDoc.Height + 5
		DOCUMENTOS.PageFrame1.Page1.detalhest1.Top = DOCUMENTOS.PageFrame1.Page1.ContainerLinhas.Top
		myResizeDocumentos = .F.
	Endif
Endfunc


**
Function uf_documentosAbrePesquisa
	Public myDocAlteracao, myDocIntroducao

	If myDocAlteracao == .T. Or myDocIntroducao == .T.
		If DOCUMENTOS.Menu1.pesquisar.Visible == .T.
			uf_perguntalt_chama("O DOCUMENTO EST� EM MODO DE INTRODU��O/EDI��O. DEVE CANCELAR OU GRAVAR PARA PROCEDER COM ESTA AC��O.","OK","",64)
		Endif

		Return .F.
	Endif

	If Vartype(lcorigempesq)!='U'
		Do Case
			Case Alltrim(lcorigempesq) == 'CLIENTES'
				uf_pesqDocumentosclientes_Chama()
			Case Alltrim(lcorigempesq) == 'FORNECEDORES'
				uf_pesqDocumentos_Chama()
			Otherwise
				Local tabdocpesq
				tabdocpesq = ''
				Select ucrsComboDoc
				Go Top
				Scan
					If Alltrim(ucrsComboDoc.nomedoc) == Alltrim(cabdoc.Doc)
						tabdocpesq = Alltrim(ucrsComboDoc.bdempresas)
					Endif
				Endscan
				If Alltrim(tabdocpesq)='CL'
					uf_pesqDocumentosclientes_Chama()
				Else
					uf_pesqDocumentos_Chama()
				Endif
		Endcase
	Else
		uf_pesqDocumentos_Chama()
	Endif

Endfunc


** Controla Workflow do Documento Escolhido
Function uf_documentos_controlaWkLinhas

	If Empty(myTipoDoc)
		Return .F.
	Endif

	Local ti, i, lcValorQuabraOld, lcValorTabivaAntes, lcValorCCAntes

	Store '' To lcValorQuabraOld, lcValorCCAntes
	Store 0 To lcValorTabivaAntes

	Select cabdoc
	Go Top
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_documentos_getWklCols <<ASTR(cabDoc.numinternodoc)>>, '<<ALLTRIM(myTipoDoc)>>', '<<ALLTRIM(cabDoc.doc)>>'
	ENDTEXT

	**Apaga colunas documento anterior se existirem
	DOCUMENTOS.PageFrame1.Page1.gridDoc.ColumnCount = 0

	If !uf_gerais_actGrelha("", "ucrsWkLinhas", lcSQL)
		uf_perguntalt_chama("N�O FOI POSSIVEL IDENTIFICAR A CONFIGURA��O WORKFLOW ASSOCIADO AO DOCUMENTO.","OK","",16)
		Return .F.
	Endif

	**
	i=1

	* VALIDA AS COLUNAS A MOSTRAR
	Select ucrsWkLinhas
	Go Top
	Scan

		If (Left(Upper(Alltrim(ucrsWkLinhas.wkfield)),2) == "BI" Or  Left(Upper(Alltrim(ucrsWkLinhas.wkfield)),2) == "FN")
			&&;AND UPPER(Alltrim(ucrsWkLinhas.wkfield)) != 'FN.FNCCUSTO' AND UPPER(Alltrim(ucrsWkLinhas.wkfield)) != 'BI.CCUSTO'
			&& Os campos de custo, s�o combos tratados posteriormente

			If Type(ucrsWkLinhas.wkfield) == 'C' Or  Type(ucrsWkLinhas.wkfield) == 'N' Or  Type(ucrsWkLinhas.wkfield) == 'L'

				With DOCUMENTOS.PageFrame1.Page1.gridDoc

					.AddColumn
					.RowHeight = uf_gerais_getRowHeight()
					.Columns(i).ControlSource = Alltrim(ucrsWkLinhas.wkfield)
					.Columns(i).header1.FontName = "Verdana"
					.Columns(i).FontName = "Verdana"
					.Columns(i).FontSize = 10
					.Columns(i).ReadOnly = ucrsWkLinhas.WKREADONLY
					.Columns(i).text1.ReadOnly = ucrsWkLinhas.WKREADONLY
					.Columns(i).ColumnOrder= ucrsWkLinhas.Ordem + 1

					Do Case
						Case Upper(Alltrim(ucrsWkLinhas.wkfield)) == "BI.REF" Or Upper(Alltrim(ucrsWkLinhas.wkfield)) == "FN.REF"
							.Columns(i).DynamicBackColor = "uf_documentos_dynamicBackColorGridAlerta('REF')"
							.Columns(i).DynamicForeColor = "uf_documentos_dynamicForeColorGridAlerta('REF')"
						Case Upper(Alltrim(ucrsWkLinhas.wkfield)) == "FN.QTT"
							.Columns(i).DynamicBackColor = "uf_documentos_dynamicBackColorGridAlerta('QTT')"
							.Columns(i).DynamicForeColor = "uf_documentos_dynamicForeColorGridAlerta('QTT')"
						Case Upper(Alltrim(ucrsWkLinhas.wkfield)) == "FN.ETILIQUIDO"
							.Columns(i).DynamicBackColor = "uf_documentos_dynamicBackColorGridAlerta('TOTAL')"
							.Columns(i).DynamicForeColor = "uf_documentos_dynamicForeColorGridAlerta('TOTAL')"
						Case Upper(Alltrim(ucrsWkLinhas.wkfield)) == "FN.QTREC"
							.Columns(i).DynamicBackColor = "uf_documentos_dynamicBackColorGridAlerta('QTREC')"
							.Columns(i).DynamicForeColor = "uf_documentos_dynamicForeColorGridAlerta('QTREC')"
						Case Upper(Alltrim(ucrsWkLinhas.wkfield)) == "FN.U_PVP"
							.Columns(i).DynamicBackColor = "uf_documentos_dynamicBackColorGridAlerta('U_PVP')"
							.Columns(i).DynamicForeColor = "uf_documentos_dynamicForeColorGridAlerta('U_PVP')"
						Case Upper(Alltrim(ucrsWkLinhas.wkfield)) == "FN.U_DTVAL"
							.Columns(i).DynamicBackColor = "uf_documentos_dynamicBackColorGridAlerta('U_DTVAL')"
							.Columns(i).DynamicForeColor = "uf_documentos_dynamicForeColorGridAlerta('U_DTVAL')"
						Case Upper(Alltrim(ucrsWkLinhas.wkfield)) == "FN.DESCONTO"
							.Columns(i).DynamicBackColor = "uf_documentos_dynamicBackColorGridAlerta('DESCONTO')"
							.Columns(i).DynamicForeColor = "uf_documentos_dynamicForeColorGridAlerta('DESCONTO')"
						Case Upper(Alltrim(ucrsWkLinhas.wkfield)) == "FN.U_UPC"
							.Columns(i).DynamicBackColor = "uf_documentos_dynamicBackColorGridAlerta('U_UPC')"
							.Columns(i).DynamicForeColor = "uf_documentos_dynamicForeColorGridAlerta('U_UPC')"


						Otherwise
							.Columns(i).DynamicBackColor = "uf_documentos_dynamicBackColorGridDefault()"
					Endcase

					Do Case
						Case myTipoDoc == "BO"
							.Columns(i).DynamicFontBold = "BI.PESQUISA"
						Case myTipoDoc == "FO"
							.Columns(i).DynamicFontBold = "FN.PESQUISA"
						Otherwise
					Endcase

					If ucrsWkLinhas.largura != 0
						.Columns(i).Width = ucrsWkLinhas.largura
					Endif

					If 	!Empty(ucrsWkLinhas.mascara)
						.Columns(i).InputMask=Alltrim(ucrsWkLinhas.mascara)
						.Columns(i).text1.InputMask=Alltrim(ucrsWkLinhas.mascara)
					Endif

					.Columns(i).header1.Caption = Alltrim(ucrsWkLinhas.wktitle)
					.Columns(i).header1.Alignment = 2
					.Columns(i).text1.BorderStyle =  0
					.Columns(i).text1.ForeColor = Rgb(0,0,0)
					.Columns(i).text1.SelectedForeColor = Rgb(0,0,0)
					.Columns(i).text1.ControlSource = Alltrim(ucrsWkLinhas.wkfield)
					.Columns(i).Format="RTK"
					.Columns(i).text1.Format="RTK"
					.Columns(i).text1.HideSelection=.T.

					If Inlist(Alltrim(ucrsWkLinhas.wkfield), 'fn.desconto', 'fn.desc2', 'fn.desc3', 'fn.desc4', 'bi.desconto', 'bi.desc2', 'bi.desc3', 'bi.desc4')

						.Columns(i).text1.InputMask = '999.99'

					Endif

					If Type(ucrsWkLinhas.wkfield) == 'L'
						.Columns(i).RemoveObject("Text1")
						.Columns(i).AddObject("Check1","Checkbox")
						.Columns(i).check1.Caption = ""
						.Columns(i).Alignment = 2
						.Columns(i).check1.Alignment = 2
						.Columns(i).Sparse = .F.
						.Columns(i).Visible = .T.
						.Columns(i).check1.SpecialEffect = 1
						.Columns(i).check1.Themes = .F.
						.Columns(i).check1.Style = 1
						.Columns(i).check1.Picture = myPath + "\imagens\icons\unchecked_b_24.bmp"
						.Columns(i).check1.DownPicture = myPath + "\imagens\icons\checked_b_24.bmp"
					Endif

					&&ocultar coluna lote
					If Upper(Alltrim(ucrsWkLinhas.wkfield)) == "BI.LOTE" Or Upper(Alltrim(ucrsWkLinhas.wkfield)) == "FN.LOTE"
						If !uf_gerais_getParameter("ADM0000000211","BOOL")
							.Columns(i).Visible = .F.
						Endif
					Endif
				Endwith
			Else
				uf_perguntalt_chama("N�O FOI POSSIVEL APRESENTAR O CAMPO: " + Alltrim(ucrsWkLinhas.wkfield) + ".","OK","",48)
			Endif
		Else

			&&Botao Origens
			**			If Upper(Alltrim(ucrsWkLinhas.wkfield)) == "WKORIGENS"
			**
			**				WITH DOCUMENTOS.PageFrame1.Page1.griddoc
			**					.AddColumn
			**					If ucrsWkLinhas.largura != 0
			**						.Columns(i).width = ucrsWkLinhas.largura
			**					ENDIF
			**					.columns(i).header1.caption = Alltrim(ucrsWkLinhas.wktitle)
			**					.columns(i).header1.Alignment = 2
			**					.Columns(i).AddObject('ORIGENS','CommandButtonOrigens')
			**					.Columns(i).ORIGENS.visible = .t.
			**					.Columns(i).Sparse = .f.
			**					.Columns(i).bound= .f.
			**					.Columns(i).ORIGENS.enabled= .t.
			**					.Columns(i).CurrentControl = 'ORIGENS'
			**					.Columns(i).ORIGENS.SpecialEffect = 1
			**					.Columns(i).ORIGENS.Picture = myPath + "\imagens\icons\left_b_24.bmp"
			**					.Columns(i).ORIGENS.PicturePosition = 14
			**					* DEFINE ORDEM
			**					.Columns(i).columnorder=ucrsWkLinhas.Ordem
			**				ENDWITH
			**			ENDIF

			&&Botao Destinos
			If Upper(Alltrim(ucrsWkLinhas.wkfield)) == "WKDESTINOS"

				With DOCUMENTOS.PageFrame1.Page1.gridDoc
					.AddColumn
					If ucrsWkLinhas.largura != 0
						.Columns(i).Width = ucrsWkLinhas.largura
					Endif
					.Columns(i).header1.Caption = Alltrim(ucrsWkLinhas.wktitle)
					.Columns(i).header1.Alignment = 2
					.Columns(i).AddObject('DESTINOS','CommandButtonDestinos')
					.Columns(i).DESTINOS.Visible = .T.
					.Columns(i).Sparse = .F.
					.Columns(i).Bound= .F.
					.Columns(i).DESTINOS.Enabled= .T.
					.Columns(i).CurrentControl = 'DESTINOS'
					.Columns(i).DESTINOS.SpecialEffect = 1
					.Columns(i).DESTINOS.Picture = myPath + "\imagens\icons\right_b_24.bmp"
					.Columns(i).DESTINOS.PicturePosition = 14

					* DEFINE ORDEM
					.Columns(i).ColumnOrder=ucrsWkLinhas.Ordem
				Endwith
			Endif
		Endif
		i=i+1
	Endscan

	uf_documentos_ControlaEventosLinhas()
	DOCUMENTOS.PageFrame1.Page1.gridDoc.Refresh
	DOCUMENTOS.Refresh
Endfunc


**
Function uf_documentosControlaObj

	** Em modo de Leitura
	If myDocAlteracao == .F. And myDocIntroducao == .F.


		With DOCUMENTOS.ContainerCab
			.NUMDOC.ReadOnly = .T.
			.Data.Enabled = .F.
		Endwith

		With DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.page10.ContainerTotais
			.DESCFIN.ReadOnly = .T.
			.VALORDESCFIN.ReadOnly = .T.
			.DESCFIN.Enabled = .F.
			.VALORDESCFIN.Enabled = .F.
			.CONDPAG.Enabled = .F.
			.DATAVENC.Enabled = .F.
			.txtLocalTesouraria.Enabled = .F.
			.imput_desp.ReadOnly = .T.
			.MOEDA.DisabledBackColor = Rgb(255,255,255)

			.apa1.ReadOnly = .T.
			.apa2.ReadOnly = .T.
			.apa3.ReadOnly = .T.
			.apa4.ReadOnly = .T.
			.apat.ReadOnly = .T.

			.txtentrega.Enabled = .F.
			.txtpagamento.Enabled = .F.
			.txtmomentopagam.Enabled = .F.
			.txtestadoenc.Enabled = .F.
			.txtentrega.ReadOnly = .T.
			.txtpagamento.ReadOnly = .T.
			.txtmomentopagam.ReadOnly = .T.
			.txtestadoenc.ReadOnly = .T.
		Endwith

		With DOCUMENTOS.ContainerCab
			.documento.DisabledBackColor = Rgb(255,255,255)
			.NUMDOC.DisabledBackColor = Rgb(255,255,255)
			.Nome.DisabledBackColor = Rgb(255,255,255)
			.Data.DisabledBackColor = Rgb(255,255,255)
			.ESTADO.DisabledBackColor = Iif(Alltrim(DOCUMENTOS.ContainerCab.ESTADO.Value) == "A",Rgb[232,76,61],Rgb(255,255,255))
			.ESTADO.ForeColor = Iif(Alltrim(DOCUMENTOS.ContainerCab.ESTADO.Value) == "A",Rgb(255,255,255),Rgb(0,0,0))
			.ESTADO.FontBold = Iif(Alltrim(DOCUMENTOS.ContainerCab.ESTADO.Value) == "A",.T.,.F.)
		Endwith

		**Outros Dados
		With DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.page11.ContainerOutrosDados

			.DOCFL.Enabled = .F.
			.DOCFLNO.ReadOnly = .T.
			.DOCFL.Enabled = .T.
			.DOCFL.DisabledBackColor = Rgb(255,255,255)
			.DOCFLNO.Enabled = .F.
			.DATAINTERNA.Enabled = .F.
			.DATAENTREGA.Enabled = .F.
			.DATAFINAL.Enabled = .F.
			.ARMAZEM.Enabled = .F.

			.contabilizado.Enable(.F.)
			.chkBloqPag.Enable(.F.)
			.consignacao.Enable(.F.)
			.editobs.ReadOnly = .T.
			.NRVIAS.ReadOnly = .T.

		Endwith

		With DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.page14

			.XPDDATA.ReadOnly = .T.
			.XPDHORA.ReadOnly = .T.
			.XPDVIATURA.ReadOnly = .T.
			.XPDCONTACTO.ReadOnly = .T.
			.XPDTELEFONE.ReadOnly = .T.
			.XPDMORADA.ReadOnly = .T.
			.XPDCODPOST.ReadOnly = .T.
			.XPDEMAIL.ReadOnly = .T.
			.xpdLocalidade.ReadOnly = .T.

		Endwith

		With DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.Page12.PageFrame1.Page1.Container1
			.txtQtRec.ReadOnly = .T.
			.txtQtFac.ReadOnly = .T.
			.txtQtBonus.ReadOnly = .T.
			.txtDesc1.ReadOnly = .T.
			.txtDesc2.ReadOnly = .T.
			.txtTotal.ReadOnly = .T.
			.txtPVP.ReadOnly = .T.
			.txtValidade.ReadOnly = .T.
			.txtLote.ReadOnly = .T.
			.txtLoteConf.ReadOnly = .T.
			.txtLoteConf.Visible = .F.
		Endwith

		With DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.Page12.PageFrame1.Page2.Container2
			.txtQtRec.ReadOnly = .T.
			.txtQtFac.ReadOnly = .T.
			.txtQtBonus.ReadOnly = .T.
			.txtDesc1.ReadOnly = .T.
			.txtDesc2.ReadOnly = .T.
			.txtTotal.ReadOnly = .T.
			.txtPVP.ReadOnly = .T.
			.txtMC.ReadOnly = .T.
			.txtValidade.ReadOnly = .T.
			.txtLote.ReadOnly = .T.
			.txtLoteConf.ReadOnly = .T.
			.txtLoteConf.Visible = .F.
		Endwith

		********************************************

		**Grid; Controla Combos
		For i=1 To DOCUMENTOS.PageFrame1.Page1.gridDoc.ColumnCount

			DOCUMENTOS.PageFrame1.Page1.gridDoc.Columns(i).ReadOnly = .T.

			If Upper(DOCUMENTOS.PageFrame1.Page1.gridDoc.Columns(i).ControlSource)=="BI.CCUSTO";
					OR Upper(DOCUMENTOS.PageFrame1.Page1.gridDoc.Columns(i).ControlSource)=="FN.FNCCUSTO";
					OR Upper(DOCUMENTOS.PageFrame1.Page1.gridDoc.Columns(i).ControlSource)=="BI.TABIVA";
					OR Upper(DOCUMENTOS.PageFrame1.Page1.gridDoc.Columns(i).ControlSource)=="FN.TABIVA"
				DOCUMENTOS.PageFrame1.Page1.gridDoc.Columns(i).Enabled = .F.
			Endif

		Endfor



		uf_documentos_alternaMenu()

	Else &&Modo de Edi��o

		*documentos.containercab.documento.enabled = .t.

		With DOCUMENTOS.ContainerCab
			Select cabdoc
			&&V/Factura; V/Guia, V/Nt. Cr, V/Nt. Deb, V/Vd. Din
			If cabdoc.NUMINTERNODOC != 55 And cabdoc.NUMINTERNODOC != 101 And cabdoc.NUMINTERNODOC != 56  And cabdoc.NUMINTERNODOC != 3 And cabdoc.NUMINTERNODOC != 100 And cabdoc.NUMINTERNODOC != 58
				.NUMDOC.ReadOnly = .T.
			Else
				.NUMDOC.ReadOnly = .F.
			Endif
			*.estado.readonly = .t.
			*.nome.readonly = .f.
			.Data.Enabled = .T.

		Endwith

		With DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.page10.ContainerTotais
			.DESCFIN.ReadOnly = .F.
			.VALORDESCFIN.ReadOnly = .F.
			.DESCFIN.Enabled = .T.
			.VALORDESCFIN.Enabled = .T.
			.CONDPAG.Enabled = .T.
			.txtLocalTesouraria.Enabled = .T.
			.imput_desp.ReadOnly = .F.
			.DATAVENC.Enabled = .T.
			.DATAVENC.DisabledBackColor = Rgb(255,255,255)
			.MOEDA.DisabledBackColor = Rgb(191,223,223)

			If !Empty(.apa1.Value)
				.apa1.ReadOnly = .F.
			Endif

			If !Empty(.apa2.Value)
				.apa2.ReadOnly = .F.
			Endif

			If !Empty(.apa3.Value)
				.apa3.ReadOnly = .F.
			Endif

			If !Empty(.apa4.Value)
				.apa4.ReadOnly = .F.
			Endif

			.txtentrega.Enabled = .T.
			.txtpagamento.Enabled = .T.
			.txtmomentopagam.Enabled = .T.
			.txtestadoenc.Enabled = .T.
			.txtentrega.ReadOnly = .F.
			.txtpagamento.ReadOnly = .F.
			.txtmomentopagam.ReadOnly = .F.
			.txtestadoenc.ReadOnly = .F.

		Endwith

		With DOCUMENTOS.ContainerCab

			.documento.DisabledBackColor = Rgb(191,223,223)
			.NUMDOC.DisabledBackColor = Rgb(191,223,223)
			.Nome.DisabledBackColor = Rgb(191,223,223)
			.Data.DisabledBackColor = Rgb(191,223,223)
			.ESTADO.DisabledBackColor = Iif(Alltrim(DOCUMENTOS.ContainerCab.ESTADO.Value) == "A",Rgb[232,76,61],Rgb(255,255,255))
			.ESTADO.ForeColor = Iif(Alltrim(DOCUMENTOS.ContainerCab.ESTADO.Value) == "A",Rgb(255,255,255),Rgb(0,0,0))
			.ESTADO.FontBold = Iif(Alltrim(DOCUMENTOS.ContainerCab.ESTADO.Value) == "A",.T.,.F.)

		Endwith

		**Outros Dados
		**Estes campos est�o disponiveis apenas para dossiers Internos
		With DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.page11.ContainerOutrosDados
			**IF myTipoDoc =="BO"
			**
			**	.xpddata.readonly = .f.
			**	.xpdhora.readonly = .f.
			**	.xpdviatura.readonly = .f.
			**	.xpdcontacto.readonly = .f.
			**	.xpdtelefone.readonly = .f.
			**	.xpdmorada.readonly = .f.
			**	.xpdcodpost.readonly = .f.
			**	.xpdemail.readonly = .f.
			**	.chkBloqPag.enable(.f.)
			**	.consignacao.enable(.f.)
			**	.editobs.readonly = .f.
			**ELSE
			**
			**	.xpddata.readonly = .t.
			**	.xpdhora.readonly = .t.
			**	.xpdviatura.readonly = .t.
			**	.xpdcontacto.readonly = .t.
			**	.xpdtelefone.readonly = .t.
			**	.xpdmorada.readonly = .t.
			**	.xpdcodpost.readonly = .t.
			**	.xpdemail.readonly = .t.
			**	.chkBloqPag.enable(.t.)
			**	.consignacao.enable(.t.)
			**	.contabilizado.enable(.t.)
			**	.editobs.readonly = .f.
			**
			**ENDIF

			.ARMAZEM.Enabled = .T.
			.DOCFL.Enabled = .T.
			.DOCFL.DisabledBackColor = Rgb(191,223,223)
			.DOCFLNO.ReadOnly = .F.
			.DOCFL.Enabled = .T.
			.DOCFLNO.Enabled = .T.
			.DATAINTERNA.Enabled = .T.
			.DATAENTREGA.Enabled = .T.
			.DATAINTERNA.DisabledBackColor = Rgb(255,255,255)
			.DATAENTREGA.DisabledBackColor = Rgb(255,255,255)
			.DATAFINAL.DisabledBackColor = Rgb(255,255,255)
			.DATAFINAL.Enabled = .T.
			.ARMAZEM.DisabledBackColor = Rgb(255,255,255)
			.NRVIAS.ReadOnly = .F.
			.editobs.ReadOnly = .F.



		Endwith

		********************************************

		**Excep��es campos de apenas alguns documentos
		If Alltrim(Upper(cabdoc.Doc)) == "V/NT. CR�DITO" Or;
				ALLTRIM(Upper(cabdoc.Doc)) == "N/GUIA ENTRADA" Or;
				ALLTRIM(Upper(cabdoc.Doc)) == "V/NT. D�BITO" Or;
				ALLTRIM(Upper(cabdoc.Doc)) == "V/VD. DINHEI."

			With DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.page11.ContainerOutrosDados
				.DOCFL.Enabled = .T.
				.DOCFLNO.Enabled = .T.
				.LabelDocF.Visible = .T.
			Endwith
		Endif

		With DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.page11.ContainerOutrosDados
			If Alltrim(Upper(cabdoc.Doc)) == "ENCOMENDA A FORNECEDOR" Or Alltrim(Upper(cabdoc.Doc)) == "ENCOMENDA VIA VERDE"
				.enviadafornec.Visible = .T.
				.enviadafornec.Enabled = .T.
			Else
				.enviadafornec.Enabled = .F.
			Endif
		Endwith

		With DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.Page12.PageFrame1.Page1.Container1
			.txtQtRec.ReadOnly = .F.
			.txtQtFac.ReadOnly = .F.
			.txtQtBonus.ReadOnly = .F.
			.txtDesc1.ReadOnly = .F.
			.txtDesc2.ReadOnly = .F.
			.txtTotal.ReadOnly = .F.
			.txtPVP.ReadOnly = .F.
			.txtValidade.ReadOnly = .F.
			.txtLote.ReadOnly = .F.
			.txtLoteConf.ReadOnly = .F.
			.txtLoteConf.Visible = .F.
		Endwith

		With DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.Page12.PageFrame1.Page2.Container2
			.txtQtRec.ReadOnly = .F.
			.txtQtFac.ReadOnly = .F.
			.txtQtBonus.ReadOnly = .F.
			.txtDesc1.ReadOnly = .F.
			.txtDesc2.ReadOnly = .F.
			.txtTotal.ReadOnly = .F.
			.txtPVP.ReadOnly = .F.
			.txtMC.ReadOnly = .F.
			.txtValidade.ReadOnly = .F.
			.txtLote.ReadOnly = .F.
			.txtLoteConf.ReadOnly = .F.
			**.txtLoteConf.visible = .f.
		Endwith

		With DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.page14

			If myTipoDoc =="BO"

				.XPDDATA.ReadOnly = .F.
				.XPDHORA.ReadOnly = .F.
				.XPDVIATURA.ReadOnly = .F.
				.XPDCONTACTO.ReadOnly = .F.
				.XPDTELEFONE.ReadOnly = .F.
				.XPDMORADA.ReadOnly = .F.
				.XPDCODPOST.ReadOnly = .F.
				.XPDEMAIL.ReadOnly = .F.
				.dataentr.ReadOnly = .F.
				.horaentr.ReadOnly = .F.
				.xpdLocalidade.ReadOnly = .F.

			Else

				.XPDDATA.ReadOnly = .T.
				.XPDHORA.ReadOnly = .T.
				.XPDVIATURA.ReadOnly = .T.
				.XPDCONTACTO.ReadOnly = .T.
				.XPDTELEFONE.ReadOnly = .T.
				.XPDMORADA.ReadOnly = .T.
				.XPDCODPOST.ReadOnly = .T.
				.XPDEMAIL.ReadOnly = .T.
				.horaentr.ReadOnly = .T.
				.xpdLocalidade.ReadOnly = .T.

			Endif

		Endwith

		**Grid; Controla Combos
		For i=1 To DOCUMENTOS.PageFrame1.Page1.gridDoc.ColumnCount

			If Upper(DOCUMENTOS.PageFrame1.Page1.gridDoc.Columns(i).ControlSource)=="BI.CCUSTO";
					OR Upper(DOCUMENTOS.PageFrame1.Page1.gridDoc.Columns(i).ControlSource)=="FN.FNCCUSTO";
					OR Upper(DOCUMENTOS.PageFrame1.Page1.gridDoc.Columns(i).ControlSource)=="BI.TABIVA";
					OR Upper(DOCUMENTOS.PageFrame1.Page1.gridDoc.Columns(i).ControlSource)=="FN.TABIVA";
					OR (Upper(DOCUMENTOS.PageFrame1.Page1.gridDoc.Columns(i).ControlSource)=="BI.QTT" And uf_gerais_getParameter_Site('ADM0000000060', 'BOOL', mySite))

				DOCUMENTOS.PageFrame1.Page1.gridDoc.Columns(i).ReadOnly = .T.
				DOCUMENTOS.PageFrame1.Page1.gridDoc.Columns(i).Enabled = .T.
			Endif
		Endfor

		**N�mero do Documento
		If Used("ucrsConfigDoc")
			Select ucrsConfigDoc
			If ucrsConfigDoc.nrseq == .T.
				DOCUMENTOS.ContainerCab.NUMDOC.ReadOnly = .T.
			Else
				DOCUMENTOS.ContainerCab.NUMDOC.ReadOnly = .F.
			Endif
		Endif
		***********************************************
		uf_documentos_alternaMenu()

	Endif

	**Documentos de Fornecedor associados ao documento
	If Alltrim(Upper(cabdoc.Doc)) == "V/NT. CR�DITO" Or;
			ALLTRIM(Upper(cabdoc.Doc)) == "N/GUIA ENTRADA" Or;
			ALLTRIM(Upper(cabdoc.Doc)) == "V/NT. D�BITO" Or;
			ALLTRIM(Upper(cabdoc.Doc)) == "V/VD. DINHEI."

		With DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.page11.ContainerOutrosDados
			.LabelDocF.Visible = .T.
			.DOCFL.Visible = .T.
			.DOCFLNO.Visible = .T.
			.LabelDocF.Enabled = .T.
			.DOCFL.Enabled = .T.
			.DOCFLNO.Enabled = .T.
		Endwith
	Else
		With DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.page11.ContainerOutrosDados
			.LabelDocF.Enabled = .F.
			.DOCFL.Enabled = .F.
			.DOCFLNO.Enabled = .F.
		Endwith
	Endif

	With DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.page11.ContainerOutrosDados
		If Alltrim(Upper(cabdoc.Doc)) == "ENCOMENDA A FORNECEDOR" Or Alltrim(Upper(cabdoc.Doc)) == "ENCOMENDA VIA VERDE"
			.enviadafornec.Visible = .T.
			.enviadafornec.Enabled = .T.
		Else
			.enviadafornec.Enabled = .F.
		Endif

		If Alltrim(Upper(cabdoc.Doc)) == "ENCOMENDA VIA VERDE" Or Alltrim(Upper(cabdoc.Doc)) == "DEVOL. A FORNECEDOR VV" Or Alltrim(Upper(cabdoc.Doc)) == "ENCOMENDA DE CLIENTE"
			.lbReceita.Visible = .T.
			.NRRECEITA.Visible = .T.
			.lblCodAc.Visible = .T.
			.receitacodac.Visible = .T.
			.lblcodop.Visible = .T.
			.receitacodop.Visible = .T.
			.lbReceita.Enabled = .T.
			.NRRECEITA.Enabled = .T.
			.lblCodAc.Enabled = .T.
			.receitacodac.Enabled = .T.
			.lblcodop.Enabled = .T.
			.receitacodop.Enabled = .T.

			If !Empty(cabdoc.NRRECEITA)
				.NRRECEITA.Value = cabdoc.NRRECEITA
			Endif
			If !Empty(cabdoc.pinacesso)
				.receitacodac.Value = cabdoc.pinacesso
			Endif
			If !Empty(cabdoc.pinopcao)
				.receitacodop.Value = cabdoc.pinopcao
			Endif

		Else
			.lbReceita.Enabled = .F.
			.NRRECEITA.Enabled = .F.
			.lblCodAc.Enabled = .F.
			.receitacodac.Enabled = .F.
			.lblcodop.Enabled = .F.
			.receitacodop.Enabled = .F.
		Endif
		If !Empty(cabdoc.CODEXT)
			.NRDOCEXT.Visible=.T.
			.lbNrDocExt.Visible=.T.
			.NRDOCEXT.Value = Alltrim(cabdoc.CODEXT)
		Else
			.NRDOCEXT.Enabled=.F.
			.lbNrDocExt.Enabled=.F.
		Endif
		If Used("ltBO2")
			.NRVIAS.Visible=.T.
			.lblNRVIAS.Visible=.T.
			.NRVIAS.Value = ltBO2.nrviasimp
		Else
			.NRVIAS.Enabled=.F.
			.lblNRVIAS.Enabled=.F.
		Endif


	Endwith
	***********************************************

	**Data Vencimento e Cond Pag
	If Alltrim(Upper(cabdoc.Doc)) == "V/FACTURA" Or Alltrim(Upper(cabdoc.Doc)) == "V/FACTURA MED."
		With DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.page10.ContainerTotais
			.DATAVENC.Visible = .T.
			.ldatavenc.Visible = .T.
			.CONDPAG.Visible = .T.
			.lcondPag.Visible = .T.
			.txtLocalTesouraria.Visible = .T.
			.imput_desp.Visible = .T.
			.label2.Visible = .T.
		Endwith
	Else
		With DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.page10.ContainerTotais
			.DATAVENC.Visible = .F.
			.ldatavenc.Visible = .F.
			.CONDPAG.Visible = .F.
			.lcondPag.Visible = .F.
			.txtLocalTesouraria.Visible = .F.
			.imput_desp.Visible = .F.
			.label2.Visible = .F.
		Endwith
	Endif
	***********************************************

	**Data Final/Validade
	If Alltrim(Upper(cabdoc.Doc)) == "RESERVA DE CLIENTE"
		With DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.page11.ContainerOutrosDados
			.DATAFINAL.Visible = .T.
			.ldataFinal.Visible = .T.
			.DATAFINAL.Enabled = .T.
			.ldataFinal.Enabled = .T.
		Endwith
	Else
		With DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.page11.ContainerOutrosDados
			.DATAFINAL.Enabled = .F.
			.ldataFinal.Enabled = .F.
		Endwith
	Endif
	***********************************************

	**Data Vencimento e Cond Pag
	If Alltrim(Upper(cabdoc.Doc)) == "ENCOMENDA DE CLIENTE"
		With DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.page10.ContainerTotais
			.lblentrega.Visible = .T.
			.lblpagamento.Visible = .T.
			.lblmomentopagam.Visible = .T.
			.lblestadoenc.Visible = .T.
			.txtentrega.Visible = .T.
			.txtpagamento.Visible = .T.
			.txtmomentopagam.Visible = .T.
			.txtestadoenc.Visible = .T.
		Endwith
	Else
		With DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.page10.ContainerTotais
			.lblentrega.Visible = .F.
			.lblpagamento.Visible = .F.
			.lblmomentopagam.Visible = .F.
			.lblestadoenc.Visible = .F.
			.txtentrega.Visible = .F.
			.txtpagamento.Visible = .F.
			.txtmomentopagam.Visible = .F.
			.txtestadoenc.Visible = .F.
		Endwith
	Endif
	***********************************************

	**Data datainterna
	If Alltrim(Upper(cabdoc.Doc)) == "N/GUIA ENTRADA" Or;
			ALLTRIM(Upper(cabdoc.Doc)) == "N/NT. CR�DITO" Or;
			ALLTRIM(Upper(cabdoc.Doc)) == "N/NT. D�BITO" Or;
			ALLTRIM(Upper(cabdoc.Doc)) == "V/GUIA TRANSP." Or;
			ALLTRIM(Upper(cabdoc.Doc)) == "V/NT. CR�DITO" Or;
			ALLTRIM(Upper(cabdoc.Doc)) == "V/NT. D�BITO" Or;
			ALLTRIM(Upper(cabdoc.Doc)) == "V/VD. DINHEI." Or;
			ALLTRIM(Upper(cabdoc.Doc)) == "V/FACTURA"

		With DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.page11.ContainerOutrosDados
			.DATAINTERNA.Visible = .T.
			.ldataInterna.Visible = .T.
			.DATAINTERNA.Enabled = .T.
			.ldataInterna.Enabled = .T.
		Endwith
	Else
		With DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.page11.ContainerOutrosDados
			.DATAINTERNA.Enabled = .F.
			.ldataInterna.Enabled = .F.
		Endwith
	Endif
	***********************************************

	**Data Entrega
	If Alltrim(Upper(cabdoc.Doc)) == "ENCOMENDA A FORNECEDOR" Or;
			ALLTRIM(Upper(cabdoc.Doc)) == "ENCOMENDA TIPO" Or;
			ALLTRIM(Upper(cabdoc.Doc)) == "ENCOMENDA DE CLIENTE"

		With DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.page11.ContainerOutrosDados
			.DATAENTREGA.Visible = .T.
			.ldataentrega.Visible = .T.
			.DATAENTREGA.Enabled = .T.
			.ldataentrega.Enabled = .T.
		Endwith
	Else
		With DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.page11.ContainerOutrosDados
			.DATAENTREGA.Enabled = .F.
			.ldataentrega.Enabled = .F.
		Endwith
	Endif
	***********************************************

	**Caso o documento esteja em modo de altera��o,
	**n�o � possivel alterar o tipo de Documento
	If 	myDocAlteracao == .T.
		With DOCUMENTOS.ContainerCab
			.documento.Enabled = .F.
		Endwith
	Endif

	Do Case
		Case myDocIntroducao = .T.
			DOCUMENTOS.Caption = "Documentos - Introdu��o"
		Case myDocAlteracao = .T.
			DOCUMENTOS.Caption = "Documentos - Altera��o"
		Otherwise
			DOCUMENTOS.Caption = "Documentos"
	Endcase

	**esconder chkBloqPag
	If Used("uCrsConfigDoc")
		With DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.page11.ContainerOutrosDados
			Select ucrsConfigDoc
			If ucrsConfigDoc.folanfc
				.chkBloqPag.Visible = .T.
			Else
				.chkBloqPag.Enabled = .F.
			Endif
		Endwith


		**CONSIGNACAO, V/Guia TRANSPORTE, N/Guia Entrada
		If Used("uCrsConfigDoc")
			With DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.page11.ContainerOutrosDados
				Select ucrsConfigDoc
				If ucrsConfigDoc.NUMINTERNODOC == 101 Or ucrsConfigDoc.NUMINTERNODOC == 102
					.consignacao.Visible = .T.
					.consignacao.Enabled = .T.
				Else
					.consignacao.Enabled = .F.
				Endif
			Endwith
		Endif

	Endif

	With DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.page10

		If myTipoDoc == "FO"
			.ContainerTotais.lblArredondamento.Visible = .T.
			.ContainerTotais.LabelAposArr.Visible = .T.
			.ContainerTotais.Arredondamento1.Visible = .T.
			.ContainerTotais.Arredondamento2.Visible = .T.
			.ContainerTotais.Arredondamento3.Visible = .T.
			.ContainerTotais.Arredondamento4.Visible = .T.

			.ContainerTotais.apa1.Visible = .T.
			.ContainerTotais.apa2.Visible = .T.
			.ContainerTotais.apa3.Visible = .T.
			.ContainerTotais.apa4.Visible = .T.

		Else
			.ContainerTotais.lblArredondamento.Visible = .F.
			.ContainerTotais.LabelAposArr.Visible = .F.
			.ContainerTotais.Arredondamento1.Visible = .F.
			.ContainerTotais.Arredondamento2.Visible = .F.
			.ContainerTotais.Arredondamento3.Visible = .F.
			.ContainerTotais.Arredondamento4.Visible = .F.

			.ContainerTotais.apa1.Visible = .F.
			.ContainerTotais.apa2.Visible = .F.
			.ContainerTotais.apa3.Visible = .F.
			.ContainerTotais.apa4.Visible = .F.

		Endif
	Endwith

	If cabdoc.anexos=.T.
		DOCUMENTOS.ContainerCab.Btanexos.Visible=.T.
	Else
		DOCUMENTOS.ContainerCab.Btanexos.Visible=.F.
	Endif



	**
	uf_documentos_ControlaDetalheST()
Endfunc


** Configura menu principal
Function uf_documentos_alternaMenu
	** Em modo de Leitura
	If Type("DOCUMENTOS")!="U"
		If myDocAlteracao == .F. And myDocIntroducao == .F.
			** menu1
			DOCUMENTOS.Menu1.ESTADO("opcoes, pesquisar, novo, editar, anterior, seguinte, verFicha", "SHOW", "Gravar", .F., "Menu Inicial", .T.)
			DOCUMENTOS.Menu1.ESTADO("impDoc, novaLinha, eliminarLinha, pesquisarStock, aplicarConf", "HIDE")
			**		DOCUMENTOS.menu1.estado("impDoc, novaLinha, eliminarLinha, pesquisarStock, aplicarConf, alterviz", "HIDE")
			If !uf_gerais_validaPermUser(ch_userno, ch_grupo, 'Documentos Fornecedores - Criar')
				DOCUMENTOS.Menu1.ESTADO("novo", "HIDE")
			Endif
			If !uf_gerais_validaPermUser(ch_userno, ch_grupo, 'Documentos Fornecedores - Alterar')
				DOCUMENTOS.Menu1.ESTADO("editar", "HIDE")
			Endif

			** menu_opcoes
			With DOCUMENTOS.menu_opcoes
				.ESTADO("ultimo, eliminar, etiquetas, enviarEnc, opcoesDiversas, imprimir", "SHOW")
				.ESTADO("descComerc", "HIDE")
				If !uf_gerais_validaPermUser(ch_userno, ch_grupo, 'Documentos Fornecedores - Eliminar')
					.ESTADO("eliminar", "HIDE")
				Endif
				If Alltrim(cabdoc.Doc)=='Encomenda a Fornecedor'
					.ESTADO("estadob2b", "SHOW")
				Else
					.ESTADO("estadob2b", "HIDE")
				Endif
			Endwith

			With DOCUMENTOS.menu_opcoes
				.ESTADO("prepararEnc, conferenciaFact", "SHOW")
				If Alltrim(cabdoc.Doc)=='Encomenda de Cliente'
					.ESTADO("processar", "SHOW")
				Else
					.ESTADO("processar", "HIDE")
				Endif

			Endwith

			If DOCUMENTOS.Height < 640 && Para controlar se as op��es cabem no ecra
				DOCUMENTOS.Menu1.ESTADO("anterior, seguinte, verFicha", "HIDE")
			Endif

			DOCUMENTOS.menu_opcoes.ESTADO("AgrupaRefs", "HIDE")

		Else
			Select cabdoc
			If Empty(cabdoc.Doc)
				** menu1
				DOCUMENTOS.Menu1.ESTADO("pesquisar, novo, editar, anterior, seguinte, verFicha, novaLinha, eliminarLinha, pesquisarStock", "HIDE", "Gravar", .T., "Cancelar", .T.)
				If !uf_gerais_validaPermUser(ch_userno, ch_grupo, 'Documentos Fornecedores - Criar')
					DOCUMENTOS.Menu1.ESTADO("novo", "HIDE")
				Endif
				If !uf_gerais_validaPermUser(ch_userno, ch_grupo, 'Documentos Fornecedores - Alterar')
					DOCUMENTOS.Menu1.ESTADO("editar", "HIDE")
				Endif
				** menu_opcoes
				With DOCUMENTOS.menu_opcoes
					.ESTADO("ultimo, eliminar, etiquetas, enviarEnc, opcoesDiversas, imprimir, estadob2b", "HIDE")
				Endwith

				With DOCUMENTOS.menu_opcoes
					.ESTADO("prepararEnc, conferenciaFact", "HIDE")
					If Alltrim(cabdoc.Doc)=='Encomenda de Cliente'
						.ESTADO("processar", "SHOW")
					Else
						.ESTADO("processar", "HIDE")
					Endif
				Endwith

				DOCUMENTOS.Menu1.ESTADO("impDoc", "HIDE")

			Else
				** menu1
				DOCUMENTOS.Menu1.ESTADO("pesquisar, novo, editar, anterior, seguinte, verFicha", "HIDE", "Gravar", .T., "Cancelar", .T.)
				**DOCUMENTOS.menu1.estado("impDoc,novaLinha, eliminarLinha, pesquisarStock, aplicarConf, alterviz ", "SHOW")
				DOCUMENTOS.Menu1.ESTADO("impDoc,novaLinha, eliminarLinha, pesquisarStock, aplicarConf ", "SHOW")
				If !uf_gerais_validaPermUser(ch_userno, ch_grupo, 'Documentos Fornecedores - Criar')
					DOCUMENTOS.Menu1.ESTADO("novo", "HIDE")
				Endif
				If !uf_gerais_validaPermUser(ch_userno, ch_grupo, 'Documentos Fornecedores - Alterar')
					DOCUMENTOS.Menu1.ESTADO("editar", "HIDE")
				Endif
				** menu_opcoes
				With DOCUMENTOS.menu_opcoes
					.ESTADO("ultimo, eliminar, etiquetas, enviarEnc, opcoesDiversas, imprimir, estadob2b", "HIDE")
				Endwith

				With DOCUMENTOS.menu_opcoes
					.ESTADO("prepararEnc, conferenciaFact", "HIDE")
				Endwith

				DOCUMENTOS.menu_opcoes.ESTADO("AgrupaRefs ", "SHOW")

				** Conferencia Robotica s� est� disponivel para o documento V/Factura
				If Alltrim(cabdoc.Doc) == "V/Factura" Or Alltrim(cabdoc.Doc) == "V/Guia Transp." Or Alltrim(cabdoc.Doc) == "N/Guia Entrada" Or Alltrim(cabdoc.Doc) == "V/Factura Med."
					DOCUMENTOS.Menu1.ESTADO("aplicarConf", "SHOW")
					**DOCUMENTOS.menu_opcoes.estado("confRobotica", "SHOW")
					**DOCUMENTOS.menu_opcoes.estado("abrirConfRobotica", "SHOW")

				Else
					DOCUMENTOS.Menu1.ESTADO("aplicarConf", "HIDE")
					**DOCUMENTOS.menu_opcoes.estado("confRobotica", "HIDE")
					**DOCUMENTOS.menu_opcoes.estado("abrirConfRobotica", "HIDE")
				Endif
				DOCUMENTOS.menu_opcoes.ESTADO("descComerc", "SHOW")

			Endif
		Endif
	Endif
Endfunc


**
Function uf_documentos_chamaConfEnc
	If Empty(Alltrim(cabdoc.Doc))
		uf_perguntalt_chama("DEVE SELECIONAR UM DOCUMENTO PARA PROSSEGUIR COM A CONFER�NCIA. POR FAVOR VERIFIQUE!","OK","",64)
	Else
		DOCUMENTOS.ContainerCab.NUMDOC.SetFocus
		uf_confencomendas_chama()
	Endif
Endfunc


** Novo Documento **
Function uf_documentos_novoDoc
	Lparameters lnAbreEscolheDoc, lnorigempesq


	If Vartype(lnorigempesq)=='C'
		lcorigempesq = Alltrim(lnorigempesq)
	Else
		lcorigempesq = ''
	Endif

	** Verificar se existe nova vers�o para reiniciar o software
	uf_chk_nova_versao()

	If myDocAlteracao == .T. Or myDocIntroducao == .T.
		uf_perguntalt_chama("O DOCUMENTO EST� EM MODO DE INTRODU��O/EDI��O. DEVE CANCELAR OU GRAVAR PARA PROCEDER COM ESTA AC��O.","OK","",64)
		Return .F.
	Endif

	Store .F. To myDocAlteracao
	Store .T. To myDocIntroducao

	myTipoDoc = ""

	**Limpa Cursores
	If Used("cabDoc")
		Select cabdoc
		Go Top
		Scan
			Delete
		Endscan
		Select cabdoc
		Append Blank
	Endif

	** Elimina as Linhas
	If Used("BI")
		Select Bi
		Scan
			Delete
		Endscan
	Endif

	If Used("BI2")
		Select BI2
		Scan
			Delete
		Endscan
	Endif

	If Used("FN")
		Select FN
		Scan
			Delete
		Endscan
	Endif

	If Used("ucrsfndesclin")
		Select ucrsfndesclin
		Scan
			Delete
		Endscan
	Endif

	If Used("ucrsTotaisDocumento")
		Select ucrsTotaisDocumento
		Go Top
		Scan
			Delete
		Endscan
		uf_documentos_CalculaTotais()
	Endif


	Select cabdoc
	Replace cabdoc.SITE With mySite

	**
	DOCUMENTOS.ContainerCab.documento.Enabled = .T.
	DOCUMENTOS.ContainerCab.documento.DisabledBackColor = Rgb(191,223,223)

	uf_documentos_alternaMenu()

	DOCUMENTOS.Refresh

	**Abre pesquisa de Documento a Introduzir


	If uf_documentos_pendentes()
		If uf_perguntalt_chama("Deseja retomar o documento anterior?","Sim","N�o")
			uf_documentos_criaNovaLinhaTemp()
		Else
			uf_documentos_apagarTemp()
			If lnAbreEscolheDoc == .F.
				DOCUMENTOS.ContainerCab.documento.Click
			Endif
		Endif
	Else
		If lnAbreEscolheDoc == .F.
			DOCUMENTOS.ContainerCab.documento.Click
		Endif
	Endif

Endfunc

** Novo Documento **
Function uf_documentos_novoDoc_escolhe
	Lparameters lnDocumento, lnTemp
	Local lcDocumento
	lcDocumento = Alltrim(lnDocumento)

	** Verificar se existe nova vers�o para reiniciar o software
	uf_chk_nova_versao()

	If (myDocAlteracao == .T. Or myDocIntroducao == .T.  ) And !lnTemp
		uf_perguntalt_chama("O DOCUMENTO EST� EM MODO DE INTRODU��O/EDI��O. DEVE CANCELAR OU GRAVAR PARA PROCEDER COM ESTA AC��O.","OK","",64)
		Return .F.
	Endif

	Store .F. To myDocAlteracao
	Store .T. To myDocIntroducao

	myTipoDoc = ""

	**Limpa Cursores
	If Used("cabDoc")
		Select cabdoc
		Go Top
		Scan
			Delete
		Endscan
		Select cabdoc
		Append Blank
	Endif

	** Elimina as Linhas
	If Used("BI")
		Select Bi
		Scan
			Delete
		Endscan
	Endif

	If Used("BI2")
		Select BI2
		Scan
			Delete
		Endscan
	Endif

	If Used("FN")
		Select FN
		Scan
			Delete
		Endscan
	Endif

	If Used("ucrsfndesclin")
		Select ucrsfndesclin
		Scan
			Delete
		Endscan
	Endif

	If Used("ucrsTotaisDocumento")
		Select ucrsTotaisDocumento
		Go Top
		Scan
			Delete
		Endscan
		uf_documentos_CalculaTotais()
	Endif

	**
	DOCUMENTOS.ContainerCab.documento.Enabled = .T.
	DOCUMENTOS.ContainerCab.documento.DisabledBackColor = Rgb(191,223,223)

	Select cabdoc
	Replace cabdoc.Doc With Alltrim(lcDocumento)

	uf_documentos_ConfiguracoesDoc()
	uf_documentos_alternaMenu()
	uf_documentos_criaNovaLinha(.T.,.T.)


	DOCUMENTOS.Refresh

Endfunc


** Editar Documento
Function uf_documentos_alterarDoc

	** Verificar se existe nova vers�o para reiniciar o software
	uf_chk_nova_versao()


	&& Verificar data fechada para altera��es
	If uf_gerais_getParameter_Site('ADM0000000006','BOOL', mySite_nr)
		If !Empty(Alltrim(uf_gerais_getParameter_Site('ADM0000000007', 'text', mySite_nr))) And Dtoc(cabdoc.DATADOC,1)<uf_gerais_getParameter_Site('ADM0000000007', 'text', mySite_nr)
			uf_perguntalt_chama("N�o pode alterar/eliminar um documento com data inferior � data fechada para altera��es/elimina��es.","OK","",64)
			Return .F.
		Endif
	Endif

	&& Valida��es
	If Upper(Alltrim(ucrse1.PAIS)) == 'PORTUGAL' And cabdoc.ARMAZEM != myArmazem
		uf_perguntalt_chama("N�O PODE EDITAR DOCUMENTOS QUE N�O TENHAM SIDO CRIADOS NO ARMAZEM EM QUE SE ENCONTRA NESTE MOMENTO.","OK","",64)
		Return .F.
	Endif

	If myDocAlteracao == .T. Or myDocIntroducao == .T.
		uf_perguntalt_chama("O DOCUMENTO EST� EM MODO DE INTRODU��O/EDI��O. DEVE CANCELAR OU GRAVAR PARA PROCEDER COM ESTA AC��O.","OK","",64)
		Return .F.
	Endif
	If !Used("cabDoc")
		uf_perguntalt_chama("DEVE SELECIONAR O DOCUMENTO A EDITAR. POR FAVOR VERIFIQUE.","OK","",64)
		Return .F.
	Endif
	If Reccount("cabDoc") == 0
		uf_perguntalt_chama("DEVE SELECIONAR O DOCUMENTO A EDITAR. POR FAVOR VERIFIQUE.","OK","",64)
		Return .F.
	Endif
	If Empty(Alltrim(cabdoc.cabstamp))
		uf_perguntalt_chama("DEVE SELECIONAR O DOCUMENTO A EDITAR. POR FAVOR VERIFIQUE.","OK","",64)
		Return .F.
	Endif
	If Alltrim(cabdoc.ESTADODOC) == "F"
		uf_perguntalt_chama("N�O � POSSIVEL EDITAR UM DOCUMENTO COM ESTADO FECHADO.","OK","",64)
		Return .F.
	Endif
	If Alltrim(cabdoc.Doc) == "Reserva de Cliente"

		Select Bi
		Calculate Count() To uv_count For !Empty(NRRECEITA)

		If uv_count <> 0
			uf_perguntalt_chama("N�O � PERMITIDA A EDI��O DE RESERVAS GERADAS A PARTIR DE VENDAS COM RECEITA.","OK","",64)
			Return .F.
		Endif

	Endif

	&& Validade se � encomenda via verde e n�o permite editar caso tenha sido enviada com sucesso
	If Alltrim(cabdoc.Doc) == "Encomenda Via Verde"
		lcSQL = ''
		TEXT TO lcSQL NOSHOW TEXTMERGE
			select * from via_verde_med (nolock) where bostamp = '<<ALLTRIM(cabdoc.cabstamp)>>' and aceite = 1
		ENDTEXT
		If !uf_gerais_actGrelha("", "uCrsViaVerdeAux", lcSQL)
			uf_perguntalt_chama("N�o foi possivel validar liga��es do documento.","OK","",16)
			Return .F.
		Endif
		If Reccount("uCrsViaVerdeAux") > 0
			uf_perguntalt_chama("N�o pode editar Encomendas do tipo Via Verde que j� tenham sido enviadas com sucesso para o fornecedor.","OK","",16)
			If Used("uCrsViaVerdeAux")
				fecha("uCrsViaVerdeAux")
			Endif
			Return .F.
		Endif
	Endif

	&& valida perfil
	Select cabdoc
	If !(uf_gerais_validaPermUser(ch_userno, ch_grupo, Alltrim(cabdoc.Doc) + ' - Alterar'))
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE EDITAR ESTE TIPO DE DOCUMENTO.","OK","",48)
		Return .F.
	Endif

	&& verifica Dev. a Fornecedor
	Select cabdoc
	If !Empty(Alltrim(cabdoc.ATDocCode))
		uf_perguntalt_chama("O documento j� foi submetido � AT. Por isso n�o poder� ser alterado.","OK","",48)
		Return .F.
	Endif

	&&verifica se o doc j� foi exportado
	Select cabdoc
	If !Empty(cabdoc.EXPORTADO)
		If uf_gerais_validaPermUser(ch_userno, ch_grupo, 'Altera��o registos exportados') == .T.
			uf_perguntalt_chama("O seu perfil n�o permite editar registos exportados para a contabilidade.","OK","",32)
			Return .F.
		Endif

		If uf_perguntalt_chama("Documento j� exportado para a Contabilidade. Pretende anular a exporta��o?","Sim","N�o",48)

			If myTipoDoc == "BO"
				TEXT TO lcSQL NOSHOW TEXTMERGE
					UPDATE BO SET exportado = 0 WHERE bostamp = '<<ALLTRIM(cabDoc.cabstamp)>>'
				ENDTEXT
			Else
				TEXT TO lcSQL NOSHOW TEXTMERGE
					UPDATE FO SET exportado = 0 WHERE fostamp = '<<ALLTRIM(cabDoc.cabstamp)>>'
				ENDTEXT
			Endif

			If !uf_gerais_actGrelha("", "", lcSQL)
				uf_perguntalt_chama("N�o foi possivel anular a exporta��o do documento","OK","",16)
				Return .F.
			Else

				Select cabdoc
				Replace cabdoc.EXPORTADO With .F.

				uf_perguntalt_chama("Exporta��o anulada com sucesso.","OK","",64)
			Endif

		Else
			Return .F.
		Endif

	Endif

	&& se for encomenda tipo sugere a copia do documento e coloca em modo de edi��o - Lu�s Leal 20160517
	If Upper(Alltrim(cabdoc.Doc)) == 'ENCOMENDA TIPO'
		uf_documentos_editaEncTipo()
		Return .F.
	Else
		** Valida se o documento tem liga��es a outros documentos, caso tenha n�o permite a edi��o - Quest�o de Performance aqui ??
		lcSQL = ''
		TEXT TO lcSQL NOSHOW TEXTMERGE
			exec up_Documentos_verificaLigacoes '<<ALLTRIM(myTipoDoc)>>','<<ALLTRIM(cabDoc.cabstamp)>>'
		ENDTEXT
		If !uf_gerais_actGrelha("", "ucrsLigacoesDoc", lcSQL)
			uf_perguntalt_chama("N�o foi possivel validar liga��es do documento.","OK","",16)
			Return .F.
		Endif
		If Reccount("ucrsLigacoesDoc") != 0
			uf_perguntalt_chama("N�o � possivel alterar um documento com liga��es a outro documento. Verique os destinos do documento.","OK","",48)
			Return .F.
		Endif
		**
	Endif

	If myTipoDoc == "BO"
		If cabdoc.NUMINTERNODOC == 46 &&ConversaoUnidades
			uf_documentos_EliminaLinhasComponentes()
		Endif
	Endif

	Store .T. To myDocAlteracao
	Store .F. To myDocIntroducao

	uf_documentosControlaObj()
	uf_documentos_controlaWkLinhas()

	If  Alltrim(myTipoDoc) == "BO"

		Select Bi
		Goto Top
		Scan

			uf_documentos_editarTemp(Bi.bistamp)

		Endscan

	Else

		Select FN
		Goto Top
		Scan
			uf_documentos_editarTemp(FN.fnstamp)
		Endscan

	Endif



Endfunc


**
Function uf_documentos_actualizaDoc

	If myDocAlteracao == .T. Or myDocIntroducao == .T.
		uf_perguntalt_chama("O DOCUMENTO EST� EM MODO DE INTRODU��O/EDI��O. DEVE CANCELAR OU GRAVAR PARA PROCEDER COM ESTA AC��O.","OK","",64)
		Return .F.
	Endif
	If myDocAlteracao == .F. And myDocIntroducao == .F.
		If Used("cabDoc")
			Select cabdoc
			uf_documentos_Chama(Alltrim(cabdoc.cabstamp))
		Endif
	Endif
Endfunc


** Actualiza o Ecra com o Ultimo Documento que encontrar
Function uf_documentos_ultimoRegisto
	Local lcStamp

	**Se estiver em modo de edi��o n�o � possivel usar esta fun��o
	If myDocAlteracao == .T. Or myDocIntroducao
		uf_perguntalt_chama("O DOCUMENTO EST� EM MODO DE INTRODU��O/EDI��O. DEVE CANCELAR OU GRAVAR PARA PROCEDER COM ESTA AC��O.","OK","",64)
		Return .F.
	Endif

	**Eliminina possiveis registos de Cabe�alho e linhas
	uf_documentos_apagaRegistosCabDoc()
	uf_documentos_apagaRegistosLinhas()
	**App Gerais (O parametro � irrelevante, tratamento igual para BO ou FO)
	lcStamp = uf_gerais_devolveUltRegisto('BO')
	uf_documentos_Chama(lcStamp)
Endfunc


**Apaga Registos do Cursor de Cabe�alho
Function uf_documentos_apagaRegistosCabDoc

	If myDocAlteracao == .F. And myDocIntroducao == .F.
		Select cabdoc
		Scan
			Delete
		Endscan
	Endif
Endfunc


**Apaga Registos do Cursor das Linhas
Function uf_documentos_apagaRegistosLinhas

	If myDocAlteracao == .F. And myDocIntroducao == .F.
		If myTipoDoc = "BO"

			Delete From BI2
			Select BI2
			Go Top
			Delete From Bi
			Select Bi
			Go Top
		Endif

		If myTipoDoc = "FO"

			Delete From FN
			Select FN
			Go Top

		Endif
	Endif
Endfunc


**Fun��o para eliminar Documentos
Function uf_documentos_eliminarDoc
	If myDocAlteracao == .T. Or myDocIntroducao == .T.
		uf_perguntalt_chama("O DOCUMENTO EST� EM MODO DE INTRODU��O/EDI��O. DEVE CANCELAR OU GRAVAR PARA PROCEDER COM ESTA AC��O.","OK","",64)
		Return .F.
	Endif

	If uf_gerais_getParameter_Site('ADM0000000006','BOOL', mySite_nr)
		If !Empty(Alltrim(uf_gerais_getParameter_Site('ADM0000000007', 'text', mySite_nr))) And Dtoc(cabdoc.DATADOC,1)<uf_gerais_getParameter_Site('ADM0000000007', 'text', mySite_nr)
			uf_perguntalt_chama("N�o pode eliminar/alterar um documento com data inferior � data fechada para altera��es/elimina��es.","OK","",64)
			Return .F.
		Endif
	Endif

	If !Used("cabDoc")
		Return .F.
	Endif
	If Empty(cabdoc.cabstamp)
		Return .F.
	Endif

	Local lcValida,lcValidaLigacaoDocs
	Store .F. To lcValidaLigacaoDocs

	** Valida Perfil de Elimina��o <nomeDoc - Eliminar>
	lcValida = uf_documentos_validaPerfilEliminar()

	&&verifica Dev. a Fornecedor
	Select cabdoc
	Go Top
	If !Empty(Alltrim(cabdoc.ATDocCode))
		uf_perguntalt_chama("O documento j� foi submetido � AT. Por isso n�o poder� ser eliminado.","OK","",48)
		Return .F.
	Endif

	&& Validade se � encomenda via verde e n�o permite editar caso tenha sido enviada com sucesso
	If Alltrim(cabdoc.Doc) == "Encomenda Via Verde"
		lcSQL = ''
		TEXT TO lcSQL NOSHOW TEXTMERGE
			select * from via_verde_med (nolock) where bostamp = '<<ALLTRIM(cabdoc.cabstamp)>>' and aceite = 1
		ENDTEXT
		If !uf_gerais_actGrelha("", "uCrsViaVerdeAux", lcSQL)
			uf_perguntalt_chama("N�o foi possivel validar liga��es do documento.","OK","",16)
			Return .F.
		Endif
		If Reccount("uCrsViaVerdeAux") > 0
			uf_perguntalt_chama("N�o pode eliminar Encomendas do tipo Via Verde que j� tenham sido enviadas com sucesso para o fornecedor.","OK","",16)
			If Used("uCrsViaVerdeAux")
				fecha("uCrsViaVerdeAux")
			Endif
			Return .F.
		Endif
	Endif

	Select cabdoc
	If Alltrim(Upper(myTipoDoc)) == "BO"

		uv_valid = .T.

		Select cabdoc

		Do Case
			Case uf_gerais_getUmvalor("ext_esb_orders", "count(*)", "bostamp = '" + Alltrim(cabdoc.cabstamp) + "'") <> 0
				uv_valid = .F.

			Case uf_gerais_getUmvalor("bo", "logi1", "bostamp = '" + Alltrim(cabdoc.cabstamp) + "'")
				uv_valid = .F.

			Otherwise
				uv_valid = .T.

		Endcase

		If !uv_valid

			uf_perguntalt_chama("N�o pode eliminar o documento pois o mesmo j� foi comunicado.","OK","",16)
			Return .F.

		Endif

	Endif

	* Tem permiss�es para eliminar
	If lcValida == .T.
		If uf_documentos_verificaContadoresPsicoBenzo() == .F.
			Return .F.
		Endif

		DOCUMENTOS.ContainerCab.documento.SetFocus

		If uf_perguntalt_chama("Quer mesmo Eliminar o documento?","Sim","N�o")
			** valida password de gest�o de documentos **
			Local lcValidaPass
			lcValidaPass = uf_documentos_controlaPass()
			If !lcValidaPass
				Return .F.
			Endif
			*********************************************

			&& Valida liga��o a documentos
			lcValidaLigacaoDocs = uf_documentos_validaLigacaoDocs()
			If lcValidaLigacaoDocs
				Return .F.
			Endif
			*********************************************

			**Valida��o para evitar erros
			If !(myTipoDoc == "BO") And !(myTipoDoc == "FO")
				Return .F.
			Endif

			Select cabdoc
			lcSQL = ""
			TEXT TO lcSQL NOSHOW textmerge
				exec up_documentos_EliminaDocumento '<<Alltrim(cabDoc.cabstamp)>>','<<UPPER(ALLTRIM(myTipoDoc))>>'
			ENDTEXT
			If !uf_gerais_actGrelha("", "", lcSQL)
				uf_perguntalt_chama("N�O FOI POSS�VEL ELIMINAR O DOCUMENTO. POR FAVOR CONTACTE O SUPORTE. COD. DELETEBO","OK","",16)
				Return .F.
			Endif

			** limpar cursor de pesquisa
			If Used("ucrsPesqDocs")
				Select ucrsPesqDocs
				Locate For Alltrim(ucrsPesqDocs.cabstamp) == Alltrim(cabdoc.cabstamp)
				Delete
				Go Top
			Endif
			****************************************

			uf_gerais_registaOcorrencia('Documentos','Elimina��o do Documento: ' + Alltrim(cabdoc.Doc) + " Nr. " + ASTR(cabdoc.NUMDOC),2,'','',Alltrim(cabdoc.cabstamp),ch_userno,mytermno,Date())
			uf_documentos_ultimoRegisto()
		Endif
	Endif
Endfunc


** Fun��o de Controlo de Elimina��o de Documentos
Function uf_documentos_validaPerfilEliminar
	If Used("cabDoc")
		Select cabdoc
		If !(uf_gerais_validaPermUser(ch_userno, ch_grupo, Alltrim(cabdoc.Doc) + ' - Eliminar'))
			uf_perguntalt_chama("O SEU PERFIL N�O PERMITE ELIMINAR ESTE TIPO DE DOCUMENTO.","OK","",48)
			Return .F.
		Endif
	Endif

	Return .T.
Endfunc


**
Function uf_documentos_validaLigacaoDocs

	** Valida se o documento tem liga��es a outros documentos, caso tenha n�o permite a edi��o
	TEXT TO lcSQL NOSHOW TEXTMERGE
		exec up_Documentos_verificaLigacoes '<<ALLTRIM(myTipoDoc)>>','<<ALLTRIM(cabDoc.cabstamp)>>'
	ENDTEXT
	If !uf_gerais_actGrelha("", "ucrsLigacoesDoc", lcSQL)
		uf_perguntalt_chama("N�o foi possivel validar liga��es do documento.","OK","",16)
		Return .T.
	Endif
	If Reccount("ucrsLigacoesDoc") != 0
		uf_perguntalt_chama("N�o � possivel eliminar um documento com liga��es a outro documento. Verique os destinos do documento.","OK","",48)
		Return .T.
	Endif
	***************************************

	Return .F.
Endfunc


** Navega para o registo anterior
Function uf_documentos_registoanterior

	If  myDocAlteracao == .T. Or myDocIntroducao == .T. Or !Used("cabDoc")
		uf_perguntalt_chama("O DOCUMENTO EST� EM MODO DE INTRODU��O/EDI��O. DEVE CANCELAR OU GRAVAR PARA PROCEDER COM ESTA AC��O.","OK","",64)
		Return .F.
	Endif

	** BO
	*!*		If mytipoDoc = "BO"
	*!*			lctipodoc = cabDoc.doc
	*!*			lcnumdoc = Val(cabDoc.Numdoc) - 1
	*!*
	*!*			IF !uf_gerais_actGrelha("", "ucrsNovoRegisto", "SELECT bostamp as cabstamp FROM bo (nolock) WHERE YEAR(bo.dataobra)="+astr(YEAR(cabdoc.datadoc))+" and nmdos = '" + Alltrim(lctipodoc) + "' AND OBRANO = "+ Str(lcnumdoc) +"")
	*!*				RETURN .f.
	*!*			ENDIF
	*!*
	*!*			SELECT ucrsNovoRegisto
	*!*			lcnovostamp= ucrsNovoRegisto.cabstamp
	*!*			IF USED("ucrsNovoRegisto")
	*!*				fecha("ucrsNovoRegisto")
	*!*			ENDIF
	*!*
	*!*			If !Empty(Alltrim(lcnovostamp))
	*!*				uf_documentos_Chama(lcnovostamp)
	*!*			Endif
	*!*		Endif
	*!*
	*!*		** FO
	*!*		If mytipoDoc = "FO"
	*!*			lcSQL = ""
	*!*			TEXT TO lcSQL TEXTMERGE NOSHOW
	*!*				SELECT 	ousrhora,ousrdata,docdata,docnome,fostamp
	*!*				FROM 		FO (nolock)
	*!*				WHERE 	docnome = '<<Alltrim(cabDoc.Doc)>>'
	*!*							AND Ltrim(Rtrim(adoc))= '<<Alltrim(cabDoc.numdoc)>>'
	*!*							AND docdata = '<<Alltrim(uf_gerais_getDate(cabDoc.datadoc,"SQL"))>>'
	*!*			ENDTEXT
	*!*			If uf_gerais_actGrelha("", "tempSQLdocactual", lcSQL)
	*!*				lcSQL = ""
	*!*				TEXT TO lcSQL TEXTMERGE NOSHOW
	*!*					SELECT 	TOP 1 cabstamp = fostamp
	*!*					FROM 		FO (nolock)
	*!*					WHERE 		ousrdata + ousrhora < CONVERT(datetime,'<<uf_gerais_getDate(tempSQLdocactual.ousrdata,"SQL")>>') + '<<Alltrim(tempSQLdocactual.ousrhora)>>'
	*!*								AND docnome = '<<Alltrim(tempSQLdocactual.docnome)>>'
	*!*					ORDER BY ousrdata + ousrhora desc
	*!*				ENDTEXT
	*!*				If !uf_gerais_actGrelha("", "ucrsNovoRegisto", lcSQL)
	*!*					RETURN .f.
	*!*				ENDIF
	*!*				SELECT ucrsNovoRegisto
	*!*				lcnovostamp=Alltrim(ucrsNovoRegisto.cabstamp)
	*!*				IF USED("ucrsNovoRegisto")
	*!*					fecha("ucrsNovoRegisto")
	*!*				ENDIF
	*!*				If !Empty(Alltrim(lcnovostamp))
	*!*					uf_documentos_Chama(lcnovostamp)
	*!*				Endif
	*!*			Endif
	*!*		Endif

	Select cabdoc
	Local lcstampftactual, lcnovostamp
	Store '' To lcnovostamp
	lcstampftactual = Alltrim(cabdoc.cabstamp)
	If Used("ucrsPesqDocscl")
		Local lcnext
		Store .F. To lcnext
		Select ucrsPesqDocscl
		Go Top
		Scan
			If Alltrim(lcstampftactual) == Alltrim(ucrsPesqDocscl.cabstamp)
				lcnext = .T.
			Endif
			If lcnext = .F.
				lcnovostamp= Alltrim(ucrsPesqDocscl.cabstamp)
				lcnext = .F.
			Endif
		Endscan
		If Empty(lcnovostamp)
			Select ucrsPesqDocscl
			Go Bottom
			lcnovostamp= ucrsPesqDocscl.cabstamp
		Endif

	Else
		Local lcnext
		Store .F. To lcnext
		If Used("ucrsPesqDocs")
			Select ucrsPesqDocs
			Go Top
			Scan
				If Alltrim(lcstampftactual) == Alltrim(ucrsPesqDocs.cabstamp)
					lcnext = .T.
				Endif
				If lcnext = .F.
					lcnovostamp= Alltrim(ucrsPesqDocs.cabstamp)
					lcnext = .F.
				Endif
			Endscan
			If Empty(lcnovostamp)
				Select ucrsPesqDocs
				Go Bottom
				lcnovostamp= ucrsPesqDocs.cabstamp
			Endif
		Endif
	Endif

	If !Empty(Alltrim(lcnovostamp))
		uf_documentos_Chama(lcnovostamp)
	Endif

Endfunc


**
Function uf_documentos_registoseguinte

	If  myDocAlteracao == .T. Or myDocIntroducao == .T. Or !Used("cabDoc")
		uf_perguntalt_chama("O DOCUMENTO EST� EM MODO DE INTRODU��O/EDI��O. DEVE CANCELAR OU GRAVAR PARA PROCEDER COM ESTA AC��O.","OK","",64)
		Return .F.
	Endif

	Select cabdoc
	** BO
	*!*		If mytipoDoc = "BO"
	*!*
	*!*			lctipodoc = cabDoc.doc
	*!*			lcnumdoc = Val(cabDoc.Numdoc) + 1
	*!*
	*!*			If !uf_gerais_actGrelha("", "ucrsNovoRegisto", "SELECT bostamp as cabstamp FROM bo (nolock) WHERE YEAR(bo.dataobra)="+astr(YEAR(cabdoc.datadoc))+" and nmdos = '" + Alltrim(lctipodoc) + "' AND OBRANO = "+ Str(lcnumdoc) +"")
	*!*				RETURN .f.
	*!*			Endif
	*!*			SELECT ucrsNovoRegisto
	*!*			lcnovostamp= ucrsNovoRegisto.cabstamp
	*!*			IF USED("ucrsNovoRegisto")
	*!*				fecha("ucrsNovoRegisto")
	*!*			ENDIF
	*!*			If !Empty(Alltrim(lcnovostamp))
	*!*				uf_documentos_Chama(lcnovostamp)
	*!*			Endif
	*!*		Endif
	*!*
	*!*		** FO
	*!*		If mytipoDoc = "FO"
	*!*			lcSQL = ""
	*!*			TEXT TO lcSQL TEXTMERGE NOSHOW
	*!*				SELECT 	ousrhora,ousrdata,docdata,docnome,fostamp
	*!*				FROM 		FO (nolock)
	*!*				WHERE 	docnome = '<<Alltrim(cabDoc.Doc)>>'
	*!*							AND Ltrim(Rtrim(adoc))= '<<Alltrim(cabDoc.numdoc)>>'
	*!*							AND docdata = '<<Alltrim(uf_gerais_getDate(cabDoc.datadoc,"SQL"))>>'
	*!*			ENDTEXT
	*!*			If uf_gerais_actGrelha("", "tempSQLdocactual", lcSQL)
	*!*				lcSQL = ""
	*!*				TEXT TO lcSQL TEXTMERGE NOSHOW
	*!*					SELECT 	TOP 1 cabstamp = fostamp
	*!*					FROM 		FO (nolock)
	*!*					WHERE 		ousrdata + ousrhora > CONVERT(datetime,'<<uf_gerais_getDate(tempSQLdocactual.ousrdata,"SQL")>>') + '<<Alltrim(tempSQLdocactual.ousrhora)>>'
	*!*								AND docnome = '<<Alltrim(tempSQLdocactual.docnome)>>'
	*!*					ORDER BY ousrdata + ousrhora asc
	*!*				ENDTEXT
	*!*				If !uf_gerais_actGrelha("", "ucrsNovoRegisto", lcSQL)
	*!*					RETURN .f.
	*!*				ENDIF
	*!*				SELECT ucrsNovoRegisto
	*!*				lcnovostamp= ucrsNovoRegisto.cabstamp
	*!*				IF USED("ucrsNovoRegisto")
	*!*					fecha("ucrsNovoRegisto")
	*!*				ENDIF
	*!*				If !Empty(Alltrim(lcnovostamp))
	*!*					uf_documentos_Chama(lcnovostamp)
	*!*				Endif
	*!*			Endif
	*!*		ENDIF

	Local lcstampftactual, lcnovostamp
	Store '' To lcnovostamp
	lcstampftactual = Alltrim(cabdoc.cabstamp)

	If Used("ucrsPesqDocscl")

		Local lcnext
		Store .F. To lcnext
		Select ucrsPesqDocscl
		Go Top
		Scan
			If lcnext = .T.
				lcnovostamp= Alltrim(ucrsPesqDocscl.cabstamp)
				lcnext = .F.
			Endif
			If Alltrim(lcstampftactual) == Alltrim(ucrsPesqDocscl.cabstamp)
				lcnext = .T.
			Endif
		Endscan
		If Empty(lcnovostamp)
			Select ucrsPesqDocscl
			Go Top
			lcnovostamp= ucrsPesqDocscl.cabstamp
		Endif

	Else

		Local lcnext
		Store .F. To lcnext
		If(Used("ucrsPesqDocs"))
			Select ucrsPesqDocs
			Go Top
			Scan
				If lcnext = .T.
					lcnovostamp= Alltrim(ucrsPesqDocs.cabstamp)
					lcnext = .F.
				Endif
				If Alltrim(lcstampftactual) == Alltrim(ucrsPesqDocs.cabstamp)
					lcnext = .T.
				Endif
			Endscan
			If Empty(lcnovostamp)
				Select ucrsPesqDocs
				Go Top
				lcnovostamp= ucrsPesqDocs.cabstamp
			Endif
		Endif

	Endif

	If !Empty(Alltrim(lcnovostamp))
		uf_documentos_Chama(lcnovostamp)
	Endif
Endfunc


** lost focus data documento
Function uf_documentos_ValidaDataEntrega
	If !Used("cabDoc")
		Return .F.
	Endif

	Select cabdoc
	If !Empty(cabdoc.DATAENTREGA) And cabdoc.DATAENTREGA<cabdoc.DATADOC
		Replace  cabdoc.DATAENTREGA With cabdoc.DATADOC
	Endif
Endfunc

**removar linhas vazias
Function uf_documentos_removeLinhasVazias

	If(Used("fn"))
		Delete From FN Where Alltrim(ref)='' And Empty(Alltrim(Design)) And qtt=0
	Endif

	If(Used("bi"))
		Delete From Bi Where Alltrim(ref)='' And Empty(Alltrim(Design)) And qtt=0
	Endif

	If(Used("fi"))
		Delete From fi  Where Alltrim(ref)='' And Empty(Alltrim(Design)) And qtt=0
	Endif



Endfunc


Function uf_documentos_testes


	fecha("ucrsTestes")
	Local lcSQL
	lcSQL = "select ref,stock, epv1,epcpond,epcult,epcusto, usaid, udata from st(nolock) where ref in ('5442520','3809787','8113837','5440987') and site_nr = 1"

	If !uf_gerais_actGrelha("", "ucrsTestes", lcSQL )
		uf_perguntalt_chama("N�o foi possivel validar liga��es do documento.","OK","",16)
		Return .F.
	Endif

	fecha("ucrsTestes")

Endfunc





**	GRAVAR DOCUMENTO
Function uf_documentos_gravar

	Local lcSqlValidaTrfArm, lcParmBoolValidaTrfArm, lcParamConsStockArm, uv_calc, lcValidaLoteVet
	Store "" To lcSqlValidaTrfArm, lcParmBoolValidaTrfArm, lcParamConsStockArm
	** Verificar se existe nova vers�o para reiniciar o software
	uf_chk_nova_versao()


	lcValidaLoteVet = uf_documentos_validaLotesVet()
	If !Empty(lcValidaLoteVet)
		uf_perguntalt_chama("O produto " + Alltrim(lcValidaLoteVet) + " necessita ter o campo Lote preenchido. Por favor valide.", "OK", "", 16)
		Return .F.
	Endif



	If uf_gerais_getParameter_Site('ADM0000000006','BOOL', mySite_nr)
		If !Empty(Alltrim(uf_gerais_getParameter_Site('ADM0000000007', 'text', mySite_nr))) And Dtoc(cabdoc.DATADOC,1)<uf_gerais_getParameter_Site('ADM0000000007', 'text', mySite_nr)
			uf_perguntalt_chama("N�o pode criar um documento com data inferior � data fechada para altera��es/elimina��es.","OK","",64)
			Return .F.
		Endif
	Endif

	If myTipoDoc == "FO"
		Select FN
		Go Top
		Scan
			If !Empty(FN.ref) And Empty(FN.Design) And FN.qtt>0 Then
				uf_perguntalt_chama("N�o pode criar um documento com linhas em que h� refer�ncias sem designa��o. Por favor retifique.","OK","",64)
				Return .F.
			Endif
		Endscan
	Endif



	If myTipoDoc == "BO" And Used("cabDoc")
		If ucrsts.tipodos = 3
			Select Bi
			Go Top
			uv_calc = 0
			Calculate Count() For !Empty(Bi.ref) And (Empty(Bi.ar2mazem) Or Bi.ar2mazem = 0) To uv_calc

			If uv_calc <> 0
				uf_perguntalt_chama("N�o pode criar um documento com linhas com o Arm. Destino por preencher. Por favor retifique.","OK","",64)
				Return .F.
			Endif

			lcParamConsStockArm = uf_gerais_getParameter('ADM0000000366','BOOL')
			If lcParamConsStockArm = .F.
				Select Bi
				Go Top
				Scan
					If (Empty(Bi.ar2mazem) Or Bi.ar2mazem = 0)
						uf_perguntalt_chama("N�o Pode ter Armaz�m Destino vazio ou a 0","OK","",64)
						Return .F.
					Endif
					If (Empty(Bi.ARMAZEM) Or Bi.ARMAZEM = 0)
						uf_perguntalt_chama("N�o Pode ter Armaz�m Origem vazio ou a 0","OK","",64)
						Return .F.
					Endif
				Endscan
			Endif

			lcParmBoolValidaTrfArm = uf_gerais_getParameter('ADM0000000366','BOOL')
			If lcParmBoolValidaTrfArm

				Local lctokenTempFilter, lcValueText, lcFieldToAply, lcSite, lcSplit, lcMaxInsert
				lctokenTempFilter 	= uf_gerais_stamp()
				lcValueText 		= Bi.ref
				lcFieldToAply		= "REFERENCIA"
				lcSite 				= mySite
				lcSplit 			= ','
				lcMaxInsert 		= 200

				Select Bi
				Go Top
				Scan
					uf_gerais_insert_tempFilters(lctokenTempFilter ,Bi.ref, 0, lcFieldToAply, lcSite, lcSplit, lcMaxInsert)
				Endscan

				Select Bi
				Go Top
				TEXT TO lcSqlValidaTrfArm NOSHOW TEXTMERGE
					exec up_trfArm_valida_StockValidade '<<lctokenTempFilter>>','<<bi.armazem>>' ,'<<bi.ar2mazem>>', '<<lcSite>>'
				ENDTEXT

				If !uf_gerais_actGrelha("","",lcSqlValidaTrfArm)
					uf_perguntalt_chama("Ocorreu um erro ao atualizar Stocks/Validades na Trf/Armaz�m ","OK","",16)
					Return .F.
				Endif

			Endif
		Endif
	Endif




	If myTipoDoc == "BO" And Used("cabDoc")

		Select cabdoc
		If cabdoc.NUMINTERNODOC = 3 And Empty(cabdoc.NRRECEITA)
			*uf_perguntalt_chama("N�o pode criar um documento do tipo Encomenda Via Verde sem preencher o n�mero da receita. Por favor retifique.","OK","",64)

			Public myReceitaNr
			Store '' To myReceitaNr

			uf_tecladoalpha_chama("myReceitaNr", "Introduza o Nr. da Receita:", .F., .F., 0)

			If Empty(myReceitaNr) Or (Len(Alltrim(myReceitaNr)) != 13 And Len(Alltrim(myReceitaNr)) != 19)
				uf_perguntalt_chama("O N� DE RECEITA TEM DE TER 13 OU 19 DIGITOS","OK","", 16)

				Release myReceitaNr

				Return .F.
			Else

				Select cabdoc
				Replace cabdoc.NRRECEITA With myReceitaNr

			Endif

		Endif

	Endif

	If uf_gerais_getParameter_Site('ADM0000000197','BOOL')
		If (Alltrim(cabdoc.Doc)='V/Factura' Or Alltrim(cabdoc.Doc)='V/Guia Transp.') And Alltrim(cabdoc.Doc)!='V/Factura Med.'

			Select FN
			Go Top
			Locate For (Empty(uf_gerais_getdate(FN.u_validade,"SQL")) Or uf_gerais_getdate(FN.u_validade,"SQL")  < uf_gerais_getdate(Date(),"SQL")) And  uf_gerais_getdate(FN.u_validact, "SQL") < uf_gerais_getdate(Date(),"SQL") And !Empty(FN.ref) And FN.qtt>0
			If Found()
				If !uf_perguntalt_chama("Aten��o: tem produtos sem validade ou com validade inferior � atual. Pretende continuar?","Sim","N�o",64)
					Return .F.
				Endif
			Endif
		Endif
		If Alltrim(cabdoc.Doc)='V/Factura Med.'
			Select FN
			Go Top
			Locate For uf_gerais_getdate(FN.u_dtval,"SQL") < uf_gerais_getdate(Date(),"SQL") And !Empty(FN.ref) And FN.qtt>0
			If Found()
				If !uf_perguntalt_chama("Aten��o: tem produtos sem validade ou com validade inferior � atual. Pretende continuar?","Sim","N�o",64)
					Return .F.
				Endif
			Endif
		Endif
	Endif


	Public myDocAGravar
	Store .T. To myDocAGravar && por causa do evento que chama o painel de pesquisa de stocks

	Local lcvalidaCamposObrigatorios, lcValidaRegrasDoc, lcValidaCompras, lcValidaInsercaoDocumento
	Store .F. To lcvalidaCamposObrigatorios, lcValidaRegrasDoc, lcValidaCompras, lcValidaInsercaoDocumento

	&& Set focus para garantir act dos das ultimas altera��es
	DOCUMENTOS.PageFrame1.Page1.ContainerLinhas.Total.SetFocus

	&&Remove linhas a branco
	uf_documentos_removeLinhasVazias()

	&& Verifica Campos Obrigat�rios na grava��o do documento e valida��es mediante tipo de documento
	lcvalidaCamposObrigatorios = uf_documentos_validaCamposObrigatorios()

	If lcvalidaCamposObrigatorios == .F.
		Return .F.	&& A mensagem ao utilizador � dada na fun��o anterior
	Endif

	&& Calcula Contador
	lcdoccont = uf_documentos_contadorTipoDocumento()

	If myTipoDoc == "BO"
		If cabdoc.NUMINTERNODOC == 46 &&Conversao Unidades
			uf_documentos_ConversaoUnidades()
		Endif
	Endif


	If Alltrim(DOCUMENTOS.PageFrame1.Page1.BtnEsgot.Label1.Caption)=='Mostrar Esgotados'
		If Used("bi")
			Select Bi
			Set Filter To
		Endif
		If Used("fn")
			Select FN
			Set Filter To
		Endif
	Endif
	DOCUMENTOS.PageFrame1.Page1.gridDoc.Refresh
	DOCUMENTOS.Refresh



	&& INTRODU��O DE DOCUMENTOS
	If myDocIntroducao == .T.

		&& DOSSIER INTERNO
		If myTipoDoc == "BO"

			Local lcSqlCert
			Store '' To  lcSqlCert


			lcValidaRegrasDoc = uf_documentos_validacoesEnc()

			If lcValidaRegrasDoc == .F.
				Return .F.
			Endif

			If !uf_documentos_verificaCaixas()
				Return .F.
			Endif

			Set Hours To 24
			Atime=Ttoc(Datetime()+(difhoraria*3600),2)
			ASTR=Left(Atime,8)

			Local lcHora
			lcHora = ASTR
			**lcHora = TIME()

			regua(0,100,"A PROCESSAR A GRAVA��O DO DOCUMENTO...",.T.)

			&& Cabe�alho
			lcSqlInsercaoCabBo = uf_documentos_insereCabBO(lcHora, lcdoccont)
			lcSqlInsercaoCabBo = uf_gerais_trataPlicasSQL(lcSqlInsercaoCabBo)

			&& Linhas
			lcSqlInsercaoBi = uf_documentos_actualizaLinhasBI()
			lcSqlInsercaoBi = uf_gerais_trataPlicasSQL(lcSqlInsercaoBi)



			&&certifica documento
			&&lcSqlCert = uf_pagamento_gravarCert(ALLTRIM(CabDoc.Cabstamp), uf_gerais_getDate(CabDoc.datadoc,"SQL"), lcHora, CabDoc.numInternoDoc, ROUND(cabdoc.total,2),'BO',0)
			&&lcSqlCert = uf_gerais_trataPlicasSQL(lcSqlCert)

			If !Empty(lcSqlInsercaoCabBo)

				lcSQL = ''
				TEXT TO lcSQL NOSHOW TEXTMERGE
					exec up_gerais_execSql 'Documentos - Insert', 1,'<<lcSqlInsercaoCabBo>>', '<<lcSqlInsercaoBi>>', '', '' , '', ''
				ENDTEXT

				If !uf_gerais_actGrelha("","",lcSQL)
					uf_perguntalt_chama("OCORREU UMA ANOMALIA AO GRAVAR O CABE�ALHO DO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE."+Chr(10)+"Cod: DOCUMENTOS","OK","",16)

					&& Erro na Gravacao
					lcValidaInsercaoDocumento = .F.

				Else
					&& Tudo Ok
					lcValidaInsercaoDocumento = .T.
				Endif

			Endif



			If lcValidaInsercaoDocumento == .T. && Tudo Ok, insere o Documento na BD

				&& gravar certifica��o se for Devolu��o a Fornecedor - ndos = 17
				Select cabdoc
				If ucrsts.tipodos = 2 &&CabDoc.numinternodoc == 17
					uf_gravarCert(Alltrim(cabdoc.cabstamp), Val(cabdoc.NUMDOC), cabdoc.NUMINTERNODOC, uf_gerais_getdate(cabdoc.DATADOC,"SQL"), uf_gerais_getdate(cabdoc.DATADOC,"SQL"), lcHora, cabdoc.Total, 'BO')
				Endif
				**

				&& actualiza ultimo registo
				Select cabdoc
				uf_gerais_gravaUltRegisto('bo', Alltrim(cabdoc.cabstamp))

				&& tabela Precos, Documentos Condi��es Comerciais
				Select cabdoc
				If cabdoc.NUMINTERNODOC == 40
					uf_documentos_actualizaTabelaPrecos()
				Endif

				regua(2)

			Else && Erro regista problema na tabela b_elog e sai da funcao

				Select cabdoc
				Local lcmensagem, lcorigem
				lcmensagem 	= "Erro Inser��o tabela BO:" + Alltrim(lcSqlInsercaoCabBo) + "Erro Inser��o na tabela BI:" + Alltrim(lcSqlInsercaoBi)
				lcorigem 	= "P. Documentos: uf_gravarDocumento BO"

				lcSQL = ""
				TEXT TO lcSQL TEXTMERGE NOSHOW
					Insert Into B_elog (tipo, status, mensagem, origem)
					Values 	('E','A','<<Alltrim(lcmensagem)>>', '<<Alltrim(lcorigem)>>')
				ENDTEXT

				uf_gerais_actGrelha("", "", lcSQL)

				regua(2)

				Return .F.
			Endif
		Endif

		&& DOCUMENTOS COMPRAS
		If myTipoDoc == "FO"

			lcValidaCompras = uf_documentos_ValidacoesCompras()
			If lcValidaCompras == .F.
				Return .F.
			Endif


			If !uf_documentos_verificaCaixas()
				Return .F.
			Endif




			regua(0,100,"A PROCESSAR A GRAVA��O DO DOCUMENTO...",.T.)

			Set Hours To 24
			Atime=Ttoc(Datetime()+(difhoraria*3600),2)
			ASTR=Left(Atime,8)

			Local lcHora
			lcHora = ASTR
			**lcHora = TIME()

			&& Cabe�alho
			lcSqlInsercaoCabFo = uf_documentos_insereCabFO(lcdoccont)
			lcSqlInsercaoCabFo = uf_gerais_trataPlicasSQL(lcSqlInsercaoCabFo)

			&& Linha
			lcSqlInsercaoFN = uf_documentos_actualizaLinhasFN()
			lcSqlInsercaoFN = uf_gerais_trataPlicasSQL(lcSqlInsercaoFN)

			&&_cliptext= lcSqlInsercaoCabFo + CHR(13)	+ lcSqlInsercaoFN
			If !Empty(lcSqlInsercaoCabFo)
				lcSQL = ''
				**_cliptext=lcSqlInsercaoCabFo + CHR(13) + lcSqlInsercaoFN
				TEXT TO lcSQL NOSHOW TEXTMERGE
					exec up_gerais_execSql 'Documentos - Insert', 1, '<<lcSqlInsercaoCabFo>>', '<<lcSqlInsercaoFN>>', '', '', '', ''
				ENDTEXT

				If !uf_gerais_actGrelha("","",lcSQL)
					uf_perguntalt_chama("OCORREU UMA ANOMALIA AO GRAVAR O CABE�ALHO DO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE."+Chr(10)+"Cod: DOC-DOC COMPRAS","OK","",16)



					&& Erro na Grava��o
					lcValidaInsercaoDocumento = .F.
				Else
					&& Tudo Ok
					lcValidaInsercaoDocumento = .T.
				Endif

			Endif



			If lcValidaInsercaoDocumento == .T. && Tudo Ok, inseriu corretamente o Documento na BD

				Select cabdoc

				&& actualiza ultimo registo
				uf_gerais_gravaUltRegisto('FO', Alltrim(cabdoc.cabstamp))

				If Type("DOCUMENTOS.MyTimerCenc") = "O"

					DOCUMENTOS.MyTimerCenc.Enabled = .F.

					If uCrsRecRoboPend.ori = 1
						uf_gerais_actGrelha("", "", "update B_movtec_robot set conferido = 1 where cast(ltrim(rtrim(Delivery_Number)) as varchar(15)) = '" + Alltrim(uCrsRecRoboPend.nrConf) + "'")
					Else
						uf_gerais_actGrelha("", "", "update robot_response_payload set conferido = 1 where cast(ltrim(rtrim(DeliveryNumber)) as varchar(15)) = '" + Alltrim(uCrsRecRoboPend.nrConf) + "'")
					Endif

				Endif

				regua(2)
			Else && Regista Problema na Tabela de Logs e sai da funcao

				Select cabdoc
				Local lcmensagem, lcorigem
				lcmensagem 	= "Erro Inser��o tabela FO:" + Alltrim(lcSqlInsercaoCabFo) + "Erro Inser��o na tabela FN:" + Alltrim(lcSqlInsercaoFN)
				lcorigem 	= "P. Documentos: uf_gravarDocumento FO"

				lcSQL = ""
				TEXT TO lcSQL TEXTMERGE NOSHOW
					Insert Into B_elog (tipo, status, mensagem, origem)
					Values 	('E','A','<<Alltrim(lcmensagem)>>', '<<Alltrim(lcorigem)>>')
				ENDTEXT

				uf_gerais_actGrelha("", "", lcSQL)

				regua(2)

				Return .F.
			Endif
		Endif
	Endif


	&& ALTERA��O DE DOCUMENTOS
	If myDocAlteracao = .T.

		&& DOSSIER INTERNOS
		If myTipoDoc = "BO"

			lcValidaRegrasDoc = uf_documentos_validacoesEnc()

			If lcValidaRegrasDoc == .F.
				Return .F.
			Endif


			If !uf_documentos_verificaCaixas()
				Return .F.
			Endif

			regua(0,100,"A PROCESSAR A GRAVA��O DO DOCUMENTO...",.T.)

			If uf_gerais_getParameter_Site('ADM0000000199', 'BOOL', mySite) And Used("ucrsts") And ucrsts.tipodos = 5

				TEXT TO msel TEXTMERGE NOSHOW
					SELECT
						convert(varchar,dateadd(HOUR,<<difhoraria>>, getdate()),102) as usrdata,
						convert(varchar,dateadd(HOUR,<<difhoraria>>, getdate()),8) as usrhora
				ENDTEXT

				If !uf_gerais_actGrelha("", "uc_dataAlter", msel)
					uf_perguntalt_chama("Erro a atualizar o documento. Por favor contacte o suporte.","OK","", 16)
					Return .F.
				Endif

				Select uc_dataAlter
				&& Cabe�alho
				lcSqlInsercaoCabBo = uf_documentos_actualizaCabBO('','','', uc_dataAlter.usrdata, uc_dataAlter.usrhora)
				lcSqlInsercaoCabBo = uf_gerais_trataPlicasSQL(lcSqlInsercaoCabBo)

			Else

				&& Cabe�alho
				lcSqlInsercaoCabBo = uf_documentos_actualizaCabBO()
				lcSqlInsercaoCabBo = uf_gerais_trataPlicasSQL(lcSqlInsercaoCabBo)

			Endif

			&& Linhas
			lcSqlInsercaoBi = uf_documentos_actualizaLinhasBI()
			lcSqlInsercaoBi = uf_gerais_trataPlicasSQL(lcSqlInsercaoBi)



			If uf_gerais_getParameter_Site('ADM0000000199', 'BOOL', mySite) And Used("ucrsts") And ucrsts.tipodos = 5

				Create Cursor uc_updateStatusEnc (Status c(50), pagamento c(50), modo_envio c(50), usrdata D, usrhora c(8), usrinis c(30), FECHADA L)

				Select cabdoc

				Select uc_updateStatusEnc
				Append Blank

				Replace uc_updateStatusEnc.Status With Alltrim(cabdoc.estadoenc),;
					uc_updateStatusEnc.pagamento With Alltrim(cabdoc.pagamento),;
					uc_updateStatusEnc.modo_envio With Alltrim(cabdoc.entrega),;
					uc_updateStatusEnc.usrinis With Alltrim(m_chinis),;
					uc_updateStatusEnc.FECHADA With Iif(uf_gerais_compstr(cabdoc.ESTADODOC, "A"), .F., .T.)

				If Used("uc_dataAlter")

					Select uc_dataAlter

					Select uc_updateStatusEnc
					Replace uc_updateStatusEnc.usrdata With Ctod(uc_dataAlter.usrdata)
					Replace uc_updateStatusEnc.usrhora With uc_dataAlter.usrhora

				Else
					Select uc_updateStatusEnc
					Replace uc_updateStatusEnc.usrdata With uf_gerais_getdate(Date())
					Replace uc_updateStatusEnc.usrhora With Time()
				Endif


				If !uf_documentos_updateEncCentral(cabdoc.cabstamp, "uc_updateStatusEnc")
					regua(2)
					uf_perguntalt_chama("N�o foi poss�vel atualizar a central. Por favor volte a tentar mais tarde.","OK","", 16)
					Return .F.
				Endif

			Endif


			&& Processa Grava��o do Documento
			If !Empty(lcSqlInsercaoCabBo)

				lcSQL = ''
				TEXT TO lcSQL NOSHOW TEXTMERGE
					exec up_gerais_execSql 'Documentos - Insert', 1, '<<lcSqlInsercaoCabBo>>', '<<lcSqlInsercaoBi>>', '', '', '', ''
				ENDTEXT

				If !uf_gerais_actGrelha("","",lcSQL)
					uf_perguntalt_chama("OCORREU UMA ANOMALIA AO GRAVAR O CABE�ALHO DO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE."+Chr(10)+"Cod: DOC-DO INTER.","OK","",16)

					&& Erro ao Gravar
					lcValidaInsercaoDocumento = .F.
				Else
					&& Tudo Ok
					lcValidaInsercaoDocumento = .T.
				Endif

			Endif





			*!*				IF(lcValidaInsercaoDocumento and ALLTRIM(UPPER(mypaisconfsoftw))=='ANGOLA')
			*!*					IF(uf_gerais_validaExisteMecosycn())
			*!*						&& Cabe�alho
			*!*						LOCAL lcSqlInsercaoCabBoSync, lcSqlInsercaoBiSync
			*!*						STORE '' TO lcSqlInsercaoCabBoSync, lcSqlInsercaoBiSync

			*!*						lcSqlInsercaoCabBoSync= uf_documentos_actualizaCabBO('mecosync.dbo.bo', 'mecosync.dbo.bo2')
			*!*						lcSqlInsercaoCabBoSync= uf_gerais_trataPlicasSQL(lcSqlInsercaoCabBoSync)

			*!*						&& Linhas
			*!*						lcSqlInsercaoBiSync = uf_documentos_actualizaLinhasBI('mecosync.dbo.bi', 'mecosync.dbo.bi2')
			*!*						lcSqlInsercaoBiSync = uf_gerais_trataPlicasSQL(lcSqlInsercaoBiSync )

			*!*					ENDIF
			*!*				ENDIF
			*!*


			&&
			If lcValidaInsercaoDocumento == .T. && Tudo Ok, inseriu o Documento na BD

				** actualiza ultimo registo **
				Select cabdoc
				uf_gerais_gravaUltRegisto('bo', Alltrim(cabdoc.cabstamp))

				regua(2)

			Else && Regista Problema na Tabela de Logs

				Select cabdoc
				Local lcmensagem, lcorigem
				lcmensagem 	= "Erro Atualizacao tabela BO:" + Alltrim(lcSqlInsercaoCabBo) + "Erro Atualizacao tabela BI:" + Alltrim(lcSqlInsercaoBi)
				lcorigem 	= "P. Documentos: uf_gravarDocumento BO"

				lcSQL = ""
				TEXT TO lcSQL TEXTMERGE NOSHOW
					Insert Into B_elog (tipo, status, mensagem, origem)
					Values 	('E','A','<<Alltrim(lcmensagem)>>', '<<Alltrim(lcorigem)>>')
				ENDTEXT

				uf_gerais_actGrelha("", "", lcSQL)

				regua(2)

				Return .F.
			Endif
		Endif


		* DOCUMENTOS
		If myTipoDoc = "FO"

			&& Vali��es Adicionais, n�o tem impacto na perfomance
			lcValidaCompras = uf_documentos_ValidacoesCompras()
			If lcValidaCompras == .F.
				Return .F.
			Endif

			If !uf_documentos_verificaCaixas()
				Return .F.
			Endif

			regua(0,100,"A PROCESSAR A GRAVA��O DO DOCUMENTO...",.T.)

			&& Cabe�alho
			lcSqlInsercaoCabFo = uf_documentos_actualizaCabFO()
			lcSqlInsercaoCabFo = uf_gerais_trataPlicasSQL(lcSqlInsercaoCabFo)

			&& Linhas
			lcSqlInsercaoFN = uf_documentos_actualizaLinhasFN()
			lcSqlInsercaoFN = uf_gerais_trataPlicasSQL(lcSqlInsercaoFN)


			** _cliptext= lcSqlInsercaoCabFo + CHR(13)	+ lcSqlInsercaoFN
			If !Empty(lcSqlInsercaoCabFo)
				&&_cliptext=lcSqlInsercaoCabFo + CHR(13) + lcSqlInsercaoFN
				lcSQL = ''
				TEXT TO lcSQL NOSHOW TEXTMERGE
					exec up_gerais_execSql 'Documentos - Insert', 1, '<<lcSqlInsercaoCabFo>>', '<<lcSqlInsercaoFN>>', '', '', '', ''
				ENDTEXT
				If !uf_gerais_actGrelha("","",lcSQL)
					uf_perguntalt_chama("OCORREU UMA ANOMALIA A GRAVAR O CABE�ALHO DO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE."+Chr(10)+"Cod: DOC-DOC COMPRAS","OK","",16)

					lcValidaInsercaoDocumento = .F.
				Else
					&& Tudo Ok
					lcValidaInsercaoDocumento = .T.
				Endif

			Endif


			If lcValidaInsercaoDocumento == .T. && Tudo Ok, insere o Documento na BD

				** actualiza ultimo registo **
				Select cabdoc
				uf_gerais_gravaUltRegisto('FO', Alltrim(cabdoc.cabstamp))

				regua(2)

			Else

				** Regista Problema na Tabela de Logs
				Select cabdoc
				Local lcmensagem, lcorigem
				lcmensagem 	= "Erro Atualizacao tabela FO; Doc: " + Alltrim(lcSqlInsercaoCabFo) + "Erro Atualizacao tabela FN:" + Alltrim(lcSqlInsercaoFN)
				lcorigem 	= "P. Documentos: uf_gravarDocumento FO"

				lcSQL = ""
				TEXT TO lcSQL TEXTMERGE NOSHOW
					Insert Into B_elog (tipo, status, mensagem, origem)
					Values 	('E','A','<<Alltrim(lcmensagem)>>', '<<Alltrim(lcorigem)>>')
				ENDTEXT

				uf_gerais_actGrelha("", "", lcSQL)

				regua(2)

				Return .F.
			Endif
		Endif
	Endif



	&& fecho de documentos associados - LL 20151123
	If myTipoDoc = "FO"
		uf_fechoautomaticodoc_chama('FO')
	Else
		uf_fechoautomaticodoc_chama('BO')
	Endif

	&& Calculos adicionais que ocorrem apo�s a grava��o do documento
	uf_documentos_CalculaArredondamentos()

	regua(0,100,"A VERIFICAR PCP'S DOS ARTIGOS.",.T.)



	If myDocAlteracao = .T. And myTipoDoc = "FO"
		uf_Corrige_mov_compra_pcp_artigo()
	Endif

	regua(0,100,"A EFETUAR VERIFICA��ES ADICIONAIS.",.T.)


	** verificar quantidades encomendadas pendentes (clientes e fornecedores)
	If myTipoDoc == "BO"
		If Alltrim(cabdoc.Doc)='Encomenda a Fornecedor'
			Select Bi
			Go Top
			Scan
				If !Empty(Bi.ref) And Bi.qtt>0
					lcSQL = ""
					TEXT TO lcSQL TEXTMERGE NOSHOW
						exec up_stocks_Corrige_enc_pend_fl <<bi.armazem>>, '<<ALLTRIM(bi.ref)>>'
					ENDTEXT
					regua(2)
					uf_gerais_actGrelha("", "", lcSQL)
				Endif
				Select Bi
			Endscan
		Endif
		If Alltrim(cabdoc.Doc)='Encomenda de Cliente'
			Select Bi
			Go Top
			Scan
				If !Empty(Bi.ref) And Bi.qtt>0
					lcSQL = ""
					TEXT TO lcSQL TEXTMERGE NOSHOW
						exec up_stocks_Corrige_enc_pend_cl <<bi.armazem>>, '<<ALLTRIM(bi.ref)>>'
					ENDTEXT
					regua(2)
					uf_gerais_actGrelha("", "", lcSQL)
				Endif
				Select Bi
			Endscan
		Endif
	Endif
	If myTipoDoc == "FO"
		If Alltrim(cabdoc.Doc)='V/Factura' Or Alltrim(cabdoc.Doc)='V/Factura' Or Alltrim(cabdoc.Doc)='V/Guia Transp.'
			Select FN
			Go Top
			Scan
				If !Empty(FN.ref) And FN.qtt>0 And !Empty(FN.bistamp)
					lcSQL = ""
					TEXT TO lcSQL TEXTMERGE NOSHOW
						exec up_stocks_Corrige_enc_pend_fl <<fn.armazem>>, '<<ALLTRIM(fn.ref)>>'
					ENDTEXT
					regua(2)
					uf_gerais_actGrelha("", "", lcSQL)
				Endif
				Select FN
			Endscan
		Endif
	Endif

	myDocIntroducao	= .F.
	myDocAlteracao	= .F.
	uf_documentosControlaObj()

	**APAGA	TABELA TEMP
	lcSQL = ''
	TEXT TO lcSql TEXTMERGE NOSHOW
		DELETE FROM temp_bi WHERE terminal = <<mytermno>>
		DELETE FROM temp_bi2 WHERE terminal = <<mytermno>>

		DELETE FROM temp_fn WHERE terminal = <<mytermno>>
	ENDTEXT
	If !uf_gerais_actGrelha("", "ucrsTemp", lcSQL)
		uf_perguntalt_chama("N�O FOI POSS�VEL VERIFICAR A LISTAGEM DE CENTROS DE CUSTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		Return .F.
	Endif



	uf_documentos_eventosAposGravar()



	regua(2)

	If uf_gerais_getParameter_Site('ADM0000000009','BOOL') And myTipoDoc = "FO"
		uf_Corrige_mov_compra()
	Endif



	**
	If myDocIntroducao	== .F. And myDocAlteracao == .F.
		uf_documentos_actualizaDoc()
	Endif


Endfunc


**
Function uf_documentos_enviaDT

	Select cabdoc
	If ucrsts.comunicaAT And Upper(Alltrim(ucrse1.PAIS)) == 'PORTUGAL' && Devolu��es // devolu��es VV // Tranf Entre Armazens

		If !uf_perguntalt_chama("Pretende fazer a comunica��o de transporte � Autoridade Tribut�ria e Aduaneira (AT)?","Sim","N�o",64)
			Return .F.
		Endif

		If Empty(ucrse1.UtilizadorDT) Or Empty(ucrse1.PasswordDT)
			uf_perguntalt_chama("Dados de configura��o para envio de Documentos de Transporte n�o definidos na ficha da Empresa. Por favor verifique.","OK","",32)
			Return .F.
		Endif

		&& Valida introdu��o do n� de Receita para devolu��es do tipo Via Verde
		If ucrsts.viaVerde &&cabdoc.numInternoDoc == 16
			Public myReceitaNr
			Store '' To myReceitaNr

			uf_tecladoalpha_chama("myReceitaNr", "Introduza o Nr. da Receita:", .F., .F., 0)

			If Empty(myReceitaNr) Or !uf_gerais_checkNumReceita(myReceitaNr) &&(LEN(ALLTRIM(myReceitaNr)) != 13 AND LEN(ALLTRIM(myReceitaNr)) != 19)
				**uf_perguntalt_chama("O N� DE RECEITA TEM DE TER 13 OU 19 DIGITOS","OK","", 16)

				uf_gerais_msgErroReceita()

				Release myReceitaNr

				Return .F.
			Else
				If !Empty(cabdoc.NRRECEITA)
					If cabdoc.NRRECEITA != myReceitaNr
						If (uf_perguntalt_chama("O n�mero que inseriu � diferente do n�mero de receita que inseriu anteriormente."+ Chr(13) + "Pretende continuar? " ,"Sim","N�o"))
							uf_documentos_changeNrReceita(myReceitaNr)
						Else
							Release myReceitaNr
							Return .F.
						Endif
					Endif
				Else
					uf_documentos_changeNrReceita(myReceitaNr)
				Endif
			Endif
		Endif

		If Empty(Alltrim(cabdoc.ATDocCode))
			regua(0,15,"A enviar Documento para a AT...")
			regua(1,1,"A enviar Documento para a AT...")

			*internet
			If !uf_gerais_verifyInternet()
				uf_perguntalt_chama("ESTA FUNCIONALIDADE REQUER LIGA��O � INTERNET."+Chr(13)+"Por favor valide.","OK","",64)
				regua(2)
				Return .F.
			Endif
			**

			regua(1,2,"A enviar Documento para a AT...")
			DOCUMENTOS.stampTmrDT = uf_gerais_stamp()

			Select cabdoc
			lcSQL = ''
			TEXT TO lcSQL TEXTMERGE NOSHOW
				INSERT INTO b_DT_resposta
				(
					stamp
					,stampDoc
				)
				values
				(
					'<<ALLTRIM(documentos.stampTmrDT)>>'
					,'<<ALLTRIM(cabDoc.cabstamp)>>'
				)
			ENDTEXT
			If !uf_gerais_actGrelha("","",lcSQL)
				regua(2)
			Endif
			regua(1,3,"A enviar Documento para a AT...")
			If Directory(Alltrim(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\DT')
				Select cabdoc
				**lcWsPath = ALLTRIM(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\DT\DT "' + ALLTRIM(documentos.stampTmrDT) + '" "' + ALLTRIM(sql_db) + '" "' + ALLTRIM(cabDoc.cabstamp) + '" "' + ALLTRIM(mySite) + '"'
				lcWsPath = Alltrim(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\DT\ATDocTransporte.jar "' + Alltrim(DOCUMENTOS.stampTmrDT) + '" "' + Alltrim(sql_db) + '" "' + Alltrim(cabdoc.cabstamp) + '" "' + Alltrim(mySite) + '"'

				** messagebox(lcWsPath)
				oWSShell = Createobject("WScript.Shell")
				oWSShell.Run(lcWsPath,1,.F.)
				regua(1,4,"A enviar Documento para a AT...")
				DOCUMENTOS.tmrDT.Enabled = .T.
			Endif
		Endif
	Else
		uf_perguntalt_chama("Esta funcionalidade s� est� dispon�vel para documentos do tipo Devolu��es a Fornecedor ou Trf. Entre Armaz�ns.","OK","",16)
		Return .F.
	Endif

	&& inserir dados nas tabelas via_verde_med; via_verde_med_d
	If ucrsts.viaVerde &&cabdoc.numInternoDoc == 16
		uf_documento_infoDevViaVerde()
	Endif

	Release myReceitaNr

Endfunc


**
Function uf_documentos_tmrDT
	DOCUMENTOS.ntmrDT = DOCUMENTOS.ntmrDT + 1
	If DOCUMENTOS.ntmrDT > 10 && 10 tentativas de 3 segundos
		regua(2)
		uf_perguntalt_chama("N�o foi poss�vel obter resposta do WebService. Por favor volte a tentar mais tarde.","OK","",16)
		DOCUMENTOS.tmrDT.Enabled = .F.
		DOCUMENTOS.ntmrDT = 0
		DOCUMENTOS.stampTmrDT = ''
		Return .F.
	Endif
	regua(1,DOCUMENTOS.ntmrDT + 2,"A enviar Documento para a AT...")
	TEXT TO lcSql NOSHOW textmerge
		SELECT
			codResposta
			,descrResposta
			,ATDocCode
			,DataEnvio
		from
			b_DT_resposta (nolock)
		where
			stamp = '<<ALLTRIM(documentos.stampTmrDT)>>'
	ENDTEXT
	If !uf_gerais_actGrelha("","uCrsVerificaResposta",lcSQL)
		regua(2)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VERIFICAR A RESPOSTA DO WEB SERVICE. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		DOCUMENTOS.tmrDT.Enabled = .F.
		DOCUMENTOS.ntmrDT = 0
		DOCUMENTOS.stampTmrDT = ''
		Return .F.
	Else

		Select uCrsVerificaResposta
		If Reccount("uCrsVerificaResposta") > 0 And !Empty(Alltrim(uCrsVerificaResposta.CodResposta))
			Select uCrsVerificaResposta
			Go Top

			*Verifica se n�o existe codigo de anomalia
			If Alltrim(uCrsVerificaResposta.CodResposta) != "0" And Alltrim(uCrsVerificaResposta.CodResposta) != "-100"
				regua(2)
				uf_perguntalt_chama("N�o foi possivel enviar o documento." + Chr(13) + "Motivo: "+ Upper(Alltrim(uCrsVerificaResposta.DescrResposta)),"OK","",64)

				DOCUMENTOS.tmrDT.Enabled = .F.
				DOCUMENTOS.ntmrDT = 0
				DOCUMENTOS.stampTmrDT = ''
				Return .F.
			Endif

			If Alltrim(uCrsVerificaResposta.CodResposta) == "-100"
				regua(2)
				uf_perguntalt_chama(Upper(Alltrim(uCrsVerificaResposta.DescrResposta)),"OK","",64)

				Select cabdoc
				lcSQL=''
				TEXT TO lcSQL TEXTMERGE NOSHOW
					IF exists (select bo2.bo2stamp from bo2 where bo2.bo2stamp = '<<ALLTRIM(cabdoc.cabstamp)>>' AND bo2.ATDocCode = '')
					BEGIN
						UPDATE
							bo2
						SET
							ATDocCode 	= '-100'
							,xpddata 	= '<<uf_gerais_getDate(uCrsVerificaResposta.DataEnvio,"SQL")>>'
							,xpdhora	= '<<LEFT(TIME(uCrsVerificaResposta.DataEnvio),5)>>'
						WHERE
							bo2.bo2stamp = '<<ALLTRIM(cabdoc.cabstamp)>>'
					END
				ENDTEXT
				uf_gerais_actGrelha("","",lcSQL)

				Select cabdoc
				Replace cabdoc.ATDocCode With '-100'

				DOCUMENTOS.tmrDT.Enabled = .F.
				DOCUMENTOS.ntmrDT = 0
				DOCUMENTOS.stampTmrDT = ''
				Return .F.
			Endif

			Select uCrsVerificaResposta
			Select cabdoc
			lcSQL=''
			TEXT TO lcSQL TEXTMERGE NOSHOW
				IF exists (select bo2.bo2stamp from bo2 where bo2.bo2stamp = '<<ALLTRIM(cabdoc.cabstamp)>>' AND bo2.ATDocCode = '')
				BEGIN
					UPDATE
						bo2
					SET
						ATDocCode	= '<<ALLTRIM(uCrsVerificaResposta.ATDocCode)>>'
						,xpddata 	= '<<uf_gerais_getDate(uCrsVerificaResposta.DataEnvio,"SQL")>>'
						,xpdhora	= '<<LEFT(TIME(uCrsVerificaResposta.DataEnvio),5)>>'
					WHERE
						 bo2.bo2stamp = '<<ALLTRIM(cabdoc.cabstamp)>>'

					UPDATE bo SET logi1=1 WHERE bo.bostamp = '<<ALLTRIM(cabdoc.cabstamp)>>'
				END
			ENDTEXT
			uf_gerais_actGrelha("","",lcSQL)

			Select cabdoc
			Replace cabdoc.ATDocCode With Alltrim(uCrsVerificaResposta.ATDocCode)

			regua(2)
			uf_perguntalt_chama("DOCUMENTO ENVIADO COM SUCESSO.","OK","",64)
			DOCUMENTOS.tmrDT.Enabled = .F.
			DOCUMENTOS.ntmrDT = 0
			DOCUMENTOS.stampTmrDT = ''
		Endif
	Endif
Endfunc




Function uf_documentos_Reservas

	uf_reservas_cria('LISTA',.T.,'documentos')

Endfunc






Function uf_documentos_preparaEnvioSMSReserva


	**IF uf_gerais_getParameter("ADM0000000143","BOOL")&&usa SMS - alterado a 20190228 para os parametros da empresa
	If uf_gerais_getParameter_Site("ADM0000000022","BOOL", mySite)&&usa SMS

		uf_gerais_actGrelha("","uCrsTempResActivo","select activo,texto from b_res_notificacoes (nolock)")

		Select uCrsTempResActivo



		If uCrsTempResActivo.activo

			If Len(Alltrim(uCrsTempResActivo.texto))>0

				Create Cursor uCrsCliSMSEnviar(;
					sel L;
					,NO N(10), ESTAB N(3),tlmvl c(45),clstamp c(25), Nome c(55);
					,bistamp c(25),BOSTAMP c(25);
					,OBRANO N(10), DATAOBRA D;
					,Design c(60), ref c(18);
					)

				Insert Into uCrsCliSMSEnviar(;
					sel;
					,NO,ESTAB,tlmvl,Nome,clstamp;
					,bistamp,BOSTAMP;
					,OBRANO,DATAOBRA;
					,Design,ref);
					Select;
					sel;
					,NO,ESTAB,tlmvl,nomeCliente,clstamp;
					,bistamp,BOSTAMP;
					,OBRANO,DATAOBRA;
					,Design ,ref;
					From uCrsListaReservas;
					WHERE sel=.T.;


				If Used("uCrsCliSMSEnviar")
					Select uCrsCliSMSEnviar
					If Reccount("uCrsCliSMSEnviar") > 0
						Select uCrsCliSMSEnviar
						Go Top
						uf_reservaclsms_chama()
					Endif
				Endif
			Else
				uf_perguntalt_chama("N�o tem a SMS de reserva configurada, por favor contacte o suporte.","OK","",64)
			Endif


		Else
			uf_perguntalt_chama("N�o tem a SMS de reserva activa, por favor contacte o suporte.","OK","",64)
		Endif


	Else
		uf_perguntalt_chama("N�o tem o m�dulo de SMS configurado, por favor contacte o suporte.","OK","",64)
	Endif



Endfunc




**
Function uf_documentos_verificaEnvioSMSReserva

	&&correr nas v/facturas, n/guias de entrada, v/guias de transporte, v/ vd. a dinheiro
	**IF uf_gerais_getParameter("ADM0000000143","BOOL")&&usa SMS - alterado a 20190228 para os parametros da empresa
	If uf_gerais_getParameter_Site("ADM0000000022","BOOL", mySite)&&usa SMS
		&&verificar se tem sms para reservas activo
		uf_gerais_actGrelha("","uCrsTempResActivo","select activo from b_res_notificacoes (nolock)")
		Select uCrsTempResActivo
		If uCrsTempResActivo.activo &&texto para reservas configurado
			Create Cursor uCrsCliSMSEnviar(sel L, NO N(10), ESTAB N(3), tlmvl c(45), Nome c(55), bistamp c(25), OBRANO N(10), DATAOBRA D, Design c(60), ref c(18), BOSTAMP c(25))
			Select FN
			Locate For u_reserva == .T.
			If Found()
				If uf_perguntalt_chama("Pretende proceder ao envio de SMS para alertar os clientes?","Sim","N�o",64)
					Local lcContQtt
					Store 0 To lcContQtt
					Select FN
					Scan For !Empty(FN.u_reserva)
						Store 0 To lcContQtt

						If !Empty(Alltrim(FN.bistamp))
							TEXT TO lcSQL TEXTMERGE NOSHOW
								SELECT bi.ndos, bo.no, bo.estab, bi.obistamp, bo.nome, bi.bistamp, bi.obrano, bi.dataobra, bi.design, bi.ref, qtt = (bi.qtt - bi.qtt2), bo.bostamp
								FROM bi (nolock)
								inner join bo (nolock) on bo.bostamp = bi.bostamp
								where
									bi.bistamp='<<ALLTRIM(fn.bistamp)>>'
									and bi.bistamp not in (select b_res_enviar.bistamp FROM  b_res_enviar (nolock))
							ENDTEXT
						Else
							TEXT TO lcSQL TEXTMERGE NOSHOW
								SELECT
									bi.ndos, bo.no, bo.estab, bi.obistamp, bo.nome, bi.bistamp, bi.obrano, bi.dataobra, bi.design, bi.ref, qtt = (bi.qtt - bi.qtt2), bo.bostamp
								FROM bi (nolock)
								inner join bo (nolock) on bo.bostamp = bi.bostamp
								where
									bo.fechada = 0
									and bo.ndos = 5 /*reserva*/
									and bi.ref='<<ALLTRIM(fn.ref)>>'
									and bi.bistamp not in (select b_res_enviar.bistamp FROM  b_res_enviar (nolock))
								order by
									dataobra asc, obrano asc
							ENDTEXT
						Endif
						uf_gerais_actGrelha("","uCrsOriRes",lcSQL)
						Select uCrsOriRes
						If uCrsOriRes.ndos == 2 &&encontrou encomenda - tem que navegar para a reserva
							TEXT TO lcSQL TEXTMERGE noshow
								SELECT bi.ndos, bo.no, bo.estab, bo.nome, bi.bistamp, bi.obrano, bi.dataobra, bi.design, bi.ref, qtt = (bi.qtt - bi.qtt2), bo.bostamp
								FROM bi (nolock)
								inner join bo (nolock) on bo.bostamp = bi.bostamp
								where
									bi.bistamp='<<ALLTRIM(uCrsOriRes.obistamp)>>' and bo.ndos = 5
									and bi.bistamp not in (select b_res_enviar.bistamp FROM  b_res_enviar (nolock))
							ENDTEXT
							uf_gerais_actGrelha("","uCrsOriRes",lcSQL)

							Select uCrsOriRes
							If Reccount("uCrsOriRes") > 0
								** encontrou liga��o a reserva
							Else
								&&procurar reservas do produto


								TEXT TO lcSQL TEXTMERGE NOSHOW
									SELECT
										bi.ndos, bo.no, bo.estab, bi.obistamp, bo.nome, bi.bistamp, bi.obrano, bi.dataobra, bi.design, bi.ref, qtt = (bi.qtt - bi.qtt2), bo.bostamp
									FROM bi (nolock)
									inner join bo (nolock) on bo.bostamp = bi.bostamp
									where
										bo.fechada = 0
										and bo.ndos = 5 /*reserva*/
										and bi.ref='<<ALLTRIM(fn.ref)>>'
										and bi.bistamp not in (select b_res_enviar.bistamp FROM  b_res_enviar (nolock)  )
									order by
										dataobra asc, obrano asc
								ENDTEXT
								uf_gerais_actGrelha("","uCrsOriRes",lcSQL)
							Endif




						Endif

						Select uCrsOriRes
						Go Top
						Scan
							If uCrsOriRes.ndos == 5
								lcContQtt = lcContQtt + uCrsOriRes.qtt
								If FN.qtt >= lcContQtt &&avisa os utentes disponiveis, se apenas pedir 1 avisa o primeiro utente
									uf_gerais_actGrelha("","uCrsTlmCli","select tlmvl from b_utentes (nolock) where no=" + Alltrim(Str(uCrsOriRes.NO)) + " and estab=" + Alltrim(Str(uCrsOriRes.ESTAB)))
									Select uCrsTlmCli
									Select uCrsCliSMSEnviar
									&&verificar se j� existe
									Locate For Alltrim(uCrsCliSMSEnviar.bistamp) == Alltrim(uCrsOriRes.bistamp) Or (uCrsCliSMSEnviar.NO == uCrsOriRes.NO And uCrsCliSMSEnviar.ESTAB == uCrsOriRes.ESTAB)
									If !Found()
										Append Blank
										Replace ;
											uCrsCliSMSEnviar.NO		  With uCrsOriRes.NO ;
											uCrsCliSMSEnviar.ESTAB    With uCrsOriRes.ESTAB ;
											uCrsCliSMSEnviar.sel 	  With .F. ;
											uCrsCliSMSEnviar.tlmvl 	  With uCrsTlmCli.tlmvl ;
											uCrsCliSMSEnviar.Nome 	  With uCrsOriRes.Nome ;
											uCrsCliSMSEnviar.bistamp  With uCrsOriRes.bistamp ;
											uCrsCliSMSEnviar.OBRANO   With uCrsOriRes.OBRANO ;
											uCrsCliSMSEnviar.DATAOBRA With uCrsOriRes.DATAOBRA ;
											uCrsCliSMSEnviar.Design   With uCrsOriRes.Design ;
											uCrsCliSMSEnviar.ref      With uCrsOriRes.ref ;
											uCrsCliSMSEnviar.BOSTAMP  With uCrsOriRes.BOSTAMP
									Endif
								Endif
							Endif
						Endscan
					Endscan
				Endif
			Endif
		Endif
	Endif

	If Used("uCrsOriRes")
		fecha("uCrsOriRes")
	Endif

	If Used("uCrsCliSMSEnviar")
		Select uCrsCliSMSEnviar
		If Reccount("uCrsCliSMSEnviar") > 0
			Select uCrsCliSMSEnviar
			Go Top
			uf_reservaclsms_chama()
		Endif
	Endif
Endfunc


**
Function uf_documentos_validaCamposObrigatorios
	Public mytermno
	Local lc_numregistos

	&&  Valida��es
	If Empty(mytermno)
		uf_perguntalt_chama("N�O � POSS�VEL GRAVAR DOCUMENTOS SEM A IDENTIFICA��O DO TERMINAL! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		Return .F.
	Endif

	Select cabdoc
	Go Top
	If Empty(cabdoc.MOEDA)
		uf_perguntalt_chama("EXISTEM CAMPOS OBRIGAT�RIOS POR PREENCHER. POR FAVOR VERIFIQUE."+Chr(10)+Chr(10)+"CAMPO: MOEDA","OK","",48)
		Return .F.
	Endif
	If Empty(cabdoc.Doc)
		uf_perguntalt_chama("EXISTEM CAMPOS OBRIGAT�RIOS POR PREENCHER. POR FAVOR VERIFIQUE. CAMPO: DOCUMENTO.","OK","",48)
		Return .F.
	Endif
	If Empty(cabdoc.Nome)
		uf_perguntalt_chama("EXISTEM CAMPOS OBRIGAT�RIOS POR PREENCHER. POR FAVOR VERIFIQUE. CAMPO: NOME.","OK","",48)
		Return .F.
	Endif
	If Empty(cabdoc.NO) Or cabdoc.NO == 0
		uf_perguntalt_chama("EXISTEM CAMPOS OBRIGAT�RIOS POR PREENCHER. POR FAVOR VERIFIQUE. CAMPO: NO.","OK","",48)
		Select cabdoc
		Replace cabdoc.Nome	With ''
		Return .F.
	Endif
	If Empty(cabdoc.NUMDOC)
		uf_perguntalt_chama("EXISTEM CAMPOS OBRIGAT�RIOS POR PREENCHER. POR FAVOR VERIFIQUE. CAMPO: N�MERO DOCUMENTO.","OK","",48)
		Return .F.
	Endif
	If Empty(cabdoc.ESTADODOC)
		uf_perguntalt_chama("EXISTEM CAMPOS OBRIGAT�RIOS POR PREENCHER. POR FAVOR VERIFIQUE. CAMPO: ESTADO DOCUMENTO.","OK","",48)
		Return .F.
	Endif
	If Empty(cabdoc.DATADOC)
		uf_perguntalt_chama("EXISTEM CAMPOS OBRIGAT�RIOS POR PREENCHER. POR FAVOR VERIFIQUE. CAMPO: DATA DOCUMENTO.","OK","",48)
		Return .F.
	Endif
	If Empty(cabdoc.ARMAZEM)
		uf_perguntalt_chama("EXISTEM CAMPOS OBRIGAT�RIOS POR PREENCHER. POR FAVOR VERIFIQUE. CAMPO: ARMAZ�M.","OK","",48)
		Return .F.
	Endif

	If myTipoDoc = "FO"
		If cabdoc.NUMINTERNODOC = 102
			If Empty(cabdoc.DOCFL)
				uf_perguntalt_chama("EXISTEM CAMPOS OBRIGAT�RIOS POR PREENCHER. POR FAVOR VERIFIQUE. CAMPO: DOC. FORNECEDOR","OK","",48)
				If myResizeDocumentos == .T.
					uf_documentos_expandeGridLinhas()
				Endif
				Return .F.
			Endif

			If Empty(cabdoc.DOCFLNO)
				If myResizeDocumentos == .T.
					uf_documentos_expandeGridLinhas()
				Endif
				uf_perguntalt_chama("EXISTEM CAMPOS OBRIGAT�RIOS POR PREENCHER. POR FAVOR VERIFIQUE. CAMPO: N�M. DOC. FORNECEDOR","OK","",48)
				Return .F.
			Endif
		Endif

		** Lotes
		If uf_gerais_getParameter("ADM0000000211","BOOL") And uf_gerais_getParameter("ADM0000000215","BOOL") == .F.
			Select FN
			Locate For Empty(Alltrim(FN.LOTE)) And !Empty(FN.usaLote)
			If Found()
				Select ucrse1
				If Alltrim(ucrse1.tipoempresa)='FARMACIA' Then
					uf_perguntalt_chama("O produto:" + Alltrim(FN.Design) + ", tem informa��o em falta." + Chr(13) + "Por favor preencha o n� do contentor.","OK","",48)
					Return .F.
				Else
					uf_perguntalt_chama("O produto:" + Alltrim(FN.Design) + ", est� marcado como usa Lotes." + Chr(13) + "Por favor verifique.","OK","",48)
					Return .F.
				Endif
			Endif
			Select FN
		Endif
	Endif

	&& Numero de Linhas do Documento
	lc_numregistos = 0
	If myTipoDoc = "FO"
		Select FN
		Go Top
		Select FN
		Count To lc_numregistos
	Endif
	If myTipoDoc = "BO"
		Select Bi
		Select Bi
		Count To lc_numregistos
	Endif
	If lc_numregistos = 0
		uf_perguntalt_chama("N�O � POSS�VEL A GRAVA��O DE DOCUMENTOS SEM LINHAS. POR FAVOR VERIFIQUE.","OK","",48)
		Return .F.
	Endif
	**

	&& valida��es que se aplicam apenas no cursores BO/BI/BI2 - [via verde e armazens inv�lidos]
	If myTipoDoc = "BO"

		**IF UPPER(ALLTRIM(CABDOC.DOC)) == "TRF ENTRE ARMAZ�NS"
		If cabdoc.tipodos = 3
			If !uf_documentos_checkArmazemDestino()
				Return .F.
			Endif
		Endif

		&& verifica se existem produtos que n�o sejam abrangidos pelo protocolo via verde na encomenda
		If Upper(Alltrim(cabdoc.Doc)) == "ENCOMENDA VIA VERDE"
			Local lcViaVerde, lcTarv
			Store .F. To lcViaVerde, lcTarv

			Select Bi
			Go Top
			Scan
				If Empty(Bi.viaVerde)
					uf_perguntalt_chama("O produto:" + Alltrim(Bi.Design) + ", n�o � abrangido pelo protocolo Via Verde / TARV." + Chr(13) + "Por favor verifique.","OK","",48)
					Return .F.
				Else
					If (Bi.viaVerde==1)
						lcViaVerde = .T.
					Endif
					If (Bi.viaVerde==2)
						lcTarv = .T.
					Endif
				Endif
				Select Bi
			Endscan

			If (lcViaVerde And lcTarv)
				uf_perguntalt_chama("N�o pode incluir produtos abrangidos pelo protocolo Via Verde e TARV na mesma encomenda.","OK","",48)
				Return .F.
			Endif
		Endif

		**Verifica se exite armazem a Zero
		**Controi cursores com os armazem existenstes
		lcSQL = ""
		TEXT TO lcsql TEXTMERGE NOSHOW
			exec up_gerais_armazensDisponiveis '<<ALLTRIM(mysite)>>', ''
		ENDTEXT
		If !uf_gerais_actGrelha("", "ucrsArmsValidos", lcSQL)
			uf_perguntalt_chama("N�O FOI POSSIVEL VERIFICAR A LISTA DE ARMAZ�NS VALIDOS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			Return .F.
		Endif

		If cabdoc.tipodos = 3
			Select Bi
			Go Top
			Locate For Bi.ARMAZEM == 0
			If Found()
				uf_perguntalt_chama("O DOCUMENTO CONT�M ARMAZ�NS INV�LIDOS, N�O � POSSIVEL A GRAVA��O. POR FAVOR VERIFIQUE.","OK","",48)
				Return .F.
			Endif
		Endif

		If cabdoc.tipodos = 3
			Select * From Bi Left Join ucrsArmsValidos On Bi.ARMAZEM == ucrsArmsValidos.ARMAZEM Where ucrsArmsValidos.ARMAZEM Is Null Into Cursor ucrsArmInvalidados Readwrite
			If Reccount("ucrsArmInvalidados") > 0
				uf_perguntalt_chama("O DOCUMENTO CONT�M ARMAZ�NS INV�LIDOS, N�O � POSSIVEL A GRAVA��O. POR FAVOR VERIFIQUE.","OK","",48)
				Return .F.
			Endif
		Endif

		If cabdoc.tipodos = 3
			&& Armazem 2
			If ucrsConfigDoc.TRFA == .T. && So para dossiers de transferencia que tem 2 armazens
				Select * From Bi Left Join ucrsArmsValidos On Bi.ar2mazem == ucrsArmsValidos.ARMAZEM Where ucrsArmsValidos.ARMAZEM Is Null Into Cursor ucrsArmInvalidados Readwrite
				If Reccount("ucrsArmInvalidados") > 0
					uf_perguntalt_chama("O DOCUMENTO CONT�M ARMAZ�NS INV�LIDOS, N�O � POSSIVEL A GRAVA��O. POR FAVOR VERIFIQUE.","OK","",48)
					Return .F.
				Endif
			Endif
		Endif

		If Used("ucrsArmsValidos")
			fecha("ucrsArmsValidos")
		Endif
		If Used("ucrsArmInvalidados")
			fecha("ucrsArmInvalidados")
		Endif

		** Lotes
		If uf_gerais_getParameter("ADM0000000211","BOOL") And uf_gerais_getParameter("ADM0000000215","BOOL") == .F.
			Select Bi
			Locate For Empty(Alltrim(FN.LOTE)) And !Empty(FN.usaLote)
			If Found()
				Select ucrse1
				If Alltrim(ucrse1.tipoempresa)='FARMACIA' Then
					uf_perguntalt_chama("O produto:" + Alltrim(Bi.Design) + ", tem informa��o em falta." + Chr(13) + "Por favor preencha o n� do contentor.","OK","",48)
					Return .F.
				Else
					uf_perguntalt_chama("O produto:" + Alltrim(Bi.Design) + ", est� marcado como usa Lotes." + Chr(13) + "Por favor verifique.","OK","",48)
					Return .F.
				Endif
			Endif
			Select Bi
		Endif
	Endif
	*****

	&& nao podem existir linhas sem ref nas reservas
	Select cabdoc
	If Alltrim(Upper(cabdoc.Doc)) == Upper("Reserva de Cliente")
		If Used("BI")
			Select Bi
			Scan
				If Empty(Alltrim(Bi.ref)) == .T.
					uf_perguntalt_chama("N�O � POSSIVEL GRAVAR RESERVAS DE CLIENTES COM LINHAS SEM REFER�NCIA. POR FAVOR VERIFIQUE.","OK","",48)
					Return .F.
				Endif
			Endscan
		Endif
	Endif

	** Lotes
	If uf_gerais_getParameter("ADM0000000211","BOOL") And uf_gerais_getParameter("ADM0000000215","BOOL") == .F. And ucrsConfigDoc.folanSl == .T.

		If myTipoDoc = "BO"
			Select Bi
			Calculate Sum(qtt) For Empty(Alltrim(Bi.LOTE)) And !Empty(Bi.usaLote) And !Empty(Bi.ref) To lcControlaLinhasSemLote
			If lcControlaLinhasSemLote!=0
				Select ucrse1
				If Alltrim(ucrse1.tipoempresa)='FARMACIA' Then
					uf_perguntalt_chama("Existem linhas em que o n� do contentor Valormed n�o foi identificado, para produtos com a referencia VALORMED." + Chr(13) + "Por favor verifique.","OK","",48)
					Return .F.
				Else
					uf_perguntalt_chama("Existem linhas em que o Lote n�o foi identificado, para produtos configurados como usando Lotes." + Chr(13) + "Por favor verifique.","OK","",48)
					Return .F.
				Endif
			Endif
		Endif


		If myTipoDoc = "FO"
			Select FN
			Calculate Sum(qtt) For Empty(Alltrim(FN.LOTE)) And !Empty(FN.usaLote) And !Empty(FN.ref) To lcControlaLinhasSemLote
			If lcControlaLinhasSemLote!=0
				** Verificar se � farm�cio o lote � utilizado para n� contentor
				Select ucrse1
				If Alltrim(ucrse1.tipoempresa)='FARMACIA' Then
					uf_perguntalt_chama("Existem linhas em que o n� do contentor valormed n�o foi inserido. Por favor verifique." + Chr(13) + "Por favor verifique.","OK","",48)
					Return .F.
				Else
					uf_perguntalt_chama("Existem linhas em que o Lote n�o foi identificado, para produtos configurados como usando Lotes." + Chr(13) + "Por favor verifique.","OK","",48)
					Return .F.
				Endif
			Endif
		Endif
	Endif

	** Verifica existencia de produtos inativos
	If Used("ucrsProdutosInactivosDoc")
		fecha("ucrsProdutosInactivosDoc")
	Endif

	TEXT TO lcSQL NOSHOW TEXTMERGE
		select ref as refinativa from st (nolock) where inactivo = 1 AND st.site_nr = <<mysite_nr>>
	ENDTEXT
	If !uf_gerais_actGrelha("", "ucrsRefsInativas", lcSQL)
		uf_perguntalt_chama("N�O FOI POSSIVEL VERIFICAR A LISTA DE REFER�NCIAS INATIVAS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		Return .F.
	Endif

	Do Case
		Case myTipoDoc == "BO"
			Select Distinct refinativa From Bi Left Join ucrsRefsInativas On Bi.ref = ucrsRefsInativas.refinativa Where refinativa Is Not Null Into Cursor ucrsProdutosInactivosDoc Readwrite
		Case myTipoDoc == "FO"
			Select Distinct refinativa From FN Left Join ucrsRefsInativas On FN.ref = ucrsRefsInativas.refinativa Where refinativa Is Not Null Into Cursor ucrsProdutosInactivosDoc Readwrite
	Endcase
	Local lcRefs

	If Reccount("ucrsProdutosInactivosDoc") != 0
		lcRefs = ""
		Select ucrsProdutosInactivosDoc
		Go Top
		Scan
			If Empty(lcRefs)
				lcRefs  = Alltrim(ucrsProdutosInactivosDoc.refinativa)
			Else
				lcRefs  = lcRefs + ", " + Alltrim(ucrsProdutosInactivosDoc.refinativa)
			Endif

		Endscan

		uf_perguntalt_chama("Existem produtos inativos no documento: Ex. " + Left(lcRefs,150) + Chr(13) + Chr(13) + "N�o � possivel gravar." ,"OK","",16)
		Return .F.
	Endif

	Return .T.
Endfunc


** Conjunto Valida��es que ocorrem antes da Grava��o dos Documentos
Function uf_documentos_validacoesEnc
	Local lcValidaEnc, lcFlNo, lcSQL
	Store .T. To lcValidaEnc
	Store '' To lcSQL

	** Verificar ee o documento j� foi exportado
	Select cabdoc
	If !Empty(cabdoc.EXPORTADO)
		uf_perguntalt_chama("O Documento j� foi exportado. As altera��es efectuadas poder�o n�o ser aplicadas no destino.","OK","",64)
	Endif

	** Verificar se existem refer�ncias repetidas // Linhas com qtt = 0
	Select cabdoc
	If cabdoc.NUMINTERNODOC == 2 Or cabdoc.NUMINTERNODOC == 37
		&& Linhas Duplicadas
		Select Count(bistamp),ref, Design From Bi Where !Empty(Alltrim(Bi.ref)) Group By ref, Design Having Count(bistamp) > 1 Into Cursor ucrsBiRefsDuplicadas Readwrite
		Select ucrsBiRefsDuplicadas
		If Reccount("ucrsBiRefsDuplicadas")>0
			If !uf_perguntalt_chama("Aten��o! Existem refer�ncias duplicadas nas linhas. Pretende mesmo assim continuar?"+Chr(10)+Chr(13)+Chr(10)+Chr(13)+"Produto duplicado: "+ ucrsBiRefsDuplicadas.Design ,"Sim","N�o")
				Return .F.
			Endif
		Endif

		&& Linhas com qqt = 0
		Select Count(bistamp),ref, Design From Bi Where Bi.qtt = 0 Group By ref, Design  Into Cursor uCrsAuxBiRefQtt Readwrite
		Select uCrsAuxBiRefQtt
		If Reccount("uCrsAuxBiRefQtt")>0
			If !uf_perguntalt_chama("Aten��o! Existem refer�ncias com quantidade 0 nas linhas. Pretende mesmo assim continuar?"+Chr(10)+Chr(13)+Chr(10)+Chr(13)+"Produto quantidade = 0: "+ uCrsAuxBiRefQtt.Design ,"Sim","N�o")
				Return .F.
			Endif
		Endif
	Endif

	** VALIDAR FORNECEDOR PREFER�NCIAL E FONTE DO ARTIGO
	Select cabdoc
	If ucrsts.tipodos = 2 Or cabdoc.NUMINTERNODOC==2 Or cabdoc.NUMINTERNODOC==37
		lcFlNo = cabdoc.NO

		If lcFlNo!=0

			** valida fonte do artigo
			If uf_gerais_getParameter("ADM0000000105","BOOL")
				** verificar se o perfil do user pode encomendas/devolver artigos externos **
				If !(uf_gerais_validaPermUser(ch_userno, ch_grupo,'Encomendas - Artigos Externos'))
					Select Bi
					Go Top
					Scan For !Empty(Bi.ref)
						lcSQL = "select u_fonte from st (nolock) where ref='"+Alltrim(Bi.ref)+"'"
						If uf_gerais_actGrelha("", "uCrsFonteArt", lcSQL)
							If Reccount()>0
								If uCrsFonteArt.u_fonte=='E'
									uf_perguntalt_chama("O SEU PERFIL N�O LHE PERMITE ENCOMENDAS PRODUTOS DE FONTE EXTERNA, REF: "+Alltrim(Bi.ref)+".","OK","",48)
									lcValidaEnc = .F.
									fecha("uCrsFonteArt")
									Exit
								Endif
							Endif
							fecha("uCrsFonteArt")
						Endif
						Select Bi
					Endscan
				Endif
			Endif
		Endif
	Endif

	If !lcValidaEnc
		Return lcValidaEnc
	Endif


	** Valida Data de Entrega
	Select cabdoc
	If !Empty(cabdoc.DATAENTREGA) And cabdoc.DATAENTREGA<cabdoc.DATADOC And !(uf_gerais_getdate(cabdoc.DATAENTREGA,"SQL")=='19000101') And Alltrim(cabdoc.Doc)!='Encomenda de Cliente'
		uf_perguntalt_chama("N�O PODE TER UMA DATA DE ENTREGA INFERIOR � DATA DO DOCUMENTO.","OK","",48)
		lcValidaEnc = .F.
	Endif

	If !lcValidaEnc
		Return lcValidaEnc
	Endif
	**

	** Motivo de Quebra Obrigat�rio no Documento Stock Quebra
	Select cabdoc
	If cabdoc.NUMINTERNODOC == 18
		Local lcValor
		Store .T. To lcValor
		Select Bi
		Go Top
		Scan For !Empty(Bi.ref)
			If Empty(Bi.u_mquebra)
				uf_perguntalt_chama("Deve seleccionar o motivo de acerto nas linhas.", "OK", "", 48)
				lcValidaEnc = .F.
				Return .F.
			Endif
		Endscan

		** valida data do documento, n�o pode ser inferior ao dia de hoje **
		Select cabdoc
		If cabdoc.DATADOC < Date()
			uf_perguntalt_chama("N�o � possivel editar documentos de acerto de stock anteriores ao dia de hoje.", "OK", "", 48)
			lcValidaEnc = .F.
			Return .F.
		Endif
	Endif
	**

	If !lcValidaEnc
		Return lcValidaEnc
	Endif
	**

	** N�o Permite Psicos no Documento Consumo Interno **
	Select cabdoc
	If cabdoc.NUMINTERNODOC==6
		Select Bi
		Locate For !Empty(Bi.psico) Or !Empty(Bi.benzo)
		If Found()
			uf_perguntalt_chama("N�O PODE INCLUIR PSICOTR�PICOS NUM DOCUMENTO DE CONSUMO INTERNO.","OK","",48)
			Return .F.
		Endif
	Endif

	** Valida Plafond das Encomendas a Fornecedor **
	If (cabdoc.NUMINTERNODOC==2 Or cabdoc.NUMINTERNODOC==37)
		lcValidaEnc = uf_documentos_verifyPlafondEncForn()
		If !lcValidaEnc
			uf_perguntalt_chama("N�O PODE ULTRAPASSAR O PLAFOND DAS ENCOMENDAS A FORNCEDOR.","OK","",48)
			Return lcValidaEnc
		Endif
	Endif
	**

	** Valor por defeito para gerar contador de Psicotr�picos e Benzodiazepinas
	Local lnContador, lcSiteContador, lcContadorFn, lnContadorPsi, lnContadorBenzo
	Store 0 To lnContador, lcContadorFn, lnContadorPsi, lnContadorBenzo
	Store '' To lcSiteContador

	If Inlist(ucrsts.codigoDoc,33,34,35,54)

		Select Bi
		Goto Top
		Scan For (!Empty(Bi.ref))

			TEXT TO lcSql NOSHOW TEXTMERGE PRETEXT 7
				Select psico, benzo FROM fprod (nolock) WHERE cnp='<<bi.ref>>'
			ENDTEXT
			If uf_gerais_actGrelha("", "uCrsPsiBen", lcSQL)
				Select uCrsPsiBen
				If Reccount()>0
					Select uCrsPsiBen
					If uCrsPsiBen.psico
						If Empty(Bi.u_psicont) Or Bi.u_psicont == 0
							**Comentado ate de desenvolver a parte do acerto stock entradas
							** como nao � doc fornecedor nao contabiliza e nao da para guardar
							**acerto de stock
							*!*								IF (ucrsTS.codigoDoc = 33)
							*!*									IF(bi.qtt >0)
							*!*										**entrada
							*!*										lnContador = uf_psicobenzo_contador_psico_benzo_entradasSaidas(0,1,'')
							*!*									ELSE
							*!*										**saida
							*!*										lnContador = uf_psicobenzo_contador_psico_benzo_entradasSaidas(1,1,'')
							*!*									ENDIF
							*!*								ENDIF
							**Comentado ate de desenvolver a parte das transferencias de armazem
							** falta conseguir maneira de guardar o valor da trnasferencia na outra loja
							*!*								IF (ucrsTS.codigoDoc = 54)
							*!*									lcSiteContador = uf_gerais_getSitePorArmazem(bi.ar2mazem)
							*!*									IF(!uf_gerais_compStr(lcSiteContador, mysite))
							*!*										** entrada psico na outra loja
							*!*										lcContadorFn = uf_psicobenzo_contador_psico_benzo_entradasSaidas(0,1,lcSiteContador)
							*!*										** saida do psico na loja
							*!*										lnContador = uf_psicobenzo_contador_psico_benzo_entradasSaidas(1,1,'')
							*!*									ENDIF
							*!*
							*!*								ENDIF

							If Inlist(ucrsts.codigoDoc,34,35) And lnContadorPsi= 0
								lnContadorPsi= uf_psicobenzo_contador_psico_benzo_entradasSaidas(1,1,'')
							Else
								lnContadorPsi= lnContadorPsi+ 1
							Endif

							Select Bi
							Replace Bi.u_psicont With lnContadorPsi
						Endif
					Else
						If uCrsPsiBen.benzo
							If Bi.u_bencont=0
								**Comentado ate de desenvolver a parte do acerto stock entradas
								** como nao � doc fornecedor nao contabiliza e nao da para guardar
								**acerto de stock
								*!*									IF (ucrsTS.codigoDoc = 33)
								*!*										IF(bi.qtt >0)
								*!*											**entrada
								*!*											**lnContador = uf_psicobenzo_contador_psico_benzo_entradasSaidas(0,2,'')
								*!*										ELSE
								*!*											**saida
								*!*											lnContador = uf_psicobenzo_contador_psico_benzo_entradasSaidas(1,2,'')
								*!*										ENDIF
								*!*									ENDIF
								**Comentado ate de desenvolver a parte das transferencias de armazem
								** falta conseguir maneira de guardar o valor da trnasferencia na outra loja
								*!*									IF (ucrsTS.codigoDoc = 54)
								*!*										lcSiteContador = uf_gerais_getSitePorArmazem(bi.ar2mazem)
								*!*										IF(!uf_gerais_compStr(lcSiteContador, mysite))
								*!*											** entrada psico na outra loja
								*!*											lcContadorFn = uf_psicobenzo_contador_psico_benzo_entradasSaidas(0,2,lcSiteContador)
								*!*											IF USED("fn")
								*!*												SELECT FN
								*!*												REPLACE fn.u_psicont WITH lcContadorFn
								*!*											ENDIF
								*!*											** saida do psico na loja
								*!*											lnContador = uf_psicobenzo_contador_psico_benzo_entradasSaidas(1,2,'')
								*!*										ENDIF
								*!*									ENDIF

								If Inlist(ucrsts.codigoDoc,34,35) And lnContadorBenzo = 0
									lnContadorBenzo= uf_psicobenzo_contador_psico_benzo_entradasSaidas(1,2,'')
								Else
									lnContadorBenzo= lnContadorBenzo+ 1
								Endif

								Select Bi
								Replace Bi.u_bencont With lnContadorBenzo

							Endif
						Endif
					Endif
				Endif
			Endif
		Endscan
	Endif
	**

	** Valida Valores **
	Local lcValBonus
	Store '' To lcValBonus

	Select Bi
	Go Top
	Scan For !Empty(Bi.ref) And !Empty(Bi.u_bonus)
		If Bi.u_bonus>=Bi.qtt Or Bi.u_bonus<0
			lcValBonus = Alltrim(Bi.Design)
		Endif
	Endscan

	If !Empty(lcValBonus)
		If !uf_perguntalt_chama("Aten��o! O produto: ["+Alltrim(lcValBonus)+"] cont�m quantidades de B�nus inv�lidas. Pretende mesmo assim continuar?"+Chr(13)+"Quantidades de bonus inv�lidas ser�o ignoradas no envio da encomenda.","Sim","N�o")
			lcValidaEnc = .F.
		Endif
	Endif

	If !lcValidaEnc
		Return lcValidaEnc
	Endif
	**

	** Verifica se o N� do Documento j� existe **
	If myDocIntroducao == .T.
		**Local lcSql
		**lcSql=""
		**Text To lcSql Noshow textmerge
		**	Select 	obrano
		**	From 	bo (nolock)
		**	Where	ndos = <<astr(cabdoc.numInternoDoc)>>
		**			And obrano = <<ALLTRIM(cabdoc.numdoc)>>
		**			And YEAR(dataobra) = <<YEAR(cabdoc.datadoc)>>
		**			and bo.bostamp != '<<ALLTRIM(cabdoc.cabstamp)>>'
		**Endtext
		**IF uf_gerais_actGrelha("", "uCrsChkNumDoc", lcSql)
		**	IF RECCOUNT()>0
		**
		**		uf_perguntalt_chama("ATEN��O: J� EXISTE UM DOCUMENTO COM O MESMO N�MERO. O N�MERO IR� SER INCREMENTADO. POR FAVOR TENTE NOVAMENTE.","OK","",48)
		**		SELECT cabdoc
		**		replace cabdoc.numdoc WITH ASTR(VAL(cabdoc.numdoc)+1)
		**		DOCUMENTOS.ContainerCab.numdoc.refresh
		**
		**		**
		**		UPDATE bi SET obrano = VAL(cabdoc.numdoc)
		**
		**		lcValidaEnc = .f.
		**	ENDIF
		**	fecha("uCrsChkNumDoc")
		**ELSE
		**	uf_perguntalt_chama("N�O FOI POSS�VEL VALIDAR O N�MERO DO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		**	lcValidaEnc = .f.
		**ENDIF

		uf_documentos_novoDocNr(.T.)

	Endif

	**  Valida Dados de Reservas
	Select cabdoc
	If Upper(Alltrim(cabdoc.Doc)) == Upper("Reserva de Cliente")
		If cabdoc.NO < 200
			uf_perguntalt_chama("N�O PODE EFECTUAR RESERVAS PARA CLIENTES DO TIPO ENTIDADE. POR FAVOR VERIFIQUE.","OK","",48)
			lcValidaEnc = .F.
		Endif

		If cabdoc.NO = 200
			uf_perguntalt_chama("N�O PODE EFECTUAR RESERVAS PARA O CLIENTE N� 200!","OK","",48)
			lcValidaEnc = .F.
		Endif

		&& Verifica se o cliente tem reservas canceladas
		If !uf_gerais_actGrelha("", "uCrsTmpCheckReserva", [Select naoencomenda from b_utentes (nolock) where no = ]+ASTR(cabdoc.NO)+[ and estab =]+ASTR(cabdoc.ESTAB)+[])
			uf_perguntalt_chama("OCORREU UMA ANOMALIA A VERIFICAR SE O CLIENTE TEM RESERVAS CANCELADAS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			lcValidaEnc = .F.
		Endif

		Select uCrsTmpCheckReserva
		If uCrsTmpCheckReserva.naoencomenda
			uf_perguntalt_chama("N�O PODE CRIAR RESERVAS PARA UM CLIENTE COM A OP��O RESERVAS CANCELADAS ACTIVA FICHA.","OK","",48)
			fecha("uCrsTmpCheckReserva")
			lcValidaEnc = .F.
		Endif

		If Used("uCrsTmpCheckReserva")
			fecha("uCrsTmpCheckReserva")
		Endif
	Endif

	*!*		** Fecho autom�tico de Documentos de Compras e Dossiers - passa a ser chamada ap�s a grava��o dos documentos - LL 2015/11/23
	*!*		IF lcValidaEnc
	*!*			uf_fechoautomaticodoc_chama('BO')
	*!*		ENDIF
	*!*		**

	If Used("ucrsBiRefsDuplicadas")
		fecha("ucrsBiRefsDuplicadas")
	Endif

	If Used("uCrsAuxBiRefQtt")
		fecha("uCrsAuxBiRefQtt")
	Endif

	Return lcValidaEnc
Endfunc


** Insere cabe�alho Dossiers Internos
Function uf_documentos_insereCabBO
	Lparameters lnHora, lcdoccont

	Local lcExecuteSQL
	lcExecuteSQL = ""

	If Type("lnHora") != "C" Or Empty(Alltrim(lnHora))
		Set Hours To 24
		Atime=Ttoc(Datetime()+(difhoraria*3600),2)
		ASTR=Left(Atime,8)
		lnHora = ASTR
		**lnHora = TIME()
	Endif

	Local lcValorIva1, lcValorIva2, lcValorIva3, lcValorIva4, lcValorIva5, lcValorIva6, lcValorIva7, lcValorIva8, lcValorIva9, lcValorIva10,lcValorIva11, lcValorIva12, lcValorIva13;
		,lcTotalSemIvaDi1, lcTotalSemIvaDi2, lcTotalSemIvaDi3, lcTotalSemIvaDi4, lcTotalSemIvaDi5, lcTotalSemIvaDi6, lcTotalSemIvaDi7;
		,lcTotalSemIvaDi8, lcTotalSemIvaDi9, lcTotalSemIvaDi10, lcTotalSemIvaDi11, lcTotalSemIvaDi12, lcTotalSemIvaDi13, lcCusto, lcQtSum, lcTotalIva, lcEstadoDocBO

	Store 0 To lcValorIva1,lcValorIva2,lcValorIva3,lcValorIva4,lcValorIva5,lcValorIva6,lcValorIva7,lcValorIva8,lcValorIva9, lcValorIva10,lcValorIva11, lcValorIva12, lcValorIva13;
		,lcTotalSemIvaDi1, lcTotalSemIvaDi2, lcTotalSemIvaDi3, lcTotalSemIvaDi4, lcTotalSemIvaDi5, lcTotalSemIvaDi6;
		,lcTotalSemIvaDi7, lcTotalSemIvaDi8, lcTotalSemIvaDi9, lcTotalSemIvaDi10, lcTotalSemIvaDi11, lcTotalSemIvaDi12, lcTotalSemIvaDi13, lcCusto, lcQtSum, lcTotalIva, lcEstadoDocBO

	Set Point To "."

	Select cabdoc

	** CALCULA TOTAIS
	totalciva = 0
	Select Bi
	Go Top
	Scan
		lcQtSum = lcQtSum + Bi.qtt
		lcCusto = lcCusto + Bi.EPCUSTO
		**CALCULA SUBTOTAIS IVA
		Do Case
			Case Bi.tabiva=1
				If Bi.ivaincl
					lcValorIva1=lcValorIva1 + Bi.ettdeb - (Bi.ettdeb/(Bi.IVA/100+1))
					lcTotalSemIvaDi1 = lcTotalSemIvaDi1 + (Bi.ettdeb/(Bi.IVA/100+1))
				Else
					lcValorIva1=lcValorIva1+(Bi.ettdeb*(Bi.IVA/100))
					lcTotalSemIvaDi1 = lcTotalSemIvaDi1 + Bi.ettdeb
				Endif


			Case Bi.tabiva=2
				If Bi.ivaincl
					lcValorIva2=lcValorIva2 + Bi.ettdeb - (Bi.ettdeb/(Bi.IVA/100+1))
					lcTotalSemIvaDi2 = lcTotalSemIvaDi2 + (Bi.ettdeb/(Bi.IVA/100+1))
				Else
					lcValorIva2=lcValorIva2+(Bi.ettdeb*(Bi.IVA/100))
					lcTotalSemIvaDi2 = lcTotalSemIvaDi2 + Bi.ettdeb
				Endif
			Case Bi.tabiva=3
				If Bi.ivaincl
					lcValorIva3=lcValorIva3 + Bi.ettdeb - (Bi.ettdeb/(Bi.IVA/100+1))
					lcTotalSemIvaDi3 = lcTotalSemIvaDi3 + (Bi.ettdeb/(Bi.IVA/100+1))
				Else
					lcValorIva3=lcValorIva3+(Bi.ettdeb*(Bi.IVA/100))
					lcTotalSemIvaDi3 = lcTotalSemIvaDi3 + Bi.ettdeb
				Endif
			Case Bi.tabiva=4
				If Bi.ivaincl
					lcValorIva4=lcValorIva4 + Bi.ettdeb - (Bi.ettdeb/(Bi.IVA/100+1))
					lcTotalSemIvaDi14= lcTotalSemIvaDi4 + (Bi.ettdeb/(Bi.IVA/100+1))
				Else
					lcValorIva4=lcValorIva4+(Bi.ettdeb*(Bi.IVA/100))
					lcTotalSemIvaDi4 = lcTotalSemIvaDi4 + Bi.ettdeb
				Endif
			Case Bi.tabiva=5
				If Bi.ivaincl
					lcValorIva5=lcValorIva5 + Bi.ettdeb - (Bi.ettdeb/(Bi.IVA/100+1))
					lcTotalSemIvaDi5 = lcTotalSemIvaDi5 + (Bi.ettdeb/(Bi.IVA/100+1))
				Else
					lcValorIva5=lcValorIva5+(Bi.ettdeb*(Bi.IVA/100))
					lcTotalSemIvaDi5 = lcTotalSemIvaDi5 + Bi.ettdeb
				Endif
			Case Bi.tabiva=6
				If Bi.ivaincl
					lcValorIva6=lcValorIva6 + Bi.ettdeb - (Bi.ettdeb/(Bi.IVA/100+1))
					lcTotalSemIvaDi6 = lcTotalSemIvaDi6 + (Bi.ettdeb/(Bi.IVA/100+1))
				Else
					lcValorIva6=lcValorIva6+(Bi.ettdeb*(Bi.IVA/100))
					lcTotalSemIvaDi6 = lcTotalSemIvaDi6 + Bi.ettdeb
				Endif
			Case Bi.tabiva=7
				If Bi.ivaincl
					lcValorIva7=lcValorIva7 + Bi.ettdeb - (Bi.ettdeb/(Bi.IVA/100+1))
					lcTotalSemIvaDi7 = lcTotalSemIvaDi7 + (Bi.ettdeb/(Bi.IVA/100+1))
				Else
					lcValorIva7=lcValorIva7+(Bi.ettdeb*(Bi.IVA/100))
					lcTotalSemIvaDi7 = lcTotalSemIvaDi7 + Bi.ettdeb
				Endif
			Case Bi.tabiva=8
				If Bi.ivaincl
					lcValorIva8=lcValorIva8 + Bi.ettdeb - (Bi.ettdeb/(Bi.IVA/100+1))
					lcTotalSemIvaDi8 = lcTotalSemIvaDi8 + (Bi.ettdeb/(Bi.IVA/100+1))
				Else
					lcValorIva8=lcValorIva8+(Bi.ettdeb*(Bi.IVA/100))
					lcTotalSemIvaDi8 = lcTotalSemIvaDi8 + Bi.ettdeb
				Endif
			Case Bi.tabiva=9
				If Bi.ivaincl
					lcValorIva9=lcValorIva9 + Bi.ettdeb - (Bi.ettdeb/(Bi.IVA/100+1))
					lcTotalSemIvaDi9 = lcTotalSemIvaDi9 + (Bi.ettdeb/(Bi.IVA/100+1))
				Else
					lcValorIva9=lcValorIva9+(Bi.ettdeb*(Bi.IVA/100))
					lcTotalSemIvaDi9 = lcTotalSemIvaDi9 + Bi.ettdeb
				Endif
			Case Bi.tabiva=10
				If Bi.ivaincl
					lcValorIva10=lcValorIva10 + Bi.ettdeb - (Bi.ettdeb/(Bi.IVA/100+1))
					lcTotalSemIvaDi10 = lcTotalSemIvaDi10 + (Bi.ettdeb/(Bi.IVA/100+1))
				Else
					lcValorIva10=lcValorIva10+(Bi.ettdeb*(Bi.IVA/100))
					lcTotalSemIvaDi10 = lcTotalSemIvaDi10 + Bi.ettdeb
				Endif
			Case Bi.tabiva=11
				If Bi.ivaincl
					lcValorIva11=lcValorIva11 + Bi.ettdeb - (Bi.ettdeb/(Bi.IVA/100+1))
					lcTotalSemIvaDi11 = lcTotalSemIvaDi11 + (Bi.ettdeb/(Bi.IVA/100+1))
				Else
					lcValorIva11=lcValorIva11+(Bi.ettdeb*(Bi.IVA/100))
					lcTotalSemIvaDi11 = lcTotalSemIvaDi11 + Bi.ettdeb
				Endif
			Case Bi.tabiva=12
				If Bi.ivaincl
					lcValorIva12=lcValorIva12 + Bi.ettdeb - (Bi.ettdeb/(Bi.IVA/100+1))
					lcTotalSemIvaDi12 = lcTotalSemIvaDi12 + (Bi.ettdeb/(Bi.IVA/100+1))
				Else
					lcValorIva12=lcValorIva12+(Bi.ettdeb*(Bi.IVA/100))
					lcTotalSemIvaDi12 = lcTotalSemIvaDi12 + Bi.ettdeb
				Endif
			Case Bi.tabiva=13
				If Bi.ivaincl
					lcValorIva13=lcValorIva13 + Bi.ettdeb - (Bi.ettdeb/(Bi.IVA/100+1))
					lcTotalSemIvaDi13 = lcTotalSemIvaDi13 + (Bi.ettdeb/(Bi.IVA/100+1))
				Else
					lcValorIva13=lcValorIva13+(Bi.ettdeb*(Bi.IVA/100))
					lcTotalSemIvaDi13 = lcTotalSemIvaDi13 + Bi.ettdeb
				Endif
		Endcase

		lcTotalSemIvaDI = lcTotalSemIvaDi1 + lcTotalSemIvaDi2 + lcTotalSemIvaDi3 + lcTotalSemIvaDi4 + lcTotalSemIvaDi5 + lcTotalSemIvaDi6 + lcTotalSemIvaDi7 + lcTotalSemIvaDi8 + lcTotalSemIvaDi9 + lcTotalSemIvaDi10 + lcTotalSemIvaDi11 + lcTotalSemIvaDi12 + lcTotalSemIvaDi13
	Endscan

	lcValorIva1 = Round(lcValorIva1,2)
	lcValorIva2 = Round(lcValorIva2,2)
	lcValorIva3 = Round(lcValorIva3,2)
	lcValorIva4 = Round(lcValorIva4,2)
	lcValorIva5 = Round(lcValorIva5,2)
	lcValorIva6 = Round(lcValorIva6,2)
	lcValorIva7 = Round(lcValorIva7,2)
	lcValorIva8 = Round(lcValorIva8,2)
	lcValorIva9 = Round(lcValorIva9,2)
	lcValorIva10 = Round(lcValorIva10,2)
	lcValorIva11 = Round(lcValorIva11,2)
	lcValorIva12 = Round(lcValorIva12,2)
	lcValorIva13 = Round(lcValorIva13,2)
	lcTotalIva = lcValorIva1 + lcValorIva2 + lcValorIva3 + lcValorIva4 + lcValorIva5 + lcValorIva6 + lcValorIva7 + lcValorIva8 + lcValorIva9 + lcValorIva10 + lcValorIva11 + lcValorIva12 + lcValorIva13


	* INSERT BO, BO2 *
	Select cabdoc

	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE PRETEXT 8
		Insert into bo2 (
			bo2stamp
			,autotipo
			,pdtipo
			,etotalciva
			,u_doccont
			,ETOTIVA
			,telefone
			,email
			,xpddata
			,xpdhora
			,xpdviatura
			,morada
			,codpost
			,contacto
			,armazem
			,u_class
			,ousrinis
			,ousrdata
			,ousrhora
			,usrinis
			,usrdata
			,usrhora
			,ATDocCode
			,ebo71_bins
			,ebo72_bins
			,ebo81_bins
			,ebo82_bins
			,ebo91_bins
			,ebo92_bins
			,ebo71_iva
			,ebo72_iva
			,ebo81_iva
			,ebo82_iva
			,ebo91_iva
			,ebo92_iva
			,ivatx1
			,ivatx2
			,ivatx3
			,ivatx4
			,ivatx5
			,ivatx6
			,ivatx7
			,ivatx8
			,ivatx9
			,ivatx10
			,ivatx11
			,ivatx12
			,ivatx13
			,nrReceita
			,pinacesso
			,pinopcao
			,status
			,modo_envio
			,pagamento
			,momento_pagamento
			,ebo101_bins
			,ebo111_bins
			,ebo121_bins
			,ebo131_bins
			,ebo101_iva
			,ebo111_iva
			,ebo121_iva
			,ebo131_iva
			,xpdLocalidade
		)
		Values (
			'<<Alltrim(cabDoc.cabstamp)>>'
			,1
			,1
			,<<ROUND(lcTotalSemIvaDI,2)>>
			,<<lcdoccont>>
			,<<Round(lcTotalIva,2)>>
			,'<<Alltrim(cabdoc.xpdtelefone)>>'
			,'<<Alltrim(cabdoc.xpdemail)>>'
			,'<<uf_gerais_getDate(cabdoc.xpddata,"SQL")>>'
			,'<<Alltrim(cabdoc.xpdhora)>>'
			,'<<Alltrim(cabdoc.xpdviatura)>>'
			,'<<Alltrim(cabdoc.xpdmorada)>>'
			,'<<Alltrim(cabdoc.xpdcodpost)>>'
			,'<<Alltrim(cabdoc.xpdcontacto)>>'
			,<<cabDoc.armazem>>
			,'<<Alltrim(cabdoc.u_class)>>'
			,'<<m_chinis>>'
			, convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
			, convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8)
			,'<<m_chinis>>'
			, convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
			, convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8)
			,'<<ALLTRIM(cabDoc.ATDocCode)>>'
			,<<lcTotalSemIvaDi7>>
			,<<lcTotalSemIvaDi7>>
			,<<lcTotalSemIvaDi8>>
			,<<lcTotalSemIvaDi8>>
			,<<lcTotalSemIvaDi9>>
			,<<lcTotalSemIvaDi9>>
			,<<lcValorIva7>>
			,<<lcValorIva7>>
			,<<lcValorIva8>>
			,<<lcValorIva8>>
			,<<lcValorIva9>>
			,<<lcValorIva9>>
			,ISNULL((select top 1 taxa from taxasiva where codigo=1),0)
			,ISNULL((select top 1 taxa from taxasiva where codigo=2),0)
			,ISNULL((select top 1 taxa from taxasiva where codigo=3),0)
			,ISNULL((select top 1 taxa from taxasiva where codigo=4),0)
			,ISNULL((select top 1 taxa from taxasiva where codigo=5),0)
			,ISNULL((select top 1 taxa from taxasiva where codigo=6),0)
			,ISNULL((select top 1 taxa from taxasiva where codigo=7),0)
			,ISNULL((select top 1 taxa from taxasiva where codigo=8),0)
			,ISNULL((select top 1 taxa from taxasiva where codigo=9),0)
			,ISNULL((select top 1 taxa from taxasiva where codigo=10),0)
			,ISNULL((select top 1 taxa from taxasiva where codigo=11),0)
			,ISNULL((select top 1 taxa from taxasiva where codigo=12),0)
			,ISNULL((select top 1 taxa from taxasiva where codigo=13),0)
			,'<<Alltrim(cabDoc.NRRECEITA)>>'
			,'<<Alltrim(cabdoc.pinacesso)>>'
			,'<<Alltrim(cabdoc.pinopcao)>>'
			,'<<Alltrim(cabdoc.estadoenc)>>'
			,'<<Alltrim(cabdoc.entrega)>>'
			,'<<Alltrim(cabdoc.pagamento)>>'
			,'<<Alltrim(cabdoc.momentopagam)>>'
			,<<lcTotalSemIvaDi10>>
			,<<lcTotalSemIvaDi11>>
			,<<lcTotalSemIvaDi12>>
			,<<lcTotalSemIvaDi13>>
			,<<lcValorIva10>>
			,<<lcValorIva11>>
			,<<lcValorIva12>>
			,<<lcValorIva13>>
			,'<<ALLTRIM(cabDoc.xpdLocalidade)>>'
		)
	ENDTEXT
	lcExecuteSQL  = lcExecuteSQL + Chr(13) + Chr(13) + lcSQL

	** Estado do documento
	Select cabdoc
	Go Top
	lcEstadoDocBO = Iif(Alltrim(cabdoc.ESTADODOC) = "F",1,0)

	uv_datEntr =  Iif(Empty(cabdoc.u_dataentr), '1900/01/01', uf_gerais_getdate(cabdoc.u_dataentr)) + ' ' + Iif(Empty(cabdoc.u_horaentr) Or Alltrim(cabdoc.u_horaentr) = ":", '00:00', Alltrim(cabdoc.u_horaentr))

	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE PRETEXT 8
		Insert into bo
		(
			bostamp
			,ndos
			,nmdos
			,obrano
			,dataobra
			,nome
			,nome2
			,no
			,estab
			,morada
			,local
			,codpost
			,tipo
			,tpdesc
			,ncont
			,boano
			,etotaldeb
			,dataopen
			,ecusto
			,etotal
			,moeda
			,memissao
			,origem
			,site
			,vendedor
			,vendnm
			,sqtt14
			,ebo_1tvall
			,ebo_2tvall
			,ebo_totp1
			,ebo_totp2
			,ebo11_bins
			,ebo12_bins
			,ebo21_bins
			,ebo22_bins
			,ebo31_bins
			,ebo32_bins
			,ebo41_bins
			,ebo42_bins
			,ebo51_bins
			,ebo52_bins
			,ebo61_bins
			,ebo62_bins
			,ebo11_iva
			,ebo12_iva
			,ebo21_iva
			,ebo22_iva
			,ebo31_iva
			,ebo32_iva
			,ebo41_iva
			,ebo42_iva
			,ebo51_iva
			,ebo52_iva
			,ebo62_iva
			,ocupacao
			,fechada
			,logi1
			,u_dataentr
			,obs
			,ousrinis
			,ousrdata
			,ousrhora
			,usrinis
			,usrdata
			,usrhora
			,datafinal
			,ccusto
			,pnome
			,pno
			,tabIva
			,EDESCC
		)
		Values (
			'<<Alltrim(cabDoc.cabStamp)>>'
			,<<CABDOC.NUMINTERNODOC>>
			,'<<Alltrim(cabDoc.Doc)>>'
			,'<<Alltrim(cabDoc.NUMDOC)>>'
			,'<<uf_gerais_getDate(cabDoc.DATADOC,"SQL")>>'
			,'<<ALLTRIM(cabDoc.Nome)>>'
			,''
			,<<cabDoc.NO>>
			,<<CabDoc.Estab>>
			,'<<ALLTRIM(cabDoc.morada)>>'
			,'<<ALLTRIM(cabDoc.local)>>'
			,'<<ALLTRIM(cabDoc.codPost)>>'
			,'<<ALLTRIM(cabdoc.tipo)>>'
			, '<<Alltrim(cabdoc.condpag)>>'
			,'<<ALLTRIM(cabDoc.ncont)>>'
			,Year('<<uf_gerais_getDate(cabDoc.DATADOC,"SQL")>>')
			,<<ROUND(cabDoc.BASEINC,2)>>
			,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
			,<<ROUND(lcTotalSemIvaDi,2)>>
			,<<ROUND(cabDoc.TOTAL,2)>>
			,'<<ALLTRIM(cabDoc.Moeda)>>'
			,'EURO'
			,'BO'
			, '<<ALLTRIM(cabDoc.Site)>>',
			<<ch_vendedor>>
			,'<<ALLTRIM(ch_vendnm)>>'
			,<<lcQtSum>>
			,<<ROUND(lcTotalSemIvaDi,2)>>
			,<<ROUND(lcTotalSemIvaDi,2)>>
			,<<ROUND(lcTotalSemIvaDi,2)>>
			,<<ROUND(lcTotalSemIvaDi,2)>>
			,<<lcTotalSemIvaDi1>>
			,<<lcTotalSemIvaDi1>>
			,<<lcTotalSemIvaDi2>>
			,<<lcTotalSemIvaDi2>>
			,<<lcTotalSemIvaDi3>>
			,<<lcTotalSemIvaDi3>>
			,<<lcTotalSemIvaDi4>>
			,<<lcTotalSemIvaDi4>>
			,<<lcTotalSemIvaDi5>>
			,<<lcTotalSemIvaDi5>>
			,<<lcTotalSemIvaDi6>>
			,<<lcTotalSemIvaDi6>>
			,<<lcValorIva1>>
			,<<lcValorIva1>>
			,<<lcValorIva2>>
			,<<lcValorIva2>>
			,<<lcValorIva3>>
			,<<lcValorIva3>>
			,<<lcValorIva4>>
			,<<lcValorIva4>>
			,<<lcValorIva5>>
			,<<lcValorIva5>>
			,<<lcValorIva6>>
			,4
			,<<lcEstadoDocBO>>
			,<<Iif(cabdoc.logi1 or !EMPTY(cabdoc.ATDocCode), 1, 0)>>
            ,'<<ALLTRIM(uv_datEntr)>>'
			,'<<Alltrim(cabDoc.obs)>>'
			,'<<m_chinis>>'
			, convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
			,'<<lnHora>>'
			,'<<m_chinis>>'
			, convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
			,'<<lnHora>>'
			,'<<uf_gerais_getDate(cabDoc.datafinal,"SQL")>>'
			,'<<cabdoc.ccusto>>'
			,'<<myTerm>>'
			,'<<myTermNo>>'
			,<<cabDoc.tabIva>>
			,<<cabDoc.EDESCC>>
		)
	ENDTEXT

	lcExecuteSQL  = lcExecuteSQL + Chr(13) + Chr(13) + lcSQL

	** Insere totais
	Select ucrsTotaisDocumento
	Go Top
	Scan
		TEXT TO lcSQL NOSHOW TEXTMERGE PRETEXT 8
			INSERT INTO bo_totais(
				stamp
				,taxa
				,baseinc
				,valoriva
                ,tabIva
			)values(
				'<<Alltrim(cabDoc.cabStamp)>>'
				,<<ucrsTotaisDocumento.taxa>>
				,<<ucrsTotaisDocumento.baseinc>>
				,<<ucrsTotaisDocumento.valoriva>>
                ,<<ucrsTotaisDocumento.tabIva>>
			)
		ENDTEXT

		lcExecuteSQL  = lcExecuteSQL + Chr(13) + Chr(13) + lcSQL

	Endscan

	Return lcExecuteSQL
Endfunc


**
Function uf_documentos_actualizaTabelaPrecos

	Select * From Bi Into Cursor ucrsBiTabelaPrecos Readwrite

	Select ucrsBiTabelaPrecos
	Go Top
	Scan For !Empty(ucrsBiTabelaPrecos.ref)

		lcStamp = uf_gerais_stamp()

		TEXT TO lcSQL NOSHOW TEXTMERGE

			IF (select COUNT(ref) from tabelaPrecos where ref = '<<ALLTRIM(ucrsBiTabelaPrecos.ref)>>' and fornec = <<cabdoc.no>> and fornestab = <<cabdoc.estab>>) = 0
			BEGIN
				INSERT INTO tabelaPrecos (
					ref
					,fornecedor
					,fornec
					,fornestab
					,PCL
					,PCT
					,PVP
					,desconto
					,tipo
					,data
				) VALUES (
					'<<ucrsBiTabelaPrecos.ref>>'
					,'<<cabdoc.nome>>'
					,<<cabdoc.no>>
					,<<cabdoc.estab>>
					,<<ucrsBiTabelaPrecos.u_upc>>
					,<<ucrsBiTabelaPrecos.edebito>>
					,0
					,<<ucrsBiTabelaPrecos.desconto>>
					,'FL'
					,dateadd(HOUR, <<difhoraria>>, getdate())
				)
			END
			ELSE
			BEGIN
				UPDATE
					tabelaPrecos
				SET
					PCL = <<ucrsBiTabelaPrecos.u_upc>>
					,PCT = <<ucrsBiTabelaPrecos.edebito>>
					,PVP = 0
					,desconto = <<ucrsBiTabelaPrecos.desconto>>
					,data = dateadd(HOUR, <<difhoraria>>, getdate())
				Where
					ref = '<<ucrsBiTabelaPrecos.ref>>'
					and fornec = <<cabdoc.no>>
					and fornestab = <<cabdoc.estab>>
			END
		ENDTEXT

		If !uf_gerais_actGrelha("","",lcSQL)
			uf_perguntalt_chama("OCORREU UMA ANOMALIA AO ATUALIZAR TABELA DE PRECOS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			Return .F.
		Endif
	Endscan
Endfunc


**
Function uf_documentos_insereCabFO
	Lparameters lcdoccont

	Local lcExecuteSQL
	lcExecuteSQL = ""

	Local lcValorIva1, lcValorIva2, lcValorIva3, lcValorIva4, lcValorIva5, lcValorIva6, lcValorIva7, lcValorIva8, lcValorIva9;
		,lcTotalSemIvaFO1, lcTotalSemIvaFO2, lcTotalSemIvaFO3, lcTotalSemIvaFO4, lcTotalSemIvaFO5, lcTotalSemIvaFO6;
		,lcTotalSemIvaFO7, lcTotalSemIvaFO8, lcTotalSemIvaFO9, lcTotalIva, lcCusto

	Store 0 To lcValorIva1,lcValorIva2,lcValorIva3,lcValorIva4,lcValorIva5,lcValorIva6,lcValorIva7,lcValorIva8,lcValorIva9;
		,lcTotalSemIvaFO1, lcTotalSemIvaFO2, lcTotalSemIvaFO3, lcTotalSemIvaFO4, lcTotalSemIvaFO5, lcTotalSemIvaFO6;
		,lcTotalSemIvaFO7, lcTotalSemIvaFO8, lcTotalSemIvaFO9, lcTotalIva, lcCusto

	*********************************

	If Upper(Alltrim(cabdoc.Doc)) <> "V/FACTURA RESUMO"

		Select FN
		Go Top
		Scan
			**CALCULA SUBTOTAIS IVA
			Do Case
				Case FN.tabiva=1
					If FN.ivaincl
						lcValorIva1=lcValorIva1 + FN.etiliquido - (FN.etiliquido/(FN.IVA/100+1))
						lcTotalSemIvaFO1 = lcTotalSemIvaFO1 + (FN.etiliquido/(FN.IVA/100+1))
					Else
						lcValorIva1=lcValorIva1+(FN.etiliquido*(FN.IVA/100))
						lcTotalSemIvaFO1 = lcTotalSemIvaFO1 + FN.etiliquido
					Endif
				Case FN.tabiva=2
					If FN.ivaincl
						lcValorIva2=lcValorIva2+ FN.etiliquido - (FN.etiliquido/(FN.IVA/100+1))
						lcTotalSemIvaFO2 = lcTotalSemIvaFO2 + (FN.etiliquido/(FN.IVA/100+1))
					Else
						lcValorIva2=lcValorIva2+(FN.etiliquido*(FN.IVA/100))
						lcTotalSemIvaFO2 = lcTotalSemIvaFO2 + FN.etiliquido
					Endif
				Case FN.tabiva=3
					If FN.ivaincl
						lcValorIva3=lcValorIva3+ FN.etiliquido - (FN.etiliquido/(FN.IVA/100+1))
						lcTotalSemIvaFO3 = lcTotalSemIvaFO3 + (FN.etiliquido/(FN.IVA/100+1))
					Else
						lcValorIva3=lcValorIva3+(FN.etiliquido*(FN.IVA/100))
						lcTotalSemIvaFO3 = lcTotalSemIvaFO3 + FN.etiliquido
					Endif
				Case FN.tabiva=4
					If FN.ivaincl
						lcValorIva4=lcValorIva4+ FN.etiliquido - (FN.etiliquido/(FN.IVA/100+1))
						lcTotalSemIvaFO4 = lcTotalSemIvaFO4 + (FN.etiliquido/(FN.IVA/100+1))
					Else
						lcValorIva4=lcValorIva4+(FN.etiliquido*(FN.IVA/100))
						lcTotalSemIvaFO4 = lcTotalSemIvaFO4 + FN.etiliquido
					Endif
				Case FN.tabiva=5
					If FN.ivaincl
						lcValorIva5=lcValorIva5+ FN.etiliquido - (FN.etiliquido/(FN.IVA/100+1))
						lcTotalSemIvaFO5 = lcTotalSemIvaFO5 + (FN.etiliquido/(FN.IVA/100+1))
					Else
						lcValorIva5=lcValorIva5+(FN.etiliquido*(FN.IVA/100))
						lcTotalSemIvaFO5 = lcTotalSemIvaFO5 + FN.etiliquido
					Endif
				Case FN.tabiva=6
					If FN.ivaincl
						lcValorIva6=lcValorIva6+ FN.etiliquido - (FN.etiliquido/(FN.IVA/100+1))
						lcTotalSemIvaFO6 = lcTotalSemIvaFO6 + (FN.etiliquido/(FN.IVA/100+1))
					Else
						lcValorIva6=lcValorIva6+(FN.etiliquido*(FN.IVA/100))
						lcTotalSemIvaFO6 = lcTotalSemIvaFO6 + FN.etiliquido
					Endif
				Case FN.tabiva=7
					If FN.ivaincl
						lcValorIva7=lcValorIva7+ FN.etiliquido - (FN.etiliquido/(FN.IVA/100+1))
						lcTotalSemIvaFO7 = lcTotalSemIvaFO7 + (FN.etiliquido/(FN.IVA/100+1))
					Else
						lcValorIva7=lcValorIva7+(FN.etiliquido*(FN.IVA/100))
						lcTotalSemIvaFO7 = lcTotalSemIvaFO7 + FN.etiliquido
					Endif
				Case FN.tabiva=8
					If FN.ivaincl
						lcValorIva8=lcValorIva8+ FN.etiliquido - (FN.etiliquido/(FN.IVA/100+1))
						lcTotalSemIvaFO8 = lcTotalSemIvaFO8 + (FN.etiliquido/(FN.IVA/100+1))
					Else
						lcValorIva8=lcValorIva8+(FN.etiliquido*(FN.IVA/100))
						lcTotalSemIvaFO8 = lcTotalSemIvaFO8 + FN.etiliquido
					Endif
				Case FN.tabiva=9
					If FN.ivaincl
						lcValorIva9=lcValorIva9+ FN.etiliquido - (FN.etiliquido/(FN.IVA/100+1))
						lcTotalSemIvaFO9 = lcTotalSemIvaFO9 + (FN.etiliquido/(FN.IVA/100+1))
					Else
						lcValorIva9=lcValorIva9+(FN.etiliquido*(FN.IVA/100))
						lcTotalSemIvaFO9 = lcTotalSemIvaFO9 + FN.etiliquido
					Endif
			Endcase

			lcTotalSemIvaFO = lcTotalSemIvaFO1 + lcTotalSemIvaFO2 + lcTotalSemIvaFO3 + lcTotalSemIvaFO4 + lcTotalSemIvaFO5 + lcTotalSemIvaFO6 + lcTotalSemIvaFO7 + lcTotalSemIvaFO8 + lcTotalSemIvaFO9
		Endscan

	Else

		Select ucrsTotaisDocumento
		Go Top
		Scan

			Do Case
				Case ucrsTotaisDocumento.tabiva=1

					lcValorIva1 = ucrsTotaisDocumento.valorIva
					lcTotalSemIvaFO1 = lcTotalSemIvaFO1 + ucrsTotaisDocumento.BASEINC

				Case ucrsTotaisDocumento.tabiva=2

					lcValorIva2 = ucrsTotaisDocumento.valorIva
					lcTotalSemIvaFO2 = lcTotalSemIvaFO2 + ucrsTotaisDocumento.BASEINC

				Case ucrsTotaisDocumento.tabiva=3

					lcValorIva3 = ucrsTotaisDocumento.valorIva
					lcTotalSemIvaFO3 = lcTotalSemIvaFO3 + ucrsTotaisDocumento.BASEINC

				Case ucrsTotaisDocumento.tabiva=4

					lcValorIva4 = ucrsTotaisDocumento.valorIva
					lcTotalSemIvaFO4 = lcTotalSemIvaFO4 + ucrsTotaisDocumento.BASEINC

				Case ucrsTotaisDocumento.tabiva=5

					lcValorIva5 = ucrsTotaisDocumento.valorIva
					lcTotalSemIvaFO5 = lcTotalSemIvaFO5 + ucrsTotaisDocumento.BASEINC

				Case ucrsTotaisDocumento.tabiva=6

					lcValorIva6 = ucrsTotaisDocumento.valorIva
					lcTotalSemIvaFO6 = lcTotalSemIvaFO6 + ucrsTotaisDocumento.BASEINC

				Case ucrsTotaisDocumento.tabiva=7

					lcValorIva7 = ucrsTotaisDocumento.valorIva
					lcTotalSemIvaFO7 = lcTotalSemIvaFO7 + ucrsTotaisDocumento.BASEINC

				Case ucrsTotaisDocumento.tabiva=8

					lcValorIva8 = ucrsTotaisDocumento.valorIva
					lcTotalSemIvaFO8 = lcTotalSemIvaFO8 + ucrsTotaisDocumento.BASEINC

				Case ucrsTotaisDocumento.tabiva=9

					lcValorIva9 = ucrsTotaisDocumento.valorIva
					lcTotalSemIvaFO9 = lcTotalSemIvaFO9 + ucrsTotaisDocumento.BASEINC

			Endcase

			lcTotalSemIvaFO = lcTotalSemIvaFO1 + lcTotalSemIvaFO2 + lcTotalSemIvaFO3 + lcTotalSemIvaFO4 + lcTotalSemIvaFO5 + lcTotalSemIvaFO6 + lcTotalSemIvaFO7 + lcTotalSemIvaFO8 + lcTotalSemIvaFO9


			Select ucrsTotaisDocumento
		Endscan

	Endif

	** Retira Desconto Financeiro
	If !(cabdoc.DESCFIN == 0)
		If lcValorIva1 > 0
			lcValorIva1 = (lcValorIva1 / ((cabdoc.DESCFIN/100) + 1))
		Endif
		If lcValorIva2 > 0
			lcValorIva2 = (lcValorIva2 / ((cabdoc.DESCFIN/100) + 1))
		Endif
		If lcValorIva3 > 0
			lcValorIva3 = (lcValorIva3 / ((cabdoc.DESCFIN/100) + 1))
		Endif
		If lcValorIva4 > 0
			lcValorIva4 = (lcValorIva4 / ((cabdoc.DESCFIN/100) + 1))
		Endif
		If lcValorIva5 > 0
			lcValorIva5 = (lcValorIva5 / ((cabdoc.DESCFIN/100) + 1))
		Endif
		If lcValorIva6 > 0
			lcValorIva6 = (lcValorIva6 / ((cabdoc.DESCFIN/100) + 1))
		Endif
		If lcValorIva7 > 0
			lcValorIva7 = (lcValorIva7 / ((cabdoc.DESCFIN/100) + 1))
		Endif
		If lcValorIva8 > 0
			lcValorIva8 = (lcValorIva8 / ((cabdoc.DESCFIN/100) + 1))
		Endif
		If lcValorIva9 > 0
			lcValorIva9 = (lcValorIva9 / ((cabdoc.DESCFIN/100) + 1))
		Endif
	Endif

	lcValorIva1 = Round(lcValorIva1,2)
	lcValorIva2 = Round(lcValorIva2,2)
	lcValorIva3 = Round(lcValorIva3,2)
	lcValorIva4 = Round(lcValorIva4,2)
	lcValorIva5 = Round(lcValorIva5,2)
	lcValorIva6 = Round(lcValorIva6,2)
	lcValorIva7 = Round(lcValorIva7,2)
	lcValorIva8 = Round(lcValorIva8,2)
	lcValorIva9 = Round(lcValorIva9,2)
	lcTotalIva = lcValorIva1 + lcValorIva2 + lcValorIva3 + lcValorIva4 + lcValorIva5 + lcValorIva6 + lcValorIva7 + lcValorIva8 + lcValorIva9

	Select cabdoc
	lcSQL=''
	TEXT TO lcSql NOSHOW TEXTMERGE PRETEXT 8
		INSERT INTO fo2	(fo2stamp,ivatx1,ivatx2,ivatx3,ivatx4,ivatx5,ivatx6,ivatx7,ivatx8,ivatx9,u_doccont,u_docfl,u_docflno,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora,u_armazem)
		values ('<<Alltrim(cabDoc.cabStamp)>>',(select taxa from taxasiva (nolock) where codigo=1),(select taxa from taxasiva (nolock) where codigo=2),(select taxa from taxasiva (nolock) where codigo=3)
				,(select taxa from taxasiva (nolock) where codigo=4),(select taxa from taxasiva (nolock) where codigo=5),(select taxa from taxasiva (nolock) where codigo=6),(select taxa from taxasiva (nolock) where codigo=7)
				,(select taxa from taxasiva (nolock) where codigo=8),(select taxa from taxasiva (nolock) where codigo=9),<<IIF(EMPTY(lcdoccont),0,lcdoccont)>>,'<<alltrim(cabdoc.docfl)>>','<<Alltrim(cabdoc.docflno)>>'
				,'<<m_chinis>>', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8),'<<m_chinis>>', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8),<<cabDoc.Armazem>>)
	ENDTEXT
	**
	lcExecuteSQL  = lcExecuteSQL + Chr(13) + Chr(13) + lcSQL


	*INSERE FO,FO2
	lcSQL = ""
	TEXT TO lcSql NOSHOW TEXTMERGE PRETEXT 8
		INSERT INTO FO(
			fostamp,docnome,doccode,nome,no, estab,adoc, morada,
			local, codpost, ncont, data, docdata, foano, pdata,
			tpdesc, moeda, memissao, moeda2, ollocal, telocal, final,
			total, etotal, ivain, eivain, ttiva, ettiva, ettiliq, ivainsns,
			eivainsns, ivav1, eivav1, ivav2, eivav2, ivav3, eivav3,
			ivav4, eivav4, ivav5, eivav5, ivav6, eivav6, ivav7, eivav7,
			ivav8, eivav8, ivav9, eivav9,  epaivav1, epaivav2,	epaivav3, epaivav4,
			epaivav5, epaivav6, epaivav7, epaivav8,	epaivav9,
			epaivain, patotal, epatotal, site, pnome, pno, u_status,
			EAIVAIN, EAIVAV1, EAIVAV2, EAIVAV3, EAIVAV4,
			EAIVAV5, EAIVAV6, EAIVAV7, EAIVAV8, EAIVAV9
			,ousrinis
			, ousrdata
			, ousrhora
			, usrinis
			, usrdata
			,usrhora
			, fin
			, efinv
			, pais
			, ccusto
			, plano
			, tabIva
			,bloqpag
			, EDESCC
			, consignacao
			, id_tesouraria_conta
			, imput_desp
		)VALUES(
			'<<Alltrim(cabDoc.cabStamp)>>'
			,'<<ALLTRIM(cabDoc.DOC)>>'
			,<<cabDoc.NUMINTERNODOC>>
			,'<<ALLTRIM(cabDoc.NOME)>>'
			,<<cabDoc.NO>>
			,<<cabDoc.ESTAB>>
			,'<<Alltrim(cabDoc.NUMDOC)>>'
			,'<<ALLTRIM(cabDoc.MORADA)>>'
			,'<<ALLTRIM(cabDoc.LOCAL)>>'
			,'<<ALLTRIM(cabDoc.codpost)>>'
			,'<<ALLTRIM(cabDoc.ncont)>>'
			,'<<uf_gerais_getDate(cabDoc.DATAINTERNA,"SQL")>>'
			,'<<uf_gerais_getDate(cabDoc.DATADOC,"SQL")>>'
			,<<YEAR(cabDoc.DATADOC)>>
			,'<<uf_gerais_getDate(cabDoc.DATAVENC,"SQL")>>'
			,'<<Alltrim(cabdoc.condpag)>>'
			,'<<Alltrim(CabDOC.MOEDA)>>'
			,'EURO'
			,'EURO'
			,'Caixa'
			,'C'
			,'<<Alltrim(cabDoc.obs)>>'
			,<<Round(cabDoc.TOTAL*200.482,2)>>
			,<<Round(cabDoc.TOTAL,2)>>
			,<<Round(cabDoc.BASEINC*200.482,2)>>
			,<<Round(cabDoc.BASEINC,2)>>
			,<<Round(cabDoc.IVA*200.482,2)>>
			,<<Round(cabDoc.IVA,2)>>
			,<<Round(cabDoc.BASEINC,2)>>
			,<<Round(cabDoc.totalNservico*200.482,2)>>
			,<<Round(cabDoc.totalNservico,2)>>
			,<<lcValorIva1*200.482>>
			,<<lcValorIva1>>
			,<<lcValorIva2*200.482>>
			,<<lcValorIva2>>
			,<<lcValorIva3*200.482>>
			,<<lcValorIva3>>
			,<<lcValorIva4*200.482>>
			,<<lcValorIva4>>
			,<<lcValorIva5*200.482>>
			,<<lcValorIva5>>
			,<<lcValorIva6*200.482>>
			,<<lcValorIva6>>
			,<<lcValorIva7*200.482>>
			,<<lcValorIva7>>
			,<<lcValorIva8*200.482>>
			,<<lcValorIva8>>
			,<<lcValorIva9*200.482>>
			,<<lcValorIva9>>
			,<<lcValorIva1>>
			,<<lcValorIva2>>
			,<<lcValorIva3>>
			,<<lcValorIva4>>
			,<<lcValorIva5>>
			,<<lcValorIva6>>
			,<<lcValorIva7>>
			,<<lcValorIva8>>
			,<<lcValorIva9>>
			,<<cabDoc.BASEINC>>
			,<<cabDoc.TOTAL*200.482>>
			,<<ROUND(cabDoc.TOTAL,2)>>
			,'<<ALLTRIM(cabdoc.Site)>>'
			,'<<ALLTRIM(myTerm)>>'
			,<<myTermNo>>
			,'<<Alltrim(CabDOC.ESTADODOC)>>',
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0
			, '<<m_chinis>>'
			, convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
			, convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8)
			,'<<m_chinis>>'
			, convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
			, convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8)
			,<<CabDoc.DESCFIN>>
			,<<Round(CabDoc.VALORDESCFIN,2)>>
			,<<cabdoc.pais>>
			,'<<cabdoc.ccusto>>'
			,<<IIF(cabdoc.plano,1,0)>>
			,<<cabdoc.tabIva>>
			,<<IIF(cabdoc.bloqpag,1,0)>>
			,<<cabDoc.EDESCC>>
			,<<IIF(CabDoc.consignacao,1,0)>>
			,<<CabDoc.id_tesouraria_conta>>
			, <<cabdoc.imput_desp>>
		)
	ENDTEXT

	lcExecuteSQL  = lcExecuteSQL + Chr(13) + Chr(13) + lcSQL

	** Insere totais
	Select ucrsTotaisDocumento
	Go Top
	Scan
		TEXT TO lcSQL NOSHOW TEXTMERGE PRETEXT 8
			INSERT INTO fo_totais(
				stamp
				,taxa
				,baseinc
				,valoriva
				,arredondamento
				,valoraposarredondamento
                ,tabIva
			)values(
				'<<Alltrim(cabDoc.cabStamp)>>'
				,<<ucrsTotaisDocumento.taxa>>
				,<<ucrsTotaisDocumento.baseinc>>
				,<<ucrsTotaisDocumento.valoriva>>
				,<<ucrsTotaisDocumento.arredondamento>>
				,<<ucrsTotaisDocumento.valoraposarredondamento>>
                ,<<ucrsTotaisDocumento.tabIva>>
			)
		ENDTEXT

		lcExecuteSQL  = lcExecuteSQL + Chr(13) + Chr(13) + lcSQL

	Endscan

	Return lcExecuteSQL
Endfunc


**
Function uf_documentos_contadorTipoDocumento

	** Incrementa Contador por tipo de documento e origem **
	Local lcDocCont2, lcSQL
	Store 0 To lcDocCont2

	If myTipoDoc == "BO"
		lcSQL = ""
		TEXT TO lcSql NOSHOW textmerge
			select ISNULL(MAX(bo2.u_doccont),0) as doccont
			from bo2 (nolock)
			inner join bo (nolock) on bo.bostamp=bo2.bo2stamp
			where bo.nmdos='<<Alltrim(cabDoc.DOC)>>'
		ENDTEXT
	Endif

	If myTipoDoc == "FO"
		lcSQL = ""
		TEXT TO lcSql NOSHOW textmerge
			select ISNULL(MAX(fo2.u_doccont),0) as doccont
			from fo2 (nolock)
			inner join fo (nolock) on fo.fostamp=fo2.fo2stamp
			where fo.docnome='<<Alltrim(cabDoc.DOC)>>'
		ENDTEXT
	Endif

	If !uf_gerais_actGrelha("",[uCrsContDoc],lcSQL)
		uf_perguntalt_chama("N�O FOI POSS�VEL INCREMENTAR CONTADOR POR TIPO DE DOCUMENTO E ORIGEM. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
	Else
		Select uCrsContDoc
		If Reccount()>0
			lcDocCont2 = uCrsContDoc.doccont + 1
		Else
			lcDocCont2 = 1
		Endif
	Endif

	Return lcDocCont2
Endfunc


**ACTUALIZA CABE�ALHO - BO
Function uf_documentos_actualizaCabBO
	Lparameters lcTabela1,  lcTabela2, lcTabela3, lcDataAlt, lcHoraAlt



	If(Empty(lcTabela1))
		lcTabela1 = "BO"
	Endif


	If(Empty(lcTabela2))
		lcTabela2 = "BO2"
	Endif

	If(Empty(lcTabela3))
		lcTabela3= "bo_totais"
	Endif




	Local lcExecuteSQL
	lcExecuteSQL = ""

	Local lcValorIva1, lcValorIva2, lcValorIva3, lcValorIva4, lcValorIva5, lcValorIva6, lcValorIva7, lcValorIva8, lcValorIva9, lcValorIva10 ,lcValorIva11, lcValorIva12, lcValorIva13, lcTotal;
		,lcTotalSemIvaDi1, lcTotalSemIvaDi2, lcTotalSemIvaDi3, lcTotalSemIvaDi4, lcTotalSemIvaDi5, lcTotalSemIvaDi6, lcTotalSemIvaDi7;
		,lcTotalSemIvaDi8, lcTotalSemIvaDi9,lcTotalSemIvaDi10,lcTotalSemIvaDi11, lcTotalSemIvaDi12, lcTotalSemIvaDi13, lcCusto, lcQtSum, lcTotalIva, lcEstadoDocBO, lcdoccont

	Store 0 To lcValorIva1, lcValorIva2, lcValorIva3, lcValorIva4, lcValorIva5, lcValorIva6, lcValorIva7, lcValorIva8, lcValorIva9, lcValorIva10 ,lcValorIva11, lcValorIva12, lcValorIva13, lcTotal;
		,lcTotalSemIvaDi1, lcTotalSemIvaDi2, lcTotalSemIvaDi3, lcTotalSemIvaDi4, lcTotalSemIvaDi5, lcTotalSemIvaDi6, lcTotalSemIvaDi7;
		,lcTotalSemIvaDi8, lcTotalSemIvaDi9,lcTotalSemIvaDi10,lcTotalSemIvaDi11, lcTotalSemIvaDi12, lcTotalSemIvaDi13, lcCusto, lcQtSum, lcTotalIva, lcEstadoDocBO, lcdoccont

	Set Point To "."

	** CALCULA TOTAIS
	totalciva = 0
	Select Bi
	Go Top
	Scan
		lcQtSum = lcQtSum + Bi.qtt
		lcCusto = lcCusto + Bi.EPCUSTO

		** CALCULA SUBTOTAIS IVA
		Do Case
			Case Bi.tabiva=1
				If Bi.ivaincl
					lcValorIva1=lcValorIva1 + Bi.ettdeb - (Bi.ettdeb/(Bi.IVA/100+1))
					lcTotalSemIvaDi1	= lcTotalSemIvaDi1 + (Bi.ettdeb/(Bi.IVA/100+1))
				Else
					lcValorIva1=lcValorIva1+(Bi.ettdeb*(Bi.IVA/100))
					lcTotalSemIvaDi1	= lcTotalSemIvaDi1 + Bi.ettdeb
				Endif


			Case Bi.tabiva=2
				If Bi.ivaincl
					lcValorIva2=lcValorIva2 + Bi.ettdeb - (Bi.ettdeb/(Bi.IVA/100+1))
					lcTotalSemIvaDi2	= lcTotalSemIvaDi2 + (Bi.ettdeb/(Bi.IVA/100+1))
				Else
					lcValorIva2=lcValorIva2+(Bi.ettdeb*(Bi.IVA/100))
					lcTotalSemIvaDi2	= lcTotalSemIvaDi2 + Bi.ettdeb
				Endif
			Case Bi.tabiva=3
				If Bi.ivaincl
					lcValorIva3=lcValorIva3 + Bi.ettdeb - (Bi.ettdeb/(Bi.IVA/100+1))
					lcTotalSemIvaDi3	= lcTotalSemIvaDi3 + (Bi.ettdeb/(Bi.IVA/100+1))
				Else
					lcValorIva3=lcValorIva3+(Bi.ettdeb*(Bi.IVA/100))
					lcTotalSemIvaDi3	= lcTotalSemIvaDi3 + Bi.ettdeb
				Endif
			Case Bi.tabiva=4
				If Bi.ivaincl
					lcValorIva4 = lcValorIva4 + Bi.ettdeb - (Bi.ettdeb/(Bi.IVA/100+1))
					lcTotalSemIvaDi4	= lcTotalSemIvaDi4 + (Bi.ettdeb/(Bi.IVA/100+1))
				Else
					lcValorIva4=lcValorIva4+(Bi.ettdeb*(Bi.IVA/100))
					lcTotalSemIvaDi4	= lcTotalSemIvaDi4 + Bi.ettdeb
				Endif
			Case Bi.tabiva=5
				If Bi.ivaincl
					lcValorIva5 = lcValorIva5 + Bi.ettdeb - (Bi.ettdeb/(Bi.IVA/100+1))
					lcTotalSemIvaDi5	= lcTotalSemIvaDi5 + (Bi.ettdeb/(Bi.IVA/100+1))
				Else
					lcValorIva5=lcValorIva5+(Bi.ettdeb*(Bi.IVA/100))
					lcTotalSemIvaDi5	= lcTotalSemIvaDi5 + Bi.ettdeb
				Endif
			Case Bi.tabiva=6
				If Bi.ivaincl
					lcValorIva6=lcValorIva6 + Bi.ettdeb - (Bi.ettdeb/(Bi.IVA/100+1))
					lcTotalSemIvaDi6	= lcTotalSemIvaDi6 + (Bi.ettdeb/(Bi.IVA/100+1))
				Else
					lcValorIva6=lcValorIva6+(Bi.ettdeb*(Bi.IVA/100))
					lcTotalSemIvaDi6	= lcTotalSemIvaDi6 + Bi.ettdeb
				Endif
			Case Bi.tabiva=7
				If Bi.ivaincl
					lcValorIva7=lcValorIva7 + Bi.ettdeb - (Bi.ettdeb/(Bi.IVA/100+1))
					lcTotalSemIvaDi7	= lcTotalSemIvaDi7 + (Bi.ettdeb/(Bi.IVA/100+1))
				Else
					lcValorIva7=lcValorIva7+(Bi.ettdeb*(Bi.IVA/100))
					lcTotalSemIvaDi7	= lcTotalSemIvaDi7 + Bi.ettdeb
				Endif
			Case Bi.tabiva=8
				If Bi.ivaincl
					lcValorIva8=lcValorIva8 + Bi.ettdeb - (Bi.ettdeb/(Bi.IVA/100+1))
					lcTotalSemIvaDi8	= lcTotalSemIvaDi8 + (Bi.ettdeb/(Bi.IVA/100+1))
				Else
					lcValorIva8=lcValorIva8+(Bi.ettdeb*(Bi.IVA/100))
					lcTotalSemIvaDi8	= lcTotalSemIvaDi8 + Bi.ettdeb
				Endif
			Case Bi.tabiva=9
				If Bi.ivaincl
					lcValorIva9 = lcValorIva9 + Bi.ettdeb - (Bi.ettdeb/(Bi.IVA/100+1))
					lcTotalSemIvaDi9	= lcTotalSemIvaDi9 + (Bi.ettdeb/(Bi.IVA/100+1))
				Else
					lcValorIva9=lcValorIva9+(Bi.ettdeb*(Bi.IVA/100))
					lcTotalSemIvaDi9	= lcTotalSemIvaDi9 + Bi.ettdeb
				Endif
			Case Bi.tabiva=10
				If Bi.ivaincl
					lcValorIva10 = lcValorIva10 + Bi.ettdeb - (Bi.ettdeb/(Bi.IVA/100+1))
					lcTotalSemIvaDi10	= lcTotalSemIvaDi10 + (Bi.ettdeb/(Bi.IVA/100+1))
				Else
					lcValorIva10=lcValorIva10+(Bi.ettdeb*(Bi.IVA/100))
					lcTotalSemIvaDi10	= lcTotalSemIvaDi10 + Bi.ettdeb
				Endif
			Case Bi.tabiva=11
				If Bi.ivaincl
					lcValorIva11 = lcValorIva11 + Bi.ettdeb - (Bi.ettdeb/(Bi.IVA/100+1))
					lcTotalSemIvaDi11	= lcTotalSemIvaDi11 + (Bi.ettdeb/(Bi.IVA/100+1))
				Else
					lcValorIva12=lcValorIva12+(Bi.ettdeb*(Bi.IVA/100))
					lcTotalSemIvaDi12	= lcTotalSemIvaDi12 + Bi.ettdeb
				Endif
			Case Bi.tabiva=13
				If Bi.ivaincl
					lcValorIva13 = lcValorIva13 + Bi.ettdeb - (Bi.ettdeb/(Bi.IVA/100+1))
					lcTotalSemIvaDi13	= lcTotalSemIvaDi13 + (Bi.ettdeb/(Bi.IVA/100+1))
				Else
					lcValorIva13=lcValorIva13+(Bi.ettdeb*(Bi.IVA/100))
					lcTotalSemIvaDi13	= lcTotalSemIvaDi13 + Bi.ettdeb
				Endif
		Endcase

		lcTotalSemIvaDI = lcTotalSemIvaDi1 + lcTotalSemIvaDi2 + lcTotalSemIvaDi3 + lcTotalSemIvaDi4 + lcTotalSemIvaDi5 + lcTotalSemIvaDi6 + lcTotalSemIvaDi7 + lcTotalSemIvaDi8 + lcTotalSemIvaDi9 + lcTotalSemIvaDi10 + lcTotalSemIvaDi11 + lcTotalSemIvaDi12 + lcTotalSemIvaDi13

	Endscan

	lcTotalIva = lcValorIva1 + lcValorIva2 + lcValorIva3 + lcValorIva4 + lcValorIva5 + lcValorIva6 + lcValorIva7 + lcValorIva8 + lcValorIva9 + lcValorIva10 + lcValorIva11 + lcValorIva12 + lcValorIva13
	lcTotal = lcTotalSemIvaDI+lcTotalIva

	Select cabdoc
	Go Top
	lcEstadoDocBO = Iif(Alltrim(cabdoc.ESTADODOC) == "F",1,0)

	uv_datEntr =  Iif(Empty(cabdoc.u_dataentr), '1900/01/01', uf_gerais_getdate(cabdoc.u_dataentr)) + ' ' + Iif(Empty(cabdoc.u_horaentr) Or Alltrim(cabdoc.u_horaentr) = ":", '00:00', Alltrim(cabdoc.u_horaentr))

	* UPDATE BO, BO2 *


	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE PRETEXT 8
		UPDATE
			<<ALLTRIM(lcTabela1)>>
		SET
			ndos		=	<<cabDoc.numinternodoc>>,
			nmdos		=	'<<alltrim(cabDoc.Doc)>>',
			obrano		=	<<Alltrim(cabDoc.NUMDOC)>>,
			dataobra	=	'<<uf_gerais_getDate(cabDoc.DATADOC,"SQL")>>',
			nome		=	'<<ALLTRIM(cabDoc.Nome)>>',
			nome2		=	'<<Alltrim(cabdoc.nome2)>>',
			no			=	<<cabDoc.NO>>,
			estab		=	<<CabDoc.Estab>>,
			morada		=	'<<ALLTRIM(cabDoc.morada)>>',
			local		=	'<<ALLTRIM(cabDoc.local)>>',
			codpost		=	'<<ALLTRIM(cabDoc.codPost)>>',
			tipo		=	'<<ALLTRIM(cabdoc.tipo)>>',
			ncont		=	'<<ALLTRIM(cabDoc.ncont)>>',
			tpdesc		=	'<<Alltrim(cabdoc.condpag)>>',
			boano		=	Year('<<uf_gerais_getDate(cabDoc.DATADOC,"SQL")>>'),
			etotaldeb	=	<<ROUND(lcTotalSemIvaDI,2)>>,
			dataopen	=	convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102),
			ecusto		=	<<ROUND(lcTotalSemIvaDi,2)>>,
			etotal		=	<<ROUND(lcTotal,2)>>,
			moeda		=	'<<ALLTRIM(cabDoc.Moeda)>>',
			memissao	=	'EURO',
			origem		=	'BO',
			site		=	'<<ALLTRIM(cabDoc.site)>>',
			vendedor	=	<<ch_vendedor>>,
			vendnm		=	'<<ALLTRIM(ch_vendnm)>>',
			sqtt14		=	<<lcQtSum>>,
			ebo_1tvall	=	<<ROUND(lcTotalSemIvaDi,2)>>,
			ebo_2tvall	=	<<ROUND(lcTotalSemIvaDi,2)>>,
			ebo_totp1	=	<<ROUND(lcTotalSemIvaDi,2)>>,
			ebo_totp2	=	<<ROUND(lcTotalSemIvaDi,2)>>,
			ebo11_bins	=	<<lcTotalSemIvaDi1>>,
			ebo12_bins	=	<<lcTotalSemIvaDi1>>,
			ebo21_bins	=	<<lcTotalSemIvaDi2>>,
			ebo22_bins	=	<<lcTotalSemIvaDi2>>,
			ebo31_bins	=	<<lcTotalSemIvaDi3>>,
			ebo32_bins	=	<<lcTotalSemIvaDi3>>,
			ebo41_bins	=	<<lcTotalSemIvaDi4>>,
			ebo42_bins	=	<<lcTotalSemIvaDi4>>,
			ebo51_bins	=	<<lcTotalSemIvaDi5>>,
			ebo52_bins	=	<<lcTotalSemIvaDi5>>,
			ebo11_iva	=	<<lcValorIva1>>,
			ebo12_iva	=	<<lcValorIva1>>,
			ebo21_iva	=	<<lcValorIva2>>,
			ebo22_iva	=	<<lcValorIva2>>,
			ebo31_iva	=	<<lcValorIva3>>,
			ebo32_iva	=	<<lcValorIva3>>,
			ebo41_iva	=	<<lcValorIva4>>,
			ebo42_iva	=	<<lcValorIva4>>,
			ebo51_iva	=	<<lcValorIva5>>,
			ebo52_iva	=	<<lcValorIva5>>,
			ocupacao	=	4,
			fechada		=	<<lcEstadoDocBO>>,
			dataFecho 	= 	<<IIF(lcEstadoDocBO = 1, IIF(!EMPTY(lcDataAlt), "'" + uf_gerais_getDate(lcDataAlt) + " " + IIF(!EMPTY(lcHoraAlt), ALLTRIM(lcHoraAlt),"") + "'", 'convert(varchar,dateadd(HOUR,' + ASTR(difhoraria) + ', getdate()),102)'), 'dataFecho')>>,
			usrinis		=	'<<m_chinis>>',
			usrdata		=	<<IIF(!EMPTY(lcDataAlt), "'" + uf_gerais_getDate(lcDataAlt, 'SQL') + "'", 'convert(varchar,dateadd(HOUR,' + ASTR(difhoraria) + ', getdate()),102)')>>,
			usrhora		=	<<IIF(!EMPTY(lcHoraAlt), "'" + ALLTRIM(lcHoraAlt) + "'", 'convert(varchar,dateadd(HOUR,' + ASTR(difhoraria) + ', getdate()),8)')>>,
			Logi1		=	<<Iif(cabdoc.logi1, 1, 0)>>,
			u_dataentr	=	'<<alltrim(uv_datEntr)>>',
			obs			=	'<<Alltrim(cabDoc.obs)>>',
			datafinal	=	'<<uf_gerais_getDate(cabdoc.datafinal,"SQL")>>',
			ccusto		=	'<<cabdoc.ccusto>>',
			EDESCC		= 	<<cabDoc.EDESCC>>
		WHERE
			BOSTAMP =  '<<Alltrim(cabDoc.cabstamp)>>'
	ENDTEXT

	lcExecuteSQL  = lcExecuteSQL + Chr(13) + Chr(13) + lcSQL

	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE PRETEXT 8
		UPDATE
			<<ALLTRIM(lcTabela2)>>
		SET
			autotipo	= 1,
			pdtipo		= 1,
			etotalciva	= <<ROUND(lcTotalSemIvaDI,2)>>,
			u_doccont 	= <<lcdoccont>>,
			ETOTIVA		= <<Round(lcTotalIva,2)>>,
			telefone 	= '<<Alltrim(cabdoc.xpdtelefone)>>',
			email 		= '<<Alltrim(cabdoc.xpdemail)>>',
			usrinis		= '<<m_chinis>>',
			usrdata		=	<<IIF(!EMPTY(lcDataAlt), "'" + uf_gerais_getDate(lcDataAlt, 'SQL') + "'", 'convert(varchar,dateadd(HOUR,' + ASTR(difhoraria) + ', getdate()),102)')>>,
			usrhora		=	<<IIF(!EMPTY(lcHoraAlt), "'" + ALLTRIM(lcHoraAlt) + "'", 'convert(varchar,dateadd(HOUR,' + ASTR(difhoraria) + ', getdate()),8)')>>,
			armazem 	= <<cabDoc.Armazem>>,
			u_class		= '<<Alltrim(cabdoc.u_class)>>',
			xpddata		= '<<uf_gerais_getDate(cabdoc.xpddata,"SQL")>>',
			xpdhora		= '<<Alltrim(cabdoc.xpdhora)>>',
			xpdviatura	= '<<Alltrim(cabdoc.xpdviatura)>>',
			morada		= '<<Alltrim(cabdoc.xpdmorada)>>',
			contacto	= '<<Alltrim(cabdoc.xpdcontacto)>>',
			codpost		= '<<Alltrim(cabdoc.xpdcodpost)>>',
			ATDocCode	= '<<ALLTRIM(cabdoc.ATDocCode)>>',
			nrReceita   ='<<ALLTRIM(cabdoc.NRRECEITA)>>',
			nrviasimp	= <<DOCUMENTOS.PageFrame1.Page1.DetalheSt1.pageFrame1.page11.ContainerOutrosDados.NRVIAS.value>>,
			status 		= '<<Alltrim(cabdoc.estadoenc)>>',
			modo_envio 	= '<<Alltrim(cabdoc.entrega)>>',
			pagamento 	= '<<Alltrim(cabdoc.pagamento)>>',
			momento_pagamento = '<<Alltrim(cabdoc.momentopagam)>>',
			pinacesso = '<<Alltrim(cabdoc.pinacesso)>>',
			pinopcao = '<<Alltrim(cabdoc.pinopcao)>>',
			xpdLocalidade = '<<ALLTRIM(cabdoc.xpdLocalidade)>>'
		FROM
			BO2
		WHERE
			BO2STAMP =   '<<Alltrim(cabDoc.cabstamp)>>'
	ENDTEXT

	lcExecuteSQL  = lcExecuteSQL + Chr(13) + Chr(13) + lcSQL

	** Actualiza totais
	TEXT TO lcSQL NOSHOW TEXTMERGE PRETEXT 8
		DELETE from bo_totais WHERE stamp = '<<Alltrim(cabDoc.cabStamp)>>'
	ENDTEXT

	lcExecuteSQL  = lcExecuteSQL + Chr(13) + Chr(13) + lcSQL

	Select ucrsTotaisDocumento
	Go Top
	Scan
		TEXT TO lcSQL NOSHOW TEXTMERGE PRETEXT 8
			INSERT INTO <<ALLTRIM(lcTabela3)>>(
				stamp
				,taxa
				,baseinc
				,valoriva
                ,tabIva
			)values(
				'<<Alltrim(cabDoc.cabStamp)>>'
				,<<ucrsTotaisDocumento.taxa>>
				,<<ucrsTotaisDocumento.baseinc>>
				,<<ucrsTotaisDocumento.valoriva>>
                ,<<ucrsTotaisDocumento.tabIva>>
			)
		ENDTEXT

		lcExecuteSQL  = lcExecuteSQL + Chr(13) + Chr(13) + lcSQL

	Endscan

	Return lcExecuteSQL
Endfunc


*!*	** Funcao Insert / Update Linhas BI antiga
Function uf_documentos_actualizaLinhasBI
	Lparameters lcTabela1, lcTabela2

	If(Empty(lcTabela1))
		lcTabela1 = "BI"
	Endif

	If(Empty(lcTabela2))
		lcTabela2 = "BI2"
	Endif

	Local lcExecuteSQL, lcContador, lcTotalRegistos ,lcBistamp,lcDoc
	Store '' To lcExecuteSQL, lcBistamp
	Store 0 To lcContador

	Select cabdoc
	lcDoc = cabdoc.Doc

	Select cabdoc
	lcEstadoDocBO = Iif(Alltrim(cabdoc.ESTADODOC) == "F",1,0)

	&& update datas linhas em caso de altera��o do cabe�alho
	Select cabdoc
	Update Bi Set DATAOBRA = cabdoc.DATADOC, DATAFINAL= cabdoc.DATADOC, rdata= cabdoc.DATADOC

	&& Apagar as Linhas, que j� n�o existem no cursor
	Select ucrsBiCompare.bistamp As bistamp_compare, Bi.bistamp  From ucrsBiCompare Left Join Bi On Bi.bistamp = ucrsBiCompare.bistamp Into Cursor ucrsBiCompareInsertDelete Readwrite
	Select ucrsBiCompareInsertDelete
	Go Top
	Scan For Isnull(ucrsBiCompareInsertDelete.bistamp) == .T.
		lcSQL = ""
		TEXT TO lcSQL NOSHOW TEXTMERGE PRETEXT 8
			DELETE FROM	<<ALLTRIM(lcTabela1)>> WHERE BISTAMP = '<<ALLTRIM(ucrsBiCompareInsertDelete.bistamp_compare)>>'
			DELETE FROM	<<ALLTRIM(lcTabela2)>> WHERE BI2STAMP = '<<ALLTRIM(ucrsBiCompareInsertDelete.bistamp_compare)>>'
		ENDTEXT
		lcExecuteSQL  = lcExecuteSQL + Chr(13) + Chr(13) + lcSQL
	Endscan

	&& UPDATE LINHAS BI
	lcContador = 0
	Select Bi
	Go Top
	Scan
		** resfor=1 quando o documento for encomenda de fornecedor
		Local lcResFor
		lcResFor = Iif(Upper(Alltrim(cabdoc.Doc))=="ENCOMENDA A FORNECEDOR",1,0)

		** rescli=1 quando o documento for reserva de cliente
		Local lcResCli
		lcResCli = Iif(Upper(Alltrim(cabdoc.Doc))=="RESERVA DE CLIENTE",1,0)

		Local lcBiComunicadaSucesso
		lcBiComunicadaSucesso = Iif(Bi.comunicada_ws,1,0)

		lcSQLBi1 = ""
		TEXT TO lcSQLBi1 NOSHOW TEXTMERGE PRETEXT 8

			<<IIF(myDocIntroducao==.t., '', 'DELETE from bi where bistamp = '+ ['] + alltrim(BI.bistamp)+['])>>

			INSERT INTO <<ALLTRIM(lcTabela1)>>(bistamp, bostamp, nmdos, ndos, obrano, ref, codigo, design, qtt, qtt2, uni2qtt, u_psicont, u_bencont, u_stockact
							,iva, tabiva, armazem, ar2mazem, stipo, cpoc,u_bonus, desconto, desc2, desc3, desc4, familia,no,nome,local,morada
							,codpost,epu,edebito,epcusto,ettdeb,ecustoind,edebitoori,u_upc,rdata,dataobra,dataopen,obistamp, resfor, rescli,lordem
							,lobs,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora, datafinal,u_reserva, vendedor, vendnm, CCUSTO,u_epv1act, lote
							,num1, binum1, binum2, binum3, qtrec, u_mquebra,eslvu,esltt, fechada, usr2, comunicada_ws, comunicada_ws_descr
			)Values (
				'<<alltrim(BI.bistamp)>>','<<ALLTRIM(cabDoc.cabStamp)>>','<<alltrim(BI.nmdos)>>', <<cabDoc.numinternodoc>>,<<BI.obrano>>,
				'<<Alltrim(BI.ref)>>', '<<Alltrim(BI.ref)>>', '<<Alltrim(BI.design)>>', <<BI.qtt>>, <<BI.qtt2>>, <<BI.uni2qtt>>, <<bi.u_psicont>>, <<bi.u_bencont>>,
				<<BI.u_stockact>>, <<BI.IVA>>,<<BI.TABIVA>>,<<BI.ARMAZEM>>,<<BI.AR2MAZEM>>,<<BI.STIPO>>,<<BI.cpoc>>,<<bi.u_bonus>>, <<bi.desconto>>, <<bi.desc2>>, <<bi.desc3>>, <<bi.desc4>>,
				'<<BI.Familia>>', <<cabDoc.No>>,'<<ALLTRIM(cabDoc.Nome)>>','<<ALLTRIM(BI.Local)>>', '<<ALLTRIM(BI.Morada)>>', '<<ALLTRIM(BI.CodPost)>>',
				<<BI.EPU>>, <<BI.EDEBITO>>, <<BI.EPCUSTO>>, <<BI.ETTDEB>>, <<BI.ecustoind>>, <<BI.edebitoori>>, <<BI.u_upc>>, '<<Iif(Empty(Bi.rdata),'19000101',uf_gerais_getDate(BI.rdata,"SQL"))>>',
				'<<Iif(Empty(Bi.dataobra),'19000101',uf_gerais_getDate(BI.dataobra,"SQL"))>>', '<<Iif(Empty(Bi.dataopen),'19000101',uf_gerais_getDate(BI.dataopen,"SQL"))>>','<<Alltrim(BI.OBISTAMP)>>',
				<<lcResFor>>, <<lcResCli>>, <<BI.lordem>>, '<<Alltrim(BI.lobs)>>', '<<IIF(EMPTY(bi.ousrinis),m_chinis, bi.ousrinis)>>', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102),convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8),'<<m_chinis>>',convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
				, convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8), '<<uf_gerais_getDate(cabDoc.datafinal,"SQL")>>',<<IIF(BI.u_reserva==.t.,1,0)>>,<<ch_vendedor>>, '<<ALLTRIM(ch_vendnm)>>', '<<ALLTRIM(BI.CCUSTO)>>',<<BI.u_epv1act>>,
				'<<ALLTRIM(bi.lote)>>',<<bi.num1>>,<<bi.binum1>>,<<bi.binum2>>,<<bi.binum3>>,<<bi.qtrec>>,'<<ALLTRIM(bi.u_mquebra)>>',<<BI.eslvu>>,<<BI.esltt>>, <<lcEstadoDocBO>>, '<<ALLTRIM(bi.usr2)>>'
				,<<lcBiComunicadaSucesso>>, '<<ALLTRIM(bi.comunicada_ws_descr)>>'
			)
		ENDTEXT

		lcExecuteSQL  = lcExecuteSQL + Chr(13) + Alltrim(lcSQLBi1)

	Endscan

	**UPDATE LINHAS BI2
	lcContador = 0
	Select Bi
	Go Top
	Scan
		Select BI2
		Locate For Alltrim(Bi.bistamp) == Alltrim(BI2.bi2stamp)

		lcSQLBi2 = ""
		TEXT TO lcSQLBi2 NOSHOW TEXTMERGE PRETEXT 8
				DELETE FROM	<<ALLTRIM(lcTabela2)>> WHERE BI2STAMP = '<<alltrim(Bi.bistamp)>>'
				Insert into <<ALLTRIM(lcTabela2)>> (bi2stamp, bostamp, morada, local, codpost, fnstamp, fistamp)
				Values('<<alltrim(Bi.bistamp)>>','<<ALLTRIM(cabDoc.cabStamp)>>','<<ALLTRIM(BI.Morada)>>','<<ALLTRIM(BI.Local)>>','<<ALLTRIM(BI.CodPost)>>', '<<ALLTRIM(bi2.fnstamp)>>', '<<ALLTRIM(bi2.fistamp)>>')
		ENDTEXT

		lcExecuteSQL  = lcExecuteSQL + Chr(13) + lcSQLBi2
	Endscan

	Return lcExecuteSQL

Endfunc


** Nova funcao de update / insert das linhas na BI - Lu�s Leal 2015/11/23
*!*	FUNCTION uf_documentos_actualizaLinhasBI
*!*		LOCAL lcExecuteSQL, lcContador, lcTotalRegistos ,lcbistamp,lcDoc, lcNrTotalRegBI, lcNrTotalRegBI2, lcContador, lcContador2, lcContadorAux, lcContador2Aux
*!*		STORE '' TO lcExecuteSQL, lcbistamp
*!*		STORE 0 TO lcContador, lcNrTotalRegBI, lcNrTotalRegBI2, lcContador, lcContador2, lcContadorAux, lcContador2Aux
*!*
*!*		SELECT CabDoc
*!*		lcDoc = CabDoc.doc
*!*
*!*		SELECT cabDoc
*!*		lcEstadoDocBO = IIF(Alltrim(cabDoc.EstadoDoc) == "F",1,0)
*!*
*!*		&& update datas linhas em caso de altera��o do cabe�alho
*!*		SELECT CabDoc
*!*		UPDATE bi SET dataobra = CabDoc.dataDoc, datafinal= CabDoc.dataDoc, rdata= CabDoc.dataDoc
*!*
*!*		&& Apagar as Linhas, que j� n�o existem no cursor
*!*		SELECT ucrsBiCompare.bistamp as bistamp_compare, bi.bistamp  FROM ucrsBiCompare LEFT JOIN bi ON bi.bistamp = ucrsBiCompare.bistamp INTO CURSOR ucrsBiCompareInsertDelete READWRITE
*!*		SELECT ucrsBiCompareInsertDelete
*!*		GO TOP
*!*		SCAN FOR ISNULL(ucrsBiCompareInsertDelete.bistamp) == .t.
*!*			lcSQL = ""
*!*			TEXT TO lcSQL NOSHOW TEXTMERGE PRETEXT 8
*!*				DELETE FROM	Bi WHERE BISTAMP = '<<ALLTRIM(ucrsBiCompareInsertDelete.bistamp_compare)>>'
*!*				DELETE FROM	BI2 WHERE BI2STAMP = '<<ALLTRIM(ucrsBiCompareInsertDelete.bistamp_compare)>>'
*!*			ENDTEXT
*!*			lcExecuteSQL  = lcExecuteSQL + CHR(13) + CHR(13) + lcSQL
*!*		ENDSCAN

*!*		&& delete linhas pelo bostamp no caso de ser edicao de documento
*!*		IF myDocAlteracao == .t.
*!*			select CabDoc
*!*			lcSQL = ''
*!*			TEXT TO lcSQL NOSHOW TEXTMERGE PRETEXT 8
*!*				DELETE from bi WHERE bostamp = '<<ALLTRIM(CabDoc.CabStamp)>>'
*!*			ENDTEXT

*!*		lcExecuteSQL  = lcExecuteSQL + CHR(13) + ALLTRIM(lcSQL)
*!*		ENDIF

*!*		&&  Update / Insert Linhas BI

*!*		&& resfor=1 quando o documento for encomenda de fornecedor
*!*		LOCAL lcResFor
*!*		lcResFor = IIF(UPPER(alltrim(cabDoc.DOC))=="ENCOMENDA A FORNECEDOR",1,0)
*!*
*!*		&& rescli=1 quando o documento for reserva de cliente
*!*		LOCAL lcResCli
*!*		lcResCli = IIF(UPPER(alltrim(cabDoc.DOC))=="RESERVA DE CLIENTE",1,0)
*!*
*!*		&& Insert / update Linhas BI
*!*		lcSQLBi1 = ""
*!*		TEXT TO lcSQLBi1 NOSHOW TEXTMERGE PRETEXT 8
*!*			INSERT INTO bi (bistamp, bostamp, nmdos, ndos, obrano, ref, codigo, design, qtt, qtt2, uni2qtt, u_psicont, u_bencont, u_stockact
*!*							,iva, tabiva, armazem, ar2mazem, stipo, cpoc,u_bonus, desconto, desc2, desc3, desc4, familia,no,nome,local,morada
*!*							,codpost,epu,edebito,epcusto,ettdeb,ecustoind,edebitoori,u_upc,rdata,dataobra,dataopen,obistamp, resfor, rescli,lordem
*!*							,lobs,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora, datafinal,u_reserva, vendedor, vendnm, CCUSTO,u_epv1act, lote
*!*							,num1, binum1, binum2, binum3, qtrec, u_mquebra,eslvu,esltt, fechada
*!*							) VALUES
*!*		ENDTEXT
*!*
*!*		lcExecuteSQL  = lcExecuteSQL + CHR(13) + ALLTRIM(lcSQLBi1)
*!*
*!*		&& Values - tem contador para os casos em que s�o mais de 1k linhas
*!*		SELECT * FROM Bi INTO CURSOR uCrsBiAuxTemp READWRITE

*!*		lcNrTotalRegBI = RECCOUNT("uCrsBiAuxTemp")
*!*		IF USED("uCrsBiAuxTemp ")
*!*			fecha("uCrsBiAuxTemp ")
*!*		ENDIF
*!*
*!*		&&
*!*		SELECT BI
*!*		GO TOP
*!*		SCAN
*!*			lcContador = lcContador + 1
*!*			lcContadorAux = lcContadorAux + 1
*!*
*!*			lcSQLBi1 = ""
*!*			TEXT TO lcSQLBi1 NOSHOW TEXTMERGE PRETEXT 8
*!*				('<<alltrim(BI.bistamp)>>','<<ALLTRIM(cabDoc.cabStamp)>>','<<alltrim(BI.nmdos)>>', <<cabDoc.numinternodoc>>,<<BI.obrano>>,
*!*				'<<Alltrim(BI.ref)>>', '<<Alltrim(BI.ref)>>', '<<Alltrim(BI.design)>>', <<BI.qtt>>, <<BI.qtt2>>, <<BI.uni2qtt>>, <<bi.u_psicont>>, <<bi.u_bencont>>,
*!*				<<BI.u_stockact>>, <<BI.IVA>>,<<BI.TABIVA>>,<<BI.ARMAZEM>>,<<BI.AR2MAZEM>>,<<BI.STIPO>>,<<BI.cpoc>>,<<bi.u_bonus>>, <<bi.desconto>>, <<bi.desc2>>, <<bi.desc3>>, <<bi.desc4>>,
*!*				'<<BI.Familia>>', <<cabDoc.No>>,'<<ALLTRIM(cabDoc.Nome)>>','<<ALLTRIM(BI.Local)>>', '<<ALLTRIM(BI.Morada)>>', '<<ALLTRIM(BI.CodPost)>>',
*!*				<<BI.EPU>>, <<BI.EDEBITO>>, <<BI.EPCUSTO>>, <<BI.ETTDEB>>, <<BI.ecustoind>>, <<BI.edebitoori>>, <<BI.u_upc>>, '<<Iif(Empty(Bi.rdata),'19000101',uf_gerais_getDate(BI.rdata,"SQL"))>>',
*!*				'<<Iif(Empty(Bi.dataobra),'19000101',uf_gerais_getDate(BI.dataobra,"SQL"))>>', '<<Iif(Empty(Bi.dataopen),'19000101',uf_gerais_getDate(BI.dataopen,"SQL"))>>','<<Alltrim(BI.OBISTAMP)>>',
*!*				<<lcResFor>>, <<lcResCli>>, <<BI.lordem>>, '<<Alltrim(BI.lobs)>>', '<<m_chinis>>', convert(varchar,getdate(),102),convert(varchar,getdate(),8),'<<m_chinis>>',convert(varchar,getdate(),102)
*!*				, convert(varchar,getdate(),8), '<<uf_gerais_getDate(cabDoc.datafinal,"SQL")>>',<<IIF(BI.u_reserva==.t.,1,0)>>,<<ch_vendedor>>, '<<ALLTRIM(ch_vendnm)>>', '<<ALLTRIM(BI.CCUSTO)>>',<<BI.u_epv1act>>,
*!*				'<<ALLTRIM(bi.lote)>>',<<bi.num1>>,<<bi.binum1>>,<<bi.binum2>>,<<bi.binum3>>,<<bi.qtrec>>,'<<ALLTRIM(bi.u_mquebra)>>',<<BI.eslvu>>,<<BI.esltt>>, <<lcEstadoDocBO>> ) <<IIF(lcContador = lcNrTotalRegBI or lcContadorAux = lcNrTotalRegBI or lcContador > 455,'',',')>>
*!*			ENDTEXT

*!*			lcExecuteSQL  = lcExecuteSQL + CHR(13) + ALLTRIM(lcSQLBi1)
*!*
*!*			IF lcContador > 455
*!*				lcContador = 0

*!*				lcSQLBiAux = ''
*!*				TEXT TO lcSQLBiAux NOSHOW TEXTMERGE PRETEXT 8
*!*					INSERT INTO bi (bistamp, bostamp, nmdos, ndos, obrano, ref, codigo, design, qtt, qtt2, uni2qtt, u_psicont, u_bencont, u_stockact
*!*							,iva, tabiva, armazem, ar2mazem, stipo, cpoc,u_bonus, desconto, desc2, desc3, desc4, familia,no,nome,local,morada
*!*							,codpost,epu,edebito,epcusto,ettdeb,ecustoind,edebitoori,u_upc,rdata,dataobra,dataopen,obistamp, resfor, rescli,lordem
*!*							,lobs,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora, datafinal,u_reserva, vendedor, vendnm, CCUSTO,u_epv1act, lote
*!*							,num1, binum1, binum2, binum3, qtrec, u_mquebra,eslvu,esltt, fechada
*!*							) VALUES
*!*				ENDTEXT
*!*
*!*				lcExecuteSQL  = lcExecuteSQL + CHR(13) + ALLTRIM(lcSQLBiAux)
*!*
*!*			ENDIF
*!*		ENDSCAN
*!*
*!*		** Update / Insert Linhas BI2
*!*		&& delete
*!*		IF myDocAlteracao == .t.
*!*			SELECT CabDoc
*!*			lcSQLBi2 = ""
*!*			TEXT TO lcSQLBi2 NOSHOW TEXTMERGE PRETEXT 8
*!*				DELETE FROM	BI2 WHERE bostamp = '<<ALLTRIM(CabDoc.CabStamp)>>'
*!*			ENDTEXT

*!*		lcExecuteSQL  = lcExecuteSQL + CHR(13) + ALLTRIM(lcSQLBi2)
*!*		ENDIF

*!*		&& Insert
*!*		SELECT bi2
*!*		GO TOP
*!*		SELECT * FROM Bi2 INTO CURSOR uCrsBi2AuxTemp READWRITE
*!*
*!*		lcNrTotalRegBI2 = RECCOUNT("uCrsBi2AuxTemp")
*!*
*!*		IF USED("uCrsBi2AuxTemp")
*!*			fecha("uCrsBi2AuxTemp")
*!*		ENDIF
*!*
*!*		SELECT BI
*!*		lcSQLBi2 = ""
*!*		TEXT TO lcSQLBi2 NOSHOW TEXTMERGE PRETEXT 8
*!*			INSERT INTO bi2 (bi2stamp, bostamp, morada, local, codpost, fnstamp) VALUES
*!*		ENDTEXT
*!*
*!*		lcExecuteSQL  = lcExecuteSQL + CHR(13) + ALLTRIM(lcSQLBi2)

*!*		&& Values - tem contador para os casos em que s�o mais de 1k linhas
*!*		Select BI
*!*		GO TOP
*!*		SCAN
*!*			**SELECT Bi2
*!*			**LOCATE FOR ALLTRIM(Bi.bistamp) == ALLTRIM(bi2.bi2stamp)
*!*
*!*			lcContador2 = lcContador2 + 1
*!*			lcContador2Aux =  lcContador2Aux + 1
*!*
*!*			lcSQLBi2 = ""
*!*			TEXT TO lcSQLBi2 NOSHOW TEXTMERGE PRETEXT 8
*!*				('<<ALLTRIM(Bi.bistamp)>>','<<ALLTRIM(cabDoc.cabStamp)>>','<<ALLTRIM(BI.Morada)>>','<<ALLTRIM(BI.Local)>>','<<ALLTRIM(BI.CodPost)>>', '<<ALLTRIM(bi2.fnstamp)>>') <<IIF(lcContador2 = lcNrTotalRegBI2 or lcContador2Aux = lcNrTotalRegBI2 or lcContador2 > 455,'',',')>>
*!*			ENDTEXT

*!*			lcExecuteSQL  = lcExecuteSQL + CHR(13) + lcSQLBi2
*!*
*!*			IF lcContador2 > 455
*!*
*!*				lcContador2 = 0

*!*				lcSQLBiAux = ''
*!*				TEXT TO lcSQLBi2Aux NOSHOW TEXTMERGE PRETEXT 8
*!*					INSERT INTO bi2 (bi2stamp, bostamp, morada, local, codpost, fnstamp) VALUES
*!*				ENDTEXT
*!*
*!*				lcExecuteSQL  = lcExecuteSQL + CHR(13) + ALLTRIM(lcSQLBi2Aux)
*!*
*!*			ENDIF
*!*
*!*		ENDSCAN

*!*		RETURN lcExecuteSQL
*!*
*!*	ENDFUNC


**ACTUALIZA CABE�ALHO COMPRAS - FO
Function uf_documentos_actualizaCabFO
	Local lcExecuteSQL
	Store "" To lcExecuteSQL

	Local lcValorIva1, lcValorIva2, lcValorIva3, lcValorIva4, lcValorIva5, lcValorIva6, lcValorIva7, lcValorIva8, lcValorIva9;
		,lcTotalSemIvaFO1, lcTotalSemIvaFO2, lcTotalSemIvaFO3, lcTotalSemIvaFO4, lcTotalSemIvaFO5, lcTotalSemIvaFO6;
		,lcTotalSemIvaFO7, lcTotalSemIvaFO8, lcTotalSemIvaFO9, lcTotalIva

	Store 0 To lcValorIva1, lcValorIva2, lcValorIva3, lcValorIva4, lcValorIva5, lcValorIva6, lcValorIva7, lcValorIva8, lcValorIva9;
		,lcTotalSemIvaFO1, lcTotalSemIvaFO2, lcTotalSemIvaFO3, lcTotalSemIvaFO4, lcTotalSemIvaFO5, lcTotalSemIvaFO6, lcTotalSemIvaFO7;
		,lcTotalSemIvaFO8, lcTotalSemIvaFO9, lcTotalIva

	Set Point To "."

	Select FN
	Go Top
	Scan
		**CALCULA SUBTOTAIS IVA
		Do Case
			Case FN.tabiva=1
				If FN.ivaincl
					lcValorIva1=lcValorIva1 + FN.etiliquido - (FN.etiliquido/(FN.IVA/100+1))
					lcTotalSemIvaFO1 = lcTotalSemIvaFO1 + (FN.etiliquido/(FN.IVA/100+1))
				Else
					lcValorIva1=lcValorIva1+(FN.etiliquido*(FN.IVA/100))
					lcTotalSemIvaFO1 = lcTotalSemIvaFO1 + FN.etiliquido
				Endif
			Case FN.tabiva=2
				If FN.ivaincl
					lcValorIva2=lcValorIva2+ FN.etiliquido - (FN.etiliquido/(FN.IVA/100+1))
					lcTotalSemIvaFO2 = lcTotalSemIvaFO2 + (FN.etiliquido/(FN.IVA/100+1))
				Else
					lcValorIva2=lcValorIva2+(FN.etiliquido*(FN.IVA/100))
					lcTotalSemIvaFO2 = lcTotalSemIvaFO2 + FN.etiliquido
				Endif
			Case FN.tabiva=3
				If FN.ivaincl
					lcValorIva3=lcValorIva3+ FN.etiliquido - (FN.etiliquido/(FN.IVA/100+1))
					lcTotalSemIvaFO3 = lcTotalSemIvaFO3 + (FN.etiliquido/(FN.IVA/100+1))
				Else
					lcValorIva3=lcValorIva3+(FN.etiliquido*(FN.IVA/100))
					lcTotalSemIvaFO3 = lcTotalSemIvaFO3 + FN.etiliquido
				Endif
			Case FN.tabiva=4
				If FN.ivaincl
					lcValorIva4=lcValorIva4+ FN.etiliquido - (FN.etiliquido/(FN.IVA/100+1))
					lcTotalSemIvaFO4 = lcTotalSemIvaFO4 + (FN.etiliquido/(FN.IVA/100+1))
				Else
					lcValorIva4=lcValorIva4+(FN.etiliquido*(FN.IVA/100))
					lcTotalSemIvaFO4 = lcTotalSemIvaFO4 + FN.etiliquido
				Endif
			Case FN.tabiva=5
				If FN.ivaincl
					lcValorIva5=lcValorIva5+ FN.etiliquido - (FN.etiliquido/(FN.IVA/100+1))
					lcTotalSemIvaFO5 = lcTotalSemIvaFO5 + (FN.etiliquido/(FN.IVA/100+1))
				Else
					lcValorIva5=lcValorIva5+(FN.etiliquido*(FN.IVA/100))
					lcTotalSemIvaFO5 = lcTotalSemIvaFO5 + FN.etiliquido
				Endif
			Case FN.tabiva=6
				If FN.ivaincl
					lcValorIva6=lcValorIva6+ FN.etiliquido - (FN.etiliquido/(FN.IVA/100+1))
					lcTotalSemIvaFO6 = lcTotalSemIvaFO6 + (FN.etiliquido/(FN.IVA/100+1))
				Else
					lcValorIva6=lcValorIva6+(FN.etiliquido*(FN.IVA/100))
					lcTotalSemIvaFO6 = lcTotalSemIvaFO6 + FN.etiliquido
				Endif
			Case FN.tabiva=7
				If FN.ivaincl
					lcValorIva7=lcValorIva7+ FN.etiliquido - (FN.etiliquido/(FN.IVA/100+1))
					lcTotalSemIvaFO7 = lcTotalSemIvaFO7 + (FN.etiliquido/(FN.IVA/100+1))
				Else
					lcValorIva7=lcValorIva7+(FN.etiliquido*(FN.IVA/100))
					lcTotalSemIvaFO7 = lcTotalSemIvaFO7 + FN.etiliquido
				Endif
			Case FN.tabiva=8
				If FN.ivaincl
					lcValorIva8=lcValorIva8+ FN.etiliquido - (FN.etiliquido/(FN.IVA/100+1))
					lcTotalSemIvaFO8 = lcTotalSemIvaFO8 + (FN.etiliquido/(FN.IVA/100+1))
				Else
					lcValorIva8=lcValorIva8+(FN.etiliquido*(FN.IVA/100))
					lcTotalSemIvaFO8 = lcTotalSemIvaFO8 + FN.etiliquido
				Endif
			Case FN.tabiva=9
				If FN.ivaincl
					lcValorIva9=lcValorIva9+ FN.etiliquido - (FN.etiliquido/(FN.IVA/100+1))
					lcTotalSemIvaFO9 = lcTotalSemIvaFO9 + (FN.etiliquido/(FN.IVA/100+1))
				Else
					lcValorIva9=lcValorIva9+(FN.etiliquido*(FN.IVA/100))
					lcTotalSemIvaFO9 = lcTotalSemIvaFO9 + FN.etiliquido
				Endif
		Endcase
		lcTotalSemIvaFO = lcTotalSemIvaFO1 + lcTotalSemIvaFO2 + lcTotalSemIvaFO3 + lcTotalSemIvaFO4 + lcTotalSemIvaFO5 + lcTotalSemIvaFO6 + lcTotalSemIvaFO7 + lcTotalSemIvaFO8 + lcTotalSemIvaFO9
	Endscan

	** Retira Desconto Financeiro
	If !(cabdoc.DESCFIN == 0)
		If lcValorIva1 > 0
			lcValorIva1 = (lcValorIva1 / ((cabdoc.DESCFIN/100) + 1))
		Endif
		If lcValorIva2 > 0
			lcValorIva2 = (lcValorIva2 / ((cabdoc.DESCFIN/100) + 1))
		Endif
		If lcValorIva3 > 0
			lcValorIva3 = (lcValorIva3 / ((cabdoc.DESCFIN/100) + 1))
		Endif
		If lcValorIva4 > 0
			lcValorIva4 = (lcValorIva4 / ((cabdoc.DESCFIN/100) + 1))
		Endif
		If lcValorIva5 > 0
			lcValorIva5 = (lcValorIva5 / ((cabdoc.DESCFIN/100) + 1))
		Endif
		If lcValorIva6 > 0
			lcValorIva6 = (lcValorIva6 / ((cabdoc.DESCFIN/100) + 1))
		Endif
		If lcValorIva7 > 0
			lcValorIva7 = (lcValorIva7 / ((cabdoc.DESCFIN/100) + 1))
		Endif
		If lcValorIva8 > 0
			lcValorIva8 = (lcValorIva8 / ((cabdoc.DESCFIN/100) + 1))
		Endif
		If lcValorIva9 > 0
			lcValorIva9 = (lcValorIva9 / ((cabdoc.DESCFIN/100) + 1))
		Endif
	Endif

	lcTotalIva = lcValorIva1 + lcValorIva2 + lcValorIva3 + lcValorIva4 + lcValorIva5 + lcValorIva6 + lcValorIva7 + lcValorIva8 + lcValorIva9

	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE PRETEXT 8

		Update
			FO
		SET
			docnome 	= '<<ALLTRIM(cabDoc.DOC)>>',
			doccode		=	<<cabDoc.NUMINTERNODOC>> ,
			nome		=	'<<ALLTRIM(cabDoc.NOME)>>',
			no			=	<<cabDoc.NO>>,
			estab		=	<<cabDoc.ESTAB>>,
			adoc		=	'<<Alltrim(cabDoc.NUMDOC)>>',
			morada		=	'<<ALLTRIM(cabDoc.MORADA)>>',
			local		=	'<<ALLTRIM(cabDoc.LOCAL)>>',
			codpost		=	'<<ALLTRIM(cabDoc.codpost)>>',
			ncont		=	'<<ALLTRIM(cabDoc.ncont)>>',
			data		=	'<<uf_gerais_getDate(cabDoc.DATAINTERNA,"SQL")>>',
			docdata		=	'<<uf_gerais_getDate(cabDoc.DATADOC,"SQL")>>',
			foano		=	<<YEAR(cabDoc.DATAINTERNA)>>,
			pdata		=	'<<uf_gerais_getDate(cabDoc.DATAVENC,"SQL")>>',
			tpdesc		=	'<<Alltrim(cabdoc.condpag)>>',
			moeda		=	'<<Alltrim(CabDOC.MOEDA)>>',
			memissao	=	'EURO',
			moeda2		=	'EURO',
			ollocal		=	'Caixa',
			telocal		=	'C',
			total		=	<<Round(cabDoc.TOTAL*200.482,2)>>,
			etotal		=	<<Round(cabDoc.TOTAL,2)>>,
			ivain		=	<<Round(cabDoc.BASEINC*200.482,2)>>,
			eivain		=	<<Round(cabDoc.BASEINC,2)>>,
			ttiva		=	<<Round(cabDoc.IVA*200.482,2)>>,
			ettiva		=	<<Round(cabDoc.IVA,2)>>,
			ettiliq		=	<<Round(cabDoc.BASEINC,2)>>,
			ivainsns	=	<<Round(cabDoc.totalNservico*200.482,2)>>,
			eivainsns	=	<<Round(cabDoc.totalNservico,2)>>,
			ivav1		=	<<Round(lcValorIva1*200.482,2)>>,
			eivav1		=	<<Round(lcValorIva1,2)>>,
			ivav2		=	<<Round(lcValorIva2*200.482,2)>>,
			eivav2		=	<<Round(lcValorIva2,2)>>,
			ivav3		=	<<Round(lcValorIva3*200.482,2)>>,
			eivav3		=	<<Round(lcValorIva3,2)>>,
			ivav4		=	<<Round(lcValorIva4*200.482,2)>>,
			eivav4		=	<<Round(lcValorIva4,2)>>,
			ivav5		=	<<Round(lcValorIva5*200.482,2)>>,
			eivav5		=	<<Round(lcValorIva5,2)>>,
			ivav6		=	<<Round(lcValorIva6*200.482,2)>>,
			eivav6		=	<<Round(lcValorIva6,2)>>,
			ivav7		=	<<Round(lcValorIva7*200.482,2)>>,
			eivav7		=	<<Round(lcValorIva7,2)>>,
			ivav8		=	<<Round(lcValorIva8*200.482,2)>>,
			eivav8		=	<<Round(lcValorIva8,2)>>,
			ivav9		=	<<Round(lcValorIva9*200.482,2)>>,
			eivav9		=	<<Round(lcValorIva9,2)>>,
			epaivav1	=	<<Round(lcValorIva1,2)>>,
			epaivav2	=	<<Round(lcValorIva2,2)>>,
			epaivav3	=	<<Round(lcValorIva3,2)>>,
			epaivav4	=	<<Round(lcValorIva4,2)>>,
			epaivav5	=	<<Round(lcValorIva5,2)>>,
			epaivav6	=	<<Round(lcValorIva6,2)>>,
			epaivav7	=	<<Round(lcValorIva7,2)>>,
			epaivav8	=	<<Round(lcValorIva8,2)>>,
			epaivav9	=	<<Round(lcValorIva9,2)>>,
			epaivain	=	<<Round(cabDoc.BASEINC,2)>>,
			patotal		=	<<Round(cabDoc.TOTAL*200.482,2)>>,
			epatotal	=	<<Round(cabDoc.TOTAL,2)>>,
			site		=	'<<ALLTRIM(cabDoc.site)>>',
			pnome		=	'<<ALLTRIM(myTerm)>>',
			pno			=	<<myTermNo>>,
			u_status	=	'<<Alltrim(CabDOC.ESTADODOC)>>',
			usrinis		=	'<<m_chinis>>',
			usrdata		=	convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102),
			usrhora		=	convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8),
			Final		=	'<<Alltrim(cabDoc.obs)>>',
			fin			=	<<CabDoc.DESCFIN>>,
			efinv		=	<<Round(CabDoc.VALORDESCFIN,2)>>,
			ccusto		=	'<<cabdoc.ccusto>>',
			plano		=	<<IIF(cabdoc.plano,1,0)>>,
			bloqPAg		= 	<<IIF(cabdoc.bloqpag,1,0)>>,
			EDESCC		= 	<<cabDoc.EDESCC>>,
			pais 		=   <<cabDoc.pais>>,
			consignacao =  <<IIF(cabDoc.consignacao,1,0)>>,
			id_tesouraria_conta = <<cabDoc.id_tesouraria_conta>>,
			imput_desp	= 	<<cabdoc.imput_desp>>
		FROM
			FO
		WHERE
			FOSTAMP = '<<Alltrim(cabDoc.cabstamp)>>'
	ENDTEXT

	lcExecuteSQL  = lcExecuteSQL + Chr(13) + Chr(13) + lcSQL

	lcSQL=''
	TEXT TO lcSql NOSHOW TEXTMERGE PRETEXT 8
		UPDATE FO2
		SET			ivatx1 = (select taxa from taxasiva (nolock) where codigo=1),
					ivatx2 = (select taxa from taxasiva (nolock) where codigo=2),
					ivatx3 = (select taxa from taxasiva (nolock) where codigo=3),
					ivatx4 = (select taxa from taxasiva (nolock) where codigo=4),
					ivatx5 = (select taxa from taxasiva (nolock) where codigo=5),
					ivatx6 = (select taxa from taxasiva (nolock) where codigo=6),
					ivatx7 = (select taxa from taxasiva (nolock) where codigo=7),
					ivatx8 = (select taxa from taxasiva (nolock) where codigo=8),
					ivatx9 = (select taxa from taxasiva (nolock) where codigo=9),
					u_docfl		= '<<alltrim(cabdoc.docfl)>>',
					u_docflno	= '<<Alltrim(cabdoc.docflno)>>',
					usrinis = '<<m_chinis>>',
					usrdata = convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102),
					usrhora = convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8),
					u_armazem = <<cabDoc.Armazem>>
		FROM		FO2
		WHERE	FO2STAMP = '<<Alltrim(cabDoc.cabstamp)>>'
	ENDTEXT

	lcExecuteSQL  = lcExecuteSQL + Chr(13) + Chr(13) + lcSQL

	** Actualiza totais
	TEXT TO lcSQL NOSHOW TEXTMERGE PRETEXT 8
		DELETE from fo_totais WHERE stamp = '<<Alltrim(cabDoc.cabStamp)>>'
	ENDTEXT
	lcExecuteSQL  = lcExecuteSQL + Chr(13) + Chr(13) + lcSQL

	Select ucrsTotaisDocumento
	Go Top
	Scan
		TEXT TO lcSQL NOSHOW TEXTMERGE PRETEXT 8
			INSERT INTO fo_totais(
				stamp
				,taxa
				,baseinc
				,valoriva
				,arredondamento
				,valoraposarredondamento
                ,tabIva
			)values(
				'<<Alltrim(cabDoc.cabStamp)>>'
				,<<ucrsTotaisDocumento.taxa>>
				,<<ucrsTotaisDocumento.baseinc>>
				,<<ucrsTotaisDocumento.valoriva>>
				,<<ucrsTotaisDocumento.arredondamento>>
				,<<ucrsTotaisDocumento.valoraposarredondamento>>
                ,<<ucrsTotaisDocumento.tabIva>>
			)
		ENDTEXT
		lcExecuteSQL  = lcExecuteSQL + Chr(13) + Chr(13) + lcSQL

	Endscan

	Return lcExecuteSQL

Endfunc


** funcao insert / update fn - antiga
*!*	FUNCTION uf_documentos_actualizaLinhasFN
*!*		LOCAL lcStamp, lcExecuteSQL, lcContador, lcDoc
*!*
*!*		STORE "" TO lcExecuteSQL

*!*		SELECT CabDoc
*!*		lcDoc = UPPER(ALLTRIM(CabDoc.Doc))
*!*
*!*		SELECT fn
*!*		GO TOP
*!*		SELECT ucrsFnCompare
*!*		GO TOP

*!*		** Apagar as Linhas, que j� n�o existem no cursor
*!*		SELECT ucrsFnCompare.fnstamp as fnstamp_compare, fn.fnstamp  FROM ucrsFnCompare LEFT JOIN fn ON fn.fnstamp = ucrsFnCompare.Fnstamp INTO CURSOR ucrsFnCompareInsertDelete READWRITE
*!*
*!*		SELECT ucrsFnCompareInsertDelete
*!*		GO TOP
*!*		SCAN FOR ISNULL(ucrsFnCompareInsertDelete.fnstamp) == .t.
*!*			lcSQL = ""
*!*			TEXT TO lcSQL NOSHOW TEXTMERGE PRETEXT 8
*!*				DELETE FROM	Fn WHERE FNSTAMP = '<<ALLTRIM(ucrsFnCompareInsertDelete.fnstamp_compare)>>'
*!*				DELETE FROM	FN_HONORARIOS WHERE FNSTAMP = '<<ALLTRIM(ucrsFnCompareInsertDelete.fnstamp_compare)>>'
*!*			ENDTEXT
*!*			lcExecuteSQL  = lcExecuteSQL + CHR(13) + CHR(13) + lcSQL
*!*		ENDSCAN
*!*

*!*		SELECT FN
*!*		GO TOP
*!*		SCAN
*!*			lcSQLFN = ""
*!*			TEXT TO lcSQLFN NOSHOW TEXTMERGE PRETEXT 8
*!*
*!*			<<IIF(myDocIntroducao==.t., '', 'DELETE from fn where fnstamp = '+ ['] +ALLTRIM(fn.fnstamp)+['])>>
*!*
*!*			INSERT INTO fn (fnstamp, fostamp, data, ref, oref, codigo, design,stns, usr1, usr2, usr3, usr4, usr5,
*!*					 usr6, docnome, adoc, unidade, qtt, uni2qtt, iva, ivaincl, u_bonus, desconto, desc2, desc3, desc4,
*!*					 tabiva, armazem, cpoc, lordem, epv, pv, etiliquido, tiliquido, u_psicont, u_bencont, qtrec, u_pvp,
*!*					 u_validade, esltt, sltt, u_upc, eslvu, slvu,u_stockAct, u_epv1act, marcada,ousrinis, ousrdata, ousrhora,
*!*					 usrinis, usrdata, usrhora, ofnstamp, bistamp, u_qttenc, u_upce, fmarcada, u_reserva, fnccusto, SUJINV,lote, u_margpct, descFin_pcl, num1, familia
*!*			)VALUES (
*!*				'<<ALLTRIM(fn.fnstamp)>>','<<ALLTRIM(cabDoc.cabStamp)>>','<<uf_gerais_getDate(cabDoc.dataInterna,"SQL")>>','<<ALLTRIM(FN.REF)>>','<<Alltrim(FN.OREF)>>','<<ALLTRIM(FN.REF)>>',
*!*				 '<<ALLTRIM(FN.design)>>',<<IIF(FN.stns, 1, 0)>>,'<<ALLTRIM(FN.usr1)>>','<<ALLTRIM(FN.usr2)>>','<<ALLTRIM(FN.usr3)>>','<<ALLTRIM(FN.usr4)>>','<<ALLTRIM(FN.usr5)>>',
*!*				 '<<ALLTRIM(FN.usr6)>>','<<ALLTRIM(cabDoc.DOC)>>','<<Alltrim(cabDoc.NUMDOC)>>','<<ALLTRIM(FN.unidade)>>',<<FN.qtt>>,<<FN.uni2qtt>>,<<FN.iva>>,<<IIF(FN.ivaincl,1,0)>>,
*!*				 <<fn.u_bonus>>, <<fn.desconto>>, <<fn.desc2>>, <<fn.desc3>>, <<fn.desc4>>,
*!*				 <<FN.tabiva>>,<<FN.armazem>>,<<FN.cpoc>>,<<FN.lordem>>,<<Round(FN.epv,2)>>,<<Round(FN.epv*200.482,2)>>,<<FN.etiliquido>>,<<FN.etiliquido*200.482>>,
*!*				 <<fn.u_psicont>>, <<fn.u_bencont>>, <<fn.qtrec>>, <<fn.u_pvp>>, '<<IIF(!EMPTY(fn.u_validade),uf_gerais_getDate(fn.u_validade,"SQL"),'19000101')>>',
*!*				 <<FN.esltt>>,<<FN.esltt*200.482>>,<<FN.u_upc>>,<<FN.eslvu>>,<<FN.eslvu * 200.482>>,<<FN.u_stockact>>,<<FN.u_epv1act>>, <<Iif(fn.marcada,1,0)>>,
*!*				 '<<m_chinis>>',convert(varchar,getdate(),102), convert(varchar,getdate(),8),'<<m_chinis>>',convert(varchar,getdate(),102),convert(varchar,getdate(),8),
*!*				 '<<Alltrim(FN.OFNSTAMP)>>', '<<Alltrim(FN.BISTAMP)>>', <<FN.u_qttenc>>, <<FN.u_upce>>, <<Iif(fn.fmarcada,1,0)>>,<<IIF(FN.u_reserva,1,0)>>,'<<FN.FNCCUSTO>>',<<IIF(FN.SUJINV,1,0)>>
*!*				 ,'<<ALLTRIM(fn.lote)>>',<<fn.u_margpct>>,<<IIF(ISNULL(fn.descFin_pcl),0,fn.descFin_pcl)>>,<<fn.num1>>, '<<ALLTRIM(fn.familia)>>'
*!*			)
*!*			ENDTEXT
*!*
*!*			lcExecuteSQL  = lcExecuteSQL + CHR(13) + CHR(13) + lcSQLFN

*!*		ENDSCAN
*!*
*!*		IF UPPER(ALLTRIM(lcDoc)) == UPPER("Mapa Honorarios")
*!*			SELECT FN
*!*			GO TOP
*!*			SCAN
*!*				Select FN
*!*				TEXT TO lcSql NOSHOW TEXTMERGE PRETEXT 8
*!*					DELETE FROM	FN_honorarios WHERE FNSTAMP = '<<alltrim(FN.fnstamp)>>'
*!*					INSERT INTO FN_honorarios
*!*						(	FNstamp
*!*							,servmrstamp
*!*							,data
*!*							,EntidadeNome
*!*							,EntidadeNo
*!*							,Utente
*!*							,UtenteNo
*!*							,pvp
*!*							,ValorUtente
*!*							,ValorEntidade
*!*							,despesa
*!*							,honorario
*!*							,HonorarioDeducao
*!*							,HonorarioValor
*!*							,HonorarioBi
*!*							,HonorarioTipo
*!*							,userno
*!*							,id_cpt_val_cli
*!*							,convencao
*!*						 )
*!*					Values
*!*						(
*!*							'<<ALLTRIM(FN.FNstamp)>>'
*!*							,'<<ALLTRIM(FN.servmrstamp)>>'
*!*							,'<<uf_gerais_getdate(ALLTRIM(FN.data_h),'SQL')>>'
*!*							,'<<ALLTRIM(FN.EntidadeNome)>>'
*!*							,<<FN.EntidadeNo>>
*!*							,'<<ALLTRIM(FN.Utente)>>'
*!*							,<<FN.UtenteNo>>
*!*							,<<FN.pvp>>
*!*							,<<FN.ValorUtente>>
*!*							,<<FN.ValorEntidade>>
*!*							,<<FN.despesa>>
*!*							,<<FN.honorario>>
*!*							,<<FN.HonorarioDeducao>>
*!*							,<<FN.HonorarioValor>>
*!*							,'<<ALLTRIM(FN.HonorarioBi)>>'
*!*							,'<<ALLTRIM(FN.HonorarioTipo)>>'
*!*							,<<FN.userno>>
*!*							,'<<ALLTRIM(FN.id_cpt_val_cli)>>'
*!*							,'<<ALLTRIM(FN.convencao)>>'
*!*
*!*						 )
*!*				ENDTEXT
*!*				lcExecuteSQL  = lcExecuteSQL + CHR(13) + CHR(13) + lcSQL
*!*			ENDSCAN
*!*		ENDIF

*!*		Return lcExecuteSQL
*!*	ENDFUNC






Function uf_documentos_retornaRefsReservaListar
	Lparameters lcCursor

	Local lcRef
	Store  '' To  lcRef

	Select &lcCursor
	Go Top
	Scan
		lcRef =lcRef  +  "''" + Alltrim(&lcCursor..ref) + "'',"
	Endscan


	If(Len(lcRef )>1)
		lcRef =Substr(lcRef , 1, Len(lcRef ) - 1)
	Endif


	Return   lcRef

Endfunc



** Nova funcao de update / insert das linhas na FN - Lu�s Leal 2015/11/23
Function uf_documentos_actualizaLinhasFN
	Local lcStamp, lcExecuteSQL, lcContador, lcDoc, lcContador, lcNrTotalRegFN, lcContadorAux
	Store 0 To lcContador, lcNrTotalRegFN, lcContadorAux

	Store "" To lcExecuteSQL

	Select cabdoc
	lcDoc = Upper(Alltrim(cabdoc.Doc))

	Select FN
	Go Top
	Select ucrsFnCompare
	Go Top

	&& Apagar as Linhas, que j� n�o existem no cursor
	Select ucrsFnCompare.fnstamp As fnstamp_compare, FN.fnstamp  From ucrsFnCompare Left Join FN On FN.fnstamp = ucrsFnCompare.fnstamp Into Cursor ucrsFnCompareInsertDelete Readwrite

	Select ucrsFnCompareInsertDelete
	Go Top
	Scan For Isnull(ucrsFnCompareInsertDelete.fnstamp) == .T.
		lcSQL = ""
		TEXT TO lcSQL NOSHOW TEXTMERGE PRETEXT 8
			DELETE FROM	Fn WHERE FNSTAMP = '<<ALLTRIM(ucrsFnCompareInsertDelete.fnstamp_compare)>>'
			DELETE FROM	FN_HONORARIOS WHERE FNSTAMP = '<<ALLTRIM(ucrsFnCompareInsertDelete.fnstamp_compare)>>'
			DELETE FROM	fndesclin WHERE FNSTAMP = '<<ALLTRIM(ucrsFnCompareInsertDelete.fnstamp_compare)>>'
		ENDTEXT
		lcExecuteSQL  = lcExecuteSQL + Chr(13) + Chr(13) + lcSQL
	Endscan

	&& delete as linhas pelo fostamp do documento
	Select cabdoc
	lcSQL = ''
	TEXT TO lcSQL NOSHOW TEXTMERGE PRETEXT 8
		DELETE FROM	fndesclin WHERE FNSTAMP in (select fnstamp from fn WHERE fostamp = '<<ALLTRIM(CabDoc.CabStamp)>>')
		DELETE from fn WHERE fostamp = '<<ALLTRIM(CabDoc.CabStamp)>>'
	ENDTEXT

	lcExecuteSQL  = lcExecuteSQL + Chr(13) + Chr(13) +Alltrim(lcSQL)

	&& insert / update linhas FN

	&& Insert
	lcSQLFN = ''
	TEXT TO lcSQLFN NOSHOW TEXTMERGE PRETEXT 8
		INSERT INTO fn (fnstamp, fostamp, data, ref, oref, codigo, design,stns, usr1, usr2, usr3, usr4, usr5,
			 usr6, docnome, adoc, unidade, qtt, uni2qtt, iva, ivaincl, u_bonus, desconto, desc2, desc3, desc4,
			 tabiva, armazem, cpoc, lordem, epv, pv, etiliquido, tiliquido, u_psicont, u_bencont, qtrec, u_pvp,
			 u_validade, esltt, sltt, u_upc, eslvu, slvu,u_stockAct, u_epv1act, marcada,ousrinis, ousrdata, ousrhora,
			 usrinis, usrdata, usrhora, ofnstamp, bistamp, u_qttenc, u_upce, fmarcada, u_reserva, fnccusto, SUJINV,lote, u_margpct, descFin_pcl, num1, familia, stamp_ext_doc,
			 u_dtval, u_descenc, u_oqt, val_desp_lin, fistamp
		) VALUES
	ENDTEXT

	lcExecuteSQL  = lcExecuteSQL + Chr(13) + Alltrim(lcSQLFN)

	&& n� de registos no cursor - tem de ser criar cursor aux porque reccount n�o lida com os rows apagados do cursor
	Select * From FN Into Cursor uCrsFNAuxTemp Readwrite
	lcNrTotalRegFN = Reccount("uCrsFNAuxTemp")

	If Used("uCrsFNAuxTemp")
		fecha("uCrsFNAuxTemp")
	Endif

	&&
	Select FN
	Go Top
	Scan
		&& uma variavel para controlar "grupos de insert" outra para controlar o ultimo registo
		lcContador = lcContador + 1
		lcContadorAux = lcContadorAux + 1

		If lcContador > 455
			**lcContador = 0

			lcSQLFNAux = ''
			TEXT TO lcSQLFNAux NOSHOW TEXTMERGE PRETEXT 8
				INSERT INTO fn (fnstamp, fostamp, data, ref, oref, codigo, design,stns, usr1, usr2, usr3, usr4, usr5,
					 usr6, docnome, adoc, unidade, qtt, uni2qtt, iva, ivaincl, u_bonus, desconto, desc2, desc3, desc4,
					 tabiva, armazem, cpoc, lordem, epv, pv, etiliquido, tiliquido, u_psicont, u_bencont, qtrec, u_pvp,
					 u_validade, esltt, sltt, u_upc, eslvu, slvu,u_stockAct, u_epv1act, marcada,ousrinis, ousrdata, ousrhora,
					 usrinis, usrdata, usrhora, ofnstamp, bistamp, u_qttenc, u_upce, fmarcada, u_reserva, fnccusto, SUJINV,lote, u_margpct, descFin_pcl, num1, familia, stamp_ext_doc,
					 u_dtval, u_descenc , u_oqt, val_desp_lin, fistamp
				) VALUES
			ENDTEXT

			lcExecuteSQL  = lcExecuteSQL + Chr(13) + Alltrim(lcSQLFNAux)
		Endif

		lcSQLFN1 = ""
		TEXT TO lcSQLFN1 NOSHOW TEXTMERGE PRETEXT 8
			('<<ALLTRIM(fn.fnstamp)>>','<<ALLTRIM(cabDoc.cabStamp)>>','<<uf_gerais_getDate(cabDoc.dataInterna,"SQL")>>','<<ALLTRIM(FN.REF)>>','<<Alltrim(FN.OREF)>>','<<ALLTRIM(FN.REF)>>',
			 '<<ALLTRIM(FN.design)>>',<<IIF(FN.stns, 1, 0)>>,'<<ALLTRIM(FN.usr1)>>','<<ALLTRIM(FN.usr2)>>','<<ALLTRIM(FN.usr3)>>','<<ALLTRIM(FN.usr4)>>','<<ALLTRIM(FN.usr5)>>',
			 '<<ALLTRIM(FN.usr6)>>','<<ALLTRIM(cabDoc.DOC)>>','<<Alltrim(cabDoc.NUMDOC)>>','<<ALLTRIM(FN.unidade)>>',<<FN.qtt>>,<<FN.uni2qtt>>,<<FN.iva>>,<<IIF(FN.ivaincl,1,0)>>,
			 <<fn.u_bonus>>, <<fn.desconto>>, <<fn.desc2>>, <<fn.desc3>>, <<fn.desc4>>,
			 <<FN.tabiva>>,<<FN.armazem>>,<<FN.cpoc>>,<<FN.lordem>>,<<Round(FN.epv,2)>>,<<Round(FN.epv*200.482,2)>>,<<FN.etiliquido>>,<<FN.etiliquido*200.482>>,
			 <<fn.u_psicont>>, <<fn.u_bencont>>, <<fn.qtrec>>, <<fn.u_pvp>>, '<<iif(ALLTRIM(cabdoc.doc)='V/Factura  Med.', alltrim(fn.u_dtval)+'01' , (IIF(!EMPTY(fn.u_validade),uf_gerais_getDate(fn.u_validade,"SQL"),'19000101')))>>',
			 <<FN.esltt>>,<<FN.esltt*200.482>>,<<FN.u_upc>>,<<FN.eslvu>>,<<FN.eslvu * 200.482>>,<<FN.u_stockact>>,<<FN.u_epv1act>>, <<Iif(fn.marcada,1,0)>>,

			 '<<IIF(EMPTY(Alltrim(fn.ousrinis)),ALLTRIM(m_chinis) ,fn.ousrinis)>>'
			 ,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)

			 , convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8)
			 ,'<<m_chinis>>'
			 ,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
			 ,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8),
			 '<<Alltrim(FN.OFNSTAMP)>>', '<<Alltrim(FN.BISTAMP)>>', <<FN.u_qttenc>>, <<FN.u_upce>>, <<Iif(fn.fmarcada,1,0)>>,<<IIF(FN.u_reserva,1,0)>>,'<<FN.FNCCUSTO>>',<<IIF(FN.SUJINV,1,0)>>,
			 '<<ALLTRIM(fn.lote)>>',<<fn.u_margpct>>,<<IIF(ISNULL(fn.descFin_pcl),0,fn.descFin_pcl)>>,<<fn.num1>>,'<<ALLTRIM(fn.familia)>>', '<<ALLTRIM(fn.stamp_ext_doc)>>',
			 '<<ALLTRIM(fn.u_dtval)>>', <<fn.u_descenc>>, <<fn.u_oqt>>, <<fn.val_desp_lin>>, '<<ALLTRIM(fn.fistamp)>>' ) <<IIF(lcContador = lcNrTotalRegFN or lcContadorAux = lcNrTotalRegFN or lcContador > 454,'',',')>>
		ENDTEXT

		lcExecuteSQL  = lcExecuteSQL + Chr(13) + Alltrim(lcSQLFN1)

		*!*			IF lcContador > 455
		*!*				lcContador = 0
		*!*
		*!*				lcSQLFNAux = ''
		*!*				TEXT TO lcSQLFNAux NOSHOW TEXTMERGE PRETEXT 8
		*!*					INSERT INTO fn (fnstamp, fostamp, data, ref, oref, codigo, design,stns, usr1, usr2, usr3, usr4, usr5,
		*!*						 usr6, docnome, adoc, unidade, qtt, uni2qtt, iva, ivaincl, u_bonus, desconto, desc2, desc3, desc4,
		*!*						 tabiva, armazem, cpoc, lordem, epv, pv, etiliquido, tiliquido, u_psicont, u_bencont, qtrec, u_pvp,
		*!*						 u_validade, esltt, sltt, u_upc, eslvu, slvu,u_stockAct, u_epv1act, marcada,ousrinis, ousrdata, ousrhora,
		*!*						 usrinis, usrdata, usrhora, ofnstamp, bistamp, u_qttenc, u_upce, fmarcada, u_reserva, fnccusto, SUJINV,lote, u_margpct, descFin_pcl, num1, familia, stamp_ext_doc
		*!*					) VALUES
		*!*				ENDTEXT
		*!*
		*!*				lcExecuteSQL  = lcExecuteSQL + CHR(13) + ALLTRIM(lcSQLFNAux)
		*!*			ENDIF

	Endscan

	lcExecuteSQL  = lcExecuteSQL + Chr(13) + Chr(13) &&+ALLTRIM(lcSQL)


	&& Inser��o de linhas de desconto de compras
	If Used("ucrsfndesclin")
		**SELECT ucrsfndesclin
		**SELECT * FROM ucrsfndesclin WHERE !EMPTY(fnstamp) INTO CURSOR ucrsfndesclin_temp readwrite
		Select ucrsfndesclin
		Select Distinct fnstamp From ucrsfndesclin Where !Empty(fnstamp) Into Cursor ucrsfndesclin_stamp Readwrite
		Select ucrsfndesclin_stamp
		If Reccount("ucrsfndesclin_stamp") > 0
			Select ucrsfndesclin_stamp
			Go Top
			Scan
				lcSQLFN = ''
				TEXT TO lcSQLFN NOSHOW TEXTMERGE
					insert into fndesclin (fndesclinstamp, fnstamp ,nrlin ,qtd ,euro ,percentagem ,ousrdata ,ousrinis ,usrdata ,usrinis
					) VALUES
				ENDTEXT

				lcExecuteSQL  = lcExecuteSQL + Chr(13) + Alltrim(lcSQLFN)

				If Used("ucrsfndesclinAuxTemp")
					fecha("ucrsfndesclinAuxTemp")
				Endif
				lcstampDesc=Alltrim(ucrsfndesclin_stamp.fnstamp)
				lcContador = 0
				&&
				Select ucrsfndesclin
				Go Top
				Scan
					If Alltrim(ucrsfndesclin.fnstamp) == Alltrim(lcstampDesc)
						lcContador = lcContador + 1

						lcSQLFN1 = ""
						TEXT TO lcSQLFN1 NOSHOW TEXTMERGE PRETEXT 8
							( LEFT(newid(),21), '<<ALLTRIM(ucrsfndesclin.fnstamp)>>' ,<<ucrsfndesclin.nrlin>> ,<<ucrsfndesclin.qtd>> ,<<ucrsfndesclin.euro>> ,<<ucrsfndesclin.percentagem>>	, getdate()
							, '<<IIF(EMPTY(Alltrim(ucrsfndesclin.ousrinis)),ALLTRIM(m_chinis) ,ucrsfndesclin.ousrinis)>>'
							, getdate() , '<<m_chinis>>' ) <<IIF(lcContador = 3,'',',')>>
						ENDTEXT

						lcExecuteSQL  = lcExecuteSQL + Chr(13) + Alltrim(lcSQLFN1)
					Endif
					Select ucrsfndesclin
				Endscan
				Select ucrsfndesclin_stamp
			Endscan
		Endif
		**fecha("ucrsfndesclin_temp")
		fecha("ucrsfndesclin_stamp")

		&&_cliptext=lcExecuteSQL
	Endif

	Select FN
	Go Top
	Scan
		lcSQLEXTDOC = ""
		If !Empty(Alltrim(FN.stamp_ext_doc)) Then

			TEXT TO lcSQLEXTDOC NOSHOW TEXTMERGE PRETEXT 8
				update ext_doc_d SET stamplin='<<ALLTRIM(fn.fnstamp)>>' WHERE id_ext_doc = '<<ALLTRIM(fn.stamp_ext_doc)>>' AND ref='<<ALLTRIM(FN.REF)>>'
			ENDTEXT
			**uf_perguntalt_chama("atualizou..." ,"OK","",64)
			**_cliptext=lcSQLEXTDOC

		Endif
		lcExecuteSQL  = lcExecuteSQL + Chr(13) + Alltrim(lcSQLEXTDOC)

		** Update � MariaDb
		If Vartype(myMariaDb) == 'U'
			*****
		Else
			If myMariaDb=.T. Then

				lcConnection_stringCC = "DSN=MariaDB_MySQL_UNICODE;UID=logiserv;PWD=ls2k16...#;DATABASE=ltsfx;App=LT"
				myConnHandleCC = Sqlstringconn(lcConnection_stringCC)

				TEXT TO lcSQL TEXTMERGE NOSHOW
					update docs_d SET stamplinha='<<ALLTRIM(fn.fnstamp)>>' WHERE id_docs = '<<ALLTRIM(fn.stamp_ext_doc)>>' AND ref='<<ALLTRIM(FN.REF)>>'
				ENDTEXT
				SQLExec(myConnHandleCC, lcSQL ,"")

				**				TEXT TO lcSQL TEXTMERGE NOSHOW
				**					update docs set estado = 1 WHERE docs.id = '<<ALLTRIM(fn.stamp_ext_doc)>>' AND docs.id in (select distinct id_docs from docs_d where stamplinha='' AND id_docs = '<<ALLTRIM(fn.stamp_ext_doc)>>')
				**				ENDTEXT
				**				SQLEXEC(myConnHandleCC, lcSQL ,"")

				SQLDisconnect(myConnHandleCC)
			Endif
		Endif


	Endscan

	&& C�digo tem de ser posto de acordo com o c�digo acima, n�o � grave porque raramente � executado.
	If Upper(Alltrim(lcDoc)) == Upper("Mapa Honorarios")
		Select FN
		Go Top
		Scan
			Select FN
			TEXT TO lcSql NOSHOW TEXTMERGE PRETEXT 8
				DELETE FROM	FN_honorarios WHERE FNSTAMP = '<<alltrim(FN.fnstamp)>>'
				INSERT INTO FN_honorarios
					(	FNstamp
						,servmrstamp
						,data
						,EntidadeNome
						,EntidadeNo
						,Utente
						,UtenteNo
						,pvp
						,ValorUtente
						,ValorEntidade
						,despesa
						,honorario
						,HonorarioDeducao
						,HonorarioValor
						,HonorarioBi
						,HonorarioTipo
						,userno
						,id_cpt_val_cli
						,convencao
					 )
				Values
					(
						'<<ALLTRIM(FN.FNstamp)>>'
						,'<<ALLTRIM(FN.servmrstamp)>>'
						,'<<uf_gerais_getdate(ALLTRIM(FN.data_h),'SQL')>>'
						,'<<ALLTRIM(FN.EntidadeNome)>>'
						,<<FN.EntidadeNo>>
						,'<<ALLTRIM(FN.Utente)>>'
						,<<FN.UtenteNo>>
						,<<FN.pvp>>
						,<<FN.ValorUtente>>
						,<<FN.ValorEntidade>>
						,<<FN.despesa>>
						,<<FN.honorario>>
						,<<FN.HonorarioDeducao>>
						,<<FN.HonorarioValor>>
						,'<<ALLTRIM(FN.HonorarioBi)>>'
						,'<<ALLTRIM(FN.HonorarioTipo)>>'
						,<<FN.userno>>
						,'<<ALLTRIM(FN.id_cpt_val_cli)>>'
						,'<<ALLTRIM(FN.convencao)>>'
					 )
			ENDTEXT
			lcExecuteSQL  = lcExecuteSQL + Chr(13) + Chr(13) + lcSQL
		Endscan
	Endif

	** Verificar se � V/fatura e foi copiada de uma guia para atualizar pre�os

	If Upper(Alltrim(lcDoc)) == Upper("V/Factura") Or Upper(Alltrim(lcDoc)) == Upper("V/Factura Med.")
		Local lcExecuteSQLupdlinOri
		lcExecuteSQLupdlinOri = uf_documentos_updatelinhasorigem()
		lcExecuteSQL  = lcExecuteSQL + Chr(13) + Chr(13) + lcExecuteSQLupdlinOri
	Endif

	Return lcExecuteSQL

Endfunc

** Atualiza��o de valores na linha de origem do documento
Function uf_documentos_updatelinhasorigem()

	Local lcExecuteSQL1
	Store '' To lcExecuteSQL1

	** atualiza guias de transporte que movimentaram o stock
	Select FN
	Go Top
	Scan
		If !Empty(Alltrim(FN.ofnstamp)) And Empty(Alltrim(FN.ref)) And !Empty(Alltrim(FN.oref)) And FN.qtt>0
			TEXT TO lcSqlupdsource NOSHOW TEXTMERGE PRETEXT 8
				UPDATE fn set
					fn.epv=<<fn.epv>>
					,fn.u_upc=<<fn.u_upc>>
					,fn.etiliquido=fn.qtt*<<fn.u_upc>>
				where fnstamp='<<ALLTRIM(fn.ofnstamp)>>'
                    and docnome='V/Guia Transp.'
			ENDTEXT
			lcExecuteSQL1  = lcExecuteSQL1 + Chr(13) + Chr(13) + lcSqlupdsource
		Endif
	Endscan

	Return lcExecuteSQL1

Endfunc


** Caso seja uma transferencia de armazens
** Gravar o pcl na loja destino

Function uf_documentos_updatePclStDoc
	Local lcSQL

	lcSQL = ''
	TEXT TO lcSql NOSHOW TEXTMERGE
		SET Context_info 0x55556
		UPDATE
			ST
		SET
			st.epcult = bi.u_upc
			,st.usrdata = convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
			,st.usrhora  =convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108)
			,st.usrinis = '<<m_chinis>>'
		From
			st
			inner join bi on st.ref = bi.ref
		Where
			bi.ref != ''
			and st.site_nr = bi.ar2mazem
			and st.ref = bi.ref
			and bi.bostamp = '<<ALLTRIM(cabDoc.cabstamp)>>'

		SET Context_info 0x0
	ENDTEXT



	If !uf_gerais_actGrelha("", "", lcSQL)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA AO ATUALIZAR A INFORMA��O DO PRODUTO NO ARMAZ�M DESTINO! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		Return .F.
	Endif
	Return .T.
Endfunc


** Eventos ap�s a grava��o
Function uf_documentos_eventosAposGravar

	** Actualizar Margens na ST**
	Do Case
		Case myTipoDoc == "FO"

			**

			uf_documentos_updateMargensFoStDoc()


		Case myTipoDoc == "BO"
			&& Documento de Condi��es Comerciais , actualiza pre�os de custo ST no Armazem
			If cabdoc.NUMINTERNODOC = 40 And uf_gerais_getParameter("ADM0000000214","BOOL") == .T. && Armazem
				lcSQL = ''
				TEXT TO lcSql NOSHOW TEXTMERGE
					SET Context_info 0x55556

					UPDATE
						ST
					SET
						epcusto = bi.epu
						,epcult = bi.u_upc
						,usrdata = convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
						,usrhora  =convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108)
						,usrinis = '<<m_chinis>>'
					From
						st
						inner join bi on st.ref = bi.ref
					Where
						bi.bostamp = '<<cabDoc.cabstamp>>'
						and bi.ref != ''
						and st.site_nr = <<mysite_nr>>

					SET Context_info 0x0
				ENDTEXT

				If !uf_gerais_actGrelha("", "", lcSQL)
					uf_perguntalt_chama("OCORREU UMA ANOMALIA AO ATUALIZAR A INFORMA��O DO PRODUTO! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
					Return .F.
				Endif
			Endif

			If  cabdoc.NUMINTERNODOC = 46 && Conversao Unidades
				lcSQL = ''
				TEXT TO lcSQL NOSHOW TEXTMERGE
					SET Context_info 0x55556

					UPDATE
						ST
					SET
						epcult   = bi.edebito
						,usrdata = convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
						,usrhora = convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108)
						,usrinis = '<<m_chinis>>'
					From
						st
						inner join bi on st.ref = bi.ref
					Where
						bi.bostamp = '<<cabDoc.cabstamp>>'
						and bi.ref != ''
						and st.site_nr = <<mysite_nr>>
						and bi.lobs = 'Componente'

					SET Context_info 0x0

				ENDTEXT

				If !uf_gerais_actGrelha("", "", lcSQL)
					uf_perguntalt_chama("OCORREU UMA ANOMALIA AO ATUALIZAR A INFORMA��O DO PRODUTO! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
					Return .F.
				Endif
			Endif

			*!*				&& actualiza o PCL caso seja uma transferencia entre armazens
			*!*				IF  cabDoc.numinternodoc == 7
			*!*					IF(!uf_documentos_updatePclStDoc())
			*!*						RETURN .f.
			*!*					ENDIF
			*!*				ENDIF
			*!*				&& actualiza o PCL e PCT caso seja uma transferencia entre armazens e parametro empresa 92 ativo
			*!*				IF  cabDoc.numinternodoc == 7 AND uf_gerais_getParameter_site('ADM0000000092', 'BOOL', mySite)
			*!*					lcSQL = ''
			*!*					TEXT TO lcSQL NOSHOW TEXTMERGE
			*!*						update st
			*!*						set
			*!*							epcult=(select stt.epcult from st stt (nolock) where stt.ref=bi.ref and stt.site_nr=bi.armazem)
			*!*							,epcusto=(select stt.epcusto from st stt (nolock) where stt.ref=bi.ref and stt.site_nr=bi.armazem)
			*!*							,st.usrdata = convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
			*!*							,st.usrhora  =convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108)
			*!*							,st.usrinis = '<<m_chinis>>'
			*!*						from st (nolock)
			*!*						inner join bi (nolock) on st.ref = bi.ref
			*!*						where
			*!*							bi.ref != ''
			*!*							and st.site_nr=bi.ar2mazem
			*!*							and bi.bostamp = '<<cabDoc.cabstamp>>'
			*!*					ENDTEXT
			*!*
			*!*					IF !uf_gerais_actgrelha("", "", lcSQL)
			*!*						uf_perguntalt_chama("OCORREU UMA ANOMALIA AO ATUALIZAR O PCT E O PCL DO PRODUTO! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			*!*						RETURN .f.
			*!*					ENDIF
			*!*				ENDIF
			uf_documentos_updateMargensFoStDoc()

			Select Bi
			Go Top

			** tabela Precos, documentos Condi��es Comerciais
			Select cabdoc
			If cabdoc.NUMINTERNODOC == 40
				uf_documentos_actualizaTabelaPrecos()
			Endif

	Endcase



	** Fun��es Externas
	** Fun��es que altera as Guias quando n�o se usa Conferencia de Facturas
	uf_documentos_alteraGuiaSFO()

	** Campos usado FN, BI - PBruto - tratamento passou a ser feito no trigger LL 20151126
	**uf_documentos_actualizaQuantidadeMovimentada()



	*!*		** Verifica notifica��o Reserva
	*!*		SELECT cabdoc
	*!*		IF cabdoc.numInternoDoc == 55 OR cabdoc.numInternoDoc == 102 OR cabdoc.numInternoDoc == 101 OR cabdoc.numInternoDoc == 56
	*!*			** 	Chama painel reservas
	*!*			uf_documentos_Reservas()
	*!*			RETURN .t.
	*!*			**uf_documentos_verificaEnvioSMSReserva()
	*!*		ENDIF
	*!*
	*!*		** Reservas Impress�o Tal�o
	*!*		SELECT cabdoc
	*!*		IF uf_gerais_getParameter("ADM0000000013","BOOL") == .t. AND cabdoc.numInternoDoc == 55  OR cabdoc.numInternoDoc == 101
	*!*			SELECT fn
	*!*			LOCATE FOR u_reserva == .t.
	*!*			IF FOUND()
	*!*				IF uf_perguntalt_chama("Foram rececionados produtos que estavam reservados." + chr(13) + "Pretende imprimir tal�o com informa��o da Reserva","Sim","N�o",64)
	*!*					uf_Documentos_ImprimeTalaoPOSReservaIN()
	*!*				ENDIF
	*!*			ENDIF
	*!*		ENDIF
	*!*
	** Envio Documentos de Transporte
	If Used("ucrsTS")
		Select cabdoc
		If Inlist(ucrsts.tipodos, 2,3) And (Upper(Alltrim(ucrse1.PAIS))) == 'PORTUGAL'  &&cabdoc.numInternoDoc == 17 OR cabdoc.numInternoDoc == 16 OR cabdoc.numInternoDoc == 7  && Devolu��es // devolu��es VV // Tranf Entre Armazens
			uf_documentos_enviaDT()
		Endif
	Endif

	** Pedido de Regulariza��o
	uf_documentos_trataPedidoRegularizacao()



	** Prop�e gerar encomenda de Esgotados
	**uf_documentos_geraEncomendaEsgotados()


	** Central de Compras - Grupo INOUT
	**	IF !EMPTY(documentos.centralComprasImport)
	**		uf_documentos_actualizaDocCentralCompras()
	**	ENDIF

	** se estiver na conferencia robotica sai para o detalhe principal
	If (DOCUMENTOS.containerRECROBPEND.Visible)
		** fecha conferencia
		Select cabdoc
		TEXT TO lcsql NOSHOW textmerge
			UPDATE b_movtec_robot set num_foll='x' where ltrim(delivery_number)='<<ALLTRIM(cabdoc.numdoc)>>' and conferido=1
		ENDTEXT
		If !uf_gerais_actGrelha("", "", lcSQL)
			uf_perguntalt_chama("Ocorreu uma anomalia a fechar confer�ncia robotica! Por favor contacte o suporte.","OK","",16)
		Endif

		uf_CONFROBOTICA_Chama(.T.,"1")
	Endif



	Select cabdoc
	If cabdoc.NUMINTERNODOC == 55 Or cabdoc.NUMINTERNODOC == 102 Or cabdoc.NUMINTERNODOC == 101 Or cabdoc.NUMINTERNODOC == 56 Or cabdoc.NUMINTERNODOC == 58


		fecha("uCrsListaReservasCopy")
		**valida se tem reservas
		Local lcRef

		lcRef=uf_documentos_retornaRefsReservaListar("fn")

		**existem medicamentos
		If(Len(Alltrim(lcRef))>0)

			TEXT TO lcSQl TEXTMERGE noshow
				exec up_reservas_listar <<-1>>, <<-1>>, <<myArmazem>>, <<mysite_nr>>, '<<ALLTRIM(lcRef)>>'
			ENDTEXT

			If !uf_gerais_actGrelha("","uCrsListaReservasCopy",lcSQL)
				uf_perguntalt_chama("OCORREU UM ERRO A VERIFICAR SE O CLIENTE TEM RESERVAS DE ARTIGOS!","OK","",16)
				Return .F.
			Else

				If(Reccount("uCrsListaReservasCopy")>0)
					** 	Chama painel reservas
					uf_documentos_Reservas()
				Else
					**	uf_documentos_geraEncomendaEsgotados()
				Endif

			Endif


		Else

			**	uf_documentos_geraEncomendaEsgotados()

		Endif



		**uf_documentos_verificaEnvioSMSReserva()
		** Prop�e gerar encomenda de Esgotados
		uf_documentos_geraEncomendaEsgotados()

	Endif


Endfunc


**
Function uf_documentos_alteraGuiaSFO
	Select cabdoc
	If cabdoc.NUMINTERNODOC!=55 And cabdoc.NUMINTERNODOC!=58
		Return
	Endif

	Local lcValidate, lcValidaAltLinha, lcValidaAltLinhaPreco, lcValidaProdutosNovos
	lcValidate=''
	Store 0 To lcValidaAltLinha, lcValidaAltLinhaPreco, lcValidaProdutosNovos

	lcValidate= uf_gerais_getParameter("ADM0000000029","TEXT")

	If Upper(Alltrim(lcValidate))=='QUANTIDADES DA FACTURA'
		Local lcValAlt, lcValAviso
		Public lcValidaErro
		Store .F. To lcValidaErro, lcValAlt, lcValAviso

		If uf_gerais_getParameter("ADM0000000030","BOOL")
			lcValAviso=.T.
		Endif

		Select FN && validar se � necess�rios fazer altera��es e reportar ao utilizador
		Go Top
		Scan For !Empty(FN.ofnstamp) And !Empty(FN.oref)
			If uf_gerais_actGrelha("",[uCrsAltLinha],[select qtt, epv from fn where fnstamp=?fn.ofnstamp])
				If Reccount()>0
					lcValidaAltLinha		= uCrsAltLinha.qtt
					lcValidaAltLinhaPreco	= uCrsAltLinha.epv
				Endif
				fecha("uCrsAltLinha")
			Endif
			If lcValidaAltLinha != FN.qtt Or lcValidaAltLinhaPreco != FN.epv && valida se � suposto alterar linha da guia
				lcValAlt=.T.
			Endif
		Endscan

		If lcValAlt

		Else
			Return
		Endif

		** Actualizar linhas da Guia de Entrada/Transporte
		Select FN
		Go Top
		Scan For !Empty(FN.ofnstamp) And !Empty(FN.oref)
			** Verificar se � suposto alterar linha da Guia
			If uf_gerais_actGrelha("",[uCrsAltLinha],[select qtt, epv from fn where fnstamp=?fn.ofnstamp])
				If Reccount()>0
					lcValidaAltLinha		= uCrsAltLinha.qtt
					lcValidaAltLinhaPreco	= uCrsAltLinha.epv
				Endif
				fecha("uCrsAltLinha")
			Endif
			If lcValidaAltLinha != FN.qtt Or lcValidaAltLinhaPreco != FN.epv && valida se � suposto alterar linha da guia
				uf_documentos_alteraLinhasGuia()
			Endif

			lcValidaAltLinha=0
			Select FN
		Endscan

		If lcValidaErro
			uf_perguntalt_chama("OCORREU UMA ANOMALIA AO ACTUALIZAR A GUIA! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			Return
		Endif

		** guardar em cursor apenas os fostamps �nicos
		If Used("uCrsStampsGuiasFo")
			Select Distinct FOSTAMP From uCrsStampsGuiasFo Into Cursor uCrsStampsGuiasFoDistinct
		Else
			Return .F.
		Endif

		** calcular totais por documento
		uf_documentos_alteraTotaisGuia()
		********************************

		** fechar cursores
		If Used("uCrsStampsGuiasFo")
			fecha("uCrsStampsGuiasFo")
		Endif
		If Used("uCrsStampsGuiasFoDistinct")
			fecha("uCrsStampsGuiasFoDistinct")
		Endif

	Endif && fim valida se altera guia com base nas quantidades da factura
Endfunc


**
Function uf_documentos_alteraTotaisGuia
	Local VALORIVA1, VALORIVA2, VALORIVA3, VALORIVA4, VALORIVA5, totalIva, totalSemIva, totalComIva, TOTALNSERVICO
	Local lcFoStamp, lcOFnStamp, lcValidaErro, lcValidaAltLinha
	Store 0 To VALORIVA1, VALORIVA2, VALORIVA3, VALORIVA4, VALORIVA5, totalIva, totalSemIva, totalComIva, TOTALNSERVICO, lcValidaAltLinha
	Store '' To lcFoStamp, lcOFnStamp
	Store .F. To lcValidaErro

	Select uCrsStampsGuiasFoDistinct
	Go Top
	Scan
		* percorrer as linhas da guia alterada e guardar totais
		* NOTA: os totais antes dos arrendondamentos s�o mantidos iguais aos totais depois dos arredondamentos para efeitos de simplicidade. N�o devem existir arrendondamentos nas Guias
		If uf_gerais_actGrelha("",[uCrsFnGuia],[select * from fn where fn.fostamp=']+Alltrim(uCrsStampsGuiasFoDistinct.FOSTAMP)+['])
			If Reccount()>0
				Select uCrsFnGuia
				Go Top
				Scan
					Do Case
						Case uCrsFnGuia.tabiva=1
							If uCrsFnGuia.ivaincl
								VALORIVA1=VALORIVA1+(uCrsFnGuia.etiliquido/(uCrsFnGuia.IVA/100+1))
							Else
								VALORIVA1=VALORIVA1+(uCrsFnGuia.etiliquido*(uCrsFnGuia.IVA/100))
							Endif
						Case uCrsFnGuia.tabiva=2
							If uCrsFnGuia.ivaincl
								VALORIVA2=VALORIVA2+(uCrsFnGuia.etiliquido/(uCrsFnGuia.IVA/100+1))
							Else
								VALORIVA2=VALORIVA2+(uCrsFnGuia.etiliquido*(uCrsFnGuia.IVA/100))
							Endif
						Case uCrsFnGuia.tabiva=3
							If uCrsFnGuia.ivaincl
								VALORIVA3=VALORIVA3+(uCrsFnGuia.etiliquido/(uCrsFnGuia.IVA/100+1))
							Else
								VALORIVA3=VALORIVA3+(uCrsFnGuia.etiliquido*(uCrsFnGuia.IVA/100))
							Endif
						Case uCrsFnGuia.tabiva=4
							If uCrsFnGuia.ivaincl
								VALORIVA4=VALORIVA4+(uCrsFnGuia.etiliquido/(uCrsFnGuia.IVA/100+1))
							Else
								VALORIVA4=VALORIVA4+(uCrsFnGuia.etiliquido*(uCrsFnGuia.IVA/100))
							Endif
						Case uCrsFnGuia.tabiva=5
							If uCrsFnGuia.ivaincl
								VALORIVA5=VALORIVA5+(uCrsFnGuia.etiliquido/(uCrsFnGuia.IVA/100+1))
							Else
								VALORIVA5=VALORIVA5+(uCrsFnGuia.etiliquido*(uCrsFnGuia.IVA/100))
							Endif
					Endcase

					If uCrsFnGuia.ivaincl
						totalComIva=totalComIva + uCrsFnGuia.etiliquido
						totalSemIva=totalSemIva + (uCrsFnGuia.etiliquido/(uCrsFnGuia.IVA/100+1))
					Else
						totalComIva=totalComIva + (uCrsFnGuia.etiliquido*(uCrsFnGuia.IVA/100+1))
						totalSemIva=totalSemIva + (uCrsFnGuia.etiliquido)
					Endif

					If !uCrsFnGuia.stns
						If uCrsFnGuia.ivaincl
							TOTALNSERVICO=TOTALNSERVICO + ( uCrsFnGuia.etiliquido/(uCrsFnGuia.IVA/100+1) )
						Else
							TOTALNSERVICO=TOTALNSERVICO + uCrsFnGuia.etiliquido
						Endif
					Endif

					lcValidaAltTotais=.T.
					lcFoStamp = uCrsFnGuia.FOSTAMP
					Select uCrsFnGuia
				Endscan

				totalIva=VALORIVA1+VALORIVA2+VALORIVA3+VALORIVA4+VALORIVA5

				** Alterar totais da Guia
				If lcValidaAltTotais
					TEXT To lcSql Noshow textmerge
						Update FO
						Set etotal		=	<<Round(totalComIva,2)>>,
							eivain		=	<<Round(totalSemIva,2)>>,
							ettiva		=	<<Round(totalIva,2)>>,
							ettiliq		=	<<Round(totalSemIva,2)>>,
							eivainsns	=	<<Round(totalNservico,2)>>,
							eivav1		=	<<Round(valorIva1,2)>>,
							eivav2		=	<<Round(valorIva2,2)>>,
							eivav3		=	<<Round(valorIva3,2)>>,
							eivav4		=	<<Round(valorIva4,2)>>,
							eivav5		=	<<Round(valorIva5,2)>>,
							epaivav1	=	<<Round(valorIva1,2)>>,
							epaivav2	=	<<Round(valorIva2,2)>>,
							epaivav3	=	<<Round(valorIva3,2)>>,
							epaivav4	=	<<Round(valorIva4,2)>>,
							epaivav5	=	<<Round(valorIva5,2)>>,
							--eaivav1=, eaivav2=, eaivav3=, eaivav4=, eaivav5=,
							epaivain	=	<<Round(totalSemIva,2)>>,
							epatotal	=	<<Round(totalComIva,2)>>
						where fo.fostamp='<<lcFoStamp>>'
					ENDTEXT
					If !uf_gerais_actGrelha("","",lcSQL)
						uf_perguntalt_chama("OCORREU UMA ANOMALIA AO ACTUALIZAR TOTAIS DA GUIA. POR FAVOR CONTACTE O SUPORTE","OK","",16)
					Endif
				Endif

				** Reset aos valores
				Store 0 To VALORIVA1, VALORIVA2, VALORIVA3, VALORIVA4, VALORIVA5, totalIva, totalSemIva, totalComIva, TOTALNSERVICO, lcValidaAltLinha
				Store .F. To lcValidaAltTotais
				Store '' To lcFoStamp
			Endif

			fecha("uCrsFnGuia")
		Endif

		Select uCrsStampsGuiasFoDistinct
	Endscan && fim valida se altera totais guia
Endfunc


**
Function uf_documentos_alteraLinhasGuia

	Local lcConversao, lcRef
	Store 0 To lcConversao
	Store "" To lcRef

	Select FN

	lcRef = Iif(Empty(FN.ref),FN.oref,FN.ref)

	TEXT TO lcSql NOSHOW textmerge
		exec up_stocks_factorConversaoFl '<<ALLTRIM(lcRef)>>', <<cabdoc.no>>, <<mySite_nr>>
	ENDTEXT
	If uf_gerais_actGrelha("",[uCrsConv],lcSQL)
		If Reccount()>0
			Select FN
			lcConversao = Int(FN.qtt*uCrsConv.conversao)
		Endif
		fecha("uCrsConv")
	Endif

	TEXT TO mTXT NOSHOW textmerge
		UPDATE fn
		SET fn.u_opc=fn.u_upc, fn.u_upc=?fn.u_upc,
			fn.u_oqt=fn.qtt, fn.qtt=?fn.qtt, fn.uni2qtt=<<lcConversao>>,
			fn.u_bonus=?fn.u_bonus, fn.desconto=?fn.desconto, fn.desc2=?fn.desc2, fn.desc3=?fn.desc3, fn.desc4=?fn.desc4, fn.u_bonusmrg=?fn.u_bonusmrg, fn.u_desccom=?fn.u_desccom,
			fn.etiliquido=?fn.etiliquido, fn.tiliquido=?fn.tiliquido,
			fn.u_psicont=?fn.u_psicont, fn.u_bencont=?fn.u_bencont,
			fn.esltt=?fn.esltt, fn.sltt=?fn.sltt, fn.qtrec=?fn.qtrec,
			fn.taxaiva=?fn.taxaiva, fn.iva=?fn.iva, fn.tabiva=?fn.tabiva

		where fn.fnstamp=?fn.ofnstamp
	ENDTEXT
	If !uf_gerais_actGrelha("","",mTXT)
		lcValidaErro=.T.
	Else
		** Guardar stamps FO num cursor para posteriormente alterar totais
		If !Used("uCrsStampsGuiasFo")
			Create Cursor uCrsStampsGuiasFo (FOSTAMP c(25))
			Create Cursor uCrsStampsGuiasFoDistinct (FOSTAMP c(25))
		Endif
		If uf_gerais_actGrelha("",[uCrsGetFoStamps],[select fostamp from fn (nolock) where fnstamp=']+Alltrim(FN.ofnstamp)+['])
			If Reccount()>0
				Insert Into uCrsStampsGuiasFo (FOSTAMP) Values (Alltrim(uCrsGetFoStamps.FOSTAMP))
			Endif
			fecha("uCrsGetFoStamps")
		Endif
	Endif
Endfunc



*************************************
** PEDIDO DE REGULARIZA��O
*** Verifica necessidade de Gerar Pedido de Regulariza��o
*** Pressupostos:
*** Documento de Origem: V/Factura

Function uf_documentos_trataPedidoRegularizacao
	Local lcVarCount, lcDif
	Store 0 To lcVarCount, lcDif

	If uf_gerais_getParameter("ADM0000000332","BOOL")

		If  Alltrim(cabdoc.Doc) == "V/Factura"

			** Verifica se existem diferen�as que justifiquem o pedido de regulariza��o
			Select * From FN Where (FN.qtrec != FN.qtt Or FN.qtrec != FN.u_qttenc Or FN.qtt != FN.u_qttenc Or FN.u_upce != FN.u_upc) Into Cursor ucrsDiferencasPedidoReg Readwrite

			Select ucrsDiferencasPedidoReg
			If Reccount("ucrsDiferencasPedidoReg") > 0

				If uf_perguntalt_chama("Existem diferen�as de confer�ncia. Deseja imprimir um Pedido de Regulariza��o?","Sim","N�o")

					** Chama painel de Impress�o
					uf_imprimirgerais_Chama('DOCUMENTOS',.F.,.T.,"Pedido Regulariza��o")
				Endif

			Endif
			**
			If Used("ucrsDiferencasPedidoReg")
				fecha("ucrsDiferencasPedidoReg")
			Endif
		Endif

	Endif
Endfunc


*!*	*************************************
*!*	** AJUSTE
*!*	*** Trata Gera��o de Ajuste de Regulariza��o
*!*	*** Pressupostos:
*!*	*** Documento de Origem: Pedido de Regulariza��o
*!*	*** Documento de Destino: V/Nota de Credito
*!*	Function uf_documentos_trataAjusteRegularizacao
*!*		Local lcVarCount, lcDif
*!*		Store 0 To lcVarCount, lcDif

*!*		If  Alltrim(cabDoc.Doc) == "V/Nt. Cr�dito" OR Alltrim(cabDoc.Doc) == "V/Nt. D�bito"
*!*
*!*			lcSQL = ""
*!*			TEXT TO lcSQL TEXTMERGE NOSHOW
*!*				SELECT
*!*					DISTINCT
*!*					  '<<Alltrim(cabDoc.cabStamp)>>' as 'STAMPCRED'	,
*!*				        ISNULL((SELECT NMDOS FROM BI (nolock) WHERE BI.BISTAMP = FN.BISTAMP),'') as NUMERO,
*!*				        ISNULL((SELECT OBRANO FROM BI (nolock) WHERE BI.BISTAMP = FN.BISTAMP),0) as NUMERO,
*!*				        ISNULL((SELECT BOSTAMP FROM BI (nolock) WHERE BI.BISTAMP = FN.BISTAMP),'') as BOSTAMP,
*!*				        ABS((SELECT SUM(ETTDEB) FROM bi (nolock) WHERE BOSTAMP = ISNULL((SELECT BOSTAMP FROM BI (nolock) WHERE BI.BISTAMP = FN.BISTAMP),''))) as VALORPEDIDOREG,
*!*				        (SELECT SUM(ETILIQUIDO) FROM FN (nolock) a WHERE a.bistamp IN (SELECT bistamp FROM bi (nolock) WHERE BOSTAMP = ISNULL((SELECT BOSTAMP FROM BI (nolock) WHERE BI.BISTAMP = FN.BISTAMP),''))) as VALORVNCRED,
*!*				        (SELECT SUM(QTT) FROM bi (nolock) WHERE BOSTAMP = ISNULL((SELECT BOSTAMP FROM BI (nolock) WHERE BI.BISTAMP = FN.BISTAMP),'')) as QTTPEDIDOREG,
*!*				        (SELECT SUM(QTT) FROM FN (nolock) a WHERE a.bistamp IN ((SELECT bistamp FROM bi (nolock) WHERE BOSTAMP = ISNULL((SELECT BOSTAMP FROM BI (nolock) WHERE BI.BISTAMP = FN.BISTAMP),'')))) as QTTVNCRED,
*!*				        4 as tabiva, 0 as iva, CONVERT(bit,0) as ivaincl
*!*					FROM
*!*						FN (nolock)
*!*					WHERE
*!*						fostamp = '<<ALLTRIM(cabDoc.cabStamp)>>'
*!*					       AND     ISNULL((SELECT BOSTAMP FROM BI (nolock) WHERE BI.BISTAMP = FN.BISTAMP),'') != ''
*!*					       AND  '<<Alltrim(cabDoc.cabStamp)>>' NOT IN (SELECT U_FOSTAMP fROM bO2)
*!*			ENDTEXT
*!*			If !uf_gerais_actGrelha("","uc_DiferencasAjusteReg",lcSQL)
*!*				uf_perguntalt_chama("N�O FOI POSSIVEL DETERMINAR AS DIFEREN�AS PARA AJUSTE. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
*!*				Return
*!*			Endif
*!*
*!*			Select uc_DiferencasAjusteReg
*!*			Scan
*!*
*!*				If uc_DiferencasAjusteReg.VALORPEDIDOREG != uc_DiferencasAjusteReg.VALORVNCRED
*!*
*!*					lcDif = uc_DiferencasAjusteReg.VALORVNCRED - uc_DiferencasAjusteReg.VALORPEDIDOREG
*!*
*!*					lcStringPergunta = "Existe uma diferen�a de: " + Str(lcDif,9,2) + ". Deseja emitir um Documento de Ajuste?" + CHR(13) + 'Ir� proceder � cria��o de um documento de Ajuste de Regulariza��o.'
*!*					If uf_perguntalt_chama(lcStringPergunta,"Sim","N�o")
*!*						uf_documentos_CriaAjusteRegularizacao(uc_DiferencasAjusteReg.BOSTAMP)
*!*					Endif
*!*				Endif
*!*			Endscan
*!*		Endif

*!*		* Se for efectuado o Fecho do Pedido de Regulariza��o, sem importar nenhum Documento dever� ser gerado o pedido de regulariza��o
*!*		If Alltrim(cabDoc.Doc) == "Pedido de Regulariza��o"
*!*
*!*			Select cabDoc
*!*			If Alltrim(CabDoc.ESTADODOC) == "F"
*!*				** Verifica se existe liga��es/ obistamp
*!*				Select Bi
*!*				Go Top
*!*				Scan
*!*					If !Empty(Bi.bistamp)
*!*						lcSQL = ""
*!*						TEXT TO lcSQL TEXTMERGE NOSHOW
*!*							Select bistamp From bi Where obistamp ='<<Alltrim(Bi.BIstamp)>>'
*!*						ENDTEXT
*!*						If uf_gerais_actGrelha("","uc_verificaDestinosPedidos",lcSQL)
*!*							If Reccount("uc_verificaDestinosPedidos")>0
*!*								lcVarCount = lcVarCount + 1
*!*							Endif
*!*						Endif
*!*					Endif
*!*				Endscan

*!*				** N�o encontrou liga��es a outros Documentos
*!*				If lcVarCount == 0
*!*					* Cria Documento de Regulariza��o
*!*					If uf_perguntalt_chama("Deseja Proceder � cria��o de um Ajuste de Regulariza��o?" + CHR(13) + "Ir� proceder � cria��o de um documento de Ajuste de Regulariza��o.","Sim","N�o")
*!*						uf_documentos_CriaAjusteRegularizacaoPorFecho()
*!*					Endif
*!*				Endif
*!*			Endif
*!*		Endif
*!*	ENDFUNC


*!*	**
*!*	Function uf_documentos_CriaAjusteRegularizacao
*!*		Lparameters lcBostampPedidoRef
*!*		Local lcValorIva1, lcValorIva2,lcValorIva3, lcValorIva4,lcValorIva5, lcValorIva6,lcValorIva7, lcValorIva8,lcValorIva9, lcTotalFinal
*!*		Store 0 To lcValorIva1, lcValorIva2,lcValorIva3, lcValorIva4,lcValorIva5, lcValorIva6,lcValorIva7, lcValorIva8,lcValorIva9, lcTotalFinal
*!*		STORE 0 TO totalSemIva1, totalSemIva2, totalSemIva3, totalSemIva4, totalSemIva5,totalSemIva6, totalSemIva7, totalSemIva8, totalSemIva9,lcQtSum

*!*		Select uc_DiferencasAjusteReg
*!*		Go top
*!*		Scan

*!*			** Guardar n�mero do documento **
*!*			IF uf_gerais_actGrelha("",[uCrsNrMax],[select ISNULL((MAX(convert(numeric,obrano))),0) as nr from bo (nolock) where bo.ndos=39])
*!*				IF RECCOUNT()>0
*!*					lcNrDoc = uCrsNrMax.nr + 1
*!*				ENDIF
*!*				fecha("uCrsNrMax")
*!*			ELSE
*!*				uf_perguntalt_chama("O DOCUMENTO [AJUSTE DE REGULARIZA��O] N�O EST� A SER ENCONTRADO. O DOCUMENTO N�O SER� CRIADO.","OK","",16)
*!*				RETURN
*!*			ENDIF

*!*			lcbostamp = uf_gerais_stamp()
*!*			lcDocCode = 39
*!*			lcDocNome = 'Ajuste de Regulariza��o'
*!*			lcNome = cabDoc.Nome
*!*			lcNo = cabDoc.NO
*!*			lcEstab = cabDoc.ESTAB
*!*			lcMorada = cabDoc.Morada
*!*			lcLocal = cabDoc.Local
*!*			lcCodPost = cabDoc.CodPost
*!*			lcNcont = cabDoc.Ncont
*!*			lcMoeda = cabDoc.Moeda
*!*
*!*			lcTotalFinal = uc_DiferencasAjusteReg.VALORVNCRED - uc_DiferencasAjusteReg.VALORPEDIDOREG
*!*			totalSemIva = uc_DiferencasAjusteReg.VALORVNCRED - uc_DiferencasAjusteReg.VALORPEDIDOREG
*!*
*!*			DO CASE
*!*				CASE uc_DiferencasAjusteReg.tabiva=1
*!*					IF uc_DiferencasAjusteReg.ivaincl
*!*						lcValorIva1=lcValorIva1+(lcTotalFinal /(uc_DiferencasAjusteReg.iva/100+1))
*!*					ELSE
*!*						lcValorIva1=lcValorIva1+(lcTotalFinal *(uc_DiferencasAjusteReg.iva/100))
*!*					ENDIF
*!*				CASE uc_DiferencasAjusteReg.tabiva=2
*!*					IF fn.ivaincl
*!*						lcValorIva2=lcValorIva2+(lcTotalFinal /(uc_DiferencasAjusteReg.iva/100+1))
*!*					ELSE
*!*						lcValorIva2=lcValorIva2+(lcTotalFinal *(uc_DiferencasAjusteReg.iva/100))
*!*					ENDIF
*!*				CASE uc_DiferencasAjusteReg.tabiva=3
*!*					IF fn.ivaincl
*!*						lcValorIva3=lcValorIva3+(lcTotalFinal /(uc_DiferencasAjusteReg.iva/100+1))
*!*					ELSE
*!*						lcValorIva3=lcValorIva3+(lcTotalFinal *(uc_DiferencasAjusteReg.iva/100))
*!*					ENDIF
*!*				CASE uc_DiferencasAjusteReg.tabiva=4
*!*					IF fn.ivaincl
*!*						lcValorIva4=lcValorIva4+(lcTotalFinal /(uc_DiferencasAjusteReg.iva/100+1))
*!*					ELSE
*!*						lcValorIva4=lcValorIva4+(lcTotalFinal *(uc_DiferencasAjusteReg.iva/100))
*!*					ENDIF
*!*				CASE uc_DiferencasAjusteReg.tabiva=5
*!*					IF fn.ivaincl
*!*						lcValorIva5=lcValorIva5+(lcTotalFinal /(uc_DiferencasAjusteReg.iva/100+1))
*!*					ELSE
*!*						lcValorIva5=lcValorIva5+(lcTotalFinal *(uc_DiferencasAjusteReg.iva/100))
*!*					ENDIF
*!*			ENDCASE

*!*			TEXT TO lcSql NOSHOW textmerge
*!*				Insert into bo (bostamp, ndos, nmdos, obrano, dataobra,
*!*						nome, nome2, [no], estab, morada, [local], codpost, ncont,
*!*						etotaldeb, dataopen, boano, ecusto, etotal, moeda, memissao, origem, site,
*!*						vendedor, vendnm,
*!*						sqtt14, ebo_1tvall, ebo_2tvall, ebo_totp1, ebo_totp2,
*!*						ebo11_bins, ebo12_bins, ebo21_bins, ebo22_bins, ebo31_bins, ebo32_bins, ebo41_bins, ebo42_bins, ebo51_bins, ebo52_bins,
*!*						ebo11_iva, ebo12_iva, ebo21_iva, ebo22_iva, ebo31_iva, ebo32_iva, ebo41_iva, ebo42_iva, ebo51_iva, ebo52_iva,
*!*						ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora, obs, fechada
*!*						,pnome, pno)
*!*				Values ('<<ALLTRIM(lcBostamp)>>', <<lcDocCode>>, '<<lcDocNome>>', <<lcNrDoc>>, convert(varchar,GETDATE(),102),
*!*						'<<ALLTRIM(lcNome)>>', '', <<lcNo>>, <<lcEstab>>, '<<ALLTRIM(lcMorada)>>', '<<ALLTRIM(lcLocal)>>', '<<ALLTRIM(lcCodPost)>>', '<<ALLTRIM(lcNcont)>>',
*!*						<<ROUND(totalSemIva,2)>>,
*!*						convert(varchar,GETDATE(),102),
*!*						year(getdate()),
*!*						<<ROUND(totalSemIva,2)>>,
*!*						<<ROUND(totalSemIva,2)>>,
*!*						'<<ALLTRIM(lcMoeda)>>',
*!*						'EURO', 'BO', '<<ALLTRIM(mySite)>>',
*!*						<<ch_vendedor>>, '<<ALLTRIM(ch_vendnm)>>',
*!*						<<lcQtSum>>,
*!*						<<ROUND(totalSemIva,2)>>, <<ROUND(totalSemIva,2)>>, <<ROUND(totalSemIva,2)>>, <<ROUND(totalSemIva,2)>>,
*!*						<<totalSemIva1>>, <<totalSemIva1>>, <<totalSemIva2>>, <<totalSemIva2>>, <<totalSemIva3>>, <<totalSemIva3>>, <<totalSemIva4>>, <<totalSemIva4>>, <<totalSemIva5>>, <<totalSemIva5>>,
*!*						<<lcValorIva1>>, <<lcValorIva1>>, <<lcValorIva2>>, <<lcValorIva2>>, <<lcValorIva3>>, <<lcValorIva3>>, <<lcValorIva4>>, <<lcValorIva4>>, <<lcValorIva5>>, <<lcValorIva5>>,
*!*						'ADM', convert(varchar,getdate(),102), convert(varchar,getdate(),8), 'ADM', convert(varchar,getdate(),102), convert(varchar,getdate(),8),
*!*						'DOCUMENTO GERADO POR AJUSTE AUTOM�TICO',1
*!*						,'<<myTerm>>','<<myTermNo>>'
*!*				)
*!*				Endtext
*!*				IF !uf_gerais_actGrelha("","",lcSql)
*!*					uf_perguntalt_chama("OCORREU UM ERRO A GERAR DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
*!*					RETURN
*!*				ENDIF
*!*
*!*				TEXT TO lcSql NOSHOW textmerge
*!*					Insert into bo2 (bo2stamp, autotipo, pdtipo, etotalciva, armazem,
*!*									ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora, u_fostamp)
*!*					Values ('<<ALLTRIM(lcBostamp)>>', 1, 1, <<lcTotalFinal>>, <<cabdoc.armazem>>,
*!*							'ADM', convert(varchar,getdate(),102), convert(varchar,getdate(),8), 'ADM', convert(varchar,getdate(),102), convert(varchar,getdate(),8), '<<Alltrim(uc_DiferencasAjusteReg.STAMPCRED)>>')
*!*				ENDTEXT
*!*				IF !uf_gerais_actGrelha("","",lcSql)
*!*					uf_perguntalt_chama("OCORREU UM ERRO A GERAR DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
*!*					RETURN
*!*				ENDIF
*!*
*!*
*!*				** INSERE LINHA
*!*				lcBiStamp = uf_gerais_stamp()
*!*				** Contador para as linhas
*!*				lcOrdem = 10000
*!*
*!*				**Quantidade
*!*				lcQt = 1
*!*				lcQtAltern = 1
*!*				lcTotalLiq = lcTotalFinal
*!*				lcRef = ""

*!*				** Preencher Linhas da Bi **
*!*				TEXT TO lcSql NOSHOW textmerge
*!*					Insert into bi
*!*					(bistamp, bostamp, nmdos, obrano, ref, codigo,
*!*					design, qtt, qtt2, uni2qtt,
*!*					u_stockact, iva, tabiva, armazem, stipo, ndos, cpoc, familia,
*!*					no, nome, local, morada, codpost,
*!*					epu, edebito, eprorc, epcusto, ettdeb, ecustoind, edebitoori, u_upc,
*!*					rdata, dataobra, dataopen, resfor, lordem,
*!*					lobs,
*!*					ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora
*!*					)
*!*					Values (
*!*					'<<ALLTRIM(lcBiStamp)>>', '<<ALLTRIM(lcBoStamp)>>', '<<Alltrim(lcDocNome)>>', <<lcNrDoc>>, '<<ALLTRIM(lcRef)>>', '',
*!*					'AJUSTE DE REGULARIZA��O', <<lcQt>>, 0, <<lcQtAltern>>,
*!*					0, <<uc_DiferencasAjusteReg.iva>>, <<uc_DiferencasAjusteReg.tabiva>>, <<myarmazem>>, 4, <<lcDocCode>>,0, '',
*!*					<<lcNo>>, '<<ALLTRIM(lcNome)>>', '<<ALLTRIM(lcLocal)>>', '<<ALLTRIM(lcMorada)>>', '<<ALLTRIM(lcCodPost)>>',
*!*					<<lcTotalLiq / lcQt>>, <<lcTotalLiq / lcQt>>, <<lcTotalLiq / lcQt>>, <<lcTotalLiq / lcQt>>, <<lcTotalLiq>>, <<lcTotalLiq / lcQt>>, <<lcTotalLiq / lcQt>>, <<lcTotalLiq / lcQt>>,
*!*					convert(varchar,getdate(),102), convert(varchar,getdate(),102), convert(varchar,getdate(),102), 0, <<lcOrdem>>,
*!*					'',
*!*					'ADM', convert(varchar,getdate(),102), convert(varchar,getdate(),8), 'ADM', convert(varchar,getdate(),102), convert(varchar,getdate(),8)
*!*					)
*!*				Endtext
*!*				IF !uf_gerais_actGrelha("","",lcSql)
*!*					uf_perguntalt_chama("OCORREU UM ERRO A GERAR DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
*!*					RETURN
*!*				Endif
*!*
*!*				** Preencher Linhas da Bi2 **
*!*				TEXT TO lcSql NOSHOW textmerge
*!*					Insert into bi2
*!*							(bi2stamp, bostamp, morada, local, codpost)
*!*					Values ('<<ALLTRIM(lcBiStamp)>>', '<<ALLTRIM(lcBoStamp)>>', '<<ALLTRIM(lcMorada)>>', '<<ALLTRIM(lcLocal)>>', '<<ALLTRIM(lcCodPost)>>')
*!*				Endtext
*!*				IF !uf_gerais_actGrelha("","",lcSql)
*!*					uf_perguntalt_chama("OCORREU UM ERRO A GERAR DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
*!*					RETURN
*!*				ENDIF
*!*
*!*				uf_perguntalt_chama("DOCUMENTO E AJUSTE DE REGULARIZA��O CRIADO COM SUCESSO!","OK","",64)
*!*		Endscan
*!*	Endfunc


**
Function uf_documentos_CriaAjusteRegularizacaoPorFecho
	Local lcValorIva1, lcValorIva2,lcValorIva3, lcValorIva4,lcValorIva5, lcValorIva6,lcValorIva7, lcValorIva8,lcValorIva9, lcTotalFinal
	Store 0 To lcValorIva1, lcValorIva2,lcValorIva3, lcValorIva4,lcValorIva5, lcValorIva6,lcValorIva7, lcValorIva8,lcValorIva9, lcTotalFinal
	Store 0 To totalSemIva1, totalSemIva2, totalSemIva3, totalSemIva4, totalSemIva5,totalSemIva6, totalSemIva7, totalSemIva8, totalSemIva9, lcQtSum

	** Guardar n�mero do documento **
	If uf_gerais_actGrelha("",[uCrsNrMax],[select ISNULL((MAX(convert(numeric,obrano))),0) as nr from bo (nolock) where bo.ndos=39])
		If Reccount()>0
			lcNrDoc = uCrsNrMax.nr + 1
		Endif
		fecha("uCrsNrMax")
	Else
		uf_perguntalt_chama("O DOCUMENTO [AJUSTE DE REGULARIZA��O] N�O EST� A SER ENCONTRADO. O DOCUMENTO N�O SER� CRIADO.","OK","",16)
		Return
	Endif

	lcbostamp = uf_gerais_stamp()
	lcDocCode = 39
	lcDocNome = 'Ajuste de Regulariza��o'
	lcNome = cabdoc.Nome
	lcNo = cabdoc.NO
	lcEstab = cabdoc.ESTAB
	lcMorada = cabdoc.MORADA
	lcLocal = cabdoc.Local
	lcCodPost = cabdoc.CODPOST
	lcNcont = cabdoc.NCONT
	lcMoeda = cabdoc.MOEDA

	Select cabdoc
	lcTotalFinal 	= cabdoc.BASEINC
	totalSemIva = cabdoc.Total

	Select Bi
	Scan
		Do Case
			Case Bi.tabiva=1
				If Bi.ivaincl
					lcValorIva1=lcValorIva1+(Bi.ettdeb/(Bi.IVA/100+1))
				Else
					lcValorIva1=lcValorIva1+(Bi.ettdeb*(Bi.IVA/100))
				Endif
			Case Bi.tabiva=2
				If Bi.ivaincl
					lcValorIva2=lcValorIva2+(Bi.ettdeb/(Bi.IVA/100+1))
				Else
					lcValorIva2=lcValorIva2+(Bi.ettdeb*(Bi.IVA/100))
				Endif
			Case Bi.tabiva=3
				If Bi.ivaincl
					lcValorIva3=lcValorIva3+(Bi.ettdeb/(Bi.IVA/100+1))
				Else
					lcValorIva3=lcValorIva3+(Bi.ettdeb*(Bi.IVA/100))
				Endif
			Case Bi.tabiva=4
				If Bi.ivaincl
					lcValorIva4=lcValorIva4+(Bi.ettdeb/(Bi.IVA/100+1))
				Else
					lcValorIva4=lcValorIva4+(Bi.ettdeb*(Bi.IVA/100))
				Endif
			Case Bi.tabiva=5
				If Bi.ivaincl
					lcValorIva5=lcValorIva5+(Bi.ettdeb/(Bi.IVA/100+1))
				Else
					lcValorIva5=lcValorIva5+(Bi.ettdeb*(Bi.IVA/100))
				Endif
		Endcase
	Endscan

	TEXT TO lcSql NOSHOW textmerge
			Insert into bo (
				bostamp
				,ndos
				,nmdos
				,obrano
				,dataobra
				,nome
				,nome2
				,[no]
				,estab
				,morada
				,[local]
				,codpost
				,ncont
				,etotaldeb
				,dataopen
				,boano
				,ecusto
				,etotal
				,moeda
				,memissao
				,origem
				,site
				,vendedor
				,vendnm
				,sqtt14
				,ebo_1tvall
				,ebo_2tvall
				,ebo_totp1
				,ebo_totp2
				,ebo11_bins
				,ebo12_bins
				,ebo21_bins
				,ebo22_bins
				,ebo31_bins
				,ebo32_bins
				,ebo41_bins
				,ebo42_bins
				,ebo51_bins
				,ebo52_bins
				,ebo11_iva
				,ebo12_iva
				,ebo21_iva
				,ebo22_iva
				,ebo31_iva
				,ebo32_iva
				,ebo41_iva
				,ebo42_iva
				,ebo51_iva
				,ebo52_iva
				,ousrinis
				,ousrdata
				,ousrhora
				,usrinis
				,usrdata
				,usrhora
				,obs
				,fechada
				,pnome
				,pno
		)Values (
				'<<Alltrim(lcbostamp)>>'
				,<<lcDocCode>>
				,'<<lcDocNome>>'
				,<<lcNrDoc>>
				,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
				,'<<ALLTRIM(lcNome)>>'
				,''
				,<<lcNo>>
				,<<lcEstab>>
				,'<<ALLTRIM(lcMorada)>>'
				,'<<ALLTRIM(lcLocal)>>'
				,'<<ALLTRIM(lcCodPost)>>'
				,'<<ALLTRIM(lcNcont)>>'
				,<<ROUND(totalSemIva*-1,2)>>
				,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
				,year(dateadd(HOUR, <<difhoraria>>, getdate()))
				,<<ROUND(totalSemIva*-1,2)>>
				,<<ROUND(totalSemIva*-1,2)>>
				,'<<ALLTRIM(lcMoeda)>>'
				,'EURO'
				,'BO'
				,'<<ALLTRIM(mySite)>>'
				,<<ch_vendedor>>
				, '<<ALLTRIM(ch_vendnm)>>'
				,<<lcQtSum>>
				,<<ROUND(totalSemIva,2)>>
				,<<ROUND(totalSemIva,2)>>
				,<<ROUND(totalSemIva,2)>>
				,<<ROUND(totalSemIva,2)>>
				,<<totalSemIva1>>
				,<<totalSemIva1>>
				,<<totalSemIva2>>
				,<<totalSemIva2>>
				,<<totalSemIva3>>
				,<<totalSemIva3>>
				,<<totalSemIva4>>
				,<<totalSemIva4>>
				,<<totalSemIva5>>
				,<<totalSemIva5>>
				,<<lcValorIva1>>
				,<<lcValorIva1>>
				,<<lcValorIva2>>
				,<<lcValorIva2>>
				,<<lcValorIva3>>
				,<<lcValorIva3>>
				,<<lcValorIva4>>
				,<<lcValorIva4>>
				,<<lcValorIva5>>
				,<<lcValorIva5>>
				,'ADM'
				,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
				,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8)
				,'ADM'
				,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
				,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8)
				,'DOCUMENTO GERADO POR AJUSTE AUTOM�TICO - FECHO'
				,1
				,'<<myTerm>>'
				,'<<myTermNo>>'
		)
	ENDTEXT

	If !uf_gerais_actGrelha("","",lcSQL)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA AO GERAR DOCUMENTO. POR FAVOR CONTACTE O SUPORTE. BO","OK","",16)
		Return
	Endif

	TEXT TO lcSql NOSHOW textmerge
		INSERT INTO  bo2 (bo2stamp, autotipo, pdtipo, etotalciva,armazem, ousrinis, ousrdata, ousrhora, usrinis, usrdata, usrhora, status, modo_envio, pagamento, momento_pagamento)
		Values ('<<ALLTRIM(lcbostamp)>>', 1, 1, <<lcTotalFinal>>,<<cabdoc.armazem>>, 'ADM', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8), 'ADM', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8), '<<Alltrim(cabdoc.estadoenc)>>', '<<Alltrim(cabdoc.entrega)>>', '<<Alltrim(cabdoc.pagamento)>>', '<<Alltrim(cabdoc.momentopagam)>>')
	ENDTEXT

	If !uf_gerais_actGrelha("","",lcSQL)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA AO GERAR DOCUMENTO. POR FAVOR CONTACTE O SUPORTE. BO2","OK","",16)
		Return
	Endif

	** INSERE LINHAS
	Select Bi
	Go Top
	Scan

		lcBistamp = uf_gerais_stamp()
		** Contador para as linhas
		lcOrdem = 10000

		**Quantidade
		lcQt = 1
		lcQtAltern = 1
		lcTotalLiq = lcTotalFinal
		lcRef = ""

		** Preencher Linhas da Bi **
		TEXT TO lcSql NOSHOW textmerge
			Insert into bi(
				bistamp
				,bostamp
				,nmdos
				,obrano
				,ref
				,codigo
				,design
				,qtt
				,qtt2
				,uni2qtt
				,u_stockact
				,iva
				,tabiva
				,armazem
				,stipo
				,ndos
				,cpoc
				,familia
				,no
				,nome
				,local
				,morada
				,codpost
				,epu
				,edebito
				,eprorc
				,epcusto
				,ettdeb
				,ecustoind
				,edebitoori
				,u_upc
				,rdata
				,dataobra
				,dataopen
				,resfor
				,lordem
				,lobs
				,ousrinis
				,ousrdata
				,ousrhora
				,usrinis
				,usrdata
				,usrhora
				,obistamp
				,lote
				,num1
				,binum1
				,binum2
				,binum3
				,qtrec
				,fechada
				,usr2
			)Values ('<<ALLTRIM(lcBiStamp)>>', '<<ALLTRIM(lcbostamp)>>', '<<Alltrim(lcDocNome)>>', <<lcNrDoc>>, '<<ALLTRIM(lcRef)>>', '',
					'AJUSTE DE REGULARIZA��O', <<Bi.qtt *-1>>, 0, <<Bi.qtt2*-1>>,
					0, <<Bi.iva>>, <<Bi.tabiva>>, <<myarmazem>>, 4, <<lcDocCode>>,0, '',
					<<lcNo>>, '<<ALLTRIM(lcNome)>>', '<<ALLTRIM(lcLocal)>>', '<<ALLTRIM(lcMorada)>>', '<<ALLTRIM(lcCodPost)>>',
					<<BI.epu>>, <<Bi.edebito*-1>>, <<Bi.eprorc>>, <<Bi.epcusto>>, <<BI.ettdeb*-1>>, <<BI.ecustoind>>, <<BI.edebitoori>>, <<BI.u_upc>>,
					convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), 0, <<lcOrdem>>,
					'',
					'ADM', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8), 'ADM', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8),
					'<<Alltrim(Bi.bistamp)>>', '<<ALLTRIM(bi.lote)>>', <<bi.num1>>, <<bi.binum1>>, <<bi.binum2>>, <<bi.binum3>>, <<bi.qtrec>>, 1, '<<ALLTRIM(bi.usr2)>>'
			 )
		ENDTEXT

		If !uf_gerais_actGrelha("","",lcSQL)
			uf_perguntalt_chama("OCORREU UMA ANOMALIA AO GERAR DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			Return
		Endif

		** Preencher Linhas da Bi2 **
		TEXT TO lcSql NOSHOW textmerge
			Insert into bi2
					(bi2stamp, bostamp, morada, local, codpost, fistamp)
			Values ('<<ALLTRIM(lcBiStamp)>>', '<<ALLTRIM(lcbostamp)>>', '<<ALLTRIM(lcMorada)>>', '<<ALLTRIM(lcLocal)>>', '<<ALLTRIM(lcCodPost)>>', '<<ALLTRIM(bi2.fistamp)>>')
		ENDTEXT
		If !uf_gerais_actGrelha("","",lcSQL)
			uf_perguntalt_chama("OCORREU UMA ANOMALIA AO GERAR DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			Return
		Endif

		uf_perguntalt_chama("DOCUMENTO E AJUSTE DE REGULARIZA��O CRIADO COM SUCESSO!","OK","",64)
	Endscan
Endfunc


** Regras Antes da Grava��o de Documentos de Compras (FO)
Function uf_documentos_ValidacoesCompras
	Local lcValida, lcSQL
	Store .T. To lcValida
	Store '' To lcSQL

	Select cabdoc
	Go Top

	** N�o deixa gravar documentos sem linhas
	If !emdesenvolvimento
		lcValida=.F.
		Select FN
		Go Top
		Scan
			If !Empty(FN.ref) Or !Empty(FN.Design) Or !Empty(FN.oref)
				lcValida=.T.
			Endif
		Endscan
		If !lcValida
			uf_perguntalt_chama("N�O PODE GRAVAR DOCUMENTOS SEM LINHAS.","OK","",48)
			Return lcValida
		Endif
	Endif
	lcValida=.T.

	If Used("ucrsFnRefsDuplicadas")
		fecha("ucrsFnRefsDuplicadas")
	Endif
	&& V/FACTURA e V/GUIA TRANSP.
	If cabdoc.NUMINTERNODOC == 55 Or cabdoc.NUMINTERNODOC == 101
		&& Linhas Duplicadas
		Select Count(fnstamp),ref, Design From FN Where !Empty(Alltrim(FN.ref)) Group By ref, Design Having Count(fnstamp) > 1 Into Cursor ucrsFnRefsDuplicadas Readwrite
		Select ucrsFnRefsDuplicadas
		If Reccount("ucrsFnRefsDuplicadas")>0
			If !uf_perguntalt_chama("Aten��o! Existem refer�ncias duplicadas nas linhas. Pretende mesmo assim continuar?"+Chr(10)+Chr(13)+Chr(10)+Chr(13)+"Produto duplicado: "+ ucrsFnRefsDuplicadas.Design ,"Sim","N�o")
				Return .F.
			Endif
		Endif
	Endif
	If Used("ucrsFnRefsDuplicadas")
		fecha("ucrsFnRefsDuplicadas")
	Endif


	**

	** guardar se o documento mexe em stock **
	Local lcMexeStock, lcComprasEntrada
	Store .F. To lcMexeStock
	Store 0 To lcComprasEntrada

	Select ucrsConfigDoc
	lcMexeStock			= ucrsConfigDoc.folanSl
	lcComprasEntrada	= ucrsConfigDoc.fosl
	**

	** Avisa operador se existem psico ou benzo **
	Local lcCntPsico, lcCntBenzo
	Store 0 To lcCntPsico, lcCntBenzo

	If myDocIntroducao == .T. And lcMexeStock==.T.
		Select FN
		Go Top
		Locate For FN.qtt>0 And (!Empty(FN.psico) Or !Empty(FN.benzo))
		If Found()
			If !uf_perguntalt_chama("Aten��o! Vai gravar um documento com psicotr�picos ou benzodiazepinas. Ap�s a grava��o n�o ser� mais poss�vel eliminar estas refer�ncias do documento."+ Chr(13) + "Tem a certeza que pretende continuar?.","Sim","N�o")
				Return .F.
			Endif
		Endif
	Endif
	**

	** N�o deixa eliminar linhas que j� possuam contador de psico ou benzo **
	If myDocAlteracao
		Store 0 To lcCntPsico, lcCntBenzo

		Select cabdoc
		TEXT TO lcSQL NOSHOW TEXTMERGE
			select
				fn.ref, fn.design, fn.qtt, fn.u_psicont, fn.u_bencont
			from
				fn (nolock)
				inner join fprod (nolock) on fn.ref=fprod.cnp
			where
				(fprod.psico=1 or fprod.benzo=1)
				and fostamp='<<ALLTRIM(cabdoc.cabstamp)>>'
		ENDTEXT

		If uf_gerais_actGrelha("",[fnNaoApagaPsico],lcSQL)
			If Reccount()>0
				Select fnNaoApagaPsico
				Go Top
				Scan
					Store 0 To lcCntPsico, lcCntBenzo

					** psico
					Select FN
					Calculate Cnt(FN.qtt) To lcCntPsico For FN.u_psicont==fnNaoApagaPsico.u_psicont
					If lcCntPsico==0
						uf_perguntalt_chama("N�O PODE REMOVER UM PRODUTO QUE J� POSSUA CONTADOR PSICOCOTR�PICOS ATRIBU�DO. PRODUTO: "+Alltrim(fnNaoApagaPsico.Design)+Chr(10)+"ASSIM N�O PODE CONTINUAR.","OK","",48)
						lcValida = .F.
						Exit
					Endif

					** benzo
					Select FN
					Calculate Cnt(FN.qtt) To lcCntBenzo For FN.u_bencont==fnNaoApagaPsico.u_bencont
					If lcCntBenzo==0
						uf_perguntalt_chama("N�O PODE REMOVER UM PRODUTO QUE J� POSSUA CONTADOR BENZODIAZEPINAS ATRIBU�DO. PRODUTO: "+Alltrim(fnNaoApagaPsico.Design)+Chr(10)+"ASSIM N�O PODE CONTINUAR.","OK","",48)
						lcValida = .F.
						Exit
					Endif

					Select fnNaoApagaPsico
				Endscan
			Endif
			fecha("fnNaoApagaPsico")
		Endif
		If !lcValida
			Select FN
			Go Top
			Return .F.
		Endif
	Endif
	**

	** Controla PCTs negativos, descontos superiores a 100, bonus superiores a qtt, validades Ilegais **
	Local lcValidaPct, lcValidaDesc, lcValidaBonus, lcDesign, lcValidaValidade
	Store .F. To lcValidaPct, lcValidaDesc, lcValidaBonus, lcValidaValidade
	Store '' To lcDesign

	Select FN
	Go Top
	Scan
		If FN.desconto>100 Or FN.desc2>100 Or FN.desc3>100 Or FN.desc4>100
			lcValidaDesc=.T.
			lcDesign = FN.Design
		Endif
		If (FN.epv<0 Or FN.etiliquido<0) And FN.stns == .F.
			lcValidaPct=.T.
			lcDesign = FN.Design
		Endif
		If FN.u_bonus>FN.qtt And cabdoc.NUMINTERNODOC==55 And FN.u_bonus!=0
			lcValidaBonus=.T.
			lcDesign = FN.Design
		Endif
		If uf_gerais_getdate(FN.u_validade,"SQL") < "19000101" And !Empty(FN.u_validade)
			lcValidaValidade=.T.
			lcDesign = FN.Design
		Endif
	Endscan

	If lcValidaBonus
		uf_perguntalt_chama("N�O PODE TER QUANTIDADES DE BONUS SUPERIORES �S QUANTIDADES DO DOCUMENTO."+Chr(10)+"Produto: "+Alltrim(lcDesign),"OK","",16)
		Return .F.
	Endif
	If lcValidaPct
		uf_perguntalt_chama("N�O PODE TER PRE�OS A NEGATIVO."+Chr(10)+"Produto: ["+Alltrim(lcDesign)+"]","OK","",16)
		Return .F.
	Endif
	If lcValidaDesc
		uf_perguntalt_chama("O TOTAL DE DESCONTOS DA LINHA ["+Alltrim(lcDesign)+"] N�O PODE SER SUPERIOR A 100%.","OK","",48)
		Return .F.
	Endif
	If lcValidaValidade
		uf_perguntalt_chama("N�O PODE TER VALIDADES INFERIORES A 01.01.1900."+Chr(10)+Chr(10)+"PRODUTO: "+Alltrim(lcDesign),"OK","",48)
		Return .F.
	Endif
	**

	** Obriga o registo do documento de fornecedor nas N/Guia Entrada **
	Select cabdoc
	If cabdoc.NUMINTERNODOC=102
		If Empty(DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.page11.ContainerOutrosDados.DOCFL.Value) Or Empty(DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.page11.ContainerOutrosDados.DOCFLNO.Value)
			uf_perguntalt_chama("N�O PODE GRAVAR UMA GUIA DE ENTRADA SEM PREENCHER O DOCUMENTO/N� DO FORNECEDOR.","OK","",48)
			Return .F.
		Endif
	Endif
	**

	** valida se o n�mero de documento j� existe **
	If myDocIntroducao == .T. Or myDocAlteracao == .T.

		** incrementar o n�mero **
		Select ucrsConfigDoc
		If ucrsConfigDoc.nrseq == .T. && Tem Numero Sequencial

			Select cabdoc
			TEXT TO lcSql NOSHOW textmerge
				SELECT
					TOP 1 adoc
				FROM
					fo (nolock)
				where
					fo.adoc			= '<<ALLTRIM(cabdoc.numDoc)>>'
					and fo.doccode	= <<cabdoc.numInternoDoc>>
					and fo.foano	= <<YEAR(cabdoc.datadoc)>>
					and fo.fostamp != '<<ALLTRIM(cabdoc.cabstamp)>>'
			ENDTEXT
			If !uf_gerais_actGrelha("","cabdocnumverify",lcSQL)
				uf_perguntalt_chama("OCORREU UM PROBLEMA A VALIDAR N�MERO DO DOCUMENTO. POR FAVOR CONTACTE OP SUPORTE.","OK","",16)
				Return .F.
			Else
				If Reccount()>0
					uf_perguntalt_chama("J� EXISTE UM DOCUMENTO COM O MESMO N�MERO. O N�MERO SER� INCREMENTADO.","OK","",48)

					** incrementar o n�mero **
					Select cabdoc
					Replace cabdoc.NUMDOC With ASTR(Val(cabdoc.NUMDOC)+1)
					DOCUMENTOS.ContainerCab.NUMDOC.Refresh

					fecha("cabdocnumverify")
					Return .F.
				Endif
				fecha("cabdocnumverify")
			Endif
		Else && N�o tem Adoc Numerico
			Select cabdoc
			TEXT TO lcSql NOSHOW textmerge
				SELECT 	TOP 1 adoc
				FROM 	fo (nolock)
				where 	fo.adoc			=	'<<ALLTRIM(cabdoc.numDoc)>>'
						and fo.doccode	=	<<cabdoc.numInternoDoc>>
						and foano		=	<<YEAR(cabdoc.datadoc)>>
						and fo.no		=	<<cabdoc.no>>
						and fo.fostamp != 	'<<ALLTRIM(cabdoc.cabStamp)>>'
			ENDTEXT
			If !uf_gerais_actGrelha("","cabdocnumverify",lcSQL)
				uf_perguntalt_chama("OCORREU UM PROBLEMA A VALIDAR N�MERO DO DOCUMENTO. POR FAVOR CONTACTE OP SUPORTE.","OK","",16)
				Return .F.
			Else
				If Reccount()>0
					uf_perguntalt_chama("J� EXISTE UM DOCUMENTO COM O MESMO N�MERO.","OK","",48)
					fecha("cabdocnumverify")
					Return .F.
				Endif
				fecha("cabdocnumverify")

			Endif
			**
		Endif
	Endif
	**

	Select cabdoc
	Go Top

	** Actualiza contador Psicotr�picos / Benzodiazepinas ***
	Local lnContador, lnContador2
	Store '' To lcSQL
	Store 0 To lnContador, lnContador2

	If lcMexeStock == .T. And lcComprasEntrada < 50 && O Documento tem que movimentar stock como Entrada

		Select FN
		Scan For (!Empty(FN.ref)) And (FN.qtt>0)

			lcSQL = ""
			TEXT TO lcSQL NOSHOW TEXTMERGE
				SELECT
					psico
					,benzo
				FROM
					fprod (nolock)
				WHERE
					cnp='<<FN.ref>>'
			ENDTEXT

			If !uf_gerais_actGrelha("", "uCrsFProd", lcSQL)
				uf_perguntalt_chama("OCORREU UMA ANOMALIA NA VERIFICA��O DE PSICOTR�PICOS E BENZODIAZEPINAS.","OK","",16)
				Return .F.
			Endif

			If uCrsFProd.psico
				If FN.u_psicont=0
					If lnContador=0	&& Primeira contagem para as Linhas
						TEXT TO lcSql NOSHOW TEXTMERGE
							exec up_gerais_psico 0,'<<mysite>>'
						ENDTEXT

						If !uf_gerais_actGrelha("", "uCrsContador", lcSQL)
							uf_perguntalt_chama("OCORREU UMA ANOMALIA NA VERIFICA��O DE PSICOTR�PICOS E BENZODIAZEPINAS.","OK","",16)
							Return .F.
						Endif

						lnContador = uCrsContador.ct +1
						If Used("uCrsContador")
							fecha("uCrsContador")
						Endif
					Else
						lnContador = lnContador	+ 1
					Endif

					Select FN
					Replace FN.u_psicont With lnContador
				Endif
			Endif
			If uCrsFProd.benzo
				If FN.u_bencont=0
					If lnContador2=0	&& Primeira contagem para as Linhas
						TEXT TO lcSql NOSHOW TEXTMERGE
							exec up_gerais_benzo 0,'<<mysite>>'
						ENDTEXT
						uf_gerais_actGrelha("",[uCrsContador2],lcSQL)
						lnContador2 = uCrsContador2.ct +1
						fecha([uCrsContador2])
					Else
						lnContador2 = lnContador2	+ 1
					Endif
					Select FN
					Replace FN.u_bencont With lnContador2
				Endif
			Endif
			If Used("uCrsFProd")
				fecha("uCrsFProd")
			Endif
		Endscan
	Endif
	**

	Return lcValida
Endfunc


**
Function uf_documentos_AlteraEstadoDoc
	Local lcValidaPass

	&& Verifica no fecho de encomendas o pagamento
	If myTipoDoc == "BO" And Alltrim(cabdoc.Doc)=='Encomenda de Cliente' And Upper(Alltrim(ucrse1.PAIS)) != 'PORTUGAL'
		Local lctppagam
		lcSQL = ""
		TEXT To lcSql Noshow textmerge
			select pagamento from bo2 where bo2stamp='<<alltrim(cabdoc.cabstamp)>>'
		ENDTEXT
		uf_gerais_actGrelha("","ucrspagam",lcSQL)
		Select ucrspagam
		lctppagam = Alltrim(ucrspagam.pagamento)
		fecha("ucrspagam")
		If Alltrim(lctppagam) != 'Pag.Entrega'
			uf_perguntalt_chama("N�O PODE FECHAR UMA ENCOMENDA J� PAGA.","OK","",16)
			Return .F.
		Endif
	Endif

	If !Used("ucrsEstadoCombo")
		Create Cursor ucrsEstadoCombo (ESTADO c(25))
		Select ucrsEstadoCombo
		Append Blank
		Replace ucrsEstadoCombo.ESTADO With "A"
		Append Blank
		Replace ucrsEstadoCombo.ESTADO With "F"
	Endif

	lcValidaPass= uf_documentos_controlaPass()
	If !lcValidaPass
		Return .T.
	Endif

	If myDocAlteracao == .T. Or myDocIntroducao == .T.

		uf_valorescombo_chama(Sys(1272, DOCUMENTOS.ContainerCab.ESTADO), 1, "ucrsEstadoCombo", 2, "estado", "estado")

		With DOCUMENTOS.ContainerCab
			.ESTADO.DisabledBackColor = Iif(Alltrim(DOCUMENTOS.ContainerCab.ESTADO.Value) == "A",Rgb[232,76,61],Rgb(255,255,255))
			.ESTADO.ForeColor = Iif(Alltrim(DOCUMENTOS.ContainerCab.ESTADO.Value) == "A",Rgb(255,255,255),Rgb(0,0,0))
			.ESTADO.FontBold = Iif(Alltrim(DOCUMENTOS.ContainerCab.ESTADO.Value) == "A",.T.,.F.)
		Endwith


		Return .T.
	Else

		** Validado a Permiss�o
		If uf_perguntalt_chama("Quer mesmo alterar o estado do Documento? "+ Chr(13) + "Ir� proceder � grava��o na Base de Dados.","Sim","N�o")

			regua(0,100,"A PROCESSAR A ALTERA��O DO ESTADO DO DOCUMENTO...",.F.)

			If myTipoDoc == "FO"
				Select cabdoc
				If Alltrim(Upper(cabdoc.ESTADODOC)) == "A"
					lcSQL = ""
					TEXT To lcSql Noshow textmerge
                  Update
                     FO
                  Set
                     U_STATUS = 'F'
                  Where
                     Fostamp = '<<Alltrim(cabDoc.cabstamp)>>'
					ENDTEXT
					If !uf_gerais_actGrelha("","",lcSQL)
						uf_perguntalt_chama("N�O FOI POSS�VEL ALTERAR O ESTADO DO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
						Return
					ELSE
						LCSQL = ''
						TEXT TO lcsql TEXTMERGE NOSHOW
									INSERT into B_ocorrencias
										(stamp, linkstamp, tipo, grau, descr,
										ovalor, dvalor,
										usr, date, site)
									values
										(LEFT(newid(),25), '', 'Documentos', 2, 'Fecho manual do documento: '+ ALLTRIM(Cabdoc.Doc) + ' N�' + ALLTRIM(cabdoc.numdoc),
										'Aberto', 'Fechado',
										<<ch_userno>>, dateadd(HOUR, <<difhoraria>>, getdate()), '<<mySite>>')
                      ENDTEXT
                      IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
                         uf_perguntalt_chama("OCORREU UM PROBLEMA A GUARDAR REGISTO DE ALTERA��O DO ESTADO DO DOCUMENTO.", "OK", "", 16)
                         RETURN .F.
                      ENDIF
					Endif

					Select cabdoc
					Replace cabdoc.ESTADODOC With "F"
				Else
					lcSQL = ""
					TEXT To lcSql Noshow textmerge
	                  Update
	                     FO
	                  Set
	                     U_STATUS = 'A'
	                  Where
                     	Fostamp = '<<Alltrim(cabDoc.cabstamp)>>'
					ENDTEXT
					If !uf_gerais_actGrelha("","",lcSQL)
						uf_perguntalt_chama("N�O FOI POSS�VEL ALTERAR O ESTADO DO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
						RETURN
					ELSE 
						LCSQL = ''
						TEXT TO lcsql TEXTMERGE NOSHOW
									INSERT into B_ocorrencias
										(stamp, linkstamp, tipo, grau, descr,
										ovalor, dvalor,
										usr, date, site)
									values
										(LEFT(newid(),25), '', 'Documentos', 2, 'Fecho manual do documento: '+ ALLTRIM(Cabdoc.Doc) + ' N�' + ALLTRIM(cabdoc.numdoc),
										'Fechado', 'Aberto',
										<<ch_userno>>, dateadd(HOUR, <<difhoraria>>, getdate()), '<<mySite>>')
                      ENDTEXT
                      IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
                         uf_perguntalt_chama("OCORREU UM PROBLEMA A GUARDAR REGISTO DE ALTERA��O DO ESTADO DO DOCUMENTO.", "OK", "", 16)
                         RETURN .F.
                      ENDIF
					Endif
					Select cabdoc
					Replace cabdoc.ESTADODOC With "A"
				Endif
			Endif
			If myTipoDoc == "BO"
				If Alltrim(Upper(cabdoc.ESTADODOC)) == "A"

					Local uv_sql

					TEXT TO uv_sql NOSHOW TEXTMERGE
		                  Update BO SET
		                     bo.Fechada = 1,
		                     DATAFECHO = dateadd(HOUR, <<difhoraria>>, getdate()),
		                     usrdata = dateadd(HOUR, <<difhoraria>>, getdate()),
		                     usrhora = convert(varchar,getdate(),8),
		                     usrinis =	'<<m_chinis>>'
		                  WHERE bo.Bostamp = '<<Alltrim(cabDoc.cabstamp)>>'

		                  Update BI SET
		                     bi.Fechada = 1,
		                     usrdata = dateadd(HOUR, <<difhoraria>>, getdate()),
		                     usrhora = convert(varchar,getdate(),8),
		                     usrinis	=	'<<m_chinis>>'
		                  WHERE bi.Bostamp = '<<Alltrim(cabDoc.cabstamp)>>'

	                   	UPDATE bo2 SET status='Cancelada', usrdata = dateadd(HOUR, <<difhoraria>>, getdate()), usrhora = convert(varchar,getdate(),8),   usrinis	=	'<<m_chinis>>' from bo2 inner join bo on bo.bostamp=bo2.bo2stamp WHERE bo2stamp='<<Alltrim(cabDoc.cabstamp)>>' and bo.nmdos='Encomenda de Cliente' and status not like '%faturad%'  and status not like '%entregue%'
					ENDTEXT

					If uf_gerais_getParameter_Site('ADM0000000199', 'BOOL', mySite) And Used("ucrsts") And ucrsts.tipodos = 5

						If !uf_gerais_runSQLCentral(uv_sql)
							uf_perguntalt_chama("Erro a atualizar a Encomenda na Central.","OK","",16)
							Return .F.
						Endif

					Endif

					If !uf_gerais_actGrelha("","",uv_sql)
						uf_perguntalt_chama("N�O FOI POSS�VEL ALTERAR O ESTADO DO DOCUMENTO! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
						Return .F.
					ELSE 
						LCSQL = ''
						TEXT TO lcsql TEXTMERGE NOSHOW
									INSERT into B_ocorrencias
										(stamp, linkstamp, tipo, grau, descr,
										ovalor, dvalor,
										usr, date, site)
									values
										(LEFT(newid(),25), '', 'Documentos', 2, 'Fecho manual do documento: '+ ALLTRIM(Cabdoc.Doc) + ' N�' + ALLTRIM(cabdoc.numdoc),
										'Aberto', 'Fechado',
										<<ch_userno>>, dateadd(HOUR, <<difhoraria>>, getdate()), '<<mySite>>')
                      ENDTEXT
                      IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
                         uf_perguntalt_chama("OCORREU UM PROBLEMA A GUARDAR REGISTO DE ALTERA��O DO ESTADO DO DOCUMENTO.", "OK", "", 16)
                         RETURN .F.
                      ENDIF
					Endif
					Select cabdoc
					Replace cabdoc.ESTADODOC With "F"

					**
					* Se for efectuado o Fecho do Pedido de Regulariza��o, sem importar nenhum Documento dever� ser gerado o pedido de regulariza��o
					If Alltrim(cabdoc.Doc) = "Pedido de Regulariza��o"

						Select cabdoc
						If Alltrim(cabdoc.ESTADODOC) == "F"
							** Verifica se existe liga��es/ obistamp
							Select Bi
							Go Top
							Scan
								If !Empty(Bi.bistamp)
									lcSQL = ""
									TEXT TO lcSQL TEXTMERGE NOSHOW
                              			Select bistamp From bi Where obistamp ='<<Alltrim(Bi.BIstamp)>>'
									ENDTEXT
									If uf_gerais_actGrelha("","uc_verificaDestinosPedidos",lcSQL)
										If Reccount("uc_verificaDestinosPedidos")>0
											lcVarCount = lcVarCount + 1
										Endif
									Endif
								Endif
							Endscan

							** N�o encontrou liga��es a outros Documentos
							If lcVarCount == 0
								* Cria Documento de Regulariza��o
								If uf_perguntalt_chama("Deseja Proceder � cria��o de um Ajuste de Regulariza��o?"+ Chr(13) + "Ir� proceder � cria��o de um documento de Ajuste de Regulariza��o.","Sim","N�o")
									uf_documentos_CriaAjusteRegularizacaoPorFecho()
									LCSQL = ''
									TEXT TO lcsql TEXTMERGE NOSHOW
												INSERT into B_ocorrencias
													(stamp, linkstamp, tipo, grau, descr,
													ovalor, dvalor,
													usr, date, site)
												values
													(LEFT(newid(),25), '', 'Documentos', 2, 'Fecho manual do documento: '+ ALLTRIM(Cabdoc.Doc) + ' N�' + ALLTRIM(cabdoc.numdoc),
													'Aberto', 'Fechado',
													<<ch_userno>>, dateadd(HOUR, <<difhoraria>>, getdate()), '<<mySite>>')
			                      ENDTEXT
			                      IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
			                         uf_perguntalt_chama("OCORREU UM PROBLEMA A GUARDAR REGISTO DE ALTERA��O DO ESTADO DO DOCUMENTO.", "OK", "", 16)
			                         RETURN .F.
			                      ENDIF
								Endif
							Endif
						Endif
					Endif
				Else
					&& fix para cen�rio especifico das reservas em que documentos s�o reabertos manualmente e geram erros no atendimento
					If cabdoc.NUMINTERNODOC = 5
						TEXT TO uv_sql NOSHOW TEXTMERGE
                     Update BO SET
					 	bo.Fechada = 0 ,
						usrdata = dateadd(HOUR, <<difhoraria>>, getdate()),
                     	usrhora = convert(varchar,getdate(),8),
                     	usrinis =	'<<m_chinis>>'
					 WHERE bo.Bostamp = '<<Alltrim(cabDoc.cabstamp)>>'
                     Update BI SET
					 	bi.Fechada = 0, qtt2 = 0,
						usrdata = dateadd(HOUR, <<difhoraria>>, getdate()),
                     	usrhora = convert(varchar,getdate(),8),
                     	usrinis =	'<<m_chinis>>'
					 WHERE bi.Bostamp = '<<Alltrim(cabDoc.cabstamp)>>'
					 UPDATE bo2
					 SET
					 	usrdata = dateadd(HOUR, <<difhoraria>>, getdate()),
                     	usrhora = convert(varchar,getdate(),8),
                     	usrinis =	'<<m_chinis>>'
					WHERE bo2stamp = '<<Alltrim(cabDoc.cabstamp)>>'
						ENDTEXT
					Else
						TEXT TO uv_sql NOSHOW TEXTMERGE
                     Update BO SET
					 	Fechada = 0,
						usrdata = dateadd(HOUR, <<difhoraria>>, getdate()),
                     	usrhora = convert(varchar,getdate(),8),
                     	usrinis =	'<<m_chinis>>'
					 WHERE bo.Bostamp = '<<Alltrim(cabDoc.cabstamp)>>'
                     Update BI SET
					 	bi.Fechada = 0,
						usrdata = dateadd(HOUR, <<difhoraria>>, getdate()),
                     	usrhora = convert(varchar,getdate(),8),
                     	usrinis =	'<<m_chinis>>'
					 WHERE bi.Bostamp = '<<Alltrim(cabDoc.cabstamp)>>'
					 UPDATE bo2
					 SET
					 	usrdata = dateadd(HOUR, <<difhoraria>>, getdate()),
                     	usrhora = convert(varchar,getdate(),8),
                     	usrinis =	'<<m_chinis>>'
					WHERE bo2stamp = '<<Alltrim(cabDoc.cabstamp)>>'
						ENDTEXT
					Endif

					If uf_gerais_getParameter_Site('ADM0000000199', 'BOOL', mySite) And Used("ucrsts") And ucrsts.tipodos = 5

						If !uf_gerais_runSQLCentral(uv_sql)
							uf_perguntalt_chama("Erro a atualizar a Encomenda na Central.","OK","",16)
							Return .F.
						Endif

					Endif

					If !uf_gerais_actGrelha("","",uv_sql)
						uf_perguntalt_chama("N�O FOI POSS�VEL ALTERAR O ESTADO DO DOCUMENTO! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
						RETURN
					ELSE
						LCSQL = ''
						TEXT TO lcsql TEXTMERGE NOSHOW
									INSERT into B_ocorrencias
										(stamp, linkstamp, tipo, grau, descr,
										ovalor, dvalor,
										usr, date, site)
									values
										(LEFT(newid(),25), '', 'Documentos', 2, 'Fecho manual do documento: '+ ALLTRIM(Cabdoc.Doc) + ' N�' + ALLTRIM(cabdoc.numdoc),
										'Fechado', 'aberto',
										<<ch_userno>>, dateadd(HOUR, <<difhoraria>>, getdate()), '<<mySite>>')
                      ENDTEXT
                      IF  .NOT. uf_gerais_actgrelha("", "", lcsql)
                         uf_perguntalt_chama("OCORREU UM PROBLEMA A GUARDAR REGISTO DE ALTERA��O DO ESTADO DO DOCUMENTO.", "OK", "", 16)
                         RETURN .F.
                      ENDIF
					Endif
					Select cabdoc
					Replace cabdoc.ESTADODOC With "A"
				Endif
			Endif
		Endif
		regua(2)

		With DOCUMENTOS.ContainerCab
			.ESTADO.DisabledBackColor = Iif(Alltrim(DOCUMENTOS.ContainerCab.ESTADO.Value) == "A",Rgb[232,76,61],Rgb(255,255,255))
			.ESTADO.ForeColor = Iif(Alltrim(DOCUMENTOS.ContainerCab.ESTADO.Value) == "A",Rgb(255,255,255),Rgb(0,0,0))
			.ESTADO.FontBold = Iif(Alltrim(DOCUMENTOS.ContainerCab.ESTADO.Value) == "A",.T.,.F.)
		Endwith
		DOCUMENTOS.ContainerCab.Refresh
	Endif
Endfunc


**
Function uf_documentos_actualizaDataFinalLinhas

	If myTipoDoc == "BO"
		Select cabdoc
		Select Bi
		Go Top
		Scan
			Replace Bi.DATAFINAL With cabdoc.DATAFINAL
		Endscan
	Endif

Endfunc


**
Function uf_documentos_CalculaDataVencimento
	Lparameters tcBool
	Local lcSQL

	** Evento da Data
	If tcBool
		If cabdoc.DATADOC <= cabdoc.DATAVENC And Empty(cabdoc.CONDPAG)
			Return
		Endif
	Endif

	Select cabdoc
	If !Empty(cabdoc.DATADOC)
		lcSQL = ""
		TEXT TO lcSQL TEXTMERGE NOSHOW
			exec up_Documentos_CondPag '<<CabDoc.no>>', '<<CabDoc.estab>>', '<<Alltrim(CabDOC.CONDPAG)>>','<<uf_gerais_getDate(cabDoc.dataDoc,"SQL")>>'
		ENDTEXT


		If uf_gerais_actGrelha("","uc_DataVencimento",lcSQL)
			If Reccount("uc_DataVencimento") > 0
				Replace cabdoc.DATAVENC With uc_DataVencimento.DATAVENCIMENTO
			Else
				Replace cabdoc.DATAVENC With cabdoc.DATADOC
			Endif
			fecha("uc_DataVencimento")
		Else
			Replace cabdoc.DATAVENC With cabdoc.DATADOC
		Endif

		**Actualiza Numero de Documento em fun��o do Ano da Data
		If myDocAlteracao == .F.
			uf_documentos_controlaNumeroDoc()
		Endif

		DOCUMENTOS.ContainerCab.Refresh
	Endif
Endfunc


*!*	**
*!*	FUNCTION uf_documentos_DataVencimentoFornec
*!*			Local lcSQL
*!*
*!*			Select CabDoc
*!*			If Empty(Alltrim(cabDoc.CONDPAG))
*!*				return
*!*			Endif
*!*
*!*			If !Empty(cabDoc.dataDoc)
*!*				lcSQL = ""
*!*				TEXT TO lcSQL TEXTMERGE NOSHOW
*!*					exec up_Documentos_CondPag '<<CabDoc.no>>', '<<CabDoc.estab>>', '<<Alltrim(CabDOC.CONDPAG)>>','<<uf_gerais_getDate(cabDoc.dataDoc,"SQL")>>'
*!*				ENDTEXT
*!*				If uf_gerais_actGrelha("","ucrsDataVencimento",lcSQL)
*!*					If Reccount("ucrsDataVencimento") > 0
*!*						Replace cabDoc.DATAVENC With ucrsDataVencimento.DATAVENCIMENTO
*!*					Else
*!*						Replace cabDoc.datavenc with cabdoc.datadoc
*!*					Endif
*!*					Fecha("ucrsDataVencimento")
*!*				Else
*!*					Replace cabDoc.datavenc with cabdoc.datadoc
*!*				Endif
*!*
*!*				**Actualiza Numero de Documento em fun��o do Ano da Data
*!*				If myDocAlteracao == .f.
*!*					uf_documentos_controlaNumeroDoc()
*!*				Endif
*!*
*!*				DOCUMENTOS.ContainerCab.refresh
*!*			ENDIF
*!*	ENDFUNC


**
Function uf_documentos_seltipoDoc
	Local lcSQL

	** limpar dados entidade/fornecedor/cliente
	uf_documentos_limpaNome()
	uf_documentos_ConfiguracoesDoc()
	uf_documentos_alternaMenu()


	Select cabdoc
	Replace cabdoc.SITE With mySite
	DOCUMENTOS.Refresh

	**Cria 1a linha por defeito
	uf_documentos_criaNovaLinha(.T.,.T.)

Endfunc


**
Function uf_documentos_ConfiguracoesDoc
	Lparameters tcBool, tcDuplicaDoc

	If !Used("cabDoc")
		Return .F.
	Endif

	Select cabdoc
	If Empty(Alltrim(cabdoc.Doc))
		Return .F.
	Endif

	Local lcSQL
	lcSQL = ""
	TEXT TO lcSQL TEXTMERGE NOSHOW
		exec up_documentos_configDoc '<<ALLTRIM(cabDoc.doc)>>', '<<ALLTRIM(mySite)>>'
	ENDTEXT
	If !uf_gerais_actGrelha("","ucrsConfigDoc",lcSQL)
		Return .F.
	Endif


	myTipoDoc = ucrsConfigDoc.tipoDoc

	If tcBool == .F. && Se vier a .f. limpa dados do documento
		uf_documentos_preencheCabDoc('')
		uf_documentos_valoresDefeitoCab()
		uf_documentos_controlaNumeroDoc()
		uf_documentos_apagaRegistosLinhas()
		uf_documentos_controlaWkLinhas()
		uf_documentosControlaObj()

		DOCUMENTOS.PageFrame1.Page1.gridDoc.Refresh
	Endif

	&& a .t. mantem dados do doc, altera s� o necess�rio para ser poss�vel gerar novo doc com as mesmas linhas
	If tcDuplicaDoc == .T.
		uf_documentos_valoresDefeitoCab()
		uf_documentos_controlaNumeroDoc()
		uf_documentos_controlaWkLinhas()
		uf_documentosControlaObj()

		DOCUMENTOS.PageFrame1.Page1.gridDoc.Refresh
	Endif

	If Alltrim(cabdoc.Doc)=='V/Factura' Or Alltrim(cabdoc.Doc)=='V/Factura Med.' Or Alltrim(cabdoc.Doc)=='V/Guia Transp.'
		DOCUMENTOS.PageFrame1.Page1.ShapeEsgot.Visible = .T.
		DOCUMENTOS.PageFrame1.Page1.BtnEsgot.Visible = .T.
	Else
		DOCUMENTOS.PageFrame1.Page1.ShapeEsgot.Visible = .F.
		DOCUMENTOS.PageFrame1.Page1.BtnEsgot.Visible = .F.
	Endif

	uf_documentos_controlaTipoEntidade()

Endfunc


** Controla Tipo de Entidade em fun��o do Documento Escolhido
** Comntrola Bot�o de Entidade
Function uf_documentos_controlaTipoEntidade
	If !Used("ucrsConfigDoc")
		uf_perguntalt_chama("N�O FOI POSSIVEL VERIFICAR AS DEFINI��ES DO DOCUMENTO SELECIONADO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		Return .F.
	Endif

	If Alltrim(ucrsConfigDoc.bdempresas) = "FL"
		DOCUMENTOS.ContainerCab.Label1.Caption = "Fornecedor"
	Endif

	If Alltrim(ucrsConfigDoc.bdempresas) = "CL"
		DOCUMENTOS.ContainerCab.Label1.Caption = "Cliente"
	Endif

	If Alltrim(ucrsConfigDoc.bdempresas) = "AG"
		DOCUMENTOS.ContainerCab.Label1.Caption = "Entidade"
	Endif
	DOCUMENTOS.ContainerCab.Label1.Refresh
Endfunc


** Coloca N�mero de Documento
Function uf_documentos_controlaNumeroDoc
	If !Used("ucrsConfigDoc")
		uf_perguntalt_chama("N�O FOI POSSIVEL VERIFICAR AS DEFINI��ES DO DOCUMENTO SELECIONADO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		Return .F.
	Endif

	**Verifica Tipo Documento
	If Alltrim(ucrsConfigDoc.tipoDoc) = "BO"
		**lcSQL = ""
		**TEXT TO lcSQL TEXTMERGE NOSHOW
		**	SELECT
		**		ISNULL(MAX(OBRANO),0)+1 as numeracao
		**	FROM
		**		BO (nolock)
		**	WHERE
		**		--nmdos = '<<Alltrim(ucrsConfigDoc.NOMEDOC)>>'
		**		ndos = <<ASTR(ucrsConfigDoc.numInternoDoc)>>
		**		AND YEAR(dataobra) = YEAR('<<Alltrim(uf_gerais_getDate(cabDoc.datadoc,"SQL"))>>')
		**Endtext
		**If !uf_gerais_actGrelha("","ucrsNumeracaoDoc",lcSQL)
		**	uf_perguntalt_chama("N�O FOI POSS�VEL DETERMINAR O N�MERO A ATRIBUIR. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		**	Return
		**Endif
		**
		**Select CabDoc
		**Go top
		**replace cabDoc.numdoc With Alltrim(Str(ucrsNumeracaoDoc.numeracao))

		If !uf_documentos_novoDocNr()

			DOCUMENTOS.Refresh
			Return

		Endif

		**fecha("ucrsNumeracaoDoc")
		DOCUMENTOS.Refresh
	Endif

	If Alltrim(ucrsConfigDoc.tipoDoc) = "FO"

		**Controla se o Documento de Compras est� configurado para utilizar numera��o Sequencial
		If ucrsConfigDoc.nrseq  == .T.

			Store "" To lcSQL
			TEXT TO lcSQL TEXTMERGE NOSHOW
				SELECT
					ISNULL(MAX(ISNULL(convert(numeric,adoc),0)),0) + 1 as numeracao
				from
					fo (nolock)
				where
					ISNUMERIC(adoc) = 1
					and docnome = '<<Alltrim(ucrsConfigDoc.NOMEDOC)>>'
					AND YEAR(fo.docdata) = YEAR('<<Alltrim(uf_gerais_getDate(cabDoc.datadoc,"SQL"))>>')
			ENDTEXT
			If !uf_gerais_actGrelha("","ucrsNumeracaoDoc",lcSQL)
				uf_perguntalt_chama("N�O FOI POSS�VEL DETERMINAR O N�MERO A ATRIBUIR. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				Return
			Endif

			Select cabdoc
			Go Top
			Replace cabdoc.NUMDOC With Alltrim(Str(ucrsNumeracaoDoc.numeracao))
			fecha("ucrsNumeracaoDoc")
			DOCUMENTOS.Refresh
		Endif
	Endif
Endfunc


**Valores por Defeito cabe�alho
Function uf_documentos_valoresDefeitoCab

	Select ucrsConfigDoc
	Replace cabdoc.NUMINTERNODOC With ucrsConfigDoc.NUMINTERNODOC

	** Datas
	Local lcStamp
	lcStamp = uf_gerais_stamp()

	Select cabdoc
	Replace cabdoc.cabstamp 	With lcStamp
	Replace cabdoc.Doc			With Alltrim(ucrsConfigDoc.nomedoc)
	Replace cabdoc.DATAVENC		With Datetime()+(difhoraria*3600)
	Replace cabdoc.DATADOC		With Datetime()+(difhoraria*3600)
	Replace cabdoc.DATAINTERNA	With Datetime()+(difhoraria*3600)
	Replace cabdoc.DATAENTREGA	With Datetime()+(difhoraria*3600)
	Replace cabdoc.DATAFINAL	With Ctod(Dtoc(Datetime()+(difhoraria*3600)))
	Replace cabdoc.XPDDATA		With Datetime()+(difhoraria*3600)

	If Used("BI")

		Replace cabdoc.tipodos With uf_gerais_getUmvalor("ts", "(CASE WHEN tipodos = '' THEN 0 ELSE CONVERT(NUMERIC,tipodos) END)", "ndos = " + ASTR(cabdoc.NUMINTERNODOC))

	Endif

	** Status do Documento
	Replace cabdoc.ESTADODOC 	With "A"

	&& Controlo Moeda
	Local lcMoedaDefault
	Store 'EURO' To lcMoedaDefault

	lcMoedaDefault = uf_gerais_getParameter("ADM0000000260","text")

	&& Se parametro n�o estiver preenchido assume � por default
	If Empty(lcMoedaDefault)
		lcMoedaDefault = 'EURO'
	Endif

	Select cabdoc
	Replace cabdoc.MOEDA With lcMoedaDefault

	Replace cabdoc.ARMAZEM With myArmazem

	fecha("ucrsTS")
	fecha("ucrsCM1")

	If myTipoDoc == "BO"
		uf_gerais_actGrelha("","ucrsTS", "exec up_documentos_getTS " + ASTR(cabdoc.NUMINTERNODOC))
	Endif

	If myTipoDoc == "FO"
		uf_gerais_actGrelha("","ucrsCM1", "exec up_documentos_getCM " + ASTR(cabdoc.NUMINTERNODOC))
	Endif


	DOCUMENTOS.Refresh
Endfunc


** Coloca campos limpos Entidade/Fornecedor/Cliente devido � mudan�a de Documento
Function uf_documentos_limpaNome
	Select cabdoc
	Replace cabdoc.Nome			With ''
	Replace cabdoc.NOME2		With ''
	Replace cabdoc.NO 			With 0
	Replace cabdoc.ESTAB 		With 0
	Replace cabdoc.MORADA		With ''
	Replace cabdoc.Local 		With ''
	Replace cabdoc.CODPOST		With ''
	Replace cabdoc.NCONT 		With ''
	Replace cabdoc.TIPO			With ''
	Replace cabdoc.EMAIL		With ''
	Replace cabdoc.TELEFONE		With ''
	Replace cabdoc.CCUSTO		With ''
	Replace cabdoc.NRRECEITA	With ''
Endfunc



** Cria nova linha
Function uf_documentos_criaNovaLinha
	Lparameters tcBool, lcGravaTemp

	Local lcStamp


	** Verificar se existe nova vers�o para reiniciar o software
	uf_chk_nova_versao()

	** Previne erros informa��o cursor por gravar (For�a lostfocus)
	*!*		documentos.lockscreen = .t.
	*!*		documentos.ContainerCab.TextPesq.SetFocus
	*!*		DOCUMENTOS.PageFrame1.Page1.gridDoc.setfocus
	*!*		documentos.lockscreen = .f.
	*********************************************

	If !Used("cabDoc")
		Return .F.
	Endif
	If Empty(cabdoc.Doc)
		Return .F.
	Endif

	If myDocAlteracao == .F. And myDocIntroducao == .F.
		Return .F.
	Endif

	If DOCUMENTOS.eventoref == .F. && se estiver a correr os eventos
		uf_documentos_EventosGridDoc(.F.)
		&&RETURN .f.
	Endif
	********************************************************************
	** DOSSIERS INTERNOS
	********************************************************************
	If Alltrim(myTipoDoc) == "BO"
		Select BI2
		Append Blank
		Select Bi
		Delete Tag All
		Select Bi
		Append Blank

		** BI **
		** VALORES POR DEFEITO

		** BISTAMP
		lcStamp = uf_gerais_stamp()

		Select Bi
		Replace Bi.bistamp 		With lcStamp
		Replace Bi.BOSTAMP 		With cabdoc.cabstamp
		Replace Bi.NMDOS 		With Alltrim(ucrsConfigDoc.nomedoc)
		Replace Bi.ndos 		With ucrsConfigDoc.NUMINTERNODOC
		Replace Bi.OBRANO		With Val(cabdoc.NUMDOC)
		Replace Bi.DATAFINAL 	With cabdoc.DATAFINAL
		Replace Bi.PESQUISA 	With .F.
		If Type("upv_armazemDestino") <> "U" And cabdoc.tipodos = 3 &&UPPER(ALLTRIM(cabdoc.doc)) == "TRF ENTRE ARMAZ�NS"
			**REPLACE BI.AR2MAZEM WITH upv_armazemDestino
		Endif
		** IVA
		Local lcMoedaDefault
		Store 'EURO' To lcMoedaDefault

		lcMoedaDefault = uf_gerais_getParameter("ADM0000000260","text")

		&& Se parametro n�o estiver preenchido assume � por default
		If Empty(lcMoedaDefault)
			lcSQL = ""
			TEXT TO lcSQL TEXTMERGE NOSHOW
				SELECT CODIGO,TAXA FROM  taxasiva (nolock) WHERE codigo = 1
			ENDTEXT
			If uf_gerais_actGrelha("","ucrsIvaDefault",lcSQL)
				Replace Bi.IVA With ucrsIvaDefault.taxa
				If Bi.tabiva = 0
					Replace Bi.tabiva With 1
				Endif
			Endif
		Else
			Replace Bi.IVA With 0
			Replace Bi.tabiva With 4
		Endif

		** ARMAZEM
		If !Empty(DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.page11.ContainerOutrosDados.ARMAZEM.Value)
			Replace Bi.ARMAZEM With DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.page11.ContainerOutrosDados.ARMAZEM.Value
		Else
			If 	ucrsConfigDoc.ARMAZEM != 0
				Replace Bi.ARMAZEM With ucrsConfigDoc.ARMAZEM
			Else
				Replace Bi.ARMAZEM With myArmazem
			Endif
		Endif

		** LORDEM - Trata Ordem das Linhas
		Select Max(lordem) As lordem From Bi Into Cursor ucrsMaxLordem
		Select ucrsMaxLordem
		Replace Bi.lordem With Iif(Isnull(ucrsMaxLordem.lordem),0,ucrsMaxLordem.lordem) + 1

		** BI2 **
		** VALORES POR DEFEITO

		** BISTAMP
		Replace BI2.bi2stamp 	With Bi.bistamp
		Replace BI2.BOSTAMP		With Alltrim(cabdoc.cabstamp)


	Endif


	********************************************************************
	** DOCUMENTO DE COMPRAS
	********************************************************************
	If Alltrim(myTipoDoc) == "FO"

		** Limpa informa��o de Validade anterior
		DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.Page12.PageFrame1.Page1.Container1.txtValidade.Value = ""
		DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.Page12.PageFrame1.Page2.Container2.txtValidade.Value = ""
		**
		Select FN
		Delete Tag All
		Select FN
		Append Blank

		* VALORES POR DEFEITO
		** BISTAMP
		lcStamp = uf_gerais_stamp()

		Select FN
		Replace FN.fnstamp 	With lcStamp
		Replace FN.FOSTAMP 	With cabdoc.cabstamp
		Replace FN.docnome 	With Alltrim(ucrsConfigDoc.nomedoc)
		Replace FN.Adoc 	With cabdoc.NUMDOC
		Replace FN.PESQUISA With .F.


		** IVA
		Local lcMoedaDefault
		Store 'EURO' To lcMoedaDefault

		lcMoedaDefault = uf_gerais_getParameter("ADM0000000260","text")

		&& Se parametro n�o estiver preenchido assume � por default
		If Empty(lcMoedaDefault)
			lcSQL = ""
			TEXT TO lcSQL TEXTMERGE NOSHOW
				SELECT CODIGO, TAXA FROM taxasiva (nolock) WHERE codigo = 1
			ENDTEXT
			If uf_gerais_actGrelha("", "ucrsIvaDefault",lcSQL)
				Replace FN.IVA With ucrsIvaDefault.taxa
				If FN.tabiva == 0
					Replace FN.tabiva With 1
				Endif
			Endif
		Else
			Replace FN.IVA With 0
			Replace FN.tabiva With 4
		Endif

		** ARMAZEM
		If !Empty(DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.page11.ContainerOutrosDados.ARMAZEM.Value)
			Replace FN.ARMAZEM With DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.page11.ContainerOutrosDados.ARMAZEM.Value
		Else
			If ucrsConfigDoc.ARMAZEM != 0
				Replace FN.ARMAZEM With ucrsConfigDoc.ARMAZEM
			Else
				Replace FN.ARMAZEM With myArmazem
			Endif
		Endif

		** LORDEM - Trata Ordem das Linhas
		Select Max(lordem) As lordem From FN Into Cursor ucrsMaxLordem
		Select ucrsMaxLordem
		Replace FN.lordem With Iif(Isnull(ucrsMaxLordem.lordem),0,ucrsMaxLordem.lordem) + 1


	Endif

	If 	lcGravaTemp
		uf_documentos_gravarTempLinhas()
	Endif


	If tcBool == .F.
		** coloca o focus na coluna REF **
		With DOCUMENTOS.PageFrame1.Page1.gridDoc
			Go Bottom
			.SetFocus() && So activecolumn would work
			&&.Columns(.ActiveColumn).ReadOnly = .F.
		Endwith

		Try
			For i=1 To DOCUMENTOS.PageFrame1.Page1.gridDoc.ColumnCount
				If Upper(DOCUMENTOS.PageFrame1.Page1.gridDoc.Columns(i).ControlSource)=="FN.REF" Or;
						UPPER(DOCUMENTOS.PageFrame1.Page1.gridDoc.Columns(i).ControlSource)=="BI.REF"

					DOCUMENTOS.PageFrame1.Page1.gridDoc.Columns(i).SetFocus
					Exit
				Endif
			Endfor
		Catch
			**
		Endtry
	Endif


	DOCUMENTOS.PageFrame1.Page1.gridDoc.Refresh

	Return lcStamp
Endfunc


** Apaga Linha
Function uf_documentos_apagaLinhaDoc
	Local lcLordem, lcStampNovaPosicao

	If Empty(myTipoDoc) == .T. Or Empty(Alltrim(DOCUMENTOS.ContainerCab.documento.Value)) == .T. Or;
			(myDocAlteracao == .F. And myDocIntroducao==.F.)
		Return .F.
	Endif

	** N�o deixa eliminar linhas que j� possuam contador de psico ou benzo **
	If myDocAlteracao == .T.
		Do Case
			Case myTipoDoc == "BO"
				Select Bi
				If Bi.u_bencont	!= 0 Or Bi.u_psicont != 0
					uf_perguntalt_chama("N�O PODE REMOVER UM PRODUTO QUE J� POSSUA CONTADOR PSICOCOTR�PICOS ATRIBU�DO. PRODUTO: "+Alltrim(Bi.ref)+Chr(10)+".","OK","",48)
					Return .F.
				Endif

			Case myTipoDoc == "FO"
				Select FN
				If FN.u_bencont	!= 0 Or FN.u_psicont != 0
					uf_perguntalt_chama("N�O PODE REMOVER UM PRODUTO QUE J� POSSUA CONTADOR PSICOCOTR�PICOS ATRIBU�DO. PRODUTO: "+Alltrim(FN.ref)+Chr(10)+".","OK","",48)
					Return .F.
				Endif

			Otherwise
				**
		Endcase
	Endif

	If myTipoDoc == "BO"

		DOCUMENTOS.LockScreen = .T.

		Select Bi
		lcLordem = Bi.lordem
		lcBistamp= Bi.bistamp
		Select Top 1 bistamp,lordem From Bi Where Bi.lordem > lcLordem Order By Bi.lordem Asc Into Cursor ucrsNovoStamp Readwrite

		If Used("fi_trans_info")
			Select fi_trans_info
			Go Top
			Locate For uf_gerais_compstr(fi_trans_info.recStamp, lcBistamp)
			If Found()
				Select fi_trans_info
				Delete
			Endif
		Endif

		**
		Select BI2
		Delete From BI2 Where Alltrim(bi2stamp) = Alltrim(lcBistamp)

		Select Bi
		Delete From Bi Where Alltrim(bistamp) = Alltrim(lcBistamp)


		lcSQL = ''
		TEXT TO lcSql TEXTMERGE NOSHOW
			DELETE from temp_bi WHERE terminal = <<mytermno>> and bistamp = '<<lcBistamp>>'

			DELETE from temp_bi2 WHERE terminal = <<mytermno>> and bi2stamp = '<<lcBistamp>>'
		ENDTEXT
		If !uf_gerais_actGrelha("", "ucrsTemp", lcSQL)
			uf_perguntalt_chama("N�O FOI POSS�VEL VERIFICAR A LISTAGEM DE CENTROS DE CUSTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			Return .F.
		Endif


		uf_documentos_actualizaTotaisCAB()

		If Reccount("ucrsNovoStamp")>0
			Select ucrsNovoStamp
			lcStampNovaPosicao = ucrsNovoStamp.bistamp

			Select Bi
			Locate For Bi.bistamp = lcStampNovaPosicao
			If Found()
				**
			Else
				Select Bi
				Go Top
			Endif
		Else
			Select Bi
			Go Bottom
		Endif

		DOCUMENTOS.PageFrame1.Page1.gridDoc.Refresh

		DOCUMENTOS.PageFrame1.Page1.ContainerLinhas.Total.Refresh
		DOCUMENTOS.PageFrame1.Page1.gridDoc.SetFocus

		If(Used("fi_trans_info"))

			Select fi_trans_info
			Go Top

			Scan

				Select * From Bi Into Cursor uc_tmpBIDel

				Select uc_tmpBIDel
				Go Top

				Locate For uf_gerais_compstr(uc_tmpBIDel.bistamp, fi_trans_info.recStamp)

				If !Found()

					Select fi_trans_info
					Delete

				Endif

				Select fi_trans_info
			Endscan

		Endif


		fecha("uc_tmpBIDel")

		DOCUMENTOS.LockScreen = .F.

	Endif

	If myTipoDoc = "FO"

		DOCUMENTOS.LockScreen = .T.

		Select FN
		lcLordem = FN.lordem
		lcFnstamp = FN.fnstamp
		Select Top 1 fnstamp,lordem From FN Where FN.lordem > lcLordem Order By FN.lordem Asc Into Cursor ucrsNovoStamp Readwrite

		If Used("fi_trans_info")
			Select fi_trans_info
			Go Top
			Locate For uf_gerais_compstr(fi_trans_info.recStamp, lcFnstamp)
			If Found()
				Select fi_trans_info
				Delete
			Endif
		Endif

		**
		Select FN
		Delete From FN Where Alltrim(fnstamp) = Alltrim(lcFnstamp)
		lcSQL = ''
		TEXT TO lcSql TEXTMERGE NOSHOW
			DELETE from temp_fn WHERE terminal = <<mytermno>> and fnstamp = '<<lcFnstamp>>'
		ENDTEXT
		If !uf_gerais_actGrelha("", "ucrsTemp", lcSQL)
			uf_perguntalt_chama("N�O FOI POSS�VEL VERIFICAR A LISTAGEM DE CENTROS DE CUSTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			Return .F.
		Endif


		uf_documentos_actualizaTotaisCAB()

		If Used("ucrsNovoStamp") And Reccount("ucrsNovoStamp")>0
			Select ucrsNovoStamp
			lcStampNovaPosicao = ucrsNovoStamp.fnstamp

			Select FN
			Locate For FN.fnstamp = lcStampNovaPosicao
			If Found()
				**
			Else
				Select FN
				Go Top
			Endif
		Else
			Select FN
			Go Bottom
		Endif

		DOCUMENTOS.PageFrame1.Page1.gridDoc.Refresh

		DOCUMENTOS.PageFrame1.Page1.ContainerLinhas.Total.Refresh
		DOCUMENTOS.PageFrame1.Page1.gridDoc.SetFocus

		If Used("fi_trans_info")

			Select fi_trans_info
			Go Top

			Scan

				Select * From FN Into Cursor uc_tmpFnDel

				Select uc_tmpFnDel
				Go Top

				Locate For uf_gerais_compstr(uc_tmpFnDel.fnstamp, fi_trans_info.recStamp)

				If !Found()

					Select fi_trans_info
					Delete

				Endif

				Select fi_trans_info
			Endscan

		Endif


		fecha("uc_tmpFnDel")

		DOCUMENTOS.LockScreen = .F.

	Endif


	&&Limpa Lote Conf
	With DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.Page12.PageFrame1
		.Page1.Container1.txtLoteConf.Value = ""
		.Page2.Container2.txtLoteConf.Value = ""
	Endwith
Endfunc


***	Actualiza Totais do Cabe�alho
Function uf_documentos_actualizaTotaisCAB
	Lparameters tcBool, lcCursorCab, lcCursorLin, lcCursorCab2 &&Controlo do calculo do desconto financeiro

	If Empty(lcCursorCab)
		lcCursorCab = "cabDoc"
	Endif

	If !Used(lcCursorCab)
		Return .F.
	Endif

	Local totalComIva, totalSemIva, lcValorIva1, lcValorIva2, lcValorIva3, lcValorIva4;
		,lcValorIva5, lcValorIva6, lcValorIva7, lcValorIva8, lcValorIva9, lcTotalIva, TOTALNSERVICO;
		,lcValorSemIva, lcDesc, lcTotDesc
	Store 0 To totalComIva, totalSemIva, lcValorIva1, lcValorIva2, lcValorIva3, lcValorIva4;
		,lcValorIva5, lcValorIva6, lcValorIva7, lcValorIva8, lcValorIva9, lcTotalIva, TOTALNSERVICO;
		,lcValorSemIva, lcDesc, lcTotDesc

	If Alltrim(myTipoDoc)=="BO"

		If Empty(lcCursorLin)
			lcCursorLin = "BI"
		Endif

		Local lcBiPos
		Store 0 To lcBiPos

		** TOTAIS
		Select &lcCursorLin
		lcBiPos = Recno()
		Go Top
		Scan


			If &lcCursorLin..ivaincl = .T.
				totalComIva = totalComIva + &lcCursorLin..ettdeb
				totalSemIva = totalSemIva + (&lcCursorLin..ettdeb/(&lcCursorLin..IVA/100+1))
			Else
				totalComIva=totalComIva + (&lcCursorLin..ettdeb*(&lcCursorLin..IVA/100+1))
				totalSemIva=totalSemIva + (&lcCursorLin..ettdeb)
			Endif

			** IVA
			Do Case
				Case &lcCursorLin..tabiva=1
					If &lcCursorLin..ivaincl
						lcValorIva1=lcValorIva1 + &lcCursorLin..ettdeb - (&lcCursorLin..ettdeb/(&lcCursorLin..IVA/100+1))
					Else
						lcValorIva1=lcValorIva1+(&lcCursorLin..ettdeb*(&lcCursorLin..IVA/100))
					Endif
				Case &lcCursorLin..tabiva=2
					If &lcCursorLin..ivaincl
						lcValorIva2=lcValorIva2 + &lcCursorLin..ettdeb - (&lcCursorLin..ettdeb/(&lcCursorLin..IVA/100+1))
					Else
						lcValorIva2=lcValorIva2+(&lcCursorLin..ettdeb*(&lcCursorLin..IVA/100))
					Endif
				Case &lcCursorLin..tabiva=3
					If &lcCursorLin..ivaincl
						lcValorIva3=lcValorIva3 + &lcCursorLin..ettdeb - (&lcCursorLin..ettdeb/(&lcCursorLin..IVA/100+1))
					Else
						lcValorIva3=lcValorIva3+(&lcCursorLin..ettdeb*(&lcCursorLin..IVA/100))
					Endif
				Case &lcCursorLin..tabiva=4
					If &lcCursorLin..ivaincl
						lcValorIva4=lcValorIva4 + &lcCursorLin..ettdeb - (&lcCursorLin..ettdeb/(&lcCursorLin..IVA/100+1))
					Else
						lcValorIva4=lcValorIva4+(&lcCursorLin..ettdeb*(&lcCursorLin..IVA/100))
					Endif
				Case &lcCursorLin..tabiva=5
					If &lcCursorLin..ivaincl
						lcValorIva5=lcValorIva5 + &lcCursorLin..ettdeb - (&lcCursorLin..ettdeb/(&lcCursorLin..IVA/100+1))
					Else
						lcValorIva5=lcValorIva5+(&lcCursorLin..ettdeb*(&lcCursorLin..IVA/100))
					Endif
				Case &lcCursorLin..tabiva=6
					If &lcCursorLin..ivaincl
						lcValorIva6=lcValorIva6 + &lcCursorLin..ettdeb - (&lcCursorLin..ettdeb/(&lcCursorLin..IVA/100+1))
					Else
						lcValorIva6=lcValorIva6+(&lcCursorLin..ettdeb*(&lcCursorLin..IVA/100))
					Endif
				Case &lcCursorLin..tabiva=7
					If &lcCursorLin..ivaincl
						lcValorIva7=lcValorIva7 + &lcCursorLin..ettdeb - (&lcCursorLin..ettdeb/(&lcCursorLin..IVA/100+1))
					Else
						lcValorIva7=lcValorIva7+(&lcCursorLin..ettdeb*(&lcCursorLin..IVA/100))
					Endif
				Case &lcCursorLin..tabiva=8
					If &lcCursorLin..ivaincl
						lcValorIva8=lcValorIva8 + &lcCursorLin..ettdeb - (&lcCursorLin..ettdeb/(&lcCursorLin..IVA/100+1))
					Else
						lcValorIva8=lcValorIva8+(&lcCursorLin..ettdeb*(&lcCursorLin..IVA/100))
					Endif
				Case &lcCursorLin..tabiva=9
					If &lcCursorLin..ivaincl
						lcValorIva9=lcValorIva9 + &lcCursorLin..ettdeb - (&lcCursorLin..ettdeb/(&lcCursorLin..IVA/100+1))
					Else
						lcValorIva9=lcValorIva9+(&lcCursorLin..ettdeb*(&lcCursorLin..IVA/100))
					Endif
			Endcase


			** Desconto Comercial
			If &lcCursorLin..desconto > 0  Or &lcCursorLin..desc2>0

				If &lcCursorLin..ivaincl
					lcValorSemIva = (&lcCursorLin..edebito*&lcCursorLin..qtt) / (&lcCursorLin..IVA / 100 + 1)
				Else
					lcValorSemIva = &lcCursorLin..edebito*&lcCursorLin..qtt
				Endif

				If &lcCursorLin..desconto>0
					lcDesc = lcValorSemIva - (lcValorSemIva * ((100-&lcCursorLin..desconto)/100))
					lcTotDesc = lcTotDesc + lcDesc
				Endif

				If &lcCursorLin..desc2>0
					lcDesc = lcValorSemIva - (lcValorSemIva * ((100-&lcCursorLin..desc2)/100))
					lcTotDesc = lcTotDesc + lcDesc
				Endif
			Endif


			** N�O SERVI�O
			If &lcCursorLin..stns = .F.
				If &lcCursorLin..ivaincl
					TOTALNSERVICO = TOTALNSERVICO + (&lcCursorLin..ettdeb/(&lcCursorLin..IVA/100+1))
				Else
					TOTALNSERVICO = TOTALNSERVICO + &lcCursorLin..ettdeb
				Endif
			Endif
		Endscan

		If Alltrim(Upper(lcCursorCab)) == "CABDOC"

			** Verifica desconto Financeiro **
			If tcBool == .F.
				Select &lcCursorCab
				If &lcCursorCab..DESCFIN == 0
					Replace &lcCursorCab..VALORDESCFIN With 0
				Endif

				If &lcCursorCab..DESCFIN > 0
					Replace &lcCursorCab..VALORDESCFIN With ((totalSemIva * &lcCursorCab..DESCFIN) / 100)
				Endif
			Else
				Select &lcCursorCab
				**Altera��o do Desconto em Valor
				If &lcCursorCab..VALORDESCFIN == 0
					Replace &lcCursorCab..DESCFIN With 0
				Endif
				If &lcCursorCab..VALORDESCFIN > 0 And &lcCursorCab..VALORDESCFIN < totalSemIva
					Replace &lcCursorCab..DESCFIN With &lcCursorCab..VALORDESCFIN * 100 / totalSemIva &&Round(&lcCursorCab..VALORDESCFIN * 100 / totalSemIva, 2)
				Else
					Replace &lcCursorCab..VALORDESCFIN With 0
					Replace &lcCursorCab..DESCFIN With 0
				Endif
			Endif

			**************************

			** Retira Desconto Financeiro
			If !(&lcCursorCab..DESCFIN == 0)
				If lcValorIva1 > 0
					lcValorIva1 = (lcValorIva1 - (lcValorIva1 * (&lcCursorCab..DESCFIN/100)))
				Endif
				If lcValorIva2 > 0
					lcValorIva2 = (lcValorIva2 - (lcValorIva2 * (&lcCursorCab..DESCFIN/100)))
				Endif
				If lcValorIva3 > 0
					lcValorIva3 = (lcValorIva3 - (lcValorIva3 * (&lcCursorCab..DESCFIN/100)))
				Endif
				If lcValorIva4 > 0
					lcValorIva4 = (lcValorIva4 - (lcValorIva4 * (&lcCursorCab..DESCFIN/100)))
				Endif
				If lcValorIva5 > 0
					lcValorIva5 = (lcValorIva5 - (lcValorIva5 * (&lcCursorCab..DESCFIN/100)))
				Endif
				If lcValorIva6 > 0
					lcValorIva6 = (lcValorIva6 - (lcValorIva6 * (&lcCursorCab..DESCFIN/100)))
				Endif
				If lcValorIva7 > 0
					lcValorIva7 = (lcValorIva7 - (lcValorIva7 * (&lcCursorCab..DESCFIN/100)))
				Endif
				If lcValorIva8 > 0
					lcValorIva8 = (lcValorIva8 - (lcValorIva8 * (&lcCursorCab..DESCFIN/100)))
				Endif
				If lcValorIva9 > 0
					lcValorIva9 = (lcValorIva9 - (lcValorIva9 * (&lcCursorCab..DESCFIN/100)))
				Endif
				**

				totalSemIva	= totalSemIva - (totalSemIva * (&lcCursorCab..DESCFIN/100))
				totalComIva	= totalComIva - (totalComIva * (&lcCursorCab..DESCFIN/100))
			Endif


			lcTotalIva = Round(lcValorIva1,2) + Round(lcValorIva2,2) + Round(lcValorIva3,2) + Round(lcValorIva4,2);
				+ Round(lcValorIva5,2) + Round(lcValorIva6,2) + Round(lcValorIva7,2) + Round(lcValorIva8,2) + Round(lcValorIva9,2)


			Select &lcCursorCab
			Replace &lcCursorCab..BASEINC	With Round(totalSemIva,2)
			Replace &lcCursorCab..IVA		With Round(lcTotalIva,2)
			Replace &lcCursorCab..Total		With Round(&lcCursorCab..BASEINC + &lcCursorCab..IVA,2)

			Replace &lcCursorCab..VALORIVA1 With lcValorIva1
			Replace &lcCursorCab..VALORIVA2 With lcValorIva2
			Replace &lcCursorCab..VALORIVA3 With lcValorIva3
			Replace &lcCursorCab..VALORIVA4 With lcValorIva4
			Replace &lcCursorCab..VALORIVA5 With lcValorIva5
			Replace &lcCursorCab..VALORIVA6 With lcValorIva6
			Replace &lcCursorCab..VALORIVA7 With lcValorIva7
			Replace &lcCursorCab..VALORIVA8 With lcValorIva8
			Replace &lcCursorCab..VALORIVA9 With lcValorIva9
			Replace &lcCursorCab..TOTALNSERVICO	With TOTALNSERVICO

			DOCUMENTOS.Refresh

			Select &lcCursorLin
			Count To lcNrRows
			If lcBiPos>0 And lcBiPos<=lcNrRows
				Try
					Go lcBiPos
				Catch
				Endtry
			Endif

		Else && Chamado para calculo de totais fora do apainel de documentos


			lcTotalIva = Round(lcValorIva1,2) + Round(lcValorIva2,2) + Round(lcValorIva3,2) + Round(lcValorIva4,2);
				+ Round(lcValorIva5,2) + Round(lcValorIva6,2) + Round(lcValorIva7,2) + Round(lcValorIva8,2) + Round(lcValorIva9,2)
			&&lcTotalIva = lcValorIva1 + lcValorIva2 + lcValorIva3 + lcValorIva4 + lcValorIva5 + lcValorIva6 + lcValorIva7 + lcValorIva8 + lcValorIva9

			Select &lcCursorCab
			Select &lcCursorCab2

			Replace &lcCursorCab..etotaldeb			With Round(totalSemIva,2)
			Replace &lcCursorCab2..ETOTIVA			With Round(lcTotalIva,2)
			Replace &lcCursorCab..etotal			With Round(totalSemIva,2)  + Round(lcTotalIva,2)

			Replace &lcCursorCab..EBO11_IVA			With	lcValorIva1
			Replace &lcCursorCab..ebo12_iva			With	lcValorIva1

			Replace &lcCursorCab..EBO21_IVA	With	lcValorIva2
			Replace &lcCursorCab..ebo22_iva	With	lcValorIva2


			Replace &lcCursorCab..EBO31_IVA With	lcValorIva3
			Replace &lcCursorCab..ebo32_iva With	lcValorIva3

			Replace &lcCursorCab..EBO41_IVA With	lcValorIva4
			Replace &lcCursorCab..ebo42_iva With	lcValorIva4

			Replace &lcCursorCab..EBO51_IVA With	lcValorIva5
			Replace &lcCursorCab..ebo52_iva With	lcValorIva5

			Replace &lcCursorCab..EBO61_IVA With	lcValorIva6
			Replace &lcCursorCab..ebo62_iva With	lcValorIva6


			Replace &lcCursorCab2..EBO71_IVA With	lcValorIva7
			Replace &lcCursorCab2..ebo72_iva With	lcValorIva7

			Replace &lcCursorCab2..EBO81_IVA With	lcValorIva8
			Replace &lcCursorCab2..ebo82_iva With	lcValorIva8

			Replace &lcCursorCab2..EBO91_IVA With	lcValorIva9
			Replace &lcCursorCab2..ebo92_iva With	lcValorIva9

		Endif
	Endif

	If Alltrim(myTipoDoc)=="FO"

		If Empty(lcCursorLin)
			lcCursorLin = "FN"
		Endif

		Local lcFnPos
		Store 0 To lcFnPos
		** TOTAIS
		Select 	&lcCursorLin
		lcFnPos = Recno()
		Go Top
		Scan

			If Upper(Alltrim(cabdoc.Doc)) == "V/FACTURA RESUMO"

				If &lcCursorLin..ivaincl = .T.
					totalComIva = totalComIva + (&lcCursorLin..etiliquido * Iif(At('V/Nt. Cr�dito', &lcCursorLin..Design) = 0, 1, -1))
					totalSemIva = totalSemIva + ((&lcCursorLin..etiliquido/(&lcCursorLin..IVA/100+1)) * Iif(At('V/Nt. Cr�dito', &lcCursorLin..Design) = 0, 1, -1))
				Else
					totalComIva=totalComIva + ((&lcCursorLin..etiliquido*(&lcCursorLin..IVA/100+1)) * Iif(At('V/Nt. Cr�dito', &lcCursorLin..Design) = 0, 1, -1))
					totalSemIva=totalSemIva + ((&lcCursorLin..etiliquido) * Iif(At('V/Nt. Cr�dito', &lcCursorLin..Design) = 0, 1, -1))
				Endif

			Else

				If &lcCursorLin..ivaincl = .T.
					totalComIva = totalComIva + &lcCursorLin..etiliquido
					totalSemIva = totalSemIva + (&lcCursorLin..etiliquido/(&lcCursorLin..IVA/100+1))
				Else
					totalComIva=totalComIva + (&lcCursorLin..etiliquido*(&lcCursorLin..IVA/100+1))
					totalSemIva=totalSemIva + (&lcCursorLin..etiliquido)
				Endif

			Endif

			**IVA
			Do Case
				Case &lcCursorLin..tabiva=1
					If &lcCursorLin..ivaincl
						lcValorIva1=lcValorIva1 + &lcCursorLin..etiliquido - (&lcCursorLin..etiliquido/(&lcCursorLin..IVA/100+1))
					Else
						lcValorIva1=lcValorIva1+(&lcCursorLin..etiliquido*(&lcCursorLin..IVA/100))
					Endif
				Case &lcCursorLin..tabiva=2
					If &lcCursorLin..ivaincl
						lcValorIva2=lcValorIva2 + &lcCursorLin..etiliquido - (&lcCursorLin..etiliquido/(&lcCursorLin..IVA/100+1))
					Else
						lcValorIva2=lcValorIva2+(&lcCursorLin..etiliquido*(&lcCursorLin..IVA/100))
					Endif
				Case &lcCursorLin..tabiva=3
					If &lcCursorLin..ivaincl
						lcValorIva3=lcValorIva3 + &lcCursorLin..etiliquido - (&lcCursorLin..etiliquido/(&lcCursorLin..IVA/100+1))
					Else
						lcValorIva3=lcValorIva3+(&lcCursorLin..etiliquido*(&lcCursorLin..IVA/100))
					Endif
				Case &lcCursorLin..tabiva=4
					If &lcCursorLin..ivaincl
						lcValorIva4=lcValorIva4 + &lcCursorLin..etiliquido - (&lcCursorLin..etiliquido/(&lcCursorLin..IVA/100+1))
					Else
						lcValorIva4=lcValorIva4+(&lcCursorLin..etiliquido*(&lcCursorLin..IVA/100))
					Endif
				Case &lcCursorLin..tabiva=5
					If &lcCursorLin..ivaincl
						lcValorIva5=lcValorIva5 + &lcCursorLin..etiliquido - (&lcCursorLin..etiliquido/(&lcCursorLin..IVA/100+1))
					Else
						lcValorIva5=lcValorIva5+(&lcCursorLin..etiliquido*(&lcCursorLin..IVA/100))
					Endif
				Case &lcCursorLin..tabiva=6
					If &lcCursorLin..ivaincl
						lcValorIva6=lcValorIva6 + &lcCursorLin..etiliquido - (&lcCursorLin..etiliquido/(&lcCursorLin..IVA/100+1))
					Else
						lcValorIva6=lcValorIva6+(&lcCursorLin..etiliquido*(&lcCursorLin..IVA/100))
					Endif
				Case &lcCursorLin..tabiva=7
					If &lcCursorLin..ivaincl
						lcValorIva7=lcValorIva7 + &lcCursorLin..etiliquido - (&lcCursorLin..etiliquido/(&lcCursorLin..IVA/100+1))
					Else
						lcValorIva7=lcValorIva7+(&lcCursorLin..etiliquido*(&lcCursorLin..IVA/100))
					Endif
				Case &lcCursorLin..tabiva=8
					If &lcCursorLin..ivaincl
						lcValorIva8=lcValorIva8 + &lcCursorLin..etiliquido - (&lcCursorLin..etiliquido/(&lcCursorLin..IVA/100+1))
					Else
						lcValorIva8=lcValorIva8+(&lcCursorLin..etiliquido*(&lcCursorLin..IVA/100))
					Endif
				Case &lcCursorLin..tabiva=9
					If &lcCursorLin..ivaincl
						lcValorIva9=lcValorIva9 + &lcCursorLin..etiliquido - (&lcCursorLin..etiliquido/(&lcCursorLin..IVA/100+1))
					Else
						lcValorIva9=lcValorIva9+(&lcCursorLin..etiliquido*(&lcCursorLin..IVA/100))
					Endif
			Endcase

			** Desconto Comercial
			If &lcCursorLin..desconto > 0  Or &lcCursorLin..desc2>0
				If &lcCursorLin..ivaincl
					lcValorSemIva = (&lcCursorLin..epv*&lcCursorLin..qtt) / (&lcCursorLin..IVA / 100 + 1)
				Else
					lcValorSemIva = &lcCursorLin..epv*&lcCursorLin..qtt
				Endif

				If &lcCursorLin..desconto>0
					lcDesc = lcValorSemIva - (lcValorSemIva * ((100-&lcCursorLin..desconto)/100))
					lcTotDesc = lcTotDesc + lcDesc
				Endif

				If &lcCursorLin..desc2>0
					lcDesc = lcValorSemIva - (lcValorSemIva * ((100-&lcCursorLin..desc2)/100))
					lcTotDesc = lcTotDesc + lcDesc
				Endif
			Endif


			**N�O SERVI�O
			If &lcCursorLin..stns = .F.
				If &lcCursorLin..ivaincl
					TOTALNSERVICO=TOTALNSERVICO + (&lcCursorLin..etiliquido/(&lcCursorLin..IVA/100+1) )
				Else
					TOTALNSERVICO=TOTALNSERVICO + &lcCursorLin..etiliquido
				Endif
			Endif
		Endscan


		If Alltrim(Upper(lcCursorCab)) == "CABDOC"

			** Coloca Desconto Comercial Total
			Select &lcCursorCab
			Replace &lcCursorCab..EDESCC With lcTotDesc

			** Verifica desconto Financeiro
			If tcBool == .F.
				Select &lcCursorCab
				If &lcCursorCab..DESCFIN == 0
					Replace &lcCursorCab..VALORDESCFIN With 0
				Endif

				If &lcCursorCab..DESCFIN > 0
					Replace &lcCursorCab..VALORDESCFIN With Round(((totalSemIva * &lcCursorCab..DESCFIN) / 100), 2)
				Endif
			Else

				Select &lcCursorCab
				**Altera��o do Desconto em Valor
				If &lcCursorCab..VALORDESCFIN == 0
					Replace &lcCursorCab..DESCFIN With 0
				Endif
				If &lcCursorCab..VALORDESCFIN > 0 And &lcCursorCab..VALORDESCFIN < totalSemIva
					Replace &lcCursorCab..DESCFIN With  Round((Round(&lcCursorCab..VALORDESCFIN,2) * 100 / Round(totalSemIva,2)), 2) &&Round(&lcCursorCab..VALORDESCFIN * 100 / totalSemIva, 2)
				Else
					Replace &lcCursorCab..VALORDESCFIN With 0
					Replace &lcCursorCab..DESCFIN With 0
				Endif
			Endif
			*************************

			** Retira Desconto Financeiro
			If !(&lcCursorCab..DESCFIN == 0)

				** ALtera��o Realizada a 12-04-2011
				If lcValorIva1 > 0
					lcValorIva1 = (lcValorIva1 - (lcValorIva1 * (&lcCursorCab..DESCFIN/100)))
				Endif
				If lcValorIva2 > 0
					lcValorIva2 = (lcValorIva2 - (lcValorIva2 * (&lcCursorCab..DESCFIN/100)))
				Endif
				If lcValorIva3 > 0
					lcValorIva3 = (lcValorIva3 - (lcValorIva3 * (&lcCursorCab..DESCFIN/100)))
				Endif
				If lcValorIva4 > 0
					lcValorIva4 = (lcValorIva4 - (lcValorIva4 * (&lcCursorCab..DESCFIN/100)))
				Endif
				If lcValorIva5 > 0
					lcValorIva5 = (lcValorIva5 - (lcValorIva5 * (&lcCursorCab..DESCFIN/100)))
				Endif
				If lcValorIva6 > 0
					lcValorIva6 = (lcValorIva6 - (lcValorIva6 * (&lcCursorCab..DESCFIN/100)))
				Endif
				If lcValorIva7 > 0
					lcValorIva7 = (lcValorIva7 - (lcValorIva7 * (&lcCursorCab..DESCFIN/100)))
				Endif
				If lcValorIva8 > 0
					lcValorIva8 = (lcValorIva8 - (lcValorIva8 * (&lcCursorCab..DESCFIN/100)))
				Endif
				If lcValorIva9 > 0
					lcValorIva9 = (lcValorIva9 - (lcValorIva9 * (&lcCursorCab..DESCFIN/100)))
				Endif
				**

				totalSemIva	= totalSemIva - (totalSemIva * (&lcCursorCab..DESCFIN/100))
				totalComIva	= totalComIva - (totalComIva * (&lcCursorCab..DESCFIN/100))

			Endif
		Endif

		lcTotalIva = Round(lcValorIva1,2) + Round(lcValorIva2,2) + Round(lcValorIva3,2) + Round(lcValorIva4,2);
			+ Round(lcValorIva5,2) + Round(lcValorIva6,2) + Round(lcValorIva7,2) + Round(lcValorIva8,2) + Round(lcValorIva9,2)

		If Alltrim(Upper(lcCursorCab)) == "CABDOC"

			** Desconto financeiro
			Select &lcCursorCab
			Replace &lcCursorCab..BASEINC	With	Round(totalSemIva,2)
			Replace &lcCursorCab..IVA		With	Round(lcTotalIva,2)
			Replace &lcCursorCab..Total		With	Round(totalComIva,2)

			Replace &lcCursorCab..VALORIVA1 With	lcValorIva1
			Replace &lcCursorCab..VALORIVA2 With	lcValorIva2
			Replace &lcCursorCab..VALORIVA3 With	lcValorIva3
			Replace &lcCursorCab..VALORIVA4 With	lcValorIva4
			Replace &lcCursorCab..VALORIVA5 With	lcValorIva5
			Replace &lcCursorCab..VALORIVA6 With	lcValorIva6
			Replace &lcCursorCab..VALORIVA7 With	lcValorIva7
			Replace &lcCursorCab..VALORIVA8 With	lcValorIva8
			Replace &lcCursorCab..VALORIVA9 With	lcValorIva9
			Replace &lcCursorCab..TOTALNSERVICO	With	TOTALNSERVICO

			DOCUMENTOS.Refresh

			Select &lcCursorLin
			Count To lcNrRows
			If lcFnPos>0 And lcFnPos<=lcNrRows
				Try
					Go lcFnPos
				Catch
				Endtry
			Endif

		Endif
	Endif

	If Alltrim(Upper(lcCursorCab)) == "CABDOC"
		uf_documentos_calculaTotaisRodape()
		uf_documentos_CalculaTotais(cabdoc.VALORDESCFIN)
	Endif

	&&
	DOCUMENTOS.PageFrame1.Page1.gridDoc.Refresh

Endfunc



**
Function uf_documentos_ControlaEventosLinhas
	* EVENTOS GRID
	With DOCUMENTOS.PageFrame1.Page1.gridDoc
		For i=1 To .ColumnCount

			If uf_gerais_compstr(.Columns(i).ControlSource, "BI.LOBS")
				Bindevent(.Columns(i).text1,"LostFocus",DOCUMENTOS.oCust,"proc_lostFocusObs")
			Endif


			If Upper(.Columns(i).ControlSource)=="BI.REF" Or Upper(.Columns(i).ControlSource)=="FN.REF"
				Bindevent(.Columns(i).text1,"InteractiveChange",DOCUMENTOS.oCust,"proc_autocompleteRef")
				Bindevent(.Columns(i).text1,"dblClick",DOCUMENTOS.oCust,"proc_navegast")
				Bindevent(.Columns(i).text1,"KeyPress",DOCUMENTOS.oCust,"proc_ApagarAutocompleteRef")
				Bindevent(.Columns(i).text1,"InteractiveChange",DOCUMENTOS.oCust,"proc_ActivaEventosDoc")
				Bindevent(.Columns(i).header1,"Click",DOCUMENTOS.oCust,"proc_OrdenaPorRefDoc")
			Endif

			*AUTOCOMPLETE DESIGN
			If Upper(.Columns(i).ControlSource)=="BI.DESIGN" Or;
					Upper(.Columns(i).ControlSource)=="FN.DESIGN"

				Bindevent(.Columns(i).text1,"InteractiveChange",DOCUMENTOS.oCust,"proc_ActivaEventosDoc")
				Bindevent(.Columns(i).header1,"Click",DOCUMENTOS.oCust,"proc_OrdenaPorDesignDoc")
			Endif

			If uf_gerais_getParameter_Site('ADM0000000060', 'BOOL', mySite)
				If Upper(.Columns(i).ControlSource)=="BI.QTT" Or;
						Upper(.Columns(i).ControlSource)=="FN.QTT"
					Bindevent(.Columns(i).text1,"InteractiveChange",DOCUMENTOS.oCust,"proc_ActivaEventosDoc")
					Bindevent(.Columns(i).text1,"Click",DOCUMENTOS.oCust,"proc_AlteraQTT")
				Endif
			Else
				If Upper(.Columns(i).ControlSource)=="BI.QTT" Or;
						Upper(.Columns(i).ControlSource)=="FN.QTT"
					Bindevent(.Columns(i).text1,"InteractiveChange",DOCUMENTOS.oCust,"proc_ActivaEventosDoc")
				Endif
			Endif

			If Upper(.Columns(i).ControlSource)=="FN.QTREC"
				Bindevent(.Columns(i).text1,"InteractiveChange",DOCUMENTOS.oCust,"proc_ActivaEventosDoc")
			Endif

			If Upper(.Columns(i).ControlSource)=="BI.UNI2QTT" Or;
					Upper(.Columns(i).ControlSource)=="FN.UNI2QTT"

				Bindevent(.Columns(i).text1,"InteractiveChange",DOCUMENTOS.oCust,"proc_ActivaEventosDoc")
			Endif

			If Upper(.Columns(i).ControlSource)=="BI.DESCONTO" Or;
					Upper(.Columns(i).ControlSource)=="FN.DESCONTO"
				Bindevent(.Columns(i).text1,"InteractiveChange",DOCUMENTOS.oCust,"proc_ActivaEventosDoc")
			Endif

			If Upper(.Columns(i).ControlSource)=="BI.DESC2" Or;
					Upper(.Columns(i).ControlSource)=="FN.DESC2"

				Bindevent(.Columns(i).text1,"InteractiveChange",DOCUMENTOS.oCust,"proc_ActivaEventosDoc")
			Endif

			If Upper(.Columns(i).ControlSource)=="BI.DESC3" Or;
					Upper(.Columns(i).ControlSource)=="FN.DESC3"

				Bindevent(.Columns(i).text1,"InteractiveChange",DOCUMENTOS.oCust,"proc_ActivaEventosDoc")
			Endif

			If Upper(.Columns(i).ControlSource)=="BI.DESC4" Or;
					Upper(.Columns(i).ControlSource)=="FN.DESC4"

				Bindevent(.Columns(i).text1,"InteractiveChange",DOCUMENTOS.oCust,"proc_ActivaEventosDoc")
			Endif

			*EVENTO EDEBITO - PRECO
			If Upper(.Columns(i).ControlSource)=="BI.EDEBITO" Or;
					Upper(.Columns(i).ControlSource)=="FN.EPV" Or;
					Upper(.Columns(i).ControlSource)=="FN.DESPESA"

				Bindevent(.Columns(i).text1,"InteractiveChange",DOCUMENTOS.oCust,"proc_ActivaEventosDoc")
			Endif

			*EVENTO ETTDEB - TOTAL
			If Upper(.Columns(i).ControlSource)=="BI.ETTDEB" Or;
					Upper(.Columns(i).ControlSource)=="FN.ETILIQUIDO"

				Bindevent(.Columns(i).text1,"InteractiveChange",DOCUMENTOS.oCust,"proc_ActivaEventosDoc")
			Endif

			*EVENTO BONUS
			If Upper(.Columns(i).ControlSource)=="BI.U_BONUS" Or;
					Upper(.Columns(i).ControlSource)=="FN.U_BONUS"

				Bindevent(.Columns(i).text1,"InteractiveChange",DOCUMENTOS.oCust,"proc_ActivaEventosDoc")
			Endif

			*EVENTO ARMAZEM
			If Upper(.Columns(i).ControlSource)=="BI.ARMAZEM" Or;
					Upper(.Columns(i).ControlSource)=="FN.ARMAZEM" Or;
					Upper(.Columns(i).ControlSource)=="BI.AR2MAZEM"

				Bindevent(.Columns(i).text1,"InteractiveChange",DOCUMENTOS.oCust,"proc_ActivaEventosDoc")
			Endif

			**EVENTO CCUSTO
			If Upper(.Columns(i).ControlSource)=="BI.CCUSTO" Or;
					Upper(.Columns(i).ControlSource)=="FN.FNCCUSTO"

				Bindevent(.Columns(i).text1,"Click",DOCUMENTOS.oCust,"proc_clickCCUSTO")
			Endif

			**EVENTO IVA
			If Upper(.Columns(i).ControlSource)=="BI.IVA" Or;
					Upper(.Columns(i).ControlSource)=="FN.IVA"

				Bindevent(.Columns(i).text1,"Click",DOCUMENTOS.oCust,"proc_clickIVA")

			Endif

			**Estat�stica 4
			If Upper(.Columns(i).ControlSource)=="BI.USR2"
				Bindevent(.Columns(i).text1,"Click",DOCUMENTOS.oCust,"proc_clickUSR2")
			Endif

			**EVENTO Lote
			If Upper(.Columns(i).ControlSource)=="BI.LOTE" Or;
					Upper(.Columns(i).ControlSource)=="FN.LOTE"


				Bindevent(.Columns(i).text1,"Click",DOCUMENTOS.oCust,"proc_clickLOTE")
				Bindevent(.Columns(i).text1,"gotFocus",DOCUMENTOS.oCust,"proc_gotFocusLOTE")
				Bindevent(.Columns(i).text1,"lostFocus",DOCUMENTOS.oCust,"proc_lostFocusLOTE")
				Bindevent(.Columns(i).text1,"interactiveChange",DOCUMENTOS.oCust,"proc_interactiveChangeLOTE")

			Endif

			**EVENTO Lote
			If Upper(.Columns(i).ControlSource)=="BI.U_MQUEBRA"

				Bindevent(.Columns(i).text1,"Click",DOCUMENTOS.oCust,"proc_clickMQUEBRA")
				Bindevent(.Columns(i).text1,"gotFocus",DOCUMENTOS.oCust,"proc_gotFocusMQUEBRA")
			Endif

			If Upper(.Columns(i).ControlSource)=="BI.NUM1"
				Bindevent(.Columns(i).text1,"InteractiveChange",DOCUMENTOS.oCust,"proc_ActivaEventosDoc")
			Endif

			If Upper(.Columns(i).ControlSource)=="BI.BINUM3"
				Bindevent(.Columns(i).text1,"InteractiveChange",DOCUMENTOS.oCust,"proc_ActivaEventosDoc")
			Endif

			&& Evento Iva Inclu�do
			If Upper(.Columns(i).ControlSource) == "BI.IVAINCL" Or;
					Upper(.Columns(i).ControlSource) == "FN.IVAINCL"

				Bindevent(.Columns(i).check1,"Click",DOCUMENTOS.oCust,"proc_actualizaTotaisDOCIvaIncl")
				Bindevent(.Columns(i).check1,"Lostfocus",DOCUMENTOS.oCust,"proc_actualizaTotaisDOCIvaIncl")

			Endif

			**EVENTO Desconto compra armaz�m
			If Upper(.Columns(i).ControlSource)=="FN.DESCONTO"

				Bindevent(.Columns(i).text1,"RightClick",DOCUMENTOS.oCust,"proc_clickDESCPANEL")
				Bindevent(.Columns(i).text1,"DblClick",DOCUMENTOS.oCust,"proc_clickDESCPANEL")
				Bindevent(.Columns(i).text1,"KeyPress",DOCUMENTOS.oCust,"proc_pressDESCPANEL")
				Bindevent(.Columns(i).text1,"InteractiveChange",DOCUMENTOS.oCust,"proc_insDESCPANEL")

			Endif

			** EVENTO muda PVPf
			If Upper(.Columns(i).ControlSource)=="FN.U_PVP"
				Bindevent(.Columns(i).text1,"InteractiveChange",DOCUMENTOS.oCust,"proc_ActivaEventosDoc")
				Bindevent(.Columns(i).text1,"Click",DOCUMENTOS.oCust,"proc_clickPvpValido")
			Endif

			** EVENTO muda dATA VALIDADE
			If Upper(.Columns(i).ControlSource)=="FN.U_DTVAL"
				Bindevent(.Columns(i).text1,"InteractiveChange",DOCUMENTOS.oCust,"proc_ActivaEventosDoc")
				Bindevent(.Columns(i).text1,"KeyPress",DOCUMENTOS.oCust,"proc_pressVALPANEL")
			Endif

			** EVENTO DESPESA ADICIONAL NA LINHA DO DOCUMENTO DE COMPRA
			If Upper(.Columns(i).ControlSource)=="FN.VAL_DESP_LIN"
				Bindevent(.Columns(i).text1,"InteractiveChange",DOCUMENTOS.oCust,"proc_ActivaEventosDoc")
			Endif

			** EVENTO CLICK ARMAZEM DESTINO
			If Upper(.Columns(i).ControlSource)=="BI.AR2MAZEM" And cabdoc.tipodos = 3 &&UPPER(ALLTRIM(cabdoc.doc)) == "TRF ENTRE ARMAZ�NS"
				**BindEvent(.columns(i).Text1,"Click",DOCUMENTOS.oCust,"proc_selArmazemDestinoTrf")
			Endif

		Endfor
	Endwith
Endfunc


**
Function uf_documentos_EventosGridDoc
	Lparameters tcBool && .f. activa os eventos .t. desactiva
	DOCUMENTOS.eventoref = tcBool


	* EVENTOS GRIDDOC BO
	With DOCUMENTOS.PageFrame1.Page1.gridDoc

		For i=1 To .ColumnCount

			If Upper(.Columns(i).ControlSource)=="BI.REF" Or;
					Upper(.Columns(i).ControlSource)=="FN.REF"
				If tcBool == .F.
					Bindevent(.Columns(i).text1,"LostFocus",DOCUMENTOS.oCust,"proc_eventoref")
				Else
					Unbindevent(.Columns(i).text1,"LostFocus",DOCUMENTOS.oCust,"proc_eventoref")
				Endif
			Endif

			If Upper(.Columns(i).ControlSource)=="BI.QTT" Or;
					Upper(.Columns(i).ControlSource)=="FN.QTT"
				If tcBool == .F.
					Bindevent(.Columns(i).text1,"LostFocus",DOCUMENTOS.oCust,"proc_actualizaTotaisLinhas")
				Else
					Unbindevent(.Columns(i).text1,"LostFocus",DOCUMENTOS.oCust,"proc_actualizaTotaisLinhas")
				Endif
			Endif

			If Upper(.Columns(i).ControlSource)=="FN.QTREC"
				If tcBool == .F.
					Bindevent(.Columns(i).text1,"LostFocus",DOCUMENTOS.oCust,"proc_actualizaqtrec")
				Else
					Unbindevent(.Columns(i).text1,"LostFocus",DOCUMENTOS.oCust,"proc_actualizaqtrec")
				Endif
			Endif



			If uf_gerais_compstr(.Columns(i).ControlSource, "BI.LOBS")
				Bindevent(.Columns(i).text1,"LostFocus",DOCUMENTOS.oCust,"proc_lostFocusObs")
			Endif



			If Upper(.Columns(i).ControlSource)=="BI.UNI2QTT" Or;
					Upper(.Columns(i).ControlSource)=="FN.UNI2QTT"
				If tcBool == .F.
					Bindevent(.Columns(i).text1,"LostFocus",DOCUMENTOS.oCust,"proc_actualizaUni2Qtt")
				Else
					Unbindevent(.Columns(i).text1,"LostFocus",DOCUMENTOS.oCust,"proc_actualizaUni2Qtt")
				Endif
			Endif

			If Upper(.Columns(i).ControlSource)=="BI.DESCONTO" Or;
					Upper(.Columns(i).ControlSource)=="FN.DESCONTO"
				If tcBool == .F.
					Bindevent(.Columns(i).text1,"LostFocus",DOCUMENTOS.oCust,"proc_actualizaTotaisLinhas")
				Else
					Unbindevent(.Columns(i).text1,"LostFocus",DOCUMENTOS.oCust,"proc_actualizaTotaisLinhas")
				Endif
			Endif

			If Upper(.Columns(i).ControlSource)=="BI.DESC2" Or;
					Upper(.Columns(i).ControlSource)=="FN.DESC2"
				If tcBool == .F.
					Bindevent(.Columns(i).text1,"LostFocus",DOCUMENTOS.oCust,"proc_actualizaTotaisLinhas")
				Else
					Unbindevent(.Columns(i).text1,"LostFocus",DOCUMENTOS.oCust,"proc_actualizaTotaisLinhas")
				Endif
			Endif

			If Upper(.Columns(i).ControlSource)=="BI.DESC3" Or;
					Upper(.Columns(i).ControlSource)=="FN.DESC3"
				If tcBool == .F.
					Bindevent(.Columns(i).text1,"LostFocus",DOCUMENTOS.oCust,"proc_actualizaTotaisLinhas")
				Else
					Unbindevent(.Columns(i).text1,"LostFocus",DOCUMENTOS.oCust,"proc_actualizaTotaisLinhas")
				Endif
			Endif

			If Upper(.Columns(i).ControlSource)=="BI.DESC4" Or;
					Upper(.Columns(i).ControlSource)=="FN.DESC4"
				If tcBool == .F.
					Bindevent(.Columns(i).text1,"LostFocus",DOCUMENTOS.oCust,"proc_actualizaTotaisLinhas")
				Else
					Unbindevent(.Columns(i).text1,"LostFocus",DOCUMENTOS.oCust,"proc_actualizaTotaisLinhas")
				Endif
			Endif

			*EVENTO EDEBITO - PRECO
			If Upper(.Columns(i).ControlSource)=="BI.EDEBITO" Or;
					Upper(.Columns(i).ControlSource)=="FN.EPV" Or;
					Upper(.Columns(i).ControlSource)=="FN.DESPESA"
				If tcBool == .F.
					Bindevent(.Columns(i).text1,"LostFocus",DOCUMENTOS.oCust,"proc_actualizaTotaisLinhas")
				Else
					Unbindevent(.Columns(i).text1,"LostFocus",DOCUMENTOS.oCust,"proc_actualizaTotaisLinhas")
				Endif
			Endif

			*EVENTO EDEBITO - PVPf
			If Upper(.Columns(i).ControlSource)=="FN.U_PVP"
				If tcBool == .F.
					Bindevent(.Columns(i).text1,"LostFocus",DOCUMENTOS.oCust,"proc_actualizaTotaisLinhas")
				Else
					Unbindevent(.Columns(i).text1,"LostFocus",DOCUMENTOS.oCust,"proc_actualizaTotaisLinhas")
				Endif
			Endif

			*EVENTO U_DTVAL
			If Upper(.Columns(i).ControlSource)=="FN.U_DTVAL"
				If tcBool == .F.
					Bindevent(.Columns(i).text1,"LostFocus",DOCUMENTOS.oCust,"proc_valid_u_dtval")
				Else
					Unbindevent(.Columns(i).text1,"LostFocus",DOCUMENTOS.oCust,"proc_valid_u_dtval")
				Endif
			Endif

			*BONUS
			If Upper(.Columns(i).ControlSource)=="BI.U_BONUS" Or;
					Upper(.Columns(i).ControlSource)=="FN.U_BONUS"
				If tcBool == .F.
					Bindevent(.Columns(i).text1,"LostFocus",DOCUMENTOS.oCust,"proc_actualizaTotaisLinhas")
				Else
					Unbindevent(.Columns(i).text1,"LostFocus",DOCUMENTOS.oCust,"proc_actualizaTotaisLinhas")
				Endif
			Endif

			*EVENTO ETTDEB - TOTAL
			If Upper(.Columns(i).ControlSource)=="BI.ETTDEB" Or;
					Upper(.Columns(i).ControlSource)=="FN.ETILIQUIDO"
				If tcBool == .F.
					Bindevent(.Columns(i).text1,"LostFocus",DOCUMENTOS.oCust,"proc_calcularPrecoUni")
				Else
					Unbindevent(.Columns(i).text1,"LostFocus",DOCUMENTOS.oCust,"proc_calcularPrecoUni")
				Endif
			Endif

			*EVENTO ARMAZEM
			If Upper(.Columns(i).ControlSource)=="BI.ARMAZEM" Or;
					Upper(.Columns(i).ControlSource)=="FN.ARMAZEM" Or;
					Upper(.Columns(i).ControlSource)=="BI.AR2MAZEM"

				If tcBool == .F.
					Bindevent(.Columns(i).text1,"LostFocus",DOCUMENTOS.oCust,"proc_ValidaArmazem")
				Else
					Unbindevent(.Columns(i).text1,"LostFocus",DOCUMENTOS.oCust,"proc_ValidaArmazem")
				Endif
			Endif

			If Upper(.Columns(i).ControlSource)=="BI.NUM1" && Fee Keiretsu
				If tcBool == .F.
					Bindevent(.Columns(i).text1,"LostFocus",DOCUMENTOS.oCust,"proc_FeeKeiretsu")
				Else
					Unbindevent(.Columns(i).text1,"LostFocus",DOCUMENTOS.oCust,"proc_FeeKeiretsu")
				Endif
			Endif

			If Upper(.Columns(i).ControlSource)=="BI.BINUM3" && Fee A
				If tcBool == .F.
					Bindevent(.Columns(i).text1,"LostFocus",DOCUMENTOS.oCust,"proc_FeeKeiretsu")
				Else
					Unbindevent(.Columns(i).text1,"LostFocus",DOCUMENTOS.oCust,"proc_FeeKeiretsu")
				Endif
			Endif

			If Upper(.Columns(i).ControlSource)=="FN.VAL_DESP_LIN"
				If tcBool == .F.
					Bindevent(.Columns(i).text1,"LostFocus",DOCUMENTOS.oCust,"proc_actualiza_desp_Linhas")
				Else
					Unbindevent(.Columns(i).text1,"LostFocus",DOCUMENTOS.oCust,"proc_actualiza_desp_Linhas")
				Endif
			Endif


		Endfor
	Endwith

Endfunc


* Corre com duplo click na Referencia
Function uf_documentos_navegast

	Do Case +++++++++
		Case Alltrim(myTipoDoc) = "BO"
			Select Bi
			If !Empty(Bi.ref)
				* Verifica Existencia da Referencia
				TEXT TO lcSQL TEXTMERGE NOSHOW
					Select
						st.*
						,psico = ISNULL(fprod.psico,convert(bit,0))
						,benzo = ISNULL(fprod.benzo ,convert(bit,0))
					FROM
						ST (nolock)
						left join fprod (nolock) on st.ref = fprod.cnp
					WHERE
						ST.REF = '<<Alltrim(BI.REF)>>'
				ENDTEXT
				If !uf_gerais_actGrelha("","ucrsExisteRef",lcSQL)
					uf_perguntalt_chama("OCORREU UMA ANOMALIA NA VERIFICA��O DOS DADOS DA REFER�NCIA, POR FAVOR CONTACTE O SUPORTE.","OK","",16)
					Return .F.
				Endif

				If Reccount("ucrsExisteRef") > 0
					&& VERIFICA SE ESTAVA ALGUM REGISTO EM EDI��O / INSER��O
					If !(Type("gestao_stocks") == "U")
						If !(gestao_stocks.Caption) == "Stocks e Servi�os"
							uf_perguntalt_chama("O ECR� DE STOCKS ENCONTRA-SE EM MODO DE ALTERA��O / INTRODU��O! TERMINE ESTA OPERA��O E TENTE NOVAMENTE.","OK","",64)
							Return .F.
						Endif
					Endif
					uf_stocks_chama(Alltrim(Bi.ref))
				Endif
				If Used("ucrsExisteRef")
					fecha("ucrsExisteRef")
				Endif
			Endif
		Case Alltrim(myTipoDoc) = "FO"
			Select FN
			If !Empty(FN.ref)
				* Verifica Existencia da Referencia
				TEXT TO lcSQL TEXTMERGE NOSHOW
					Select
						st.*
						,psico = ISNULL(fprod.psico,convert(bit,0))
						,benzo = ISNULL(fprod.benzo ,convert(bit,0))
					FROM
						ST (nolock)
						left join fprod (nolock) on st.ref = fprod.cnp
					WHERE
						ST.REF = '<<Alltrim(FN.REF)>>'
				ENDTEXT
				If !uf_gerais_actGrelha("","ucrsExisteRef",lcSQL)
					uf_perguntalt_chama("OCORREU UMA ANOMALIA NA VERIFICA��O DOS DADOS DA REFER�NCIA, POR FAVOR CONTACTE O SUPORTE.","OK","",16)
					Return .F.
				Endif

				If Reccount("ucrsExisteRef") > 0
					&& VERIFICA SE ESTAVA ALGUM REGISTO EM EDI��O / INSER��O
					If !(Type("gestao_stocks") == "U")
						If !(gestao_stocks.Caption) == "Stocks e Servi�os"
							uf_perguntalt_chama("O ECR� DE STOCKS ENCONTRA-SE EM MODO DE ALTERA��O / INTRODU��O! TERMINE ESTA OPERA��O E TENTE NOVAMENTE.","OK","",64)
							Return .F.
						Endif
					Endif
					uf_stocks_chama(Alltrim(FN.ref))
				Endif
				If Used("ucrsExisteRef")
					fecha("ucrsExisteRef")
				Endif
			Endif
	Endcase
Endfunc


** Corre na Altera��o da Coluna de Referencia
Function uf_documentos_eventoref
	Local lcSQL


	** Valida��es
	If  myDocAlteracao == .F. And myDocIntroducao == .F.
		Return .F.
	Endif

	If !Used("BI") And !Used("FN")
		Return .F.
	Endif

	* Verifica Existencia da Referencia, com verifica��o de codigos alternativos
	Do Case
		Case myTipoDoc == "BO"

			Select Bi
			If Empty(Bi.ref)
				Return .F.
			Endif

			lcSQL = ""
			TEXT TO lcSQL TEXTMERGE NOSHOW
				exec up_stocks_procuraRefBO '<<Alltrim(BI.REF)>>', <<mysite_nr>>
			ENDTEXT

		Case myTipoDoc == "FO"

			Select cabdoc
			Select FN
			If Empty(FN.ref)
				Return .F.
			Endif

			lcSQL = ""
			TEXT TO lcSQL TEXTMERGE NOSHOW
				exec up_stocks_procuraRef '<<Alltrim(FN.REF)>>',<<mysite_nr>>, <<IIF(EMPTY(cabdoc.no),0,cabdoc.no)>>, <<IIF(EMPTY(cabdoc.estab),0,cabdoc.estab)>>
			ENDTEXT

		Otherwise
			**
			Return .F.
	Endcase

	If !uf_gerais_actGrelha("", "ucrsExisteRef", lcSQL)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VALIDAR O PRODUTO. POR FAVOR CONTACTE O SUPORTE.","OK","", 16)
		Return .F.
	Endif

	** N�o encontrou nenhuma referencia
	If Reccount("ucrsExisteRef") == 0
		If Type("IMPORTARDOC")=="U"
			** N�o Existe pergunta se deseja criar nova ficha
			If uf_perguntalt_chama("Desculpe, mas essa refer�ncia n�o existe... Quer introduzir uma nova refer�ncia?"+ Chr(13) + "Nota - Vai abrir um novo artigo ou servi�o.","Sim","N�o")

				&& Valida se o ecra esta em modo de consulta
				If !(Type("STOCKS")=="U") And !(STOCKS.Caption=="Stocks e Servi�os")
					uf_perguntalt_chama("N�O � POSS�VEL REALIZAR ESTA AC��O PORQUE O ECR� GEST�O DE STOCKS EST� EM MODO DE EDI��O. POR FAVOR TERMINE ESTA OPERA��O E TENTE NOVAMENTE","OK","",48)
					Return .F.
				Else
					uf_stocks_chama('')
					uf_stocks_novo()
					Select ST
					If myTipoDoc=="BO"
						Replace ST.ref With Alltrim(Bi.ref)
					Else
						Replace ST.ref With Alltrim(FN.ref)
					Endif
					STOCKS.Refresh

					Return .F.
				Endif
			Else
				If myTipoDoc=="BO"
					Select Bi
					Replace Bi.ref With ""
				Else
					Select FN
					Replace FN.ref	With ""
				Endif
				Return .F.
			Endif
		Endif

	Else

		Select ucrsExisteRef
		If ucrsExisteRef.inactivo == .T.
			uf_perguntalt_chama("PRODUTO MARCADO COMO INATIVO.","OK","",64)

			If myTipoDoc=="BO"
				Select Bi
				Replace Bi.ref With ""
			Else
				Select FN
				Replace FN.ref	With ""
			Endif

			Return .F.
		Else

			If myTipoDoc=="BO"
				&& Codigo Alternativo
				Select Bi
				If Alltrim(Bi.ref) != Alltrim(ucrsExisteRef.ref)
					Replace Bi.ref With Alltrim(ucrsExisteRef.ref)
				Endif
			Else
				Select FN
				If Alltrim(FN.ref) != Alltrim(ucrsExisteRef.ref)
					Replace FN.ref	With Alltrim(ucrsExisteRef.ref)
				Endif
			Endif

		Endif
	Endif

	**************************************
	** A fonte do produto j� n�o � utilizado **
	** remover assim que poss�vel **
	** Validar permiss�o por fonte do artigo **
	*!*		Select ucrsExisteRef
	*!*		Go top
	*!*		If Alltrim(ucrsExisteRef.u_fonte) == "E"
	*!*			** VALIDA PERFIL
	*!*			Text To lcSql Noshow textmerge
	*!*				exec up_perfis_validaAcesso 'Gest�o de Stock Externo'
	*!*			Endtext
	*!*			If !uf_gerais_actGrelha("", "uCrsInserirArtigosAcesso", lcSql)
	*!*				uf_perguntalt_chama("OCORREU UM ERRO A VALIDAR ACESSOS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
	*!*				RETURN .f.
	*!*			Endif
	*!*
	*!*			Select uCrsInserirArtigosAcesso
	*!*			If Reccount("uCrsInserirArtigosAcesso")>0
	*!*				Go top
	*!*				Scan
	*!*					If uCrsInserirArtigosAcesso.userno==ch_userno Or Upper(Alltrim(uCrsInserirArtigosAcesso.grupo))==Upper(Alltrim(ch_grupo))
	*!*						lcValidaAcesso=.t.
	*!*					Endif
	*!*				Endscan

	*!*				If lcValidaAcesso == .F.
	*!*					If mytipoDoc == "BO"
	*!*						Select BI
	*!*						Replace BI.REF With ""
	*!*					Endif
	*!*					If mytipoDoc == "FO"
	*!*						Select FN
	*!*						Replace FN.REF With ""
	*!*					Endif
	*!*					uf_perguntalt_chama("N�O TEM ACESSO � INTRODU��O DE PRODUTOS CLASSIFICADOS COMO EXTERNOS. POR FAVOR RECTIFIQUE.","OK","",48)
	*!*					Return
	*!*				Endif
	*!*			Endif
	*!*		Endif
	***********************************



	If myTipoDoc = "BO"
		Select ucrsExisteRef
		Select cabdoc
		Go Top

		** OBTEM DADOS DA ENTIDADE ESCOLHIDA
		If Alltrim(ucrsConfigDoc.bdempresas) == "FL"
			lcSQL = ""
			TEXT TO lcSQL TEXTMERGE NOSHOW
				SELECT * FROM FL (nolock) WHERE no = <<cabdoc.NO>> AND ESTAB =	<<cabdoc.ESTAB>>
			ENDTEXT
			uf_gerais_actGrelha("", "ucrsEntidade", lcSQL)
		Endif

		If Alltrim(ucrsConfigDoc.bdempresas) = "CL"
			TEXT TO lcSQL TEXTMERGE NOSHOW
				SELECT * FROM b_utentes (nolock) WHERE no = <<cabdoc.NO>> AND ESTAB =	<<cabdoc.ESTAB>>
			ENDTEXT
			uf_gerais_actGrelha("", "ucrsEntidade", lcSQL)
		Endif

		If Alltrim(ucrsConfigDoc.bdempresas) = "AG"
			TEXT TO lcSQL TEXTMERGE NOSHOW
				Select '' as tipo,* From AG (nolock) WHERE no = <<cabdoc.NO>>
			ENDTEXT
			uf_gerais_actGrelha("", "ucrsEntidade", lcSQL)
		Endif

		Select ucrsExisteRef
		Select Bi
		Replace Bi.Design 		With ucrsExisteRef.Design

		If uf_gerais_getParameter_Site('ADM0000000060', 'BOOL', mySite)
			Replace Bi.qtt With ucrsExisteRef.qttembal
		Else
			If Bi.qtt == 0
				**IF BI.ndos != 46 && colocado por jg em 2020/08/12 para possibilitar multiplica��o das Qtt no tipo de documento "conversao unidades (46)"

				If 	uf_gerais_getParameter_Site('ADM0000000218', 'BOOL', mySite) And ucrsConfigDoc.NUMINTERNODOC = 2 And ucrsExisteRef.qttembenc <> 0
					Replace Bi.qtt 			With ucrsExisteRef.qttembenc
				Else
					Replace Bi.qtt 			With 1 && substitui Qtt por 1 em todas as Qtt excepto conversao unidades
				Endif





				**ENDIF
			Endif


		Endif

		Replace Bi.codigo 		With ucrsExisteRef.codigo
		Replace Bi.u_stockact	With ucrsExisteRef.STOCK
		Replace Bi.cpoc			With ucrsExisteRef.cpoc
		Replace Bi.familia 		With ucrsExisteRef.familia
		Replace Bi.NO			With cabdoc.NO
		Replace Bi.Nome			With cabdoc.Nome
		Replace Bi.Local 		With ucrsEntidade.Local
		Replace Bi.MORADA		With ucrsEntidade.MORADA
		Replace Bi.CODPOST		With ucrsEntidade.CODPOST
		Replace Bi.epu			With ucrsExisteRef.EPCUSTO
		Replace Bi.usaLote		With ucrsExisteRef.usaLote
		Replace Bi.psico		With ucrsExisteRef.psico
		Replace Bi.benzo		With ucrsExisteRef.benzo
		Replace Bi.familia 		With ucrsExisteRef.familia
		Replace Bi.viaVerde 	With ucrsExisteRef.viaVerde
		Replace Bi.u_reserva 	With Iif(ucrsExisteRef.qttcli != 0,.T.,.F.)

		** IVA
		Replace Bi.tabiva 		With ucrsExisteRef.tabiva

		** For�a IVA a .t. nas Reservas
		*IF UPPER(Alltrim(cabdoc.doc)) == "RESERVA DE CLIENTE"
		**IF UPPER(Alltrim(cabdoc.doc)) != "DEVOL. A FORNECEDOR" AND UPPER(Alltrim(cabdoc.doc)) != "DEVOL. A FORNECEDOR VV"
		**IF !INLIST(UPPER(ALLTRIM(cabdoc.doc)),"DEVOL. A FORNECEDOR","DEVOL. A FORNECEDOR VV","ACERTO DE STOCK")
		If !Inlist(cabdoc.tipodos, 2,4)
			Select Bi
			Replace Bi.ivaincl		With .T.
		Else
			Select Bi
			Replace Bi.ivaincl		With .F.
		Endif
		**ENDIF



		lcSQL = ""
		TEXT TO lcSQL TEXTMERGE NOSHOW
			SELECT CODIGO,TAXA FROM taxasiva (nolock) WHERE codigo = <<ucrsExisteRef.TABIVA>> and ativo=1
		ENDTEXT
		If uf_gerais_actGrelha("", "ucrsIvaDefault", lcSQL)
			Replace Bi.IVA With ucrsIvaDefault.taxa
		Endif


		** CONTROLA O PRE�O A UTILIZAR DEFINIDO NA CONFIGURA��O DO DOSSIER

		** Pre�o Utilizado para Pr. Custo
		Do Case
				** 0
			Case  	ucrsConfigDoc.QPRECOCUSTO = 0
				myprecocusto = ucrsExisteRef.EPCUSTO
				** Pre�o de Custo Ponderado
			Case	ucrsConfigDoc.QPRECOCUSTO = 1
				myprecocusto = ucrsExisteRef.EPCPOND
				** �ltimo pre�o de Custo
			Case	ucrsConfigDoc.QPRECOCUSTO = 2
				myprecocusto = ucrsExisteRef.EPCULT
				** Pre�o de Custo Actual
			Case	ucrsConfigDoc.QPRECOCUSTO = 3
				myprecocusto = ucrsExisteRef.EPCUSTO
		Endcase



		** Pre�o Utilizado para Pr. Unit�rio
		Do Case
				** 0 - SEM PRE�O DE VENDA
			Case	ucrsConfigDoc.QPRECO = 0
				myprecounitario = 0
				** 1 - Pre�o de Venda
			Case  	ucrsConfigDoc.QPRECO = 1
				myprecounitario = ucrsExisteRef.EPV1
				** 2 - Pre�o de Custo
			Case	ucrsConfigDoc.QPRECO = 2
				myprecounitario = ucrsExisteRef.EPCUSTO
				** 3 - Pre�o M�dio de Custo
			Case	ucrsConfigDoc.QPRECO = 3
				myprecounitario = ucrsExisteRef.EPCPOND
				** 4 - Zero
			Case	ucrsConfigDoc.QPRECO  = 4
				myprecounitario = 0
				** 5 - Pre�o de Venda 2
			Case  	ucrsConfigDoc.QPRECO = 5
				myprecounitario =  ucrsExisteRef.EPV2
				** 6 - Pre�o de Venda 3
			Case	ucrsConfigDoc.QPRECO = 6
				myprecounitario = ucrsExisteRef.EPV3
				** 7 - Pre�o de Venda 4
			Case	ucrsConfigDoc.QPRECO = 7
				myprecounitario = ucrsExisteRef.EPV4
				** 8 - Pre�o de Venda 5
			Case	ucrsConfigDoc.QPRECO = 8
				myprecounitario = ucrsExisteRef.EPV5
				** 9 - �ltimo Pre�o de Custo
			Case	ucrsConfigDoc.QPRECO = 9
				myprecounitario = ucrsExisteRef.EPCULT
		Endcase

		** arredondar os pre�os para casos em que a st contenha pre�os com mais de 2 casas decimais **

		If cabdoc.NUMINTERNODOC != 2
			myprecocusto = Round(myprecocusto,2)
			myprecounitario = Round(myprecounitario,2)
		Else && Nas encomendas a forncedor caso exsita aplica tabela de pre�os
			myprecocusto = Round(Iif(ucrsExisteRef.pclfornec == 0,myprecocusto,ucrsExisteRef.pclfornec),2)
			myprecounitario = Round(Iif(ucrsExisteRef.pclfornec == 0,myprecounitario,ucrsExisteRef.pclfornec),2)
		Endif
		**********************************************************************************************

		Set Hours To 24
		Atime=Ttoc(Datetime()+(difhoraria*3600),2)
		ASTR=Left(Atime,8)

		Replace Bi.edebito		With myprecounitario
		Replace Bi.eprorc		With myprecounitario
		Replace Bi.EPCUSTO		With myprecocusto
		Replace Bi.eslvu		With myprecounitario
		Replace Bi.esltt		With myprecounitario*Bi.qtt

		Replace Bi.ettdeb		With myprecounitario*Bi.qtt
		Replace Bi.ecustoind	With myprecounitario
		Replace Bi.edebitoori	With myprecounitario
		Replace Bi.u_upc		With myprecocusto
		Replace Bi.rdata		With Date()
		Replace Bi.DATAOBRA		With Date()
		Replace Bi.dataopen		With Date()
		Replace Bi.ousrinis		With m_chinis
		Replace Bi.ousrdata		With Date()
		Replace Bi.ousrHORA		With ASTR
		Replace Bi.usrinis		With m_chinis
		Replace Bi.usrdata		With Date()
		Replace Bi.usrhora		With ASTR

		** aplica desconto por fornecedor **
		If Upper(Alltrim(cabdoc.Doc))=="ENCOMENDA A FORNECEDOR"
			Select ucrsExisteRef
			If ucrsExisteRef.fornec == cabdoc.NO And ucrsExisteRef.fornestab == cabdoc.ESTAB
				If ucrsExisteRef.mfornec>0
					*replace bi.desconto	With ucrsExisteRef.mfornec
					Replace Bi.desconto	With 0
				Endif

				If ucrsExisteRef.mfornec2>0
					Replace Bi.desc2	With 0
					*replace bi.desc2	With ucrsExisteRef.mfornec2
				Endif
			Endif
		Endif
		**

		** Actualiza Dados da Ref
		TEXT TO lcSQL TEXTMERGE NOSHOW
			exec up_stocks_verDadosActuaisporarmazem '<<Alltrim(ucrsExisteRef.REF)>>',<<MYARMAZEM>>, <<cabDoc.NO>>, <<cabDoc.estab>>,<<mysite_nr>>
		ENDTEXT

		If uf_gerais_actGrelha("", "ucrsOutrosDadosRef", lcSQL)
			If Reccount("ucrsOutrosDadosRef") > 0
				Select ucrsOutrosDadosRef
				Go Top
				Select Bi
				Replace Bi.u_epv1act With ucrsOutrosDadosRef.EPV1
				Replace Bi.u_stockact With ucrsOutrosDadosRef.STOCK
			Endif
			fecha("ucrsOutrosDadosRef")
		Endif

		If Used("ucrsts")
			If ucrsts.usaDescForn

				Select cabdoc
				If !Empty(cabdoc.NO)

					Select Bi

					TEXT TO lcSQL TEXTMERGE NOSHOW
						up_documentos_getDescForn '<<ALLTRIM(bi.ref)>>', <<cabdoc.no>>, <<cabdoc.estab>>
					ENDTEXT

					If !uf_gerais_actGrelha("", "uc_descFornEncAuto", lcSQL)
						uf_perguntalt_chama("Erro a obter desconto de Fornecedor." + Chr(13) + "Por favor contacte o suporte.", "OK", "", 48)
					Else
						Select uc_descFornEncAuto
						Select Bi

						Replace Bi.desconto With uc_descFornEncAuto.desconto

						fecha("uc_descFornEncAuto")
					Endif

				Endif

			Endif
		Endif

		** UNI2QTT
		uf_documentos_actualizaUni2Qtt()
		uf_documentos_recalculaTotaisBI()

		If Used("CABDOC")
			Local lcValUnitCaixa
			Store .F. To lcValUnitCaixa
			Select cabdoc
			If Alltrim(cabdoc.Doc)='Imprimir Etiquetas'
				Select Bi
				Replace Bi.edebito With Round(Bi.edebito * Bi.qtt,2)
			Endif

		Endif

	Endif

	*** FO - LINHAS COMPRAS
	If myTipoDoc = "FO"

		Select ucrsExisteRef
		Select FN
		If Empty(FN.Design) And FN.qtt>0 And FN.etiliquido>0 Then

			Set Hours To 24
			Atime=Ttoc(Datetime()+(difhoraria*3600),2)
			ASTR=Left(Atime,8)

			Replace FN.Design 		With ucrsExisteRef.Design
			Replace FN.tabiva 		With ucrsExisteRef.tabiva
			Replace FN.pvporig		With ucrsExisteRef.pvpdic
			TEXT TO lcSQL TEXTMERGE NOSHOW
				SELECT CODIGO,TAXA FROM taxasiva (nolock) WHERE codigo = <<ucrsExisteRef.TABIVA>> and ativo=1
			ENDTEXT
			If uf_gerais_actGrelha("", "ucrsIvaDefault", lcSQL)
				Replace FN.IVA With ucrsIvaDefault.taxa
			Endif
			Replace FN.ivaincl		With .F.
			Replace FN.cpoc			With ucrsExisteRef.cpoc
			Replace FN.marcada		With .F.
			Replace FN.ousrinis		With m_chinis
			Replace FN.ousrdata		With Date()
			Replace FN.ousrHORA		With Time()
			Replace FN.usrinis		With m_chinis
			Replace FN.usrdata		With Date()
			Replace FN.usrhora		With ASTR
			Replace FN.u_reserva 	With Iif(ucrsExisteRef.qttcli != 0,.T.,.F.)
			Replace FN.familia 		With ucrsExisteRef.familia
			Replace FN.stkatualloja		With ucrsExisteRef.STOCK
			Replace FN.u_dtval 		With Alltrim(Str(Year(ucrsExisteRef.validade)))+Right(('0'+Alltrim(Str(Month(ucrsExisteRef.validade)))),2)
			Select FN
			Replace FN.desconto 	With 0.00
			Replace FN.totalant		With FN.etiliquido
			Replace FN.alttotal		With .F.
			Replace FN.descant		With FN.desconto
			Replace FN.totalant		With FN.etiliquido

			If(Upper(Alltrim(cabdoc.Doc))=="V/FACTURA MED.")
				If ucrsExisteRef.pvpdic=0
					Replace FN.U_PVP 	With 0
				Else
					Replace FN.U_PVP 	With ucrsExisteRef.EPV1
				Endif
			Endif

			Select FN

		Else

			Replace FN.num1 		With ucrsExisteRef.qttcli
			Replace FN.Design 		With ucrsExisteRef.Design
			Replace FN.stns			With ucrsExisteRef.stns
			Replace FN.usr1 		With ucrsExisteRef.usr1
			Replace FN.unidade 		With ucrsExisteRef.unidade
			Replace FN.usaLote		With ucrsExisteRef.usaLote
			Replace FN.psico		With ucrsExisteRef.psico
			Replace FN.benzo		With ucrsExisteRef.benzo
			Replace FN.pvporig		With ucrsExisteRef.pvpdic

			If FN.qtt == 0

				Replace FN.qtt 		With 1
				Replace FN.qtrec 	With 1

				&&Limpa Lote Conf
				With DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.Page12.PageFrame1
					.Page1.Container1.txtLoteConf.Value = ""
					.Page2.Container2.txtLoteConf.Value = ""
				Endwith
			Endif


			**IVA
			Replace FN.tabiva 		With ucrsExisteRef.tabiva

			lcSQL = ""
			TEXT TO lcSQL TEXTMERGE NOSHOW
				SELECT CODIGO,TAXA FROM taxasiva (nolock) WHERE codigo = <<ucrsExisteRef.TABIVA>> and ativo=1
			ENDTEXT
			If uf_gerais_actGrelha("", "ucrsIvaDefault", lcSQL)
				Replace FN.IVA With ucrsIvaDefault.taxa
			Endif

			Set Hours To 24
			Atime=Ttoc(Datetime()+(difhoraria*3600),2)
			ASTR=Left(Atime,8)

			Select ucrsExisteRef
			**LORDEM
			** Trata Ordem das Linhas
			Replace FN.lordem 		With Recno("FN") * 1000
			Replace FN.ivaincl		With .F.
			Replace FN.cpoc			With ucrsExisteRef.cpoc
			Replace FN.epv			With ucrsExisteRef.EPCUSTO
			Replace FN.eslvu		With ucrsExisteRef.EPCUSTO
			Replace FN.esltt		With ucrsExisteRef.EPCUSTO
			Replace FN.descFin_pcl	With ucrsExisteRef.descFin_pcl
			Replace FN.etiliquido	With ucrsExisteRef.EPCUSTO
			Replace FN.u_stockact	With ucrsExisteRef.STOCK
			Replace FN.marcada		With .F.
			Replace FN.ousrinis		With m_chinis
			Replace FN.ousrdata		With Date()
			Replace FN.ousrHORA		With ASTR
			Replace FN.usrinis		With m_chinis
			Replace FN.usrdata		With Date()
			Replace FN.usrhora		With ASTR
			Replace FN.u_reserva 	With Iif(ucrsExisteRef.qttcli != 0,.T.,.F.)
			Replace FN.familia 		With ucrsExisteRef.familia
			Replace FN.stkatualloja		With ucrsExisteRef.STOCK
			Replace FN.u_dtval 		With Alltrim(Str(Year(ucrsExisteRef.validade)))+Right(('0'+Alltrim(Str(Month(ucrsExisteRef.validade)))),2)
			**validade   =  DATE(VAL(LEFT(ALLTRIM(fn.u_dtval),4)), VAL(RIGHT(ALLTRIM(fn.u_dtval),2)), 1)
			**lcUltimoDia  = GOMONTH(validade   , 1) - day(validade   )
			**replace FN.dt_val_orig	WITH lcUltimoDia
			If(Upper(Alltrim(cabdoc.Doc))=="V/FACTURA MED.")
				If ucrsExisteRef.pvpdic=0
					Replace FN.U_PVP 	With 0
				Else
					Replace FN.U_PVP 	With ucrsExisteRef.EPV1
				Endif
			Endif

			Select FN
			** aplica desconto por fornecedor **
			If Upper(Alltrim(cabdoc.Doc))=="V/FACTURA" Or Upper(Alltrim(cabdoc.Doc))=="V/FACTURA MED."
				Select ucrsExisteRef
				If ucrsExisteRef.fornec == cabdoc.NO And ucrsExisteRef.fornestab == cabdoc.ESTAB
					If ucrsExisteRef.mfornec>0
						Replace FN.desconto	With ucrsExisteRef.mfornec
					Endif

					If ucrsExisteRef.mfornec2>0
						Replace FN.desc2	With ucrsExisteRef.mfornec2
					Endif
				Endif
			Endif
			*****************************


			Select FN
			TEXT TO lcSQL TEXTMERGE NOSHOW
				exec up_stocks_verDadosActuaisporarmazem '<<Alltrim(FN.REF)>>',<<myarmazem>>,<<cabDoc.NO>>,<<cabDoc.estab>>,<<mySite_nr>>
			ENDTEXT
			If uf_gerais_actGrelha("", "ucrsOutrosDadosRef", lcSQL)
				If Reccount("ucrsOutrosDadosRef") > 0
					Select ucrsOutrosDadosRef
					Go Top
					Select FN
					Replace FN.u_epv1act With ucrsOutrosDadosRef.EPV1
					Replace FN.u_validact With ucrsOutrosDadosRef.validade
					Replace FN.u_stockact With ucrsOutrosDadosRef.STOCK
				Endif
				fecha("ucrsOutrosDadosRef")
			Endif

			&& Desconto habitual Fornecedor
			If !Empty(DOCUMENTOS.descontofl)
				Select FN
				Replace FN.desconto With DOCUMENTOS.descontofl
			Endif

			Select FN
			Replace FN.desconto 	With 0.00
			Replace FN.totalant		With FN.etiliquido
			Replace FN.alttotal		With .F.
			Replace FN.descant		With FN.desconto
			Replace FN.totalant		With FN.etiliquido

		Endif

		uf_documentos_actualizaUni2Qtt()
		uf_documentos_recalculaTotaisFN()

	Endif

	If ucrsExisteRef.dispositivo_seguranca

		Select fi_trans_info
		If uf_gerais_compstr(myTipoDoc, "BO")
			Locate For uf_gerais_compstr(fi_trans_info.recStamp, Bi.bistamp)
		Else
			Locate For uf_gerais_compstr(fi_trans_info.recStamp, FN.fnstamp)
		Endif

		If !Found()
			If uf_gerais_compstr(myTipoDoc, "BO")
				uf_insert_fi_trans_info_seminfo(Bi.ref, Bi.bistamp)
			Else
				uf_insert_fi_trans_info_seminfo(FN.ref, FN.fnstamp)
			Endif
		Endif

	Endif

	uf_documentos_actualizaTotaisCAB()

	If Used("ucrsExisteRef")
		fecha("ucrsExisteRef")
	Endif

	uf_documentos_EventosGridDoc(.T.)

	Return .T.

Endfunc


**
Function uf_documentos_actualizaUni2Qtt
	Local lcFlNo
	Store 0 To lcFlNo

	Select cabdoc
	If !Empty(cabdoc.NO)
		lcFlNo = cabdoc.NO
	Endif

	Do Case
		Case Alltrim(myTipoDoc) == "FO"

			Select FN
			If !Empty(FN.ref) And !Empty(FN.uni2qtt)
				If FN.uni2qtt<=999999
					TEXT TO lcSql NOSHOW textmerge
						exec up_stocks_factorConversaoFl '<<fn.ref>>', <<lcFlNo>> ,<<mySite_nr>>
					ENDTEXT
					If uf_gerais_actGrelha("", "uCrsGetConv", lcSQL)
						If Reccount()>0
							If Empty(uCrsGetConv.conversao)
								Replace FN.qtt With FN.uni2qtt
							Else
								Replace FN.qtt With Int(FN.uni2qtt/uCrsGetConv.conversao)
							Endif
						Endif
						fecha("uCrsGetConv")
					Endif
				Else
					Select FN
					Replace FN.uni2qtt With 0
				Endif
			Endif

			uf_documentos_recalculaTotaisFN()
		Case Alltrim(myTipoDoc) == "BO"

			Select Bi
			If !Empty(Bi.ref) And !Empty(Bi.uni2qtt)
				If Bi.uni2qtt<=999999
					TEXT TO lcSql NOSHOW textmerge
						exec up_stocks_factorConversaoFl '<<bi.ref>>', <<lcFlNo>>, <<mySite_nr>>
					ENDTEXT
					If uf_gerais_actGrelha("", "uCrsGetConv", lcSQL)
						If Reccount()>0
							If Empty(uCrsGetConv.conversao)
								Replace Bi.qtt With Bi.uni2qtt
							Else
								Replace Bi.qtt With Int(Bi.uni2qtt/uCrsGetConv.conversao)
							Endif
						Endif
						fecha("uCrsGetConv")
					Endif
				Else
					Select Bi
					Replace Bi.uni2qtt With 0
				Endif
			Endif

			uf_documentos_recalculaTotaisBI()
	Endcase
Endfunc


**
Function uf_documentos_recalculaTotaisBI
	Lparameters lcCursorCab, lcCursorLin && usado para calcular valores sem o painel de documentos aberto

	If Empty(lcCursorCab)
		lcCursorCab = "CabDoc"
	Endif
	If Empty(lcCursorLin)
		lcCursorLin = "BI"
	Endif

	Local lcTotal, lcValida, lcFlNo, lcTabIva
	Store 0 To lcFlNo, lcTotal, lcTabIva

	** Tabela Fixa de IVA
	If !Empty(&lcCursorCab..tabiva) && se a tabela fixa de iva vier vazia, aplica a taxa/Tabela do produto ST

		lcTabIva = &lcCursorCab..tabiva
		lcTaxaIva = uf_gerais_getTabelaIva(lcTabIva,"TAXA")
		Select &lcCursorLin
		Replace &lcCursorLin..tabiva 	With lcTabIva
		Replace &lcCursorLin..IVA 		With lcTaxaIva

	Endif
	**

	Select &lcCursorCab
	lcFlNo = &lcCursorCab..NO

	Select &lcCursorLin
	lcRef = &lcCursorLin..ref
	lcQtt = &lcCursorLin..qtt

	** calcular quantidades alternativas **
	Select &lcCursorLin
	If !Empty(lcRef) And !Empty(lcQtt)
		lcSQL = ""
		TEXT TO lcSql NOSHOW TEXTMERGE
			exec up_stocks_factorConversaoFl '<<ALLTRIM(lcRef)>>', <<lcFlNo>>, <<mySite_nr>>
		ENDTEXT

		If uf_gerais_actGrelha("", "uCrsGetConv", lcSQL)
			If Reccount()>0
				Replace &lcCursorLin..uni2qtt With Int(lcQtt*uCrsGetConv.conversao)
			Endif
			fecha("uCrsGetConv")
		Endif
	Else
		Select &lcCursorLin
		Replace &lcCursorLin..uni2qtt With 0
	Endif

	** preencher stock actual **
	If !Empty(lcRef)
		TEXT TO lcSQL NOSHOW TEXTMERGE
			exec up_stocks_verStockAct '<<ALLTRIM(lcRef)>>',<<mySite_nr>>
		ENDTEXT

		If uf_gerais_actGrelha("", "uCrsStockActBi", lcSQL)
			If Reccount()>0
				Select &lcCursorLin
				Replace &lcCursorLin..u_stockact With uCrsStockActBi.STOCK
			Endif
			fecha("uCrsStockActBi")
		Endif
	Endif
	****************************

	** Calcular totais **
	Select &lcCursorLin
	*	IF !EMPTY(bi.qtt)
	If &lcCursorLin..qtt == 0
		Replace &lcCursorLin..u_bonus With 0
	Endif

	lcTotal = &lcCursorLin..edebito*(&lcCursorLin..qtt)
	If &lcCursorLin..u_bonus>0
		lcTotal = &lcCursorLin..edebito*(&lcCursorLin..qtt - &lcCursorLin..u_bonus)
	Endif
	If (&lcCursorLin..desconto>0) And (&lcCursorLin..desconto<=100)
		lcTotal = lcTotal - (lcTotal*(&lcCursorLin..desconto/100))
	Endif
	If (&lcCursorLin..desc2>0) And (&lcCursorLin..desc2<=100)
		lcTotal = lcTotal - (lcTotal*(&lcCursorLin..desc2/100))
	Endif
	If (&lcCursorLin..desc3>0) And (&lcCursorLin..desc3<=100)
		lcTotal = lcTotal - (lcTotal*(&lcCursorLin..desc3/100))
	Endif
	If (&lcCursorLin..desc4>0) And (&lcCursorLin..desc4<=100)
		lcTotal = lcTotal - (lcTotal*(&lcCursorLin..desc4/100))
	Endif



	Replace &lcCursorLin..esltt With Round(lcTotal,2)
	Replace &lcCursorLin..ettdeb With Round(lcTotal,2)


	If !Empty(Bi.ref) And Bi.qtt=0
		Replace Bi.eslvu		With 0
	Endif

	If  &lcCursorLin..qtt>0 And &lcCursorLin..edebito>0
		uf_documentos_calculaPercDescCom(lcCursorLin)
		uf_documentos_calculaUpc(lcCursorLin)
	Else
		Select &lcCursorLin
		Replace &lcCursorLin..u_desccom	With 0
	Endif


Endfunc


** Calcula Valores das Linhas em documentos de Compra
Function uf_documentos_recalculaTotaisFN
	Lparameters uv_fromEvent

	Local lcTotal,lcTabIva, nmdoc, dataValida

	Store 0 To lcTotal,lcTabIva

	dataValida = .T.
	nmdoc = ""




	If !Empty(FN.qtt) Or FN.qtt>=0

		If(Used("cabDoc"))
			Select  cabdoc
			nmdoc = Alltrim(Upper(cabdoc.Doc))
		Endif

		**IF fn.epv < fn.u_upc AND ALLTRIM(UPPER(cabDoc.Doc)) != ALLTRIM(UPPER("V/Factura Med."))
		**	uf_perguntalt_chama("ATEN��O! O PCT DA LINHA N�O PODE SER INFERIOR AO PCL.","OK","",16)
		**	replace fn.epv WITH fn.pctant
		**ENDIF

		Select FN
		If FN.qtt == 0
			Replace FN.u_bonus With 0
		Endif

		** Tabela Fixa de IVA
		**IF !EMPTY(CabDoc.tabIva) && se a tabela fixa de iva vier vazia, aplica a taxa/Tabela do produto ST
		**	lcTabIva = CabDoc.tabIva
		**	lcTaxaIva = uf_gerais_getTabelaIva(lcTabIva,"TAXA")
		If !Empty(FN.ref) And mySelIvaMan=0
			TEXT TO lcSql NOSHOW textmerge
				select ref,tabiva, (select taxa from taxasiva(nolock) where st.tabiva=taxasiva.codigo) as iva from st(nolock) WHERE st.ref='<<ALLTRIM(fn.ref)>>'
			ENDTEXT
			uf_gerais_actGrelha("", "uCrsvaliva", lcSQL)

			Replace FN.tabiva 	With uCrsvaliva.tabiva
			Replace FN.IVA 		With uCrsvaliva.IVA
		Endif
		**ENDIF

		**
		If  Alltrim(Upper(cabdoc.Doc)) != Alltrim(Upper("V/Factura Med."))
			Select FN

			lcTotal = FN.epv*(FN.qtt-FN.u_bonus)



			If (FN.desconto > 0) And (FN.desconto <= 100)
				lcTotal = lcTotal - (lcTotal*(FN.desconto/100))
			Endif
			If (FN.desc2>0) And (FN.desc2<=100)
				lcTotal = lcTotal - (lcTotal*(FN.desc2/100))
			Endif
			If (FN.desc3>0) And (FN.desc3<=100)
				lcTotal = lcTotal - (lcTotal*(FN.desc3/100))
			Endif
			If (FN.desc4>0) And (FN.desc4<=100)
				lcTotal = lcTotal - (lcTotal*(FN.desc4/100))
			Endif
			Select cabdoc
			If Upper(Alltrim(cabdoc.Doc)) == Upper("Mapa Honorarios")
				lcTotal = lcTotal + FN.DESPESA
			Endif
			Select FN

			Replace FN.etiliquido 	With Round(lcTotal,2)
			Replace FN.esltt 		With Round(lcTotal,2)


			If (!Empty(FN.ref) Or !Empty(FN.oref)) And FN.qtt = 0
				Replace FN.u_upc		With 0
				Replace FN.eslvu		With 0
			Endif



			** % desconto comercial
			If (!Empty(FN.u_bonus) Or !Empty(FN.desconto) Or !Empty(FN.desc2) Or !Empty(FN.desc3) Or !Empty(FN.desc4)) And (FN.epv > 0 And FN.qtt > 0)
				uf_documentos_calculaPercDescCom()
			Endif

		Else
			Select FN
			Do Case
				Case FN.descant != FN.desconto And FN.qtt>0

					Replace FN.epv With Round((FN.etiliquido/FN.qtt)/Iif(FN.desconto <> 100, (1-(FN.desconto/100)), 1),2)
					Replace FN.etiliquido With Iif(FN.desconto <> 100, FN.etiliquido, 0)
					Replace FN.descant With FN.desconto

					If (!Empty(FN.ref) Or !Empty(FN.oref)) And FN.qtt = 0
						Replace FN.u_upc		With 0
						Replace FN.eslvu		With 0
					Endif

				Case FN.pctant != FN.epv And FN.epv!=0 And FN.qtt>0
					uv_desc = (1-((FN.etiliquido/FN.qtt)/FN.epv))*100
					uv_desc = Round(uv_desc, 2)
					Replace FN.desconto With Iif(uv_desc < 0, 0, uv_desc)

					Replace FN.pctant With FN.epv
					Replace FN.descant With FN.desconto

				Case FN.totalant != FN.etiliquido And FN.qtt>0

					Replace FN.alttotal With .T.
					Replace FN.totalant With FN.etiliquido

					If FN.etiliquido = 0
						Replace FN.desconto With 100
						Replace FN.descant With 100
					Else
						If FN.epv <> 0 And FN.qtt <> 0

							uv_desc = (1-((FN.etiliquido/FN.qtt)/FN.epv))*100

							uv_desc = Round(uv_desc, 2)

							Replace FN.desconto With Iif(uv_desc < 0, 0, uv_desc)
							Replace FN.descant With FN.desconto

						Endif

					Endif

				Case FN.epv = 0 And FN.qtt <> 0
					Replace FN.desconto With 0
					Replace FN.descant With 0
					Replace FN.pctant With (FN.etiliquido/FN.qtt)
					Replace FN.epv With (FN.etiliquido/FN.qtt)

				Otherwise
					lcTotal = FN.epv*(FN.qtt-FN.u_bonus)
					If (FN.desconto > 0) And (FN.desconto <= 100)
						lcTotal = lcTotal - (lcTotal*(FN.desconto/100))
					Endif
					If (FN.desc2>0) And (FN.desc2<=100)
						lcTotal = lcTotal - (lcTotal*(FN.desc2/100))
					Endif
					If (FN.desc3>0) And (FN.desc3<=100)
						lcTotal = lcTotal - (lcTotal*(FN.desc3/100))
					Endif
					If (FN.desc4>0) And (FN.desc4<=100)
						lcTotal = lcTotal - (lcTotal*(FN.desc4/100))
					Endif
					Select cabdoc
					If Upper(Alltrim(cabdoc.Doc)) == Upper("Mapa Honorarios")
						lcTotal = lcTotal + FN.DESPESA
					Endif
					Select FN

					If !uv_fromEvent
						Replace FN.etiliquido 	With Round(lcTotal,2)
					Endif

					Replace FN.totalant 	With Round(lcTotal,2)
					Replace FN.esltt 		With Round(lcTotal,2)
					If (!Empty(FN.ref) Or !Empty(FN.oref)) And FN.qtt = 0
						Replace FN.u_upc		With 0
						Replace FN.eslvu		With 0
					Endif

					** % desconto comercial
					If (!Empty(FN.u_bonus) Or !Empty(FN.desconto) Or !Empty(FN.desc2) Or !Empty(FN.desc3) Or !Empty(FN.desc4)) And (FN.epv > 0 And FN.qtt > 0)
						uf_documentos_calculaPercDescCom()
					Endif
			Endcase

			**MESSAGEBOX(fn.epv)
		Endif

		** calcula precos de custo


		If FN.qtt > 0 &&AND FN.etiliquido > 0


			uf_documentos_calculaUpc()
			uf_documentos_calculaPercMrgSpct()

			uf_documentos_calculaPercMrgSupc()

			If FN.u_bonus > 0 And Alltrim(Upper(cabdoc.Doc)) != Alltrim(Upper("V/Factura Med."))
				uf_documentos_calculaPercBonusFN()
			Endif
		Endif


		** verifica validade PVPf
		&& preencher a validade do PVP da ficha nas faturas de armaz�m - igual � valida��o do atendimento

		If FN.U_PVP = FN.pvporig And Alltrim(cabdoc.Doc) == "V/Factura Med."
			uf_documentos_validaPrecoAtribuiCor()
		Else
			uf_documentos_validaPrecoAtribuiCor()
		Endif

	Endif
Endfunc

Function uf_documentos_validaPrecoAtribuiCor

	Local lcSqlCor
	Store '' To lcSqlCor

	If(!Used("FN"))
		uf_perguntalt_chama("Ocorreu um erro ao criar o cursor FN", "", "OK", 16)
		Return .F.
	Endif

	Select FN
	TEXT TO lcSqlCor NOSHOW TEXTMERGE
		exec up_precosValidosCor'<<ALLTRIM(fn.ref)>>', <<fn.u_pvp>>
	ENDTEXT

	If uf_gerais_actGrelha("","uCrsCor",lcSqlCor)

		Select FN
		If Inlist(Alltrim(FN.familia),'1','2','10','23','58')
			Do Case
				Case uf_gerais_compstr(uCrsCor.cor, '255,0,0')
					Replace FN.pvpval With '255,0,0'
				Case uf_gerais_compstr(uCrsCor.cor, '241,196,15')
					Replace FN.pvpval With '241,196,15'
				Otherwise
					Replace FN.pvpval With  '255,255,255'
			Endcase
		Else
			Replace FN.pvpval With  '255,255,255'
		Endif
	Endif
	fecha("uCrsCor")

Endfunc

**
Function uf_documentos_calculaPercDescCom
	Lparameters lcCursorLin

	If Empty(lcCursorLin)
		Do Case
			Case myTipoDoc == "FO"
				lcCursorLin = "FN"
			Case myTipoDoc == "BO"
				lcCursorLin = "BI"
		Endcase
	Endif

	Local lcDesc
	Store 0 To lcDesc

	If myTipoDoc == "FO"
		Select &lcCursorLin
		If (!Empty(&lcCursorLin..desconto) Or !Empty(&lcCursorLin..u_bonus)) And (&lcCursorLin..epv > 0 And &lcCursorLin..qtt > 0)
			If Empty(&lcCursorLin..u_bonus) Or (&lcCursorLin..desconto <= 100 And &lcCursorLin..u_bonus < &lcCursorLin..qtt)
				lcDesc = (1 - (&lcCursorLin..etiliquido / (&lcCursorLin..epv*&lcCursorLin..qtt)) ) * 100
				Replace &lcCursorLin..u_desccom With lcDesc
			Else
				If &lcCursorLin..desconto>100
					lcDesc = 100
					Replace &lcCursorLin..u_desccom With lcDesc
				Else
					Replace &lcCursorLin..u_desccom With 0
				Endif
			Endif
		Else
			Replace &lcCursorLin..u_desccom With 0
		Endif
	Endif

	If myTipoDoc == "BO"
		Select &lcCursorLin
		If (!Empty(&lcCursorLin..desconto) Or !Empty(&lcCursorLin..u_bonus)) And (&lcCursorLin..edebito>0 And &lcCursorLin..qtt>0)
			If Empty(&lcCursorLin..u_bonus) Or (&lcCursorLin..desconto<=100 And &lcCursorLin..u_bonus<=&lcCursorLin..qtt)
				lcDesc = (1 - (&lcCursorLin..ettdeb / (&lcCursorLin..edebito*&lcCursorLin..qtt)) ) * 100
				Select &lcCursorLin
				Replace &lcCursorLin..u_desccom With Round(lcDesc,2)
			Else
				If &lcCursorLin..desconto>100
					lcDesc = 100
					Select &lcCursorLin
					Replace &lcCursorLin..u_desccom With lcDesc
				Else
					Select &lcCursorLin
					Replace &lcCursorLin..u_desccom With 0
				Endif
			Endif
		Else
			Select &lcCursorLin
			Replace &lcCursorLin..u_desccom With 0
		Endif
	Endif
Endfunc


** Calcula Precos de Custo
Function uf_documentos_calculaUpc
	Lparameters lcCursorLin
	Local lcEvuSIva, lcU_upcSIVA
	Store 0 To lcEvuSIva, lcU_upcSIVA

	If myTipoDoc == "BO"

		If Empty(lcCursorLin)
			lcCursorLin = "BI"
		Endif

		Select &lcCursorLin
		If (&lcCursorLin..qtt > 0) And (&lcCursorLin..ettdeb > 0)
			Replace &lcCursorLin..u_upc With Round(&lcCursorLin..ettdeb / &lcCursorLin..qtt, 2)

			** Valoriza��o � sempre sem IVA
			If &lcCursorLin..ivaincl == .T.
				lcEvuSIva = (&lcCursorLin..ettdeb / &lcCursorLin..qtt)/(&lcCursorLin..IVA/100+1)
			Else
				lcEvuSIva = (&lcCursorLin..ettdeb / &lcCursorLin..qtt)
			Endif

			Replace &lcCursorLin..eslvu With lcEvuSIva

		Else
			If &lcCursorLin..ettdeb=0
				Replace &lcCursorLin..u_upc With 0
				Replace &lcCursorLin..eslvu With 0
			Endif
		Endif
	Endif

	If myTipoDoc == "FO"

		If Empty(lcCursorLin)
			lcCursorLin = "FN"
		Endif

		Select &lcCursorLin
		If &lcCursorLin..qtt > 0 And &lcCursorLin..etiliquido > 0

			&& Valoriza��o do PCL por defeito sem IVA
			If &lcCursorLin..ivaincl == .T.
				lcU_upcSIVA = (&lcCursorLin..etiliquido / &lcCursorLin..qtt)/(&lcCursorLin..IVA/100+1)
			Else
				lcU_upcSIVA = (&lcCursorLin..etiliquido / &lcCursorLin..qtt)
			Endif

			Replace &lcCursorLin..u_upc With lcU_upcSIVA

			** Valoriza��o do PCT por defeito sem IVA
			If &lcCursorLin..ivaincl == .T.
				lcEvuSIva = (&lcCursorLin..etiliquido / &lcCursorLin..qtt)/(&lcCursorLin..IVA/100+1)
			Else
				lcEvuSIva = (&lcCursorLin..etiliquido / &lcCursorLin..qtt)
			Endif

			Replace &lcCursorLin..eslvu With lcEvuSIva

		Else
			If &lcCursorLin..etiliquido=0
				Replace &lcCursorLin..u_upc With 0
				Replace &lcCursorLin..eslvu With 0
			Endif
		Endif

	Endif
Endfunc


**
Function uf_documentos_calculaPercMrgSpct
	Lparameters lcTrataIvaIncl, lcIvaInclPvp

	Local lcValor
	Store 0 To lcValor

	Select FN
	If (FN.U_PVP>0) And (FN.epv>0) && and (FN.iva!=0)
		If !Empty(lcTrataIvaIncl)

			If !Empty(lcIvaInclPvp)
				lcValor = ( ( FN.U_PVP / FN.epv / (FN.IVA/100+1) ) - 1)  * 100
			Else
				lcValor = ( ( FN.U_PVP / FN.epv ) - 1)  * 100
			Endif
		Else
			lcValor = ( ( FN.U_PVP / FN.epv / (FN.IVA/100+1) ) - 1)  * 100
		Endif
		If lcValor >= -999999 And lcValor <= 999999
			Select FN
			Replace FN.u_margpct With Round(lcValor,2)
		Else
			Select FN
			Replace FN.u_margpct With 0
		Endif
	Else
		Select FN
		Replace FN.u_margpct With 0
	Endif
Endfunc


**
Function uf_documentos_calculaPercMrgSupc
	Local lcValor
	Store 0 To lcValor

	Select FN
	If (FN.U_PVP>0) And (FN.u_upc>0) And (FN.IVA!=0)
		lcValor = ( (FN.U_PVP / FN.u_upc / (FN.IVA/100+1) ) - 1)   * 100
		If lcValor >= -999999 And lcValor <= 999999
			Replace FN.u_margupc With Round(lcValor,2)
		Else
			Replace FN.u_margupc With 0
		Endif
	Else
		Replace FN.u_margupc With 0
	Endif
Endfunc



**
Function uf_documentos_calculaPercBonusFN
	Local lcBonus
	Store 0 To lcBonus

	If !Empty(FN.u_bonus) And !Empty(FN.qtt)
		If FN.qtt>FN.u_bonus
			lcBonus = FN.u_bonus / (FN.qtt-FN.u_bonus) * 100
		Else
			lcBonus=0
		Endif
	Else
		lcBonus=0
	Endif

	Replace FN.u_bonusmrg With Round(lcBonus,2)
Endfunc



**
Function uf_documentos_calcularPrecoUni
	Local lcPrecoUni
	Store 0 To lcPrecoUni

	If myTipoDoc == "BO"
		Select Bi
		If Bi.qtt == 0 Or Bi.qtt-Bi.u_bonus = 0
			Replace Bi.ettdeb With 0
		Endif

		Select Bi
		If (Bi.qtt-Bi.u_bonus!=0)
			lcPrecoUni		= Bi.ettdeb
			If Bi.desconto>0 And Bi.desconto<100
				lcPrecoUni	= lcPrecoUni / ((100-Bi.desconto)/100)
			Endif
			If Bi.desc2>0 And Bi.desc2<100
				lcPrecoUni	= lcPrecoUni / ((100-Bi.desc2)/100)
			Endif
			If Bi.desc3>0 And Bi.desc3<100
				lcPrecoUni	= lcPrecoUni / ((100-Bi.desc3)/100)
			Endif
			If Bi.desc4>0 And Bi.desc4<100
				lcPrecoUni	= lcPrecoUni / ((100-Bi.desc4)/100)
			Endif
			lcPrecoUni		= lcPrecoUni / (Bi.qtt-Bi.u_bonus)

			If !lcPrecoUni=0
				Replace Bi.edebito With Round(lcPrecoUni,2)
				Replace Bi.eslvu		With Round(lcPrecoUni,2)
				Replace Bi.esltt		With Round((lcPrecoUni*Bi.qtt), 2)
			Endif

			uf_documentos_calculaPercDescCom()
			uf_documentos_calculaUpc()
			uf_documentos_actualizaTotaisCAB()
		Endif
	Endif

	If myTipoDoc == "FO"

		Select FN
		If FN.qtt == 0 Or FN.qtt-FN.u_bonus = 0
			Replace  FN.etiliquido With 0
		Endif

		If Alltrim(Upper(cabdoc.Doc)) == Alltrim(Upper("V/Factura Med."))
			If FN.totalant != FN.etiliquido
				Replace FN.alttotal With .T.
				Replace FN.totalant With FN.etiliquido

				If FN.epv <> 0 And FN.qtt <> 0
					uv_desc = (1-((FN.etiliquido/FN.qtt)/FN.epv))*100

					uv_desc = Round(uv_desc, 2)
					Replace FN.desconto With Iif(uv_desc < 0, 0, uv_desc)
					Replace FN.descant With FN.desconto
				Endif

			Endif
			If Empty(FN.totalant)
				Replace FN.alttotal With .T.
				Replace FN.totalant With FN.etiliquido
			Endif
			If FN.etiliquido = 0 And FN.qtt <> 0
				Replace FN.desconto With 100
				Replace FN.descant With 100
				Replace FN.alttotal With .T.
				Replace FN.totalant With FN.etiliquido
			Endif
		Endif


		If (FN.qtt-FN.u_bonus!=0)
			lcPrecoUni		= FN.etiliquido
			If FN.desconto>0 And FN.desconto<100
				lcPrecoUni	= lcPrecoUni / ((100-FN.desconto)/100)
			Endif
			If FN.desc2>0 And FN.desc2<100
				lcPrecoUni	= lcPrecoUni / ((100-FN.desc2)/100)
			Endif
			If FN.desc3>0 And FN.desc3<100
				lcPrecoUni	= lcPrecoUni / ((100-FN.desc3)/100)
			Endif
			If FN.desc4>0 And FN.desc4<100
				lcPrecoUni	= lcPrecoUni / ((100-FN.desc4)/100)
			Endif
			lcPrecoUni		= lcPrecoUni / (FN.qtt-FN.u_bonus)

			If !lcPrecoUni=0
				Select FN
				Replace FN.epv 		With Round(lcPrecoUni,2)
				Replace FN.eslvu		With Round(lcPrecoUni,2)
				Replace FN.esltt		With Round((lcPrecoUni * FN.qtt), 2)

				If FN.epv <> 0 And FN.qtt <> 0 And Upper(Alltrim(cabdoc.Doc)) <> "V/FACTURA"
					If Alltrim(Upper(cabdoc.Doc)) == Alltrim(Upper("V/Factura Med."))
						uv_desc = (1-((FN.etiliquido/FN.qtt)/FN.epv))*100

						uv_desc = Round(uv_desc, 2)
						Replace FN.desconto With Iif(uv_desc < 0, 0, uv_desc)
						Replace FN.descant With FN.desconto
					Else
						Replace FN.desconto With (Round(1-((FN.etiliquido/FN.qtt)/FN.epv),2))*100
						Replace FN.descant With FN.desconto
					Endif
				Endif

			Endif

			uf_documentos_calculaPercDescCom()
			uf_documentos_calculaUpc()
			uf_documentos_calculaPercBonusFN()
			uf_documentos_calculaPercMrgSupc()
			uf_documentos_calculaPercMrgSpct()

			uf_documentos_actualizaTotaisCAB()
		Endif

	Endif

Endfunc


**
Function uf_documentos_autocompleteRef
	Lparameters nKeyCode

	Store 0 To lcTamanhoAntigo,lcNovoTamanho

	********
	With DOCUMENTOS.PageFrame1.Page1.gridDoc
		For i=1 To .ColumnCount
			If Upper(.Columns(i).ControlSource)=="FN.REF" Or Upper(.Columns(i).ControlSource)=="BI.REF"

				** trata plicas
				.Columns(i).text1.Value = Strtran(.Columns(i).text1.Value, Chr(39), '')

				DOCUMENTOS.valorparapesquisa = Alltrim(.Columns(i).text1.Value)
				lcTamanhoAntigo = Len(Alltrim(.Columns(i).text1.Value))

				lcSQL = ""
				&&lcsql = "SELECT TOP 1 convert(nvarchar,REF) as REF FROM ST (nolock) WHERE REF LIKE '"+ Alltrim(documentos.valorparapesquisa) + "%' ORDER BY REF ASC"
				TEXT TO lcSQL NOSHOW TEXTMERGE
					SELECT
						TOP 1 convert(nvarchar,REF) as REF
					FROM
						ST (nolock)
					WHERE
						st.site_nr = <<mysite_nr>>
						and st.REF LIKE '<<Alltrim(documentos.valorparapesquisa)>>%'
					ORDER BY
						st.REF ASC
				ENDTEXT
				uf_gerais_actGrelha("","tempSQLAutoComp",lcSQL)

				If Reccount("tempSQLAutoComp") > 0

					Select tempSQLAutoComp
					lcNovoValor =Alltrim(tempSQLAutoComp.ref)
					lcNovoTamanho = Len(Alltrim(tempSQLAutoComp.ref))

					If mykeypressed != 127 And mykeypressed != 7

						.Columns(i).text1.Value = Alltrim(tempSQLAutoComp.ref)

						lc_diftamanhos = Abs(lcNovoTamanho - lcTamanhoAntigo)

						If  !Empty(lcNovoValor)
							.Columns(i).text1.SelStart = lcTamanhoAntigo
							.Columns(i).text1.SelLength = lc_diftamanhos
						Endif
					Endif
				Endif
			Endif
		Endfor
	Endwith
Endfunc


**
Function uf_documentos_SugereCriarFornecedor
	Lparameters tcBool

	If myDocAlteracao == .F. And myDocIntroducao == .F.
		Return .F.
	Endif

	Local lcNome, lcSQL

	If !Used("CabDoc")
		Return .F.
	Endif

	Select cabdoc
	If !Empty(Alltrim(cabdoc.Nome)) And !Empty(Alltrim(ucrsConfigDoc.bdempresas))

		Do Case
			Case Alltrim(ucrsConfigDoc.bdempresas) == "FL" && FORNECEDORES
				lcSQL = ""
				TEXT TO lcSQL NOSHOW TEXTMERGE
					SELECT
						NO,ESTAB,PAIS
					FROM
						FL (nolock)
					WHERE
						Upper(Rtrim(Ltrim(NOME))) = '<<Alltrim(CabDoc.nome)>>'
				ENDTEXT
				If uf_gerais_actGrelha("", "templcSQL", lcSQL)
					If Reccount("templcSQL")=0
						If uf_perguntalt_chama("O fornecedor n�o existe... Quer introduzir criar um novo fornecedor?","Sim","N�o")
							&& Valida se o ecra esta em modo de consulta
							If !(Type("gestao_fornecedores")=="U") And !(gestao_fornecedores.Caption=="Gest�o de Fornecedores")
								uf_perguntalt_chama("N�O � POSS�VEL REALIZAR ESTA AC��O PORQUE O ECR� GEST�O DE FORNECEDORES EST� EM MODO DE EDI��O. POR FAVOR TERMINE ESTA OPERA��O E TENTE NOVAMENTE","OK","",48)
								Return
							Else
								uf_forn_novo()
								forn.AlwaysOnTop = .T.

								If !Used("FL")
									Return .F.
								Endif

								Select fl
								Replace fl.Nome With Alltrim(cabdoc.Nome)
								forn.Show
								forn.Refresh

								uf_documentos_limpaNome()

							Endif
						Else
							Select cabdoc
							uf_documentos_limpaNome()
						Endif
					Else
						Select cabdoc
						**Replace cabDoc.NO 		With templcSQL.NO
						**Replace cabDoc.ESTAB 	With templcSQL.ESTAB

						** preenche restantes dados de fornecedor **
						TEXT TO lcSql NOSHOW TEXTMERGE
							exec up_gerais_dadosFornecedor <<cabDoc.NO>>, <<cabDoc.ESTAB>>
						ENDTEXT
						If !uf_gerais_actGrelha("", "uCrsDadosFlDoc", lcSQL)
							uf_perguntalt_chama("OCORREU UMA ANOMALIA AO PROCURAR DADOS DO FORNECEDOR! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
						Else
							If Reccount("uCrsDadosFlDoc") > 0
								Select uCrsDadosFlDoc
								Select cabdoc
								Replace cabdoc.MOEDA		With	Alltrim(uCrsDadosFlDoc.MOEDA)
								Replace cabdoc.NOME2		With 	Alltrim(uCrsDadosFlDoc.NOME2)
								Replace cabdoc.MORADA		With 	Alltrim(uCrsDadosFlDoc.MORADA)
								Replace cabdoc.Local		With 	Alltrim(uCrsDadosFlDoc.Local)
								Replace cabdoc.CODPOST		With 	Alltrim(uCrsDadosFlDoc.CODPOST)
								Replace cabdoc.NCONT		With 	Alltrim(uCrsDadosFlDoc.NCONT)
								Replace cabdoc.TIPO			With 	Alltrim(uCrsDadosFlDoc.TIPO)
								Replace cabdoc.EMAIL		With 	Alltrim(uCrsDadosFlDoc.EMAIL)
								Replace cabdoc.TELEFONE		With 	Alltrim(uCrsDadosFlDoc.TELEFONE)
								Replace cabdoc.XPDEMAIL		With 	Alltrim(uCrsDadosFlDoc.EMAIL)
								Replace cabdoc.XPDTELEFONE	With 	Alltrim(uCrsDadosFlDoc.TELEFONE)
								Replace cabdoc.PAIS 		With	uCrsDadosFlDoc.PAIS
								Replace cabdoc.CCUSTO 		With	uCrsDadosFlDoc.CCUSTO
								Replace cabdoc.CONDPAG 		With	Alltrim(uCrsDadosFlDoc.TPDESC)
							Endif
							fecha("uCrsDadosFlDoc")

						Endif
						**
					Endif
				Endif
			Case Alltrim(ucrsConfigDoc.bdempresas) == "AG" && ENTIDADES
				myControloDoc = 1

				lcSQL = ""
				lcSQL = "SELECT NO FROM AG (nolock) WHERE Upper(Rtrim(Ltrim(NOME))) = '" + Alltrim(cabdoc.Nome) + "'"
				If uf_gerais_actGrelha("", "templcSQL", lcSQL)
					If Reccount("templcSQL")=0
						If uf_perguntalt_chama("A Entidade n�o existe... Quer introduzir criar um nova Entidade?","Sim","N�o")

							NAVEGA("AG")

							uf_documentos_limpaNome()
						Else
							Select cabdoc
							uf_documentos_limpaNome()
						Endif
					Else
						** preenche restantes dados de entidades **
						lcSQL = ''
						TEXT To lcSql Noshow textmerge
							exec up_documentos_pesquisaEntidades '<<Alltrim(CabDoc.nome)>>', 0, '', '', ''
						ENDTEXT
						If !uf_gerais_actGrelha("", "uCrsDadosAGDoc", lcSQL)
							uf_perguntalt_chama("OCORREU UMA ANOMALIA AO PROCURAR DADOS DA ENTIDADE! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
							Return .F.
						Else
							If Reccount("uCrsDadosAGDoc")>0
								Select cabdoc
								Replace cabdoc.Nome			With	uCrsDadosAGDoc.Nome
								Replace cabdoc.NO 			With	uCrsDadosAGDoc.NO
								Replace cabdoc.MORADA		With	Alltrim(uCrsDadosAGDoc.MORADA)
								Replace cabdoc.Local		With	Alltrim(uCrsDadosAGDoc.Local)
								Replace cabdoc.CODPOST		With	Alltrim(uCrsDadosAGDoc.CODPOST)
								Replace cabdoc.NCONT		With	Alltrim(uCrsDadosAGDoc.NCONT)

								Replace cabdoc.XPDDATA		With	Ctod('1900.01.01')
								Replace	cabdoc.CCUSTO		With	''
							Endif
							fecha("uCrsDadosAGDoc")
						Endif
					Endif
				Endif

			Case Alltrim(ucrsConfigDoc.bdempresas) == "CL" && CLIENTES
				myControloDoc = 1
				lcSQL = ""
				lcSQL = "SELECT NO,ESTAB FROM b_utentes (nolock) WHERE Upper(Rtrim(Ltrim(NOME))) = '" + Alltrim(cabdoc.Nome) + "'"

				If uf_gerais_actGrelha("", "templcSQL", lcSQL)
					If Reccount("templcSQL")=0
						If uf_perguntalt_chama("O cliente n�o existe... Quer criar um novo?","Sim","N�o")

							&& Valida se o ecra esta em modo de consulta
							If !(Type("GESTAO_CLIENTES")=="U") And !(Gestao_clientes.Caption=="Gest�o de Clientes")
								uf_perguntalt_chama("N�O � POSS�VEL REALIZAR ESTA AC��O PORQUE O ECR� DE CLIENTES EST� EM MODO DE EDI��O. POR FAVOR TERMINE ESTA OPERA��O E TENTE NOVAMENTE","OK","",48)
								*RETURN
							Else

								uf_utentes_novo()
								utentes.AlwaysOnTop = .T.

								Select Cl
								Replace Cl.Nome With Alltrim(cabdoc.Nome)
								utentes.Refresh
								utentes.Show
								Return .T.
							Endif

							uf_documentos_limpaNome()

						Else
							Select cabdoc
							uf_documentos_limpaNome()
						Endif
					Else
						Select cabdoc
						Replace cabdoc.NO With templcSQL.NO
						Replace cabdoc.ESTAB With templcSQL.ESTAB

						** preenche restantes dados de clientes **
						TEXT To lcSql Noshow textmerge
							exec up_gerais_dadosCliente <<templcSQL.NO>>, <<templcSQL.ESTAB>>
						ENDTEXT
						If !uf_gerais_actGrelha("", "uCrsDadosClDoc", lcSQL)
							uf_perguntalt_chama("OCORREU UMA ANOMALIA AO PROCURAR DADOS DO CLIENTE! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
						Else
							If Reccount("uCrsDadosClDoc")>0
								Select cabdoc
								Replace cabdoc.MOEDA		With Alltrim(uCrsDadosClDoc.MOEDA)
								Replace cabdoc.NOME2		With Alltrim(uCrsDadosClDoc.NOME2)
								Replace cabdoc.MORADA		With Alltrim(uCrsDadosClDoc.MORADA)
								Replace cabdoc.Local		With Alltrim(uCrsDadosClDoc.Local)
								Replace cabdoc.CODPOST		With Alltrim(uCrsDadosClDoc.CODPOST)
								Replace cabdoc.NCONT		With Alltrim(uCrsDadosClDoc.NCONT)
								Replace cabdoc.TIPO			With Alltrim(uCrsDadosClDoc.TIPO)
								Replace cabdoc.EMAIL		With Alltrim(uCrsDadosClDoc.EMAIL)
								Replace cabdoc.TELEFONE		With Alltrim(uCrsDadosClDoc.TELEFONE)
								Replace cabdoc.XPDDATA		With Ctod('1900.01.01')
								Replace cabdoc.CCUSTO		With uCrsDadosClDoc.CCUSTO
								Replace cabdoc.CONDPAG		With uCrsDadosClDoc.TPDESC
							Endif
							fecha("uCrsDadosClDoc")
						Endif
						**********************************
					Endif
				Endif

			Otherwise
				**
		Endcase
	Else
		uf_documentos_limpaNome()
	Endif

	DOCUMENTOS.ContainerCab.Refresh
Endfunc


** Coloca campos limpos Entidade/Fornecedor/Cliente devido � mudan�a de Documento
Function uf_documentos_limpaNome
	Select cabdoc
	Replace cabdoc.Nome			With ''
	Replace cabdoc.NOME2		With ''
	Replace cabdoc.NO 			With 0
	Replace cabdoc.ESTAB 		With 0
	Replace cabdoc.MORADA		With ''
	Replace cabdoc.Local 		With ''
	Replace cabdoc.CODPOST		With ''
	Replace cabdoc.NCONT 		With ''
	Replace cabdoc.TIPO			With ''
	Replace cabdoc.EMAIL		With ''
	Replace cabdoc.TELEFONE		With ''
	Replace cabdoc.CCUSTO		With ''
	Replace cabdoc.NRRECEITA	With ''
	DOCUMENTOS.ContainerCab.Refresh
Endfunc


**
Function uf_documentos_navegaconfigdoc
	Local lcSQL

	If !Empty(DOCUMENTOS.ContainerCab.documento.Value)
		TEXT TO lcSQL TEXTMERGE NOSHOW
			exec up_documentos_configDoc '<<ALLTRIM(DOCUMENTOS.ContainerCab.documento.value)>>', '<<ALLTRIM(mySite)>>'
		ENDTEXT
		If uf_gerais_actGrelha("", "ucrsNavegaConfigDoc", lcSQL)
			NAVEGA(Alltrim(ucrsNavegaConfigDoc.ECRAN),Alltrim(ucrsNavegaConfigDoc.STAMP))
			fecha("ucrsNavegaConfigDoc")
		Endif
	Endif
Endfunc


**
Function uf_documentos_controlaPass
	Lparameters tcBool, tcBool2, lcPainel

	If Empty(lcPainel)
		lcPainel = "DOCUMENTOS"
	Endif


	Public myVirtualText, myVirtualVar, myPassWordChar

	Local lcPass, cval, lcVal
	Store '' To lcPass, cval
	Store .F. To lcVal

	If tcBool == .F. And tcBool2 == .F. && password supervisor

		If uf_gerais_getParameter("ADM0000000076","BOOL") == .T.
			** guardar pass **
			If !Empty(uf_gerais_getParameter("ADM0000000076","TEXT"))
				lcPass = uf_gerais_getParameter("ADM0000000076","TEXT")
			Endif

			** pedir password **
			DOCUMENTOS.Pass.Value = ''

			**chama Painel Virtual, funcoes genericas
			uf_tecladoalpha_chama('DOCUMENTOS.pass','INTRODUZA PASSWORD DE SUPERVISOR DE DOCUMENTOS:',.T.,.F.,1)

			cval=Alltrim(DOCUMENTOS.Pass.Value)

			Do Case
				Case Empty(cval) Or Empty(lcPass)
					uf_perguntalt_chama("DEVE INTRODUZIR A PASSWORD DE SUPERVISOR PARA REALIZAR ESTA OPERA��O.","OK","",48)
				Case !(Upper(Alltrim(lcPass))==Upper(Alltrim(cval)))
					uf_perguntalt_chama("A PASSWORD N�O EST� CORRECTA. POR FAVOR VERIFIQUE SE EST� A INTRODUZIR CORRECTAMENTE.","OK","",16)
				Otherwise
					lcVal = .T.
			Endcase
		Else
			lcVal = .T.
		Endif

	Endif

	If tcBool == .T. && Painel de Gest�o de Documentos ou de Encomendas Autom�ticas (Controlo de Plafond)

		If uf_gerais_getParameter("ADM0000000109","BOOL") == .T.
			** guardar pass **
			If !Empty(uf_gerais_getParameter("ADM0000000109","TEXT"))
				lcPass = uf_gerais_getParameter("ADM0000000109","TEXT")
			Endif

			** pedir password **
			If !tcBool2
				DOCUMENTOS.Pass.Value = ''

				**chama Painel Virtual, funcoes genericas
				&&uf_tecladoVirtual_Chama('DOCUMENTOS.pass','INTRODUZA PASSWORD DE SUPERVISOR DE DOCUMENTOS:',.t.,.f.,1)
				uf_tecladoalpha_chama(Alltrim(lcPainel) + '.pass','INTRODUZA PASSWORD DE SUPERVISOR DE DOCUMENTOS:',.T.,.F.,1)
				cval=Alltrim(&lcPainel..Pass.Value)
			Else
				&lcPainel..Pass.Value = ''

				**chama Painel Virtual, funcoes genericas
				&&uf_tecladoVirtual_Chama('DOCUMENTOS.pass','INTRODUZA PASSWORD DE SUPERVISOR PARA ULTRAPASSAR PLAFOND:',.t.,.f.,1)
				uf_tecladoalpha_chama(Alltrim(lcPainel) + '.pass','INTRODUZA PASSWORD DE SUPERVISOR PARA ULTRAPASSAR PLAFOND:',.T.,.F.,1)

				cval=Alltrim(&lcPainel..Pass.Value)
			Endif

			Do Case
				Case Empty(cval) Or Empty(lcPass)
					uf_perguntalt_chama("DEVE INTRODUZIR A PASSWORD DE SUPERVISOR PARA REALIZAR ESTA OPERA��O.","OK","",48)
				Case !(Upper(Alltrim(lcPass))==Upper(Alltrim(cval)))
					uf_perguntalt_chama("A PASSWORD N�O EST� CORRECTA. POR FAVOR VERIFIQUE SE EST� A INTRODUZIR CORRECTAMENTE.","OK","",48)
				Otherwise
					lcVal = .T.
			Endcase
		Else
			lcVal = .T.
		Endif

	Endif

	Return lcVal
Endfunc


**Desconto Comercial
Function uf_documentos_aplicaDescComercial

	If Empty(myTipoDoc) == .T. Or Empty(Alltrim(DOCUMENTOS.ContainerCab.documento.Value)) == .T. Or;
			(myDocAlteracao == .F. And myDocIntroducao==.F.)
		Return .F.
	Endif

	Local lcPos2, lcDesc, lcValida
	Store 0 To lcPos2, lcDesc
	Store .F. To lcValida

	If myTipoDoc = "FO"
		Select FN
		lcPos2 = Recno()
		Go Top
		Scan
			If !Empty(FN.desconto)
				lcValida=.T.
			Endif
		Endscan

		If lcValida
			If !uf_perguntalt_chama("Aten��o! Este documento j� tem descontos aplicados, pretende continuar?","Sim","N�o")
				Select FN
				If lcPos2>0
					Try
						Go lcPos2
					Catch
					Endtry
				Endif
				Return
			Endif
		Endif

		Public myDescCom
		uf_tecladonumerico_chama("myDescCom", "", 0, .T., .F., 6, 2)
		lcDesc = myDescCom

		If Type("lcDesc") != "N"
			Return .F.
		Endif

		Select FN
		Go Top
		Scan
			If FN.qtt>0 And FN.epv>0
				Replace FN.desconto With Round(lcDesc,2)
				uf_documentos_recalculaTotaisFN()
			Endif

			Select FN
		Endscan

		Select FN
		Go Top
		uf_documentos_actualizaTotaisCAB()
		Select FN
		If lcPos2>0
			Try
				Go lcPos2
			Catch
			Endtry
		Endif
	Endif


	If myTipoDoc = "BO"
		Select Bi
		lcPos2 = Recno()
		Go Top
		Scan
			If !Empty(Bi.desconto)
				lcValida=.T.
			Endif
		Endscan

		If lcValida
			If  !uf_perguntalt_chama("Aten��o! Este documento j� tem descontos aplicados, pretende continuar?","Sim","N�o")
				Select Bi
				If lcPos2>0
					Try
						Go lcPos2
					Catch
					Endtry
				Endif
				Return
			Endif
		Endif

		lcDesc = Inputbox('Qual o Desconto Pretendido:','','0.00')

		Select Bi
		Go Top
		Scan
			If Bi.qtt>0 And Bi.edebito>0
				Replace Bi.desconto With Val(ASTR(lcDesc,8,2))
				uf_documentos_recalculaTotaisBI()
			Endif

			Select Bi
		Endscan

		Select Bi
		Go Top
		uf_documentos_actualizaTotaisCAB()
		Select Bi
		If lcPos2>0
			Try
				Go lcPos2
			Catch
			Endtry
		Endif
	Endif
Endfunc


**
Function uf_documentos_selDataTransporte
	Public myVirtualVarD
	myVirtualVarD = 'DOCUMENTOS.PageFrame1.Page1.detalhest1.pageFrame1.page11.ContainerOutrosDados.xpddata'
	uf_chamaGetDatePresc()
Endfunc


**
Function uf_documentos_actCCusto

	If myDocAlteracao == .F. And myDocIntroducao == .F.
		uf_perguntalt_chama("PARA UTILIZAR ESTA OP��O DEVE COLOCAR O DOCUMENTO EM EDI��O.","OK","",64)
		Return .F.
	Endif

	If Alltrim(myTipoDoc)= "BO"
		If Used("bi") And Used("CABDOC")
			Select Bi
			Go Top
			Scan
				Replace Bi.CCUSTO With cabdoc.CCUSTO
			Endscan
			Select Bi
			Go Top

			uf_perguntalt_chama("LINHAS ACTUALIZADAS COM SUCESSO.","OK","",64)
		Endif
	Endif

	If Alltrim(myTipoDoc)= "FO"
		If Used("fn") And Used("CABDOC")
			Select FN
			Go Top
			Scan
				Replace FN.fnccusto With cabdoc.CCUSTO
			Endscan
			Select FN
			Go Top

			uf_perguntalt_chama("LINHAS ACTUALIZADAS COM SUCESSO.","OK","",64)
		Endif
	Endif
Endfunc


**
Function uf_documentos_CalculaArredondamentos
	If myTipoDoc == "FO" And !Empty(Alltrim(cabdoc.cabstamp))
		Select cabdoc
		lcSQL = ""
		TEXT TO lcSQL NOSHOW TEXTMERGE
			SELECT
				arredondamento = EAIVAIN + EAIVAV1 + EAIVAV2 + EAIVAV3 + EAIVAV4 + EAIVAV5 + EAIVAV6 + EAIVAV7 + EAIVAV8 + EAIVAV9
			FROM
				FO (nolock)
			WHERE
				fostamp = '<<ALLTRIM(CabDoc.cabstamp)>>'
		ENDTEXT
		If !uf_gerais_actGrelha("", "ucrsArredondamentoTotal", lcSQL)
			uf_perguntalt_chama("OCORREU UMA ANOMALIA NA VERIFICA��O DE ARREDONDAMENTOS. POR FAVOR CONTACTE O SUPORTE","OK","",16)
			Return .F.
		Endif
	Endif
Endfunc


**
Function uf_documentos_CalculaTotais
	Lparameters lcValorDescFin
	If Empty(lcValorDescFin)
		lcValorDescFin = 0
	Endif

	Local lcTotalArredondamentos, lcTotalAposArredondamentos, lcCabStamp
	Store 0 To lcTotalArredondamentos, lcTotalAposArredondamentos

	If !Used("ucrsTotaisDocumento")
		Return .F.
	Endif

	Select * From ucrsTotaisDocumento Into Cursor ucrsTotaisDocumentoAux Readwrite

	Select ucrsTotaisDocumento
	Go Top
	Scan
		Delete
	Endscan

	Do Case
		Case myTipoDoc == "BO"

			Select;
				cabdoc.cabstamp As STAMP;
				,Bi.IVA As taxa;
				,Sum(Iif(Bi.ivaincl,(Bi.ettdeb- (Bi.ettdeb/(Bi.IVA/100+1))),(Bi.ettdeb*(Bi.IVA/100)))) As valorIvaOriginal;
				,Sum(Iif(Bi.ivaincl,(Bi.ettdeb- (Bi.ettdeb/(Bi.IVA/100+1))),(Bi.ettdeb*(Bi.IVA/100)))) As valorIva;
				,Sum(Iif(Bi.ivaincl,(Bi.ettdeb/(Bi.IVA/100+1)),(Bi.ettdeb))) As BASEINC;
				,0000000000.00 As arredondamento;
				,0000000000.00 As valoraposarredondamento;
				,Bi.tabiva;
				FROM;
				Bi;
				GROUP By;
				Bi.IVA;
				,Bi.tabiva;
				INTO Cursor ucrsTotaisDocumento Readwrite

		Case myTipoDoc == "FO"

			Select;
				cabdoc.cabstamp As STAMP;
				,FN.IVA As taxa;
				,Sum(Iif(FN.ivaincl,(Round(FN.etiliquido, 2) - (Round(FN.etiliquido, 2)/(FN.IVA/100+1))),(Round(FN.etiliquido, 2)*(FN.IVA/100)))) As valorIvaOriginal;
				,Sum(Iif(FN.ivaincl,(Round(FN.etiliquido, 2) - (Round(FN.etiliquido, 2)/(FN.IVA/100+1))),(Round(FN.etiliquido, 2)*(FN.IVA/100)))) As valorIva;
				,Sum(Iif(FN.ivaincl,(FN.etiliquido/(FN.IVA/100+1)),(FN.etiliquido))) As BASEINC;
				,0000000000.00 As arredondamento;
				,0000000000.00 As valoraposarredondamento;
				,0 As tabiva;
				FROM;
				FN;
				GROUP By;
				FN.IVA;
				INTO Cursor ucrsTotaisDocumento Readwrite

			If Upper(Alltrim(cabdoc.Doc)) == "V/FACTURA RESUMO"

				uv_stamps = ''

				Select FN
				Go Top
				Scan For Not Empty(FN.ofnstamp)

					uv_stamps = uv_stamps + Iif( !Empty(uv_stamps), ';', '') + Alltrim(FN.ofnstamp)

					Select FN
				Endscan

				If !Empty(uv_stamps)

					TEXT TO msel TEXTMERGE NOSHOW

                        EXEC up_documentos_calcTotsResumo '<<ALLTRIM(uv_stamps)>>'

					ENDTEXT

					Select cabdoc
					uv_curCab = cabdoc.cabstamp

					uf_gerais_actGrelha("", "uc_tots", msel)

					Select uv_curCab As STAMP, * From uc_tots With (Buffering = .T.) Into Cursor ucrsTotaisDocumento Readwrite

					Select;
						cabdoc.cabstamp As STAMP;
						,FN.IVA As taxa;
						,Sum(Iif(FN.ivaincl,(Round(FN.etiliquido, 2) - (Round(FN.etiliquido, 2)/(FN.IVA/100+1))),(Round(FN.etiliquido, 2)*(FN.IVA/100)))) As valorIvaOriginal;
						,Sum(Iif(FN.ivaincl,(Round(FN.etiliquido, 2) - (Round(FN.etiliquido, 2)/(FN.IVA/100+1))),(Round(FN.etiliquido, 2)*(FN.IVA/100)))) As valorIva;
						,Sum(Iif(FN.ivaincl,(FN.etiliquido/(FN.IVA/100+1)),(FN.etiliquido))) As BASEINC;
						,0000000000.00 As arredondamento;
						,0000000000.00 As valoraposarredondamento;
						,FN.tabiva;
						FROM;
						FN;
						WHERE;
						EMPTY(FN.ofnstamp);
						GROUP By;
						FN.IVA, FN.tabiva;
						INTO Cursor uc_totManual Readwrite

					Select uc_totManual
					Go Top
					Scan

						Select ucrsTotaisDocumento
						Locate For ucrsTotaisDocumento.taxa = uc_totManual.taxa

						If Found()

							Select ucrsTotaisDocumento
							Replace ucrsTotaisDocumento.valorIvaOriginal With ucrsTotaisDocumento.valorIvaOriginal + uc_totManual.valorIvaOriginal,;
								ucrsTotaisDocumento.valorIva With ucrsTotaisDocumento.valorIva + uc_totManual.valorIva,;
								ucrsTotaisDocumento.BASEINC With ucrsTotaisDocumento.BASEINC + uc_totManual.BASEINC

						Else

							Select ucrsTotaisDocumento
							Append Blank

							Replace ucrsTotaisDocumento.STAMP With uc_totManual.STAMP,;
								ucrsTotaisDocumento.taxa With uc_totManual.taxa,;
								ucrsTotaisDocumento.valorIvaOriginal With uc_totManual.valorIvaOriginal,;
								ucrsTotaisDocumento.valorIva With uc_totManual.valorIva,;
								ucrsTotaisDocumento.BASEINC With uc_totManual.BASEINC,;
								ucrsTotaisDocumento.arredondamento With uc_totManual.arredondamento,;
								ucrsTotaisDocumento.valoraposarredondamento With uc_totManual.valoraposarredondamento,;
								ucrsTotaisDocumento.tabiva With uc_totManual.tabiva


						Endif


						Select uc_totManual
					Endscan

				Endif

				Create Cursor uc_baseIva (BASEINC1 N(14,2), BASEINC2 N(14,2), BASEINC3 N(14,2), BASEINC4 N(14,2), BASEINC5 N(14,2), BASEINC6 N(14,2), BASEINC7 N(14,2), BASEINC8 N(14,2), BASEINC9 N(14,2))

				Select uc_baseIva
				Append Blank

				Select ucrsTotaisDocumento

				Go Top
				Scan

					Do Case
						Case ucrsTotaisDocumento.tabiva = 1

							Select uc_baseIva
							Replace uc_baseIva.BASEINC1 With uc_baseIva.BASEINC1 + ucrsTotaisDocumento.BASEINC

						Case ucrsTotaisDocumento.tabiva = 2

							Select uc_baseIva
							Replace uc_baseIva.BASEINC2 With uc_baseIva.BASEINC2 + ucrsTotaisDocumento.BASEINC

						Case ucrsTotaisDocumento.tabiva = 3

							Select uc_baseIva
							Replace uc_baseIva.BASEINC3 With uc_baseIva.BASEINC3 + ucrsTotaisDocumento.BASEINC

						Case ucrsTotaisDocumento.tabiva = 4

							Select uc_baseIva
							Replace uc_baseIva.BASEINC4 With uc_baseIva.BASEINC4 + ucrsTotaisDocumento.BASEINC

						Case ucrsTotaisDocumento.tabiva = 5

							Select uc_baseIva
							Replace uc_baseIva.BASEINC5 With uc_baseIva.BASEINC5 + ucrsTotaisDocumento.BASEINC

						Case ucrsTotaisDocumento.tabiva = 6

							Select uc_baseIva
							Replace uc_baseIva.BASEINC6 With uc_baseIva.BASEINC6 + ucrsTotaisDocumento.BASEINC

						Case ucrsTotaisDocumento.tabiva = 7

							Select uc_baseIva
							Replace uc_baseIva.BASEINC7 With uc_baseIva.BASEINC7 + ucrsTotaisDocumento.BASEINC

						Case ucrsTotaisDocumento.tabiva = 8

							Select uc_baseIva
							Replace uc_baseIva.BASEINC8 With uc_baseIva.BASEINC8 + ucrsTotaisDocumento.BASEINC

						Case ucrsTotaisDocumento.tabiva = 9

							Select uc_baseIva
							Replace uc_baseIva.BASEINC9 With uc_baseIva.BASEINC9 + ucrsTotaisDocumento.BASEINC

					Endcase

					Select ucrsTotaisDocumento
				Endscan

			Endif

			If Upper(Alltrim(cabdoc.Doc)) <> "V/FACTURA RESUMO"

				** Refletir Desconto finaceiro na BI
				Update ucrsTotaisDocumento Set BASEINC = (BASEINC - (BASEINC * (cabdoc.DESCFIN/100)))

				** Refletir Desconto finaceiro na o IVA
				Update ucrsTotaisDocumento Set valorIvaOriginal = (valorIvaOriginal - (valorIvaOriginal* (cabdoc.DESCFIN/100)))
				Update ucrsTotaisDocumento Set valorIva = (valorIva - (valorIva* (cabdoc.DESCFIN/100)))

			Endif

	Endcase

	lcObjParent = "DOCUMENTOS.PageFrame1.Page1.detalhest1.Pageframe1.page10.containerTotais"

	If Used("ucrsTotaisDocumento")

		&& Limpa dados anteriores
		For i = 1 To 4
			lcObj = lcObjParent + ".taxa" + ASTR(i)
			&lcObj..Value = ""

			lcObj = lcObjParent + ".bi" + ASTR(i)
			&lcObj..Value = ""

			lcObj = lcObjParent + ".valorIva" + ASTR(i)
			&lcObj..Value = ""

			lcObj = lcObjParent + ".Arredondamento" + ASTR(i)
			&lcObj..Value = ""

			lcObj = lcObjParent + ".apa" + ASTR(i)
			&lcObj..Value = ""
			&lcObj..ReadOnly = .T.
		Endfor
		lcObj= lcObjParent + ".Arredondamentot"
		&lcObj..Value = ""

		lcObj= lcObjParent + ".apat"
		&lcObj..Value = ""
		***

		lci = 0
		Select ucrsTotaisDocumento
		Go Top
		Scan
			lci = lci + 1
			If lci < 5

				lcObj= lcObjParent + ".taxa" + ASTR(lci)
				&lcObj..Value = Round(ucrsTotaisDocumento.taxa,0)

				lcObj= lcObjParent + ".bi" + ASTR(lci)
				lcBi = Round(ucrsTotaisDocumento.BASEINC,2)
				&lcObj..Value = lcBi

				lcObj= lcObjParent + ".valorIva" + ASTR(lci)
				lcValorIva = Round(ucrsTotaisDocumento.valorIva,2)
				&lcObj..Value = lcValorIva

				If Upper(Alltrim(myTipoDoc)) == "FO"

					If myDocIntroducao == .T. Or myDocAlteracao == .T.

						&& Caso seja feita alguma altera��o aos totais do documento � feito reset aos arredondamentos
						lcArredondamento = 0
						lcObj = lcObjParent + ".Arredondamento" + ASTR(lci)
						&lcObj..Value = lcArredondamento

					Else
						**
						Select ucrsTotaisDocumentoAux
						Locate For 	ucrsTotaisDocumento.taxa = ucrsTotaisDocumentoAux.taxa
						If Found()
							Replace ucrsTotaisDocumento.arredondamento With ucrsTotaisDocumentoAux.arredondamento
						Endif

						lcObj = lcObjParent + ".Arredondamento" + ASTR(lci)
						lcArredondamento = Round(ucrsTotaisDocumento.arredondamento,2)
						&lcObj..Value = lcArredondamento
					Endif


					**
					lcObj= lcObjParent + ".valorIva" + ASTR(lci)
					lcValorIva = Round(ucrsTotaisDocumento.valorIvaOriginal,2) + lcArredondamento
					&lcObj..Value = lcValorIva

					Select ucrsTotaisDocumento
					Replace ucrsTotaisDocumento.valorIva With lcValorIva

					**
					lcObj= lcObjParent + ".apa" + ASTR(lci)
					lcValorAposArredondamento = Round(lcValorIva,2) + Round(ucrsTotaisDocumento.BASEINC,2)
					&lcObj..Value = lcValorAposArredondamento


					If lcValorAposArredondamento != 0
						&lcObj..ReadOnly = .F.
					Endif

					lcTotalArredondamentos = lcTotalArredondamentos + lcArredondamento
					lcTotalAposArredondamentos = lcTotalAposArredondamentos + lcValorAposArredondamento
				Endif

			Endif
		Endscan

	Endif

	Select cabdoc
	lcObj= lcObjParent + ".bit"
	If Upper(Alltrim(cabdoc.Doc)) == "V/FACTURA RESUMO"
		Select Sum(Iif(At('V/Nt. Cr�dito', FN.Design) = 0, FN.epv, FN.epv * -1)) As totPCT From FN With (Buffering = .T.)  Into Cursor uc_totPCT

		Select cabdoc
		Replace cabdoc.BASEINC With Iif(Isnull(Round(uc_totPCT.totPCT,2)), 0, Round(uc_totPCT.totPCT,2))



		&lcObj..Value = Iif(Isnull(Round(uc_totPCT.totPCT,2)), 0, Round(uc_totPCT.totPCT,2))
	Else
		&lcObj..Value = Round(cabdoc.BASEINC,2)
	Endif



	Select ucrsTotaisDocumento
	Calculate Sum(ucrsTotaisDocumento.valorIva) To lcValorIvaTotal

	lcObj = lcObjParent + ".valorIvat"
	&lcObj..Value = Round(lcValorIvaTotal,2)

	If Upper(Alltrim(myTipoDoc)) == "FO"
		lcObj= lcObjParent + ".Arredondamentot"
		&lcObj..Value = lcTotalArredondamentos

		lcObj= lcObjParent + ".apat"
		&lcObj..Value = lcTotalAposArredondamentos

		Select cabdoc
		Replace cabdoc.Total With Round(lcTotalAposArredondamentos,2)
		Replace cabdoc.IVA With Round(lcValorIvaTotal,2)

		If Upper(Alltrim(cabdoc.Doc)) == "V/FACTURA RESUMO"

			Select cabdoc
			Replace cabdoc.Total With cabdoc.Total - (cabdoc.Total * (cabdoc.DESCFIN/100))

			&lcObjParent..Total.Value = cabdoc.Total
			DOCUMENTOS.PageFrame1.Page1.ContainerLinhas.Total.Value = cabdoc.Total

		Endif

	Endif

Endfunc



**
Function uf_documentos_ArredondaIva
	Lparameters lcTab
	**MESSAGEBOX('Arredonda IVA')
	**
	If myDocIntroducao == .F. And myDocAlteracao == .F.
		Return .F.
	Endif

	Local lcTotalAposArredondamentos, lcTotalArredondamentos, lcArredondamento, lcValorAposArredondamento, lcValorDescFin
	Store 0.00 To lcTotalAposArredondamentos, lcTotalArredondamentos, lcArredondamento, lcValorAposArredondamento, lcValorDescFin


	lci = Right(Alltrim(lcTab),1)


	lcObjParent = "DOCUMENTOS.PageFrame1.Page1.detalhest1.Pageframe1.page10.containerTotais"
	lcObjTaxa = lcObjParent + ".taxa" + lci
	lcObjBI = lcObjParent + ".bi" + lci
	lcObjValorIva = lcObjParent + ".valorIva" + lci
	lcObjArredondamento = lcObjParent + ".Arredondamento" + lci
	lcObjAposArredondamento = lcObjParent + ".apa" + lci


	lcTaxa = &lcObjTaxa..Value

	If Empty(lcTaxa)
		Return .F.
	Endif

	Do Case
		Case myTipoDoc == "FO"


			Select;
				cabdoc.cabstamp As STAMP;
				,FN.IVA As taxa;
				,Round(Sum(Iif(FN.ivaincl,(FN.etiliquido - (FN.etiliquido/(FN.IVA/100+1))),(FN.etiliquido*(FN.IVA/100)))),2) As valorIvaOriginal;
				,Round(Sum(Iif(FN.ivaincl,(FN.etiliquido - (FN.etiliquido/(FN.IVA/100+1))),(FN.etiliquido*(FN.IVA/100)))),2) As valorIva;
				,Round(Sum(Iif(FN.ivaincl,(FN.etiliquido/(FN.IVA/100+1)),(FN.etiliquido))),2) As BASEINC;
				,0000000000.00 As arredondamento;
				,0000000000.00 As valoraposarredondamento;
				FROM;
				FN;
				WHERE;
				FN.IVA = lcTaxa;
				GROUP By;
				FN.IVA;
				INTO Cursor ucrsTotaisDocumentoAux Readwrite


			** Refletir Desconto finaceiro na BI
			Update ucrsTotaisDocumentoAux Set BASEINC = Round((BASEINC - (BASEINC * (cabdoc.DESCFIN/100))),2)
			** Refletir Desconto finaceiro na o IVA
			Update ucrsTotaisDocumentoAux Set valorIvaOriginal = Round((valorIvaOriginal - (valorIvaOriginal* (cabdoc.DESCFIN/100))),2)
			Update ucrsTotaisDocumentoAux Set valorIva = Round((valorIva - (valorIva* (cabdoc.DESCFIN/100))),2)

		Case myTipoDoc == "BO"

			Select;
				cabdoc.cabstamp As STAMP;
				,Bi.IVA As taxa;
				,Round(Sum(Iif(Bi.ivaincl,(Bi.ettdeb- (Bi.ettdeb/(Bi.IVA/100+1))),(Bi.ettdeb*(Bi.IVA/100)))),2) As valorIvaOriginal;
				,Round(Sum(Iif(Bi.ivaincl,(Bi.ettdeb- (Bi.ettdeb/(Bi.IVA/100+1))),(Bi.ettdeb*(Bi.IVA/100)))),2) As valorIva;
				,Round(Sum(Iif(Bi.ivaincl,(Bi.ettdeb/(Bi.IVA/100+1)),(Bi.ettdeb))),2) As BASEINC;
				,0000000000.00 As arredondamento;
				,0000000000.00 As valoraposarredondamento;
				FROM;
				Bi;
				WHERE;
				FN.IVA = lcTaxa;
				GROUP By;
				Bi.IVA;
				INTO Cursor ucrsTotaisDocumentoAux Readwrite

		Otherwise
			Return .F.
	Endcase

	** Reset as Variaveis
	&lcObjValorIva..Value = ucrsTotaisDocumentoAux.valorIva
	&lcObjArredondamento..Value = Round(&lcObjAposArredondamento..Value - (&lcObjBI..Value + &lcObjValorIva..Value),2)
	&lcObjValorIva..Value = Round(&lcObjValorIva..Value + &lcObjArredondamento..Value,2)


	** Actualiza Cursor
	Select ucrsTotaisDocumento
	Go Top
	Scan
		Delete
	Endscan

	For i = 1 To 4

		lcObj = lcObjParent + ".bi" + ASTR(i)
		lcObj2 = lcObjParent + ".taxa" + ASTR(i)
		If !Empty(&lcObj..Value) Or !Empty(&lcObj2..Value)

			lcObj = lcObjParent + ".taxa" + ASTR(i)
			Select ucrsTotaisDocumento
			Append Blank
			Replace ucrsTotaisDocumento.STAMP With cabdoc.cabstamp
			Replace ucrsTotaisDocumento.taxa With &lcObj..Value

			lcObj = lcObjParent + ".bi" + ASTR(i)
			If !Empty(&lcObj..Value)
				Replace ucrsTotaisDocumento.BASEINC With &lcObj..Value
			Endif

			lcObj = lcObjParent + ".valorIva" + ASTR(i)
			If !Empty(&lcObj..Value)
				Replace ucrsTotaisDocumento.valorIva With &lcObj..Value
			Endif

			lcObj = lcObjParent + ".Arredondamento" + ASTR(i)
			If !Empty(&lcObj..Value)
				Replace ucrsTotaisDocumento.arredondamento With &lcObj..Value
				lcArredondamento = &lcObj..Value
				lcTotalArredondamentos = lcTotalArredondamentos + lcArredondamento
			Endif

			lcObj = lcObjParent + ".apa" + ASTR(i)
			If !Empty(&lcObj..Value)
				Replace ucrsTotaisDocumento.valoraposarredondamento With &lcObj..Value
				lcValorAposArredondamento = &lcObj..Value
				lcTotalAposArredondamentos = lcTotalAposArredondamentos + lcValorAposArredondamento
			Endif

		Endif
	Endfor


	Select cabdoc
	lcObj= lcObjParent + ".bit"
	&lcObj..Value = Round(cabdoc.BASEINC,2)

	Select ucrsTotaisDocumento
	Calculate Sum(ucrsTotaisDocumento.valorIva) To lcValorIvaTotal

	lcObj= lcObjParent + ".valorIvat"
	&lcObj..Value = Round(lcValorIvaTotal,2)


	If Upper(Alltrim(myTipoDoc)) == "FO"
		lcObj= lcObjParent + ".Arredondamentot"
		&lcObj..Value = lcTotalArredondamentos

		lcObj= lcObjParent + ".apat"
		&lcObj..Value = lcTotalAposArredondamentos

		Select cabdoc
		Replace cabdoc.Total With Round(lcTotalAposArredondamentos,2)
		Replace cabdoc.IVA With Round(lcValorIvaTotal,2)
	Endif

	DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.page10.Refresh
	DOCUMENTOS.Refresh


Endfunc


**
Function uf_documentos_importarArtigosPorIva
	Local lcTabIva, lcSQL

	Set Point To "."
	If  myDocIntroducao == .F. And  myDocAlteracao == .F.
		uf_perguntalt_chama("� NECESS�RIO QUE O ECR� DE DOCUMENTOS ESTEJA EM MODO DE EDI��O PARA UTILIZAR ESTA OP��O. POR FAVOR VERIFIQUE.","OK","",64)
		Return .F.
	Endif

	If Used("cabDoc")
		Select cabdoc
		If Alltrim(cabdoc.Doc) !="Imprimir Etiquetas"
			uf_perguntalt_chama("APENAS � POSS�VEL UTILIZAR ESTA FUN��O NO DOCUMENTO IMPRIMIR ETIQUETAS. POR FAVOR VERIFIQUE.","OK","",64)
			Return
		Endif

		lcTabIva = Inputbox('Indique a Tabela de Iva dos Artigos a Importar.','','')
		If !Empty(lcTabIva)
			lcSQL = ""
			TEXT TO lcSQL TEXTMERGE NOSHOW
				Select 	Ref, Design,epv1
				From	St (nolock)
				Where	St.tabIva = '<<Alltrim(lctabIva)>>'
			ENDTEXT
			If !uf_gerais_actGrelha("", "uc_ImportArtTabIvaDoc", lcSQL)
				uf_perguntalt_chama("N�O FOI POSS�VEL ENCONTRAR ARTIGOS PERTENCENTES � TABELA DE IVA INDICADA. POR FAVOR VERIFIQUE.","OK","",64)
				Return .F.
			Endif

			* a vari�vel guarda o total de registos do cursor tempcursor
			Select uc_ImportArtTabIvaDoc
			mntotal=Reccount("uc_ImportArtTabIvaDoc")

			* inicializa a r�gua apresentando um t�tulo e o n� total de registos
			regua(0,mntotal,"A PROCESSAR...",.F.)
			Select uc_ImportArtTabIvaDoc
			Go Top
			Scan

				** actualizar regua **
				regua[1,recno("uc_ImportArtTabIvaDoc"),"",.f.]
				Select Bi
				uf_documentos_criaNovaLinha(.F.,.T.)

				Select Bi
				Replace Bi.ref 		With 	Alltrim(uc_ImportArtTabIvaDoc.ref)
				Replace Bi.Design 	With 	Alltrim(uc_ImportArtTabIvaDoc.Design)
				Replace Bi.qtt 		With 	1
				Replace Bi.edebito 	With 	uc_ImportArtTabIvaDoc.EPV1

				uf_documentos_recalculaTotaisBI()
			Endscan

			uf_documentos_actualizaTotaisCAB()

			regua(2)
		Else
			uf_perguntalt_chama("PARA USAR ESTA OP��O, � NECESS�RIO INDICAR A TABELA DE IVA. POR FAVOR VERIFIQUE.","OK","",64)
		Endif
	Endif
Endfunc


**
Function uf_Documentos_ProdEncNaoRecDoc

	DOCUMENTOS.WindowState=1

	uf_analises_chama()

	Select uCrsAnalisesGrupo
	Locate For Alltrim(Upper(uCrsAnalisesGrupo.grupo)) == "FORNECEDORES"
	Analises.containerGrupo.gridGrupo.SetFocus
	Select uCrsAnalisesSubGrupo
	Locate For Alltrim(Upper(uCrsAnalisesSubGrupo.SubGrupo)) == "CONFER�NCIA"
	Analises.containerGrupo.gridSubGrupo.SetFocus
	Select ucrsAnalisesDescricao
	Locate For Alltrim(Upper(ucrsAnalisesDescricao.descricao)) == "RELAT�RIO COMPARATIVO DA QT. ENCOMENDA VS. QT. RECEPCIONADA"
	Analises.containerGrupo.gridDescricao.SetFocus
	uf_analises_gravar()

	uf_opcoesdiversasDoc_sair()
Endfunc


**
Function uf_Documentos_ImprimeTalaoPOSReservaIN
	Public myDocAlteracao, myDocIntroducao
	If 	myDocAlteracao == .T. Or myDocIntroducao == .T.
		uf_perguntalt_chama("N�O � POSSIVEL UTILIZAR ESTA OP��O COM O DOCUMENTO EM EDI��O.","OK","",64)
		Return .F.
	Endif

	If Used("cabdoc")
		Select cabdoc
		If cabdoc.NUMINTERNODOC == 5 Or cabdoc.NUMINTERNODOC == 55 Or cabdoc.NUMINTERNODOC  == 101 Or cabdoc.NUMINTERNODOC == 58
			If cabdoc.NUMINTERNODOC  == 55 Or cabdoc.NUMINTERNODOC  == 101 Or cabdoc.NUMINTERNODOC == 58 && V/ Factura e V/ Guia de Transporte
				uf_documentos_verificaImpReserva()
			Else && Reserva

				**IF uf_gerais_getParameter_site('ADM0000000136', 'BOOL', mySite)
				If uf_gerais_getUmvalor("B_terminal", "usaDriverWin", "terminal = '" + Alltrim(myTerm) + "'")

					uf_gerais_actGrelha("","Reservaprint", "exec up_print_reserva '', '" + Alltrim(cabdoc.cabstamp) + "'")
					uf_gerais_actGrelha("","DOCPRINTRODAPERESERVA","exec up_print_rodape_reserva '" + mySite + "' ")
					If Reccount("Reservaprint") <> 0

						Public upv_moeda

						upv_moeda = Alltrim(uf_gerais_getParameter_Site('ADM0000000004', 'text', mySite))

						lcPrintFRX = uf_gerais_getUmvalor("b_impressoes", "id", "tabela = 'FT' and nomeImpressao = 'Tal�o Atendimento Reserva'")

						uf_imprimirgerais_sendprinter_talao(lcPrintFRX, .F., .F.)
					Endif

				Else

					uf_reservas_ImprimeTalaoPOS(.T., cabdoc.cabstamp)

				Endif
			Endif

			&&refrescar o ecra
			uf_documentos_actualizaDoc()
		Else
			uf_perguntalt_chama("ESTA OPC��O APENAS EST� DISPON�VEL PARA OS DOCUMENTOS: RESERVA DE CLIENTE, V/ FACTURA E V/ GUIA TRANSPORTE.","OK","",64)
			Return .F.
		Endif
	Endif
Endfunc


**
Function uf_documentos_verificaImpReserva
	Local lcSQL, lcQt, lcQtDist,lcJaDistribuiu

	Select FN
	Go Top
	Scan
		Store '' To lcSQL
		Store 0 To lcQt
		lcQt = FN.qtt

		TEXT TO lcSQL NOSHOW TEXTMERGE
			exec up_reservas_rececao '<<ALLTRIM(fn.ref)>>', '<<mysite>>'
		ENDTEXT

		If !uf_gerais_actGrelha("","uCrsVerificaReserva",lcSQL)
			uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VERIFICAR RESERVAS DE CLIENTE.","OK","",16)
			Return .F.
		Endif

		If Reccount("uCrsVerificaReserva")> 0

			For i = 1 To lcQt

				lcJaDistribuiu = .F.

				Select uCrsVerificaReserva
				Go Top
				Scan
					Select uCrsVerificaReserva
					If uCrsVerificaReserva.qttdistribuida < uCrsVerificaReserva.qttreserva And lcJaDistribuiu == .F.

						lcJaDistribuiu = .T.

						Select uCrsVerificaReserva
						Replace uCrsVerificaReserva.qttdistribuida With uCrsVerificaReserva.qttdistribuida +1

						uf_reservas_ImprimeTalaoPOS(.T.,Alltrim(uCrsVerificaReserva.stampReserva),.T.,Alltrim(uCrsVerificaReserva.stampReservaBI))
					Endif
				Endscan
			Endfor

		Endif
	Endscan

Endfunc


**
Function uf_documentos_actualizaValoresStockAct
	Lparameters tcTabela

	If Empty(tcTabela)
		Return .F.
	Endif

	If Upper(Alltrim(tcTabela))=='BO'
		** actualiza stock actual **
		Select Bi
		If !Empty(Bi.ref)

			lcSQL = ""
			TEXT TO lcSQL NOSHOW TEXTMERGE
				exec up_stocks_verStockAct '<<Alltrim(bi.ref)>>',<<mysite_nr>>
			ENDTEXT

			If uf_gerais_actGrelha("", "uCrsStockActBi",lcSQL)
				If Reccount()>0
					Select Bi
					Replace Bi.u_stockact With uCrsStockActBi.STOCK
				Endif
				fecha("uCrsStockActBi")
			Endif
		Endif
		****************************

		Select Bi
	Endif

	If Upper(Alltrim(tcTabela))=='FO'
		** actualiza stock actual **
		Select FN
		If !Empty(FN.ref)



			lcSQL = ""
			TEXT TO lcSQL NOSHOW TEXTMERGE
				exec up_stocks_verStockAct '<<Alltrim(fn.ref)>>',<<mysite_nr>>
			ENDTEXT

			If uf_gerais_actGrelha("", "uCrsStockActFn", lcSQL)
				If Reccount()>0
					Select FN
					Replace FN.u_stockact With uCrsStockActFn.STOCK
				Endif
				fecha("uCrsStockActFn")
			Endif
		Endif
		**

		Select FN
	Endif
Endfunc


**
Function uf_documentos_etiquetas
	&&Valida acesso ao Painel
	If (uf_gerais_validaPermUser(ch_userno,ch_grupo,'Stock - Imprimir Etiquetas'))
		If !uf_gerais_getParameter("ADM0000000343","BOOL")
			uf_etiquetas_chama('DOC')
		Else
			*!*				uv_validImp = .F.

			*!*	            DIMENSION laPrinters[1,1]
			*!*	            = APRINTERS(laPrinters)
			*!*
			*!*	            FOR lnIndex = 1 TO ALEN(laPrinters,1)
			*!*	                lcPrinterName=laPrinters(lnIndex,1)
			*!*	                IF LEFT(UPPER(alltrim(lcPrinterName)), 9) = "ETIQUETAS"
			*!*	                    uv_validImp = .T.
			*!*	                ENDIF
			*!*	            ENDFOR

			*!*	            IF uv_validImp
			*!*	                uf_imprimirgerais_Chama("DOCS_ETIQUETAS")
			*!*	            ELSE
			*!*	                uf_perguntalt_chama("N�o existe nenhuma impressora de Etiquetas." + chr(13) + "Por favor contacte o suporte.","OK","", 48)
			*!*	                RETURN .F.
			*!*	            ENDIF

			uf_imprimirgerais_Chama("DOCS_ETIQUETAS")

		Endif
	Else
		uf_perguntalt_chama("O SEU PERFIL N�O PERMITE IMPRIMIR ETIQUETAS.","OK","",48)
	Endif
Endfunc


**
Function uf_documentos_updateMargensFoStDoc
	Local lcUsaLotes
	Do Case
		Case myTipoDoc == "FO"

			** Correr c�digo apenas para documentos V/factura, V/Guia de Remessa e N/Guia de Entrada e V/factura Med. **
			Select cabdoc
			If cabdoc.NUMINTERNODOC==55 Or cabdoc.NUMINTERNODOC==101 Or cabdoc.NUMINTERNODOC==102 Or cabdoc.NUMINTERNODOC==58

				lcUsaLotes = uf_gerais_getParameter("ADM0000000211","BOOL")

				Local lcSqlVal, lcSqlPVP, lcSqlValPVP, lcDesign, lcCntPvp, lcValidaUltDoc
				Store "" To lcSQL
				Store 0 To lcCntPvp

				If Used("crsres")
					fecha("crsres")
				Endif
				Create Cursor CrsRes (ref c(18), Design c(55), Data_Antiga D, Nova_Data D, PVP_Antigo N(16,6), PVP_Novo N(16,6), PESQUISA L, fnstamp c(50))

				** Actualizar Custos PCT **
				TEXT TO lcSql NOSHOW TEXTMERGE
					exec up_Documentos_AtualizaST '<<cabDoc.cabstamp>>', <<ch_userno>>, <<IIF(lcUsaLotes,1,0)>>,<<mysite_nr>>
				ENDTEXT
				_Cliptext = lcSQL

				If !uf_gerais_actGrelha("","UcrsDadosActualizadosSt",lcSQL)
					uf_perguntalt_chama("Ocorreu uma anomalia na atualiza��o dos dados do Produto. Contacte o suporte.","OK","",16)
					Return .F.
				Endif

				** atualiza  a data de validade do artigo se a mesma for alterada na linha da compra - apenas na importa��o de encomendas
				If uf_gerais_getParameter_Site('ADM0000000030', 'BOOL', mySite) &&AND (Upper(Alltrim(cabdoc.doc))=="V/FACTURA" OR Upper(Alltrim(cabdoc.doc))=="V/FACTURA MED." OR Upper(Alltrim(cabdoc.doc))=="V/GUIA TRANSP.")

					Select FN
					Go Top
					Scan
						If !Empty(FN.ref) And FN.qtt > 0 And FN.dt_val_orig <> FN.u_validade
							**IF (!EMPTY(fn.bistamp) OR !EMPTY(fn.stamp_ext_doc)) AND fn.dt_val_orig <> fn.u_validade AND !EMPTY(fn.ref)

							Local lctmpfnstamp
							lctmpfnstamp = Alltrim(FN.fnstamp)
							TEXT TO lcSql NOSHOW TEXTMERGE PRETEXT 8
									select validade FROM st(nolock) where st.ref='<<alltrim(fn.ref)>>' and st.site_nr = <<mysite_nr>>
							ENDTEXT
							If !uf_gerais_actGrelha("","curvaltemp",lcSQL)
								uf_perguntalt_chama("Ocorreu uma anomalia na atualiza��o da validade dos artigos. Contacte o suporte.","OK","",16)
								Return .F.
							Endif

							Select CrsRes
							Append Blank
							Replace CrsRes.fnstamp With lctmpfnstamp
							Replace CrsRes.Data_Antiga With curvaltemp.validade

							fecha("curvaltemp")

							Select FN
							TEXT TO lcSql NOSHOW TEXTMERGE PRETEXT 8
									UPDATE st SET validade = '<<uf_gerais_getDate(fn.u_validade,"SQL")>>' where st.ref='<<alltrim(fn.ref)>>' and st.site_nr = <<mysite_nr>>
							ENDTEXT
							If !uf_gerais_actGrelha("","",lcSQL)
								uf_perguntalt_chama("Ocorreu uma anomalia na atualiza��o da validade dos artigos. Contacte o suporte.","OK","",16)
								Return .F.
							Endif
							Select FN

						Endif
					Endscan
				Endif


				** Actualiza Iva incluido na st
				If Used("ucrsAtualizaStIvaIncl")

					Select ucrsAtualizaStIvaIncl
					Go Top
					Scan
						** Actualizar Custos PCT **
						TEXT TO lcSql NOSHOW TEXTMERGE
							UPDATE
								st
							SET
								st.ivaincl = <<IIF(ucrsAtualizaStIvaIncl.ivaincl,1,0)>>
							where
								st.ref = '<<ALLTRIM(ucrsAtualizaStIvaIncl.ref)>>'
								AND st.site_nr = <<mysite_nr>>
						ENDTEXT

						If !uf_gerais_actGrelha("","",lcSQL)
							uf_perguntalt_chama("Ocorreu uma anomalia na atualiza��o dos dados do Produto. Contacte o suporte.","OK","",16)
							Return .F.
						Endif


					Endscan
				Endif
				If Used("ucrsAtualizaStIvaIncl")
					fecha("ucrsAtualizaStIvaIncl")
				Endif

				Select UcrsDadosActualizadosSt
				Go Top
				Scan For UcrsDadosActualizadosSt.ValidadeAnterior != UcrsDadosActualizadosSt.ValidadeNova Or UcrsDadosActualizadosSt.PVPAnterior != UcrsDadosActualizadosSt.PVPNovo
					Select CrsRes
					Append Blank
					Replace CrsRes.ref With UcrsDadosActualizadosSt.ref
					Replace CrsRes.Design With UcrsDadosActualizadosSt.Design
					Replace CrsRes.Data_Antiga With UcrsDadosActualizadosSt.ValidadeAnterior
					Replace CrsRes.Nova_Data With UcrsDadosActualizadosSt.ValidadeNova
					Replace CrsRes.PVP_Antigo With UcrsDadosActualizadosSt.PVPAnterior
					Replace CrsRes.PVP_Novo With UcrsDadosActualizadosSt.PVPNovo
				Endscan
				If uf_gerais_getParameter_Site('ADM0000000030', 'BOOL', mySite) &&AND (Upper(Alltrim(cabdoc.doc))=="V/FACTURA" OR Upper(Alltrim(cabdoc.doc))=="V/FACTURA MED." OR Upper(Alltrim(cabdoc.doc))=="V/GUIA TRANSP.")

					Select FN
					Go Top
					Scan
						**IF (!EMPTY(fn.bistamp) OR !EMPTY(fn.stamp_ext_doc)) AND fn.dt_val_orig <> fn.u_validade AND !EMPTY(fn.ref)
						If FN.dt_val_orig <> FN.u_validade And !Empty(FN.ref)
							Select CrsRes
							Go Top
							Scan
								If Alltrim(CrsRes.fnstamp) == Alltrim(FN.fnstamp)
									Replace CrsRes.ref With FN.ref
									Replace CrsRes.Design With 'MAN. - '+ Alltrim(FN.Design)
									Replace CrsRes.Nova_Data With FN.u_validade
								Endif
							Endscan
							Select FN

						Endif
					Endscan
				Endif



				If Reccount([CrsRes])>0 And uf_gerais_getParameter("ADM0000000340","BOOL")
					If uf_perguntalt_chama("Foi actualizada a Validade e/ou PVP de Alguns Produtos. Deseja Visualiz�-los?"+ Chr(13) + "Produtos com PVP/Validade Actualizada","Sim","N�o")
						uf_valpvpalt_chama()
					Endif
				Endif

				If Used([CrsRes])
					fecha([CrsRes])
				Endif
			Endif

			******************************

		Case myTipoDoc == "BO"	And !Empty(ucrsConfigDoc.folanSl)

			** Actualizar Margens **
			TEXT TO lcSql NOSHOW TEXTMERGE
				update
					st
				set
					-- Margem Comercial
					marg1	= case when epv1>0 AND ROUND(st.epcusto,2)>0
									then ROUND( ((st.epv1 / (taxasiva.taxa/100+1) / round(st.epcusto,2)) -1) * 100 ,2)
									ELSE 0 end,
					-- Desconto Comercial
					marg2	= case when ROUND(st.epcusto,2)>0 and ROUND(st.epcpond,2)>0
									then ROUND( ((round(st.epcusto,2) / round(st.epcpond,2)) -1) * 100 ,2)
									ELSE 0 end,
					-- Margem Bruta
					marg3	= case when st.epv1>0 and ROUND(st.epcpond,2)>0
									then ROUND( ((st.epv1 / (taxasiva.taxa/100+1) / round(st.epcpond,2)) -1) * 100 ,2)
									ELSE 0 end,
					-- Benificio Bruto
					marg4	= case when st.epv1>0
									then ROUND( (st.epv1 / (taxasiva.taxa/100+1)) - round(st.<<ALLTRIM(uf_gerais_getGrossProfitAtribute())>>,2) ,2)
									ELSE 0 end
				from
					bi
					inner join st on bi.ref = st.ref
					inner join taxasiva (nolock) on taxasiva.codigo = st.tabiva
				where
					bi.bostamp = '<<ALLTRIM(cabDoc.cabstamp)>>'
					and bi.ref != ''
					and st.site_nr = <<mysite_nr>>
			ENDTEXT

			If !uf_gerais_actGrelha("", "", lcSQL)
				uf_perguntalt_chama("OCORREU UMA ANOMALIA A ACTUALIZAR MARGENS DO PRODUTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			Endif

		Otherwise
			**
	Endcase


Endfunc
**


** Fun��o usada no Pagamento e Factura��o
Function uf_documentos_updateMargensFoSt
	Lparameters tcRef, tcStamp

	If Empty(tcRef)
		Return .F.
	Endif

	If !(Type("tcStamp") == "C")
		tcStamp = ''
	Endif


	** guardar valores originais para actualizar historico pvp **
	If !Empty(tcStamp)

		lcSQL = ""
		TEXT TO lcSQL NOSHOW TEXTMERGE
			select top 1
				marg1, marg2, marg3, marg4
			from
				st (nolock)
			where
				st.ref = '<<ALLTRIM(tcRef)>>'
				and st.site_nr= <<mysite_nr>>
		ENDTEXT

		If uf_gerais_actGrelha("", "uCrsHistoricoPvp", lcSQL)
			If !Reccount()>0
				fecha("uCrsHistoricoPvp")
			Endif
		Endif
	Endif
	*************************************************************

	** Actualizar Margens **
	TEXT TO lcSql NOSHOW TEXTMERGE
		UPDATE st
		SET
			-- Margem Comercial
			marg1	= case when epv1>0 AND ROUND(epcusto,2)>0
							then ROUND( ((st.epv1 / (taxasiva.taxa/100+1) / round(st.epcusto,2)) -1) * 100 ,2)
							ELSE 0 end,
			-- Desconto Comercial
			marg2	= case when ROUND(st.epcusto,2)>0 and ROUND(st.epcpond,2)>0
							then ROUND( ((round(st.epcusto,2) / round(st.epcpond,2)) -1) * 100 ,2)
							ELSE 0 end,
			-- Margem Bruta
			marg3	= case when st.epv1>0 and ROUND(st.epcpond,2)>0
							then ROUND( ((st.epv1 / (taxasiva.taxa/100+1) / round(st.epcpond,2)) -1) * 100 ,2)
							ELSE 0 end,
			-- Benificio Bruto
			marg4	= case when st.epv1>0
							then ROUND( (st.epv1 / (taxasiva.taxa/100+1)) - round(st.<<ALLTRIM(uf_gerais_getGrossProfitAtribute())>>,2) ,2)
							ELSE 0 end
		from
			st (nolock)
			inner join taxasiva (nolock) on taxasiva.codigo = st.tabiva
		where
			st.ref='<<alltrim(tcRef)>>'
			and st.site_nr = <<mysite_nr>>
	ENDTEXT

	If !uf_gerais_actGrelha("", "", lcSQL)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA AO ACTUALIZAR MARGENS DO PRODUTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
	Endif

	** actualizar historico caso exista [Valores Finais]**
	If !Empty(tcStamp) And Used("uCrsHistoricoPvp")
		TEXT TO lcSql NOSHOW TEXTMERGE
			UPDATE
				B_historicopvp
			SET
				oMc		= <<uCrsHistoricoPvp.marg1>>
				,dMc	= ISNULL((select top 1 marg1 from st (nolock) where st.ref='<<ALLTRIM(tcRef)>>'),0)
				,oDc	= <<uCrsHistoricoPvp.marg2>>
				,dDc	= ISNULL((select top 1 marg2 from st (nolock) where st.ref='<<ALLTRIM(tcRef)>>'),0)
				,oMb	= <<uCrsHistoricoPvp.marg3>>
				,dMb	= ISNULL((select top 1 marg3 from st (nolock) where st.ref='<<ALLTRIM(tcRef)>>'),0)
				,oBb	= <<uCrsHistoricoPvp.marg4>>
				,dBb	= ISNULL((select top 1 marg4 from st (nolock) where st.ref='<<ALLTRIM(tcRef)>>'),0)
			where
				stamp='<<ALLTRIM(tcStamp)>>'
		ENDTEXT
		If !uf_gerais_actGrelha("", "", lcSQL)
			uf_perguntalt_chama("OCORREU UMA ANOMALIA AO ACTUALIZAR HIST�RICO DE PVP'S. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		Endif

		fecha("uCrsHistoricoPvp")
	Endif
	**
Endfunc


** Verificar B�nus Dispon�veis
Function uf_documentos_verBonus
	Select cabdoc
	If myTipoDoc == "FO"
		lcSQL = "exec up_Documentos_verBonusFN '" + Alltrim(cabdoc.cabstamp) + "'"
	Else
		lcSQL = "exec up_Documentos_verBonusBI '" + Alltrim(cabdoc.cabstamp) + "'"
	Endif
	If uf_gerais_actGrelha("","uCrsBonus",lcSQL)
		Select uCrsBonus
		Go Top
		uf_bonusprod_chama()
	Endif
Endfunc


** Verifica Plafond
Function uf_documentos_verifyPlafondEncForn
	Lparameters tcBool, lcPainel

	Local lcValorPlafond, lcValorEnc, lcPlafond
	Store 0 To lcValorPlafond, lcValorEnc

	** verifica se controla Plafond nas Encomendas a Fornecedor **
	lcValorPlafond  = uf_gerais_getParameter("ADM0000000109","NUM")

	If Empty(lcValorPlafond) Or lcValorPlafond==0
		Return .T.
	Endif
	**************************************************************

	** Guardar valor das encomendas j� realizadas hoje **
	If !tcBool && painel de gest�o de documentos
		Select cabdoc
		TEXT TO lcSql NOSHOW TEXTMERGE
			SELECT
				ISNULL(round(sum(etotaldeb),2),0) as total
			from
				bo
			where
				dataobra=convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
				and (ndos=2 or ndos=37)
				and bo.obrano != <<ALLTRIM(cabdoc.numdoc)>>
				And YEAR(dataobra) = YEAR('<<uf_gerais_getDate(cabdoc.datadoc,"SQL")>>')
		ENDTEXT
		If !uf_gerais_actGrelha("", "uCrsPlafond",lcSQL)
			uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VALIDAR PLAFOND DAS ENCOMENDAS A FORNECEDOR. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			Return .T.
		Else
			Select uCrsPlafond
			lcValorEnc = Round(uCrsPlafond.Total,2)
			** acrescentar valor da encomenda actual **
			Select cabdoc
			lcValorEnc = Round(lcValorEnc + cabdoc.Total,2)
		Endif
	Else && painel de encomendas semi-autom�ticas
		TEXT TO lcSql NOSHOW TEXTMERGE
			SELECT
				ISNULL(round(sum(etotaldeb),2),0) as total
			from
				bo(nolock)
			where
				dataobra=convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
				and (ndos=2 or ndos=37)
		ENDTEXT

		If !uf_gerais_actGrelha("", "uCrsPlafond",lcSQL)
			uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VALIDAR PLAFOND DAS ENCOMENDAS A FORNECEDOR. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			Return .T.
		Else
			Select uCrsPlafond
			lcValorEnc = Round(uCrsPlafond.Total,2)
			** acrescentar valor da encomenda actual **
			If !Empty(ENCAUTOMATICAS.containerSubtotais.valorsum.Value)
				lcValorEnc = Round(lcValorEnc + Val(ASTR(ENCAUTOMATICAS.containerSubtotais.valorsum.Value,8,2)),2)
			Endif
		Endif
	Endif
	*****************************************************

	If lcValorEnc==0
		Return .T.
	Else
		If lcValorEnc > lcValorPlafond
			uf_perguntalt_chama("EXCEDEU O PLAFOND DI�RIO DAS ENCOMENDAS A FORNECEDOR EM "+ASTR(Round(lcValorEnc-lcValorPlafond,2))+" �."+Chr(10)+Chr(10)+"PLAFOND: "+ASTR(lcValorPlafond),"OK","",48)
			** PEDIR PASSWORD PARA APROVAR ENCOMENDA **
			If !tcBool
				lcPlafond = uf_documentos_controlaPass(.T., .F.,lcPainel)
			Else
				lcPlafond = uf_documentos_controlaPass(.T., .T.,lcPainel)
			Endif
			**
		Else
			If uf_gerais_getParameter("ADM0000000110","BOOL") == .T.
				uf_perguntalt_chama("AVISO: J� UTILIZOU "+ASTR(lcValorEnc)+" � DO SEU PLAFOND DI�RIO DE "+ASTR(lcValorPlafond)+" �.","OK","",64)
			Endif

			lcPlafond = .T.
		Endif
	Endif

	Return lcPlafond
Endfunc


**
Function uf_documentos_enviarEncomenda

	Local lcSQL, lcNrReceitaTeclado
	Store '' To lcSQL, lcNrReceitaTeclado

	&& Valida��es
	Select cabdoc
	If Upper(Alltrim(cabdoc.Doc)) != "ENCOMENDA A FORNECEDOR" And Upper(Alltrim(cabdoc.Doc)) != "ENCOMENDA VIA VERDE"
		uf_perguntalt_chama("ESTA FUNCIONALIDADE S� EST� DISPON�VEL PARA ENCOMENDAS A FORNECEDOR.","OK","",64)
		Return .F.
	Endif

	If Alltrim(cabdoc.SITE)<>Alltrim(mySite)
		uf_perguntalt_chama("N�O PODE ENVIAR UMA ENCOMENDA DE UMA LOJA DIFERENTE DA QUE SE ENCONTRA.","OK","",64)
		Return .F.
	Endif

	&& Valida se pretende enviar novamente a encomenda
	Select cabdoc
	Do Case
		Case cabdoc.NUMINTERNODOC == 2 And Upper(Alltrim(myTipoDoc)) == "BO"
			If cabdoc.LOGI1 == .T.
				If !uf_perguntalt_chama("ATEN��O: ESTA ENCOMENDA J� FOI ENVIADA PARA O FORNECEDOR, PRETENDE ENVIAR NOVAMENTE?","Sim","N�o")
					Return .F.
				Endif
			Endif
		Case cabdoc.NUMINTERNODOC == 3 And Upper(Alltrim(myTipoDoc)) == "BO"
			If cabdoc.LOGI1 == .T.
				uf_perguntalt_chama("ESTA ENCOMENDA J� FOI ENVIADA. N�O PODE ENVIAR NOVAMENTE O ESTE TIPO DE DOCUMENTO.","OK","",64)
				Return .F.
			Endif
		Otherwise
			Return .F.
	Endcase

	Select cabdoc

	If Upper(Alltrim(cabdoc.Doc)) == "ENCOMENDA VIA VERDE"

		If Empty(uf_gerais_getUmvalor("fl_site","via_verde_user","nr_fl = " + ASTR(cabdoc.NO) + " and dep_fl = " + ASTR(cabdoc.ESTAB)));
				OR Empty(uf_gerais_getUmvalor("fl_site","via_verde_pass","nr_fl = " + ASTR(cabdoc.NO) + " and dep_fl = " + ASTR(cabdoc.ESTAB)))
			uf_perguntalt_chama("O FORNECEDOR " + Alltrim(cabdoc.Nome) + " N�O TEM OS DADOS DE VIA VERDE PREENCHIDOS." + Chr(13) + "POR FAVOR PREENCHA E VOLTE A TENTAR.","OK","", 48)
			regua(2)
			Return .F.
		Endif

	Endif

	&& Verifica a informa��o do fornecedor
	lcSQL = ''
	TEXT TO lcSql NOSHOW TEXTMERGE
		select tipo, usa_esb, id_esb from fl (nolock) LEFT JOIN fl_site ON fl.no = fl_site.nr_fl AND fl.estab = fl_site.dep_fl where no = <<cabDoc.no>> and estab = <<cabDoc.estab>> and site_nr = <<mysite_nr>>
	ENDTEXT
	If !uf_gerais_actGrelha("", "uCrsTipoFl",lcSQL)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VERIFICAR TIPO DO FORNECEDOR. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		Return .F.
	Endif

	If Alltrim(uCrsTipoFl.id_esb)='B2B' And Alltrim(Upper(cabdoc.Doc)) <> "ENCOMENDA VIA VERDE"
		uf_gerais_ChamaEsbOrders_PedidoEncomenda(Alltrim(cabdoc.cabstamp))
		uf_documentos_actualizaDoc()
	Else

		&& Valida Envio de Encomenda para Grupo

		Select uCrsTipoFl
		If Alltrim(Upper(uCrsTipoFl.TIPO)) == 'GRUPO'  && Encomendas de GRUPO
			uf_condicoesComerciais_enviarEncomenda()
		Else

			&& Verificar Loja Da Encomenda
			TEXT TO lcSql NOSHOW TEXTMERGE
				select site from bo (nolock) where bostamp = '<<cabDoc.cabstamp>>'
			ENDTEXT
			If !uf_gerais_actGrelha("", "uCrsSiteEnc",lcSQL)
				uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VERIFICAR O LOCAL DA ENCOMENDA. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				Return .F.
			Endif

			Select uCrsSiteEnc
			If !(Alltrim(uCrsSiteEnc.SITE) == Alltrim(mySite))
				If !uf_perguntalt_chama("Aten��o: A encomenda foi feita no local: " + Alltrim(uCrsSiteEnc.SITE) + Chr(13) + "Est� a enviar a encomenda a partir do local: " + Alltrim(mySite) + Chr(13) + "Vai usar as configura��es do local atual. Pretende continuar?","Sim","N�o")
					Return .F.
				Endif
			Endif
			If Used("uCrsSiteEnc")
				fecha("uCrsSiteEnc")
			Endif
			**

			&& Se tem linhas e qtt superior a 0
			If !(DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.page10.ContainerTotais.Totalrefs.Value > 0)
				uf_perguntalt_chama("N�o pode enviar encomendas sem refer�ncias." + Chr(13) + "Por favor verifique.","OK","",64)
				Return .F.
			Endif
			If !(DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.page10.ContainerTotais.TotalQts.Value > 0)
				uf_perguntalt_chama("N�o pode enviar encomendas com quantidade total igual a 0." + Chr(13) + "Por favor verifique.","OK","",64)
				Return .F.
			Endif
			**

			&& Prepara o envio da Encomenda
			Select cabdoc
			&& Tipo Via Verde
			If Upper(Alltrim(cabdoc.Doc)) == "ENCOMENDA VIA VERDE"

				&& Envia encomenda Via Verde
				&& Valida introdu��o do n� de Receita
				Public myReceitaNr
				Store '' To myReceitaNr

				If Type("documentos.pageframe1.page1.detalhest1.pageframe1.page11.containerOutrosDados.NRRECEITA") !="U"
					lcNrReceitaTeclado = DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.page11.ContainerOutrosDados.NRRECEITA.Value
				Endif

				uf_tecladoalpha_chama("myReceitaNr", "Introduza o Nr. da Receita:", .F., .F., 0, .F., lcNrReceitaTeclado )

				If Empty(myReceitaNr) Or (Len(Alltrim(myReceitaNr)) != 13 And Len(Alltrim(myReceitaNr)) != 19)
					uf_perguntalt_chama("O N� DE RECEITA TEM DE TER 13 OU 19 DIGITOS","OK","", 16)

					Release myReceitaNr

					Return .F.
				Else
					If !Empty(cabdoc.NRRECEITA)
						If cabdoc.NRRECEITA != myReceitaNr
							If (uf_perguntalt_chama("O n�mero que inseriu � diferente do n�mero de receita que inseriu anteriormente."+ Chr(13) + "Pretende continuar? " ,"Sim","N�o"))
								uf_documentos_changeNrReceita(myReceitaNr)
							Else
								Release myReceitaNr
								Return .F.
							Endif
						Endif
					Else
						uf_documentos_changeNrReceita(myReceitaNr)
					Endif
				Endif

				regua(0,3,"A enviar Encomenda, por favor aguarde.")

				&& Guardar caminho do software de envio
				Local  lcWsPath, lcIDCliente, lcNomeJar
				Store '' To lcWsPath, lcIDCliente, lcNomeJar

				&& id do cliente LTS
				Select ucrse1
				If Empty(Alltrim(ucrse1.id_lt))
					uf_perguntalt_chama("O C�DIGO DE CLIENTE LOGITOOLS N�O EST� CORRETAMENTE PREENCHIDO. POR FAVOR CONTACTE O SUPORTE.","OK","", 16)
					Return .F.
				Endif

				lcIDCliente = Alltrim(ucrse1.id_lt)

				&& valida e configura software p envio encomenda
				lcNomeJar = 'ViaVerdeMed.jar'
				If !File(Alltrim(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\ViaVerdeMed\' + Alltrim(lcNomeJar))
					uf_perguntalt_chama("O SOFTWARE DE ENVIO N�O EST� CONFIGURADO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
					regua(2)
					Return .F.
				Endif

				&& Define de usa testes ou n�o
				&& verifica se utiliza webservice de testes
				Local lcDefineWsTest
				Store 0 To lcDefineWsTest
				If uf_gerais_getParameter("ADM0000000227","BOOL") == .F.
					lcDefineWsTest = 0
				Else
					lcDefineWsTest = 1
				Endif

				&& Tenta o envio
				regua(1,2,"A enviar Encomenda, por favor aguarde.")

				lcWsPath = Alltrim(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\ViaVerdeMed\' + Alltrim(lcNomeJar) + ' ENVIO ';
					+ ' "--orderStamp=' + Alltrim(cabdoc.cabstamp) + ["];
					+ ' "--orderNr=' + Alltrim(cabdoc.NUMDOC) + ["];
					+ ' "--clID=' + lcIDCliente + ["];
					+ ' "--site=' + Upper(Alltrim(mySite)) + ["];
					+ ' --supplierNr=' + Alltrim(Str(cabdoc.NO));
					+ ' --supplierDep=' + Alltrim(Str(cabdoc.ESTAB));
					+ ' "--IDReceita=' + Alltrim(myReceitaNr) + ["];
					+ ' --test=' + Alltrim(Str(lcDefineWsTest))

				&& Envia comando Java
				lcWsPath = "javaw -jar " + lcWsPath

				&& aguarda que software termine o envio da informa��o 3� PARAMETRO = .T.
				oWSShell = Createobject("WScript.Shell")
				oWSShell.Run(lcWsPath, 1, .T.)

				&& verifica resposta
				regua(1,3,"A verificar resposta do Fornecedor.")

				&& consultar info registada na BD para dar indica��o ao utilizador
				lcSQL = ''
				TEXT TO lcSQL TEXTMERGE NOSHOW
						select TOP 1 * FROM via_verde_med WHERE bostamp = '<<ALLTRIM(cabdoc.cabstamp)>>' ORDER BY data desc
				ENDTEXT
				If !uf_gerais_actGrelha("","uCrsAuxResposta",lcSQL)
					uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VERIFICAR A RESPOSTA DO FORNECEDOR. POR FAVOR CONTACTE O SUPORTE.", "Ok", "", 16)
					regua(2)
					Return .F.
				Else
					If Reccount("uCrsAuxResposta") > 0
						&& Encomenda aceite sem erros pelo fornecedor
						If uCrsAuxResposta.aceite == .T.

							&& atualiza informa��o de documento enviado
							lcSQL = ''
							TEXT TO lcSQL TEXTMERGE NOSHOW
									UPDATE bo SET logi1 = 1 WHERE bostamp = '<<ALLTRIM(uCrsAuxResposta.bostamp)>>'
							ENDTEXT
							If !uf_gerais_actGrelha("","",lcSQL)
								uf_perguntalt_chama("OCORREU UMA ANOMALIA A ATUALIZAR A ENCOMENDA. POR FAVOR CONTACTE O SUPORTE."+Chr(10)+"Cod: Enc. At.","OK","",16)
								Return .F.
							Endif

							uf_perguntalt_chama("Encomenda enviada com sucesso! ","OK","",64)


							Select uCrsAuxResposta
							If(uf_documentos_fecha_encomendas_semProducto(Alltrim(uCrsAuxResposta.ESTADO),Alltrim(uCrsAuxResposta.BOSTAMP)))
								regua(2)
								Return .F.
							Endif

							&& informa��o proveniente do forncedor
							lcSQL = ''
							TEXT TO lcSQL TEXTMERGE NOSHOW
									SELECT vv.ref, bi.design,  vv.qt_pedida, vv.qt_entregue FROM via_verde_med_d vv (nolock)
									INNER JOIN bi (nolock) ON vv.ref = bi.ref
									WHERE id_via_verde_med = '<<ALLTRIM(uCrsAuxResposta.id)>>'
									AND bi.bostamp = '<<ALLTRIM(uCrsAuxResposta.bostamp)>>'
							ENDTEXT

							If !uf_gerais_actGrelha("","uCrsAuxRespostaDetalhe",lcSQL)
								uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VERIFICAR A RESPOSTA DO FORNECEDOR. POR FAVOR CONTACTE O SUPORTE.", "Ok", "", 16)
								regua(2)
								Return .F.
							Else
								&&  Mostra a informa��o com qtt pedida e qtt fornecida pelo forn (n�o foi testado mt bem pq os acessos de testes aos fornecedores estavam em baixo)
								Local lcInfForn
								Store '' To lcInfForn

								Select uCrsAuxRespostaDetalhe
								Go Top
								Scan
									lcInfForn = lcInfForn + Chr(13) + Alltrim(uCrsAuxRespostaDetalhe.ref) + ' ' + Alltrim(uCrsAuxRespostaDetalhe.Design) + ' Qtt Pedida '  + Alltrim(Str(uCrsAuxRespostaDetalhe.qt_pedida))  + ' Qtt Entregue '  + Alltrim(Str(uCrsAuxRespostaDetalhe.qt_entregue))
								Endscan

								uf_perguntalt_chama(Left(lcInfForn,200),"OK","",16)
								regua(2)
							Endif

						Else
							&& Encomenda com erros mostra output
							lcSQL = ''
							TEXT TO lcSQL TEXTMERGE NOSHOW
									select TOP 2 * FROM via_verde_med_d WHERE id_via_verde_med = '<<ALLTRIM(uCrsAuxResposta.id)>>'
							ENDTEXT

							If !uf_gerais_actGrelha("","uCrsAuxRespostaDetalhe",lcSQL)
								uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VERIFICAR A RESPOSTA DO FORNECEDOR. POR FAVOR CONTACTE O SUPORTE.", "Ok", "", 16)
								regua(2)
								Return .F.
							Else
								Local lcInfForn
								Store '' To lcInfForn

								Select uCrsAuxRespostaDetalhe
								Go Top
								Scan
									lcInfForn = lcInfForn + Chr(13) + Alltrim(uCrsAuxRespostaDetalhe.response_descr)
								Endscan

								&& mostra a mensagem
								If uf_gerais_compstr(lcInfForn , "")
									uf_perguntalt_chama(Left(lcInfForn,100),"OK","",16)
									regua(2)
								Else
									Select uCrsAuxResposta
									If(uf_documentos_fecha_encomendas_semProducto(Alltrim(uCrsAuxResposta.ESTADO),Alltrim(uCrsAuxResposta.BOSTAMP)))
										regua(2)
										Return .F.
									Endif
								Endif



							Endif
						Endif
					Else
						uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VERIFICAR A RESPOSTA DO FORNECEDOR. POR FAVOR CONTACTE O SUPORTE.", "Ok", "", 16)
						regua(2)
						Return .F.
					Endif

					If Used("uCrsAuxResposta")
						fecha("uCrsAuxResposta")
					Endif

					If Used("uCrsAuxRespostaDetalhe")
						fecha("uCrsAuxRespostaDetalhe")
					Endif

					Release myReceitaNr

				Endif

				regua(2)

			Else
				&& Encomenda do Tipo Normal - pode ser enviada por postal normal ou por LTS ESB
				If !Empty(uCrsTipoFl.usa_esb) && encomenda ESB e n�o b2b

					&& id do fornecedor no ESB
					If Empty(uCrsTipoFl.id_esb)
						uf_perguntalt_chama("Falta preencher o c�digo do fornecedor no ESB. Por favor contacte o suporte.","OK","", 16)
						Return .F.
					Endif

					&& Guardar caminho do software de envio
					Local  lcWsPath, lcIDCliente, lcNomeJar, lcToken
					Store '' To lcWsPath, lcIDCliente, lcNomeJar, lcToken

					&& atribui valor ao lcToken que depois � necess�rio para aceder � informa��o que � escrita na BD
					lcToken = uf_gerais_stamp()

					&& id do cliente LTS
					Select ucrse1
					If Empty(Alltrim(ucrse1.id_lt))
						uf_perguntalt_chama("O C�DIGO DE CLIENTE LOGITOOLS N�O EST� CORRETAMENTE PREENCHIDO. POR FAVOR CONTACTE O SUPORTE.","OK","", 16)
						Return .F.
					Endif
					lcIDCliente = Alltrim(ucrse1.id_lt)

					&&LOCAL lcEsbType
					&&lcEsbType =   ALLTRIM(uCrsE1.id_esb)



					&&outras <> B2B

					&& valida e configura software p envio encomenda
					lcNomeJar = 'LTSESBOrdersClient.jar'
					If !File(Alltrim(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\LTSESBOrdersClient\' + Alltrim(lcNomeJar))
						uf_perguntalt_chama("O SOFTWARE DE ENVIO LTS ESB N�O EST� CONFIGURADO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
						Return .F.
					Endif

					regua(1,2,"A enviar Encomenda, por favor aguarde.")

					&& Define de usa testes ou n�o
					&& verifica se utiliza webservice de testes
					Local lcDefineWsTest
					Store 0 To lcDefineWsTest
					If uf_gerais_getParameter("ADM0000000227","BOOL") == .F.
						lcDefineWsTest = 0
					Else
						lcDefineWsTest = 1
					Endif

					lcWsPath = Alltrim(uf_gerais_getParameter("ADM0000000185","TEXT")) + '\LTSESBOrdersClient\' + Alltrim(lcNomeJar) +  ' --requestType=POST_ORDERS';
						+ ' "--idOrder=' + Alltrim(cabdoc.cabstamp) + ["];
						+ ' "--entity=' + Alltrim(uCrsTipoFl.id_esb) + ["];
						+ ' "--idCl=' + lcIDCliente + ["];
						+ ' "--site=' + Upper(Alltrim(mySite)) + ["];
						+ ' --siteNr=' + Alltrim(Str(mySite_nr));
						+ ' --test=' + Alltrim(Str(lcDefineWsTest));
						+ ' "--Token=' + Alltrim(lcToken) + ["]






					&& Envia comando Java
					lcWsPath = "javaw -jar " + lcWsPath

					&& aguarda que software termine o envio da informa��o 3� PARAMETRO = .T.
					oWSShell = Createobject("WScript.Shell")
					oWSShell.Run(lcWsPath, 1, .T.)

					regua(2)

					&& mostra informa��o retornada pelo fornecedor.
					lcSQL = ''
					TEXT TO lcSQL TEXTMERGE NOSHOW
						SELECT TOP 1 * FROM ext_esb_msgStatus (nolock) WHERE token = '<<lcToken>>'
					ENDTEXT
					If !uf_gerais_actGrelha("","uCrsAuxRespostaDetalheESB",lcSQL)
						uf_perguntalt_chama("OCORREU UMA ANOMALIA AO VERIFICAR A RESPOSTA DO FORNECEDOR. POR FAVOR CONTACTE O SUPORTE.", "Ok", "", 16)
						Return .F.
					Else
						Local lcInfForn
						Store '' To lcInfForn

						If Alltrim(uCrsAuxRespostaDetalheESB.Id) == '200.1'
							uf_perguntalt_chama("ENCOMENDA ENVIADA COM SUCESSO.", "Ok", "", 64)
						Else
							lcInfForn = Alltrim(uCrsAuxRespostaDetalheESB.Descr)
							uf_perguntalt_chama(Left(lcInfForn,200),"OK","",16)
						Endif
					Endif

					If Used("uCrsAuxRespostaDetalheESB")
						fecha("uCrsAuxRespostaDetalheESB")
					Endif

				Else
					&& Guardar caminho do Postal
					Local postalPath
					postalPath = uf_gerais_getParameter('ADM0000000185', 'TEXT')
					If !(Right(postalPath,1)=="\")
						postalPath = Alltrim(postalPath) + '\'
					Endif
					postalPath = postalPath + "Postal\Postal.jar"

					** valida exist�ncia do ficheiro
					If !File('&postalPath')
						uf_perguntalt_chama("O POSTAL N�O EST� A SER ENCONTRADO. ASSIM N�O � POSS�VEL ENVIAR A ENCOMENDA. POR FAVOR CONTACTE O SUPORTE.","OK","", 48)
						Return .F.
					Endif

					** Criar parametros para Postal
					Select cabdoc
					postalPath = "javaw -jar " + postalPath + " " + Alltrim(Str(cabdoc.NO)) + ' ' + Alltrim(Str(cabdoc.ESTAB)) + ' ' + Alltrim(cabdoc.NUMDOC) + [ "] + Alltrim(cabdoc.cabstamp) + [" ] +Alltrim(Str(ch_userno)) + [ "] + Alltrim(mySite) + [" "] + Alltrim(sql_db) +  ["]

					** Executar Postal (enviar a encomenda)
					oWSShell = Createobject("WScript.Shell")
					oWSShell.Run(postalPath,1,.T.)
				Endif
			Endif

			uf_documentos_actualizaDoc()
		Endif

	Endif
Endfunc

Function  uf_documentos_fecha_encomendas_semProducto
	Lparameters lcEstado, lcbostamp


	Do Case
		Case  uf_gerais_compstr(lcEstado , "Sem Produto")
			uf_perguntalt_chama("Sem stock no fornecedor.","OK","",64)
			regua(2)
			TEXT TO lcsql TEXTMERGE NOSHOW
					UPDATE bo SET fechada = 1 WHERE bostamp = '<<ALLTRIM(uCrsAuxResposta.bostamp)>>'
			ENDTEXT
			If  .Not. uf_gerais_actGrelha("", "", lcSQL)
				uf_perguntalt_chama("OCORREU UMA ANOMALIA A FECHAR A ENCOMENDA. POR FAVOR CONTACTE O SUPORTE."+Chr(10)+"Cod: Enc. At.", "OK", "", 16)
				Return .F.
			Endif

		Case !uf_gerais_compstr(lcEstado , "")
			uf_perguntalt_chama("Mensagem do Fornecedor:" + lcEstado,"OK","",64)
			regua(2)
		Otherwise
			uf_perguntalt_chama("Por favor contacte o suporte.","OK","",64)
			regua(2)
	Endcase



	Return .T.
Endfunc



**
Function uf_documentos_calculaTotaisRodape

	Do Case
		Case myTipoDoc == "BO"

			Select ref From Bi Group By ref Into Cursor ucrsTotalNRefs

			If Reccount("ucrsTotalNRefs")>0
				Select ucrsTotalNRefs
				DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.page10.ContainerTotais.Totalrefs.Value = Reccount("ucrsTotalNRefs")
			Else
				DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.page10.ContainerTotais.Totalrefs.Value = 0
			Endif

			fecha("ucrsTotalNRefs")

			Select Sum(Bi.u_upc*Bi.qtt) As TotalPCL From Bi Into Cursor ucrsTotalPCL
			If Reccount("ucrsTotalPCL")>0
				DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.page10.ContainerTotais.TotalPCL.Value = ucrsTotalPCL.TotalPCL
			Else
				DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.page10.ContainerTotais.TotalPCL.Value = 0
			Endif

			fecha("ucrsTotalPCL")

			Select Sum(Bi.EPCUSTO*Bi.qtt) As TotalPCT From Bi Into Cursor ucrsTotalPCT
			If Reccount("ucrsTotalPCT")>0
				DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.page10.ContainerTotais.TotalPCT.Value = ucrsTotalPCT.TotalPCT
			Else
				DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.page10.ContainerTotais.TotalPCT.Value = 0
			Endif

			fecha("ucrsTotalPCT")

			Select Sum(qtt) As TotalQtt From Bi Into Cursor ucrsTotalQtt
			If Reccount("ucrsTotalQtt")>0
				DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.page10.ContainerTotais.TotalQts.Value  = ucrsTotalQtt.TotalQtt
			Else
				DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.page10.ContainerTotais.TotalQts.Value = 0
			Endif

			fecha("ucrsTotalQtt")

		Case myTipoDoc == "FO"

			If Used("FN")
				Select FN

				Select ref From FN Group By ref Into Cursor ucrsTotalNRefs

				If Reccount("ucrsTotalNRefs")>0
					Select ucrsTotalNRefs
					DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.page10.ContainerTotais.Totalrefs.Value = Reccount("ucrsTotalNRefs")
				Else
					DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.page10.ContainerTotais.Totalrefs.Value = 0
				Endif
				fecha("ucrsTotalNRefs")

				Select Sum(FN.u_upc*FN.qtt) As TotalPCL From FN Into Cursor ucrsTotalPCL
				If Reccount("ucrsTotalPCL")>0
					DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.page10.ContainerTotais.TotalPCL.Value = ucrsTotalPCL.TotalPCL
				Else
					DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.page10.ContainerTotais.TotalPCL.Value = 0
				Endif

				fecha("ucrsTotalPCL")

				Select Sum(Iif(At('V/Nt. Cr�dito', FN.Design) = 0, FN.epv, FN.epv * -1) * FN.qtt) As TotalPCT From FN Into Cursor ucrsTotalPCT
				If Reccount("ucrsTotalPCT")>0
					DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.page10.ContainerTotais.TotalPCT.Value = ucrsTotalPCT.TotalPCT
				Else
					DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.page10.ContainerTotais.TotalPCT.Value = 0
				Endif

				fecha("ucrsTotalPCT")

				Select Sum(qtt) As TotalQtt From FN Into Cursor ucrsTotalQtt
				If Reccount("ucrsTotalQtt")>0
					DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.page10.ContainerTotais.TotalQts.Value  = ucrsTotalQtt.TotalQtt
				Else
					DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.page10.ContainerTotais.TotalQts.Value = 0
				Endif

				fecha("ucrsTotalQtt")
			Endif
		Otherwise
			**
	Endcase
Endfunc


**
Function uf_documentos_validaArmazem
	Do Case
		Case myTipoDoc == "BO"

			Select Bi
			TEXT TO lcSQL NOSHOW TEXTMERGE
				exec up_gerais_armazensDisponiveis '<<ALLTRIM(mysite)>>', '<<ALLTRIM(bi.ref)>>'
			ENDTEXT

			If !uf_gerais_actGrelha("", "uCrsArmazensDisp",lcSQL)
				uf_perguntalt_chama("N�O FOI POSSIVEL VERIFICAR OS ARMAZ�NS DISPONIVEIS. POR FAVOR CONTACTE O SUPORTE","OK","",16)
				Return .F.
			Endif

			Select uCrsArmazensDisp
			Locate For uCrsArmazensDisp.ARMAZEM == Bi.ARMAZEM
			If !Found()
				uf_perguntalt_chama("Armaz�m inv�lido para Empresa Atual. Por favor verifique.","OK","",48)
				Select Bi
				Replace Bi.ARMAZEM With myArmazem
			Endif

			&& Aplicado para validar armazem destino
			If !Empty(Bi.ar2mazem) And Bi.ar2mazem != 0
				Locate For uCrsArmazensDisp.ARMAZEM == Bi.ar2mazem
				If !Found()
					uf_perguntalt_chama("Armaz�m inv�lido para Empresa Atual. Por favor verifique.","OK","",48)
					Select Bi
					Replace Bi.ar2mazem With myArmazem
				Endif
			Endif

			**         SELECT DISTINCT bi.ar2mazem FROM bi WITH (BUFFERING = .T.) INTO CURSOR uc_tmpBI
			**
			**         SELECT uc_tmpBI
			**         COUNT TO uv_count
			**
			**         IF uv_count > 1
			**            uf_perguntalt_chama("N�o pode escolher mais que um armazem de destino!.","OK","",48)
			**				SELECT bi
			**				Replace bi.ar2mazem WITH myarmazem
			**         ENDIF


			If Used("uCrsArmazensDisp")
				fecha("uCrsArmazensDisp")
			Endif

		Case myTipoDoc == "FO"

			Select FN
			TEXT TO lcSQL NOSHOW TEXTMERGE
				exec up_gerais_armazensDisponiveis '<<ALLTRIM(mysite)>>', ''
			ENDTEXT

			If !uf_gerais_actGrelha("", "uCrsArmazensDisp",lcSQL)
				uf_perguntalt_chama("N�O FOI POSSIVEL VERIFICAR OS ARMAZ�NS DISPONIVEIS. POR FAVOR CONTACTE O SUPORTE","OK","",16)
				Return .F.
			Endif

			Select uCrsArmazensDisp
			Locate For uCrsArmazensDisp.ARMAZEM == FN.ARMAZEM
			If !Found()
				uf_perguntalt_chama("Armaz�m inv�lido para Empresa Atual. Por favor verifique.","OK","",48)
				Select FN
				Replace FN.ARMAZEM With myArmazem
			Endif
			If Used("uCrsArmazensDisp")
				fecha("uCrsArmazensDisp")
			Endif

		Otherwise
			**
	Endcase


Endfunc


** Verifica se o documento tem
Function uf_documentos_verificaContadoresPsicoBenzo

	Do Case
		Case myTipoDoc == "BO"
			Select Sum(Bi.u_psicont) As cntPsico, Sum(Bi.u_bencont) As cntBenzo From Bi Into Cursor ucrsValidaContadoresPsicoBenzo
		Case myTipoDoc == "FO"
			Select Sum(FN.u_psicont) As cntPsico, Sum(FN.u_bencont) As cntBenzo From FN Into Cursor ucrsValidaContadoresPsicoBenzo
		Otherwise
			**
	Endcase

	Select ucrsValidaContadoresPsicoBenzo
	If ucrsValidaContadoresPsicoBenzo.cntPsico > 0 Or ucrsValidaContadoresPsicoBenzo.cntBenzo > 0
		uf_perguntalt_chama("N�O PODE ELIMINAR UM DOCUMENTO QUE POSSUA CONTADORES DE PSICOCOTR�PICOS  E/OU BENZODIAZEPINAS ATRIBU�DOS.","OK","",48)
		Return .F.
	Endif

Endfunc


**
Function uf_documentos_OrdenaPorDesignDoc
	If myDocAlteracao == .T. Or myDocIntroducao == .T.

		If myTipoDoc == "FO"

			If DOCUMENTOS.OrderDesign == .F.

				Select FN
				Index On Upper(Design) Tag Design
				Set Collate To "GENERAL"

				DOCUMENTOS.OrderDesign = .T.
			Else

				Select FN
				Index On Upper(Design) Tag Design Desc
				Set Collate To "GENERAL"

				DOCUMENTOS.OrderDesign = .F.
			Endif

			lcOrdemDasLinhas = 10000
			Select FN
			Go Top
			Scan
				Replace FN.lordem With lcOrdemDasLinhas
				lcOrdemDasLinhas = lcOrdemDasLinhas + 100
			Endscan
			Select FN
			Go Top
		Endif

		If myTipoDoc == "BO"

			If DOCUMENTOS.OrderDesign == .F.
				Select Bi
				Index On Upper(Design) Tag Design

				DOCUMENTOS.OrderDesign = .T.
			Else
				Select Bi
				Index On Upper(Design) Tag Design Desc

				DOCUMENTOS.OrderDesign = .F.
			Endif
			lcOrdemDasLinhas = 10000

			Select Bi
			Go Top
			Scan
				Replace Bi.lordem With lcOrdemDasLinhas
				lcOrdemDasLinhas = lcOrdemDasLinhas + 100
			Endscan
			Select Bi
			Go Top
		Endif

		DOCUMENTOS.Refresh
	Endif
Endfunc

**	valida numero maximo de linhas a importar a partir de xls
**

Function uf_documentos_verificaLimiteLinhas
	Lparameters lcTotalLinhas
	Local lcTotalLinhasMax

	lcTotalLinhasMax=uf_gerais_getParameter('ADM0000000274','NUM')

	If(lcTotalLinhasMax < lcTotalLinhas)
		uf_perguntalt_chama("N�o � possivel importar o ficheiro. Apenas s�o importados ficheiros com o m�ximo de " + Alltrim(Str(lcTotalLinhasMax))+" linhas.","OK","",16)
		Return .F.
	Else
		Return .T.
	Endi


Endfunc

**
Function uf_documentos_OrdenaPorRefDoc
	If myDocAlteracao == .T. Or myDocIntroducao == .T.

		If myTipoDoc == "FO"

			If DOCUMENTOS.OrderRef == .F.
				Select FN
				Index On Upper(ref) Tag Design

				DOCUMENTOS.OrderRef = .T.
			Else
				Select FN
				Index On Upper(ref) Tag Design Desc

				DOCUMENTOS.OrderRef = .F.
			Endif

			lcOrdemDasLinhas = 10000

			Select FN
			Go Top
			Scan
				Replace FN.lordem With lcOrdemDasLinhas
				lcOrdemDasLinhas = lcOrdemDasLinhas + 100
			Endscan
			Select FN
			Go Top
		Endif

		If myTipoDoc == "BO"

			If DOCUMENTOS.OrderRef == .F.
				Select Bi
				Index On Upper(ref) Tag Design

				DOCUMENTOS.OrderRef = .T.
			Else
				Select Bi
				Index On Upper(ref) Tag Design Desc

				DOCUMENTOS.OrderRef = .F.
			Endif
			lcOrdemDasLinhas = 10000

			Select Bi
			Go Top
			Scan
				Replace Bi.lordem With lcOrdemDasLinhas
				lcOrdemDasLinhas = lcOrdemDasLinhas + 100
			Endscan
			Select Bi
			Go Top
		Endif

		DOCUMENTOS.Refresh
	Endif
Endfunc


**
Function uf_documentos_chamaImportacao
	Select cabdoc
	If Upper(Alltrim(cabdoc.Doc)) == Upper("Mapa Honorarios")

		If Empty(Alltrim(cabdoc.Nome))
			uf_perguntalt_chama("Identifique o Fornecedor.","OK","",64)
			Return .F.
		Endif

		** calcula informa��o do fornecedor u_no, para mapear com o utilizador
		TEXT TO lcSQL NOSHOW TEXTMERGE
			SELECT u_no FROM fl (nolock) WHERE no = <<CabDoc.no>>
		ENDTEXT
		If !uf_gerais_actGrelha("", "uCrsU_No",lcSQL)
			uf_perguntalt_chama("N�o foi possivel encontrar utilizador correspondente ao fonecedor.","OK","",16)
			Return .F.
		Endif

		Select uCrsU_No
		uf_importhonorarios_chama(uCrsU_No.u_no,cabdoc.Nome)
	Else
		uf_importDoc_Chama()
	Endif
Endfunc

Function uf_documentos_chamaImportacao_enc
	Select cabdoc
	uf_importDoc_Chama()
	IMPORTARDOC.text1.Value='Encomenda'
	uf_importarDoc_FiltraDocs()
Endfunc

** Recalcula totais das linhas dos documentos de compras importados
Function uf_recalc_linhas_fn_imp

	Sele FN
	Go Top
	Scan
		TEXT TO lcSQL NOSHOW TEXTMERGE
			select tabiva, (select taxa from taxasiva where taxasiva.codigo=st.tabiva) as iva from st where ref='<<ALLTRIM(fn.ref)>>' and site_nr=<<mysite_nr>>
		ENDTEXT
		If uf_gerais_actGrelha("", "uCrsTabIvaST",lcSQL) And Alltrim(IMPORTARDOC.Local.Value) == 'EXTERNO'
			If FN.IVA<>uCrsTabIvaST.IVA
				Replace FN.IVA With uCrsTabIvaST.IVA
				Replace FN.tabiva With uCrsTabIvaST.tabiva
			Endif
		Endif
		Replace FN.eslvu With FN.epv-(FN.epv*(FN.desconto/100))
		Replace FN.etiliquido With Round((FN.qtt-FN.u_bonus)*FN.eslvu,2)
		Replace FN.esltt With FN.etiliquido
	Endscan
	DOCUMENTOS.PageFrame1.Page1.ContainerLinhas.Total.Refresh
	DOCUMENTOS.PageFrame1.Page1.detalhest1.Refresh
	uf_documentos_actualizaTotaisCAB()
	DOCUMENTOS.Refresh
Endfunc

**
Function uf_documentos_agrupaRefs

	If Empty(myTipoDoc)
		Return .F.
	Endif


	** coloca o focus na coluna REF **
	With DOCUMENTOS.PageFrame1.Page1.gridDoc
		*Go BOTTOM
		.SetFocus() && So activecolumn would work
		&&.Columns(.ActiveColumn).ReadOnly = .F.
	Endwith

	For i=1 To DOCUMENTOS.PageFrame1.Page1.gridDoc.ColumnCount
		If Upper(DOCUMENTOS.PageFrame1.Page1.gridDoc.Columns(i).ControlSource)=="FN.REF" Or;
				Upper(DOCUMENTOS.PageFrame1.Page1.gridDoc.Columns(i).ControlSource)=="BI.REF"

			DOCUMENTOS.PageFrame1.Page1.gridDoc.Columns(i).SetFocus
			Exit
		Endif
	Endfor

	Do Case
		Case myTipoDoc == "BO"


			** Verifica Linhas com origens
			Select * From Bi Where !Empty(Bi.obistamp) Or !Empty(Bi.oobistamp) Into Cursor ucrsLinhasComOrigem Readwrite
			If Reccount("ucrsLinhasComOrigem")>0
				uf_perguntalt_chama("O documento cont�m linhas importadas. N�o � possivel agrupar.","OK","",64)

				If Used("ucrsLinhasComOrigem")
					fecha("ucrsLinhasComOrigem")
				Endif
				Return .F.
			Endif
			If Used("ucrsLinhasComOrigem")
				fecha("ucrsLinhasComOrigem")
			Endif
			*****************************

			Select ref, Sum(qtt) As qtt From Bi Group By ref Order By ref Asc Into Cursor ucrsLinhasAgrupadas Readwrite


			**Apaga as linhas sem ref
			Select Bi
			Go Top
			Scan
				If !Empty(Alltrim(Bi.ref))
					Delete
				Endif
			Endscan

			**Insere novas linhas agrupadas
			Select ucrsLinhasAgrupadas
			Go Top
			Scan For !Empty(Alltrim(ucrsLinhasAgrupadas.ref))

				Select Bi
				uf_documentos_criaNovaLinha(.T.,.T.)

				Select Bi
				Replace Bi.ref With ucrsLinhasAgrupadas.ref
				Replace Bi.qtt With ucrsLinhasAgrupadas.qtt

				uf_documentos_eventoref()
				uf_documentos_recalculaTotaisBI()

				Select ucrsLinhasAgrupadas
			Endscan

			Select Bi
			Go Top


		Case myTipoDoc == "FO"

			** Verifica Linhas com origens
			Select * From FN Where !Empty(FN.bistamp) Or !Empty(FN.ofnstamp) Into Cursor ucrsLinhasComOrigem Readwrite
			If Reccount("ucrsLinhasComOrigem")>0
				uf_perguntalt_chama("O documento cont�m linhas importadas. N�o � possivel agrupar.","OK","",64)

				If Used("ucrsLinhasComOrigem")
					fecha("ucrsLinhasComOrigem")
				Endif
				Return .F.
			Endif
			If Used("ucrsLinhasComOrigem")
				fecha("ucrsLinhasComOrigem")
			Endif
			*****************************

			Select ref, Sum(qtt) As qtt From FN Group By ref Order By ref Asc Into Cursor ucrsLinhasAgrupadas Readwrite

			**Apaga as linhas sem ref
			Select FN
			Go Top
			Scan
				If !Empty(Alltrim(FN.ref))
					Delete
				Endif
			Endscan

			**Insere novas linhas agrupadas
			Select ucrsLinhasAgrupadas
			Go Top
			Scan For !Empty(Alltrim(ucrsLinhasAgrupadas.ref))

				Select FN
				uf_documentos_criaNovaLinha(.T.,.T.)

				Select FN
				Replace FN.ref With ucrsLinhasAgrupadas.ref
				Replace FN.qtt With ucrsLinhasAgrupadas.qtt
				uf_documentos_eventoref()
				uf_documentos_recalculaTotaisFN()

				Select ucrsLinhasAgrupadas
			Endscan

			Select FN
			Go Top

		Otherwise
			**
	Endcase

	uf_documentos_actualizaTotaisCAB()
	uf_perguntalt_chama("REFER�NCIAS AGRUPADAS COM SUCESSO!","OK","",64)

Endfunc


**
Function uf_documentos_geraEncomendaEsgotados
	Local lcEsgotados , lcbostamp
	Store '' To lcbostamp
	Select ucrsConfigDoc
	**Verifica Documento
	If (ucrsConfigDoc.NUMINTERNODOC==55 Or ucrsConfigDoc.NUMINTERNODOC==101 Or ucrsConfigDoc.NUMINTERNODOC==102 Or ucrsConfigDoc.NUMINTERNODOC==58) And ucrsConfigDoc.folanSl = .T.

		**Verifica se existem liga��es a encomendas e qtt= 0 (esgotados)
		lcEsgotados = .F.
		Select FN
		Go Top
		Scan
			If (!Empty(FN.bistamp) And FN.qtt ==0) Or (!Empty(FN.bistamp) And FN.qtt != FN.u_qttenc And FN.u_qttenc > FN.qtt)

				**Verifica se stamp � de uma encomenda
				lcSQL = ""
				TEXT TO lcSQL NOSHOW TEXTMERGE
					Select bi.ndos, bi.bostamp from bi (nolock) where bistamp = '<<ALLTRIM(fn.bistamp)>>' and bi.ndos = 2
				ENDTEXT
				If !uf_gerais_actGrelha("", "uCrsTempValidaEncomenda",lcSQL)
					uf_perguntalt_chama("N�O FOI POSSIVEL VALIDAR A ORIGEM DAS LINHAS. POR FAVOR CONTACTE O SUPORTE","OK","",16)
					Return .F.
				Endif

				If Reccount("uCrsTempValidaEncomenda") > 0 && Tem como origem Encomenda a Fornecedor
					lcEsgotados = .T.
					**lcBoStamp =	uCrsTempValidaEncomenda.bostamp
					**EXIT
					If Len(lcbostamp)=0 Then
						lcbostamp =	Alltrim(uCrsTempValidaEncomenda.BOSTAMP)
					Else
						lcbostamp =	lcbostamp + "','" + Alltrim(uCrsTempValidaEncomenda.BOSTAMP)
					Endif
				Endif
				Select FN
			Endif
		Endscan
		If Empty(lcbostamp)
			Select FN
			Go Top
			Scan
				If !Empty(FN.bistamp)
					TEXT TO lcSQL NOSHOW TEXTMERGE
						Select bi.bostamp from bi (nolock) where bistamp = '<<ALLTRIM(fn.bistamp)>>' and bi.ndos = 2
					ENDTEXT
					If !uf_gerais_actGrelha("", "uCrsTempValidaEncomenda",lcSQL)
						uf_perguntalt_chama("N�O FOI POSSIVEL VALIDAR A ORIGEM DAS LINHAS. POR FAVOR CONTACTE O SUPORTE","OK","",16)
						Return .F.
					Endif
					If Reccount("uCrsTempValidaEncomenda") > 0 && Tem como origem Encomenda a Fornecedor
						lcEsgotados = .T.
						**lcBoStamp =	uCrsTempValidaEncomenda.bostamp
						**EXIT
						If Len(lcbostamp)=0 Then
							lcbostamp =	Alltrim(uCrsTempValidaEncomenda.BOSTAMP)
						Else
							lcbostamp =	lcbostamp + "','" + Alltrim(uCrsTempValidaEncomenda.BOSTAMP)
						Endif
					Endif
				Endif
			Endscan
		Endif

		If !Empty(lcbostamp)
			** Verifica se a encomenda tem artigos que ainda n�o foram satisfeitos
			TEXT TO lcSQL NOSHOW TEXTMERGE
				Select (select top 1 fnstamp from fn (nolock) where fn.fostamp=bi.ofostamp and fn.ref=bi.ref) as fnstamp ,* from bi (nolock) where bostamp in ('<<ALLTRIM(lcBoStamp)>>') and bi.ndos = 2 AND bi.qtt2<bi.qtt --and bi.ofostamp=''
				-- Select * from bi (nolock) where bostamp = '<<ALLTRIM(lcBoStamp)>>' and bi.ndos = 2 AND bi.qtt2<bi.qtt --and bi.ofostamp=''
			ENDTEXT
			If !uf_gerais_actGrelha("", "uCrsTempValidaEncomenda2",lcSQL)
				uf_perguntalt_chama("N�O FOI POSSIVEL VALIDAR A ORIGEM DAS LINHAS. POR FAVOR CONTACTE O SUPORTE","OK","",16)
				Return .F.
			Endif
			&&_cliptext=lcSql
			Select uCrsTempValidaEncomenda2
			If Reccount("uCrsTempValidaEncomenda2")>0
				lcEsgotados = .T.
			Else
				lcEsgotados = .F.
			Endif
		Endif
		If lcEsgotados = .F.
			Return .T.
		Endif

		If uf_perguntalt_chama("Existem produtos Esgotados (QtFt=0 ou QtEnc>QtFt) provenientes de uma Encomenda. Pretende criar uma ENCOMENDA A FORNECEDOR destes produtos?","Sim","N�o")
			**SELECT ref, qtt, u_qttenc, fnstamp FROM fn WHERE !EMPTY(fn.bistamp) AND fn.qtt != fn.u_qttenc AND fn.u_qttenc >  fn.qtt INTO CURSOR ucrsBiTemp READWRITE
			Select uCrsTempValidaEncomenda2
			Select ref, qtt, qtt2 , fnstamp From uCrsTempValidaEncomenda2 Into Cursor ucrsBiTemp2 Readwrite

			**Cria documento e prenche cabDoc
			uf_documentos_novoDoc(.T.)
			DOCUMENTOS.ContainerCab.documento.Value = "Encomenda a Fornecedor"

			*!*				Select uCrsBiTemp
			*!*				GO TOP
			*!*				SCAN
			*!*					uf_documentos_criaNovaLinha()

			*!*					Replace bi.ref 			WITH ucrsBiTemp.ref
			*!*					Replace bi.qtt 			WITH ucrsBiTemp.u_qttenc - ucrsBiTemp.qtt
			*!*					Replace bi2.Bi2stamp 	WITH bi.bistamp && Necess�rio para atribu��o de valores na grava��o do documento
			*!*					Replace bi2.fnstamp 	WITH ucrsBiTemp.fnstamp

			*!*					uf_documentos_eventoref()
			*!*				ENDSCAN

			Select ucrsBiTemp2
			Go Top
			Scan
				uf_documentos_criaNovaLinha(.F.,.T.)
				Select Bi
				Replace Bi.ref 			With ucrsBiTemp2.ref
				Replace Bi.qtt 			With ucrsBiTemp2.qtt - ucrsBiTemp2.qtt2
				Select BI2
				Replace BI2.bi2stamp 	With Bi.bistamp && Necess�rio para atribu��o de valores na grava��o do documento
				If Isnull(ucrsBiTemp2.fnstamp)
					Replace BI2.fnstamp 	With ''
				Else
					Replace BI2.fnstamp 	With ucrsBiTemp2.fnstamp
				Endif

				uf_documentos_eventoref()
				Select ucrsBiTemp2
			Endscan

			fecha("ucrsBiTemp")
			fecha("ucrsBiTemp2")
		Endif
	Endif
Endfunc


**
Function uf_documentos_fecharDocs
	Public myDataFechaDocs
	**
	uf_getdate_chama(.F., "myDataFechaDocs", 0, "uf_documentos_fecharDocsFunction")
Endfunc


**
Function uf_documentos_fecharDocsFunction
	Public myDataFechaDocs
	**
	Select cabdoc
	If Empty(cabdoc.Doc) Or Empty(myDataFechaDocs)
		Return .F.
	Endif
	If !uf_perguntalt_chama("Vai fechar todos os documentos do tipo: " + Upper(Alltrim(cabdoc.Doc)) + ", com data inferior ou igual a: " + uf_gerais_getdate(myDataFechaDocs,"DATA") + Chr(13) + "Pretende continuar?","Sim","N�o",32)
		Return .F.
	Endif

	TEXT to lcSQL noshow textmerge
		update  bo set fechada = 1,
					DATAFECHO = dateadd(HOUR, <<difhoraria>>, getdate()),
					usrdata = dateadd(HOUR, <<difhoraria>>, getdate()),
					usrhora = convert(varchar,getdate(),8)
				where dataobra <= '<<uf_gerais_getDate(myDataFechaDocs,"SQL")>>' and ndos = <<cabDoc.numinternodoc>>
		update  bi set fechada = 1,
						usrdata = dateadd(HOUR, <<difhoraria>>, getdate()),
						usrhora = convert(varchar,getdate(),8)
				where dataobra <= '<<uf_gerais_getDate(myDataFechaDocs,"SQL")>>' and ndos = <<cabDoc.numinternodoc>>
	ENDTEXT

	regua(0,1,"A PROCESSAR...",.F.)

	If !uf_gerais_actGrelha("", "",lcSQL)
		uf_perguntalt_chama("N�O FOI POSS�VEL FECHAR OS DOCUMENTOS. POR FAVOR CONTACTE O SUPORTE","OK","",16)
		Return .F.
	Endif

	regua(2)

	uf_perguntalt_chama("DOCUMENTOS FECHADOS COM SUCESSO!","OK","",64)
Endfunc


**
Function uf_documentos_TabelaPrecosDocumentos
	Local lcprecocusto, lcprecounitario
	Store 0 To lcprecounitario,lcprecocusto

	If cabdoc.NO = 0
		Return .F.
	Endif

	Select cabdoc
	TEXT TO lcSQL NOSHOW TEXTMERGE
		Select ref from tabelaPrecos where fornec = <<cabdoc.no>> and fornestab = <<cabdoc.estab>>
	ENDTEXT

	If !uf_gerais_actGrelha("", "ucrsFornComPrecos",lcSQL)
		uf_perguntalt_chama("N�O FOI POSS�VEL VERIFICAR TABELA DE PRE�OS. POR FAVOR CONTACTE O SUPORTE","OK","",16)
		Return .F.
	Endif

	Select * From ucrsFornComPrecos INNER Join Bi On Bi.ref = ucrsFornComPrecos.ref Into Cursor ucrsProdutosDoFornNaEncomenda Readwrite
	If Reccount("ucrsProdutosDoFornNaEncomenda")>0
		If uf_perguntalt_chama("Pretende atualizar documento com pre�os do Fornecedor (Condi��es Comerciais) ?","Sim","N�o", 64)

			Select Bi
			Go Top
			Scan For !Empty(Bi.ref)

				lcSQL = ""
				TEXT TO lcSQL TEXTMERGE NOSHOW
					Select
						top 1 pclfornec = ISNULL(tabelaPrecos.pcl,0)
						,ST.*
					FROM
						ST (nolock)
						left join tabelaPrecos (nolock) on st.ref = tabelaPrecos.ref  and tabelaPrecos.fornec = <<cabdoc.no>> and tabelaPrecos.fornestab = <<cabdoc.estab>>
					WHERE
						ST.REF = '<<Alltrim(BI.REF)>>'
				ENDTEXT
				If !uf_gerais_actGrelha("", "ucrsPrecosForn",lcSQL)
					uf_perguntalt_chama("N�O FOI POSS�VEL VERIFICAR TABELA DE PRE�OS. POR FAVOR CONTACTE O SUPORTE","OK","",16)
					Return .F.
				Endif

				Select ucrsPrecosForn
				lcprecocusto = Round(Iif(ucrsPrecosForn.pclfornec == 0,ucrsPrecosForn.EPCULT,ucrsPrecosForn.pclfornec),2)
				lcprecounitario = Round(Iif(ucrsPrecosForn.pclfornec == 0,ucrsPrecosForn.EPCULT,ucrsPrecosForn.pclfornec),2)

				Replace Bi.edebito		With lcprecounitario
				Replace Bi.eprorc		With lcprecounitario
				Replace Bi.EPCUSTO		With lcprecocusto
				Replace Bi.ettdeb		With lcprecounitario *Bi.qtt
				Replace Bi.ecustoind	With lcprecounitario
				Replace Bi.edebitoori	With lcprecounitario
				Replace Bi.u_upc		With lcprecocusto

				** UNI2QTT
				uf_documentos_actualizaUni2Qtt()
				uf_documentos_recalculaTotaisBI()

			Endscan
			uf_documentos_actualizaTotaisCAB()

		Endif
	Endif

	If Used("ucrsFornComPrecos")
		fecha("ucrsFornComPrecos")
	Endif
	If Used("ucrsPrecosForn")
		fecha("ucrsPrecosForn")
	Endif

Endfunc


** Cancelar
Function uf_documentos_sair

	uf_documentos_apagarTemp()

	DOCUMENTOS.PageFrame1.Page1.gridDoc.Refresh

	**
	DoEvents Force

	**
	If DOCUMENTOS.eventoref == .F. && se estiver a correr os eventos
		uf_documentos_EventosGridDoc(.F.)
	Endif

	* Verifica se o Documento esta em Modo de Edicao ou Introducao
	If myDocAlteracao == .T. Or myDocIntroducao == .T.

		If Type("IMPORTARDOC") != "U"
			uf_perguntalt_chama("DEVE FECHAR O PAINEL DE IMPORTA��O DE DOCUMENTOS ANTES DE CANCELAR O DOCUMENTO.","OK","",64)
			Return .F.
		Endif
		**
		If Type("CONFENCOMENDAS") != "U"
			uf_perguntalt_chama("DEVE FECHAR O PAINEL DE CONFER�NCIA DE ENCOMENDAS ANTES DE CANCELAR O DOCUMENTO.","OK","",64)
			Return .F.
		Endif
		**
		If Type("PESQFORNECEDORES") != "U"
			uf_perguntalt_chama("DEVE FECHAR O PAINEL DE PESQUISA DE FORNECEDORES ANTES DE CANCELAR O DOCUMENTO.","OK","",64)
			Return .F.
		Endif

		If Type("DOCUMENTOS.MyTimerCenc") = "O" && Conferencia de Encomendas
			If uf_perguntalt_chama("Ao cancelar a edi��o do documento, vai cancelar a confer�cia rob�tica. Pretende continuar?","Sim","N�o")
				DOCUMENTOS.MyTimerCenc.Enabled = .F.
			Else
				Return .T.
			Endif

		Endif


		If uf_perguntalt_chama("Quer mesmo cancelar o documento? Ir� perder as �ltimas altera��es feitas.","Sim","N�o")
			myDocIntroducao	= .F.
			myDocAlteracao	= .F.

			*DOCUMENTOS.menu.estado(1)
			uf_documentos_alternaMenu()

			uf_documentosControlaObj()

			**ultimo registo alterado
			uf_documentos_ultimoRegisto()

			**libertar a conferencia robotica
			uf_CONFROBOTICA_Chama(.T.,"1")

		Endif
	Else
		DOCUMENTOS.ContainerCab.documento.SetFocus
		uf_documentos_exit()
	Endif
Endfunc


**
Function uf_documentos_importLinhasFromTable
	Local lcSQL


	TEXT TO lcSQL NOSHOW TEXTMERGE
		SELECT * FROM information_schema.tables where TABLE_NAME = 'tempImport'
	ENDTEXT
	If !uf_gerais_actGrelha("", "ucrsVerificaTabelaImport",lcSQL)
		uf_perguntalt_chama("N�O FOI POSS�VEL A EXISTENCIA DA TABELA DE IMPORTA��O","OK","",16)
		Return .F.
	Endif


	TEXT TO lcSQL NOSHOW TEXTMERGE
		SElect * FROM tempImport
	ENDTEXT
	If !uf_gerais_actGrelha("", "ucrsDadosTabelaImport",lcSQL)
		uf_perguntalt_chama("N�O FOI POSS�VEL OBTER CONTEUDO DA TABELA FONTE DE DADOS","OK","",16)
		Return .F.
	Endif


	If Reccount("ucrsDadosTabelaImport")>0

		Select ucrsDadosTabelaImport
		mntotal=Reccount("ucrsDadosTabelaImport")

		regua(0,mntotal,"A PROCESSAR...",.F.)
		Select ucrsDadosTabelaImport
		Go Top
		Scan



			** actualizar regua **
			regua[1,recno("ucrsDadosTabelaImport"),"",.f.]

			If myTipoDoc == "FO"
				Select FN
				uf_documentos_criaNovaLinha(.F.,.T.)
				Select FN
				Replace FN.ref 		With 	Alltrim(ucrsDadosTabelaImport.ref)
				Replace FN.Design 	With 	Alltrim(ucrsDadosTabelaImport.Design)
				Replace FN.qtt 		With 	ucrsDadosTabelaImport.qtt

				uf_documentos_recalculaTotaisFN()
			Else
				Select Bi
				uf_documentos_criaNovaLinha(.F.,.T.)
				Select Bi
				Replace Bi.ref 		With 	Alltrim(ucrsDadosTabelaImport.ref)
				Replace Bi.Design 	With 	Alltrim(ucrsDadosTabelaImport.Design)
				Replace Bi.qtt 		With 	ucrsDadosTabelaImport.qtt

				uf_documentos_recalculaTotaisBI()
			Endif
		Endscan

		uf_documentos_actualizaTotaisCAB()

		regua(2)
	Endif

Endfunc


**
Function uf_documentos_recalculoDocForn
	Lparameters lcTabIva
	Local lcTaxaIva, lcSQL, lcSql2, lcExisteRegistos
	lcTaxaIva = 0
	Store "" To lcSQL, lcSql2
	lcExisteRegistos = .F.

	Select cabdoc
	Replace cabdoc.tabiva With lcTabIva


	Do Case
		Case myTipoDoc == "FO"
			** valida descontos financeiros por escal�o
			If (cabdoc.NUMINTERNODOC == 55 Or cabdoc.NUMINTERNODOC == 58)
				TEXT TO lcSql NOSHOW TEXTMERGE
					SELECT
						st.ref
						,descFin_pcl = ISNULL(case
										when pvporig<=0 or protocolo=1 then 0
										else ISNULL((select [desc]
													from fl_descontos fld (nolock)
													where fld.site_nr = <<mysite_nr>>
														and fld.nr_fl=<<cabdoc.no>> and fld.dep_fl=<<cabdoc.estab>>
														and fld.escalao = isnull((Select top 1 escalao from b_mrgRegressivas (nolock) Where pvporig between PVPmin and PVPmax order by versao desc),0)
												),0)
										end,0)
					from
						st (nolock)
						inner join fprod (nolock) on fprod.cnp=st.ref
					where
						st.ref in (
				ENDTEXT

				Select FN
				Go Top
				Scan For !Empty(FN.ref) Or !Empty(FN.oref)
					If lcExisteRegistos = .F.
						lcSQL = lcSQL + "'" + Alltrim(Iif(Empty(FN.ref),FN.oref,FN.ref)) + "'"
					Else
						lcSQL = lcSQL + ",'" + Alltrim(Iif(Empty(FN.ref),FN.oref,FN.ref)) + "'"
					Endif
					lcExisteRegistos=.T.
				Endscan

				If lcExisteRegistos = .T.
					lcSQL = lcSQL + ")"
					If uf_gerais_actGrelha("", "ucrsDescFinPclForn",lcSQL)
						Select FN
						Update FN Set FN.descFin_pcl = Iif(Isnull(ucrsDescFinPclForn.descFin_pcl),0,ucrsDescFinPclForn.descFin_pcl) From ucrsDescFinPclForn INNER Join FN On ucrsDescFinPclForn.ref=FN.ref Or ucrsDescFinPclForn.ref=FN.oref
					Endif
				Endif
			Endif

			** recalcula todas as linhas
			Select FN
			Go Top
			Scan
				uf_documentos_recalculaTotaisFN()
			Endscan

		Case myTipoDoc == "BO"
			Select Bi
			Go Top
			Scan
				uf_documentos_recalculaTotaisBI()
			Endscan

		Otherwise
			**
	Endcase

	uf_documentos_actualizaTotaisCAB()

Endfunc



**
Function uf_documentos_TabelaFixaIvaForn
	Lparameters lcTabIva
	Local lcTaxaIva, lcSQL
	lcTaxaIva = 0

	Select cabdoc
	Replace cabdoc.tabiva With lcTabIva

	Do Case
		Case myTipoDoc == "FO"
			Select FN
			Go Top
			Scan
				uf_documentos_recalculaTotaisFN()
			Endscan

		Case myTipoDoc == "BO"
			Select Bi
			Go Top
			Scan
				uf_documentos_recalculaTotaisBI()
			Endscan

		Otherwise
			**
	Endcase

	uf_documentos_actualizaTotaisCAB()

Endfunc



**
Function uf_documentos_exit
	** Fecha Cursores
	If Used("cabDoc")
		fecha("cabDoc")
	Endif
	If Used("BI")
		fecha("BI")
	Endif
	If Used("FN")
		fecha("FN")
	Endif
	If Used("TD")
		fecha("TD")
	Endif
	If Used("ucrsTempPesq")
		fecha("ucrsTempPesq")
	Endif
	If Used("ucrsvaliva")
		fecha("ucrsvaliva")
	Endif
	If Used("ucrsfndesclin")
		fecha("ucrsfndesclin")
	Endif

	&& Conf Robo
	If Used("uCrsRecRoboPend")
		fecha("uCrsRecRoboPend")
	Endif
	If Used("uCrsRecRoboMf")
		fecha("uCrsRecRoboMf")
	Endif
	If Used("uCrsDifEncRec")
		fecha("uCrsDifEncRec")
	Endif
	If Used("uCrsErro07")
		fecha("uCrsErro07")
	Endif
	If Used("uCrsDifMcr")
		fecha("uCrsDifMcr")
	Endif
	If Used("uCrsAtribuiLote")
		fecha("uCrsAtribuiLote")
	Endif
	If Used("uCrsCcer")
		fecha("uCrsCcer")
	Endif

	** Fecha paineis dependentes
	If Type("IMPORTARDOC") != "U"
		uf_IMPORTARDOC_sair()
	Endif
	**
	If Type("CONFENCOMENDAS") != "U"
		uf_CONFENCOMENDAS_sair()
	Endif
	**
	If Type("PESQFORNECEDORES") != "U"
		uf_PESQFORNECEDORES_sair()
	Endif
	**
	If Type("DADOSENTIDADE") != "U"
		uf_dadosentidade_sair()
	Endif
	**
	If Type("ORIGENSDESTINOS") != "U"
		uf_OrigensDestinos_sair()
	Endif
	**
	If Type("IMPRIMIRGERAIS") != "U"
		uf_IMPRIMIRGERAIS_sair()
	Endif

	If Type("RESERVAS") != "U"
		uf_reservas_sair()
	Endif

	If Type("RESERVACLSMS") != "U"
		uf_reservaclsms_sair()
	Endif

	If(Vartype("lcorigempesq") != "U")
		Release lcorigempesq
	Endif



	DOCUMENTOS.Hide
	DOCUMENTOS.Release
Endfunc



**uf_DOCUMENTOS_ImportarDadosOPL_XLS
Function uf_DOCUMENTOS_ImportarDadosOPL_XLS


	Local lcFilePath, lcLordem, lcColumnName, lcQttCursor, lcFicheiroStocks, lcNomePCLk
	Public lcFarmaciaSel

	lcFicheiroStocks = .F.
	lcFarmaciaSel = ""
	lcNomePCLk = ""

	Store '' To lcColumnName
	If myDocIntroducao == .F. And myDocAlteracao == .F.
		uf_perguntalt_chama("O documento deve estar em modo de edi��o!","OK","",64)
		Return .F.
	Endif

	lcLordem = 1

	lcFilePath = Getfile("xls","Ficheiro a importar")
	If !UF_GERAIS_xls2Cursor(lcFilePath,"relatorio_keiretsu_OPL","ucrsOPL")

		&& Vai tentar novo nome de ficheiro - stokcs
		If !UF_GERAIS_xls2Cursor(lcFilePath,"relatorio_keiretsu_distribuicao","ucrsOPL")
			uf_perguntalt_chama("N�o foi possivel ler o ficheiro indicado. Verifique as defini��es do ficheiro.","OK","",16)
			Return .F.
		Endif

		lcFicheiroStocks = .T. && este ficheiro tem uma estrutura diferente

	Endif

	If !Used("ucrsOPL")
		Return .F.
	Endif

	If Used("ucrsFarmaciasOPL")
		fecha("ucrsFarmaciasOPL")
	Endif

	Create Cursor ucrsFarmaciasOPL(farmacia c(254))
	Select ucrsOPL
	For i=1 To Fcount('ucrsOPL')
		If !Empty(Field(i,"ucrsOPL")) And (Like("FARM�CIA*",Field(i,"ucrsOPL")) Or Like("PARAF*",Field(i,"ucrsOPL")))
			Select ucrsFarmaciasOPL
			Append Blank
			Replace ucrsFarmaciasOPL.farmacia With Field(i,"ucrsOPL")
		Endif

		If Upper(Alltrim(Field(i,"ucrsOPL"))) == "PCLK"
			lcNomePCLk = "ucrsOPL.PCLK"
		Endif

		If Upper(Alltrim(Field(i,"ucrsOPL"))) == "PCL_K"
			lcNomePCLk = "ucrsOPL.PCL_K"
		Endif

	Next i

	uf_valorescombo_chama("lcFarmaciaSel", 0, "ucrsFarmaciasOPL", 2, "farmacia", "farmacia")

	If Empty(lcFarmaciaSel)
		Return .F.
	Endif


	lcQttCursor = "ucrsOPL." + Alltrim(lcFarmaciaSel)

	uf_documentos_criaNovaLinha(.F.,.T.)
	Select Bi
	Replace Bi.ref With ""
	Replace Bi.Design With Alltrim(Left(lcFarmaciaSel,60))
	Replace Bi.lordem With lcLordem + 1
	lcLordem = lcLordem +1

	If Used("ucrsLinhasInseridasOPL")
		fecha("ucrsLinhasInseridasOPL")
	Endif
	Create Cursor ucrsLinhasInseridasOPL (bistamp c(25), qtt N(9,2), edebito N(9,2))


	Select ucrsOPL
	Go Top
	Scan

		If lcFicheiroStocks == .F.

			If !Empty(ucrsOPL.cnp) And !Isnull(ucrsOPL.cnp) And !Isnull(&lcQttCursor)
				uf_documentos_criaNovaLinha(.F.,.T.)
				Select Bi
				Replace Bi.ref With Iif(Type("ucrsOPL.cnp")=="N", Str(ucrsOPL.cnp),ucrsOPL.cnp)

				Select ucrsLinhasInseridasOPL
				Append Blank
				Replace ucrsLinhasInseridasOPL.bistamp With Bi.bistamp
				Replace ucrsLinhasInseridasOPL.qtt With &lcQttCursor
				**
				If !Empty(lcNomePCLk)
					Replace ucrsLinhasInseridasOPL.edebito With Iif(Isnull(&lcNomePCLk),0,Round(&lcNomePCLk,2))
				Endif


				Select Bi
				Replace Bi.lordem With lcLordem + 1
				lcLordem = lcLordem +1


				** Actualiza Dados da Referencia
				uf_documentos_eventoref()

			Endif
		Else
			If !Empty(ucrsOPL.Produto) And !Isnull(ucrsOPL.Produto) And !Isnull(&lcQttCursor)
				uf_documentos_criaNovaLinha(.F.,.T.)
				Select Bi
				Replace Bi.ref With Iif(Type("ucrsOPL.Produto")=="N", Str(ucrsOPL.Produto),ucrsOPL.Produto)

				Select ucrsLinhasInseridasOPL
				Append Blank
				Replace ucrsLinhasInseridasOPL.bistamp With Bi.bistamp
				Replace ucrsLinhasInseridasOPL.qtt With &lcQttCursor

				If !Empty(lcNomePCLk)
					Replace ucrsLinhasInseridasOPL.edebito With Iif(Isnull(&lcNomePCLk),0,Round(&lcNomePCLk,2))
				Endif

				Select Bi
				Replace Bi.lordem With lcLordem + 1
				lcLordem = lcLordem +1

				** Actualiza Dados da Referencia
				uf_documentos_eventoref()

			Endif
		Endif

		Select ucrsOPL
	Endscan

	Select Bi
	Go Top
	Scan
		Select ucrsLinhasInseridasOPL
		Go Top
		Scan For Bi.bistamp == ucrsLinhasInseridasOPL.bistamp

			Select Bi
			Replace Bi.qtt With ucrsLinhasInseridasOPL.qtt
			Replace Bi.edebito With ucrsLinhasInseridasOPL.edebito
			uf_documentos_recalculaTotaisBI()

		Endscan

	Endscan

	uf_documentos_actualizaTotaisCAB()

	Select Bi
	Go Top

	uf_perguntalt_chama("Importa��o concluida. Verifique o documento.","OK","",64)

Endfunc


**
Function uf_DOCUMENTOS_ImportarDados_XLS
	Local lcFilePath, lcLordem, lcQttZero, lcShouldImportXls
	Store '' To lcColumnName
	Store .F. To lcShouldImportXls

	If myDocIntroducao == .F. And myDocAlteracao == .F.
		uf_perguntalt_chama("O documento deve estar em modo de edi��o!","OK","",64)
		Return .F.
	Endif

	If !uf_perguntalt_chama("Nome das Colunas Permitidas: REF, QTT, PRECO, DESCONTO. (Obrigat�rias: REF, QTT). Pretende importar linhas com quantidades a Zero?","Sim","N�o",64)
		lcQttZero = .F.
	Else
		lcQttZero = .T.
	Endif

	lcLordem = 1

	lcFilePath = uf_gerais_getFile(Alltrim(uf_gerais_getUmvalor("B_terminal", "pathReport", "terminal = '" + Alltrim(myTerm) + "'")), 'xls')
	If Empty(lcFilePath)
		Return .F.
	Endif

	uf_gerais_meiaRegua("A validar dados do XLS...")

	If !UF_GERAIS_xls2Cursor(lcFilePath, "","ucrsDados", .T.)
		regua(2)
		uf_perguntalt_chama("N�o foi possivel ler o ficheiro indicado.","OK","",16)
		Return .F.
	Endif

	regua(2)

	mntotal=Reccount("ucrsDados")

	**lcShouldImportXls=uf_documentos_verificaLimiteLinhas(mntotal)
	**
	**IF(lcShouldImportXls==.f.)
	**	RETURN .f.
	**ENDIF

	Local uv_camposErro
	Store '' To uv_camposErro

	If Type("ucrsDados.qtt") <> "N" And Type("ucrsDados.qtt") <> "U"
		uv_camposErro = "Qtt"
	Endif

	If Type("ucrsDados.preco") <> "N" And Type("ucrsDados.preco") <> "U"
		uv_camposErro = uv_camposErro + Iif(Empty(uv_camposErro), '', ',') +  "Preco"
	Endif

	If !Empty(uv_camposErro)

		Local uv_msgErro

		uv_msgErro = "Os dados presentes " + Iif(At(',', uv_camposErro) <> 0, 'nas colunas ', 'na coluna ') + Alltrim(uv_camposErro) + " n�o est�o definidos como num�rico."

		regua(2)
		uf_perguntalt_chama(Alltrim(uv_msgErro) + Chr(13) + "Por favor edite o ficheiro a importar para o formato correto.","OK","",16)
		Return .F.
	Endif

	regua(0,mntotal,"A PROCESSAR...",.F.)

	If Type("ucrsDados.REF") == "U"
		regua(2)
		uf_perguntalt_chama("N�o foi possivel encontrar a coluna REF no ficheiro.","OK","",16)
		Return .F.
	Endif

	regua(0,Recno(),"A PROCESSAR...",.F.)


	Select ucrsDados
	Go Top

	Scan For !Isnull(ucrsDados.ref)

		lcRef = Iif(Type("ucrsDados.ref") = "N", ASTR(ucrsDados.ref), Alltrim(ucrsDados.ref))

		If lcQttZero == .F. And ucrsDados.qtt == 0
			&& N�o importa linhas com quantidades a Zero
		Else
			fecha("uCrsSTFichaTemp")
			** Verifica se a referencia existe
			lcSQL = ''
			TEXT TO lcSQL TEXTMERGE NOSHOW
				select ref from st (nolock) where ref = '<<ALLTRIM(lcRef)>>' and site_nr =  <<mySite_nr>>
			ENDTEXT

			If !uf_gerais_actGrelha("", "uCrsSTFichaTemp", lcSQL)
				regua(2)
				uf_perguntalt_chama("MAS N�O FOI POSSIVEL VERIFICAR O C�DIGO DO ARTIGO:" + Alltrim(lcRef) + ". POR FAVOR CONTACTE O SUPORTE.","OK","", 16)
				Return .F.
			Endif


			** sen�o existe a ficha na loja tenta criar e colta a preencher
			If Reccount("uCrsSTFichaTemp") == 0
				fecha("uCrsSTFichaTemp")
				Local lcSQLFichasDados

				TEXT TO lcSQLFichasDados NOSHOW TEXTMERGE
					exec up_stocks_criaSt '<<ALLTRIM(lcRef)>>', <<mysite_nr>>
				ENDTEXT

				If uf_gerais_actGrelha("", "", lcSQLFichasDados )

				Endif


				Local lcSQLFicha
				lcSQLFicha = ''
				TEXT TO lcSQLFicha TEXTMERGE NOSHOW
					select ref from st (nolock) where ref = '<<ALLTRIM(lcRef)>>' and site_nr =  <<mySite_nr>>
				ENDTEXT


				If !uf_gerais_actGrelha("", "uCrsSTFichaTemp", lcSQLFicha)
					regua(2)
					uf_perguntalt_chama("MAS N�O FOI POSSIVEL VERIFICAR O C�DIGO DO ARTIGO:" + Alltrim(lcRef) + ". POR FAVOR CONTACTE O SUPORTE.","OK","", 16)
					Return .F.
				Endif



				If(Used("uCrsSTFichaTemp"))
					Select uCrsSTFichaTemp
					Go Top
				Endif


			Endif


			If Used("uCrsSTFichaTemp") And Reccount("uCrsSTFichaTemp") > 0

				uf_documentos_criaNovaLinha(.F.,.T.)
				&& comentado porque est� a dar erros com refs de 6 digitos e n�o h� necessidade disto aqui.
				*!*					IF len(ALLTRIM(STR(ucrsDados.ref))) == 6
				*!*						&&lcRef = uf_stocks_checkDigitoCNP(ALLTRIM(STR(ucrsDados.ref)))
				*!*					ELSE
				*!*						lcRef = ALLTRIM(STR(ucrsDados.ref))
				*!*					ENDIF


				If myTipoDoc == "BO"
					Select Bi
					Replace Bi.ref With lcRef

					** Actualiza Dados da Referencia
					uf_documentos_eventoref()

					Select Bi
					Replace Bi.lordem With lcLordem + 1

					If Type("ucrsDados.QTT") != "U"
						If !Isnull(ucrsDados.qtt)
							Replace Bi.qtt With ucrsDados.qtt
						Else
							Replace Bi.qtt With 0
						Endif
					Endif
					If Type("ucrsDados.PRECO") != "U"
						If !Isnull(ucrsDados.PRECO)
							Replace Bi.edebito With ucrsDados.PRECO
						Else
							Replace Bi.edebito With 0
						Endif
					Endif
					If Type("ucrsDados.desconto") != "U"
						If !Isnull(ucrsDados.desconto)
							Replace Bi.desconto With ucrsDados.desconto
						Else
							Replace Bi.desconto With 0
						Endif
					Endif
					If Upper(Alltrim(cabdoc.Doc)) = 'TAB. COMPARTICIPA��ES'
						If Type("ucrsDados.OBS") != "U"
							If !Isnull(ucrsDados.OBS)
								Replace Bi.lobs With ucrsDados.OBS
							Else
								Replace Bi.lobs With ''
							Endif
						Endif
					Endif
					lcLordem = lcLordem +1
				Endif

				If myTipoDoc == "FO"
					Select FN
					Replace FN.ref With lcRef

					** Actualiza Dados da Referencia
					uf_documentos_eventoref()

					Select FN
					Replace FN.lordem With lcLordem + 1

					If Type("ucrsDados.QTT") != "U"
						If !Isnull(ucrsDados.qtt)
							Replace FN.qtt With ucrsDados.qtt
						Else
							Replace FN.qtt With 0
						Endif
					Endif
					If Type("ucrsDados.PRECO") != "U"
						If !Isnull(ucrsDados.PRECO)
							Replace FN.epv With ucrsDados.PRECO
						Else
							Replace FN.epv With 0
						Endif
					Endif
					If Type("ucrsDados.desconto") != "U"
						If !Isnull(ucrsDados.desconto)
							Replace FN.desconto With ucrsDados.desconto
						Else
							Replace FN.desconto With 0
						Endif
					Endif
					lcLordem = lcLordem +1
				Endif
			Endif
		Endif

		Select ucrsDados
	Endscan

	If myTipoDoc == "BO"
		Select Bi
		Go Top
		Scan
			uf_documentos_recalculaTotaisBI()
		Endscan
	Endif

	If myTipoDoc == "FO"
		Select FN
		Go Top
		Scan
			uf_documentos_recalculaTotaisFN()
		Endscan
	Endif

	uf_documentos_actualizaTotaisCAB()

	If myTipoDoc == "BO"
		Select Bi
		Go Top
	Endif
	If myTipoDoc == "FO"
		Select FN
		Go Top
	Endif

	regua(2)
	uf_perguntalt_chama("Importa��o concluida. Verifique o documento.","OK","",64)

Endfunc


**
Function uf_documentos_atualizarCondComercForn

	If myTipoDoc != "BO"
		uf_perguntalt_chama("N�o � possivel aplicar esta funcionalidade neste documento.","OK","",32)
		Return .F.
	Endif

	If myDocAlteracao == .F. And myDocIntroducao == .F.
		uf_documentos_alterarDoc()
	Endif

	Select Bi
	Go Top
	Scan

		lcSQL = ""
		TEXT TO lcSQL TEXTMERGE NOSHOW
			exec up_keiretsu_Documentos_ActualizaCondComercForn '<<ALLTRIM(bi.ref)>>'
		ENDTEXT

		If !uf_gerais_actGrelha("", "ucrsActualizaCondComercForn", lcSQL)
			uf_perguntalt_chama("N�O FOI POSS�VEL VERIFICAR A LISTAGEM DE LOTES. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			Return .F.
		Endif

		If Reccount("ucrsActualizaCondComercForn")>0

			Select ucrsActualizaCondComercForn
			Select Bi
			Replace Bi.edebito With ucrsActualizaCondComercForn.pclk



			uf_documentos_recalculaTotaisBI()


		Endif
	Endscan


	uf_documentos_actualizaTotaisCAB()

Endfunc


&& Mostra alerta caso n�o exista o producto na factura

Function uf_documentos_deveCriarNovaLinha
	Lparameters lcCabDoc

	Local lcTemEncomenda, lcPos
	Store .F. To  lcTemEncomenda



	If(!Used("FN"))
		Return .T.
	Endif

	lcPos=Recno("FN")
	&&valida se alguma vem de uma encomenda importada
	Select FN
	Scan

		If(Type("fn.BISTAMP")!="U")
			If(!Empty(FN.bistamp))
				lcTemEncomenda = .T.
			Endif
		Endif
	Endscan


	Select FN
	Try
		Go lcPos
	Catch
	Endtry


	If(lcTemEncomenda ==.F.)
		Return .T.
	Endif


	If Alltrim(lcCabDoc) == "V/Factura" Or Alltrim(cabdoc.Doc) == "V/Guia Transp." Or Alltrim(lcCabDoc) == "V/Factura Med."

		If uf_perguntalt_chama("Este produto n�o existe na(s) encomenda(s) importada(s), pretende adicionar?","Sim","N�o")
			Return .T.
		Else
			Return .F.
		Endif

	Else

		Return .T.

	Endif

	Return .T.


Endfunc


**
Function uf_documentos_scanner

	If myDocIntroducao == .F. And myDocAlteracao == .F.
		Return .F.
	Endif


	If (myTipoDoc == "FO" And Alltrim(FN.ref)=='VALORMED') Or (myTipoDoc == "BO" And Alltrim(Bi.ref)=='VALORMED')
		If myTipoDoc == "FO"
			Replace FN.LOTE With Strtran(DOCUMENTOS.txtScanner.Value,Chr(39),'')

			Select FN
			uf_documentos_editarTemp(FN.fnstamp)
		Else

			Replace Bi.LOTE With Strtran(DOCUMENTOS.txtScanner.Value,Chr(39),'')

			Select Bi
			uf_documentos_editarTemp(Bi.bistamp)

		Endif


		DOCUMENTOS.PageFrame1.Page1.gridDoc.Refresh



	Else


		&&
		If Empty(Alltrim(DOCUMENTOS.txtScanner.Value))
			Return .F.
		Endif

		&& Se o numero do documento estiver va<io a leitura do scanner � feita para este campo
		If Empty(Alltrim(DOCUMENTOS.ContainerCab.NUMDOC.Value)) And myTipoDoc == "FO"
			DOCUMENTOS.txtScanner.Value= Strtran(DOCUMENTOS.txtScanner.Value,Chr(39),'')
			DOCUMENTOS.ContainerCab.NUMDOC.Value = Alltrim(DOCUMENTOS.txtScanner.Value)

			Return .F.
		Endif

		Local lcRef, lcCursor, lcLote, lcCriouLinha, lcPosicaoStamp, uv_dMatrix, lcForcaNovaLinha
		lcLote = ""
		lcPosicaoStamp = ""
		lcCriouLinha = .F.

		lcCursor = Iif(myTipoDoc=="FO","FN","BI")
		uv_dMatrix = Alltrim(DOCUMENTOS.txtScanner.Value)

		&& Procurar na FN a Refer�ncia selecionada
		DOCUMENTOS.txtScanner.Value= Strtran(DOCUMENTOS.txtScanner.Value,Chr(39),'')

		lcRef = Alltrim(DOCUMENTOS.txtScanner.Value)

		If Len(Alltrim(lcRef))>20

			Local lcEANID, lcDtValID, lcLoteID, lcIdID, lcRefID, lcRefPaisID, lcRefTempID, lcPosDelimiterID, lcConfereID, lcleituracampos, lcSn
			Store '' To lcEANID, lcDtValID, lcLoteID, lcIdID, lcRefID, lcRefPaisID, lcleituracampos, lcSn
			Store 0 To lcPosDelimiterID

			uv_dMatrix = Alltrim(lcRef)

			lcRefTempID = uf_gerais_remove_last_caracter_by_char(lcRef,'#')
			lcConfereID = uf_gerais_remove_last_caracter_by_char(lcRef,'#')

			If uf_gerais_retorna_campos_datamatrix_sql(lcRefTempID)

				Select ucrDataMatrixTemp
				If ucrDataMatrixTemp.valido

					lcDtValID = uf_gerais_retornaApenasAlphaNumericos(ucrDataMatrixTemp.Data)
					lcRefID  = uf_gerais_retornaApenasAlphaNumericos(ucrDataMatrixTemp.ref)
					lcRefPaisID = ucrDataMatrixTemp.countryCode
					lcEANID = ucrDataMatrixTemp.pc
					lcLoteID = ucrDataMatrixTemp.LOTE
					lcSn = ucrDataMatrixTemp.sn

					Select fi_trans_info
					Go Top

					Locate For uf_gerais_compstr(lcEANID, fi_trans_info.productcode) And uf_gerais_compstr(lcSn, fi_trans_info.packserialnumber)

					If Found()

						uf_perguntalt_chama("Este pack j� foi conferido.", "OK", "", 64)
						Return .F.

					Else

						If Alltrim(cabdoc.Doc) == "V/Factura" Or Alltrim(cabdoc.Doc) == "V/Guia Transp." Or Alltrim(cabdoc.Doc) == "V/Factura Med."

							lcForcaNovaLinha = .F.

						Else

							lcForcaNovaLinha = .T.

						Endif

					Endif

				Endif

			Endif

			**ENDIF

			If Empty(lcRefID)
				**uf_gerais_notif("Medicamento conferido." + chr(13) + "Datamatrix n�o desativado.", 3000)
				**uf_gerais_registaerrolog("Erro ao ler o datamatrix no uf_atendimento_scanner:  " +  uv_dMatrix, "datamatrix")

				lcRefID = uf_gerais_getFromString(uv_dMatrix, '714', 7, .T.)
				If Len(lcRefID) <> 7
					uf_gerais_registaerrolog("Artigo n�o encontrado: " +  uv_dMatrix, "datamatrixCnp")
					uf_perguntalt_chama("ERRO NA LEITURA OU C�DIGO DE BARRAS INV�LIDO.", "OK", "", 64)
					Return .F.
				Endif

			Endif

			lcRef = Alltrim(lcRefID)

			If Empty(lcRef) Or Isnull(lcRef)
				uf_perguntalt_chama("N�o foi possivel validar o datamatrix", "", "OK", 16)
				Return .F.
			Endif

		Endif

		DOCUMENTOS.LockScreen = .T.

		&& Coloca na PageFrame de Conferencia
		If Alltrim(cabdoc.Doc) == "V/Factura" Or Alltrim(cabdoc.Doc) == "V/Guia Transp."
			DOCUMENTOS.PageFrame1.Page1.detalhest1.lbl12.Click
		Endif

		&&
		With DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.Page12.PageFrame1
			If .ActivePage == 1
				lcLote = Alltrim(.Page1.Container1.txtLoteConf.Value)
			Else
				lcLote = Alltrim(.Page2.Container2.txtLoteConf.Value)
			Endif
		Endwith

		**
		lcSQL = ''
		TEXT TO lcsql Noshow Textmerge
			select top 1
				st.ref
				,st.design
				,st.epv1
				,st.epcusto
				,st.stock
				,st.validade
				,st.marg1
				,st.inactivo
				,st.usalote
				,psico = ISNULL(fprod.psico,convert(bit,0))
				,benzo = ISNULL(fprod.benzo ,convert(bit,0))
			from
					st (nolock)
				left join bc (nolock) on bc.ref=st.ref and bc.site_nr = <<mySite_nr>>
				left join fprod (nolock) on st.ref = fprod.cnp
			where
				(st.ref='<<Alltrim(lcRef)>>'
				OR bc.codigo='<<Alltrim(lcRef)>>'
				OR st.codigo='<<Alltrim(lcRef)>>')
				AND st.site_nr=<<mySite_nr>>
		ENDTEXT

		If uf_gerais_actGrelha("",[uCrsGetRef],lcSQL) And Reccount("uCrsGetRef")>0
			lcRef = Alltrim(uCrsGetRef.ref)
		Else

			DOCUMENTOS.LockScreen = .F.

			If uf_perguntalt_chama("Desculpe, mas essa refer�ncia n�o existe... Quer introduzir uma nova refer�ncia?"+ Chr(13) + "Nota - Vai abrir um novo artigo ou servi�o.","Sim","N�o")

				If !(Type("STOCKS")=="U") And !(STOCKS.Caption=="Stocks e Servi�os")
					uf_perguntalt_chama("N�O � POSS�VEL REALIZAR ESTA AC��O PORQUE O ECR� GEST�O DE STOCKS EST� EM MODO DE EDI��O. POR FAVOR TERMINE ESTA OPERA��O E TENTE NOVAMENTE","OK","",48)
					Return .F.
				Else

					lcCursor = Iif(myTipoDoc=="FO","FN","BI")

					lcPosicaoStamp = uf_documentos_criaNovaLinha(.F.,.T.)


					Select uCrsGetRef
					Select &lcCursor
					Replace &lcCursor..ref With lcRef

					uf_stocks_chama('')
					uf_stocks_novo()
					Select ST

					Replace ST.ref With Alltrim(lcRef)

					uf_documento_conferereferencia(uv_dMatrix, lcPosicaoStamp)

					STOCKS.Refresh

					Return .F.
				Endif

			Else
				DOCUMENTOS.LockScreen = .F.
				Return .F.
			Endif

		Endif

		lcCursor = Iif(myTipoDoc=="FO","FN","BI")

		Select &lcCursor
		Go Top
		Locate For Upper(Alltrim(&lcCursor..ref)) == Alltrim(Upper(lcRef));
			AND (Upper(Alltrim(&lcCursor..LOTE)) == Alltrim(Upper(lcLote)) Or &lcCursor..usaLote == .F.)
		If !Found() Or lcForcaNovaLinha

			If(uf_documentos_deveCriarNovaLinha(cabdoc.Doc))

				lcPosicaoStamp = uf_documentos_criaNovaLinha(.F.,.T.)

				Select uCrsGetRef
				Select &lcCursor
				Replace &lcCursor..ref With lcRef

				If myTipoDoc == "FO"
					lcPosicaoStamp = FN.fnstamp
				Endif
				If myTipoDoc == "BO"
					lcPosicaoStamp = Bi.bistamp
				Endif

				uf_documento_conferereferencia(uv_dMatrix, lcPosicaoStamp)

				uf_documentos_eventoref()

				lcCriouLinha = .T.

			Endif

		Else && produto na encomenda
			If myTipoDoc == "FO"
				lcPosicaoStamp = FN.fnstamp
			Endif
			If myTipoDoc == "BO"
				lcPosicaoStamp = Bi.bistamp
			Endif

			uf_documento_conferereferencia(uv_dMatrix, lcPosicaoStamp)

		Endif

		&&Produto encontrado na encomenda, usa a primeira linha que encontrar
		Select &lcCursor
		If Alltrim(cabdoc.Doc) == "V/Factura" Or Alltrim(cabdoc.Doc) == "V/Guia Transp." Or Alltrim(cabdoc.Doc) == "V/Factura Med."

			If !Empty(lcPosicaoStamp)
				If myTipoDoc == "FO"
					Select FN
					Locate For Alltrim(FN.fnstamp) == Alltrim(lcPosicaoStamp)
				Endif
				If myTipoDoc == "BO"
					Select Bi
					Locate For Alltrim(Bi.bistamp) == Alltrim(lcPosicaoStamp)
				Endif
			Endif

			If lcCriouLinha = .T.


				Select &lcCursor
				Replace &lcCursor..qtrec With 1
				Replace &lcCursor..qtt With 1




				If Type("lcDtValID") <> "U" And Not Isnull(lcDtValID) And !Empty(lcDtValID)
					uv_lastDay 		= ASTR(Day(Gomonth(Date(Val('20' + Left(Alltrim(lcDtValID),2))	, Val(Right(Left(Alltrim(lcDtValID),4),2)), 1),1)-1))

					Do Case
						Case Len(lcDtValID) = 4
							uv_dtVal = '20' + lcDtValID + uv_lastDay

						Otherwise
							uv_dtVal = '20' + Left(lcDtValID,4) + uv_lastDay

					Endcase

					uv_data 	= uf_gerais_getUmvalor("empresa", "CONVERT(datetime, '" + Alltrim(uv_dtVal) + "')")
					uv_dataST 	= uf_gerais_getUmvalor("st", "validade", "ref = '"+&lcCursor..ref+"' and site_nr =" + Str(mySite_nr) )

					Do Case
						Case Alltrim(&lcCursor..u_dtval) == '190001'
							Replace &lcCursor..u_validade With uv_data
							Replace &lcCursor..u_dtval With ASTR(Year(uv_data)) + Right('00' + ASTR(Month(uv_data)),2)

						Case &lcCursor..u_stockact <= 0
							Replace &lcCursor..u_validade With uv_data
							Replace &lcCursor..u_dtval With ASTR(Year(uv_data)) + Right('00' + ASTR(Month(uv_data)),2)

						Case Val(&lcCursor..u_dtval) > Val('20' + Left(lcDtValID,4))
							Replace &lcCursor..u_validade With uv_data
							Replace &lcCursor..u_dtval With ASTR(Year(uv_data)) + Right('00' + ASTR(Month(uv_data)),2)

						Case Val(&lcCursor..u_dtval) <= Val('20' + Left(lcDtValID,4))
							Replace &lcCursor..u_validade With uv_dataST
							Replace &lcCursor..u_dtval With ASTR(Year(uv_data)) + Right('00' + ASTR(Month(uv_data)),2)


					Endcase





					*!*	                    uv_data 	= uf_gerais_getUmValor("empresa", "CONVERT(datetime, '" + ALLTRIM(uv_dtVal) + "')")
					*!*	                    uv_dataST 	= uf_gerais_getUmValor("st", "validade", "ref = '"+&lcCursor..ref+"'")
					*!*	                   	Mess
					*!*						DO CASE
					*!*							CASE uv_dataST == ctod('1900/01/01')
					*!*								replace &lcCursor..u_validade WITH uv_data
					*!*	                     		replace &lcCursor..u_dtval with ASTR(YEAR(uv_data)) + RIGHT('00' + ASTR(MONTH(uv_data)),2)

					*!*							CASE &lcCursor..u_stockAct <= 0
					*!*
					*!*								replace &lcCursor..u_validade WITH uv_data
					*!*	                            replace &lcCursor..u_dtval with ASTR(YEAR(uv_data)) + RIGHT('00' + ASTR(MONTH(uv_data)),2)

					*!*							CASE year(uv_dataST) > VAL('20' + LEFT(lcDtValID,4)) AND ALLTRIM(&lcCursor..u_dtval) != '190001' AND &lcCursor..u_stockAct >= 0
					*!*
					*!*								replace &lcCursor..u_validade WITH uv_data
					*!*	                            replace &lcCursor..u_dtval with ASTR(YEAR(uv_data)) + RIGHT('00' + ASTR(MONTH(uv_data)),2)
					*!*
					*!*
					*!*							CASE year(uv_dataST) < VAL('20' + LEFT(lcDtValID,2)) AND ALLTRIM(&lcCursor..u_dtval) != '190001' AND &lcCursor..u_stockAct >= 0
					*!*
					*!*								replace &lcCursor..u_validade WITH uv_dataSt
					*!*	                            replace &lcCursor..u_dtval with ASTR(YEAR(uv_data)) + RIGHT('00' + ASTR(MONTH(uv_data)),2)

					*!*						ENDCASE




				Endif
			Else

				Local lcPosLin

				Select &lcCursor
				lcPosLin = Recno("&lcCursor")
				Replace &lcCursor..qtrec With &lcCursor..qtrec + 1
				Replace &lcCursor..qtt With &lcCursor..qtrec

				If Type("lcDtValID") <> "U" And Not Isnull(lcDtValID) And !Empty(lcDtValID)

					uv_lastDay = ASTR(Day(Gomonth(Date(Val('20' + Left(Alltrim(lcDtValID),2)), Val(Right(Left(Alltrim(lcDtValID),4),2)), 1),1)-1))

					Do Case
						Case Len(lcDtValID) = 4
							uv_dtVal = '20' + lcDtValID + uv_lastDay

						Otherwise
							uv_dtVal = '20' + Left(lcDtValID,4) + uv_lastDay

					Endcase

					uv_data = uf_gerais_getUmvalor("empresa", "CONVERT(datetime, '" + Alltrim(uv_dtVal) + "')")

					uv_dataST 	= uf_gerais_getUmvalor("st", "validade", "ref = '"+&lcCursor..ref+"' and site_nr =" + Str(mySite_nr) )

					Do Case
						Case uv_dataST == Ctod('1900/01/01')
							Replace &lcCursor..u_validade With uv_data
							Replace &lcCursor..u_dtval With ASTR(Year(uv_data)) + Right('00' + ASTR(Month(uv_data)),2)

						Case &lcCursor..u_stockact <= 0
							Replace &lcCursor..u_validade With uv_data
							Replace &lcCursor..u_dtval With ASTR(Year(uv_data)) + Right('00' + ASTR(Month(uv_data)),2)

						Case Year(uv_dataST) > Val('20' + Left(lcDtValID,2))  And &lcCursor..u_stockact >= 0
							Replace &lcCursor..u_validade With uv_data
							Replace &lcCursor..u_dtval With ASTR(Year(uv_data)) + Right('00' + ASTR(Month(uv_data)),2)


						Case Year(uv_dataST) <= Val('20' + Left(lcDtValID,4))  And &lcCursor..u_stockact >= 0
							Replace &lcCursor..u_validade With uv_dataST
							Replace &lcCursor..u_dtval With ASTR(Year(uv_data)) + Right('00' + ASTR(Month(uv_data)),2)

					Endcase


				Endif

			Endif

			If &lcCursor..usaLote == .T. And !Empty(Alltrim(Upper(lcLote)))
				Replace &lcCursor..LOTE With Alltrim(Upper(lcLote))
			Endif
		Else
			If lcCriouLinha == .F.
				Select &lcCursor
				Replace &lcCursor..qtt With &lcCursor..qtt + 1
			Endif
		Endif

		If myTipoDoc == "FO"
			uf_documentos_recalculaTotaisFN()
			lcPosicaoStamp = FN.fnstamp
		Endif
		If myTipoDoc == "BO"
			uf_documentos_recalculaTotaisBI()
			lcPosicaoStamp = Bi.bistamp
		Endif

		uf_documentos_actualizaTotaisCAB()

		If !Empty(lcPosicaoStamp)
			If myTipoDoc == "FO"
				Select FN
				Locate For Alltrim(FN.fnstamp) == Alltrim(lcPosicaoStamp)
			Endif
			If myTipoDoc == "BO"
				Select Bi
				Locate For Alltrim(Bi.bistamp) == Alltrim(lcPosicaoStamp)
			Endif
		Endif

		Select &lcCursor
		DOCUMENTOS.PageFrame1.Page1.gridDoc.Refresh
		DOCUMENTOS.PageFrame1.Page1.gridDoc.Click
		DOCUMENTOS.PageFrame1.Page1.gridDoc.SetFocus

		**
		DOCUMENTOS.txtScanner.Value = ""

		**
		DOCUMENTOS.PageFrame1.Page1.gridDoc.Refresh
		DOCUMENTOS.PageFrame1.Page1.gridDoc.AfterRowColChange
		DOCUMENTOS.PageFrame1.Page1.detalhest1.Refresh
		uf_documentos_controlaAltertaDetalheSt()

		**
		If DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.ActivePage = 12
			With DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.Page12.PageFrame1
				If .ActivePage == 1
					.Page1.Container1.txtQtRec.Refresh
					.Page1.Container1.txtQtRec.SetFocus
				Else
					.Page2.Container2.txtQtRec.Refresh
					.Page2.Container2.txtQtRec.SetFocus
				Endif
			Endwith
		Endif

		If  Alltrim(myTipoDoc) == "BO"
			Select Bi
			uf_documentos_editarTemp(Bi.bistamp)
		Else
			Select FN
			uf_documentos_editarTemp(FN.fnstamp)
		Endif


		DOCUMENTOS.LockScreen = .F.

		If lcCriouLinha = .F.
			If myTipoDoc == "FO"
				Select FN
				Try
					Go lcPosLin
				Catch
				Endtry
			Endif
			If myTipoDoc == "BO"
				Select Bi
				Try
					Go lcPosLin
				Catch
				Endtry
			Endif
		Endif

	Endif

Endfunc



**
Function uf_documentos_controlaAltertaDetalheSt
	Local lcTextoAlerta, lcEPCP, lcMBPCP
	lcTextoAlerta = ""
	Store 0 To  lcEPCP, lcMBPCP


	&& Visibilidade Campo Lote
	With DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.Page12.PageFrame1.Page1.Container1
		Select FN
		If !Empty(FN.usaLote)

			.txtLote.Visible = .T.

			If myDocAlteracao != .F. Or myDocIntroducao != .F.
				.LabelLoteConf.Visible = .T.
				.txtLoteConf.Visible = .T.
			Endif
		Else

			.txtLote.Visible = .F.
			.LabelLoteConf.Visible = .F.
			.txtLoteConf.Visible = .F.
		Endif

	Endwith

	With DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.Page12.PageFrame1.Page2.Container2
		Select FN
		If !Empty(FN.usaLote)

			.txtLote.Visible = .T.
			If myDocAlteracao != .F. Or myDocIntroducao != .F.
				.LabelLoteConf.Visible = .T.
				.txtLoteConf.Visible = .T.
			Endif
		Else

			.txtLote.Visible = .F.
			.LabelLoteConf.Visible = .F.
			.txtLoteConf.Visible = .F.
		Endif
	Endwith
	**********************************

	&& Quantidade Recebida
	With DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.Page12.PageFrame1.Page1.Container1
		Select FN
		If FN.qtrec != FN.qtt

			If !Empty(lcTextoAlerta)
				lcTextoAlerta = lcTextoAlerta + Chr(13) + Chr(13)
			Endif
			lcTextoAlerta = lcTextoAlerta + "- Qt.Rec."

			.txtQtRec.BackColor = Rgb[232,76,61]
			.txtQtRec.DisabledBackColor = Rgb[232,76,61]
			.txtQtRec.ForeColor = Rgb(255,255,255)
		Else
			Select FN
			If FN.qtrec != FN.u_qttenc And !Empty(FN.bistamp)

				If !Empty(lcTextoAlerta)
					lcTextoAlerta = lcTextoAlerta + Chr(13) + Chr(13)
				Endif
				lcTextoAlerta = lcTextoAlerta + "- Qt.Enc."

				.txtQtRec.BackColor = Rgb[238,142,56]
				.txtQtRec.DisabledBackColor = Rgb[238,142,56]
				.txtQtRec.ForeColor = Rgb(255,255,255)
			Else
				.txtQtRec.BackColor = Rgb(191,223,223)
				.txtQtRec.DisabledBackColor = Rgb[255,255,255]
				.txtQtRec.ForeColor = Rgb(0,0,0)
			Endif
		Endif
	Endwith

	With DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.Page12.PageFrame1.Page2.Container2
		Select FN
		If FN.qtrec != FN.qtt

			.txtQtRec.BackColor = Rgb[232,76,61]
			.txtQtRec.DisabledBackColor = Rgb[232,76,61]
			.txtQtRec.ForeColor = Rgb(255,255,255)
		Else

			Select FN
			If FN.qtrec != FN.u_qttenc And !Empty(FN.bistamp)

				.txtQtRec.BackColor = Rgb[238,142,56]
				.txtQtRec.DisabledBackColor = Rgb[238,142,56]
				.txtQtRec.ForeColor = Rgb(255,255,255)
			Else
				.txtQtRec.BackColor = Rgb(191,223,223)
				.txtQtRec.DisabledBackColor = Rgb[255,255,255]
				.txtQtRec.ForeColor = Rgb(0,0,0)
			Endif
		Endif
	Endwith

	With DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.Page12.PageFrame1.Page2.Container2
		Select uCrsConferenciaRef
		Select FN
		If uCrsConferenciaRef.pvp != FN.U_PVP And !Empty(FN.U_PVP)
			.txtPVP.BackColor = Rgb[232,76,61]
			.txtPVP.DisabledBackColor = Rgb[232,76,61]
			.txtPVP.ForeColor = Rgb(255,255,255)
		Else
			.txtPVP.BackColor = Rgb(191,223,223)
			.txtPVP.DisabledBackColor = Rgb[255,255,255]
			.txtPVP.ForeColor = Rgb(0,0,0)
		Endif
	Endwith

	&& TOTAL LINHA
	With DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.Page12.PageFrame1.Page1.Container1
		Select uCrsDetalheEncomendaForn
		Select FN
		If uCrsDetalheEncomendaForn.ettdeb != FN.etiliquido And !Empty(FN.bistamp)
			If !Empty(lcTextoAlerta)
				lcTextoAlerta = lcTextoAlerta + Chr(13) + Chr(13)
			Endif
			lcTextoAlerta = lcTextoAlerta + "- Total Linha"

			.txtTotal.BackColor = Rgb[232,76,61]
			.txtTotal.DisabledBackColor = Rgb[232,76,61]
			.txtTotal.ForeColor = Rgb(255,255,255)
		Else
			.txtTotal.BackColor = Rgb(191,223,223)
			.txtTotal.DisabledBackColor = Rgb[255,255,255]
			.txtTotal.ForeColor = Rgb(0,0,0)
		Endif
	Endwith

	With DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.Page12.PageFrame1.Page2.Container2
		Select uCrsDetalheEncomendaForn
		Select FN
		If uCrsDetalheEncomendaForn.ettdeb != FN.etiliquido And !Empty(FN.bistamp)
			.txtTotal.BackColor = Rgb[232,76,61]
			.txtTotal.DisabledBackColor = Rgb[232,76,61]
			.txtTotal.ForeColor = Rgb(255,255,255)
		Else
			.txtTotal.BackColor = Rgb(191,223,223)
			.txtTotal.DisabledBackColor = Rgb[255,255,255]
			.txtTotal.ForeColor = Rgb(0,0,0)
		Endif
	Endwith

	&&VALIDADE vs VALIDADE FICHA
	With DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.Page12.PageFrame1.Page1.Container1
		Select FN
		If FN.u_validade != uCrsConferenciaRef.validade And !Empty(FN.u_validade) And Year(FN.u_validade) != 1900
			If !Empty(lcTextoAlerta)
				lcTextoAlerta = lcTextoAlerta + Chr(13) + Chr(13)
			Endif
			lcTextoAlerta = lcTextoAlerta + "- Validade"
			.txtValidade.BackColor = Rgb[232,76,61]
			.txtValidade.DisabledBackColor = Rgb[232,76,61]
			.txtValidade.ForeColor = Rgb(255,255,255)
		Else
			.txtValidade.BackColor = Rgb(191,223,223)
			.txtValidade.DisabledBackColor = Rgb[255,255,255]
			.txtValidade.ForeColor = Rgb(0,0,0)
		Endif
	Endwith

	With DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.Page12.PageFrame1.Page2.Container2
		Select FN
		If FN.u_validade != uCrsConferenciaRef.validade And !Empty(FN.u_validade) And Year(FN.u_validade) != 1900
			.txtValidade.BackColor = Rgb[232,76,61]
			.txtValidade.DisabledBackColor = Rgb[232,76,61]
			.txtValidade.ForeColor = Rgb(255,255,255)
		Else
			.txtValidade.BackColor = Rgb(191,223,223)
			.txtValidade.DisabledBackColor = Rgb[255,255,255]
			.txtValidade.ForeColor = Rgb(0,0,0)
		Endif
	Endwith

	Select uCrsConferenciaRef
	If uCrsConferenciaRef.qttcli > 0
		If !Empty(lcTextoAlerta)
			lcTextoAlerta = lcTextoAlerta + Chr(13) + Chr(13)
		Endif
		lcTextoAlerta = lcTextoAlerta + "- Reservado: " + ASTR(uCrsConferenciaRef.qttcli)
	Endif
	Select uCrsConferenciaRef
	If !Empty(uCrsConferenciaRef.psico)
		If !Empty(lcTextoAlerta)
			lcTextoAlerta = lcTextoAlerta + Chr(13) + Chr(13)
		Endif
		lcTextoAlerta = lcTextoAlerta + "- Psicotr�pico"
	Endif
	Select uCrsConferenciaRef
	If !Empty(uCrsConferenciaRef.benzo)
		If !Empty(lcTextoAlerta)
			lcTextoAlerta = lcTextoAlerta + Chr(13) + Chr(13)
		Endif
		lcTextoAlerta = lcTextoAlerta + "- Benzodiazepina"
	Endif

	DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.Page12.PageFrame1.Page1.Container1.EditAlertas.Value = lcTextoAlerta
	DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.Page12.PageFrame1.Page2.Container2.EditAlertas.Value = lcTextoAlerta


	&& PVA e DescFinPCL Val
	With DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.Page12.PageFrame1.Page1.Container1
		Select uCrsConferenciaRef
		Select FN

		** Calcular PVA
		Local lcPVA, lcDescFinPCL
		lcPVA = 0
		If uCrsConferenciaRef.escalao > 0
			If FN.U_PVP > 0
				lcPVA = (FN.U_PVP - uCrsConferenciaRef.factorPond2) / uCrsConferenciaRef.factorPond
			Else
				lcPVA = (uCrsConferenciaRef.pvporig - uCrsConferenciaRef.factorPond2) / uCrsConferenciaRef.factorPond
			Endif
		Endif
		.txtPVA.Value = lcPVA

		lcDescFinPCL = FN.u_upc*FN.descFin_pcl/100
		.txtDescFinPCL.Value = lcDescFinPCL

	Endwith

Endfunc



**
Function uf_documentos_actualizaDadosConferencia
	Lparameters lcCampo, lcObj

	Local lcVarPublica, lcFnstamp
	lcVarPublica = "my" + Alltrim(lcCampo) + "Conf"

	If &lcVarPublica == .T.

		DOCUMENTOS.LockScreen = .T.

		Select FN
		lcFnstamp = FN.fnstamp

		Do Case
			Case myTipoDoc == "FO" And (myDocIntroducao == .T. Or myDocAlteracao == .T.) And Upper(Alltrim(lcCampo)) != "TXTTOTAL"

				uf_documentos_recalculaTotaisFN()
				uf_documentos_actualizaTotaisCAB()

				Select FN
				Locate For Alltrim(FN.fnstamp) == Alltrim(lcFnstamp)
				If Found()

					** Encontrou a posi��o, posiciona-se no cursor
					DOCUMENTOS.PageFrame1.Page1.gridDoc.SetFocus
					&lcObj..SetFocus

				Else
					** N�o encontrou a posi��o
				Endif
			Case  myTipoDoc == "FO" And (myDocIntroducao == .T. Or myDocAlteracao == .T.) And Upper(Alltrim(lcCampo)) == "TXTTOTAL" && Altera��o do Total da Linha

				uf_documentos_calcularPrecoUni()
				uf_documentos_actualizaTotaisCAB()

				Select FN
				Locate For Alltrim(FN.fnstamp) == Alltrim(lcFnstamp)
				If Found()

					** Encontrou a posi��o, posiciona-se no cursor
					DOCUMENTOS.PageFrame1.Page1.gridDoc.SetFocus
					&lcObj..SetFocus

				Else
					** N�o encontrou a posi��o
				Endif
		Endcase

		DOCUMENTOS.LockScreen = .F.
	Endif
Endfunc


**
Function uf_Documentos_calcMargensConfPvp
	Lparameters tcMargem

	Local lcIvaIncl
	lcIvaIncl = .F.

	If Used("uCrsConferenciaRef")
		Select uCrsConferenciaRef
		lcIvaIncl = uCrsConferenciaRef.ivaincl
	Endif

	Select cabdoc
	lcArredondaPVP = cabdoc.arredondaPVP


	Local lcValor
	Store 0 To lcValor

	If tcMargem
		** calcula pvp com base na margem comercial
		Select FN
		If !Empty(lcIvaIncl)
			lcValor = (FN.u_margpct/100+1) * FN.epv * (FN.IVA/100+1) && pvp final
		Else
			lcValor = (FN.u_margpct/100+1) * FN.epv && pvp final
		Endif

		If lcArredondaPVP
			lcCentimos = 0.05
			lcValor = Round(lcValor/lcCentimos ,0)*lcCentimos
		Endif

		If lcValor > 0
			Select FN
			Replace FN.U_PVP With Round(lcValor,2)
		Endif
	Else


		If lcArredondaPVP
			Select FN
			lcValor = FN.U_PVP
			lcCentimos = 0.05
			lcValor = Round(lcValor/lcCentimos ,0)*lcCentimos
			Select FN
			Replace FN.U_PVP With Round(lcValor,2)
		Endif

		**
		uf_documentos_calculaPercMrgSpct(.T.,lcIvaIncl)


	Endif
	**
	DOCUMENTOS.Refresh
Endfunc


Function uf_documentos_escolheLote

	If myDocIntroducao == .T. Or myDocAlteracao == .T.
		If myTipoDoc == "FO"
			Select FN
			If Empty(FN.usaLote)
				Return .F.
			Endif
		Endif
		If myTipoDoc == "BO"
			Select Bi
			If Empty(Bi.usaLote)
				Return .F.
			Endif
		Endif

		&& cursor lotes
		If myTipoDoc == "FO"
			Select FN
		Else
			Select Bi
		Endif

		lcSQL = ""
		TEXT TO lcSQL TEXTMERGE NOSHOW
			Select
				distinct lote
			from
				st_lotes (nolock)
			where
				st_lotes.ref = '<<IIF(myTipoDoc == "FO",FN.ref,BI.ref)>>'
				and st_lotes.lote != ''
				and st_lotes.stock > 0
			order by
				lote
		ENDTEXT

		If !uf_gerais_actGrelha("", "ucrsLote", lcSQL)
			uf_perguntalt_chama("N�O FOI POSS�VEL VERIFICAR A LISTAGEM DE LOTES. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			Return .F.
		Endif
		***************************************

		uf_valorescombo_chama(Iif(myTipoDoc == "FO","FN.LOTE","BI.LOTE"), 2, "ucrsLote", 2, "lote", "lote",.F.,.F.,.T.)

		With DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.Page12.PageFrame1
			If .ActivePage == 1
				.Page1.Container1.txtLoteConf.Value = Iif(myTipoDoc == "FO",FN.LOTE,Bi.LOTE)
				.Page1.Container1.txtLoteConf.Refresh
			Else
				.Page2.Container2.txtLoteConf.Value = Iif(myTipoDoc == "FO",FN.LOTE,Bi.LOTE)
				.Page1.Container1.txtLoteConf.Refresh
			Endif
		Endwith


	Endif
Endfunc


**
Function uf_documentos_dynamicBackColorGridDefault

	Return Iif(Recno()%2!=0,Rgb[255,255,255],Rgb[240,240,240])

Endfunc

**
Function uf_documentos_dynamicBackColorGridAlerta
	Lparameters lcCampo

	Select cabdoc
	Do Case
		Case lcCampo == "REF"
			Do Case
				Case myTipoDoc == "FO"

					If Alltrim(Upper(cabdoc.Doc)) == Alltrim(Upper("N/Guia Entrada"));
							OR Alltrim(Upper(cabdoc.Doc)) == Alltrim(Upper("V/Factura"));
							OR Alltrim(Upper(cabdoc.Doc)) == Alltrim(Upper("V/Guia Transp."));
							OR Alltrim(Upper(cabdoc.Doc)) == Alltrim(Upper("V/Factura Med."))

						Do Case
							Case !Empty(FN.u_reserva) Or FN.num1>0
								Return Rgb[34,185,178]
							Case FN.psico Or FN.benzo
								Return Rgb[42,144,155]
							Otherwise
								Return Iif(Recno()%2!=0,Rgb[255,255,255],Rgb[240,240,240])
						Endcase
					Else

						Do Case
							Case !Empty(FN.u_reserva) Or FN.num1>0
								Return Rgb[34,185,178]
							Case FN.psico Or FN.benzo
								Return Rgb[42,144,155]
							Otherwise
								Return Iif(Recno()%2!=0,Rgb[255,255,255],Rgb[240,240,240])
						Endcase
					Endif
				Case myTipoDoc == "BO"

					Do Case
						Case !Empty(Bi.u_reserva) And ucrsConfigDoc.bdempresas <> 'CL'
							Return Rgb[34,185,178]
						Case Bi.psico Or Bi.benzo
							Return Rgb[42,144,155]
						Otherwise
							Return Iif(Recno()%2!=0,Rgb[255,255,255],Rgb[240,240,240])
					Endcase
			Endcase

		Case lcCampo == "QTT"

			If myTipoDoc == "FO"
				If Alltrim(Upper(cabdoc.Doc)) == Alltrim(Upper("N/Guia Entrada"));
						OR Alltrim(Upper(cabdoc.Doc)) == Alltrim(Upper("V/Factura"));
						OR Alltrim(Upper(cabdoc.Doc)) == Alltrim(Upper("V/Guia Transp."))

					Select FN
					If FN.qtrec != FN.qtt And !Empty(FN.bistamp)
						Return Rgb[232,76,61]
					Else
						If FN.qtrec != FN.u_qttenc And !Empty(FN.bistamp)
							Return Rgb[238,142,56]
						Else
							Return Rgb[255,255,255]
						Endif
					Endif
				Endif

				If Alltrim(Upper(cabdoc.Doc)) == Alltrim(Upper("V/Factura Med."))

					Select FN
					Do Case
						Case FN.qtt > FN.qtrec
							Return Rgb[255,0,0]
						Case FN.qtrec > FN.qtt
							Return Rgb[0,223,0]
						Otherwise
							Return Rgb[255,255,255]
					Endcase

				Endif

			Endif

			Return Iif(Recno()%2!=0,Rgb[255,255,255],Rgb[240,240,240])
		Case Upper(lcCampo) == "TOTAL"

			If myTipoDoc == "FO"
				If Alltrim(Upper(cabdoc.Doc)) == Alltrim(Upper("N/Guia Entrada"));
						OR Alltrim(Upper(cabdoc.Doc)) == Alltrim(Upper("V/Factura"));
						OR Alltrim(Upper(cabdoc.Doc)) == Alltrim(Upper("V/Guia Transp."))

					Select FN
					If FN.u_upce != FN.u_upc And !Empty(FN.bistamp)
						Return Rgb[255,0,0]
					Endif
				Endif
			Endif

			Return Iif(Recno()%2!=0,Rgb[255,255,255],Rgb[240,240,240])

		Case Upper(lcCampo) == "QTREC"
			If Alltrim(Upper(cabdoc.Doc)) == Alltrim(Upper("V/Factura Med."))

				Select FN
				Do Case
					Case FN.qtrec < FN.u_oqt
						Return Rgb[241,196,15]
					Case FN.qtrec > FN.u_oqt
						Return Rgb[241,196,15]
					Otherwise
						Return Rgb[255,255,255]
				Endcase

			Endif

		Case uf_gerais_compstr(lcCampo,"U_PVP")

			If uf_gerais_compstr(cabdoc.Doc,"V/Factura Med.")
				**quando vem de uma ecomenda e assim para nao ter cor

				Select FN
				If Empty(FN.qtt) Or FN.qtt <= 0
					Return Rgb[255,255,255]
				Endif
				Select FN
				Do Case
					Case Alltrim(FN.pvpval) = '255,0,0'
						Return Rgb[255,0,0]
					Case Alltrim(FN.pvpval) = '241,196,15'
						Return Rgb[241,196,15]
					Otherwise
						Return Rgb[255,255,255]
				Endcase

			Endif

		Case Upper(lcCampo) == "U_DTVAL"
			If Alltrim(Upper(cabdoc.Doc)) == Alltrim(Upper("V/Factura Med."))

				Select FN
				Do Case
					Case Left(Alltrim(FN.u_dtval),4) == '1900' And FN.u_stockact>0
						Return Rgb[255,0,0]
					Case Left(Alltrim(FN.u_dtval),4) == '1900' And FN.u_stockact<=0
						Return Rgb[241,196,15]
					Case Empty(FN.u_dtval)
						Return Rgb[241,196,15]
					Case FN.stkatualloja=0 And FN.u_stockact=0
						Return Rgb[241,196,15]
					Case FN.stkatualloja>0 And FN.u_validade < Date() And !Empty(FN.u_validade)
						Return Rgb[255,0,0]
					Case FN.u_stockact<=0
						Return Rgb[241,196,15]
					Otherwise
						Return Rgb[255,255,255]
				Endcase

			Endif

		Case Upper(lcCampo) == "DESCONTO"
			If Alltrim(Upper(cabdoc.Doc)) == Alltrim(Upper("V/Factura Med."))

				Select FN
				Do Case
					Case FN.desconto < FN.u_descenc
						Return Rgb[255,0,0]
					Case FN.desconto > FN.u_descenc
						Return Rgb[0,223,0]
					Otherwise
						Return Rgb[255,255,255]
				Endcase

			Endif
		Case Upper(lcCampo) == "U_UPC"
			If Alltrim(Upper(cabdoc.Doc)) == Alltrim(Upper("V/Factura Med."))

				Select FN
				Do Case
					Case FN.u_upc > FN.u_upce
						Return Rgb[255,0,0]
					Otherwise
						Return Rgb[255,255,255]
				Endcase

			Endif

	Endcase

Endfunc


**
Function uf_documentos_dynamicForeColorGridAlerta
	Lparameters lcCampo

	Select cabdoc
	Do Case
		Case lcCampo == "REF"
			Do Case
				Case myTipoDoc == "FO"

					If Alltrim(Upper(cabdoc.Doc)) == Alltrim(Upper("N/Guia Entrada"));
							OR Alltrim(Upper(cabdoc.Doc)) == Alltrim(Upper("V/Factura"));
							OR Alltrim(Upper(cabdoc.Doc)) == Alltrim(Upper("V/Guia Transp."));
							OR Alltrim(Upper(cabdoc.Doc)) == Alltrim(Upper("V/Factura Med."))

						Do Case
							Case FN.psico Or FN.benzo Or FN.num1>0 Or !Empty(FN.u_reserva)
								Return Rgb[255,255,255]
							Otherwise
								Return Rgb[0,0,0]
						Endcase
					Else

						Do Case
							Case FN.psico Or FN.benzo Or !Empty(FN.u_reserva)
								Return Rgb[255,255,255]
							Otherwise
								Return Rgb[0,0,0]
						Endcase
					Endif
				Case myTipoDoc == "BO"

					Do Case
						Case Bi.psico Or Bi.benzo Or (!Empty(Bi.u_reserva) And ucrsConfigDoc.bdempresas <> 'CL')
							Return Rgb[255,255,255]
						Otherwise
							Return Rgb[0,0,0]
					Endcase
			Endcase

		Case lcCampo == "QTT"

			*!*				IF myTipoDoc == "FO"
			*!*					IF ALLTRIM(UPPER(cabDoc.Doc)) == ALLTRIM(UPPER("N/Guia Entrada"));
			*!*							OR ALLTRIM(UPPER(cabDoc.Doc)) == ALLTRIM(UPPER("V/Factura"));
			*!*							OR ALLTRIM(UPPER(cabDoc.Doc)) == ALLTRIM(UPPER("V/Guia Transp."));
			*!*							OR ALLTRIM(UPPER(cabDoc.Doc)) == ALLTRIM(UPPER("V/Factura Med."))
			*!*
			*!*						Select FN
			*!*						IF fn.qtrec != fn.qtt AND !EMPTY(fn.bistamp)
			*!*							RETURN Rgb[255,255,255]
			*!*						Else
			*!*							IF fn.qtrec != fn.u_qttenc AND !EMPTY(fn.bistamp)
			*!*								RETURN Rgb[255,255,255]
			*!*							ENDIF
			*!*						ENDIF
			*!*
			*!*					ENDIF
			*!*				ENDIF

			Return Rgb[0,0,0]

		Case lcCampo == "TOTAL"

			If myTipoDoc == "FO"
				If Alltrim(Upper(cabdoc.Doc)) == Alltrim(Upper("N/Guia Entrada"));
						OR Alltrim(Upper(cabdoc.Doc)) == Alltrim(Upper("V/Factura"));
						OR Alltrim(Upper(cabdoc.Doc)) == Alltrim(Upper("V/Guia Transp."))
					** ALLTRIM(UPPER(cabDoc.Doc)) == ALLTRIM(UPPER("V/Factura Med."))

					Select FN
					If FN.u_upce != FN.u_upc And !Empty(FN.bistamp)
						Return Rgb[255,255,255]
					Endif
				Endif
			Endif

			Return Rgb[0,0,0]

	Endcase

Endfunc


**
Function uf_documentos_aplicarConferencia

	If !uf_perguntalt_chama("Vai atualizar a Qt. da Fatura com a Qt. Rececionada. Pretende continuar?","Sim","N�o")
		Return .F.
	Endif

	If myTipoDoc == "FO"
		Update FN Set qtt = qtrec
	Endif

	DOCUMENTOS.LockScreen =  .T.
	Select FN
	Go Top
	Scan
		uf_documentos_recalculaTotaisFN()
	Endscan
	uf_documentos_actualizaTotaisCAB()
	Select FN
	Go Top

	DOCUMENTOS.LockScreen = .F.

Endfunc


**
Function uf_Documentos_actualizaIvaIncluidoSt
	Local lcRef, lcIvaIncl

	If !Used("uCrsConferenciaRef") Or !Used("FN")
		Return .F.
	Endif

	If !Used("ucrsAtualizaStIvaIncl")
		Create Cursor ucrsAtualizaStIvaIncl(ref c(18), ivaincl L)
	Endif


	Select uCrsConferenciaRef
	lcIvaIncl = uCrsConferenciaRef.ivaincl

	Select ucrsAtualizaStIvaIncl
	Locate For ucrsAtualizaStIvaIncl.ref == FN.ref
	If !Found()

		Select ucrsAtualizaStIvaIncl
		Append Blank
		Replace ucrsAtualizaStIvaIncl.ref With FN.ref
		Replace ucrsAtualizaStIvaIncl.ivaincl With lcIvaIncl

	Else
		Select ucrsAtualizaStIvaIncl
		Replace ucrsAtualizaStIvaIncl.ivaincl With lcIvaIncl
	Endif

Endfunc


**
Function uf_documentos_actualizaDocCentralCompras
	Local lcConnection_stringCC , gnConnHandleCC

	If uf_gerais_getParameter("ADM0000000214","BOOL") == .F.
		Return .F.
	Endif

	Do Case
		Case myTipoDoc == "FO"
			Select Distinct ofnstamp As Id From FN Where !Empty(FN.ofnstamp) Into Cursor ucrsFechaDocsCentralCompras Readwrite
		Case myTipoDoc == "BO"
			Select Distinct obistamp As Id From Bi Where !Empty(Bi.obistamp) Into Cursor ucrsFechaDocsCentralCompras Readwrite
		Otherwise
			Return .F.
	Endcase

	If Reccount("ucrsFechaDocsCentralCompras") == 0
		Return .F.
	Endif

	lcConnection_stringCC = "DSN=MariaDB_MySQL_UNICODE;UID=root;PWD=ls2k12...;DATABASE=ltsfx;App=LT"
	gnConnHandleCC = Sqlstringconn(lcConnection_stringCC)

	If gnConnHandleCC > 0
	Else
		uf_perguntalt_chama("N�o foi possivel a liga��o � Central de Compras. Contacte o Suporte.","OK","",16)
		Return .F.
	Endif

	Select ucrsFechaDocsCentralCompras
	Go Top
	Scan

		lcSQL = ""
		TEXT TO lcSQL TEXTMERGE NOSHOW
			update docs set estado = 1 WHERE docs.id = '<<Alltrim(ucrsFechaDocsCentralCompras.id)>>'
		ENDTEXT

		**
		SQLExec(gnConnHandleCC, lcSQL ,"ucrsActEstadoDocsCentralCompras")
		SQLDisconnect(gnConnHandleCC)

	Endscan
Endfunc


**
Function uf_documentos_aplicaDescontoComFl

	If !Empty(DOCUMENTOS.descontofl)
		If myTipoDoc == "FO"
			Select FN
			Go Top
			Scan
				Replace FN.desconto With DOCUMENTOS.descontofl
				uf_documentos_recalculaTotaisFN()
			Endscan
			uf_documentos_actualizaTotaisCAB()

		Endif
	Endif

Endfunc


**
Function uf_documentos_ConversaoUnidades
	Local lcComponentes, lcLordem, lcValorDistribuido, lcLinhas, lcAcerto
	Store 0 To lcComponentes, lcLordem, lcValorDistribuido

	DOCUMENTOS.LockScreen = .T.

	uf_documentos_EliminaLinhasComponentes()
	Create Cursor ucrsComponentesIntroduzir (ref c(18), qtt N(9,3), edebito N(12,3), lordem N(9,0))

	lcLordem = 0



	Select Bi
	Go Top
	Scan For !Empty(Bi.ref)
		lcLordem = lcLordem + 1

		Replace Bi.lordem With lcLordem

		If Used("ucrsComponentesBI")
			fecha("ucrsComponentesBI")
		Endif

		TEXT TO lcSQL NOSHOW TEXTMERGE
			exec up_stocks_componentes  '<<ALLTRIM(BI.ref)>>',<<mysite_nr>>
		ENDTEXT
		If !uf_gerais_actGrelha("", "ucrsComponentesBI", lcSQL)
			uf_perguntalt_chama("N�O FOI POSS�VEL VERIFICAR COMPONENTES DO PRODUTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			Return .F.
		Endif

		If Reccount("ucrsComponentesBI") > 0

			lcComponentes = Reccount("ucrsComponentesBI")
			lcQtt = Abs(Bi.qtt)
			lcValor = Abs(Bi.edebito)

			Select Bi
			Replace Bi.qtt With Abs(Bi.qtt)*-1
			Replace Bi.lobs With "Composto"
			Replace Bi.tabiva With 4
			Replace Bi.IVA With 0
			uf_documentos_recalculaTotaisBI()

			Select ucrsComponentesBI
			Calculate Sum(ucrsComponentesBI.qtt) To lcSomaQTT

			lcLinhas = 1
			lcAcerto = 0
			lcValorDistribuido = 0
			Select ucrsComponentesBI
			Go Top
			Scan
				lcLinhas = lcLinhas +1
				lcLordem = lcLordem + 1






				Select ucrsComponentesIntroduzir
				Append Blank

				Replace ucrsComponentesIntroduzir.ref With 	Alltrim(ucrsComponentesBI.ref)
				Replace ucrsComponentesIntroduzir.qtt With 	Abs(lcQtt*ucrsComponentesBI.qtt)
				Replace ucrsComponentesIntroduzir.edebito With Round(lcValor/lcSomaQTT,3) * (lcQtt*ucrsComponentesBI.qtt)
				Replace ucrsComponentesIntroduzir.lordem With lcLordem

				lcValorDistribuido = lcValorDistribuido + Round(lcValor/lcSomaQTT,3) * (lcQtt*ucrsComponentesBI.qtt)
			Endscan

			If lcValorDistribuido != (lcValor *lcQtt)
				Select ucrsComponentesIntroduzir
				Replace ucrsComponentesIntroduzir.edebito With ucrsComponentesIntroduzir.edebito - (lcValorDistribuido-(lcValor *lcQtt))
			Endif

		Endif
	Endscan

	Select ucrsComponentesIntroduzir
	Go Top
	Scan

		uf_documentos_criaNovaLinha(.F.,.T.)

		Select Bi
		Replace Bi.ref 		With 	ucrsComponentesIntroduzir.ref
		Replace Bi.qtt 		With 	ucrsComponentesIntroduzir.qtt
		Replace Bi.lordem 	With 	ucrsComponentesIntroduzir.lordem
		Replace Bi.lobs 	With 	"Componente"



		uf_documentos_eventoref()



		Select Bi
		Replace Bi.ettdeb 	With Abs(ucrsComponentesIntroduzir.edebito)
		Replace Bi.esltt With Bi.ettdeb
		Replace Bi.tabiva With 4
		Replace Bi.IVA With 0

		uf_documentos_calcularPrecoUni()



	Endscan



	If Used("ucrsComponentesIntroduzir")
		fecha("ucrsComponentesIntroduzir")
	Endif

	uf_documentos_actualizaTotaisCAB()

	DOCUMENTOS.LockScreen = .F.

	DOCUMENTOS.PageFrame1.Page1.gridDoc.Refresh
Endfunc


**
Function uf_documentos_EliminaLinhasComponentes
	Select Bi
	Go Top
	Scan For Alltrim(Bi.lobs) == "Componente"
		Select Bi
		Delete
	Endscan
	Select Bi
	Go Top

	DOCUMENTOS.PageFrame1.Page1.gridDoc.Refresh
Endfunc


**
Function uf_documentos_editaEncTipo
	Local lcbostamp, lcBistamp, lcBi2stmap
	Store '' To lcbostamp, lcBistamp, lcBi2stmap

	uf_perguntalt_chama("IR� SER GERADA UMA NOVA ENCOMENDA TIPO IGUAL � ENCOMENDA ATUAL EM MODO DE EDI��O.","OK","",64)

	Store .F. To myDocAlteracao
	Store .T. To myDocIntroducao

	&& atualiza dados ncess�rios
	uf_documentos_ConfiguracoesDoc(.T., .T.)

	&& atualiza Stamps
	lcbostamp = uf_gerais_stamp()

	Update cabdoc Set cabstamp = lcbostamp

	Select Bi
	Go Top
	Scan
		lcBistamp = uf_gerais_stamp()

		Select Bi
		Replace Bi.bistamp  With lcBistamp
		Replace Bi.obistamp With ''
		Replace Bi.BOSTAMP  With lcbostamp

		Select Bi
	Endscan

	Select BI2
	Go Top
	Scan
		lcBi2Stamp = uf_gerais_stamp()

		Select BI2
		Replace BI2.bi2stamp With lcBistamp
		Replace BI2.BOSTAMP  With lcbostamp

		Select BI2
	Endscan
	**

	&& apaga registos de compara��o p n�o eliminar da BD
	If Used("uCrsBiCompare")
		Select ucrsBiCompare
		Go Top
		Scan
			Delete
		Endscan
	Endif

	&&
	DOCUMENTOS.ContainerCab.documento.Enabled = .T.
	DOCUMENTOS.ContainerCab.documento.DisabledBackColor = Rgb(191,223,223)

	uf_documentos_alternaMenu()

	DOCUMENTOS.Refresh

Endfunc


**
Function uf_documento_infoDevViaVerde
	Local lcSQL, lcExecuteSQL, lcbostamp, lcBistamp
	Store '' To lcSQL, lcExecuteSQL, lcStamp, lcBistamp

	lcbostamp = uf_gerais_stamp()

	Select cabdoc
	&& inserir cabe�alho
	TEXT TO lcSQL NOSHOW TEXTMERGE
		INSERT INTO via_verde_med (id, bostamp, aceite, datahora_entrega, estado, data, id_receita)
		VALUES('<<ALLTRIM(lcBoStamp)>>','<<ALLTRIM(cabDoc.cabstamp)>>', 1, '<<uf_gerais_getDate(cabdoc.xpddata,"SQL")>>' ,'Devolucao', '<<uf_gerais_getDate(cabdoc.xpddata,"SQL")>>' ,'<<myReceitaNr>>')
	ENDTEXT

	lcExecuteSQL  = lcExecuteSQL + Chr(13) + Chr(13) + lcSQL

	Select Bi
	Go Top
	Scan
		lcBistamp = uf_gerais_stamp()

		lcSQL = ''
		TEXT TO lcSQL NOSHOW TEXTMERGE
			INSERT INTO via_verde_med_d(id,id_via_verde_med,ref,response_code,response_descr,qt_pedida,qt_entregue,pvf,pvp,iva,data)
			VALUES('<<ALLTRIM(lcBiStamp)>>','<<lcBoStamp>>','<<bi.ref>>', '', 'Devolucao', <<BI.qtt>>, <<BI.qtt2>>,<<bi.u_epv1act>>,<<bi.u_epv1act>>,<<BI.IVA>>, '<<uf_gerais_getDate(BI.dataobra,"SQL"))>>')
		ENDTEXT
		Select Bi

		lcExecuteSQL  = lcExecuteSQL + Chr(13) + Alltrim(lcSQL)
	Endscan

	If !uf_gerais_actGrelha("", "", lcExecuteSQL)
		uf_perguntalt_chama("N�O FOI POSS�VEL REGISTAR OS DADOS DE ENVIO CORRETAMENTE. POR FAVOR CONTACTE O SUPORTE. OBRIGADO.","OK","",16)
		**RETURN .f.
	Endif

Endfunc

*!*	FUNCTION uf_documentos_alterviz

*!*		DO CASE
*!*			CASE mytipoviz = 3
*!*				TEXT TO lcSql Noshow Textmerge
*!*					update wklcols set u_ordem=3 where wkstamp=(select wkstamp from wk where wkdocdescr='V/Factura') and wktitle='PCT'
*!*					update wklcols set u_ordem=4 where wkstamp=(select wkstamp from wk where wkdocdescr='V/Factura') and wktitle='Qt.'
*!*					update wklcols set u_ordem=5 where wkstamp=(select wkstamp from wk where wkdocdescr='V/Factura') and wktitle='Qt.Bonus'
*!*					update wklcols set u_ordem=6 where wkstamp=(select wkstamp from wk where wkdocdescr='V/Factura') and wktitle='Desc.1'
*!*					update wklcols set u_ordem=7 where wkstamp=(select wkstamp from wk where wkdocdescr='V/Factura') and wktitle='Desc.2'
*!*					update wklcols set u_ordem=8 where wkstamp=(select wkstamp from wk where wkdocdescr='V/Factura') and wktitle='Desc.3'
*!*					update wklcols set u_ordem=9 where wkstamp=(select wkstamp from wk where wkdocdescr='V/Factura') and wktitle='Desc.4'
*!*					update wklcols set u_ordem=10 where wkstamp=(select wkstamp from wk where wkdocdescr='V/Factura') and wktitle='PCL'
*!*					update wklcols set u_ordem=11 where wkstamp=(select wkstamp from wk where wkdocdescr='V/Factura') and wktitle='Total'
*!*				ENDTEXT
*!*			CASE mytipoviz = 2
*!*				TEXT TO lcSql Noshow Textmerge
*!*					update wklcols set u_ordem=3 where wkstamp=(select wkstamp from wk where wkdocdescr='V/Factura') and wktitle='Qt.'
*!*					update wklcols set u_ordem=4 where wkstamp=(select wkstamp from wk where wkdocdescr='V/Factura') and wktitle='Qt.Bonus'
*!*					update wklcols set u_ordem=5 where wkstamp=(select wkstamp from wk where wkdocdescr='V/Factura') and wktitle='Desc.1'
*!*					update wklcols set u_ordem=6 where wkstamp=(select wkstamp from wk where wkdocdescr='V/Factura') and wktitle='PCL'
*!*					update wklcols set u_ordem=7 where wkstamp=(select wkstamp from wk where wkdocdescr='V/Factura') and wktitle='PCT'
*!*					update wklcols set u_ordem=8 where wkstamp=(select wkstamp from wk where wkdocdescr='V/Factura') and wktitle='Total'
*!*					update wklcols set u_ordem=9 where wkstamp=(select wkstamp from wk where wkdocdescr='V/Factura') and wktitle='Desc.2'
*!*					update wklcols set u_ordem=10 where wkstamp=(select wkstamp from wk where wkdocdescr='V/Factura') and wktitle='Desc.3'
*!*					update wklcols set u_ordem=11 where wkstamp=(select wkstamp from wk where wkdocdescr='V/Factura') and wktitle='Desc.4'
*!*				ENDTEXT
*!*			CASE mytipoviz = 1
*!*				TEXT TO lcSql Noshow Textmerge
*!*					update wklcols set u_ordem=3 where wkstamp=(select wkstamp from wk where wkdocdescr='V/Factura') and wktitle='Qt.'
*!*					update wklcols set u_ordem=4 where wkstamp=(select wkstamp from wk where wkdocdescr='V/Factura') and wktitle='PCT'
*!*					update wklcols set u_ordem=5 where wkstamp=(select wkstamp from wk where wkdocdescr='V/Factura') and wktitle='PCL'
*!*					update wklcols set u_ordem=6 where wkstamp=(select wkstamp from wk where wkdocdescr='V/Factura') and wktitle='Qt.Bonus'
*!*					update wklcols set u_ordem=7 where wkstamp=(select wkstamp from wk where wkdocdescr='V/Factura') and wktitle='Desc.1'
*!*					update wklcols set u_ordem=8 where wkstamp=(select wkstamp from wk where wkdocdescr='V/Factura') and wktitle='Total'
*!*					update wklcols set u_ordem=9 where wkstamp=(select wkstamp from wk where wkdocdescr='V/Factura') and wktitle='Desc.2'
*!*					update wklcols set u_ordem=10 where wkstamp=(select wkstamp from wk where wkdocdescr='V/Factura') and wktitle='Desc.3'
*!*					update wklcols set u_ordem=11 where wkstamp=(select wkstamp from wk where wkdocdescr='V/Factura') and wktitle='Desc.4'
*!*				ENDTEXT
*!*		ENDCASE

*!*		uf_gerais_actGrelha("","",lcSql)
*!*		uf_documentos_controlaWkLinhas()
*!*		uf_documentosControlaObj()
*!*		DOCUMENTOS.pageframe1.page1.gridDoc.refresh
*!*		i=1
*!*
*!*		DO CASE
*!*			CASE mytipoviz = 3
*!*				mytipoviz = 1
*!*			CASE mytipoviz = 2
*!*				ti = DOCUMENTOS.PageFrame1.Page1.griddoc.columnCount
*!*				FOR i=1 TO ti
*!*					WITH DOCUMENTOS.PageFrame1.Page1.griddoc
*!*						DO CASE
*!*							CASE Alltrim(.columns(i).controlsource) == "fn.u_upc"
*!*								.columns(i).readonly = .f.
*!*							CASE Alltrim(.columns(i).controlsource) == "fn.epv"
*!*								.columns(i).readonly = .t.
*!*							CASE Alltrim(.columns(i).controlsource) == "fn.desc2"
*!*								.columns(i).visible = .f.
*!*							CASE Alltrim(.columns(i).controlsource) == "fn.desc3"
*!*								.columns(i).visible = .f.
*!*							CASE Alltrim(.columns(i).controlsource) == "fn.desc4"
*!*								.columns(i).visible = .f.
*!*							CASE Alltrim(.columns(i).controlsource) == "fn.armazem"
*!*								.columns(i).visible = .f.
*!*							CASE Alltrim(.columns(i).controlsource) == "fn.lote"
*!*								.columns(i).visible = .f.
*!*							CASE Alltrim(.columns(i).controlsource) == "fn.fnccusto"
*!*								.columns(i).visible = .f.
*!*							CASE Alltrim(.columns(i).controlsource) == ""
*!*								.columns(i).visible = .f.
*!*						ENDCASE
*!*					ENDWITH
*!*				ENDFOR
*!*				mytipoviz = 3
*!*			CASE mytipoviz = 1
*!*				ti = DOCUMENTOS.PageFrame1.Page1.griddoc.columnCount
*!*				FOR i=1 TO ti
*!*					WITH DOCUMENTOS.PageFrame1.Page1.griddoc
*!*						DO CASE
*!*							CASE Alltrim(.columns(i).controlsource) == "fn.u_upc"
*!*								.columns(i).readonly = .f.
*!*							CASE Alltrim(.columns(i).controlsource) == "fn.desconto"
*!*								.columns(i).readonly = .t.
*!*							CASE Alltrim(.columns(i).controlsource) == "fn.desc2"
*!*								.columns(i).visible = .f.
*!*							CASE Alltrim(.columns(i).controlsource) == "fn.desc3"
*!*								.columns(i).visible = .f.
*!*							CASE Alltrim(.columns(i).controlsource) == "fn.desc4"
*!*								.columns(i).visible = .f.
*!*							CASE Alltrim(.columns(i).controlsource) == "fn.armazem"
*!*								.columns(i).visible = .f.
*!*							CASE Alltrim(.columns(i).controlsource) == "fn.lote"
*!*								.columns(i).visible = .f.
*!*							CASE Alltrim(.columns(i).controlsource) == "fn.fnccusto"
*!*								.columns(i).visible = .f.
*!*							CASE Alltrim(.columns(i).controlsource) == ""
*!*								.columns(i).visible = .f.
*!*						ENDCASE
*!*					ENDWITH
*!*				ENDFOR
*!*				mytipoviz = 2
*!*		ENDCASE
*!*		DOCUMENTOS.pageframe1.page1.gridDoc.refresh
*!*
*!*	ENDFUNC


Function uf_Corrige_mov_compra
	Local lcEPcpond, lcMovEntrada, lcStock, lcQttMov, lcCont, lcCont2 , lcRef, lcStampSt, lcMesesAtua

	Store 0.00 To lcEPcpond
	Store .F. To lcMovEntrada
	Store 0 To lcStock, lcQttMov, lcCont
	Store 1 To lcCont2
	Store '' To lcRef, lcStampSt

	If !uf_gerais_getParameter_Site('ADM0000000130', 'BOOL', mySite)
		If !uf_perguntalt_chama("DESEJA CORRIGIR O PRE�O DE CUSTO PONDERADO DE TODOS OS ARTIGO DESTA COMPRA E RESPETIVO HIST�RICO DE MOVIMENTOS?" + Chr(13) + "ATEN��O - ESTA OP��O PODE SER DEMORADA"  + Chr(13) + "** ACONSELHADO NA ALTERA��O DOS DOCUMENTOS! **","Sim","N�o")
			Return .F.
		Endif
	Endif

	lcMesesAtua = uf_gerais_getParameter_Site('ADM0000000130', 'NUM', mySite)
	If lcMesesAtua <= 0
		lcMesesAtua = -1900
	Else
		lcMesesAtua = lcMesesAtua  * -1
	Endif

	TEXT To lcSQL TEXTMERGE NOSHOW
			select
				distinct case when fn.ref='' then fn.oref else fn.ref end as ref, st.ststamp
			from
				fn(NOLOCK)
			inner join
				st(NOLOCK) on (fn.ref=st.ref or fn.oref=st.ref) and fn.armazem=st.site_nr
			where
				fostamp='<<ALLTRIM(cabdoc.cabstamp)>>'
				and st.stns = 0
				and fn.ousrdata >= DATEADD(month, <<lcMesesAtua>> ,GETDATE())
	ENDTEXT

	uf_gerais_actGrelha("","ucrsListART",lcSQL)
	lcCont= Reccount("ucrsListART")
	regua(0,lcCont,"A Atualizar artigos ... ")
	Select ucrsListART
	Go Top
	Scan
		regua(1,lcCont2,"A Atualizar Documentos... Artigo " + Alltrim(ucrsListART.ref))
		lcEPcpond = 0
		Store .F. To lcMovEntrada
		lcStock = 0
		lcQttMov = 0
		lcRef = Alltrim(ucrsListART.ref)
		lcStampSt = Alltrim(ucrsListART.ststamp)
		If uf_gerais_getParameter("ADM0000000301","BOOL")
			TEXT To lcSQL TEXTMERGE NOSHOW
					SELECT sl.*, cm2.mudapcpond FROM sl (NOLOCK)
					inner join cm2 (nolock) on sl.cm=cm2.cm
					where ref='<<ALLTRIM(lcRef)>>'
						and sl.ousrdata >= DATEADD(month, <<lcMesesAtua>> ,GETDATE())
					order by sl.datalc, sl.ousrdata, sl.ousrhora
			ENDTEXT
		Else
			TEXT To lcSQL TEXTMERGE NOSHOW
					SELECT sl.*, cm2.mudapcpond FROM sl (NOLOCK)
					inner join cm2 (nolock) on sl.cm=cm2.cm
					where
						ref='<<ALLTRIM(lcRef)>>' and armazem=<<mySite_nr>>
						and sl.ousrdata >= DATEADD(month, <<lcMesesAtua>> ,GETDATE())
					order by sl.datalc, sl.ousrdata, sl.ousrhora
			ENDTEXT
		Endif
		uf_gerais_actGrelha("","ucrsListMov",lcSQL)

		If Reccount("ucrsListMov") > 0
			** Fazer o rec�lculo e atualiza��o de valores
			Select ucrsListMov
			Go Top
			Scan

				** atualiza��o de movimentos da sl
				TEXT To lcSQL TEXTMERGE NOSHOW
						UPDATE sl
						SET epcpond=<<lcEPcpond>>,
							pcpond=<<ROUND(lcEPcpond*200.482,2)>>,
							qtt=qtt
						where slstamp='<<ALLTRIM(ucrsListMov.slstamp)>>'
				ENDTEXT
				uf_gerais_actGrelha("","",lcSQL)

				Select ucrsListMov
				** atualiza��o de movimentos da fi
				If !Empty(ucrsListMov.fistamp)
					TEXT To lcSQL TEXTMERGE NOSHOW
							UPDATE fi SET epcp=<<lcEPcpond>>, pcp=<<ROUND(lcEPcpond*200.482,2)>> , qtt=qtt  where fistamp='<<ALLTRIM(ucrsListMov.fistamp)>>'
					ENDTEXT
					uf_gerais_actGrelha("","",lcSQL)
				Endif

				Select ucrsListMov
				** atualiza��o de movimentos da bi
				If !Empty(ucrsListMov.bistamp)
					TEXT To lcSQL TEXTMERGE NOSHOW
							UPDATE bi SET epcusto=<<lcEPcpond>>, pcusto=<<ROUND(lcEPcpond*200.482,2)>>, qtt=qtt   where bistamp='<<ALLTRIM(ucrsListMov.bistamp)>>'
					ENDTEXT
					uf_gerais_actGrelha("","",lcSQL)
				Endif


				Select ucrsListMov

				**IF ALLTRIM(ucrsListMov.cmdesc)='V/Factura' OR ALLTRIM(ucrsListMov.cmdesc)='V/Vd. Dinhei.' OR ALLTRIM(ucrsListMov.cmdesc)='V/Vd. Dinhei.' OR ALLTRIM(ucrsListMov.cmdesc)='V/Guia Transp.' OR ALLTRIM(ucrsListMov.cmdesc)='V/Factura Med.'
				If ucrsListMov.mudapcpond = .T.
					Replace ucrsListMov.EPCPOND With lcEPcpond
					If ucrsListMov.cm < 50
						lcQttMov = ucrsListMov.qtt
					Else
						lcQttMov = ucrsListMov.qtt * (-1)
					Endif

					Do Case
						Case (lcStock + lcQttMov ) <= 0
							lcEPcpond = 0
						Case (lcStock + lcQttMov ) > 0
							If lcStock > 0 Then
								**lcEPcpond = ROUND(((lcStock * lcEPcpond)+(ucrsListMov.ett)) / (lcStock  + ucrsListMov.qtt),2)
								lcEPcpond = Round(((lcStock * lcEPcpond)+(ucrsListMov.evu * ucrsListMov.qtt)) / (lcStock  + ucrsListMov.qtt),2)
							Else
								**lcEPcpond = ROUND(((0 * lcEPcpond)+(ucrsListMov.ett)) / (0  + ucrsListMov.qtt),2)
								lcEPcpond = Round(((0 * lcEPcpond)+(ucrsListMov.evu * ucrsListMov.qtt)) / (0  + ucrsListMov.qtt),2)
							Endif
						Otherwise
					Endcase
					lcStock  = lcStock + lcQttMov
				Else
					Replace ucrsListMov.EPCPOND With lcEPcpond
					If ucrsListMov.cm < 50 Then
						lcStock = lcStock + ucrsListMov.qtt
					Else
						lcStock = lcStock - ucrsListMov.qtt
					Endif
				Endif

				Select ucrsListMov
			Endscan

			If uf_gerais_getParameter("ADM0000000301","BOOL")
				TEXT To lcSQL TEXTMERGE NOSHOW
						UPDATE st SET epcpond=<<lcEPcpond>>, pcpond=<<ROUND(lcEPcpond*200.482,2)>> where ref='<<ALLTRIM(lcRef)>>'
				ENDTEXT
			Else
				TEXT To lcSQL TEXTMERGE NOSHOW
						UPDATE st SET epcpond=<<lcEPcpond>>, pcpond=<<ROUND(lcEPcpond*200.482,2)>> where ref='<<ALLTRIM(lcRef)>>' and site_nr=<<mySite_nr>>
				ENDTEXT
			Endif
			uf_gerais_actGrelha("","",lcSQL)

		Endif

		If Used("ucrsListMov")
			fecha("ucrsListMov")
		Endif
		lcCont2 = lcCont2 + 1
		Select ucrsListART
	Endscan

	If Used("ucrsListART")
		fecha("ucrsListART")
	Endif
	If !uf_gerais_getParameter_Site('ADM0000000130', 'BOOL', mySite)
		uf_perguntalt_chama("PROCESSO CONCLU�DO COM SUCESSO.", "Ok", "", 64)
	Endif
	regua(2)

	**ENDIF
Endfunc


Function uf_Corrige_mov_compra_pcp_artigo
	Local lcEPcpond, lcMovEntrada, lcStock, lcQttMov, lcCont, lcCont2 , lcRef, lcStampSt, lcMesesAtua

	Store 0.00 To lcEPcpond
	Store .F. To lcMovEntrada
	Store 0 To lcStock, lcQttMov, lcCont
	Store 1 To lcCont2
	Store '' To lcRef, lcStampSt

	lcMesesAtua = uf_gerais_getParameter_Site('ADM0000000130', 'NUM', mySite)
	If lcMesesAtua <= 0
		lcMesesAtua = -1900
	Else
		lcMesesAtua = lcMesesAtua  * -1
	Endif

	TEXT To lcSQL TEXTMERGE NOSHOW
			select
				case when fn.ref='' then fn.oref else fn.ref end as ref, st.ststamp
			from
				fn (nolock)
			inner join
				st (nolock) on (fn.ref=st.ref or fn.oref=st.ref) and fn.armazem=st.site_nr
			where
				fostamp='<<ALLTRIM(cabdoc.cabstamp)>>'
				and st.stns = 0

	ENDTEXT
	uf_gerais_actGrelha("","ucrsListART",lcSQL)
	Select ucrsListART
	Go Top
	Scan
		lcEPcpond = 0
		Store .F. To lcMovEntrada
		lcStock = 0
		lcQttMov = 0
		lcRef = Alltrim(ucrsListART.ref)
		lcStampSt = Alltrim(ucrsListART.ststamp)

		TEXT To lcSQL TEXTMERGE NOSHOW
				SELECT sl.*, cm2.mudapcpond FROM sl (NOLOCK)
				inner join cm2 (nolock) on sl.cm=cm2.cm
				where ref='<<ALLTRIM(lcRef)>>' and armazem=<<mySite_nr>>
					and sl.ousrdata >= DATEADD(month, <<lcMesesAtua>> ,GETDATE())
				order by sl.datalc, sl.ousrdata, sl.ousrhora
		ENDTEXT
		uf_gerais_actGrelha("","ucrsListMov",lcSQL)

		If Reccount("ucrsListMov") > 0
			** Fazer o rec�lculo e atualiza��o de valores
			Select ucrsListMov
			Go Top
			Scan

				Select ucrsListMov

				**IF ALLTRIM(ucrsListMov.cmdesc)='V/Factura' OR ALLTRIM(ucrsListMov.cmdesc)='V/Vd. Dinhei.' OR ALLTRIM(ucrsListMov.cmdesc)='V/Vd. Dinhei.' OR ALLTRIM(ucrsListMov.cmdesc)='V/Guia Transp.'
				If ucrsListMov.mudapcpond = .T.
					Replace ucrsListMov.EPCPOND With lcEPcpond
					If ucrsListMov.cm < 50
						lcQttMov = ucrsListMov.qtt
					Else
						lcQttMov = ucrsListMov.qtt * (-1)
					Endif

					Do Case
						Case (lcStock + lcQttMov ) <= 0
							lcEPcpond = 0
						Case (lcStock + lcQttMov ) > 0
							If lcStock > 0 Then
								**lcEPcpond = ROUND(((lcStock * lcEPcpond)+(ucrsListMov.ett)) / (lcStock  + ucrsListMov.qtt),2)
								lcEPcpond = Round(((lcStock * lcEPcpond)+(ucrsListMov.evu * ucrsListMov.qtt)) / (lcStock  + ucrsListMov.qtt),2)
							Else
								**lcEPcpond = ROUND(((0 * lcEPcpond)+(ucrsListMov.ett)) / (0  + ucrsListMov.qtt),2)
								lcEPcpond = Round(((0 * lcEPcpond)+(ucrsListMov.evu * ucrsListMov.qtt)) / (0  + ucrsListMov.qtt),2)
							Endif
						Otherwise
					Endcase
					lcStock  = lcStock + lcQttMov
				Else
					Replace ucrsListMov.EPCPOND With lcEPcpond
					If ucrsListMov.cm < 50 Then
						lcStock = lcStock + ucrsListMov.qtt
					Else
						lcStock = lcStock - ucrsListMov.qtt
					Endif
				Endif

				Select ucrsListMov
			Endscan


			TEXT To lcSQL TEXTMERGE NOSHOW
					UPDATE st SET epcpond=<<lcEPcpond>>, pcpond=<<ROUND(lcEPcpond*200.482,2)>> where ref='<<ALLTRIM(lcRef)>>' and site_nr=<<mySite_nr>>
			ENDTEXT
			uf_gerais_actGrelha("","",lcSQL)

		Endif

		If Used("ucrsListMov")
			fecha("ucrsListMov")
		Endif
		lcCont2 = lcCont2 + 1
		Select ucrsListART
	Endscan

	If Used("ucrsListART")
		fecha("ucrsListART")
	Endif

Endfunc


Function uf_documentos_input_despesa_linhas
	Local lcTotDesp, lcTotDoc, lcTotLin, lcPercLin, lcFee
	Store 0 To lcTotLin, lcPercLin, lcFee
	lcTotDesp = cabdoc.imput_desp
	lcTotDoc = cabdoc.Total
	If lcTotDoc > 0
		Select FN
		Go Top
		Scan
			If Alltrim(FN.ref)=='FEE' Or Alltrim(FN.ref)=='SG000' Or Alltrim(FN.ref)=='TRA000' Or Alltrim(FN.ref)=='TRM000'
				If FN.ivaincl=.T.
					lcTotDoc = lcTotDoc - FN.etiliquido
				Else
					lcTotDoc = lcTotDoc - (FN.etiliquido + Round(FN.etiliquido*(FN.IVA/100),2))
				Endif
			Endif
		Endscan

		Select FN
		Go Top
		Scan
			If Alltrim(FN.ref)!='FEE' And Alltrim(FN.ref)!='SG000' And Alltrim(FN.ref)!='TRA000' And Alltrim(FN.ref)!='TRM000'
				If FN.ivaincl=.T.
					lcTotLin = FN.etiliquido
				Else
					lcTotLin = FN.etiliquido + Round(FN.etiliquido*(FN.IVA/100),2)
				Endif
				lcPercLin = Round((lcTotLin * 100) / lcTotDoc ,2)
				Replace FN.val_desp_lin With lcTotDesp * (lcPercLin / 100)
				Replace FN.desp_lin_orig With lcTotDesp * (lcPercLin / 100)
			Endif
		Endscan
		Select FN
		Go Top
		DOCUMENTOS.PageFrame1.Page1.gridDoc.Refresh
	Endif
Endfunc

Function uf_documentos_recalc_despesa_linhas
	Local lcValManDesp, lcValManDoc , lcPos, lcTotDesp, lcTotDoc, lcTotLin, lcPercLin, lcTotDespFim, lcTotDespOrig
	Store 0 To lcValManDesp, lcValManDoc, lcTotLin, lcPercLin, lcTotDespFim
	lcTotDespOrig  = cabdoc.imput_desp
	lcTotDesp = cabdoc.imput_desp
	lcTotDoc = cabdoc.Total

	** registar despesas na linha sem que tenha valor no cabe�alho
	If cabdoc.imput_desp = 0 And FN.val_desp_lin > 0
		uf_perguntalt_chama("N�O PODE IMPUTAR DESPESAS NA LINHA SEM TER O TOTAL DAS MESMAS PREENCHIDO NO DOCUMENTO. OBRIGADO.","OK","",16)
		Replace FN.val_desp_lin With 0
		Return .F.
	Endif

	If cabdoc.imput_desp < FN.val_desp_lin
		uf_perguntalt_chama("N�O PODE IMPUTAR DESPESAS NA LINHA SUPERIOR AO TOTAL DAS MESMAS PREENCHIDO NO DOCUMENTO. OBRIGADO.","OK","",16)
		Replace FN.val_desp_lin With 0
		Return .F.
	Endif

	If lcTotDoc > 0
		Select FN
		lcPos=Recno("FN")

		Select FN
		Go Top
		Scan
			If Alltrim(FN.ref)=='FEE' Or Alltrim(FN.ref)=='SG000' Or Alltrim(FN.ref)=='TRA000' Or Alltrim(FN.ref)=='TRM000'
				If FN.ivaincl=.T.
					lcTotDoc = lcTotDoc - FN.etiliquido
				Else
					lcTotDoc = lcTotDoc - (FN.etiliquido + Round(FN.etiliquido*(FN.IVA/100),2))
				Endif
			Endif
		Endscan
		Select FN
		Go Top
		Scan
			If FN.desp_lin_orig != FN.val_desp_lin And (Alltrim(FN.ref)!='FEE' And Alltrim(FN.ref)!='SG000' And Alltrim(FN.ref)!='TRA000' And Alltrim(FN.ref)!='TRM000')
				If FN.ivaincl=.T.
					lcValManDoc = lcValManDoc + FN.etiliquido
				Else
					lcValManDoc = lcValManDoc + FN.etiliquido + Round(FN.etiliquido*(FN.IVA/100),2)
				Endif
				lcValManDesp = lcValManDesp + FN.val_desp_lin
			Endif
		Endscan
		lcTotDoc = lcTotDoc - lcValManDoc
		lcTotDesp = lcTotDesp - lcValManDesp

		Select FN
		Go Top
		Scan
			If FN.etiliquido > 0
				If FN.desp_lin_orig = FN.val_desp_lin And (Alltrim(FN.ref)!='FEE' And Alltrim(FN.ref)!='SG000' And Alltrim(FN.ref)!='TRA000' And Alltrim(FN.ref)!='TRM000')
					If FN.ivaincl=.T.
						lcTotLin = FN.etiliquido
					Else
						lcTotLin = FN.etiliquido + Round(FN.etiliquido*(FN.IVA/100),2)
					Endif
					lcPercLin = Round((lcTotLin * 100) / lcTotDoc ,2)
					Replace FN.val_desp_lin With lcTotDesp * (lcPercLin / 100)
					Replace FN.desp_lin_orig With lcTotDesp * (lcPercLin / 100)
				Endif
			Endif
			lcTotDespFim = lcTotDespFim + FN.val_desp_lin
		Endscan
		If lcTotDespFim <> lcTotDespOrig
			Select FN
			Go Bottom
			Replace FN.val_desp_lin With FN.val_desp_lin + (lcTotDespOrig  - lcTotDespFim)
			Replace FN.desp_lin_orig With FN.val_desp_lin
		Endif

		Select FN
		Try
			Go lcPos
		Catch
		Endtry

		DOCUMENTOS.PageFrame1.Page1.gridDoc.Refresh
	Endif
Endfunc


Function uf_documentos_valid_u_dtval

	Select FN
	If !Empty(FN.u_dtval)

		If Len(Alltrim(FN.u_dtval))<>6
			uf_perguntalt_chama("DATA INV�LIDA.","OK","",16)
			**dataValida = .f.
			TEXT TO lcSql NOSHOW textmerge
				select validade from st WHERE st.ref='<<ALLTRIM(fn.ref)>>' and site_nr=<<mysite_nr>>
			ENDTEXT
			uf_gerais_actGrelha("", "uCrsvalidade", lcSQL)
			Replace FN.u_dtval With Alltrim(Str(Year(uCrsvalidade.validade)))+Right(('0'+Alltrim(Str(Month(uCrsvalidade.validade)))),2)
			fecha("uCrsvalidade")
		Else
			If (Val(Left(Alltrim(FN.u_dtval),4)) <= Year(Date())+100 And Val(Left(Alltrim(FN.u_dtval),4))>=1900) And (Val(Right(Alltrim(FN.u_dtval),2))>=1 And Val(Right(Alltrim(FN.u_dtval),2))<=12)
				** Tudo OK
			Else
				uf_perguntalt_chama("DATA DE VALIDADE INV�LIDA.","OK","",16)
				TEXT TO lcSql NOSHOW textmerge
					select validade from st WHERE st.ref='<<ALLTRIM(fn.ref)>>' and site_nr=<<mysite_nr>>
				ENDTEXT
				uf_gerais_actGrelha("", "uCrsvalidade", lcSQL)
				Replace FN.u_dtval With Alltrim(Str(Year(uCrsvalidade.validade)))+Right(('0'+Alltrim(Str(Month(uCrsvalidade.validade)))),2)
				fecha("uCrsvalidade")
			Endif

		Endif

		&& Copiar u_dtval (ultimo dia do mes) para a  u_validade
		If  Alltrim(Upper(cabdoc.Doc)) == Alltrim(Upper("V/Factura Med.")) &&AND dataValida
			Local validade, lcUltimoDia
			validade   =  Date(Val(Left(Alltrim(FN.u_dtval),4)), Val(Right(Alltrim(FN.u_dtval),2)), 1)
			lcUltimoDia  = Gomonth(validade   , 1) - Day(validade   )
			Select FN
			Replace FN.u_validade With lcUltimoDia

		Endif

		Select FN

	Endif
Endfunc

Function uf_actualizaqtrec
	Replace FN.qtt With FN.qtrec

	DOCUMENTOS.LockScreen = .T.
	If myTipoDoc == "BO"
		lcBistamp = Bi.bistamp
		uf_documentos_recalculaTotaisBI()

		**
		If  uf_gerais_getParameter("ADM0000000214","BOOL") == .T.
			uf_condicoesComerciais_FeeGestaoLinha()
		Endif
	Endif
	If myTipoDoc == "FO"
		lcFnstamp = FN.fnstamp
		uf_documentos_recalculaTotaisFN()
	Endif

	uf_documentos_actualizaTotaisCAB()

	&& Actualiza Detalhes
	DOCUMENTOS.PageFrame1.Page1.detalhest1.Refresh

	If myTipoDoc == "FO" And !Empty(lcFnstamp)
		Select FN
		Locate For FN.fnstamp = lcFnstamp
	Endif
	If myTipoDoc == "BO" And !Empty(lcBistamp)
		Select Bi
		Locate For Bi.bistamp = lcBistamp
	Endif

	uf_documentos_EventosGridDoc(.T.) &&desactiva()
	DOCUMENTOS.LockScreen = .F.

	**
	DOCUMENTOS.PageFrame1.Page1.gridDoc.SetFocus && Corrige bug Documento sem linhas

	DOCUMENTOS.PageFrame1.Page1.ContainerLinhas.Total.Refresh
	&& Actualiza Detalhes
	DOCUMENTOS.PageFrame1.Page1.detalhest1.Refresh


	If  Alltrim(myTipoDoc) == "BO"
		Select Bi
		uf_documentos_editarTemp(Bi.bistamp)
	Else
		Select FN
		uf_documentos_editarTemp(FN.fnstamp)
	Endif
Endfunc

Function uf_documentos_alterblocoLinhaDoc
	Do Case
		Case Alltrim(cabdoc.Doc) = 'V/Factura Med.' Or Alltrim(cabdoc.Doc) = 'V/Factura'
			TEXT TO lcSQL TEXTMERGE NOSHOW
				select '' as campo  union select 'Desconto' as campo
			ENDTEXT
			uf_gerais_actGrelha("", "ucrsComboDoc", lcSQL)
		Case cabdoc.tipodos = 3 &&ALLTRIM(cabdoc.doc) = 'Trf entre Armaz�ns'
			TEXT TO lcSQL TEXTMERGE NOSHOW
				select '' as campo  union select 'Arm. Destino' as campo union select 'Arm. Origem' as campo
			ENDTEXT
			uf_gerais_actGrelha("", "ucrsComboDoc", lcSQL)
		Case Alltrim(cabdoc.Doc) = 'Acerto de Stock'
			TEXT TO lcSQL TEXTMERGE NOSHOW
				select '' as campo  union select 'Motivo Acerto' as campo
			ENDTEXT
			uf_gerais_actGrelha("", "ucrsComboDoc", lcSQL)
		Case Alltrim(cabdoc.Doc) = 'V/Nt. Cr�dito'
			TEXT TO lcSQL TEXTMERGE NOSHOW
				select '' as campo  union select 'IVA' as campo
			ENDTEXT
			uf_gerais_actGrelha("", "ucrsComboDoc", lcSQL)

		Case uf_gerais_compstr(cabdoc.Doc,'Condicoes Comerc. Forn.')
			TEXT TO lcSQL TEXTMERGE NOSHOW
				select '' as campo  union select 'Desconto' as campo
			ENDTEXT
			uf_gerais_actGrelha("", "ucrsComboDoc", lcSQL)
		Otherwise
			uf_perguntalt_chama("OP��O N�O DISPON�VEL PARA ESTE DOCUMENTO.", "Ok", "", 64)
			Return .F.
	Endcase

	Public lcEscolha
	lcEscolha = ""
	If Used("ucrsComboDoc")

		Select ucrsComboDoc
		Go Top
		uf_valorescombo_chama("lcEscolha", 0, "ucrsComboDoc", 2, "Campo", "Campo")

	Endif

	If !Empty(lcEscolha)

		Local lcFnPos
		Store 0 To lcPos
		If Alltrim(myTipoDoc)=="FO"
			Select FN
		Else
			Select Bi
		Endif
		lcPos = Recno()

		Do Case
			Case Alltrim(lcEscolha) = 'Desconto'
				Public myDescontoLinhas
				Store 0 To myDescontoLinhas
				uf_tecladonumerico_chama("myDescontoLinhas", "Introduza o Desconto (%):", 0, .T., .F., 6, 2)

			Case Alltrim(lcEscolha) = 'Arm. Destino' Or Alltrim(lcEscolha) = 'Arm. Origem'
				Public lcArmazemSel
				TEXT TO lcSQL TEXTMERGE NOSHOW
					exec up_gerais_getArmazens
				ENDTEXT
				uf_gerais_actGrelha("", "ucrsarmSel", lcSQL)
				uf_valorescombo_chama("lcArmazemSel", 0, "ucrsarmSel", 2, "Armazem", "Armazem")

			Case Alltrim(lcEscolha) = 'Motivo Acerto'
				Public myMotquebra
				lcSQL = ""
				TEXT TO lcSQL TEXTMERGE NOSHOW
					select descr as motivo from B_motivosQuebra (nolock) ORDER BY descr
				ENDTEXT

				If !uf_gerais_actGrelha("", "ucrsMotivosQuebra", lcSQL)
					uf_perguntalt_chama("N�O FOI POSS�VEL VERIFICAR MOTIVOS DE QUEBRA. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
					Return .F.
				Endif
				**
				Select Bi
				uf_valorescombo_chama("myMotquebra", 0, "ucrsMotivosQuebra", 2, "motivo", "motivo")

			Case Alltrim(lcEscolha) = 'IVA'
				Public mylcCodIva
				Local lcTaxaIva, lcTabIva
				uf_valorescombo_chama("mylcCodIva", 0, "select sel = convert(bit,0), codigo = codigo,taxa from taxasiva (nolock) where ativo=1", 1, "codigo", "taxa")
				lcTaxaIva = uf_gerais_getTabelaIva(mylcCodIva,"TAXA")
				lcTabIva = mylcCodIva

			Otherwise

		Endcase


		If Alltrim(myTipoDoc)=="FO"
			Select FN
		Else
			Select Bi
		Endif
		Go Top
		Scan
			Do Case
				Case Alltrim(lcEscolha) = 'Desconto'
					If (!Empty(myDescontoLinhas) Or (Vartype(myDescontoLinhas) == "N" And myDescontoLinhas>=0))
						If Alltrim(myTipoDoc) = "FO"
							Replace FN.desconto With myDescontoLinhas
							uf_documentos_recalculaTotaisFN()
						Else
							Replace Bi.desconto With myDescontoLinhas
							uf_documentos_recalculaTotaisBI()
						Endif
					Endif
				Case Alltrim(lcEscolha) = 'Arm. Destino'
					If (!Empty(lcArmazemSel) Or (Vartype(lcArmazemSel) == "N" And lcArmazemSel>=0))
						Replace Bi.ar2mazem With lcArmazemSel
						uf_documentos_recalculaTotaisBI()
					Endif
				Case Alltrim(lcEscolha) = 'Arm. Origem'
					If (!Empty(lcArmazemSel) Or (Vartype(lcArmazemSel) == "N" And lcArmazemSel>=0))
						Replace Bi.ARMAZEM With lcArmazemSel
						uf_documentos_recalculaTotaisBI()
					Endif
				Case Alltrim(lcEscolha) = 'Motivo Acerto'
					If (!Empty(myMotquebra) Or (Vartype(myMotquebra) == "N" And myMotquebra>=0))
						Replace Bi.u_mquebra With myMotquebra
						uf_documentos_recalculaTotaisBI()
						If  Alltrim(myTipoDoc) == "BO"
							Select Bi
							uf_documentos_editarTemp(Bi.bistamp)
						Else
							Select FN
							uf_documentos_editarTemp(FN.fnstamp)
						Endif

					Endif
				Case Alltrim(lcEscolha) = 'IVA'
					If (!Empty(lcTabIva) Or (Vartype(lcTabIva) == "N" And lcTabIva>=0))
						Replace FN.tabiva With lcTabIva
						Replace FN.IVA With lcTaxaIva
						Select FN
						mySelIvaMan=1
						uf_documentos_recalculaTotaisFN()
					Endif
				Otherwise

			Endcase
		Endscan

		If Alltrim(myTipoDoc)=="FO"
			Select FN
		Else
			Select Bi
		Endif
		If lcPos>0
			Try
				Go lcPos
			Catch
			Endtry
		Endif

		uf_documentos_actualizaTotaisCAB()
	Endif

	fecha("ucrsComboDoc")
Endfunc

Function uf_documentos_changeNrReceita
	Lparameters myReceitaNr
	lcSQL = ""
	TEXT TO lcSQL NOSHOW textmerge
		UPDATE BO2 SET nrReceita= '<<Alltrim(myReceitaNr)>>' where bo2stamp='<<Alltrim(cabDoc.cabstamp)>>'
	ENDTEXT
	If !uf_gerais_actGrelha("", "", lcSQL)
		uf_perguntalt_chama("OCORREU UMA ANOMALIA A GUARDAR O NOVO N�MERO DE RECEITA", "OK",16)
		Return .F.
	Endif
	Replace cabdoc.NRRECEITA    With myReceitaNr

	*!*	mudar o valor dentro da aba outros
	With DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.page11.ContainerOutrosDados
		.NRRECEITA.Value = cabdoc.NRRECEITA
	Endwith


Endfunc

Function uf_documentos_filtrar_esgotados
	If Alltrim(DOCUMENTOS.PageFrame1.Page1.BtnEsgot.Label1.Caption)=='Ocultar Esgotados'
		DOCUMENTOS.PageFrame1.Page1.BtnEsgot.Label1.Caption = 'Mostrar Esgotados'
		If Used("bi")
			Select Bi
			Set Filter To qtt>0
		Endif
		If Used("fn")
			Select FN
			Set Filter To qtt>0
		Endif
	Else
		DOCUMENTOS.PageFrame1.Page1.BtnEsgot.Label1.Caption = 'Ocultar Esgotados'
		If Used("bi")
			Select Bi
			Set Filter To
		Endif
		If Used("fn")
			Select FN
			Set Filter To
		Endif
	Endif
	DOCUMENTOS.PageFrame1.Page1.gridDoc.Refresh
	DOCUMENTOS.Refresh
Endfunc

Function uf_documentos_alteraqtt
	If myDocAlteracao == .T. Or myDocIntroducao == .T.
		If Used("bi")
			Local ti, lcQtt1, lcQtt2, lcQttPos, lcValida, lcrefval
			Store 0 To lcQtt1, lcQtt2, ti, lcQttPos
			Store '' To lcrefval
			Store .F. To lcValida

			Select Bi
			lcQtt1 = Bi.qtt
			lcrefval = Alltrim(Bi.ref)

			uf_tecladonumerico_chama("bi.qtt", "Introduza a Quantidade:", 2, .F., .F., 6, 0)

			Local uv_usaQttMin

			uv_usaQttMin = .T.

			If Used("ucrsTs")
				uv_usaQttMin = ucrsts.usaQttMinima
			Endif

			Select Bi
			lcQtt2 = Bi.qtt
			If (lcQtt1 != lcQtt2) And uv_usaQttMin
				Local lcIntBit, lcDecBit, lcConversao
				TEXT TO lcSQL NOSHOW textmerge
					select qttembal from st (nolock) where ref='<<ALLTRIM(lcrefval)>>' and site_nr=<<mysite_nr>>
				ENDTEXT
				If !uf_gerais_actGrelha("", "uCrsQttMVST", lcSQL)
					uf_perguntalt_chama("OCORREU UMA ANOMALIA A GUARDAR O NOVO N�MERO DE RECEITA", "OK",16)
					Return .F
				Endif
				lcConversao = lcQtt2 / uCrsQttMVST.qttembal
				IntBit = Int(lcConversao)
				DecBit = (lcConversao  - IntBit) * 10
				fecha("uCrsQttMVST")
				If DecBit <> 0
					**IF !uf_gerais_valida_password_admin('ATENDIMENTO', 'ALTERA��O QTT')
					uf_perguntalt_chama("A quantidade inserida n�o � v�lida. Por favor verifique.","OK","",64)
					Select Bi
					Replace Bi.qtt With lcQtt1
					DOCUMENTOS.PageFrame1.Page1.gridDoc.Refresh
					DOCUMENTOS.Refresh
					Return .F.
					**	ENDIF
				Endif
			Endif
			lcBistamp = Bi.bistamp
			uf_documentos_recalculaTotaisBI()
			uf_documentos_actualizaTotaisCAB()
			DOCUMENTOS.PageFrame1.Page1.detalhest1.Refresh
			Select Bi
			Locate For Bi.bistamp = lcBistamp
			uf_documentos_EventosGridDoc(.T.) &&desactiva()
			DOCUMENTOS.LockScreen = .F.
			DOCUMENTOS.PageFrame1.Page1.gridDoc.SetFocus && Corrige bug Documento sem linhas

			DOCUMENTOS.PageFrame1.Page1.ContainerLinhas.Total.Refresh
			DOCUMENTOS.PageFrame1.Page1.detalhest1.Refresh
		Endif
		If Used("fn")

		Endif
	Endif

Endfunc

Procedure uf_documentos_filtraConfRob
	Lparameters uv_id, uv_pend

	uv_filtro = ''

	If !Empty(uv_pend)
		uv_filtro = ', ' + ASTR(uv_pend)
	Endif

	TEXT TO msel TEXTMERGE NOSHOW
        exec up_robot_recPendentes '<<ALLTRIM(uv_id)>>' <<ALLTRIM(uv_filtro)>>
	ENDTEXT

	If Wexist("DOCUMENTOS")

		loGrid = DOCUMENTOS.PageFrame1.Page1.detalhest1.PageFrame1.Page13.gridP
		lcCursor = "uCrsRecRoboPend"

		lnColumns = loGrid.ColumnCount

		If lnColumns > 0

			Dimension laControlSource[lnColumns]
			For lnColumn = 1 To lnColumns
				laControlSource[lnColumn] = loGrid.Columns[lnColumn].ControlSource
			Endfor

		Endif

		loGrid.RecordSource = ""

		If !uf_gerais_actGrelha("", "uCrsRecRoboPend", msel)
			uf_perguntalt_chama("OCORREU UM PROBLEMA A PROCURAR RECEP��ES PENDENTES.","OK","",16)
			Return .F.
		Endif

		loGrid.RecordSource = lcCursor

		For lnColumn = 1 To lnColumns
			loGrid.Columns[lnColumn].ControlSource = laControlSource[lnColumn]
		Endfor

	Else

		If !uf_gerais_actGrelha("", "uCrsRecRoboPend", msel)
			uf_perguntalt_chama("OCORREU UM PROBLEMA A PROCURAR RECEP��ES PENDENTES.","OK","",16)
			Return .F.
		Endif

	Endif

	uf_CONFROBOTICA_recalcDoc()

Endproc

Procedure uf_corrigeDesconto
	Lparameters uv_curLin, uv_curStamp

	If Empty(uv_curLin) Or !Used(uv_curLin) Or Empty(uv_curStamp)
		Return .F.
	Endif

	Select &uv_curLin.
	Go Top
	Scan For desconto > 100 Or desc2 > 100 Or desc3 > 100 Or desc4 > 100

		If desconto > 100
			Replace desconto With 0
		Endif

		If desc2 > 100
			Replace desc2 With 0
		Endif

		If desc3 > 100
			Replace desc3 With 0
		Endif

		If desc4 > 100
			Replace desc4 With 0
		Endif

		Select &uv_curLin.
	Endscan

	uv_stampField = Alltrim(uv_curLin) + "stamp"

	Select &uv_curLin.
	Locate For Alltrim(&uv_stampField.) == Alltrim(uv_curStamp)

Endproc

Function uf_documentos_selArmazemDestino
	Lparameters uv_entidade

	If Empty(uv_entidade)
		If Used("cabdoc")
			uv_entidade = cabdoc.NO
		Else
			Return .F.
		Endif
	Endif

	TEXT TO lcSQL NOSHOW TEXTMERGE
		SELECT armazem as [Armazem] FROM empresa_arm(nolock) WHERE empresa_no = <<ASTR(uv_entidade)>>
	ENDTEXT

	If !uf_gerais_actGrelha("","uc_armazemDestino", lcSQL)
		uf_perguntalt_chama("OCURREU UM ERRO A VERIFICAR OS ARMAZENS DISPON�VEIS.","OK","",16)
		Return .F.
	Endif

	Select uc_armazemDestino
	Go Top

	If Reccount() = 0
		Return .F.
	Endif

	If Type("upv_armazemDestino") == "U"
		Public upv_armazemDestino
		upv_armazemDestino = uf_gerais_getUmvalor("empresa_arm", "armazem", "empresa_no = " + ASTR(mySite_nr), "armazem asc")
	Endif

	uf_valorescombo_chama('upv_armazemDestino', 0, "uc_armazemDestino", 2, "Armazem", "Armazem", .F., .F.,.F.,.T.)

Endfunc

Function uf_documentos_checkArmazemDestino

	If !Used("BI")
		Return .T.
	Endif

	Select Distinct ar2mazem From Bi With (Buffering = .T.) Order By ar2mazem Into Cursor uc_tmpBIarm

	Select uc_tmpBIarm
	Count To uv_count

	fecha("uc_tmpBIarm")

	If uv_count > 1
		uf_perguntalt_chama("N�o pode escolher mais que um armaz�m de destino!","OK","",48)
		Return .F.
	Endif

	Return .T.

Endfunc

Function uf_documentos_updateEncCentral
	Lparameters uv_bostamp, uv_localCur

	If !uf_gerais_chkconexao()
		Return .F.
	Endif

	Local uv_sql, uv_updateStatus, uv_dataFecho

	TEXT TO uv_sql TEXTMERGE NOSHOW
		select
			status, bo2.pagamento, bo2.modo_envio, bo.usrhora, bo.usrdata, bo.usrinis, bo.fechada
		from
			bo2 (nolock)
			inner join bo (nolock) on bo.bostamp=bo2.bo2stamp
		where
			bo2.bo2stamp='<<ALLTRIM(uv_bostamp)>>'
	ENDTEXT

	If !uf_gerais_runSQLCentral(uv_sql, "uc_estadoEncCentral")
		Return .F.
	Endif

	If Empty(uv_localCur)

		If !uf_gerais_actGrelha("", "uc_estadoEncLocal", uv_sql)
			Return .F.
		Endif

	Else

		Select * From (uv_localCur) With (Buffering = .T.) Into Cursor uc_estadoEncLocal

	Endif

	If !uf_gerais_compstr(uc_estadoEncLocal.Status, uc_estadoEncCentral.Status) Or !uf_gerais_compstr(uc_estadoEncLocal.modo_envio, uc_estadoEncCentral.modo_envio) Or !uf_gerais_compstr(uc_estadoEncLocal.pagamento, uc_estadoEncCentral.pagamento) Or uc_estadoEncLocal.FECHADA <> uc_estadoEncCentral.FECHADA

		If uc_estadoEncLocal.usrdata > uc_estadoEncCentral.usrdata Or (uc_estadoEncLocal.usrdata = uc_estadoEncCentral.usrdata And uc_estadoEncLocal.usrhora > uc_estadoEncCentral.usrhora) Or !Empty(uv_localCur)

			uv_dataFecho = Alltrim(uf_gerais_getdate(uc_estadoEncLocal.usrdata)) + " " + Alltrim(uc_estadoEncLocal.usrhora)

			TEXT TO uv_updateStatus TEXTMERGE NOSHOW
				UPDATE bo2 SET
					status = '<<ALLTRIM(uc_estadoEncLocal.status)>>',
					pagamento = '<<ALLTRIM(uc_estadoEncLocal.pagamento)>>',
					modo_envio = '<<ALLTRIM(uc_estadoEncLocal.modo_envio)>>',
					usrdata = '<<uf_gerais_getDate(uc_estadoEncLocal.usrdata, "SQL")>>',
					usrhora = '<<uc_estadoEncLocal.usrhora>>',
					usrinis = '<<ALLTRIM(uc_estadoEncLocal.usrinis)>>'
				WHERE
					bo2stamp = '<<ALLTRIM(uv_bostamp)>>'

				UPDATE bo SET
					usrdata = '<<uf_gerais_getDate(uc_estadoEncLocal.usrdata, "SQL")>>',
					usrhora = '<<uc_estadoEncLocal.usrhora>>',
					usrinis = '<<uc_estadoEncLocal.usrinis>>',
					fechada = '<<IIF(uc_estadoEncLocal.fechada, "1", "0")>>',
					datafecho = <<IIF(uc_estadoEncLocal.fechada, "'" + uv_dataFecho + "'" , "datafecho")>>
				WHERE
					bostamp = '<<ALLTRIM(uv_bostamp)>>'

				Update BI SET
					bi.Fechada = '<<IIF(uc_estadoEncLocal.fechada, "1", "0")>>',
					usrdata = '<<uf_gerais_getDate(uc_estadoEncLocal.usrdata, "SQL")>>',
					usrhora = '<<uc_estadoEncLocal.usrhora>>',
					usrinis	= '<<uc_estadoEncLocal.usrinis>>'
				WHERE bi.Bostamp = '<<ALLTRIM(uv_bostamp)>>'

			ENDTEXT

			If !uf_gerais_runSQLCentral(uv_updateStatus)
				uf_perguntalt_chama("Erro a atualizar a Encomenda na Central.","OK","",16)
				Return .F.
			Endif

		Endif

		If Empty(uv_localCur)

			If uc_estadoEncCentral.usrdata > uc_estadoEncLocal.usrdata Or (uc_estadoEncCentral.usrdata = uc_estadoEncLocal.usrdata And uc_estadoEncCentral.usrhora > uc_estadoEncLocal.usrhora)

				uv_dataFecho = Alltrim(uf_gerais_getdate(uc_estadoEncCentral.usrdata)) + " " + Alltrim(uc_estadoEncCentral.usrhora)

				TEXT TO uv_updateStatus TEXTMERGE NOSHOW
					UPDATE bo2 SET
						status = '<<ALLTRIM(uc_estadoEncCentral.status)>>',
						pagamento = '<<ALLTRIM(uc_estadoEncCentral.pagamento)>>',
						modo_envio = '<<ALLTRIM(uc_estadoEncCentral.modo_envio)>>',
						usrdata = '<<uf_gerais_getDate(uc_estadoEncCentral.usrdata, "SQL")>>',
						usrhora = '<<uc_estadoEncCentral.usrhora>>',
						usrinis = '<<ALLTRIM(uc_estadoEncCentral.usrinis)>>'
					WHERE
						bo2stamp = '<<ALLTRIM(uv_bostamp)>>'

					UPDATE bo SET
						usrdata = '<<uf_gerais_getDate(uc_estadoEncCentral.usrdata, "SQL")>>',
						usrhora = '<<uc_estadoEncCentral.usrhora>>',
						usrinis = '<<uc_estadoEncCentral.usrinis>>',
						fechada = '<<IIF(uc_estadoEncCentral.fechada, "1", "0")>>',
						datafecho = <<IIF(uc_estadoEncCentral.fechada, "'" + uv_dataFecho + "'", "datafecho")>>
					WHERE
						bostamp = '<<ALLTRIM(uv_bostamp)>>'

					Update BI SET
						bi.Fechada = '<<IIF(uc_estadoEncCentral.fechada, "1", "0")>>',
						usrdata = '<<uf_gerais_getDate(uc_estadoEncCentral.usrdata, "SQL")>>',
						usrhora = '<<uc_estadoEncCentral.usrhora>>',
						usrinis	= '<<uc_estadoEncCentral.usrinis>>'
					WHERE bi.Bostamp = '<<ALLTRIM(uv_bostamp)>>'

				ENDTEXT

				If !uf_gerais_actGrelha("", "", uv_updateStatus)
					uf_perguntalt_chama("Erro a atualizar a Encomenda Localmente.","OK","",16)
					Return .F.
				Endif

			Endif

		Endif

	Endif

	fecha("uc_estadoEncCentral")
	fecha("uc_estadoEncLocal")

Endfunc

Procedure uf_documentos_actulizaTotaisLinhas_NoBind
	DOCUMENTOS.LockScreen = .T.
	If myTipoDoc == "BO"
		lcBistamp = Bi.bistamp

		uf_corrigeDesconto("BI", lcBistamp)

		uf_documentos_recalculaTotaisBI()

		**
		If  uf_gerais_getParameter("ADM0000000214","BOOL") == .T.
			uf_condicoesComerciais_FeeGestaoLinha()
		Endif
	Endif
	If myTipoDoc == "FO"
		lcFnstamp = FN.fnstamp
		uf_corrigeDesconto("FN",lcFnstamp)
		uf_documentos_recalculaTotaisFN(.T.)
	Endif

	uf_documentos_actualizaTotaisCAB()

	&& Actualiza Detalhes
	DOCUMENTOS.PageFrame1.Page1.detalhest1.Refresh

	If myTipoDoc == "FO" And !Empty(lcFnstamp)
		Select FN
		Locate For FN.fnstamp = lcFnstamp
	Endif
	If myTipoDoc == "BO" And !Empty(lcBistamp)
		Select Bi
		Locate For Bi.bistamp = lcBistamp
	Endif

	uf_documentos_EventosGridDoc(.T.) &&desactiva()
	DOCUMENTOS.LockScreen = .F.

	**
	DOCUMENTOS.PageFrame1.Page1.gridDoc.SetFocus && Corrige bug Documento sem linhas

	DOCUMENTOS.PageFrame1.Page1.ContainerLinhas.Total.Refresh
	&& Actualiza Detalhes
	DOCUMENTOS.PageFrame1.Page1.detalhest1.Refresh
Endproc

Function uf_documentos_precosValidos
	Lparameters lcCampoSaida, lcCampoValorManual, lcComCombo

	Local lcSqlPrecoValido
	Store "" To lcSqlPrecoValido

	If !Used("FN")
		Return .F.
	Endif

	If Empty(lcCampoSaida) Or Empty(lcCampoValorManual)
		uf_perguntalt_chama("Ocorreu uma anomalia no campo de saida. Por favor Contacte o Suporte.", "OK", "", 16)
		Return .F.
	Endif

	If myDocAlteracao == .T. Or myDocIntroducao == .T.
		Select FN
		If Inlist(Alltrim(FN.familia),'1','2','10','23','58')
			Select FN
			TEXT TO lcSqlPrecoValido TEXTMERGE NOSHOW
		 		exec up_stocks_hpr '<<ALLTRIM(fn.ref)>>', 3, '<<uf_gerais_getdate(date(),"SQL")>>'
			ENDTEXT

			If  .Not. (uf_gerais_actGrelha("", "uCrsPicValidoData", lcSqlPrecoValido))
				uf_perguntalt_chama("Ocorreu uma anomalia a verificar pre�os dispon�veis para o produto. Por favor Contacte o Suporte.", "OK", "", 16)
				Return .F.
			Else

				If Empty(uCrsPicValidoData.pic)
					Return .F.
				Endif
				If lcComCombo == .T.
					uf_valorescombo_chama(lcCampoSaida, 2, "uCrsPicValidoData", 2, "PIC", "PIC, PRECO_VALIDO, DATA_FIM_ESCOAMENTO", .F., .F., .T., .T., .F., .T., .F., lcCampoValorManual)
				Endif
			Endif
		Else
			uf_documento_criarCursorVazioUCrsPicValidoData()
		Endif
	Else
		uf_documento_criarCursorVazioUCrsPicValidoData(Alltrim(FN.ref))
	Endif

Endfunc

Function uf_documento_criarCursorVazioUCrsPicValidoData
	Lparameters lcRef

	If Empty(lcRef)
		lcRef = ''
	Endif
	**criado cursor a vazio por causa do detalhesST quando andamos a saltar entre faturas
	TEXT TO lcSqlPrecoValido TEXTMERGE NOSHOW
	 		exec up_stocks_hpr '<<ALLTRIM(lcRef)>>', 3, '<<uf_gerais_getdate(date(),"SQL")>>'
	ENDTEXT

	If  .Not. (uf_gerais_actGrelha("", "uCrsPicValidoData", lcSqlPrecoValido))
		uf_perguntalt_chama("Ocorreu uma anomalia a verificar pre�os dispon�veis para o produto. Por favor Contacte o Suporte.", "OK", "", 16)
		Return .F.
	Endif

	Return .F.
Endfunc

Function uf_documento_controlaCoresDetalheSTPVP
	Lparameters lcCombo, lcObjeto

	Select FN
	uf_documentos_precosValidos("fn.u_pvp", "fn.u_pvp", lcCombo)


	If Empty(FN.qtt) Or FN.qtt <= 0 Or Empty(FN.U_PVP)
		lcObjeto.DisabledBackColor= Rgb[255,255,255]
		lcObjeto.BackColor = Rgb[255,255,255]
	Else
		If Used("uCrsPicValidoData")
			Select uCrsPicValidoData
			Locate For uCrsPicValidoData.pic ==  FN.U_PVP
			If Found()
				Select uCrsPicValidoData
				If !Empty(uCrsPicValidoData.data_fim) And uf_gerais_getdate(uCrsPicValidoData.data_fim, "SQL") < uf_gerais_getdate(Date(),"SQL")
					If uf_gerais_compstr(uCrsPicValidoData.preco_valido, "NAO")
						lcObjeto.DisabledBackColor= Rgb[255,0,0]
						lcObjeto.BackColor = Rgb[255,0,0]
					Else
						lcObjeto.DisabledBackColor= Rgb[241,196,15]
						lcObjeto.BackColor = Rgb[241,196,15]
					Endif
				Else
					lcObjeto.DisabledBackColor= Rgb[255,255,255]
					lcObjeto.BackColor = Rgb[255,255,255]
				Endif
			Else
				lcObjeto.DisabledBackColor= Rgb[255,0,0]
				lcObjeto.BackColor = Rgb[255,0,0]
			Endif
		Endif
	Endif

	lcObjeto.Parent.Refresh()

Endfunc

Function uf_documento_conferereferencia
	Lparameters lcstring, uv_lineStamp

	If Empty(lcstring) Or Empty(uv_lineStamp)
		Return .F.
	Endif

	Local lcEANID, lcDtValID, lcLoteID, lcIdID, lcRefID, lcRefPaisID, lcRefTempID, lcPosDelimiterID, lcConfereID, lcleituracampos, uv_newStamp
	Store '' To lcEANID, lcDtValID, lcLoteID, lcIdID, lcRefID, lcRefPaisID, lcleituracampos, uv_newStamp
	Store 0 To lcPosDelimiterID

	Local lcMax
	lcMax = 100

	Local lcEan
	lcEan = 14

	Local lcInitial
	lcInitial = lcstring

	uv_dMatrix = lcstring

	lcstring= Strtran(lcstring, "'", "-")
	lcstring= uf_gerais_remove_last_caracter_by_char(lcstring,'#')

	lcRefTempID = Alltrim(lcstring)
	lcConfereID = Alltrim(lcstring)

	lcRefTempID = uf_gerais_remove_last_caracter_by_char(lcRefTempID,'#')
	lcConfereID = uf_gerais_remove_last_caracter_by_char(lcConfereID,'#')


	If uf_gerais_retorna_campos_datamatrix_sql(lcRefTempID)

		Select ucrDataMatrixTemp
		If ucrDataMatrixTemp.valido

			lcDtValID = uf_gerais_retornaApenasAlphaNumericos(ucrDataMatrixTemp.Data)
			lcRefID  = uf_gerais_retornaApenasAlphaNumericos(ucrDataMatrixTemp.ref)
			lcRefPaisID = ucrDataMatrixTemp.countryCode
			lcEANID = ucrDataMatrixTemp.pc
			lcLoteID = ucrDataMatrixTemp.LOTE
			lcIdID = ucrDataMatrixTemp.sn

		Endif

	Endif

	If Empty(lcRefID)

		lcRefID = uf_gerais_getFromString(uv_dMatrix, '714', 7, .T.)
		If Len(lcRefID) <> 7
			Return .F.
		Endif

	Endif


	Local lcjalida
	lcjalida = .F.
	Select fi_trans_info
	Goto Top
	Scan
		If Alltrim(fi_trans_info.productcode)==Alltrim(lcEANID) .And. Alltrim(fi_trans_info.packserialnumber)==Alltrim(lcIdID)
			If fi_trans_info.conferido=.T.
				uf_perguntalt_chama("ESTA EMBALAGEM J� FOI CONFERIDA!.", "OK", "", 64)
				Return (.F.)
			Else
				Replace fi_trans_info.conferido With .T.
				lcjalida = .T.
			Endif
		Endif
	Endscan

	Local lcatribuiu
	lcatribuiu = .F.

	Select fi_trans_info
	Goto Top

	Scan
		If lcjalida=.F.
			If Alltrim(fi_trans_info.productnhrn)==Alltrim(lcRefID) .And. Empty(fi_trans_info.packserialnumber) .And. lcatribuiu=.F.


				uv_newStamp = uf_gerais_stamp()

				Select fi_trans_info

				Replace fi_trans_info.token With Alltrim(uv_newStamp)
				Replace fi_trans_info.productcode With lcEANID
				Replace fi_trans_info.productcodescheme With 'GTIN'
				Replace fi_trans_info.batchid With lcLoteID

				If(!Empty(uf_gerais_eFormatoDataValidoDataMatrix(lcDtValID)))
					Set Century On
					If Alltrim(Substr(lcDtValID, 5, 2))<>'00' And !Empty(Alltrim(Substr(lcDtValID, 5, 2)))
						Replace fi_trans_info.batchexpirydate With Date(2000+Val(Substr(lcDtValID, 1, 2)), Val(Substr(lcDtValID, 3, 2)), Val(Substr(lcDtValID, 5, 2)))
					Else
						Replace fi_trans_info.batchexpirydate With Gomonth(Date(2000+Val(Substr(lcDtValID, 1, 2)), Val(Substr(lcDtValID, 3, 2)), 1), 1)-1
					Endif
				Endif

				Replace fi_trans_info.packserialnumber With lcIdID
				Replace fi_trans_info.posterminal With Alltrim(myTerm)
				Replace fi_trans_info.country_productnhrn With Val(uf_gerais_retornaApenasAlphaNumericos(lcRefPaisID))
				Replace fi_trans_info.ousrinis With Alltrim(m_chinis)
				Replace fi_trans_info.ousrdata With Datetime()
				Replace fi_trans_info.conferido With .T.
				lcatribuiu = .T.
			Endif
		Endif
	Endscan

	If !lcatribuiu

		uv_newStamp = uf_gerais_stamp()

		Select fi_trans_info
		Append Blank
		Replace fi_trans_info.productnhrn With Alltrim(lcRefID)
		Replace fi_trans_info.token With Alltrim(uv_newStamp)
		Replace fi_trans_info.productcode With lcEANID
		Replace fi_trans_info.productcodescheme With 'GTIN'
		Replace fi_trans_info.batchid With lcLoteID
		Set Century On
		If(!Empty(uf_gerais_eFormatoDataValidoDataMatrix(lcDtValID)))
			If Alltrim(Substr(lcDtValID, 5, 2))<>'00' And !Empty(Alltrim(Substr(lcDtValID, 5, 2)))
				Replace fi_trans_info.batchexpirydate With Date(2000+Val(Substr(lcDtValID, 1, 2)), Val(Substr(lcDtValID, 3, 2)), Val(Substr(lcDtValID, 5, 2)))
			Else
				Replace fi_trans_info.batchexpirydate With Gomonth(Date(2000+Val(Substr(lcDtValID, 1, 2)), Val(Substr(lcDtValID, 3, 2)), 1), 1)-1
			Endif
		Endif
		Replace fi_trans_info.packserialnumber With lcIdID
		Replace fi_trans_info.posterminal With Alltrim(myTerm)
		Replace fi_trans_info.country_productnhrn With Val(uf_gerais_retornaApenasAlphaNumericos(lcRefPaisID))
		Replace fi_trans_info.ousrinis With Alltrim(m_chinis)
		Replace fi_trans_info.ousrdata With Datetime()
		Replace fi_trans_info.TIPO With 'S'
		Replace fi_trans_info.recStamp With Alltrim(uv_lineStamp)
		Replace fi_trans_info.recTable With Alltrim(myTipoDoc)

		Local uv_newClientTrxId
		uv_newClientTrxId = uf_gerais_stamp()

		Select fi_trans_info

		Replace fi_trans_info.clienttrxid With Alltrim(uv_newClientTrxId)
		Replace fi_trans_info.conferido With .T.

		Select fi_trans_info
		Goto Top
	Endif

Endfunc

Function uf_documento_tipoEstadoInfoEmbal

	If myTipoDoc == "FO"

		Select ucrscm1

		If ucrscm1.fosl > 50

			Select fi_trans_info
			Replace fi_trans_info.TIPO With 'S' For 1=1

		Else

			Select fi_trans_info
			Replace fi_trans_info.TIPO With 'E' For 1=1

		Endif

	Else

		Select ucrsts

		Local uv_codMov
		uv_codMov = Iif(ucrsts.producao, ucrsts.cm2Stocks, ucrsts.cmStocks)

		If uf_gerais_compstr("Acerto de Stock", ucrsts.NMDOS)

			Select fi_trans_info
			Replace fi_trans_info.TIPO With '' For 1=1

			Select * From Bi With (Buffering = .T.) Into Cursor uc_tmpBI

			Select fi_trans_info
			Go Top
			Scan

				Select uc_tmpBI
				Go Top

				Locate For uf_gerais_compstr(uc_tmpBI.bistamp, fi_trans_info.recStamp) And uc_tmpBI.qtt < 0

				If Found()
					Select fi_trans_info
					Replace fi_trans_info.TIPO With 'S'
				Endif

				Select fi_trans_info
			Endscan

		Else

			If uv_codMov > 50

				Select fi_trans_info
				Replace fi_trans_info.TIPO With 'S' For 1=1

			Else

				Select fi_trans_info
				Replace fi_trans_info.TIPO With 'E' For 1=1

			Endif

		Endif

	Endif

Endfunc

Function uf_documentos_verificaCaixas
	Local lcConfigCur
	lcConfigCur = Iif(uf_gerais_compstr(myTipoDoc, "FO"), "ucrsCm1", "ucrsTs")

	If &lcConfigCur..mvoVerificaEstado
		If  .Not. uf_valida_info_caixas_verificacao()
			Return .F.
		Endif

		uf_documento_tipoEstadoInfoEmbal()

		If myDocAlteracao
			uf_infoembal_pullRefs(.T., .T.)
		Endif
		If !uf_atendimento_valida_estado_datamatrix(.T., .T.)
			Return .F.
		Endif
	Endif

	If !Used("fi_trans_info")
		Return .T.
	Endif
	Select fi_trans_info
	Go Top

	Calculate Count() For 1=1 To uv_count
	If uv_count = 0
		Return .T.
	Endif
	If &lcConfigCur..mvoAlteraEstado
		Local uv_alteraEstado
		Store .F. To uv_alteraEstado

		Select fi_trans_info
		Go Top

		Select * From fi_trans_info Where fi_trans_info.TIPO='S' Into Cursor fi_trans_info_s Readwrite

		If Reccount("fi_trans_info_s")>0
			uv_alteraEstado = .T.

			If &lcConfigCur..mvoOpcional
				If !uf_perguntalt_chama("Diretiva de Medicamentos Falsificados." + Chr(13) + "Quer inativar as caixas conferidas?","Sim","N�o")
					uv_alteraEstado = .F.
				Endif
			Endif
			If uv_alteraEstado

				uf_Atendimento_DispensePackVerify()
			Endif
		Endif



		fecha("fi_trans_info_s")

		Select fi_trans_info
		Go Top

		Select * From fi_trans_info Where fi_trans_info.TIPO='E' Into Cursor fi_trans_info_e Readwrite

		If Reccount("fi_trans_info_e")>0
			uv_alteraEstado = .T.

			If &lcConfigCur..mvoOpcional
				If !uf_perguntalt_chama("Diretiva de Medicamentos Falsificados." + Chr(13) + "Quer ativar as caixas conferidas?","Sim","N�o")
					uv_alteraEstado = .F.
				Endif
			Endif
			If uv_alteraEstado
				uf_Atendimento_UndoDispenseSinglePack()
			Endif
		Endif

		fecha("fi_trans_info_e")

		If uv_alteraEstado

			Select fi_trans_info

			Select fi_trans_info
			Go Top

			uf_valida_info_caixas_dispensa(.T., .T.)

		Endif


		Select fi_trans_info
	Endif


	Select fi_trans_info

	If Used("mixed_bulk_pend")
		If Reccount("mixed_bulk_pend")>0
			lcSqlmixed_bulk_pend = uf_PAGAMENTO_GravarReg_mixed_bulk_pend()
			uf_gerais_actGrelha("","",lcSqlmixed_bulk_pend )
		Endif
		fecha("mixed_bulk_pend")
	Endif



	Return .T.

Endfunc

Function uf_documentos_novoDocNr
	Lparameters uv_saveNum

	Local uv_newDocNr
	Store 0 To uv_newDocNr

	Select cabdoc

	uv_newDocNr = uf_gerais_newDocNr("BO", cabdoc.NUMINTERNODOC, uv_saveNum)

	If uv_newDocNr = 0
		uf_perguntalt_chama("Erro a gerar o n�mero do documento." + Chr(13) + "Por favor contacte o suporte.", "OK", "", 48)
		Return .F.
	Endif

	Select cabdoc

	If Empty(cabdoc.NUMDOC) Or Isnull(cabdoc.NUMDOC) Or uf_gerais_compstr(cabdoc.NUMDOC, "0")

		Select cabdoc
		Replace cabdoc.NUMDOC With ASTR(uv_newDocNr)

		If Used("bi")

			Update Bi Set OBRANO = uv_newDocNr

		Endif

	Else

		If !uf_gerais_compstr(cabdoc.NUMDOC, ASTR(uv_newDocNr))

			uf_perguntalt_chama("ATEN��O: J� EXISTE UM DOCUMENTO COM O MESMO N�MERO. O N�MERO IR� SER INCREMENTADO. POR FAVOR TENTE NOVAMENTE.","OK","",48)

			Select cabdoc
			Replace cabdoc.NUMDOC With ASTR(uv_newDocNr)

			If Used("bi")

				Update Bi Set OBRANO = uv_newDocNr

			Endif

			Return .F.

		Endif

	Endif

	Return .T.

Endfunc






*******************************
***GEST�O GRAVA��O REAL_TIME***
*******************************






Function uf_documentos_gravarTempLinhas

	If !myDocAlteracao
		If  Alltrim(myTipoDoc) == "BO"
			Local lcNovoBiStamp
			lcNovoBiStamp = uf_gerais_stamp()
			Select Bi
			Replace Bi.bistamp With lcNovoBiStamp
			Select Bi
			If Reccount() > 0
				TEXT TO lcSql  TEXTMERGE NOSHOW

					INSERT INTO temp_BI(bistamp, bostamp, nmdos, ndos, obrano, ref, codigo, design, qtt, qtt2, uni2qtt, u_psicont, u_bencont, u_stockact
									,iva, tabiva, armazem, ar2mazem, stipo, cpoc,u_bonus, desconto, desc2, desc3, desc4, familia,no,nome,local,morada
									,codpost,epu,edebito,epcusto,ettdeb,ecustoind,edebitoori,u_upc,rdata,dataobra,dataopen,obistamp, resfor, rescli,lordem
									,lobs,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora, datafinal,u_reserva, vendedor, vendnm, CCUSTO,u_epv1act, lote
									,num1, binum1, binum2, binum3, qtrec, u_mquebra,eslvu,esltt, fechada, usr2, comunicada_ws, comunicada_ws_descr, terminal
					)Values (
						'<<lcNovoBiStamp>>','<<ALLTRIM(cabDoc.cabStamp)>>','<<alltrim(BI.nmdos)>>', <<cabDoc.numinternodoc>>,<<BI.obrano>>,
						'<<Alltrim(BI.ref)>>', '<<Alltrim(BI.ref)>>', '<<Alltrim(BI.design)>>', <<BI.qtt>>, <<BI.qtt2>>, <<BI.uni2qtt>>, <<bi.u_psicont>>, <<bi.u_bencont>>,
						<<BI.u_stockact>>, <<BI.IVA>>,<<BI.TABIVA>>,<<BI.ARMAZEM>>,<<BI.AR2MAZEM>>,<<BI.STIPO>>,<<BI.cpoc>>,<<bi.u_bonus>>, <<bi.desconto>>, <<bi.desc2>>, <<bi.desc3>>, <<bi.desc4>>,
						'<<BI.Familia>>', <<cabDoc.No>>,'<<ALLTRIM(cabDoc.Nome)>>','<<ALLTRIM(BI.Local)>>', '<<ALLTRIM(BI.Morada)>>', '<<ALLTRIM(BI.CodPost)>>',
						<<BI.EPU>>, <<BI.EDEBITO>>, <<BI.EPCUSTO>>, <<BI.ETTDEB>>, <<BI.ecustoind>>, <<BI.edebitoori>>, <<BI.u_upc>>, '<<Iif(Empty(Bi.rdata),'19000101',uf_gerais_getDate(BI.rdata,"SQL"))>>',
						'<<Iif(Empty(Bi.dataobra),'19000101',uf_gerais_getDate(BI.dataobra,"SQL"))>>', '<<Iif(Empty(Bi.dataopen),'19000101',uf_gerais_getDate(BI.dataopen,"SQL"))>>','<<Alltrim(BI.OBISTAMP)>>',
						0, 0, <<BI.lordem>>, '<<Alltrim(BI.lobs)>>', '<<IIF(EMPTY(bi.ousrinis),m_chinis, bi.ousrinis)>>', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102),convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8),'<<m_chinis>>',convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
						, convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8), '<<uf_gerais_getDate(cabDoc.datafinal,"SQL")>>',<<IIF(BI.u_reserva==.t.,1,0)>>,<<ch_vendedor>>, '<<ALLTRIM(ch_vendnm)>>', '<<ALLTRIM(BI.CCUSTO)>>',<<BI.u_epv1act>>,
						'<<ALLTRIM(bi.lote)>>',<<bi.num1>>,<<bi.binum1>>,<<bi.binum2>>,<<bi.binum3>>,<<bi.qtrec>>,'<<ALLTRIM(bi.u_mquebra)>>',<<BI.eslvu>>,<<BI.esltt>>, 0, '<<ALLTRIM(bi.usr2)>>'
						,0, '<<ALLTRIM(bi.comunicada_ws_descr)>>', <<mytermno>>
					)


					Insert into temp_BI2 (bi2stamp, bostamp, morada, local, codpost, fnstamp, fistamp)
					Values('<<lcNovoBiStamp>>','<<ALLTRIM(cabDoc.cabStamp)>>','<<ALLTRIM(BI.Morada)>>','<<ALLTRIM(BI.Local)>>','<<ALLTRIM(BI.CodPost)>>', '<<ALLTRIM(bi2.fnstamp)>>', '<<ALLTRIM(bi2.fistamp)>>')
				ENDTEXT
				If !uf_gerais_actGrelha("", "ucrsTemp", lcSQL)
					uf_perguntalt_chama("N�O FOI POSS�VEL VERIFICAR A LISTAGEM DE CENTROS DE CUSTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
					Return .F.
				Endif
			Endif

		Else
			Local lcNovoFNStamp
			lcNovoFNStamp = uf_gerais_stamp()


			Select FN
			Replace FN.fnstamp With lcNovoFNStamp
			If Reccount() > 0
				lcSQL = ''
				TEXT TO lcSql TEXTMERGE NOSHOW
					INSERT INTO temp_fn (fnstamp, fostamp, data, ref, oref, codigo, design,stns, usr1, usr2, usr3, usr4, usr5,
						 usr6, docnome, adoc, unidade, qtt, uni2qtt, iva, ivaincl, u_bonus, desconto, desc2, desc3, desc4,
						 tabiva, armazem, cpoc, lordem, epv, pv, etiliquido, tiliquido, u_psicont, u_bencont, qtrec, u_pvp,
						 u_validade, esltt, sltt, u_upc, eslvu, slvu,u_stockAct, u_epv1act, marcada,ousrinis, ousrdata, ousrhora,
						 usrinis, usrdata, usrhora, ofnstamp, bistamp, u_qttenc, u_upce, fmarcada, u_reserva, fnccusto, SUJINV,lote, u_margpct, descFin_pcl, num1, familia, stamp_ext_doc,
						 u_dtval, u_descenc , u_oqt, val_desp_lin, fistamp, terminal
					) VALUES
					('<<lcNovofnStamp>>','<<ALLTRIM(cabDoc.cabStamp)>>','<<uf_gerais_getDate(cabDoc.dataInterna,"SQL")>>','<<ALLTRIM(FN.REF)>>','<<Alltrim(FN.OREF)>>','<<ALLTRIM(FN.REF)>>',
					'<<ALLTRIM(FN.design)>>',<<IIF(FN.stns, 1, 0)>>,'<<ALLTRIM(FN.usr1)>>','<<ALLTRIM(FN.usr2)>>','<<ALLTRIM(FN.usr3)>>','<<ALLTRIM(FN.usr4)>>','<<ALLTRIM(FN.usr5)>>',
					'<<ALLTRIM(FN.usr6)>>','<<ALLTRIM(cabDoc.DOC)>>','<<Alltrim(cabDoc.NUMDOC)>>','<<ALLTRIM(FN.unidade)>>',<<FN.qtt>>,<<FN.uni2qtt>>,<<FN.iva>>,<<IIF(FN.ivaincl,1,0)>>,
					<<fn.u_bonus>>, <<fn.desconto>>, <<fn.desc2>>, <<fn.desc3>>, <<fn.desc4>>,
					<<FN.tabiva>>,<<FN.armazem>>,<<FN.cpoc>>,<<FN.lordem>>,<<Round(FN.epv,2)>>,<<Round(FN.epv*200.482,2)>>,<<FN.etiliquido>>,<<FN.etiliquido*200.482>>,
					<<fn.u_psicont>>, <<fn.u_bencont>>, <<fn.qtrec>>, <<fn.u_pvp>>, '<<iif(ALLTRIM(cabdoc.doc)='V/Factura  Med.', alltrim(fn.u_dtval)+'01' , (IIF(!EMPTY(fn.u_validade),uf_gerais_getDate(fn.u_validade,"SQL"),'19000101')))>>',
					<<FN.esltt>>,<<FN.esltt*200.482>>,<<FN.u_upc>>,<<FN.eslvu>>,<<FN.eslvu * 200.482>>,<<FN.u_stockact>>,<<FN.u_epv1act>>, <<Iif(fn.marcada,1,0)>>,
					'<<m_chinis>>',convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8),'<<m_chinis>>',convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102),convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8),
					'<<Alltrim(FN.OFNSTAMP)>>', '<<Alltrim(FN.BISTAMP)>>', <<FN.u_qttenc>>, <<FN.u_upce>>, <<Iif(fn.fmarcada,1,0)>>,<<IIF(FN.u_reserva,1,0)>>,'<<FN.FNCCUSTO>>',<<IIF(FN.SUJINV,1,0)>>,
					'<<ALLTRIM(fn.lote)>>',<<fn.u_margpct>>,<<IIF(ISNULL(fn.descFin_pcl),0,fn.descFin_pcl)>>,<<fn.num1>>,'<<ALLTRIM(fn.familia)>>', '<<ALLTRIM(fn.stamp_ext_doc)>>',
					'<<ALLTRIM(fn.u_dtval)>>', <<fn.u_descenc>>, <<fn.u_oqt>>, <<fn.val_desp_lin>>, '<<ALLTRIM(fn.fistamp)>>', <<mytermno>> )
				ENDTEXT
				If !uf_gerais_actGrelha("", "ucrsTemp", lcSQL)
					uf_perguntalt_chama("N�O FOI POSS�VEL VERIFICAR A LISTAGEM DE CENTROS DE CUSTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
					Return .F.
				Endif
			Endif
		Endif




	Endif



Endfunc




Function uf_documentos_editarTemp
	Lparameters lcStamp
	If !myDocAlteracao

		lcSQL = ''
		TEXT TO lcSql TEXTMERGE NOSHOW
			DELETE FROM temp_bi 			WHERE bistamp = '<<lcStamp>>'

			DELETE FROM temp_bi2 			WHERE bi2stamp = '<<lcStamp>>'

			DELETE FROM temp_fn 			WHERE fnstamp = '<<lcStamp>>'

		ENDTEXT
		If !uf_gerais_actGrelha("", "ucrsTemp", lcSQL)
			uf_perguntalt_chama("N�O FOI POSS�VEL VERIFICAR A LISTAGEM DE CENTROS DE CUSTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			Return .F.
		Endif


		If  Alltrim(myTipoDoc) == "BO"

			Local lcNovoBiStamp
			lcNovoBiStamp = uf_gerais_stamp()


			Select Bi
			Replace Bi.bistamp With lcNovoBiStamp
			TEXT TO lcSql  TEXTMERGE NOSHOW

				INSERT INTO temp_BI(bistamp, bostamp, nmdos, ndos, obrano, ref, codigo, design, qtt, qtt2, uni2qtt, u_psicont, u_bencont, u_stockact
								,iva, tabiva, armazem, ar2mazem, stipo, cpoc,u_bonus, desconto, desc2, desc3, desc4, familia,no,nome,local,morada
								,codpost,epu,edebito,epcusto,ettdeb,ecustoind,edebitoori,u_upc,rdata,dataobra,dataopen,obistamp, resfor, rescli,lordem
								,lobs,ousrinis,ousrdata,ousrhora,usrinis,usrdata,usrhora, datafinal,u_reserva, vendedor, vendnm, CCUSTO,u_epv1act, lote
								,num1, binum1, binum2, binum3, qtrec, u_mquebra,eslvu,esltt, fechada, usr2, comunicada_ws, comunicada_ws_descr, terminal
				)Values (
					'<<lcNovoBiStamp>>','<<ALLTRIM(cabDoc.cabStamp)>>','<<alltrim(BI.nmdos)>>', <<cabDoc.numinternodoc>>,<<BI.obrano>>,
					'<<Alltrim(BI.ref)>>', '<<Alltrim(BI.ref)>>', '<<Alltrim(BI.design)>>', <<BI.qtt>>, <<BI.qtt2>>, <<BI.uni2qtt>>, <<bi.u_psicont>>, <<bi.u_bencont>>,
					<<BI.u_stockact>>, <<BI.IVA>>,<<BI.TABIVA>>,<<BI.ARMAZEM>>,<<BI.AR2MAZEM>>,<<BI.STIPO>>,<<BI.cpoc>>,<<bi.u_bonus>>, <<bi.desconto>>, <<bi.desc2>>, <<bi.desc3>>, <<bi.desc4>>,
					'<<BI.Familia>>', <<cabDoc.No>>,'<<ALLTRIM(cabDoc.Nome)>>','<<ALLTRIM(BI.Local)>>', '<<ALLTRIM(BI.Morada)>>', '<<ALLTRIM(BI.CodPost)>>',
					<<BI.EPU>>, <<BI.EDEBITO>>, <<BI.EPCUSTO>>, <<BI.ETTDEB>>, <<BI.ecustoind>>, <<BI.edebitoori>>, <<BI.u_upc>>, '<<Iif(Empty(Bi.rdata),'19000101',uf_gerais_getDate(BI.rdata,"SQL"))>>',
					'<<Iif(Empty(Bi.dataobra),'19000101',uf_gerais_getDate(BI.dataobra,"SQL"))>>', '<<Iif(Empty(Bi.dataopen),'19000101',uf_gerais_getDate(BI.dataopen,"SQL"))>>','<<Alltrim(BI.OBISTAMP)>>',
					0, 0, <<BI.lordem>>, '<<Alltrim(BI.lobs)>>', '<<IIF(EMPTY(bi.ousrinis),m_chinis, bi.ousrinis)>>', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102),convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8),'<<m_chinis>>',convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
					, convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8), '<<uf_gerais_getDate(cabDoc.datafinal,"SQL")>>',<<IIF(BI.u_reserva==.t.,1,0)>>,<<ch_vendedor>>, '<<ALLTRIM(ch_vendnm)>>', '<<ALLTRIM(BI.CCUSTO)>>',<<BI.u_epv1act>>,
					'<<ALLTRIM(bi.lote)>>',<<bi.num1>>,<<bi.binum1>>,<<bi.binum2>>,<<bi.binum3>>,<<bi.qtrec>>,'<<ALLTRIM(bi.u_mquebra)>>',<<BI.eslvu>>,<<BI.esltt>>, 0, '<<ALLTRIM(bi.usr2)>>'
					,0, '<<ALLTRIM(bi.comunicada_ws_descr)>>', <<mytermno>>
				)


				Insert into temp_BI2 (bi2stamp, bostamp, morada, local, codpost, fnstamp, fistamp)
				Values('<<lcNovoBiStamp>>','<<ALLTRIM(cabDoc.cabStamp)>>','<<ALLTRIM(BI.Morada)>>','<<ALLTRIM(BI.Local)>>','<<ALLTRIM(BI.CodPost)>>', '<<ALLTRIM(bi2.fnstamp)>>', '<<ALLTRIM(bi2.fistamp)>>')
			ENDTEXT
			If !uf_gerais_actGrelha("", "ucrsTemp", lcSQL)
				uf_perguntalt_chama("N�O FOI POSS�VEL GRAVAR O SEU PROGRESSO DE CRIA��O/EDI��O. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				Return .F.
			Endif

		Else
			Local lcNovoFNStamp
			lcNovoFNStamp = uf_gerais_stamp()
			Select FN
			Replace FN.fnstamp With lcNovoFNStamp
			lcSQL = ''
			TEXT TO lcSql TEXTMERGE NOSHOW
				INSERT INTO temp_fn (fnstamp, fostamp, data, ref, oref, codigo, design,stns, usr1, usr2, usr3, usr4, usr5,
					 usr6, docnome, adoc, unidade, qtt, uni2qtt, iva, ivaincl, u_bonus, desconto, desc2, desc3, desc4,
					 tabiva, armazem, cpoc, lordem, epv, pv, etiliquido, tiliquido, u_psicont, u_bencont, qtrec, u_pvp,
					 u_validade, esltt, sltt, u_upc, eslvu, slvu,u_stockAct, u_epv1act, marcada,ousrinis, ousrdata, ousrhora,
					 usrinis, usrdata, usrhora, ofnstamp, bistamp, u_qttenc, u_upce, fmarcada, u_reserva, fnccusto, SUJINV,lote, u_margpct, descFin_pcl, num1, familia, stamp_ext_doc,
					 u_dtval, u_descenc , u_oqt, val_desp_lin, fistamp, terminal
				) VALUES
				('<<lcNovoFNStamp>>','<<ALLTRIM(cabDoc.cabStamp)>>','<<uf_gerais_getDate(cabDoc.dataInterna,"SQL")>>','<<ALLTRIM(FN.REF)>>','<<Alltrim(FN.OREF)>>','<<ALLTRIM(FN.REF)>>',
				'<<ALLTRIM(FN.design)>>',<<IIF(FN.stns, 1, 0)>>,'<<ALLTRIM(FN.usr1)>>','<<ALLTRIM(FN.usr2)>>','<<ALLTRIM(FN.usr3)>>','<<ALLTRIM(FN.usr4)>>','<<ALLTRIM(FN.usr5)>>',
				'<<ALLTRIM(FN.usr6)>>','<<ALLTRIM(cabDoc.DOC)>>','<<Alltrim(cabDoc.NUMDOC)>>','<<ALLTRIM(FN.unidade)>>',<<FN.qtt>>,<<FN.uni2qtt>>,<<FN.iva>>,<<IIF(FN.ivaincl,1,0)>>,
				<<fn.u_bonus>>, <<fn.desconto>>, <<fn.desc2>>, <<fn.desc3>>, <<fn.desc4>>,
				<<FN.tabiva>>,<<FN.armazem>>,<<FN.cpoc>>,<<FN.lordem>>,<<Round(FN.epv,2)>>,<<Round(FN.epv*200.482,2)>>,<<FN.etiliquido>>,<<FN.etiliquido*200.482>>,
				<<fn.u_psicont>>, <<fn.u_bencont>>, <<fn.qtrec>>, <<fn.u_pvp>>, '<<iif(ALLTRIM(cabdoc.doc)='V/Factura  Med.', alltrim(fn.u_dtval)+'01' , (IIF(!EMPTY(fn.u_validade),uf_gerais_getDate(fn.u_validade,"SQL"),'19000101')))>>',
				<<FN.esltt>>,<<FN.esltt*200.482>>,<<FN.u_upc>>,<<FN.eslvu>>,<<FN.eslvu * 200.482>>,<<FN.u_stockact>>,<<FN.u_epv1act>>, <<Iif(fn.marcada,1,0)>>,
				'<<m_chinis>>',convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8),'<<m_chinis>>',convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102),convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8),
				'<<Alltrim(FN.OFNSTAMP)>>', '<<Alltrim(FN.BISTAMP)>>', <<FN.u_qttenc>>, <<FN.u_upce>>, <<Iif(fn.fmarcada,1,0)>>,<<IIF(FN.u_reserva,1,0)>>,'<<FN.FNCCUSTO>>',<<IIF(FN.SUJINV,1,0)>>,
				'<<ALLTRIM(fn.lote)>>',<<fn.u_margpct>>,<<IIF(ISNULL(fn.descFin_pcl),0,fn.descFin_pcl)>>,<<fn.num1>>,'<<ALLTRIM(fn.familia)>>', '<<ALLTRIM(fn.stamp_ext_doc)>>',
				'<<ALLTRIM(fn.u_dtval)>>', <<fn.u_descenc>>, <<fn.u_oqt>>, <<fn.val_desp_lin>>, '<<ALLTRIM(fn.fistamp)>>', <<mytermno>> )
			ENDTEXT
			If !uf_gerais_actGrelha("", "ucrsTemp", lcSQL)
				uf_perguntalt_chama("N�O FOI POSS�VEL GRAVAR O SEU PROGRESSO DE CRIA��O/EDI��O. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				Return .F.
			Endif

		Endif


	Endif


Endfunc

**
Function uf_documentos_apagarTemp
	If !myDocAlteracao
		lcSQL = ''
		TEXT TO lcSql TEXTMERGE NOSHOW
			DELETE FROM temp_bi 			WHERE terminal = <<mytermno>>

			DELETE FROM temp_bi2 			WHERE terminal = <<mytermno>>

			DELETE FROM temp_fn 			WHERE terminal = <<mytermno>>
		ENDTEXT
		If !uf_gerais_actGrelha("", "ucrsTemp", lcSQL)
			uf_perguntalt_chama("N�O FOI POSS�VEL GRAVAR O SEU PROGRESSO DE CRIA��O/EDI��O. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			Return .F.
		Endif
	Endif

Endfunc
**


Function uf_documentos_pendentes


	lcSQL = ''
	TEXT TO lcSql TEXTMERGE NOSHOW
		SELECT COUNT(*) as nr_linhas FROM temp_bi WHERE terminal = <<mytermno>>
	ENDTEXT
	If !uf_gerais_actGrelha("", "ucrsTempBi", lcSQL)
		uf_perguntalt_chama("N�O FOI POSS�VEL VERIFICAR A LISTAGEM DE CENTROS DE CUSTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		Return .F.
	Endif

	lcSQL = ''
	TEXT TO lcSql TEXTMERGE NOSHOW
		SELECT COUNT(*) as nr_linhas FROM temp_fn WHERE terminal = <<mytermno>>
	ENDTEXT
	If !uf_gerais_actGrelha("", "ucrsTempFn", lcSQL)
		uf_perguntalt_chama("N�O FOI POSS�VEL GRAVAR O SEU PROGRESSO DE CRIA��O/EDI��O. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		Return .F.
	Endif

	Local uc_nrLinhas
	uc_temTemp = Iif(ucrsTempBi.nr_linhas != ucrsTempFn.nr_linhas, .T., .F.)

	fecha("ucrsTempBi")
	fecha("ucrsTempFn")
	Return uc_temTemp

Endfunc


Function uf_documentos_criaNovaLinhaTemp


	If !myDocAlteracao

		Local uc_cursorTemp
		uc_cursorTemp = ''

		lcSQL = ''
		TEXT TO lcSql TEXTMERGE NOSHOW
			SELECT * FROM temp_bi WHERE terminal = <<mytermno>>
		ENDTEXT
		If !uf_gerais_actGrelha("", "ucrsTempBi", lcSQL)
			uf_perguntalt_chama("N�O FOI POSS�VEL VERIFICAR A LISTAGEM DE CENTROS DE CUSTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			Return .F.
		Endif


		lcSQL = ''
		TEXT TO lcSql TEXTMERGE NOSHOW
			SELECT * FROM temp_bi2 WHERE terminal = <<mytermno>>
		ENDTEXT
		If !uf_gerais_actGrelha("", "ucrsTempBi2", lcSQL)
			uf_perguntalt_chama("N�O FOI POSS�VEL GRAVAR O SEU PROGRESSO DE CRIA��O/EDI��O. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			Return .F.
		Endif

		lcSQL = ''
		TEXT TO lcSql TEXTMERGE NOSHOW
			SELECT * FROM temp_fn WHERE terminal = <<mytermno>>
		ENDTEXT
		If !uf_gerais_actGrelha("", "ucrsTempFn", lcSQL)
			uf_perguntalt_chama("N�O FOI POSS�VEL GRAVAR O SEU PROGRESSO DE CRIA��O/EDI��O. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			Return .F.
		Endif




		If Reccount("ucrsTempBi") > Reccount("ucrsTempFn")
			uf_documentos_novoDoc_escolhe(ucrsTempBi.NMDOS, .T.)
		Else
			uf_documentos_novoDoc_escolhe(ucrsTempFn.docnome,.T.)
		Endif

		Select cabdoc
		Replace cabdoc.SITE With mySite


		DOCUMENTOS.Refresh

		Local lcStamp
		** Verificar se existe nova vers�o para reiniciar o software
		uf_chk_nova_versao()

		** Previne erros informa��o cursor por gravar (For�a lostfocus)
		DOCUMENTOS.LockScreen = .T.
		DOCUMENTOS.ContainerCab.TextPesq.SetFocus
		DOCUMENTOS.PageFrame1.Page1.gridDoc.SetFocus
		DOCUMENTOS.LockScreen = .F.
		*********************************************

		If DOCUMENTOS.eventoref == .F. && se estiver a correr os eventos
			uf_documentos_EventosGridDoc(.F.)
			&&RETURN .f.
		Endif
		********************************************************************
		** DOSSIERS INTERNOS
		********************************************************************

		If Reccount("ucrsTempBi") > Reccount("ucrsTempFn")
			Select ucrsTempBi
			Goto Top
			Scan
				Select uCrsTempBi2
				Goto Top
				Locate For uCrsTempBi2.bistamp = ucrsTempBi.bistamp

				Select BI2
				Append Blank
				Select Bi
				Replace BI2.bi2stamp 			With BI2.bi2stamp
				Replace BI2.fnstamp 			With BI2.fnstamp
				Replace BI2.BOSTAMP 			With BI2.BOSTAMP
				Replace BI2.ousrinis 			With BI2.ousrinis
				Replace BI2.ousrdata 			With BI2.ousrdata
				Replace BI2.ousrHORA 			With BI2.ousrHORA
				Replace BI2.usrinis 			With BI2.usrinis
				Replace BI2.usrdata 			With BI2.usrdata
				Replace BI2.usrhora 			With BI2.usrhora
				Replace BI2.MORADA 				With BI2.MORADA
				Replace BI2.Local 				With BI2.Local
				Replace BI2.CODPOST 			With BI2.CODPOST
				Replace BI2.fistamp 			With BI2.fistamp
				Replace BI2.EXPORTADO 			With BI2.EXPORTADO
				Replace BI2.valeNr 				With BI2.valeNr
				Replace BI2.idCampanha 			With BI2.idCampanha
				Replace BI2.qttembal 			With BI2.qttembal
				Replace BI2.valcartao 			With BI2.valcartao
				Replace BI2.NRRECEITA 			With BI2.NRRECEITA
				Replace BI2.obsInt 				With BI2.obsInt
				Replace BI2.obsCl 				With BI2.obsCl
				Replace BI2.fistamp_insercao 	With BI2.fistamp_insercao
				Delete Tag All



				Select Bi
				Append Blank

				Replace Bi.bistamp 				With	ucrsTempBi.bistamp
				Replace Bi.NMDOS 				With    ucrsTempBi.NMDOS
				Replace Bi.OBRANO 				With    ucrsTempBi.OBRANO
				Replace Bi.ref 					With    ucrsTempBi.ref
				Replace Bi.Design 				With    ucrsTempBi.Design
				Replace Bi.qtt 					With    ucrsTempBi.qtt
				Replace Bi.qtt2 				With    ucrsTempBi.qtt2
				Replace Bi.IVA 					With    ucrsTempBi.IVA
				Replace Bi.tabiva 				With    ucrsTempBi.tabiva
				Replace Bi.ARMAZEM 				With    ucrsTempBi.ARMAZEM
				Replace Bi.stipo 				With    ucrsTempBi.stipo
				Replace Bi.NO 					With    ucrsTempBi.NO
				Replace Bi.ndos 				With    ucrsTempBi.ndos
				Replace Bi.forref 				With    ucrsTempBi.forref
				Replace Bi.rdata 				With    ucrsTempBi.rdata
				Replace Bi.lobs 				With    ucrsTempBi.lobs
				Replace Bi.FECHADA 				With    ucrsTempBi.FECHADA
				Replace Bi.DATAFINAL 			With    ucrsTempBi.DATAFINAL
				Replace Bi.DATAOBRA 			With    ucrsTempBi.DATAOBRA
				Replace Bi.dataopen 			With    ucrsTempBi.dataopen
				Replace Bi.resfor 				With    ucrsTempBi.resfor
				Replace Bi.rescli 				With    ucrsTempBi.rescli
				Replace Bi.ar2mazem 			With    ucrsTempBi.ar2mazem
				Replace Bi.lrecno 				With    ucrsTempBi.lrecno
				Replace Bi.lordem 				With    ucrsTempBi.lordem
				Replace Bi.Local 				With    ucrsTempBi.Local
				Replace Bi.MORADA 				With    ucrsTempBi.MORADA
				Replace Bi.CODPOST 				With    ucrsTempBi.CODPOST
				Replace Bi.Nome 				With    ucrsTempBi.Nome
				Replace Bi.vendedor 			With    ucrsTempBi.vendedor
				Replace Bi.vendnm 				With    ucrsTempBi.vendnm
				Replace Bi.LOTE 				With    ucrsTempBi.LOTE
				Replace Bi.uni2qtt 				With    ucrsTempBi.uni2qtt
				Replace Bi.epu 					With    ucrsTempBi.epu
				Replace Bi.edebito 				With    ucrsTempBi.edebito
				Replace Bi.eprorc 				With    ucrsTempBi.eprorc
				Replace Bi.EPCUSTO 				With    ucrsTempBi.EPCUSTO
				Replace Bi.ettdeb 				With    ucrsTempBi.ettdeb
				Replace Bi.Adoc 				With    ucrsTempBi.Adoc
				Replace Bi.binum1 				With    ucrsTempBi.binum1
				Replace Bi.binum2 				With    ucrsTempBi.binum2
				Replace Bi.codigo 				With    ucrsTempBi.codigo
				Replace Bi.cpoc 				With    ucrsTempBi.cpoc
				Replace Bi.obistamp 			With    ucrsTempBi.obistamp
				Replace Bi.oobistamp			With    ucrsTempBi.oobistamp
				Replace Bi.familia 				With    ucrsTempBi.familia
				Replace Bi.desconto 			With    ucrsTempBi.desconto
				Replace Bi.desc2 				With    ucrsTempBi.desc2
				Replace Bi.desc3 				With    ucrsTempBi.desc3
				Replace Bi.desc4 				With    ucrsTempBi.desc4
				Replace Bi.CCUSTO 				With    ucrsTempBi.CCUSTO
				Replace Bi.num1 				With    ucrsTempBi.num1
				Replace Bi.pbruto 				With    ucrsTempBi.pbruto
				Replace Bi.ecustoind 			With    ucrsTempBi.ecustoind
				Replace Bi.BOSTAMP 				With    ucrsTempBi.BOSTAMP
				Replace Bi.ousrinis 			With    ucrsTempBi.ousrinis
				Replace Bi.ousrdata 			With    ucrsTempBi.ousrdata
				Replace Bi.ousrHORA 			With    ucrsTempBi.ousrHORA
				Replace Bi.usrinis 				With    ucrsTempBi.usrinis
				Replace Bi.usrdata 				With    ucrsTempBi.usrdata
				Replace Bi.usrhora 				With    ucrsTempBi.usrhora
				Replace Bi.edebitoori 			With    ucrsTempBi.edebitoori
				Replace Bi.u_mquebra 			With    ucrsTempBi.u_mquebra
				Replace Bi.u_epv1act 			With    ucrsTempBi.u_epv1act
				Replace Bi.u_stockact 			With    ucrsTempBi.u_stockact
				Replace Bi.u_bencont 			With    ucrsTempBi.u_bencont
				Replace Bi.u_psicont 			With    ucrsTempBi.u_psicont
				Replace Bi.u_bonus 				With    ucrsTempBi.u_bonus
				Replace Bi.u_upc 				With    ucrsTempBi.u_upc
				Replace Bi.u_reserva 			With    ucrsTempBi.u_reserva
				Replace Bi.unidade 				With    ucrsTempBi.unidade
				Replace Bi.ivaincl 				With    ucrsTempBi.ivaincl
				Replace Bi.ESTAB 				With    ucrsTempBi.ESTAB
				Replace Bi.nopat 				With    ucrsTempBi.nopat
				Replace Bi.usr1 				With    ucrsTempBi.usr1
				Replace Bi.usr2 				With    ucrsTempBi.usr2
				Replace Bi.usr3 				With    ucrsTempBi.usr3
				Replace Bi.usr4 				With    ucrsTempBi.usr4
				Replace Bi.usr5 				With    ucrsTempBi.usr5
				Replace Bi.usr6 				With    ucrsTempBi.usr6
				Replace Bi.stns 				With    ucrsTempBi.stns
				Replace Bi.emconf 				With    ucrsTempBi.emconf
				Replace Bi.marcada 				With    ucrsTempBi.marcada
				Replace Bi.cativo 				With    ucrsTempBi.cativo
				Replace Bi.resusr 				With    ucrsTempBi.resusr
				Replace Bi.resrec 				With    ucrsTempBi.resrec
				Replace Bi.fno 					With    ucrsTempBi.fno
				Replace Bi.nmdoc 				With    ucrsTempBi.nmdoc
				Replace Bi.ndoc 				With    ucrsTempBi.ndoc
				Replace Bi.partes2 				With    ucrsTempBi.partes2
				Replace Bi.partes 				With    ucrsTempBi.partes
				Replace Bi.oftstamp 			With    ucrsTempBi.oftstamp
				Replace Bi.fdata 				With    ucrsTempBi.fdata
				Replace Bi.ofostamp 			With    ucrsTempBi.ofostamp
				Replace Bi.debito 				With    ucrsTempBi.debito
				Replace Bi.ttdeb 				With    ucrsTempBi.ttdeb
				Replace Bi.slvumoeda 			With    ucrsTempBi.slvumoeda
				Replace Bi.bifref 				With    ucrsTempBi.bifref
				Replace Bi.ncusto 				With    ucrsTempBi.ncusto
				Replace Bi.slvu 				With    ucrsTempBi.slvu
				Replace Bi.eslvu 				With    ucrsTempBi.eslvu
				Replace Bi.sltt 				With    ucrsTempBi.sltt
				Replace Bi.esltt 				With    ucrsTempBi.esltt
				Replace Bi.cor 					With    ucrsTempBi.cor
				Replace Bi.tam 					With    ucrsTempBi.tam
				Replace Bi.producao 			With    ucrsTempBi.producao
				Replace Bi.composto 			With    ucrsTempBi.composto
				Replace Bi.trocaequi 			With    ucrsTempBi.trocaequi
				Replace Bi.slttmoeda 			With    ucrsTempBi.slttmoeda
				Replace Bi.pcusto 				With    ucrsTempBi.pcusto
				Replace Bi.u_desccom 			With    ucrsTempBi.u_desccom
				Replace Bi.u_nodesc 			With    ucrsTempBi.u_nodesc
				Replace Bi.EXPORTADO 			With    ucrsTempBi.EXPORTADO
				Replace Bi.binum3 				With    ucrsTempBi.binum3
				Replace Bi.qtrec 				With    ucrsTempBi.qtrec
				Replace Bi.diploma 				With    ucrsTempBi.diploma
				Replace Bi.descval 				With    ucrsTempBi.descval
				Replace Bi.exepcaoTrocaMed 		With    ucrsTempBi.exepcaoTrocaMed
				Replace Bi.comunicada_ws 		With    ucrsTempBi.comunicada_ws
				Replace Bi.comunicada_ws_descr 	With    ucrsTempBi.comunicada_ws_descr
				Replace Bi.comunicada 			With    ucrsTempBi.comunicada



			Endscan

		Else
			Select ucrsTempFn
			Goto Top
			Scan

				Select FN
				Append Blank
				Replace fnstamp 		With ucrsTempFn.fnstamp
				Replace ref 			With ucrsTempFn.ref
				Replace Design 		With ucrsTempFn.Design
				Replace docnome 		With ucrsTempFn.docnome
				Replace Adoc 			With ucrsTempFn.Adoc
				Replace unidade 		With ucrsTempFn.unidade
				Replace qtt 			With ucrsTempFn.qtt
				Replace Pv 			With ucrsTempFn.Pv
				Replace IVA 			With ucrsTempFn.IVA
				Replace tiliquido 		With ucrsTempFn.tiliquido
				Replace ivaincl 		With ucrsTempFn.ivaincl
				Replace tabiva 		With ucrsTempFn.tabiva
				Replace ARMAZEM 		With ucrsTempFn.ARMAZEM
				Replace cpoc 			With ucrsTempFn.cpoc
				Replace composto 		With ucrsTempFn.composto
				Replace lordem 		With ucrsTempFn.lordem
				Replace fmarcada 		With ucrsTempFn.fmarcada
				Replace Data 			With ucrsTempFn.Data
				Replace etiliquido 	With ucrsTempFn.etiliquido
				Replace epv 			With ucrsTempFn.epv
				Replace uni2qtt 		With ucrsTempFn.uni2qtt
				Replace codigo 		With ucrsTempFn.codigo
				Replace LOTE 			With ucrsTempFn.LOTE
				Replace slvu 			With ucrsTempFn.slvu
				Replace eslvu 			With ucrsTempFn.eslvu
				Replace sltt 			With ucrsTempFn.sltt
				Replace esltt 			With ucrsTempFn.esltt
				Replace slvumoeda 		With ucrsTempFn.slvumoeda
				Replace bistamp 		With ucrsTempFn.bistamp
				Replace stns 			With ucrsTempFn.stns
				Replace fnccusto 		With ucrsTempFn.fnccusto
				Replace usr1 			With ucrsTempFn.usr1
				Replace usr2 			With ucrsTempFn.usr2
				Replace usr3 			With ucrsTempFn.usr3
				Replace usr4 			With ucrsTempFn.usr4
				Replace usr5 			With ucrsTempFn.usr5
				Replace usr6 			With ucrsTempFn.usr6
				Replace cor 			With ucrsTempFn.cor
				Replace tam 			With ucrsTempFn.tam
				Replace familia 		With ucrsTempFn.familia
				Replace oref 			With ucrsTempFn.oref
				Replace desconto 		With ucrsTempFn.desconto
				Replace desc2 			With ucrsTempFn.desc2
				Replace desc3 			With ucrsTempFn.desc3
				Replace desc4 			With ucrsTempFn.desc4
				Replace descFin_pcl	With ucrsTempFn.descFin_pcl
				Replace num1 			With ucrsTempFn.num1
				Replace pbruto 		With ucrsTempFn.pbruto
				Replace ofnstamp 		With ucrsTempFn.ofnstamp
				Replace FOSTAMP 		With ucrsTempFn.FOSTAMP
				Replace ousrinis 		With ucrsTempFn.ousrinis
				Replace ousrdata 		With ucrsTempFn.ousrdata
				Replace ousrHORA 		With ucrsTempFn.ousrHORA
				Replace usrinis 		With ucrsTempFn.usrinis
				Replace usrdata 		With ucrsTempFn.usrdata
				Replace usrhora 		With ucrsTempFn.usrhora
				Replace marcada 		With ucrsTempFn.marcada
				Replace svdata 		With ucrsTempFn.svdata
				Replace sujinv 		With ucrsTempFn.sujinv
				Replace u_upc 			With ucrsTempFn.u_upc
				Replace u_desccom 		With ucrsTempFn.u_desccom
				Replace u_bonusmrg 	With ucrsTempFn.u_bonusmrg
				Replace u_validade 	With ucrsTempFn.u_validade
				Replace U_PVP 			With ucrsTempFn.U_PVP
				Replace u_epv1act 		With ucrsTempFn.u_epv1act
				Replace u_validact 	With ucrsTempFn.u_validact
				Replace u_stockact 	With ucrsTempFn.u_stockact
				Replace u_bonus 		With ucrsTempFn.u_bonus
				Replace u_marg1act 	With ucrsTempFn.u_marg1act
				Replace u_psicont 		With ucrsTempFn.u_psicont
				Replace u_bencont 		With ucrsTempFn.u_bencont
				Replace u_nodesc 		With ucrsTempFn.u_nodesc
				Replace u_margupc 		With ucrsTempFn.u_margupc
				Replace u_margpct 		With ucrsTempFn.u_margpct
				Replace u_condicao 	With ucrsTempFn.u_condicao
				Replace u_qttenc 		With ucrsTempFn.u_qttenc
				Replace u_upce 		With ucrsTempFn.u_upce
				Replace u_oqt 			With ucrsTempFn.u_oqt
				Replace u_opc 			With ucrsTempFn.u_opc
				Replace u_reserva 		With ucrsTempFn.u_reserva
				Replace tmoeda 		With ucrsTempFn.tmoeda
				Replace fnfref 		With ucrsTempFn.fnfref
				Replace fnncusto 		With ucrsTempFn.fnncusto
				Replace taxaiva 		With ucrsTempFn.taxaiva
				Replace EXPORTADO 		With ucrsTempFn.EXPORTADO
				Replace qtrec 			With ucrsTempFn.qtrec
				Replace stamp_ext_doc 	With ucrsTempFn.stamp_ext_doc
				Replace u_descenc 		With ucrsTempFn.u_descenc
				Replace u_dtval 		With ucrsTempFn.u_dtval
				Replace val_desp_lin 	With ucrsTempFn.val_desp_lin
				Replace fistamp 		With ucrsTempFn.fistamp

			Endscan

		Endif
		uf_documentos_criaNovaLinha(.F.,.F.)
		DOCUMENTOS.PageFrame1.Page1.gridDoc.Refresh

	Endif


Endfunc




Function uf_documentos_validaLotesVet

	Local lcStamp


	If uf_gerais_compstr(myTipoDoc, "BO")
		If uf_gerais_getUmvalor("tS", "cmstocks", "ndos = '" + Str(cabdoc.NUMINTERNODOC) + "'")  > 0
			Select Bi
			Go Top
			lcStamp = Bi.bistamp

			Select Bi.ref, Bi.LOTE From Bi ;
				WHERE Bi.familia In ("97", "98");
				INTO Cursor uc_tmpFIVet

			Select Bi
			Locate For Bi.bistamp 	= lcStamp

			Select uc_tmpFIVet
			Go Top
			Scan
				If Empty(uc_tmpFIVet.LOTE)
					Return uc_tmpFIVet.ref
				Endif
			Endscan

			fecha("uc_tmpFIVet")

			Return ''

		Endif

	Else
		If uf_gerais_getUmvalor("cm1", "folansl", "codigoDoc = '" + Str(cabdoc.NUMINTERNODOC) + "'")
			Select FN
			Go Top
			lcStamp = FN.fnstamp
			Select FN.ref, FN.LOTE From FN ;
				WHERE FN.familia In ("97", "98");
				INTO Cursor uc_tmpFIVet

			Select FN
			Locate For FN.fnstamp 	= lcStamp
			Select uc_tmpFIVet
			Go Top
			Scan

				If Empty(uc_tmpFIVet.LOTE)
					Return uc_tmpFIVet.ref
				Endif
			Endscan

			fecha("uc_tmpFIVet")

			Return ''

		Endif

	Endif


	Return ''
Endfunc
