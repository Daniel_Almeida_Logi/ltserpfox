**
FUNCTION uf_condicoesComerciais_chama

	IF !used("cabDoc")
		uf_perguntalt_chama("Esta op��o s� est� dipon�vel no documento [Condicoes Comerc. Forn.]!", "", "OK", 64)
		return .f.
	ENDIF

	select cabDoc
	IF UPPER(ALLTRIM(cabDoc.doc)) != UPPER(ALLTRIM("Condicoes Comerc. Forn."))
		uf_perguntalt_chama("Esta op��o s� est� dipon�vel no documento [Condicoes Comerc. Forn.]!", "", "OK", 64)
		return .f.
	ENDIF

	Public myCONDICOESCOMERCIAISIntroducao, myCONDICOESCOMERCIAISAlteracao

	TEXT TO lcSQL TEXTMERGE NOSHOW
		Select sel = convert(bit,0), nome,bdname,odbc, aplicado = convert(bit,0), sede, no, estab from condComercConfig
	ENDTEXT 
	IF !uf_gerais_actgrelha("", "ucrsCondicoesClientes", lcSql)
		MESSAGEBOX("N�o foi possivel verificar os clientes para aplicar as defini��es. Contacte o suporte.",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF

	IF TYPE("CONDICOESCOMERCIAIS") == "U"
		DO FORM CONDICOESCOMERCIAIS
	ELSE
		CONDICOESCOMERCIAIS.show()
	ENDIF
	
	uf_condicoesComerciais_actualizaClientes()
	
ENDFUNC


**
FUNCTION uf_condicoesComerciais_actualizaClientes

	Select CabDoc						
	TEXT TO lcSQL TEXTMERGE NOSHOW
		Select 
			sel = convert(bit,0)
			,nome
			,bdname
			,odbc
			,aplicado = case when (select count(bostamp) from condComercLink where bostamp = '<<CabDoc.cabstamp>>' and condComercLink.bdname = condComercConfig.bdname) = 0 then  convert(bit,0) else  convert(bit,1) end
			,sede
			,no
			,estab
		from 
			condComercConfig
	ENDTEXT 
	IF !uf_gerais_actgrelha("CONDICOESCOMERCIAIS.GridPesq", "ucrsCondicoesClientes", lcSql)
		MESSAGEBOX("N�o foi possivel verificar os clientes para aplicar as defini��es. Contacte o suporte.",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF
	
	CONDICOESCOMERCIAIS.GridPesq.refresh
ENDFUNC



**
FUNCTION uf_CONDICOESCOMERCIAIS_init
	
	&& Configura menu principal
	WITH CONDICOESCOMERCIAIS.menu1
		.adicionaOpcao("aplicar","Aplicar",myPath + "\imagens\icons\ferramentas_w.png","uf_CONDICOESCOMERCIAI_AplicarCondicoes", "A")
		.adicionaOpcao("seltodas","Sel.Todos",myPath + "\imagens\icons\unchecked_w.png","uf_CONDICOESCOMERCIAIS_inverteSelecao","T")
		.adicionaOpcao("editar","Editar",myPath + "\imagens\icons\lapis_w.png","uf_CONDICOESCOMERCIAIS_editar","E")
		.adicionaOpcao("reset","Reset",myPath + "\imagens\icons\unchecked_w.png","uf_CONDICOESCOMERCIAIS_reset","R")
	ENDWITH

	uf_CONDICOESCOMERCIAIS_alternaMenu()
ENDFUNC


**
FUNCTION uf_CONDICOESCOMERCIAIS_alternaMenu
	
	WITH CONDICOESCOMERCIAIS.menu1	
		IF myCONDICOESCOMERCIAISIntroducao == .t. OR myCONDICOESCOMERCIAISAlteracao == .t.
			.estado("", "SHOW", "Gravar", .t., "Cancelar", .t.)
			.estado("editar,aplicar", "HIDE")
			
			WITH CONDICOESCOMERCIAIS.GridPesq
				For i=1 To .columncount
					IF .Columns(i).name != "sel" AND .Columns(i).name != "aplicado"
						.Columns(i).readonly = .f.
					ENDIF
				ENDFOR
			ENDWITH
		ELSE 
			.estado("aplicar, editar", "SHOW", "Gravar", .f., "Sair", .t.)
			.estado("", "HIDE")
			
			WITH CONDICOESCOMERCIAIS.GridPesq
				For i=1 To .columncount
					IF .Columns(i).name != "sel" AND .Columns(i).name != "aplicado"
						.Columns(i).readonly = .t.
					ENDIF
				ENDFOR
			ENDWITH
		ENDIF 
	ENDWITH

		
	CONDICOESCOMERCIAIS.refresh
ENDFUNC


**
FUNCTION uf_CONDICOESCOMERCIAIS_inverteSelecao

	IF CONDICOESCOMERCIAIS.menu1.seltodas.tag == 'false'
	
		&& aplica sele��o
		CONDICOESCOMERCIAIS.menu1.seltodas.tag = 'true'
		
		&& altera o botao
		CONDICOESCOMERCIAIS.menu1.seltodas.config("Sel.Todos", myPath + "\imagens\icons\checked_w.png", "uf_CONDICOESCOMERCIAIS_inverteSelecao","C")
		
		Select ucrsCondicoesClientes
		Go Top
		Scan
			Replace ucrsCondicoesClientes.sel with .t.
		Endscan
		Select ucrsCondicoesClientes
		Go Top

	ELSE
	
		&& aplica sele��o
		CONDICOESCOMERCIAIS.menu1.seltodas.tag = 'false'
		
		&& altera o botao
		CONDICOESCOMERCIAIS.menu1.seltodas.config("Sel.Todos", myPath + "\imagens\icons\unchecked_w.png", "uf_CONDICOESCOMERCIAIS_inverteSelecao","C")
		
		Select ucrsCondicoesClientes
		Go Top
		Scan
			Replace ucrsCondicoesClientes.sel with .f.
		Endscan
		Select ucrsCondicoesClientes
		Go Top
		
	ENDIF

	CONDICOESCOMERCIAIS.refresh

ENDFUNC


**
FUNCTION uf_CONDICOESCOMERCIAIS_Reset

	IF CONDICOESCOMERCIAIS.menu1.reset.tag == 'false'
		&& aplica sele��o
		CONDICOESCOMERCIAIS.menu1.reset.tag = 'true'
		&& altera o botao
		CONDICOESCOMERCIAIS.menu1.reset.config("Reset", myPath + "\imagens\icons\checked_w.png", "uf_CONDICOESCOMERCIAIS_reset","R")
	ELSE
		&& aplica sele��o
		CONDICOESCOMERCIAIS.menu1.reset.tag = 'false'
		&& altera o botao
		CONDICOESCOMERCIAIS.menu1.reset.config("Reset", myPath + "\imagens\icons\unchecked_w.png", "uf_CONDICOESCOMERCIAIS_reset","R")
	ENDIF
	
	CONDICOESCOMERCIAIS.refresh

ENDFUNC


** 
FUNCTION uf_CONDICOESCOMERCIAIS_editar

	IF myCONDICOESCOMERCIAISIntroducao==.f. AND myCONDICOESCOMERCIAISAlteracao==.f.
		myCONDICOESCOMERCIAISAlteracao=.t.
		uf_CONDICOESCOMERCIAIS_alternaMenu()			
	ENDIF
	
	CONDICOESCOMERCIAIS.refresh

ENDFUNC


**
FUNCTION uf_condicoescomerciais_novaLinha
	
	IF myCONDICOESCOMERCIAISIntroducao== .f. AND myCONDICOESCOMERCIAISAlteracao== .f.
		
		uf_perguntalt_chama("Deve colocar o ecr� em edi��o para alterar as definic�es dos clientes!", "", "OK", 64)
		return .f.
	ENDIF
	
	
	SELECT ucrsCondicoesClientes
	APPEND BLANK

	CONDICOESCOMERCIAIS.GridPesq.refresh
ENDFUNC


**
FUNCTION uf_condicoescomerciais_apagaLinha

	IF myCONDICOESCOMERCIAISIntroducao== .f. AND myCONDICOESCOMERCIAISAlteracao== .f.
		
		uf_perguntalt_chama("Deve colocar o ecr� em edi��o para alterar as definic�es dos clientes!", "", "OK", 64)
		return .f.
	ENDIF
	
	select ucrsCondicoesClientes
	DELETE 
	
	Select ucrsCondicoesClientes
	lcPos = recno()
	select ucrsCondicoesClientes
	TRY
		IF lcPos>1
			go lcPos-1
		ENDIF
	CATCH
		**
	ENDTRY

	CONDICOESCOMERCIAIS.GridPesq.refresh
	
	select ucrsCondicoesClientes
	GO BOTTOM 
ENDFUNC



**
FUNCTION uf_condicoescomerciais_eliminaLinha

	SELECT ucrsCondicoesClientes
	APPEND BLANK

	CONDICOESCOMERCIAIS.GridPesq.refresh
ENDFUNC


**
FUNCTION uf_CONDICOESCOMERCIAI_AplicarCondicoes
	LOCAL lcBody, lcFooter, lcSubject, lcTo, lcAttach, lcPath, lcAppprtScreen

	IF myCONDICOESCOMERCIAISIntroducao==.t. OR myCONDICOESCOMERCIAISAlteracao==.t.
		return .f.	
	ENDIF

	Select * from ucrsCondicoesClientes where sel = .t. and aplicado = .t. into CURSOR ucrsVerificaExistenciaCComericais readwrite 
	Select ucrsVerificaExistenciaCComericais
	IF RECCOUNT("ucrsVerificaExistenciaCComericais")>0
		IF !uf_perguntalt_chama("SELECIONOU ALGUNS CLIENTES QUE J� T�M AS CONDI��ES APLICADAS, DESEJA SUBSTITUIR AS CONDI��ES COMERCIAIS NOS CLIENTES?","Sim","N�o")
			return .f.
		ENDIF
	ENDIF
	IF USED("ucrsVerificaExistenciaCComericais")
		fecha("ucrsVerificaExistenciaCComericais")
	ENDIF	
	
	If !uf_perguntalt_chama("ATEN��O: VAI REFLECTIR AS ALTERA��ES NA FICHA DO PRODUTO NAS BDs DOS CLIENTES. PRETENDE CONTINUAR?","Sim","N�o")
		RETURN .f.
	ENDIF
	
	regua(0,reccount("ucrsCondicoesClientes"),"A actualizar BDs dos clientes...")
	regua(1,1,"A actualizar BDs dos clientes...")
	
	Select ucrsCondicoesClientes
	Go Top
	Scan for ucrsCondicoesClientes.sel == .t.
		
			regua(1,recno(),"A actualizar BDs do cliente:" + ALLTRIM(ucrsCondicoesClientes.nome))
		
			** Constroi liga��o � BD do cliente 
			IF uf_condicoesComerciais_ligacaoBDCliente(ucrsCondicoesClientes.bdname,ucrsCondicoesClientes.odbc)

				lcSQL=''
				TEXT TO lcSQl TEXTMERGE NOSHOW
					exec up_keiretsu_ResetCondicoesComerciais <<IIF(CONDICOESCOMERCIAIS.menu1.reset.tag == 'true',0,cabDoc.numdoc)>>
				ENDTEXT

				IF !uf_condicoesComerciais_sqlExec(lcSQL,"ucrsUpdate")
					uf_perguntalt_chama("OCORREU UM ERRO A ACTUALIZAR CONDI��ES COMERCIAIS.","OK","", 16)
					RETURN .f.
				ENDIF
				
				uf_condicoesComerciais_disconnect()						
			ENDIF		
	
			Select ucrsCondicoesClientes
		
	ENDSCAN
	
	regua(2)
	uf_condicoesComerciais_actualizaClientes()
	
	uf_perguntalt_chama("BDs dos clientes actualizados com sucesso.","","OK")
ENDFUNC 



**
FUNCTION uf_condicoesComerciais_ligacaoBDCliente
	lparameters lcNomeBD, lcOdbc
	LOCAL lcConStr, lcSQL

	STORE "" TO lcConStr, lcSQL
	lcConStr = "DSN=" + ALLTRIM(lcOdbc) + ";UID=logiserv;PWD=ls2k12...;DATABASE="+ALLTRIM(lcNomeBD)+";"

	IF uf_condicoesComerciais_connectDB(lcConStr)
		Return .t.
	ELSE
		RETURN .f.
	ENDIF 

ENDFUNC


**
FUNCTION uf_condicoesComerciais_connectDB
	LPARAMETERS lcConnection_string

	PUBLIC gnConnHandle2
	
	gnConnHandle2=SQLSTRINGCONN(lcConnection_string)

	IF gnConnHandle2 > 0
	  RETURN .t.
	ELSE
	  MESSAGEBOX("Connection Failed",16,"ODBC Problem")
	  RETURN .f.
	ENDIF

	RETURN .f.
ENDFUNC 

**
FUNCTION uf_condicoesComerciais_sqlExec
	LPARAMETERS lcSQL, lcCursor
	
	IF TYPE("lcCursor") != "C"
		lcCursor = ""
	ENDIF 
	
	TRY
		IF !EMPTY(lcCursor)
			TABLEUPDATE(2,.t.,lcCursor)
		ELSE
			TABLEUPDATE()
		ENDIF
	CATCH
		**
	FINALLY 
		**
	ENDTRY
	 
	IF SQLEXEC(gnConnHandle2,lcSQL,lcCursor) < 0
		RETURN .f.
	ELSE	
		RETURN .t.
	ENDIF 
ENDFUNC


FUNCTION uf_condicoesComerciais_disconnect
	TRY
		SQLDISCONN(gnConnHandle2)
	CATCH
	ENDTRY
ENDFUNC 


**
FUNCTION uf_condicoesComerciais_gravar

	** ELimina defini��es anteriores
	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE
		delete from condComercConfig
	ENDTEXT
	IF !uf_gerais_actGrelha("", "",lcSQL)
		MESSAGEBOX("OCORREU UM ERRO A GRAVAR AS DEFINI�OES! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		return .f.
	ENDIF		
	
	SELECT ucrsCondicoesClientes
	Go Top
	SCAN
		lcSQL = ""
		TEXT TO lcSQL NOSHOW TEXTMERGE
			INSERT INTO	condComercConfig (nome,bdname,odbc,no,estab)
			VALUES('<<ucrsCondicoesClientes.nome>>','<<ucrsCondicoesClientes.bdname>>','<<ucrsCondicoesClientes.odbc>>',<<ucrsCondicoesClientes.no>>,<<ucrsCondicoesClientes.estab>>)
		ENDTEXT

		IF !uf_gerais_actGrelha("", "",lcSQL)
			MESSAGEBOX("OCORREU UM ERRO A GRAVAR AS DEFINI�OES! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
			return .f.
		ENDIF		
	ENDSCAN
	
	myCONDICOESCOMERCIAISIntroducao= .f.
	myCONDICOESCOMERCIAISAlteracao= .f.
	uf_CONDICOESCOMERCIAIS_alternaMenu()
	
	uf_perguntalt_chama("Defini��es dos clientes gravados com sucesso!", "", "OK", 64)
	
ENDFUNC


**
Function uf_condicoesComerciais_ListaODBC	

	&& calcular DataSources
	DECLARE SHORT SQLDataSources IN odbc32;
    INTEGER   EnvironmentHandle,;
    INTEGER   Direction,;
    STRING  @ ServerName,;
    INTEGER   BufferLength1,;
    INTEGER @ NameLength1Ptr,;
    STRING  @ Description,;
    INTEGER   BufferLength2,;
    INTEGER @ NameLength2Ptr

	LOCAL lnRet, lnEnvHandle, lcDriver, lnBufferLen
	lnEnvHandle = VAL(SYS(3053))
	lnBufferLen = 1024
	lcDriver = REPLICATE(CHR(0),lnBufferLen)
	lnRet = SQLDataSources(lnEnvHandle, 2, @lcDriver, lnBufferLen, @lnBufferLen, 0, 0, 0)

	CREATE CURSOR uCrsDSouces (Name CHAR (100))
	lnCount = 1
	DO WHILE lnRet = 0
	  INSERT INTO uCrsDSouces VALUES(LEFT(lcDriver,lnBufferLen))
	  lnBufferLen = 1024
	  lcDriver = REPLICATE(CHR(0),lnBufferLen)
	  lnRet = SQLDataSources(lnEnvHandle, 1, @lcDriver, lnBufferLen, @lnBufferLen, 0, 0, 0)
	ENDDO
	GO TOP
ENDFUNC


&& Grava Encomendas na BD Central e marca encomenda como enviada logi1 = .t.
FUNCTION uf_condicoesComerciais_enviarEncomenda
	Local lcControlaEnvio
	lcControlaEnvio = .f.

	Select CabDoc

	TEXT TO lcSQL TEXTMERGE NOSHOW
		Select
			top 1 nome
			,no
			,estab
			,bdname
			,odbc
			,sede
		from
			condComercConfig
		Where
			sede = 1
	ENDTEXT
	IF !uf_gerais_actgrelha("", "ucrsEnviaEncomendasSede", lcSql)
		MESSAGEBOX("N�o foi possivel verificar os dados do servidor para envio [Tabela condComercConfig]. Contacte o suporte.",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF

	regua(0,4,"A enviar encomenda...")
	regua(1,1,"A enviar encomenda...")

	** Constroi cursores do documento de gest�o a passar para os clientes
	TEXT To lcSQL noshow TEXTMERGE
		Select * from bo where bostamp = '<<CabDoc.cabstamp>>'
	ENDTEXT
	IF !uf_gerais_actGrelha("", "ucrsCabDocEncomendaSede",lcSQL)
		MESSAGEBOX("OCORREU UM ERRO A VERIFICAR DADOS DO DOCUMENTO! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		return .f.
	ENDIF	
	TEXT To lcSQL noshow TEXTMERGE
		Select * from bo2 where bo2stamp = '<<CabDoc.cabstamp>>'
	ENDTEXT
	IF !uf_gerais_actGrelha("", "ucrsbo2EncomendaSede",lcSQL)
		MESSAGEBOX("OCORREU UM ERRO A VERIFICAR DADOS DO DOCUMENTO! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		return .f.
	ENDIF
	TEXT To lcSQL noshow TEXTMERGE
		Select * from bi where bostamp = '<<CabDoc.cabstamp>>'
	ENDTEXT
	IF !uf_gerais_actGrelha("", "ucrsbiEncomendaSede",lcSQL)
		MESSAGEBOX("OCORREU UM ERRO A VERIFICAR DADOS DO DOCUMENTO! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		return .f.
	ENDIF
	TEXT To lcSQL noshow TEXTMERGE
		Select * from bi2 where bostamp = '<<CabDoc.cabstamp>>'
	ENDTEXT
	IF !uf_gerais_actGrelha("", "ucrsbi2EncomendaSede",lcSQL)
		MESSAGEBOX("OCORREU UM ERRO A VERIFICAR DADOS DO DOCUMENTO! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		return .f.
	ENDIF	
	regua(1,2,"A enviar encomenda...")
	
	
	**
	Select ucrsCabDocEncomendaSede
	IF ucrsCabDocEncomendaSede.logi1 == .t.
		uf_perguntalt_chama("Encomenda j� foi enviada, n�o � possivel enviar a mesma encomenda duas vezes.", "", "OK", 64)
		regua(2)
		return .t.
	ENDIF
	
	
	** Constroi liga��o � BD da sede
	Select ucrsEnviaEncomendasSede
	IF uf_condicoesComerciais_ligacaoBDCliente(ucrsEnviaEncomendasSede.bdname,ucrsEnviaEncomendasSede.odbc)
		regua(1,3,"A enviar encomenda...")
		
		lcExisteClienteNaSede = uf_condicoesComerciais_ValidaExisteClientesNaSede()
		IF lcExisteClienteNaSede == .f.
			regua(2)
			return .f.
		ENDIF 
				
		lcValidaExisteSt = uf_condicoesComerciais_ValidaExisteStNaSede()
		IF lcValidaExisteSt == .t.
			lcControlaEnvio =  uf_condicoesComerciais_insereEncomendaNaSede()
		ELSE
			MESSAGEBOX("OS PRODUTOS SELECIONADOS NA ENCOMENDA N�O EXISTEM NA BASE DE DADOS DO FORNECEDOR. N�O � POSSIVEL PROCEDER AO ENVIO DA ENCOMENDA.",16,"LOGITOOLS SOFTWARE")
			regua(2)
			return .f.
		ENDIF
	ENDIF		

	IF lcControlaEnvio == .t.

		SElect CabDoc
		** Constroi cursores do documento de gest�o a passar para os clientes
		TEXT To lcSQL noshow TEXTMERGE
			update bo set logi1= 1 where bostamp = '<<CabDoc.cabstamp>>'
		ENDTEXT
		IF !uf_gerais_actGrelha("", "",lcSQL)
			MESSAGEBOX("OCORREU UM ERRO A MARCAR ENCOMENDA COM O ENVIADA! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
			return .f.
		ENDIF
	ELSE
		uf_perguntalt_chama("N�o foi possivel enviar a encomenda!", "", "OK", 64)
	ENDIF

	Select cabDoc
	uf_documentos_Chama(cabDoc.cabstamp)

	uf_perguntalt_chama("Encomenda enviada com sucesso!", "", "OK", 64)
	regua(2)
ENDFUNC


** Caso n�o exista referencia na BD da sede elimina, n�o � possivel encomendar
FUnction uf_condicoesComerciais_ValidaExisteStNaSede
	Local lcNumeroregistos
	lcNumeroregistos = 0

	Select ucrsbiEncomendaSede
	Go Top
	scan
		
		TEXT To lcSQL noshow TEXTMERGE
			Select ref from st (nolock) where ref = '<<ucrsbiEncomendaSede.ref>>'
		ENDTEXT
		IF !uf_condicoesComerciais_sqlExec(lcSQL,"ucrsVerificaStSede")
			MESSAGEBOX("OCORREU UM ERRO A VERIFICAR EXISTENCIA DO PRODUTO NA SEDE! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
			return .f.
		ENDIF
		IF RECCOUNT("ucrsVerificaStSede")== 0
			SElect ucrsbiEncomendaSede
			delete
		ENDIF
		
		SElect ucrsbiEncomendaSede
	ENDscan


	Select ucrsbiEncomendaSede
	Count to lcNumeroregistos
	IF lcNumeroregistos ==0
		RETURN .F.
	else
		RETURN .T.
	endif
	
ENDFUNC


**
Function uf_condicoesComerciais_ValidaExisteClientesNaSede
	Local lcSQL

	Select ucrsEnviaEncomendasSede
	select ucrsE1

	lcSQL = ""
	TEXT TO lcSQL NOSHOW TEXTMERGE 
		select * from b_utentes (nolock) Where ncont = '<<ALLTRIM(ucrsE1.ncont)>>' 
	ENDTEXT
	IF !uf_condicoesComerciais_sqlExec(lcSQL,"ucrsClienteNaSede")
		uf_perguntalt_chama("OCORREU UM ERRO A VERIFICAR EXISTENCIA DE CLIENTE NA SEDE. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN .f.
	ENDIF

	Select ucrsClienteNaSede
	IF reccount("ucrsClienteNaSede") == 0 && N�o existe na Sede vai criar novo cliente
		uf_perguntalt_chama("CLIENTE N�O CONFIGURADO NO FORNECEDOR DE GRUPO.","OK","",16)
		RETURN .f.
	ENDIF
	
	RETURN .t.
ENDfunc 



**
Function uf_condicoesComerciais_insereEncomendaNaSede
	Local lcBoStamp
	
	** Constroi cursores do documento de gest�o a passar para os clientes
	Select ucrsCabDocEncomendaSede

	lcBoStamp = uf_gerais_stamp()

	
	
	TEXT TO lcSQL NOSHOW textmerge
		select numero = isnull(max(obrano),0)+1 from bo where ndos = 41
	Endtext
	IF !uf_condicoesComerciais_sqlExec(lcSQL,"ucrsNumeroEncCliente")
		uf_perguntalt_chama("OCORREU UM ERRO A GRAVAR O CABE�ALHO DO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE."+CHR(10)+"Cod: DOC-BO","OK","",16)
		RETURN
	Endif
	
	Select ucrsNumeroEncCliente
	
	lcSQL = ""
	TEXT TO lcSQL NOSHOW textmerge
		Insert into bo
		(	
			bostamp
			,ndos
			,nmdos
			,obrano
			,dataobra
			,nome
			,nome2
			,no
			,estab
			,morada
			,local
			,codpost
			,tipo
			,tpdesc
			,ncont
			,boano
			,etotaldeb
			,dataopen
			,ecusto
			,etotal
			,moeda
			,memissao
			,origem
			,site
			,vendedor
			,vendnm
			,sqtt14
			,ebo_1tvall
			,ebo_2tvall
			,ebo_totp1
			,ebo_totp2
			,ebo11_bins
			,ebo12_bins
			,ebo21_bins
			,ebo22_bins
			,ebo31_bins
			,ebo32_bins
			,ebo41_bins
			,ebo42_bins
			,ebo51_bins
			,ebo52_bins
			,ebo11_iva
			,ebo12_iva
			,ebo21_iva
			,ebo22_iva
			,ebo31_iva
			,ebo32_iva
			,ebo41_iva
			,ebo42_iva
			,ebo51_iva
			,ebo52_iva
			,ocupacao
			,obs
			,ousrinis
			,ousrdata
			,ousrhora
			,usrinis
			,usrdata
			,usrhora
			,datafinal
			,ccusto
			,pnome
			,pno
			
		)Values (
			'<<ALLTRIM(ucrsCabDocEncomendaSede.bostamp)>>'
			,41
			,'Encomenda de Cliente'
			,<<ucrsNumeroEncCliente.numero>>
			,'<<uf_gerais_getDate(getdate(),"SQL")>>'
			,'<<ucrsClienteNaSede.nome>>'
			,''
			,<<ucrsClienteNaSede.no>>
			,<<ucrsClienteNaSede.estab>>
			,'<<ALLTRIM(ucrsClienteNaSede.morada)>>'
			,'<<ALLTRIM(ucrsClienteNaSede.local)>>'
			,'<<ALLTRIM(ucrsClienteNaSede.codPost)>>'
			,'<<ALLTRIM(ucrsClienteNaSede.tipo)>>'
			,'<<Alltrim(ucrsClienteNaSede.tpdesc)>>'
			,'<<ALLTRIM(ucrsClienteNaSede.ncont)>>'
			,Year(dateadd(HOUR, <<difhoraria>>, getdate()))
			,<<ucrsCabDocEncomendaSede.etotaldeb>>
			,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
			,<<ucrsCabDocEncomendaSede.ecusto>>
			,<<ucrsCabDocEncomendaSede.etotal>>
			,'<<ucrsCabDocEncomendaSede.Moeda>>'
			,'EURO'
			,'BO'
			,'<<ALLTRIM(mySite)>>'
			,<<ucrsCabDocEncomendaSede.vendedor>>
			,'<<ucrsCabDocEncomendaSede.vendnm>>'
			,<<ucrsCabDocEncomendaSede.sqtt14>>
			,<<ucrsCabDocEncomendaSede.ebo_1tvall>>
			,<<ucrsCabDocEncomendaSede.ebo_2tvall>>
			,<<ucrsCabDocEncomendaSede.ebo_totp1>>
			,<<ucrsCabDocEncomendaSede.ebo_totp2>>
			,<<ucrsCabDocEncomendaSede.ebo11_bins>>
			,<<ucrsCabDocEncomendaSede.ebo12_bins>>
			,<<ucrsCabDocEncomendaSede.ebo21_bins>>
			,<<ucrsCabDocEncomendaSede.ebo22_bins>>
			,<<ucrsCabDocEncomendaSede.ebo31_bins>>
			,<<ucrsCabDocEncomendaSede.ebo32_bins>>
			,<<ucrsCabDocEncomendaSede.ebo41_bins>>
			,<<ucrsCabDocEncomendaSede.ebo42_bins>>
			,<<ucrsCabDocEncomendaSede.ebo51_bins>>
			,<<ucrsCabDocEncomendaSede.ebo52_bins>>
			,<<ucrsCabDocEncomendaSede.ebo11_iva>>
			,<<ucrsCabDocEncomendaSede.ebo12_iva>>
			,<<ucrsCabDocEncomendaSede.ebo21_iva>>
			,<<ucrsCabDocEncomendaSede.ebo22_iva>>
			,<<ucrsCabDocEncomendaSede.ebo31_iva>>
			,<<ucrsCabDocEncomendaSede.ebo32_iva>>
			,<<ucrsCabDocEncomendaSede.ebo41_iva>>
			,<<ucrsCabDocEncomendaSede.ebo42_iva>>
			,<<ucrsCabDocEncomendaSede.ebo51_iva>>
			,<<ucrsCabDocEncomendaSede.ebo52_iva>>
			,<<ucrsCabDocEncomendaSede.ocupacao>>
			,'<<ucrsCabDocEncomendaSede.obs>>'
			,'ADM'
			,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
			,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8)
			,'ADM'
			,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
			,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8)
			,'<<uf_gerais_getDate(ucrsCabDocEncomendaSede.datafinal,"SQL")>>'
			,'<<ucrsCabDocEncomendaSede.ccusto>>'
			,'<<ucrsCabDocEncomendaSede.pnome>>'
			,<<ucrsCabDocEncomendaSede.pno>>
		)
	Endtext

	IF !uf_condicoesComerciais_sqlExec(lcSQL,"")
		uf_perguntalt_chama("OCORREU UM ERRO A GRAVAR O CABE�ALHO DO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE."+CHR(10)+"Cod: DOC-BO","OK","",16)
		RETURN
	Endif
	
	
	Select ucrsbo2EncomendaSede
	TEXT TO lcSql NOSHOW textmerge
		Insert into bo2
	    (
			bo2stamp
			,autotipo
		    ,pdtipo
		    ,etotalciva
		    ,armazem
		    ,ousrinis
		    ,ousrdata
		    ,ousrhora
		    ,usrinis
		    ,usrdata
		    ,usrhora
		    ,u_fostamp
		)
		
		Values 
		(
			'<<ALLTRIM(ucrsCabDocEncomendaSede.bostamp)>>'
			,1
			,1
			,<<ucrsbo2EncomendaSede.etotalciva>>
			,<<ucrsbo2EncomendaSede.armazem>>
			,'<<ucrsbo2EncomendaSede.ousrinis>>'
			,'<<uf_gerais_getDate(ucrsbo2EncomendaSede.ousrdata,"SQL")>>'
			,'<<ucrsbo2EncomendaSede.ousrhora>>'
			,'<<ucrsbo2EncomendaSede.usrinis>>'
			,'<<uf_gerais_getDate(ucrsbo2EncomendaSede.usrdata,"SQL")>>'
			,'<<ucrsbo2EncomendaSede.usrhora>>'
			,'<<ucrsbo2EncomendaSede.u_fostamp>>'
		)
	ENDTEXT
	IF !uf_condicoesComerciais_sqlExec(lcSQL,"")
		uf_perguntalt_chama("OCORREU UM ERRO A GERAR DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN
	ENDIF
	

	Select ucrsbiEncomendaSede
	Go Top
	Scan	

		lcBiStamp = uf_gerais_stamp()
		
		TEXT TO lcSql NOSHOW textmerge
				Insert into bi
				(
					bistamp
					,bostamp
					,nmdos
					,obrano
					,ref
					,codigo
					,design
					,qtt
					,qtt2
					,uni2qtt
					,u_stockact
					,iva
					,tabiva
					,armazem
					,stipo
					,ndos
					,cpoc
					,familia
					,no
					,nome
					,local
					,morada
					,codpost
					,epu
					,edebito
					,eprorc
					,epcusto
					,ettdeb
					,ecustoind
					,edebitoori
					,u_upc
					,rdata
					,dataobra
					,dataopen
					,lordem
					,lobs
					,ousrinis
					,ousrdata
					,ousrhora
					,usrinis
					,usrdata
					,usrhora
					,obistamp
					,desconto
					,desc2
					,desc3
					,desc4
				)
				Values (
					'<<ucrsbiEncomendaSede.bistamp>>'
					,'<<ALLTRIM(ucrsCabDocEncomendaSede.bostamp)>>'
					,'Encomenda de Cliente'
					,<<ucrsNumeroEncCliente.numero>>
					,'<<ucrsbiEncomendaSede.ref>>'
					,'<<ucrsbiEncomendaSede.codigo>>'
					,'<<ucrsbiEncomendaSede.design>>'
					,<<ucrsbiEncomendaSede.qtt>>
					,<<ucrsbiEncomendaSede.qtt2>>
					,<<ucrsbiEncomendaSede.uni2qtt>>
					,<<ucrsbiEncomendaSede.u_stockact>>
					,<<ucrsbiEncomendaSede.iva>>
					,<<ucrsbiEncomendaSede.tabiva>>
					,<<ucrsbiEncomendaSede.armazem>>
					,<<ucrsbiEncomendaSede.stipo>>
					,41
					,<<ucrsbiEncomendaSede.cpoc>>
					,'<<ucrsbiEncomendaSede.familia>>'
					,<<ucrsClienteNaSede.no>>
					,'<<ucrsClienteNaSede.nome>>'
					,'<<ucrsbiEncomendaSede.local>>'
					,'<<ucrsbiEncomendaSede.morada>>'
					,'<<ucrsbiEncomendaSede.codpost>>'
					,<<ucrsbiEncomendaSede.epu>>
					,<<ucrsbiEncomendaSede.edebito>>
					,<<ucrsbiEncomendaSede.eprorc>>
					,<<ucrsbiEncomendaSede.epcusto>>
					,<<ucrsbiEncomendaSede.ettdeb>>
					,<<ucrsbiEncomendaSede.ecustoind>>
					,<<ucrsbiEncomendaSede.edebitoori>>
					,<<ucrsbiEncomendaSede.u_upc>>
					,'<<uf_gerais_getDate(ucrsbiEncomendaSede.rdata,"SQL")>>'
					,'<<uf_gerais_getDate(ucrsbiEncomendaSede.dataobra,"SQL")>>'
					,'<<uf_gerais_getDate(ucrsbiEncomendaSede.dataopen,"SQL")>>'
					,<<ucrsbiEncomendaSede.lordem>>
					,'<<ucrsbiEncomendaSede.lobs>>'
					,'ADM', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8), 'ADM', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102), convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8)
					,'<<ucrsbiEncomendaSede.obistamp>>'
					,<<ucrsbiEncomendaSede.desconto>>
					,<<ucrsbiEncomendaSede.desc2>>
					,<<ucrsbiEncomendaSede.desc3>>
					,<<ucrsbiEncomendaSede.desc4>>
				)
			Endtext
			IF !uf_condicoesComerciais_sqlExec(lcSQL,"")
				uf_perguntalt_chama("OCORREU UM ERRO A GRAVAR O CABE�ALHO DO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE."+CHR(10)+"Cod: DOC-BO","OK","",16)
				RETURN .f.
			Endif
			
			TEXT TO lcSql NOSHOW textmerge
				Insert into bi2
					(
						bi2stamp
						,bostamp
						,morada
						,local
						,codpost
					)
				Values 
					(
					 	'<<ALLTRIM(lcBiStamp)>>'
					 	,'<<ALLTRIM(lcBoStamp)>>'
					 	,'<<ALLTRIM(ucrsCabDocEncomendaSede.morada)>>'
					 	,'<<ALLTRIM(ucrsCabDocEncomendaSede.local)>>'
					 	,'<<ALLTRIM(ucrsCabDocEncomendaSede.codpost)>>'
					 )
			Endtext
			IF !uf_condicoesComerciais_sqlExec(lcSQL,"")
				uf_perguntalt_chama("OCORREU UM ERRO A GERAR DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				RETURN .f.
			ENDIF
			
			** Actualiza Informa��o das tabelas de Iva de acordo com a Sede, para evitar erros ap�s importa��o dos documentos para a factura��o	
			TEXT TO lcSql NOSHOW textmerge
					update 
						bi
					set 
						bi.iva = taxasiva.taxa
						,bi.tabiva = st.tabiva
					from 
						bi (nolock)
						inner join st (nolock) on bi.ref = st.ref 
						left join taxasiva (nolock) on st.tabiva = taxasiva.codigo
					where 
						bostamp = '<<ALLTRIM(lcBoStamp)>>'
			Endtext
			IF !uf_condicoesComerciais_sqlExec(lcSQL,"")
				uf_perguntalt_chama("OCORREU UM ERRO A ATUALIZAR TAXAS DE IVA NA SEDE. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				RETURN .f.
			ENDIF
				
		ENDSCAN
	
ENDFUNC


**
FUNCTION uf_condicoesComerciais_FeeGestao
	public myFeeKeiretsu
	Local lcPct, lcPcl, lcFee, lcPCLK,lcDescontoValorK 
	lcDescontoValorK = 0 
	
	myFeeKeiretsu = 0
	uf_tecladonumerico_chama("myFeeKeiretsu", "", 0, .t., .f., 5, 2,.f.)

	Select bi
	go top
	scan
		replace bi.num1 with myFeeKeiretsu
				
		** C�lculo PCLk
		lcPct = bi.edebito
		lcPcl = bi.u_upc
		lcFee = bi.num1
		lcPCLK = lcPcl * (1+lcFee/100)
		Replace bi.binum2 with lcPCLK

		** C�lculo desconto k
		lcDescontoValorK = lcPct - lcPCLK
		lcDescontok = (lcDescontoValorK *100) / lcPct 
	
		Replace bi.binum1 with lcDescontok

	endscan
	Select bi
	go top
	
ENDFUNC


**
FUNCTION uf_condicoesComerciais_FeeGestaoLinha
	Local lcPct, lcPcl, lcFee, lcPCLK,lcDescontoValorK 
	lcDescontoValorK = 0 

	Select bi
	
	IF bi.edebito != 0			
		** C�lculo PCLk
		lcPct = bi.edebito
		lcPcl = bi.u_upc
		lcFee = bi.num1 + bi.binum3
		lcPCLK = lcPcl * (1+lcFee/100)
		Replace bi.binum2 with lcPCLK

		** C�lculo desconto k
		lcDescontoValorK = lcPct - lcPCLK
		lcDescontok = ROUND((lcDescontoValorK *100) / lcPct ,2)

		Replace bi.binum1 with lcDescontok
	ENDIF
	
ENDFUNC



**
FUNCTION uf_condicoesComerciais_CalculaFeeGestao
	local lcDesconto, lcDescontok, lcPclk, lcPcl, lcPCT
	store 0 to pclk, lcDescontok, myFeeKeiretsu

	Select bi
	go top
	scan
		replace bi.u_bonus with 0
		replace bi.desconto with bi.binum1
		
		uf_documentos_recalculaTotaisBI()
	endscan

	uf_documentos_actualizaTotaisCAB()
ENDFUNC

**
Function uf_condicoesComerciais_EFO

	** Verifica se est� no documento Encomenda de Fornecedores
	Select cabDoc
	IF cabDoc.numinternodoc!=2
		uf_perguntalt_chama("Esta funcionalidade apenas est� dispon�vel no documento: [Encomenda a Fornecedor]", "OK", "", 32)
		return .f.
	ENDIF

	uf_gerais_chamaReport('relatorio_keiretsu_EFO','&encstamp='+ALLTRIM(cabDoc.cabstamp),'excel')
ENDFUNC


**
Function uf_condicoesComerciais_OPL
	uf_gerais_chamaReport('relatorio_keiretsu_OPL','&bostamp='+ALLTRIM(cabDoc.cabstamp),'excel')
ENDFUNC


**
FUNCTION uf_condicoesComerciais_distribuicaoEncomendas
	uf_gerais_chamaReport('relatorio_keiretsu_distribuicaoEncomendas','','',.t.)
ENDFUNC

**
Function uf_condicoesComerciais_ReportRelacoes
	** Verifica se est� no documento Encomenda de Fornecedores
	Select cabDoc
	IF cabDoc.numinternodoc!=2
		uf_perguntalt_chama("Esta funcionalidade apenas est� dispon�vel no documento: [Encomenda a Fornecedor]", "OK", "", 32)
		return .f.
	ENDIF

	uf_gerais_chamaReport('relatorio_keiretsu_RelacaoEncClientes','&encstamp='+ALLTRIM(cabDoc.cabstamp) + '&site='+ ALLTRIM(mysite))

ENDFUNC





** 
Function uf_condicoesComerciais_actualizaSTCliente

	Select ucrsbiComericais
	
	IF empty(ucrsbiComericais.ref)
		return .f.
	ENDIF
	
	TEXT TO lcSql NOSHOW textmerge
		Select ref from st (nolock) where ref = '<<ucrsbiComericais.ref>>'
	ENDTEXT
	
	IF !uf_condicoesComerciais_sqlExec(lcSQL,"ucrsDadosStCliente")
		uf_perguntalt_chama("OCORREU UM ERRO A VERIFICAR EXISTENCIA DO PRODUTO NA BD DO CLIENTE. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN
	ENDIF

	Select ucrsDadosStCliente
	IF reccount("ucrsDadosStCliente")==0
		
		lcStamp = uf_gerais_stamp()
		lcRef = ALLTRIM(ucrsbiComericais.ref)
		lcDesign = strtran(ALLTRIM(ucrsbiComericais.design),chr(39),'')
		lcTabIva = ucrsbiComericais.tabIva
		
		TEXT TO lcSql NOSHOW TEXTMERGE
			INSERT INTO st
			(
				ststamp
				,ref
				,design
				,ivaincl
				,st.iva1incl
				,st.iva2incl
				,st.iva3incl
				,st.iva4incl
				,st.iva5incl
				,st.tabiva
				,ousrdata
				,ousrhora
				,ousrinis
				,usrdata
				,usrhora
				,usrinis
				,familia
				,faminome
			)
			values
			(
				'<<ALLTRIM(lcStamp)>>'
				,'<<ALLTRIM(lcRef)>>'
				,'<<ALLTRIM(lcDesign)>>'
				,1
				,1
				,1
				,1
				,1
				,1
				,<<lcTabIva>>
				,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
				,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108)
				,'<<m_chinis>>',
				convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
				,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),108)
				,'<<m_chinis>>'
				,99
				,'Outros'
		)
		ENDTEXT
		IF !uf_condicoesComerciais_sqlExec(lcSQL,"")
			uf_perguntalt_chama("OCORREU UM ERRO AO INSERIR O ARTIGO NA BD DO CLIENTE! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
			return .f.
		ENDIF
	ENDIF
ENDFUNC


**
FUNCTION uf_condicoesComerciais_descontosFornecedor
	LOCAL lcSQL
	
	IF myDocAlteracao == .f. AND myDocAlteracao == .f.
		uf_perguntalt_chama("O documento deve estar em modo de Edi��o/Altera��o.","OK","",64)
		RETURN .f.
	ENDIF

	IF EMPTY(cabDoc.no)
		uf_perguntalt_chama("Deve selecionar o fornecedor antes de usar esta funcionalidade.","OK","",64)
		RETURN .f.
	ENDIF

	Select Bi
	GO TOp
	SCAN
	
		lcSQL = ""
		TEXT TO lcSQL NOSHOW TEXTMERGE 
			select top 1 desconto,desc2, u_upc from bi (nolock) where ndos = 40 and no = <<cabDoc.no>> and estab = <<cabDoc.estab>> and ref = '<<bi.ref>>' order by dataobra desc,obrano desc
		ENDTEXT
		
		IF !uf_gerais_actgrelha("", "ucrsDescontoFornecedorCondComerc", lcSql)
			MESSAGEBOX("N�o foi possivel verificar as condi��es do fornecedor. Contacte o suporte.",16,"LOGITOOLS SOFTWARE")
			RETURN .f.
		ENDIF

		Select ucrsDescontoFornecedorCondComerc
		IF RECCOUNT("ucrsDescontoFornecedorCondComerc")>0
			Select bi
			Replace bi.desconto WITH ucrsDescontoFornecedorCondComerc.desconto
			Replace Bi.desc2 WITH ucrsDescontoFornecedorCondComerc.desc2 
			uf_documentos_recalculaTotaisBI()
			
			IF ucrsDescontoFornecedorCondComerc.desconto == 0 && Caso o PCL seja calculado atrav�s de Bonus (Fee de Gest�o)
				Replace Bi.u_upc WITH ucrsDescontoFornecedorCondComerc.u_upc 
			ENDIF
		ENDIF

	ENDSCAN
	uf_documentos_actualizatotaisCAB()
	
	uf_perguntalt_chama("Documento atualizado com sucesso.","OK","",64)

ENDFUNC


**
FUNCTION uf_condicoesComerciais_sair

	IF myCONDICOESCOMERCIAISIntroducao== .t. OR myCONDICOESCOMERCIAISAlteracao== .t.
		IF uf_perguntalt_chama("Pretende cancelar as altera��es?", "Sim", "N�o", 32)
			
			myCONDICOESCOMERCIAISIntroducao= .f.
			myCONDICOESCOMERCIAISAlteracao= .f.
			uf_CONDICOESCOMERCIAIS_alternaMenu()
		ENDIF
	ELSE
		uf_condicoesComerciais_exit()
	ENDIF

ENDFUNC


**
FUNCTION uf_condicoesComerciais_exit
	CONDICOESCOMERCIAIS.hide
	CONDICOESCOMERCIAIS.release
ENDFUNC


**
Function uf_condicoesComerciais_insereConferenciaEncClientes
	LPARAMETERS lcCabStamp
	Local lcBoStamp
	PUBLIC myTipoDoc
	myTipoDoc = "BO"
	
	** Verifica se existe pelo menos uma linha com liga��es
	TEXT To lcSQL noshow TEXTMERGE
		select * from bi where bostamp = '<<lcCabStamp>>' and bistamp in (select obistamp from bi where bostamp != '<<lcCabStamp>>')
	ENDTEXT
	
	IF !uf_gerais_actGrelha("", "ucrsVerificaExisteConferenciaEncCLiente",lcSQL)
		MESSAGEBOX("OCORREU UM ERRO A VERIFICAR EXISTENCIA DE CONFER�NCIA DE ENCOMENDA DE CLIENTE! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		return .f.
	ENDIF	
	
	IF RECCOUNT("ucrsVerificaExisteConferenciaEncCLiente") > 0
		RETURN .f.
	ENDIF
		
	regua(0,4,"A gerar Confer�ncia de Encomenda de Cliente...")
	regua(1,1,"A gerar Confer�ncia de Encomenda de Cliente...")
	
	** Constroi cursores do documento de gest�o a passar para os clientes
	TEXT To lcSQL noshow TEXTMERGE
		Select * from bo where bostamp = '<<lcCabStamp>>'
	ENDTEXT
	IF !uf_gerais_actGrelha("", "ucrsCabDocEncomendaSede",lcSQL)
		MESSAGEBOX("OCORREU UM ERRO A VERIFICAR DADOS DO DOCUMENTO! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		return .f.
	ENDIF	
	TEXT To lcSQL noshow TEXTMERGE
		Select * from bo2 where bo2stamp = '<<lcCabStamp>>'
	ENDTEXT
	IF !uf_gerais_actGrelha("", "ucrsbo2EncomendaSede",lcSQL)
		MESSAGEBOX("OCORREU UM ERRO A VERIFICAR DADOS DO DOCUMENTO! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		return .f.
	ENDIF
	TEXT To lcSQL noshow TEXTMERGE
		Select * from bi where bostamp = '<<lcCabStamp>>'
	ENDTEXT
	IF !uf_gerais_actGrelha("", "ucrsbiEncomendaSede",lcSQL)
		MESSAGEBOX("OCORREU UM ERRO A VERIFICAR DADOS DO DOCUMENTO! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		return .f.
	ENDIF
	TEXT To lcSQL noshow TEXTMERGE
		Select * from bi2 where bostamp = '<<lcCabStamp>>'
	ENDTEXT
	IF !uf_gerais_actGrelha("", "ucrsbi2EncomendaSede",lcSQL)
		MESSAGEBOX("OCORREU UM ERRO A VERIFICAR DADOS DO DOCUMENTO! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		return .f.
	ENDIF	
	regua(1,2,"A gerar Confer�ncia de Encomenda de Cliente...")
	
	
	** Actualiza Dados do Documento para os dados actuais, os dados que est�o na Encomenda de cliente n�o s�o fidedignos porque prov�m de outros sistemas
	
		
	** Cliente de Grupo / ou Keiretsu (tipo)
	Select ucrsCabDocEncomendaSede
	TEXT To lcSQL noshow TEXTMERGE
		Select tipo from b_utentes where no = <<ucrsCabDocEncomendaSede.no>> and estab = <<ucrsCabDocEncomendaSede.estab>>
	ENDTEXT
	IF !uf_gerais_actGrelha("", "ucrsDadosClienteEnc",lcSQL)
		MESSAGEBOX("OCORREU UM ERRO A VERIFICAR DADOS DO CLIENTE! POR FAVOR CONTACTE O SUPORTE.",16,"LOGITOOLS SOFTWARE")
		return .f.
	ENDIF	
	
	Select ucrsDadosClienteEnc
	IF UPPER(ALLTRIM(ucrsDadosClienteEnc.tipo)) == "KEIRETSU" OR UPPER(ALLTRIM(ucrsDadosClienteEnc.tipo)) == "GRUPO"
	
		SET HOURS TO 24
		Atime=ttoc(datetime()+(difhoraria*3600),2)
		Astr=left(Atime,8)

		Select ucrsbiEncomendaSede
		GO Top
		SCAN 	
			
			lcSQL = ""
			TEXT TO lcSQL TEXTMERGE NOSHOW
				Select 
					taxasiva.taxa
					,st.* 
				FROM
					st 
					left join taxasiva on st.tabiva = taxasiva.codigo  
				WHERE 
					st.ref = '<<ALLTRIM(ucrsbiEncomendaSede.ref)>>'
			ENDTEXT
			If !uf_gerais_actGrelha("", "ucrsExisteRef", lcSql)
				uf_perguntalt_chama("N�O FOI POSS�VEL VERIFICAR OS DADOS DO PRODUTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				RETURN .f.
			ENDIF 
			
			lcSQL = ""
			TEXT TO lcSQL TEXTMERGE NOSHOW
				exec up_keiretsu_Documentos_ActualizaCondComercForn '<<ALLTRIM(ucrsbiEncomendaSede.ref)>>'
			ENDTEXT

			If !uf_gerais_actGrelha("", "ucrsActualizaCondComercForn", lcSql)
				uf_perguntalt_chama("N�O FOI POSS�VEL VERIFICAR O PRECO A APLICAR NAS CONDICOES COMERCIAIS. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				RETURN .f.
			ENDIF 
			
			IF RECCOUNT("ucrsActualizaCondComercForn")>0
				
				Select ucrsActualizaCondComercForn
				REPLACE ucrsbiEncomendaSede.codigo 		WITH ucrsExisteRef.CODIGO
				REPLACE ucrsbiEncomendaSede.u_stockact	WITH ucrsExisteRef.STOCK
				REPLACE ucrsbiEncomendaSede.cpoc		WITH ucrsExisteRef.CPOC
				REPLACE ucrsbiEncomendaSede.familia 	WITH ucrsExisteRef.FAMILIA
				REPLACE ucrsbiEncomendaSede.no			WITH ucrsCabDocEncomendaSede.NO
				REPLACE ucrsbiEncomendaSede.NOME		WITH ucrsCabDocEncomendaSede.NOME
				REPLACE ucrsbiEncomendaSede.epu			WITH ucrsExisteRef.EPCUSTO
					
				** IVA
				REPLACE ucrsbiEncomendaSede.tabiva 		WITH ucrsExisteRef.TABIVA
				REPLACE ucrsbiEncomendaSede.Ivaincl		WITH ucrsExisteRef.ivaincl
				REPLACE ucrsbiEncomendaSede.IVA			WITH ucrsExisteRef.taxa
	
				
				Select ucrsActualizaCondComercForn
				Select ucrsbiEncomendaSede
				Replace ucrsbiEncomendaSede.edebito 	WITH ucrsActualizaCondComercForn.pclk
				REPLACE ucrsbiEncomendaSede.eprorc		WITH ucrsActualizaCondComercForn.pclk  
				REPLACE ucrsbiEncomendaSede.epcusto		WITH ucrsActualizaCondComercForn.pclk  
				REPLACE ucrsbiEncomendaSede.ettdeb		WITH ucrsActualizaCondComercForn.pclk * ucrsbiEncomendaSede.QTT
				REPLACE ucrsbiEncomendaSede.ecustoind	WITH ucrsActualizaCondComercForn.pclk
				REPLACE ucrsbiEncomendaSede.edebitoori	WITH ucrsActualizaCondComercForn.pclk
				REPLACE ucrsbiEncomendaSede.u_upc		WITH ucrsActualizaCondComercForn.pclk
				REPLACE ucrsbiEncomendaSede.rdata		WITH Date()
				REPLACE ucrsbiEncomendaSede.dataobra	WITH Date()
				REPLACE ucrsbiEncomendaSede.dataopen	WITH Date()
				REPLACE ucrsbiEncomendaSede.ousrinis	WITH m_chinis
				REPLACE ucrsbiEncomendaSede.ousrdata	WITH Date()
				REPLACE ucrsbiEncomendaSede.ousrhora	WITH Astr
				REPLACE ucrsbiEncomendaSede.usrinis		WITH m_chinis
				REPLACE ucrsbiEncomendaSede.usrdata		WITH Date()
				REPLACE ucrsbiEncomendaSede.usrhora		WITH Astr
				
				** Actualiza Dados da Ref
				TEXT TO lcSQL TEXTMERGE NOSHOW
					exec up_stocks_verDadosActuaisporarmazem '<<Alltrim(ucrsExisteRef.REF)>>',<<MYARMAZEM>>, <<ucrsCabDocEncomendaSede.NO>>, <<ucrsCabDocEncomendaSede.estab>>, <<mysite_nr>>
				ENDTEXT

				If uf_gerais_actGrelha("", "ucrsOutrosDadosRef", lcSql)
					If Reccount("ucrsOutrosDadosRef") > 0
						Select ucrsOutrosDadosRef
						Go top
						Select ucrsbiEncomendaSede
						Replace ucrsbiEncomendaSede.u_epv1act With ucrsOutrosDadosRef.epv1
						Replace ucrsbiEncomendaSede.u_stockact With ucrsOutrosDadosRef.stock
					Endif
					Fecha("ucrsOutrosDadosRef")
				ENDIF
				
				uf_documentos_recalculaTotaisBI("ucrsCabDocEncomendaSede","ucrsbiEncomendaSede")
				
			ENDIF 
		ENDSCAN 
		
		
		uf_documentos_actualizaTotaisCAB(.f.,"ucrsCabDocEncomendaSede","ucrsbiEncomendaSede","ucrsbo2EncomendaSede")

	ELSE && Clientes que n�o pertencem ao grupo, vai aplicar as difini��es actuais
	
		SET HOURS TO 24
		Atime=ttoc(datetime()+(difhoraria*3600),2)
		Astr=left(Atime,8)

		Select ucrsbiEncomendaSede
		GO Top
		SCAN 	
			
			lcSQL = ""
			TEXT TO lcSQL TEXTMERGE NOSHOW
				Select 
					taxa = ISNULL(taxasiva.taxa,0)
					,st.* 
				FROM
					st 
					left join taxasiva on st.tabiva = taxasiva.codigo  
				WHERE 
					st.ref = '<<ALLTRIM(ucrsbiEncomendaSede.ref)>>'
				
			ENDTEXT
			If !uf_gerais_actGrelha("", "ucrsExisteRef", lcSql)
				uf_perguntalt_chama("N�O FOI POSS�VEL VERIFICAR OS DADOS DO PRODUTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				RETURN .f.
			ENDIF 
											
			IF RECCOUNT("ucrsExisteRef")>0
				
				Select ucrsbiEncomendaSede
				REPLACE ucrsbiEncomendaSede.codigo 		WITH ucrsExisteRef.CODIGO
				REPLACE ucrsbiEncomendaSede.u_stockact	WITH ucrsExisteRef.STOCK
				REPLACE ucrsbiEncomendaSede.cpoc		WITH ucrsExisteRef.CPOC
				REPLACE ucrsbiEncomendaSede.familia 	WITH ucrsExisteRef.FAMILIA
				REPLACE ucrsbiEncomendaSede.no			WITH ucrsCabDocEncomendaSede.NO
				REPLACE ucrsbiEncomendaSede.NOME		WITH ucrsCabDocEncomendaSede.NOME
				REPLACE ucrsbiEncomendaSede.epu			WITH ucrsExisteRef.EPCUSTO
					
				** IVA
				REPLACE ucrsbiEncomendaSede.tabiva 		WITH ucrsExisteRef.TABIVA
				REPLACE ucrsbiEncomendaSede.Ivaincl		WITH ucrsExisteRef.ivaincl
				REPLACE ucrsbiEncomendaSede.IVA 		WITH ucrsExisteRef.taxa
				
				Select ucrsbiEncomendaSede
				Replace ucrsbiEncomendaSede.edebito 	WITH ucrsExisteRef.EPCULT
				REPLACE ucrsbiEncomendaSede.eprorc		WITH ucrsExisteRef.EPCULT
				REPLACE ucrsbiEncomendaSede.epcusto		WITH ucrsExisteRef.EPCULT
				REPLACE ucrsbiEncomendaSede.ettdeb		WITH ucrsExisteRef.EPCULT* ucrsbiEncomendaSede.QTT
				REPLACE ucrsbiEncomendaSede.ecustoind	WITH ucrsExisteRef.EPCULT
				REPLACE ucrsbiEncomendaSede.edebitoori	WITH ucrsExisteRef.EPCULT
				REPLACE ucrsbiEncomendaSede.u_upc		WITH ucrsExisteRef.EPCULT
				REPLACE ucrsbiEncomendaSede.rdata		WITH Date()
				REPLACE ucrsbiEncomendaSede.dataobra	WITH Date()
				REPLACE ucrsbiEncomendaSede.dataopen	WITH Date()
				REPLACE ucrsbiEncomendaSede.ousrinis	WITH m_chinis
				REPLACE ucrsbiEncomendaSede.ousrdata	WITH Date()
				REPLACE ucrsbiEncomendaSede.ousrhora	WITH Astr
				REPLACE ucrsbiEncomendaSede.usrinis		WITH m_chinis
				REPLACE ucrsbiEncomendaSede.usrdata		WITH Date()
				REPLACE ucrsbiEncomendaSede.usrhora		WITH Astr
				
				** Actualiza Dados da Ref
				TEXT TO lcSQL TEXTMERGE NOSHOW
					exec up_stocks_verDadosActuaisporarmazem '<<Alltrim(ucrsExisteRef.REF)>>',<<MYARMAZEM>>, <<ucrsCabDocEncomendaSede.NO>>, <<ucrsCabDocEncomendaSede.estab>>, <<mysite_nr>>
				ENDTEXT

				If uf_gerais_actGrelha("", "ucrsOutrosDadosRef", lcSql)
					If Reccount("ucrsOutrosDadosRef") > 0
						Select ucrsOutrosDadosRef
						Go top
						Select ucrsbiEncomendaSede
						Replace ucrsbiEncomendaSede.u_epv1act With ucrsOutrosDadosRef.epv1
						Replace ucrsbiEncomendaSede.u_stockact With ucrsOutrosDadosRef.stock
					Endif
					Fecha("ucrsOutrosDadosRef")
				ENDIF
				
				uf_documentos_recalculaTotaisBI("ucrsCabDocEncomendaSede","ucrsbiEncomendaSede")
				
			ENDIF 
		ENDSCAN 
		
		
		uf_documentos_actualizaTotaisCAB(.f.,"ucrsCabDocEncomendaSede","ucrsbiEncomendaSede","ucrsbo2EncomendaSede")
	
	ENDIF  

	****************************************************************
	
	** Constroi cursores do documento de gest�o a passar para os clientes
	Select ucrsCabDocEncomendaSede

	lcBoStamp = uf_gerais_stamp()

	TEXT TO lcSQL NOSHOW textmerge
		select numero = isnull(max(obrano),0)+1 from bo where ndos = 43
	Endtext
	
	IF !uf_gerais_actgrelha("", "ucrsNumeroEncCliente", lcSql)
		MESSAGEBOX("N�o foi possivel verificar o numero a atribuir � conferencia de Encomenda de Cliente. Contacte o suporte.",16,"LOGITOOLS SOFTWARE")
		RETURN .f.
	ENDIF
	
	
	Select ucrsNumeroEncCliente
		
	Select ucrsbo2EncomendaSede
	TEXT TO lcSql NOSHOW textmerge
		Insert into bo2 (
			bo2stamp
			,autotipo
			,pdtipo
			,etotiva
			,etotalciva
			,armazem
			,ousrinis
			,ousrdata
			,ousrhora
			,usrinis
			,usrdata
			,usrhora
			,u_fostamp
		)Values (
			'<<lcBoStamp>>'
			,1
			,1
			,<<ucrsbo2EncomendaSede.etotiva>>
			,<<ucrsbo2EncomendaSede.etotalciva>>
			,<<ucrsbo2EncomendaSede.armazem>>
			,'<<ucrsbo2EncomendaSede.ousrinis>>'
			,'<<uf_gerais_getDate(ucrsbo2EncomendaSede.ousrdata,"SQL")>>'
			,'<<ucrsbo2EncomendaSede.ousrhora>>'
			,'<<ucrsbo2EncomendaSede.usrinis>>'
			,'<<uf_gerais_getDate(ucrsbo2EncomendaSede.usrdata,"SQL")>>'
			,'<<ucrsbo2EncomendaSede.usrhora>>'
			, '<<ucrsbo2EncomendaSede.u_fostamp>>'
		)
	ENDTEXT

	IF !uf_gerais_actgrelha("", "", lcSql)
		uf_perguntalt_chama("OCORREU UM ERRO A GERAR DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
		RETURN
	ENDIF
	
	
	lcSQL = ""
	TEXT TO lcSQL NOSHOW textmerge
		Insert into bo
		(	bostamp
			,ndos
			,nmdos
			,obrano
			,dataobra
			,nome
			,nome2
			,no
			,estab
			,morada
			,local
			,codpost
			,tipo
			,tpdesc
			,ncont
			,boano
			,etotaldeb
			,dataopen
			,ecusto
			,etotal
			,moeda
			,memissao
			,origem
			,site
			,vendedor
			,vendnm
			,sqtt14
			,ebo_1tvall
			,ebo_2tvall
			,ebo_totp1
			,ebo_totp2
			,ebo11_bins
			,ebo12_bins
			,ebo21_bins
			,ebo22_bins
			,ebo31_bins
			,ebo32_bins
			,ebo41_bins
			,ebo42_bins
			,ebo51_bins
			,ebo52_bins
			,ebo11_iva
			,ebo12_iva
			,ebo21_iva
			,ebo22_iva
			,ebo31_iva
			,ebo32_iva
			,ebo41_iva
			,ebo42_iva
			,ebo51_iva
			,ebo52_iva
			,ocupacao
			,obs
			,ousrinis
			,ousrdata
			,ousrhora
			,usrinis
			,usrdata
			,usrhora
			,datafinal
			,ccusto
			,pnome
			,pno
			
		)Values (
			'<<lcBoStamp>>'
			,43
			,'Confirm. Entrega Cliente'
			,<<ucrsNumeroEncCliente.numero>>
			,'<<uf_gerais_getDate(getdate(),"SQL")>>'
			,'<<ucrsCabDocEncomendaSede.nome>>'
			,'ucrsCabDocEncomendaSede.nome2'
			,<<ucrsCabDocEncomendaSede.no>>
			,<<ucrsCabDocEncomendaSede.estab>>
			,'<<ALLTRIM(ucrsCabDocEncomendaSede.morada)>>'
			,'<<ALLTRIM(ucrsCabDocEncomendaSede.local)>>'
			,'<<ALLTRIM(ucrsCabDocEncomendaSede.codPost)>>'
			,'<<ALLTRIM(ucrsCabDocEncomendaSede.tipo)>>'
			,'<<Alltrim(ucrsCabDocEncomendaSede.tpdesc)>>'
			,'<<ALLTRIM(ucrsCabDocEncomendaSede.ncont)>>'
			,Year(dateadd(HOUR, <<difhoraria>>, getdate())),<<ucrsCabDocEncomendaSede.etotaldeb>>
			,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
			,<<ucrsCabDocEncomendaSede.ecusto>>
			,<<ucrsCabDocEncomendaSede.etotal>>
			,'<<ucrsCabDocEncomendaSede.Moeda>>'
			,'EURO'
			,'BO'
			,'<<ALLTRIM(mySite)>>'
			,<<ucrsCabDocEncomendaSede.vendedor>>
			,'<<ucrsCabDocEncomendaSede.vendnm>>'
			,<<ucrsCabDocEncomendaSede.sqtt14>>
			,<<ucrsCabDocEncomendaSede.ebo_1tvall>>
			,<<ucrsCabDocEncomendaSede.ebo_2tvall>>
			,<<ucrsCabDocEncomendaSede.ebo_totp1>>
			,<<ucrsCabDocEncomendaSede.ebo_totp2>>
			,<<ucrsCabDocEncomendaSede.ebo11_bins>>
			,<<ucrsCabDocEncomendaSede.ebo12_bins>>
			,<<ucrsCabDocEncomendaSede.ebo21_bins>>
			,<<ucrsCabDocEncomendaSede.ebo22_bins>>
			,<<ucrsCabDocEncomendaSede.ebo31_bins>>
			,<<ucrsCabDocEncomendaSede.ebo32_bins>>
			,<<ucrsCabDocEncomendaSede.ebo41_bins>>
			,<<ucrsCabDocEncomendaSede.ebo42_bins>>
			,<<ucrsCabDocEncomendaSede.ebo51_bins>>
			,<<ucrsCabDocEncomendaSede.ebo52_bins>>
			,<<ucrsCabDocEncomendaSede.ebo11_iva>>
			,<<ucrsCabDocEncomendaSede.ebo12_iva>>
			,<<ucrsCabDocEncomendaSede.ebo21_iva>>
			,<<ucrsCabDocEncomendaSede.ebo22_iva>>
			,<<ucrsCabDocEncomendaSede.ebo31_iva>>
			,<<ucrsCabDocEncomendaSede.ebo32_iva>>
			,<<ucrsCabDocEncomendaSede.ebo41_iva>>
			,<<ucrsCabDocEncomendaSede.ebo42_iva>>
			,<<ucrsCabDocEncomendaSede.ebo51_iva>>
			,<<ucrsCabDocEncomendaSede.ebo52_iva>>
			,<<ucrsCabDocEncomendaSede.ocupacao>>
			,'<<ucrsCabDocEncomendaSede.obs>>'
			,'ADM'
			,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
			,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8)
			,'ADM', convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
			,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8)
			,'<<uf_gerais_getDate(ucrsCabDocEncomendaSede.datafinal,"SQL")>>'
			,'<<ucrsCabDocEncomendaSede.ccusto>>'
			,'<<ucrsCabDocEncomendaSede.pnome>>'
			,<<ucrsCabDocEncomendaSede.pno>>
		)
	Endtext

	IF !uf_gerais_actgrelha("", "", lcSql)
		uf_perguntalt_chama("OCORREU UM ERRO A GRAVAR O CABE�ALHO DO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE."+CHR(10)+"Cod: DOC-BO","OK","",16)
		RETURN
	Endif
	
	
	Select ucrsbiEncomendaSede
	Go Top
	Scan	

		lcBiStamp = uf_gerais_stamp()
		
		TEXT TO lcSql NOSHOW textmerge
				Insert into bi
				(
					bistamp
					,bostamp
					,nmdos
					,obrano
					,ref
					,codigo
					,design
					,qtt
					,qtt2
					,uni2qtt
					,u_stockact
					,iva
					,tabiva
					,armazem
					,stipo
					,ndos
					,cpoc
					,familia
					,no
					,nome
					,local
					,morada
					,codpost
					,epu
					,edebito
					,eprorc
					,epcusto
					,ettdeb
					,ecustoind
					,edebitoori
					,u_upc
					,rdata
					,dataobra
					,dataopen
					,lordem
					,lobs
					,ousrinis
					,ousrdata
					,ousrhora
					,usrinis
					,usrdata
					,usrhora
					,obistamp
					,desconto
					,desc2
					,desc3
					,desc4
					,ivaincl
				)
				Values (
					'<<lcBiStamp>>'
					,'<<lcBoStamp>>'
					,'Confirm. Entrega Cliente'
					,<<ucrsNumeroEncCliente.numero>>
					,'<<ucrsbiEncomendaSede.ref>>'
					,'<<ucrsbiEncomendaSede.codigo>>'
					,'<<ucrsbiEncomendaSede.design>>'
					,<<ucrsbiEncomendaSede.qtt>>
					,0
					,<<ucrsbiEncomendaSede.uni2qtt>>
					,<<ucrsbiEncomendaSede.u_stockact>>
					,<<ucrsbiEncomendaSede.iva>>
					,<<ucrsbiEncomendaSede.tabiva>>
					,<<ucrsbiEncomendaSede.armazem>>
					,<<ucrsbiEncomendaSede.stipo>>
					,43
					,<<ucrsbiEncomendaSede.cpoc>>
					,'<<ucrsbiEncomendaSede.familia>>'
					,<<ucrsCabDocEncomendaSede.no>>
					,'<<ucrsCabDocEncomendaSede.nome>>'
					,'<<ucrsbiEncomendaSede.local>>'
					,'<<ucrsbiEncomendaSede.morada>>'
					,'<<ucrsbiEncomendaSede.codpost>>'
					,<<ucrsbiEncomendaSede.epu>>
					,<<ucrsbiEncomendaSede.edebito>>
					,<<ucrsbiEncomendaSede.eprorc>>
					,<<ucrsbiEncomendaSede.epcusto>>
					,<<ucrsbiEncomendaSede.ettdeb>>
					,<<ucrsbiEncomendaSede.ecustoind>>
					,<<ucrsbiEncomendaSede.edebitoori>>
					,<<ucrsbiEncomendaSede.u_upc>>
					,'<<uf_gerais_getDate(ucrsbiEncomendaSede.rdata,"SQL")>>'
					,'<<uf_gerais_getDate(ucrsbiEncomendaSede.dataobra,"SQL")>>'
					,'<<uf_gerais_getDate(ucrsbiEncomendaSede.dataopen,"SQL")>>'
					,<<ucrsbiEncomendaSede.lordem>>
					,'<<ucrsbiEncomendaSede.lobs>>'
					,'ADM'
					,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
					,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8)
					,'ADM'
					,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),102)
					,convert(varchar,dateadd(HOUR, <<difhoraria>>, getdate()),8)
					,'<<ucrsbiEncomendaSede.bistamp>>'
					,<<ucrsbiEncomendaSede.desconto>>
					,<<ucrsbiEncomendaSede.desc2>>
					,<<ucrsbiEncomendaSede.desc3>>
					,<<ucrsbiEncomendaSede.desc4>>	
					,<<IIF(ucrsbiEncomendaSede.ivaincl,1,0)>>	
				)
			ENDTEXT
			
			IF !uf_gerais_actgrelha("", "", lcSql)
				uf_perguntalt_chama("OCORREU UM ERRO A GRAVAR O CABE�ALHO DO DOCUMENTO. POR FAVOR CONTACTE O SUPORTE."+CHR(10)+"Cod: DOC-BO","OK","",16)
				RETURN .f.
			Endif
			
			TEXT TO lcSql NOSHOW textmerge
				Insert into bi2(
					bi2stamp
					,bostamp
					,morada
					,local
					,codpost
				)Values (
					'<<ALLTRIM(lcBiStamp)>>'
					,'<<ALLTRIM(lcBoStamp)>>'
					,'<<ALLTRIM(ucrsCabDocEncomendaSede.morada)>>'
					,'<<ALLTRIM(ucrsCabDocEncomendaSede.local)>>'
					,'<<ALLTRIM(ucrsCabDocEncomendaSede.codpost)>>'
				)
			ENDTEXT
	
			IF !uf_gerais_actgrelha("", "", lcSql)
				uf_perguntalt_chama("OCORREU UM ERRO A GERAR DOCUMENTO. POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				RETURN .f.
			ENDIF
				
		ENDSCAN
		
		
		
		uf_gerais_gravaUltRegisto('BO', ALLTRIM(lcBoStamp))
	regua(2)
ENDFUNC