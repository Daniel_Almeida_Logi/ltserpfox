**	Configura cada container e o container principal para fazer scroll
FUNCTION uf_listareservas_confGridLista
	LOCAL lcObj, lcNumReg, lcCor, lcCursor 
	
	lcCursor = 'uCrsListaReservas'
	
	STORE 0 TO lcNumReg
	
	SELECT  &lcCursor
	COUNT TO lcNumReg	
	
	lcObj = 'reservas.pageframe1.page2'
	
	&& Cor do PVP e mascara
	FOR i=1 TO &lcobj..grid1.columncount		
		&lcobj..grid1.pvpact.dynamicForeColor	=	"IIF(&lcCursor..pvpact != &lcCursor..pvpres,Rgb[255,0,0],rgb[0,0,0])"
		&lcobj..grid1.Columns(i).dynamicForeColor = "IIF((&lcCursor..fechada == .t. OR &lcCursor..qttComp > 0), Rgb[0,128,0], Rgb[0,0,0])"	
		&lcobj..grid1.Columns(i).dynamicBackColor = "IIF(&lcCursor..row % 2 != 0,Rgb[255,255,255],rgb[240,240,240])"
	ENDFOR
		
	SELECT  &lcCursor
	GO TOP 
	&lcObj..grid1.refresh
ENDFUNC



FUNCTION uf_listaReservas_selTodos 
	LPARAMETERS lcBool
	LOCAL lcNomeMed, lcTotal
	STORE '' TO lcNomeMed
	STORE 0 TO lcTotal;
	
	IF(myPainelAnterior=="documentos")
		SELECT * FROM uCrsFnAux INTO CURSOR lcCursor
	ENDIF
	
	SELECT uCrsListaReservas
	GO TOP
	IF lcBool
		RESERVAS.menu1.selTodos.config("Des. Todos",myPath + "\imagens\icons\checked_w.png","uf_listaReservas_selTodos with .f.","T")
		replace ALL uCrsListaReservas.sel WITH .t.
	ELSE
		RESERVAS.menu1.selTodos.config("Sel. Todos",myPath + "\imagens\icons\unchecked_w.png","uf_listaReservas_selTodos with .t.","T")
		replace ALL uCrsListaReservas.sel WITH .f.
	ENDIF 
	
	
	**informa maximo de reserva atingida
	
	IF(lcBool==.t.)
	
		SELECT lcCursor
		GO TOP
		SCAN	
			lcRef=ALLTRIM(lcCursor.ref)
				
			SELECT  count(*) as totalqtt  from uCrsListaReservas WHERE  uCrsListaReservas.ref=lcRef  and sel=.t. INTO CURSOR lcCursorAux

			SELECT  lcCursorAux
				IF(lcCursorAux.totalqtt >lcCursor.qtt)
						lcTotal=lcTotal + 1
						lcNomeMed = lcNomeMed + ALLTRIM(lcCursor.Design) + CHR(13) 
						
				ENDIF
			
			IF(USED("lcCursorAux"))
				fecha("lcCursorAux")
			ENDIF
		
		ENDSCAN	
		
	ENDIF
	
	**se tem alerta para mostrar
	
	IF LEN(lcNomeMed)>1
		If(lcTotal==1)
			uf_perguntalt_chama("Est� a seleccionar mais clientes que a quantidade rececionada para o seguinte medicamento:"+ CHR(13)+lcNomeMed,"OK","",64)
		ELSE
			uf_perguntalt_chama("Est� a seleccionar mais clientes que a quantidade rececionada para os seguintes medicamentos:"+ CHR(13)+lcNomeMed,"OK","",64)
		ENDIF
		
	ENDIF
	
	

	IF(USED("lcCursor"))
		fecha("lcCursor")
	ENDIF
	
		
	SELECT uCrsListaReservas
	GO TOP
	
	

	RESERVAS.refresh()
ENDFUNC 





FUNCTION  uf_listaReservas_selBackoffice
	LPARAMETERS lcCursor
	LOCAL lcPosListaReservas, lcRefAux, lcSel, lcSumQtt
	LOCAL lcRefAux
	
	STORE 0 TO lcSumQtt
	** lcCursor -> ucrsFnAux
	
	SELECT uCrsListaReservas
	lcPosListaReservas= RECNO("uCrsListaReservas")	
	lcRefAux=ALLTRIM(uCrsListaReservas.ref)
	lcSel=uCrsListaReservas.sel


	**Apenas � valida se selecionar a linha
	

	IF(lcSel==.t.)
		&&:TODO
	
		SELECT  ucrsFnAux
		GO TOP
		SCAN FOR ALLTRIM(ucrsFnAux.ref)==lcRefAux 		

			LOCAL lcNomeProd
			lcNomeProd=ALLTRIM(ucrsFnAux.Design)
			LOCAL lcQttFn 
			lcQttFn = ucrsFnAux.qtt
			
			SELECT uCrsListaReservas
			GO TOP
			CALCULATE SUM(uCrsListaReservas.qtt) FOR  ALLTRIM(uCrsListaReservas.ref)=lcRefAux and uCrsListaReservas.sel=.T. TO lcSumQtt
			

			IF(lcSumQtt>lcQttFn )
				uf_perguntalt_chama("Est� a seleccionar mais clientes que a quantidade rececionada para o seguinte medicamento:"+ CHR(13)+lcNomeProd,"OK","",64)
				
			ENDIF
			IF(USED("lcCursorLinhasAux"))
				fecha("lcCursorLinhasAux")
			ENDIF
			
			
		SELECT  ucrsFnAux	
		ENDSCAN
	
	ENDIF
			
	
	SELECT uCrsListaReservas
	GOTO  lcPosListaReservas


ENDFUNC


**	Fun��o que adiciona um produto ao atendimento	
FUNCTION uf_listaReservas_Sel
	LOCAL lcCursor

	lcCursor = 'uCrsListaReservas'
	
	atendimento.lockscreen = .t.
	
	atendimento.grdAtend.setfocus()
	
	LOCAL lcPos, lnObrano, lcCodComp
	lnObrano=0
	lcCodComp = ""
	
	&& Valida��o do cursor
	IF EMPTY(lcCursor)
		uf_perguntalt_chama("O PAR�METRO: LCCURSOR, N�O PODE SER VAZIO! POR FAVOR CONTACTE O SUPORTE.","OK","",48)
		atendimento.lockscreen = .f.
		RETURN .f.
	ENDIF
	SELECT &lcCursor
	SCAN
		&&validar se a linha se encontra fechada
		SELECT &lcCursor
		IF &lcCursor..sel AND (&lcCursor..fechada OR &lcCursor..qttComp > 0)
			uf_perguntalt_chama("Este produto j� foi inclu�do numa receita ou j� se encontra regularizado. N�o pode ser inclu�do neste atendimento."+CHR(13)+CHR(13)+"Nota: se todos os produtos j� estiverem regularizados dever� fechar a Reserva.","OK","",48)
			replace &lcCursor..sel WITH .f.
			atendimento.lockscreen = .f.
			RETURN .f.
		ENDIF
		**
		
		LOCAL lcValidaPreco, lcValidaAplicaComp
		STORE .T. TO lcValidaPreco
		
		LOCAL lcPosFi, lnDesc
		STORE 0 TO lcPosFI
		lnDesc = 0
		
		SELECT &lcCursor
		IF &lcCursor..sel
			SELECT &lcCursor
			lcStampBi = &lcCursor..bistamp
			SELECT FI
			*lcPosFi = RECNO("fi")
			SELECT fi
			LOCATE FOR ALLTRIM(FI.BISTAMP) == ALLTRIM(lcStampBi)
			IF FOUND("FI")
				lcValidaExiste = .t.
			ELSE
				lcValidaExiste = .f.
			ENDIF 
			
			IF !lcValidaExiste
				SELECT fi
				GO BOTTOM 
			
				&& Adiciona Venda por cada comparticipa��o
				IF !(ALLTRIM(&lcCursor..codComp)==ALLTRIM(lcCodComp)) AND !EMPTY(lcCodComp) &&AND EMPTY(ALLTRIM(&lcCursor..stampAdiantamento)) 
					select fi
					GO BOTTOM 
					uf_Atendimento_novaVenda(.f.)
				ENDIF
				**
				
				lcCodComp = ALLTRIM(&lcCursor..codComp)			
			
				&&valida se j� est� paga
				SELECT &lcCursor
				IF !EMPTY(&lcCursor..stampAdiantamento)
					lcFiStampAdiant = &lcCursor..stampAdiantamento
					lcSQL = ''
					TEXT TO lcSQL TEXTMERGE NOSHOW
						exec up_reservas_regularizarAdiantamento '<<lcFiStampAdiant>>'
					ENDTEXT
					
					uf_gerais_actGrelha("","uCrsValeReserva",lcSQL)
					IF USED("ucrsVale")
						SELECT * FROM uCrsVale UNION ALL SELECT * FROM uCrsValeReserva INTO CURSOR ucrsVale2 READWRITE 
					ELSE
						SELECT * FROM uCrsValeReserva INTO CURSOR ucrsVale2 READWRITE
					ENDIF		
					
					select ucrsvale2
					
					uf_regvendas_regVendasSel(.t., IIF(&lcCursor..obrano==lnObrano,.t.,.f.))
			
					lnObrano = &lcCursor..obrano
			
					select uCrsValeReserva
					select fi
					LOCATE FOR ALLTRIM(fi.ofistamp) == ALLTRIM(lcFiStampAdiant) 
					IF FOUND() && Alterado dia 23/10/2014
						replace fi.design WITH "  " + uCrsValeReserva.design IN "FI"
						replace fi.bistamp WITH &lcCursor..bistamp IN "FI"
						lnDesc = fi.u_descval
					ENDIF
					
					*replace fi.u_descval WITH 0
					uf_atendimento_devolver()
					fecha("uCrsValeReserva")
					fecha("uCrsVale2")
					
				ENDIF 

				&&	
				IF !EMPTY(ALLTRIM(&lcCursor..codComp))
					lcValidaAplicaComp = .t.
				ELSE
					lcValidaAplicaComp = .f.
				ENDIF
					
				&& Valida��o do PVPACT VS PVPres
				IF &lcCursor..PVPRES != &lcCursor..PVPACT
					
					&& Workaround ao lost focus do formulario (nao pode ser modal)
					PUBLIC myLstReservaVar
					STORE 1 TO myLstReservaVar
					
					IF uf_perguntalt_chama("O PRE�O DO ARTIGO FOI ALTERADO DESDE QUE FOI FEITA A RESERVA:";
								+CHR(13)+CHR(13)+ "PVP DE RESERVA: " +ASTR(&lcCursor..PVPRES,8,2) +" �";
								+CHR(13)+"PVP ATUAL: "+ASTR(&lcCursor..PVPACT,8,2) +" �";
								+CHR(13)+"Qual PVP pretende utilizar?","Reserva","Atual")
								
						lcValidaPreco = .F.	
					ENDIF
					
					&& liberta variavel criada
					RELEASE myLstReservaVar
				ENDIF

*!*					&& Adiciona Venda por cada comparticipa��o
*!*					IF !(ALLTRIM(&lcCursor..codComp)==ALLTRIM(lcCodComp)) AND EMPTY(ALLTRIM(&lcCursor..stampAdiantamento)) 
*!*						select fi
*!*						GO BOTTOM 
*!*						uf_Atendimento_novaVenda(.f.)
*!*					ENDIF
*!*					**
*!*					
*!*					lcCodComp = ALLTRIM(&lcCursor..codComp)			
				
				&& Adiciona � venda - C�digo proveniente do atendimento
				Select FI
				GO BOTTOM 
				Select FI
				uf_atendimento_AdicionaLinhaFi(.F.,.F.)	

				SELECT fi
				lcPosFi = RECNO("fi")
				
				Select &lcCursor
				Select FI
				replace fi.ref with &lcCursor..ref

				SELECT fi

				uf_atendimento_actRef()
				
				**Verifica se coloca linha para preencher informa��o da embalagem
		        SELECT fi 
		        lcRef = ALLTRIM(fi.ref)
		        lcSQL = ''
		        TEXT TO lcSQL NOSHOW TEXTMERGE 
		            exec up_stocks_procuraRef '<<Alltrim(lcRef)>>', <<mysite_nr>>
		        ENDTEXT 
		        uf_gerais_actGrelha("","uCrsFindRef",lcSQL)
		        lcArvato= uCrsFindRef.dispositivo_seguranca
		        
		        IF lcArvato
		            uf_insert_fi_trans_info_seminfo()
		            Atendimento.pgfDados.page2.txtRef.disabledbackcolor = RGB(255,0,0)
		            Atendimento.pgfDados.page2.txtRef.forecolor = RGB(255,255,255)
		        ENDIF 
		        fecha("uCrsFindRef")
        

				SELECT fi
				TRY 
					GO lcPosFi
				CATCH
					GO bottom
				ENDTRY

				&& Muda PVP 
				IF lcValidaPreco
				    && PVP ACTUAL
					replace fi.amostra	with	.F.	IN "FI"
				ELSE
					&& PVP RESERVA
					SELECT &lcCursor
					SELECT  fi
					replace fi.u_epvp	with	ROUND(&lcCursor..PVPRES,2) IN "FI"
					replace fi.amostra	with	.T. IN "FI"
				ENDIF 
				**
							
				&& Replace do bistamp
				SELECT &lcCursor
				SELECT FI
				replace fi.bistamp	WITH &lcCursor..bistamp IN "FI"
				replace fi.qtt		WITH &lcCursor..qtt IN "FI"
				
				&& Alterado dia 23/10/2014
				replace fi.u_descval WITH &lcCursor..descval + lnDesc IN "FI"
				replace fi.desconto WITH &lcCursor..desconto IN "FI"
				
				&& Diploma
				replace fi.u_diploma	WITH	ALLTRIM(&lcCursor..diploma) IN "FI"
				
				&&Exepcoes
				replace fi.lobs3		WITH	ALLTRIM(&lcCursor..exepcaoTrocaMed) IN "FI"
			
				&& actualiza ecra atendimento
				uf_atendimento_fiiliq()

				SELECT fi
				TRY 
					GO lcPosFi
				CATCH
					GO bottom
				ENDTRY
				
				uf_atendimento_eventoFi()

				SELECT fi
				TRY 
					GO lcPosFi
				CATCH
					GO bottom
				ENDTRY
				uf_atendimento_dadosST()
				
				SELECT fi
				TRY 
					GO lcPosFi
				CATCH
					GO bottom
				ENDTRY
				uf_atendimento_infoTotais()
				
				SELECT fi
				TRY 
					GO lcPosFi
				CATCH
					GO bottom
				ENDTRY
				
				IF !EMPTY(&lcCursor..stampAdiantamento)	
			
					IF !EMPTY(ALLTRIM(uf_atendimento_verificaCompart()))

						Do while !BOF("FI")
							If Left(fi.design,1) == "."
								If Left(fi.design,2) != ".S"
									IF Right(alltrim(fi.design),10)=="*SUSPENSA*" OR Right(alltrim(fi.design),18)=="*SUSPENSA RESERVA*"
										*j� est� suspensa - n�o faz nada
									ELSE 
										lcStampFiVerSusp = &lcCursor..stampAdiantamento
										lcSQL = ''
										TEXT TO lcSQL TEXTMERGE NOSHOW
											DECLARE @stampft char(25)
											
											SELECT @stampft = ftstamp FROM fi (nolock) WHERE fistamp = '<<ALLTRIM(lcStampFiVerSusp)>>'
											SELECT cobrado FROM ft WHERE ftstamp = @stampft
										ENDTEXT 

										uf_gerais_actGrelha("","crsVerSuspTmp",lcSQL)
										SELECT crsVerSuspTmp
										IF crsVerSuspTmp.cobrado == .t.
											&&a venda j� foi dada como suspensa, n�o fazer nada, deixar a farmacia decidir
											*replace fi.design with alltrim(fi.design) + " *SUSPENSA*" IN "FI"
										ELSE
											replace fi.design with alltrim(fi.design) + " *SUSPENSA RESERVA*" IN "FI"
										ENDIF 
									ENDIF
									Exit
								EndIf
								Exit
							ENDIF
							TRY
								Skip -1
							CATCH 
							ENDTRY
						ENDDO
					ENDIF 
				ENDIF 
				
				SELECT fi
				TRY 
					GO lcPosFi
				CATCH
					GO bottom
				ENDTRY
			
				IF lcValidaAplicaComp
					SELECT fi
					Do while !Bof("FI")
						SELECT fi
						If Left(fi.design,1) == "."
						
							IF uf_atendimento_verificaCompart() == ""
								uf_atendimento_aplicaCompart(ALLTRIM(&lcCursor..codComp),.T.,.t.) && 3� parametro indica que vem de reserva para n�o limpar diploma nas linhas
							ENDIF 
							
							IF uf_gerais_getParameter("ADM0000000222","BOOL") AND !uf_atendimento_verificaSuspensa()
								uf_atendimento_SuspendeVenda(.t.)
							ENDIF 
							
							Exit
						ENDIF
						TRY
							Skip -1
						CATCH 
						ENDTRY
					ENDDO 
				ENDIF 
						
				SELECT fi
				TRY 
					GO lcPosFi
				CATCH
					GO bottom
				ENDTRY
			ENDIF
			
		ELSE && REMOVE LINHA DO ATENDIMENTO
			LOCAL lcPosFIRes
			SELECT FI
			GO TOP
			SCAN FOR ALLTRIM(FI.BISTAMP) == ALLTRIM(&lcCursor..bistamp)
				lcPosFIRes = RECNO("FI")
				uf_atendimento_apagaLinha()
				GO TOP 
			ENDSCAN
		ENDIF
		
		SELECT &lcCursor
	ENDSCAN
	atendimento.lockscreen =  .f.
	reservas.show
ENDFUNC 


**
FUNCTION uf_listaReservas_navegaBO
	LPARAMETERS lcCursor
	
	IF EMPTY(ALLTRIM(lcCursor))
		uf_perguntalt_chama("RESERVAS: O PAR�METRO N�O PODE SER VAZIO!","OK","",48)
		RETURN .F.
	ENDIF
	
	uf_documentos_chama(Alltrim(&lcCursor..bostamp))
	uf_Reservas_sair()
	
	TRY 
		documentos.show()
	CATCH
		***
	ENDTRY
ENDFUNC 


**
FUNCTION uf_listareservas_fecha
	
	select ucrslistareservas
	lnNumReserva = ucrslistareservas.obrano
	select ucrslistareservas
	locate for ucrslistareservas.obrano == lnNumReserva and sel == .t.
	if found()
		uf_perguntalt_chama("N�o pode fechar uma reserva com linha(s) seleccionada(s) ou inclu�da(s) no atendimento.","OK","",48)
		RETURN .f.
	endif
	select ucrslistareservas
	locate for ucrslistareservas.obrano == lnNumReserva
	
	LOCAL lcCursor
	lcCursor = 'uCrsListaReservas'
	
	IF uf_perguntalt_chama("Quer mesmo fechar a Reserva n� "+astr(uCrsListaReservas.obrano)+"? "+ CHR(13) + "Deixar� de estar dispon�vel para utilizar no atendimento.","Sim","N�o")
		LOCAL lcVal
		lcVal = .f.
		IF uf_gerais_getParameter("ADM0000000066","BOOL") == .t. 
			** guardar pass **
			IF !EMPTY(uf_gerais_getParameter("ADM0000000066","TEXT"))
				lcPass = uf_gerais_getParameter("ADM0000000066","TEXT")
			ENDIF
			** pedir password **
			PUBLIC passFechar
			STORE '' TO passFechar

			**chama Painel Virtual, funcoes genericas
			uf_tecladoAlpha_Chama("passFechar","INTRODUZA PASSWORD DE SUPERVISOR DE DOCUMENTOS:",.t.,.f.,0)

			IF !EMPTY(passFechar)
				cval=ALLTRIM(passFechar)
			ELSE
				cval= ""
			ENDIF 

			RELEASE passFechar
			
			DO CASE
				CASE empty(cval) Or Empty(lcPass)
					uf_perguntalt_chama("DEVE INTRODUZIR A PASSWORD DE SUPERVISOR PARA REALIZAR ESTA OPERA��O.","OK","",48)
				CASE !(Upper(Alltrim(lcPass))==Upper(Alltrim(cval)))
					uf_perguntalt_chama("A PASSWORD N�O EST� CORRECTA. POR FAVOR VERIFIQUE SE EST� A INTRODUZIR CORRECTAMENTE.","OK","",16)
				OTHERWISE
					lcVal = .t.
			ENDCASE
		ELSE
			lcVal = .t.
		ENDIF
		
		IF lcVal == .t.
			IF !uf_gerais_actGrelha("","","UPDATE BO SET Fechada = 1 WHERE Bostamp = '" + Alltrim(&lcCursor..bostamp) + "'") OR !uf_gerais_actGrelha("","","UPDATE BI SET Fechada = 1 WHERE Bostamp = '" + Alltrim(&lcCursor..bostamp) + "'")
				uf_perguntalt_chama("N�O FOI POSS�VEL ALTERAR O ESTADO DO DOCUMENTO! POR FAVOR CONTACTE O SUPORTE.","OK","",16)
				RETURN .f.
			ELSE
				&&tratar adiantamentos
				LOCAL lnObrano
				SELECT &lcCursor
				lnObrano = &lcCursor..obrano
				SELECT &lcCursor
				GO TOP 
				SCAN FOR &lcCursor..obrano == lnObrano
					IF !EMPTY(&lcCursor..stampAdiantamento)
						SELECT fi
						GO BOTTOM 
						SELECT &lcCursor
						lcFiStampAdiant = &lcCursor..stampAdiantamento
						lcSQL = ''
						TEXT TO lcSQL TEXTMERGE NOSHOW
							exec up_reservas_regularizarAdiantamento '<<lcFiStampAdiant>>'
						ENDTEXT 
						uf_gerais_actGrelha("","uCrsValeReserva",lcSQL)

						IF USED("ucrsVale")
							SELECT * FROM ucrsVale UNION ALL SELECT * FROM uCrsValeReserva INTO CURSOR ucrsVale2 READWRITE 
						else
							SELECT * FROM uCrsValeReserva INTO CURSOR ucrsVale2 READWRITE
						ENDIF
						select ucrsvale2
						uf_regvendas_regVendasSel(.t.,.t.)
						select uCrsValeReserva
						select fi
						LOCATE FOR ALLTRIM(fi.ofistamp) == ALLTRIM(lcFiStampAdiant)
						replace fi.design WITH "  " + uCrsValeReserva.design IN "FI"
						replace fi.bistamp WITH &lcCursor..bistamp IN "FI"
						lnDesc = fi.u_descval
						uf_atendimento_devolver()
						fecha("uCrsValeReserva")
						fecha("uCrsVale2")
					ENDIF 
				ENDSCAN
				
				uf_Reservas_sair()
			ENDIF
		ENDIF
	ENDIF
ENDFUNC 

FUNCTION uf_ListaReservas_alterarQttRecebida

	LPARAMETERS lcCursor
	
	
	uf_tecladonumerico_chama(lcCursor + ".qttRec ", "Introduza a Quantidade Recebida:", 2, .f., .f., 6, 0)
	

ENDFUNC 



**
FUNCTION uf_listareservas_alterarQtt 
	LPARAMETERS lcCursor
	
	LOCAL lcQttAnt, lcValida
	STORE .f. TO lcValida
	
	SELECT &lcCursor
	lcQttAnt = &lcCursor..qtt
	
	uf_tecladonumerico_chama(lcCursor + ".qtt", "Introduza a Quantidade:", 2, .f., .f., 6, 0)
	
	SELECT &lcCursor
	IF &lcCursor..qtt != lcQttAnt
		IF &lcCursor..qtt > lcQttAnt
			uf_perguntalt_chama("N�o pode alterar a quantidade para um valor superior ao que foi reservado.","OK","",48)
			lcValida = .t.
		ENDIF
		
		IF &lcCursor..qtt == 0
			uf_perguntalt_chama("N�o pode alterar a quantidade para 0 (zero).","OK","",48)
			lcValida = .t.
		ENDIF
	ENDIF
	
	IF lcValida
		SELECT &lcCursor
		replace &lcCursor..qtt WITH lcQttAnt
	ENDIF
ENDFUNC 


**
FUNCTION uf_listareservas_fechaLinha
	LOCAL lcCursor
	
	lcCursor = 'uCrsListaReservas'
	
	SELECT &lcCursor
	IF &lcCursor..sel
		uf_perguntalt_chama("N�o pode fechar uma linha j� inclu�da no atendimento.","OK","",48)
		RETURN .f.
	ENDIF 
	
	IF uf_perguntalt_chama("Vai fechar a linha na reserva de forma permanente." + CHR(13) + CHR(13) + "Tem a certeza que pretende continuar?","Sim","N�o")
		LOCAL lcSQL, lcBIstamp, lcBOstamp
		
		&&valida se j� est� paga
		SELECT &lcCursor
		IF !EMPTY(&lcCursor..stampAdiantamento)
			SELECT fi
			GO BOTTOM 
			SELECT &lcCursor
			lcFiStampAdiant = &lcCursor..stampAdiantamento
			lcSQL = ''
			TEXT TO lcSQL TEXTMERGE NOSHOW
				exec up_reservas_regularizarAdiantamento '<<lcFiStampAdiant>>'
			ENDTEXT 
			uf_gerais_actGrelha("","uCrsValeReserva",lcSQL)
			IF USED("ucrsVale")
				select * FROM ucrsVale UNION ALL SELECT * FROM uCrsValeReserva INTO CURSOR ucrsVale2 READWRITE 
			else
				SELECT * FROM uCrsValeReserva INTO CURSOR ucrsVale2 READWRITE
			ENDIF
			select ucrsvale2
			uf_regvendas_regVendasSel(.t.)
			select uCrsValeReserva
			select fi
			LOCATE FOR ALLTRIM(fi.ofistamp) == ALLTRIM(lcFiStampAdiant)
			replace fi.design WITH "  " + uCrsValeReserva.design IN "FI"
			replace fi.bistamp WITH &lcCursor..bistamp IN "FI"
			lnDesc = fi.u_descval
			uf_atendimento_devolver()
			fecha("uCrsValeReserva")
			fecha("uCrsVale2")
		ENDIF 
			
		SELECT &lcCursor
		lcBIstamp = &lcCursor..bistamp
		lcBOstamp = &lcCursor..bostamp
		
		lcSQL = ''
		TEXT TO lcSQL TEXTMERGE NOSHOW 
			UPDATE bi
			SET fechada = 1
			where bistamp = '<<lcBIstamp>>'
			
			DECLARE @nRowsF numeric(5,0)
			
			SELECT @nRowsF = COUNT(bistamp) FROM bi (nolock) WHERE fechada = 0 AND bostamp = '<<lcBOstamp>>'
			
			IF @nRowsF = 0
			BEGIN 
				UPDATE bo
				SET fechada = 1
				where bostamp = '<<lcBOstamp>>'
			END
		ENDTEXT
		
		uf_gerais_actGrelha("","",lcSQL)
		
		SELECT &lcCursor
		DELETE
	ELSE
		RETURN .f.
	ENDIF
	
	SELECT &lcCursor
	COUNT FOR NOT DELETED() TO nActive

	IF nActive == 0
		uf_Reservas_sair()
	ELSE 
		reservas.refresh()
	ENDIF 
	
	RETURN .t.
ENDFUNC

FUNCTION uf_listareservas_filtraQtt
	LPARAMETERS uv_filtra

	IF uv_filtra

		SELECT uCrsListaReservas
		SET FILTER TO uCrsListaReservas.qttRec > 0

		RESERVAS.menu1.filtrar.config("Qt.Rec >0",myPath + "\imagens\icons\checked_w.png","uf_listareservas_filtraQtt with .f.","T")

	ELSE

		SELECT uCrsListaReservas
		SET FILTER TO 

		RESERVAS.menu1.filtrar.config("Qt.Rec >0",myPath + "\imagens\icons\unchecked_w.png","uf_listareservas_filtraQtt with .t.","T")

	ENDIF

	RESERVAS.refresh()

ENDFUNC